package com.pilog.t8.definition.cache;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionFileHandler;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.T8L2DefinitionCache;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8FileDefinitionCache implements T8L2DefinitionCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FileDefinitionCache.class);

    private final String cacheIdentifier;
    private final Map<String, String> definitionMap; // Key: Definition Identifier, Value: Serialized Definition Strings.
    private final Map<String, String> typeMap; // Key: Definition Identifier, Value: Definition Type Identifier.
    private final Map<String, T8DefinitionMetaData> metaDataMap; // Key: Definition Identifier, Value: Definition Meta Data Object.
    private final Set<String> groupIdentifierSet; // Contains the identifiers of all existing groups.
    private final T8ServerContext serverContext;
    private final T8DefinitionSerializer serializer;
    private final T8DefinitionManager definitionManager;
    private final Set<String> storedTypes;
    private File contextPath;

    public T8FileDefinitionCache(T8Context context, T8DefinitionManager definitionManager, File contextPath, String identifier, Set<String> typeIdentifiers)
    {
        this.serverContext = context.getServerContext();
        this.definitionManager = definitionManager;
        this.serializer = new T8DefinitionSerializer(definitionManager);
        this.cacheIdentifier = identifier;
        this.contextPath = contextPath;
        this.definitionMap = Collections.synchronizedMap(new HashMap<String, String>());
        this.typeMap = Collections.synchronizedMap(new HashMap<String, String>());
        this.metaDataMap = Collections.synchronizedMap(new HashMap<String, T8DefinitionMetaData>());
        this.groupIdentifierSet = new HashSet<>();
        this.storedTypes = new HashSet<>();
        if (typeIdentifiers != null) storedTypes.addAll(typeIdentifiers);
    }

    @Override
    public void init()
    {
    }

    @Override
    public void destroy()
    {
        // We are going to try and make it easier for the GC to cleanup by detaching the objects inside
        clearCache();
        storedTypes.clear();
        contextPath = null;
    }

    @Override
    public void setContextPath(File contextPath) throws Exception
    {
        LOGGER.log("Setting context path on definition cache '" + cacheIdentifier + "' to: " + contextPath);
        this.contextPath = contextPath;
    }

    @Override
    public void clearCache()
    {
        definitionMap.clear();
        typeMap.clear();
        metaDataMap.clear();
        groupIdentifierSet.clear();
    }

    @Override
    public void cacheDefinitions() throws Exception
    {
        loadDefinitions();
        cacheDefinitionMetaData();
    }

    @Override
    public boolean containsDefinition(String identifier)
    {
        return definitionMap.containsKey(identifier);
    }

    @Override
    public boolean containsDefinitionGroup(String groupIdentifier)
    {
        return groupIdentifierSet.contains(groupIdentifier);
    }

    @Override
    public boolean loadsType(T8DefinitionTypeMetaData typeMetaData)
    {
        return (storedTypes.contains(typeMetaData.getTypeId()));
    }

    @Override
    public boolean savesType(T8DefinitionTypeMetaData typeMetaData)
    {
        return (storedTypes.contains(typeMetaData.getTypeId()));
    }

    private void loadDefinitions() throws Exception
    {
        HashSet<File> loadedPaths;

        // Set the context path and sub folders.
        if (!contextPath.exists())
        {
            LOGGER.log("Creating meta folders at: " + contextPath);
            contextPath.mkdirs();
        }
        else LOGGER.log("Caching meta from: " + contextPath);

        // Load all of the definition types from their respective storage paths.
        loadedPaths = new HashSet<>();
        for (T8DefinitionTypeMetaData metaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (loadsType(metaData))
            {
                if (metaData.getStoragePath() != null)
                {
                    File storagePath;

                    storagePath = new File(contextPath.getAbsolutePath() + metaData.getStoragePath());
                    if ((storagePath.exists()) && (!loadedPaths.contains(storagePath)))
                    {
                        loadedPaths.add(storagePath);

                        try
                        {
                            HashMap<String, String> loadedDefinitions;

                            loadedDefinitions = T8DefinitionFileHandler.loadDefinitions(storagePath);
                            if (loadedDefinitions != null)
                            {
                                // Add the loaded definition Strings to the first level cache.
                                for (String identifier : loadedDefinitions.keySet())
                                {
                                    if (!definitionMap.containsKey(identifier))
                                    {
                                        definitionMap.put(identifier, loadedDefinitions.get(identifier));
                                    }
                                    else throw new Exception("Duplicate Definition: " + identifier);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while loading definitions from path: " + storagePath, e);
                        }
                    }
                }
            }
        }
    }

    private void cacheDefinitionMetaData()
    {
        metaDataMap.clear();
        for (String identifier : definitionMap.keySet())
        {
            try
            {
                T8DefinitionMetaData metaData;
                T8DefinitionTypeMetaData typeMetaData;
                T8Definition definition;

                definition = loadDefinition(identifier);
                metaData = definition.getMetaData();
                typeMetaData = definition.getTypeMetaData();

                metaDataMap.put(definition.getIdentifier(), metaData);
                typeMap.put(definition.getIdentifier(), typeMetaData.getTypeId());
                groupIdentifierSet.add(typeMetaData.getGroupId());
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading definition '" + identifier + "'.", e);
            }
        }
    }

    @Override
    public T8Definition loadDefinition(String identifier) throws Exception
    {
        String definitionData;

        definitionData = definitionMap.get(identifier);
        if (definitionData != null)
        {
            T8Definition rawDefinition;

            rawDefinition = serializer.deserializeDefinition(definitionData);
            return rawDefinition;
        }
        else return null;
    }

    @Override
    public void saveDefinition(T8Definition definition) throws Exception
    {
        String identifier;
        T8DefinitionTypeMetaData typeMetaData;
        T8DefinitionMetaData metaData;
        String serializedDefinition;
        File storagePath;

        // Get all the required meta data.
        identifier = definition.getIdentifier();
        metaData = definition.getMetaData();
        typeMetaData = definition.getTypeMetaData();
        storagePath = new File(contextPath + typeMetaData.getStoragePath());

        // Set the 'patched' flag on the meta data (when a definition is saved, it can no longer be patch since it is saved with all patch changes).
        metaData.setPatched(false);
        metaData.setPatchedContent(false);

        // Serialize the definition.
        definition.setChecksum(T8DefinitionUtilities.CHECKSUM_REPLACEMENT_STRING); // We have to replace the existing definition checksum with a standardized replacement to ensure consistency.
        serializedDefinition = serializer.serializeDefinition(definition);

        // Update the definition checksum.
        definition.setChecksum(T8DefinitionUtilities.computeChecksumHex(serializedDefinition));
        metaData.setChecksum(definition.getChecksum());

        // Update the serialized checksum that will be persisted.
        serializedDefinition = serializedDefinition.replace(T8DefinitionUtilities.CHECKSUM_REPLACEMENT_STRING, definition.getChecksum());

        // Store the serialized definition an accompanying meta data.
        if (!storagePath.exists()) storagePath.mkdirs();
        T8DefinitionFileHandler.saveDefinition(storagePath, definition.getIdentifier(), serializedDefinition);
        definitionMap.put(identifier, serializedDefinition);
        metaDataMap.put(definition.getIdentifier(), metaData);
        typeMap.put(definition.getIdentifier(), typeMetaData.getTypeId());
        groupIdentifierSet.add(typeMetaData.getGroupId()); // We add the group identifier in case the definition being saved is the first of its group.
    }

    @Override
    public void deleteDefinition(String identifier) throws Exception
    {
        String typeIdentifier;

        typeIdentifier = typeMap.get(identifier);
        if (typeIdentifier != null)
        {
            T8DefinitionTypeMetaData typeMetaData;
            File storagePath;

            typeMetaData = definitionManager.getDefinitionTypeMetaData(typeIdentifier);
            if (typeMetaData != null)
            {
                storagePath = new File(contextPath + typeMetaData.getStoragePath());

                T8DefinitionFileHandler.deleteDefinition(storagePath, identifier);
                definitionMap.remove(identifier);
                typeMap.remove(identifier);
                metaDataMap.remove(identifier);
            }
            else throw new Exception("Definition type meta data not found: " + typeIdentifier);
        }
        else throw new Exception("Definition not found: " + identifier);
    }

    @Override
    public long getCachedCharacterSize()
    {
        long dataSize;

        dataSize = 0;
        for (String definitionData : definitionMap.values())
        {
            dataSize += definitionData.length();
        }

        return dataSize;
    }

    @Override
    public int getCachedDefinitionCount()
    {
        return definitionMap.size();
    }

    @Override
    public Set<String> getDefinitionIdentifiers()
    {
        return new TreeSet<>(definitionMap.keySet());
    }

    @Override
    public Set<String> getDefinitionIdentifiers(String typeId)
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        synchronized (this.typeMap)
        {
            for (String definitionId : typeMap.keySet())
            {
                if (typeMap.get(definitionId).equals(typeId))
                {
                    idSet.add(definitionId);
                }
            }
        }

        return idSet;
    }

    @Override
    public Set<String> getGroupDefinitionIdentifiers(String groupId) throws Exception
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (typeMetaData.getGroupId().equals(groupId))
            {
                synchronized (this.typeMap)
                {
                    for (String definitionId : typeMap.keySet())
                    {
                        if (typeMap.get(definitionId).equals(typeMetaData.getTypeId()))
                        {
                            identifierSet.add(definitionId);
                        }
                    }
                }
            }
        }

        return identifierSet;
    }

    @Override
    public Set<String> getGroupIdentifiers()
    {
        return groupIdentifierSet;
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String identifier)
    {
        return metaDataMap.get(identifier);
    }

    @Override
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String typeIdentifier)
    {
        List<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<>();
        synchronized (this.typeMap)
        {
            for (String identifier : typeMap.keySet())
            {
                if (typeMap.get(identifier).equals(typeIdentifier))
                {
                    metaDataList.add(metaDataMap.get(identifier));
                }
            }
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String groupIdentifier) throws Exception
    {
        ArrayList<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (typeMetaData.getGroupId().equals(groupIdentifier))
            {
                synchronized (this.typeMap)
                {
                    for (String identifier : typeMap.keySet())
                    {
                        if (typeMap.get(identifier).equals(typeMetaData.getTypeId()))
                        {
                            metaDataList.add(metaDataMap.get(identifier));
                        }
                    }
                }
            }
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getAllDefinitionMetaData()
    {
        ArrayList<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<>(metaDataMap.values());
        return metaDataList;
    }

    @Override
    public List<String> findDefinitionsByText(String searchText)
    {
        ArrayList<String> identifiers;

        identifiers = new ArrayList<>();
        for (String identifier : definitionMap.keySet())
        {
            String content;

            content = definitionMap.get(identifier);
            if (content.contains(searchText))
            {
                identifiers.add(identifier);
            }
        }

        return identifiers;
    }

    @Override
    public List<String> findDefinitionsByRegularExpression(String expression)
    {
        ArrayList<String> identifiers;
        Pattern pattern;

        pattern = Pattern.compile(expression);
        identifiers = new ArrayList<>();
        for (String identifier : definitionMap.keySet())
        {
            Matcher matcher;
            String content;

            content = definitionMap.get(identifier);
            matcher = pattern.matcher(content);
            if (matcher.find())
            {
                identifiers.add(identifier);
            }
        }

        return identifiers;
    }

    @Override
    public List<String> findDefinitionsByIdentifier(String identifier)
    {
        List<String> results;
        String globalPart;
        String localPart;

        // Find all definitions containing the identifier (references).
        results = findDefinitionsByRegularExpression("[^@\\$0-9a-zA-Z_]" + Pattern.quote(identifier) + "[^0-9a-zA-Z_]");

        // Also try to find the definition itself (this will not be picked up by the previous step).
        localPart = T8IdentifierUtilities.getLocalIdentifierPart(identifier);
        globalPart = T8IdentifierUtilities.getGlobalIdentifierPart(identifier);
        if ((globalPart != null) && (containsDefinition(globalPart)) && (!results.contains(globalPart)))
        {
            try
            {
                T8Definition containingDefinition;

                // Load the definition using the global part of the identifier and then see if it contains the local definition specified.
                containingDefinition = loadDefinition(globalPart);
                if (containingDefinition.getLocalDefinition(localPart) != null)
                {
                    results.add(globalPart);
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while loading definition: " + globalPart, e);
            }
        }

        // Return the results.
        return results;
    }

    @Override
    public List<String> findDefinitionsByDatum(String datumIdentifier, T8DataType dataType, String content)
    {
        StringBuffer searchText;

        searchText = new StringBuffer();
        if (datumIdentifier != null) searchText.append(datumIdentifier.toUpperCase());
        searchText.append(" TYPE=\"").append(dataType).append("\">").append(content).append("<");
        return findDefinitionsByText(searchText.toString());
    }
}
