package com.pilog.version;

/**
 * @author  Bouwer du Preez
 */
public class T8ClientVersion
{
    public final static String VERSION = "1073";
}
