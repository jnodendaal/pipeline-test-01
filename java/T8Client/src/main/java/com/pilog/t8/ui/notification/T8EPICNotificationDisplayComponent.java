package com.pilog.t8.ui.notification;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.definition.notification.T8EPICNotificationDefinition;
import com.pilog.t8.definition.notification.T8EPICNotificationParameterDefinition;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICNotificationDisplayComponent extends T8DefaultNotificationDisplayPanel implements T8NotificationDisplayComponent
{
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8Notification notification;
    private final T8EPICNotificationDefinition definition;
    private final List<T8EPICNotificationParameterDefinition> parameterDefinitions;
    
    public T8EPICNotificationDisplayComponent(T8ClientContext clientContext, T8EPICNotificationDefinition definition, T8Notification notification)
    {
        this.clientContext = clientContext;
        this.sessionContext = clientContext.getSessionContext();
        this.definition = definition;
        this.notification = notification;
        this.parameterDefinitions = definition.getNotificationParameterDefinitions();
    }
    
    @Override
    public T8Notification getNotification()
    {
        return notification;
    }

    @Override
    public void initialize()
    {
        Map<String, ? extends Object> notificationParameters;
        
        notificationParameters = notification.getParameters();
        if (notificationParameters != null )
        {
            for (T8EPICNotificationParameterDefinition parameterDefinition : parameterDefinitions)
            {
                String parameterIdentifier;
                String parameterDisplayName;
                Object value;
                
                // Get the parameter details from the definition.
                parameterIdentifier = parameterDefinition.getIdentifier();
                parameterDisplayName = parameterDefinition.getDisplayName();
                value = notificationParameters.get(parameterIdentifier);
                addProperty(parameterDisplayName, value);
            }
        }
    }
    
    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }
}
