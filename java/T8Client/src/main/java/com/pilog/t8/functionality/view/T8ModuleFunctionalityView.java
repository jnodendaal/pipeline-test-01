package com.pilog.t8.functionality.view;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.functionality.T8ModuleFunctionalityAccessHandle;
import com.pilog.t8.security.T8Context.T8FunctionalityContext;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.ui.module.T8Module;
import java.awt.BorderLayout;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleFunctionalityView extends JPanel implements T8FunctionalityView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ModuleFunctionalityView.class);

    private final T8ModuleFunctionalityAccessHandle accessHandle;
    private final T8ComponentController parentController;
    private final T8ComponentController controller;
    private final T8ComponentEventListener viewListener;
    private final T8ModuleDefinition moduleDefinition;
    private final List<String> finalizationEventIds;
    private final Map<String, Object> inputParameters;
    private final EventListenerList viewListeners;
    private T8Module module;

    public T8ModuleFunctionalityView(T8ComponentController parentController, T8ModuleFunctionalityAccessHandle accessHandle, T8ModuleDefinition moduleDefinition, Map<String, Object> inputParameters, List<String> finalizationEventIds)
    {
        this.parentController = parentController;
        this.controller = new T8DefaultComponentController(parentController, new T8FunctionalityContext(parentController.getContext(), moduleDefinition.getRootProjectId(), accessHandle), accessHandle.getFunctionalityId(), false);
        this.viewListener = new ViewEventListener();
        this.accessHandle = accessHandle;
        this.moduleDefinition = moduleDefinition;
        this.inputParameters = inputParameters;
        this.finalizationEventIds = finalizationEventIds;
        this.viewListeners = new EventListenerList();
        this.setOpaque(false);
        setLayout(new BorderLayout());
    }

    @Override
    public T8FunctionalityAccessHandle getExecutionHandle()
    {
        return accessHandle;
    }

    @Override
    public void initialize() throws Exception
    {
        // Stop the module already loaded.
        if (module != null)
        {
            module.stopComponent();
        }

        // Load the module.
        if (moduleDefinition != null)
        {
            module = (T8Module)moduleDefinition.getNewComponentInstance(controller);
            add(module, BorderLayout.CENTER);
            module.initializeComponent(inputParameters);
        }
        else throw new RuntimeException("Module definition not found: " + moduleDefinition);
    }

    @Override
    public void startComponent()
    {
        parentController.addComponentEventListener(viewListener);
        if (module != null)
        {
            module.startComponent();
        }
    }

    @Override
    public void stopComponent()
    {
        parentController.removeComponentEventListener(viewListener);
        if (module != null)
        {
            module.stopComponent();
        }
    }

    @Override
    public void addFunctionalityViewListener(T8FunctionalityViewListener listener)
    {
        viewListeners.add(T8FunctionalityViewListener.class, listener);
    }

    @Override
    public void removeFunctionalityViewListener(T8FunctionalityViewListener listener)
    {
        viewListeners.remove(T8FunctionalityViewListener.class, listener);
    }

    private void fireFunctionalityViewUpdatedEvent()
    {
        final T8FunctionalityViewUpdatedEvent event;

        // Create and fire the event.
        event = new T8FunctionalityViewUpdatedEvent(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                for (T8FunctionalityViewListener viewListener : viewListeners.getListeners(T8FunctionalityViewListener.class))
                {
                    viewListener.functionalityViewUpdated(event);
                }
            }
        });
    }

    private void fireFunctionalityViewClosedEvent()
    {
        final T8FunctionalityViewClosedEvent event;

        // Create and fire the event.
        event = new T8FunctionalityViewClosedEvent(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                for (T8FunctionalityViewListener viewListener : viewListeners.getListeners(T8FunctionalityViewListener.class))
                {
                    viewListener.functionalityViewClosed(event);
                }
            }
        });
    }

    public T8Module getModule()
    {
        return module;
    }

    @Override
    public String toString()
    {
        return "T8ModuleFunctionalityView{" + "moduleDefinition=" + moduleDefinition + '}';
    }

    private class ViewEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            String eventIdentifier;

            eventDefinition = event.getEventDefinition();
            eventIdentifier = eventDefinition.getPublicIdentifier();

            if ((finalizationEventIds != null) && (finalizationEventIds.contains(eventIdentifier)))
            {
                // If a finalization event has occurred, the functionality execution has ended.
                LOGGER.log("Firing view closed event after: " + eventDefinition);
                fireFunctionalityViewClosedEvent();
            }
            else
            {
                // The default implementation for this view is to fire a view-updated event for every module event that occurs.
                LOGGER.log("Firing view updated event after: " + eventDefinition);
                fireFunctionalityViewUpdatedEvent();
            }
        }
    }
}
