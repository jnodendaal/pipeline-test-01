package com.pilog.t8.ui.flowtasklist;

import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.flowtasklist.T8FlowTaskListDefinition;
import com.pilog.t8.flow.task.T8TaskDataObject;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class TaskList extends JXPanel
{
    private final T8FlowTaskList flowTaskList;
    private final JXTaskPaneContainer taskListContainer;
    private Painter taskBackgroundPainter;
    private Painter taskHighPriorityBackgroundPainter;
    private Painter taskVeryHighPriorityBackgroundPainter;

    public TaskList(T8FlowTaskList flowTaskList)
    {
        this.flowTaskList = flowTaskList;
        initComponents();
        this.taskListContainer = new JXTaskPaneContainer();
        this.taskListContainer.setOpaque(false);
        this.taskListContainer.setBackgroundPainter(null);
        this.jScrollPaneTaskList.setOpaque(false);
        this.jScrollPaneTaskList.getViewport().setOpaque(false);
        this.jScrollPaneTaskList.setViewportView(taskListContainer);
        this.jScrollPaneTaskList.setBorder(null);
        setupPainters(flowTaskList.getComponentDefinition());
    }

    private void setupPainters(T8FlowTaskListDefinition definition)
    {
        T8PainterDefinition painterDefinition;

        // Initialize the painters for painting the background of the task panels.
        painterDefinition = definition.getTaskBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            this.taskBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }

        painterDefinition = definition.getTaskHighPriorityBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            this.taskHighPriorityBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
        else
        {
            this.taskHighPriorityBackgroundPainter = taskBackgroundPainter;
        }

        painterDefinition = definition.getTaskVeryHighPriorityBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            this.taskVeryHighPriorityBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
        else
        {
            this.taskVeryHighPriorityBackgroundPainter = taskBackgroundPainter;
        }
    }

    public void clearTasks()
    {
        taskListContainer.removeAll();
    }

    public void setTaskList(List<T8TaskDataObject> taskObjects)
    {
        // Clear the task and task type containers and then add all the new tasks and task types.
        clearTasks();
        for (T8TaskDataObject taskObject :  taskObjects)
        {
            addTask(taskObject);
        }

        // Ensure that the layout is up-to-date.
        taskListContainer.revalidate();
        revalidate();
    }

    public void resetScrollPane()
    {
        SwingUtilities.invokeLater(() ->
        {
            jScrollPaneTaskList.getVerticalScrollBar().setValue(0);
        });
    }

    public void addTask(T8TaskDataObject taskObject)
    {
        taskListContainer.add(createTaskPanel(taskObject));
    }

    private JComponent createTaskPanel(T8TaskDataObject taskObject)
    {
        JXPanel taskPane;
        GridBagConstraints constraints;

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 0.1;
        constraints.weighty = 0.1;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);

        // Create a new task pane.
        taskPane = new JXPanel();
        taskPane.setOpaque(false);
        taskPane.setLayout(new GridBagLayout());
        taskPane.add(new TaskPanel(flowTaskList, taskObject), constraints);

        // Set the correct painter for the task, based on priority.
        switch (T8TaskDetails.PriorityLevel.getPriorityLevel(taskObject.getPriority()))
        {
            case VERY_HIGH:
                taskPane.setBackgroundPainter(taskVeryHighPriorityBackgroundPainter);
                break;
            case HIGH:
                taskPane.setBackgroundPainter(taskHighPriorityBackgroundPainter);
                break;
            case NORMAL:
                taskPane.setBackgroundPainter(taskBackgroundPainter);
                break;
            case LOW:
                taskPane.setBackgroundPainter(taskBackgroundPainter);
                break;
            case VERY_LOW:
                taskPane.setBackgroundPainter(taskBackgroundPainter);
                break;
            default:
                throw new IllegalStateException("Invalid priority level found for Task: " + taskObject);
        }

        return taskPane;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jScrollPaneTaskList = new javax.swing.JScrollPane();

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jScrollPaneTaskList.setOpaque(false);
        add(jScrollPaneTaskList, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPaneTaskList;
    // End of variables declaration//GEN-END:variables
}
