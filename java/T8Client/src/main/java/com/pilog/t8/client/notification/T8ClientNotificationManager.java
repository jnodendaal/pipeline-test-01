package com.pilog.t8.client.notification;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationFilter;
import com.pilog.t8.notification.T8NotificationSummary;
import com.pilog.t8.definition.notification.T8NotificationManagerResource;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ClientNotificationManager implements T8NotificationManager
{
    private final T8ClientContext clientContext;

    public T8ClientNotificationManager(T8ClientContext clientContext)
    {
        this.clientContext = clientContext;
    }

    @Override
    public void init() throws Exception
    {
    }

    @Override
    public void start() throws Exception
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public void sendNotification(T8Context context, String notificationIdentifier, Map<String, ? extends Object> notificationParameters) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(T8NotificationManagerResource.PARAMETER_NOTIFICATION_IDENTIFIER, notificationIdentifier);
        operationParameters.put(T8NotificationManagerResource.PARAMETER_NOTIFICATION_PARAMETERS, notificationParameters);

        T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_SEND_NOTIFICATION, operationParameters);
    }

    @Override
    public List<T8Notification> getUserNotifications(T8Context context, T8NotificationFilter filter) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createSingular(T8NotificationManagerResource.PARAMETER_NOTIFICATION_FILTER, filter);
        return (List<T8Notification>) T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_GET_USER_NOTIFICATIONS, operationParameters).get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_LIST);
    }

    @Override
    public void closeNotification(T8Context context, String notificationInstanceIdentifier) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_CLOSE_NOTIFICATION, HashMaps.createSingular(T8NotificationManagerResource.PARAMETER_NOTIFICATION_IID, notificationInstanceIdentifier));
    }

    @Override
    public void markWithStatus(T8Context context, String notificationInstanceIdentifier, T8Notification.NotificationStatus status) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>(2, 1f);
        operationParameters.put(T8NotificationManagerResource.PARAMETER_NOTIFICATION_IID, notificationInstanceIdentifier);
        operationParameters.put(T8NotificationManagerResource.PARAMETER_NOTIFICATION_STATUS, status);

        T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_NOTIFICATION_MARK_AS, operationParameters);
    }

    @Override
    public T8NotificationSummary getUserNotificationSummary(T8Context context, T8NotificationFilter filter) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createSingular(T8NotificationManagerResource.PARAMETER_NOTIFICATION_FILTER, filter);
        return (T8NotificationSummary) T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_GET_USER_NOTIFICATION_SUMMARY, operationParameters).get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_SUMMARY);
    }

    @Override
    public boolean hasNewNotifications(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int countNotifications(T8Context context, T8NotificationFilter filter) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createSingular(T8NotificationManagerResource.PARAMETER_NOTIFICATION_FILTER, filter);
        return (Integer)T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_COUNT_NOTIFICATIONS, operationParameters).get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_COUNT);
    }

    @Override
    public void closeAllNotifications(T8Context context) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(context, T8NotificationManagerResource.OPERATION_CLOSE_ALL_NOTIFICATIONS, new HashMap<>());
    }
}
