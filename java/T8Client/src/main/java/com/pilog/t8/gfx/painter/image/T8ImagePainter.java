package com.pilog.t8.gfx.painter.image;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.image.T8ImagePainterDefinition;
import com.pilog.t8.definition.gfx.painter.image.T8ImagePainterDefinition.FillMode;
import com.pilog.t8.gfx.painter.T8PainterConfigurator;

/**
 * @author Bouwer du Preez
 */
public class T8ImagePainter extends ImagePainter implements T8Painter
{
    private T8ImagePainterDefinition definition;

    public T8ImagePainter(T8ImagePainterDefinition definition) throws Exception
    {
        super();
        this.definition = definition;
        setupPainter();
    }

    private void setupPainter() throws Exception
    {
        FillMode fillMode;
        
        // Configure this painter's super type properties.
        T8PainterConfigurator.setupAbstractAreaPainter(this, definition);
        
        // Set the image.
        setImage(definition.getImageDefinition().getImage().getBufferedImage());

        // Set the fill mode to use when the image does not fit the required dimensions.
        fillMode = definition.getFillMode();
        if (fillMode == FillMode.REPEAT_HORIZONTAL)
        {
            setHorizontalRepeat(true);
        }
        else if (fillMode == FillMode.REPEAT_VERTICAL)
        {
            setVerticalRepeat(true);
        }
        else if (fillMode == FillMode.REPEAT_HORIZONTAL_AND_VERTICAL)
        {
            setHorizontalRepeat(true);
            setVerticalRepeat(true);
        }
        else if (fillMode == FillMode.SCALE_INSIDE_FIT)
        {
            setScaleToFit(true);
            setScaleType(ScaleType.InsideFit);
        }
        else if (fillMode == FillMode.SCALE_OUTSIDE_FIT)
        {
            setScaleToFit(true);
            setScaleType(ScaleType.OutsideFit);
        }
        else if (fillMode == FillMode.SCALE_DISTORT)
        {
            setScaleToFit(true);
            setScaleType(ScaleType.Distort);
        }
    }
}
