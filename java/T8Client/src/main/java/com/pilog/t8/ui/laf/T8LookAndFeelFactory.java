/**
 * Created on 11 Apr 2016, 8:07:32 AM
 */
package com.pilog.t8.ui.laf;

import javax.swing.LookAndFeel;

/**
 * @author Gavin Boshoff
 */
public class T8LookAndFeelFactory
{
    /**
     * An {@code enum} as a set of operating system dependant suppliers for the
     * look and feel of the application.
     */
    private enum T8LookAndFeel
    {
        T8_WINDOWS
        {
            @Override
            LookAndFeel getLAFInstance()
            {
                return new T8WindowsLookAndFeel();
            }
        },
        T8_MAC
        {
            @Override
            LookAndFeel getLAFInstance()
            {
                return new T8MacLookAndFeel();
            }
        },
        T8_UNIX,
        T8_SOLARIS;

        private T8LookAndFeel() {}

        /**
         * Gets the {@code LookAndFeel} instance as is applicable to the current
         * operating system.
         *
         * @return The {@code LookAndFeel} applicable as extended by the T8
         *      framework and made applicable to the current operating system
         */
        LookAndFeel getLAFInstance() //Package private to allow overriding
        {
            throw new UnsupportedOperationException("The T8 Look And Feel does not currently support: " + this);
        }

        /**
         * Determines the applicable {@code T8LookAndFeel} supplier based on
         * the supplied operating system.
         *
         * @param operatingSystem The {@code String} representing the system
         *      property for the current client operating system name
         *
         * @return The applicable {@code T8LookAndFeel} supplier
         */
        private static T8LookAndFeel findApplicableSupplier(String operatingSystem)
        {
            if (operatingSystem.contains("win")) return T8_WINDOWS;
            else if (operatingSystem.contains("mac")) return T8_MAC;
            else if (operatingSystem.contains("nix") || operatingSystem.contains("nux") || operatingSystem.contains("aix")) return T8_UNIX;
            else if (operatingSystem.contains("sunos")) return T8_SOLARIS;
            else throw new IllegalArgumentException("The Look And Feel for the Operating System [" + operatingSystem + "] could not be determined.");
        }
    }

    public static LookAndFeel getLookAndFeel(String operatingSystem)
    {
        T8LookAndFeel lafSupplier;

        lafSupplier = T8LookAndFeel.findApplicableSupplier(operatingSystem.toLowerCase());

        return lafSupplier.getLAFInstance();
    }

    private T8LookAndFeelFactory() {}
}