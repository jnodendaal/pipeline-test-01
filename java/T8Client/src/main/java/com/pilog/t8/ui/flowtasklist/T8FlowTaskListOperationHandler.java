package com.pilog.t8.ui.flowtasklist;

import com.pilog.t8.definition.ui.flowtasklist.T8FlowTaskListResource;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskListOperationHandler extends T8ComponentOperationHandler
{
    private final T8FlowTaskList flowTaskList;

    public T8FlowTaskListOperationHandler(T8FlowTaskList flowTaskList)
    {
        super(flowTaskList);
        this.flowTaskList = flowTaskList;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8FlowTaskListResource.OPERATION_FILTER_TASK_LIST))
        {
            String taskIdentifier;
            Boolean includeClaimedTasks;
            Boolean includeUnclaimedTasks;

            taskIdentifier = (String)operationParameters.get(T8FlowTaskListResource.PARAMETER_TASK_IDENTIFIER);
            includeClaimedTasks = (Boolean)operationParameters.get(T8FlowTaskListResource.PARAMETER_INCLUDE_CLAIMED_TASKS);
            includeUnclaimedTasks = (Boolean)operationParameters.get(T8FlowTaskListResource.PARAMETER_INCLUDE_UNCLAIMED_TASKS);
            
            if (includeClaimedTasks == null) includeClaimedTasks = true;
            if (includeUnclaimedTasks == null) includeUnclaimedTasks = true;
            
            flowTaskList.filterTaskList(taskIdentifier, includeClaimedTasks, includeUnclaimedTasks);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
