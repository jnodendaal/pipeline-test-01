package com.pilog.t8.ui.notification;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.process.T8Process;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.notification.T8ProcessNotificationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.processlist.ProcessPanel;
import java.awt.BorderLayout;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessNotificationDisplayComponent extends JPanel implements T8NotificationDisplayComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProcessNotificationDisplayComponent.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8Notification notification;
    private final ProcessUpdater processUpdater;
    private final String processIid;
    private T8ProcessDetails processDetails;
    private ProcessPanel processPanel;

    public T8ProcessNotificationDisplayComponent(T8ClientContext clientContext, T8Context context, T8ProcessNotificationDefinition definition, T8Notification notification)
    {
        this.clientContext = clientContext;
        this.context = context;
        this.notification = notification;
        initComponents();
        this.processUpdater = new ProcessUpdater();
        this.processIid = (String)this.notification.getParameters().get(T8ProcessNotificationDefinition.PARAMETER_PROCESS_IID);
    }

    @Override
    public T8Notification getNotification()
    {
        return notification;
    }

    @Override
    public void initialize()
    {
        try
        {
            processDetails = this.clientContext.getProcessManager().getProcessDetails(context, processIid);
            if (processDetails != null)
            {
                T8SessionContext sessionContext;

                sessionContext = context.getSessionContext();
                processPanel = new ProcessPanel(clientContext, context, this.processDetails);
                processPanel.setControlsVisible(sessionContext.getUserIdentifier().equals(processDetails.getInitiatorId()));
                add(processPanel, BorderLayout.CENTER);
            }
            else // The process is not in progress, so use the notification parameters to construct a default layout.
            {
                Map<String, ? extends Object> notificationParameters;
                T8DefaultNotificationDisplayPanel panel;

                notificationParameters = notification.getParameters();
                panel = new T8DefaultNotificationDisplayPanel();
                panel.addProperty("Process", notificationParameters.get(T8ProcessNotificationDefinition.PARAMETER_PROCESS_DISPLAY_NAME));
                panel.addProperty("Description", notificationParameters.get(T8ProcessNotificationDefinition.PARAMETER_PROCESS_DESCRIPTION));
                panel.addProperty("Initiator", notificationParameters.get(T8ProcessNotificationDefinition.PARAMETER_PROCESS_INITIATOR_DISPLAY_NAME));
                panel.addProperty("Started At", new T8Timestamp(notification.getSentTime()));
                add(panel, BorderLayout.CENTER);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while constructing process notification display component from notification: " + notification, e);
        }
    }

    @Override
    public void startComponent()
    {
        // We only need to start the process updater thread if the process is in progress (we have a process details to work from).
        if (processDetails != null)
        {
            processUpdater.start();
        }
    }

    @Override
    public void stopComponent()
    {
        processUpdater.cancel();
    }

    private void setProcessDetails(T8ProcessDetails processDetails)
    {
        this.processDetails = processDetails;
        SwingUtilities.invokeLater(() -> processPanel.setProcessDetails(T8ProcessNotificationDisplayComponent.this.processDetails));
    }

    private class ProcessUpdater extends Thread
    {
        private boolean stop = false;

        private ProcessUpdater()
        {
            setName("Process_Updater");
        }

        public void cancel()
        {
            this.stop = true;
        }

        @Override
        @SuppressWarnings("SleepWhileInLoop")
        public void run()
        {
            // Update it atleast once so we can get the executing status.
            updateProcessDetails();
            while (!stop && (processDetails.isExecuting() || processDetails.getStatus() == T8Process.T8ProcessStatus.CREATED))
            {
                updateProcessDetails();

                try
                {
                    Thread.sleep(500l);
                }
                catch (InterruptedException ie)
                {
                    LOGGER.log(T8Logger.Level.WARNING, "Sleep in loop interrupted. Continuing to execute loop. Exception: " + ie.getMessage());
                }
            }
        }

        private void updateProcessDetails()
        {
            SwingUtilities.invokeLater(() ->
            {
                try
                {
                    setProcessDetails(clientContext.getProcessManager().getProcessDetails(context, processDetails.getProcessIid()));
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to refresh process details for process " + processDetails.getProcessIid() + ". Stopping refresh operation.", ex);
                    stop = true;
                }
            });
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
