package com.pilog.t8.ui.dialog;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentContainer;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.T8RootPane;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.script.T8ComponentEventHandlerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.dialog.T8DialogAPIHandler;
import com.pilog.t8.definition.ui.dialog.T8DialogDefinition;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.border.DropShadowBorder;

/**
 * @author Bouwer du Preez
 */
public class T8Dialog extends JDialog implements T8RootPane
{
    private static final T8Logger logger = T8Log.getLogger(T8Dialog.class);

    private final T8DialogDefinition definition;
    private final T8ComponentController parentController;
    private final JXTitledPanel titledPanel;
    private final JXPanel contentPanel;
    private final T8DefaultComponentContainer componentContainer;
    private T8Component childComponent;
    private final T8DialogOperationHandler operationHandler;
    private final MoveMouseListener dialogMouseListener;
    protected Map<String, Object> inputParameters;
    private boolean hasCloseButtonEventHandler;

    public T8Dialog(T8DialogDefinition definition, T8ComponentController controller)
    {
        // Make sure we show on top of the correct window
        super(controller.getParentWindow(), definition.isModal() ? ModalityType.DOCUMENT_MODAL : ModalityType.MODELESS);
        this.definition = definition;
        this.parentController = controller;
        this.componentContainer = new T8DefaultComponentContainer();
        this.operationHandler = new T8DialogOperationHandler(this);
        this.setTitle(definition.getTitle());
        this.setUndecorated(true);
        this.getRootPane().setOpaque(false);
        this.getContentPane().setBackground(new Color(0, 0, 0, 0));
        this.setBackground(new Color(0, 0, 0, 0));

        // From SwingX 1.6.5 it has become necessary to set the content container.  This has to be done before the rest of the components are initialized.
        this.titledPanel = new JXTitledPanel(definition.getTitle());
        this.titledPanel.setOpaque(false);
        this.contentPanel = new JXPanel();
        this.contentPanel.setOpaque(true);
        this.contentPanel.setLayout(new BorderLayout());
        this.contentPanel.add(componentContainer, java.awt.BorderLayout.CENTER);
        this.titledPanel.setContentContainer(contentPanel);
        this.titledPanel.setBorder(new CompoundBorder(new DropShadowBorder(), new LineBorder(Color.DARK_GRAY, 1)));
        this.dialogMouseListener = new MoveMouseListener(titledPanel);
        this.titledPanel.addMouseListener(dialogMouseListener);
        this.titledPanel.addMouseMotionListener(dialogMouseListener);
        this.titledPanel.setTitleFont(LAFConstants.MAIN_HEADER_FONT);
        this.titledPanel.setTitleForeground(LAFConstants.MAIN_HEADER_TEXT_COLOR);
        this.titledPanel.setRightDecoration(buildDialogControls());
        initComponents();
        this.add(titledPanel, java.awt.BorderLayout.CENTER);

        // Set the dialog resizability.
        setResizable(definition.isResizable());

        setupDialog();
    }

    private void setupDialog()
    {
        T8PainterDefinition backgroundPainterDefinition;

        backgroundPainterDefinition = definition.getBackgroundPainterDefinition();
        if (backgroundPainterDefinition != null)
        {
            contentPanel.setPaintBorderInsets(false);
            contentPanel.setBackgroundPainter(new T8PainterAdapter(backgroundPainterDefinition.getNewPainterInstance()));
        }

        // Check to see if we have any close button event handlers (if not we will use the default behavior).
        hasCloseButtonEventHandler = false;
        for (T8ComponentEventHandlerScriptDefinition eventHandlerDefinition : definition.getEventHandlerScriptDefinitions())
        {
            if (T8DialogAPIHandler.EVENT_DIALOG_CLOSE_BUTTON_PRESSED.equals(eventHandlerDefinition.getEventIdentifier()))
            {
                hasCloseButtonEventHandler = true;
            }
        }
    }

    private JComponent buildDialogControls()
    {
        JXButton closeButton;

        closeButton = new JXButton();
        closeButton.setMargin(new Insets(0, 0, 0, 0));
        closeButton.setPreferredSize(new Dimension(20, 20));
        closeButton.setOpaque(false);
        closeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/dialogCloseIcon.png")));
        closeButton.setFocusable(false);
        closeButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (!hasCloseButtonEventHandler)
                {
                    // If no event handler for the close button event exists, we invoke the default close behavior.
                    closeDialog();
                }
                else
                {
                    handleCloseButtonAction();
                }
            }
        });

        return closeButton;
    }

    protected T8ComponentContainer getContainer()
    {
        return componentContainer;
    }

    protected void handleCloseButtonAction()
    {
        getController().reportEvent(T8Dialog.this, definition.getComponentEventDefinition(T8DialogAPIHandler.EVENT_DIALOG_CLOSE_BUTTON_PRESSED), new HashMap<>(0));
    }

    public void setHeaderTitle(String title)
    {
        titledPanel.setTitle(title);
    }

    public JXPanel getContentPanel()
    {
        return contentPanel;
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return inputParameters;
    }

    public void setInputParameters(Map<String, Object> inputParameters)
    {
        this.inputParameters = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, true);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.inputParameters = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, true);
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        // If this dailog is still open, lets close it now
        closeDialog();
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        this.childComponent = component;
        componentContainer.setComponent((JComponent) component);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return this.childComponent;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        if (this.childComponent != null)
        {
            if (this.childComponent.getComponentDefinition().getIdentifier().equals(identifier))
            {
                T8Component removedComponent;

                removedComponent = this.childComponent;
                componentContainer.setComponent(null);
                this.childComponent = null;
                return removedComponent;
            }
            else return null;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<>();
        if (this.childComponent != null) childComponents.add(this.childComponent);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return this.childComponent != null ? 1 : 0;
    }

    @Override
    public void clearChildComponents()
    {
        this.componentContainer.setComponent(null);
        this.childComponent = null;
    }

    @Override
    public void lockUI(String message)
    {
        componentContainer.setMessage(message);
        componentContainer.setLocked(true);
    }

    @Override
    public void unlockUI()
    {
        componentContainer.setLocked(false);
    }

    protected void setDialogSize()
    {
        // Maximize the dialog if required.
        if (definition.isOpenMaximized())
        {
            setSize(parentController.getClientContext().getParentWindow().getToolkit().getScreenSize());
        }
        else
        {
            if (definition.getSizeType() == T8DialogDefinition.SizeType.ABSOLUTE)
            {
                setSize(definition.getWidth(), definition.getHeight());
            }
            else
            {
                T8ClientContext clientContext;
                Dimension parentSize;
                Double dialogWidth;
                Double dialogHeight;

                clientContext = parentController.getClientContext();
                parentSize = clientContext.getParentWindow().getSize();
                dialogWidth = parentSize.width * (definition.getWidth()/100.00);
                dialogHeight = parentSize.height * (definition.getHeight()/100.00);
                setSize(dialogWidth.intValue(), dialogHeight.intValue());
            }
        }

        // Always open in middle of parent window, this will also fix dailogs opening on wrong screen on multi screen setups
        setLocationRelativeTo(parentController.getClientContext().getParentWindow());
    }

    /**
     * This is the standard method for showing a T8Dialog.
     */
    public void openDialog()
    {
        openDialog(isModal());
    }

    /**
     * This is a specialized method for opening a dialog to specify whether or
     * not it should be modal. The default is to be set non-modal.
     *
     * @param modal {@code true} to set the dialog modal. {@code false}
     *      otherwise
     */
    public void openDialog(boolean modal)
    {
        if (!isVisible())
        {
            // Set the dialog sizes etc, but only while it is still invisible.
            setDialogSize();

            // Set the dialog modality, if required
            setModal(modal);

            // Report the event if required.
            parentController.reportEvent(T8Dialog.this, definition.getComponentEventDefinition(T8DialogAPIHandler.EVENT_DIALOG_OPENED), inputParameters);

            // This should always be called last because if the dialog is modal then the thread will block here.
            super.setVisible(true);
        }
    }

    /**
     * This is the standard method for hiding a currently displayed T8Dialog.
     */
    public void closeDialog()
    {
        if (isVisible())
        {
            // Request focus in the window incase there is any focus listeners that need to do something before we close.
            requestFocusInWindow();

            // Dispose the dialog.
            dispose();

            // Report the event.
            parentController.reportEvent(T8Dialog.this, definition.getComponentEventDefinition(T8DialogAPIHandler.EVENT_DIALOG_CLOSED), new HashMap<>(0));
        }
    }

    private enum ResizeDirection {N, NW, W, SW, S, SE, E, NE};

    public class MoveMouseListener extends MouseAdapter implements MouseMotionListener
    {
        private final JComponent target;
        private Point startDragLocation;
        private Point startWindowLocation;
        private Rectangle startBounds;
        private boolean moveInProgress;
        private boolean resizeInProgress;
        private ResizeDirection resizeDirection;

        public MoveMouseListener(JComponent target)
        {
            this.target = target;
            this.moveInProgress = false;
            this.resizeInProgress = false;
            this.resizeDirection = null;
        }

        public Window getWindow(Container target)
        {
            return SwingUtilities.getWindowAncestor(target);
        }

        private Point getScreenLocation(MouseEvent e)
        {
            Point cursorLocation;
            Point targetLocation;

            cursorLocation = e.getPoint();
            targetLocation = this.target.getLocationOnScreen();
            return new Point((int)(targetLocation.getX() + cursorLocation.getX()), (int)(targetLocation.getY() + cursorLocation.getY()));
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
            if ((!moveInProgress) && (!resizeInProgress))
            {
                titledPanel.setCursor(Cursor.getDefaultCursor());
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            startDragLocation = this.getScreenLocation(e);
            startWindowLocation = this.getWindow(this.target).getLocation();
            startBounds = getBounds();

            if (e.getX() < 5)
            {
                resizeInProgress = true;
                resizeDirection = ResizeDirection.E;
            }
            else if (e.getX() > (getWidth() - 10))
            {
                resizeInProgress = true;
                resizeDirection = ResizeDirection.W;
            }
            else if (e.getY() < 10)
            {
                resizeInProgress = true;
                resizeDirection = ResizeDirection.N;
            }
            else if (e.getY() > (getHeight() - 10))
            {
                resizeInProgress = true;
                resizeDirection = ResizeDirection.S;
            }
            else if (e.getY() < 30)
            {
                moveInProgress = true;
            }
            else
            {
                resizeInProgress = false;
                moveInProgress = false;
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            moveInProgress = false;
            resizeInProgress =  false;
            titledPanel.setCursor(Cursor.getDefaultCursor());
        }

        @Override
        public void mouseDragged(MouseEvent e)
        {
            Point currentLocation;
            Point newLocation;
            Point offset;

            currentLocation = this.getScreenLocation(e);
            offset = new Point((int)currentLocation.getX() - (int)startDragLocation.getX(), (int)currentLocation.getY() - (int)startDragLocation.getY());
            newLocation = new Point((int)(this.startWindowLocation.getX() + offset.getX()), (int)(this.startWindowLocation.getY() + offset.getY()));

            if (moveInProgress)
            {
                Window frame;

                frame = getWindow(target);
                frame.setLocation(newLocation);
            }
            else if (resizeInProgress)
            {
                Rectangle newBounds;

                // Compute the new bounds of the window using the drag offset.
                newBounds = startBounds.getBounds();
                if (resizeDirection == ResizeDirection.E) newBounds.width -= offset.x;
                if (resizeDirection == ResizeDirection.W) newBounds.width += offset.x;
                if (resizeDirection == ResizeDirection.N) newBounds.height -= offset.y;
                if (resizeDirection == ResizeDirection.S) newBounds.height += offset.y;

                // Make sure we never resize the window to a size that is not valid.
                if (newBounds.width < 50) newBounds.width = 50;
                if (newBounds.height < 50) newBounds.height = 50;

                // If we are resizing using the east border we need to relocate the window to keep it in the same spot.
                if (resizeDirection == ResizeDirection.E)
                {
                    Point cursorLocation;

                    cursorLocation = e.getPoint();
                    SwingUtilities.convertPointToScreen(cursorLocation, getContentPane());
                    newBounds.x = cursorLocation.x;
                }

                // If we are resizing using the north border we need to relocate the window to keep it in the same spot.
                if (resizeDirection == ResizeDirection.N)
                {
                    Point cursorLocation;

                    cursorLocation = e.getPoint();
                    SwingUtilities.convertPointToScreen(cursorLocation, getContentPane());
                    newBounds.y = cursorLocation.y;
                }

                // Set the new bounds of the dialog.
                invalidate();
                setBounds(newBounds);
                validate();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e)
        {
            if ((e.getX() < 5) || (e.getX() > (getWidth() - 10)))
            {
                titledPanel.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
            }
            else if ((e.getY() < 10) || (e.getY() > (getHeight() - 10)))
            {
                titledPanel.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
            }
            else if (e.getY() < 30)
            {
                titledPanel.setCursor(new Cursor(Cursor.MOVE_CURSOR));
            }
            else
            {
                titledPanel.setCursor(Cursor.getDefaultCursor());
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
