package com.pilog.t8.ui.chart.bar;

import com.pilog.t8.definition.ui.chart.bar.T83DBarChartAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.List;
import java.util.Map;
import org.jfree.data.category.CategoryDataset;

/**
 * @author Bouwer du Preez
 */
public class T83DBarChartOperationHandler extends T8ComponentOperationHandler
{
    private final T83DBarChart chart;

    public T83DBarChartOperationHandler(T83DBarChart chart)
    {
        super(chart);
        this.chart = chart;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T83DBarChartAPIHandler.OPERATION_UPDATE_DATA_SET))
        {
            CategoryDataset dataSet;

            dataSet = (CategoryDataset)operationParameters.get(T83DBarChartAPIHandler.PARAMETER_BAR_DATA_SET);
            if (dataSet != null)
            {
                chart.setDataSet(dataSet);
            }
            else
            {
                List<List<Object>> dataList;

                dataList = (List<List<Object>>)operationParameters.get(T83DBarChartAPIHandler.PARAMETER_BAR_DATA_LIST);
                if (dataList != null)
                {
                    chart.setDataList(dataList);
                }
            }

            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
