package com.pilog.t8.gfx.painter.compound;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.gfx.painter.compound.T8CompoundPainterDefinition;
import java.util.ArrayList;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8CompoundPainter extends CompoundPainter implements T8Painter
{
    private T8CompoundPainterDefinition definition;
    
    public T8CompoundPainter(T8CompoundPainterDefinition definition)
    {
        this.definition = definition;
        setupPainter();
    }
    
    private void setupPainter()
    {
        ArrayList<T8PainterDefinition> painterDefinitions;
        Painter[] painters;
        
        // Create an array of painter instances.
        painterDefinitions = definition.getPainterDefinitions();
        painters = new Painter[painterDefinitions.size()];
        for (int painterIndex = 0; painterIndex < painterDefinitions.size(); painterIndex++)
        {
            painters[painterIndex] = new T8PainterAdapter(painterDefinitions.get(painterIndex).getNewPainterInstance());
        }
        
        // Set the painter array.
        this.setPainters(painters);
    }
}
