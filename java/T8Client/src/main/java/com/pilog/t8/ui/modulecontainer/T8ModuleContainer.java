package com.pilog.t8.ui.modulecontainer;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.modulecontainer.T8ModuleContainerDefinition;
import com.pilog.t8.definition.ui.modulecontainer.T8ModuleInterfaceEventMappingDefinition;
import com.pilog.t8.definition.ui.modulecontainer.T8ModuleInterfaceMappingDefinition;
import com.pilog.t8.definition.ui.modulecontainer.T8ModuleInterfaceOperationMappingDefinition;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleContainer extends T8DefaultComponentContainer implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ModuleContainer.class);

    protected T8ConfigurationManager configurationManager;
    protected T8Component contentModule;
    protected T8ModuleInterfaceMappingDefinition contentMappingDefinition;

    private final T8ModuleContainerDefinition definition;
    private final T8ComponentController parentController;
    private final T8ComponentController moduleController;
    private final ModuleEventListener moduleListener;
    private final T8ModuleContainerOperationHandler operationHandler;

    public T8ModuleContainer(T8ModuleContainerDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.parentController = controller;
        this.moduleController = new T8DefaultComponentController(controller, new T8ProjectContext(controller.getContext(), definition.getRootProjectId()), definition.getIdentifier(), false);
        this.moduleListener = new ModuleEventListener();
        this.moduleController.addComponentEventListener(moduleListener);
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.operationHandler = new T8ModuleContainerOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        clearModule();
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(parentController.getContext(), inputString);
    }

    public T8Component getContentComponent()
    {
        return contentModule;
    }

    public void reloadModule()
    {
        if (contentMappingDefinition != null)
        {
            loadModule(contentMappingDefinition.getIdentifier());
        }
    }

    public void clearModule()
    {
        // Check to see if we need to stop the current content component.
        if (contentModule != null)
        {
            contentModule.stopComponent();
            contentModule = null;
            setComponent(null);
        }
    }

    public void loadModule(String mappingId)
    {
        long startTime;

        // Make sure this method is not called from the EDT.
        if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Method cannot be called from EDT.");

        // Lock the UI.
        startTime = System.currentTimeMillis();
        setMessage(translate("Loading Component..."));
        lock();

        // Load the new content module.
        if (mappingId != null)
        {
            T8ModuleInterfaceMappingDefinition mappingDefinition;

            mappingDefinition = definition.getModuleInterfaceMappingDefinitions(mappingId);
            if (mappingDefinition != null)
            {
                try
                {
                    T8ComponentDefinition moduleDefinition;
                    String moduleId;

                    moduleId = mappingDefinition.getModuleIdentifier();
                    moduleDefinition = (T8ComponentDefinition)parentController.getClientContext().getDefinitionManager().getInitializedDefinition(parentController.getContext(), definition.getRootProjectId(), moduleId, null);
                    if (moduleDefinition != null)
                    {
                        // Clear the existing content module.
                        clearModule();
                        contentModule = moduleDefinition.getNewComponentInstance(moduleController);
                        contentModule.initializeComponent(null);
                        setComponent((JComponent)contentModule);
                        contentModule.startComponent();
                        contentMappingDefinition = mappingDefinition;
                    }
                    else throw new RuntimeException("Module not found: " + moduleId);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while loading module using mapping: " + mappingDefinition, e);
                }
            }
            else throw new RuntimeException("Invalid Mapping identifier encountered: " + mappingId);
        }
        else throw new RuntimeException("Null Module identifier encountered.");

        // Unlock the UI.
        unlock();
        LOGGER.log("Module Loaded: " + (System.currentTimeMillis() - startTime));
        transferFocus();
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        T8ModuleInterfaceOperationMappingDefinition operationMappingDefinition;
        String moduleIdentifier;

        // First get the operation mapping definition for the operation that has been invoked.
        moduleIdentifier = contentModule != null ? contentModule.getComponentDefinition().getIdentifier() : null;
        operationMappingDefinition = moduleIdentifier != null ? getInterfaceOperationMappingDefinition(moduleIdentifier, operationIdentifier) : null;
        if (operationMappingDefinition != null)
        {
            if (contentModule != null)
            {
                Map<String, Object> outputParameters;
                Map<String, Object> localOperationParameters;
                String localOperationIdentifier;

                // First we have to map and strip the identifiers of the operation and its input parameters.
                localOperationIdentifier = T8IdentifierUtilities.stripNamespace(operationMappingDefinition.getModuleOperationIdentifier(), moduleIdentifier, true);
                localOperationParameters = T8IdentifierUtilities.mapParameters(operationParameters, operationMappingDefinition.getInputParameterMapping());
                localOperationParameters = T8IdentifierUtilities.stripNamespace(moduleIdentifier, localOperationParameters, true);

                // Now we execute the module operation and prepend the module identifier back on to the output parameters.
                outputParameters = contentModule.executeOperation(localOperationIdentifier, localOperationParameters != null ? localOperationParameters : new HashMap<>());
                outputParameters = T8IdentifierUtilities.prependNamespace(moduleIdentifier, outputParameters, true);
                return T8IdentifierUtilities.mapParameters(outputParameters, operationMappingDefinition.getOutputParameterMapping());
            }
            else return null;
        }
        else
        {
            // If no operation mapping was found, simply execute the operation normally using this container's operation handler.
            return operationHandler.executeOperation(operationIdentifier, operationParameters);
        }
    }

    @Override
    public void setComponent(JComponent component)
    {
        super.setComponent(component);
    }

    private T8ModuleInterfaceEventMappingDefinition getInterfaceEventMappingDefinition(String moduleIdentifier, String moduleEventIdentifier)
    {
        for (T8ModuleInterfaceMappingDefinition interfaceDefinition : definition.getModuleInterfaceMappingDefinitions())
        {
            if (moduleIdentifier.equals(interfaceDefinition.getModuleIdentifier()))
            {
                for (T8ModuleInterfaceEventMappingDefinition eventMappingDefinition : interfaceDefinition.getEventMappingDefinitions())
                {
                    if (moduleEventIdentifier.equals(eventMappingDefinition.getModuleEventIdentifier()))
                    {
                        return eventMappingDefinition;
                    }
                }
            }
        }

        return null;
    }

    private T8ModuleInterfaceOperationMappingDefinition getInterfaceOperationMappingDefinition(String moduleIdentifier, String interfaceOperationIdentifier)
    {
        for (T8ModuleInterfaceMappingDefinition interfaceDefinition : definition.getModuleInterfaceMappingDefinitions())
        {
            if (moduleIdentifier.equals(interfaceDefinition.getModuleIdentifier()))
            {
                for (T8ModuleInterfaceOperationMappingDefinition operationMappingDefinition : interfaceDefinition.getOperationMappingDefinitions())
                {
                    if (interfaceOperationIdentifier.equals(operationMappingDefinition.getInterfaceOperationIdentifier()))
                    {
                        return operationMappingDefinition;
                    }
                }
            }
        }

        return null;
    }

    private class ModuleEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ModuleInterfaceEventMappingDefinition eventMappingDefinition;
            T8ComponentEventDefinition eventDefinition;
            Map<String, Object> eventParameters;
            String moduleIdentifier;
            T8Component component;

            eventDefinition = event.getEventDefinition();
            component = event.getComponent();
            eventParameters = event.getEventParameters();

            // First get the event mapping definition for the event that has been reported by the sub-module.
            LOGGER.log("Module Container received event: " + eventDefinition.getPublicIdentifier() + " with parameters: " + eventParameters);
            moduleIdentifier = component.getComponentDefinition().getIdentifier();
            eventMappingDefinition = getInterfaceEventMappingDefinition(moduleIdentifier, eventDefinition.getPublicIdentifier());
            if (eventMappingDefinition != null)
            {
                T8ComponentEventDefinition interfaceEventDefinition;

                // Now get the interface/component event that is mapped to the sub-module event.
                interfaceEventDefinition = definition.getComponentEventDefinition(eventMappingDefinition.getInterfaceEventIdentifier());
                if (interfaceEventDefinition != null)
                {
                    // Re-route the event to the parent parentController of the container.
                    parentController.reportEvent(T8ModuleContainer.this, interfaceEventDefinition, T8IdentifierUtilities.mapParameters(eventParameters, eventMappingDefinition.getParameterMapping()));
                }
            }
        }
    }
}
