package com.pilog.t8.ui.flowtasklist;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8DataObjectClientApi;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.flowtasklist.T8FlowTaskListResource;
import com.pilog.t8.definition.ui.flowtasklist.T8FlowTaskListDefinition;
import com.pilog.t8.flow.task.T8TaskDataObject;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskList extends JXPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FlowTaskList.class);

    private final T8FlowTaskListDefinition definition;
    private final T8ComponentController controller;
    private final T8ConfigurationManager configurationManager;
    private final T8SessionContext sessionContext;
    private T8FlowManager flowManager;
    private TaskList taskListPanel;
    private T8TaskListSummary taskListSummary;
    private List<T8TaskDataObject> taskList;
    private final T8TaskFilter taskFilter;
    private T8DefaultComponentContainer contentContainer;
    private final T8FlowTaskListOperationHandler operationHandler;
    private final String taskObjectGroupId;
    private final T8DataObjectClientApi objApi;
    private TaskLoader lasTaskLoader;
    private Painter backgroundPainter;
    private final TaskViewListener taskViewListener;

    public T8FlowTaskList(T8FlowTaskListDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.sessionContext = controller.getSessionContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.taskFilter = new T8TaskFilter();
        this.taskFilter.setPageSize(10);
        this.taskViewListener = new TaskViewListener();
        this.operationHandler = new T8FlowTaskListOperationHandler(this);
        this.objApi = controller.getApi(T8DataObjectClientApi.API_IDENTIFIER);
        this.taskObjectGroupId = definition.getTaskObjectGroupId();
        initComponents();
        setupComponents();
        setupPainters();
        this.jXSearchFieldDescriptiveProp.setInstantSearchDelay(1000);
    }

    private void setupComponents()
    {
        taskListPanel = new TaskList(this);
        jPanelContent.add(taskListPanel, java.awt.BorderLayout.CENTER);

        contentContainer = new T8DefaultComponentContainer();
        contentContainer.setOpaque(false);
        contentContainer.setComponent(jPanelContent);
        add(contentContainer, BorderLayout.CENTER);
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize the painter for painting the background of this component.
        painterDefinition = definition.getBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
            setBackgroundPainter(backgroundPainter);
        }
    }

    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(controller.getContext(), inputString);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8FlowTaskListDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.flowManager = controller.getClientContext().getFlowManager();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void showContentView()
    {
        contentContainer.unlock();
        repaint();
    }

    public void showBusyView()
    {
        contentContainer.setMessage(translate("Loading Task List..."));
        contentContainer.lock();
        repaint();
    }

    private void performSearch()
    {
        taskFilter.setPageOffset(0);
        taskFilter.setDescriptivePropertySearchString(jXSearchFieldDescriptiveProp.getText());
        refreshTaskList();
    }

    public void retrieveNextPage()
    {
        int pageOffset;

        pageOffset = taskFilter.getPageOffset();
        pageOffset += taskFilter.getPageSize();
        taskFilter.setPageOffset(pageOffset);
        refreshTaskList();
    }

    private void retrievePreviousPage()
    {
        int pageOffset;

        pageOffset = taskFilter.getPageOffset();
        pageOffset -= taskFilter.getPageSize();
        if (pageOffset < 0) pageOffset = 0;
        taskFilter.setPageOffset(pageOffset);
        refreshTaskList();
    }

    public void filterTaskList(String taskId, boolean includedClaimedTasks, boolean includedUnclaimedTasks)
    {
        LOGGER.log("Filtering task list: " + taskId + " " + includedClaimedTasks + " " + includedUnclaimedTasks);
        taskFilter.clearIncludedTaskTypes();
        if (taskId != null) taskFilter.addIncludedTaskType(taskId);
        taskFilter.setPageOffset(0);
        taskFilter.setIncludeClaimedTasks(includedClaimedTasks);
        taskFilter.setIncludeUnclaimedTasks(includedUnclaimedTasks);
        refreshTaskList();
    }

    public void refreshTaskList()
    {
        if (lasTaskLoader != null) lasTaskLoader.cancel(false);

        showBusyView();
        lasTaskLoader = new TaskLoader();
        lasTaskLoader.execute();
    }

    private void executeTaskStartedFunctionality(T8TaskDataObject taskObject)
    {
        String functionalityId;

        functionalityId = definition.getTaskFunctionalityId();
        if (functionalityId != null)
        {
            // Execute the functionality.
            try
            {
                T8FunctionalityAccessHandle executionHandle;
                T8FunctionalityController functionalityController;

                functionalityController = controller.getFunctionalityController();
                if (functionalityController != null)
                {
                    Map<String, Object> functionalityParameters;

                    functionalityParameters = new HashMap<>();
                    functionalityParameters.put(functionalityId + "$P_FLOW_IID", taskObject.getFlowIid());
                    functionalityParameters.put(functionalityId + "$P_TASK_IID", taskObject.getTaskIid());
                    executionHandle = functionalityController.accessFunctionality(functionalityId, functionalityParameters);
                    if (executionHandle != null)
                    {
                        T8FunctionalityView functionalityView;

                        functionalityView = functionalityController.getFunctionalityView(executionHandle.getFunctionalityIid());
                        if (functionalityView != null)
                        {
                            functionalityView.addFunctionalityViewListener(taskViewListener);
                        }
                    }
                }
                else throw new Exception("No functionality controller found from: " + definition);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while executing task functionality: " + functionalityId + " using target object: " + taskObject, e);
            }
        }
    }

    private class TaskLoader extends SwingWorker<Void, Void>
    {
        @Override
        protected Void doInBackground() throws Exception
        {
            // Update the task list with the latest task details from the server.
            LOGGER.log("Refreshing task list for  User: " + sessionContext.getUserIdentifier() + " Profiles: " + sessionContext.getWorkFlowProfileIdentifiers() + "...");
            taskListSummary = flowManager.getTaskListSummary(controller.getContext(), taskFilter);
            taskList = objApi.searchGroup(taskObjectGroupId, taskFilter.getObjectFilter(sessionContext), taskFilter.getDescriptivePropertySearchString(), taskFilter.getPageOffset(), taskFilter.getPageSize(), false);
            return null;
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    int taskCount;

                    // The only pruposes of this statement is to rethrow any exceptions.
                    get();

                    // Set the task total and paginng counts on the UI.
                    taskCount = taskListSummary.getTaskCount(taskFilter);
                    jLabelTaskCount.setText("" + taskCount + "  "); // The last two spaces just adds a bit of padding on the tool bar.
                    jLabelPageStartOffset.setText("" + taskFilter.getPageOffset());
                    jLabelPageEndOffset.setText("" + (taskFilter.getPageOffset() + taskFilter.getPageSize() - 1));
                    jButtonPreviousPage.setEnabled(taskFilter.getPageOffset() != 0);
                    jButtonNextPage.setEnabled((taskFilter.getPageOffset() + taskFilter.getPageSize()) < taskCount);
                    taskListPanel.setTaskList(taskList);
                    taskListPanel.resetScrollPane();
                }
                catch (InterruptedException | ExecutionException e)
                {
                    LOGGER.log("Exception while refreshing task list.", e);
                }

                // Show the loaded data.
                showContentView();
            }
        }
    }

    void startTask(final T8TaskDataObject taskObject)
    {
        // Claim the task if it hasn't been alread or continue the task if it has.
        if (taskObject.isClaimed())
        {
            fireTaskContinuedEvent(taskObject);
        }
        else
        {
            try
            {
                flowManager.claimTask(controller.getContext(), taskObject.getTaskIid());
                fireTaskClaimedEvent(taskObject);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while trying to claim task: " + taskObject.getTaskIid(), e);
            }
        }

        // Execute the task started functionality.
        new Thread()
        {
            @Override
            public void run()
            {
                executeTaskStartedFunctionality(taskObject);
            }
        }.start();

        // Refresh the task list.
        refreshTaskList();
    }

    protected void fireTaskContinuedEvent(T8TaskDataObject taskObject)
    {
        HashMap<String, Object> eventParameters;

        LOGGER.log("Continuing Task: " + taskObject.getTaskIid() + " in flow: " + taskObject.getFlowIid());
        eventParameters = new HashMap<>();
        eventParameters.put(T8FlowTaskListResource.PARAMETER_TASK_INSTANCE_IDENTIFIER, taskObject.getTaskIid());
        eventParameters.put(T8FlowTaskListResource.PARAMETER_FLOW_INSTANCE_IDENTIFIER, taskObject.getFlowIid());
        controller.reportEvent(this, definition.getComponentEventDefinition(T8FlowTaskListResource.EVENT_TASK_CONTINUED), eventParameters);
    }

    protected void fireTaskClaimedEvent(T8TaskDataObject taskObject)
    {
        HashMap<String, Object> eventParameters;

        // Signal the task-claimed event.
        LOGGER.log("Claiming Task: " + taskObject.getTaskIid() + " in flow: " + taskObject.getFlowIid());
        eventParameters = new HashMap<>();
        eventParameters.put(T8FlowTaskListResource.PARAMETER_TASK_INSTANCE_IDENTIFIER, taskObject.getTaskIid());
        eventParameters.put(T8FlowTaskListResource.PARAMETER_FLOW_INSTANCE_IDENTIFIER, taskObject.getFlowIid());
        controller.reportEvent(this, definition.getComponentEventDefinition(T8FlowTaskListResource.EVENT_TASK_CLAIMED), eventParameters);
    }

    protected void fireTaskViewUpdatedEvent(T8FunctionalityView taskView)
    {
        HashMap<String, Object> eventParameters;

        // Signal the task-claimed event.
        eventParameters = new HashMap<>();
        eventParameters.put(T8FlowTaskListResource.PARAMETER_TASK_INSTANCE_IDENTIFIER, null);
        eventParameters.put(T8FlowTaskListResource.PARAMETER_FLOW_INSTANCE_IDENTIFIER, null);
        controller.reportEvent(this, definition.getComponentEventDefinition(T8FlowTaskListResource.EVENT_TASK_FUNCTIONALITY_EXECUTION_UPDATED), eventParameters);
    }

    protected void fireTaskViewClosedEvent(T8FunctionalityView taskView)
    {
        HashMap<String, Object> eventParameters;

        // Signal the task-claimed event.
        eventParameters = new HashMap<>();
        eventParameters.put(T8FlowTaskListResource.PARAMETER_TASK_INSTANCE_IDENTIFIER, null);
        eventParameters.put(T8FlowTaskListResource.PARAMETER_FLOW_INSTANCE_IDENTIFIER, null);
        controller.reportEvent(this, definition.getComponentEventDefinition(T8FlowTaskListResource.EVENT_TASK_FUNCTIONALITY_EXECUTION_ENDED), eventParameters);
    }

    private class TaskViewListener implements T8FunctionalityViewListener
    {
        @Override
        public void functionalityViewUpdated(T8FunctionalityViewUpdatedEvent event)
        {
            // Refresh the task list.
            refreshTaskList();

            // Fire an event to external listeners.
            fireTaskViewUpdatedEvent(event.getFunctionalityView());
        }

        @Override
        public void functionalityViewClosed(T8FunctionalityViewClosedEvent event)
        {
            // Refresh the task list.
            refreshTaskList();

            // Fire an event to external listeners.
            fireTaskViewClosedEvent(event.getFunctionalityView());
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jPanelToolBars = new javax.swing.JPanel();
        jLabelTaskSearch = new javax.swing.JLabel();
        jXSearchFieldDescriptiveProp = new org.jdesktop.swingx.JXSearchField();
        jToolBarPaging = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelTask = new javax.swing.JLabel();
        jLabelPageStartOffset = new javax.swing.JLabel();
        jLabelPageRangeDash = new javax.swing.JLabel();
        jLabelPageEndOffset = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jLabelTaskCount = new javax.swing.JLabel();

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.BorderLayout());

        jPanelToolBars.setOpaque(false);
        jPanelToolBars.setLayout(new java.awt.GridBagLayout());

        jLabelTaskSearch.setText(translate("Task Search:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelToolBars.add(jLabelTaskSearch, gridBagConstraints);

        jXSearchFieldDescriptiveProp.setMargin(new java.awt.Insets(3, 3, 3, 3));
        jXSearchFieldDescriptiveProp.setPreferredSize(new java.awt.Dimension(91, 21));
        jXSearchFieldDescriptiveProp.setPrompt(translate("Search"));
        jXSearchFieldDescriptiveProp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXSearchFieldDescriptivePropActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelToolBars.add(jXSearchFieldDescriptiveProp, gridBagConstraints);

        jToolBarPaging.setFloatable(false);
        jToolBarPaging.setRollover(true);
        jToolBarPaging.setOpaque(false);

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(translate("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPaging.add(jButtonPreviousPage);

        jLabelTask.setText(translate("Task: "));
        jToolBarPaging.add(jLabelTask);
        jToolBarPaging.add(jLabelPageStartOffset);

        jLabelPageRangeDash.setText("-");
        jToolBarPaging.add(jLabelPageRangeDash);
        jToolBarPaging.add(jLabelPageEndOffset);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(translate("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPaging.add(jButtonNextPage);

        jLabelTaskCount.setText("(0)  ");
        jToolBarPaging.add(jLabelTaskCount);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelToolBars.add(jToolBarPaging, gridBagConstraints);

        jPanelContent.add(jPanelToolBars, java.awt.BorderLayout.NORTH);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        retrievePreviousPage();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        retrieveNextPage();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    private void jXSearchFieldDescriptivePropActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXSearchFieldDescriptivePropActionPerformed
    {//GEN-HEADEREND:event_jXSearchFieldDescriptivePropActionPerformed
        performSearch();
    }//GEN-LAST:event_jXSearchFieldDescriptivePropActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JLabel jLabelPageEndOffset;
    private javax.swing.JLabel jLabelPageRangeDash;
    private javax.swing.JLabel jLabelPageStartOffset;
    private javax.swing.JLabel jLabelTask;
    private javax.swing.JLabel jLabelTaskCount;
    private javax.swing.JLabel jLabelTaskSearch;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelToolBars;
    private javax.swing.JToolBar jToolBarPaging;
    private org.jdesktop.swingx.JXSearchField jXSearchFieldDescriptiveProp;
    // End of variables declaration//GEN-END:variables

}
