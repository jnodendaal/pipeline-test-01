package com.pilog.t8.ui.flowclient;

import com.pilog.t8.ui.T8ClientController;
import com.pilog.t8.definition.ui.T8ClientControllerApiHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowClientOperationHandler extends T8ComponentOperationHandler
{
    private final T8ClientController flowClient;

    public T8FlowClientOperationHandler(T8ClientController flowClient)
    {
        super(flowClient);
        this.flowClient = flowClient;
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        if (operationId.equals(T8ClientControllerApiHandler.OPERATION_LOAD_MODULE))
        {
            try
            {
                String moduleId;

                moduleId = (String)operationParameters.get(T8ClientControllerApiHandler.PARAMETER_MODULE_IDENTIFIER);
                flowClient.loadModule(moduleId, (Map<String, Object>)operationParameters.get(T8ClientControllerApiHandler.PARAMETER_INPUT_PARAMETERS));
                return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while executing flow client operation: " + T8ClientControllerApiHandler.OPERATION_LOAD_MODULE, e);
            }
        }
        else if (operationId.equals(T8ClientControllerApiHandler.OPERATION_SWITCH_FLOW))
        {
            String flowInstanceIdentifier;

            flowInstanceIdentifier = (String)operationParameters.get(T8ClientControllerApiHandler.PARAMETER_FLOW_INSTANCE_IDENTIFIER);

            try
            {
                flowClient.switchFlow(flowInstanceIdentifier, null);
                return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while switching to flow instance: " + flowInstanceIdentifier, e);
            }
        }
        else if (operationId.equals(T8ClientControllerApiHandler.OPERATION_START_NEW_FLOW))
        {
            flowClient.startNewFlow((String)operationParameters.get(T8ClientControllerApiHandler.PARAMETER_FLOW_IDENTIFIER));
            return null;
        }
        else return super.executeOperation(operationId, operationParameters);
    }
}
