package com.pilog.t8.ui.busypanel;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.concurrent.TimeUnit;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.swingx.painter.BusyPainter;

/**
 * @author Bouwer du Preez
 */
public class T8BusyPainter extends BusyPainter
{
    private Component parentComponent;
    private Animator animator;
    private int delay;
    private int height;
    private int currentFrame;
    private Direction direction;

    public T8BusyPainter(Component parentComponent, int height, int points, int trailLength)
    {
        super(getScaledDefaultPoint(height, points), getScaledDefaultTrajectory(height, points));
        this.parentComponent = parentComponent;
        this.delay = 100;
        this.height = height;
        this.currentFrame = 0;
        this.direction = Direction.RIGHT;
        this.setPoints(points);
        this.setTrailLength(trailLength);
        this.setAntialiasing(true);
        this.setCacheable(false);

        initializeAnimator();
    }

    protected static Shape getScaledDefaultTrajectory(int height, int points)
    {
        return new Ellipse2D.Float(((height * 8) / 26) / 2, ((height * 8) / 26) / 2, height - ((height * 8) / 26), height - ((height * 8) / 26));
    }

    protected static Shape getScaledDefaultPoint(int height, int points)
    {
        return new Rectangle2D.Float(0, 0, (height * 8) / 26, 2);
    }

    private BusyPainter getBusyPainter()
    {
        return this;
    }

    private void frameChanged()
    {
        if (parentComponent.isVisible())
        {
            parentComponent.repaint();
        }
    }

    private void initializeAnimator()
    {
        int maxFrames;

        maxFrames = getBusyPainter().getPoints();

        animator = new Animator.Builder()
                .addTarget(PropertySetter.getTarget(this, "currentFrame", 0, maxFrames))
                .setRepeatBehavior(Animator.RepeatBehavior.LOOP)
                .setRepeatCount(Animator.INFINITE)
                .setDuration(maxFrames / 2, TimeUnit.SECONDS)
                .setStartDirection(direction == Direction.RIGHT ? Animator.Direction.FORWARD : Animator.Direction.BACKWARD)
                .build();
    }

    public void startAnimation()
    {
        if(!animator.isRunning())
        {
            animator.start();
        }
    }

    public void stopAnimation()
    {
        if(animator.isRunning())
        {
            animator.stop();
        }
    }

    @Override
    protected void doPaint(Graphics2D g, Object t, int width, int height)
    {
        super.doPaint(g, t, this.height, this.height);
    }

    public int getCurrentFrame()
    {
        return currentFrame;
    }

    public void setCurrentFrame(int currentFrame)
    {
        this.currentFrame = currentFrame;
        getBusyPainter().setFrame(currentFrame);

        frameChanged();
    }
}
