package com.pilog.t8.ui.componentcontainer;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.task.T8ComponentTask;
import com.pilog.t8.operation.progressreport.T8CategorizedProgressReport;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.ui.T8ComponentContainer;
import com.pilog.t8.ui.busypanel.T8BusyPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultComponentContainer extends JComponent implements T8ComponentContainer
{
    private static final T8Logger logger = T8Log.getLogger(T8DefaultComponentContainer.class);

    private final T8BusyPanel busyPanel;
    private final List<T8ComponentTask> executingTasks;
    private final JLayeredPane layer;
    private final JPanel contentContainer;
    private final JComponent glassPaneComponent;
    private JComponent component;
    private int progressUpdateInterval;
    private Timer timer; // Timer used to schedule maintenance tasks.

    public T8DefaultComponentContainer(JComponent component)
    {
        this.executingTasks = new ArrayList<>();
        this.layer = new JLayeredPane();
        this.busyPanel = new T8BusyPanel("");
        this.glassPaneComponent = createGlassPaneComponent();
        this.contentContainer = new JPanel(new BorderLayout());

        this.setLayout(new BorderLayout());
        this.add(layer, BorderLayout.CENTER);
        this.progressUpdateInterval = 200;

        layer.add(glassPaneComponent, new Integer(100));
        layer.add(busyPanel, new Integer(50));
        layer.add(contentContainer, new Integer(0));

        this.glassPaneComponent.setLocation(0, 0);
        this.busyPanel.setLocation(0, 0);
        this.contentContainer.setLocation(0, 0);
        this.contentContainer.setOpaque(false);

        setComponent(component);
        setLocked(false);
        setSize(500, 500);
    }

    public T8DefaultComponentContainer()
    {
        this(null);
    }

    public void setContainerOpaque(boolean opaque)
    {
        setOpaque(opaque);
        contentContainer.setOpaque(opaque);
        layer.setOpaque(opaque);

        repaint();
    }

    @Override
    public JComponent getComponent()
    {
        return component;
    }

    @Override
    public void setComponent(JComponent component)
    {
        // If the specified component is already set, no need to do anything.
        if (this.component != component)
        {
            this.component = component;
            this.contentContainer.removeAll();
            if (component != null)
            {
                this.contentContainer.add(component, BorderLayout.CENTER);
                this.component.invalidate();
                this.contentContainer.validate();
            }
        }

        repaint();
    }

    @Override
    public void setMessage(String message)
    {
        busyPanel.setMessage(message);
    }

    @Override
    public void setProgressMessage(String message)
    {
        busyPanel.setProgressMessage(message);
    }

    @Override
    public void setCategoryMessage(String message)
    {
        busyPanel.setCategoryMessage(message);
    }

    @Override
    public void setProgress(int progress)
    {
        busyPanel.setProgress(progress);
    }

    @Override
    public void setCategoryProgress(int progress)
    {
        busyPanel.setCategoryProgress(progress);
    }

    @Override
    public void setIndeterminate(boolean indeterminate)
    {
        busyPanel.setIndeterminate(indeterminate);
    }

    @Override
    public void setProgressUpdateInterval(int milliseconds)
    {
        this.progressUpdateInterval = milliseconds;
    }

    @Override
    public void lock()
    {
        setLocked(true);
    }

    @Override
    public void unlock()
    {
        setLocked(false);
    }

    @Override
    public void setLocked(final boolean locked)
    {
        busyPanel.setBusy(locked);
        busyPanel.setVisible(locked);
        glassPaneComponent.setVisible(locked);

        repaint();
    }

    @Override
    public boolean isLocked()
    {
        return busyPanel.isBusy();
    }

    public void addTask(T8ComponentTask task)
    {
        synchronized (executingTasks)
        {
            executingTasks.add(task);
            if (timer == null)
            {
                timer = new Timer();
                timer.schedule(new OperationExpirationChecker(), progressUpdateInterval, progressUpdateInterval);
            }
        }
    }

    @Override
    public int getExecutingTaskCount()
    {
        return executingTasks.size();
    }

    private final JComponent createGlassPaneComponent()
    {
        JComponent glassPaneComponent;

        glassPaneComponent = new JComponent(){};

        glassPaneComponent.addMouseListener(new MouseAdapter(){});
        glassPaneComponent.addKeyListener(new KeyAdapter(){});
        glassPaneComponent.setOpaque(false);

        return glassPaneComponent;
    }

    @Override
    public void setBounds(int x, int y, int width, int height)
    {
        this.glassPaneComponent.setSize(width, height);
        this.busyPanel.setSize(width, height);
        this.contentContainer.setSize(width, height);

        super.setBounds(x, y, width, height);
    }

    @Override
    public Dimension getPreferredSize()
    {
        return contentContainer.getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize()
    {
        return contentContainer.getMaximumSize();
    }

    @Override
    public Dimension getMaximumSize()
    {
        return contentContainer.getMinimumSize();
    }

    private class OperationExpirationChecker extends TimerTask
    {
        @Override
        public void run()
        {
            synchronized (executingTasks)
            {
                List<T8ComponentTask> completedTasks;
                T8ComponentTask lastExecutingTask;

                lastExecutingTask = null;
                completedTasks = new ArrayList<T8ComponentTask>();
                for (T8ComponentTask task : executingTasks)
                {
                    synchronized (task)
                    {
                        if (task.isActive())
                        {
                            lastExecutingTask = task;
                        }
                        else
                        {
                            completedTasks.add(task);
                            try
                            {
                                task.notifyAll();
                            }
                            catch (IllegalMonitorStateException e)
                            {
                                logger.log("Exception while notifying the waiting threads that this thread has completed.", e);
                            }
                        }
                    }
                }

                // Remove all completed tasks and also update the UI message with that of the last executing task found in the stack.
                executingTasks.removeAll(completedTasks);
                if (lastExecutingTask != null)
                {
                    int progress;

                    progress = (int)lastExecutingTask.getProgress();
                    setIndeterminate(lastExecutingTask.isIndeterminate());

                    if (lastExecutingTask.getProgressReportObject() instanceof T8CategorizedProgressReport)
                    {
                        T8CategorizedProgressReport progressReport = (T8CategorizedProgressReport)lastExecutingTask.getProgressReportObject();
                        setMessage(lastExecutingTask.getMessage());
                        setProgressMessage(progressReport.getProgressMessage());
                        setProgress(progressReport.getProgress().intValue());
                        setCategoryMessage(progressReport.getCategoryMessage());
                        setCategoryProgress(progressReport.getCategoryProgress().intValue());
                    }
                    else if (lastExecutingTask.getProgressReportObject() instanceof T8ProgressReport)
                    {
                        T8ProgressReport progressReport = (T8ProgressReport)lastExecutingTask.getProgressReportObject();
                        setMessage(lastExecutingTask.getMessage());
                        setProgressMessage(progressReport.getProgressMessage());
                        setProgress(progressReport.getProgress().intValue());
                    }
                    else
                    {
                        setMessage("    " + lastExecutingTask.getMessage() + progress + "%    ");
                        setProgress(progress);
                    }
                    lock();
                }
                else
                {
                    // If all tasks in the stack have been completed, end the timer and unlock the UI.
                    timer.cancel();
                    timer = null;
                    unlock();
                }
            }
        }
    }
}
