package com.pilog.t8.ui.columnpane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.columnpane.T8ColumnPaneColumnDefinition;
import com.pilog.t8.definition.ui.columnpane.T8ColumnPaneDefinition;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class T8ColumnPane extends JPanel implements T8Component
{
    private HashMap<String, T8Component> childComponents;
    private T8ColumnPaneDefinition definition;
    private T8ComponentController controller;
    private List<JPanel> columnContainers;
    private double totalWeight;

    public T8ColumnPane(T8ColumnPaneDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.childComponents = new HashMap<String, T8Component>();
        setOpaque(definition.isOpaque());
        setBorder(null);
        this.columnContainers = new ArrayList<JPanel>();
        this.totalWeight = 0.0f;
        setLayout(new GridBagLayout());
        createColumns();
    }

    private void createColumns()
    {
        removeAll();
        totalWeight = 0.0f;
        columnContainers.clear();
        for (T8ColumnPaneColumnDefinition columnDefinition : definition.getColumnDefinitions())
        {
            GridBagConstraints constraints;
            JPanel columnContainer;

            columnContainer = new ColumnContainer(this, columnDefinition);
            constraints = new GridBagConstraints();
            constraints.anchor = GridBagConstraints.NORTH;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.weightx = columnDefinition.getWeight();
            constraints.weighty = 0.1;
            constraints.insets = new Insets(0, getColumnLeftInset(), 0, getColumnRightInset());

            totalWeight += columnDefinition.getWeight();
            columnContainers.add(columnContainer);
            add(columnContainer, constraints);
        }
    }

    private double getTotalWeight()
    {
        return totalWeight;
    }

    public int getColumnCount()
    {
        return definition.getColumnDefinitions().size();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> hm)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        List<T8ColumnPaneColumnDefinition> columnDefinitions;

        columnDefinitions = definition.getColumnDefinitions();
        for (int columnIndex = 0; columnIndex < columnDefinitions.size(); columnIndex++)
        {
            T8ColumnPaneColumnDefinition columnDefinition;

            columnDefinition = columnDefinitions.get(columnIndex);
            for (T8ComponentDefinition columnComponentDefinition : columnDefinition.getComponentDefinitions())
            {
                if (childComponent.getComponentDefinition().getIdentifier().equals(columnComponentDefinition.getIdentifier()))
                {
                    GridBagConstraints gridConstraints;
                    JPanel columnContainer;

                    columnContainer = columnContainers.get(columnIndex);

                    gridConstraints = new GridBagConstraints();
                    gridConstraints.anchor = GridBagConstraints.NORTH;
                    gridConstraints.fill = GridBagConstraints.HORIZONTAL;
                    gridConstraints.weightx = 0.1;
                    gridConstraints.weighty = 0;
                    gridConstraints.gridx = 0;
                    gridConstraints.gridy = columnContainer.getComponentCount();
                    gridConstraints.insets = new Insets(getComponentTopInset(), 0, getComponentBottomInset(), 0);
                    columnContainer.add((Component)childComponent, gridConstraints);
                }
            }
        }

        validate();
    }

    @Override
    public T8Component getChildComponent(String componentIdentifier)
    {
        return childComponents.get(componentIdentifier);
    }

    @Override
    public T8Component removeChildComponent(String componentIdentifier)
    {
        if (childComponents.containsKey(componentIdentifier))
        {
            Component childComponent;

            childComponent = (Component)childComponents.get(componentIdentifier);
            for (JPanel columnContainer : columnContainers)
            {
                if (columnContainer.isAncestorOf(childComponent))
                {
                    columnContainer.remove((Component)childComponents.get(componentIdentifier));
                    return childComponents.remove(componentIdentifier);
                }
            }

            return null;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<T8Component>(childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponents.clear();
    }

    protected int getComponentTopInset()
    {
        int verticalSpacing;

        verticalSpacing = definition.getVerticalSpacing();
        return verticalSpacing / 2;
    }

    protected int getComponentBottomInset()
    {
        int verticalSpacing;

        verticalSpacing = definition.getVerticalSpacing();
        return verticalSpacing / 2;
    }

    protected List<JPanel> getColumnContainers()
    {
        return this.columnContainers;
    }

    private int getColumnLeftInset()
    {
        int horizontalSpacing;

        horizontalSpacing = definition.getHorizontalSpacing();
        return horizontalSpacing / 2;
    }

    private int getColumnRightInset()
    {
        int horizontalSpacing;

        horizontalSpacing = definition.getHorizontalSpacing();
        return horizontalSpacing / 2;
    }

    private class ColumnContainer extends JPanel
    {
        private T8ColumnPane columnPane;
        private T8ColumnPaneColumnDefinition columnDefinition;

        public ColumnContainer(T8ColumnPane columnPane, T8ColumnPaneColumnDefinition columnDefinition)
        {
            this.columnPane = columnPane;
            this.columnDefinition = columnDefinition;
            this.setOpaque(false);
            this.setLayout(new GridBagLayout());
        }

        @Override
        public Dimension getPreferredSize()
        {
            int preferredWidth;

            preferredWidth = columnDefinition.getPreferredWidth();
            return new Dimension(preferredWidth > 0 ? preferredWidth : 10, super.getPreferredSize().height);
        }

        @Override
        public Dimension getMinimumSize()
        {
            return new Dimension(0, 0);
        }
    }
}
