package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.security.sso.T8SingleSignOnHandlerDefinition;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8SingleSignOnHandler;
import java.util.Map;

import static com.pilog.t8.definition.system.T8SecurityManagerResource.*;
import com.sun.jna.platform.win32.Secur32;
import com.sun.jna.platform.win32.Secur32Util;

/**
 * @author Bouwer du Preez
 */
public class T8SingleSignOnClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private T8SingleSignOnHandler ssoHandler;

    public static final String API_IDENTIFIER = "@API_SSO_CLIENT";

    public T8SingleSignOnClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8SingleSignOnClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    private T8SingleSignOnHandler getHandler()
    {
        if (ssoHandler != null)
        {
            return ssoHandler;
        }
        else
        {
            try
            {
                T8SingleSignOnHandlerDefinition definition;
                Map<String, Object> outputParameters;

                outputParameters = T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_SSO_HANDLER_DEFINITION, null);
                definition = (T8SingleSignOnHandlerDefinition)outputParameters.get(PARAMETER_DEFINITION);
                if (definition != null)
                {
                    ssoHandler = definition.createSingleSignOnHandler(clientContext);
                    return ssoHandler;
                }
                else return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception during request for SSO handler definition.", e);
            }
        }
    }

    public boolean isSsoAvailable()
    {
        T8SingleSignOnHandler handler;

        handler = getHandler();
        return handler != null;
    }

    public boolean signOn()
    {
        T8SingleSignOnHandler handler;

        handler = getHandler();
        if (handler != null)
        {
            return handler.signOn();
        }
        else throw new RuntimeException("SSO not configured.");
    }

    public String getCurrentUserName()
    {
        T8SingleSignOnHandler handler;

        handler = getHandler();
        if (handler != null)
        {
            return Secur32Util.getUserNameEx(Secur32.EXTENDED_NAME_FORMAT.NameSamCompatible);
        }
        else throw new RuntimeException("SSO not configured.");
    }

    public String getCurrentUserDisplayName()
    {
        T8SingleSignOnHandler handler;

        handler = getHandler();
        if (handler != null)
        {
            return Secur32Util.getUserNameEx(Secur32.EXTENDED_NAME_FORMAT.NameDisplay);
        }
        else throw new RuntimeException("SSO not configured.");
    }
}
