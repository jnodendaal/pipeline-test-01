package com.pilog.t8.ui.busypanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.jdesktop.swingx.painter.AbstractPainter;

/**
 * @author Bouwer du Preez
 */
public class T8BusyLabelPainter extends AbstractPainter<Object>
{
    private T8BusyPainter busyPainter;
    private int busyPainterHeight;
    private String text;
    private int leftInset;
    private int textInset;
    
    public T8BusyLabelPainter(Component parentComponent, int height, int points, int trailLength, String text, int leftInset, int textInset)
    {
        this.busyPainter = new T8BusyPainter(parentComponent, height, points, trailLength);
        this.busyPainterHeight = height;
        this.text = text;
        this.leftInset = leftInset;
        this.textInset = textInset;
    }
    
    public void startAnimation()
    {
        busyPainter.startAnimation();
    }
    
    public void stopAnimation()
    {
        busyPainter.stopAnimation();
    }

    @Override
    protected void doPaint(Graphics2D g2, Object input, int width, int height)
    {
        Graphics2D busyPainterG;
        Rectangle clipBounds;
        FontMetrics fontMetrics;
        float textY;
        int inset;
        
        inset = (int)((float)(height - busyPainterHeight) / 2f);
        busyPainterG = (Graphics2D)g2.create(leftInset, inset, busyPainterHeight, busyPainterHeight);
        
        // Paint the busy indicator.
        busyPainter.doPaint(busyPainterG, input, width, height);
        
        // Paint the label text.
        g2.setColor(Color.BLACK);
        fontMetrics = g2.getFontMetrics();
        clipBounds = g2.getClipBounds();
        textY = clipBounds.y + (clipBounds.height / 2.0f) + (fontMetrics.getHeight() / 2.0f);
        textY = textY - fontMetrics.getDescent();
        g2.drawString(text, clipBounds.x + busyPainterHeight + leftInset + textInset, textY);
    }
}
