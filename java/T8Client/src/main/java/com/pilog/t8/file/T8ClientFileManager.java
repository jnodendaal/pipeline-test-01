package com.pilog.t8.file;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.file.T8FileManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFileManager implements T8FileManager
{
    private final Map<String, T8FileUploadOperation> uploadOperations;
    private final Map<String, T8FileDownloadOperation> downloadOperations;

    public T8ClientFileManager()
    {
        this.uploadOperations = Collections.synchronizedMap(new HashMap<String, T8FileUploadOperation>());
        this.downloadOperations = Collections.synchronizedMap(new HashMap<String, T8FileDownloadOperation>());
    }

    @Override
    public boolean openFileContext(T8Context context, String contextIid, String contextId, T8TimeUnit expirationTime) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_CONTEXT_ID, contextId);
        operationParameters.put(PARAMETER_EXPIRATION_TIME, expirationTime);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_OPEN_FILE_CONTEXT, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public boolean closeFileContext(T8Context context, String contextIid) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CLOSE_FILE_CONTEXT, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public boolean fileContextExists(T8Context context, String contextIid) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CHECK_FILE_CONTEXT_EXISTENCE, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public boolean fileExists(T8Context context, String contextIid, String filePath) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CHECK_FILE_EXISTENCE, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public boolean createDirectory(T8Context context, String contextIid, String directoryPath) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, directoryPath);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_DIRECTORY, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public void renameFile(T8Context context, String contextIid, String filePath, String newFilename) throws Exception, FileNotFoundException
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        operationParameters.put(PARAMETER_FILE_NAME, newFilename);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_RENAME_FILE, operationParameters);
    }

    @Override
    public boolean deleteFile(T8Context context, String contextIid, String filePath) throws Exception, FileNotFoundException
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_DELETE_FILE, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public long getFileSize(T8Context context, String contextIid, String filePath) throws Exception, FileNotFoundException
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        return (Long)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FILE_SIZE, operationParameters).get(PARAMETER_BYTE_SIZE);
    }

    @Override
    public String getMd5Checksum(T8Context context, String contextIid, String filePath) throws Exception, FileNotFoundException
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        return (String)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_MD5_CHECKSUM, operationParameters).get(PARAMETER_MD5_CHECKSUM_HEX);
    }

    @Override
    public T8FileDetails getFileDetails(T8Context context, String contextIid, String path) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, path);
        return (T8FileDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FILE_DETAILS, operationParameters).get(PARAMETER_FILE_DETAILS);
    }

    @Override
    public List<T8FileDetails> getFileList(T8Context context, String contextIid, String filePath) throws Exception, FileNotFoundException
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        return (List<T8FileDetails>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FILE_DETAILS_LIST, operationParameters).get(PARAMETER_FILE_DETAILS_LIST);
    }

    @Override
    public T8FileDetails uploadFile(T8Context context, String contextIid, String localFilePath, String remoteFilePath) throws Exception
    {
        if (!uploadOperations.containsKey(localFilePath))
        {
            try
            {
                final T8FileUploadOperation uploadOperation;

                uploadOperation = new T8FileUploadOperation(context, this, contextIid, localFilePath, remoteFilePath);
                uploadOperations.put(localFilePath, uploadOperation);
                uploadOperation.start();
                return uploadOperation.getUploadedFileDetails();
            }
            finally
            {
                uploadOperations.remove(localFilePath);
            }
        }
        else throw new RuntimeException("Duplicate file upload attempt: " + localFilePath);
    }

    @Override
    public long getUploadOperationBytesUploaded(T8Context context, String filePath) throws RuntimeException
    {
        T8FileUploadOperation uploadOperation;

        uploadOperation = uploadOperations.get(filePath);
        if (uploadOperation != null)
        {
            return uploadOperation.getByteSizeUploaded();
        }
        else return -1;
    }

    @Override
    public long getUploadOperationFileSize(T8Context context, String filePath) throws RuntimeException
    {
        T8FileUploadOperation uploadOperation;

        uploadOperation = uploadOperations.get(filePath);
        if (uploadOperation != null)
        {
            return uploadOperation.getTotalByteSize();
        }
        else return -1;
    }

    @Override
    public int getUploadOperationProgress(T8Context context, String filePath) throws RuntimeException
    {
        T8FileUploadOperation uploadOperation;

        uploadOperation = uploadOperations.get(filePath);
        if (uploadOperation != null)
        {
            return uploadOperation.getProgress();
        }
        else return -1;
    }

    @Override
    public long downloadFile(T8Context context, String contextIid, String localFilePath, String remoteFilePath) throws Exception, FileNotFoundException
    {
        if (!downloadOperations.containsKey(localFilePath))
        {
            try
            {
                final T8FileDownloadOperation downloadOperation;

                downloadOperation = new T8FileDownloadOperation(context, this, contextIid, localFilePath, remoteFilePath);
                downloadOperations.put(localFilePath, downloadOperation);
                downloadOperation.start();
                return downloadOperation.getByteSizeDownloaded();
            }
            finally
            {
                downloadOperations.remove(localFilePath);
            }
        }
        else throw new RuntimeException("Duplicate file download attempt: " + localFilePath);
    }

    @Override
    public long getDownloadOperationBytesDownloaded(T8Context context, String localFilePath) throws RuntimeException
    {
        T8FileDownloadOperation downloadOperation;

        downloadOperation = downloadOperations.get(localFilePath);
        if (downloadOperation != null)
        {
            return downloadOperation.getByteSizeDownloaded();
        }
        else return -1;
    }

    @Override
    public long getDownloadOperationFileSize(T8Context context, String localFilePath) throws RuntimeException, FileNotFoundException
    {
        T8FileDownloadOperation downloadOperation;

        downloadOperation = downloadOperations.get(localFilePath);
        if (downloadOperation != null)
        {
            return downloadOperation.getTotalByteSize();
        }
        else return -1;
    }

    @Override
    public int getDownloadOperationProgress(T8Context context, String localFilePath) throws RuntimeException
    {
        T8FileDownloadOperation downloadOperation;

        downloadOperation = downloadOperations.get(localFilePath);
        if (downloadOperation != null)
        {
            return downloadOperation.getProgress();
        }
        else return -1;
    }

    @Override
    public long uploadFileData(T8Context context, String contextIid, String filePath, byte[] fileData) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        operationParameters.put(PARAMETER_BINARY_DATA, fileData);
        return (Long)T8MainServerClient.executeSynchronousOperation(context, OPERATION_UPLOAD_FILE_DATA, operationParameters).get(PARAMETER_BYTE_SIZE);
    }

    @Override
    public byte[] downloadFileData(T8Context context, String contextIid, String filePath, long fileOffset, int byteSize) throws Exception, FileNotFoundException
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_CONTEXT_IID, contextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        operationParameters.put(PARAMETER_FILE_OFFSET, fileOffset);
        operationParameters.put(PARAMETER_BYTE_SIZE, byteSize);
        return (byte[])T8MainServerClient.executeSynchronousOperation(context, OPERATION_DOWNLOAD_FILE_DATA, operationParameters).get(PARAMETER_BINARY_DATA);
    }

    @Override
    public void init() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void destroy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public InputStream getFileInputStream(T8Context context, String contextIid, String filePath) throws Exception, FileNotFoundException
    {
        File tempFile;

        tempFile = File.createTempFile(T8IdentifierUtilities.createNewGUID(), null);
        tempFile.deleteOnExit();

        downloadFile(context, contextIid, tempFile.getAbsolutePath(), filePath);

        return new FileInputStream(tempFile);
    }

    @Override
    public OutputStream getFileOutputStream(T8Context context, String contextIid, String filePath, boolean append) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RandomAccessStream getFileRandomAccessStream(T8Context context, String contextIid, String filePath) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8FileHandler getFileHandler(T8Context context, String fileTypeId, String contextIid, String filePath)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File getFile(T8Context context, String contextIid, String filePath) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
