package com.pilog.t8.config;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8UserManagerConfiguration;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerEventAdapter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.definition.system.property.T8SystemPropertyDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.definition.ui.laf.T8LookAndFeelDefinition;
import com.pilog.t8.definition.user.property.T8UserPropertyDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.api.T8Api;

import static com.pilog.t8.definition.system.T8ConfigurationManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientConfigurationManager implements T8ConfigurationManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClientConfigurationManager.class);

    private final T8ClientContext clientContext;
    private final T8DefinitionManager definitionManager;
    private final Map<String, Map<String, String>> translationMaps;
    private final Map<String, Set<String>> untranslatedSets;
    private final Map<String, Set<String>> requestedPhraseSets;
    private final Map<String, Object> userProperties;
    private final Map<String, T8Api> apiCache;
    private Map<String, String> apiClassMapping;
    private Thread requestorThread; // This variable holds a reference to the current requestor thread (used to request new UI translations).
    private T8Context requesterContext;
    private T8LookAndFeelManager lafManager;

    public T8ClientConfigurationManager(T8ClientContext clientContext)
    {
        this.clientContext = clientContext;
        this.definitionManager = clientContext.getDefinitionManager();
        this.clientContext.getDefinitionManager().addDefinitionManagerListener(new DefinitionRefreshListener());
        this.translationMaps = new HashMap<>();
        this.untranslatedSets = new HashMap<>();
        this.requestedPhraseSets = new HashMap<>();
        this.apiCache = new HashMap<>();
        this.userProperties = new HashMap<>();
    }

    @Override
    public void init()
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public String getUITranslation(T8Context context, String inputString)
    {
        return getUITranslation(context, context.getSessionContext().getLanguageIdentifier(), inputString);
    }

    @Override
    public String getUITranslation(T8Context context, String languageIdentifier, String inputString)
    {
        if (languageIdentifier != null)
        {
            Map<String, String> translationMap;

            // If the required language translation Map is not available, fetch it from the server.
            if (!translationMaps.containsKey(languageIdentifier))
            {
                LOGGER.log("Retrieving UI Translation Map for language: " + languageIdentifier);
                translationMaps.put(languageIdentifier, getUITranslationMap(context, languageIdentifier));
            }

            // Get the translation map for the required language and then fetch the translated String from it.
            translationMap = translationMaps.get(languageIdentifier);
            if (translationMap != null)
            {
                String translatedString;

                translatedString = translationMap.get(inputString);
                if (translatedString != null)
                {
                    return translatedString;
                }
                else
                {
                    // Request the translation if it has not already been done.
                    if (!hasUITranslationBeenRequested(languageIdentifier, inputString))
                    {
                        addUITranslationRequest(context, languageIdentifier, inputString);
                    }

                    // Return the input String untranslated.
                    return inputString;
                }
            }
            else return inputString;
        }
        else return inputString;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <P> P getProperty(T8Context context, String propertyId)
    {
        try
        {
            T8SystemPropertyDefinition propertyDefinition;

            propertyDefinition = (T8SystemPropertyDefinition)definitionManager.getRawDefinition(context, null, propertyId);
            if (propertyDefinition != null)
            {
                return (P)propertyDefinition.getPropertyValue();
            }
            else throw new RuntimeException("System Property '" + propertyId + "' not found.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while retrieving system property: " + propertyId, e);
            throw new RuntimeException("Exception while retrieving system property: " + propertyId, e);
        }
    }

    @Override
    public <P> P getProperty(T8Context context, String propertyId, P defaultValue)
    {
        try
        {
            return getProperty(context, propertyId);
        }
        catch (Exception ex)
        {
            LOGGER.log(T8Logger.Level.WARNING, "System property '" + propertyId + "' not found. Returning default value: " + defaultValue);
            return defaultValue;
        }
    }

    @Override
    public Map<String, String> getUITranslationMap(T8Context context, String languageIdentifier)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_LANGUAGE_IDENTIFIER, languageIdentifier);
            return (Map<String, String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_UI_TRANSLATION_MAP, operationParameters).get(PARAMETER_UI_TRANSLATION_MAP);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while retrieving UI Translation Map for language: " + languageIdentifier, e);
            return null;
        }
    }

    @Override
    public void requestUITranslations(T8Context context, String languageIdentifier, Set<String> phrases)
    {
        if (phrases != null)
        {
            for (String phrase : phrases)
            {
                // Request the translation if it has not already been done.
                if (!hasUITranslationBeenRequested(languageIdentifier, phrase))
                {
                    addUITranslationRequest(context, languageIdentifier, phrase);
                }
            }
        }
    }

    private void addUITranslationRequest(T8Context context, String languageIdentifier, String inputString)
    {
        synchronized (untranslatedSets)
        {
            Set<String> untranslatedSet;

            // If the required list is not available, create it.
            if (!untranslatedSets.containsKey(languageIdentifier))
            {
                untranslatedSets.put(languageIdentifier, new HashSet<>());
            }

            // Add the untranslated String to the set of requests.
            untranslatedSet = untranslatedSets.get(languageIdentifier);
            untranslatedSet.add(inputString);

            // If not requestor thread is running, start a new one.
            if (requestorThread == null)
            {
                requesterContext = context;
                requestorThread = new RequestorThread();
                requestorThread.start();
            }
        }
    }

    /**
     * Checks the requested phrase collection to find out if a specific
     * translation request has already been sent to the server.
     *
     * @param languageIdentifier The identifier of the translation language.
     * @param inputString The phrase to be translated.
     * @return A boolean value indicating whether or not the specified phrase
     * has already been requested from the server in the specified translation
     * language.
     */
    private boolean hasUITranslationBeenRequested(String languageIdentifier, String inputString)
    {
        Set<String> requestedTranslationSet;

        requestedTranslationSet = requestedPhraseSets.get(languageIdentifier);
        if (requestedTranslationSet != null)
        {
            return requestedTranslationSet.contains(inputString);
        }
        else return false;
    }

    /**
     * Adds all of the supplied phrases to the requested phrase collection which
     * is used to check phrases already requested from the server so that no
     * duplicate requests are sent.
     *
     * @param languageIdentifier The identifier of the translation language.
     * @param phrases The phrase to be translated.
     */
    private void addRequestedPhrases(String languageIdentifier, Set<String> phrases)
    {
        synchronized (requestedPhraseSets)
        {
            Set<String> untranslatedSet;

            // If the required list is not available, create it.
            if (!requestedPhraseSets.containsKey(languageIdentifier))
            {
                requestedPhraseSets.put(languageIdentifier, new HashSet<>());
            }

            // Add the untranslated String to the set of requests.
            untranslatedSet = requestedPhraseSets.get(languageIdentifier);
            untranslatedSet.addAll(phrases);
        }
    }

    @Override
    public T8Api getAPI(T8Context context, String apiId)
    {
        // We synchronize on the API cache so that it can not be altered while we are potentially making changes to it.
        synchronized (apiCache)
        {
            T8Api api;

            // First try to fetch the API from the cache.
            api = apiCache.get(apiId);
            if (api != null)
            {
                return api;
            }
            else // The API could not be found in the cache, so create a new instance.
            {
                try
                {
                    Map<String, String> apiClassMap;

                    apiClassMap = getAPIClassMap(context);
                    if (apiClassMap != null)
                    {
                        String apiClassName;

                        apiClassName = apiClassMap.get(apiId);
                        if (!Strings.isNullOrEmpty(apiClassName))
                        {
                            api = T8Reflections.getFastFailInstance(apiClassName, new Class<?>[]{T8Context.class}, context);

                            // Add the new instance to the API cache and then return it.
                            // TODO:  Some API facade's will not be cachable in future, so we will need to add a check for those cases.
                            apiCache.put(apiId, api);
                            return api;
                        }
                        else throw new Exception("API not found: " + apiId);
                    }
                    else throw new Exception("Class Mapping not found while trying to resolved API request: " + apiId);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Could not initialize API: " + apiId, e);
                }
            }
        }
    }

    @Override
    public Map<String, String> getAPIClassMap(T8Context context)
    {
        try
        {
            if (apiClassMapping == null)
            {
                apiClassMapping = (Map<String, String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_API_CLASS_MAP, null).get(PARAMETER_API_CLASS_MAP);
                return apiClassMapping;
            }
            else return apiClassMapping;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching API Class Mapping.", e);
        }
    }

    @Override
    public T8LookAndFeelManager getLookAndFeelManager(T8Context context)
    {
        if (lafManager != null)
        {
            return lafManager;
        }
        else
        {
            // Create a new look and feel manager.
            lafManager = new T8LookAndFeelManager(clientContext, context);

            try
            {
                T8LookAndFeelDefinition lookAndFeelDefinition;

                lookAndFeelDefinition = (T8LookAndFeelDefinition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_LOOK_AND_FEEL_DEFINITION, null).get(PARAMETER_LOOK_AND_FEEL_DEFINITION);
                if (lookAndFeelDefinition != null) lafManager.init(lookAndFeelDefinition);
                return lafManager;
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while fetching Look and Feel Definition.", e);
                return lafManager;
            }
        }
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void clearCache()
    {
        LOGGER.log("Clearing client translation cache...");
        untranslatedSets.clear();
        requestedPhraseSets.clear();
        translationMaps.clear();
    }

    @Override
    public <V> void setUserProperty(T8Context context, String propertyId, V value)
    {
        T8UserPropertyDefinition propertyDefinition;

        try
        {
            propertyDefinition = definitionManager.getRawDefinition(context, null, propertyId);

            // If the definition exists, we can set the value
            if (propertyDefinition != null) this.userProperties.put(propertyId, value);
            else throw new IllegalArgumentException("User Property {"+propertyId+"} has not been set up and cannot be used.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while retrieving user property definition: " + propertyId, e);
            throw new RuntimeException("Exception while retrieving user property: " + propertyId, e);
        }

        if (propertyDefinition == null) throw new IllegalArgumentException("User Property {"+propertyId+"} has not been set up and cannot be used.");
    }

    @Override
    public <V> V getUserProperty(T8Context context, String identifier, V defaultValue)
    {
        return (V)this.userProperties.getOrDefault(identifier, defaultValue);
    }

    @Override
    public T8OperationalHoursCalculator getOperationalHoursCalculator(T8Context context) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8UserManagerConfiguration getUserManagerConfiguration()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class RequestorThread extends Thread
    {
        private RequestorThread()
        {
            this.setDaemon(true);
            this.setName("T8UITranslationRequester");
        }

        @Override
        public void run()
        {
            try
            {
                // Wait for 60 seconds.  This allows time for a batch of UI requests to be accumulated before sending the requests to the server.
                Thread.sleep(60000);
            }
            catch (InterruptedException e)
            {
                LOGGER.log("Exception while waiting for new UI Requests to accumulate.", e);
            }

            // Send the requests to the server.
            synchronized (untranslatedSets)
            {
                for (String languageIdentifier : untranslatedSets.keySet())
                {
                    try
                    {
                        HashMap<String, Object> operationParameters;
                        Set<String> untranslatedSet;

                        // Get the untranslated set of phrases.
                        untranslatedSet = untranslatedSets.get(languageIdentifier);

                        LOGGER.log("Requesting UI Translations for language: " + languageIdentifier);
                        operationParameters = new HashMap<>();
                        operationParameters.put(PARAMETER_LANGUAGE_IDENTIFIER, languageIdentifier);
                        operationParameters.put(PARAMETER_UI_UNTRANSLATED_PHRASE_SET, untranslatedSet);
                        T8MainServerClient.executeSynchronousOperation(requesterContext, OPERATION_REQUEST_UI_TRANSLATIONS, operationParameters);

                        // Because we don't want the phrases to be re-added to the untranslated set, we store them for checking in the requested phrase collection.
                        addRequestedPhrases(languageIdentifier, untranslatedSet);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while requesting UI Translations for language: " + languageIdentifier, e);
                    }
                }

                // Remove the untranslated phrases so they are not sent to the server again later.
                untranslatedSets.clear();
            }

            // Finally clear the requestor thread reference, so that subsequent requests will generate a new thread.
            requestorThread = null;
            LOGGER.log("UI Translations requestor thread ends.");
        }
    }

    private class DefinitionRefreshListener extends T8DefinitionManagerEventAdapter
    {
        @Override
        public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent event)
        {
            clearCache();
        }
    }
}
