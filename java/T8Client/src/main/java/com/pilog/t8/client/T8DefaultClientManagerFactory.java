package com.pilog.t8.client;

import com.pilog.t8.client.notification.T8ClientNotificationManager;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.definition.T8ClientDefinitionManager;
import com.pilog.t8.config.T8ClientConfigurationManager;
import com.pilog.t8.file.T8ClientFileManager;
import com.pilog.t8.flow.T8ClientFlowManager;
import com.pilog.t8.functionality.T8ClientFunctionalityManager;
import com.pilog.t8.process.T8ClientProcessManager;
import com.pilog.t8.security.T8ClientSecurityManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.service.T8ClientServiceManager;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultClientManagerFactory
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public T8DefaultClientManagerFactory(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
    }

    public T8DefinitionManager constructDefinitionManager()
    {
        return new T8ClientDefinitionManager(context);
    }

    public T8SecurityManager constructSecurityManager()
    {
        return new T8ClientSecurityManager(clientContext);
    }

    public T8ProcessManager constructProcessManager()
    {
        return new T8ClientProcessManager();
    }

    public T8FlowManager constructFlowManager()
    {
        return new T8ClientFlowManager();
    }

    public T8FunctionalityManager constructFunctionalityManager()
    {
        return new T8ClientFunctionalityManager();
    }

    public T8FileManager constructFileManager()
    {
        return new T8ClientFileManager();
    }

    public T8ConfigurationManager constructConfigurationManager()
    {
        return new T8ClientConfigurationManager(clientContext);
    }

    public T8ServiceManager constructServiceManager()
    {
        return new T8ClientServiceManager(context);
    }

    public T8CommunicationManager constructCommunicationManager()
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.communication.T8ClientCommunicationManager", new Class<?>[]{T8Context.class}, this.context);
    }

    public T8NotificationManager constructNotificationManager()
    {
        return new T8ClientNotificationManager(this.clientContext);
    }
}
