package com.pilog.t8.client.notification;

import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.notification.T8NotificationFilter;
import com.pilog.t8.notification.T8NotificationSummary;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8ClientNotificationManagerScriptFacade
{
    private final T8NotificationManager notificationManager;
    private final T8Context context;

    public T8ClientNotificationManagerScriptFacade(T8Context context, T8NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
        this.context = context;
    }

    public T8NotificationSummary getUserNotificationSummary(T8NotificationFilter notificationFilter) throws Exception
    {
        return this.notificationManager.getUserNotificationSummary(context, notificationFilter);
    }
}