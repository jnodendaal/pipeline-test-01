package com.pilog.t8.ui.datacombobox;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxDefinition;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxDataHandler
{
    private final T8DataComboBoxDefinition tableDefinition;

    public T8DataComboBoxDataHandler(T8DataComboBoxDefinition comboBoxDefinition)
    {
        this.tableDefinition = comboBoxDefinition;
    }

    public DefaultComboBoxModel<T8DataEntity> loadComboBoxData(T8Context context, T8DataFilter dataFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;
        List<T8DataEntity> retrievedData;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, tableDefinition.getDataEntityIdentifier());
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);
        operationParameters.put(PARAMETER_PAGE_OFFSET, 0);
        operationParameters.put(PARAMETER_PAGE_SIZE, 5000);

        retrievedData = (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_LIST);
        return new DefaultComboBoxModel(retrievedData.toArray());
    }
}
