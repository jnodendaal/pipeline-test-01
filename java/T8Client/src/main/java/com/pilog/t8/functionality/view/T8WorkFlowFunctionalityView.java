package com.pilog.t8.functionality.view;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ClientControllerApiHandler;
import com.pilog.t8.definition.ui.T8ClientControllerDefinition;
import com.pilog.t8.functionality.T8WorkflowFunctionalityAccessHandle;
import com.pilog.t8.ui.T8ClientController;
import java.awt.BorderLayout;
import java.util.Map;
import java.util.Objects;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowFunctionalityView extends JPanel implements T8FunctionalityView
{
    private static final T8Logger logger = T8Log.getLogger(T8WorkFlowFunctionalityView.class);

    private final T8WorkflowFunctionalityAccessHandle accessHandle;
    private final T8ComponentController controller;
    private final T8ComponentEventListener viewListener;
    private final String flowInstanceIdentifier;
    private final String taskInstanceIdentifier;
    private final EventListenerList viewListeners;
    private T8ClientController flowClient;

    public T8WorkFlowFunctionalityView(T8ComponentController controller, T8WorkflowFunctionalityAccessHandle accessHandle, String flowIid, String taskIid)
    {
        this.controller = controller;
        this.viewListener = new ViewEventListener(flowIid, taskIid);
        this.accessHandle = accessHandle;
        this.flowInstanceIdentifier = flowIid;
        this.taskInstanceIdentifier = taskIid;
        this.viewListeners = new EventListenerList();
        this.setOpaque(false);
        setLayout(new BorderLayout());
    }

    @Override
    public T8FunctionalityAccessHandle getExecutionHandle()
    {
        return accessHandle;
    }

    @Override
    public void initialize() throws Exception
    {
        // Stop the module already loaded.
        if (flowClient == null)
        {
            flowClient = new T8ClientController(new T8ClientControllerDefinition(accessHandle.getFunctionalityIid()), controller);
            add(flowClient, BorderLayout.CENTER);
        }
    }

    @Override
    public void startComponent()
    {
        controller.addComponentEventListener(viewListener);
        if (flowClient != null)
        {
            try
            {
                flowClient.startComponent();
                flowClient.switchFlow(flowInstanceIdentifier, taskInstanceIdentifier);
            }
            catch (Exception e)
            {
                logger.log("Exception while switching to functionality workflow.", e);
            }
        }
    }

    @Override
    public void stopComponent()
    {
        controller.removeComponentEventListener(viewListener);
        if (flowClient != null)
        {
            flowClient.stopComponent();
        }
    }

    private void fireFunctionalityViewUpdatedEvent()
    {
        final T8FunctionalityViewUpdatedEvent event;

        // Create and fire the event.
        event = new T8FunctionalityViewUpdatedEvent(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                for (T8FunctionalityViewListener viewListener : viewListeners.getListeners(T8FunctionalityViewListener.class))
                {
                    viewListener.functionalityViewUpdated(event);
                }
            }
        });
    }

    private void fireFunctionalityViewClosedEvent()
    {
        final T8FunctionalityViewClosedEvent event;

        // Create and fire the event.
        event = new T8FunctionalityViewClosedEvent(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                for (T8FunctionalityViewListener viewListener : viewListeners.getListeners(T8FunctionalityViewListener.class))
                {
                    viewListener.functionalityViewClosed(event);
                }
            }
        });
    }

    @Override
    public void addFunctionalityViewListener(T8FunctionalityViewListener listener)
    {
        viewListeners.add(T8FunctionalityViewListener.class, listener);
    }

    @Override
    public void removeFunctionalityViewListener(T8FunctionalityViewListener listener)
    {
        viewListeners.remove(T8FunctionalityViewListener.class, listener);
    }

    @Override
    public String toString()
    {
        return "T8WorkFlowClientFunctionalityView{" + "flowInstanceIdentifier=" + flowInstanceIdentifier + ", taskInstanceIdentifier=" + taskInstanceIdentifier + '}';
    }

    private class ViewEventListener implements T8ComponentEventListener
    {
        private final String parentFlowIid;
        private final String parentTaskIid;

        public ViewEventListener(String parentFlowIid, String parentTaskIid)
        {
            this.parentFlowIid = parentFlowIid;
            this.parentTaskIid = parentTaskIid;
        }

        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            Map<String, Object> eventParameters;
            String eventIdentifier;

            eventDefinition = event.getEventDefinition();
            eventParameters = event.getEventParameters();
            eventIdentifier = eventDefinition.getIdentifier();

            switch (eventIdentifier)
            {
                case T8ClientControllerApiHandler.EVENT_TASK_COMPLETED:
                {
                    // Make sure we have some event parameters before proceeding.
                    if (eventParameters != null)
                    {
                        String eventFlowIid;
                        String eventTaskIid;

                        // Get the Flow iid and Task iid of the event.
                        eventFlowIid = (String)eventParameters.get(T8ClientControllerApiHandler.PARAMETER_FLOW_INSTANCE_IDENTIFIER);
                        eventTaskIid = (String)eventParameters.get(T8ClientControllerApiHandler.PARAMETER_TASK_INSTANCE_IDENTIFIER);

                        // If the flow event occurred on the same flow as the one this view is on, then this functionality has been updated.
                        if (Objects.equals(parentFlowIid, eventFlowIid))
                        {
                            logger.log("Signalling " + accessHandle + " view update.  Cause: " + event);
                            fireFunctionalityViewUpdatedEvent();
                        }
                    }

                    break;
                }
                case T8ClientControllerApiHandler.EVENT_TASKS_COMPLETED:
                {
                    // Make sure we have some event parameters before proceeding.
                    if (eventParameters != null)
                    {
                        String eventFlowIid;
                        String eventTaskIid;

                        // Get the Flow iid and Task iid of the event.
                        eventFlowIid = (String)eventParameters.get(T8ClientControllerApiHandler.PARAMETER_FLOW_INSTANCE_IDENTIFIER);
                        eventTaskIid = (String)eventParameters.get(T8ClientControllerApiHandler.PARAMETER_TASK_INSTANCE_IDENTIFIER);

                        // If the flow event occurred on the same flow as the one this view is on, then this functionality has been updated.
                        if (Objects.equals(parentFlowIid, eventFlowIid))
                        {
                            logger.log("Signalling " + accessHandle + " view closure.  Cause: " + event);
                            fireFunctionalityViewClosedEvent();
                        }
                    }

                    break;
                }
            }
        }
    }
}
