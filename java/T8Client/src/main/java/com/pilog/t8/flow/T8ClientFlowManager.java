package com.pilog.t8.flow;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.flow.event.T8FlowEventListener;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFlowManager implements T8FlowManager
{
    public T8ClientFlowManager()
    {
    }

    @Override
    public void addFlowEventListener(T8Context context, T8FlowEventListener tl)
    {
        throw new UnsupportedOperationException("Not supported from client-side.");
    }

    @Override
    public void removeFlowEventListener(T8Context context, T8FlowEventListener tl)
    {
        throw new UnsupportedOperationException("Not supported from client-side.");
    }

    @Override
    public void retryExecution(T8Context context, String flowInstanceIdentifier, String nodeInstanceIdentifier)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_FLOW_IID, flowInstanceIdentifier);
            operationParameters.put(PARAMETER_NODE_IID, nodeInstanceIdentifier);
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_RETRY_EXECUTION, operationParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrying flow execution.", e);
        }
    }

    @Override
    public List<String> findActiveInputNodeInstanceIdentifiers(T8Context context, String flowInstanceIdentifier, String nodeInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FLOW_IID, flowInstanceIdentifier);
        operationParameters.put(PARAMETER_NODE_IID, nodeInstanceIdentifier);
        return (List<String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_FIND_ACTIVE_INPUT_NODE_IIDS, operationParameters).get(PARAMETER_NODE_IIDS);
    }

    @Override
    public int getTaskCount(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = HashMaps.createSingular(PARAMETER_TASK_FILTER, (Object)taskFilter);
        return (int)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_TASK_COUNT, operationParameters).get(PARAMETER_TASK_COUNT);
    }

    @Override
    public T8TaskListSummary getTaskListSummary(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = HashMaps.createSingular(PARAMETER_TASK_FILTER, (Object)taskFilter);
        return (T8TaskListSummary)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_TASK_LIST_SUMMARY, operationParameters).get(PARAMETER_TASK_LIST_SUMMARY);
    }

    @Override
    public T8TaskList getTaskList(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_FILTER, taskFilter);
        return (T8TaskList)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_TASK_LIST, operationParameters).get(PARAMETER_TASK_LIST);
    }

    @Override
    public void claimTask(T8Context context, String taskInstanceIdentifier)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_CLAIM_TASK, operationParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while claiming task: " + taskInstanceIdentifier, e);
        }
    }

    @Override
    public void unclaimTask(T8Context context, String taskInstanceIdentifier)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_UNCLAIM_TASK, operationParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while unclaiming task: " + taskInstanceIdentifier, e);
        }
    }

    @Override
    public T8TaskCompletionResult completeTask(T8Context context, String taskInstanceIdentifier, Map<String, Object> outputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
        operationParameters.put(PARAMETER_OUTPUT_PARAMETERS, outputParameters);
        return (T8TaskCompletionResult)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COMPLETE_TASK, operationParameters).get(PARAMETER_TASK_COMPLETION_RESULT);
    }

    @Override
    public T8FlowStatus getFlowStatus(T8Context context, String flowInstanceIdentifier, boolean includeDisplayData) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FLOW_IID, flowInstanceIdentifier);
        operationParameters.put(PARAMETER_INCLUDE_DISPLAY_DATA, includeDisplayData);
        return (T8FlowStatus)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FLOW_STATUS, operationParameters).get(PARAMETER_FLOW_STATUS);
    }

    @Override
    public T8FlowState getFlowState(T8Context context, String flowInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FLOW_IID, flowInstanceIdentifier);
        return (T8FlowState)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FLOW_STATE, operationParameters).get(PARAMETER_FLOW_STATE);
    }

    @Override
    public T8FlowStatus queueFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FLOW_ID, flowId);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (T8FlowStatus)T8MainServerClient.executeSynchronousOperation(context, OPERATION_QUEUE_FLOW, operationParameters).get(PARAMETER_FLOW_STATUS);
    }

    @Override
    public T8FlowStatus startFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FLOW_ID, flowId);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (T8FlowStatus)T8MainServerClient.executeSynchronousOperation(context, OPERATION_START_NEW_FLOW, operationParameters).get(PARAMETER_FLOW_STATUS);
    }

    @Override
    public void killFlow(T8Context context, String flowInstanceIdentifier)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_FLOW_IID, flowInstanceIdentifier);
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_KILL_FLOW, operationParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while stopping server-side flow: " + flowInstanceIdentifier, e);
        }
    }

    @Override
    public void fireSignalEvent(T8Context context, String signalIdentifier, Map<String, Object> signalParameters)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_SIGNAL_ID, signalIdentifier);
            operationParameters.put(PARAMETER_SIGNAL_PARAMETERS, signalParameters);
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_SIGNAL, operationParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching flow count from server.", e);
        }
    }

    @Override
    public int getCachedFlowCount(T8Context context)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_CACHED_FLOW_COUNT, operationParameters).get(PARAMETER_FLOW_COUNT);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching flow count from server.", e);
        }
    }

    @Override
    public int clearFlowCache(T8Context context)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CLEAR_FLOW_CACHE, operationParameters).get(PARAMETER_FLOW_COUNT);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while requesting flow cache clear from server.", e);
        }
    }

    @Override
    public int getFlowCount(T8Context context)
    {
        try
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<>();
            return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FLOW_COUNT, operationParameters).get(PARAMETER_FLOW_COUNT);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching flow count from server.", e);
        }
    }

    @Override
    public void init() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void destroy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8TaskDetails getTaskDetails(T8Context context, String taskInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
        return (T8TaskDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_TASK_DETAIL, operationParameters).get(PARAMETER_TASK_DETAIL);
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTaskPriority(T8Context context, String taskInstanceIdentifier, int priority) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
        operationParameters.put(PARAMETER_TASK_PRIORITY, priority);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_SET_TASK_PRIORITY, operationParameters);
    }

    @Override
    public void decreaseTaskPriority(T8Context context, String taskInstanceIdentifier, int amount) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
        operationParameters.put(PARAMETER_TASK_PRIORITY_AMOUNT, amount);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_DECREASE_TASK_PRIORITY, operationParameters);
    }

    @Override
    public void increaseTaskPriority(T8Context context, String taskInstanceIdentifier, int amount) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
        operationParameters.put(PARAMETER_TASK_PRIORITY_AMOUNT, amount);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_INCREASE_TASK_PRIORITY, operationParameters);
    }

    @Override
    public void reassignTask(T8Context context, String taskInstanceIdentifier, String userIdentifier, String profileIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TASK_IID, taskInstanceIdentifier);
        operationParameters.put(PARAMETER_USER_ID, userIdentifier);
        operationParameters.put(PARAMETER_PROFILE_ID, profileIdentifier);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_REASSIGN_TASK, operationParameters);
    }

    @Override
    public void refreshTaskList(T8Context context, T8TaskListRefreshType refreshType, List<String> ids) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_REFRESH_TYPE, refreshType.toString());
        operationParameters.put(PARAMETER_ID_LIST, ids);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_REFRESH_TASK_LIST, operationParameters);
    }

    @Override
    public Map<String, Object> getTaskUiData(T8Context context, String taskIid) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void recacheTaskEscalationTriggers(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_RECACHE_TASK_ESCALATION_TRIGGERS, operationParameters);
    }
}
