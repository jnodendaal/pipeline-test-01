package com.pilog.t8.script;

import com.pilog.epic.EPIC;
import com.pilog.epic.ParserContext;
import com.pilog.epic.Program;
import com.pilog.t8.client.T8ScriptConfigurationHandler;
import com.pilog.t8.client.definition.T8ClientDefinitionManagerScriptFacade;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.T8ClientDataManagerScriptFacade;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.data.document.path.EpicDocPathParser;
import com.pilog.t8.definition.script.T8ClientContextScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.file.T8ClientFileManagerScriptFacade;
import com.pilog.t8.flow.T8ClientFlowManagerScriptFacade;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8ClientSecurityManagerScriptFacade;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultClientContextScript implements T8ClientContextScript
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultClientContextScript.class);

    protected T8ClientContext clientContext;
    protected T8SessionContext sessionContext;
    protected T8Context context;
    protected T8ScriptDefinition scriptDefinition;
    protected final String projectId;
    protected Program program;
    protected double progress;
    protected T8ProgressReport progressReport;
    private final Object progressLock = new Object();
    private final T8ScriptExecutionHandle executionHandle;
    private final T8ScriptConfigurationHandler configurationHandler;
    protected final DocPathEvaluatorFactory docPathFactory;

    public T8DefaultClientContextScript(T8Context context, T8ClientContextScriptDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.sessionContext = context.getSessionContext();
        this.scriptDefinition = definition;
        this.projectId = scriptDefinition.getRootProjectId();
        this.executionHandle = new T8ScriptExecutionHandle();
        this.docPathFactory = DocPathEvaluatorFactory.getFactory(context);
        this.configurationHandler = new T8ScriptConfigurationHandler(context, docPathFactory);
        this.progress = -1;
    }

    public T8DefaultClientContextScript(T8ComponentController controller, T8ClientContextScriptDefinition definition)
    {
        this.clientContext = controller.getClientContext();
        this.sessionContext = controller.getSessionContext();
        this.context = controller.getContext();
        this.scriptDefinition = definition;
        this.projectId = scriptDefinition.getRootProjectId();
        this.executionHandle = new T8ScriptExecutionHandle();
        this.docPathFactory = DocPathEvaluatorFactory.getFactory(context);
        this.configurationHandler = new T8ScriptConfigurationHandler(controller, docPathFactory);
        this.progress = -1;
    }

    @Override
    public double getProgress()
    {
        synchronized(progressLock)
        {
            return progress;
        }
    }

    @Override
    public T8ProgressReport getProgressReport()
    {
        return progressReport;
    }

    @Override
    public void prepareScript() throws Exception
    {
        List<T8ScriptClassImport> classImports;
        List<T8ScriptMethodImport> methodImports;
        ParserContext parserContext;

        // Create the new parser context.
        parserContext = new ParserContext();
        parserContext.addExternalExpressionParser(new EpicDocPathParser(docPathFactory));

        // Add all standard server-side imports to the EPIC program.
        addProgramImports(parserContext);

        // Get the script namespace and compiled the script into an EPIC program.
        program = EPIC.compileProgram(scriptDefinition.getScript(), "UTF-8", parserContext);

        // Add all class imports that are specific to this script type.
        classImports = scriptDefinition.getScriptClassImports();
        if (classImports != null)
        {
            for (T8ScriptClassImport classImport : classImports)
            {
                program.addClassImport(classImport.getReference(), classImport.getClassToImport());
            }
        }

        // Add all method imports that are specific to this script type.
        methodImports = scriptDefinition.getScriptMethodImports();
        if (methodImports != null)
        {
            for (T8ScriptMethodImport methodImport : methodImports)
            {
                program.addImport(methodImport.getProcedureName(), methodImport.getContextObject(), methodImport.getMethod());
            }
        }
    }

    @Override
    public void finalizeScript() throws Exception
    {
        // Ensure that all open resources are closed.
    }

    @Override
    public Map<String, Object> executePreparedScript(Map<String, ? extends Object> inputParameters) throws Exception
    {
        Map<String, Object> scriptInputParameters;
        Object scriptOutputObject;

        // Get the script namespace and script parameters.
        scriptInputParameters = T8IdentifierUtilities.stripNamespace(scriptDefinition.getNamespace(), inputParameters, false);
        if (scriptInputParameters == null)
        {
            scriptInputParameters = new HashMap<String, Object>();
        }

        // Add the default script parameters.
        scriptInputParameters.put("session", sessionContext);
        scriptInputParameters.put("access", context);
        scriptInputParameters.put("executionHandle", executionHandle);
        scriptInputParameters.put("dataManager", new T8ClientDataManagerScriptFacade(context));
        scriptInputParameters.put("flowManager", new T8ClientFlowManagerScriptFacade(context, clientContext.getFlowManager()));
        scriptInputParameters.put("fileManager", new T8ClientFileManagerScriptFacade(context, clientContext.getFileManager()));
        scriptInputParameters.put("definitionManager", new T8ClientDefinitionManagerScriptFacade(context, clientContext.getDefinitionManager()));
        scriptInputParameters.put("securityManager", new T8ClientSecurityManagerScriptFacade(context, clientContext.getSecurityManager()));

        // Execute the program.
        try
        {
            program.execute(scriptInputParameters);
        }
        catch (Exception e)
        {
            // Log the exception.
            LOGGER.log("Exception while executing script '" + scriptDefinition + "' using input parameters: " + inputParameters, e);
            throw e;
        }

        // Get the EPIC program output and add all the context output parameters.
        scriptOutputObject = program.getReturnObject();
        if ((scriptOutputObject == null) || (scriptOutputObject instanceof Map))
        {
            Map<String, Object> scriptOutputParameters;

            // Convert all output parameters to script output parameters.
            scriptOutputParameters = (Map<String, Object>)program.getReturnObject();
            scriptOutputParameters = T8IdentifierUtilities.prependNamespace(scriptDefinition.getNamespace(), scriptOutputParameters, false);
            return scriptOutputParameters;
        }
        else throw new Exception("Script '" + scriptDefinition + "' did not return a Map result.  Return type: " + scriptOutputObject.getClass());
    }

    @Override
    public Map<String, Object> executeScript(Map<String, ? extends Object> inputParameters) throws Exception
    {
        Map<String, Object> scriptOutputParameters;

        // Start a new data transaciton.
        prepareScript();

        // Run the prepared script and do some cleanup.
        try
        {
            // Run the prepared script.
            scriptOutputParameters = executePreparedScript(inputParameters);

            // Return the script output parameters.
            return scriptOutputParameters;
        }
        finally
        {
            // Finalize the script.
            finalizeScript();
        }
    }

    protected void addProgramImports(ParserContext parserContext) throws Exception
    {
        configurationHandler.configureParserContext(parserContext);
    }

    @Override
    public void addMethodImport(String procedureName, Object contextObject, Method method)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
