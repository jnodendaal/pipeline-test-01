package com.pilog.t8.client;

import com.pilog.epic.ParserContext;
import com.pilog.epic.annotation.EPICClass;
import com.pilog.epic.annotation.EPICMethod;
import com.pilog.epic.annotation.EPICParameter;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.tag.T8DataTagFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.exception.T8RaisedException;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.time.T8Date;
import com.pilog.t8.time.T8Time;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.security.T8PasswordChangeResponse;
import com.pilog.t8.security.T8UserLoginResponse;
import com.pilog.t8.ui.dialog.exception.T8ExceptionDialog;
import com.pilog.t8.ui.module.T8ModuleComponentController;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Day;
import com.pilog.t8.time.T8Hour;
import com.pilog.t8.time.T8Millisecond;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.time.T8Second;
import com.pilog.t8.time.T8Year;
import com.pilog.t8.utilities.strings.Strings;

/**
 * @author Bouwer du Preez
 */
@EPICClass(PreferredVariableName = "",VariableCreationTemplate = "", onlyShowDeclaredMethods = true)
public class T8ScriptConfigurationHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ScriptConfigurationHandler.class);

    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DialogHandler dialogHandler;
    private final DocPathEvaluatorFactory docPathFactory;

    public T8ScriptConfigurationHandler(T8ComponentController controller, DocPathEvaluatorFactory docPathFactory)
    {
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.dialogHandler = new T8DialogHandler(controller);
        this.docPathFactory = docPathFactory;
    }

    public T8ScriptConfigurationHandler(T8Context context, DocPathEvaluatorFactory docPathFactory)
    {
        this.controller = null;
        this.clientContext = context.getClientContext();
        this.context = context;
        this.dialogHandler = null; // No dialogs can be used if a component controller is not supplied.
        this.docPathFactory = docPathFactory;
    }

    @EPICMethod(Hidden = true, Description = "")
    public void configureParserContext(ParserContext parserContext)
    {
        try
        {
            // Class imports.
            parserContext.addClassImport(T8IdentifierUtilities.class.getSimpleName(), T8IdentifierUtilities.class);
            parserContext.addClassImport(T8DataUtilities.class.getSimpleName(), T8DataUtilities.class);
            parserContext.addClassImport(T8DataFilter.class.getSimpleName(), T8DataFilter.class);
            parserContext.addClassImport(T8DataFilterCriteria.class.getSimpleName(), T8DataFilterCriteria.class);
            parserContext.addClassImport(T8DataFilterCriterion.class.getSimpleName(), T8DataFilterCriterion.class);
            parserContext.addClassImport(T8DataTagFilter.class.getSimpleName(), T8DataTagFilter.class);
            parserContext.addClassImport(Arrays.class.getSimpleName(), Arrays.class);
            parserContext.addClassImport(T8OntologyConcept.class.getSimpleName(), T8OntologyConcept.class);
            parserContext.addClassImport(T8OntologyTerm.class.getSimpleName(), T8OntologyTerm.class);
            parserContext.addClassImport(T8OntologyAbbreviation.class.getSimpleName(), T8OntologyAbbreviation.class);
            parserContext.addClassImport(T8OntologyDefinition.class.getSimpleName(), T8OntologyDefinition.class);
            parserContext.addClassImport(T8OntologyCode.class.getSimpleName(), T8OntologyCode.class);
            parserContext.addClassImport(T8OntologyLink.class.getSimpleName(), T8OntologyLink.class);
            parserContext.addClassImport(T8OntologyConcept.class.getSimpleName(), T8OntologyConcept.class);
            parserContext.addClassImport(T8OntologyTerm.class.getSimpleName(), T8OntologyTerm.class);
            parserContext.addClassImport(T8OntologyAbbreviation.class.getSimpleName(), T8OntologyAbbreviation.class);
            parserContext.addClassImport(T8OntologyDefinition.class.getSimpleName(), T8OntologyDefinition.class);
            parserContext.addClassImport(T8OntologyCode.class.getSimpleName(), T8OntologyCode.class);
            parserContext.addClassImport(T8OntologyConceptType.class.getSimpleName(), T8OntologyConceptType.class);
            parserContext.addClassImport(T8PrimaryOntologyDataType.class.getSimpleName(), T8PrimaryOntologyDataType.class);
            parserContext.addClassImport(RequirementType.class.getSimpleName(), RequirementType.class);
            parserContext.addClassImport(T8Timestamp.class.getSimpleName(), T8Timestamp.class);
            parserContext.addClassImport(T8Time.class.getSimpleName(), T8Time.class);
            parserContext.addClassImport(T8Date.class.getSimpleName(), T8Date.class);
            parserContext.addClassImport(Strings.class.getSimpleName(), Strings.class);

            // Method imports.
            parserContext.addMethodImport("docPath", this, this.getClass().getDeclaredMethod("docPath", Object.class, String.class));
            parserContext.addMethodImport("getFlowCount", this, this.getClass().getDeclaredMethod("getFlowCount", new Class<?>[0]));
            parserContext.addMethodImport("getSystemProperty", this, this.getClass().getDeclaredMethod("getSystemProperty", String.class));
            parserContext.addMethodImport("do", this, this.getClass().getDeclaredMethod("executeComponentOperation", String.class, String.class, Map.class));
            parserContext.addMethodImport("doGet", this, this.getClass().getDeclaredMethod("executeComponentGetOperation", String.class, String.class, Map.class, String.class));
            parserContext.addMethodImport("accessFunctionality", this, this.getClass().getDeclaredMethod("accessFunctionality", String.class, Map.class, String.class));
            parserContext.addMethodImport("execControllerScript", this, this.getClass().getDeclaredMethod("executeControllerScript", String.class, Map.class));
            parserContext.addMethodImport("execModuleScript", this, this.getClass().getDeclaredMethod("executeModuleScript", String.class, Map.class));
            parserContext.addMethodImport("execBackgroundServerProc", this, this.getClass().getDeclaredMethod("executeAsynchronousServerProcess", String.class, Map.class));
            parserContext.addMethodImport("execServerOp", this, this.getClass().getDeclaredMethod("executeSynchronousServerOperation", String.class, Map.class, String.class));
            parserContext.addMethodImport("execProgressiveServerOp", this, this.getClass().getDeclaredMethod("executeAsynchronousServerOperation", String.class, Map.class, String.class));
            parserContext.addMethodImport("execBackgroundServerOp", this, this.getClass().getDeclaredMethod("executeAsynchronousServerOperation", String.class, Map.class, String.class));
            parserContext.addMethodImport("execClientOp", this, this.getClass().getDeclaredMethod("executeClientOperation", String.class, Map.class));
            parserContext.addMethodImport("execServerScript", this, this.getClass().getDeclaredMethod("executeSynchronousServerOperation", String.class, Map.class, String.class));
            parserContext.addMethodImport("execBackgroundServerScript", this, this.getClass().getDeclaredMethod("executeAsynchronousServerOperation", String.class, Map.class, String.class));
            parserContext.addMethodImport("fireGlobalEvent", this, this.getClass().getDeclaredMethod("fireGlobalEvent", String.class, Map.class));
            parserContext.addMethodImport("signal", this, this.getClass().getDeclaredMethod("signalModuleEvent", String.class, Map.class));
            parserContext.addMethodImport("showInfo", this, this.getClass().getDeclaredMethod("showInformationMessage", String.class, String.class));
            parserContext.addMethodImport("showError", this, this.getClass().getDeclaredMethod("showErrorMessage", String.class, String.class));
            parserContext.addMethodImport("showException", this, this.getClass().getDeclaredMethod("showExceptionMessage", String.class, String.class, Exception.class));
            parserContext.addMethodImport("showDialog", this, this.getClass().getDeclaredMethod("showDialog", String.class, Map.class, boolean.class));
            parserContext.addMethodImport("getInput", this, this.getClass().getDeclaredMethod("getInput", String.class, String.class));
            parserContext.addMethodImport("getConfirmation", this, this.getClass().getDeclaredMethod("getConfirmation", String.class, String.class));
            parserContext.addMethodImport("setVar", this, this.getClass().getDeclaredMethod("setVariable", String.class, Object.class));
            parserContext.addMethodImport("getVar", this, this.getClass().getDeclaredMethod("getVariable", String.class));
            parserContext.addMethodImport("login", this, this.getClass().getDeclaredMethod("login", String.class, char[].class, String.class, boolean.class));
            parserContext.addMethodImport("logout", this, this.getClass().getDeclaredMethod("logout", new Class<?>[0]));
            parserContext.addMethodImport("requestPasswordChange", this, this.getClass().getDeclaredMethod("requestPasswordChange", String.class));
            parserContext.addMethodImport("isPasswordChangeAvailable", this, this.getClass().getDeclaredMethod("isPasswordChangeAvailable", String.class));
            parserContext.addMethodImport("isUsernameAvailable", this, this.getClass().getDeclaredMethod("isUsernameAvailable", String.class));
            parserContext.addMethodImport("changePassword", this, this.getClass().getDeclaredMethod("changePassword", String.class, char[].class));
            parserContext.addMethodImport("startFlow", this, this.getClass().getDeclaredMethod("startFlow", String.class, Map.class));
            parserContext.addMethodImport("signalFlow", this, this.getClass().getDeclaredMethod("signalFlow", String.class, Map.class));
            parserContext.addMethodImport("getFlowStatus", this, this.getClass().getDeclaredMethod("getFlowStatus", String.class));
            parserContext.addMethodImport("translate", this, this.getClass().getDeclaredMethod("translate", String.class, Object[].class));
            parserContext.addMethodImport("switchUserProfile", this, this.getClass().getDeclaredMethod("switchUserProfile", String.class));
            parserContext.addMethodImport("exit", this, this.getClass().getDeclaredMethod("exit", new Class<?>[0]));
            parserContext.addMethodImport("lockUI", this, this.getClass().getDeclaredMethod("lockUI", String.class));
            parserContext.addMethodImport("unlockUI", this, this.getClass().getDeclaredMethod("unlockUI", new Class<?>[0]));
            parserContext.addMethodImport("createNewGUID", this, this.getClass().getDeclaredMethod("createNewGUID", new Class<?>[0]));
            parserContext.addMethodImport("getTime", this, this.getClass().getDeclaredMethod("getTime", new Class<?>[0]));
            parserContext.addMethodImport("sendCommunication", this, this.getClass().getDeclaredMethod("sendCommunication", String.class, Map.class));
            parserContext.addMethodImport("sendNotification", this, this.getClass().getDeclaredMethod("sendNotification", String.class, Map.class));
            parserContext.addMethodImport("getAPI", this, this.getClass().getDeclaredMethod("getAPI", String.class));
            parserContext.addMethodImport("toastInfo", this, this.getClass().getDeclaredMethod("toastInfo", String.class, Integer.class));
            parserContext.addMethodImport("toastError", this, this.getClass().getDeclaredMethod("toastError", String.class, Integer.class));
            parserContext.addMethodImport("toastSuccess", this, this.getClass().getDeclaredMethod("toastSuccess", String.class, Integer.class));
            parserContext.addMethodImport("getTimeUnit", this, this.getClass().getDeclaredMethod("getTimeUnit", String.class, Integer.class));
        }
        catch (NoSuchMethodException | SecurityException e)
        {
            LOGGER.log("Exception while importing client-side script methods.", e);
        }
    }

    public Object docPath(Object documentObject, String expression)
    {
        DocPathExpressionEvaluator expressionEvaluator;

        expressionEvaluator = docPathFactory.getEvaluator();
        return expressionEvaluator.evaluateExpression(documentObject, expression);
    }

    public T8FunctionalityAccessHandle accessFunctionality(String functionalityId, Map<String, Object> parameters, String functionalityControllerId) throws Exception
    {
        T8FunctionalityController functionalityController;

        // Try to find a functionality controller (either the one specified, or the parent controller available in this context).
        if (functionalityControllerId != null)
        {
            functionalityController = (T8FunctionalityController)controller.findComponent(functionalityControllerId);
        }
        else functionalityController = controller.getFunctionalityController();

        // Now invoke the functionality.
        if (functionalityController != null)
        {
            return functionalityController.accessFunctionality(functionalityId, parameters);
        }
        else throw new Exception("Cannot invoke functionality from current context because the functionality controller was not found.  Context: " + controller + " Specified Controller: " + functionalityControllerId);
    }

    @EPICMethod(Description = "Show a new information toast message.", Name = "toastInfo")
    public void toastInfo(@EPICParameter(VariableName = "messageText") String messageText, @EPICParameter(VariableName = "duration") Integer duration) throws Exception
    {
        if (duration != null)
        {
            Toast.makeText(clientContext.getParentWindow(), messageText, duration, Toast.Style.NORMAL).display();
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), messageText, Toast.Style.NORMAL).display();
        }
    }

    @EPICMethod(Description = "Show a new error toast message.", Name = "toastError")
    public void toastError(@EPICParameter(VariableName = "messageText") String messageText, @EPICParameter(VariableName = "duration") Integer duration) throws Exception
    {
        if (duration != null)
        {
            Toast.makeText(clientContext.getParentWindow(), messageText, duration, Toast.Style.ERROR).display();
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), messageText, Toast.Style.ERROR).display();
        }
    }

    @EPICMethod(Description = "Show a new success toast message.", Name = "toastSuccess")
    public void toastSuccess(@EPICParameter(VariableName = "messageText") String messageText, @EPICParameter(VariableName = "duration") Integer duration) throws Exception
    {
        if (duration != null)
        {
            Toast.makeText(clientContext.getParentWindow(), messageText, duration, Toast.Style.SUCCESS).display();
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), messageText, Toast.Style.SUCCESS).display();
        }
    }

    @EPICMethod(Description = "Gets the api associated with the identifier", Name = "getAPI")
    public T8Api getAPI(@EPICParameter(VariableName = "apiIdentifier") String apiIdentifier)
    {
        return controller.getApi(apiIdentifier);
    }

    @EPICMethod(Description = "Creates a new GUID", Name = "createNewGUID")
    public String createNewGUID()
    {
        return T8IdentifierUtilities.createNewGUID();
    }

    @EPICMethod(Description = "Returns the current System time as a long value.", Name = "getTime")
    public long getTime()
    {
        return System.currentTimeMillis();
    }

    public T8TimeUnit getTimeUnit(String unitType, Integer number)
    {
        switch (unitType)
        {
            case "MILLISECOND":
                return new T8Millisecond(number);
            case "SECOND":
                return new T8Second(number);
            case "MINUTE":
                return new T8Minute(number);
            case "HOUR":
                return new T8Hour(number);
            case "DAY":
                return new T8Day(number);
            case "YEAR":
                return new T8Year(number);
            default:
                throw new IllegalArgumentException("Unsupported T8TimeUnit type: " + unitType);
        }
    }

    @EPICMethod(Description = "Locks the User Interface", Name = "lockUI")
    public void lockUI(String message)
    {
        controller.lockUi(message);
    }

    @EPICMethod(Description = "Un-Locks the User Interface", Name = "unlockUI")
    public void unlockUI()
    {
        controller.unlockUi();
    }

    @EPICMethod(Description = "Translates the given input String to the session langauge", Name = "translate")
    public String translate(String inputString, Object... args)
    {
        return String.format(clientContext.getConfigurationManager().getUITranslation(context, inputString), args);
    }

    public boolean switchUserProfile(String userProfileIdentifier) throws Exception
    {
        return clientContext.getSecurityManager().switchUserProfile(context, userProfileIdentifier);
    }

    @EPICMethod(Description = "Gets the flow count for the specified flow.", Name = "getFlowCount")
    public int getFlowCount()
    {
        return clientContext.getFlowManager().getFlowCount(context);
    }

    @EPICMethod(Description = "Returns the status of the specified flow.", Name = "getFlowStatus")
    public T8FlowStatus getFlowStatus(@EPICParameter(VariableName = "flowInstanceIdentifier") String flowIid) throws Exception
    {
        return clientContext.getFlowManager().getFlowStatus(context, flowIid, true);
    }

    @EPICMethod(Description = "Gets the specified system property.", Name = "getSystemProperty")
    public Object getSystemProperty(String propertyIdentifier)
    {
        return clientContext.getConfigurationManager().getProperty(context, propertyIdentifier);
    }

    @EPICMethod(Description = "Executes the specified server operation synchronously.", Name = "execServerOp")
    public Map<String, Object> executeSynchronousServerOperation(@EPICParameter(VariableName = "serverOperationIdentifier") String operationId, @EPICParameter(VariableName = "paramMap") Map<String, Object> operationParameters, @EPICParameter(VariableName = "clientMessage") String message) throws Exception
    {
        try
        {
            return controller.executeSynchronousServerOperation(operationId, operationParameters, message);
        }
        catch(Exception e)
        {
            if (ExceptionUtilities.getRootCause(e) instanceof T8RaisedException)
            {
                T8ExceptionDialog.showExceptionDialog(context, translate("Operation Failed"), translate("Failed to complete server operation"), e);
            }

            throw e;
        }
    }

    @EPICMethod(Description = "Executes the specified server operation asynchronously.", Name = "execBackgroundServerOp")
    public Map<String, Object> executeAsynchronousServerOperation(@EPICParameter(VariableName = "serverOperationIdentifier") String operationId, @EPICParameter(VariableName = "paramMap") Map<String, Object> operationParameters, @EPICParameter(VariableName = "clientMessage") String message) throws Exception
    {
        return controller.executeAsynchronousServerOperation(operationId, operationParameters, message);
    }

    @EPICMethod(Description = "Executes the specified client operation synchronously.", Name = "execClientOp")
    public Map<String, Object> executeClientOperation(@EPICParameter(VariableName = "componentOperationIdentifier") String operationId, @EPICParameter(VariableName = "paramMap") Map<String, Object> operationParameters) throws Exception
    {
        Map<String, Object> inputParameters;
        Map<String, Object> outputParameters;

        inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
        outputParameters = controller.executeClientOperation(operationId, inputParameters);
        return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
    }

    public T8ServerOperationStatusReport executeAsynchronousClientOperation(String operationIdentifier, Map<String, Object> operationParameters) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @EPICMethod(Description = "Executes the specified server process asynchronously.", Name = "execBackgroundServerProc")
    public T8ProcessDetails executeAsynchronousServerProcess(@EPICParameter(VariableName = "serverProcessIdentifier") String processIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> processParameters) throws Exception
    {
        T8ProcessDetails processDetails;

        // Start execution of the new process.
        processDetails = clientContext.getProcessManager().startProcess(context, processIdentifier, processParameters);

        // Refresh notifications.
        clientContext.refreshNotifications();

        // Return the new process' details.
        return processDetails;
    }

    @EPICMethod(Description = "Executes an operation on the specified component.", Name = "do")
    public Map<String, Object> executeComponentOperation(@EPICParameter(VariableName = "componentIdentifier") String componentIdentifier, @EPICParameter(VariableName = "componentOperationIdentifier") String operationIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> operationParameters) throws Exception
    {
        T8Component targetComponent;

        targetComponent = controller.findComponent(componentIdentifier);
        if (targetComponent != null)
        {
            return targetComponent.executeOperation(operationIdentifier, operationParameters != null ? operationParameters : new HashMap<>());
        }
        else throw new Exception("Component '" + componentIdentifier + "' not found within context of controller: " + controller.getId());
    }

    @EPICMethod(Description = "Executes an operation on the specified component, and then returns the specified parameter.", Name = "doGet")
    public Object executeComponentGetOperation(@EPICParameter(VariableName = "componentIdentifier") String componentIdentifier, @EPICParameter(VariableName = "componentOperationIdentifier") String operationIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> operationParameters, @EPICParameter(VariableName = "componentOperationOutputParameterIdentifier") String parameterIdentifier) throws Exception
    {
        Map<String, Object> outputParameters;

        outputParameters = executeComponentOperation(componentIdentifier, operationIdentifier, operationParameters != null ? operationParameters : new HashMap<>());
        return outputParameters != null ? outputParameters.get(parameterIdentifier) : null;
    }

    @EPICMethod(Description = "Executes the specified Module Script.", Name = "execModuleScript")
    public Object executeModuleScript(@EPICParameter(VariableName = "scriptIdentifier") String scriptIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> inputParameters) throws Exception
    {
        if (this.controller instanceof T8ModuleComponentController)
        {
            return this.controller.executeScript(scriptIdentifier, inputParameters);
        }
        else throw new Exception("Command 'execModuleScript' can only be executed from within a module.");
    }

    @EPICMethod(Description = "Executes the specified Controller Script.", Name = "execControllerScript")
    public Object executeControllerScript(@EPICParameter(VariableName = "scriptIdentifier") String scriptIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> inputParameters) throws Exception
    {
        return controller.executeScript(scriptIdentifier, inputParameters);
    }

    @EPICMethod(Description = "Signals a new module event with the specified event parameters.", Name = "signal")
    public void signalModuleEvent(@EPICParameter(VariableName = "eventIdentifier") String eventIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> eventParameters) throws Exception
    {
        if (controller instanceof T8ModuleComponentController)
        {
            ((T8ModuleComponentController)controller).getModule().signalModuleEvent(eventIdentifier, eventParameters);
        }
        else throw new Exception("Module event '" + eventIdentifier + "' signalled outside of any module.  Controller: " + controller.getId());
    }

    @EPICMethod(Description = "Show a new information dialog to the client.", Name = "showInfo")
    public void showInformationMessage(@EPICParameter(VariableName = "messageHeader") String messageHeader, @EPICParameter(VariableName = "messageContent") String messageContent) throws Exception
    {
        JOptionPane.showMessageDialog(controller.getParentWindow(), messageContent, messageHeader, JOptionPane.INFORMATION_MESSAGE);
    }

    @EPICMethod(Description = "Show a new error dialog to the client.", Name = "showError")
    public void showErrorMessage(@EPICParameter(VariableName = "messageHeader") String messageHeader, @EPICParameter(VariableName = "messageContent") String messageContent) throws Exception
    {
        JOptionPane.showMessageDialog(controller.getParentWindow(), messageContent, messageHeader, JOptionPane.ERROR_MESSAGE);
    }

    @EPICMethod(Description = "Show a new exception dialog to the client.", Name = "showException")
    public void showExceptionMessage(@EPICParameter(VariableName = "messageHeader") String messageHeader, @EPICParameter(VariableName = "messageContent") String messageContent, @EPICParameter(VariableName = "exception") Exception ex) throws Exception
    {
        T8ExceptionDialog.showExceptionDialog(context, messageHeader, messageContent, ex);
    }

    @EPICMethod(Description = "Shows a new dialog an returns dialog output once the dialog is closed.", Name = "showDialog")
    public Map<String, Object> showDialog(@EPICParameter(VariableName = "dialogIdentifier") String dialogId, @EPICParameter(VariableName = "inputParameters") Map<String, Object> inputParameters, @EPICParameter(VariableName = "keepAlive") boolean keepAlive) throws Exception
    {
        if (dialogHandler != null)
        {
            return dialogHandler.showDialog(dialogId, inputParameters, keepAlive);
        }
        else throw new RuntimeException("Global Dialogs cannot be used in this context.");
    }

    @EPICMethod(Description = "Creates a new Input dialog and returns the user entered input when the dialog closes.", Name = "getInput")
    public String getInput(@EPICParameter(VariableName = "messageHeader") String messageHeader, @EPICParameter(VariableName = "messageContent") String messageContent) throws Exception
    {
        return JOptionPane.showInputDialog(controller.getParentWindow(), messageContent, messageHeader, JOptionPane.PLAIN_MESSAGE);
    }

    @EPICMethod(Description = "Creates a new Confirmation dialog and returns the user selection when the dialog closes.", Name = "getConfirmation")
    public boolean getConfirmation(@EPICParameter(VariableName = "messageHeader") String messageHeader, @EPICParameter(VariableName = "messageContent") String messageContent) throws Exception
    {
        int option;

        option = JOptionPane.showConfirmDialog(null, messageContent, messageHeader, JOptionPane.YES_NO_OPTION);
        return option == JOptionPane.YES_OPTION;
    }

    @EPICMethod(Description = "Sets a module level variable. If this variable does not exist it will be created, if it does exists the previous value will be replaced.", Name = "setVar")
    public void setVariable(@EPICParameter(VariableName = "name") String name, @EPICParameter(VariableName = "value") Object value)
    {
        controller.setVariable(name, value);
    }

    @EPICMethod(Description = "Gets a module level variable. If it does not exists, null will be returned.", Name = "getVar")
    public Object getVariable(@EPICParameter(VariableName = "name") String name)
    {
        return controller.getVariable(name);
    }

    public T8UserLoginResponse login(String userIdentifier, char[] password, String loginLocation, boolean endExistingSession) throws Exception
    {
        T8UserLoginResponse loginResponse;

        loginResponse = clientContext.getSecurityManager().login(this.context, T8SecurityManager.LoginLocation.valueOf(loginLocation), userIdentifier, password, endExistingSession);
        return loginResponse;
    }

    public boolean logout() throws Exception
    {
        return clientContext.getSecurityManager().logout(context);
    }

    public boolean isPasswordChangeAvailable(String userIdentifier) throws Exception
    {
        return clientContext.getSecurityManager().isPasswordChangeAvailable(context, userIdentifier);
    }

    public boolean isUsernameAvailable(String username) throws Exception
    {
        return clientContext.getSecurityManager().isUsernameAvailable(context, username);
    }

    public boolean requestPasswordChange(String userIdentifier) throws Exception
    {
        return clientContext.getSecurityManager().requestPasswordChange(context, userIdentifier);
    }

    public T8PasswordChangeResponse changePassword(String userIdentifier, char[] password) throws Exception
    {
        return clientContext.getSecurityManager().changePassword(context, userIdentifier, password);
    }

    @EPICMethod(Description = "Starts the specified flow with the provided input paramters.", Name = "startFlow")
    public void startFlow(@EPICParameter(VariableName = "flowIdentifier") String flowId, @EPICParameter(VariableName = "paramMap") Map<String, Object> inputParameters) throws Exception
    {
        clientContext.getFlowManager().startFlow(context, flowId, inputParameters);
    }

    @EPICMethod(Description = "Signals a flow using the signal identifier and parameters.", Name = "signalFlow")
    public void signalFlow(@EPICParameter(VariableName = "signalIdentifier") String signalIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> signalParameters) throws Exception
    {
        clientContext.getFlowManager().fireSignalEvent(context, signalIdentifier, signalParameters);
    }

    @EPICMethod(Description = "Fires a global UI event.", Name = "fireGlobalEvent")
    public void fireGlobalEvent(@EPICParameter(VariableName = "eventIdentifier") String eventIdentifier, @EPICParameter(VariableName = "paramMap") Map<String, Object> eventParameters) throws Exception
    {
        controller.fireGlobalEvent(eventIdentifier, eventParameters);
    }

    public void exit()
    {
        try
        {
            // Try to log out first.
            logout();
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while trying to log user out during exit procedure.", e);
        }

        // Exit the system.
        System.exit(0);
    }

    @EPICMethod(Description = "Generates and Queues a new message to be sent.", Name = "sendCommunication")
    public void sendCommunication(@EPICParameter(VariableName = "communicationIdentifier") String communicationID, @EPICParameter(VariableName = "paramMap") Map<String, Object> communicationParameters) throws Exception
    {
        clientContext.getCommunicationManager().sendCommunication(context, communicationID, communicationParameters);
    }

    @EPICMethod(Description = "Generates and requests a new notification to be sent.", Name = "sendNotification")
    public void sendNotification(@EPICParameter(VariableName = "notificationIdentifier") String notificationIdentifier, @EPICParameter(VariableName = "notificationParameters") Map<String, Object> notificationParameters) throws Exception
    {
        this.clientContext.getNotificationManager().sendNotification(this.context, notificationIdentifier, notificationParameters);
    }
}
