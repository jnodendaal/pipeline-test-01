package com.pilog.t8.client;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.dialog.T8GlobalDialog;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DialogHandler
{
    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final Map<String, T8GlobalDialog> dialogCache;

    public T8DialogHandler(T8ComponentController controller)
    {
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.dialogCache = new HashMap<>();
    }

    public void stopDialogs()
    {
        for (T8GlobalDialog dialog : dialogCache.values())
        {
            dialog.stopComponent();
        }
    }

    private T8GlobalDialog loadDialog(String dialogId, Map<String, Object> inputParameters, boolean keepAlive)
    {
        T8GlobalDialog dialog;

        // Try to get the dialog from cache (if applicable).
        if (keepAlive)
        {
            dialog = dialogCache.get(dialogId);
            if (dialog != null) return dialog;
        }

        // Load the dialog.
        try
        {
            T8GlobalDialogDefinition dialogDefinition;

            // Load the dialog definition to use.
            dialogDefinition = (T8GlobalDialogDefinition)clientContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), dialogId, inputParameters);

            // Display the dialog.
            if (dialogDefinition != null)
            {
                // Initialize the dialog.
                dialog = (T8GlobalDialog)dialogDefinition.getNewComponentInstance(controller);
                dialog.initializeComponent(inputParameters);

                // Start the dialog.
                dialog.startComponent();

                // Cache the dialog if applicable.
                if (keepAlive)
                {
                    dialogCache.put(dialogId, dialog);
                }

                // Return the dialog.
                return dialog;
            }
            else throw new RuntimeException("Dialog definition not found: " + dialogId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while attempting to show global dialog '" + dialogId + "' using parameters: " + inputParameters, e);
        }
    }

    public Map<String, Object> showDialog(String dialogId, Map<String, Object> inputParameters, boolean keepAlive)
    {
        try
        {
            Map<String, Object> outputParameters;
            T8GlobalDialog dialog;

            // Initialize the dialog.
            dialog = loadDialog(dialogId, inputParameters, keepAlive);
            dialog.setInputParameters(inputParameters); // We have to set the input parameters, because the dialog might have been loaded from cached state.

            // Show the dialog.
            dialog.openDialog();

            // Get the selected entities (this point will only be reached once the dialog is disposed).
            outputParameters = dialog.getOutputParameters();
            T8Log.log("Dialog output parameters: " + outputParameters);

            // Stop the dialog.
            if (!keepAlive) dialog.stopComponent();

            // Return the dialog's output parameters.
            return outputParameters;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while attempting to show global dialog '" + dialogId + "' using parameters: " + inputParameters, e);
        }
    }
}
