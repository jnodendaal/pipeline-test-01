/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.client;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * @author Bouwer du Preez
 */
public class T8ClientUtilities
{
    /**
     * This method uses the available cell renderer to determine the maximum
     * width for the values in each of the columns in the supplied table and
     * then to resize the columns accordingly so that all values are displayed
     * in full.
     * 
     * @param table The table for which columns will be resized.
     * @param minimumWidth  The minimum width to which columns will be resized.
     */
    public static void autoSizeTableColumns(JTable table, int minimumWidth)
    {
        for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++)
        {
            autoSizeTableColumn(table, columnIndex, minimumWidth);
        }
    }
    
    /**
     * This method uses the available cell renderer to determine the maximum
     * width for the values in each of the columns in the supplied table and
     * then to resize the columns accordingly so that all values are displayed
     * in full.
     * 
     * @param table The table for which columns will be resized.
     * @param columnIndex The index of the column to auto-size (column model).
     * @param minimumWidth  The minimum width to which columns will be resized.
     */
    public static void autoSizeTableColumn(JTable table, int columnIndex, int minimumWidth)
    {
        TableCellRenderer renderer;
        TableColumn column;
        int maxWidth;

        column = table.getColumnModel().getColumn(columnIndex);
        renderer = column.getCellRenderer();
        if (renderer == null) renderer = table.getDefaultRenderer(String.class);
        maxWidth = minimumWidth;
        for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++)
        {
            Component rendererComponent;
            Object value;
            int width;

            value = table.getValueAt(rowIndex, columnIndex);
            rendererComponent = renderer.getTableCellRendererComponent(table, value, false, false, rowIndex, columnIndex);
            width = rendererComponent.getPreferredSize().width + 5;
            if (width > maxWidth) maxWidth = width;
        }

        column.setPreferredWidth(maxWidth);
    }
}
