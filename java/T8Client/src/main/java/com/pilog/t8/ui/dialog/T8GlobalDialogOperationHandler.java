package com.pilog.t8.ui.dialog;

import com.pilog.t8.definition.ui.dialog.T8GlobalDialogAPIHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8GlobalDialogOperationHandler extends T8DialogOperationHandler
{
    private final T8GlobalDialog dialog;

    public T8GlobalDialogOperationHandler(T8GlobalDialog dialog)
    {
        super(dialog);
        this.dialog = dialog;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8GlobalDialogAPIHandler.OPERATION_CLOSE_DIALOG))
        {
            Map<String, Object> outputParameters;

            // Get the output parameters from the operation parameters.
            outputParameters = (Map<String, Object>)operationParameters.get(T8GlobalDialogAPIHandler.PARAMETER_OUTPUT_PARAMETERS);
            
            // Set the output parameters of the dialog and then close it.
            dialog.setOutputParameters(outputParameters);
            dialog.closeDialog();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
