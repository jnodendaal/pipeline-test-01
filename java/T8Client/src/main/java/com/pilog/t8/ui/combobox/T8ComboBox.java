package com.pilog.t8.ui.combobox;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.combobox.T8ComboBoxAPIHandler;
import com.pilog.t8.definition.ui.combobox.T8ComboBoxDefinition;
import com.pilog.t8.definition.ui.combobox.T8ComboBoxItemDefinition;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 * @author Bouwer du Preez
 */
public class T8ComboBox extends JComboBox implements T8Component
{
    private static final T8Logger logger = T8Log.getLogger(T8ComboBox.class);

    private final T8ComboBoxDefinition definition;
    private final T8ComponentController controller;
    private final T8ComboBoxOperationHandler operationHandler;
    private final T8SessionContext sessionContext;

    public T8ComboBox(T8ComboBoxDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.sessionContext = controller.getSessionContext();
        this.operationHandler = new T8ComboBoxOperationHandler(this);
        this.setRenderer(new T8ComboBoxListCellRenderer(definition));
        this.addItemListener(new SelectionChangeListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        DefaultComboBoxModel model;
        ExpressionEvaluator evaluator;

        // Create an expression evaluator for evaluating item value expressions.
        evaluator = new ExpressionEvaluator();

        // Create a new model.
        model = new DefaultComboBoxModel();

        // Add all the combobox items to the new model and add data values to the value list.
        for (T8ComboBoxItemDefinition itemDefinition : definition.getItemDefinitions())
        {
            try
            {
                T8ComboBoxElement item;
                Object dataValue;

                dataValue = evaluator.evaluateExpression(itemDefinition.getValueExpression(), null, null);
                item = new T8ComboBoxElement(itemDefinition.getIdentifier(), itemDefinition.getItemName(), dataValue);
                model.addElement(item);
            }
            catch (Exception e)
            {
                logger.log("Exception while evaluating value expression for combobox item: " + itemDefinition, e);
            }
        }

        // Set the model.
        setModel(model);
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void addElement(String elementIdentifier, String displayName, Object dataValue)
    {
        DefaultComboBoxModel model;

        model = (DefaultComboBoxModel)this.getModel();
        model.addElement(new T8ComboBoxElement(elementIdentifier, displayName, dataValue));
    }

    public void removeElement(String elementIdentifier)
    {
        DefaultComboBoxModel model;
        T8ComboBoxElement elementToRemove;

        elementToRemove = null;
        model = (DefaultComboBoxModel)this.getModel();
        for (int index = 0; index < model.getSize(); index++)
        {
            T8ComboBoxElement element;

            element = (T8ComboBoxElement)model.getElementAt(index);
            if ((element != null) && (element.getIdentifier().equals(elementIdentifier)))
            {
                elementToRemove = element;
                break;
            }
        }

        if (elementToRemove != null) model.removeElement(elementToRemove);
    }

    public Object getSelectedDataValue()
    {
        T8ComboBoxElement selectedElement;

        selectedElement = (T8ComboBoxElement)getSelectedItem();
        return selectedElement != null ? selectedElement.getDataValue() : null;
    }

    public void setSelectedDataValue(Object dataValue)
    {
        ComboBoxModel model;

        model = this.getModel();
        for (int itemIndex = 0; itemIndex < model.getSize(); itemIndex++)
        {
            T8ComboBoxElement element;

            element = (T8ComboBoxElement)model.getElementAt(itemIndex);
            if (element == null)
            {
                if (dataValue == null)
                {
                    setSelectedIndex(itemIndex);
                    return;
                }
            }
            else if (Objects.equals(element.getDataValue(), (dataValue)))
            {
                this.setSelectedItem(element);
                return;
            }
        }
    }

    private class SelectionChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<String, Object>();
                eventParameters.put(T8ComboBoxAPIHandler.PARAMETER_DATA_VALUE, getSelectedDataValue());
                controller.reportEvent(T8ComboBox.this, definition.getComponentEventDefinition(T8ComboBoxAPIHandler.EVENT_SELECTION_CHANGE), eventParameters);
            }
        }
    }
}
