package com.pilog.t8.flow.work.task.user;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8FlowTaskStatus;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskModuleDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskModuleEventDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskUIDefinition;
import com.pilog.t8.definition.script.T8ClientContextScriptDefinition;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowModuleTask implements T8FlowTask
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8WorkFlowModuleTask.class);

    private final T8WorkFlowUserTaskDefinition definition;
    private T8WorkFlowUserTaskModuleDefinition moduleUiDefinition;
    private T8WorkFlowUserTaskModuleEventDefinition finalizationEventDefinition;
    private T8ComponentEvent finalizationEvent;
    private final T8TaskDetails taskDetails;
    private final TaskModuleEventListener taskModuleEventListener;
    private TaskStatus status;
    private String statusMessage;
    private boolean stop;

    public T8WorkFlowModuleTask(T8WorkFlowUserTaskDefinition definition, T8TaskDetails taskDetails)
    {
        this.definition = definition;
        this.finalizationEvent = null;
        this.taskDetails = taskDetails;
        this.status = TaskStatus.NOT_STARTED;
        this.taskModuleEventListener = new TaskModuleEventListener();
        this.stop = false;
    }

    @Override
    public T8FlowTaskDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8TaskDetails getTaskDetails()
    {
        return taskDetails;
    }

    @Override
    public T8FlowTaskStatus getStatus()
    {
        return new T8FlowTaskStatus(taskDetails.getTaskId(), taskDetails.getTaskIid(), status, -1, statusMessage);
    }

    @Override
    public Map<String, Object> doTask(T8ComponentController controller, Map<String, Object> inputParameters) throws Exception
    {
        try
        {
            T8Context context;

            // Get the client context.
            context = controller.getContext();

            // Load the module.
            synchronized (this)
            {
                // Set the status to indicate that the task is active.
                status = TaskStatus.ACTIVE;

                // Extract the required definition information.
                moduleUiDefinition = null;
                for (T8WorkFlowUserTaskUIDefinition uiDefinition : definition.getUserInterfaceDefinitions())
                {
                    if (uiDefinition instanceof T8WorkFlowUserTaskModuleDefinition)
                    {
                        moduleUiDefinition = (T8WorkFlowUserTaskModuleDefinition)uiDefinition;
                        break;
                    }
                }

                // If we found an applicable UI definition, use it.
                if (moduleUiDefinition != null)
                {
                    Map<String, Object> moduleInputParameters;
                    String moduleId;

                    LOGGER.log("Module Task '" + definition.getIdentifier() + "' starts with parameters: " + inputParameters);

                    // Load the required module, passing the task input parameters to the module so they can be distributed to the UI components.
                    moduleId = moduleUiDefinition.getModuleIdentifier();
                    moduleInputParameters = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, true);
                    moduleInputParameters = T8IdentifierUtilities.mapParameters(moduleInputParameters, moduleUiDefinition.getModuleInputParameterMapping());
                    moduleInputParameters.putAll(getAdditionalModuleInputParameters(context, inputParameters));
                    controller.loadComponent(null, definition.getRootProjectId(), moduleId, moduleInputParameters);
                    controller.addComponentEventListener(taskModuleEventListener);
                }
                else throw new Exception("No Module UI Definition found for task: " + definition);
            }

            // Wait until one of the finalization events occur.
            statusMessage = "Waiting for module finalization event.";
            finalizationEvent = waitForFinalizationEvent();

            // Only continue execution if this task has not already been stopped while waiting for the finalization event.
            synchronized (this)
            {
                if (!stop)
                {
                    Map<String, Object> finalizationEventParameters;
                    Map<String, Object> taskOutputParameters;

                    // Get the finalization event parameters.
                    finalizationEventParameters = finalizationEvent.getEventParameters();
                    if (finalizationEventParameters == null) finalizationEventParameters = new HashMap<String, Object>();

                    // Execute the finalization script and get the result.
                    taskOutputParameters = T8IdentifierUtilities.mapParameters(finalizationEventParameters, finalizationEventDefinition.getModuleOutputParameterMapping());
                    taskOutputParameters.put("$P_EVENT_IDENTIFIER", finalizationEvent.getEventDefinition().getIdentifier());
                    taskOutputParameters.putAll(getAdditionalOutputParameters(context, finalizationEventDefinition, finalizationEventParameters));

                    // If an action value parameter is specified, use it.
                    if (finalizationEventDefinition.getActionParameterValueIdentifier() != null)
                    {
                        taskOutputParameters.put(finalizationEventDefinition.getActionParameterIdentifier(), finalizationEventDefinition.getActionParameterValueIdentifier());
                    }

                    // Set the status to indicate the completion of the task.
                    status = TaskStatus.COMPLETED;

                    // Return the final results.
                    return T8IdentifierUtilities.prependNamespace(definition.getIdentifier(), taskOutputParameters);
                }
                else
                {
                    // Set the status to indicate that the task was stopped.
                    status = TaskStatus.STOPPED;
                    return null;
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception during module task execution.", e);
            throw e;
        }
        finally
        {
            controller.removeComponentEventListener(taskModuleEventListener);
        }
    }

    @Override
    public Map<String, Object> doTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters)
    {
        throw new RuntimeException("Task cannot be executed on server-side.");
    }

    @Override
    public Map<String, Object> resumeTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception
    {
        throw new RuntimeException("Task cannot be executed on server-side.");
    }

    @Override
    public synchronized void stopTask()
    {
        // Set the status to indicate that the task is stopping.
        status = TaskStatus.STOPPING;
        stop = true;

        // Notify any threads that are waiting.
        T8WorkFlowModuleTask.this.notifyAll();
    }

    @Override
    public void cancelTask()
    {
        // Nothing to do here, since this task is executed on the client-side and wont received the server's cancellation request.
    }

    private Map<String, Object> getAdditionalModuleInputParameters(T8Context context, Map<String, Object> inputParameters) throws Exception
    {
        T8ClientContextScriptDefinition scriptDefinition;
        Map<String, Object> additionalParameters;

        additionalParameters = new HashMap<String, Object>();
        scriptDefinition = moduleUiDefinition.getModuleInputParameterScriptDefinition();
        if ((scriptDefinition != null) && (scriptDefinition.containsScript()))
        {
            T8Script script;
            Map<String, Object> outputParameters;

            script = scriptDefinition.getNewScriptInstance(context);
            outputParameters = script.executeScript(T8IdentifierUtilities.stripNamespace(inputParameters));
            if (outputParameters != null)
            {
                additionalParameters.putAll(outputParameters);
            }
        }

        return additionalParameters;
    }

    private Map<String, Object> getAdditionalOutputParameters(T8Context context, T8WorkFlowUserTaskModuleEventDefinition finalizationEventDefinition, Map<String, Object> finalizationEventParameters) throws Exception
    {
        T8ClientContextScriptDefinition scriptDefinition;
        Map<String, Object> additionalParameters;

        additionalParameters = new HashMap<String, Object>();
        scriptDefinition = finalizationEventDefinition.getTaskOutputParameterScriptDefinition();
        if ((scriptDefinition != null) && (scriptDefinition.containsScript()))
        {
            T8Script script;
            Map<String, Object> outputParameters;

            script = scriptDefinition.getNewScriptInstance(context);
            outputParameters = script.executeScript(T8IdentifierUtilities.stripNamespace(finalizationEventParameters));
            if (outputParameters != null)
            {
                additionalParameters.putAll(outputParameters);
            }
        }

        return additionalParameters;
    }

    private synchronized T8ComponentEvent waitForFinalizationEvent() throws Exception
    {
        // Wait until the task is claimed.
        statusMessage = "Waiting for finalization event.";
        while ((!stop) && (finalizationEvent == null))
        {
            wait(); // The task has not been claimed yet so wait some more and then try again.
        }
        statusMessage = "Stopped waiting for finalization event.";

        return finalizationEvent;
    }

    private class TaskModuleEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            synchronized (T8WorkFlowModuleTask.this)
            {
                for (T8WorkFlowUserTaskModuleEventDefinition eventDefinition : moduleUiDefinition.getFinalizationEventDefinitions())
                {
                    if (event.getEventDefinition().getPublicIdentifier().equals(eventDefinition.getModuleEventIdentifier()))
                    {
                        finalizationEvent = event;
                        finalizationEventDefinition = eventDefinition;
                        T8WorkFlowModuleTask.this.notifyAll();
                    }
                }
            }
        }
    }
}
