package com.pilog.t8.ui.hyperlink;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.ui.hyperlink.T8HyperlinkAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8HyperlinkOperationHandler extends T8ComponentOperationHandler
{
    private static final T8Logger logger = T8Log.getLogger(T8HyperlinkOperationHandler.class);

    private final T8Hyperlink hyperlink;

    public T8HyperlinkOperationHandler(T8Hyperlink button)
    {
        super(button);
        this.hyperlink = button;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8HyperlinkAPIHandler.OPERATION_ENABLE_LINK))
        {
            hyperlink.setEnabled(true);
            return null;
        }
        else if(operationIdentifier.equals(T8HyperlinkAPIHandler.OPERATION_DISABLE_LINK))
        {
            hyperlink.setEnabled(false);
            return null;
        }
        else if(operationIdentifier.equals(T8HyperlinkAPIHandler.OPERATION_SET_TEXT))
        {
            hyperlink.setText((String)operationParameters.get(T8HyperlinkAPIHandler.PARAMETER_TEXT));
            return null;
        }
        else if(operationIdentifier.equals(T8HyperlinkAPIHandler.OPERATION_GET_TEXT))
        {
            return HashMaps.createSingular(T8HyperlinkAPIHandler.PARAMETER_TEXT, this.hyperlink.getText());
        }
        else if(operationIdentifier.equals(T8HyperlinkAPIHandler.OPERATION_SET_URI))
        {
            try
            {
                hyperlink.setURI((String)operationParameters.get(T8HyperlinkAPIHandler.PARAMETER_URI));
                return null;
            }
            catch (Exception e)
            {
                logger.log("Exception while executing hyperlink operation.", e);
                return null;
            }
        }
        else if(operationIdentifier.equals(T8HyperlinkAPIHandler.OPERATION_SET_LINK))
        {
            try
            {
                hyperlink.setURI((String)operationParameters.get(T8HyperlinkAPIHandler.PARAMETER_URI));
                hyperlink.setText((String)operationParameters.get(T8HyperlinkAPIHandler.PARAMETER_TEXT));
                return null;
            }
            catch (Exception e)
            {
                logger.log("Exception while executing hyperlink operation.", e);
                return null;
            }
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
