package com.pilog.t8.gfx.painter.effect.area;

import com.pilog.t8.ui.T8PainterEffect;
import com.pilog.t8.definition.gfx.painter.effect.area.T8AreaPainterEffectDefinition;
import org.jdesktop.swingx.painter.effects.AbstractAreaEffect;

/**
 * @author Bouwer du Preez
 */
public class T8AreaPainterEffect extends AbstractAreaEffect implements T8PainterEffect
{
    private T8AreaPainterEffectDefinition definition;
    
    public T8AreaPainterEffect(T8AreaPainterEffectDefinition definition)
    {
        super();
        this.definition = definition;
        setupPainter();
    }
    
    private void setupPainter()
    {
        this.setBrushColor(definition.getBrushColor());
        this.setBrushSteps(definition.getBrushSteps());
        this.setEffectWidth(definition.getEffectWidth());
        this.setRenderInsideShape(definition.getRenderInsideShape());
        this.setShouldFillShape(definition.getFillInsideShape());
        this.setShapeMasked(definition.getShapeMasked());
    }
}
