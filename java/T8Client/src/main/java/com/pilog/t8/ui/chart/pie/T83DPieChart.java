package com.pilog.t8.ui.chart.pie;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.chart.pie.T83DPieChartDefinition;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.chart.util.Rotation;
import org.jfree.chart.util.SortOrder;

/**
 * @author Bouwer du Preez
 */
public class T83DPieChart extends ChartPanel implements T8Component
{
    private final T83DPieChartDefinition definition;
    private final T8ComponentController controller;
    private final T83DPieChartOperationHandler operationHandler;
    private JFreeChart chart;

    public T83DPieChart(T83DPieChartDefinition definition, T8ComponentController controller)
    {
        super(null);
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T83DPieChartOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void setDataMap(Map<String, Number> dataMap)
    {
        DefaultPieDataset dataSet;

        T8Log.log("Setting Pie Data Map: " + dataMap);
        dataSet = new DefaultPieDataset();
        for (String key : dataMap.keySet())
        {
            dataSet.setValue(key, dataMap.get(key));
        }

        dataSet.sortByKeys(SortOrder.ASCENDING);
        setDataSet(dataSet);
    }

    public void setDataSet(PieDataset dataset)
    {
        PiePlot3D plot;
        List<T8PaintDefinition> chartSeriesPaintDefinitions;

        // Create the new chart.
        chart = ChartFactory.createPieChart3D(definition.getTitle(), // chart title
                dataset, // data
                definition.isLegendVisible(), // include legend
                true,
                false);

        // Make the chart background translucent.
        chart.setBackgroundImageAlpha(0.0f);
        chart.setBackgroundPaint(new Color(255, 255, 255, 0));

        // Set the plot configuration.
        plot = (PiePlot3D)chart.getPlot();
        plot.setOutlineVisible(definition.isOutlineVisible());
        plot.setStartAngle(290);
        plot.setBackgroundAlpha(0.0f);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        plot.setDepthFactor(definition.getDepthFactor());

        chartSeriesPaintDefinitions = definition.getChartSeriesPaintDefinitions();
        if(chartSeriesPaintDefinitions != null && !chartSeriesPaintDefinitions.isEmpty())
        {
            int index = 0;
            for (T8PaintDefinition t8PaintDefinition : chartSeriesPaintDefinitions)
            {
                plot.setSectionPaint(index++, t8PaintDefinition.getNewPaintInstance());
            }
        }

        setChart(chart);
    }
}
