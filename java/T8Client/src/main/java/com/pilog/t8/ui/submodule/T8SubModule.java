package com.pilog.t8.ui.submodule;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.ui.submodule.T8SubModuleAPIHandler;
import com.pilog.t8.definition.ui.submodule.T8SubModuleDefinition;
import com.pilog.t8.ui.module.T8Module;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SubModule extends javax.swing.JComponent implements T8Component
{
    private static final T8Logger logger = T8Log.getLogger(T8SubModule.class);
    private final T8SubModuleDefinition definition;
    private final T8ComponentController controller;
    private final T8SubModuleOperationHandler operationHandler;
    private T8Module module;
    private Map<String, Object> inputParameters;

    public T8SubModule(T8SubModuleDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8SubModuleOperationHandler(this);
        this.module = null;
        this.setOpaque(false);
        initComponents();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        // If the operation is intended for execution by the sub-module wrapper then use the local operation handler, else route the invocation to the content sub-module.
        if (operationIdentifier.equals(T8SubModuleAPIHandler.OPERATION_RELOAD_MODULE)) // TODO:  Implement a better way to identify wrapper operations.
        {
            return operationHandler.executeOperation(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8SubModuleAPIHandler.OPERATION_UNLOAD_MODULE)) // TODO:  Implement a better way to identify wrapper operations.
        {
            return operationHandler.executeOperation(operationIdentifier, operationParameters);
        }
        else
        {
            if (definition.getModuleIdentifier() != null)
            {
                String localOperationIdentifier;
                Map<String, Object> localOperationParameters;

                localOperationIdentifier = T8IdentifierUtilities.stripNamespace(operationIdentifier, definition.getModuleIdentifier(), true);
                localOperationParameters = T8IdentifierUtilities.stripNamespace(definition.getModuleIdentifier(), operationParameters, true);
                return module != null ? T8IdentifierUtilities.prependNamespace(definition.getModuleIdentifier(),  module.executeOperation(localOperationIdentifier, localOperationParameters != null ? localOperationParameters : new HashMap<>())) : null;
            }
            else return null;
        }
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        Map<String, Object> subModuleInputParameters;

        this.inputParameters = inputParameters;
        subModuleInputParameters = T8IdentifierUtilities.stripNamespace(inputParameters);
        subModuleInputParameters = T8IdentifierUtilities.mapParameters(subModuleInputParameters, definition.getInputParameterMapping());
        loadModule(definition.getModuleDefinition(), subModuleInputParameters);
    }

    @Override
    public void startComponent()
    {
        if (module != null)
        {
            module.startComponent();
        }
    }

    @Override
    public void stopComponent()
    {
        if (module != null)
        {
            module.stopComponent();
        }
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        if (module != null) module.stopComponent();
        removeAll();
        module = (T8Module)component;
        add(module, java.awt.BorderLayout.CENTER);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return module;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        if (module != null)
        {
            if (module.getComponentDefinition().getIdentifier().equals(identifier))
            {
                T8Component removedComponent;

                module.stopComponent();
                removedComponent = module;
                removeAll();
                module = null;
                return removedComponent;
            }
            else return null;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<T8Component>();
        if (module != null) childComponents.add(module);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return module != null ? 1 : 0;
    }

    @Override
    public void clearChildComponents()
    {
        unloadModule();
    }

    public void unloadModule()
    {
        if (module != null)
        {
            module.stopComponent();
            removeAll();
            module = null;
        }
    }

    public void reloadModule()
    {
        try
        {
            T8DefinitionManager definitionManager;
            T8ModuleDefinition moduleDefinition;
            Map<String, Object> subModuleInputParameters;

            // Unload the current module (if any).
            unloadModule();

            // Get the input parameters for the sub-module.
            subModuleInputParameters = T8IdentifierUtilities.stripNamespace(inputParameters);
            subModuleInputParameters = T8IdentifierUtilities.mapParameters(subModuleInputParameters, definition.getInputParameterMapping());

            logger.log("Reloading sub-module: " + definition);
            definitionManager = controller.getClientContext().getDefinitionManager();
            moduleDefinition = (T8ModuleDefinition)definitionManager.getInitializedDefinition(controller.getContext(), definition.getRootProjectId(), definition.getModuleIdentifier(), subModuleInputParameters);
            moduleDefinition.addEventHandlerScriptDefinitions(definition.getEventHandlerScriptDefinitions());
            loadModule(moduleDefinition, subModuleInputParameters);
            module.startComponent();
        }
        catch (Exception e)
        {
            logger.log("Exception while attempting to reload sub-module: " + definition.getModuleIdentifier(), e);
        }
    }

    public void loadModule(T8ModuleDefinition moduleDefinition, Map<String, Object> subModuleInputParameters)
    {
        if (moduleDefinition != null)
        {
            try
            {
                T8Module newModule;

                newModule = (T8Module)moduleDefinition.getNewComponentInstance(controller);
                addChildComponent(newModule, null);
                newModule.initializeComponent(subModuleInputParameters);
            }
            catch (Exception ex)
            {
                logger.log("Failed to load module " + moduleDefinition.getIdentifier(), ex);
            }
        }
        else
        {
            clearChildComponents();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
