package com.pilog.t8.ui.flowclient;

import com.pilog.t8.flow.phase.T8FlowPhase;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhaseBlock
{
    private String identifier;
    private Node node;
    private Text statusText;
    private Rectangle rectangle;

    public T8FlowPhaseBlock(String phaseIdentifier)
    {
        this.identifier = phaseIdentifier;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public Node getNode()
    {
        return node;
    }

    public void setNode(Node node)
    {
        this.node = node;
    }

    public Text getStatusText()
    {
        return statusText;
    }

    public void setStatusText(Text statusText)
    {
        this.statusText = statusText;
    }

    public Rectangle getRectangle()
    {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle)
    {
        this.rectangle = rectangle;
    }

    public void update(T8FlowPhase phase)
    {
        rectangle.setFill(phase.getStatus() == T8FlowPhase.T8FlowPhaseStatus.IN_PROGRESS ? Color.BISQUE : Color.CADETBLUE);
        statusText.setText(phase.getStatus().getDisplayName());
    }
}
