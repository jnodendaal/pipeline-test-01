package com.pilog.t8.ui.componentcontroller;

import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.definition.script.T8ComponentEventHandlerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class EventExecution implements Execution
{
    private static final T8Logger LOGGER = T8Log.getLogger(EventExecution.class);

    private final T8Component component;
    private final T8ComponentEventDefinition eventDefinition;
    private final Map<String, Object> eventParameters;
    private final T8ClientScriptRunner scriptRunner;

    public EventExecution(T8ClientScriptRunner scriptRunner, T8Component component, T8ComponentEventDefinition eventDefinition, Map<String, Object> eventParameters)
    {
        this.scriptRunner = scriptRunner;
        this.component = component;
        this.eventDefinition = eventDefinition;

        // Set the event parameters.
        if (eventParameters == null)
        {
            this.eventParameters = new HashMap<String, Object>();
        }
        else this.eventParameters = eventParameters;

        // Add the default event parameter.
        this.eventParameters.put("$P_EVENT_IDENTIFIER", eventDefinition.getIdentifier());
    }

    public T8Component getComponent()
    {
        return component;
    }

    public T8ComponentEventDefinition getEventDefinition()
    {
        return eventDefinition;
    }

    public Map<String, Object> getEventParameters()
    {
        return eventParameters;
    }

    @Override
    public void execute()
    {
        ArrayList<T8ComponentEventHandlerScriptDefinition> eventHandlerDefinitions;

        eventHandlerDefinitions = component.getComponentDefinition().getEventHandlerScriptDefinitions();
        if (eventHandlerDefinitions != null)
        {
            for (T8ComponentEventHandlerScriptDefinition eventHandlerDefinition : eventHandlerDefinitions)
            {
                String eventIdentifier;

                eventIdentifier = eventHandlerDefinition.getEventIdentifier();
                if ((eventIdentifier.equals(eventDefinition.getIdentifier())) || (eventIdentifier.equals(eventDefinition.getPublicIdentifier())) || (eventIdentifier.equals("*")))
                {
                    try
                    {
                        // Execute the event handler script.
                        LOGGER.log("Executing event Handler: " + eventHandlerDefinition.getIdentifier());
                        scriptRunner.runScript(eventHandlerDefinition, eventParameters);
                        LOGGER.log("Finished Executing event Handler: " + eventHandlerDefinition.getIdentifier());
                    }
                    catch (Exception ex)
                    {
                        LOGGER.log("Failed to complete execution of event handler ["+eventHandlerDefinition.getIdentifier()+"]", ex);
                    }
                }
            }
        }
    }
}
