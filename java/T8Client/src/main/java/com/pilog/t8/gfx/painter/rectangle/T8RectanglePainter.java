package com.pilog.t8.gfx.painter.rectangle;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.rectangle.T8RectanglePainterDefinition;
import com.pilog.t8.gfx.painter.T8PainterConfigurator;
import java.awt.Insets;
import org.jdesktop.swingx.painter.RectanglePainter;

/**
 * @author Bouwer du Preez
 */
public class T8RectanglePainter extends RectanglePainter implements T8Painter
{
    private T8RectanglePainterDefinition definition;
    
    public T8RectanglePainter(T8RectanglePainterDefinition definition)
    {
        super(new Insets(definition.getTopInset(), definition.getRightInset(), definition.getBottomInset(), definition.getLeftInset()), definition.getWidth(), definition.getHeight(), definition.getRoundWidth(), definition.getRoundHeight(), true, definition.getFillPaintDefinition().getNewPaintInstance(), definition.getBorderWidth(), definition.getBorderPaintDefinition().getNewPaintInstance());
        this.definition = definition;
        setupPainter();
    }
    
    private void setupPainter()
    {
        // Configure this painter's super type properties.
        T8PainterConfigurator.setupAbstractAreaPainter(this, definition);
    }
}
