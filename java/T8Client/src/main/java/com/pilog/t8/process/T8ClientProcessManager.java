package com.pilog.t8.process;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.process.event.T8ProcessManagerListener;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.process.T8ProcessManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientProcessManager implements T8ProcessManager
{
    @Override
    public void init()
    {
        // Not applicable from client-side.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void destroy()
    {
        // Not applicable from client-side.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ProcessDetails getProcessDetails(T8Context context, String processIid) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_IID, processIid);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_PROCESS_DETAILS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessSummary getProcessSummary(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        return (T8ProcessSummary)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_PROCESS_SUMMARY, operationParameters).get(PARAMETER_PROCESS_SUMMARY);
    }

    @Override
    public int countProcesses(T8Context context, T8ProcessFilter processFilter, String searchExpression) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_FILTER, processFilter);
        operationParameters.put(PARAMETER_SEARCH_EXPRESSION, searchExpression);
        return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COUNT_PROCESSES, operationParameters).get(PARAMETER_PROCESS_COUNT);
    }

    @Override
    public List<T8ProcessDetails> searchProcesses(T8Context context, T8ProcessFilter processFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_FILTER, processFilter);
        operationParameters.put(PARAMETER_SEARCH_EXPRESSION, searchExpression);
        operationParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        operationParameters.put(PARAMETER_PAGE_SIZE, pageSize);
        return (List<T8ProcessDetails>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SEARCH_PROCESSES, operationParameters).get(PARAMETER_PROCESS_LIST);
    }

    @Override
    public T8ProcessDetails startProcess(T8Context context, String processIdentifier, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_ID, processIdentifier);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_START_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessDetails startProcess(T8Context context, T8ProcessDefinition processDefinition, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_DEFINITION, processDefinition);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_START_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessDetails stopProcess(T8Context context, String processInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_IID, processInstanceIdentifier);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_STOP_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessDetails cancelProcess(T8Context context, String processInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_IID, processInstanceIdentifier);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CANCEL_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessDetails pauseProcess(T8Context context, String processInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_IID, processInstanceIdentifier);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_PAUSE_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessDetails resumeProcess(T8Context context, String processInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_IID, processInstanceIdentifier);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_RESUME_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public T8ProcessDetails finalizeProcess(T8Context context, String processInstanceIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROCESS_IID, processInstanceIdentifier);
        return (T8ProcessDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_FINALIZE_PROCESS.toString(), operationParameters).get(PARAMETER_PROCESS_DETAILS);
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addEventListener(T8ProcessManagerListener listener)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeEventListener(T8ProcessManagerListener listener)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
