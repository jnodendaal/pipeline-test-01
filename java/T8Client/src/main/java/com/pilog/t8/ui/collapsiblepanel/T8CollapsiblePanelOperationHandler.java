package com.pilog.t8.ui.collapsiblepanel;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.HashMap;
import java.util.Map;
import org.jdesktop.swingx.painter.Painter;

import static com.pilog.t8.definition.ui.collapsiblepanel.T8CollapsiblePanelAPIHandler.*;


/**
 * @author Bouwer du Preez
 */
public class T8CollapsiblePanelOperationHandler extends T8ComponentOperationHandler
{
    private T8CollapsiblePanel panel;
    private Map<String, Painter> painterCache;

    public T8CollapsiblePanelOperationHandler(T8CollapsiblePanel panel)
    {
        super(panel);
        this.panel = panel;
        this.painterCache = new HashMap<>();
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (OPERATION_SET_HEADER_TEXT.equals(operationIdentifier))
        {
            String text;

            text = (String) operationParameters.get(PARAMETER_TEXT);
            panel.setHeaderText(text);
            return null;
        } else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
