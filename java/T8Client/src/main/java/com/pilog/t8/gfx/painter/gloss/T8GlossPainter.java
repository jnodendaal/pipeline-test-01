package com.pilog.t8.gfx.painter.gloss;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.gloss.T8GlossPainterDefinition;
import org.jdesktop.swingx.painter.GlossPainter;

/**
 * @author Bouwer du Preez
 */
public class T8GlossPainter extends GlossPainter implements T8Painter
{
    private T8GlossPainterDefinition definition;
    
    public T8GlossPainter(T8GlossPainterDefinition definition)
    {
        super(definition.getGlossPaintDefinition().getNewPaintInstance(), definition.getGlossPosition() == T8GlossPainterDefinition.GlossPosition.TOP ? GlossPainter.GlossPosition.TOP : GlossPainter.GlossPosition.BOTTOM);
        this.definition = definition;
    }
}
