package com.pilog.t8.ui.processlist;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.process.T8ProcessFilter;
import com.pilog.t8.process.T8ProcessSummary;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.processlist.T8ProcessListAPIHandler;
import com.pilog.t8.definition.ui.processlist.T8ProcessListDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessListView extends JXPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProcessListView.class);

    protected T8ProcessListDefinition definition;
    protected T8ComponentController controller;
    protected T8ConfigurationManager configurationManager;
    protected T8Context context;
    protected T8ProcessManager processManager;
    protected ProcessList processListPanel;
    protected T8ProcessSummary processListSummary;
    protected List<T8ProcessDetails> processList;
    protected T8ProcessFilter processFilter;
    protected T8DefaultComponentContainer contentContainer;
    protected T8ProcessListOperationHandler operationHandler;
    private Timer timer; // Timer used to update process details.
    private ProcessListLoader lastLoader;
    private Painter backgroundPainter;

    private static final long PROCESS_MAINTENANCE_INTERVAL = 2000; // The interval in milliseconds at which the maintenance thread is executed.

    public T8ProcessListView(T8ProcessListDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.processFilter = new T8ProcessFilter();
        this.processFilter.setPageSize(10);
        this.operationHandler = new T8ProcessListOperationHandler(this);
        initComponents();
        setupComponents();
        setupPainters();
    }

    private void setupComponents()
    {
        processListPanel = new ProcessList(this);
        jPanelContent.add(processListPanel, java.awt.BorderLayout.CENTER);

        contentContainer = new T8DefaultComponentContainer();
        contentContainer.setOpaque(false);
        contentContainer.setComponent(jPanelContent);
        add(contentContainer, BorderLayout.CENTER);
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize the painter for painting the background of this component.
        painterDefinition = definition.getBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
            setBackgroundPainter(backgroundPainter);
        }
    }

    public String getTranslatedString(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    public T8Context getAccessContext()
    {
        return context;
    }

    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ProcessListDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.processManager = controller.getClientContext().getProcessManager();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
        try
        {
            filterProcessList(null);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing process list.", e);
        }

        // Start the maintenance timer.
        if (timer != null) timer.cancel(); // Make sure any previous timers have been cancelled before starting a new one.
        timer = new Timer() ;
        timer.schedule(new ProcessChecker(), PROCESS_MAINTENANCE_INTERVAL, PROCESS_MAINTENANCE_INTERVAL);
    }

    @Override
    public void stopComponent()
    {
        // Terminate the timer thread and all scheduled tasks.
        timer.cancel() ;
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void showContentView()
    {
        contentContainer.unlock();
        repaint();
    }

    public void showBusyView()
    {
        contentContainer.setMessage(getTranslatedString("Loading Process List..."));
        contentContainer.lock();
        repaint();
    }

    public void retrieveNextPage()
    {
        int pageOffset;

        pageOffset = processFilter.getPageOffset();
        pageOffset += processFilter.getPageSize();
        processFilter.setPageOffset(pageOffset);
        refreshProcessList();
    }

    private void retrievePreviousPage()
    {
        int pageOffset;

        pageOffset = processFilter.getPageOffset();
        pageOffset -= processFilter.getPageSize();
        if (pageOffset < 0) pageOffset = 0;
        processFilter.setPageOffset(pageOffset);
        refreshProcessList();
    }

    public void filterProcessList(String processId)
    {
        LOGGER.log("Filtering process list: " + processId);
        processFilter.clearProcessIds();
        if (processId != null) processFilter.addProcessId(processId);
        processFilter.setPageOffset(0);
        processListPanel.clearProcesses();
        refreshProcessList();
    }

    public void refreshProcessList()
    {
        if (lastLoader != null) lastLoader.cancel(false);

        // Run the loader in a new Thread.
        lastLoader = new ProcessListLoader();
        lastLoader.execute();
    }

    private class ProcessListLoader extends SwingWorker<Void, Integer>
    {
        @Override
        protected Void doInBackground() throws Exception
        {
            // Update the process list with the latest process details from the server.
            LOGGER.log("Refreshing process list:  User: " + context.getSessionContext().getUserIdentifier() + "...");
            super.publish(0);
            processListSummary = processManager.getProcessSummary(context);
            processList = processManager.searchProcesses(context, processFilter, null, 0, 10);
            super.publish(100);
            return null;
        }

        @Override
        protected void process(List<Integer> chunks)
        {
            if(!chunks.contains(100))
                showBusyView();
        }

        @Override
        public void done()
        {
            if (!isCancelled())
            {
                // Set the process total and paging counts on the UI.
                try
                {
                    int processCount;

                    // The only purpose of this statement is to rethrow exceptions.
                    get();

                    // Set the process total and paginng counts on the UI.
                    processCount = processListSummary.getProcessCount(processFilter);
                    jLabelProcessCount.setText("" + processCount+ "  "); // The last two spaces just adds a bit of padding on the tool bar.
                    jLabelPageStartOffset.setText("" + processFilter.getPageOffset());
                    jLabelPageEndOffset.setText("" + (processFilter.getPageOffset() + processFilter.getPageSize() - 1));
                    jButtonPreviousPage.setEnabled(processFilter.getPageOffset() != 0);
                    jButtonNextPage.setEnabled((processFilter.getPageOffset() + processFilter.getPageSize()) < processCount);
                    processListPanel.setProcessList(processList);
                }
                catch (InterruptedException | ExecutionException e)
                {
                    LOGGER.log("Exception while refreshing process list.", e);
                }
            }

            // Show the loaded data.
            showContentView();
        }
    }

    void finalizeProcess(T8ProcessDetails processDetails)
    {
        try
        {
            processManager.finalizeProcess(context, processDetails.getProcessIid());
            fireProcessFinalizedEvent(processDetails);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while finalizing process: " + processDetails.getProcessIid(), e);
        }

        // Refresh the process list to reflect the changes.
        refreshProcessList();
    }

    void stopProcess(T8ProcessDetails processDetails)
    {
        try
        {
            processManager.stopProcess(context, processDetails.getProcessIid());
            fireProcessStoppedEvent(processDetails);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while stopping process: " + processDetails.getProcessIid(), e);
        }

        // Refresh the process list to reflect the changes.
        refreshProcessList();
    }

    protected void fireProcessStoppedEvent(T8ProcessDetails processDetails)
    {
        HashMap<String, Object> eventParameters;

        LOGGER.log("Stopped Process: " + processDetails.getProcessIid());
        eventParameters = new HashMap<String, Object>();
        eventParameters.put(T8ProcessListAPIHandler.PARAMETER_PROCESS_IDENTIFIER, processDetails.getProcessId());
        eventParameters.put(T8ProcessListAPIHandler.PARAMETER_PROCESS_INSTANCE_IDENTIFIER, processDetails.getProcessIid());
        controller.reportEvent(this, definition.getComponentEventDefinition(T8ProcessListAPIHandler.EVENT_PROCESS_STOPPED), eventParameters);
    }

    protected void fireProcessFinalizedEvent(T8ProcessDetails processDetails) throws Exception
    {
        HashMap<String, Object> eventParameters;

        LOGGER.log("Finalized Process: " + processDetails.getProcessIid());
        eventParameters = new HashMap<String, Object>();
        eventParameters.put(T8ProcessListAPIHandler.PARAMETER_PROCESS_IDENTIFIER, processDetails.getProcessId());
        eventParameters.put(T8ProcessListAPIHandler.PARAMETER_PROCESS_INSTANCE_IDENTIFIER, processDetails.getProcessIid());
        controller.reportEvent(this, definition.getComponentEventDefinition(T8ProcessListAPIHandler.EVENT_PROCESS_FINALIZED), eventParameters);
    }

    /**
     * This class is a task that is scheduled using a timer object and is used
     * to run regular maintenance checks on the currently executing processes
     * such as updating the list of active process details.
     */
    private class ProcessChecker extends TimerTask
    {
        @Override
        public void run()
        {
            if (lastLoader.isDone())
            {
                refreshProcessList();
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jPanelToolBars = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jToolBarPaging = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelProcess = new javax.swing.JLabel();
        jLabelPageStartOffset = new javax.swing.JLabel();
        jLabelPageRangeDash = new javax.swing.JLabel();
        jLabelPageEndOffset = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jLabelProcessCount = new javax.swing.JLabel();

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.BorderLayout());

        jPanelToolBars.setOpaque(false);
        jPanelToolBars.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(getTranslatedString("Refresh"));
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelToolBars.add(jToolBarMain, gridBagConstraints);

        jToolBarPaging.setFloatable(false);
        jToolBarPaging.setRollover(true);
        jToolBarPaging.setOpaque(false);

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(getTranslatedString("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPaging.add(jButtonPreviousPage);

        jLabelProcess.setText(getTranslatedString("Process: "));
        jToolBarPaging.add(jLabelProcess);
        jToolBarPaging.add(jLabelPageStartOffset);

        jLabelPageRangeDash.setText("-");
        jToolBarPaging.add(jLabelPageRangeDash);
        jToolBarPaging.add(jLabelPageEndOffset);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(getTranslatedString("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPaging.add(jButtonNextPage);

        jLabelProcessCount.setText("(0)  ");
        jToolBarPaging.add(jLabelProcessCount);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelToolBars.add(jToolBarPaging, gridBagConstraints);

        jPanelContent.add(jPanelToolBars, java.awt.BorderLayout.NORTH);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshProcessList();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        retrievePreviousPage();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        retrieveNextPage();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelPageEndOffset;
    private javax.swing.JLabel jLabelPageRangeDash;
    private javax.swing.JLabel jLabelPageStartOffset;
    private javax.swing.JLabel jLabelProcess;
    private javax.swing.JLabel jLabelProcessCount;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelToolBars;
    private javax.swing.JToolBar jToolBarMain;
    private javax.swing.JToolBar jToolBarPaging;
    // End of variables declaration//GEN-END:variables

}
