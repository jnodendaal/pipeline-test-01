package com.pilog.t8.process;

import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ClientProcessManagerScriptFacade
{
    private final T8ProcessManager processManager;
    private final T8Context context;

    public T8ClientProcessManagerScriptFacade(T8Context context, T8ProcessManager processManager)
    {
        this.processManager = processManager;
        this.context = context;
    }

    public T8ProcessSummary getProcessSummary() throws Exception
    {
        return processManager.getProcessSummary(context);
    }

    public T8ProcessDetails getProcessDetails(String processInstanceIdentifier) throws Exception
    {
        return processManager.getProcessDetails(context, processInstanceIdentifier);
    }
}
