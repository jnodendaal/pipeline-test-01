package com.pilog.t8.functionality.view;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8OperationFunctionalityAccessHandle;
import com.pilog.t8.ui.operationexecutiondialog.T8DefaultOperationExecutionDialog;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8OperationFunctionalityView extends T8DefaultOperationExecutionDialog implements T8FunctionalityView
{
    private final T8OperationFunctionalityAccessHandle accessHandle;
    private final T8ComponentController controller;
    private final EventListenerList viewListeners;

    public T8OperationFunctionalityView(T8ComponentController controller, T8OperationFunctionalityAccessHandle accessHandle)
    {
        super(controller, accessHandle.getDisplayName(), "Executing operation...");
        this.controller = controller;
        this.accessHandle = accessHandle;
        this.viewListeners = new EventListenerList();
    }

    @Override
    public T8FunctionalityAccessHandle getExecutionHandle()
    {
        return accessHandle;
    }

    @Override
    public void initialize() throws Exception
    {
    }

    @Override
    public void startComponent()
    {
        displayExecution(accessHandle.getOperationStatusReport().getOperationInstanceIdentifier());
        setVisible(true);
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addFunctionalityViewListener(T8FunctionalityViewListener listener)
    {
        viewListeners.add(T8FunctionalityViewListener.class, listener);
    }

    @Override
    public void removeFunctionalityViewListener(T8FunctionalityViewListener listener)
    {
        viewListeners.remove(T8FunctionalityViewListener.class, listener);
    }

    @Override
    protected void completeExecution()
    {
        super.completeExecution();
        fireFunctionalityViewClosedEvent();
    }

    private void fireFunctionalityViewUpdatedEvent()
    {
        final T8FunctionalityViewUpdatedEvent event;

        // Create and fire the event.
        event = new T8FunctionalityViewUpdatedEvent(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                for (T8FunctionalityViewListener viewListener : viewListeners.getListeners(T8FunctionalityViewListener.class))
                {
                    viewListener.functionalityViewUpdated(event);
                }
            }
        });
    }

    private void fireFunctionalityViewClosedEvent()
    {
        final T8FunctionalityViewClosedEvent event;

        // Create and fire the event.
        event = new T8FunctionalityViewClosedEvent(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                for (T8FunctionalityViewListener viewListener : viewListeners.getListeners(T8FunctionalityViewListener.class))
                {
                    viewListener.functionalityViewClosed(event);
                }
            }
        });
    }
}
