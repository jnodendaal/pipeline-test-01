package com.pilog.t8.ui.taskpanecontainer;

import java.awt.Insets;
import org.jdesktop.swingx.JXTaskPaneContainer;

/**
 * @author Bouwer du Preez
 */
public class T8TaskPaneContainer extends JXTaskPaneContainer
{
    private Insets insets;
    
    public T8TaskPaneContainer()
    {
        insets = new Insets(10, 10, 10, 10);
    }
    
    @Override
    public Insets getInsets()
    {
        return (Insets)insets.clone();
    }
}
