package com.pilog.t8.ui.laf;

import com.pilog.t8.gfx.T8GraphicsUtilities;
import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.LookAndFeel;
import javax.swing.plaf.ComponentUI;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.plaf.basic.BasicTaskPaneContainerUI;

/**
 * @author Bouwer du Preez
 */
public class T8TaskPaneContainerUI extends BasicTaskPaneContainerUI
{
    /**
     * Returns a new instance of T8TaskPaneContainerUI.  T8TaskPaneContainerUI 
     * delegates are allocated one per JXTaskPaneContainer.
     *
     * @return A new T8TaskPaneContainerUI implementation for the Basic look and
     * feel.
     */
    public static ComponentUI createUI(JComponent c)
    {
        return new T8TaskPaneContainerUI();
    }

    /**
     * Installs the default colors, border, and painter of the task pane
     * container.
     */
    @Override
    protected void installDefaults()
    {
        LookAndFeel.installColors(taskPane, "TaskPaneContainer.background", "TaskPaneContainer.foreground");
        LookAndFeel.installBorder(taskPane, "TaskPaneContainer.border");
        //LookAndFeelAddons.installBackgroundPainter(taskPane, "TaskPaneContainer.backgroundPainter");
        taskPane.setBackgroundPainter(new MattePainter(T8GraphicsUtilities.createTopToBottomLinearGradient(new Color(153, 180, 209), new Color(191, 205, 219)), true));
        LookAndFeel.installProperty(taskPane, "opaque", Boolean.TRUE);
    }
}
