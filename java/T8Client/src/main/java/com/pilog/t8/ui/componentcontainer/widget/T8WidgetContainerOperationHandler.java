/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.ui.componentcontainer.widget;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8WidgetContainerOperationHandler extends T8ComponentOperationHandler
{
    private final T8WidgetContainer container;

    public T8WidgetContainerOperationHandler(T8WidgetContainer container)
    {
        super(container);
        this.container = container;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return super.executeOperation(operationIdentifier, operationParameters);
    }
}
