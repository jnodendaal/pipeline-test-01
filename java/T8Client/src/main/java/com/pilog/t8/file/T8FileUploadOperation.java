package com.pilog.t8.file;

import com.pilog.t8.security.T8Context;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * @author Bouwer du Preez
 */
public class T8FileUploadOperation
{
    private final T8ClientFileManager client;
    private final T8Context context;
    private final String contextIid;
    private final String localFilePath;
    private final String remoteFilePath;
    private String fileMediaType;
    private long fileSize;
    private long bytesUploaded;
    private boolean stopFlag;

    private static final int FILE_SPLICE_SIZE = (1024*1024); // The number of bytes that will be uploaded to the server at a time.

    public T8FileUploadOperation(T8Context context, T8ClientFileManager client, String contextID, String localFilePath, String remoteFilePath)
    {
        this.contextIid = contextID;
        this.context = context;
        this.localFilePath = localFilePath;
        this.remoteFilePath = remoteFilePath;
        this.client = client;
        this.stopFlag = false;
    }

    public void start() throws Exception
    {
        File targetFile;

        targetFile = new File(localFilePath);
        if (targetFile.exists())
        {
            BufferedInputStream inputStream = null;

            try
            {
                byte[] buffer;
                int bytesRead;

                stopFlag = false;
                bytesUploaded = 0;
                fileSize = targetFile.length();
                // TODO: GBO - This doesn't work properly. Want to remove the reference on the client side
                // Check that it is removed properly instead of only partially.
                fileMediaType = Files.probeContentType(targetFile.toPath());

                inputStream = new BufferedInputStream(new FileInputStream(targetFile));
                buffer = new byte[FILE_SPLICE_SIZE];
                while (((bytesRead = inputStream.read(buffer, 0, FILE_SPLICE_SIZE)) >= 0) && (!stopFlag))
                {
                    if (bytesRead < FILE_SPLICE_SIZE)
                    {
                        bytesUploaded = client.uploadFileData(context, contextIid, remoteFilePath, Arrays.copyOf(buffer, bytesRead));
                    }
                    else
                    {
                        bytesUploaded = client.uploadFileData(context, contextIid, remoteFilePath, buffer);
                    }
                }
            }
            finally
            {
                if (inputStream != null) inputStream.close();
            }
        }
        else throw new Exception("File not found: " + localFilePath);
    }

    public T8FileDetails getUploadedFileDetails()
    {
        return new T8FileDetails(contextIid, null, remoteFilePath, bytesUploaded, false, null, fileMediaType);
    }

    public void stop()
    {
        stopFlag = true;
    }

    public long getByteSizeUploaded()
    {
        return bytesUploaded;
    }

    public long getTotalByteSize()
    {
        return fileSize;
    }

    public int getProgress()
    {
        return (int)(((float)bytesUploaded) / ((float)fileSize) * 100.00);
    }
}
