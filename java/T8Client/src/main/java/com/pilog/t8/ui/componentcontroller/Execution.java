package com.pilog.t8.ui.componentcontroller;

/**
 * @author Bouwer du Preez
 */
public interface Execution
{
    public void execute();
}
