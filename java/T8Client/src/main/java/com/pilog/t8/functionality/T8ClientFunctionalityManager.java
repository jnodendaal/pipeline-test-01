package com.pilog.t8.functionality;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.functionality.access.T8FunctionalityAccessRights;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.log.T8Logger;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.functionality.T8FunctionalityManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFunctionalityManager implements T8FunctionalityManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClientFunctionalityManager.class);

    @Override
    public void init() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearCache(T8Context context)
    {
        try
        {
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_CLEAR_FUNCTIONALITY_CACHE, null);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to clear the functionality cache", ex);
        }
    }

    @Override
    public void reloadAccessPolicy(T8Context context)
    {
        try
        {
            T8MainServerClient.executeSynchronousOperation(context, OPERATION_RELOAD_FUNCTIONALITY_ACCESS_POLICY, null);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while reloading functionality access policy.", e);
        }
    }

    @Override
    public T8FunctionalityGroupHandle getFunctionalityGroupHandle(T8Context context, String functionalityGroupId, String dataObjectId, String dataObjectIid) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FUNCTIONALITY_GROUP_ID, functionalityGroupId);
        operationParameters.put(PARAMETER_DATA_OBJECT_ID, dataObjectId);
        operationParameters.put(PARAMETER_DATA_OBJECT_IID, dataObjectIid);
        return (T8FunctionalityGroupHandle)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FUNCTIONALITY_GROUP_HANDLE, operationParameters).get(PARAMETER_FUNCTIONALITY_GROUP_HANDLE);
    }

    @Override
    public T8FunctionalityHandle getFunctionalityHandle(T8Context context, String functionalityId, String dataObjectId, String dataObjectIid) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FUNCTIONALITY_ID, functionalityId);
        operationParameters.put(PARAMETER_DATA_OBJECT_ID, dataObjectId);
        operationParameters.put(PARAMETER_DATA_OBJECT_IID, dataObjectIid);
        return (T8FunctionalityHandle)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_FUNCTIONALITY_HANDLE, operationParameters).get(PARAMETER_FUNCTIONALITY_HANDLE);
    }

    @Override
    public T8FunctionalityAccessHandle accessFunctionality(T8Context context, String functionalityId, Map<String, Object> parameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FUNCTIONALITY_ID, functionalityId);
        operationParameters.put(PARAMETER_FUNCTIONALITY_PARAMETERS, parameters);
        return (T8FunctionalityAccessHandle)T8MainServerClient.executeSynchronousOperation(context, OPERATION_ACCESS_FUNCTIONALITY, operationParameters).get(PARAMETER_FUNCTIONALITY_EXECUTION_HANDLE);
    }

    @Override
    public void accessDataObject(T8DataTransaction tx, String functionalityIid, String dataObjectId, String dataObjectIid) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void releaseFunctionality(T8Context context, String functionalityIid) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_FUNCTIONALITY_IID, functionalityIid);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_RELEASE_FUNCTIONALITY, operationParameters);
    }

    @Override
    public T8FunctionalityAccessRights getFunctionalityAccessRights(T8Context context, String functionalityIid)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getStateConceptId(T8Context context, String stateId) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8FunctionalityAccessRights getWorkflowAccessRights(T8Context context, String flowIid)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
