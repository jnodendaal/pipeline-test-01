package com.pilog.t8.definition;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerListener;
import com.pilog.t8.definition.filter.T8DefinitionFilter;
import com.pilog.t8.definition.patch.T8DefinitionPatch;
import com.pilog.t8.project.T8ProjectImportReport;
import com.pilog.t8.system.T8SystemDetails;
import com.pilog.t8.definition.system.T8SetupDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.event.EventListenerList;

import static com.pilog.t8.definition.T8DefinitionManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ClientDefinitionManager implements T8DefinitionManager
{
    private final T8Context context;
    private final EventListenerList eventListeners;
    private final Map<String, T8DataType> dataTypeCache;

    public T8ClientDefinitionManager(T8Context context)
    {
        this.context = context;
        this.eventListeners = new EventListenerList();
        this.dataTypeCache = new HashMap<>();
    }

    @Override
    public void init() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void destroy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8Definition initializeDefinition(T8Context context, T8Definition definition, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION, definition);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_INITIALIZE_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public List<String> getDefinitionTypeIdentifiers() throws Exception
    {
        return getDefinitionTypeIdentifiers(null);
    }

    @Override
    public List<String> getDefinitionTypeIdentifiers(String groupIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_GROUP_ID, groupIdentifier);
        return (List<String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_TYPE_IDS, operationParameters).get(PARAMETER_DEFINITION_TYPE_ID_LIST);
    }

    @Override
    public List<String> getDefinitionIdentifiers(String projectId, String typeId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_TYPE_ID, typeId);
        return (List<String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_IDS, operationParameters).get(PARAMETER_DEFINITION_ID_LIST);
    }

    @Override
    public List<String> getGroupDefinitionIdentifiers(String projectId, String groupId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_GROUP_ID, groupId);
        return (List<String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_GROUP_DEFINITION_IDS, operationParameters).get(PARAMETER_DEFINITION_ID_LIST);
    }

    @Override
    public List<String> getDefinitionGroupIdentifiers() throws Exception
    {
        return (List<String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_GROUP_IDS, null).get(PARAMETER_DEFINITION_GROUP_ID_LIST);
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        return (T8DefinitionMetaData)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA);
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(T8DefinitionHandle definitionHandle) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        return (T8DefinitionMetaData)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA);
    }

    @Override
    public List<T8DefinitionMetaData> getDefinitionMetaData(List<T8DefinitionHandle> definitionHandles) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE_LIST, definitionHandles);
        return (List<T8DefinitionMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA_LIST);
    }

    @Override
    public ArrayList<T8DefinitionMetaData> getProjectDefinitionMetaData(String projectId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        return (ArrayList<T8DefinitionMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_PROJECT_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA_LIST);
    }

    @Override
    public ArrayList<T8DefinitionMetaData> getTypeDefinitionMetaData(String projectId, String typeId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_TYPE_ID, typeId);
        return (ArrayList<T8DefinitionMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_TYPE_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA_LIST);
    }

    @Override
    public ArrayList<T8DefinitionMetaData> getGroupDefinitionMetaData(String projectId, String groupId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_GROUP_ID, groupId);
        return (ArrayList<T8DefinitionMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_GROUP_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA_LIST);
    }

    @Override
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_TYPE_ID, typeId);
        return (T8DefinitionTypeMetaData)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_TYPE_META_DATA, operationParameters).get(PARAMETER_DEFINITION_TYPE_META_DATA);
    }

    @Override
    public ArrayList<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_GROUP_ID, groupId);
        return (ArrayList<T8DefinitionTypeMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_GROUP_DEFINITION_TYPE_META_DATA, operationParameters).get(PARAMETER_DEFINITION_TYPE_META_DATA_LIST);
    }

    @Override
    public ArrayList<T8DefinitionGroupMetaData> getAllDefinitionGroupMetaData() throws Exception
    {
        return (ArrayList<T8DefinitionGroupMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ALL_DEFINITION_GROUP_META_DATA, null).get(PARAMETER_DEFINITION_GROUP_META_DATA);
    }

    @Override
    public ArrayList<T8DefinitionTypeMetaData> getAllDefinitionTypeMetaData() throws Exception
    {
        return (ArrayList<T8DefinitionTypeMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ALL_DEFINITION_TYPE_META_DATA, null).get(PARAMETER_DEFINITION_TYPE_META_DATA_LIST);
    }

    @Override
    public ArrayList<T8Definition> getRawGroupDefinitions(T8Context context, String projectId, String groupId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_GROUP_ID, groupId);
        return (ArrayList<T8Definition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_RAW_GROUP_DEFINITIONS, operationParameters).get(PARAMETER_DEFINITION_LIST);
    }

    @Override
    public <T extends T8Definition> List<T> getInitializedGroupDefinitions(T8Context context, String projectId, String groupId, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_GROUP_ID, groupId);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (List<T>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_INITIALIZED_GROUP_DEFINITIONS, operationParameters).get(PARAMETER_DEFINITION_LIST);
    }

    @Override
    public T8Definition getResolvedDefinition(T8Context context, String typeId, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_TYPE_ID, typeId);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_RESOLVED_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <D extends T8Definition> D getInitializedDefinition(T8Context context, String projectId, String definitionId, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);
        return (D) T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_INITIALIZED_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public List<T8DefinitionHandle> renameDefinition(T8Context context, T8Definition definition, String newId) throws Exception
    {
        HashMap<String, Object> operationParameters;
        Map<String, Object> outputParameters;
        T8Definition renamedDefinition;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION, definition);
        operationParameters.put(PARAMETER_NEW_DEFINITION_ID, newId);
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, OPERATION_RENAME_DEFINITION, operationParameters);
        renamedDefinition = (T8Definition)outputParameters.get(PARAMETER_DEFINITION);
        if (renamedDefinition != null)
        {
            definition.setIdentifier(renamedDefinition.getIdentifier(), true);
        }

        return (List<T8DefinitionHandle>)outputParameters.get(PARAMETER_DEFINITION_HANDLE_LIST);
    }

    @Override
    public void transferDefinitionReferences(T8Context context, String oldId, String newId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_OLD_DEFINITION_ID, oldId);
        operationParameters.put(PARAMETER_NEW_DEFINITION_ID, newId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_TRANSFER_DEFINITION_REFERENCES, operationParameters);
    }

    @Override
    public T8Definition copyDefinition(T8Context context, T8Definition definition, String newId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION, definition);
        operationParameters.put(PARAMETER_NEW_DEFINITION_ID, newId);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COPY_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8Definition copyDefinition(T8Context context, String projectId, String definitionId, String newDefinitionId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        operationParameters.put(PARAMETER_NEW_DEFINITION_ID, newDefinitionId);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COPY_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public boolean copyDefinitionToProject(T8Context context, String projectId, String definitionId, String newProjectId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        operationParameters.put(PARAMETER_NEW_PROJECT_ID, newProjectId);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COPY_DEFINITION_TO_PROJECT, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public boolean moveDefinitionToProject(T8Context context, String projectId, String definitionId, String newProjectId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        operationParameters.put(PARAMETER_NEW_PROJECT_ID, newProjectId);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_MOVE_DEFINITION_TO_PROJECT, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public T8Definition createNewDefinition(String identifier, String typeId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_ID, identifier);
        operationParameters.put(PARAMETER_DEFINITION_TYPE_ID, typeId);
        operationParameters.put(PARAMETER_DEFINITION_CONSTRUCTOR_PARAMETER_LIST, null);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_NEW_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8Definition createNewDefinition(String id, String typeId, List constructorParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_ID, id);
        operationParameters.put(PARAMETER_DEFINITION_TYPE_ID, typeId);
        operationParameters.put(PARAMETER_DEFINITION_CONSTRUCTOR_PARAMETER_LIST, constructorParameters);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_NEW_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public <D extends T8Definition> D getRawDefinition(T8Context context, String projectId, String definitionId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        return (D)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_RAW_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8Definition getRawDefinition(T8Context context, T8DefinitionHandle definitionHandle) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_RAW_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8Definition lockDefinition(T8Context context, T8DefinitionHandle definitionHandle, String keyId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        operationParameters.put(PARAMETER_KEY_ID, keyId);
        return (T8Definition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_LOCK_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8Definition unlockDefinition(T8Context context, T8DefinitionHandle definitionHandle, String keyIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        operationParameters.put(PARAMETER_KEY_ID, keyIdentifier);
        return (T8Definition) T8MainServerClient.executeSynchronousOperation(context, OPERATION_UNLOCK_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8Definition unlockDefinition(T8Context context, T8Definition definition, String keyIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION, definition);
        operationParameters.put(PARAMETER_KEY_ID, keyIdentifier);
        return (T8Definition) T8MainServerClient.executeSynchronousOperation(context, OPERATION_UNLOCK_DEFINITION, operationParameters).get(PARAMETER_DEFINITION);
    }

    @Override
    public T8DefinitionLockDetails getDefinitionLockDetails(T8Context context, T8DefinitionHandle definitionHandle) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        return (T8DefinitionLockDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_LOCK_DETAILS, operationParameters).get(PARAMETER_DEFINITION_LOCK_DETAILS);
    }

    @Override
    public void removeDefinitionReferences(T8Context context, String identifier, String keyIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_ID, identifier);
        operationParameters.put(PARAMETER_KEY_ID, keyIdentifier);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_REMOVE_DEFINITION_REFERENCES, operationParameters);
    }

    @Override
    public void deleteDefinition(T8Context context, String projectId, String definitionId, boolean safeDeletion, String keyIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        operationParameters.put(PARAMETER_KEY_ID, keyIdentifier);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_DELETE_DEFINITION, operationParameters);
    }

    @Override
    public T8DefinitionMetaData finalizeDefinition(T8Context context, T8DefinitionHandle definitionHandle, String comment) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        operationParameters.put(PARAMETER_COMMENT, comment);
        return (T8DefinitionMetaData)T8MainServerClient.executeSynchronousOperation(context, OPERATION_FINALIZE_DEFINITION, operationParameters).get(PARAMETER_DEFINITION_META_DATA);
    }

    @Override
    public boolean deleteDefinition(T8Context context, T8DefinitionHandle definitionHandle, boolean safeDeletion, String keyIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_HANDLE, definitionHandle);
        operationParameters.put(PARAMETER_SAFE_DELETION, safeDeletion);
        operationParameters.put(PARAMETER_KEY_ID, keyIdentifier);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_DELETE_DEFINITION, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public T8DefinitionMetaData saveDefinition(T8Context context, T8Definition definition, String keyIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;
        T8DefinitionMetaData metaData;

        // Execute the operation.
        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION, definition);
        operationParameters.put(PARAMETER_KEY_ID, keyIdentifier);
        metaData = (T8DefinitionMetaData)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SAVE_DEFINITION, operationParameters).get(PARAMETER_DEFINITION_META_DATA);

        // Update the local definition with the new meta data received from the server.
        if (metaData != null) definition.setDefinitionMetaData(metaData, true);
        return metaData;
    }

    @Override
    public void loadDefinitionData(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_RELOAD_DEFINITIONS, operationParameters);

        // Fire an event to let all listeners know that the cache has been reloaded.
        fireDefinitionCacheReloadedEvent();
    }

    @Override
    public boolean checkIdentifierAvailability(T8Context context, String projectId, String definitionId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CHECK_ID_AVAILABILITY, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByText(T8Context context, String text) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TEXT, text);
        return (List<T8DefinitionHandle>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_FIND_DEFINITIONS_BY_TEXT, operationParameters).get(PARAMETER_DEFINITION_HANDLE_LIST);
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByRegularExpression(T8Context context, String expression) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_REGULAR_EXPRESSION, expression);
        return (List<T8DefinitionHandle>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_FIND_DEFINITIONS_BY_REGULAR_EXPRESSION, operationParameters).get(PARAMETER_DEFINITION_HANDLE_LIST);
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByIdentifier(T8Context context, String identifier, boolean includeReferencesOnly) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_ID, identifier);
        operationParameters.put(PARAMETER_INCLUDE_REFERENCES_ONLY, includeReferencesOnly);
        return (List<T8DefinitionHandle>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_FIND_DEFINITIONS_BY_ID, operationParameters).get(PARAMETER_DEFINITION_HANDLE_LIST);
    }

    @Override
    public List<T8DefinitionHandle> getAllDefinitionHandles(T8Context context, T8DefinitionFilter definitionFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_FILTER, definitionFilter);
        return (List<T8DefinitionHandle>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ALL_DEFINITION_HANDLES, operationParameters).get(PARAMETER_DEFINITION_HANDLE_LIST);
    }

    @Override
    public ArrayList<T8DefinitionMetaData> getAllDefinitionMetaData(T8Context context, T8DefinitionFilter definitionFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION_FILTER, definitionFilter);
        return (ArrayList<T8DefinitionMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ALL_DEFINITION_META_DATA, operationParameters).get(PARAMETER_DEFINITION_META_DATA_LIST);
    }

    @Override
    public void importDefinition(T8Context context, T8Definition definition) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DEFINITION, definition);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_IMPORT_DEFINITION, operationParameters);
    }

    @Override
    public int getCachedDefinitionTypeCount(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getCachedDefinitionCount(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getCachedDataSize(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addDefinitionManagerListener(T8DefinitionManagerListener listener)
    {
        eventListeners.add(T8DefinitionManagerListener.class, listener);
    }

    @Override
    public void removeDefinitionManagerListener(T8DefinitionManagerListener listener)
    {
        if (eventListeners != null) eventListeners.remove(T8DefinitionManagerListener.class, listener);
    }

    private void fireDefinitionCacheReloadedEvent()
    {
        if (eventListeners != null)
        {
            T8DefinitionCacheReloadedEvent event;

            event = new T8DefinitionCacheReloadedEvent(this);
            for (T8DefinitionManagerListener listener : eventListeners.getListeners(T8DefinitionManagerListener.class))
            {
                listener.definitionCacheReloaded(event);
            }
        }
    }

    @Override
    public T8SetupDefinition getSetupDefinition(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void saveSetupDefinition(T8Context context, T8SetupDefinition setupDefinition) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8SystemDefinition getSystemDefinition(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8SystemDetails getSystemDetails()
    {
        try
        {
            return (T8SystemDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_SYSTEM_DETAILS, null).get(PARAMETER_SYSTEM_DETAILS);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Could not retrieve system identifier from server.", e);
        }
    }

    @Override
    public List<T8DefinitionPatch> getDefinitionPatches()
    {
        // Patches are not available on the client-side.  It is important for
        // this method to conform to the conract of the method by returning an
        // empty list instead of null.
        return new ArrayList<>();
    }

    @Override
    public T8DataType createDataType(String dataTypeString)
    {
        T8DataType dataType;

        dataType = dataTypeCache.get(dataTypeString);
        if (dataType != null) return dataType;
        else
        {
            try
            {
                HashMap<String, Object> operationParameters;

                operationParameters = new HashMap<>();
                operationParameters.put(PARAMETER_DATA_TYPE_STRING, dataTypeString);
                dataType = (T8DataType)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_DATA_TYPE, operationParameters).get(PARAMETER_DATA_TYPE);
                dataTypeCache.put(dataTypeString, dataType);
                return dataType;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Could not create data type: " + dataTypeString, e);
            }
        }
    }

    @Override
    public T8DataType getObjectDataType(Object object)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DefinitionComposition getDefinitionComposition(T8Context context, String projectId, String definitionId, boolean includeDescendants) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        operationParameters.put(PARAMETER_INCLUDE_DESCENDANTS, includeDescendants);
        return (T8DefinitionComposition)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DEFINITION_COMPOSITION, operationParameters).get(PARAMETER_DEFINITION_COMPOSITION);
    }

    @Override
    public void deleteProjects(T8Context context, List<String> projectIds) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_IDS, projectIds);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_DELETE_PROJECTS, operationParameters);
    }

    @Override
    public void exportProjects(T8Context context, String fileContextIid, String filePath, List<String> projectIds) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_IDS, projectIds);
        operationParameters.put(PARAMETER_FILE_CONTEXT_IID, fileContextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_EXPORT_PROJECTS, operationParameters);
    }

    @Override
    public T8ProjectImportReport importProjects(T8Context context, String fileContextIid, String filePath, List<String> projectIds) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PROJECT_IDS, projectIds);
        operationParameters.put(PARAMETER_FILE_CONTEXT_IID, fileContextIid);
        operationParameters.put(PARAMETER_FILE_PATH, filePath);
        return (T8ProjectImportReport)T8MainServerClient.executeSynchronousOperation(context, OPERATION_IMPORT_PROJECTS, operationParameters).get(PARAMETER_PROJECT_IMPORT_REPORT);
    }
}
