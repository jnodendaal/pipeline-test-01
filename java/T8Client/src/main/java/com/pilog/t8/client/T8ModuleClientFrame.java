package com.pilog.t8.client;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SecurityManager.T8LoginResponseType;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8ClientSecurityManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ClientController;
import com.pilog.t8.ui.laf.T8LookAndFeelFactory;
import com.pilog.t8.utilities.codecs.Base64Codec;
import com.pilog.t8.utilities.encryption.ByteEncryptor;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import com.pilog.version.T8ClientVersion;
import java.awt.Dimension;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleClientFrame extends javax.swing.JFrame
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ModuleClientFrame.class);

    /** The key value for the operating system name property. */
    private static final String OS_PROPERTY = "os.name";

    private T8SessionContext sessionContext;
    private T8Context context;
    private T8ClientController flowClient;
    private String startupModuleId;
    private String startupUsername;
    private String startupPassword;

    /** Creates new form T8DeveloperFrame */
    public T8ModuleClientFrame()
    {
        try
        {
            T8ClientSecurityManager securityManager;
            Map<String, String> parameterMap;

            LOGGER.log("Starting T8ModuleClient Version " + T8ClientVersion.VERSION + "...");

            // Get the initialization parameters.
            parameterMap = getInitializationParameters();
            LOGGER.log("Parameter Map: " + parameterMap);
            startupModuleId = (String)parameterMap.get("MODULE_IDENTIFIER");
            startupUsername = (String)parameterMap.get("USERNAME");
            startupPassword = (String)parameterMap.get("PASSWORD");

            // Initialize the Swing components.
            initComponents();

            // Create a new Security Manager and Session Context.
            securityManager = new T8ClientSecurityManager(null);
            context = new T8Context((T8ClientContext)null, securityManager.createNewSessionContext());
            if (securityManager.login(context, T8SecurityManager.LoginLocation.CLIENT, startupUsername, startupPassword.toCharArray(), false).getResponseType() != T8LoginResponseType.FAILURE)
            {
                setPiLogLookAndFeel();

                // Set the frame size;
                this.setPreferredSize(new Dimension(800, 800));
                this.setMinimumSize(new Dimension(800, 800));
                this.setExtendedState(JFrame.MAXIMIZED_BOTH);

                // Instantiate a new client.
                flowClient = new T8ClientController(context);
                flowClient.initializeComponent(null);
                context = flowClient.getAccessContext();

                // Add the new Client to the frame.
                add(flowClient, java.awt.BorderLayout.CENTER);
                validate();
            }
            else throw new Exception("Invalid username or password.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while starting new T8Client.", e);
            throw new RuntimeException(e);
        }
    }

    private void setPiLogLookAndFeel()
    {
        LookAndFeel lookAndFeel;

        try
        {
            LOGGER.log(()->"JVM Set LAF : [" + UIManager.getLookAndFeel() + "] - System LAF : [" + UIManager.getSystemLookAndFeelClassName() + "] - Cross Platform LAF : [" + UIManager.getCrossPlatformLookAndFeelClassName() + "]");
            lookAndFeel = T8LookAndFeelFactory.getLookAndFeel(System.getProperty(OS_PROPERTY));

            UIManager.setLookAndFeel(lookAndFeel);
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (UnsupportedLookAndFeelException | RuntimeException e)
        {
            LOGGER.log("Error encountered while setting Look & Feel.", e);
        }
    }

    public void startup()
    {
        try
        {
            flowClient.startComponent();
            flowClient.loadModule(startupModuleId, null);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while starting main module.", e);
        }
    }

    public void shutdown()
    {
        try
        {
            LOGGER.log("Loggin out session '" + context.getSessionContext().getSessionIdentifier() + "'.");
            flowClient.stopComponent();
            flowClient.getSecurityManager().logout(context);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while trying to log out.", e);
        }
    }

    private Map<String, String> getInitializationParameters() throws Exception
    {
        String parameterString;

        parameterString = System.getProperty("INIT_DATA");
        if (parameterString != null)
        {
            Map<String, String> parameterMap;
            byte[] bytes;

            bytes = Base64Codec.decode(parameterString);
            bytes = ByteEncryptor.decryptBytes(bytes, "T8-MODULE-CLIENT");
            parameterMap = (Map<String, String>)SerializationUtilities.deserializeObject(bytes);

            return parameterMap;
        }
        else throw new Exception("Invalid initialization.");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tech 8 Developer");
        setName("Tech 8 Developer"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        startup();
    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        shutdown();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new T8ModuleClientFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
