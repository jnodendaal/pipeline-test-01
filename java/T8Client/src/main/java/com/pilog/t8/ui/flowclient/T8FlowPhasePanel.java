package com.pilog.t8.ui.flowclient;

import com.pilog.t8.T8Log;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.phase.T8FlowPhase;
import com.pilog.t8.flow.phase.T8FlowPhase.T8FlowPhaseStatus;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.animation.FillTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.ColumnConstraintsBuilder;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraintsBuilder;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class T8FlowPhasePanel extends JFXPanel
{
    private final MouseEventHandler mouseEventHandler;
    private final EventHandler buttonEventHandler;
    private final Map<String, T8FlowPhaseBlock> phaseBlocks;
    private T8FlowDetailsPane detailsPane;
    private T8FlowStatus flowStatus;
    private Text waitingText;
    private Scene scene;

    public T8FlowPhasePanel()
    {
        this.mouseEventHandler = new MouseEventHandler();
        this.buttonEventHandler = new ButtonEventHandler();
        this.phaseBlocks = new HashMap<String, T8FlowPhaseBlock>();
        this.setBackground(java.awt.Color.WHITE);
        Platform.setImplicitExit(false);
    }

    public void setFlowStatus(T8FlowStatus newFlowStatus)
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                if ((flowStatus != null) && (newFlowStatus != null) && (flowStatus.getFlowIid().equals(newFlowStatus.getFlowIid())))
                {
                    flowStatus = newFlowStatus;
                    updateScene(newFlowStatus);
                }
                else
                {
                    flowStatus = newFlowStatus;
                    constructScene(newFlowStatus);
                }
            }
        });
    }

    private void updateScene(T8FlowStatus flowStatus)
    {
        T8FlowPhase phaseModel;

        phaseModel = flowStatus.getPhases();
        T8Log.log("Updating Scene from Waiting Flow Phase Model: " + (phaseModel != null ? phaseModel.toStringRepresentation() : null));

        if (detailsPane != null)
        {
            detailsPane.update(flowStatus);
        }

        if (phaseModel != null)
        {
            for (T8FlowPhase phase : phaseModel.getSubsequentPhases())
            {
                T8FlowPhaseBlock phaseBlock;

                phaseBlock = phaseBlocks.get(phase.getIdentifier());
                if (phaseBlock != null)
                {
                    phaseBlock.update(phase);
                }
            }
        }
    }

    private void constructScene(T8FlowStatus flowStatus)
    {
        T8FlowPhase phaseModel;
        GridPane mainGrid;

        phaseModel = flowStatus != null ? flowStatus.getPhases() : null;
        T8Log.log("Contructing Scene from Waiting Flow Phase Model: " + (phaseModel != null ? phaseModel.toStringRepresentation() : null));
        phaseBlocks.clear();

        // Create the main grid.
        mainGrid = new GridPane();
        mainGrid.setBackground(Background.EMPTY);
        mainGrid.setAlignment(Pos.CENTER);

        if (flowStatus != null)
        {
            ScrollPane scrollPane;

            scrollPane = new ScrollPane();
            scrollPane.setContent(phaseModel != null ? createPhaseBox(phaseModel, false) : null);
            scrollPane.setFitToHeight(true);
            scrollPane.setFitToWidth(true);
            scrollPane.setPannable(true);
            scrollPane.setBackground(Background.EMPTY);
            scrollPane.setStyle("-fx-background:white;-fx-background-color:transparent;");

            detailsPane = createDetailsPane(flowStatus);
            mainGrid.add(detailsPane.getNode(), 0, 0);
            mainGrid.add(scrollPane, 0, 1);

            // Set row constraints.
            mainGrid.getRowConstraints().setAll
            (
                RowConstraintsBuilder.create().fillHeight(false).vgrow(Priority.NEVER).build(),
                RowConstraintsBuilder.create().fillHeight(true).vgrow(Priority.ALWAYS).build()
            );

            // Set column constraints.
            mainGrid.getColumnConstraints().setAll
            (
                ColumnConstraintsBuilder.create().fillWidth(true).hgrow(Priority.ALWAYS).build()
            );
        }
        else
        {
            detailsPane = null;
        }

        // Create the scene.
        scene = new Scene(mainGrid);
        setScene(scene);
    }

    private Node createPhaseBox(T8FlowPhase phaseModel, boolean addReflection)
    {
        List<List<T8FlowPhase>> phaseLists;
        GridPane container;
        HBox phaseHBox;
        int arrowIndex;
        int arrowCount;

        // Create vertical phase lists from the phase model.
        phaseLists = createPhaseLists(phaseModel);
        arrowIndex = 0;
        arrowCount = phaseLists.size() + 1;

        // Create the phase box and add the first arrow to it.
        phaseHBox = new HBox();
        phaseHBox.setSpacing(10.0f);
        phaseHBox.setAlignment(Pos.CENTER);
        phaseHBox.setBackground(Background.EMPTY);
        phaseHBox.getChildren().add(createArrow(arrowIndex++, arrowCount, false));

        if (addReflection)
        {
            Reflection reflection;

            reflection = new Reflection();
            phaseHBox.setEffect(reflection);
        }

        // Add all the phase lists to the layout.
        for (List<T8FlowPhase> phaseList : phaseLists)
        {
            VBox phaseVBox;

            phaseVBox = new VBox();
            phaseVBox.setSpacing(10.0f);
            phaseVBox.setAlignment(Pos.CENTER);
            phaseVBox.setBackground(Background.EMPTY);
            for (T8FlowPhase phase : phaseList)
            {
                T8FlowPhaseBlock phaseBlock;

                phaseBlock = createPhaseBlock(phase, 0.0f, 0.0f, false, false);
                phaseVBox.getChildren().add(phaseBlock.getNode());
                phaseBlocks.put(phase.getIdentifier(), phaseBlock);
            }

            phaseHBox.getChildren().add(phaseVBox);
            phaseHBox.getChildren().add(createArrow(arrowIndex++, arrowCount, false));
        }

        // Create the container.
        container = new GridPane();
        container.add(phaseHBox, 0, 0);
        container.setAlignment(Pos.CENTER);
        container.setBackground(Background.EMPTY);

        // Set row constraints.
        container.getRowConstraints().setAll
        (
            RowConstraintsBuilder.create().fillHeight(false).vgrow(Priority.ALWAYS).build()
        );

        // Set column constraints.
        container.getColumnConstraints().setAll
        (
            ColumnConstraintsBuilder.create().fillWidth(true).hgrow(Priority.ALWAYS).build()
        );

        return container;
    }

    private List<List<T8FlowPhase>> createPhaseLists(T8FlowPhase phaseModel)
    {
        List<List<T8FlowPhase>> phaseLists;
        List<T8FlowPhase> firstList;

        phaseLists = new ArrayList<List<T8FlowPhase>>();
        firstList = new ArrayList<T8FlowPhase>();
        firstList.add(phaseModel);
        phaseLists.add(firstList);

        while (true)
        {
            List<T8FlowPhase> currentList;
            List<T8FlowPhase> nextList;

            currentList = phaseLists.get(phaseLists.size() - 1);
            nextList = new ArrayList<T8FlowPhase>();
            for (T8FlowPhase currentPhase : currentList)
            {
                // Add all of the next phases to the list.
                for (T8FlowPhase nextPhase : currentPhase.getNextPhases())
                {
                    if (!nextList.contains(nextPhase)) nextList.add(nextPhase);
                }
            }

            // If any of the phases in the current list exist in the next list, remove them from the current list.
            currentList.removeAll(nextList);

            if (nextList.size() > 0)
            {
                phaseLists.add(nextList);
            }
            else
            {
                break;
            }
        }

        return phaseLists;
    }

    private T8FlowDetailsPane createDetailsPane(T8FlowStatus flowStatus)
    {
        ProgressIndicator progressIndicator;
        T8FlowDetailsPane newDetailsPane;
        Text headerText;
        Text labelText;
        Text valueText;
        VBox vBox;
        VBox detailBox;
        HBox detailFieldBox;

        // Create the pane object.
        newDetailsPane = new T8FlowDetailsPane();

        // Create header text.
        headerText = new Text(flowStatus.getFlowDisplayName() + " in Progress");
        headerText.setFont(Font.font("Tahoma", FontWeight.THIN, FontPosture.ITALIC, 40));

        // Create the details.
        detailBox = vBox = new VBox();

        // Add the initiator details.
        labelText = new Text("Initiator:");
        labelText.setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, 13));
        valueText = new Text(flowStatus.getInitiatorUserDisplayName());
        detailFieldBox = new HBox();
        detailFieldBox.setSpacing(5.0f);
        detailFieldBox.setAlignment(Pos.CENTER);
        detailFieldBox.getChildren().add(labelText);
        detailFieldBox.getChildren().add(valueText);
        detailBox.getChildren().add(detailFieldBox);
        newDetailsPane.setInitiatorText(valueText);

        // Add the start time details.
        labelText = new Text("Start Time:");
        labelText.setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, 13));
        valueText = new Text(new T8Timestamp(flowStatus.getStartTime()).toString());
        detailFieldBox = new HBox();
        detailFieldBox.setSpacing(5.0f);
        detailFieldBox.setAlignment(Pos.CENTER);
        detailFieldBox.getChildren().add(labelText);
        detailFieldBox.getChildren().add(valueText);
        detailBox.getChildren().add(detailFieldBox);
        newDetailsPane.setStartedTimeText(valueText);

        // Add the current phase details.
        labelText = new Text("Current Phases:");
        labelText.setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, 13));
        valueText = new Text(flowStatus.getCurrentPhaseDisplayString());
        detailFieldBox = new HBox();
        detailFieldBox.setSpacing(5.0f);
        detailFieldBox.setAlignment(Pos.CENTER);
        detailFieldBox.getChildren().add(labelText);
        detailFieldBox.getChildren().add(valueText);
        detailBox.getChildren().add(detailFieldBox);
        newDetailsPane.setCurrentPhasesText(valueText);

        // Add the current steps details.
        labelText = new Text("Current Steps:");
        labelText.setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, 13));
        valueText = new Text(Strings.truncate(flowStatus.getCurrentStepsDisplayString(), 100));
        detailFieldBox = new HBox();
        detailFieldBox.setSpacing(5.0f);
        detailFieldBox.setAlignment(Pos.CENTER);
        detailFieldBox.getChildren().add(labelText);
        detailFieldBox.getChildren().add(valueText);
        detailBox.getChildren().add(detailFieldBox);
        newDetailsPane.setCurrentStepsText(valueText);

        // Add the current responsible parties details.
        labelText = new Text("Current Responsible Parties:");
        labelText.setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, 13));
        valueText = new Text(Strings.truncate(flowStatus.getCurrentResponsiblePartiesDisplayString(), 100));
        detailFieldBox = new HBox();
        detailFieldBox.setSpacing(5.0f);
        detailFieldBox.setAlignment(Pos.CENTER);
        detailFieldBox.getChildren().add(labelText);
        detailFieldBox.getChildren().add(valueText);
        detailBox.getChildren().add(detailFieldBox);
        newDetailsPane.setCurrentResponsiblePartiesText(valueText);

        // Create waiting text and processing indicator.
        waitingText = new Text("Waiting for your next task to become available...");
        progressIndicator = new ProgressIndicator();
        progressIndicator.setProgress(-1.0f);

        // Create the vertical layout and add the components to it.
        vBox = new VBox();
        vBox.setSpacing(20.0f);
        vBox.setPadding(new Insets(60, 0, 20, 0));
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(headerText);
        vBox.getChildren().add(detailBox);
        vBox.getChildren().add(waitingText);
        vBox.getChildren().add(progressIndicator);
        newDetailsPane.setNode(vBox);
        return newDetailsPane;
    }

    private T8FlowPhaseBlock createPhaseBlock(T8FlowPhase phase, float x, float y, boolean addShadow, boolean addReflection)
    {
        T8FlowPhaseBlock phaseBlock;
        Reflection reflection;
        DropShadow shadow;
        Rectangle rect;
        VBox textBox;
        Text text;
        StackPane stack;

        // Create the phase block.
        phaseBlock = new T8FlowPhaseBlock(phase.getIdentifier());

        // Create the stack.
        stack = new StackPane();
        phaseBlock.setNode(stack);

        // Create the rectangle and add it to the stack.
        rect = new Rectangle();
        rect.setFill(phase.getStatus() == T8FlowPhaseStatus.IN_PROGRESS ? Color.BISQUE : Color.CADETBLUE);
        rect.setWidth(140);
        rect.setHeight(70);
        rect.setX(x);
        rect.setY(y);
        rect.setArcHeight(15.0f);
        rect.setArcWidth(15.0f);
        rect.addEventFilter(MouseEvent.MOUSE_ENTERED, mouseEventHandler);
        rect.addEventFilter(MouseEvent.MOUSE_EXITED, mouseEventHandler);
        stack.getChildren().add(rect);
        phaseBlock.setRectangle(rect);

        // Create the text and add it to the stack.
        textBox = new VBox();
        textBox.setAlignment(Pos.CENTER);
        text = new Text(phase.getDisplayName());
        text.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 16));
        textBox.getChildren().add(text);
        text = new Text(phase.getStatus().getDisplayName());
        text.setFont(Font.font("Tahoma", FontWeight.THIN, FontPosture.REGULAR, 13));
        textBox.getChildren().add(text);
        stack.getChildren().add(textBox);
        phaseBlock.setStatusText(text);

        // Create the reflection.
        if (addReflection)
        {
            reflection = new Reflection();
            reflection.setTopOffset(10.0);
        }
        else reflection = null;

        // Create the shadow effect and chain it to the reflection.
        if (addShadow)
        {
            shadow = new DropShadow();
            shadow.setOffsetY(5.0);
            shadow.setOffsetX(5.0);
            shadow.setColor(Color.GRAY);
        }
        else shadow = null;

        // Add the effects to the rectangle.
        if ((addShadow) && (addReflection))
        {
            shadow.setInput(reflection);
            stack.setEffect(shadow);
        }
        else if (addShadow)
        {
            stack.setEffect(shadow);
        }
        else if (addReflection)
        {
            stack.setEffect(reflection);
        }

        // Return the stack.
        return phaseBlock;
    }

    private Node createArrow(int index, int count, boolean addShadow)
    {
        SequentialTransition seqTransition;
        FillTransition fillTransition;
        Path arrow;

        // Create the arrow shape.
        arrow = new Path();
        arrow.getElements().add(new MoveTo(0, 0));
        arrow.getElements().add(new LineTo(20, 20));
        arrow.getElements().add(new LineTo(0, 40));
        arrow.getElements().add(new LineTo(0, 0));
        arrow.setStrokeWidth(1);
        arrow.setStroke(Color.BLACK);

        // Create the shadow effect.
        if (addShadow)
        {
            DropShadow shadow;

            shadow = new DropShadow();
            shadow.setOffsetY(5.0);
            shadow.setOffsetX(5.0);
            shadow.setColor(Color.GRAY);
            arrow.setEffect(shadow);
        }

        // Create a transition that will fill the arrow with green.
        fillTransition = new FillTransition(Duration.millis(1000), arrow, Color.GREEN, Color.WHITE);
        fillTransition.setCycleCount(1);
        fillTransition.setAutoReverse(true);

        // Create a sequence transition that will pause after playing the fill transition, so that we can fill the arrows one after the other.
        seqTransition = new SequentialTransition(fillTransition, new PauseTransition(Duration.millis(count * 1000)));
        seqTransition.setCycleCount(Timeline.INDEFINITE);
        seqTransition.setDelay(Duration.millis(index * 1000));
        seqTransition.play();

        // Return the arrow shape.
        return arrow;
    }

    private class MouseEventHandler implements EventHandler<MouseEvent>
    {
        @Override
        public void handle(MouseEvent t)
        {
            Rectangle rect;

            rect = (Rectangle)t.getSource();
            if (rect.contains(t.getX(), t.getY()))
            {
                rect.setFill(Color.BISQUE);
            }
            else
            {
                rect.setFill(Color.CADETBLUE);
            }
        }
    }

    private class ButtonEventHandler implements EventHandler<ActionEvent>
    {
        @Override
        public void handle(ActionEvent t)
        {
            System.out.println("Button Pressed");
        }
    }
}
