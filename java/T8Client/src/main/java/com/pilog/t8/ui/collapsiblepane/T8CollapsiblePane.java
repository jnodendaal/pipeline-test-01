package com.pilog.t8.ui.collapsiblepane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.collapsiblepane.T8CollapsiblePaneDefinition;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Map;
import org.jdesktop.swingx.JXCollapsiblePane;

/**
 * @author Bouwer du Preez
 */
public class T8CollapsiblePane extends JXCollapsiblePane implements T8Component
{
    private final T8CollapsiblePaneDefinition definition;
    private final T8ComponentController controller;
    private T8Component childComponent;

    public T8CollapsiblePane(T8CollapsiblePaneDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.childComponent = null;
        this.setAnimated(definition.isAnimated());
        this.setOpaque(definition.isOpaque());
        this.setLayout(new BorderLayout());
        this.setCollapsed(definition.isCollapsed());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        removeAll();
        childComponent = component;
        add(java.awt.BorderLayout.CENTER, (Component)childComponent);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return childComponent;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        if (childComponent != null)
        {
            if (childComponent.getComponentDefinition().getIdentifier().equals(identifier))
            {
                T8Component removedComponent;

                removedComponent = childComponent;
                removeAll();
                childComponent = null;
                return removedComponent;
            }
            else return null;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<T8Component>();
        if (childComponent != null) childComponents.add(childComponent);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponent != null ? 1 : 0;
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponent = null;
    }
}
