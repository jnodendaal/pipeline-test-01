package com.pilog.t8.ui.datacombobox;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxOperationHandler extends T8ComponentOperationHandler
{
    private T8DataComboBox comboBox;

    public T8DataComboBoxOperationHandler(T8DataComboBox comboBox)
    {
        super(comboBox);
        this.comboBox = comboBox;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DataComboBoxAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8DataComboBoxAPIHandler.PARAMETER_DATA_ENTITY, comboBox.getSelectedDataEntity());
        }
        else if (operationIdentifier.equals(T8DataComboBoxAPIHandler.OPERATION_SET_PREFILTER))
        {
            T8DataFilter prefilter;
            String filterIdentifier;
            Boolean refresh;
            
            prefilter = (T8DataFilter)operationParameters.get(T8DataComboBoxAPIHandler.PARAMETER_DATA_FILTER);
            filterIdentifier = (String)operationParameters.get(T8DataComboBoxAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            refresh = (Boolean)operationParameters.get(T8DataComboBoxAPIHandler.PARAMETER_REFRESH_DATA);
            if (refresh == null) refresh = true;
            
            comboBox.setPrefilter(filterIdentifier, prefilter);
            if (refresh) comboBox.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(T8DataComboBoxAPIHandler.OPERATION_SET_SELECTED_DATA_ENTITY))
        {
            Map<String, Object> keyMap;
            
            keyMap = (Map<String, Object>)operationParameters.get(T8DataComboBoxAPIHandler.PARAMETER_DATA_VALUE_MAP);
            comboBox.setSelectedKey(keyMap);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
