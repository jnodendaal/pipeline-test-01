package com.pilog.t8.ui.componentcontainer.widget;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.componentcontainer.widget.T8WidgetContainerDefinition;
import com.pilog.t8.definition.ui.module.widget.T8WidgetModuleDefinition;
import com.pilog.t8.functionality.view.T8ModuleFunctionalityView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.columnpane.T8ColumnPane;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Phaser;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8WidgetContainer extends T8ColumnPane implements T8Component, T8FunctionalityController
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8WidgetContainer.class);

    private final T8WidgetContainerDefinition definition;
    private final T8FunctionalityManager functionalityManager;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8WidgetContainerOperationHandler operationHandler;
    private final List<T8Widget> widgets;
    private final T8ConfigurationManager configurationManager;
    private final Phaser widgetLoaderPhaser;
    private int lastColumnContainer;

    public T8WidgetContainer(T8WidgetContainerDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.functionalityManager = clientContext.getFunctionalityManager();
        this.operationHandler = new T8WidgetContainerOperationHandler(this);
        this.widgets = Collections.synchronizedList(new ArrayList<T8Widget>());
        this.lastColumnContainer = -1;
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.widgetLoaderPhaser = new Phaser();
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationId, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        super.initializeComponent(inputParameters);
    }

    @Override
    public void startComponent()
    {
        super.startComponent();
        try
        {
            loadWidgets(getInputParameters());
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to load widgets", ex);
        }
    }

    @Override
    public void stopComponent()
    {
        widgetLoaderPhaser.register();

        //Make sure all widget loaders are complete before stopping this component
        widgetLoaderPhaser.arriveAndAwaitAdvance();

        for (T8Widget widget : widgets)
        {
            widget.closeWidget();
        }

        widgets.clear();
        super.stopComponent();
    }

    @Override
    public T8ComponentController getFunctionalityComponentController()
    {
        return getController();
    }

    @Override
    public T8FunctionalityView getFunctionalityView(String functionalityInstanceIdentifier)
    {
        return null;
    }

    public void addFunctionalityView(T8FunctionalityView view)
    {
    }

    @Override
    public T8FunctionalityAccessHandle accessFunctionality(String functionalityId, Map<String, Object> parameters)
    {
        return null;
    }

    private synchronized JPanel getNextColumnContainer()
    {
        lastColumnContainer++;
        if (lastColumnContainer >= getColumnContainers().size())
        {
            lastColumnContainer = 0;
        }
        return getColumnContainers().get(lastColumnContainer);
    }

    private void loadWidgets(Map<String, Object> inputParametersMap) throws Exception
    {
        String functionalityGroupId;
        T8FunctionalityGroupHandle functionalityGroup;

        functionalityGroupId = definition.getFuntionalityGroupIdentifier();
        functionalityGroup = functionalityManager.getFunctionalityGroupHandle(context, functionalityGroupId, null, null);

        if (functionalityGroup.containsAvailableFunctionality())
        {
            for (T8FunctionalityHandle functionality : functionalityGroup.getFunctionalities())
            {
                new WidgetLoader(functionality, inputParametersMap).execute();
            }
        }
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    private class WidgetLoader extends SwingWorker<T8FunctionalityView, Void>
    {
        private T8FunctionalityHandle functionalityHandle;
        private Map<String, Object> inputParameters;
        private JPanel columnContainer;

        public WidgetLoader(T8FunctionalityHandle functionalityHandle,
                            Map<String, Object> inputParameters)
        {
            this.functionalityHandle = functionalityHandle;
            this.inputParameters = inputParameters;
        }

        public WidgetLoader(JPanel columnContainer,
                            T8FunctionalityHandle functionalityHandle,
                            Map<String, Object> inputParameters)
        {
            this(functionalityHandle, inputParameters);
            this.columnContainer = columnContainer;
        }

        @Override
        protected T8FunctionalityView doInBackground() throws Exception
        {
            try
            {
                widgetLoaderPhaser.register();
                T8FunctionalityView functionalityView;
                T8FunctionalityAccessHandle functionalityExecutionHandle;

                functionalityExecutionHandle = functionalityManager.accessFunctionality(context, functionalityHandle.getId(), functionalityHandle.getPublicParameters());
                functionalityView = functionalityExecutionHandle.createViewComponent(getController());
                functionalityView.initialize();

                return functionalityView;
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to to load functionality " + functionalityHandle.getId(), ex);
            }
            return null;
        }

        @Override
        protected void done()
        {
            T8Widget widget;
            T8WidgetModuleDefinition widgetDefinition;
            T8FunctionalityView moduleFunctionalityView;

            try
            {
                moduleFunctionalityView = get();
                widget = new T8Widget((T8ModuleFunctionalityView) moduleFunctionalityView, inputParameters);

                widgetDefinition = widget.getDefinition();

                if(widgetDefinition.getPreferredWidth() != 0 && widgetDefinition.getPreferredHeight() != 0)
                    widget.setPreferredSize(new Dimension(widgetDefinition.getPreferredWidth(), widgetDefinition.getPreferredHeight()));

                if (widgetDefinition.getTitleBackgroundPainterIdentifier() != null)
                {
                    T8Painter painter;

                    painter = widgetDefinition.getTitleBackgroundPainterDefinition().getNewPainterInstance();
                    widget.setTitleBackgroundPainter(new T8PainterAdapter(painter));
                }
                if (widgetDefinition.getTitleForegroundColor() != null)
                {
                    widget.setTitleTextForeground(widgetDefinition.getTitleForegroundColor());
                }

                widgets.add(widget);

                GridBagConstraints gridConstraints;
                JPanel columnContainer;

                columnContainer = this.columnContainer == null ? getNextColumnContainer() : this.columnContainer;
                gridConstraints = new GridBagConstraints();
                gridConstraints.anchor = GridBagConstraints.NORTH;
                gridConstraints.fill = GridBagConstraints.HORIZONTAL;
                gridConstraints.weightx = 0.1;
                gridConstraints.weighty = 0;
                gridConstraints.gridx = 0;
                gridConstraints.gridy = columnContainer.getComponentCount();
                gridConstraints.insets = new Insets(getComponentTopInset(), 0, getComponentBottomInset(), 0);
                columnContainer.add(widget, gridConstraints);

                moduleFunctionalityView.startComponent();

                columnContainer.revalidate();
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to to load widget module " + functionalityHandle.getId(), ex);
            }
            finally
            {
                widgetLoaderPhaser.arriveAndDeregister();
            }
        }
    }
}
