package com.pilog.t8.flow;

import com.pilog.t8.T8FlowManager;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFlowManagerScriptFacade
{
    private final T8FlowManager flowManager;
    private final T8Context context;

    public T8ClientFlowManagerScriptFacade(T8Context context, T8FlowManager flowManager)
    {
        this.flowManager = flowManager;
        this.context = context;
    }

    public void claimTask(String taskIid) throws Exception
    {
        flowManager.claimTask(context, taskIid);
    }

    public T8TaskCompletionResult completeTask(String taskIid, Map<String, Object> taskOutputParameters) throws Exception
    {
        return flowManager.completeTask(context, taskIid, taskOutputParameters);
    }

    public int getTaskCount(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskCount(context, taskFilter);
    }

    public T8TaskListSummary getTaskListSummary(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskListSummary(context, taskFilter);
    }

    public T8TaskList getTaskList(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskList(context, taskFilter);
    }

    public T8TaskDetails getTaskDetails(String taskIid) throws Exception
    {
        return flowManager.getTaskDetails(context, taskIid);
    }

    public void reassignTask(String taskIid, String userIdentifier, String userProfileIdentifier) throws Exception
    {
        flowManager.reassignTask(context, taskIid, userIdentifier, userProfileIdentifier);
    }

    public void decreaseTaskPriority(String taskIid, int amount) throws Exception
    {
        flowManager.decreaseTaskPriority(context, taskIid, amount);
    }

    public void increaseTaskPriority(String taskIid, int amount) throws Exception
    {
        flowManager.increaseTaskPriority(context, taskIid, amount);
    }
}
