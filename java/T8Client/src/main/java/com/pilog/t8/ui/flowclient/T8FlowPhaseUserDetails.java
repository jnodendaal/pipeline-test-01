package com.pilog.t8.ui.flowclient;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhaseUserDetails implements Serializable
{
    private final String identifier;
    
    public T8FlowPhaseUserDetails(String identifier)
    {
        this.identifier = identifier;
    }
    
    public String getIdentifier()
    {
        return identifier;
    }
}
