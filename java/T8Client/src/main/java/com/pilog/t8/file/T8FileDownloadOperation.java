package com.pilog.t8.file;

import com.pilog.t8.security.T8Context;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author Bouwer du Preez
 */
public class T8FileDownloadOperation
{
    private final T8ClientFileManager client;
    private final T8Context context;
    private final String contextID;
    private final String localFilePath;
    private final String remoteFilePath;
    private long fileSize;
    private long bytesDownloaded;
    private boolean stopFlag;

    private static final int FILE_SPLICE_SIZE = (1024*1024); // The number of bytes that will be uploaded to the server at a time.

    public T8FileDownloadOperation(T8Context context, T8ClientFileManager client, String contextID, String localFilePath, String remoteFilePath)
    {
        this.contextID = contextID;
        this.context = context;
        this.localFilePath = localFilePath;
        this.remoteFilePath = remoteFilePath;
        this.client = client;
        this.stopFlag = false;
    }

    public void start() throws Exception, FileNotFoundException
    {
        OutputStream outputStream = null;
        File targetFile;

        targetFile = new File(localFilePath);

        try
        {
            byte[] fileData;
            long offset;

            offset = 0;
            bytesDownloaded = 0;
            fileSize = client.getFileSize(context, contextID, remoteFilePath);
            outputStream = new BufferedOutputStream(new FileOutputStream(targetFile));
            while ((fileData = client.downloadFileData(context, contextID, remoteFilePath, offset, FILE_SPLICE_SIZE)).length > 0)
            {
                outputStream.write(fileData);
                offset += FILE_SPLICE_SIZE;
                bytesDownloaded += fileData.length;
            }
        }
        finally
        {
            if (outputStream != null) outputStream.close();
        }
    }

    public void stop()
    {
        stopFlag = true;
    }

    public long getByteSizeDownloaded()
    {
        return bytesDownloaded;
    }

    public long getTotalByteSize()
    {
        return fileSize;
    }

    public int getProgress()
    {
        return (int)(((float)bytesDownloaded) / ((float)fileSize) * 100.00);
    }
}
