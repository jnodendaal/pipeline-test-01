package com.pilog.t8.ui.componentcontroller;

import com.pilog.t8.script.T8ClientContextScript;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ScriptExecution implements Execution
{
    private final T8ClientContextScript script;
    private final Map<String, ? extends Object> inputParameters;
    private Map<String, Object> outputParameters;
    private boolean completed;

    public ScriptExecution(T8ClientContextScript script, Map<String, ? extends Object> inputParameters)
    {
        this.script = script;
        this.inputParameters = inputParameters;
        this.completed = false;
    }

    public synchronized boolean hasCompleted()
    {
        return completed;
    }

    public Map<String, Object> getOutputParameters()
    {
        return outputParameters;
    }

    @Override
    public void execute()
    {
        try
        {
            // Execute the script and store the output parameters.
            outputParameters = script.executeScript(inputParameters);
            completed = true;

            // Notify all waiting threads that execution has completed.
            synchronized (this)
            {
                this.notifyAll();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Controller script execution exception.", e);
        }
    }
}
