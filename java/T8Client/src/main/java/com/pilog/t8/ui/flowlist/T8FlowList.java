package com.pilog.t8.ui.flowlist;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.flowlist.T8FlowListDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

/**
 * @author Bouwer du Preez
 */
public class T8FlowList extends JPanel implements T8Component
{
    private T8FlowListDefinition definition;
    private T8ComponentController controller;
    private JXTaskPane taskPane;
    private JXTaskPaneContainer taskPaneContainer;

    public T8FlowList(T8FlowListDefinition definition, T8ComponentController controller)
    {
        initComponents();
        this.definition = definition;
        this.controller = controller;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        taskPane = new JXTaskPane("Item Creation");
        taskPane.setOpaque(false);
        taskPane.add(new JLabel("No unfinished processes."));
        taskPaneContainer = new JXTaskPaneContainer();
        taskPaneContainer.add(taskPane);
        jScrollPaneFlowList.setViewportView(taskPaneContainer);
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
    }
    
    @Override
    public void stopComponent()
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jScrollPaneFlowList = new javax.swing.JScrollPane();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setToolTipText("Refresh");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBarMain.add(jButtonRefresh);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
        add(jScrollPaneFlowList, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JScrollPane jScrollPaneFlowList;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables

}
