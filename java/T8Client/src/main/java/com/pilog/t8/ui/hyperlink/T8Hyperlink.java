package com.pilog.t8.ui.hyperlink;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.hyperlink.T8HyperlinkAPIHandler;
import com.pilog.t8.definition.ui.hyperlink.T8HyperlinkDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.jdesktop.swingx.JXHyperlink;

/**
 * @author Bouwer du Preez
 */
public class T8Hyperlink extends JXHyperlink implements T8Component
{
    private static final T8Logger logger = T8Log.getLogger(T8Hyperlink.class);

    private final T8HyperlinkDefinition definition;
    private final T8ComponentController controller;
    private final T8HyperlinkOperationHandler operationHandler;

    public T8Hyperlink(T8HyperlinkDefinition linkDefinition, T8ComponentController controller)
    {
        this.definition = linkDefinition;
        this.controller = controller;
        this.operationHandler = new T8HyperlinkOperationHandler(this);
        this.addActionListener(new HyperlinkActionListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        String uri;

        // Set the URI of the link if specified.
        uri = definition.getURI();
        if (!Strings.isNullOrEmpty(uri))
        {
            try
            {
                setURI(new URI(definition.getURI()));
            }
            catch (Exception e)
            {
                logger.log("Exception while setting URI on Hyperlinkg: " + uri, e);
            }
        }

        // Set the text and tooltip.
        setText(definition.getText());
        setToolTipText(definition.getTooltipText());
    }

    public void setURI(String uri) throws URISyntaxException
    {
        if (Strings.isNullOrEmpty(uri))
        {
            setURI((URI)null);
        }
        else
        {
            setURI(new URI(uri));
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private class HyperlinkActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            controller.reportEvent(T8Hyperlink.this, definition.getComponentEventDefinition(T8HyperlinkAPIHandler.EVENT_LINK_CLICKED), eventParameters);
        }
    }
}
