/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.gfx;

import java.awt.Color;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;

/**
 *
 * @author Bouwer.duPreez
 */
public class T8GraphicsUtilities
{
    public static LinearGradientPaint createDiagonalLinearGradient(Color startColor, Color endColor)
    {
        float[] fractions;
        Color[] colors;
        
        fractions = new float[2];
        fractions[0] = 0.0f;
        fractions[1] = 1.0f;
        
        colors = new Color[2];
        colors[0] = startColor;
        colors[1] = endColor;
        
        return new LinearGradientPaint(0.0f, 0.0f, 1.0f, 1.0f, fractions, colors, MultipleGradientPaint.CycleMethod.NO_CYCLE);
    }    
    
    public static LinearGradientPaint createTopToBottomLinearGradient(Color startColor, Color endColor)
    {
        float[] fractions;
        Color[] colors;
        
        fractions = new float[2];
        fractions[0] = 0.0f;
        fractions[1] = 1.0f;
        
        colors = new Color[2];
        colors[0] = startColor;
        colors[1] = endColor;
        
        return new LinearGradientPaint(0.0f, 0.0f, 0.0f, 1.0f, fractions, colors, MultipleGradientPaint.CycleMethod.NO_CYCLE);
    }  
    
    public static LinearGradientPaint createLeftToRightLinearGradient(Color startColor, Color endColor)
    {
        float[] fractions;
        Color[] colors;
        
        fractions = new float[2];
        fractions[0] = 0.0f;
        fractions[1] = 1.0f;
        
        colors = new Color[2];
        colors[0] = startColor;
        colors[1] = endColor;
        
        return new LinearGradientPaint(0.0f, 0.0f, 1.0f, 0.0f, fractions, colors, MultipleGradientPaint.CycleMethod.NO_CYCLE);
    }  
}
