package com.pilog.t8.ui.combobox;

import com.pilog.t8.definition.ui.combobox.T8ComboBoxAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComboBoxOperationHandler extends T8ComponentOperationHandler
{
    private T8ComboBox comboBox;

    public T8ComboBoxOperationHandler(T8ComboBox comboBox)
    {
        super(comboBox);
        this.comboBox = comboBox;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8ComboBoxAPIHandler.OPERATION_GET_SELECTED_DATA_VALUE))
        {
            return HashMaps.newHashMap(T8ComboBoxAPIHandler.PARAMETER_DATA_VALUE, comboBox.getSelectedDataValue());
        }
        else if (operationIdentifier.equals(T8ComboBoxAPIHandler.OPERATION_SET_SELECTED_DATA_VALUE))
        {
            Object dataValue;

            dataValue = operationParameters.get(T8ComboBoxAPIHandler.PARAMETER_DATA_VALUE);
            comboBox.setSelectedDataValue(dataValue);
            return null;
        }
        else if (operationIdentifier.equals(T8ComboBoxAPIHandler.OPERATION_ADD_ELEMENT))
        {
            String identifier;
            String displayName;
            Object dataValue;

            identifier = (String)operationParameters.get(T8ComboBoxAPIHandler.PARAMETER_ELEMENT_IDENTIFIER);
            displayName = (String)operationParameters.get(T8ComboBoxAPIHandler.PARAMETER_ELEMENT_NAME);
            dataValue = operationParameters.get(T8ComboBoxAPIHandler.PARAMETER_DATA_VALUE);
            comboBox.addElement(identifier, displayName, dataValue);
            return null;
        }
        else if (operationIdentifier.equals(T8ComboBoxAPIHandler.OPERATION_REMOVE_ELEMENT))
        {
            String identifier;

            identifier = (String)operationParameters.get(T8ComboBoxAPIHandler.PARAMETER_ELEMENT_IDENTIFIER);
            comboBox.removeElement(identifier);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
