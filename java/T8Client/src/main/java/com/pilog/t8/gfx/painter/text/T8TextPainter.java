package com.pilog.t8.gfx.painter.text;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.text.T8TextPainterDefinition;
import com.pilog.t8.gfx.painter.T8PainterConfigurator;
import org.jdesktop.swingx.painter.TextPainter;

/**
 * @author Bouwer du Preez
 */
public class T8TextPainter extends TextPainter implements T8Painter
{
    private T8TextPainterDefinition definition;
    
    public T8TextPainter(T8TextPainterDefinition definition)
    {
        super(definition.getText(), definition.getFontDefinition().getNewFontInstance());
        this.definition = definition;
        setupPainter();
    }
    
    private void setupPainter()
    {
        // Configure this painter's super type properties.
        T8PainterConfigurator.setupAbstractAreaPainter(this, definition);
    }
}
