package com.pilog.t8.ui.chart.bar;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.chart.bar.T83DBarChartDefinition;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberTickUnitSource;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * @author Bouwer du Preez
 */
public class T83DBarChart extends ChartPanel implements T8Component
{
    private final T83DBarChartOperationHandler operationHandler;
    private final T83DBarChartDefinition definition;
    private final T8ComponentController controller;
    private JFreeChart chart;

    public T83DBarChart(T83DBarChartDefinition definition, T8ComponentController controller)
    {
        super(null);
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T83DBarChartOperationHandler(this);
        this.setOpaque(definition.isOpaque());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void setDataList(List<List<Object>> dataList)
    {
        DefaultCategoryDataset dataSet;

        T8Log.log("Setting Bar Data List: " + dataList);
        dataSet = new DefaultCategoryDataset();
        for (List<Object> valueList : dataList)
        {
            dataSet.setValue((Number)valueList.get(0), (Comparable)valueList.get(1), (Comparable)valueList.get(2));
        }

        setDataSet(dataSet);
    }

    public void setDataSet(CategoryDataset dataset)
    {
        Plot plot;
        List<T8PaintDefinition> chartSeriesPaintDefinitions;

        // Create the new chart.
        chart = ChartFactory.createBarChart(
        definition.getTitle(), // Chart name.
        definition.getXAxisLabel(), // X axis label.
        definition.getYAxisLabel(), // Y axis label.
        dataset, // Dataset.
        PlotOrientation.VERTICAL, definition.isLegendVisible(), true, false);

        // Make the chart background translucent.
        chart.setBackgroundImageAlpha(0.0f);
        chart.setBackgroundPaint(new Color(255, 255, 255, 0));
        chart.getCategoryPlot().getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        chart.getCategoryPlot().setRangeGridlinePaint(Color.GRAY);

        chartSeriesPaintDefinitions = definition.getChartSeriesPaintDefinitions();
        if(chartSeriesPaintDefinitions != null && !chartSeriesPaintDefinitions.isEmpty())
        {
            int index = 0;
            for (T8PaintDefinition t8PaintDefinition : chartSeriesPaintDefinitions)
            {
                chart.getCategoryPlot().getRenderer().setSeriesPaint(index++, t8PaintDefinition.getNewPaintInstance());
            }
        }

        // Set limiting settings. Keep the auto-range
        applyRangeSettings();

        // Set the plot configuration.
        plot = chart.getPlot();
        plot.setOutlineVisible(definition.isOutlineVisible());
        plot.setBackgroundAlpha(0.0f);
        plot.setForegroundAlpha(0.5f);

        setChart(chart);
    }

    /**
     * Applies range specific settings and limitations as specified within the
     * definition. These settings are rather useful for manipulating the
     * automatic range creation on the chart, especially in an instance where
     * the data set for the chart contains "unintentional" data.
     */
    private void applyRangeSettings()
    {
        ValueAxis rangeAxis;
        Integer value;

        // We will only be working with the range axis
        rangeAxis = this.chart.getCategoryPlot().getRangeAxis();

        if (this.definition.isRangeIntegerValuesOnly()) rangeAxis.setStandardTickUnits(new NumberTickUnitSource(true));

        value = this.definition.getMinimumRangeSize();
        if (value != null) rangeAxis.setAutoRangeMinimumSize(value);

        value = this.definition.getRangeLowerBound();
        if (value != null) rangeAxis.setLowerBound(value);

        value = this.definition.getRangeUpperBound();
        if (value != null) rangeAxis.setUpperBound(value);
    }
}
