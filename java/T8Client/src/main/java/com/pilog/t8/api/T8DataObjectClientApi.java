package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataObjectApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_DATA_OBJECT_CLIENT";

    public T8DataObjectClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8DataObjectClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public T8DataObject retrieve(String objectId, String objectIid, boolean includeState) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_OBJECT_ID, objectId);
        inputParameters.put(PARAMETER_OBJECT_IID, objectIid);
        inputParameters.put(PARAMETER_INCLUDE_STATE, includeState);
        return (T8DataObject)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_OBJ_RETRIEVE, inputParameters).get(PARAMETER_OBJECT);
    }

    public <O extends T8DataObject> List<O> search(String objectId, T8ObjectFilter filter, String searchExpression, int pageOffset, int pageSize, boolean includeState) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_OBJECT_ID, objectId);
        inputParameters.put(PARAMETER_OBJECT_FILTER, filter);
        inputParameters.put(PARAMETER_SEARCH_EXPRESSION, searchExpression);
        inputParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        inputParameters.put(PARAMETER_PAGE_SIZE, pageSize);
        inputParameters.put(PARAMETER_INCLUDE_STATE, includeState);
        return (List<O>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_OBJ_SEARCH, inputParameters).get(PARAMETER_OBJECTS);
    }

    public <O extends T8DataObject> List<O> searchGroup(String groupId, T8ObjectFilter filter, String searchExpression, int pageOffset, int pageSize, boolean includeState) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_GROUP_ID, groupId);
        inputParameters.put(PARAMETER_OBJECT_FILTER, filter);
        inputParameters.put(PARAMETER_SEARCH_EXPRESSION, searchExpression);
        inputParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        inputParameters.put(PARAMETER_PAGE_SIZE, pageSize);
        inputParameters.put(PARAMETER_INCLUDE_STATE, includeState);
        return (List<O>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_OBJ_SEARCH_GROUP, inputParameters).get(PARAMETER_OBJECTS);
    }

    public T8DataObjectState retrieveState(String dataObjectIid) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void saveState(String dataObjectId, String dataObjectIid, String stateId) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
