package com.pilog.t8.ui.submodule;

import com.pilog.t8.definition.ui.submodule.T8SubModuleAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SubModuleOperationHandler extends T8ComponentOperationHandler
{
    private T8SubModule subModule;

    public T8SubModuleOperationHandler(T8SubModule subModule)
    {
        super(subModule);
        this.subModule = subModule;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8SubModuleAPIHandler.OPERATION_RELOAD_MODULE))
        {
            subModule.reloadModule();
            return null;
        }
        else if (operationIdentifier.equals(T8SubModuleAPIHandler.OPERATION_UNLOAD_MODULE))
        {
            subModule.unloadModule();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
