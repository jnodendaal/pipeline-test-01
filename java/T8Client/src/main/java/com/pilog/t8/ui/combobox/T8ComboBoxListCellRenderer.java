package com.pilog.t8.ui.combobox;

import com.pilog.t8.definition.ui.combobox.T8ComboBoxDefinition;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8ComboBoxListCellRenderer implements ListCellRenderer
{
    private final T8ComboBoxDefinition definition;
    private final DefaultListCellRenderer defaultRenderer;

    public T8ComboBoxListCellRenderer(T8ComboBoxDefinition definition)
    {
        this.definition = definition;
        this.defaultRenderer = new DefaultListCellRenderer();
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        if (value != null)
        {
            T8ComboBoxElement item;
            Object displayValue;

            item = (T8ComboBoxElement)value;
            displayValue = item.getDisplayName();
            return defaultRenderer.getListCellRendererComponent(list, displayValue, index, isSelected, cellHasFocus);
        }
        else return defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
}
