package com.pilog.t8.ui.processlist;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8CategorizedProgressReport;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.GridBagConstraints;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultCaret;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class ProcessPanel extends JXPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(ProcessPanel.class);

    private T8ClientContext clientContext;
    private T8Context context;
    private T8ProcessListView parentTaskList;
    private T8ProcessDetails processDetails;
    private DateFormat dateFormat;
    private boolean stopped = false;
    private long categoryStartTime;
    private double lastCategoryValue;

    private static final String DEFAULT_DATE_FORMAT = "HH:mm:ss dd/MM/yyyy";

    public ProcessPanel(T8ClientContext clientContext, T8Context context, T8ProcessDetails processDetails)
    {
        this.clientContext = clientContext;
        this.context = context;
        initComponents();
        // This will prevent the scroll pane from "jumping" by trying to move to the caret everytime the text updates
        ((DefaultCaret)this.jTextAreaDescriptionValue.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        ((DefaultCaret)this.jTextAreaProgressMessage.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        ((DefaultCaret)this.jTextAreaProgCatMessage.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        this.dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        this.jButtonClose.setVisible(false);
        this.jPanelProgressBarCategory.setVisible(false);
        this.jLabelProgressTitleDisp.setVisible(false);
        this.jTextAreaProgressMessage.setVisible(false);
        this.jLabelName.setVisible(false);
        this.jLabelNameValue.setVisible(false);
        this.setProcessDetails(processDetails);
    }

    public ProcessPanel(T8ProcessListView parentTaskList, T8ClientContext clientContext, T8Context context, T8ProcessDetails processDetails)
    {
        this(clientContext, context, processDetails);
        this.parentTaskList = parentTaskList;
        this.jButtonClose.setVisible(true);
        this.jLabelName.setVisible(true);
        this.jLabelNameValue.setVisible(true);
    }

    public String getProcessInstanceIdentifier()
    {
        return processDetails.getProcessIid();
    }

    public T8ProcessDetails getProcessDetails()
    {
        return processDetails;
    }

    public void setControlsVisible(boolean visible)
    {
        jXPanelControls.setVisible(visible);
    }

    public final void setProcessDetails(T8ProcessDetails processDetails)
    {
        T8ProgressReport progressReport;
        String processDisplayName;
        String processDescription;
        String processIdentifier;
        String failureMessage;
        long startTime;
        long endTime;

        // We need to translate these display Strings, since they will always be returned by the server in the default language.
        processDisplayName = translate(processDetails.getName());
        processDescription = translate(processDetails.getProcessDescription());
        processIdentifier = processDetails.getProcessId();
        failureMessage = processDetails.getFailureMessage();
        startTime = processDetails.getStartTime();
        endTime = processDetails.getEndTime();

        this.jButtonStop.setEnabled(stopped ? false : processDetails.isExecuting());
        this.jButtonClose.setEnabled(!processDetails.isExecuting());
        this.jLabelStatusValue.setText(processDetails.getStatus().toString());
        this.jLabelStartTimeValue.setText(startTime > 0 ? dateFormat.format(new Date(startTime)) : null);
        this.jLabelEndTimeValue.setText(endTime > 0 ? dateFormat.format(new Date(endTime)) : null);
        this.jLabelEndTimeValue.setVisible(endTime > 0);
        this.jLabelEndTime.setVisible(endTime > 0);

        this.jLabeElapsedTimeValue.setText(buildTimeString(System.currentTimeMillis() - processDetails.getStartTime()));
        this.jLabeElapsedTimeValue.setVisible(endTime < 1);
        this.jLabelElapsedTime.setVisible(endTime < 1);

        this.jLabelStatusMessage.setVisible(failureMessage != null);
        this.jLabelStatusValueMessage.setVisible(failureMessage != null);
        this.jLabelStatusValueMessage.setText("<html>" + failureMessage + "</html>");

        this.jLabelInitiatedByValue.setVisible(!Strings.isNullOrEmpty(processDetails.getInitiatorName()));
        this.jLabelInitiatedBy.setVisible(!Strings.isNullOrEmpty(processDetails.getInitiatorName()));
        this.jLabelInitiatedByValue.setText("<html>" + processDetails.getInitiatorName()+ "</html>");

        if (this.jProgressBar.getValue() != (int)processDetails.getProgress())
        {
            this.jProgressBar.setValue((int)processDetails.getProgress());
            long runningTime = System.currentTimeMillis() - startTime ;
            long timeLeft = (long) ((runningTime / processDetails.getProgress()) * (100 - processDetails.getProgress()));

            String buildTimeString = buildTimeString(timeLeft);
            this.jProgressBar.setString((int)processDetails.getProgress() + "%" + (buildTimeString.length() > 0 ? " - " : "") + buildTimeString);
        }

        // Get the progress report and set the UI according to the type of report we got.
        progressReport = processDetails.getProgressReport();
        if (progressReport instanceof T8CategorizedProgressReport)
        {
            T8CategorizedProgressReport categorizedProgressReport;

            categorizedProgressReport = (T8CategorizedProgressReport)progressReport;
            this.jPanelProgressBarCategory.setVisible(true);
            this.jLabelProgCatTitleDisp.setVisible(categorizedProgressReport.getCategoryTitle() != null);
            this.jLabelProgCatTitleDisp.setText(categorizedProgressReport.getCategoryTitle());
            this.jTextAreaProgCatMessage.setVisible(categorizedProgressReport.getCategoryMessage() != null);
            this.jTextAreaProgCatMessage.setText(categorizedProgressReport.getCategoryMessage());
            this.jLabelProgressTitleDisp.setVisible(progressReport.getTitle() != null);
            this.jLabelProgressTitleDisp.setText(progressReport.getTitle());
            this.jTextAreaProgressMessage.setVisible(progressReport.getProgressMessage() != null);
            this.jTextAreaProgressMessage.setText(progressReport.getProgressMessage());

            if (this.jProgressBarCategory.getValue() != categorizedProgressReport.getCategoryProgress().intValue())
            {
                String buildTimeString;
                long runningTime;
                long timeLeft;

                // Set the category progress value.
                this.jProgressBarCategory.setValue(categorizedProgressReport.getCategoryProgress().intValue());

                // Initialize the catagory start time with the process start time so the home panel will be overly pesimistic with this calculation.
                if (categoryStartTime == 0) categoryStartTime = startTime;
                if (categorizedProgressReport.getCategoryProgress() < lastCategoryValue)
                {
                    categoryStartTime = System.currentTimeMillis();
                }
                lastCategoryValue = categorizedProgressReport.getCategoryProgress();

                // Calculate the remaining time and update the UI.
                runningTime = System.currentTimeMillis() - categoryStartTime ;
                timeLeft = (long) ((runningTime / categorizedProgressReport.getCategoryProgress()) * (100 - categorizedProgressReport.getCategoryProgress()));
                buildTimeString = buildTimeString(timeLeft);
                this.jProgressBarCategory.setString(categorizedProgressReport.getCategoryProgress().intValue() + "%" + (buildTimeString.length() > 0 ? " - " : "") + buildTimeString);
            }
        }
        else if (progressReport != null)// No categories available so just use the default progress report.
        {
            this.jLabelProgressTitleDisp.setVisible(progressReport.getTitle() != null);
            this.jLabelProgressTitleDisp.setText(progressReport.getTitle());
            this.jTextAreaProgressMessage.setVisible(progressReport.getProgressMessage() != null);
            this.jTextAreaProgressMessage.setText(progressReport.getProgressMessage());
            this.jProgressBar.setValue(progressReport.getProgress().intValue());
        }

        // These only need to be set right at the beginning, we do this so we do not cause unnecessary validations of the UI.
        if (this.processDetails == null)
        {
            this.jTextAreaDescriptionValue.setText(processDescription);
            this.jLabelNameValue.setText(processDisplayName != null ? processDisplayName : processIdentifier);
            setDescriptiveProperties();
        }
        this.processDetails = processDetails;
        revalidate();
    }

    private String translate(String inputString)
    {
        return clientContext.getConfigurationManager().getUITranslation(context, inputString);
    }

    private void setDescriptiveProperties()
    {
        Map<String, Object> descriptiveProperties;

        jXPanelDescriptiveProperties.removeAll();
        descriptiveProperties = null; // TODO:  Implement descriptive properties.
        if (descriptiveProperties != null)
        {
            GridBagConstraints gridBagConstraints;
            int yIndex;

            yIndex = 0;
            for (String propertyKey : descriptiveProperties.keySet())
            {
                JLabel propertyLabel;
                JTextArea propertyValue;

                propertyLabel = new JLabel(propertyKey + ":");
                propertyValue = new JTextArea("" + descriptiveProperties.get(propertyKey));
                propertyValue.setLineWrap(true);
                propertyValue.setOpaque(false);
                propertyValue.setBorder(null);
                propertyValue.setColumns(1);
                propertyValue.setRows(1);
                propertyValue.setFont(jLabelName.getFont());

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = yIndex;
                gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
                jXPanelDescriptiveProperties.add(propertyLabel, gridBagConstraints);

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 1;
                gridBagConstraints.gridy = yIndex++;
                gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
                gridBagConstraints.weightx = 0.1;
                gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
                jXPanelDescriptiveProperties.add(propertyValue, gridBagConstraints);
            }
        }
        else jXPanelDescriptiveProperties.setVisible(false);

        jXPanelDescriptiveProperties.revalidate();
    }

    private String buildTimeString(long millis)
    {
        StringBuilder timeString;
        long secondInMillis = TimeUnit.SECONDS.toMillis(1);
        long minuteInMillis = TimeUnit.MINUTES.toMillis(1);
        long hourInMillis = TimeUnit.HOURS.toMillis(1);
        long elapsedHours;
        long elapsedMinutes;
        long elapsedSeconds;
        long diff;

        diff = millis;
        elapsedHours = diff / hourInMillis;
        diff %= hourInMillis;
        elapsedMinutes = diff / minuteInMillis;
        diff %= minuteInMillis;
        elapsedSeconds = diff / secondInMillis;

        timeString = new StringBuilder();
        if (elapsedHours > 100)
        {
            timeString.append("undeterminable");
        }
        else
        {
            if (elapsedHours > 0)
            {
                timeString.append(elapsedHours);
                timeString.append(" hours, ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0))
            {
                timeString.append(elapsedMinutes);
                timeString.append(" minutes, ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0) || (elapsedSeconds > 0))
            {
                timeString.append(elapsedSeconds);
                timeString.append(" seconds");
            }
        }

        return timeString.toString();
    }

    private void stopProcess()
    {
        if (parentTaskList != null)
        {
            parentTaskList.stopProcess(processDetails);
        }
        else
        {
            //Prevent another stop process from being sent
            stopped = true;
            jButtonStop.setEnabled(false);
            //Some operations may take some time to stop, and we do not want to lock the ui or prevent other process details from being set
            new Thread()
            {

                @Override
                public void run()
                {
                    final T8ProcessDetails processDetails;

                    try
                    {
                        processDetails = clientContext.getProcessManager().stopProcess(context, ProcessPanel.this.processDetails.getProcessIid());

                        SwingUtilities.invokeLater(() -> setProcessDetails(processDetails));
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while finalizing process: " + ProcessPanel.this.processDetails.getProcessIid(), e);
                    }
                }
            }.start();
        }
    }

    private void finalizeProcess()
    {
        if (parentTaskList != null)
        {
            parentTaskList.finalizeProcess(processDetails);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jXPanelDetails = new org.jdesktop.swingx.JXPanel();
        jLabelNameValue = new javax.swing.JLabel();
        jLabelStatus = new javax.swing.JLabel();
        jLabelStatusValue = new javax.swing.JLabel();
        jLabelStatusMessage = new javax.swing.JLabel();
        jLabelStatusValueMessage = new javax.swing.JLabel();
        jLabelInitiatedBy = new javax.swing.JLabel();
        jLabelInitiatedByValue = new javax.swing.JLabel();
        jLabelStartTime = new javax.swing.JLabel();
        jLabelStartTimeValue = new javax.swing.JLabel();
        jLabelEndTime = new javax.swing.JLabel();
        jLabelEndTimeValue = new javax.swing.JLabel();
        jLabelElapsedTime = new javax.swing.JLabel();
        jLabeElapsedTimeValue = new javax.swing.JLabel();
        jLabelName = new javax.swing.JLabel();
        jLabelDescription = new javax.swing.JLabel();
        jTextAreaDescriptionValue = new javax.swing.JTextArea();
        jXPanelDescriptiveProperties = new org.jdesktop.swingx.JXPanel();
        jXPanelControls = new org.jdesktop.swingx.JXPanel();
        jButtonStop = new javax.swing.JButton();
        jButtonClose = new javax.swing.JButton();
        jPanelProgressBar = new javax.swing.JPanel();
        jProgressBar = new com.pilog.t8.utilities.components.progressbar.T8ProgressBar();
        jLabelProgressTitleDisp = new javax.swing.JLabel();
        jTextAreaProgressMessage = new javax.swing.JTextArea();
        jPanelProgressBarCategory = new javax.swing.JPanel();
        jProgressBarCategory = new com.pilog.t8.utilities.components.progressbar.T8ProgressBar();
        jLabelProgCatTitleDisp = new javax.swing.JLabel();
        jTextAreaProgCatMessage = new javax.swing.JTextArea();

        setMinimumSize(new java.awt.Dimension(0, 0));
        setName(""); // NOI18N
        setOpaque(false);
        setScrollableHeightHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        setScrollableWidthHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        setLayout(new java.awt.GridBagLayout());

        jXPanelDetails.setOpaque(false);
        jXPanelDetails.setScrollableHeightHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        jXPanelDetails.setScrollableWidthHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        jXPanelDetails.setLayout(new java.awt.GridBagLayout());

        jLabelNameValue.setText("Process Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelNameValue, gridBagConstraints);

        jLabelStatus.setText(translate("Status:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelStatus, gridBagConstraints);

        jLabelStatusValue.setText("Not Started");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelStatusValue, gridBagConstraints);

        jLabelStatusMessage.setText(translate("Message:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelStatusMessage, gridBagConstraints);

        jLabelStatusValueMessage.setText("Message");
        jLabelStatusValueMessage.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelStatusValueMessage, gridBagConstraints);

        jLabelInitiatedBy.setText(translate("Initiator:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelInitiatedBy, gridBagConstraints);

        jLabelInitiatedByValue.setText("Initiator");
        jLabelInitiatedByValue.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelInitiatedByValue, gridBagConstraints);

        jLabelStartTime.setText(translate("Start Time:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelStartTime, gridBagConstraints);

        jLabelStartTimeValue.setText("Start Time");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelStartTimeValue, gridBagConstraints);

        jLabelEndTime.setText(translate("End Time:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelEndTime, gridBagConstraints);

        jLabelEndTimeValue.setText("End Time");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelEndTimeValue, gridBagConstraints);

        jLabelElapsedTime.setText(translate("Elapsed Time:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelElapsedTime, gridBagConstraints);

        jLabeElapsedTimeValue.setText("Elapsed Time");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabeElapsedTimeValue, gridBagConstraints);

        jLabelName.setText(translate("Process:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelName, gridBagConstraints);

        jLabelDescription.setText(translate("Process Description:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelDescription, gridBagConstraints);

        jTextAreaDescriptionValue.setEditable(false);
        jTextAreaDescriptionValue.setColumns(1);
        jTextAreaDescriptionValue.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jTextAreaDescriptionValue.setLineWrap(true);
        jTextAreaDescriptionValue.setRows(1);
        jTextAreaDescriptionValue.setWrapStyleWord(true);
        jTextAreaDescriptionValue.setBorder(null);
        jTextAreaDescriptionValue.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jXPanelDetails.add(jTextAreaDescriptionValue, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jXPanelDetails, gridBagConstraints);

        jXPanelDescriptiveProperties.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jXPanelDescriptiveProperties.setOpaque(false);
        jXPanelDescriptiveProperties.setScrollableHeightHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        jXPanelDescriptiveProperties.setScrollableWidthHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        jXPanelDescriptiveProperties.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jXPanelDescriptiveProperties, gridBagConstraints);

        jXPanelControls.setOpaque(false);
        jXPanelControls.setScrollableHeightHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        jXPanelControls.setScrollableWidthHint(org.jdesktop.swingx.ScrollableSizeHint.NONE);
        jXPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 1, 1));

        jButtonStop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/stopIcon.png"))); // NOI18N
        jButtonStop.setText(translate("Stop"));
        jButtonStop.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonStopActionPerformed(evt);
            }
        });
        jXPanelControls.add(jButtonStop);

        jButtonClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClose.setText(translate("Close"));
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        jXPanelControls.add(jButtonClose);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(jXPanelControls, gridBagConstraints);

        jPanelProgressBar.setToolTipText("");
        jPanelProgressBar.setOpaque(false);
        jPanelProgressBar.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 10);
        jPanelProgressBar.add(jProgressBar, gridBagConstraints);

        jLabelProgressTitleDisp.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelProgressBar.add(jLabelProgressTitleDisp, gridBagConstraints);

        jTextAreaProgressMessage.setEditable(false);
        jTextAreaProgressMessage.setColumns(20);
        jTextAreaProgressMessage.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaProgressMessage.setLineWrap(true);
        jTextAreaProgressMessage.setRows(1);
        jTextAreaProgressMessage.setWrapStyleWord(true);
        jTextAreaProgressMessage.setAutoscrolls(false);
        jTextAreaProgressMessage.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelProgressBar.add(jTextAreaProgressMessage, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelProgressBar, gridBagConstraints);

        jPanelProgressBarCategory.setOpaque(false);
        jPanelProgressBarCategory.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 10);
        jPanelProgressBarCategory.add(jProgressBarCategory, gridBagConstraints);

        jLabelProgCatTitleDisp.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelProgressBarCategory.add(jLabelProgCatTitleDisp, gridBagConstraints);

        jTextAreaProgCatMessage.setEditable(false);
        jTextAreaProgCatMessage.setColumns(20);
        jTextAreaProgCatMessage.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaProgCatMessage.setLineWrap(true);
        jTextAreaProgCatMessage.setRows(1);
        jTextAreaProgCatMessage.setWrapStyleWord(true);
        jTextAreaProgCatMessage.setAutoscrolls(false);
        jTextAreaProgCatMessage.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelProgressBarCategory.add(jTextAreaProgCatMessage, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelProgressBarCategory, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStopActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStopActionPerformed
    {//GEN-HEADEREND:event_jButtonStopActionPerformed
        stopProcess();
    }//GEN-LAST:event_jButtonStopActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        finalizeProcess();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonStop;
    private javax.swing.JLabel jLabeElapsedTimeValue;
    private javax.swing.JLabel jLabelDescription;
    private javax.swing.JLabel jLabelElapsedTime;
    private javax.swing.JLabel jLabelEndTime;
    private javax.swing.JLabel jLabelEndTimeValue;
    private javax.swing.JLabel jLabelInitiatedBy;
    private javax.swing.JLabel jLabelInitiatedByValue;
    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelNameValue;
    private javax.swing.JLabel jLabelProgCatTitleDisp;
    private javax.swing.JLabel jLabelProgressTitleDisp;
    private javax.swing.JLabel jLabelStartTime;
    private javax.swing.JLabel jLabelStartTimeValue;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelStatusMessage;
    private javax.swing.JLabel jLabelStatusValue;
    private javax.swing.JLabel jLabelStatusValueMessage;
    private javax.swing.JPanel jPanelProgressBar;
    private javax.swing.JPanel jPanelProgressBarCategory;
    private com.pilog.t8.utilities.components.progressbar.T8ProgressBar jProgressBar;
    private com.pilog.t8.utilities.components.progressbar.T8ProgressBar jProgressBarCategory;
    private javax.swing.JTextArea jTextAreaDescriptionValue;
    private javax.swing.JTextArea jTextAreaProgCatMessage;
    private javax.swing.JTextArea jTextAreaProgressMessage;
    private org.jdesktop.swingx.JXPanel jXPanelControls;
    private org.jdesktop.swingx.JXPanel jXPanelDescriptiveProperties;
    private org.jdesktop.swingx.JXPanel jXPanelDetails;
    // End of variables declaration//GEN-END:variables
}
