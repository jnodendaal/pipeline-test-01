package com.pilog.t8.ui.modulecontainer;

import com.pilog.t8.definition.ui.modulecontainer.T8ModuleContainerAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleContainerOperationHandler extends T8ComponentOperationHandler
{
    private final T8ModuleContainer container;

    public T8ModuleContainerOperationHandler(T8ModuleContainer container)
    {
        super(container);
        this.container = container;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8ModuleContainerAPIHandler.OPERATION_LOAD_MODULE))
        {
            container.loadModule((String)operationParameters.get(T8ModuleContainerAPIHandler.PARAMETER_MAPPING_IDENTIFIER));
            return null;
        }
        else if (operationIdentifier.equals(T8ModuleContainerAPIHandler.OPERATION_RELOAD_MODULE))
        {
            container.reloadModule();
            return null;
        }
        else if (operationIdentifier.equals(T8ModuleContainerAPIHandler.OPERATION_UNLOAD_MODULE))
        {
            container.clearModule();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
