/**
 * Created on 04 May 2015, 3:40:24 PM
 */
package com.pilog.t8.help;

import com.pilog.t8.definition.help.T8HTMLHelpDisplayDefinition;
import com.pilog.t8.utilities.components.desktop.DesktopUtil;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author Gavin Boshoff
 */
public class T8HTMLHelpDisplay extends T8HelpDisplay
{
    private final T8HTMLHelpDisplayDefinition definition;

    public T8HTMLHelpDisplay(T8HTMLHelpDisplayDefinition definition)
    {
        super(definition);
        this.definition = definition;
    }

    @Override
    public void display() throws URISyntaxException, IOException
    {
        if (this.definition.getHelpURLString() == null)
        {
            throw new IllegalStateException("Help not set up correctly for Help Definition : " + this.definition.getIdentifier());
        }

        DesktopUtil.openWebpage(this.definition.getHelpURL().toURI());
    }
}