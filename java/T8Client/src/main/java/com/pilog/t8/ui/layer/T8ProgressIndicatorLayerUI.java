package com.pilog.t8.ui.layer;

import com.pilog.t8.ui.busypanel.T8BusyPanel;
import java.awt.FlowLayout;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import org.jdesktop.jxlayer.JXLayer;

/**
 * @author Bouwer du Preez
 */
public class T8ProgressIndicatorLayerUI extends T8LockableLayerUI
{
    private T8BusyPanel busyPanel;
    private JComponent glassPane;

    public T8ProgressIndicatorLayerUI()
    {
        super();
        busyPanel = new T8BusyPanel("");
        busyPanel.setOpaque(false);
        busyPanel.setVisible(false);
    }

    public void setIndeterminate(boolean indeterminate)
    {
        busyPanel.setIndeterminate(indeterminate);
    }

    public void setProgress(int progress)
    {
        busyPanel.setProgress(progress);
    }

    public void setCategoryProgress(int progress)
    {
        busyPanel.setCategoryProgress(progress);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void installUI(JComponent c)
    {
        super.installUI(c);
        JXLayer<JComponent> layer = (JXLayer<JComponent>)c;
        glassPane = layer.getGlassPane();
        glassPane.setLayout(new BoxLayout(glassPane, BoxLayout.PAGE_AXIS));
        glassPane.add(Box.createVerticalGlue());
        glassPane.add(busyPanel);
        glassPane.add(Box.createVerticalGlue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void uninstallUI(JComponent c)
    {
        super.uninstallUI(c);
        JXLayer<JComponent> layer = (JXLayer<JComponent>)c;
        layer.getGlassPane().remove(busyPanel);
        layer.getGlassPane().setLayout(new FlowLayout());
    }

    @Override
    public void setLocked(boolean isLocked)
    {
        busyPanel.setBusy(isLocked);
        busyPanel.setVisible(isLocked);
        glassPane.setVisible(isLocked);
        super.setLocked(isLocked);
    }

    public void setMessage(String message)
    {
        busyPanel.setMessage(message);
    }

    public void setProgressMessage(String message)
    {
        busyPanel.setProgressMessage(message);
    }

    public void setCategoryMessage(String message)
    {
        busyPanel.setCategoryMessage(message);
    }
}
