package com.pilog.t8.ui.layer;

import java.awt.Graphics;
import javax.swing.JComponent;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;

public class T8LockableLayerUI extends LockableUI
{
    @Override
    public void paint(Graphics g, JComponent c)
    {
        if (isLocked())
        {
            c.paint(g);
        }
        else
        {
            super.paint(g, c);
        }
    }
}