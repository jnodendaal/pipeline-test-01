package com.pilog.t8.ui.notification;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationFilter;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.notification.T8NotificationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.LinearGradientPaint;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.TimingTarget;
import org.jdesktop.core.animation.timing.interpolators.AccelerationInterpolator;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.ScrollableSizeHint;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Hennie Brink
 */
public class T8ClientNotificationCentre extends JXPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClientNotificationCentre.class);
    private static final int PANEL_WIDTH = 350;

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8NotificationManager notificationManager;
    private final T8DefinitionManager definitionManager;
    private final T8LookAndFeelManager lafManager;
    private final NotificationCentreMouseListener notificationCentreMouseListener;
    private final AnimationMouseListener animationMouseListener;
    private final Map<String, T8NotificationDefinition> notificationDefinitionCache;
    private final List<NotificationContainer> notificationContainers;
    private Painter notificationBackgroundPainter;
    private JXLabel title;
    private JPanel notificationPanel;
    private int titleSize;
    private boolean visible = false; //This is to tell the animators how to behave with the various mouse events
    private boolean titleVisible = false;
    private Animator visibleAnimator;
    private Animator locationAnimator;
    private TimingTarget locationTarget;

    public T8ClientNotificationCentre(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.notificationManager = clientContext.getNotificationManager();
        this.definitionManager = clientContext.getDefinitionManager();
        this.lafManager = clientContext.getConfigurationManager().getLookAndFeelManager(context);
        this.animationMouseListener = new AnimationMouseListener();
        this.notificationCentreMouseListener = new NotificationCentreMouseListener();
        this.notificationDefinitionCache = new HashMap<String, T8NotificationDefinition>();
        this.notificationContainers = new ArrayList<NotificationContainer>();
        initializeComponents();
        createAnimators();
    }

    private void initializeComponents()
    {
        JRootPane rootPane;
        JLayeredPane layeredPane;
        JXPanel borderPanel;
        final JScrollPane jScrollPane;

        // Set the painter for painting the background of notifications.
        notificationBackgroundPainter = new T8PainterAdapter(lafManager.getNotificationBackgroundPainter());

        notificationPanel = new JPanel();
        notificationPanel.setLayout(new BoxLayout(notificationPanel, BoxLayout.Y_AXIS));
        notificationPanel.setOpaque(false);
        borderPanel = new JXPanel(new BorderLayout());
        borderPanel.add(notificationPanel, BorderLayout.NORTH);
        borderPanel.setOpaque(false);
        borderPanel.setScrollableWidthHint(ScrollableSizeHint.FIT);
        borderPanel.setScrollableHeightHint(ScrollableSizeHint.NONE);
        jScrollPane = new JScrollPane(borderPanel);
        title = new JXLabel("Notification Center");
        rootPane = SwingUtilities.getRootPane(clientContext.getParentWindow());
        layeredPane = rootPane.getLayeredPane();

        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(new BorderLayout());
        setAlpha(0.0f);

        title.setHorizontalAlignment(JXLabel.CENTER);
        title.setTextRotation((90 * Math.PI / 180)); // 270 degrees in radian
        title.setFont(LAFConstants.MAIN_HEADER_FONT);

        jScrollPane.setOpaque(false);
        jScrollPane.getViewport().setOpaque(false);
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        add(title, BorderLayout.EAST);
        add(jScrollPane, BorderLayout.CENTER);

        titleSize = title.getPreferredSize().width;

        setSize(new Dimension(PANEL_WIDTH, layeredPane.getSize().height));
        setLocation(titleSize - getSize().width, 0);
        layeredPane.addComponentListener(new ResizingComponentListener());

        layeredPane.add(this, new Integer(JLayeredPane.PALETTE_LAYER + 10));
        layeredPane.add(notificationCentreMouseListener, new Integer(JLayeredPane.PALETTE_LAYER + 9));

        updateBackgrounds();
    }

    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    public T8Context getAccessContext()
    {
        return context;
    }

    private void createAnimators()
    {
        visibleAnimator = new Animator.Builder()
                .addTarget(PropertySetter.getTarget(this, "alpha", 0f, 0.95f))
                .setDuration(100, TimeUnit.MILLISECONDS)
                .build();

        locationTarget = PropertySetter.getTarget(this, "location", new Point(titleSize - getSize().width, 0), new Point(0, 0));
        locationAnimator = new Animator.Builder()
                .addTarget(locationTarget)
                .setDuration(200, TimeUnit.MILLISECONDS)
                .setInterpolator(new AccelerationInterpolator(0.2f, 0.3f))
                .build();
    }

    private void showNotificationCentre()
    {
        if (locationAnimator.isRunning())
        {
            locationAnimator.reverseNow();
        }
        else if (!visible)
        {
            locationAnimator.start();
        }

        visible = true;

        if (getAlpha() < 0.95f)
        {
            if (visibleAnimator.isRunning())
            {
                if (visibleAnimator.getCurrentDirection() == Animator.Direction.BACKWARD)
                {
                    visibleAnimator.reverseNow();
                }
            }
            else
            {
                visibleAnimator.start();
            }
        }
        titleVisible = true;
        notificationCentreMouseListener.setVisible(true);
    }

    private void hideNotificationCentre()
    {
        if (locationAnimator.isRunning())
        {
            if (locationAnimator.getCurrentDirection() == Animator.Direction.FORWARD)
            {
                locationAnimator.reverseNow();
            }
        }
        else if (visible)
        {
            locationAnimator.startReverse();
        }
        visible = false;

        if (visibleAnimator.isRunning())
        {
            if (visibleAnimator.getCurrentDirection() == Animator.Direction.FORWARD)
            {
                visibleAnimator.reverseNow();
            }
        }
        else
        {
            visibleAnimator.startReverse();
        }
        titleVisible = false;
        notificationCentreMouseListener.setVisible(false);
    }

    private void updateBackgrounds()
    {
        Color blue = LAFConstants.NOTIFICATION_CENTRE_COLOR1;
        Color white = LAFConstants.NOTIFICATION_CENTRE_COLOR2;
        setBackgroundPainter(new MattePainter(new LinearGradientPaint(getWidth() / 2, 0, getWidth() / 2, getHeight(), new float[]
        {
            0f, 0.7f, 1f
        }, new Color[]
        {
            blue, white, blue
        })));

        blue = LAFConstants.NOTIFICATION_CENTRE_HEADER_COLOR1;
        white = LAFConstants.NOTIFICATION_CENTRE_HEADER_COLOR2;
        title.setBackgroundPainter(new MattePainter(new LinearGradientPaint(0, 0, title.getPreferredSize().width, getHeight(), new float[]
        {
            0f, 0.7f, 1f
        }, new Color[]
        {
            blue, white, blue
        })));
    }

    public void startComponent()
    {
    }

    public void stopComponent()
    {
        // Stop all notification containers.
        for (NotificationContainer container : notificationContainers)
        {
            container.stopComponent();
        }
    }

    String translate(String inputString)
    {
        return clientContext.getConfigurationManager().getUITranslation(context, inputString);
    }

    void closeNotification(NotificationContainer container)
    {
        try
        {
            notificationManager.closeNotification(context, container.getNotification().getInstanceIdentifier());
            container.stopComponent();
            notificationContainers.remove(container);
            notificationPanel.remove(container);
            if (notificationContainers.isEmpty())
            {
                removeMouseListener(animationMouseListener);
                hideNotificationCentre();
            }

            validate();
        }
        catch (Exception ex)
        {
            Toast.show("Failed to close notification", Toast.Style.ERROR);
            LOGGER.log("Failed to close the notification: " + container.getNotification(), ex);
        }
    }

    void dismissNotification(NotificationContainer container)
    {
        try
        {
            notificationManager.markWithStatus(context, container.getNotification().getInstanceIdentifier(), T8Notification.NotificationStatus.RECEIVED);
            container.stopComponent();
            notificationContainers.remove(container);
            notificationPanel.remove(container);
            if (notificationContainers.isEmpty())
            {
                removeMouseListener(animationMouseListener);
                hideNotificationCentre();
            }

            validate();
        }
        catch (Exception ex)
        {
            Toast.show("Failed to close notification", Toast.Style.ERROR);
            LOGGER.log("Failed to close the notification: " + container.getNotification(), ex);
        }
    }

    private void clearNotifications()
    {
        // Stop all existing notification components.
        for (NotificationContainer container : notificationContainers)
        {
            container.stopComponent();
        }

        // Clear the UI and container collection.
        notificationContainers.clear();
        notificationPanel.removeAll();
    }

    private List<String> getNotificationInstanceIdentifiers()
    {
        List<String> identifierList;

        identifierList = new ArrayList<String>();
        for (NotificationContainer container : notificationContainers)
        {
            identifierList.add(container.getNotification().getInstanceIdentifier());
        }

        return identifierList;
    }

    public void refreshNotifications()
    {
        try
        {
            Map<String, NotificationContainer> existingComponents;
            List<T8Notification> userNotifications;
            boolean newNotificationsAdded;

            // Create a map of existing notification containers.
            existingComponents = new HashMap<String, NotificationContainer>();
            for (NotificationContainer container : notificationContainers)
            {
                existingComponents.put(container.getNotification().getInstanceIdentifier(), container);
            }

            // Clear existing notification containers.
            notificationContainers.clear();
            notificationPanel.removeAll();

            // Get the notifications for this user and add them all to the UI.
            newNotificationsAdded = false;
            userNotifications = notificationManager.getUserNotifications(context, new T8NotificationFilter());
            for (T8Notification notification : userNotifications)
            {
                String notificationInstanceIdentifier;

                // Get the notification instance identifier and check whether or not we already have a display components for it.
                notificationInstanceIdentifier = notification.getInstanceIdentifier();
                if (existingComponents.containsKey(notificationInstanceIdentifier))
                {
                    NotificationContainer notificationContainer;

                    // Add the existing container back onto the panel.
                    notificationContainer = existingComponents.remove(notificationInstanceIdentifier);
                    notificationContainers.add(notificationContainer);
                    notificationPanel.add(notificationContainer);
                }
                else // No display component found, so create a new one.
                {
                    try
                    {
                        T8NotificationDisplayComponent displayComponent;
                        NotificationContainer notificationContainer;

                        // Create a display component for the notification and add it to the UI.
                        displayComponent = createNotificationDisplayComponent(notification);
                        notificationContainer = new NotificationContainer(this, displayComponent);
                        notificationContainer.setBackgroundPainter(notificationBackgroundPainter);
                        notificationContainers.add(notificationContainer);
                        notificationPanel.add(notificationContainer);
                        notificationContainer.startComponent();
                        validate();

                        // Set the flag to indicate that a new notification has been added.
                        newNotificationsAdded = true;
                    }
                    catch (Exception ex)
                    {
                        LOGGER.log("Failed to add notification " + notification.getInstanceIdentifier(), ex);
                    }
                }
            }

            // Stop all of the remaining notification components, that were not re-used.
            for (NotificationContainer oldContainer : existingComponents.values())
            {
                oldContainer.stopComponent();
            }

            // If we've added any new notification, show the notification center.
            if (newNotificationsAdded)
            {
                if (!Arrays.asList(getMouseListeners()).contains(animationMouseListener))
                {
                    addMouseListener(animationMouseListener);
                }

                showNotificationCentre();
            }
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to refresh client notifications", ex);
        }
    }

    private T8NotificationDisplayComponent createNotificationDisplayComponent(T8Notification notification)
    {
        T8NotificationDefinition notificationDefinition;
        String notificationId;

        notificationId = notification.getIdentifier();
        notificationDefinition = notificationDefinitionCache.get(notificationId);
        if (notificationDefinition == null)
        {
            try
            {
                notificationDefinition = (T8NotificationDefinition)definitionManager.getInitializedDefinition(context, notification.getProjectId(), notificationId, null);
                notificationDefinitionCache.put(notificationId, notificationDefinition);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading notification definition: " + notificationId, e);
                return null;
            }
        }

        // Create the display component from the definition we loaded (if we found it).
        if (notificationDefinition != null)
        {
            T8NotificationDisplayComponent displayComponent;

            displayComponent = notificationDefinition.getDisplayComponentInstance(context, notification);
            displayComponent.initialize();
            return displayComponent;
        }
        else return null;
    }

    private class AnimationMouseListener extends MouseAdapter
    {
        @Override
        public void mousePressed(MouseEvent e)
        {
            // Only show if there are any notifications.
            if (!notificationContainers.isEmpty())
            {
                showNotificationCentre();
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
            // Only show if there are any notifications.
            if (!notificationContainers.isEmpty())
            {
                if (visibleAnimator.isRunning())
                {
                    if (visibleAnimator.getCurrentDirection() == Animator.Direction.BACKWARD)
                    {
                        visibleAnimator.reverseNow();
                    }
                }
                else
                {
                    if (!titleVisible)
                    {
                        visibleAnimator.start();
                    }
                }
                titleVisible = true;
                notificationCentreMouseListener.setVisible(true);
            }
        }
    }

    private class NotificationCentreMouseListener extends JComponent
    {
        private NotificationCentreMouseListener()
        {
            setVisible(false);
            addMouseListener(new MouseAdapter()
            {
                @Override
                public void mousePressed(MouseEvent e)
                {
                    hideNotificationCentre();
                }
            });
        }
    }

    private class ResizingComponentListener extends ComponentAdapter
    {
        @Override
        public void componentResized(ComponentEvent e)
        {
            Dimension size;

            super.componentResized(e);
            size = e.getComponent().getSize();
            notificationCentreMouseListener.setSize(size);
            setSize(new Dimension(PANEL_WIDTH, size.height));
            setLocation(titleSize - getSize().width, 0);

            locationAnimator.removeTarget(locationTarget);
            locationTarget = PropertySetter.getTarget(T8ClientNotificationCentre.this, "location", new Point(titleSize - getSize().width, 0), new Point(0, 0));
            locationAnimator.addTarget(locationTarget);
            updateBackgrounds();
            validate();
        }
    }
}
