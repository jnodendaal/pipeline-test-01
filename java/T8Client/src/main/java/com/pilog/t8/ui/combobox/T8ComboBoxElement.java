package com.pilog.t8.ui.combobox;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ComboBoxElement implements Serializable
{
    private final String identifier;
    private String displayName;
    private Object dataValue;

    public T8ComboBoxElement(String identifier, String displayName, Object dataValue)
    {
        this.identifier = identifier;
        this.displayName = displayName;
        this.dataValue = dataValue;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public Object getDataValue()
    {
        return dataValue;
    }

    public void setDataValue(Object dataValue)
    {
        this.dataValue = dataValue;
    }
}
