package com.pilog.t8.ui.optionpane;

import com.pilog.t8.ui.T8ComponentController;
import java.awt.Component;
import java.awt.HeadlessException;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 * @author gavin.boshoff
 */
public class T8OptionPane extends JOptionPane
{

     /**
     * Shows the default {@code JOptionPane} input dialog with the addition of
     * translating the button text using the {@code T8ComponentController}.<br>
     * <br>
     * Shows a dialog requesting input from the user parented to
     * {@code parentComponent} with the dialog having the title
     * {@code title} and message type {@code messageType}.
     *
     * @param parentComponent  the parent {@code Component} for the
     *			dialog
     * @param message  the {@code Object} to display
     * @param title    the {@code String} to display in the dialog
     *			title bar
     * @param messageType the type of message that is to be displayed:
     *                 	{@code ERROR_MESSAGE},
     *			{@code INFORMATION_MESSAGE},
     *			{@code WARNING_MESSAGE},
     *                 	{@code QUESTION_MESSAGE},
     *			or {@code PLAIN_MESSAGE}
     * @param controller The {@code T8ComponentController} originating from the
     *      parent component which calls this dialog
     *
     * @return The {@code String} value which was input on the dialog by the user
     *
     * @throws HeadlessException if
     *   {@code GraphicsEnvironment.isHeadless} returns {@code true}
     * @see java.awt.GraphicsEnvironment#isHeadless
     */
    public static String showInputDialog(Component parentComponent, Object message,
        String title, int messageType, T8ComponentController controller) throws HeadlessException
    {
        Object[] options = {getTranslatedOption("OK", controller), getTranslatedOption("Cancel", controller)};

        //Section copied and modified from the JOptionPane class
        {
            JOptionPane pane = new JOptionPane(message, messageType, OK_CANCEL_OPTION, null, options, null);

            pane.setWantsInput(true);
            pane.setComponentOrientation(((parentComponent == null) ? getRootFrame() : parentComponent).getComponentOrientation());

            JDialog dialog = pane.createDialog(parentComponent, title);

            pane.selectInitialValue();
            dialog.setVisible(true);
            dialog.dispose();

            Object value = pane.getInputValue();

            if (value == UNINITIALIZED_VALUE) return null;
            return (String)value;
        }
    }

    /**
     * Shows the default {@code JOptionPane} message dialog with the addition of
     * translating the button text using the {@code T8ComponentController}.<br>
     * <br>
     * Brings up a dialog that displays a message using a default
     * icon determined by the {@code messageType} parameter.
     *
     * @param parentComponent determines the {@code Frame}
     *		in which the dialog is displayed; if {@code null},
     *		or if the {@code parentComponent} has no
     *		{@code Frame}, a default {@code Frame} is used
     * @param message   the {@code Object} to display
     * @param title     the title string for the dialog
     * @param messageType the type of message to be displayed:
     *                  {@code ERROR_MESSAGE},
     *			{@code INFORMATION_MESSAGE},
     *			{@code WARNING_MESSAGE},
     *                  {@code QUESTION_MESSAGE},
     *			or {@code PLAIN_MESSAGE}
     * @param controller The {@code T8ComponentController} originating from the
     *      parent component which calls this dialog
     *
     * @throws HeadlessException if
     *   {@code GraphicsEnvironment.isHeadless} returns {@code true}
     * @see java.awt.GraphicsEnvironment#isHeadless
     */
    public static void showMessageDialog(Component parentComponent, Object message,
        String title, int messageType, T8ComponentController controller) throws HeadlessException
    {
        Object[] options = {getTranslatedOption("OK", controller)};

        showOptionDialog(parentComponent, message, title, DEFAULT_OPTION, messageType, null, options, null);
    }

    /**
     * Shows the default {@code JOptionPane} confirm dialog with the addition of
     * translating the button text using the {@code T8ComponentController}.<br>
     * <br>
     * Brings up a dialog where the number of choices is determined
     * by the {@code optionType} parameter, where the
     * {@code messageType}
     * parameter determines the icon to display.
     * The {@code messageType} parameter is primarily used to supply
     * a default icon from the Look and Feel.
     *
     * @param parentComponent determines the {@code Frame} in
     *			which the dialog is displayed; if {@code null},
     *			or if the {@code parentComponent} has no
     *			{@code Frame}, a
     *                  default {@code Frame} is used.
     * @param message   the {@code Object} to display
     * @param title     the title string for the dialog
     * @param optionType an integer designating the options available
     *                   on the dialog: {@code YES_NO_OPTION},
     *                  {@code YES_NO_CANCEL_OPTION},
     *                  or {@code OK_CANCEL_OPTION}
     * @param messageType an integer designating the kind of message this is;
     *                  primarily used to determine the icon from the pluggable
     *                  Look and Feel: {@code ERROR_MESSAGE},
     *			{@code INFORMATION_MESSAGE},
     *                  {@code WARNING_MESSAGE},
     *                  {@code QUESTION_MESSAGE},
     *			or {@code PLAIN_MESSAGE}
     * @param controller The {@code T8ComponentController} originating from the
     *      parent component which calls this dialog
     *
     * @return an integer indicating the option selected by the user
     *
     * @throws HeadlessException if
     *   {@code GraphicsEnvironment.isHeadless} returns {@code true}
     * @see java.awt.GraphicsEnvironment#isHeadless
     */
    public static int showConfirmDialog(Component parentComponent, Object message,
        String title, int optionType, int messageType, T8ComponentController controller) throws HeadlessException
    {
        Object[] options;
        int dialogResult;

        switch (optionType)
        {
            case YES_NO_OPTION:
                options = new Object[]{getTranslatedOption("Yes", controller), getTranslatedOption("No", controller)};
                break;
            case OK_CANCEL_OPTION: //The okay cancel option return value will not corespond to the JOptionPaneConstants, therefore a special case
                options = new Object[]{getTranslatedOption("OK", controller), getTranslatedOption("Cancel", controller)};
                break;
            case YES_NO_CANCEL_OPTION:
            default:
                options = new Object[]{getTranslatedOption("Yes", controller), getTranslatedOption("No", controller), getTranslatedOption("Cancel", controller)};
        }

        dialogResult = showOptionDialog(parentComponent, message, title, optionType, messageType, null, options, null);

        if (optionType == OK_CANCEL_OPTION && dialogResult == 1) return CANCEL_OPTION;
        else return dialogResult;
    }

    /**
     * Method which uses the {@code T8ComponentController} to translate the
     * text which is displayed on the front end to the user
     *
     * @param optionText The {@code String} text to be translated
     * @param controller The {@code T8ComponentController} which contains all
     *      of the current user and system properties, including the
     *      ConfigurationManager and the SessionContext which is used to translate
     *      a value
     *
     * @return The {@code String} translated value for the specified string
     */
    private static String getTranslatedOption(String optionText, T8ComponentController controller)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), optionText);
    }
}
