package com.pilog.t8.ui.flowclient;

import com.pilog.t8.flow.T8FlowStatus;
import javafx.scene.Node;
import javafx.scene.text.Text;

/**
 * @author Bouwer du Preez
 */
public class T8FlowDetailsPane
{
    private Node node;
    private Text initiatorText;
    private Text startedTimeText;
    private Text currentPhasesText;
    private Text currentStepsText;
    private Text currentResponsiblePartiesText;

    public T8FlowDetailsPane()
    {
    }

    public Node getNode()
    {
        return node;
    }

    public void setNode(Node node)
    {
        this.node = node;
    }

    public Text getInitiatorText()
    {
        return initiatorText;
    }

    public void setInitiatorText(Text initiatorText)
    {
        this.initiatorText = initiatorText;
    }

    public Text getStartedTimeText()
    {
        return startedTimeText;
    }

    public void setStartedTimeText(Text startedTimeText)
    {
        this.startedTimeText = startedTimeText;
    }

    public Text getCurrentPhasesText()
    {
        return currentPhasesText;
    }

    public void setCurrentPhasesText(Text currentPhasesText)
    {
        this.currentPhasesText = currentPhasesText;
    }

    public Text getCurrentResponsiblePartiesText()
    {
        return currentResponsiblePartiesText;
    }

    public void setCurrentResponsiblePartiesText(Text currentResponsiblePartiesText)
    {
        this.currentResponsiblePartiesText = currentResponsiblePartiesText;
    }

    public Text getCurrentStepsText()
    {
        return currentStepsText;
    }

    public void setCurrentStepsText(Text currentStepsText)
    {
        this.currentStepsText = currentStepsText;
    }

    public void update(T8FlowStatus flowStatus)
    {
        currentPhasesText.setText(flowStatus.getCurrentPhaseDisplayString());
        currentStepsText.setText(flowStatus.getCurrentStepsDisplayString());
        currentResponsiblePartiesText.setText(flowStatus.getCurrentResponsiblePartiesDisplayString());
    }
}
