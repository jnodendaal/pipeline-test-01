package com.pilog.t8.ui.module;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleComponentController extends T8DefaultComponentController
{
    private final T8Module module;

    public T8ModuleComponentController(T8Context context, T8ComponentController parentController, T8Module module)
    {
        super(parentController, new T8ProjectContext(context, module.getComponentDefinition().getRootProjectId()), module.getComponentDefinition().getIdentifier(), true);
        this.module = module;
    }

    public T8Module getModule()
    {
        return module;
    }
}
