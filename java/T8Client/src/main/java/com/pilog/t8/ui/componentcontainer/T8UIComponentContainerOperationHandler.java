package com.pilog.t8.ui.componentcontainer;

import com.pilog.t8.definition.ui.componentcontainer.T8ComponentContainerAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8UIComponentContainerOperationHandler extends T8ComponentOperationHandler
{
    private T8UIComponentContainer container;

    public T8UIComponentContainerOperationHandler(T8UIComponentContainer container)
    {
        super(container);
        this.container = container;
    }
    
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8ComponentContainerAPIHandler.OPERATION_LOAD_COMPONENT))
        {
            container.loadComponent((String)operationParameters.get(T8ComponentContainerAPIHandler.PARAMETER_COMPONENT_IDENTIFIER), null);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentContainerAPIHandler.OPERATION_RELOAD_COMPONENT))
        {
            container.reloadContentComponent();
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentContainerAPIHandler.OPERATION_UNLOAD_COMPONENT))
        {
            container.clearContentComponent();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
