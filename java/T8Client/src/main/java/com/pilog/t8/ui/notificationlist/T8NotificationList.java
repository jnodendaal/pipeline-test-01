package com.pilog.t8.ui.notificationlist;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationFilter;
import com.pilog.t8.notification.T8NotificationSummary;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.notificationlist.T8NotificationListDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationList extends JXPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8NotificationList.class);

    private final T8ConfigurationManager configurationManager;
    private final T8NotificationManager notificationManager;
    private final T8NotificationListDefinition definition;
    private final T8NotificationFilter notificationFilter;
    private final T8ComponentController controller;
    private final T8Context context;
    private final T8NotificationListOperationHandler operationHandler;
    private NotificationListPane notificationListPanel;
    private T8NotificationSummary notificationSummary;
    private NotificationLoader lastNotificationLoader;
    private List<T8Notification> notificationList;
    private T8DefaultComponentContainer contentContainer;
    private Painter<?> backgroundPainter;

    public T8NotificationList(T8NotificationListDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.notificationManager = controller.getClientContext().getNotificationManager();
        this.context = controller.getContext();
        this.notificationFilter = new T8NotificationFilter();
        this.operationHandler = new T8NotificationListOperationHandler(this);
        initComponents();
        setupComponents();
        setupPainters();
    }

    private void setupComponents()
    {
        notificationListPanel = new NotificationListPane(this);
        jPanelContent.add(notificationListPanel, java.awt.BorderLayout.CENTER);

        contentContainer = new T8DefaultComponentContainer();
        contentContainer.setOpaque(false);
        contentContainer.setComponent(jPanelContent);
        add(contentContainer, BorderLayout.CENTER);
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize the painter for painting the background of this component.
        painterDefinition = definition.getBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
            setBackgroundPainter(backgroundPainter);
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return this.controller;
    }


    public T8Context getAccessContext()
    {
        return this.context;
    }

    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8NotificationListDefinition getComponentDefinition()
    {
        return this.definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        refreshNotificationList();
    }

    private String getTranslatedString(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    protected void closeNotification(T8Notification notification)
    {
        try
        {
            this.notificationManager.closeNotification(context, notification.getInstanceIdentifier());
        }
        catch (Exception ex)
        {
            Toast.show("Failed to dismiss the notification", Toast.Style.ERROR);
            LOGGER.log("Failed to dismiss the notification", ex);
        }

        refreshNotificationList();
    }

    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    private void showContentView()
    {
        contentContainer.unlock();
        repaint();
    }

    private void showBusyView()
    {
        contentContainer.setMessage(translate("Loading Task List..."));
        contentContainer.lock();
        repaint();
    }

    public void filterNotificationList(String notificationTypeIdentifier, boolean includeNewNotifications, boolean includeReceivedNotifications)
    {
        T8Log.log("Filtering notification list: " + notificationTypeIdentifier + " " + includeNewNotifications + " " + includeReceivedNotifications);
        notificationFilter.clearIncludedNotificationTypes();
        if (notificationTypeIdentifier != null) notificationFilter.addIncludedNotificationType(notificationTypeIdentifier);
        notificationFilter.setPageOffset(0);
        notificationFilter.setIncludeNewNotifications(includeNewNotifications);
        notificationFilter.setIncludeReceivedNotifications(includeReceivedNotifications);
        refreshNotificationList();
    }

    private void refreshNotificationList()
    {
        if (lastNotificationLoader != null) lastNotificationLoader.cancel(false);

        showBusyView();
        lastNotificationLoader = new NotificationLoader();
        lastNotificationLoader.execute();
    }

    public void retrieveNextPage()
    {
        int pageOffset;

        pageOffset = notificationFilter.getPageOffset();
        pageOffset += notificationFilter.getPageSize();
        notificationFilter.setPageOffset(pageOffset);
        refreshNotificationList();
    }

    private void retrievePreviousPage()
    {
        int pageOffset;

        pageOffset = notificationFilter.getPageOffset();
        pageOffset -= notificationFilter.getPageSize();
        if (pageOffset < 0) pageOffset = 0;
        notificationFilter.setPageOffset(pageOffset);
        refreshNotificationList();
    }

    private void closeAllNotifications()
    {
        NotificationCloser notificationCloser;

        if (lastNotificationLoader != null) lastNotificationLoader.cancel(false);

        showBusyView();
        notificationCloser = new NotificationCloser();
        notificationCloser.execute();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        notificationListPanel.stopComponent();
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private class NotificationLoader extends SwingWorker<Void, Void>
    {
        @Override
        protected Void doInBackground() throws Exception
        {
            T8SessionContext sessionContext;

            // Update the task list with the latest task details from the server.
            sessionContext = context.getSessionContext();
            LOGGER.log("Refreshing notification list for  User: " + sessionContext.getUserIdentifier() + " Profiles: " + sessionContext.getWorkFlowProfileIdentifiers() + "...");
            notificationSummary = notificationManager.getUserNotificationSummary(context, notificationFilter);
            notificationList = notificationManager.getUserNotifications(context, notificationFilter);
            return null;
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    int notificationCount;

                    // The only purpose of this statement is to rethrow any exceptions.
                    get();

                    // Set the task total and paginng counts on the UI.
                    notificationCount = notificationSummary.getNotificationCount(notificationFilter);
                    jLabelNotificationCount.setText("(" + notificationCount + ")"); // The last two spaces just adds a bit of padding on the tool bar.
                    notificationListPanel.setNotificationList(notificationList);
                }
                catch (InterruptedException | ExecutionException e)
                {
                    LOGGER.log("Exception while refreshing task list.", e);
                }

                // Show the loaded data.
                showContentView();
            }
        }
    }

    private class NotificationCloser extends SwingWorker<Void, Void>
    {
        @Override
        protected Void doInBackground() throws Exception
        {
            // Update the task list with the latest task details from the server.
            LOGGER.log("Closing notifications...");
            notificationManager.closeAllNotifications(context);
            notificationList = new ArrayList<>();
            return null;
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    int notificationCount;

                    // The only purpose of this statement is to rethrow any exceptions.
                    get();

                    // Set the task total and paginng counts on the UI.
                    notificationCount = notificationSummary.getNotificationCount(notificationFilter);
                    jLabelNotificationCount.setText("(" + notificationCount + ")"); // The last two spaces just adds a bit of padding on the tool bar.
                    notificationListPanel.setNotificationList(notificationList);
                }
                catch (InterruptedException | ExecutionException e)
                {
                    LOGGER.log("Exception while closing all notifications.", e);
                }

                // Show the loaded data.
                showContentView();
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jPanelToolBars = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jButtonCloseAll = new javax.swing.JButton();
        jToolBarPaging = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelNotification = new javax.swing.JLabel();
        jLabelPageStartOffset = new javax.swing.JLabel();
        jLabelPageRangeDash = new javax.swing.JLabel();
        jLabelPageEndOffset = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jLabelNotificationCount = new javax.swing.JLabel();

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.BorderLayout());

        jPanelToolBars.setOpaque(false);
        jPanelToolBars.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(getTranslatedString("Refresh"));
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonCloseAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-script.png"))); // NOI18N
        jButtonCloseAll.setText(getTranslatedString("Close All"));
        jButtonCloseAll.setToolTipText(getTranslatedString("Close all notifications sent to you."));
        jButtonCloseAll.setFocusable(false);
        jButtonCloseAll.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonCloseAll.setOpaque(false);
        jButtonCloseAll.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCloseAll.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseAllActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCloseAll);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelToolBars.add(jToolBarMain, gridBagConstraints);

        jToolBarPaging.setFloatable(false);
        jToolBarPaging.setRollover(true);
        jToolBarPaging.setOpaque(false);

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(getTranslatedString("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPaging.add(jButtonPreviousPage);

        jLabelNotification.setText(getTranslatedString("Notification: "));
        jToolBarPaging.add(jLabelNotification);
        jToolBarPaging.add(jLabelPageStartOffset);

        jLabelPageRangeDash.setText("-");
        jToolBarPaging.add(jLabelPageRangeDash);
        jToolBarPaging.add(jLabelPageEndOffset);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(getTranslatedString("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPaging.add(jButtonNextPage);

        jLabelNotificationCount.setText("(0)");
        jToolBarPaging.add(jLabelNotificationCount);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelToolBars.add(jToolBarPaging, gridBagConstraints);

        jPanelContent.add(jPanelToolBars, java.awt.BorderLayout.NORTH);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshNotificationList();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        retrievePreviousPage();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        retrieveNextPage();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    private void jButtonCloseAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseAllActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseAllActionPerformed
        closeAllNotifications();
    }//GEN-LAST:event_jButtonCloseAllActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCloseAll;
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelNotification;
    private javax.swing.JLabel jLabelNotificationCount;
    private javax.swing.JLabel jLabelPageEndOffset;
    private javax.swing.JLabel jLabelPageRangeDash;
    private javax.swing.JLabel jLabelPageStartOffset;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelToolBars;
    private javax.swing.JToolBar jToolBarMain;
    private javax.swing.JToolBar jToolBarPaging;
    // End of variables declaration//GEN-END:variables

}
