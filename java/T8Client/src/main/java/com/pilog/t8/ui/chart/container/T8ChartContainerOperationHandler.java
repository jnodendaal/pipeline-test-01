package com.pilog.t8.ui.chart.container;

import com.pilog.t8.definition.ui.chart.container.T8ChartContainerAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ChartContainerOperationHandler extends T8ComponentOperationHandler
{
    private final T8ChartContainer container;

    public T8ChartContainerOperationHandler(T8ChartContainer container)
    {
        super(container);
        this.container = container;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8ChartContainerAPIHandler.OPERATION_REFRESH_CHART))
        {
            container.refreshChart();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
