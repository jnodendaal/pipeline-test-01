package com.pilog.t8.client;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSessionDetails;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataManagerOperationHandler
{
    public static List<T8DataSessionDetails> getDataSessionDetails(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        return (List<T8DataSessionDetails>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_DATA_SESSION_DETAILS, operationParameters).get(PARAMETER_DATA_SESSION_DETAILS_LIST);
    }

    public static void killDataSession(T8Context context, String sessionId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_SESSION_IDENTIFIER, sessionId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_KILL_DATA_SESSION, operationParameters);
    }

    public static T8DataEntity retrieveDataEntity(T8Context context, String entityId, Map<String, Object> keyMap) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(PARAMETER_FIELD_VALUE_MAP, keyMap);
        return (T8DataEntity)T8MainServerClient.executeSynchronousOperation(context, OPERATION_RETRIEVE_DATA_ENTITY, operationParameters).get(PARAMETER_DATA_ENTITY);
    }

    public static boolean insertDataEntity(T8Context context, T8DataEntity dataEntity) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY, dataEntity);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_INSERT_DATA_ENTITY, operationParameters).get(PARAMETER_SUCCESS);
    }

    public static boolean updateDataEntity(T8Context context, T8DataEntity dataEntity) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY, dataEntity);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_UPDATE_DATA_ENTITY, operationParameters).get(PARAMETER_SUCCESS);
    }

    public static T8DataEntity createDataEntity(T8Context context, String entityId, Map<String, Object> keyMap) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(PARAMETER_FIELD_VALUE_MAP, keyMap);
        return (T8DataEntity)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_DATA_ENTITY, operationParameters).get(PARAMETER_DATA_ENTITY);
    }

    public static boolean deleteDataEntity(T8Context context, T8DataEntity dataEntity) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY, dataEntity);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_DELETE_DATA_ENTITY, operationParameters).get(PARAMETER_SUCCESS);
    }

    public static int countDataEntities(T8Context context, String projectId, String entityId, T8DataFilter filter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(PARAMETER_DATA_FILTER, filter);
        return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COUNT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_COUNT);
    }

    public static List<T8DataEntity> selectDataEntities(T8Context context, String entityId, Map<String, Object> keyMap) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(PARAMETER_DATA_FILTER, new T8DataFilter(entityId, keyMap));
        return (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_LIST);
    }

    public static List<T8DataEntity> selectDataEntities(T8Context context, String entityId, T8DataFilter filter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(PARAMETER_DATA_FILTER, filter);
        return (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_LIST);
    }

    public static List<T8DataEntity> selectDataEntities(T8Context context, String entityId, T8DataFilter filter, int startOffset, int pageSize) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(PARAMETER_DATA_FILTER, filter);
        operationParameters.put(PARAMETER_PAGE_OFFSET, startOffset);
        operationParameters.put(PARAMETER_PAGE_SIZE, pageSize);
        return (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_LIST);
    }

    public static List<T8DataEntityValidationReport> validateDataEntities(T8Context context, List<T8DataEntity> dataEntities, T8ServerEntityValidationScriptDefinition validationScriptDefinition) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_ENTITY_LIST, dataEntities);
        operationParameters.put(PARAMETER_DATA_ENTITY_VALIDATION_SCRIPT_DEFINITION, validationScriptDefinition);
        return (List<T8DataEntityValidationReport>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_VALIDATE_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_VALIDATION_REPORT_LIST);
    }
}
