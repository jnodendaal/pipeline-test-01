package com.pilog.t8.gfx.painter;

import com.pilog.t8.ui.T8PainterEffect;
import com.pilog.t8.ui.T8PainterEffectAdapter;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.gfx.painter.T8AbstractAreaPainterDefinition;
import com.pilog.t8.definition.gfx.painter.T8AbstractLayoutPainterDefinition;
import com.pilog.t8.definition.gfx.painter.T8AbstractLayoutPainterDefinition.HorizontalAlignment;
import com.pilog.t8.definition.gfx.painter.T8AbstractLayoutPainterDefinition.VerticalAlignment;
import com.pilog.t8.definition.gfx.painter.effect.T8PainterEffectDefinition;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.swingx.painter.AbstractAreaPainter;
import org.jdesktop.swingx.painter.AbstractLayoutPainter;
import org.jdesktop.swingx.painter.effects.AreaEffect;

/**
 *
 * @author Bouwer.duPreez
 */
public class T8PainterConfigurator
{
    public static final void setupAbstractLayoutPainter(AbstractLayoutPainter painter, T8AbstractLayoutPainterDefinition definition)
    {
        HorizontalAlignment horizontalAlignment;
        VerticalAlignment verticalAlignment;

        // Set the horizontal alignment of the painter.
        horizontalAlignment = definition.getHorizontalAlignment();
        if (horizontalAlignment != null)
        {
            if (horizontalAlignment == HorizontalAlignment.LEFT)
            {
                painter.setHorizontalAlignment(AbstractLayoutPainter.HorizontalAlignment.LEFT);
            }
            else if (horizontalAlignment == HorizontalAlignment.RIGHT)
            {
                painter.setHorizontalAlignment(AbstractLayoutPainter.HorizontalAlignment.RIGHT);
            }
            else
            {
                painter.setHorizontalAlignment(AbstractLayoutPainter.HorizontalAlignment.CENTER);
            }
        }

        // Set the vertical alignment of the painter.
        verticalAlignment = definition.getVerticalAlignment();
        if (verticalAlignment != null)
        {
            if (verticalAlignment == VerticalAlignment.TOP)
            {
                painter.setVerticalAlignment(AbstractLayoutPainter.VerticalAlignment.TOP);
            }
            else if (verticalAlignment == VerticalAlignment.BOTTOM)
            {
                painter.setVerticalAlignment(AbstractLayoutPainter.VerticalAlignment.BOTTOM);
            }
            else
            {
                painter.setVerticalAlignment(AbstractLayoutPainter.VerticalAlignment.CENTER);
            }
        }

        // Set the insets of the painter.
        painter.setInsets(definition.getInsets());
    }

    public static final void setupAbstractAreaPainter(AbstractAreaPainter painter, T8AbstractAreaPainterDefinition definition)
    {
        T8PaintDefinition fillPaintDefinition;
        T8PaintDefinition borderPaintDefinition;
        List<T8PainterEffectDefinition> areaEffectDefinitions;

        setupAbstractLayoutPainter(painter, definition);

        fillPaintDefinition = definition.getFillPaintDefinition();
        if (fillPaintDefinition != null) painter.setFillPaint(fillPaintDefinition.getNewPaintInstance());

        borderPaintDefinition = definition.getBorderPaintDefinition();
        if (borderPaintDefinition != null) painter.setBorderPaint(borderPaintDefinition.getNewPaintInstance());

        painter.setFillHorizontal(definition.getFillHorizontal());
        painter.setFillVertical(definition.getFillVertical());
        painter.setPaintStretched(definition.isPaintStreched());

        areaEffectDefinitions = definition.getAreaEffectDefinitions();
        if (areaEffectDefinitions != null)
        {
            List<AreaEffect> effects;

            effects = new ArrayList<>();
            for (T8PainterEffectDefinition areaEffectDefinition : areaEffectDefinitions)
            {
                T8PainterEffect effect;

                effect = areaEffectDefinition.getNewPainterEffectInstance();
                effects.add(new T8PainterEffectAdapter(effect));
            }

            painter.setAreaEffects(effects.toArray(new AreaEffect[effects.size()]));
        }
    }
}
