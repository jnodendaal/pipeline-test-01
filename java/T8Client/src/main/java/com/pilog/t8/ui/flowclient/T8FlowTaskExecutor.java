package com.pilog.t8.ui.flowclient;

import com.pilog.t8.ui.T8ClientController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskExecutor extends Thread
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FlowTaskExecutor.class);

    private final T8ClientContext clientContext;
    private final T8ClientController flowClient;
    private final T8ComponentController controller;
    private final T8DefinitionManager definitionManager;
    private final T8FlowManager flowManager;
    private final T8Context context;
    private final String flowInstanceIdentifier;
    private final String taskInstanceIdentifier;
    private T8FlowTask currentTask;
    private volatile boolean stop;
    private boolean taskExecuted;

    private static final long INITIAL_TASK_WAIT_TIME = 500; // The interval time waited before a next task update is fetched from the server.
    private static final long INCREMENT_TASK_WAIT_TIME = 200; // The amount by which the next task wait interval is increased if no task is available.
    private static final long MAXIMUM_TASK_WAIT_TIME = 5000; // The maximum next task wait interval that will no be exceeded.

    public T8FlowTaskExecutor(T8ClientController flowClient, T8ComponentController controller, String flowIid, String taskIid)
    {
        this.flowClient = flowClient;
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = flowClient.getAccessContext();
        this.definitionManager = clientContext.getDefinitionManager();
        this.flowManager = clientContext.getFlowManager();
        this.flowInstanceIdentifier = flowIid;
        this.taskInstanceIdentifier = taskIid;
        this.stop = false;
        this.taskExecuted = false;
        this.setDaemon(true);
        this.setName("FlowTaskExecutor_" + flowIid);
    }

    public synchronized void stopExecution()
    {
        // If there is a current task executing, stop it.
        if (currentTask != null) currentTask.stopTask();

        // Set the stop flag and notify all waiting threads.
        stop = true;
        notifyAll();
    }

    public synchronized T8FlowTask getCurrentTask()
    {
        return currentTask;
    }

    @Override
    public void run()
    {
        try
        {
            T8FlowTask nextTask;

            // Wait for the next task and then execute it once it arrives.
            nextTask = waitForNextTask(flowInstanceIdentifier, taskInstanceIdentifier, !taskExecuted);
            while (!stop && nextTask != null)
            {
                T8TaskDetails nextTaskDetails;

                // Get the next task details.
                nextTaskDetails = nextTask.getTaskDetails();

                // The next task has now successfully been instantiated, so execute it.
                try
                {
                    executeTask(nextTask);
                    taskExecuted = true;
                    flowClient.fireTaskCompletedEvent(nextTask.getTaskDetails().getFlowIid(), nextTask.getTaskDetails().getTaskIid());
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while executing client task: " + nextTask.getDefinition().getIdentifier(), e);
                }

                // Wait for the next task if required.
                if (nextTaskDetails.isSubsequentFollowUp()) nextTask = waitForNextTask(flowInstanceIdentifier, null, true);
                else nextTask = null;
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while waiting for next client task in flow: " + flowInstanceIdentifier, e);
        }

        // Signal completion of tasks (only if the executor was not stopped) and log the end of the thread.
        if (!stop) flowClient.fireTasksCompletedEvent();
        LOGGER.log("Client flow thread stopping.");
    }

    private T8FlowTask waitForNextTask(String flowIID, String taskIID, boolean indeterminate) throws Exception
    {
        int waitedCycles;
        long waitTime;

        // Set the time to wait before querying the server for a task.
        waitTime = INITIAL_TASK_WAIT_TIME;

        // Continue checking the current flow status until a task becomes available.
        waitedCycles = 0;
        while ((!stop) && ((waitedCycles < 5) || (indeterminate)))
        {
            Pair<T8FlowStatus, T8FlowTask> nextTask;

            // Fetch the next task available for the current flow and if one is found, return it, otherwise just continue the waiting loop.
            nextTask = fetchNextTask(flowIID, taskIID);
            if (nextTask.getValue2() != null)
            {
                // Task found.
                return nextTask.getValue2();
            }
            else
            {
                synchronized (this)
                {
                    T8SessionContext sessionContext;

                    // Lock the UI while waiting for the next task to process.
                    sessionContext = context.getSessionContext();
                    LOGGER.log("Waiting for next task.  Session: " + sessionContext.getSessionIdentifier() + " Flow Instance: " + flowIID + " User: " + sessionContext.getUserIdentifier() + " Profiles: " + sessionContext.getWorkFlowProfileIdentifiers());
                    flowClient.setFlowWaitingStatus(nextTask.getValue1());
                    waitedCycles++;
                    wait(waitTime);
                    if (waitTime < MAXIMUM_TASK_WAIT_TIME) waitTime += INCREMENT_TASK_WAIT_TIME;
                }
            }
        }

        // Return null, since the waiting operation was stoppped/paused.
        return null;
    }

    private Pair<T8FlowStatus, T8FlowTask> fetchNextTask(String flowIid, String taskIid) throws Exception
    {
        List<T8TaskDetails> availableTasks;
        T8FlowStatus flowStatus;

        // Get the flow status and task list.
        LOGGER.log("Fetching status for Flow Instance: " + flowIid);

        // Get the flow task list and from it the list of unclaimed tasks.
        flowStatus = flowManager.getFlowStatus(context, flowIid, true);
        availableTasks = flowStatus.getAvailableTasks();
        if (availableTasks.isEmpty())
        {
            LOGGER.log("User tasks completed.");
            return new Pair<T8FlowStatus, T8FlowTask>(flowStatus, null); // No more tasks available for this user.
        }
        else
        {
            LOGGER.log("Tasks available: " + availableTasks.size() + ".  Checking for first client-executable task...");
            for (T8TaskDetails nextTaskDetails : availableTasks)
            {
                if ((taskIid == null) || (taskIid.equals(nextTaskDetails.getTaskIid()))) // Make sure me only getch the specified task (if any).
                {
                    T8FlowTaskDefinition nextTaskDefinition;
                    String nextTaskIid;

                    if (!nextTaskDetails.isClaimed())
                    {
                        nextTaskIid = nextTaskDetails.getTaskIid();
                        flowManager.claimTask(context, nextTaskIid);
                    }

                    // If the task definition could be found, instantiate it and return the task instance.
                    nextTaskDefinition = (T8FlowTaskDefinition)definitionManager.getInitializedDefinition(context, null, nextTaskDetails.getTaskId(), null);
                    if (nextTaskDefinition != null) // Task successfully claimed.
                    {
                        if (nextTaskDefinition.getExecutionType() == T8FlowTaskDefinition.TaskExecutionType.EXTERNAL)
                        {
                            T8FlowTask nextTask;

                            LOGGER.log("Instantiating new Task Instance: " + nextTaskDefinition);
                            nextTask = nextTaskDefinition.getNewTaskInstance(nextTaskDetails);
                            return new Pair<T8FlowStatus, T8FlowTask>(flowStatus, nextTask); // Return the next task.
                        }
                        else continue; // The task has been claimed but is not externally executable.
                    }
                    else // Task could not be claimed or loaded so continue waiting for a task.
                    {
                        LOGGER.log("Next task could not be claimed/loaded.");
                        continue;
                    }
                }
            }

            // None of the available tasks were externally executable.
            return new Pair<T8FlowStatus, T8FlowTask>(flowStatus, null);
        }
    }

    private void executeTask(T8FlowTask nextTask) throws Exception
    {
        Map<String, Object> taskInputParameters;
        Map<String, Object> taskOutputParameters;
        T8TaskDetails taskDetails;

        // Get the task details.
        taskDetails = nextTask.getTaskDetails();

        // Set the access context.
        context.setFlowId(taskDetails.getFlowId());
        context.setFlowIid(taskDetails.getFlowIid());
        context.setTaskId(taskDetails.getTaskId());
        context.setTaskIid(taskDetails.getTaskIid());

        // Get the task input parameters.
        taskInputParameters = nextTask.getTaskDetails().getTaskInputParameters();

        // Start execution of the next task.
        LOGGER.log("Executing next client task: " + nextTask.getDefinition().getIdentifier() + "(" +  nextTask.getDefinition().getRootProjectId() + "), " + nextTask.getTaskDetails().getTaskIid() + " using input parameters: " + taskInputParameters);
        currentTask = nextTask;
        taskOutputParameters = currentTask.doTask(controller, taskInputParameters);
        flowClient.unlockUI(); // Unlock the layer so that the content can be accessed.

        // If the task was completed, report it to the flow manager.
        if (currentTask.getStatus().getStatus() == T8FlowTask.TaskStatus.COMPLETED)
        {
            // Log the task completion.
            LOGGER.log("Completed client task: " + nextTask.getDefinition().getIdentifier() + ", " + nextTask.getTaskDetails().getTaskIid() + " output parameters: " + taskOutputParameters);

            // Signal the completion of the task to the flow manager.
            flowManager.completeTask(context, nextTask.getTaskDetails().getTaskIid(), taskOutputParameters);

            // Now set the current task to null, since the current task is done and should no longer be taken into account.
            currentTask = null;
        }
        else
        {
            // Log the task completion.
            LOGGER.log("Client task ended: " + nextTask.getDefinition().getIdentifier() + ", " + nextTask.getTaskDetails().getTaskIid() + " output parameters: " + taskOutputParameters);
        }
    }
}
