package com.pilog.t8.ui.notificationlist;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.ex.T8CloseNotificationException;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.notification.T8NotificationDisplayComponent;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.notification.T8NotificationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
class NotificationListPane extends JXPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(NotificationListPane.class);

    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final JXTaskPaneContainer notificationListContainer;
    private final Map<String, T8NotificationDefinition> notificationDefinitionCache;
    private final List<NotificationContainer> notificationContainers;
    private final T8NotificationList parentList;

    private Painter notificationBackgroundPainter;

    NotificationListPane(T8NotificationList notificationList)
    {
        this.parentList = notificationList;
        this.context = notificationList.getController().getContext();
        this.definitionManager = context.getClientContext().getDefinitionManager();
        this.notificationContainers = new ArrayList<NotificationContainer>();
        this.notificationDefinitionCache = new HashMap<String, T8NotificationDefinition>();
        initComponents();
        this.notificationListContainer = new JXTaskPaneContainer();
        this.notificationListContainer.setOpaque(false);
        this.notificationListContainer.setBackgroundPainter(null);
        this.jScrollPaneNotificationList.setOpaque(false);
        this.jScrollPaneNotificationList.getViewport().setOpaque(false);
        this.jScrollPaneNotificationList.setViewportView(notificationListContainer);
        this.jScrollPaneNotificationList.setBorder(null);

        setupPainters();
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize the painter for painting the background of the task panels.
        painterDefinition = this.parentList.getComponentDefinition().getTaskBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            setNotificationBackgroundPainter(new T8PainterAdapter(painterDefinition.getNewPainterInstance()));
        }
    }

    public void stopComponent()
    {
        for (NotificationContainer container : notificationContainers)
        {
            container.stopComponent();
        }
    }

    void setNotificationBackgroundPainter(Painter painter)
    {
        this.notificationBackgroundPainter = painter;
    }

    void clearNotifications()
    {
        // Stop all existing notification components.
        for (NotificationContainer container : notificationContainers)
        {
            container.stopComponent();
        }

        // Clear the UI and container collection.
        notificationContainers.clear();
        notificationListContainer.removeAll();
    }

    void setNotificationList(List<T8Notification> notificationList)
    {
        // Clear the task and task type containers and then add all the new tasks and task types.
        clearNotifications();
        notificationList.stream().forEach((notification) ->
        {
            addNotification(notification);
        });

        // Ensure that the layout is up-to-date.
        notificationListContainer.revalidate();
        revalidate();
    }

    void addNotification(T8Notification notification)
    {
        try
        {
            NotificationContainer container;

            container = createNotificationPanel(notification);
            notificationListContainer.add(container);
            notificationContainers.add(container);
            container.startComponent();
        }
        catch (T8CloseNotificationException cne)
        {
            LOGGER.log(T8Logger.Level.ERROR, "Notification marked for deletion : " + cne.getNotificationInstanceIdentifier());
            this.parentList.closeNotification(notification);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to add notification display component for notification : " + notification, ex);
        }
    }

    private NotificationContainer createNotificationPanel(T8Notification notification) throws Exception
    {
        NotificationContainer notificationPane;

        // Create a new notification pane.
        notificationPane = new NotificationContainer(parentList, createNotificationDisplayComponent(notification));
        notificationPane.setOpaque(false);
        notificationPane.setBackgroundPainter(notificationBackgroundPainter);
        return notificationPane;
    }

    private T8NotificationDisplayComponent createNotificationDisplayComponent(T8Notification notification)
    {
        T8NotificationDefinition notificationDefinition;
        String notificationId;

        notificationId = notification.getIdentifier();
        notificationDefinition = notificationDefinitionCache.get(notificationId);
        if (notificationDefinition == null)
        {
            try
            {
                notificationDefinition = (T8NotificationDefinition)definitionManager.getInitializedDefinition(context, notification.getProjectId(), notificationId, null);
                notificationDefinitionCache.put(notificationId, notificationDefinition);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading notification definition: " + notificationId, e);
                return null;
            }
        }

        // Create the display component from the definition we loaded (if we found it).
        if (notificationDefinition != null)
        {
            T8NotificationDisplayComponent displayComponent;

            displayComponent = notificationDefinition.getDisplayComponentInstance(context, notification);
            displayComponent.initialize();
            return displayComponent;
        }
        else return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jScrollPaneNotificationList = new javax.swing.JScrollPane();

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jScrollPaneNotificationList.setOpaque(false);
        add(jScrollPaneNotificationList, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPaneNotificationList;
    // End of variables declaration//GEN-END:variables
}
