package com.pilog.t8.gfx.painter.matte;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.matte.T8MattePainterDefinition;
import com.pilog.t8.gfx.painter.T8PainterConfigurator;
import org.jdesktop.swingx.painter.MattePainter;

/**
 * @author Bouwer du Preez
 */
public class T8MattePainter extends MattePainter implements T8Painter
{
    private T8MattePainterDefinition definition;
    
    public T8MattePainter(T8MattePainterDefinition definition)
    {
        super(definition.getFillPaintDefinition().getNewPaintInstance(), true);
        this.definition = definition;
        setupPainter();
    }
    
    private void setupPainter()
    {
        // Configure this painter's super type properties.
        T8PainterConfigurator.setupAbstractAreaPainter(this, definition);
    }
}
