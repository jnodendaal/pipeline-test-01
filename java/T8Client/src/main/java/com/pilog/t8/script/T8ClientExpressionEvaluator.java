package com.pilog.t8.script;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.ParserContext;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.client.notification.T8ClientNotificationManagerScriptFacade;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.flow.T8ClientFlowManagerScriptFacade;
import com.pilog.t8.process.T8ClientProcessManagerScriptFacade;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.data.document.path.EpicDocPathParser;
import com.pilog.t8.data.ontology.T8OntologyStructureProvider;
import com.pilog.t8.data.org.T8OrganizationStructureProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ClientExpressionEvaluator extends ExpressionEvaluator implements T8ExpressionEvaluator
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final ParserContext parserContext;
    private final DocPathEvaluatorFactory docPathFactory;
    private final Map<String, T8Api> apiCache;

    public T8ClientExpressionEvaluator(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
        this.parserContext = new ParserContext();
        this.apiCache = new HashMap<>();
        this.docPathFactory = DocPathEvaluatorFactory.getFactory(context);
        addImports();
    }

    private void addImports()
    {
        try
        {
            parserContext.addMethodImport("getAPI", this, T8ClientExpressionEvaluator.class.getDeclaredMethod("getAPI", String.class));
            parserContext.addMethodImport("translate", this, T8ClientExpressionEvaluator.class.getDeclaredMethod("translate", String.class));
            parserContext.addMethodImport("createNewGUID", this, T8ClientExpressionEvaluator.class.getDeclaredMethod("createNewGUID"));
            parserContext.addMethodImport("docPath", this, T8ClientExpressionEvaluator.class.getDeclaredMethod("docPath", Object.class, String.class));
            parserContext.addMethodImport("getOrganizationStructure", this, this.getClass().getDeclaredMethod("getOrganizationStructure", new Class[0]));
            parserContext.addMethodImport("getOntologyStructure", this, this.getClass().getDeclaredMethod("getOntologyStructure", new Class[0]));

            parserContext.addExternalExpressionParser(new EpicDocPathParser(docPathFactory));
        }
        catch (NoSuchMethodException | SecurityException e)
        {
            throw new RuntimeException("Exception while adding client-side import.", e);
        }
    }

    /**
     * Sets the language to use when resolving expressions involving terminology.
     * @param languageId The concept id of the language to set.
     */
    @Override
    public void setLanguage(String languageId)
    {
        docPathFactory.setLanguage(languageId);
    }

    public T8Api getAPI(String apiIdentifier)
    {
        T8Api api;

        api = apiCache.get(apiIdentifier);
        if (api != null)
        {
            return api;
        }
        else
        {
            api = clientContext.getConfigurationManager().getAPI(context, apiIdentifier);
            apiCache.put(apiIdentifier, api);
            return api;
        }
    }

    public T8OrganizationStructure getOrganizationStructure()
    {
        T8OrganizationStructureProvider provider;

        // We use a String literal for the API identifier, rather than the proper constant in order to eliminate the dependency on T8DocumentClient project.
        provider = (T8OrganizationStructureProvider)getAPI("@API_ORGANIZATION_CLIENT");
        return provider.getOrganizationStructure();
    }

    public T8OntologyStructure getOntologyStructure()
    {
        T8OntologyStructureProvider provder;

        // We use a String literal for the API identifier, rather than the proper constant in order to eliminate the dependency on T8DocumentClient project.
        provder = (T8OntologyStructureProvider)getAPI("@API_ONTOLOGY_CLIENT");
        return provder.getOntologyStructure();
    }

    public Object docPath(Object documentObject, String expression)
    {
        DocPathExpressionEvaluator expressionEvaluator;

        expressionEvaluator = docPathFactory.getEvaluator();
        return expressionEvaluator.evaluateExpression(documentObject, expression);
    }

    public String translate(String inputString)
    {
        // Strings can only be translated if a valid session context is available.
        if (context == null) return inputString;
        else return clientContext.getConfigurationManager().getUITranslation(context, inputString);
    }

    public String createNewGUID()
    {
        return T8IdentifierUtilities.createNewGUID();
    }

    @Override
    public void compileExpression(String expressionString) throws EPICSyntaxException
    {
        compileExpression(expressionString, parserContext);
    }

    @Override
    public boolean evaluateBooleanExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        if (inputVariables != null) inputParameters.putAll(inputVariables);
        inputParameters.put("session", context);
        inputParameters.put("processManager", new T8ClientProcessManagerScriptFacade(context, clientContext.getProcessManager()));
        inputParameters.put("flowManager", new T8ClientFlowManagerScriptFacade(context, clientContext.getFlowManager()));
        inputParameters.put("notificationManager", new T8ClientNotificationManagerScriptFacade(context, clientContext.getNotificationManager()));

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(inputParameters.keySet());
        return super.evaluateBooleanExpression(inputParameters, null);
    }

    @Override
    public boolean evaluateBooleanExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        if (inputVariables != null) inputParameters.putAll(inputVariables);
        inputParameters.put("session", context);
        inputParameters.put("processManager", new T8ClientProcessManagerScriptFacade(context, clientContext.getProcessManager()));
        inputParameters.put("flowManager", new T8ClientFlowManagerScriptFacade(context, clientContext.getFlowManager()));
        inputParameters.put("notificationManager", new T8ClientNotificationManagerScriptFacade(context, clientContext.getNotificationManager()));

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(inputParameters.keySet());
        return evaluateBooleanExpression(parserContext, expressionString, inputParameters, null);
    }

    @Override
    public Object evaluateExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        if (inputVariables != null) inputParameters.putAll(inputVariables);
        inputParameters.put("session", context);
        inputParameters.put("processManager", new T8ClientProcessManagerScriptFacade(context, clientContext.getProcessManager()));
        inputParameters.put("flowManager", new T8ClientFlowManagerScriptFacade(context, clientContext.getFlowManager()));
        inputParameters.put("notificationManager", new T8ClientNotificationManagerScriptFacade(context, clientContext.getNotificationManager()));

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(inputParameters.keySet());
        return super.evaluateExpression(inputParameters, null);
    }

    @Override
    public Object evaluateExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        if (inputVariables != null) inputParameters.putAll(inputVariables);
        inputParameters.put("session", context);
        inputParameters.put("processManager", new T8ClientProcessManagerScriptFacade(context, clientContext.getProcessManager()));
        inputParameters.put("flowManager", new T8ClientFlowManagerScriptFacade(context, clientContext.getFlowManager()));
        inputParameters.put("notificationManager", new T8ClientNotificationManagerScriptFacade(context, clientContext.getNotificationManager()));

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(inputParameters.keySet());
        return evaluateExpression(parserContext, expressionString, inputParameters, null);
    }
}
