package com.pilog.t8.client;

import com.pilog.t8.T8ClientOperation;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultSynchronousClientOperation implements T8ClientOperation
{
    protected T8ComponentController controller;
    protected T8ClientOperationDefinition definition;

    public T8DefaultSynchronousClientOperation(T8ComponentController controller, T8ClientOperationDefinition definition, String operationInstanceIdentifier)
    {
        this.controller = controller;
        this.definition = definition;
    }

    @Override
    public abstract Map<String, Object> execute(T8SessionContext sessionContext, Map<String, Object> operationParameters) throws Exception;

    @Override
    public void stop(T8SessionContext sessionContext)
    {
    }

    @Override
    public void cancel(T8SessionContext sessionContext)
    {
    }

    @Override
    public void pause(T8SessionContext sessionContext)
    {
    }

    @Override
    public void resume(T8SessionContext sessionContext)
    {
    }

    @Override
    public double getProgress(T8SessionContext sessionContext)
    {
        return 0;
    }

    @Override
    public Object getProgressReport(T8SessionContext sessionContext)
    {
        return null;
    }
}
