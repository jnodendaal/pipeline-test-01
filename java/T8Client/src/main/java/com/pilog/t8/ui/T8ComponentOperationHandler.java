package com.pilog.t8.ui;

import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import java.awt.Component;
import java.util.Map;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentOperationHandler
{
    private final T8Component component;

    public T8ComponentOperationHandler(T8Component component)
    {
        this.component = component;
    }

    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_SET_VISIBLE))
        {
            Boolean visible;

            visible = (Boolean)operationParameters.get(T8ComponentAPIHandler.PARAMETER_VISIBLE);
            ((Component)component).setVisible(visible);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_HIDE))
        {
            ((Component)component).setVisible(false);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_SHOW))
        {
            ((Component)component).setVisible(true);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_SET_ENABLED))
        {
            Boolean enabled;

            enabled = (Boolean)operationParameters.get(T8ComponentAPIHandler.PARAMETER_ENABLED);
            ((Component)component).setEnabled(enabled);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_ENABLE))
        {
            ((Component)component).setEnabled(true);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_DISABLE))
        {
            ((Component)component).setEnabled(false);
            return null;
        }
        else if (operationIdentifier.equals(T8ComponentAPIHandler.OPERATION_SET_TOOLTIP_TEXT))
        {
            String text;

            text = (String)operationParameters.get(T8ComponentAPIHandler.PARAMETER_TEXT);
            ((JComponent)component).setToolTipText(text);
            return null;
        }
        else throw new RuntimeException("Invalid Operation Identifier: '" + operationIdentifier + "'.");
    }
}
