package com.pilog.t8.ui.laf;

import java.awt.Color;
import java.awt.Font;
import javax.swing.Icon;
import javax.swing.border.Border;

/**
 * @author Bouwer du Preez
 */
public class LAFConstants
{
    // Main Menu (Menu displayed on the main frame of the application)
    public static final Font MAIN_MENU_FONT = new Font("Tahoma", Font.BOLD, 14); // Font on Main Menu in main Frame module.
    public static final Color MAIN_MENU_TEXT_COLOR = new Color(51, 51, 51);

    // Main Header (Header on panels containing Functionality Modules)
    public static final Font MAIN_HEADER_FONT = new Font("Tahoma", Font.BOLD, 14); // Font of Main Header on all functionality modules.
    public static final Color MAIN_HEADER_TEXT_COLOR = new Color(51, 51, 51);

    // Content Header (Headers demarcating content sections on functionality modules)
    public static final Font CONTENT_HEADER_FONT = new Font("Tahoma", Font.BOLD, 13); // Font of the header text.
    public static final Color CONTENT_HEADER_HIGH_COLOR = new Color(240, 240, 240); // The high color of the Content Header gradient.
    public static final Color CONTENT_HEADER_LOW_COLOR = new Color(220, 220, 220); // The low color of the Content Header gradient.
    public static final Color CONTENT_HEADER_SELECTED_HIGH_COLOR = new Color(255, 255, 255); // The high color of the Content Header gradient when selected.
    public static final Color CONTENT_HEADER_SELECTED_LOW_COLOR = new Color(230, 230, 230); // The low color of the Content Header gradient when selected.
    public static final Color CONTENT_HEADER_TEXT_COLOR = new Color(51, 51, 51); // Color of the texgt on the Content Header.
    public static final Color CONTENT_HEADER_BORDER_COLOR = new Color(220, 220, 220);
    public static final Icon CONTENT_HEADER_COLLAPSED_ICON = new javax.swing.ImageIcon(LAFConstants.class.getResource("/com/pilog/t8/ui/icons/contentCollapsedIcon.png"));
    public static final Icon CONTENT_HEADER_EXPANDED_ICON = new javax.swing.ImageIcon(LAFConstants.class.getResource("/com/pilog/t8/ui/icons/contentExpandedIcon.png"));

    // Content Panel (Panels demarcated by a Content Header border)
    public static final Font CONTENT_FONT = new Font("Tahoma", Font.PLAIN, 11); // Font of normal text on content panels.
    public static final Color CONTENT_PANEL_BG_COLOR = new Color(240, 240, 240);

    // Hyperlinks
    public static final Color HYPER_LINK_FONT_COLOR = new Color(0, 0, 255);

    // Notifications
    public static final Color NOTIFICATION_CENTRE_HEADER_COLOR1 = new Color(180, 200, 255);
    public static final Color NOTIFICATION_CENTRE_HEADER_COLOR2 = new Color(240, 240, 255);
    public static final Color NOTIFICATION_CENTRE_COLOR1 = new Color(150, 180, 200);
    public static final Color NOTIFICATION_CENTRE_COLOR2 = new Color(160, 190, 210);

    public static final Color PROPERTY_KEY_HEADER_BG_COLOR = new Color(241, 240, 220);
    public static final Color CONTENT_GREY = new Color(241, 241, 241);
    public static final Color PRIVATE_BLUE = new Color(145, 164, 251);
    public static final Color LIGHT_BEIGE = new Color(248, 233, 199);
    public static final Color BEIGE = new Color(242, 220, 168);
    public static final Color DARK_BEIGE = new Color(239, 210, 141);
    public static final Color PROPERTY_BLUE = new Color(215, 215, 255);
    public static final Color INVALID_PINK = new Color(255, 180, 180);
    public static final Color SELECTION_ORANGE = new Color(255, 204, 102);
    public static final Color HEADER_BG_COLOR = new Color(220, 220, 220);
    public static final Color EDITED_GREEN = new Color(123, 206, 148);
    public static final Color DELETED_RED = new Color(255, 150, 150);

    public static final Border RED_LINE_BORDER = javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255));
    public static final Border GRAY_LINE_BORDER = javax.swing.BorderFactory.createLineBorder(new java.awt.Color(230, 230, 230));
    public static final Border WHITE_LINE_BORDER = javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255));
    public static final Border SELECTED_NODE_BORDER = javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 204, 0));
    public static final Border NORMAL_NODE_BORDER = javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204));
    public static final Border HEADER_BORDER = javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204));
}
