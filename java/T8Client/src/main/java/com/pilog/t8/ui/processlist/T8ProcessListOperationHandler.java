package com.pilog.t8.ui.processlist;

import com.pilog.t8.definition.ui.processlist.T8ProcessListAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessListOperationHandler extends T8ComponentOperationHandler
{
    private final T8ProcessListView processListView;

    public T8ProcessListOperationHandler(T8ProcessListView processListView)
    {
        super(processListView);
        this.processListView = processListView;
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        if (T8ProcessListAPIHandler.OPERATION_FILTER_PROCESS_LIST.equals(operationId))
        {
            String processId;

            processId = (String)operationParameters.get(T8ProcessListAPIHandler.PARAMETER_PROCESS_IDENTIFIER);
            processListView.filterProcessList(processId);
            return null;
        }
        else return super.executeOperation(operationId, operationParameters);
    }
}
