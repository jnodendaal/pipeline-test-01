package com.pilog.t8.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8TimeUnit;
import java.io.FileNotFoundException;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFileManagerScriptFacade
{
    private final T8FileManager fileManager;
    private final T8Context context;

    public T8ClientFileManagerScriptFacade(T8Context context, T8FileManager fileManager)
    {
        this.fileManager = fileManager;
        this.context = context;
    }

    public boolean openFileContext(String contextIid, String contextId, T8TimeUnit expirationTime) throws Exception
    {
        return fileManager.openFileContext(context, contextIid, contextId, expirationTime);
    }

    public boolean closeFileContext(String contextIid) throws Exception
    {
        return fileManager.closeFileContext(context, contextIid);
    }

    public long downloadFile(String contextIid, String localFilePath, String remoteFilePath) throws Exception, FileNotFoundException
    {
        return fileManager.downloadFile(context, contextIid, localFilePath, remoteFilePath);
    }

    public long getDownloadOperationBytesDownloaded(String localFilePath) throws Exception
    {
        return fileManager.getDownloadOperationBytesDownloaded(context, localFilePath);
    }

    public long getDownloadOperationFileSize(String localFilePath) throws Exception
    {
        return fileManager.getDownloadOperationFileSize(context, localFilePath);
    }

    public int getDownloadOperationProgress(String localFilePath) throws Exception
    {
        return fileManager.getDownloadOperationProgress(context, localFilePath);
    }

    public T8FileDetails uploadFile(String contextIid, String localFilePath, String remoteFilePath) throws Exception
    {
        return fileManager.uploadFile(context, contextIid, localFilePath, remoteFilePath);
    }

    public long getUploadOperationBytesUploaded(String localFilePath) throws Exception
    {
        return fileManager.getUploadOperationBytesUploaded(context, localFilePath);
    }

    public long getUploadOperationFileSize(String localFilePath) throws Exception
    {
        return fileManager.getUploadOperationFileSize(context, localFilePath);
    }

    public int getUploadOperationProgress(String localFilePath) throws Exception
    {
        return fileManager.getUploadOperationProgress(context, localFilePath);
    }
}
