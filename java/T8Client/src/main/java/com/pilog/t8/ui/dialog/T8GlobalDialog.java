package com.pilog.t8.ui.dialog;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogAPIHandler;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8GlobalDialog extends T8Dialog
{
    private final T8GlobalDialogDefinition definition;
    private final T8DefaultComponentController controller;
    private final T8GlobalDialogOperationHandler operationHandler;
    private final HashMap<String, T8Component> additionalComponents;
    private Map<String, Object> outputParameters;

    public T8GlobalDialog(T8GlobalDialogDefinition definition, T8ComponentController parentController)
    {
        super(definition, parentController);
        this.definition = definition;
        this.controller = new T8DefaultComponentController(parentController, new T8ProjectContext(parentController.getContext(), definition.getRootProjectId()), definition.getIdentifier(), true);
        this.controller.setRootComponent(T8GlobalDialog.this);
        this.controller.setContainer(super.getContainer());
        this.operationHandler = new T8GlobalDialogOperationHandler(this);
        this.additionalComponents = new HashMap<String, T8Component>();
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Set the input parameters.
        setInputParameters(inputParameters);

        // Set all of the controller scripts.
        for (T8ComponentControllerScriptDefinition scriptDefinition : definition.getControllerScriptDefinitions())
        {
            controller.addScript(scriptDefinition);
        }

        // Initialize the content component of the dialog.
        T8Log.log("Dialog input parameters: " + inputParameters);
        super.initializeComponent(inputParameters);
        if (definition.getContentComponentDefinition() != null) controller.addComponent(this, definition.getContentComponentDefinition(), inputParameters);

        // Initialize all of the additional components.
        for (T8ComponentDefinition componentDefinition : definition.getAdditionalComponentDefinitions())
        {
            controller.addComponent(this, componentDefinition, inputParameters);
        }
    }

    @Override
    public void startComponent()
    {
        // Start the component controller.
        controller.start();
        super.startComponent();

        // Signal Module Start.
        T8Log.log("Signalling Dialog start with input parameters: " + inputParameters);
        controller.reportEvent(this, definition.getComponentEventDefinition(T8GlobalDialogAPIHandler.EVENT_DIALOG_STARTED), inputParameters);
    }

    @Override
    public void stopComponent()
    {
        // Stop the component controller.
        controller.stop();
        super.stopComponent();
    }

    /**
     * This is the standard method for showing a T8GlobalDialog.
     */
    @Override
    public void openDialog()
    {
        if (!isVisible())
        {
            // Set the dialog sizes.
            setDialogSize();

            // Request focus in the dialog.
            requestFocus();
            transferFocus();

            // Reset the output parameters.
            outputParameters = null;

            // Report the event if required.
            controller.reportEvent(T8GlobalDialog.this, definition.getComponentEventDefinition(T8GlobalDialogAPIHandler.EVENT_DIALOG_OPENED), inputParameters);

            // This should always be called last because if the dialog is modal then the thread will block here.
            setVisible(true);
        }
    }

    /**
     * This is the standard method for hiding a currently displayed
     * T8GlobalDialog.
     */
    @Override
    public void closeDialog()
    {
        if (isVisible())
        {
            // Request focus in the window incase there is any focus listeners that need to do something before we close.
            requestFocusInWindow();

            // Dispose the dialog.
            dispose();

            // Report the event.
            controller.reportEvent(T8GlobalDialog.this, definition.getComponentEventDefinition(T8GlobalDialogAPIHandler.EVENT_DIALOG_CLOSED), getOutputParameters());
        }
    }

    /**
     * This method override the default implementation because we need to report
     * the event on the internal component controller, rather than its parent
     * (as is the case with the regular T8Dialog).
     */
    @Override
    protected void handleCloseButtonAction()
    {
        // Report the event.
        controller.reportEvent(T8GlobalDialog.this, definition.getComponentEventDefinition(T8GlobalDialogAPIHandler.EVENT_DIALOG_CLOSE_BUTTON_PRESSED), getOutputParameters());
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        String componentIdentifier;

        // If the child component is one of the additional components defined for this dialog, add it to the collection.
        componentIdentifier = childComponent.getComponentDefinition().getIdentifier();
        for (T8ComponentDefinition additionalComponentDefinition : definition.getAdditionalComponentDefinitions())
        {
            if (additionalComponentDefinition.getIdentifier().equals(componentIdentifier))
            {
                additionalComponents.put(childComponent.getComponentDefinition().getIdentifier(), childComponent);
                return;
            }
        }

        // The component is not one of the additional components, so invoke the default behaviour.
        super.addChildComponent(childComponent, constraints);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    public Map<String, Object> getOutputParameters()
    {
        return outputParameters;
    }

    void setOutputParameters(Map<String, Object> outputParameters)
    {
        this.outputParameters = T8IdentifierUtilities.prependNamespace(definition.getNamespace(), outputParameters);
    }
}
