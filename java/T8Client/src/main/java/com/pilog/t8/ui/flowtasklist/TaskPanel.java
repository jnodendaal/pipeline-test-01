package com.pilog.t8.ui.flowtasklist;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.ui.module.task.T8TaskDetailModuleDefinition;
import com.pilog.t8.flow.task.T8TaskDataObject;
import com.pilog.t8.ui.T8ClientController;
import java.awt.GridBagConstraints;
import java.text.SimpleDateFormat;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class TaskPanel extends JXPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(TaskPanel.class);

    private final T8FlowTaskList parentTaskList;
    private final T8FlowManager flowManager;
    private final T8DefinitionManager definitionManager;
    private T8TaskDataObject taskObject;
    private SimpleDateFormat dateFormat;
    private T8TaskDetailModuleDefinition moduleDefinition;
    private T8ClientController flowClient;
    private boolean detailsShowing = true;

    public TaskPanel(T8FlowTaskList parentTaskList, T8TaskDataObject taskObject)
    {
        this.parentTaskList = parentTaskList;
        this.definitionManager = parentTaskList.getController().getClientContext().getDefinitionManager();
        this.flowManager = parentTaskList.getController().getClientContext().getFlowManager();
        initComponents();
        setTaskDetails(taskObject);
    }

    private void setTaskDetails(T8TaskDataObject taskObject)
    {
        String taskDisplayName;
        String taskDescription;
        String taskIdentifier;

        // We need to translate these display Strings, since they will always be returned by the server in the default language.
        taskDisplayName = translate(taskObject.getTaskName());
        taskDescription = translate(taskObject.getTaskDescription());
        taskIdentifier = taskObject.getTaskId();

        this.dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss");

        this.taskObject = taskObject;
        this.jButtonStart.setText(taskObject.isClaimed() ? translate("Continue") : translate("Start"));
        this.jLabelTaskNameValue.setText(taskDisplayName != null ? taskDisplayName : taskIdentifier);
        this.jLabelStatusValue.setText(taskObject.isClaimed() ? translate("Started") : translate("Not Started"));
        this.jTextAreaDescriptionValue.setText(taskDescription);

        setPriorityText();
        setPriorityIcon();

        if (taskObject.getTimeIssued() != null)
        {
            this.jTextAreaTimeIssuedValue.setText(taskObject.getTimeIssued().toString());
        }
        else
        {
            this.jTextAreaTimeIssuedValue.setVisible(false);
            this.jLabelTimeIssued.setVisible(false);
        }
        if (taskObject.getTimeClaimed() != null)
        {
            this.jTextAreaTimeClaimedValue.setText(taskObject.getTimeClaimed().toString());
        }
        else
        {
            this.jTextAreaTimeClaimedValue.setVisible(false);
            this.jLabelTimeClaimed.setVisible(false);
        }

        setDescriptiveProperties();
        validate();
    }

    private void setPriorityText()
    {
        switch (T8TaskDetails.PriorityLevel.getPriorityLevel(this.taskObject.getPriority()))
        {
            case VERY_HIGH:
                jLabelPriorityText.setText(translate("Very High"));
                break;
            case HIGH:
                jLabelPriorityText.setText(translate("High"));
                break;
            case NORMAL:
                jLabelPriorityText.setText(translate("Normal"));
                break;
            case LOW:
                jLabelPriorityText.setText(translate("Low"));
                break;
            case VERY_LOW:
                jLabelPriorityText.setText(translate("Very Low"));
                break;
            default:
                throw new IllegalStateException("Invalid priority level found for Task: " + this.taskObject);
        }
    }

    private void setPriorityIcon()
    {
        String priorityIconName;
        String priorityTooltip;

        switch (T8TaskDetails.PriorityLevel.getPriorityLevel(this.taskObject.getPriority()))
        {
            case VERY_HIGH:
                priorityIconName = "priority-very-high.png";
                priorityTooltip = translate("Very High");
                break;
            case HIGH:
                priorityIconName = "priority-high.png";
                priorityTooltip = translate("High");
                break;
            case NORMAL:
                priorityIconName = "priority-normal.png";
                priorityTooltip = translate("Normal");
                break;
            case LOW:
                priorityIconName = "priority-low.png";
                priorityTooltip = translate("Low");
                break;
            case VERY_LOW:
                priorityIconName = "priority-very-low.png";
                priorityTooltip = translate("Very Low");
                break;
            default:
                throw new IllegalStateException("Invalid priority level found for Task: " + this.taskObject);
        }

        jLabelPriority.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/"+priorityIconName)));
        jLabelPriority.setToolTipText(priorityTooltip);
    }

    private String translate(String inputString)
    {
        return parentTaskList.translate(inputString);
    }

    private void setDescriptiveProperties()
    {
        Map<String, Object> descriptiveProperties;

        descriptiveProperties = taskObject.getFieldValues();
        if (descriptiveProperties != null)
        {
            GridBagConstraints gridBagConstraints;
            int yIndex;

            yIndex = 10;
            for (String propertyKey : descriptiveProperties.keySet())
            {
                JLabel propertyLabel;
                JTextArea propertyValue;

                propertyLabel = new JLabel(propertyKey + ":");
                propertyValue = new JTextArea("" + descriptiveProperties.get(propertyKey));
                propertyValue.setEditable(false);
                propertyValue.setLineWrap(true);
                propertyValue.setOpaque(false);
                propertyValue.setBorder(null);
                propertyValue.setColumns(1);
                propertyValue.setRows(1);
                propertyValue.setFont(jLabelTaskName.getFont());

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = yIndex;
                gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
                jXPanelDetails.add(propertyLabel, gridBagConstraints);

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 1;
                gridBagConstraints.gridy = yIndex++;
                gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
                gridBagConstraints.weightx = 0.1;
                gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
                jXPanelDetails.add(propertyValue, gridBagConstraints);
            }
        }

        jXPanelDetails.revalidate();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jXPanelDetails = new org.jdesktop.swingx.JXPanel();
        jLabelTaskNameValue = new javax.swing.JLabel();
        jLabelStatus = new javax.swing.JLabel();
        jLabelStatusValue = new javax.swing.JLabel();
        jLabelTaskName = new javax.swing.JLabel();
        jLabelDescription = new javax.swing.JLabel();
        jLabelTaskPriority = new javax.swing.JLabel();
        jTextAreaDescriptionValue = new javax.swing.JTextArea();
        jLabelPriorityText = new javax.swing.JLabel();
        jLabelTimeIssued = new javax.swing.JLabel();
        jTextAreaTimeIssuedValue = new javax.swing.JTextArea();
        jLabelTimeClaimed = new javax.swing.JLabel();
        jTextAreaTimeClaimedValue = new javax.swing.JTextArea();
        jLabelPriority = new javax.swing.JLabel();
        jXPanelTaskDetailsModule = new org.jdesktop.swingx.JXPanel();
        jXPanelControls = new org.jdesktop.swingx.JXPanel();
        jButtonStart = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jXPanelDetails.setOpaque(false);
        jXPanelDetails.setLayout(new java.awt.GridBagLayout());

        jLabelTaskNameValue.setText("Task Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelTaskNameValue, gridBagConstraints);

        jLabelStatus.setText(translate("Status:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelStatus, gridBagConstraints);

        jLabelStatusValue.setText("Not Started");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelStatusValue, gridBagConstraints);

        jLabelTaskName.setText(translate("Task:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelTaskName, gridBagConstraints);

        jLabelDescription.setText(translate("Task Description:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelDescription, gridBagConstraints);

        jLabelTaskPriority.setText(translate("Task Priority:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelTaskPriority, gridBagConstraints);

        jTextAreaDescriptionValue.setEditable(false);
        jTextAreaDescriptionValue.setColumns(1);
        jTextAreaDescriptionValue.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaDescriptionValue.setLineWrap(true);
        jTextAreaDescriptionValue.setRows(1);
        jTextAreaDescriptionValue.setBorder(null);
        jTextAreaDescriptionValue.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jXPanelDetails.add(jTextAreaDescriptionValue, gridBagConstraints);

        jLabelPriorityText.setText("Normal");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jXPanelDetails.add(jLabelPriorityText, gridBagConstraints);

        jLabelTimeIssued.setText(translate("Time Issued:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelTimeIssued, gridBagConstraints);

        jTextAreaTimeIssuedValue.setEditable(false);
        jTextAreaTimeIssuedValue.setColumns(1);
        jTextAreaTimeIssuedValue.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaTimeIssuedValue.setLineWrap(true);
        jTextAreaTimeIssuedValue.setRows(1);
        jTextAreaTimeIssuedValue.setBorder(null);
        jTextAreaTimeIssuedValue.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jXPanelDetails.add(jTextAreaTimeIssuedValue, gridBagConstraints);

        jLabelTimeClaimed.setText(translate("Time Claimed:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jXPanelDetails.add(jLabelTimeClaimed, gridBagConstraints);

        jTextAreaTimeClaimedValue.setEditable(false);
        jTextAreaTimeClaimedValue.setColumns(1);
        jTextAreaTimeClaimedValue.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaTimeClaimedValue.setLineWrap(true);
        jTextAreaTimeClaimedValue.setRows(1);
        jTextAreaTimeClaimedValue.setWrapStyleWord(true);
        jTextAreaTimeClaimedValue.setBorder(null);
        jTextAreaTimeClaimedValue.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jXPanelDetails.add(jTextAreaTimeClaimedValue, gridBagConstraints);

        jLabelPriority.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/priority-normal.png"))); // NOI18N
        jLabelPriority.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jXPanelDetails.add(jLabelPriority, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jXPanelDetails, gridBagConstraints);

        jXPanelTaskDetailsModule.setOpaque(false);
        jXPanelTaskDetailsModule.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jXPanelTaskDetailsModule, gridBagConstraints);

        jXPanelControls.setOpaque(false);
        jXPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 1, 1));

        jButtonStart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/playIcon.png"))); // NOI18N
        jButtonStart.setText(translate("Start"));
        jButtonStart.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonStartActionPerformed(evt);
            }
        });
        jXPanelControls.add(jButtonStart);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(jXPanelControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStartActionPerformed
    {//GEN-HEADEREND:event_jButtonStartActionPerformed
        parentTaskList.startTask(taskObject);
    }//GEN-LAST:event_jButtonStartActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonStart;
    private javax.swing.JLabel jLabelDescription;
    private javax.swing.JLabel jLabelPriority;
    private javax.swing.JLabel jLabelPriorityText;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelStatusValue;
    private javax.swing.JLabel jLabelTaskName;
    private javax.swing.JLabel jLabelTaskNameValue;
    private javax.swing.JLabel jLabelTaskPriority;
    private javax.swing.JLabel jLabelTimeClaimed;
    private javax.swing.JLabel jLabelTimeIssued;
    private javax.swing.JTextArea jTextAreaDescriptionValue;
    private javax.swing.JTextArea jTextAreaTimeClaimedValue;
    private javax.swing.JTextArea jTextAreaTimeIssuedValue;
    private org.jdesktop.swingx.JXPanel jXPanelControls;
    private org.jdesktop.swingx.JXPanel jXPanelDetails;
    private org.jdesktop.swingx.JXPanel jXPanelTaskDetailsModule;
    // End of variables declaration//GEN-END:variables
}
