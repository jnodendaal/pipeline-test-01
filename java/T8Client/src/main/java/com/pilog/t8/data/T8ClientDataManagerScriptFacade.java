package com.pilog.t8.data;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.data.T8DataManagerResource;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a utility class that will be called mainly from the scripts to retrieve data
 * @author Hennie Brink
 */
public class T8ClientDataManagerScriptFacade
{
    private final T8Context context;

    public T8ClientDataManagerScriptFacade(T8Context context)
    {
        this.context = context;
    }

    public List<T8DataSessionDetails> getDataSessionDetails() throws Exception
    {
        return T8DataManagerOperationHandler.getDataSessionDetails(context);
    }

    public void killDataSession(String dataSessionIdentifier) throws Exception
    {
        T8DataManagerOperationHandler.killDataSession(context, dataSessionIdentifier);
    }

    public List<T8DataEntityValidationReport> validateDataEntities(List<T8DataEntity> entityList, T8ServerEntityValidationScriptDefinition scriptDefinition) throws Exception
    {
        return T8DataManagerOperationHandler.validateDataEntities(context, entityList, scriptDefinition);
    }

    public List<T8DataSourceFieldDefinition> retrieveDataSourceFieldDefinitions(String dataSourceIdentifier) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(T8DataManagerResource.PARAMETER_DATA_SOURCE_IDENTIFIER, dataSourceIdentifier);

        return (List<T8DataSourceFieldDefinition>)T8MainServerClient.executeSynchronousOperation(context, T8DataManagerResource.OPERATION_RETRIEVE_DATA_SOURCE_FIELD_DEFINITIONS, operationParameters).get(T8DataManagerResource.PARAMETER_FIELD_DEFINITION_LIST);
    }

    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> fieldValueMap) throws Exception
    {
        return T8DataManagerOperationHandler.retrieveDataEntity(context, entityIdentifier, fieldValueMap);
    }

    public void insert(T8DataEntity entity) throws Exception
    {
        T8DataManagerOperationHandler.insertDataEntity(context, entity);
    }

    public boolean update(T8DataEntity entity) throws Exception
    {
        return T8DataManagerOperationHandler.updateDataEntity(context, entity);
    }

    public T8DataEntity createDataEntity(String entityId, Map<String, Object> keyMap) throws Exception
    {
        return T8DataManagerOperationHandler.createDataEntity(context, entityId, keyMap);
    }

    public boolean delete(T8DataEntity entity) throws Exception
    {
        return T8DataManagerOperationHandler.deleteDataEntity(context, entity);
    }

    public List<T8DataEntity> select(String entityId, T8DataFilter dataFilter, Integer pageOffset, Integer pageSize) throws Exception
    {
        return T8DataManagerOperationHandler.selectDataEntities(context, entityId, dataFilter, pageOffset, pageSize);
    }

    public List<T8DataEntity> select(String entityId, T8DataFilter dataFilter) throws Exception
    {
        return T8DataManagerOperationHandler.selectDataEntities(context, entityId, dataFilter);
    }

    public Integer count(String entityId, T8DataFilter dataFilter) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(T8DataManagerResource.PARAMETER_DATA_ENTITY_IDENTIFIER, entityId);
        operationParameters.put(T8DataManagerResource.PARAMETER_DATA_FILTER, dataFilter);

        return (Integer)T8MainServerClient.executeSynchronousOperation(context, T8DataManagerResource.OPERATION_COUNT_DATA_ENTITIES, operationParameters).get(T8DataManagerResource.PARAMETER_DATA_ENTITY_COUNT);
    }
}
