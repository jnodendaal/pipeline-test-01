package com.pilog.t8.ui.laf;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import javax.swing.UIDefaults;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler;
import org.jdesktop.swingx.plaf.basic.SpinningCalendarHeaderHandler;

/**
 * @author Bouwer du Preez
 */
public class T8WindowsLookAndFeel extends WindowsLookAndFeel
{
    @Override
    protected void initClassDefaults(UIDefaults table)
    {
        super.initClassDefaults(table);
        table.put(JXTaskPane.uiClassID, "com.pilog.t8.ui.laf.T8TaskPaneUI");
        table.put(JXTaskPaneContainer.uiClassID, "com.pilog.t8.ui.laf.T8TaskPaneContainerUI");

        table.put(CalendarHeaderHandler.uiControllerID, "org.jdesktop.swingx.plaf.basic.SpinningCalendarHeaderHandler");
        table.put(SpinningCalendarHeaderHandler.ARROWS_SURROUND_MONTH, Boolean.FALSE);
    }
}
