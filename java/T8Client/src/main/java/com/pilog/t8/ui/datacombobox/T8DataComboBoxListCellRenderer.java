package com.pilog.t8.ui.datacombobox;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxDefinition;
import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxListCellRenderer implements ListCellRenderer
{
    private T8DataComboBoxDefinition definition;
    private DefaultListCellRenderer defaultRenderer;

    private static Color SELECTED_COLOR = Color.BLUE;
    private static Color DESELECTED_COLOR = Color.BLACK;

    public T8DataComboBoxListCellRenderer(T8DataComboBoxDefinition definition)
    {
        this.definition = definition;
        this.defaultRenderer = new DefaultListCellRenderer();
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        if (value != null)
        {
            T8DataEntity entity;
            Object displayValue;
            JComponent rendererComponent;

            entity = (T8DataEntity)value;
            displayValue = entity.getFieldValue(definition.getDisplayFieldIdentifier());
            rendererComponent = (JComponent)defaultRenderer.getListCellRendererComponent(list, displayValue, index, isSelected, cellHasFocus);
            rendererComponent.setOpaque(false);
            rendererComponent.setForeground(isSelected ? SELECTED_COLOR : DESELECTED_COLOR);
            return rendererComponent;
        }
        else
        {
            JComponent rendererComponent;

            //Render null values as a empty space if no Null Selection Display Name has been specified, otherwise the renderer will not display the option as selectable
            rendererComponent = (JComponent)defaultRenderer.getListCellRendererComponent(list, definition.getNullSelectionDisplayName() != null ? definition.getNullSelectionDisplayName(): " ", index, isSelected, cellHasFocus);
            rendererComponent.setOpaque(false);
            rendererComponent.setForeground(isSelected ? SELECTED_COLOR : DESELECTED_COLOR);
            return rendererComponent;
        }
    }
}
