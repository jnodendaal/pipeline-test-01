package com.pilog.t8.security;

import com.pilog.t8.T8SecurityManager;

/**
 * @author Hennie Brink
 */
public class T8ClientSecurityManagerScriptFacade
{
    private final T8SecurityManager securityManager;
    private final T8Context context;

    public T8ClientSecurityManagerScriptFacade(T8Context context, T8SecurityManager securityManager)
    {
        this.context = context;
        this.securityManager = securityManager;
    }

    public boolean isPasswordChangeAvailable(String userId) throws Exception
    {
        return securityManager.isPasswordChangeAvailable(context, userId);
    }

    public boolean requestPasswordChange(String userId) throws Exception
    {
        return securityManager.requestPasswordChange(context, userId);
    }

    public void switchSessionLanguage(String languageId) throws Exception
    {
        securityManager.switchSessionLanguage(context, languageId);
    }
}
