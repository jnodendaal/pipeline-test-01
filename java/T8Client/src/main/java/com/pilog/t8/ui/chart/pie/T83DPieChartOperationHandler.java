package com.pilog.t8.ui.chart.pie;

import com.pilog.t8.definition.ui.chart.pie.T83DPieChartAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;
import org.jfree.data.general.PieDataset;

/**
 * @author Bouwer du Preez
 */
public class T83DPieChartOperationHandler extends T8ComponentOperationHandler
{
    private final T83DPieChart chart;

    public T83DPieChartOperationHandler(T83DPieChart chart)
    {
        super(chart);
        this.chart = chart;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T83DPieChartAPIHandler.OPERATION_UPDATE_DATA_SET))
        {
            PieDataset dataSet;

            dataSet = (PieDataset)operationParameters.get(T83DPieChartAPIHandler.PARAMETER_PIE_DATA_SET);
            if (dataSet != null)
            {
                chart.setDataSet(dataSet);
            }
            else
            {
                Map<String, Number> dataMap;

                dataMap = (Map<String, Number>)operationParameters.get(T83DPieChartAPIHandler.PARAMETER_PIE_DATA_MAP);
                if (dataMap != null)
                {
                    chart.setDataMap(dataMap);
                }
            }

            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
