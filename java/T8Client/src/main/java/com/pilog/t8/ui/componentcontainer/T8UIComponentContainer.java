package com.pilog.t8.ui.componentcontainer;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.componentcontainer.T8ComponentContainerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8UIComponentContainer extends T8DefaultComponentContainer implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8UIComponentContainer.class);

    protected T8UIComponentContainerOperationHandler operationHandler;
    protected T8ConfigurationManager configurationManager;
    protected T8DefinitionManager definitionManager;
    protected T8Component contentComponent;
    protected T8Context context;
    private final T8ComponentContainerDefinition definition;
    private final HashMap<String, T8Component> contentComponents;
    private final T8ComponentController controller;

    public T8UIComponentContainer(T8ComponentContainerDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.definitionManager = controller.getClientContext().getDefinitionManager();
        this.contentComponents = new HashMap<>();
        this.operationHandler = new T8UIComponentContainerOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        contentComponents.put(childComponent.getComponentDefinition().getIdentifier(), childComponent);
    }

    @Override
    public void startComponent()
    {
        // Add the first component if available (this will only be globally defined contentComponents, local contentComponents are added as part of the module).
        if (definition.getContentComponentDefinition() != null)
        {
            try
            {
                contentComponent = definition.getContentComponentDefinition().getNewComponentInstance(controller);
                contentComponent.initializeComponent(null);
                setComponent((JComponent)contentComponent);
                contentComponent.startComponent();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while instantiating content component: " + definition.getContentComponentDefinition(), e);
            }
        }
        else if (definition.getContentComponentIdentifier() != null)
        {
            contentComponent = contentComponents.get(definition.getContentComponentIdentifier());
            setComponent((JComponent)contentComponent);
        }
    }

    @Override
    public void stopComponent()
    {
        clearContentComponent();
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(contentComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return contentComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(controller.getContext(), inputString);
    }

    public T8Component getContentComponent()
    {
        return contentComponent;
    }

    public void reloadContentComponent()
    {
        if (contentComponent != null)
        {
            loadComponent(contentComponent.getComponentDefinition().getIdentifier(), null);
        }
    }

    public void clearContentComponent()
    {
        // Check to see if we need to stop the current content component.
        if (contentComponent != null)
        {
            // All of the content components will be stopped by the parent controller because be passed them on during initialization.
            // If the current content component is not part of the predefined list, we will discard it so it needs to be stopped first.
            if (!contentComponents.containsKey(contentComponent.getComponentDefinition().getIdentifier()))
            {
                contentComponent.stopComponent();
            }

            contentComponent = null;
            setComponent(null);
        }
    }

    public void loadComponent(String componentId, Map<String, Object> viewParameters)
    {
        long startTime;

        // Make sure this method is not called from the EDT.
        if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Method cannot be called from EDT.");

        // Lock the UI.
        startTime = System.currentTimeMillis();
        setMessage(translate("Loading Component..."));
        lock();

        // Check to see if we need to stop the current content component.
        if (contentComponent != null)
        {
            // If the current content component is not part of the predefined list, we will discard it so it needs to be stopped first.
            if (!contentComponents.containsKey(contentComponent.getComponentDefinition().getIdentifier()))
            {
                contentComponent.stopComponent();
            }
        }

        // Now load the new content component.
        if (contentComponents.containsKey(componentId))
        {
            contentComponent = contentComponents.get(componentId);
            setComponent((JComponent)contentComponent);
        }
        else
        {
            if (componentId != null)
            {
                if (T8IdentifierUtilities.isPublicId(componentId))
                {
                    try
                    {
                        T8ComponentDefinition componentDefinition;

                        componentDefinition = (T8ComponentDefinition)definitionManager.getInitializedDefinition(context, context.getProjectId(), componentId, viewParameters);
                        if (componentDefinition != null)
                        {
                            contentComponent = componentDefinition.getNewComponentInstance(controller);
                            contentComponent.initializeComponent(null);
                            setComponent((JComponent)contentComponent);
                            contentComponent.startComponent();
                        }
                        else throw new RuntimeException("Component not found: " + componentId);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while loading component definition: " + componentId, e);
                    }
                }
                else throw new RuntimeException("Invalid Component identifier encountered: " + componentId);
            }
            else throw new RuntimeException("Null Component identifier encountered.");
        }

        // Unlock the UI.
        unlock();
        LOGGER.log("Component Loaded: " + (System.currentTimeMillis() - startTime));
    }
}
