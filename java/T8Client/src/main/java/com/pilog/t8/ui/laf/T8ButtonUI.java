package com.pilog.t8.ui.laf;

import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.button.T8ButtonDefinition;
import com.sun.java.swing.plaf.windows.WindowsButtonUI;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Hennie Brink
 */
public class T8ButtonUI extends WindowsButtonUI
{
    private final T8LookAndFeelManager lafManager;
    private T8ButtonDefinition buttonDefinition;
    private boolean isToolbarButton;
    private T8PainterAdapter backgroundPainter;
    private T8PainterAdapter focusBackgroundPainter;
    private T8PainterAdapter selectedBackgroundPainter;

    public T8ButtonUI(T8LookAndFeelManager lafManager)
    {
        this.lafManager = lafManager;
    }

    public T8ButtonUI(T8LookAndFeelManager lafManager, T8ButtonDefinition buttonDefinition)
    {
        this(lafManager);
        this.buttonDefinition = buttonDefinition;
    }

    public void setIsToolbarButton(boolean isToolbarButton)
    {
        this.isToolbarButton = isToolbarButton;
    }

    @Override
    protected void installDefaults(AbstractButton b)
    {
        super.installDefaults(b);
        lafManager.getButtonBackgroundDisabledPainter();
        lafManager.getButtonBackgroundFocusedPainter();
        lafManager.getButtonBackgroundPainter();
        lafManager.getButtonBackgroundSelectedPainter();

        T8PainterDefinition painterDefinition;
        T8Painter painter;

        // Initialize painter for painting the default background of the button.
        painterDefinition = buttonDefinition != null ? buttonDefinition.getBackgroundPainterDefinition() : null;
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
        else if (isToolbarButton)
        {
            painter = lafManager.getToolBarButtonBackgroundPainter();
            if (painter != null) backgroundPainter = new T8PainterAdapter(painter);
        }
        else
        {
            painter = lafManager.getButtonBackgroundPainter();
            if (painter != null) backgroundPainter = new T8PainterAdapter(painter);
        }

        // Initialize painter for painting the focused background of the button.
        painterDefinition = buttonDefinition != null ? buttonDefinition.getBackgroundFocusedPainterDefinition() : null;
        if (painterDefinition != null)
        {
            focusBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
        else if (isToolbarButton)
        {
            painter = lafManager.getToolBarButtonBackgroundFocusedPainter();
            if (painter != null) focusBackgroundPainter = new T8PainterAdapter(painter);
        }
        else
        {
            painter = lafManager.getButtonBackgroundFocusedPainter();
            if (painter != null) focusBackgroundPainter = new T8PainterAdapter(painter);
        }

        // Initialize painter for painting the selected background of the button.
        painterDefinition = buttonDefinition != null ? buttonDefinition.getBackgroundSelectedPainterDefinition() : null;
        if (painterDefinition != null)
        {
            selectedBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
        else if (isToolbarButton)
        {
            painter = lafManager.getToolBarButtonBackgroundSelectedPainter();
            if (painter != null) selectedBackgroundPainter = new T8PainterAdapter(painter);
        }
        else
        {
            painter = lafManager.getButtonBackgroundSelectedPainter();
            if (painter != null) selectedBackgroundPainter = new T8PainterAdapter(painter);
        }

        b.setContentAreaFilled(backgroundPainter == null);
    }


    @Override
    protected void uninstallDefaults(AbstractButton b)
    {
        super.uninstallDefaults(b);
        selectedBackgroundPainter = null;
        focusBackgroundPainter = null;
        backgroundPainter = null;
    }

    @Override
    public void paint(Graphics g, JComponent c)
    {
        Graphics2D g2;
        JButton button;
        ButtonModel model;
        Painter painter;

        button = (JButton) c;
        model = button.getModel();
        g2 = (Graphics2D) g.create();
        painter = backgroundPainter;

        // Set the default painter.
        if (!model.isEnabled())
        {
            // TODO:  Set the disabled background painter.
        }
        else if (model.isArmed())
        {
            painter = selectedBackgroundPainter;
        }
        else if (model.isPressed())
        {
            painter = selectedBackgroundPainter;
        }
        else if (model.isRollover())
        {
            painter = focusBackgroundPainter;
        }


        if(painter != null)
        {
            painter.paint(g2, null, button.getWidth(), button.getHeight());
        }

        super.paint(g2, c);

        g2.dispose();
    }

}
