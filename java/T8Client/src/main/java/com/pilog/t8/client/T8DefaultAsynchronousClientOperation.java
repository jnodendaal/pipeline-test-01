package com.pilog.t8.client;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ClientOperation;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultAsynchronousClientOperation implements T8ClientOperation
{
    protected T8ClientContext clientContext;
    protected T8ClientOperationDefinition definition;
    protected boolean stopFlag = false;
    protected boolean cancelFlag = false;
    protected boolean pauseFlag = false;
    
    public T8DefaultAsynchronousClientOperation(T8ClientContext clientContext, T8ClientOperationDefinition definition, String operationInstanceIdentifier)
    {
        this.clientContext = clientContext;
        this.definition = definition;
    }
    
    @Override
    public abstract Map<String, Object> execute(T8SessionContext sessionContext, Map<String, Object> operationParameters) throws Exception;
    @Override
    public abstract double getProgress(T8SessionContext sessionContext);
    
    @Override
    public Object getProgressReport(T8SessionContext sessionContext)
    {
        return null;
    }

    @Override
    public void stop(T8SessionContext sessionContext)
    {
        stopFlag = true;
    }

    @Override
    public void cancel(T8SessionContext sessionContext)
    {
        cancelFlag = true;
    }

    @Override
    public void pause(T8SessionContext sessionContext)
    {
        pauseFlag = true;
    }

    @Override
    public void resume(T8SessionContext sessionContext)
    {
        pauseFlag = false;
    }
}
