package com.pilog.t8.ui.chart.container;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.chart.container.T8ChartContainerDefinition;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public class T8ChartContainer extends T8DefaultComponentContainer implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ChartContainer.class);

    private final T8ChartContainerDefinition definition;
    private T8Component chartComponent;
    private final T8ComponentController controller;
    private final T8ChartContainerOperationHandler operationHandler;

    public T8ChartContainer(T8ChartContainerDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8ChartContainerOperationHandler(this);
        this.setOpaque(definition.isOpaque());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        chartComponent = childComponent;
        ((JComponent)chartComponent).setOpaque(this.definition.isOpaque());
        setComponent((JComponent)chartComponent);
    }

    @Override
    public void startComponent()
    {
        if (this.definition.isChartAutoRetrieve()) new Thread(this::refreshChart).start();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return ArrayLists.typeSafeList(chartComponent);
    }

    @Override
    public int getChildComponentCount()
    {
        return chartComponent != null ? 1 : 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public T8Component getChartComponent()
    {
        return chartComponent;
    }

    public void refreshChart()
    {
        if (chartComponent != null)
        {
            try
            {
                Map<String, Object> operationParameters;
                Map<String, Object> outputParameters;

                // Set the progress layer message and then lock the layer during execution of the script.
                setMessage(definition.getChartRefreshText());
                lock();

                // Execute the script.
                outputParameters = T8MainServerClient.executeSynchronousOperation(controller.getContext(), definition.getSourceServerOperationIdentifier(), null);
                LOGGER.log("Chart Source Script Output: " + outputParameters);

                // Get the input parameters for the operation
                operationParameters = T8IdentifierUtilities.mapParameters(outputParameters, definition.getSourceParameterMapping());

                // Return the output of the script.
                chartComponent.executeOperation(definition.getChartRefreshOperationIdentifier(), operationParameters != null ? operationParameters : new HashMap<>());
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while refreshing chart: " + definition, e);
            }
            finally
            {
                // Unlock the layer.
                unlock();
            }
        }
        else LOGGER.log("No Chart Component Set.");
    }
}
