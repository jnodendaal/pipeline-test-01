package com.pilog.t8.client.definition;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.system.T8SystemDetails;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8ClientDefinitionManagerScriptFacade
{
    private final T8DefinitionManager definitionManager;
    private final T8Context context;

    public T8ClientDefinitionManagerScriptFacade(T8Context context, T8DefinitionManager definitionManager)
    {
        this.definitionManager = definitionManager;
        this.context = context;
    }

    public T8DataType getDataType(String stringRepresentation)
    {
        return definitionManager.createDataType(stringRepresentation);
    }

    public T8Definition getRawDefinition(String definitionId) throws Exception
    {
        return definitionManager.getRawDefinition(context, null, definitionId);
    }

    public T8Definition getInitializedDefinition(String definitionId, Map<String, Object> parameters) throws Exception
    {
        return definitionManager.getInitializedDefinition(context, null, definitionId, parameters);
    }

    public T8Definition getRawDefinition(String projectId, String definitionId) throws Exception
    {
        return definitionManager.getRawDefinition(context, projectId, definitionId);
    }

    public T8Definition getInitializedDefinition(String projectId, String definitionId, Map<String, Object> parameters) throws Exception
    {
        return definitionManager.getInitializedDefinition(context, projectId, definitionId, parameters);
    }

    public T8DefinitionMetaData saveDefinition(T8Definition definition, String keyIdentifier) throws Exception
    {
        return definitionManager.saveDefinition(context, definition, keyIdentifier);
    }

    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception
    {
        return definitionManager.getDefinitionMetaData(projectId, definitionId);
    }

    public <T extends T8Definition> ArrayList<T> getRawGroupDefinitions(String projectId, String definitionId) throws Exception
    {
        return definitionManager.getRawGroupDefinitions(context, projectId, definitionId);
    }

    public T8SystemDetails getSystemDetails()
    {
        return definitionManager.getSystemDetails();
    }
}
