package com.pilog.t8.ui.checkbox;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.checkbox.T8CheckBoxAPIHandler;
import com.pilog.t8.definition.ui.checkbox.T8CheckBoxDefinition;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JCheckBox;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBox extends JCheckBox implements T8Component
{
    private T8CheckBoxDefinition definition;
    private T8ComponentController controller;
    private T8CheckBoxOperationHandler operationHandler;
    private boolean eventsEnabled;

    public T8CheckBox(T8CheckBoxDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8CheckBoxOperationHandler(this);
        this.addItemListener(new CheckBoxItemListener());
        this.eventsEnabled = true;
        this.setOpaque(false);
    }

    public T8CheckBox(T8CheckBoxDefinition definition, T8ComponentController controller, boolean enableEvents)
    {
        this(definition, controller);
        this.eventsEnabled = enableEvents;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setText(definition.getText());
        setToolTipText(definition.getTooltipText());
        setSelected(definition.isChecked());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private class CheckBoxItemListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (eventsEnabled)
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<String, Object>();
                eventParameters.put(T8CheckBoxAPIHandler.PARAMETER_SELECTED, (e.getStateChange() == ItemEvent.SELECTED));
                controller.reportEvent(T8CheckBox.this, definition.getComponentEventDefinition(T8CheckBoxAPIHandler.EVENT_STATE_CHANGED), eventParameters);
            }
        }
    }
}
