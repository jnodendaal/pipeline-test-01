package com.pilog.t8.ui.componentcontainer;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.task.T8ComponentTask;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8AsynchronousOperationTask implements T8ComponentTask
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8AsynchronousOperationTask.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8ConfigurationManager configurationManager;
    private final String operationInstanceIdentifier;
    private Map<String, Object> operationResult;
    private Exception exception;
    private final String message;
    private double progress;
    private boolean active;
    private Object progressReport;

    public T8AsynchronousOperationTask(T8Context context, String operationIid, String message)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.configurationManager = clientContext.getConfigurationManager();
        this.operationInstanceIdentifier = operationIid;
        this.message = message;
        this.progress = 0;
        this.active = true;
    }

    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public Map<String, Object> getResult()
    {
        return operationResult;
    }

    public Exception getException()
    {
        return exception;
    }

    @Override
    public String getMessage()
    {
        return message != null ? message : translate("Processing...");
    }

    @Override
    public boolean isIndeterminate()
    {
        return false;
    }

    @Override
    public double getProgress()
    {
        try
        {
            T8ServerOperationStatusReport statusReport;
            T8ServerOperation.T8ServerOperationStatus status;

            // Get the status of the operation from the server.
            statusReport = T8MainServerClient.getOperationStatus(context, operationInstanceIdentifier);
            status = statusReport.getOperationStatus();

            // Check the status.
            if (status == T8ServerOperation.T8ServerOperationStatus.FAILED)
            {
                exception = (Exception)statusReport.getOperationResult();
                active = false;
                return progress;
            }
            else if (status == T8ServerOperation.T8ServerOperationStatus.IN_PROGRESS)
            {
                progress = statusReport.getOperationProgress();
                progressReport = statusReport.getProgressReportObject();
                return progress;
            }
            else
            {
                operationResult = statusReport.getOperationResult();
                active = false;
                return 100.00;
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while checking progress of asynchronous operation: " + operationInstanceIdentifier, e);
            active = false;
            return progress;
        }
    }

    @Override
    public boolean isActive()
    {
        return active;
    }

    @Override
    public Object getProgressReportObject()
    {
        return progressReport;
    }
}
