package com.pilog.t8.ui.laf;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Arrays;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import static javax.swing.SwingConstants.*;

/**
 * @author Hennie Brink
 */
public class T8TabbedPaneUI extends BasicTabbedPaneUI {

    private final int inclTab = 4;
    private final int anchoFocoV = inclTab;
    private final int anchoFocoH = 4;
    private final int anchoCarpetas = 18;
    private Polygon shape;

    public static ComponentUI createUI(JComponent c) {
        return new T8TabbedPaneUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        tabAreaInsets.right = anchoCarpetas;
    }

    @Override
    protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex) {
        if (runCount > 1) {
            int lines[] = new int[runCount];
            for (int i = 0; i < runCount; i++) {
                lines[i] = rects[tabRuns[i]].y + (tabPlacement == TOP ? maxTabHeight : 0);
            }
            Arrays.sort(lines);
            if (tabPlacement == TOP) {
                int fila = runCount;
                for (int i = 0; i < lines.length - 1; i++, fila--) {
                    Polygon carp = new Polygon();
                    carp.addPoint(0, lines[i]);
                    carp.addPoint(tabPane.getWidth() - 2 * fila - 2, lines[i]);
                    carp.addPoint(tabPane.getWidth() - 2 * fila, lines[i] + 3);
                    if (i < lines.length - 2) {
                        carp.addPoint(tabPane.getWidth() - 2 * fila, lines[i + 1]);
                        carp.addPoint(0, lines[i + 1]);
                    } else {
                        carp.addPoint(tabPane.getWidth() - 2 * fila, lines[i] + rects[selectedIndex].height);
                        carp.addPoint(0, lines[i] + rects[selectedIndex].height);
                    }
                    carp.addPoint(0, lines[i]);
                    g.setColor(hazAlfa(fila));
                    g.fillPolygon(carp);
                    g.setColor(darkShadow.darker());
                    g.drawPolygon(carp);
                }
            } else {
                int fila = 0;
                for (int i = 0; i < lines.length - 1; i++, fila++) {
                    Polygon carp = new Polygon();
                    carp.addPoint(0, lines[i]);
                    carp.addPoint(tabPane.getWidth() - 2 * fila - 1, lines[i]);
                    carp.addPoint(tabPane.getWidth() - 2 * fila - 1, lines[i + 1] - 3);
                    carp.addPoint(tabPane.getWidth() - 2 * fila - 3, lines[i + 1]);
                    carp.addPoint(0, lines[i + 1]);
                    carp.addPoint(0, lines[i]);
                    g.setColor(hazAlfa(fila + 2));
                    g.fillPolygon(carp);
                    g.setColor(darkShadow.darker());
                    g.drawPolygon(carp);
                }
            }
        }
        super.paintTabArea(g, tabPlacement, selectedIndex);
    }

    @Override
    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        Graphics2D g2D = (Graphics2D) g;
        GradientPaint gradientShadow;
        int xp[] = null;
        int yp[] = null;
        switch (tabPlacement) {
            case LEFT:
                xp = new int[]{x, x, x + w, x + w, x};
                yp = new int[]{y, y + h - 3, y + h - 3, y, y};
                break;
            case RIGHT:
                xp = new int[]{x, x, x + w - 2, x + w - 2, x};
                yp = new int[]{y, y + h - 3, y + h - 3, y, y};
                break;
            case BOTTOM:
                xp = new int[]{x, x, x + 3, x + w - inclTab - 6, x + w - inclTab - 2, x + w - inclTab, x + w - 3, x};
                yp = new int[]{y, y + h - 3, y + h, y + h, y + h - 1, y + h - 3, y, y};
                break;
            case TOP:
            default:
                xp = new int[]{x, x, x + 3, x + w - inclTab - 6, x + w - inclTab - 2, x + w - inclTab, x + w - inclTab, x};
                yp = new int[]{y + h, y + 3, y, y, y + 1, y + 3, y + h, y + h};
                break;
        }

        shape = new Polygon(xp, yp, xp.length);
        if (isSelected)
        {
            gradientShadow = new GradientPaint(x, y, LAFConstants.CONTENT_HEADER_HIGH_COLOR.brighter(), x, y + h, LAFConstants.CONTENT_HEADER_LOW_COLOR);
            g2D.setPaint(gradientShadow);
        } else
        {
            if (tabPane.isEnabled() && tabPane.isEnabledAt(tabIndex))
            {
                gradientShadow = new GradientPaint(x, y, LAFConstants.CONTENT_HEADER_HIGH_COLOR, x, y + h, LAFConstants.CONTENT_HEADER_LOW_COLOR);
                g2D.setPaint(gradientShadow);
            } else
            {
                gradientShadow = new GradientPaint(x, y, LAFConstants.CONTENT_HEADER_HIGH_COLOR, x, y + h, LAFConstants.CONTENT_HEADER_LOW_COLOR.darker());
                g2D.setPaint(gradientShadow);
            }
        }
        //selectColor = new Color(255, 255, 200);
        //deSelectColor = new Color(240, 255, 210);
        g2D.fill(shape);
        if (runCount > 1) {
            g2D.setColor(hazAlfa(getRunForTab(tabPane.getTabCount(), tabIndex) - 1));
            g2D.fill(shape);
        }
        g2D.fill(shape);
    }

    @Override
    protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected) {
        super.paintText(g, tabPlacement, font, metrics, tabIndex, title, textRect, isSelected);
    }

    @Override
    protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
        return inclTab + super.calculateTabWidth(tabPlacement, tabIndex, metrics);
    }

    @Override
    protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
        if (tabPlacement == LEFT || tabPlacement == RIGHT) {
            return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight);
        } else {
            return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight);
        }
    }

    @Override
    protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {

        Polygon newShape;

        int xp[];
        int yp[];
        switch (tabPlacement) {
            case LEFT:
                xp = new int[]{x , x, x + w, x + w};
                yp = new int[]{y + h - 3, y, y , y + h - 3};
                break;
            case RIGHT:
                xp = new int[]{x , x, x + w, x + w};
                yp = new int[]{y + h - 3, y, y , y + h - 3};
                break;
            case BOTTOM:
                xp = new int[]{x, x, x + 3, x + w - inclTab - 6, x + w - inclTab - 2, x + w - inclTab, x + w - 3};
                yp = new int[]{y, y + h - 3, y + h, y + h, y + h - 1, y + h - 3, y};
                break;
            default:
                xp = new int[]{x, x, x + 3, x + w - inclTab - 6, x + w - inclTab - 2, x + w - inclTab, x + w - inclTab};
                yp = new int[]{y + h, y + 3, y, y, y + 1, y + 3, y + h};
                break;
        }
        newShape = new Polygon(xp, yp, xp.length);
        if(isSelected) //Do not draw botton border
        {
            int[] xpReverse = new int[xp.length];
            int[] ypReverse = new int[yp.length];
            int counter = 0;
            for (int i = xp.length - 1; i >= 0; i--)
            {
                xpReverse[counter++] = xp[i];
            }
            counter = 0;
            for (int i = yp.length - 1; i >= 0; i--)
            {
                ypReverse[counter++] = yp[i];
            }

            for (int i = 0; i < xpReverse.length; i++)
            {
                newShape.addPoint(xpReverse[i], ypReverse[i]);
            }
        }
        g.setColor(UIManager.getColor("ScrollBar.thumbShadow"));
        g.drawPolygon(newShape);
    }

    @Override
    protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        if (tabPane.hasFocus() && isSelected) {
            g.setColor(UIManager.getColor("ScrollBar.thumbShadow"));
            g.drawPolygon(shape);
        }
    }

    protected Color hazAlfa(int fila) {
        int alfa = 0;
        if (fila >= 0) {
            alfa = 50 + (fila > 7 ? 70 : 10 * fila);
        }
        return new Color(0, 0, 0, alfa);
    }
}
