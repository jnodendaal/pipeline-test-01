package com.pilog.t8.client;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8SessionExpiredException;
import com.pilog.t8.security.T8ClientSecurityManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8HeartbeatResponse;
import com.pilog.t8.security.T8Session;
import com.pilog.t8.ui.T8ClientController;
import com.pilog.t8.ui.laf.T8LookAndFeelFactory;
import com.pilog.t8.ui.notification.T8ClientNotificationCentre;
import com.pilog.t8.ui.session.T8SessionExpiryDialog;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.components.tooltip.T8TooltipUI;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import com.pilog.version.T8ClientVersion;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFrame extends javax.swing.JFrame
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClientFrame.class);

    private T8SessionContext sessionContext;
    private T8Context context;
    private T8ClientController flowClient;
    private ScheduledExecutorService executor; // Heartbeat scheduler.
    private HeartbeatThread heartbeatThread;
    private T8ClientNotificationCentre notificationCenter;
    private final String startupModuleId;

    private final static long HEARTBEAT_INTERVAL = 30000; // 30 seconds.
    /**
     * The amount of time before a session expires, that a message will be
     * displayed to inform the user that the session is about to expire.
     */
    private final static long SESSION_EXP_WARNING = (5 * 60000L); // 5 minutes
    /** The key value for the operating system name property. */
    private static final String OS_PROPERTY = "os.name";

    /**
     * Creates new form T8DeveloperFrame
     * @param startupModuleId
     */
    public T8ClientFrame(String startupModuleId)
    {
        try
        {
            T8ClientSecurityManager securityManager;

            // Initialize the Swing components.
            this.startupModuleId = startupModuleId;
            LOGGER.log("Starting T8Client Version " + T8ClientVersion.VERSION + "...");
            initComponents();

            // Create a new Security Manager and Session Context.
            securityManager = new T8ClientSecurityManager(null);
            sessionContext = securityManager.createNewSessionContext();

            setPiLogLookAndFeel();

            // Set the frame size;
            this.setPreferredSize(new Dimension(800, 800));
            this.setMinimumSize(new Dimension(800, 800));
            this.setExtendedState(JFrame.MAXIMIZED_BOTH);

            // Instantiate a new client.
            flowClient = new T8ClientController(new T8Context((T8ClientContext)null, sessionContext));
            flowClient.initializeComponent(null);
            this.context = flowClient.getAccessContext();

            // Add the new Client to the frame.
            //Ensure we add it to the content pane so that the layer pane functions correctly for the notification centre
            getContentPane().add(flowClient);
            validate();

            // Do some general Swing configuration.
            ToolTipManager.sharedInstance().setDismissDelay(10000);

            // Start the heartbeat thread.
            executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8Client-Heartbeat"));
            heartbeatThread = new HeartbeatThread(this, context);
            executor.scheduleWithFixedDelay(heartbeatThread, HEARTBEAT_INTERVAL, HEARTBEAT_INTERVAL, TimeUnit.MILLISECONDS);

            // Create the notification center.
            notificationCenter = new T8ClientNotificationCentre(context);
            flowClient.setNotificationCentre(notificationCenter);

            try
            {
                String systemName;

                systemName = flowClient.getClientContext().getDefinitionManager().getSystemDetails().getDisplayName();
                setTitle(systemName);
            }
            catch(Exception ex)
            {
                LOGGER.log(T8Logger.Level.ERROR, "System Name not found.", ex);
            }

            //Set the tooltip L&F
            UIManager.put("ToolTipUI", T8TooltipUI.class.getName());
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while starting new T8Client.", e);
            throw new RuntimeException(e);
        }
    }

    private void setPiLogLookAndFeel()
    {
        LookAndFeel lookAndFeel;

        try
        {
            LOGGER.log(()->"JVM Set LAF : [" + UIManager.getLookAndFeel() + "] - System LAF : [" + UIManager.getSystemLookAndFeelClassName() + "] - Cross Platform LAF : [" + UIManager.getCrossPlatformLookAndFeelClassName() + "]");
            lookAndFeel = T8LookAndFeelFactory.getLookAndFeel(System.getProperty(OS_PROPERTY));

            UIManager.setLookAndFeel(lookAndFeel);
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (UnsupportedLookAndFeelException | RuntimeException e)
        {
            LOGGER.log("Error encountered while setting Look & Feel.", e);
        }
    }

    public void startup()
    {
        new Thread(() ->
        {
            try
            {
                flowClient.startComponent();
                flowClient.stopTaskExecution();
                flowClient.loadModule(startupModuleId, null);
                notificationCenter.startComponent();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while starting main module.", e);
            }
        }).start();
    }

    public void shutdown()
    {
        try
        {
            // Log the session out.
            LOGGER.log("Loggin out session '" + context.getSessionContext().getSessionIdentifier() + "'.");
            notificationCenter.stopComponent();
            flowClient.getNotificationManager().destroy();
            flowClient.stopComponent();
            flowClient.getSecurityManager().logout(context);

            // Terminate the heartbeat thread.
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS))
            {
                throw new Exception("Heartbeat thread executor could not be terminated.");
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while trying to log out.", e);
        }
    }

    public T8ClientController getFlowClient()
    {
        return flowClient;
    }

    T8ClientNotificationCentre getNotificationCenter()
    {
        return notificationCenter;
    }

    private static class HeartbeatThread extends Thread
    {
        private final T8Context context;
        private final T8ClientFrame clientFrame;
        private final T8SecurityManager securityManager;

        private boolean sessionExpired = false;

        private HeartbeatThread(T8ClientFrame flowClient, T8Context context)
        {
            this.clientFrame = flowClient;
            this.context = context;
            this.securityManager = clientFrame.getFlowClient().getSecurityManager();
        }

        @Override
        public void run()
        {
            if (this.context.getSessionContext().isAnonymousSession()) return;

            try
            {
                T8HeartbeatResponse response;
                long sessionTimeLeft;
                long latency;

                latency = System.currentTimeMillis();
                response = T8MainServerClient.heartbeat(new T8Context(context));
                latency = System.currentTimeMillis() - latency;
                if (response != null) // A response will only be received if the session has been logged in.
                {
                    T8ClientEnvironmentStatus status;

                    // Update the environment status.
                    status = new T8ClientEnvironmentStatus();
                    status.setNetworkLatency((int)latency);
                    status.setNetworkThroughput(T8MainServerClient.getStatistics().getAverageNetworkThroughput());
                    status.setServerActivity(response.getServerRequestsPerSecond());
                    status.setServerLoad(response.getServerLoad());
                    status.setMaximumConcurrency(response.getMaximumConcurrency());
                    clientFrame.getFlowClient().setEnvironmentStatus(status);

                    // Check for the session expiry, if the session is about to expire show the login screen
                    sessionExpired = false;
                    sessionTimeLeft = response.getExpirationTime() - response.getServerTime();
                    if (response.getExpirationTime() > 0 && (sessionTimeLeft < (HEARTBEAT_INTERVAL + 10000)))
                    {
                        logout();
                    }
                    else
                    {
                        // We first check if there are pending notifications
                        if (response.hasPendingNotifications())
                        {
                            SwingUtilities.invokeLater(() ->
                            {
                                clientFrame.getNotificationCenter().refreshNotifications();
                            });
                        }
                        // Then we check if the session is about to expire
                        if (sessionTimeLeft < SESSION_EXP_WARNING)
                        {
                            if (T8SessionExpiryDialog.showExpiryDialog(clientFrame, sessionTimeLeft) == T8SessionExpiryDialog.EXTEND_SESSION)
                            {
                                this.securityManager.extendSession(this.context);
                            } else logout();
                        }
                    }
                }
            }
            catch (T8SessionExpiredException se)
            {
                // Check if this session has expired then show the login screen.
                if (!sessionExpired)
                {
                    try
                    {
                        // We cannot do anything on the flow client any more unless we set a new anonymous session for the flow client
                        if (!context.getSessionContext().isAnonymousSession())
                        {
                            ((T8Session)context.getSessionContext()).anonymize();
                        }

                        sessionExpired = true;
                        showLogin();
                    }
                    catch (Exception ex)
                    {
                        LOGGER.log("Failed to create a new anonymous session context.", ex.initCause(se));
                    }
                }
            }
            catch(FileNotFoundException fnf)
            {
                LOGGER.log("Exception in T8Client hearbeat.", fnf);
                Toast.show("Failed to contact server, please check your connection.", Toast.Style.ERROR);

            }
            catch (Exception e)
            {
                LOGGER.log("Exception in T8Client hearbeat.", e);
            }
        }

        private void showLogin()
        {
            // Restart the client frame.
            clientFrame.getFlowClient().stopComponent();
            clientFrame.startup();
            SwingUtilities.invokeLater(() ->
            {
                JOptionPane.showMessageDialog(clientFrame, "Your session has expired, please log in again.");
            });
        }

        private void logout() throws Exception
        {
            if (securityManager.isSessionActive(context, context.getSessionContext().getSessionIdentifier()))
            {
                securityManager.logout(context);
                showLogin();
            }
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tech 8 Client");
        setName("Tech 8 Developer"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        startup();
    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        shutdown();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        for (String arg : args)
        {
            if (arg.contains("=") && !arg.startsWith("="))
            {
                System.setProperty(arg.substring(0, arg.indexOf('=')), arg.substring(arg.indexOf('=')+1));
            }
        }

        SwingTimerTimingSource timingSource;
        final String startupModule;

        timingSource = new SwingTimerTimingSource(40, TimeUnit.MILLISECONDS);
        Animator.setDefaultTimingSource(timingSource);
        timingSource.init();

        //Get the startup Module Identifier
        if(args.length > 0 && args[0].contains("STARTUP_MODULE"))
        {
            startupModule = args[0].replace("STARTUP_MODULE=", "");
        } else startupModule = "@M_MAIN";

        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new T8ClientFrame(startupModule).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
