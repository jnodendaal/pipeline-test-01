package com.pilog.t8.api;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser.FileSelectionMode;
import java.io.File;

/**
 * @author Bouwer du Preez
 */
public class T8ClientFileSystemApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_CLIENT_FILE_SYSTEM";

    private static File LAST_SELECTED_FOLDER = null;
    private static File LAST_SELECTED_FILE = null;

    public T8ClientFileSystemApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public File showInputFileSelectionDialog(String fileExtension, String fileDescription) throws Exception
    {
        return showInputFileSelectionDialog(fileExtension, fileDescription, FileSelectionMode.FILES);
    }

    public File showInputFolderSelectionDialog(String fileExtension, String fileDescription) throws Exception
    {
        return showInputFileSelectionDialog(fileExtension, fileDescription, FileSelectionMode.DIRECTORIES);
    }

    private File showInputFileSelectionDialog(String fileExtension, String fileDescription, FileSelectionMode selectionMode) throws Exception
    {
        File selectedFile;
        String filePath;

        // Display the dialog to allow the user to select a file.
        filePath = BasicFileChooser.getLoadFilePath(clientContext.getParentWindow(), fileExtension, fileDescription, LAST_SELECTED_FILE, selectionMode);
        selectedFile = filePath != null ? new File(filePath) : null;

        // If a valid file was selected, store its parent folder so that subsequent invocations of this method will reuse it.
        if (selectedFile != null)
        {
            LAST_SELECTED_FILE = selectedFile;
        }

        return selectedFile;
    }

    public File showOutputFileSelectionDialog(String suggestedFileName, String fileExtension, String fileDescription) throws Exception
    {
        return this.showOutputFileSelectionDialog(suggestedFileName, fileExtension, fileDescription, FileSelectionMode.FILES);
    }

    public File showOutputFolderSelectionDialog(String suggestedFileName, String fileExtension, String fileDescription) throws Exception
    {
        return this.showOutputFileSelectionDialog(suggestedFileName, fileExtension, fileDescription, FileSelectionMode.DIRECTORIES);
    }

    private File showOutputFileSelectionDialog(String suggestedFileName, String fileExtension, String fileDescription, FileSelectionMode selectionMode) throws Exception
    {
        String selectedFilePath;
        File selectedFile;
        File preselectedFile;

        // Determine the preselected file in the file chooser.
        if (LAST_SELECTED_FOLDER != null) preselectedFile = new File(LAST_SELECTED_FOLDER.getCanonicalFile() + "/" + suggestedFileName);
        else preselectedFile = new File(new File(suggestedFileName).getCanonicalPath());

        // Display the dialog to allow the user to select a file.
        selectedFilePath = BasicFileChooser.getSaveFilePath(clientContext.getParentWindow(), fileExtension, fileDescription, preselectedFile, selectionMode);
        selectedFile = selectedFilePath != null ? new File(selectedFilePath) : null;

        // If a valid file was selected, store its parent folder so that subsequent invocations of this method will reuse it.
        if (selectedFile != null)
        {
            LAST_SELECTED_FOLDER = selectedFile;
            if ((selectedFile.isFile()) && (selectedFile.getParentFile() != null)) LAST_SELECTED_FOLDER = LAST_SELECTED_FOLDER.getParentFile();
        }

        // Return the selecte file.
        return selectedFile;
    }
}
