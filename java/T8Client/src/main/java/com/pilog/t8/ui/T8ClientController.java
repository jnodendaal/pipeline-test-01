package com.pilog.t8.ui;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.client.T8DefaultClientManagerFactory;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8Client;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ClientOperation;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionEditorFactory;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8RootPane;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.help.T8HelpDisplayDefinition;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ClientControllerApiHandler;
import com.pilog.t8.definition.ui.T8ClientControllerDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.componentcontainer.T8AsynchronousOperationTask;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.flowclient.T8AdministrationConsole;
import com.pilog.t8.ui.flowclient.T8FlowClientOperationHandler;
import com.pilog.t8.ui.flowclient.T8FlowPhasePanel;
import com.pilog.t8.ui.flowclient.T8FlowTaskExecutor;
import com.pilog.t8.ui.module.T8Module;
import com.pilog.t8.ui.notification.T8ClientNotificationCentre;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.awt.BorderLayout;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8ClientController extends javax.swing.JPanel implements T8Client, T8RootPane
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClientController.class);

    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8Context context;
    private T8ProcessManager processManager;
    private T8ServiceManager serviceManager;
    private T8FlowManager flowManager;
    private T8FunctionalityManager functionalityManager;
    private T8CommunicationManager communicationManager;
    private T8NotificationManager notificationManager;
    private T8ConfigurationManager configurationManager;
    private T8SecurityManager securityManager;
    private T8DefinitionManager definitionManager;
    private T8FileManager fileManager;
    private T8ComponentController parentController;
    private T8DefaultComponentController controller;
    private T8DefaultComponentContainer componentContainer;
    private T8ClientControllerDefinition definition;
    private T8FlowClientOperationHandler operationHandler;
    private T8Module module;
    private T8FlowPhasePanel phasePanel;
    private T8WorkFlowDefinition flowDefinition;  // This variable will only be used during development when a flow is tested before being saved to the database.
    private T8ClientScriptRunner scriptRunner;
    private String flowIid;
    private T8FlowTaskExecutor taskExecutorThread;
    private KeyEventDispatcher globalKeyEventDispatcher;
    private T8ClientEnvironmentStatus environmentStatus;
    private T8ClientNotificationCentre notificationCentre;

    /**
     * Constructor to use for building a root flow client.
     * @param inputContext
     */
    public T8ClientController(T8Context inputContext)
    {
        try
        {
            T8DefaultClientManagerFactory managerFactory;

            this.sessionContext = inputContext.getSessionContext();
            if (sessionContext == null) throw new RuntimeException("Cannot construct flow client with null session context parameter.");
            this.clientContext = new T8ClientContext(this, sessionContext); // This line is very important.  Each Flow client constitutes a new client context.
            this.context = new T8Context(clientContext, sessionContext);
            this.context.setContext(inputContext);
            managerFactory = new T8DefaultClientManagerFactory(context);

            this.definition = new T8ClientControllerDefinition("MAIN_FLOW_CLIENT");
            this.parentController = null;
            this.componentContainer = new T8DefaultComponentContainer();
            this.sessionContext = context.getSessionContext();
            this.securityManager = managerFactory.constructSecurityManager();
            this.definitionManager = managerFactory.constructDefinitionManager();
            this.processManager = managerFactory.constructProcessManager();
            this.communicationManager = managerFactory.constructCommunicationManager();
            this.notificationManager = managerFactory.constructNotificationManager();
            this.serviceManager = managerFactory.constructServiceManager();
            this.fileManager = managerFactory.constructFileManager();
            this.flowManager = managerFactory.constructFlowManager();
            this.functionalityManager = managerFactory.constructFunctionalityManager();
            this.configurationManager = managerFactory.constructConfigurationManager();
            this.controller = new T8DefaultComponentController(context, "ROOT_FLOW_CLIENT", false);
            this.controller.setRootComponent(this);
            this.controller.setContainer(componentContainer);
            this.scriptRunner = new T8ClientScriptRunner(controller);
            this.operationHandler = new T8FlowClientOperationHandler(this);
            this.globalKeyEventDispatcher = new GlobalKeyEventDispatcher();
            this.phasePanel = new T8FlowPhasePanel();

            initComponents();
            add(componentContainer, BorderLayout.CENTER);
        }
        catch (RuntimeException e)
        {
            LOGGER.log("Exception while constructing flow client.", e);
        }
    }

    /**
     * Constructor to use for building a non-root flow client.
     * @param definition
     * @param controller
     */
    public T8ClientController(T8ClientControllerDefinition definition, T8ComponentController controller)
    {
        if (controller == null) throw new RuntimeException("Cannot construct flow client with null controller parameter.");
        this.clientContext = new T8ClientContext(this, controller.getSessionContext()); // This line is very important.  Each Flow client constitutes a new client context.
        this.definition = definition;
        this.parentController = controller;
        this.componentContainer = new T8DefaultComponentContainer();
        this.sessionContext = controller.getSessionContext();
        this.securityManager = controller.getClientContext().getSecurityManager();
        this.processManager = controller.getClientContext().getProcessManager();
        this.serviceManager = controller.getClientContext().getServiceManager();
        this.communicationManager = controller.getClientContext().getCommunicationManager();
        this.notificationManager = controller.getClientContext().getNotificationManager();
        this.definitionManager = controller.getClientContext().getDefinitionManager();
        this.fileManager = controller.getClientContext().getFileManager();
        this.flowManager = controller.getClientContext().getFlowManager();
        this.functionalityManager = controller.getClientContext().getFunctionalityManager();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.controller = new T8DefaultComponentController(parentController, new T8ProjectContext(controller.getContext(), definition.getRootProjectId()), definition.getIdentifier(), false);
        this.controller.setRootComponent(this);
        this.controller.setContainer(componentContainer);
        this.context = controller.getContext();
        this.scriptRunner = new T8ClientScriptRunner(controller);
        this.operationHandler = new T8FlowClientOperationHandler(this);
        this.phasePanel = new T8FlowPhasePanel();

        initComponents();
        add(componentContainer, BorderLayout.CENTER);
    }

    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    public T8Context getAccessContext()
    {
        return context;
    }

    @Override
    public void dispayHelp(String helpId)
    {
        T8HelpDisplayDefinition helpDisplayDefinition;

        try
        {
            // Load and initialize the Module Definition.
            LOGGER.log("Displaying help using identifier: '" + helpId + "'");
            helpDisplayDefinition = (T8HelpDisplayDefinition)definitionManager.getInitializedDefinition(context, context.getProjectId(), helpId, null);
            if (helpDisplayDefinition != null)
            {
                //Have the instance execute the required display intiative
                helpDisplayDefinition.getHelpDisplayInstance().display();
            } else throw new RuntimeException("Display Help Definition not found: " + helpId);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to display the help for identifier : " + helpId, ex);
        }
    }

    private class GlobalKeyEventDispatcher implements KeyEventDispatcher
    {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e)
        {
            if (e.getID() == KeyEvent.KEY_PRESSED)
            {
                if (e.isControlDown() && e.isAltDown() && (e.getKeyCode() == KeyEvent.VK_8))
                {
                    clientContext.showAdministrationConsole();

                    // Only one of these events have to be processed.
                    return true;
                }
            }

            // Allow the event to be redispatched.
            return false;
        }
    }

    public void setNotificationCentre(T8ClientNotificationCentre notificationCentre)
    {
        this.notificationCentre = notificationCentre;
    }

    @Override
    public void refreshNotifications()
    {
        if (notificationCentre != null)
        {
            notificationCentre.refreshNotifications();
        }
    }

    @Override
    public void showAdministrationConsole()
    {
        new T8AdministrationConsole(clientContext, sessionContext).setVisible(true);
    }

    @Override
    public T8DefinitionEditorFactory getDefinitionEditorFactory(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.T8DefaultDefinitionEditorFactory", new Class<?>[]{T8ClientContext.class}, clientContext);
    }

    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public void startNewFlow(String flowId)
    {
        try
        {
            T8FlowStatus flowStatus;

            this.flowDefinition = null;
            flowStatus = flowManager.startFlow(context, flowId, null);
            if (flowStatus != null)
            {
                switchFlow(flowStatus.getFlowIid(), null);
            }
            else throw new Exception("Flow definition '" + flowDefinition.getIdentifier() + "' could not be started on the server.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while starting new flow client using identifier: " + flowId, e);
        }
    }

    public void switchFlow(String flowIid, String taskIid) throws Exception
    {
        // Check that we don't ever call this from EDT.
        if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Cannot call method from EDT.");

        // Stop the currently execution task.
        stopTaskExecution();

        // Set the new flow instance identifier
        this.flowIid = flowIid;

        // Resume client execution.
        taskExecutorThread = new T8FlowTaskExecutor(this, controller, flowIid, taskIid);
        taskExecutorThread.start();
    }

    public void stopTaskExecution() throws Exception
    {
        // If there is a current task executor thread running, stop its execution and wait for it to complete.
        if (taskExecutorThread != null)
        {
            taskExecutorThread.stopExecution();
            if (Thread.currentThread() != taskExecutorThread) taskExecutorThread.join(); // Make sure not to join to the thread itself.
            taskExecutorThread = null;
        }
    }

    @Override
    public Window getParentWindow()
    {
        return SwingUtilities.getWindowAncestor(this);
    }

    public void setFlowWaitingStatus(T8FlowStatus waitingStatus)
    {
        // Make sure the UI is unlocked.
        unlockUI();

        // Set the latest flow status on the phase panel.
        System.out.println("Setting flow status: " + waitingStatus + " with phases: " + waitingStatus.getPhases());
        phasePanel.setFlowStatus(waitingStatus);
        System.out.println("Done setting flow status: " + waitingStatus + " with phases: " + waitingStatus.getPhases());

        // Set the phase panel as the visible content component.
        System.out.println("Setting container to flow phase panel: " + waitingStatus.getPhases());
        componentContainer.setComponent(phasePanel);
        System.out.println("Done setting container to flow phase panel: " + waitingStatus.getPhases());
    }

    @Override
    public void lockUI(String message)
    {
        componentContainer.setMessage(message);
        componentContainer.setLocked(true);
        if (module != null) module.lockUI(message);
    }

    @Override
    public void unlockUI()
    {
        componentContainer.setLocked(false);
        if (module != null) module.unlockUI();
    }

    public void loadModule(final String moduleId, final Map<String, Object> inputParameters) throws Exception
    {
        final T8ModuleDefinition moduleDefinition;

        if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Cannot be called from EDT.");

        // Load and initialize the Module Definition.
        LOGGER.log("Loading module '" + moduleId + "' from project '" + context.getProjectId() + "' with input parameters: " + inputParameters);
        moduleDefinition = (T8ModuleDefinition)definitionManager.getInitializedDefinition(context, context.getProjectId(), moduleId, inputParameters);
        if (moduleDefinition == null) throw new RuntimeException("Module Definition not found: " + moduleId);
        else
        {
            try
            {
                SwingUtilities.invokeAndWait(new Thread()
                {
                    @Override
                    public void run()
                    {
                        loadModule(moduleDefinition, inputParameters);
                    }
                });
            }
            catch (InterruptedException | InvocationTargetException e)
            {
                LOGGER.log(e);
            }
        }
    }

    /**
     * This method is mainly used by the T8Developer during testing and loads
     * a module directly from the supplied definition.
     *
     * @param moduleDefinition The definition from which the module will be
     * loaded.
     * @param inputParameters The input parameter that will be used to
     * initialize the module.
     */
    public void loadModule(T8ModuleDefinition moduleDefinition, Map<String, Object> inputParameters)
    {
        if (!SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Must be called from EDT.");

        LOGGER.log("Loading module '" + moduleDefinition + "' with input parameters: " + inputParameters);
        lockUI(translate("Loading Module..."));

        // If a module is already loaded, stop it first before replacing it.
        if (module != null)
        {
            module.stopComponent();
        }

        // Now create the new module and add it to the content pane.
        module = new T8Module(controller, moduleDefinition);
        componentContainer.setComponent(module);

        // Initialize the module.
        module.initializeComponent(inputParameters);

        // Start the module.
        module.startComponent();

        // Show the content panel.
        unlockUI();
    }

    @Override
    public T8DefinitionManager getDefinitionManager()
    {
        return definitionManager;
    }

    @Override
    public T8CommunicationManager getCommunicationManager()
    {
        return communicationManager;
    }

    @Override
    public T8NotificationManager getNotificationManager()
    {
        return notificationManager;
    }

    @Override
    public T8ProcessManager getProcessManager()
    {
        return processManager;
    }

    @Override
    public T8ServiceManager getServiceManager()
    {
        return serviceManager;
    }

    @Override
    public T8SecurityManager getSecurityManager()
    {
        return securityManager;
    }

    @Override
    public T8FlowManager getFlowManager()
    {
        return flowManager;
    }

    @Override
    public T8FunctionalityManager getFunctionalityManager()
    {
        return functionalityManager;
    }

    @Override
    public T8FileManager getFileManager()
    {
        return fileManager;
    }

    @Override
    public T8ConfigurationManager getConfigurationManager()
    {
        return configurationManager;
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return new T8ClientExpressionEvaluator(context);
    }

    @Override
    public Map<String, Object> executeClientScript(T8ScriptDefinition scriptDefinition, Map<String, Object> inputParameters) throws Exception
    {
        return (Map<String, Object>)scriptRunner.runScript(scriptDefinition, inputParameters);
    }

    public void setEnvironmentStatus(T8ClientEnvironmentStatus status)
    {
        this.environmentStatus = status;
    }

    @Override
    public T8ClientEnvironmentStatus getEnvironmentStatus()
    {
        return environmentStatus;
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    //This method should only ever be called by the heartbeat thread to set a new anonymous sessio
    public void setSessionContext(T8SessionContext sessionContext)
    {
        stopComponent();
        this.sessionContext = sessionContext;
        startComponent();
    }

    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        try
        {
            return operationHandler.executeOperation(operationIdentifier, operationParameters);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while executing operation: " + operationIdentifier + " Using Parameters: " + operationParameters, e);
            return null;
        }
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
        // Make sure to only start one task runner thread.
        if ((taskExecutorThread == null) || (!taskExecutorThread.isAlive()))
        {
            // Add the global key event dispatcher.
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(globalKeyEventDispatcher);
        }
    }

    @Override
    public void stopComponent()
    {
        LOGGER.log("Stopping client: " + definition.getIdentifier());

        // Remove the global key event dispatcher.
        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(globalKeyEventDispatcher);

        // Stop the currently execution task.
        try
        {
            stopTaskExecution();
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while stopping task execution during flow client shutdown.", e);
        }

        // If a module is currently loaded, stop it too.
        if (module != null)
        {
            module.stopComponent();
        }
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return module;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public T8FlowTask getCurrentTask()
    {
        if (taskExecutorThread != null)
        {
            return taskExecutorThread.getCurrentTask();
        }
        else return null;
    }

    public void unclaimCurrentTask() throws Exception
    {
        throw new Exception("Unclaiming a task via the flow client is not supported yet.");
    }

    protected void fireModuleLoadedEvent(String moduleIdentifier)
    {
        if (parentController != null)
        {
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8ClientControllerApiHandler.PARAMETER_MODULE_IDENTIFIER, moduleIdentifier);
            parentController.reportEvent(T8ClientController.this, definition.getComponentEventDefinition(T8ClientControllerApiHandler.EVENT_MODULE_LOADED), eventParameters);
        }
    }

    public void fireTasksCompletedEvent()
    {
        if (parentController != null)
        {
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8ClientControllerApiHandler.PARAMETER_FLOW_INSTANCE_IDENTIFIER, flowIid);
            parentController.reportEvent(T8ClientController.this, definition.getComponentEventDefinition(T8ClientControllerApiHandler.EVENT_TASKS_COMPLETED), eventParameters);
        }
    }

    public void fireTaskCompletedEvent(String flowIID, String taskIID)
    {
        if (parentController != null)
        {
            Map<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8ClientControllerApiHandler.PARAMETER_FLOW_INSTANCE_IDENTIFIER, flowIID);
            eventParameters.put(T8ClientControllerApiHandler.PARAMETER_TASK_INSTANCE_IDENTIFIER, taskIID);
            parentController.reportEvent(T8ClientController.this, definition.getComponentEventDefinition(T8ClientControllerApiHandler.EVENT_TASK_COMPLETED), eventParameters);
        }
    }

    @Override
    public T8ServerOperationStatusReport getServerOperationStatus(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport stopServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport cancelServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport pauseServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport resumeServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport executeAsynchronousOperation(final T8Context context, final String operationId, Map<String, Object> operationParameters) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Object> executeAsynchronousServerOperation(final T8Context context, final String operationId, Map<String, Object> operationParameters, final String message) throws Exception
    {
        try
        {
            T8ServerOperationStatusReport statusReport;
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;
            String operationIid;
            T8AsynchronousOperationTask task;
            Exception exception;

            // Execute the operation.
            inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
            statusReport = T8MainServerClient.executeAsynchronousOperation(context, operationId, inputParameters);
            operationIid = statusReport.getOperationInstanceIdentifier();

            // Set the progress layer message and then lock the layer during execution of the operation.
            task = new T8AsynchronousOperationTask(context, operationIid, message);
            componentContainer.setProgressUpdateInterval(500);
            componentContainer.addTask(task);
            lockUI(message);

            // Wait for execution to complete.
            synchronized(task)
            {
                while (task.isActive())
                {
                    try
                    {
                        /*
                            Wait for this task to complete, the component container will call notify on the task object once it is complete,
                            if an exception occurs or the component container failed to call the notify some reason then this thread will wait
                            for the amount specified after which it will recheck the validation in any case and continue.
                        */
                        task.wait(10 * 1000);
                    }
                    catch (IllegalMonitorStateException | InterruptedException e)
                    {
                        LOGGER.log("Exception while waiting for asynchronous task to complete.", e);
                    }
                }
            }

            // Return the output of the operation.
            exception = task.getException();
            outputParameters = task.getResult();
            if (exception != null) throw exception;
            else return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
        }
        finally
        {
            // Unlock the layer.
            unlockUI();
            componentContainer.setIndeterminate(true);
        }
    }

    @Override
    public Map<String, Object> executeSynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters, String message) throws Exception
    {
        try
        {
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;

            // Set the progress layer message and then lock the layer during execution of the operation.
            lockUI(message != null ? message : translate("Processing..."));

            // Execute the operation.
            inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
            outputParameters = T8MainServerClient.executeSynchronousOperation(context, operationId, inputParameters);

            // Return the output of the operation.
            return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
        }
        finally
        {
            // Unlock the layer.
            unlockUI();
        }
    }

    @Override
    public Map<String, Object> executeClientOperation(T8Context context, String operationId, Map<String, Object> inputParameters) throws Exception
    {
        T8ClientOperationDefinition operationDefinition;

        operationDefinition = (T8ClientOperationDefinition)clientContext.getDefinitionManager().getRawDefinition(context, context.getProjectId(), operationId);
        if (operationDefinition != null)
        {
            T8ClientOperation operation;
            String operationInstanceIdentifier;

            operationInstanceIdentifier = null;
            operation = operationDefinition.getNewOperationInstance(controller, operationInstanceIdentifier);
            return operation.execute(sessionContext, inputParameters);
        }
        else throw new Exception("Operation not found: " + operationId);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
