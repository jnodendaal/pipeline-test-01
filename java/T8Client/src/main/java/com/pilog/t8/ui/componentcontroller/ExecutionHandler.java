package com.pilog.t8.ui.componentcontroller;

import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.T8Log;
import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ExecutionHandler
{
    private final T8ComponentController parentController;
    private final T8ClientScriptRunner scriptRunner;
    private final Executor executor;
    private final Thread processorThread;

    public ExecutionHandler(T8ComponentController parentController)
    {
        this.parentController = parentController;
        this.scriptRunner = new T8ClientScriptRunner(parentController);
        this.executor = new Executor(parentController.getId() + "_EventProcessor");
        this.processorThread = new Thread(executor);
    }

    public void start()
    {
        processorThread.start();
    }

    public void stop()
    {
        executor.stop();
    }

    public Map<String, Object> executeScript(T8ClientContextScript script, Map<String, ?  extends Object> inputParameters) throws Exception
    {
        ScriptExecution scriptExecution;

        // Add the script execution to the executor.
        scriptExecution = new ScriptExecution(script, inputParameters);
        executor.addEventToQueue(scriptExecution);

        // Wait for execution to complete.
        while (!scriptExecution.hasCompleted())
        {
            synchronized (scriptExecution)
            {
                scriptExecution.wait();
            }
        }

        // Return the execution output parameters.
        return scriptExecution.getOutputParameters();
    }

    public void handleEvent(T8Component component, T8ComponentEventDefinition eventDefinition, Map<String, Object> eventParameters)
    {
        if (component == null) throw new RuntimeException("Null source component encountered while handling event.");
        if (eventDefinition == null) throw new RuntimeException("Null event definition encountered while handling event on component: " + component.getComponentDefinition());

        T8Log.log("Component: " + component.getComponentDefinition().getIdentifier() + ", Event: " +  eventDefinition.getIdentifier() + ", Event Parameters: " + eventParameters + ", Controller: " + parentController.getId());
        executor.addEventToQueue(new EventExecution(scriptRunner, component, eventDefinition, eventParameters));
    }

    private class Executor implements Runnable
    {
        private final LinkedList<Execution> executionQueue;
        private final String processorName;
        private boolean stopFlag;

        private Executor(String processorName)
        {
            this.processorName = processorName;
            executionQueue = new LinkedList<Execution>();
            stopFlag = false;
        }

        public void stop()
        {
            synchronized (executionQueue)
            {
                stopFlag = true;
                executionQueue.notifyAll();
            }
        }

        public void addEventToQueue(Execution event)
        {
            synchronized (executionQueue)
            {
                executionQueue.add(event);
                executionQueue.notifyAll();
            }
        }

        private Execution getNextExecution()
        {
            synchronized (executionQueue)
            {
                return executionQueue.size() > 0 ? executionQueue.pop() : null;
            }
        }

        @Override
        public void run()
        {
            Thread.currentThread().setName(processorName);
            while (!stopFlag)
            {
                Execution nextExecution;

                // While there are no events in the queue, wait.
                synchronized (executionQueue)
                {
                    try
                    {
                        while ((executionQueue.size() == 0) && (!stopFlag))
                        {
                            executionQueue.wait();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    // Continue to process events in the queue.
                    nextExecution = getNextExecution();
                }

                // Process the next execution.
                if (nextExecution != null)
                {
                    nextExecution.execute();
                }
            }
        }
    }
}
