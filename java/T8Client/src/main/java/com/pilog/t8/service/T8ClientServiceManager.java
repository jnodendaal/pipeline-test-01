package com.pilog.t8.service;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.service.T8ServiceManagerAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientServiceManager implements T8ServiceManager
{
    private final T8Context context;

    public T8ClientServiceManager(T8Context context)
    {
        this.context = context;
    }

    @Override
    public void init()
    {
        // Not applicable from client-side.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void destroy()
    {
        // Not applicable from client-side.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8Service getService(T8Context context, String serviceIdentifier)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Object> executeServiceOperation(T8Context context, String serviceIdentifier, String operationIdentifier, Map<String, Object> inputParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_SERVICE_IDENTIFIER, serviceIdentifier);
        operationParameters.put(PARAMETER_OPERATION_IDENTIFIER, operationIdentifier);
        operationParameters.put(PARAMETER_INPUT_PARAMETER_MAP, inputParameters);
        return (Map<String, Object>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_EXECUTE_SERVICE_OPERATION, operationParameters).get(PARAMETER_OUTPUT_PARAMETER_MAP);
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
