package com.pilog.t8.ui.module;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8RootPane;
import com.pilog.t8.definition.script.T8ModuleOperationScriptDefinition;
import com.pilog.t8.definition.script.T8ModuleScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleAPIHandler;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.flowclient.T8AdministrationConsole;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * @author Bouwer du Preez
 */
public class T8Module extends JPanel implements T8RootPane
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8Module.class);

    private final T8ComponentController controller;
    private final T8ComponentController parentController;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8Context context;
    private Map<String, Object> inputParameters;
    private final T8ModuleDefinition definition;
    private T8Component rootComponent;
    private final HashMap<String, T8Component> additionalComponents;

    public T8Module(T8ComponentController parentController, T8ModuleDefinition definition)
    {
        this.definition = definition;
        this.clientContext = parentController.getClientContext();
        this.sessionContext = parentController.getSessionContext();
        this.context = new T8Context(parentController.getContext());
        this.parentController = parentController;
        this.controller = new T8ModuleComponentController(context, parentController, this); // The module uses its own internal controller.
        this.additionalComponents = new HashMap<>();
        initComponents();
        setOpaque(definition.isOpaque());
        registerHelpKeyBinding();
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return inputParameters;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        try
        {
            return this.controller.executeScript(operationIdentifier, operationParameters);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while executing operation '" + operationIdentifier + "' using parameters: " + operationParameters, e);
            return null;
        }
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.inputParameters = inputParameters;
        LOGGER.log("Module {"+this.definition.getIdentifier()+"} input parameters: " + inputParameters);

        // Initialize the Module root component and its descendents according to the definition.
        if (definition.getRootComponentDefinition() != null) controller.addComponent(this, definition.getRootComponentDefinition(), inputParameters);

        // Set all of the module scripts to the controller.
        for (T8ModuleScriptDefinition scriptDefinition : definition.getModuleScriptDefinitions())
        {
            controller.addScript(scriptDefinition);
        }

        // Set all of the module operation scripts to the controller.
        for (T8ModuleOperationScriptDefinition scriptDefinition : definition.getModuleOperationScriptDefinitions())
        {
            controller.addScript(scriptDefinition);
        }

        // Initialize all of the additional Module components.
        for (T8ComponentDefinition componentDefinition : definition.getAdditionalComponentDefinitions())
        {
            controller.addComponent(this, componentDefinition, inputParameters);
        }
    }

    @Override
    public void startComponent()
    {
        // Start the component controller.
        controller.start();

        // Signal Module Start.
        LOGGER.log("Signalling Module start with input parameters: " + inputParameters);
        controller.reportEvent(this, definition.getComponentEventDefinition(T8ModuleAPIHandler.EVENT_MODULE_STARTED), inputParameters);

        requestFocus();
        transferFocus();
    }

    @Override
    public void stopComponent()
    {
        // Stop the component controller.
        controller.stop();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if ((rootComponent == null) && (definition.getRootComponentDefinition().getIdentifier().equals(childComponent.getComponentDefinition().getIdentifier())))
        {
            rootComponent = childComponent;
            add((Component)rootComponent, java.awt.BorderLayout.CENTER);
            validate();
        }
        else
        {
            additionalComponents.put(childComponent.getComponentDefinition().getIdentifier(), childComponent);
        }
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        if (controller.findComponent(childComponentIdentifier) == rootComponent)
        {
            return rootComponent;
        }
        else return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        if (controller.findComponent(childComponentIdentifier) == rootComponent)
        {
            T8Component formerRootComponent;

            formerRootComponent = rootComponent;
            remove((Component)rootComponent);
            validate();
            rootComponent = null;
            return formerRootComponent;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<>();
        if (rootComponent != null) childComponents.add(rootComponent);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return rootComponent == null ? 0 : 1;
    }

    @Override
    public void clearChildComponents()
    {
        if (rootComponent != null)
        {
            rootComponent = null;
            remove((Component)rootComponent);
            validate();
        }
    }

    @Override
    public void lockUI(String message)
    {
        for (T8ComponentDefinition t8ComponentDefinition : definition.getAdditionalComponentDefinitions())
        {
            T8Component childComponent;

            childComponent = controller.findComponent(t8ComponentDefinition.getIdentifier());
            if(childComponent instanceof T8RootPane)
            {
                ((T8RootPane)childComponent).lockUI(message);
            }
        }
    }

    @Override
    public void unlockUI()
    {
        for (T8ComponentDefinition t8ComponentDefinition : definition.getAdditionalComponentDefinitions())
        {
            T8Component childComponent;

            childComponent = controller.findComponent(t8ComponentDefinition.getIdentifier());
            if(childComponent instanceof T8RootPane) ((T8RootPane)childComponent).unlockUI();
        }
    }

    public Collection<String> getVariableIds()
    {
        return controller.getVariableIds();
    }

    public void signalModuleEvent(String eventIdentifier, Map<String, Object> eventParameters)
    {
        LOGGER.log("Signalling module event: " + eventIdentifier + " Parameters: " + eventParameters);
        if (parentController != null)
        {
            T8ComponentEventDefinition eventDefinition;
            Map<String, Object> publicParameters;

            publicParameters = T8IdentifierUtilities.prependNamespace(definition.getIdentifier(), eventParameters);
            eventDefinition = definition.getComponentEventDefinition(eventIdentifier);
            if (eventDefinition != null)
            {
                parentController.reportEvent(this, eventDefinition, publicParameters);
            }
            else throw new RuntimeException("Module event '" + eventIdentifier + "' signalled in module '" + definition + "' but no corresponding definition found.");
        }
    }

    private void registerHelpKeyBinding()
    {
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_F11, InputEvent.CTRL_DOWN_MASK), "showModuleHelp");
        getActionMap().put("showModuleHelp", new AbstractAction("showModuleHelp")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                LOGGER.log("Module Identifier: " + getComponentDefinition());
                new T8AdministrationConsole(clientContext, sessionContext, T8Module.this, controller).setVisible(true);
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
