package com.pilog.t8.ui.componentcontroller;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8DialogHandler;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ClientOperation;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentContainer;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8AsynchronousOperationTask;
import com.pilog.t8.ui.T8ClientController;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultComponentController implements T8ComponentController
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultComponentController.class);

    private final T8ConfigurationManager configurationManager;
    private final ExecutionHandler executionHandler;
    private final LinkedHashMap<String, T8Component> components;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8Context context;
    private final T8ComponentController parentController;
    private final String identifier;
    private final Map<String, T8Api> apiCache;
    private final HashMap<String, ButtonGroup> buttonGroups;
    private final HashMap<String, Object> variables;
    private final HashMap<String, T8ClientContextScript> controllerScripts;
    private final EventListenerList eventListeners;
    private final T8DialogHandler dialogHandler;
    private T8FunctionalityController functionalityController;
    private T8ComponentContainer container;
    private T8Component rootComponent;

    public T8DefaultComponentController(T8Context context, String controllerId, boolean eventHandling)
    {
        this.clientContext = context.getClientContext();
        this.sessionContext = clientContext.getSessionContext();
        this.context = context;
        this.parentController = null;
        this.configurationManager = clientContext.getConfigurationManager();
        this.components = new LinkedHashMap<>();
        this.variables = new HashMap<>();
        this.buttonGroups = new HashMap<>();
        this.identifier = controllerId;
        this.eventListeners = new EventListenerList();
        this.dialogHandler = new T8DialogHandler(this);
        this.controllerScripts = new HashMap<>();
        this.apiCache = new HashMap<>();
        this.container = parentController != null ? parentController.getContainer() : null; // The default container for this controller is its parent's.

        // Only enable event handling if required.
        if (eventHandling) this.executionHandler = new ExecutionHandler(this);
        else executionHandler = null;
    }

    public T8DefaultComponentController(T8ComponentController parentController, T8Context accessContext, String controllerId, boolean eventHandling)
    {
        this.clientContext = parentController.getClientContext();
        this.sessionContext = clientContext.getSessionContext();
        this.context = new T8Context(parentController.getContext());
        this.context.add(accessContext);
        this.parentController = null;
        this.configurationManager = clientContext.getConfigurationManager();
        this.components = new LinkedHashMap<>();
        this.variables = new HashMap<>();
        this.buttonGroups = new HashMap<>();
        this.identifier = controllerId;
        this.eventListeners = new EventListenerList();
        this.dialogHandler = new T8DialogHandler(this);
        this.controllerScripts = new HashMap<>();
        this.apiCache = new HashMap<>();
        this.container = parentController.getContainer(); // The default container for this controller is its parent's.

        // Only enable event handling if required.
        if (eventHandling) this.executionHandler = new ExecutionHandler(this);
        else executionHandler = null;
    }

    @Override
    public void start()
    {
        // Initialize the event handler.
        if (executionHandler != null) executionHandler.start();

        // Start all components
        for (T8Component component : components.values())
        {
            // Make sure that we are not starting the root component because this might cause cyclic invocations.
            if (component != rootComponent)
            {
                component.startComponent();
            }
        }
    }

    @Override
    public void stop()
    {
        // Stop the dialog handler.
        dialogHandler.stopDialogs();

        // Make sure to stop the event handler threads.
        if (executionHandler != null) executionHandler.stop();

        // Stop all components
        for (T8Component component : components.values())
        {
            // Make sure that we are not stopping the root component because this might cause cyclic invocations.
            if (component != rootComponent)
            {
                component.stopComponent();
            }
        }
    }

    @Override
    public void addComponentEventListener(T8ComponentEventListener listener)
    {
        this.eventListeners.add(T8ComponentEventListener.class, listener);
    }

    @Override
    public void removeComponentEventListener(T8ComponentEventListener listener)
    {
        this.eventListeners.remove(T8ComponentEventListener.class, listener);
    }

    /**
     * Sets the container to which this controller belongs.
     * The default container for this controller is its parent's but using this method a new container can be set to override the default.
     * When one of the components managed by this controller executes a server operation or issues a request for the UI to be locked, this container will be used.
     * @param container The new container to be used by this controller and all of the components it manages.
     */
    @Override
    public void setContainer(T8ComponentContainer container)
    {
        this.container = container;
    }

    @Override
    public T8ComponentContainer getContainer()
    {
        return container;
    }

    @Override
    public Window getParentWindow()
    {
        // If a valid container is set for this controller, use the parent window of the container.
        if (container != null)
        {
            return SwingUtilities.windowForComponent((Component)container);
        }
        else return clientContext.getParentWindow();
    }

    @Override
    public T8Api getApi(String apiId)
    {
        // If this controller has the same access context as its parent, we can get the requested api from the parent controller.
        if ((parentController != null) && (this.context.isEquivalent(parentController.getContext())))
        {
            return parentController.getApi(apiId);
        }
        else
        {
            // We synchronize on the API cache so that it can not be altered while we are potentially making changes to it.
            synchronized (apiCache)
            {
                T8Api api;

                // First try to fetch the API from the cache.
                api = apiCache.get(apiId);
                if (api != null)
                {
                    return api;
                }
                else // The API could not be found in the cache, so create a new instance.
                {
                    try
                    {
                        Map<String, String> apiClassMap;

                        apiClassMap = configurationManager.getAPIClassMap(context);
                        if (apiClassMap != null)
                        {
                            String apiClassName;

                            apiClassName = apiClassMap.get(apiId);
                            if (!Strings.isNullOrEmpty(apiClassName))
                            {
                                api = T8Reflections.getFastFailInstance(apiClassName, new Class<?>[]{T8ComponentController.class}, this);

                                // Add the new instance to the API cache and then return it.
                                // TODO:  Some API facade's will not be cachable in future, so we will need to add a check for those cases.
                                apiCache.put(apiId, api);
                                return api;
                            }
                            else throw new Exception("API not found: " + apiId);
                        }
                        else throw new Exception("Class Mapping not found while trying to resolved API request: " + apiId);
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Could not initialize API: " + apiId, e);
                    }
                }
            }
        }
    }

    @Override
    public String translate(String phrase)
    {
        return clientContext.getConfigurationManager().getUITranslation(context, phrase);
    }

    @Override
    public void addScript(T8ComponentControllerScriptDefinition scriptDefinition)
    {
        String scriptIdentifier;

        scriptIdentifier = scriptDefinition.getIdentifier();
        if (!controllerScripts.containsKey(scriptIdentifier))
        {
            try
            {
                controllerScripts.put(scriptIdentifier, scriptDefinition.getNewScriptInstance(this));
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while instantiating controller script from definition: " + scriptDefinition, e);
            }
        }
        else throw new RuntimeException("Cannot add multiple controller scripts with the same identifier: " + scriptDefinition);
    }

    @Override
    public boolean removeScript(String scriptIdentifier)
    {
        return controllerScripts.remove(scriptIdentifier) != null;
    }

    @Override
    public Map<String, Object> executeScript(String scriptIdentifier, Map<String, ?  extends Object> inputParameters) throws Exception
    {
        T8ClientContextScript script;

        script = controllerScripts.get(scriptIdentifier);
        if (script != null)
        {
            return T8IdentifierUtilities.stripNamespace(script.executeScript(inputParameters));
        }
        else throw new RuntimeException("Controller script not found: " + scriptIdentifier);
    }

    @Override
    public String getId()
    {
        return identifier;
    }

    @Override
    public T8ComponentController getParentController()
    {
        return parentController;
    }

    @Override
    public T8FunctionalityController getFunctionalityController()
    {
        if (functionalityController != null)
        {
            return functionalityController;
        }
        else
        {
            return parentController != null ? parentController.getFunctionalityController() : null;
        }
    }

    public void setFunctionalityController(T8FunctionalityController functionalityController)
    {
        this.functionalityController = functionalityController;
    }

    /**
     * Sets the root for the hierarchy of components managed by this controller.
     * This method also registers the supplied component on the controller so
     * that is is accessible by other descendants.
     * @param component The component to set as the root of this controller.
     */
    public void setRootComponent(T8Component component)
    {
        this.rootComponent = component;
        this.components.put(component.getComponentDefinition().getIdentifier(), component);
    }

    @Override
    public void fireGlobalEvent(String eventIdentifier, Map<String, Object> eventParameters)
    {
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public void registerComponent(T8Component component)
    {
        String componentIdentifier;

        componentIdentifier = component.getComponentDefinition().getIdentifier();
        components.put(componentIdentifier, component);
    }

    @Override
    public T8Component deregisterComponent(String componentIdentifier)
    {
        return components.remove(componentIdentifier);
    }

    @Override
    public T8Component findComponent(String componentIdentifier)
    {
        return components.get(componentIdentifier);
    }

    @Override
    public void setVariable(String name, Object value)
    {
        variables.put(name, value);
    }

    @Override
    public Object getVariable(String name)
    {
        return variables.get(name);
    }

    @Override
    public List<String> getVariableIds()
    {
        return new ArrayList<>(variables.keySet());
    }

    @Override
    public void reportEvent(T8Component component, T8ComponentEventDefinition eventDefinition, Map<String, Object> eventParameters)
    {
        // If event handling is enabled, handle the event.
        if (executionHandler != null) executionHandler.handleEvent(component, eventDefinition, eventParameters);

        // Fire the event on all listeners.
        if (eventListeners.getListenerCount() > 0)
        {
            T8ComponentEvent event;

            event = new T8ComponentEvent(component, eventDefinition, eventParameters);
            for (T8ComponentEventListener listener : eventListeners.getListeners(T8ComponentEventListener.class))
            {
                listener.componentEvent(event);
            }
        }
    }

    @Override
    public T8Component getParentComponent(String componentIdentifier)
    {
        T8Component targetComponent;

        targetComponent = components.get(componentIdentifier);
        if (targetComponent != null)
        {
            for (T8Component component : components.values())
            {
                for (T8Component childComponent : component.getChildComponents())
                {
                    if (childComponent == targetComponent)
                    {
                        return component;
                    }
                }
            }

            return null;
        }
        else throw new IllegalArgumentException("Target component '" + componentIdentifier + "' not found in controller: " + identifier);
    }

    @Override
    public void loadComponent(String parentComponentId, String componentProjectId, String componentId, Map<String, Object> inputParameters)
    {
        // IMPORTANT:  The content of this method is currently a HACK and was
        // implemented solely for first-phase development of component
        // controller changes.
        { // HACK START
            if (rootComponent instanceof T8ClientController)
            {
                try
                {
                    ((T8ClientController)rootComponent).loadModule(componentId, inputParameters);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while loading module: " + componentId, e);
                }
            }
            else throw new IllegalStateException("Cannot call method without T8FlowClient as root.");
        } // HACK END
    }

    @Override
    public void addComponent(T8Component parentComponent, T8ComponentDefinition componentDefinition, Map<String, Object> inputParameters)
    {
        try
        {
            ArrayList<? extends T8ComponentDefinition> childComponentDefinitions;
            T8Component newComponent;
            long initializationTime;

            // Check some inputs.
            if (componentDefinition == null) throw new IllegalArgumentException("Component cannot be constructed from null definition in parent: " + parentComponent);

            // Record the start time of initialization.
            initializationTime = System.currentTimeMillis();

            // Get the new component instance from its definition.
            newComponent = componentDefinition.getNewComponentInstance(this);
            ((Component)newComponent).setVisible(componentDefinition.isVisible());

            // Initialize the new component instance.
            newComponent.initializeComponent(inputParameters);

            // Add the new component instance to its parent component.
            if (parentComponent != null) parentComponent.addChildComponent(newComponent, null);

            // Add all of the child components of the new component.
            components.put(componentDefinition.getIdentifier(), newComponent);

            // Record the total initialization time.
            initializationTime = (System.currentTimeMillis() - initializationTime);

            //If the initialization time is longer than a second report it
            if(initializationTime > 1000)
            {
                LOGGER.log(String.format("Suspicious initialization time for component %1$s: %2$s milliseconds.", componentDefinition.getIdentifier(), initializationTime));
            }

            // Add the child components of the initialized component.
            childComponentDefinitions = componentDefinition.getChildComponentDefinitions();
            if (childComponentDefinitions != null)
            {
                for (T8ComponentDefinition childComponentDefinition : componentDefinition.getChildComponentDefinitions())
                {
                    addComponent(newComponent, childComponentDefinition, inputParameters);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to add component. " + e.getMessage(), e);
        }
    }

    @Override
    public void addToButtonGroup(T8Component component, String buttonGroupIdentifier)
    {
        ButtonGroup buttonGroup;

        buttonGroup = buttonGroups.get(buttonGroupIdentifier);
        if (buttonGroup == null)
        {
            buttonGroup = new ButtonGroup();
            buttonGroups.put(buttonGroupIdentifier, buttonGroup);
        }

        buttonGroup.add((AbstractButton)component);
    }

    @Override
    public void removeFromButtonGroup(T8Component component, String buttonGroupIdentifier)
    {
        ButtonGroup buttonGroup;

        buttonGroup = buttonGroups.get(buttonGroupIdentifier);
        if (buttonGroup != null)
        {
            buttonGroup.remove((AbstractButton)component);
        }
        else throw new RuntimeException("Button group not found: " + buttonGroupIdentifier);
    }

    @Override
    public Map<String, Object> showDialog(String dialogId, Map<String, Object> inputParameters, boolean keepAlive)
    {
        return dialogHandler.showDialog(dialogId, inputParameters, keepAlive);
    }

    @Override
    public void lockUi(String message)
    {
        container.setMessage(message);
        container.setLocked(true);
    }

    @Override
    public void unlockUi()
    {
        container.setLocked(false);
    }

    @Override
    public T8ServerOperationStatusReport getServerOperationStatus(String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport stopServerOperation(String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport cancelServerOperation(String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport pauseServerOperation(String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport resumeServerOperation(String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport executeAsynchronousOperation(String operationId, Map<String, Object> operationParameters) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Object> executeAsynchronousServerOperation(String operationId, Map<String, Object> operationParameters, final String message) throws Exception
    {
        try
        {
            T8ServerOperationStatusReport statusReport;
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;
            String operationIid;
            T8AsynchronousOperationTask task;
            Exception exception;

            // Execute the operation.
            inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
            statusReport = T8MainServerClient.executeAsynchronousOperation(context, operationId, inputParameters);
            operationIid = statusReport.getOperationInstanceIdentifier();

            // Set the progress layer message and then lock the layer during execution of the operation.
            task = new T8AsynchronousOperationTask(context, operationIid, message);
            container.setProgressUpdateInterval(500);
            container.addTask(task);
            lockUi(message);

            // Wait for execution to complete.
            synchronized(task)
            {
                while (task.isActive())
                {
                    try
                    {
                        /*
                            Wait for this task to complete, the component container will call notify on the task object once it is complete,
                            if an exception occurs or the component container failed to call the notify some reason then this thread will wait
                            for the amount specified after which it will recheck the validation in any case and continue.
                        */
                        task.wait(10 * 1000);
                    }
                    catch (IllegalMonitorStateException | InterruptedException e)
                    {
                        LOGGER.log("Exception while waiting for asynchronous task to complete.", e);
                    }
                }
            }

            // Return the output of the operation.
            exception = task.getException();
            outputParameters = task.getResult();
            if (exception != null) throw exception;
            else return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
        }
        finally
        {
            // Unlock the layer.
            unlockUi();
            container.setIndeterminate(true);
        }
    }

    @Override
    public Map<String, Object> executeSynchronousServerOperation(String operationId, Map<String, Object> operationParameters, String message) throws Exception
    {
        try
        {
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;

            // Set the progress layer message and then lock the layer during execution of the operation.
            lockUi(message != null ? message : translate("Processing..."));

            // Execute the operation.
            inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
            outputParameters = T8MainServerClient.executeSynchronousOperation(context, operationId, inputParameters);

            // Return the output of the operation.
            return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
        }
        finally
        {
            // Unlock the layer.
            unlockUi();
        }
    }

    @Override
    public Map<String, Object> executeClientOperation(String operationid, Map<String, Object> inputParameters) throws Exception
    {
        T8ClientOperationDefinition operationDefinition;

        operationDefinition = (T8ClientOperationDefinition)clientContext.getDefinitionManager().getRawDefinition(context, context.getProjectId(), operationid);
        if (operationDefinition != null)
        {
            T8ClientOperation operation;
            String operationInstanceIdentifier;

            operationInstanceIdentifier = null;
            operation = operationDefinition.getNewOperationInstance(this, operationInstanceIdentifier);
            return operation.execute(sessionContext, inputParameters);
        }
        else throw new Exception("Operation not found: " + operationid);
    }
}
