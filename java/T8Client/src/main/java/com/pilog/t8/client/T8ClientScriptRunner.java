package com.pilog.t8.client;

import com.pilog.epic.EPIC;
import com.pilog.epic.ParserContext;
import com.pilog.epic.Program;
import com.pilog.t8.client.definition.T8ClientDefinitionManagerScriptFacade;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.T8ClientDataManagerScriptFacade;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.data.document.path.EpicDocPathParser;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.file.T8ClientFileManagerScriptFacade;
import com.pilog.t8.flow.T8ClientFlowManagerScriptFacade;
import com.pilog.t8.process.T8ClientProcessManagerScriptFacade;
import com.pilog.t8.security.T8ClientSecurityManagerScriptFacade;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ClientScriptRunner
{
    private final T8ComponentController componentController;
    private final DocPathEvaluatorFactory docPathFactory;
    private T8ScriptDefinition scriptDefinition;
    private Program program;
    private String namespace;

    public T8ClientScriptRunner(T8ComponentController componentController)
    {
        this.componentController = componentController;
        this.docPathFactory = DocPathEvaluatorFactory.getFactory(componentController.getContext());
    }

    public void prepareScript(T8ScriptDefinition scriptDefinition) throws Exception
    {
        List<T8ScriptMethodImport> scriptSpecificMethodImports;
        List<T8ScriptClassImport> scriptSpecificClassImports;
        T8ScriptConfigurationHandler scriptConfigurationHandler;
        ParserContext parserContext;

        this.scriptDefinition = scriptDefinition;

        // Create a new parser context.
        parserContext = new ParserContext();
        parserContext.addExternalExpressionParser(new EpicDocPathParser(docPathFactory));
        scriptConfigurationHandler = new T8ScriptConfigurationHandler(componentController, docPathFactory);
        scriptConfigurationHandler.configureParserContext(parserContext);

        // Get the script namespaces, compile the script and add all default imports.
        namespace = scriptDefinition.getNamespace();
        program = EPIC.compileProgram(scriptDefinition.getScript(), "UTF-8", parserContext);

        // Add any script-specific class imports that are available.
        scriptSpecificClassImports = scriptDefinition.getScriptClassImports();
        if (scriptSpecificClassImports != null)
        {
            for (T8ScriptClassImport classImport : scriptSpecificClassImports)
            {
                program.addClassImport(classImport.getReference(), classImport.getClassToImport());
            }
        }

        // Add any script-specific method imports that are available.
        scriptSpecificMethodImports = scriptDefinition.getScriptMethodImports();
        if (scriptSpecificMethodImports != null)
        {
            for (T8ScriptMethodImport methodImport : scriptSpecificMethodImports)
            {
                program.addImport(methodImport.getProcedureName(), methodImport.getContextObject(), methodImport.getMethod());
            }
        }
    }

    public void finalizeScript()
    {
        // Nothing to do here yet.
    }

    public Map<String, Object> runScript(Map<String, Object> inputParameters) throws Exception
    {
        Map<String, Object> scriptInputParameters;
        Map<String, Object> scriptOutputParameters;
        T8Context context;

        scriptInputParameters = T8IdentifierUtilities.stripNamespace(namespace, inputParameters, false);
        if (scriptInputParameters == null)
        {
            scriptInputParameters = new HashMap<String, Object>();
        }

        // Add the default script parameters.
        context = componentController.getContext();
        scriptInputParameters.put("session", context.getSessionContext());
        scriptInputParameters.put("dataManager", new T8ClientDataManagerScriptFacade(context));
        scriptInputParameters.put("processManager", new T8ClientProcessManagerScriptFacade(context, componentController.getClientContext().getProcessManager()));
        scriptInputParameters.put("flowManager", new T8ClientFlowManagerScriptFacade(context, componentController.getClientContext().getFlowManager()));
        scriptInputParameters.put("fileManager", new T8ClientFileManagerScriptFacade(context, componentController.getClientContext().getFileManager()));
        scriptInputParameters.put("definitionManager", new T8ClientDefinitionManagerScriptFacade(context, componentController.getClientContext().getDefinitionManager()));
        scriptInputParameters.put("securityManager", new T8ClientSecurityManagerScriptFacade(context, componentController.getClientContext().getSecurityManager()));

        // Execute the program.
        program.execute(scriptInputParameters);

        // Get the EPIC program output and add all the context output parameters.
        scriptOutputParameters = (Map<String, Object>)program.getReturnObject();

        // Convert all output parameters to script output parameters.
        scriptOutputParameters = T8IdentifierUtilities.prependNamespace(scriptDefinition.getNamespace(), scriptOutputParameters, false);
        return scriptOutputParameters;
    }

    public Map<String, Object> runScript(T8ScriptDefinition scriptDefinition, Map<String, Object> inputParameters) throws Exception
    {
        Map<String, Object> scriptOutputParameters;

        // Prepare the script.
        prepareScript(scriptDefinition);

        // Run the prepared script.
        scriptOutputParameters = runScript(inputParameters);

        // Finalize the script.
        finalizeScript();

        return scriptOutputParameters;
    }
}
