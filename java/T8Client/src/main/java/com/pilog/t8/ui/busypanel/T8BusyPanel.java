package com.pilog.t8.ui.busypanel;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RadialGradientPaint;
import java.awt.geom.Point2D;
import org.jdesktop.swingx.JXBusyLabel;

/**
 * @author Bouwer du Preez
 */
public class T8BusyPanel extends javax.swing.JPanel
{
    private JXBusyLabel busyLabel;

    private Color BG_COLOR_1 = new Color(255, 255, 255, 255);
    private Color BG_COLOR_2 = new Color(255, 255, 255, 255);
    private Color BG_COLOR_3 = new Color(255, 255, 255, 0);

    private float opacity = 1.0f;
//    private Animator animator;

    public T8BusyPanel(String message)
    {
        initComponents();
        setOpaque(false);
        setupIndicator();
        setMessage(message);
        jProgressBar.setVisible(false);
        jProgressBar.setStringPainted(true);
        jProgressBar.setMinimum(0);
        jProgressBar.setMaximum(100);
        jProgressBarCategory.setVisible(false);
        jProgressBarCategory.setStringPainted(true);
        jProgressBarCategory.setMinimum(0);
        jProgressBarCategory.setMaximum(100);

//        animator = new Animator.Builder()
//                .addTarget(PropertySetter.getTarget(this, "BG_COLOR_1", new Color(255, 255, 255, 255), new Color(255, 255, 255, 240)))
//                .addTarget(PropertySetter.getTarget(this, "BG_COLOR_2", new Color(255, 255, 255, 255), new Color(255, 255, 255, 190)))
//                .setDuration(2, TimeUnit.SECONDS)
//                .setEndBehavior(Animator.EndBehavior.RESET)
//                .setRepeatBehavior(Animator.RepeatBehavior.REVERSE)
//                .setRepeatCount(Animator.INFINITE)
//                .build();
    }

    public void setIndeterminate(boolean indeterminate)
    {
        // If the progress bar is active, it shows the message in any case so the label is not required.
        jProgressBar.setVisible(!indeterminate);
        jLabelMessage.setVisible(indeterminate);
    }

    public void setProgress(int progress)
    {
        jProgressBar.setValue(progress);
    }

    public void setCategoryProgress(int progress)
    {
        jProgressBarCategory.setVisible(progress > 0);
        jProgressBarCategory.setValue(progress);
    }

    private void setupIndicator()
    {
        busyLabel = new JXBusyLabel(new Dimension(100, 100));
        busyLabel.getBusyPainter().setPoints(12);
        busyLabel.getBusyPainter().setTrailLength(5);
        busyLabel.setOpaque(false);
        jPanelIndicator.add(busyLabel, java.awt.BorderLayout.CENTER);
    }

    public final void setMessage(String message)
    {
        jLabelMessage.setText(message);
        setProgressMessage(message);
    }

    public void setProgressMessage(String message)
    {
        jProgressBar.setString(message);

        revalidate();
    }

    public void setCategoryMessage(String message)
    {
        jProgressBarCategory.setString(message);

        revalidate();
    }

    public synchronized void setBusy(boolean busy)
    {
        busyLabel.setBusy(busy);

        //Start or stop that animation as is appropriate
//        if(busy)
//            if(!animator.isRunning()) animator.start();
//        else
//            if(animator.isRunning()) animator.stopAndAwait();
    }

    public boolean isBusy()
    {
        return busyLabel.isBusy();
    }

    public void setOpacity(float opacity)
    {
        this.opacity = opacity;
        repaint();
    }

    public float getOpacity()
    {
        return opacity;
    }

    public void setBG_COLOR_1(Color BG_COLOR_1)
    {
        this.BG_COLOR_1 = BG_COLOR_1;
        repaint();
    }

    public Color getBG_COLOR_1()
    {
        return this.BG_COLOR_1;
    }

    public void setBG_COLOR_2(Color BG_COLOR_2)
    {
        this.BG_COLOR_2 = BG_COLOR_2;
        repaint();
    }

    public Color getBG_COLOR_2()
    {
        return this.BG_COLOR_2;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        Point2D center;
        float fadeRadius;
        float[] gradientDistances;
        RadialGradientPaint radialPaint;
        Color[] gradientColors;
        int layerWidth;
        int layerHeight;
        Graphics2D g2;

        // Invoke the super paintComponent first.
        super.paintComponent(g);

        //Make a copy of the graphcs object. The one we recieved gets used by all of the swing components and we do not want to change settings on that one.
        g2 = (Graphics2D)g.create();
        g2.setComposite(AlphaComposite.SrcOver.derive(opacity));
        layerWidth = getWidth();
        layerHeight = getHeight();

        g2.setColor(new Color(255, 255, 255, 50));
        g2.fillRect(0, 0, layerWidth, layerHeight);

        // Paint a radial gradient so that the part where the busy indicator will be is totally opaque.
        center = new Point2D.Float(((float)layerWidth)/2.0f, ((float)layerHeight)/2.0f - 6f); // The -6 compensates for the label below the busy indicator.
        fadeRadius = 300;
        gradientDistances = new float[]{0.0f, 0.4f, 1.0f};
        gradientColors = new Color[]{BG_COLOR_1, BG_COLOR_2, BG_COLOR_3};
        radialPaint = new RadialGradientPaint(center, fadeRadius, gradientDistances, gradientColors);
        g2.setPaint(radialPaint);
        g2.fillRect(0, 0, layerWidth, layerHeight);

        //We are finished with the graphics object so lets dispose it.
        g2.dispose();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelIndicator = new javax.swing.JPanel();
        jLabelMessage = new javax.swing.JLabel();
        jProgressBar = new com.pilog.t8.utilities.components.progressbar.T8ProgressBar();
        jProgressBarCategory = new com.pilog.t8.utilities.components.progressbar.T8ProgressBar();

        setLayout(new java.awt.GridBagLayout());

        jPanelIndicator.setOpaque(false);
        jPanelIndicator.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanelIndicator, gridBagConstraints);

        jLabelMessage.setText("Loading...");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelMessage, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jProgressBar, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jProgressBarCategory, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelMessage;
    private javax.swing.JPanel jPanelIndicator;
    private com.pilog.t8.utilities.components.progressbar.T8ProgressBar jProgressBar;
    private com.pilog.t8.utilities.components.progressbar.T8ProgressBar jProgressBarCategory;
    // End of variables declaration//GEN-END:variables
}
