package com.pilog.t8.ui.contentheaderborder;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.gfx.T8GraphicsUtilities;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.border.AbstractBorder;
import javax.swing.event.EventListenerList;
import org.jdesktop.swingx.painter.AbstractLayoutPainter;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;
import org.jdesktop.swingx.painter.TextPainter;
import sun.swing.SwingUtilities2;

/**
 * @author Bouwer du Preez
 */
public class T8ContentHeaderBorder extends AbstractBorder
{
    private static int HEIGHT = 25;
    private static final int INSET_SIZE = 3;
    private CompoundPainter compoundPainter;
    private TextPainter textPainter;
    private Painter backgroundPainter;
    private JComponent component;
    private final Map<Action, Rectangle> actions;
    private EventListenerList eventListeners;
    private BorderMouseListener listener;
    private boolean collapsible;
    private boolean collapsed;

    private final Painter DEFAULT_BACKGROUND_PAINTER = new MattePainter(T8GraphicsUtilities.createTopToBottomLinearGradient(LAFConstants.CONTENT_HEADER_HIGH_COLOR, LAFConstants.CONTENT_HEADER_LOW_COLOR), true);
    private final TextPainter DEFAULT_TEXT_PAINTER = new TextPainter(null, LAFConstants.CONTENT_HEADER_FONT, LAFConstants.CONTENT_HEADER_TEXT_COLOR);

    public static final String BORDER_ACTION_COMMAND = "BORDER_ACTION";

    public T8ContentHeaderBorder(String title)
    {
        // Configure the default painters.
        DEFAULT_TEXT_PAINTER.setHorizontalAlignment(AbstractLayoutPainter.HorizontalAlignment.LEFT);
        DEFAULT_TEXT_PAINTER.setInsets(new Insets(0, 10, 0, 0));

        // Initialize the component.
        this.backgroundPainter = DEFAULT_BACKGROUND_PAINTER;
        this.textPainter = DEFAULT_TEXT_PAINTER;
        this.compoundPainter = new CompoundPainter();
        this.compoundPainter.setPainters(backgroundPainter, textPainter);
        this.actions = new LinkedHashMap<Action, Rectangle>();
        this.eventListeners = new EventListenerList();
        this.collapsible = false;
        this.collapsed = false;
        this.setText(title);
    }

    public T8ContentHeaderBorder(JComponent component, String title, Collection<Action> actions)
    {
        this(title);
        this.component = component;
        this.listener = new BorderMouseListener();
        component.addMouseListener(listener);
        component.addMouseMotionListener(listener);
        setActions(actions);
    }

    public T8ContentHeaderBorder(JComponent component, String title)
    {
        this(component, title, null);
    }

    public void setActions(Collection<Action> newActions)
    {
        actions.clear();
        if (newActions != null)
        {
            for (Action action : newActions)
            {
                actions.put(action, null);
            }
        }
    }

    public void addAction(Action action)
    {
        actions.put(action, null);
    }

    public boolean removeAction(Action action)
    {
        if (actions.containsKey(action))
        {
            actions.remove(action);
            return true;
        }
        else return false;
    }

    public void addActionListener(ActionListener listener)
    {
        eventListeners.add(ActionListener.class, listener);
    }

    public void removeActionListener(ActionListener listener)
    {
        eventListeners.remove(ActionListener.class, listener);
    }

    public void setText(String text)
    {
        textPainter.setText(text != null ? text : "");
    }

    public void setBackgroundPainter(T8Painter painter)
    {
        // Set the background painter.
        if (painter != null)
        {
            backgroundPainter = new T8PainterAdapter(painter);
        }
        else
        {
            backgroundPainter = DEFAULT_BACKGROUND_PAINTER;
        }

        // Combine the new background painter with the existing text painter.
        compoundPainter.setPainters(backgroundPainter, textPainter);
    }

    public boolean isCollapsible()
    {
        return collapsible;
    }

    public void setCollapsible(boolean collapsible)
    {
        this.collapsible = collapsible;
    }

    public boolean isCollapsed()
    {
        return collapsed;
    }

    public void setCollapsed(boolean collapsed)
    {
        this.collapsed = collapsed;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height)
    {
        int lastXLocation;
        Graphics2D g2;

        // Paint the border background.
        g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        compoundPainter.paint(g2, c, width, HEIGHT);
        g2.setColor(LAFConstants.CONTENT_HEADER_BORDER_COLOR);
        g2.drawRect(x, y, width-1, height-1);

        // Paint the collapsed icon if required.
        if (collapsible)
        {
            BufferedImage image;
            Rectangle location;
            int xLocation;
            Icon icon;

            // Get the icon to use.
            if (collapsed)
            {
                icon = LAFConstants.CONTENT_HEADER_COLLAPSED_ICON;
            }
            else
            {
                icon = LAFConstants.CONTENT_HEADER_EXPANDED_ICON;
            }

            // Determine the x-location of the icon..
            xLocation = SwingUtilities2.stringWidth(component, SwingUtilities2.getFontMetrics(component, LAFConstants.CONTENT_HEADER_FONT), textPainter.getText());
            xLocation += 10; // Add a little margin between the icon and the text.

            // Paint the icon.
            image = g2.getDeviceConfiguration().createCompatibleImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TRANSLUCENT);
            icon.paintIcon(c, image.createGraphics(), x, y);

            // Draw the icon on the component border.
            location = new Rectangle(xLocation, y + ((HEIGHT - icon.getIconHeight()) /2), icon.getIconWidth(), icon.getIconHeight());
            g2.drawImage(image, location.x, location.y, location.width, location.height, c);
        }

        // Paint the action icons.
        lastXLocation = width;
        for (Action action : actions.keySet())
        {
            BufferedImage image;
            Rectangle location;
            Icon icon;

            // Get the action icon.
            icon = (Icon)action.getValue(Action.LARGE_ICON_KEY);

            // Adjust the x location to the width of the icon.
            lastXLocation -= icon.getIconWidth();

            // Add a inset to the x location.
            lastXLocation -= INSET_SIZE;

            // Paint the icon.
            image = g2.getDeviceConfiguration().createCompatibleImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TRANSLUCENT);
            icon.paintIcon(c, image.createGraphics(), x, y);

            // Draw the icon on the component border.
            location = new Rectangle(lastXLocation, y + INSET_SIZE, 25 - (INSET_SIZE * 2), 25 - (INSET_SIZE * 2));
            g2.drawImage(image, location.x, location.y, location.width, location.height, c);

            // Update the action icon location.
            actions.put(action, location);
        }
    }

    @Override
    public Insets getBorderInsets(Component c)
    {
        return new Insets(HEIGHT, 1, 1, 1);
    }

    /**
     * Reinitializes the insets parameter with this Border's current Insets.
     * @param c the component for which this border insets value applies
     * @param insets the object to be reinitialized
     * @return the <code>insets</code> object
     */
    @Override
    public Insets getBorderInsets(Component c, Insets insets)
    {
        insets.left = insets.right = insets.bottom = 0;
        insets.top = HEIGHT;
        return insets;
    }

    private class BorderMouseListener implements MouseListener, MouseMotionListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            Point location;

            location = e.getPoint();
            if (location.y < HEIGHT)
            {
                ActionEvent borderEvent;

                // First check whether or not one of the icons was clicked.
                for (Action action : actions.keySet())
                {
                    Rectangle iconBounds;

                    iconBounds = actions.get(action);
                    if (iconBounds.contains(location))
                    {
                        action.actionPerformed(new ActionEvent(component, ActionEvent.ACTION_PERFORMED, (String)action.getValue(Action.ACTION_COMMAND_KEY)));
                        return;
                    }
                }

                // No icon was clicked, so fire a general action performed on the border.
                borderEvent = new ActionEvent(component, ActionEvent.ACTION_PERFORMED, BORDER_ACTION_COMMAND);
                for (ActionListener actionListener : eventListeners.getListeners(ActionListener.class))
                {
                    actionListener.actionPerformed(borderEvent);
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }

        @Override
        public void mouseDragged(MouseEvent e)
        {
        }

        @Override
        public void mouseMoved(MouseEvent e)
        {
            if (collapsible)
            {
                Point location;

                location = e.getPoint();
                if (location.y < HEIGHT)
                {
                    if (component != null)
                    {
                        component.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    }
                    else
                    {
                        component.setCursor(Cursor.getDefaultCursor());
                    }
                }
            }
        }
    }
}
