package com.pilog.t8.ui.datacombobox;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxAPIHandler;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.busypanel.T8BusyPainter;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBox extends JComboBox<T8DataEntity> implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataComboBox.class);

    private final T8DataComboBoxDefinition definition;
    private final T8ComponentController controller;
    private final T8DataComboBoxOperationHandler operationHandler;
    private final T8DataComboBoxDataHandler dataHandler;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private T8DataEntityDefinition entityDefinition;
    private final Map<String, T8DataFilter> prefilters;
    private Map<String, Object> selectedKey; // This field is set while the data loader thread is busy, in order for a value to be set after data has finished loading.
    private final T8BusyPainter busyPainter;
    private boolean loadingData;

    private static final int BUSY_PAINTER_DIAMETER = 20;

    public T8DataComboBox(T8DataComboBoxDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.operationHandler = new T8DataComboBoxOperationHandler(this);
        this.dataHandler = new T8DataComboBoxDataHandler(definition);
        this.prefilters = new HashMap<>();
        this.loadingData = false;
        this.setOpaque(false);
        this.busyPainter = new T8BusyPainter(this, BUSY_PAINTER_DIAMETER, 8, 3);
        this.setRenderer(new T8DataComboBoxListCellRenderer(definition));
        this.addItemListener(new SelectionChangeListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Initialize all the available prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getPrefilterDefinitions())
        {
            prefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, null));
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        // Retrieve the combobox data.
        if(definition.isLoadOnStartup()) refreshData();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void setPrefilter(String identifier, T8DataFilter filter)
    {
        prefilters.put(identifier, filter);
    }

    public T8DataFilter getPrefilter(String identifier)
    {
        return prefilters.get(identifier);
    }

    public void showDataView()
    {
        //((CardLayout)this.getLayout()).show(this, "DATA_VIEW");
    }

    public void showProcessingView()
    {
        //((CardLayout)this.getLayout()).show(this, "PROCESSING_VIEW");
    }

    public void refreshData()
    {
        DataLoader loader;

        // Create a new loader.
        loader = new DataLoader();

        // Run the loader in a new Thread.
        new Thread(loader).start();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!loader.hasCompleted())
            {
                showProcessingView();
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to complete the loader thread. " + e.getMessage(), e);
        }
    }

    public T8DataEntity getSelectedDataEntity()
    {
        return (T8DataEntity)getSelectedItem();
    }

    public void setSelectedDataEntity(T8DataEntity entity)
    {
        setSelectedKey(entity != null ? entity.getKeyFieldValues() : null);
    }

    public void setSelectedKey(Map<String, Object> key)
    {
        selectedKey = key;
        if (key == null)
        {
            setSelectedItem(null);
        }
        else
        {
            ComboBoxModel<T8DataEntity> model;
            T8DataFilter dataFilter;

            // Create a data filter from the key.
            dataFilter = new T8DataFilter(definition.getDataEntityIdentifier(), key);

            // Run through all of the entities in the combobox and select the one that matches the filter.
            model = this.getModel();

            //Reset the combobox value, so that if no matching entity is found, the old value will not stay selected
            setSelectedItem(null);

            for (int entityIndex = 0; entityIndex < model.getSize(); entityIndex++)
            {
                T8DataEntity entity;

                entity = model.getElementAt(entityIndex);
                if ((entity != null) && (dataFilter.includesEntity(entity)))
                {
                    setSelectedIndex(entityIndex);
                    return;
                }
            }
        }
    }

    @Override
    public void paint(Graphics g)
    {
        if (loadingData)
        {
            Graphics2D g2;
            Rectangle clipBounds;
            FontMetrics fontMetrics;
            float textY;

            g2 = (Graphics2D)g;
            clipBounds = g2.getClipBounds();

            busyPainter.paint(g2, this, BUSY_PAINTER_DIAMETER, BUSY_PAINTER_DIAMETER);
            g2.setColor(getForeground());
            fontMetrics = g2.getFontMetrics();
            textY = clipBounds.y + (clipBounds.height / 2.0f) + (fontMetrics.getAscent() / 2.0f);
            g2.drawString("Loading Data...", clipBounds.x + BUSY_PAINTER_DIAMETER + 10, textY);
        }
        else
        {
            super.paint(g);
        }
    }

    private class SelectionChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<>();
                eventParameters.put(T8DataComboBoxAPIHandler.PARAMETER_DATA_ENTITY, getSelectedDataEntity());
                controller.reportEvent(T8DataComboBox.this, definition.getComponentEventDefinition(T8DataComboBoxAPIHandler.EVENT_SELECTION_CHANGE), eventParameters);
            }
            else
            {
                Object[] selectedObjects;

                // This is a very specific check for combobox selection events to work properly when a null value is selected.
                selectedObjects = e.getItemSelectable().getSelectedObjects();
                if ((selectedObjects == null) || (selectedObjects.length == 0) || ((selectedObjects.length == 1) && (selectedObjects[0] == null)))
                {
                    HashMap<String, Object> eventParameters;

                    eventParameters = new HashMap<>();
                    eventParameters.put(T8DataComboBoxAPIHandler.PARAMETER_DATA_ENTITY, getSelectedDataEntity());
                    controller.reportEvent(T8DataComboBox.this, definition.getComponentEventDefinition(T8DataComboBoxAPIHandler.EVENT_SELECTION_CHANGE), eventParameters);
                }
            }
        }
    }

    private class DataLoader implements Runnable
    {
        private boolean completed = false;

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            DefaultComboBoxModel<T8DataEntity> newModel;
            final DefaultComboBoxModel<T8DataEntity> model;

            // Set the flag to indicate that the loading data thread has started.
            loadingData = true;
            busyPainter.startAnimation();

            try
            {
                T8DataFilter combinedFilter;

                // Create a filter to contain a combination of all applicable filters for data retrieval.
                combinedFilter = new T8DataFilter(definition.getDataEntityIdentifier(), new T8DataFilterCriteria());

                // Add all available and valid prefilters.
                for (T8DataFilter prefilter : prefilters.values())
                {
                    // Only include filters that have valid filter criteria.
                    if ((prefilter != null) && (prefilter.hasFilterCriteria()))
                    {
                        combinedFilter.getFilterCriteria().addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
                    }

                    // Add field ordering (the last applicable prefilter will override the previous ones).
                    if (prefilter != null && prefilter.hasFieldOrdering())
                    {
                        combinedFilter.setFieldOrdering(prefilter.getFieldOrdering());
                    }
                }

                // Use the combined filter to retrieve the filtered data and to construct a combobox model.
                newModel = dataHandler.loadComboBoxData(controller.getContext(), combinedFilter);

                //If a null value should be added, then add it now
                if(definition.isAddNullValue())
                {
                    newModel.insertElementAt(null, 0);
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Failed to load the data combo box items. " + e.getMessage(), e);
                newModel = new DefaultComboBoxModel<>();
            }

            // Set the final model.
            model = newModel;

            // Set the new model on the UI.
            try
            {
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // Set the page number label.
                        T8DataComboBox.this.setModel(model);

                        if(model.getSize() > 0 && definition.isSelectFirstValueOnLoad()) T8DataComboBox.this.setSelectedIndex(0);
                        else T8DataComboBox.this.setSelectedIndex(-1);

                        // Set the completed flag.
                        completed = true;

                        // Show the loaded data.
                        showDataView();

                        // This next step is not always necessary, since the next selection may already be set
                        // depending on whether or not the data loader thread was busy while the method to
                        // set the selection was invoked.
                        if(selectedKey != null) setSelectedKey(selectedKey);
                    }
                });
            }
            catch (InterruptedException | InvocationTargetException e)
            {
                LOGGER.log("Exception while setting new combobox model: " + definition, e);
            }

            // Set the flag to indicate that the loading data thread has finished.
            loadingData = false;
            busyPainter.stopAnimation();
        }
    }
}
