package com.pilog.t8.ui.operationexecutiondialog;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8CategorizedProgressReport;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Window;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultOperationExecutionDialog extends JDialog
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultOperationExecutionDialog.class);

    private final T8Context context;
    private final Map<String, Object> inputParameters;
    private String operationIid;
    private final String operationId;
    private Map<String, Object> result;
    private Exception exception;
    private long startTime;

    public T8DefaultOperationExecutionDialog(Window parent, T8Context context, String operationId, String headerText, String processingText, Map<String, Object> inputParameters)
    {
        super(parent, ModalityType.APPLICATION_MODAL);
        initComponents();
        this.context = context;
        this.operationId = operationId;
        this.inputParameters = inputParameters;
        this.result = null;
        this.jLabelHeader.setText(headerText);
        this.jLabelProcessing.setText(processingText);
        this.jPanelHeader.setBackground(LAFConstants.HEADER_BG_COLOR);
        this.jLabelHeader.setBorder(LAFConstants.HEADER_BORDER);
        this.jProgressBarCat.setVisible(false);
        setPreferredSize(new Dimension(500, 200));
        pack();
        setLocationRelativeTo(null);
    }

    public T8DefaultOperationExecutionDialog(T8ComponentController controller, String headerText, String processingText)
    {
        this(controller.getClientContext().getParentWindow(), controller.getContext(), null, headerText, processingText, null);
    }

    private Exception getException()
    {
        return exception;
    }

    private void setException(Exception e)
    {
        this.exception = e;
    }

    public Map<String, Object> getResult()
    {
        return result;
    }

    public static final Map<String, Object> executeOperation(Window parentWindow, T8Context context, String operationId, String headerText, String processingText, Map<String, Object> inputParameters) throws Exception
    {
        T8DefaultOperationExecutionDialog dialog;

        dialog = new T8DefaultOperationExecutionDialog(parentWindow, context, operationId, headerText, processingText, inputParameters);
        dialog.startExecution();
        dialog.setVisible(true);
        if (dialog.getException() != null) throw dialog.getException();
        else return dialog.getResult();
    }

    private void updateProgress(final double progress)
    {
        SwingUtilities.invokeLater(() ->
        {
            long estimatedDuration;
            long remainingTime;
            long elapsedTime;

            elapsedTime = System.currentTimeMillis() - startTime;
            estimatedDuration = (long)(((double)elapsedTime) / progress * 100.00);
            remainingTime = estimatedDuration - elapsedTime;

            jLabelEstimatedTimeRemainingValue.setText(buildTimeString(remainingTime));
            jLabelElapsedTimeValue.setText(buildTimeString(elapsedTime));

            jProgressBarOverall.setIndeterminate(false);
            jProgressBarOverall.setStringPainted(true);
            jProgressBarOverall.setMinimum(0);
            jProgressBarOverall.setValue((int)progress);
            jProgressBarOverall.setMaximum(100);
        });
    }

    private void updateProgress(final T8ProgressReport progress)
    {
        SwingUtilities.invokeLater(() ->
        {
            long estimatedDuration;
            long remainingTime;
            long elapsedTime;

            elapsedTime = System.currentTimeMillis() - startTime;
            estimatedDuration = (long)(((double)elapsedTime) / progress.getProgress() * 100.00);
            remainingTime = estimatedDuration - elapsedTime;

            jLabelEstimatedTimeRemainingValue.setText(buildTimeString(remainingTime));
            jLabelElapsedTimeValue.setText(buildTimeString(elapsedTime));

            jProgressBarOverall.setIndeterminate(false);
            jProgressBarOverall.setStringPainted(true);
            jProgressBarOverall.setMinimum(0);
            jProgressBarOverall.setValue(progress.getProgress().intValue());
            jProgressBarOverall.setMaximum(100);
            jProgressBarOverall.setString(progress.getProgressMessage());

            if(progress instanceof T8CategorizedProgressReport)
            {
                T8CategorizedProgressReport catProgressReport;

                catProgressReport = (T8CategorizedProgressReport) progress;
                jProgressBarCat.setVisible(true);
                jProgressBarCat.setStringPainted(true);
                jProgressBarCat.setMinimum(0);
                jProgressBarCat.setMaximum(100);
                jProgressBarCat.setValue(catProgressReport.getCategoryProgress().intValue());
                jProgressBarCat.setString(catProgressReport.getCategoryMessage());
            }
        });
    }

    public void displayExecution(String operationIid)
    {
        // Update the UI.
        jButtonStop.setEnabled(true);

        // Start the process.
        startTime = System.currentTimeMillis();
        this.operationIid = operationIid;

        // Start the progress checker thread;
        new Thread(new Checker()).start();
    }

    public void startExecution()
    {
        // Update the UI.
        jButtonStop.setEnabled(true);

        // Start the process.
        try
        {
            T8ServerOperationStatusReport statusReport;

            startTime = System.currentTimeMillis();
            statusReport = T8MainServerClient.executeAsynchronousOperation(context, operationId, inputParameters);
            operationIid = statusReport.getOperationInstanceIdentifier();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Start the progress checker thread;
        new Thread(new Checker()).start();
    }

    protected void stopExecution()
    {
        try
        {
            // Update the UI.
            jButtonStop.setEnabled(false);
            T8MainServerClient.stopOperation(context, operationIid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        completeExecution();
    }

    protected void completeExecution()
    {
        // Enable the close button once the process has been completed.
        jButtonStop.setEnabled(false);
        updateProgress(100);
        dispose();
    }

    protected String buildTimeString(long millis)
    {
        StringBuffer timeString;
        long secondInMillis = 1000;
        long minuteInMillis = secondInMillis * 60;
        long hourInMillis = minuteInMillis * 60;
        long elapsedHours;
        long elapsedMinutes;
        long elapsedSeconds;
        long diff;

        diff = millis;
        elapsedHours = diff / hourInMillis;
        diff = diff % hourInMillis;
        elapsedMinutes = diff / minuteInMillis;
        diff = diff % minuteInMillis;
        elapsedSeconds = diff / secondInMillis;

        timeString = new StringBuffer();
        if (elapsedHours > 100)
        {
            timeString.append("undeterminable");
        }
        else
        {
            if (elapsedHours > 0)
            {
                timeString.append(elapsedHours);
                timeString.append(" hours, ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0))
            {
                timeString.append(elapsedMinutes);
                timeString.append(" minutes, ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0) || (elapsedSeconds > 0))
            {
                timeString.append(elapsedSeconds);
                timeString.append(" seconds");
            }
        }

        return timeString.toString();
    }

    private class Checker implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                T8ServerOperationStatus status;

                status = T8ServerOperationStatus.IN_PROGRESS;
                while (status == T8ServerOperationStatus.IN_PROGRESS)
                {
                    T8ServerOperationStatusReport statusReport;

                    statusReport = T8MainServerClient.getOperationStatus(context, operationIid);
                    status = statusReport.getOperationStatus();

                    if (status == T8ServerOperationStatus.FAILED)
                    {
                        Exception exception;

                        exception = (Exception)statusReport.getOperationResult();
                        setException(exception);
                        completeExecution();
                        return; // End the checker thread.
                    }
                    else if (status == T8ServerOperationStatus.IN_PROGRESS)
                    {
                        if(statusReport.getProgressReportObject() != null) updateProgress(statusReport.getProgressReportObject());
                        else updateProgress(statusReport.getOperationProgress());

                        Thread.sleep(500);
                    }
                    else
                    {
                        result = statusReport.getOperationResult();
                        completeExecution();
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception in operation progress checker Thread.", e);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelHeader = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();
        jPanelContent = new javax.swing.JPanel();
        jLabelProcessing = new javax.swing.JLabel();
        jLabelEstimatedTimeRemaining = new javax.swing.JLabel();
        jLabelEstimatedTimeRemainingValue = new javax.swing.JLabel();
        jLabelElapsedTime = new javax.swing.JLabel();
        jLabelElapsedTimeValue = new javax.swing.JLabel();
        jProgressBarOverall = new javax.swing.JProgressBar();
        jProgressBarCat = new javax.swing.JProgressBar();
        jPanelFooter = new javax.swing.JPanel();
        jButtonStop = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelHeader.setLayout(new java.awt.GridBagLayout());

        jLabelHeader.setText("Operation Execution");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelHeader.add(jLabelHeader, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelHeader, gridBagConstraints);

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jLabelProcessing.setText("Processing...");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        jPanelContent.add(jLabelProcessing, gridBagConstraints);

        jLabelEstimatedTimeRemaining.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelEstimatedTimeRemaining.setText("Time Remaining:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelContent.add(jLabelEstimatedTimeRemaining, gridBagConstraints);

        jLabelEstimatedTimeRemainingValue.setText("0 hours, 0 minutes, 0 seconds");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        jPanelContent.add(jLabelEstimatedTimeRemainingValue, gridBagConstraints);

        jLabelElapsedTime.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelElapsedTime.setText("Elapsed Time:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanelContent.add(jLabelElapsedTime, gridBagConstraints);

        jLabelElapsedTimeValue.setText("0 hours, 0 minutes, 0 seconds");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 10);
        jPanelContent.add(jLabelElapsedTimeValue, gridBagConstraints);

        jProgressBarOverall.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 10);
        jPanelContent.add(jProgressBarOverall, gridBagConstraints);

        jProgressBarCat.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 10);
        jPanelContent.add(jProgressBarCat, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelContent, gridBagConstraints);

        jPanelFooter.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 2, 2));

        jButtonStop.setText("Stop");
        jButtonStop.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonStopActionPerformed(evt);
            }
        });
        jPanelFooter.add(jButtonStop);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelFooter, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStopActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStopActionPerformed
    {//GEN-HEADEREND:event_jButtonStopActionPerformed
        stopExecution();
    }//GEN-LAST:event_jButtonStopActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonStop;
    private javax.swing.JLabel jLabelElapsedTime;
    private javax.swing.JLabel jLabelElapsedTimeValue;
    private javax.swing.JLabel jLabelEstimatedTimeRemaining;
    private javax.swing.JLabel jLabelEstimatedTimeRemainingValue;
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JLabel jLabelProcessing;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelFooter;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JProgressBar jProgressBarCat;
    private javax.swing.JProgressBar jProgressBarOverall;
    // End of variables declaration//GEN-END:variables
}
