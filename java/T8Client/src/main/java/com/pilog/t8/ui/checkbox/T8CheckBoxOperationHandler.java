package com.pilog.t8.ui.checkbox;

import com.pilog.t8.definition.ui.checkbox.T8CheckBoxAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxOperationHandler extends T8ComponentOperationHandler
{
    private T8CheckBox checkBox;

    public T8CheckBoxOperationHandler(T8CheckBox checkBox)
    {
        super(checkBox);
        this.checkBox = checkBox;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8CheckBoxAPIHandler.OPERATION_DESELECT))
        {
            checkBox.setSelected(false);
            return null;
        }
        else if (operationIdentifier.equals(T8CheckBoxAPIHandler.OPERATION_SELECT))
        {
            checkBox.setSelected(true);
            return null;
        }
        else if (operationIdentifier.equals(T8CheckBoxAPIHandler.OPERATION_SET_SELECTED))
        {
            checkBox.setSelected((Boolean)operationParameters.get(T8CheckBoxAPIHandler.PARAMETER_SELECTED));
            return null;
        }
        else if (operationIdentifier.equals(T8CheckBoxAPIHandler.OPERATION_IS_SELECTED))
        {
            return HashMaps.newHashMap(T8CheckBoxAPIHandler.PARAMETER_SELECTED, checkBox.isSelected());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
