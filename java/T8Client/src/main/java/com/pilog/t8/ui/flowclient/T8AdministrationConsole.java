package com.pilog.t8.ui.flowclient;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.mainserver.T8MainServerClientStatistics;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.module.T8Module;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import org.jdesktop.swingx.JXTextField;

/**
 * @author Bouwer du Preez
 */
public class T8AdministrationConsole extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;

    public T8AdministrationConsole(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        initComponents();
        setClientStatistics();
        setLocationRelativeTo(clientContext.getParentWindow());
        setPreferredSize(new Dimension(500, 800));
        pack();
    }

    public T8AdministrationConsole(T8ClientContext clientContext, T8SessionContext sessionContext, T8Module module, T8ComponentController moduleController)
    {
        this(clientContext, sessionContext);

        setModuleInformation(module, moduleController);
        jTabbedPaneModule.add("Module", jScrollPaneModule);
    }

    private void setClientStatistics()
    {
        T8MainServerClientStatistics statistics;

        statistics = T8MainServerClient.getStatistics();
        jLabelTotalSent.setText(StringUtilities.getFileSizeString(statistics.getTotalBytesSent()));
        jLabelTotalRecieved.setText(StringUtilities.getFileSizeString(statistics.getTotalBytesReceived()));
        jLabelAvgSent.setText(StringUtilities.getFileSizeString(statistics.getAverageBytesSent()));
        jLabelAvgReceived.setText(StringUtilities.getFileSizeString(statistics.getAverageBytesReceived()));
        jLabelAvgResponse.setText(statistics.getAverageServerResponseTime() + "ms");
    }

    private void setModuleInformation(T8Module module, T8ComponentController moduleController)
    {
        jXTextFieldModuleIdentifier.setText(module.getComponentDefinition().getIdentifier());
        Map<String, Object> inputParameters = module.getInputParameters();
        T8ComponentController controller;
        Collection<String> variableNames;
        T8Context context;
        List<String> controllerIds;
        DefaultListModel listModel;

        // Set the controllers.
        context = moduleController.getContext();
        controllerIds = new ArrayList<>();
        controller = moduleController;
        while (controller != null)
        {
            controllerIds.add(controller.getContext().getProjectId() + ":" + controller.getId());
            controller = controller.getParentController();
        }
        Collections.reverse(controllerIds);
        listModel = new DefaultListModel();
        for (String controllerId : controllerIds)
        {
            listModel.addElement(controllerId);
        }
        jListControllers.setModel(listModel);

        // Set access context.
        jTextAreaAccessContext.setText(null);
        jTextAreaAccessContext.append("Project Id: " + context.getProjectId() + "\n");
        jTextAreaAccessContext.append("Root Organization Id: " + context.getRootOrganizationId() + "\n");
        jTextAreaAccessContext.append("Flow Id: " + context.getFlowId() + "\n");
        jTextAreaAccessContext.append("Flow Iid: " + context.getFlowIid() + "\n");
        jTextAreaAccessContext.append("Functionality Id: " + context.getFunctionalityId() + "\n");
        jTextAreaAccessContext.append("Functionality Iid: " + context.getFunctionalityIid() + "\n");
        jTextAreaAccessContext.append("Operation Id: " + context.getOperationId() + "\n");
        jTextAreaAccessContext.append("Operation Iid: " + context.getOperationIid() + "\n");
        jTextAreaAccessContext.append("Organization Id: " + context.getOrganizationId() + "\n");
        jTextAreaAccessContext.append("Process Id: " + context.getProcessId() + "\n");
        jTextAreaAccessContext.append("Process Iid: " + context.getProcessIid() + "\n");
        jTextAreaAccessContext.append("Task Id: " + context.getTaskId() + "\n");
        jTextAreaAccessContext.append("Task Iid: " + context.getTaskIid() + "\n");

        // Set the input parameters.
        if (inputParameters != null)
        {
            for (String keyString : inputParameters.keySet())
            {
                JXTextField paramName;
                JXTextField paramValue;

                paramName = createTextField(keyString);
                paramValue = createValueTextField();
                if (inputParameters.get(keyString) != null)
                {
                    paramValue.setText(inputParameters.get(keyString).toString());
                }

                jPanelModuleInputParameters.add(paramName);
                jPanelModuleInputParameters.add(paramValue);
            }
        }

        // Set the module variables.
        variableNames = module.getVariableIds();
        if (variableNames != null)
        {
            for (String variableName : variableNames)
            {
                JXTextField paramName;
                JXTextField paramValue;

                paramName = createTextField(variableName);
                paramValue = createValueTextField();
                if (moduleController.getVariable(variableName) != null)
                {
                    paramValue.setText(moduleController.getVariable(variableName).toString());
                }

                jPanelModuleVariables.add(paramName);
                jPanelModuleVariables.add(paramValue);
            }
        }

        revalidate();
    }

    private JXTextField createTextField(String text)
    {
        JXTextField paramName;

        paramName = new JXTextField();
        paramName.setEditable(false);
        paramName.setBorder(null);
        paramName.setOpaque(false);
        paramName.setText(text);

        return paramName;
    }

    private JXTextField createValueTextField()
    {
        JXTextField paramValue;

        paramValue = createTextField(null);
        paramValue.setHorizontalAlignment(JXTextField.RIGHT);
        paramValue.setFont(paramValue.getFont().deriveFont(Font.BOLD));
        paramValue.setOpaque(true);

        return paramValue;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPaneModule = new javax.swing.JScrollPane();
        jPanelModule = new javax.swing.JPanel();
        jPanelControllers = new javax.swing.JPanel();
        jScrollPaneControllers = new javax.swing.JScrollPane();
        jListControllers = new javax.swing.JList<>();
        jPanelContext = new javax.swing.JPanel();
        jScrollPaneAccessContext = new javax.swing.JScrollPane();
        jTextAreaAccessContext = new javax.swing.JTextArea();
        jPanelModuleIdentifier = new javax.swing.JPanel();
        jXTextFieldModuleIdentifier = new org.jdesktop.swingx.JXTextField();
        jPanelModuleInputParameters = new javax.swing.JPanel();
        jPanelModuleVariables = new javax.swing.JPanel();
        jTabbedPaneModule = new javax.swing.JTabbedPane();
        jPanelSettingsContainer = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabelAvgSentHeader = new javax.swing.JLabel();
        jLabelTotalRecieved = new javax.swing.JLabel();
        jLabelAvgRecievedHeader = new javax.swing.JLabel();
        jLabelAvgResponseHeader = new javax.swing.JLabel();
        jLabelTotalRecievedHeader = new javax.swing.JLabel();
        jLabelTotalSentHeader = new javax.swing.JLabel();
        jLabelTotalSent = new javax.swing.JLabel();
        jLabelAvgReceived = new javax.swing.JLabel();
        jLabelAvgSent = new javax.swing.JLabel();
        jLabelAvgResponse = new javax.swing.JLabel();

        jScrollPaneModule.setToolTipText("");

        jPanelModule.setBackground(new java.awt.Color(255, 255, 255));
        jPanelModule.setName(""); // NOI18N
        jPanelModule.setLayout(new java.awt.GridBagLayout());

        jPanelControllers.setBorder(javax.swing.BorderFactory.createTitledBorder("Controllers"));
        jPanelControllers.setAlignmentX(1.0F);
        jPanelControllers.setAlignmentY(1.0F);
        jPanelControllers.setMinimumSize(new java.awt.Dimension(10, 10));
        jPanelControllers.setName(""); // NOI18N
        jPanelControllers.setOpaque(false);
        jPanelControllers.setLayout(new java.awt.GridBagLayout());

        jScrollPaneControllers.setViewportView(jListControllers);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelControllers.add(jScrollPaneControllers, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelModule.add(jPanelControllers, gridBagConstraints);

        jPanelContext.setBorder(javax.swing.BorderFactory.createTitledBorder("Context"));
        jPanelContext.setAlignmentX(1.0F);
        jPanelContext.setAlignmentY(1.0F);
        jPanelContext.setMinimumSize(new java.awt.Dimension(10, 10));
        jPanelContext.setName(""); // NOI18N
        jPanelContext.setOpaque(false);
        jPanelContext.setLayout(new java.awt.GridBagLayout());

        jTextAreaAccessContext.setColumns(20);
        jTextAreaAccessContext.setRows(12);
        jScrollPaneAccessContext.setViewportView(jTextAreaAccessContext);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelContext.add(jScrollPaneAccessContext, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelModule.add(jPanelContext, gridBagConstraints);

        jPanelModuleIdentifier.setBorder(javax.swing.BorderFactory.createTitledBorder("Module Identifier"));
        jPanelModuleIdentifier.setAlignmentX(1.0F);
        jPanelModuleIdentifier.setAlignmentY(1.0F);
        jPanelModuleIdentifier.setMinimumSize(new java.awt.Dimension(10, 10));
        jPanelModuleIdentifier.setName(""); // NOI18N
        jPanelModuleIdentifier.setOpaque(false);
        jPanelModuleIdentifier.setLayout(new java.awt.BorderLayout());

        jXTextFieldModuleIdentifier.setEditable(false);
        jXTextFieldModuleIdentifier.setBorder(null);
        jXTextFieldModuleIdentifier.setToolTipText("");
        jXTextFieldModuleIdentifier.setName(""); // NOI18N
        jXTextFieldModuleIdentifier.setOpaque(false);
        jPanelModuleIdentifier.add(jXTextFieldModuleIdentifier, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelModule.add(jPanelModuleIdentifier, gridBagConstraints);

        jPanelModuleInputParameters.setBorder(javax.swing.BorderFactory.createTitledBorder("Module Input Parameters"));
        jPanelModuleInputParameters.setAlignmentX(1.0F);
        jPanelModuleInputParameters.setAlignmentY(1.0F);
        jPanelModuleInputParameters.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanelModuleInputParameters.setMinimumSize(new java.awt.Dimension(10, 10));
        jPanelModuleInputParameters.setName(""); // NOI18N
        jPanelModuleInputParameters.setOpaque(false);
        jPanelModuleInputParameters.setLayout(new javax.swing.BoxLayout(jPanelModuleInputParameters, javax.swing.BoxLayout.PAGE_AXIS));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelModule.add(jPanelModuleInputParameters, gridBagConstraints);

        jPanelModuleVariables.setBorder(javax.swing.BorderFactory.createTitledBorder("Module Variables"));
        jPanelModuleVariables.setAlignmentX(1.0F);
        jPanelModuleVariables.setAlignmentY(1.0F);
        jPanelModuleVariables.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanelModuleVariables.setMinimumSize(new java.awt.Dimension(10, 10));
        jPanelModuleVariables.setName(""); // NOI18N
        jPanelModuleVariables.setOpaque(false);
        jPanelModuleVariables.setLayout(new javax.swing.BoxLayout(jPanelModuleVariables, javax.swing.BoxLayout.PAGE_AXIS));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelModule.add(jPanelModuleVariables, gridBagConstraints);

        jScrollPaneModule.setViewportView(jPanelModule);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPaneModule.setPreferredSize(new java.awt.Dimension(500, 500));

        javax.swing.GroupLayout jPanelSettingsContainerLayout = new javax.swing.GroupLayout(jPanelSettingsContainer);
        jPanelSettingsContainer.setLayout(jPanelSettingsContainerLayout);
        jPanelSettingsContainerLayout.setHorizontalGroup(
            jPanelSettingsContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 674, Short.MAX_VALUE)
        );
        jPanelSettingsContainerLayout.setVerticalGroup(
            jPanelSettingsContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 579, Short.MAX_VALUE)
        );

        jTabbedPaneModule.addTab("Client Settings", jPanelSettingsContainer);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabelAvgSentHeader.setText("Average Sent Bytes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelAvgSentHeader, gridBagConstraints);

        jLabelTotalRecieved.setText("jLabel2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelTotalRecieved, gridBagConstraints);

        jLabelAvgRecievedHeader.setText("Average Recieved Bytes:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelAvgRecievedHeader, gridBagConstraints);

        jLabelAvgResponseHeader.setText("Average Response Time:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelAvgResponseHeader, gridBagConstraints);

        jLabelTotalRecievedHeader.setText("Total Received Bytes:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelTotalRecievedHeader, gridBagConstraints);

        jLabelTotalSentHeader.setText("Total Sent Bytes:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelTotalSentHeader, gridBagConstraints);

        jLabelTotalSent.setText("jLabel7");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelTotalSent, gridBagConstraints);

        jLabelAvgReceived.setText("jLabel8");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelAvgReceived, gridBagConstraints);

        jLabelAvgSent.setText("jLabel9");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelAvgSent, gridBagConstraints);

        jLabelAvgResponse.setText("jLabel10");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabelAvgResponse, gridBagConstraints);

        jTabbedPaneModule.addTab("Client Statistics", jPanel1);

        getContentPane().add(jTabbedPaneModule, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelAvgReceived;
    private javax.swing.JLabel jLabelAvgRecievedHeader;
    private javax.swing.JLabel jLabelAvgResponse;
    private javax.swing.JLabel jLabelAvgResponseHeader;
    private javax.swing.JLabel jLabelAvgSent;
    private javax.swing.JLabel jLabelAvgSentHeader;
    private javax.swing.JLabel jLabelTotalRecieved;
    private javax.swing.JLabel jLabelTotalRecievedHeader;
    private javax.swing.JLabel jLabelTotalSent;
    private javax.swing.JLabel jLabelTotalSentHeader;
    private javax.swing.JList<String> jListControllers;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelContext;
    private javax.swing.JPanel jPanelControllers;
    private javax.swing.JPanel jPanelModule;
    private javax.swing.JPanel jPanelModuleIdentifier;
    private javax.swing.JPanel jPanelModuleInputParameters;
    private javax.swing.JPanel jPanelModuleVariables;
    private javax.swing.JPanel jPanelSettingsContainer;
    private javax.swing.JScrollPane jScrollPaneAccessContext;
    private javax.swing.JScrollPane jScrollPaneControllers;
    private javax.swing.JScrollPane jScrollPaneModule;
    private javax.swing.JTabbedPane jTabbedPaneModule;
    private javax.swing.JTextArea jTextAreaAccessContext;
    private org.jdesktop.swingx.JXTextField jXTextFieldModuleIdentifier;
    // End of variables declaration//GEN-END:variables
}
