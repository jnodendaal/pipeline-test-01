package com.pilog.t8.data.object;

import com.pilog.t8.api.T8DataObjectClientApi;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ClientDataObjectProvider implements T8DataObjectProvider
{
    private final T8Context context;
    private final T8DataObjectClientApi objApi;

    public T8ClientDataObjectProvider(T8Context context)
    {
        this.context = context;
        this.objApi = context.getClientContext().getConfigurationManager().getAPI(context, T8DataObjectClientApi.API_IDENTIFIER);
    }

    @Override
    public T8DataObject getDataObject(String dataObjectId, String dataObjectIid, boolean includeState)
    {
        try
        {
            return objApi.retrieve(dataObjectId, dataObjectIid, includeState);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data object: " + dataObjectIid, e);
        }
    }

    @Override
    public T8DataObjectState getDataObjectState(String dataObjectIid)
    {
        try
        {
            return objApi.retrieveState(dataObjectIid);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data object state: " + dataObjectIid, e);
        }
    }
}
