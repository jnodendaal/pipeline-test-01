package com.pilog.t8.ui.dialog;

import static com.pilog.t8.definition.ui.dialog.T8DialogAPIHandler.*;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DialogOperationHandler
{
    private final T8Dialog dialog;

    public T8DialogOperationHandler(T8Dialog dialog)
    {
        this.dialog = dialog;
    }

    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(OPERATION_OPEN_DIALOG))
        {
            /**
             * We have to open the dialog in a new thread, because the invoking
             * thread will be blocked in the case of modal dialogs.
             */
            new Thread()
            {
                @Override
                public void run()
                {
                    boolean modal;

                    modal = (operationParameters == null ? false : ((boolean)operationParameters.getOrDefault(PARAMETER_MODAL, false)));

                    dialog.openDialog(modal);
                }
            }.start();

            return null;
        }
        else if (operationIdentifier.equals(OPERATION_CLOSE_DIALOG))
        {
            dialog.closeDialog();
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_SET_TITLE))
        {
            String title;

            title = (String)operationParameters.get(PARAMETER_TITLE);
            dialog.setHeaderTitle(title);
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_SET_TITLE))
        {
            String title;

            title = (String)operationParameters.get(PARAMETER_TITLE);
            dialog.setHeaderTitle(title);
            return null;
        }
        else throw new RuntimeException("Invalid Operation Identifier: '" + operationIdentifier + "'.");
    }
}
