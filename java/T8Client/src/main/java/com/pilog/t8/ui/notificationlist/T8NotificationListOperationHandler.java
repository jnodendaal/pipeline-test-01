package com.pilog.t8.ui.notificationlist;

import com.pilog.t8.definition.ui.notificationlist.T8NotificationListAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationListOperationHandler extends T8ComponentOperationHandler
{
    private final T8NotificationList notificationList;

    public T8NotificationListOperationHandler(T8NotificationList notificationList)
    {
        super(notificationList);
        this.notificationList = notificationList;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (T8NotificationListAPIHandler.OPERATION_FILTER_NOTIFICATION_LIST.equals(operationIdentifier))
        {
            String notificationIdentifier;
            Boolean includeNewNotifications;
            Boolean includeReceivedNotifications;

            notificationIdentifier = (String)operationParameters.get(T8NotificationListAPIHandler.PARAMETER_NOTIFICATION_IDENTIFIER);
            includeNewNotifications = (Boolean)operationParameters.get(T8NotificationListAPIHandler.PARAMETER_INCLUDE_NEW_NOTIFICATIONS);
            includeReceivedNotifications = (Boolean)operationParameters.get(T8NotificationListAPIHandler.PARAMETER_INCLUDE_RECEIVED_NOTIFICATIONS);
            
            if (includeNewNotifications == null) includeNewNotifications = true;
            if (includeReceivedNotifications == null) includeReceivedNotifications = true;
            
            notificationList.filterNotificationList(notificationIdentifier, includeNewNotifications, includeReceivedNotifications);
            return null;
        } 
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
