package com.pilog.t8.definition.data.entity;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityResourceDefinition extends T8DataEntityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ENTITY_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //
    
    public T8DataEntityResourceDefinition(String identifier)
    {
        super(identifier);
    }
}
