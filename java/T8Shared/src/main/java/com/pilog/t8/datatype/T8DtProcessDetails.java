package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.process.T8Process.T8ProcessStatus;
import com.pilog.t8.process.T8ProcessDetails;

/**
 *
 * @author Pieter Strydom
 */
public class T8DtProcessDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@PROCESS_DETAILS";

    public T8DtProcessDetails() {}

    public T8DtProcessDetails(T8DefinitionManager context) {}

    public T8DtProcessDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8ProcessDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject jsonFileDetails;
            T8ProcessDetails processDetails;

            // Create the JSON object.
            processDetails = (T8ProcessDetails)object;
            jsonFileDetails = new JsonObject();
            jsonFileDetails.add("id", processDetails.getProcessId());
            jsonFileDetails.add("iid", processDetails.getProcessIid());
            jsonFileDetails.add("name", processDetails.getName());
            jsonFileDetails.add("description", processDetails.getProcessDescription());
            jsonFileDetails.add("initiatorId", processDetails.getInitiatorId());
            jsonFileDetails.add("initiatorName", processDetails.getInitiatorName());
            jsonFileDetails.add("restrictionId", processDetails.getRestrictionUserId());
            jsonFileDetails.add("restrictionProfileId", processDetails.getRestrictionUserProfileId());
            jsonFileDetails.add("failureMessage", processDetails.getFailureMessage());
            jsonFileDetails.add("status", processDetails.getStatus().toString());
            jsonFileDetails.add("progress", processDetails.getProgress());
            jsonFileDetails.add("startTime", processDetails.getStartTime());
            jsonFileDetails.add("endTime", processDetails.getEndTime());

            // Return the final JSON object.
            return jsonFileDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8ProcessDetails deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8ProcessDetails processDetails;
            JsonObject jsonProcessDetails;
            String status;

            jsonProcessDetails = jsonValue.asObject();
            status = jsonProcessDetails.getString("status");

            // Create the process details object.
            processDetails = new T8ProcessDetails(jsonProcessDetails.getString("iid"));
            processDetails.setProcessId(jsonProcessDetails.getString("id"));
            processDetails.setName(jsonProcessDetails.getString("name"));
            processDetails.setProcessDescription(jsonProcessDetails.getString("description"));
            processDetails.setInitiatorId(jsonProcessDetails.getString("initiatorId"));
            processDetails.setInitiatorName(jsonProcessDetails.getString("initiatorName"));
            processDetails.setRestrictionUserId(jsonProcessDetails.getString("restrictionId"));
            processDetails.setRestrictionUserProfileId(jsonProcessDetails.getString("restrictionProfileId"));
            processDetails.setFailureMessage(jsonProcessDetails.getString("failureMessage"));
            processDetails.setProcessStatus(status != null ? T8ProcessStatus.valueOf(status) : null);
            processDetails.setProgress(jsonProcessDetails.getDouble("progress"));
            processDetails.setStartTime(jsonProcessDetails.getLong("startTime"));
            processDetails.setEndTime(jsonProcessDetails.getLong("endTime"));

            // Return the completed object.
            return processDetails;
        }
        else return null;
    }
}
