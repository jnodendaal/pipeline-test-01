package com.pilog.t8.definition;

import java.util.List;
import javax.swing.Action;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionActionHandler
{
    public List<Action> getDefinitionEditorActions();
}
