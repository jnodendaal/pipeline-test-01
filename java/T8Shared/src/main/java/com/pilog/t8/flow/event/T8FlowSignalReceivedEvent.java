package com.pilog.t8.flow.event;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.json.JsonObject;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8JsonParameterSerializer;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey.WaitKeyIdentifier;
import com.pilog.t8.flow.key.T8SignalExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalAccumulationEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalCatchEventDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowSignalReceivedEvent extends T8FlowEvent
{
    private final T8FlowSignalDefinition signalDefinition;
    private final String projectId;
    private final String signalId;
    private final String keyParameterId;
    private final Map<String, Object> signalParameters;

    public static final String TYPE_ID = "SIGNAL_RECEIVED";

    public T8FlowSignalReceivedEvent(T8FlowSignalDefinition signalDefinition, Map<String, Object> signalParameters)
    {
        super(signalDefinition.getIdentifier());
        this.signalDefinition = signalDefinition;
        this.projectId = signalDefinition.getRootProjectId();
        this.signalId = signalDefinition.getIdentifier();
        this.keyParameterId = signalDefinition.getKeyParameterId();
        this.signalParameters = signalParameters;
    }

    public T8FlowSignalReceivedEvent(T8FlowController controller, JsonObject jsonObject)
    {
        super(jsonObject.getString("signalId"));

        try
        {
            T8JsonParameterSerializer parameterSerializer;

            parameterSerializer = new T8JsonParameterSerializer();
            this.projectId = jsonObject.getString("projectId");
            this.signalId = jsonObject.getString("signalId");
            this.signalDefinition = controller.getSignalDefinition(projectId, signalId);
            this.keyParameterId = signalDefinition.getKeyParameterId();
            this.signalParameters = parameterSerializer.deserializeParameters(signalDefinition.getParameterDefinitions(), jsonObject.getJsonObject("parameters"));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while deserializing signal received event.", e);
        }
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public String getSignalIdentifier()
    {
        return (String)source;
    }

    public Map<String, Object> getSignalParameters()
    {
        return signalParameters;
    }

    @Override
    public T8FlowExecutionKey getExecutionKey(T8WorkFlowNodeDefinition waitingNodeDefinition, T8FlowNodeState waitingNodeState)
    {
        try
        {
            String conditionExpression;

            // Get the signal condition expression (the instanceof here is bad design and will be fixed when flow definitions are revised).
            if (waitingNodeDefinition instanceof T8SignalCatchEventDefinition)
            {
                conditionExpression = ((T8SignalCatchEventDefinition)waitingNodeDefinition).getConditionExpression();
            }
            else if (waitingNodeDefinition instanceof T8SignalCatchEventDefinition)
            {
                conditionExpression = ((T8SignalAccumulationEventDefinition)waitingNodeDefinition).getConditionExpression();
            }
            else conditionExpression = null;

            // Evaluate the condition expression (if there is one) and only process the signal if the expression returns true.
            if (evaluateConditionExpression(conditionExpression, waitingNodeState.getExecutionParameters()))
            {
                return new T8SignalExecutionKey(waitingNodeState, signalId, signalParameters);
            }
            else return null;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while evaluating signal condition expression for node state: " + waitingNodeState, e);
            return null;
        }
    }

    @Override
    public T8DataFilterCriteria getWaitKeyFilterCriteria()
    {
        Object keyParameterValue;

        keyParameterValue = signalParameters.get(signalId + keyParameterId);
        if (keyParameterValue != null)
        {
            Map<String, Object> filterMap;

            filterMap = new HashMap<String, Object>();
            filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_ID", WaitKeyIdentifier.SIGNAL.toString());
            filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_SIGNAL_PROJECT_ID", projectId);
            filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_SIGNAL_ID", signalId);
            filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_SIGNAL_PARAMETER", keyParameterValue);
            return new T8DataFilterCriteria(filterMap);
        }
        else throw new RuntimeException("Signal '" + signalId + "' Key Parameter '" + keyParameterId + "' not found.  Signal parameters: " + signalParameters);
    }

    public boolean evaluateConditionExpression(String conditionExpression, Map<String, Object> conditionExpressionParameters) throws Exception
    {
        if (!Strings.isNullOrEmpty(conditionExpression))
        {
            Map<String, Object> expressionParameters;
            ExpressionEvaluator expressionEvaluator;

            expressionParameters = new HashMap<String, Object>();
            if (conditionExpressionParameters != null) expressionParameters.putAll(conditionExpressionParameters);
            if (signalParameters != null) expressionParameters.putAll(signalParameters);

            expressionEvaluator = new ExpressionEvaluator();
            return expressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null);
        }
        else return true;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        try
        {
            T8JsonParameterSerializer parameterSerializer;
            JsonObject object;

            parameterSerializer = new T8JsonParameterSerializer();

            object = new JsonObject();
            object.add("type", TYPE_ID);
            object.add("projectId", signalDefinition.getRootProjectId());
            object.add("signalId", signalId);
            object.add("keyParameterId", keyParameterId);
            object.add("parameters", parameterSerializer.serializeParameters(signalDefinition.getParameterDefinitions(), signalParameters));
            return object;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while serializing event " + this + ": " + e);
        }
    }
}
