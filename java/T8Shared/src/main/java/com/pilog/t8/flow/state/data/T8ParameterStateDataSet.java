package com.pilog.t8.flow.state.data;

import com.pilog.t8.data.T8DataEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ParameterStateDataSet implements Serializable
{
    private final Map<String, List<T8DataEntity>> parameterData;
    private final String parentParameterFieldId;

    public T8ParameterStateDataSet(String parentParameterFieldId)
    {
        this.parentParameterFieldId = parentParameterFieldId;
        this.parameterData = new LinkedHashMap<>();
    }

    public List<T8DataEntity> getParameterEntitiesByParentIID(String parentParameterIid)
    {
        List<T8DataEntity> parameterEntities;

        parameterEntities = parameterData.get(parentParameterIid);
        return parameterEntities != null ? parameterEntities : new ArrayList<>();
    }

    public int getEntityCount()
    {
        int count;

        count = 0;
        for (List<T8DataEntity> entityList : parameterData.values())
        {
            count += entityList.size();
        }

        return count;
    }

    public void addParameterData(String parentParameterIid, T8DataEntity parameterEntity)
    {
        List<T8DataEntity> entityList;

        entityList = parameterData.get(parentParameterIid);
        if (entityList == null)
        {
            entityList = new ArrayList<>();
            entityList.add(parameterEntity);
            parameterData.put(parentParameterIid, entityList);
        }
        else
        {
            entityList.add(parameterEntity);
        }
    }

    public void addParameterData(T8DataEntity entity)
    {
        String parentParameterIid;

        parentParameterIid = (String)entity.getFieldValue(parentParameterFieldId);
        addParameterData(parentParameterIid, entity);
    }

    public void addParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            String parentParameterIid;

            parentParameterIid = (String)entity.getFieldValue(parentParameterFieldId);
            addParameterData(parentParameterIid, entity);
        }
    }
}
