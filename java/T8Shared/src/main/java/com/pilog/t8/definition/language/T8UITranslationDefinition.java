package com.pilog.t8.definition.language;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8UITranslationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_TRANSLATION";
    public static final String GROUP_IDENTIFIER = "@DG_UI_TRANSLATION";
    public static final String STORAGE_PATH = "/ui_translations";
    public static final String DISPLAY_NAME = "UI Translation";
    public static final String DESCRIPTION = "A set of language strings to be used for translation of displayed text on the user interface.";
    public static final String IDENTIFIER_PREFIX = "LTUI_";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.SYSTEM;
    public enum Datum {LANGUAGE_IDENTIFIER,
                       TRANSLATION_MAP};
    // -------- Definition Meta-Data -------- //

    public T8UITranslationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.LANGUAGE_IDENTIFIER.toString(), "Language", "The language to which strings in the set are translated."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.STRING), Datum.TRANSLATION_MAP.toString(), "Translation Map", "A mapping of the input phrase (in en-US) to the output phrase in the language supported by this translation set."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.LANGUAGE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8LanguageDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getLanguageIdentifier()
    {
        return (String)getDefinitionDatum(Datum.LANGUAGE_IDENTIFIER.toString());
    }

    public void setLanguageIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.LANGUAGE_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getTranslationMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.TRANSLATION_MAP.toString());
    }

    public void setTranslationMap(Map<String, String> translationMap)
    {
        setDefinitionDatum(Datum.TRANSLATION_MAP.toString(), translationMap);
    }
}
