package com.pilog.t8.definition.flow;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskGroupDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_TASK_GROUP";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_TASK_GROUP";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "TASK_GROUP_";
    public static final String DISPLAY_NAME = "Task Group";
    public static final String DESCRIPTION = "A group to which tasks in this workflow can be assigned for escalation and reporting purposes.";
    public enum Datum {DISPLAY_NAME};
    // -------- Definition Meta-Data -------- //

    public T8FlowTaskGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The display name of this task group."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getDisplayName()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_NAME.toString());
    }

    public void setDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME.toString(), displayName);
    }
}
