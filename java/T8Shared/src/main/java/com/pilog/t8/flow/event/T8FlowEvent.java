package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FlowEvent extends EventObject
{
    private final String eventIid;
    private final long time;

    public T8FlowEvent(String eventIid, Object source)
    {
        super(source);
        this.eventIid = eventIid;
        this.time = System.currentTimeMillis();
    }

    public T8FlowEvent(Object source)
    {
        this(T8IdentifierUtilities.createNewGUID(), source);
    }

    public String getEventIid()
    {
        return eventIid;
    }

    public long getEventTimeInMillis()
    {
        return time;
    }

    public abstract String getEventTypeId();

    /**
     * Creates an execution key for from this event for the specified node
     * state.  If this event is not applicable to the supplied node state, this
     * method will return null, indicating that no execution key is applicable.
     * @param waitingNodeDefinition The definition of the waiting node for which
     * to create the execution key.
     * @param waitingNodeState The node state for which to create the execution
     * key.
     * @return The execution key matching this event, created for the supplied
     * node state.
     */
    public T8FlowExecutionKey getExecutionKey(T8WorkFlowNodeDefinition waitingNodeDefinition, T8FlowNodeState waitingNodeState)
    {
        return null;
    }

    /**
     * Returns the filter criteria that can be used to fetch all wait key
     * entities that will be matched by this event from the persistence store.
     * @return The filter criteria that can be used to fetch all wait key
     * entities that will be matched by this event from the persistence store.
     */
    public T8DataFilterCriteria getWaitKeyFilterCriteria()
    {
        return null;
    }

    /**
     * Serializes this event to JSON.
     * @param controller The flow controller where this event occurred.
     * @return The JSON string serialization of this event.
     */
    public abstract JsonObject serialize(T8FlowController controller);

    /**
     * De-serializes an event from the supplied JSON object.
     * @param controller The controller requesting the de-serialization.
     * @param eventObject The JSON object to de-serialize.
     * @return The T8FlowEvent de-serialized from the supplied JSON object.
     */
    public static T8FlowEvent deserialize(T8FlowController controller, JsonObject eventObject)
    {
        String type;

        type = eventObject.getString("type");
        if (type != null)
        {
            switch (type)
            {
                case T8FlowSignalReceivedEvent.TYPE_ID:
                    return new T8FlowSignalReceivedEvent(controller, eventObject);
                case T8FlowTaskCompletedEvent.TYPE_ID:
                    return new T8FlowTaskCompletedEvent(controller, eventObject);
                default:
                    throw new RuntimeException("Unsupported flow event encountered during deserialization: " + type);
            }
        }
        else throw new RuntimeException("Cannot deserialize T8FlowEvent from JSON string without a specified 'type': " + eventObject);
    }
}
