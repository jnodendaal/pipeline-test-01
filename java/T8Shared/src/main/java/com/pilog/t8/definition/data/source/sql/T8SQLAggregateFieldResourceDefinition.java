/**
 * Created on 08 Oct 2015, 2:35:12 PM
 */
package com.pilog.t8.definition.data.source.sql;

import com.pilog.t8.data.type.T8DataType;

/**
 * @author Gavin Boshoff
 */
public class T8SQLAggregateFieldResourceDefinition extends T8SQLAggregateFieldDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_FIELD_AGGREGATE_RESOURCE";
    public static final String DISPLAY_NAME = "Resource Aggregate Function Field";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8SQLAggregateFieldResourceDefinition(String identifier)
    {
        super(identifier);
    }

    public T8SQLAggregateFieldResourceDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, T8DataType dataType, String aggregateString)
    {
        super(identifier, sourceIdentifier, key, required, dataType);
        setAggregateFunctionString(aggregateString);
    }
}