package com.pilog.t8;

import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import java.io.Serializable;
import java.util.Map;

/**
 * This class represents a report of the current status of an operation
 * executing on the server.
 *
 * @author Bouwer du Preez
 */
public class T8ServerOperationStatusReport implements Serializable
{
    private String operationInstanceIdentifier;
    private String operationIdentifier;
    private double operationProgress;
    private long executionStartTime;
    private long executionEndTime;
    private T8ProgressReport progressReportObject;
    private Throwable operationThrowable;
    private Map<String, Object> operationResult;
    private T8ServerOperationStatus operationStatus;
    private transient T8ServerOperationNotificationHandle notificationHandle;

    public String getOperationIdentifier()
    {
        return operationIdentifier;
    }

    public void setOperationIdentifier(String identifier)
    {
        this.operationIdentifier = identifier;
    }

    public String getOperationInstanceIdentifier()
    {
        return operationInstanceIdentifier;
    }

    public void setOperationInstanceIdentifier(String operationInstanceIdentifier)
    {
        this.operationInstanceIdentifier = operationInstanceIdentifier;
    }

    public double getOperationProgress()
    {
        return operationProgress;
    }

    public void setOperationProgress(double operationProgress)
    {
        this.operationProgress = operationProgress;
    }

    public T8ServerOperationStatus getOperationStatus()
    {
        return operationStatus;
    }

    public void setOperationStatus(T8ServerOperationStatus operationStatus)
    {
        this.operationStatus = operationStatus;
    }

    public long getExecutionStartTime()
    {
        return executionStartTime;
    }

    public void setExecutionStartTime(long executionStartTime)
    {
        this.executionStartTime = executionStartTime;
    }

    public long getExecutionEndTime()
    {
        return executionEndTime;
    }

    public void setExecutionEndTime(long executionStartTime)
    {
        this.executionEndTime = executionStartTime;
    }

    public T8ProgressReport getProgressReportObject()
    {
        return progressReportObject;
    }

    public void setProgressReportObject(T8ProgressReport progressReportObject)
    {
        this.progressReportObject = progressReportObject;
    }

    public Map<String, Object> getOperationResult()
    {
        return operationResult;
    }

    public void setOperationResult(Map<String, Object> operationResult)
    {
        this.operationResult = operationResult;
    }

    public Throwable getOperationThrowable()
    {
        return operationThrowable;
    }

    public void setOperationThrowable(Throwable operationThrowable)
    {
        this.operationThrowable = operationThrowable;
    }

    public T8ServerOperationNotificationHandle getNotificationHandle()
    {
        return notificationHandle;
    }

    public void setNotificationHandle(T8ServerOperationNotificationHandle notificationHandle)
    {
        this.notificationHandle = notificationHandle;
    }
}
