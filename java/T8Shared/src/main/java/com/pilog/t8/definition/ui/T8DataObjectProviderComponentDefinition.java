package com.pilog.t8.definition.ui;

/**
 * @author Bouwer du Preez
 */
public interface T8DataObjectProviderComponentDefinition
{
    // Currently this is a marker interface and does not contain any methods.
}
