package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtMap extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@MAP";

    private final T8DataType keyDataType;
    private final T8DataType valueDataType;

    public T8DtMap(T8DataType keyDataType, T8DataType valueDataType)
    {
        this.keyDataType = keyDataType;
        this.valueDataType = valueDataType;
    }

    public T8DtMap(T8DefinitionManager context)
    {
        this(new T8DtUndefined(context), new T8DtUndefined(context));
    }

    public T8DtMap(T8DefinitionManager context, String stringRepresentation)
    {
        if (stringRepresentation.startsWith(IDENTIFIER))
        {
            String keyStringRepresentation;
            String valueStringRepresentation;
            int openBracketIndex;
            int closeBracketIndex;
            int commaIndex;

            // Get the indices of the delimiters.
            openBracketIndex = stringRepresentation.indexOf('<');
            closeBracketIndex = stringRepresentation.lastIndexOf('>');
            commaIndex = stringRepresentation.indexOf(',');

            // Get the required substrings.
            keyStringRepresentation = stringRepresentation.substring(openBracketIndex + 1, commaIndex).trim();
            valueStringRepresentation = stringRepresentation.substring(commaIndex + 1, closeBracketIndex).trim();

            // Construct the key and value types.
            keyDataType = context.createDataType(keyStringRepresentation);
            valueDataType = context.createDataType(valueStringRepresentation);
        }
        else throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    public T8DataType getKeyDataType()
    {
        return keyDataType;
    }

    public T8DataType getValueDataType()
    {
        return valueDataType;
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER + "<" + keyDataType.getDataTypeStringRepresentation(includeAttributes) + "," + valueDataType.getDataTypeStringRepresentation(includeAttributes) + ">";
    }

    @Override
    public String getDataTypeClassName()
    {
        return HashMap.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        Map<String, Object> map;

        map = (Map)object;
        if (map != null)
        {
            JsonObject jsonMap;

            jsonMap = new JsonObject();
            for (String key : map.keySet())
            {
                Object value;

                value = map.get(key);
                jsonMap.add(key, valueDataType.serialize(value));
            }

            return jsonMap;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Map deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject jsonMap;
            LinkedHashMap map;

            map = new LinkedHashMap();
            jsonMap = (JsonObject)jsonValue;
            for (String key : jsonMap.names())
            {
                JsonValue value;

                value = jsonMap.get(key);
                if (value == null) map.put(key, null);
                else map.put(key, valueDataType.deserialize(value));
            }

            return map;
        }
        else return null;
    }
}
