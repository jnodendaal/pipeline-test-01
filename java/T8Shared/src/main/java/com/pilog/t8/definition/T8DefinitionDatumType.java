package com.pilog.t8.definition;

import com.pilog.t8.data.type.T8DataType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDatumType implements Serializable
{
    private T8DataType dataType;
    private String datumIdentifier;
    private String displayName;
    private String description;
    private Object defaultValue;
    private int maximumElements;
    private boolean trim;
    private boolean hidden;
    private boolean mutable;
    private boolean persisted;
    private T8DefinitionDatumOptionType optionType;
    private List<T8DefinitionDatumDependency> datumDependencies;

    public enum T8DefinitionDatumOptionType {DEFAULT, ENUMERATION};

    public T8DefinitionDatumType(T8DataType dataType, String datumIdentifier, String displayName, String description)
    {
        this.dataType = dataType;
        this.datumIdentifier = datumIdentifier;
        this.displayName = displayName;
        this.description = description;
        this.maximumElements = (dataType.isType(T8DataType.MAP)) || (dataType.isType(T8DataType.LIST)) ? -1 : 1;
        this.hidden = false;
        this.persisted = true;
        this.mutable = true;
        this.trim = true;
        this.datumDependencies = new ArrayList<T8DefinitionDatumDependency>();
        this.optionType = T8DefinitionDatumOptionType.DEFAULT;
    }

    public T8DefinitionDatumType(T8DataType dataType, String datumIdentifier, String displayName, String description, Object defaultValue)
    {
        this(dataType, datumIdentifier, displayName, description);
        this.defaultValue = defaultValue;
    }

    public T8DefinitionDatumType(T8DataType dataType, String datumIdentifier, String displayName, String description, Object defaultValue, T8DefinitionDatumOptionType optionType)
    {
        this(dataType, datumIdentifier, displayName, description, defaultValue);
        this.optionType = optionType;
    }

    public T8DefinitionDatumType(T8DataType dataType, String datumIdentifier, String displayName, String description, int maximumContentElements, boolean persisted, boolean mutable)
    {
        this(dataType, datumIdentifier, displayName, description);
        this.maximumElements = maximumContentElements;
        this.persisted = persisted;
        this.mutable = mutable;
    }

    public static T8DefinitionDatumType createUntrimmedStringType(String datumIdentifier, String displayName, String description, String defaultValue)
    {
        T8DefinitionDatumType datumType;

        datumType = new T8DefinitionDatumType(T8DataType.STRING, datumIdentifier, displayName, description, defaultValue);
        datumType.setTrim(false);
        return datumType;
    }

    public T8DefinitionDatumOptionType getOptionType()
    {
        return optionType;
    }

    public T8DataType getDataType()
    {
        return dataType;
    }

    public String getIdentifier()
    {
        return datumIdentifier;
    }

    public int getMaximumElements()
    {
        return maximumElements;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isMutable()
    {
        return mutable;
    }

    public void setMutable(boolean mutable)
    {
        this.mutable = mutable;
    }

    public boolean isPersisted()
    {
        return persisted;
    }

    public void setPersisted(boolean persisted)
    {
        this.persisted = persisted;
    }

    public boolean isTrim()
    {
        return trim;
    }

    public void setTrim(boolean trim)
    {
        this.trim = trim;
    }

    public boolean isListType()
    {
        return dataType.isType(T8DataType.LIST);
    }

    public boolean isSubDefinitionType()
    {
        return isSingleDefinitionType() || isDefinitionListType();
    }

    public boolean isSingleDefinitionType()
    {
        return dataType.equals(T8DataType.DEFINITION);
    }

    public boolean isDefinitionListType()
    {
        return dataType.equals(T8DataType.DEFINITION_LIST);
    }

    public boolean isIdentifierListType()
    {
        return dataType.equals(T8DataType.DEFINITION_IDENTIFIER_LIST);
    }

    public Object getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(Object defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public T8DefinitionDatumType setHidden(boolean hidden)
    {
        this.hidden = hidden;
        return this;
    }

    public T8DefinitionDatumType addDatumDependency(T8DefinitionDatumDependency dependency)
    {
        if (datumDependencies == null)
        {
            datumDependencies = new ArrayList<T8DefinitionDatumDependency>();
        }

        datumDependencies.add(dependency);
        return this;
    }

    public List<T8DefinitionDatumDependency> getDatumDependencies()
    {
        return datumDependencies;
    }

    public void setDatumDependencies(List<T8DefinitionDatumDependency> datumDependencies)
    {
        this.datumDependencies = datumDependencies;
    }

    public static T8DefinitionDatumType identifierType(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, identifier, displayName, description, 1, true, true);
    }

    public static T8DefinitionDatumType identifierList(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_LIST, identifier, displayName, description, -1, true, true);
    }

    public static T8DefinitionDatumType identifierMap(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_MAP, identifier, displayName, description, -1, true, true);
    }

    public static T8DefinitionDatumType identifierValueMap(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_VALUE_MAP, identifier, displayName, description, -1, true, true);
    }

    public static T8DefinitionDatumType definitionType(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION, identifier, displayName, description, 1, true, true);
    }

    public static T8DefinitionDatumType definitionList(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION_LIST, identifier, displayName, description, -1, true, true);
    }

    public static T8DefinitionDatumType derivedDefinitionList(String identifier, String displayName, String description)
    {
        return new T8DefinitionDatumType(T8DataType.DEFINITION_LIST, identifier, displayName, description, -1, false, false);
    }

    @Override
    public String toString()
    {
        return "T8DefinitionDatumType{datumIdentifier=" + datumIdentifier + ", dataType=" + dataType + "}";
    }
}
