package com.pilog.t8.definition.ui.editor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8DefinitionEditor;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_DEFINITION_EDITOR";
    public static final String GROUP_NAME = "Definition Editors";
    public static final String GROUP_DESCRIPTION = "Specifications of components to be used when displaying and editing the content of definitions in the system.";
    public static final String STORAGE_PATH = null;
    // -------- Definition Meta-Data -------- //

    public T8DefinitionEditor getNewDefinitionEditorInstance(T8ClientContext clientContext, T8SessionContext sessionContext, T8Definition editorDefinition);
}
