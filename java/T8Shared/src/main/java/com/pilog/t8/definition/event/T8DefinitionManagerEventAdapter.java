/**
 * Created on 18 Jun 2015, 7:56:17 AM
 */
package com.pilog.t8.definition.event;

/**
 * @author Gavin Boshoff
 */
@SuppressWarnings("NoopMethodInAbstractClass")
public abstract class T8DefinitionManagerEventAdapter implements T8DefinitionEventHandler
{
    @Override
    public void initHandler() throws Exception {}

    @Override
    public void definitionDeleted(T8DefinitionDeletedEvent event) {}

    @Override
    public void definitionSaved(T8DefinitionSavedEvent event) {}

    @Override
    public void definitionLocked(T8DefinitionLockedEvent event) {}

    @Override
    public void definitionUnlocked(T8DefinitionUnlockedEvent event) {}

    @Override
    public void definitionRenamed(T8DefinitionRenamedEvent event) {}

    @Override
    public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent event) {}

    @Override
    public void definitionImported(T8DefinitionImportedEvent event) {}
}