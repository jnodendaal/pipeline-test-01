package com.pilog.t8.data.filter.groupby;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.data.filter.groupby.T8GroupByDataFilterDefinition;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8GroupByDataFilter extends T8DataFilter
{
    private T8DataFilter innerFilter;

    public T8GroupByDataFilter(String entityIdentifier)
    {
        super(entityIdentifier);
    }

    public T8GroupByDataFilter(T8GroupByDataFilterDefinition definition)
    {
        super(definition);
    }

    public T8GroupByDataFilter(T8GroupByDataFilterDefinition definition, T8DataFilter innerFilter)
    {
        super(definition);
        this.innerFilter = innerFilter;
    }

    public T8GroupByDataFilter(String entityIdentifier, Map<String, Object> keyMap)
    {
        super(entityIdentifier, keyMap);
    }

    public T8GroupByDataFilter(String entityIdentifier, Map<String, Object> keyMap, boolean caseInsensitive)
    {
        super(entityIdentifier, keyMap, caseInsensitive);
    }

    public T8GroupByDataFilter(String entityIdentifier, T8DataFilterCriteria filterCriteria)
    {
        super(entityIdentifier);
        addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, filterCriteria);
    }

    public T8GroupByDataFilter(String entityIdentifier, List<Map<String, Object>> keyList)
    {
        super(entityIdentifier, keyList);
    }

    /**
     * @return the innerFilter
     */
    public T8DataFilter getInnerFilter()
    {
        return innerFilter;
    }

    /**
     * @param innerFilter the innerFilter to set
     */
    public void setInnerFilter(T8DataFilter innerFilter)
    {
        this.innerFilter = innerFilter;
    }

    @Override
    public T8DataFilter addFilterCriteria(T8DataFilterClause.DataFilterConjunction conjunction, T8DataFilter dataFilter)
    {
        if(dataFilter instanceof T8GroupByDataFilter)
        {
            T8GroupByDataFilter filter = (T8GroupByDataFilter) dataFilter;
            if(filter.getInnerFilter() != null && filter.getInnerFilter().hasFilterCriteria())
            {
                if (innerFilter == null) innerFilter = filter.getInnerFilter();
                else innerFilter.addFilterCriteria(conjunction, filter.getInnerFilter());
            }

            super.addFilterCriteria(conjunction, dataFilter);
        }
        else if (innerFilter == null)
        {
            innerFilter = dataFilter;
        }
        else if (innerFilter != null)
        {
            innerFilter.addFilterCriteria(conjunction, dataFilter);
        }

        return this;
    }

    @Override
    public T8DataFilter addFilterCriteria(String conjunction, T8DataFilter dataFilter)
    {
        super.addFilterCriteria(conjunction, dataFilter);

        if(dataFilter instanceof T8GroupByDataFilter)
        {
            T8GroupByDataFilter filter = (T8GroupByDataFilter) dataFilter;
            if(filter.getInnerFilter() != null && filter.getInnerFilter().hasFilterCriteria())
            {
                if(innerFilter == null) innerFilter = filter.getInnerFilter();
                else innerFilter.addFilterCriteria(conjunction, filter.getInnerFilter());
            }
        }

        return this;
    }

    @Override
    public T8DataFilter copy()
    {
        T8GroupByDataFilter copiedFilter;

        copiedFilter = new T8GroupByDataFilter(getEntityIdentifier());

        copiedFilter.setFieldOrdering(new LinkedHashMap<>(getFieldOrdering()));
        if (getFilterCriteria() != null) copiedFilter.setFilterCriteria(getFilterCriteria().copy());

        copiedFilter.setFieldGrouping(new ArrayList<>(getFieldGrouping()));

        if(innerFilter != null) copiedFilter.setInnerFilter(innerFilter.copy());

        return copiedFilter;
    }
}
