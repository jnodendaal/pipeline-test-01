package com.pilog.t8.definition.ui;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.documentation.T8DefaultDefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationType;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentDefinitionDocumentationProvider extends T8DefaultDefinitionDocumentationProvider
{
    public static final String COMPONENT_API_DOCUMENTATION_TYPE_IDENTIFIER = "COMPONENT_API";
    
    public T8ComponentDefinitionDocumentationProvider(T8DefinitionContext definitionContext)
    {
        super(definitionContext);
        documentationTypes.put(COMPONENT_API_DOCUMENTATION_TYPE_IDENTIFIER, new T8DefinitionDocumentationType(COMPONENT_API_DOCUMENTATION_TYPE_IDENTIFIER, "Component API", "Documentation describing the API events and operations of the T8 Component."));
    }
    
    @Override
    public StringBuffer getHTMLDocumentation(T8Definition definition, String typeIdentifier)
    {
        if (COMPONENT_API_DOCUMENTATION_TYPE_IDENTIFIER.equals(typeIdentifier))
        {
            return createComponentAPIDocumentationString(definitionContext, (T8ComponentDefinition)definition);
        }
        else return super.getHTMLDocumentation(definition, typeIdentifier);
    }
    
    protected StringBuffer createComponentAPIDocumentationString(T8DefinitionContext definitionContext, T8ComponentDefinition componentDefinition)
    {
        T8DefinitionTypeMetaData definitionMetaData;
        StringBuffer documentation;
        
        definitionMetaData = componentDefinition.getTypeMetaData();
        
        documentation = new StringBuffer();
        documentation.append("<h1>Component API: " + definitionMetaData.getDisplayName() + "</h1>");
        documentation.append("</br>");
        documentation.append(createDefinitionDetailsString(definitionContext, componentDefinition));
        documentation.append("</br>");
        documentation.append(createEventDocumentationString(definitionContext, componentDefinition));
        documentation.append("</br>");
        documentation.append(createOperationDocumentationString(definitionContext, componentDefinition));
        documentation.append("</br>");
        documentation.append(createInputParameterDocumentationString(definitionContext, componentDefinition));
        documentation.append("</br>");
        documentation.append(createDefinitionDatumString(definitionContext, componentDefinition));
        return documentation;
    }
    
    protected StringBuffer createEventDocumentationString(T8DefinitionContext definitionContext, T8ComponentDefinition componentDefinition)
    {
        List<T8ComponentEventDefinition> eventDefinitions;
        StringBuffer documentation;
        
        eventDefinitions = componentDefinition.getComponentEventDefinitions();
        
        documentation = new StringBuffer();
        documentation.append("<h2>Events</h2>");
        if ((eventDefinitions != null) && (eventDefinitions.size() > 0))
        {
            documentation.append("<table border=\"1\">");
            documentation.append("<tr><th>Event Identifier</th><th>Event Name</th><th>Description</th><th>Parameters</th></tr>");
            for (T8ComponentEventDefinition eventDefinition : eventDefinitions)
            {
                documentation.append("<tr>");
                documentation.append("<td>" + eventDefinition.getIdentifier() + "</td>");
                documentation.append("<td>" + eventDefinition.getMetaDisplayName() + "</td>");
                documentation.append("<td>" + eventDefinition.getMetaDescription() + "</td>");

                documentation.append("<td>");
                documentation.append("<ul>");
                for (T8DataValueDefinition parameterDefinition : eventDefinition.getParameterDefinitions())
                {
                    documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
                }
                documentation.append("</ul>");
                documentation.append("</td>");

                documentation.append("</tr>");
            }
            documentation.append("</table>");
        }
        else
        {
            documentation.append("This component does not have any defined events.");
        }
        
        return documentation;
    }
    
    protected StringBuffer createOperationDocumentationString(T8DefinitionContext definitionContext, T8ComponentDefinition componentDefinition)
    {
        List<T8ComponentOperationDefinition> operationDefinitions;
        StringBuffer documentation;
        
        operationDefinitions = componentDefinition.getComponentOperationDefinitions();
        
        documentation = new StringBuffer();
        documentation.append("<h2>Operations</h2>");
        if ((operationDefinitions != null) && (operationDefinitions.size() > 0))
        {
            documentation.append("<table border=\"1\">");
            documentation.append("<tr><th>Operation Identifier</th><th>Operation Name</th><th>Description</th><th>Input Parameters</th><th>Output Parameters</th></tr>");
            for (T8ComponentOperationDefinition operationDefinition : operationDefinitions)
            {
                documentation.append("<tr>");
                documentation.append("<td>" + operationDefinition.getIdentifier() + "</td>");
                documentation.append("<td>" + operationDefinition.getMetaDisplayName() + "</td>");
                documentation.append("<td>" + operationDefinition.getMetaDescription() + "</td>");

                // Add the input parameter section.
                documentation.append("<td>");
                documentation.append("<ul>");
                for (T8DataValueDefinition parameterDefinition : operationDefinition.getInputParameterDefinitions())
                {
                    documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
                }
                documentation.append("</ul>");
                documentation.append("</td>");
                
                // Add the output parameter section.
                documentation.append("<td>");
                documentation.append("<ul>");
                for (T8DataValueDefinition parameterDefinition : operationDefinition.getOutputParameterDefinitions())
                {
                    documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
                }
                documentation.append("</ul>");
                documentation.append("</td>");

                documentation.append("</tr>");
            }
            documentation.append("</table>");
        }
        else
        {
            documentation.append("This component does not have any defined operations.");
        }
        
        return documentation;
    }
    
    protected StringBuffer createInputParameterDocumentationString(T8DefinitionContext definitionContext, T8ComponentDefinition componentDefinition)
    {
        List<T8DataValueDefinition> parameterDefinitions;
        StringBuffer documentation;
        
        parameterDefinitions = componentDefinition.getComponentInputParameterDefinitions();
        
        documentation = new StringBuffer();
        documentation.append("<h2>Input Parameters</h2>");
        if ((parameterDefinitions != null) && (parameterDefinitions.size() > 0))
        {
            documentation.append("<table border=\"1\">");
            documentation.append("<tr><th>Parameter Identifier</th><th>Parameter Name</th><th>Description</th><th>Data Type</th></tr>");
            for (T8DataValueDefinition parameterDefinition : parameterDefinitions)
            {
                documentation.append("<tr>");
                documentation.append("<td>" + parameterDefinition.getIdentifier() + "</td>");
                documentation.append("<td>" + parameterDefinition.getMetaDisplayName() + "</td>");
                documentation.append("<td>" + parameterDefinition.getMetaDescription() + "</td>");
                documentation.append("<td>" + parameterDefinition.getDataType() + "</td>");
                documentation.append("</tr>");
            }
            documentation.append("</table>");
        }
        else
        {
            documentation.append("This component does not have any defined input parameters.");
        }
        
        return documentation;
    }
}
