package com.pilog.t8.definition.ui.module;

import com.pilog.t8.definition.data.T8VariableDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleVariableDefinition extends T8VariableDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_MODULE_VARIABLE";
    public static final String DISPLAY_NAME = "Module Variable";
    public static final String IDENTIFIER_PREFIX = "V_";
    public static final String STORAGE_PATH = null;
    public static final String DESCRIPTION = "A variable used within the scopy of a ui module.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8ModuleVariableDefinition(String identifier)
    {
        super(identifier);
    }
}
