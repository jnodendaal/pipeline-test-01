package com.pilog.t8.communication;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationResult
{
    /**
     * The result of the message communication
     */
    public enum Result
    {
        Sent,
        NotSent,
        Delivered,
        Error
    }

    /***
     * This is used to tell the system if the message was successfully sent, this does NOT mean the message was delivered.
     * @return true if the message was sent
     */
    public boolean isSuccess();
    /***
     * @return The result of this communication
     */
    public Result getSendResult();
    /**
     * @return Communication result message
     */
    public String getMessage();
    /**
     * @return Exception, if an exception was thrown during communication
     */
    public Exception getException();
    /**
     * @return The Communication Message that was used when sending this communication
     */
    public T8CommunicationMessage getCommunicationMessage();
    /**
     * @return The message ID of the sent message.(Note: the service might not support message ID's)
     */
    public String getMessageId();
    /**
     * @param message The id of the sent message
     */
    public void setMessageId(String message);
}
