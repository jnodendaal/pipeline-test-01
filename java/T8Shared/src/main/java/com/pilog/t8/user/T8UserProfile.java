package com.pilog.t8.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8UserProfile
{
    private final String identifier;
    private final Set<String> functionalityIdentifiers;
    private final List<T8UserProfile> includedProfiles;

    public T8UserProfile(String identifier)
    {
        this.identifier = identifier;
        this.functionalityIdentifiers = new HashSet<>();
        this.includedProfiles = new ArrayList<>();
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void addIncludedProfile(T8UserProfile includedProfile)
    {
        includedProfiles.add(includedProfile);
    }

    public void addFunctionalityIdentifier(String identifier)
    {
        functionalityIdentifiers.add(identifier);
    }

    public Set<String> getFunctionalityIdentifiers()
    {
        return new HashSet<>(functionalityIdentifiers);
    }

    public void setFunctionalityIdentifiers(Collection<String> identifiers)
    {
        functionalityIdentifiers.clear();
        if (identifiers != null)
        {
            functionalityIdentifiers.addAll(identifiers);
        }
    }

    /**
     * Retrieves all of the accessible functionality identifiers for both the
     * current user profile as well as all of the included user profiles.
     *
     * @return The {@code Set} of accessible functionality identifiers
     */
    public Set<String> getAccessibleFunctionalityIdentifiers()
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<>(functionalityIdentifiers);
        includedProfiles.forEach(includedProfile -> identifierSet.addAll(includedProfile.getAccessibleFunctionalityIdentifiers()));

        return identifierSet;
    }
}
