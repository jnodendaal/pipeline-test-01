package com.pilog.t8.definition;

import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTypeMetaData implements Serializable
{
    private String typeId;
    private String groupId;
    private String name;
    private String description;
    private String storagePath;
    private String idPrefix;
    private String className;
    private T8DefinitionLevel definitionLevel;

    public T8DefinitionTypeMetaData()
    {
        this.definitionLevel = T8DefinitionLevel.PROJECT;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDisplayName()
    {
        return name;
    }

    public void setDisplayName(String displayName)
    {
        this.name = displayName;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getTypeId()
    {
        return typeId;
    }

    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    public String getStoragePath()
    {
        return storagePath;
    }

    public void setStoragePath(String storagePath)
    {
        this.storagePath = storagePath;
    }

    public String getClassName()
    {
        return className;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }

    public String getIdPrefix()
    {
        return idPrefix;
    }

    public void setIdPrefix(String idPrefix)
    {
        this.idPrefix = idPrefix;
    }

    public T8DefinitionLevel getDefinitionLevel()
    {
        return definitionLevel;
    }

    public void setDefinitionLevel(T8DefinitionLevel definitionLevel)
    {
        this.definitionLevel = definitionLevel;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;
        toStringBuilder = new StringBuilder("T8DefinitionTypeMetaData{");
        toStringBuilder.append("typeId=").append(this.typeId);
        toStringBuilder.append(",groupId=").append(this.groupId);
        toStringBuilder.append(",displayName=").append(this.name);
        toStringBuilder.append(",description=").append(this.description);
        toStringBuilder.append(",storagePath=").append(this.storagePath);
        toStringBuilder.append(",idPrefix=").append(this.idPrefix);
        toStringBuilder.append(",className=").append(this.className);
        toStringBuilder.append(",definitionLevel=").append(this.definitionLevel);
        toStringBuilder.append('}');
        return toStringBuilder.toString();
    }
}
