package com.pilog.t8.definition.flow.task.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8FlowTaskModuleInputParameterScriptDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowUserTaskModuleDefinition extends T8Definition implements T8WorkFlowUserTaskUIDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_TASK_USER_MODULE";
    public static final String IDENTIFIER_PREFIX = "FT_USER_MODULE_";
    public static final String DISPLAY_NAME = "User Task Module";
    public static final String DESCRIPTION = "A module that may be used for the completion of a user task.";
    public enum Datum {MODULE_IDENTIFIER,
                       MODULE_INPUT_PARAMETER_MAPPING,
                       MODULE_INPUT_PARAMETER_SCRIPT_DEFINITION,
                       FINALIZATION_EVENT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowUserTaskModuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.MODULE_IDENTIFIER.toString(), "Module", "The identifier of the module to load when executing this task."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.MODULE_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Task Input to Module Input", "A mapping of Task Input Parameters to the corresponding Module Input Parameters.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.MODULE_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.MODULE_INPUT_PARAMETER_SCRIPT_DEFINITION.toString(), "Additional Module Input Parameter Script", "A script that is executed to compile additional module input parameters."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FINALIZATION_EVENT_DEFINITIONS.toString(), "Finalization Events", "The module events that will be listened for to detect task finalization."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MODULE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ModuleDefinition.GROUP_IDENTIFIER));
        else if (Datum.MODULE_INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String moduleId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            moduleId = getModuleIdentifier();
            if (moduleId != null)
            {
                T8ModuleDefinition moduleDefinition;
                T8WorkFlowUserTaskDefinition taskDefinition;
                List<T8DataParameterDefinition> moduleParameterDefinitions;
                List<T8DataParameterDefinition> taskParameterDefinitions;
                HashMap<String, List<String>> identifierMap;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                moduleParameterDefinitions = moduleDefinition.getInputParameterDefinitions();
                taskDefinition = (T8WorkFlowUserTaskDefinition)getParentDefinition();
                taskParameterDefinitions = taskDefinition.getInputParameterDefinitions();

                identifierMap = new HashMap<String, List<String>>();
                if ((moduleParameterDefinitions != null) && (taskParameterDefinitions != null))
                {
                    for (T8DataParameterDefinition taskParameterDefinition : taskParameterDefinitions)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<String>();
                        for (T8DataParameterDefinition moduleParameterDefinition : moduleParameterDefinitions)
                        {
                            identifierList.add(moduleParameterDefinition.getPublicIdentifier());
                        }

                        identifierMap.put(taskParameterDefinition.getIdentifier(), identifierList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.MODULE_INPUT_PARAMETER_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FlowTaskModuleInputParameterScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.FINALIZATION_EVENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8WorkFlowUserTaskModuleEventDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getModuleIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_IDENTIFIER.toString());
    }

    public void setModuleIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MODULE_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getModuleInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.MODULE_INPUT_PARAMETER_MAPPING.toString());
    }

    public void setModuleInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.MODULE_INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public List<T8WorkFlowUserTaskModuleEventDefinition> getFinalizationEventDefinitions()
    {
        return (List<T8WorkFlowUserTaskModuleEventDefinition>)getDefinitionDatum(Datum.FINALIZATION_EVENT_DEFINITIONS.toString());
    }

    public void setFinalizationEventDefinitions(List<T8WorkFlowUserTaskModuleEventDefinition> definitions)
    {
        setDefinitionDatum(Datum.FINALIZATION_EVENT_DEFINITIONS.toString(), definitions);
    }

    public T8FlowTaskModuleInputParameterScriptDefinition getModuleInputParameterScriptDefinition()
    {
        return (T8FlowTaskModuleInputParameterScriptDefinition)getDefinitionDatum(Datum.MODULE_INPUT_PARAMETER_SCRIPT_DEFINITION.toString());
    }

    public void setModuleInputParameterScriptDefinition(T8FlowTaskModuleInputParameterScriptDefinition definition)
    {
        setDefinitionDatum(Datum.MODULE_INPUT_PARAMETER_SCRIPT_DEFINITION.toString(), definition);
    }
}
