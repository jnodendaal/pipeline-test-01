package com.pilog.t8.definition.ui;

import com.pilog.t8.definition.data.T8DataParameterDefinition;
import java.util.List;

/**
 * This interface can be implemented by definitions of components that provide isolated controllers for their content components.
 * All components operation within the context of a component controller and such a controller provides
 * specific functions to its content components.  Definitions of this type, supply the meta data for these functions.
 * @author Bouwer du Preez
 */
public interface T8ComponentControllerDefinition
{
    public abstract List<T8DataParameterDefinition> getInputParameterDefinitions();
}
