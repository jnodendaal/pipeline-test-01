package com.pilog.t8;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionComposition;
import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionLockDetails;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.event.T8DefinitionManagerListener;
import com.pilog.t8.definition.filter.T8DefinitionFilter;
import com.pilog.t8.definition.patch.T8DefinitionPatch;
import com.pilog.t8.project.T8ProjectImportReport;
import com.pilog.t8.system.T8SystemDetails;
import com.pilog.t8.definition.system.T8SetupDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionManager extends T8ServerModule
{
    /**
     * Creates the T8DataType specified by the supplied string representation.
     * @param stringRepresentation The string representation of the T8DataType
     * to create.
     * @return The T8DataType created from the supplied string representation.
     */
    public T8DataType createDataType(String stringRepresentation);

    /**
     * Determines the T8DataType applicable to the specified Java Object and
     * returns the data type.
     * @param object The object for which to create a T8DataType.
     * @return The T8DataType applicable to the specified Java object.
     */
    public T8DataType getObjectDataType(Object object);

    /**
     * Returns the list of definition patches loaded by the definition manager.
     * @return The list of definition patches loaded by the definition manager.
     */
    public List<T8DefinitionPatch> getDefinitionPatches();

    /**
     * Returns the setup definition.
     * @param context
     * @return The setup definition currently loaded.
     */
    public T8SetupDefinition getSetupDefinition(T8Context context);

    /**
     * Saves the specified setup definition, overwriting any existing setup.
     * @param context
     * @param setupDefinition The setup definition to save.
     * @throws Exception
     */
    public void saveSetupDefinition(T8Context context, T8SetupDefinition setupDefinition) throws Exception;

    /**
     * Returns the currently loaded system definition.
     * @param context
     * @return The currently loaded system definition.
     */
    public T8SystemDefinition getSystemDefinition(T8Context context);

    /**
     * Returns the details of the currently loaded system.
     * @return The details of the system.
     */
    public T8SystemDetails getSystemDetails();

    /**
     * Loads all definition data on the server.
     * @param context
     * @throws Exception
     */
    public void loadDefinitionData(T8Context context) throws Exception;

    /**
     * Renames the supplied definition to the new specified identifier.
     * @param context
     * @param definition
     * @param newDefinitionId
     * @return A list of handles to all definitions affected by the renaming.
     * @throws Exception
     */
    public List<T8DefinitionHandle> renameDefinition(T8Context context, T8Definition definition, String newDefinitionId) throws Exception;

    /**
     * Transfers all references to the old identifier to the new identifier.
     * @param context
     * @param oldDefinitionId
     * @param newDefinitionId
     * @throws Exception
     */
    public void transferDefinitionReferences(T8Context context, String oldDefinitionId, String newDefinitionId) throws Exception;

    /**
     * Copies the supplied definition and names the copy using the specified
     * identifier.
     * @param context
     * @param definition
     * @param newDefinitionId
     * @return
     * @throws Exception
     */
    public T8Definition copyDefinition(T8Context context, T8Definition definition, String newDefinitionId) throws Exception;

    /**
     * Loads the specified definition, copies it and names the copy using the
     * specified identifier.
     * @param context
     * @param projectId
     * @param definitionId
     * @param newDefinitionId
     * @return
     * @throws Exception
     */
    public T8Definition copyDefinition(T8Context context, String projectId, String definitionId, String newDefinitionId) throws Exception;

    /**
     * Copies the specified definition to the specified project, leaving the
     * original definition intact.
     * @param context
     * @param projectId The id of the project where the definition to be copied is located.
     * @param definitionId The identifier of the definition to copy.
     * @param newProjectId The identifier of the project to which the
     * definition will be copied.
     * @return
     * @throws Exception
     */
    public boolean copyDefinitionToProject(T8Context context, String projectId, String definitionId, String newProjectId) throws Exception;

    /**
     * Moves the specified definition to the specific project, removing the
     * original definition from the source project.
     * @param context
     * @param projectId The id of the project where the definition to be moved is located.
     * @param definitionId
     * @param newProjectId
     * @return
     * @throws Exception
     */
    public boolean moveDefinitionToProject(T8Context context, String projectId, String definitionId, String newProjectId) throws Exception;

    /**
     * Initializes the specified definition using the supplied input parameters.
     * After initialization the definition is ready for use in a live system.
     * @param context
     * @param definition
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public T8Definition initializeDefinition(T8Context context, T8Definition definition, Map<String, Object> inputParameters) throws Exception; // Initializes the supplied definition (as used by the client).

    /**
     * This method attempts to use one of the available definition resolvers to
     * find the most applicable definition of the specified type, given the
     * specified session context and input parameters.
     * @param context
     * @param definitionTypeIdentifier
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public T8Definition getResolvedDefinition(T8Context context, String definitionTypeIdentifier, Map<String, Object> inputParameters) throws Exception; // Returns an initialized definition (as used by the client).

    /**
     * Loads the specified definition and initializes it.  After initialization
     * the definition is ready for use in a live system.
     * @param <D>
     * @param context
     * @param projectId The id of the project to which the the requested definition belongs (only required if not public).
     * @param identifier
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public <D extends T8Definition> D getInitializedDefinition(T8Context context, String projectId, String identifier, Map<String, Object> inputParameters) throws Exception; // Returns an initialized definition (as used by the client).

    /**
     * Loads the specified definition from the meta-data cache and returns it.
     * This method does not initialize the definition and therefore the returned
     * definition is not ready for use in a live system but can be used for
     * administration and editing purposes.
     * @param <D>
     * @param context
     * @param projectId The id of the project to which the the requested definition belongs (only required if not public).
     * @param identifier
     * @return
     * @throws Exception
     */
    public <D extends T8Definition> D getRawDefinition(T8Context context, String projectId, String identifier) throws Exception; // Returns raw uninitialized definition (as used by developer).

    /**
     * Loads the specified definition from the meta-data cache and returns it.
     * This method does not initialize the definition and therefore the returned
     * definition is not ready for use in a live system but can be used for
     * administration and editing purposes.
     *
     * This method also locks the checked-out definition so that no other user
     * or process can make changes to the definition until it is unlocked again.
     *
     * @param context The session that will hold the lock of the
     * specified definition.
     * @param definitionHandle The handle of the definition to lock.
     * @param keyIdentifier The key identifier to use for the lock.  If none is
     * specified (null) the lock will be linked to the supplied session.
     * @return The latest state of the locked definition as loaded from the
     * definition manager.
     * @throws Exception
     */
    public T8Definition lockDefinition(T8Context context, T8DefinitionHandle definitionHandle, String keyIdentifier) throws Exception;

    /**
     * Unlocks the specified definition.  This method may only be called by the
     * user or process that currently holds the lock for the specified
     * definition.
     *
     * @param context The session that holds the lock of the specified
     * definition.
     * @param definitionHandle The handle identifying the definition to unlock.
     * @param keyIdentifier The key identifier of the lock (if one was specified
     * when the definition was locked).
     * @return The unlocked definition
     * @throws Exception
     */
    public T8Definition unlockDefinition(T8Context context, T8DefinitionHandle definitionHandle, String keyIdentifier) throws Exception;

    /**
     * Unlocks the specified definition.  This method may only be called by the
     * user or process that currently holds the lock for the specified
     * definition.  The supplied definition will be saved before it is unlocked.
     *
     * @param context The session that holds the lock of the specified
     * definition.
     * @param definition The definition to unlock.
     * @param keyIdentifier The key identifier of the lock (if one was specified
     * when the definition was locked).
     * @return The unlocked definition
     * @throws Exception
     */
    public T8Definition unlockDefinition(T8Context context, T8Definition definition, String keyIdentifier) throws Exception;

    /**
     * Returns the details of the lock on the specified definition.
     *
     * @param context The session context from which this operation is
     * performed.
     * @param definitionHandle The handle identifying th definition for which to
     * retrieve the lock details.
     * @return The lock details for the specified definition.
     * @throws Exception
     */
    public T8DefinitionLockDetails getDefinitionLockDetails(T8Context context, T8DefinitionHandle definitionHandle) throws Exception;

    /**
     * Saves the supplied definition to the meta-data cache.
     * @param context
     * @param definition
     * @param keyId
     * @return
     * @throws Exception
     */
    public T8DefinitionMetaData saveDefinition(T8Context context, T8Definition definition, String keyId) throws Exception;

    /**
     * Finalizes all in-progress updates on the definition and sets its status accordingly.
     * @param context
     * @param definitionHandle The handle of the definition to finalize.
     * @param comment The finalization comment that will be added to the definition if successfully finalized.
     * @return The meta data of the finalized definition.
     * @throws Exception
     */
    public T8DefinitionMetaData finalizeDefinition(T8Context context, T8DefinitionHandle definitionHandle, String comment) throws Exception;

    /**
     * Deletes the specified definition.  If the safeCheck parameter is supplied
     * as true, all meta-data will be check for references to the definition to
     * be deleted.  If any such references are found, the definition will not
     * be deleted and an exception will be thrown.
     * @param context
     * @param projectId
     * @param definitionId
     * @param safeCheck
     * @param keyId
     * @throws Exception
     */
    public void deleteDefinition(T8Context context, String projectId, String definitionId, boolean safeCheck, String keyId) throws Exception;

    /**
     * Returns the definition uniquely identified by the specified handle.
     * @param context
     * @param definitionHandle
     * @return
     * @throws Exception
     */
    public T8Definition getRawDefinition(T8Context context, T8DefinitionHandle definitionHandle) throws Exception;

    /**
     * Removes references to the specified definition from all other definitions in the system.
     * In order for this method to succeed the invoking user must have adequate permission to edit all affected definitions.
     * @param context The session context for the operation.
     * @param definitionId The identifier to remove from all other definitions.
     * @param keyId The key identifier to be used if any affected definitions are locked.
     * @throws Exception
     */
    public void removeDefinitionReferences(T8Context context, String definitionId, String keyId) throws Exception;

    /**
     * Deletes the definition uniquely identifier by the specified handle.
     * @param context Session context for the operation.
     * @param definitionHandle The handle identifying the definition to be
     * deleted.
     * @param safeDeletion Boolean flag indicating whether or not to check for
     * definition usage by other definitions before deletion.
     * @param keyIdentifier
     * @return Boolean value indicating success of the operation.
     * @throws Exception
     */
    public boolean deleteDefinition(T8Context context, T8DefinitionHandle definitionHandle, boolean safeDeletion, String keyIdentifier) throws Exception;

    /**
     * Returns a list of all definitions belonging to the specified group.  The
     * definitions are not initialized.
     * @param <T>
     * @param context
     * @param projectId The id of the project context for this operation.
     * @param definitionGroupId
     * @return
     * @throws Exception
     */
    public <T extends T8Definition> ArrayList<T> getRawGroupDefinitions(T8Context context, String projectId, String definitionGroupId) throws Exception;

    /**
     * Returns a list of all definitions belonging to the specified group.  All
     * definitions in the list are initialized.
     * @param <T> The definition type of the group definitions that will be returned
     * @param context
     * @param projectId The id of the project context for this operation.
     * @param definitionGroupId
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public <T extends T8Definition> List<T> getInitializedGroupDefinitions(T8Context context, String projectId, String definitionGroupId, Map<String, Object> inputParameters) throws Exception;

    /**
     * Returns the number of definitions types available on the server.
     * @param context
     * @return
     */
    public int getCachedDefinitionTypeCount(T8Context context);

    /**
     * Returns the number of definitions cached on the server.
     * @param context
     * @return
     */
    public int getCachedDefinitionCount(T8Context context);

    /**
     * Returns the total byte size of all definition data cached on the server.
     * @param context
     * @return
     */
    public long getCachedDataSize(T8Context context);

    /**
     * Creates a new definition of the specified type and names it using the
     * specified identifier.
     * @param definitionId
     * @param typeId
     * @return
     * @throws Exception
     */
    public T8Definition createNewDefinition(String definitionId, String typeId) throws Exception;

    /**
     * Creates a new definition of the specified type and names it using the
     * specified identifier.
     * @param definitionId
     * @param typeId
     * @param parameters
     * @return
     * @throws Exception
     */
    public T8Definition createNewDefinition(String definitionId, String typeId, List parameters) throws Exception;

    /**
     * Returns a list of all definition identifiers belonging to the specified type.
     * @param projectId The id of the context project to which this call is applicable.
     * @param typeId
     * @return
     * @throws Exception
     */
    public List<String> getDefinitionIdentifiers(String projectId, String typeId) throws Exception;

    /**
     * Returns the list of all definition identifiers belonging to the specified group and the specified project.
     * @param projectId The id of the context project to which this call is applicable.
     * @param groupId
     * @return
     * @throws Exception
     */
    public List<String> getGroupDefinitionIdentifiers(String projectId, String groupId) throws Exception;

    /**
     * Returns a list of all definition group identifiers.
     * @return
     * @throws Exception
     */
    public List<String> getDefinitionGroupIdentifiers() throws Exception;

    /**
     * Returns a list of all definition type identifiers.
     * @return
     * @throws Exception
     */
    public List<String> getDefinitionTypeIdentifiers() throws Exception;

    /**
     * Returns a list of all definition type identifiers belonging to the specified group.
     * @param groupId
     * @return
     * @throws Exception
     */
    public List<String> getDefinitionTypeIdentifiers(String groupId) throws Exception;

    /**
     * Returns the composition break-down of the specified definition.  The composition is
     * a hierarchical structure of definition meta data, showing all of the composite definitions
     * that make up or is used by the specified root definition, via direct or indirect reference.
     * @param context The context of the user requesting the composition.
     * @param projectId The project id where the definition is located.
     * @param definitionId The id of the definition for which the composition will be requested.
     * @param includeDescendants A boolean flag to indicate whether or not all descendant composition should be included.
     * @return The composition break-down of the specified definition.
     * @throws Exception
     */
    public T8DefinitionComposition getDefinitionComposition(T8Context context, String projectId, String definitionId, boolean includeDescendants) throws Exception;

    /**
     * Returns the definition meta data object for the specified definition.
     * @param projectId The id of the context project to which this call is applicable.
     * @param definitionId
     * @return
     * @throws Exception
     */
    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception;

    /**
     * Returns the definition meta data object for the specified definition.
     * @param definitionHandle
     * @return
     * @throws Exception
     */
    public T8DefinitionMetaData getDefinitionMetaData(T8DefinitionHandle definitionHandle) throws Exception;

    /**
     * Returns the definition meta data objects for the specified definitions.
     * @param definitionHandles
     * @return
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getDefinitionMetaData(List<T8DefinitionHandle> definitionHandles) throws Exception;

    /**
     * Returns a list of definition meta data objects for all definitions of the specified type.
     * @param projectId The id of the context project to which this call is applicable.
     * @param typeId
     * @return
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String projectId, String typeId) throws Exception;

    /**
     * Returns a list of definition meta data objects for all definitions belonging to the specified group.
     * @param projectId The id of the context project to which this call is applicable.
     * @param groupId
     * @return
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String projectId, String groupId) throws Exception;

    /**
     * Returns a list of definition meta data objects for all definitions belonging to the specified project.
     * @param projectId The id of the context project to which this call is applicable.
     * @return
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getProjectDefinitionMetaData(String projectId) throws Exception;

    /**
     * Returns a definition type meta data object for the specified definition type.
     * @param typeId
     * @return
     * @throws Exception
     */
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeId) throws Exception;

    /**
     * Returns a list of definition type meta data objects for all definition types in the system.
     * @return
     * @throws Exception
     */
    public List<T8DefinitionTypeMetaData> getAllDefinitionTypeMetaData() throws Exception;

    /**
     * Returns a list of definition group meta data objects for all definition groups in the system.
     * @return
     * @throws Exception
     */
    public List<T8DefinitionGroupMetaData> getAllDefinitionGroupMetaData() throws Exception;

    /**
     * Returns a list of definition type meta data objects for all definition types belonging to the specified group.
     * @param groupId
     * @return
     * @throws Exception
     */
    public List<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupId) throws Exception;

    /**
     * Adds a listener to the definition manager that will be notified of all
     * events that occur.
     * @param listener
     */
    public void addDefinitionManagerListener(T8DefinitionManagerListener listener);

    /**
     * Removes the specified listener from the definition manager.
     * @param listener
     */
    public void removeDefinitionManagerListener(T8DefinitionManagerListener listener);

    /**
     * Checks the availability of the specified identifier.
     * - A public identifier is available if it is unique and not used by any other public definition in the system.
     * - A private identifier is available if it is unique and not used by any other definition in the same project.
     * @param context
     * @param projectId The id of the project context for this operation.
     * @param definitionId
     * @return
     * @throws Exception
     */
    public boolean checkIdentifierAvailability(T8Context context, String projectId, String definitionId) throws Exception;

    /**
     * Finds the identifiers of all definitions containing the specified text.
     * @param context
     * @param searchString
     * @return
     * @throws Exception
     */
    public List<T8DefinitionHandle> findDefinitionsByText(T8Context context, String searchString) throws Exception;

    /**
     * Finds the identifiers of all definitions with content that matches the
     * specified regular expression.
     * @param context
     * @param expression
     * @return
     * @throws Exception
     */
    public List<T8DefinitionHandle> findDefinitionsByRegularExpression(T8Context context, String expression) throws Exception;

    /**
     * Finds the identifiers of all definitions that use or reference the
     * specified definition identifier.
     * @param context
     * @param identifier
     * @param includeReferencesOnly If true, only definitions referencing the specified identifier will be
     * included i.e. a definition will not be included if only its own identifier matches the specified identifier.
     * @return
     * @throws Exception
     */
    public List<T8DefinitionHandle> findDefinitionsByIdentifier(T8Context context, String identifier, boolean includeReferencesOnly) throws Exception;

    /**
     * Returns a list of definition meta data objects for all definitions in the
     * system that meet the specified filter criteria.
     * @param context
     * @param definitionFilter
     * @return
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getAllDefinitionMetaData(T8Context context, T8DefinitionFilter definitionFilter) throws Exception;

    /**
     * Returns the handles of all definitions from all projects and system
     * caches that meet the specified filter criteria.
     * @param context
     * @param definitionFilter
     * @return
     * @throws Exception
     */
    public List<T8DefinitionHandle> getAllDefinitionHandles(T8Context context, T8DefinitionFilter definitionFilter) throws Exception;

    /**
     * Imports the supplied definition into the system without affecting its
     * meta-data.
     * @param context
     * @param definition
     * @throws Exception
     */
    public void importDefinition(T8Context context, T8Definition definition) throws Exception;

    /**
     * Deletes the specified projects and all of the definitions they contains if the current session has the required access rights.
     * @param context
     * @param projectIds The ids of the projects to delete.
     * @throws Exception
     */
    public void deleteProjects(T8Context context, List<String> projectIds) throws Exception;

    /**
     * Imports the definition file located at the specified location in the specified file context.
     * @param context
     * @param fileContextIid The instance id of the file context where the file to import is located.
     * @param filePath The path within the file context where the file to import is located.
     * @param projectIds The id list specifying projects to import from the target file.
     * @return A report detailing the result of the requested import operation.
     * @throws Exception
     */
    public T8ProjectImportReport importProjects(T8Context context, String fileContextIid, String filePath, List<String> projectIds) throws Exception;

    /**
     * Exports the specified projects to the specified file context.  If no file context is specified
     * @param context
     * @param fileContextIid The instance id of the file context to which the export file will be written.
     * @param filePath The path within the file context to which exported projects will be written.
     * @param projectIds The id list specifying projects to export to the target file.
     * @throws Exception
     */
    public void exportProjects(T8Context context, String fileContextIid, String filePath, List<String> projectIds) throws Exception;
}
