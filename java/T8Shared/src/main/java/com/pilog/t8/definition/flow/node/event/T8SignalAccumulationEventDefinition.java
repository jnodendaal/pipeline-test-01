package com.pilog.t8.definition.flow.node.event;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This node catches a specific flow signals and accumulates data from the
 * signals until a condition is met.
 *
 * @author Bouwer du Preez
 */
public class T8SignalAccumulationEventDefinition extends T8FlowEventDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_NODE_EVENT_SIGNAL_ACCUMULATION";
    public static final String DISPLAY_NAME = "Flow Signal Accumulation Event";
    public static final String DESCRIPTION = "An event that catches and accumulates specific workflow signal data.";
    public static final String IDENTIFIER_PREFIX = "FWE_SIGA_";
    public enum Datum {SIGNAL_IDENTIFIER,
                       KEY_PARAMETER_ID,
                       CONDITION_EXPRESSION,
                       ACCUMULATION_SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8SignalAccumulationEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SIGNAL_IDENTIFIER.toString(), "Signal", "The identifier of the signal to catch."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.KEY_PARAMETER_ID.toString(), "Key Signal Parameter", "The key parameter (has to be a GUID) to use as the key that identifies the signal to catch."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated before the signal node is activated.  Use this to check signal parameters against flow parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.ACCUMULATION_SCRIPT_DEFINITION.toString(), "Signal Accumulation Script", "The script to execute when processing a signal."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SIGNAL_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FlowSignalDefinition.GROUP_IDENTIFIER));
        else if (Datum.KEY_PARAMETER_ID.toString().equals(datumIdentifier))
        {
            T8WorkFlowDefinition flowDefinition;

            flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
            if (flowDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions(flowDefinition.getFlowParameterDefinitions());
            }
            else return new ArrayList<>();
        }
        else if (Datum.ACCUMULATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8SignalAccumulationScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.CONDITION_EXPRESSION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.TextAreaDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else if (Datum.ACCUMULATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getKeyParameterId()))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.KEY_PARAMETER_ID.toString(), "No Signal Key Parameter set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.event.T8SignalAccumulationEventNode").getConstructor(T8Context.class, T8Flow.class, T8SignalAccumulationEventDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    public String getSignalIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SIGNAL_IDENTIFIER.toString());
    }

    public void setSignalIdentifier(String taskIdentifier)
    {
        setDefinitionDatum(Datum.SIGNAL_IDENTIFIER.toString(), taskIdentifier);
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public boolean evaluateConditionExpression(Map<String, Object> flowParameters, Map<String, Object> signalParameters) throws Exception
    {
        String conditionExpression;

        conditionExpression = getConditionExpression();
        if ((conditionExpression != null) && (conditionExpression.length() > 0))
        {
            Map<String, Object> expressionParameters;
            ExpressionEvaluator expressionEvaluator;

            expressionParameters = new HashMap<>();
            if (flowParameters != null) expressionParameters.putAll(flowParameters);
            if (signalParameters != null) expressionParameters.putAll(signalParameters);

            expressionEvaluator = new ExpressionEvaluator();
            return expressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null);
        }
        else return true;
    }

    public T8SignalAccumulationScriptDefinition getAccumulationScriptDefinition()
    {
        return (T8SignalAccumulationScriptDefinition)getDefinitionDatum(Datum.ACCUMULATION_SCRIPT_DEFINITION.toString());
    }

    public void setAccumulationScriptDefinition(T8SignalAccumulationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.ACCUMULATION_SCRIPT_DEFINITION.toString(), definition);
    }

    public String getKeyParameterId()
    {
        return (String)getDefinitionDatum(Datum.KEY_PARAMETER_ID.toString());
    }

    public void setKeyParameterId(String id)
    {
        setDefinitionDatum(Datum.KEY_PARAMETER_ID.toString(), id);
    }
}
