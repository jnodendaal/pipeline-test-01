package com.pilog.t8;

import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8FileManager extends T8ServerModule
{
    public T8FileHandler getFileHandler(T8Context context, String fileTypeId, String contextIid, String filePath);

    public File getFile(T8Context context, String contextIid, String filePath) throws Exception;
    public boolean fileExists(T8Context context, String contextIid, String filePath) throws Exception;
    public boolean fileContextExists(T8Context context, String contextIid) throws Exception;
    public boolean openFileContext(T8Context context, String contextIid, String contextId, T8TimeUnit expirationTime) throws Exception;
    public boolean closeFileContext(T8Context context, String contextIid) throws Exception;
    public boolean createDirectory(T8Context context, String contextIid, String filePath) throws Exception;
    public boolean deleteFile(T8Context context, String contextIid, String path) throws Exception;
    public void renameFile(T8Context context, String contextIid, String path, String newFilename) throws Exception;
    public long getFileSize(T8Context context, String contextIid, String path) throws Exception;
    public String getMd5Checksum(T8Context context, String contextIid, String path) throws Exception;
    public T8FileDetails getFileDetails(T8Context context, String contextIid, String path) throws Exception;
    public List<T8FileDetails> getFileList(T8Context context, String contextIid, String directoryPath) throws Exception;

    public InputStream getFileInputStream(T8Context context, String contextIid, String filePath) throws Exception;
    public OutputStream getFileOutputStream(T8Context context, String contextIid, String filePath, boolean append) throws Exception;
    public RandomAccessStream getFileRandomAccessStream(T8Context context, String contextIid, String filePath) throws Exception;

    public T8FileDetails uploadFile(T8Context context, String contextIid, String localFilePath, String remoteFilePath) throws Exception;
    public long getUploadOperationBytesUploaded(T8Context context, String localFilePath) throws Exception;
    public long getUploadOperationFileSize(T8Context context, String localFilePath) throws Exception;
    public int getUploadOperationProgress(T8Context context, String localFilePath) throws Exception;
    public long downloadFile(T8Context context, String contextIid, String localFilePath, String remoteFilePath) throws Exception;
    public long getDownloadOperationBytesDownloaded(T8Context context, String localFilePath) throws Exception;
    public long getDownloadOperationFileSize(T8Context context, String localFilePath) throws Exception;
    public int getDownloadOperationProgress(T8Context context, String localFilePath) throws Exception;

    public long uploadFileData(T8Context context, String contextIid, String filePath, byte[] fileData) throws Exception;
    public byte[] downloadFileData(T8Context context, String contextIid, String filePath, long fileOffset, int byteSize) throws Exception;
}
