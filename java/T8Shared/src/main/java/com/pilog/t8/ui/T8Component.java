package com.pilog.t8.ui;

import com.pilog.t8.definition.ui.T8ComponentDefinition;
import java.util.ArrayList;
import java.util.Map;

/**
 * This interface defines all of the methods that must be implemented by a
 * class in order to be used as a component in a T8 Module.
 *
 * When a component is constructed and used in a T8 Module, the following steps
 * are always followed:
 *  1. A new instance of the component is instantiated.  During instantiation
 *  the component can perform any internal action that does not require outside
 *  objects/classes.  The component may not perform any server-side
 *  communication during this step.
 *  2. The <code>initializeComponent</code> method is invoked on the component.
 *  The component is then responsible for setting itself up according to the
 *  definition and input parameters provided as arguments to the initialization
 *  method.  The component may not perform any server-side communication during
 *  this step.
 *  3. The instantiated component object is added to its parent component in the
 *  module layout.
 *  4. Once all components in a module have been properly laid out and
 *  initialized the module controller will invoke the
 *  <code>startComponent</code> method on all module components.  When this
 *  method is invoked, the component can assume that all layout and
 *  initialization operations in the module has been completed.  The component
 *  is now free to start its normal functionality.  A component should not
 *  report any event to the parent module controller before it has been started.
 *  5.  Once a component is no longer needed and right before it is discarded,
 *  the module controller will invoke the <code>stopComponent</code> method on
 *  the component.  When this method is called, the component can assume that it
 *  will never be used again and it must therefore release all allocated
 *  resources and stop all running threads.
 *
 * @author Bouwer du Preez
 */
public interface T8Component
{
    /**
     * Each T8Component will always have a reference to the controller to which
     * it belongs.
     * @return Returns the component controller to which this component belongs.
     */
    public T8ComponentController getController();

    /**
     * Returns the Input Parameters that were used by the component during
     * initialization.
     * @return Returns the input parameters used during initialization of this
     * component.
     */
    public Map<String, Object> getInputParameters();

    /**
     * This method executes one of the T8Component's operations using the
     * supplied operation parameters and returns the output parameters.
     * @param operationIdentifier The local identifier of the component
     * operation to execute.
     * @param operationParameters The input parameters to use for operation
     * executed (with local identifiers).
     * @return The output parameters of the operation (with local identifiers).
     */
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters);

    /**
     * Returns the Definition according to which the component is currently
     * initialized.
     * @return The component's definition.
     */
    public T8ComponentDefinition getComponentDefinition();

    /**
     * This method should initialize the component for use in the specified
     * context.  This method is called from a non-EDT thread an must perform all
     * heavy operations required by the component startup routine.
     * @param inputParameters The context input parameters available to the
     * component.
     */
    public void initializeComponent(Map<String, Object> inputParameters);

    /**
     * This method is invoked once the component has been completely
     * initialized and realized.  Once this method is invoked, a component can
     * assume that the controller to which it belongs has also been completely
     * initialized and that the component can now start its normal
     * functionality.  This method is always called from the EDT and must
     * therefore NOT perform any heavy initialization.  Such operations must
     * rather be performed in the initializeComponent() method.
     *
     * Components should not report events to their parent controller before
     * they have been started.
     */
    public void startComponent();

    /**
     * This method is invoked once a component's lifetime has ended and right
     * before it is totally discarded.  This method may be called after the
     * component has already been removed from the UI layout or after the
     * component has been hidden.  When this method is invoked, the component
     * must release all allocated resources and stop all running threads.
     */
    public void stopComponent();

    /**
     * Each T8Component may contains child components.  This is not a
     * requirement since not all T8Components are containers, so it is up to
     * the component to determine the implementation of these methods.
     * @param childComponent The child component to add.
     * @param constraints The constraints applicable to the addition of the new
     * child component.
     */
    public void addChildComponent(T8Component childComponent, Object constraints);
    public T8Component getChildComponent(String childComponentIdentifier);
    public T8Component removeChildComponent(String childComponentIdentifier);
    public ArrayList<T8Component> getChildComponents();
    public int getChildComponentCount();
    public void clearChildComponents();
}
