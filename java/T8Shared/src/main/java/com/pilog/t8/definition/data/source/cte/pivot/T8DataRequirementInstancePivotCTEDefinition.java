package com.pilog.t8.definition.data.source.cte.pivot;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.source.cte.T8CTEDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataRequirementInstancePivotCTEDefinition extends T8CTEDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMON_TABLE_EXPRESSION_PIVOT_DR_INSTANCE";
    public static final String DISPLAY_NAME = "Data Requirement Instance Pivot CTE";
    public static final String DESCRIPTION = "A Common Table Expression (With Clause) that can be used in other applicable data sources";
    public enum Datum
    {
        CTE_NAME,
        DATA_REQUIREMENT_INSTANCE_IDENTIFIER,
        DATA_REQUIREMENT_PROPERTIES
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRequirementInstancePivotCTEDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CTE_NAME.toString(), "CTE Name", "The name that will be used for this CTE when it is generated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString(), "Data Requirement Instance Identifier", "The data requirement instance that will be pivoted."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATA_REQUIREMENT_PROPERTIES.toString(), "Property Field Definitions", "The property fields that will be pivoted."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.DATA_REQUIREMENT_PROPERTIES.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PivotPropertyFieldDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.data.source.cte.T8DataRequirementInstancePivotCTEActionHandler").getConstructor(T8Context.class, T8DataRequirementInstancePivotCTEDefinition.class);
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

     @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.DATA_REQUIREMENT_PROPERTIES.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public String getSQLCTEString(T8DataFilter filter, int pageOffset, int pageSize)
    {
        StringBuilder CTEString;

        CTEString = new StringBuilder("-- ");

        CTEString.append(getIdentifier());
        CTEString.append("\n");


        CTEString.append("WITH ");
        CTEString.append(getCTEName());
        CTEString.append("(RECORD_ID,");

        for (Iterator<T8PivotPropertyFieldDefinition> it1 = getDataRequirementPropertyDefinitions().iterator(); it1.hasNext();)
        {
            T8PivotPropertyFieldDefinition t8PivotPropertyFieldDefinition = it1.next();
            CTEString.append(t8PivotPropertyFieldDefinition.getSourceIdentifier());
            if (it1.hasNext())
            {
                CTEString.append(",");
            }
        }
        CTEString.append(") AS ( SELECT DRP.RECORD_ID,");

        for (Iterator<T8PivotPropertyFieldDefinition> it1 = getDataRequirementPropertyDefinitions().iterator(); it1.hasNext();)
        {
            T8PivotPropertyFieldDefinition t8PivotPropertyFieldDefinition = it1.next();
            CTEString.append("MAX(CASE WHEN DRP.PROPERTY_ID = <<-");
            CTEString.append(t8PivotPropertyFieldDefinition.getPropertyID());
            CTEString.append("->> THEN DRP.VALUE_STRING ELSE NULL END) AS ");
            CTEString.append(t8PivotPropertyFieldDefinition.getSourceIdentifier());
            if (it1.hasNext())
            {
                CTEString.append(",");
            }
        }

        CTEString.append(" FROM DATA_RECORD_PROPERTY DRP ");
        CTEString.append(" INNER JOIN DATA_REQUIREMENT_INSTANCE DRI ");
        CTEString.append(" ON DRP.DR_ID = DRI.DR_ID ");
        CTEString.append(" WHERE ");
        CTEString.append(" DRI.DR_INSTANCE_ID = <<-");
        CTEString.append(getDataRequirementInstanceIdentifier());
        CTEString.append("->>");

        CTEString.append(" GROUP BY ");
        CTEString.append(" DRP.RECORD_ID) ");

        return CTEString.toString();
    }

    @Override
    public Map<String, String> getSQLParameterExpressionMap()
    {
        Map<String, String> parameterExpressionMap;

        parameterExpressionMap = new HashMap<String, String>();
        parameterExpressionMap.put(getDataRequirementInstanceIdentifier(), "dbAdaptor.getSQLColumnHexToGUID(\"'" + getDataRequirementInstanceIdentifier() + "'\")");
        for (T8PivotPropertyFieldDefinition t8PivotPropertyFieldDefinition : getDataRequirementPropertyDefinitions())
        {
            parameterExpressionMap.put(t8PivotPropertyFieldDefinition.getPropertyID(), "dbAdaptor.getSQLColumnHexToGUID(\"'" + t8PivotPropertyFieldDefinition.getPropertyID() + "'\")");
        }

        return parameterExpressionMap;
    }

    public String getCTEName()
    {
        return (String)getDefinitionDatum(Datum.CTE_NAME.toString());
    }

    public void setCTEName(String CTEName)
    {
        setDefinitionDatum(Datum.CTE_NAME.toString(), CTEName);
    }

    public String getDataRequirementInstanceIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString());
    }

    public void setDataRequirementInstanceIdentifier(String dataRequirementInstanceIdentifier)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString(), dataRequirementInstanceIdentifier);
    }

    public List<T8PivotPropertyFieldDefinition> getDataRequirementPropertyDefinitions()
    {
        return (List<T8PivotPropertyFieldDefinition>)getDefinitionDatum(Datum.DATA_REQUIREMENT_PROPERTIES.toString());
    }

    public void setDataRequirementPropertyDefinitions(List<T8PivotPropertyFieldDefinition> dataRequirementPropertyDefinitions)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_PROPERTIES.toString(), dataRequirementPropertyDefinitions);
    }
}
