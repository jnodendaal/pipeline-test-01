package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A definition of a point-to-point connection that a client can use for sending messages directly to a remote party represented by a URL.
 *
 * @author Bouwer du Preez
 */
public class T8SoapConnectionDefinition extends T8ConnectionDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REMOTE_SERVER_CONNECTION_SOAP";
    public static final String DISPLAY_NAME = "Soap Connection";
    public static final String DESCRIPTION = " definition of a point-to-point connection that a client can use for sending messages directly to a remote party represented by a URL.";
    public enum Datum {
        AUTH_USERNAME,
        AUTH_PASSWORD
    };
    // -------- Definition Meta-Data -------- //

    public T8SoapConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AUTH_USERNAME.toString(), "Authentication: Username", "The authentication username for the webservice connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AUTH_PASSWORD.toString(), "Authentication: Password", "The authentication password for the webservice connection."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Connection getNewConnectionInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.connection.T8SoapConnection", new Class<?>[]{T8Context.class, T8SoapConnectionDefinition.class}, context, this);
    }

    @Override
    public List<T8RemoteUserDefinition> getRemoteUserDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.remote.T8RemoteServerConnectionTestHarness", new Class<?>[]{});
    }

    public String getAuthenticationUsername()
    {
        return getDefinitionDatum(Datum.AUTH_USERNAME);
    }

    public void setAuthenticationUsername(String username)
    {
        setDefinitionDatum(Datum.AUTH_USERNAME, username);
    }

    public String getAuthenticationPassword()
    {
        return getDefinitionDatum(Datum.AUTH_PASSWORD);
    }

    public void setAuthenticationPassword(String password)
    {
        setDefinitionDatum(Datum.AUTH_PASSWORD, password);
    }
}
