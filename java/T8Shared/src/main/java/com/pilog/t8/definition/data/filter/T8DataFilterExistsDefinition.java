package com.pilog.t8.definition.data.filter;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilterExists;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataFilterExistsDefinition extends T8DataFilterClauseDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_FILTER_EXISTS";
    public static final String IDENTIFIER_PREFIX = "FILTER_EXISTS_";
    public static final String DISPLAY_NAME = "Data Filter Exists";
    public static final String DESCRIPTION = "An object that specifies an exists filter.";
    private enum Datum {OPERATOR,
                        EXISTS_MAPPING,
                        EXISTS_FILTER};
    // -------- Definition Meta-Data -------- //

    public enum ExistsOperator {EXISTS, NOT_EXISTS}

    private String dataEntityIdentifier;

    public T8DataFilterExistsDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();

        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATOR.toString(), "Operator", "The exists operator to use.", ExistsOperator.EXISTS.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.EXISTS_FILTER.toString(), "Exists Filter", "The filter that will be used for the exists clause."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.EXISTS_MAPPING.toString(), "Exists Mapping", "The fields on which the exists will be mapped.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.EXISTS_FILTER.toString())));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EXISTS_FILTER.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData((T8DataFilterDefinition.GROUP_IDENTIFIER)));
        if (Datum.OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(ExistsOperator.values());
        else if(Datum.EXISTS_MAPPING.toString().equals(datumIdentifier))
        {
            T8DataFilterDefinition parentFilterDefinition;
            T8DataFilterDefinition exitsFilterDefinition;

            parentFilterDefinition = getParentDataFilterDefinition(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
            exitsFilterDefinition = getExistsFilter();

            if(exitsFilterDefinition != null)
            {
                T8DataEntityDefinition parentEntityDefinition;
                T8DataEntityDefinition existsEntityDefinition;
                Map<String, List<String>> identifierMap;
                ArrayList<T8DefinitionDatumOption> optionList;

                identifierMap = new HashMap<>();
                optionList = new ArrayList<>();
                parentEntityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), parentFilterDefinition.getDataEntityIdentifier());
                existsEntityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), exitsFilterDefinition.getDataEntityIdentifier());

                for (String parentFieldIdentifier : parentEntityDefinition.getFieldIdentifiers(false))
                {
                    identifierMap.put(parentFieldIdentifier, existsEntityDefinition.getFieldIdentifiers(false));
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));

                return optionList;
            } else return new ArrayList<>();
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DataFilterDefinition parentDataFilterDefinition;

        parentDataFilterDefinition = getParentDataFilterDefinition(context.getServerContext().getDefinitionManager().getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        dataEntityIdentifier = parentDataFilterDefinition.getDataEntityIdentifier();
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(getExistsFilter() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.EXISTS_FILTER.toString(), "Exists filter is empty", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(getExistsFilterMapping() == null || getExistsFilterMapping().isEmpty()) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.EXISTS_MAPPING.toString(), "Exists Mapping Required", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    @Override
    public T8DataFilterExists getNewDataFilterClauseInstance(T8Context context, Map<String, Object> filterParameters)
    {
        T8DataFilterExists existsFilter;

        existsFilter = new T8DataFilterExists(this);
        existsFilter.setExistsFilter(getExistsFilter().getNewDataFilterInstance(context, filterParameters));

        return existsFilter;
    }

    private T8DataFilterDefinition getParentDataFilterDefinition(List<T8DefinitionTypeMetaData> definitionMetaDatas) throws Exception
    {
        for (T8DefinitionTypeMetaData t8DefinitionTypeMetaData : definitionMetaDatas)
        {
            T8Definition ancestorDefinition = getAncestorDefinition(t8DefinitionTypeMetaData.getTypeId());
            if(ancestorDefinition != null) return (T8DataFilterDefinition)ancestorDefinition;
        }
        return null;
    }

    public String getDataEntityIdentifier()
    {
        return dataEntityIdentifier;
    }

    public T8DataFilterDefinition getExistsFilter()
    {
        return (T8DataFilterDefinition) getDefinitionDatum(Datum.EXISTS_FILTER.toString());
    }

    public void setExistsFilter(T8DataFilterDefinition definition)
    {
        setDefinitionDatum(Datum.EXISTS_FILTER.toString(), definition);
    }

    public Map<String, String> getExistsFilterMapping()
    {
        return (Map<String, String>) getDefinitionDatum(Datum.EXISTS_MAPPING.toString());
    }

    public void setExistsFilterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.EXISTS_MAPPING.toString(), mapping);
    }

    public ExistsOperator getOperator()
    {
        return ExistsOperator.valueOf((String)getDefinitionDatum(Datum.OPERATOR.toString()));
    }

    public void setOperator(ExistsOperator operator)
    {
        setDefinitionDatum(Datum.OPERATOR.toString(), operator.toString());
    }
}
