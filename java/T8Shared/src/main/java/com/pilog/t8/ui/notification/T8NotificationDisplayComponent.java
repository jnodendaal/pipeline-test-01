package com.pilog.t8.ui.notification;

import com.pilog.t8.notification.T8Notification;

/**
 * @author Bouwer du Preez
 */
public interface T8NotificationDisplayComponent
{
    public T8Notification getNotification();
    public void initialize();
    public void startComponent();
    public void stopComponent();
}
