package com.pilog.t8.data.filter.expression;

/**
 * @author Hennie Brink
 */
public class T8FilterExpressionToken
{
    private final TokenType tokenType;
    private final Object data;

    public T8FilterExpressionToken(TokenType tokenType, Object data)
    {
        this.tokenType = tokenType;
        this.data = data;
    }

    public TokenType getTokenType()
    {
        return tokenType;
    }

    public Object getData()
    {
        return data;
    }

     @Override
    public String toString()
    {
        return String.format("(%s -> %s)", tokenType.getName(), data);
    }

    public enum TokenType
    {
        SYMBOL("Symbol"),
        OPERATOR("Operator"),
        KEYWORD("Keyword"),
        FIELD_IDENTIFIER("Identifier"),
        WORD("String"),
        PHRASE("Phrase"),
        INVALID("Invalid Identifier Value");

        private String name;

        private TokenType(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }
}
