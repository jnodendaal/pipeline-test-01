package com.pilog.t8.definition.system;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SetupDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SETUP";
    public static final String GROUP_IDENTIFIER = "@DG_SETUP";
    public static final String STORAGE_PATH = "/system";
    public static final String DISPLAY_NAME = "T8 Setup Definition";
    public static final String DESCRIPTION = "The first definition loaded by the T8 Framework during initialization.  This definition specified bootstrap settings to be used during startup of the framework.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    public enum Datum {SYSTEM_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8SetupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.SYSTEM_IDENTIFIER.toString(), "System",  "The system to load when the framework starts up."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SYSTEM_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8SystemDefinition.GROUP_IDENTIFIER));
        else return null;
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
  
    public String getStartupSystemIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SYSTEM_IDENTIFIER.toString());
    }
    
    public void setStartupSystemIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SYSTEM_IDENTIFIER.toString(), identifier);
    }
}

