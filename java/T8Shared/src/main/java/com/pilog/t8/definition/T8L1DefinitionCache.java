package com.pilog.t8.definition;

import com.pilog.t8.T8SessionContext;

/**
 * @author Bouwer du Preez
 */
public interface T8L1DefinitionCache
{
    public T8Definition getDefinition(T8SessionContext sessionContext, String identifier) throws Exception;    
}
