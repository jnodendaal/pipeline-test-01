package com.pilog.t8.data.filter;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SubDataFilter implements Serializable
{
    private T8DataFilter dataFilter;
    private Map<String, String> subFilterFieldMapping;
    
    public T8SubDataFilter(T8DataFilter dataFilter)
    {
        this.dataFilter = dataFilter;
    }

    public T8DataFilter getDataFilter()
    {
        return dataFilter;
    }

    public void setDataFilter(T8DataFilter dataFilter)
    {
        this.dataFilter = dataFilter;
    }

    /**
     * Returns the field mapping of Sub-Filter entity fields to Parent Filter
     * entity fields.
     * @return the field mapping of Sub-Filter entity fields to Parent Filter
     * entity fields.
     */
    public Map<String, String> getSubFilterFieldMapping()
    {
        return subFilterFieldMapping;
    }

    public void setSubFilterFieldMapping(Map<String, String> subFilterFieldMapping)
    {
        this.subFilterFieldMapping = subFilterFieldMapping;
    }
}
