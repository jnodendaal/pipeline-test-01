package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.state.T8StateUpdates;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateParameterList extends ArrayList<Object> implements T8FlowStateParameterCollection
{
    protected final T8FlowStateParameterCollection parentCollection;
    protected final String flowIID;
    protected final String parameterIID;
    protected final String entityIdentifier;
    protected final Map<Integer, String> parameterIdentifiers;
    protected final Set<Integer> insertedIndices;

    public T8FlowStateParameterList(T8FlowStateParameterCollection parentCollection, String flowIID, String parameterIID, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        this.parentCollection = parentCollection;
        this.flowIID = flowIID;
        this.parameterIID = parameterIID;
        this.entityIdentifier = entityIdentifier;
        this.parameterIdentifiers = new HashMap<>();
        this.insertedIndices = new HashSet<>();
        if (dataSet != null) addParameters(dataSet);
    }

    protected synchronized void addParameters(T8ParameterStateDataSet dataSet)
    {
        List<T8DataEntity> entityList;

        entityList = dataSet.getParameterEntitiesByParentIID(parameterIID);
        for (T8DataEntity parameterEntity : entityList)
        {
            String elementParameterIID;
            Integer parameterIndex;
            String parameterValue;
            String parameterType;

            elementParameterIID = (String)parameterEntity.getFieldValue(entityIdentifier + "$PARAMETER_IID");
            parameterType = (String)parameterEntity.getFieldValue(entityIdentifier + "$TYPE");
            parameterIndex = (Integer)parameterEntity.getFieldValue(entityIdentifier + "$SEQUENCE");
            parameterValue = (String)parameterEntity.getFieldValue(entityIdentifier + "$VALUE");

            add(createNewElementParameterValue(elementParameterIID, parameterType, parameterValue, entityIdentifier, dataSet));
            parameterIdentifiers.put(parameterIndex, elementParameterIID);
        }
    }

    public synchronized T8DataEntity createEntity(int index, T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;
        Object value;

        value = get(index);

        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityIdentifier + "$PARAMETER_IID", parameterIdentifiers.get(index));
        newEntity.setFieldValue(entityIdentifier + "$PARENT_PARAMETER_IID", parameterIID);
        newEntity.setFieldValue(entityIdentifier + "$TYPE", T8FlowStateParameterMap.getParameterType(value));
        newEntity.setFieldValue(entityIdentifier + "$SEQUENCE", index);
        newEntity.setFieldValue(entityIdentifier + "$PARAMETER_KEY", index);
        newEntity.setFieldValue(entityIdentifier + "$VALUE", T8FlowStateParameterMap.getParameterStringValue(value));
        newEntity.setFieldValue(entityIdentifier + "$FLOW_IID", flowIID);
        return newEntity;
    }

    @Override
    public synchronized T8FlowStateParameterMap createNewParameterMap(T8FlowStateParameterCollection newMapParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        if (parentCollection != null)
        {
            return parentCollection.createNewParameterMap(newMapParentCollection, parameterIID, dataSet);
        }
        else return new T8FlowStateParameterMap(newMapParentCollection, flowIID, parameterIID, entityIdentifier, dataSet);
    }

    @Override
    public synchronized T8FlowStateParameterList createNewParameterList(T8FlowStateParameterCollection newListParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        if (parentCollection != null)
        {
            return parentCollection.createNewParameterList(newListParentCollection, parameterIID, dataSet);
        }
        return new T8FlowStateParameterList(newListParentCollection, flowIID, parameterIID, entityIdentifier, dataSet);
    }

    public synchronized void statePersisted()
    {
        // Clear this state's flags.
        insertedIndices.clear();

        // Check for sub-collections.
        for (Object value : this)
        {
            if (value instanceof T8FlowStateParameterMap)
            {
                ((T8FlowStateParameterMap)value).statePersisted();
            }
            else if (value instanceof T8FlowStateParameterList)
            {
                ((T8FlowStateParameterList)value).statePersisted();
            }
        }
    }

    public synchronized void addUpdates(T8DataEntityDefinition entityDefinition, T8StateUpdates updates)
    {
        // Add the updates for each inserted index.
        for (Integer insertedIndex : insertedIndices)
        {
            updates.addInsert(createEntity(insertedIndex, entityDefinition));
        }

        // Check for sub-collections.
        for (Object value : this)
        {
            if (value instanceof T8FlowStateParameterMap)
            {
                ((T8FlowStateParameterMap)value).addUpdates(entityDefinition, updates);
            }
            else if (value instanceof T8FlowStateParameterList)
            {
                ((T8FlowStateParameterList)value).addUpdates(entityDefinition, updates);
            }
        }
    }

    @Override
    public String getParameterIid()
    {
        return parameterIID;
    }

    public int getTotalParameterCount()
    {
        int count;

        count = 0;
        count += this.size();

        // Check for sub-collections.
        for (Object value : this)
        {
            if (value instanceof T8FlowStateParameterMap)
            {
                count += ((T8FlowStateParameterMap)value).getTotalParameterCount();
            }
            else if (value instanceof T8FlowStateParameterList)
            {
                count += ((T8FlowStateParameterList)value).getTotalParameterCount();
            }
        }

        // Return the total count.
        return count;
    }

    @Override
    public synchronized Object remove(int i)
    {
        throw new RuntimeException("Cannot remove parameter.");
    }

    @Override
    public synchronized boolean add(Object value)
    {
        String parameterIdentifier;
        int index;

        index = size();
        parameterIdentifier = T8IdentifierUtilities.createNewGUID();
        parameterIdentifiers.put(index, parameterIdentifier);
        insertedIndices.add(index);

        if (value instanceof Map)
        {
            T8FlowStateParameterMap newMap;

            newMap = createNewParameterMap(this, parameterIdentifier, null);
            newMap.putAll((Map)value);
            return super.add(newMap);
        }
        else if (value instanceof List)
        {
            T8FlowStateParameterList newList;

            newList = createNewParameterList(this, parameterIdentifier, null);
            for (Object valueElement : (List)value)
            {
                newList.add(valueElement);
            }

            return super.add(newList);
        }
        else
        {
            return super.add(value);
        }
    }

    public synchronized Object createNewElementParameterValue(String parameterIID, String parameterType, String value, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        switch (parameterType)
        {
            case "MAP":
            {
                return createNewParameterMap(this, parameterIID, dataSet);
            }
            case "LIST":
            {
                return createNewParameterList(this, parameterIID, dataSet);
            }
            case "INTEGER":
            {
                return Integer.parseInt(value);
            }
            case "LONG":
            {
                return Long.parseLong(value);
            }
            case "DOUBLE":
            {
                return Double.parseDouble(value);
            }
            case "FLOAT":
            {
                return Float.parseFloat(value);
            }
            case "BOOLEAN":
            {
                return Boolean.parseBoolean(value);
            }
            case "TIMESTAMP":
            {
                return T8Timestamp.fromTime(Long.parseLong(value));
            }
            default:
            {
                return value; // Just return the String value.
            }
        }
    }
}
