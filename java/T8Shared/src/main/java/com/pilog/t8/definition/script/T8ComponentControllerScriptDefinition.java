package com.pilog.t8.definition.script;

import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentControllerScriptDefinition extends T8CustomClientContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_COMPONENT_CONTROLLER";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_COMPONENT_CONTROLLER";
    public static final String IDENTIFIER_PREFIX = "SCRIPT_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Component Controller Script";
    public static final String DESCRIPTION = "A script that is executed within the context of a UI component controller.";
    // -------- Definition Meta-Data -------- //

    public T8ComponentControllerScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8ComponentControllerScriptCompletionHandler(context, this);
    }

    public T8ClientContextScript getNewScriptInstance(T8ComponentController controller)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.script.T8DefaultClientContextScript", new Class<?>[]{T8ComponentController.class, T8ClientContextScriptDefinition.class}, controller, this);
    }
}
