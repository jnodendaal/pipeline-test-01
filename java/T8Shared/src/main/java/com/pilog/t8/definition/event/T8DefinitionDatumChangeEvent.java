package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDatumChangeEvent extends EventObject
{
    private final T8DefinitionDatumType datumType;
    private final Object oldValue;
    private final Object newValue;
    private final Object actor;
    
    public T8DefinitionDatumChangeEvent(T8Definition definition, T8DefinitionDatumType datumType, Object oldValue, Object newValue, Object actor)
    {
        super(definition);
        this.datumType = datumType;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.actor = actor;
    }
    
    public T8Definition getDefinition()
    {
        return (T8Definition)getSource();
    }
    
    public T8DefinitionDatumType getDatumType()
    {
        return datumType;
    }

    public Object getNewValue()
    {
        return newValue;
    }

    public Object getOldValue()
    {
        return oldValue;
    }
    
    public Object getActor()
    {
        return actor;
    }
}
