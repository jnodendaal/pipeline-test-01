package com.pilog.t8.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8OperationCompilationResult implements Serializable
{
    private final List<T8CompilationError> errors;
    private final List<String> options;
    private String message;
    public boolean success;

    public T8OperationCompilationResult(boolean success)
    {
        this.success = success;
        this.errors = new ArrayList<>();
        this.options = new ArrayList<>();
    }

    public T8OperationCompilationResult(boolean success, String message)
    {
        this(success);
        this.message = message;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getMessage()
    {
        return message;
    }

    public void addError(String sourceObject, long lineNumber, long columnNumber, String message)
    {
        errors.add(new T8CompilationError(sourceObject, lineNumber, columnNumber, message));
    }

    public List<T8CompilationError> getErrors()
    {
        return new ArrayList<>(errors);
    }

    public void setOptions(List<String> options)
    {
        this.options.clear();
        if (options != null)
        {
            this.options.addAll(options);
        }
    }

    public List<String> getOptions()
    {
        return new ArrayList<>(options);
    }

    @Override
    public String toString()
    {
        StringBuilder string;

        string = new StringBuilder();
        for (T8CompilationError error : errors)
        {
            string.append(error);
            string.append("\n");
        }

        return string.toString();
    }

    public static class T8CompilationError implements Serializable
    {
        private final String sourceObject;
        private final long columnNumber;
        private final long lineNumber;
        private final String message;

        public T8CompilationError(String sourceObject, long lineNumber, long columnNumber, String message)
        {
            this.sourceObject = sourceObject;
            this.columnNumber = columnNumber;
            this.lineNumber = lineNumber;
            this.message = message;
        }

        public String getSourceObject()
        {
            return sourceObject;
        }

        public long getColumnNumber()
        {
            return columnNumber;
        }

        public long getLineNumber()
        {
            return lineNumber;
        }

        public String getMessage()
        {
            return message;
        }

        @Override
        public String toString()
        {
            return sourceObject + ":[" + lineNumber + "," + columnNumber + "] " + message;
        }
    }
}
