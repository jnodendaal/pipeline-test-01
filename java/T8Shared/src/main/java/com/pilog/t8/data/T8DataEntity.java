package com.pilog.t8.data;

import com.pilog.epic.annotation.EPICClass;
import com.pilog.epic.annotation.EPICMethod;
import com.pilog.epic.annotation.EPICParameter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
@EPICClass(PreferredVariableName = "entity", VariableCreationTemplate = "entity", Description = "T8DataEntity")
public class T8DataEntity implements Serializable
{
    private T8DataEntityDefinition definition;
    private Map<String, Object> dataValues;
    private String identifier;
    private String guid;

    public T8DataEntity(T8DataEntityDefinition definition)
    {
        if (definition != null)
        {
            this.dataValues = Collections.synchronizedMap(new HashMap<String, Object>());
            this.definition = definition;
            this.identifier = definition.getIdentifier();
            this.guid = T8IdentifierUtilities.createNewGUID();
        }
        else throw new RuntimeException("Null Entity Definition.");
    }

    public T8DataEntityDefinition getDefinition()
    {
        return definition;
    }

    @EPICMethod(Description = "Sets the unique GUID value of this entity to uniquely identify it.", Hidden = true)
    public void setGUID(String guid)
    {
        this.guid = guid;
    }

    public String getGUID()
    {
        return guid;
    }

    @EPICMethod(Description = "Gets the Entity Identifier associated with this entity.")
    public String getIdentifier()
    {
        return identifier;
    }

    /**
     * Returns true if the specified field is contained by this entity.  This
     * method does not check whether the specified field contains a value, only
     * whether the field is defined for the entity.
     *
     * @param fieldId The identifier of the field to check.
     * @return true if the specified field is defined for this entity.
     */
    @EPICMethod(Description = "Checks whether or not this entity contains the specified field in its definition.")
    public boolean containsField(String fieldId)
    {
        return definition.containsField(fieldId);
    }

    @EPICMethod(Description = "Checks whether or not this entity contains the specified field value.")
    public boolean containsFieldValue(String fieldId)
    {
        return dataValues.containsKey(fieldId);
    }

    @EPICMethod(Description = "Gets all the field Identfiers available on this entity.")
    public List<String> getFieldIdentifiers()
    {
        return definition.getFieldIdentifiers(false);
    }

    @EPICMethod(Description = "Gets all the field Identifiers available on this entity that are set as key fields.")
    public List<String> getKeyFieldIdentifiers()
    {
        return definition.getKeyFieldIdentifiers(false);
    }

    @EPICMethod(Description = "Gets all the field Identifiers available on this entity that are set as required fields.")
    public List<String> getRequiredFieldIdentifiers()
    {
        return definition.getRequiredFieldIdentifiers(false);
    }

    public T8DataEntity createNewDataEntity()
    {
        return new T8DataEntity(definition);
    }

    @EPICMethod(Description = "Gets a map containing the identifier-value pairs of all the key fields available on this entity.")
    public synchronized Map<String, Object> getKeyFieldValues()
    {
        HashMap<String, Object> keyFieldValues;

        keyFieldValues = new HashMap<>();
        for (String keyField : getKeyFieldIdentifiers())
        {
            keyFieldValues.put(keyField, dataValues.get(keyField));
        }

        return keyFieldValues;
    }

    @EPICMethod(Description = "Gets all the field value for the specified field identifier.")
    public synchronized Object getFieldValue(@EPICParameter(VariableName = "fieldIdentifier",Description = "The field identfier in the format of @entityIdentifier$localField") String fieldIdentifier)
    {
        if (fieldIdentifier == null)
        {
            return null;
        }
        else if (fieldIdentifier.startsWith(identifier))
        {
            return dataValues.get(fieldIdentifier);
        }
        else // Allow access to fields using local identifier.
        {
            return dataValues.get(identifier + fieldIdentifier);
        }
    }

    @EPICMethod(Description = "Gets a map containing all of the field values.")
    public synchronized Map<String, Object> getFieldValues()
    {
        return new HashMap<String, Object>(dataValues);
    }

    @EPICMethod(Description = "Gets a map containing all of the field values where the field is not a key field.")
    public synchronized Map<String, Object> getNonKeyFieldValues()
    {
        HashMap<String, Object> fieldValues;

        fieldValues = new HashMap<>();
        for (String fieldIdentifier : getNonKeyFieldIdentifiers())
        {
            if (dataValues.containsKey(fieldIdentifier))
            {
                fieldValues.put(fieldIdentifier, dataValues.get(fieldIdentifier));
            }
        }

        return fieldValues;
    }

    @EPICMethod(Description = "Sets the specified field value")
    public synchronized Object setFieldValue(@EPICParameter(VariableName = "fieldIdentifier", Description = "The field identfier in the format of @entityIdentifier$localField") String fieldIdentifier, @EPICParameter(VariableName = "fieldValue") Object fieldValue)
    {
        if (getFieldIdentifiers().contains(fieldIdentifier))
        {
            // Set the new field value.
            return dataValues.put(fieldIdentifier, fieldValue);
        }
        else throw new RuntimeException("Invalid Field: " + fieldIdentifier + " - Valid Fields: " + getFieldIdentifiers());
    }

    @EPICMethod(Description = "Sets the field values for all fields supplied in the map, the map must contain fieldIdentifier-Value pairs.")
    public synchronized void setFieldValues(Map<String, Object> fieldValues)
    {
        for (String fieldIdentifier : fieldValues.keySet())
        {
            setFieldValue(fieldIdentifier, fieldValues.get(fieldIdentifier));
        }
    }

    @EPICMethod(Description = "Sets the field values for all fields supplied in the map where the field identifiers in the map match the field Identifiers of this entity, the map must contain fieldIdentiefier-Value pairs.")
    public synchronized void setMatchingFieldValues(Map<String, Object> fieldValues)
    {
        List<String> fieldIdentifiers;

        fieldIdentifiers = getFieldIdentifiers();
        for (String fieldIdentifier : fieldValues.keySet())
        {
            if (fieldIdentifiers.contains(fieldIdentifier))
            {
                // Set the new field value.
                dataValues.put(fieldIdentifier, fieldValues.get(fieldIdentifier));
            }
        }
    }

    @EPICMethod(Description = "Removes a value assocatiated with the specified field.")
    public synchronized Object clearField(@EPICParameter(VariableName = "fieldIdentifier",Description = "The field identfier in the format of @entityIdentifier$localField") String fieldIdentifier)
    {
        return dataValues.remove(fieldIdentifier);
    }

    @EPICMethod(Description = "Clears all of the field values from this entity.")
    public synchronized void clearAllFields()
    {
        dataValues.clear();
    }

    @EPICMethod(Description = "Returns a List containing all the field identifiers that does not have a null value.")
    public synchronized List<String> getNonEmptyFieldIdentifiers()
    {
        return new ArrayList<>(dataValues.keySet());
    }

    public List<String> getNonKeyFieldIdentifiers()
    {
        List<String> fieldIdentifiers;

        fieldIdentifiers = getFieldIdentifiers();
        fieldIdentifiers.removeAll(getKeyFieldIdentifiers());
        return fieldIdentifiers;
    }

    public synchronized T8DataEntity copy()
    {
        T8DataEntity newEntity;

        newEntity = new T8DataEntity(definition);
        newEntity.setFieldValues(dataValues);
        return newEntity;
    }

    public boolean canOrderByField(String fieldIdentifier)
    {
        return definition.canOrderByField(fieldIdentifier);
    }

    @Override
    public String toString()
    {
        StringBuffer string;

        string = new StringBuffer(getIdentifier());
        string.append(getKeyFieldValues());
        return string.toString();
    }

    public String toLongString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder('[' + getIdentifier());
        toStringBuilder.append(getFieldValues());
        toStringBuilder.append(']');

        return toStringBuilder.toString();
    }
}
