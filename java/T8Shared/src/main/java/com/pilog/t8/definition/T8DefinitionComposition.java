package com.pilog.t8.definition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionComposition implements Serializable
{
    private T8DefinitionTypeMetaData definitionTypeMetaData;
    private T8DefinitionMetaData definitionMetaData;
    private T8DefinitionComposition referrerComposition;
    private final List<T8DefinitionComposition> references;

    public T8DefinitionComposition(T8DefinitionTypeMetaData typeMetaData, T8DefinitionMetaData definitionMetaData)
    {
        this.definitionTypeMetaData = typeMetaData;
        this.definitionMetaData = definitionMetaData;
        this.references = new ArrayList<>();
        this.referrerComposition = null;
    }

    void setReferrer(T8DefinitionComposition parent)
    {
        this.referrerComposition = parent;
    }

    public T8DefinitionComposition getReferrer()
    {
        return referrerComposition;
    }

    public String getProjectId()
    {
        return definitionMetaData.getProjectId();
    }

    public String getDefinitionId()
    {
        return definitionMetaData.getId();
    }

    public T8DefinitionTypeMetaData getDefinitionTypeMetaData()
    {
        return definitionTypeMetaData;
    }

    public void setDefinitionTypeMetaData(T8DefinitionTypeMetaData definitionTypeMetaData)
    {
        this.definitionTypeMetaData = definitionTypeMetaData;
    }

    public T8DefinitionMetaData getDefinitionMetaData()
    {
        return definitionMetaData;
    }

    public void setDefinitionMetaData(T8DefinitionMetaData definitionMetaData)
    {
        this.definitionMetaData = definitionMetaData;
    }

    public T8DefinitionComposition addReference(T8DefinitionTypeMetaData referenceTypeMetaData, T8DefinitionMetaData referenceMetaData)
    {
        T8DefinitionComposition referenceComposition;

        referenceComposition = new T8DefinitionComposition(referenceTypeMetaData, referenceMetaData);
        addReference(referenceComposition);
        return referenceComposition;
    }

    public void addReference(T8DefinitionComposition referenceComposition)
    {
        referenceComposition.setReferrer(this);
        references.add(referenceComposition);
    }

    public void replaceReference(T8DefinitionComposition oldReference, T8DefinitionComposition newReference)
    {
        int index;

        index = references.indexOf(oldReference);
        if (index > -1)
        {
            references.remove(oldReference);
            oldReference.setReferrer(null);
            references.add(index, newReference);
            newReference.setReferrer(this);
        }
    }

    public void addReferences(Collection<T8DefinitionComposition> referenceCompositions)
    {
        for (T8DefinitionComposition referenceComposition : referenceCompositions)
        {
            addReference(referenceComposition);
        }
    }

    public List<String> getLineIds()
    {
        List<String> lineIds;
        T8DefinitionComposition nextComposition;

        lineIds = new ArrayList<>();
        nextComposition = this;
        lineIds.add(definitionMetaData.getId());
        while (nextComposition.getReferrer() != null)
        {
            nextComposition = nextComposition.getReferrer();
            lineIds.add(nextComposition.getDefinitionId());
        }

        // Reverse the order, so that the id's start from the root.
        Collections.reverse(lineIds);
        return lineIds;
    }

    public void clearReferences()
    {
        for (T8DefinitionComposition referenceComposition : references)
        {
            referenceComposition.setReferrer(null);
        }

        references.clear();
    }

    public int getReferenceCount()
    {
        return references.size();
    }

    public List<T8DefinitionComposition> getReferences()
    {
        return new ArrayList<T8DefinitionComposition>(references);
    }
}
