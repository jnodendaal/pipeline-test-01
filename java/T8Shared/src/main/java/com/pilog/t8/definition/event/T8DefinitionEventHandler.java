package com.pilog.t8.definition.event;

/**
 * @author Bouwer
 */
public interface T8DefinitionEventHandler extends T8DefinitionManagerListener
{
    /**
     * This method is called on the event handler in order to initialize its
     * state during system startup.
     * @throws Exception 
     */
    public void initHandler() throws Exception;
}
