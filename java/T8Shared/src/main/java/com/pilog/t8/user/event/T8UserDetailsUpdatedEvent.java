package com.pilog.t8.user.event;

import com.pilog.t8.security.T8UserDetails;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8UserDetailsUpdatedEvent extends EventObject
{
    private final T8UserDetails userDetails;

    public T8UserDetailsUpdatedEvent(T8UserDetails userDetails)
    {
        super(userDetails);
        this.userDetails = userDetails;
    }

    public T8UserDetails getUserDetails()
    {
        return userDetails;
    }
}