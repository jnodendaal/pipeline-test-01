package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.object.T8DataObjectList;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataObjectList extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_OBJECT_LIST";

    public T8DtDataObjectList() {}

    public T8DtDataObjectList(T8DefinitionManager context) {}

    public T8DtDataObjectList(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataObjectList.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtDataObject dtObject;
            JsonObject jsonList;
            JsonArray jsonObjectArray;
            T8DataObjectList objectList;

            // Create the concept JSON object.
            objectList = (T8DataObjectList)object;
            jsonList = new JsonObject();
            jsonObjectArray = new JsonArray();
            jsonList.add("objects", jsonObjectArray);

            // Add the objects to the array.
            dtObject = new T8DtDataObject(null);
            for (int objectIndex = 0; objectIndex < objectList.getSize(); objectIndex++)
            {
                T8DataObject dataObject;

                dataObject = objectList.getObject(objectIndex);
                jsonObjectArray.add(dtObject.serialize(dataObject));
            }

            // Return the final task list object.
            return jsonList;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DtDataObject dtDataObject;
            JsonObject jsonList;
            JsonArray jsonObjectArray;
            T8DataObjectList objectList;

            // Get the JSON values.
            jsonList = jsonValue.asObject();
            jsonObjectArray = jsonList.getJsonArray("objects");

            // Create the object list object and add all of the objects to it.
            objectList = new T8DataObjectList();
            dtDataObject = new T8DtDataObject(null);
            for (JsonValue jsonObject : jsonObjectArray.values())
            {
                T8DataObject dataObject;

                dataObject = dtDataObject.deserialize(jsonObject);
                objectList.addObject(dataObject);
            }

            // Return the completed object.
            return objectList;
        }
        else return null;
    }
}