package com.pilog.t8.definition.data.object;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectHandler;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataObjectDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT";
    public static final String GROUP_NAME = "Data Objects";
    public static final String GROUP_DESCRIPTION = "Definitions of the various objects that are maintained by the system as content data.  Objects are abstractions of underlying data entities and server as handles to the data while being process in the system.";
    public static final String STORAGE_PATH = "/data_objects";
    public static final String IDENTIFIER_PREFIX = "O_";
    public enum Datum
    {
        KEY_FIELD_ID,
        FIELDS
    };
    // -------- Definition Meta-Data -------- //

    public T8DataObjectDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.KEY_FIELD_ID.toString(), "Key Field", "The field that is used as the key for this data object."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELDS.toString(), "Field Definitions", "The definitions of the fields of this data object."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.KEY_FIELD_ID.toString().equals(datumId)) return createLocalIdentifierOptionsFromDefinitions((List)getFieldDefinitions());
        else if (Datum.FIELDS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataObjectFieldDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.FIELDS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                T8Log.log("Exceptino while constructing datum editor.", e);
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DataObjectHandler getDataObjectHandler(T8DataTransaction tx);

    public String getKeyFieldId()
    {
        return (String)getDefinitionDatum(Datum.KEY_FIELD_ID.toString());
    }

    public void setKeyFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.KEY_FIELD_ID.toString(), fieldId);
    }

    public T8DataObjectFieldDefinition getFieldDefinition(String fieldId)
    {
        for (T8DataObjectFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if (fieldDefinition.getIdentifier().equals(fieldId)) return fieldDefinition;
            else if (fieldDefinition.getPublicIdentifier().equals(fieldId)) return fieldDefinition;
        }

        return null;
    }

    public List<T8DataObjectFieldDefinition> getFieldDefinitions()
    {
        return (List<T8DataObjectFieldDefinition>)getDefinitionDatum(Datum.FIELDS.toString());
    }

    public void setFieldDefinitions(List<T8DataObjectFieldDefinition> fieldDefinitions)
    {
        setDefinitionDatum(Datum.FIELDS.toString(), fieldDefinitions);
    }

    public void addFieldDefinition(T8DataObjectFieldDefinition fieldDefinition)
    {
        String fieldIdentifier;

        fieldIdentifier = fieldDefinition.getIdentifier();
        if (!containsField(fieldIdentifier))
        {
            addSubDefinition(Datum.FIELDS.toString(), fieldDefinition);
        }
        else throw new RuntimeException("Attempt to add duplicate field: " + fieldIdentifier);
    }

    public T8DataObjectFieldDefinition removeFieldDefinition(String fieldIdentifier)
    {
        T8DataObjectFieldDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldIdentifier);
        if (fieldDefinition != null)
        {
            getFieldDefinitions().remove(fieldDefinition);
            return fieldDefinition;
        }
        else throw new RuntimeException("Field not found: " + fieldIdentifier);
    }

    public boolean containsField(String fieldId)
    {
        for (T8DataValueDefinition fieldDefinition : getFieldDefinitions())
        {
            if (fieldDefinition.getPublicIdentifier().equals(fieldId)) return true;
            else if (fieldDefinition.getIdentifier().equals(fieldId)) return true;
        }

        return false;
    }

    public T8DataType getFieldDataType(String fieldId)
    {
        T8DataValueDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldId);
        if (fieldDefinition == null) throw new RuntimeException("Field not found: " + fieldId);
        else return fieldDefinition.getDataType();
    }

    public List<String> getFieldIds(boolean local)
    {
        ArrayList<String> fieldId;

        fieldId = new ArrayList<String>();
        for (T8DataValueDefinition fieldDefinition : getFieldDefinitions())
        {
            fieldId.add(local ? fieldDefinition.getIdentifier() : fieldDefinition.getPublicIdentifier());
        }

        return fieldId;
    }
}
