package com.pilog.t8.data;

/**
 * @author Bouwer du Preez
 */
public interface T8DataModel
{
    public int deleteCascade(T8DataTransaction dataAccessProvider, T8DataEntity entity) throws Exception;
}
