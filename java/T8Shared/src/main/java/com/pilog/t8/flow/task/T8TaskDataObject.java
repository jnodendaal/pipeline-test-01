package com.pilog.t8.flow.task;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectResource;
import com.pilog.t8.time.T8Timestamp;

/**
 * @author Bouwer du Preez
 */
public class T8TaskDataObject extends T8DataObject
{
    public T8TaskDataObject(T8DataObjectDefinition definition, String iid)
    {
        super(definition, iid);
    }

    public String getTaskIid()
    {
        return (String)getFieldValue(T8TaskObjectResource.F_TASK_IID);
    }

    public String getTaskId()
    {
        return (String)getFieldValue(T8TaskObjectResource.F_TASK_ID);
    }

    public String getFlowIid()
    {
        return (String)getFieldValue(T8TaskObjectResource.F_FLOW_IID);
    }

    public String getFlowId()
    {
        return (String)getFieldValue(T8TaskObjectResource.F_FLOW_ID);
    }

    public String getTaskName()
    {
        return (String)getFieldValue(T8TaskObjectResource.F_TASK_NAME);
    }

    public String getTaskDescription()
    {
        return (String)getFieldValue(T8TaskObjectResource.F_TASK_DESCRIPTION);
    }

    public boolean isClaimed()
    {
        return getFieldValue(T8TaskObjectResource.F_TIME_CLAIMED) != null;
    }

    public T8Timestamp getTimeIssued()
    {
        return (T8Timestamp)getFieldValue(T8TaskObjectResource.F_TIME_ISSUED);
    }

    public T8Timestamp getTimeClaimed()
    {
        return (T8Timestamp)getFieldValue(T8TaskObjectResource.F_TIME_CLAIMED);
    }

    public Integer getPriority()
    {
        return (Integer)getFieldValue(T8TaskObjectResource.F_PRIORITY);
    }
}
