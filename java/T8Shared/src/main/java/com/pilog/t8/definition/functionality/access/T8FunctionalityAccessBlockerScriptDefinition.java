package com.pilog.t8.definition.functionality.access;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityAccessBlockerScriptDefinition extends T8Definition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FUNCTIONALITY_ACCESS_BLOCKER";
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FUNCTIONALITY_ACCESS_BLOCKER";
    public static final String DISPLAY_NAME = "Functionality Access Blocker Script";
    public static final String DESCRIPTION = "A script that is executed within the context of a functionality access blocker.";
    public enum Datum {EPIC_SCRIPT};
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public static final String PARAMETER_FUNCTIONALITY_IDENTIFIER = "$P_FUNCTIONALITY_IDENTIFIER";
    public static final String PARAMETER_DATA_OBJECTS = "$P_DATA_OBJECTS";
    public static final String PARAMETER_FUNCTIONALITY_BLOCK_LIST = "$P_FUNCTIONALITY_BLOCK_LIST";

    public T8FunctionalityAccessBlockerScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.EPIC_SCRIPT.toString(), "EPIC Script",  "The SQL script the will be executed to create functionality access blocks."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.EPIC_SCRIPT.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ScriptDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.access.T8FunctionalityAccessBlockerScript").getConstructor(T8Context.class, T8FunctionalityAccessBlockerScriptDefinition.class);
        return (T8ServerContextScript)constructor.newInstance(context, this);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<T8DataParameterDefinition>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_IDENTIFIER, "Functionality Identifier", "The functionality being accessed.", T8DataType.DEFINITION_IDENTIFIER));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_DATA_OBJECTS, "Data Objects", "The data objects on which the specified functionality is being accessed.", T8DataType.CUSTOM_OBJECT));
        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<T8DataParameterDefinition>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_BLOCK_LIST, "Functionality Block List", "The list of functionality blocks generated by the script.", T8DataType.LIST));
        return parameterList;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        return null;
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        return null;
    }

    @Override
    public String getScript()
    {
        return getEPICScript();
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }

    public String getEPICScript()
    {
        return (String)getDefinitionDatum(Datum.EPIC_SCRIPT.toString());
    }

    public void setEPICScript(String script)
    {
        setDefinitionDatum(Datum.EPIC_SCRIPT.toString(), script);
    }
}
