package com.pilog.t8.definition.script;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.node.activity.T8ScriptActivityDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowActivityScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_ACTIVITY";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FLOW_ACTIVITY";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Activity Script";
    public static final String DESCRIPTION = "A script that is executed directly from an activity in a flow.";
    // -------- Definition Meta-Data -------- //
    
    public T8FlowActivityScriptDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8ScriptActivityDefinition activityDefinition;
        
        activityDefinition = (T8ScriptActivityDefinition)getParentDefinition();
        if (activityDefinition != null)
        {
            T8WorkFlowDefinition flowDefinition;
            
            flowDefinition = (T8WorkFlowDefinition)activityDefinition.getParentDefinition();
            return flowDefinition.getFlowParameterDefinitions();
        }
        else return null;
    }
    
    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8ScriptActivityDefinition activityDefinition;
        
        activityDefinition = (T8ScriptActivityDefinition)getParentDefinition();
        if (activityDefinition != null)
        {
            T8WorkFlowDefinition flowDefinition;
            
            flowDefinition = (T8WorkFlowDefinition)activityDefinition.getParentDefinition();
            return flowDefinition.getFlowParameterDefinitions();
        }
        else return null;
    }
}
