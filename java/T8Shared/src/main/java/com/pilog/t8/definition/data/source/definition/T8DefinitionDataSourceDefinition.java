package com.pilog.t8.definition.data.source.definition;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDataSourceDefinition extends T8DataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DEFINITION";
    public static final String DISPLAY_NAME = "Definition Data Source";
    public static final String DESCRIPTION = "A Data Source that is used to access definition data.";
    public static final String VERSION = "0";
    public static enum Datum {DEFINITION_GROUP_IDENTIFIER,
                              DEFINITION_TYPE_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_GROUP_IDENTIFIER, Datum.DEFINITION_GROUP_IDENTIFIER.toString(), "Definition Group", "The definition group on which this data source is based."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_TYPE_IDENTIFIER, Datum.DEFINITION_TYPE_IDENTIFIER.toString(), "Definition Type", "The definition type on which this data source is based."));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DEFINITION_GROUP_IDENTIFIER.toString().equals(datumIdentifier)) return createStringOptions(definitionContext.getDefinitionGroupIdentifiers());
        else if (Datum.DEFINITION_TYPE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String definitionGroup;

            definitionGroup = getDefinitionGroupIdentifier();
            if (definitionGroup != null)
            {
                return createStringOptions(definitionContext.getDefinitionTypeIdentifiers(definitionGroup), true, "All");
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction dataAccessProvider)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.source.definition.T8DefinitionDataSource").getConstructor(this.getClass(), T8DataTransaction.class);
            return (T8DataSource)constructor.newInstance(this, dataAccessProvider);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getDefinitionGroupIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DEFINITION_GROUP_IDENTIFIER.toString());
    }

    public void setDefinitionGroupIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DEFINITION_GROUP_IDENTIFIER.toString(), identifier);
    }

    public String getDefinitionTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIER.toString());
    }

    public void setDefinitionTypeIdentifier(String typeIdentifier)
    {
        setDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIER.toString(), typeIdentifier);
    }
}
