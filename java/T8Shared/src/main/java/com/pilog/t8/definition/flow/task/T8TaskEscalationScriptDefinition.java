package com.pilog.t8.definition.flow.task;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TaskEscalationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_TASK_ESCALATION_SCRIPT";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_TASK_ESCALATION_SCRIPT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Task Escalation";
    public static final String DESCRIPTION = "A script that is executed directly from an activity in a flow.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_TRIGGER = "$P_TRIGGER";
    public static final String PARAMETER_TASK_STATE = "$P_TASK_STATE";

    public T8TaskEscalationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_TRIGGER, "Escalation Trigger", "The trigger that resulted in the escalation of the task.", T8DataType.CUSTOM_OBJECT));
        definitions.add(new T8DataParameterDefinition(PARAMETER_TASK_STATE, "Task State", "The state of the task that is being escalated.", T8DataType.CUSTOM_OBJECT));
        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }
}
