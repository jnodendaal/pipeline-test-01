package com.pilog.t8.definition.data;

/**
 * @author Bouwer du Preez
 */
public class T8VariableDefinition extends T8DataValueDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_VARIABLE";
    public static final String GROUP_IDENTIFIER = "@DG_VARIABLE";
    public static final String IDENTIFIER_PREFIX = "V_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Variable";
    public static final String DESCRIPTION = "A variable accessible in scripts executing within the scope of a specifc T8 context.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8VariableDefinition(String identifier)
    {
        super(identifier);
    }
}
