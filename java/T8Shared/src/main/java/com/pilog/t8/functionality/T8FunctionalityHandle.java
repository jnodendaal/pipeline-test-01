package com.pilog.t8.functionality;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityHandle implements Serializable
{
    private final String id;
    private String displayName;
    private String description;
    private String iconUri;
    private Icon icon;
    private boolean enabled;
    private String confirmationMessage;
    private String disabledMessage;
    private final Map<String, Object> parameters; // Stores the parameters for execution of the functionality to which this handle refers. Key: parameterId, Value: parameterValue.
    private final List<T8FunctionalityParameterHandle> parameterHandles;

    public T8FunctionalityHandle(String id)
    {
        this.id = id;
        this.enabled = true;
        this.parameters = new HashMap<>();
        this.parameterHandles = new ArrayList<>();
    }

    public String getId()
    {
        return id;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Icon getIcon()
    {
        return icon;
    }

    public void setIcon(Icon icon)
    {
        this.icon = icon;
    }

    public String getIconUri()
    {
        return iconUri;
    }

    public void setIconUri(String iconUri)
    {
        this.iconUri = iconUri;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getDisabledMessage()
    {
        return disabledMessage;
    }

    public void setDisabledMessage(String disabledMessage)
    {
        this.disabledMessage = disabledMessage;
    }

    public String getConfirmationMessage()
    {
        return confirmationMessage;
    }

    public void setConfirmationMessage(String confirmationMessage)
    {
        this.confirmationMessage = confirmationMessage;
    }

    public Map<String, Object> getPublicParameters()
    {
        return T8IdentifierUtilities.prependNamespace(id, parameters, false);
    }

    public Map<String, Object> getParameters()
    {
        return new HashMap<>(parameters);
    }

    public void setParameters(Map<String, Object> functionalityParameters)
    {
        this.parameters.clear();
        if (functionalityParameters != null)
        {
            this.parameters.putAll(T8IdentifierUtilities.stripNamespace(id, functionalityParameters, true));
        }
    }

    public void addParameter(String parameterId, Object parameterValue)
    {
        this.parameters.put(T8IdentifierUtilities.stripNamespace(parameterId, id, true), parameterValue);
    }

    public void setParameterHandles(List<T8FunctionalityParameterHandle> newHandles)
    {
        this.parameterHandles.clear();
        if (newHandles != null)
        {
            this.parameterHandles.addAll(newHandles);
        }
    }

    public T8FunctionalityParameterHandle getParameterHandle(String parameterId)
    {
        for (T8FunctionalityParameterHandle handle : parameterHandles)
        {
            if (handle.getParameterId().equals(parameterId))
            {
                return handle;
            }
        }

        return null;
    }

    public void addParameterHandle(T8FunctionalityParameterHandle handle)
    {
        this.parameterHandles.add(handle);
    }

    public String getParameterObjectId(String parameterId)
    {
        T8FunctionalityParameterHandle handle;

        handle = getParameterHandle(parameterId);
        return handle != null ? handle.getDataObjectId() : null;
    }

    public Set<String> getObjectIds()
    {
        Set<String> ids;

        ids = new HashSet<>();
        for (T8FunctionalityParameterHandle handle : parameterHandles)
        {
            String objectId;

            objectId = handle.getDataObjectId();
            if (objectId != null) ids.add(objectId);
        }

        return ids;
    }

    public Set<String> getRequiredObjectIds()
    {
        Set<String> ids;

        ids = new HashSet<>();
        for (T8FunctionalityParameterHandle handle : parameterHandles)
        {
            if (handle.isRequired())
            {
                String objectId;

                objectId = handle.getDataObjectId();
                if (objectId != null) ids.add(objectId);
            }
        }

        return ids;
    }

    public Map<String, String> getParameterObjectIds()
    {
        Map<String, String> objectIds;

        objectIds = new HashMap<>();
        for (T8FunctionalityParameterHandle handle : parameterHandles)
        {
            String objectId;

            objectId = handle.getDataObjectId();
            if (objectId != null)
            {
                objectIds.put(handle.getParameterId(), objectId);
            }
        }

        return objectIds;
    }

    public Map<String, String> getRequiredParameterObjectIds()
    {
        Map<String, String> objectIds;

        objectIds = new HashMap<>();
        for (T8FunctionalityParameterHandle handle : parameterHandles)
        {
            String objectId;

            objectId = handle.getDataObjectId();
            if (objectId != null)
            {
                if (handle.isRequired())
                {
                    objectIds.put(handle.getParameterId(), objectId);
                }
            }
        }

        return objectIds;
    }

    public void setTargetDataObject(T8DataObject targetObject)
    {
        Map<String, String> requiredParameterObjectIds;

        requiredParameterObjectIds = getRequiredParameterObjectIds();
        if (requiredParameterObjectIds.size() > 1)
        {
            throw new RuntimeException("Cannot set a single target object on a functionality handle where more than one object is required.");
        }
        else if (getRequiredParameterObjectIds().isEmpty())
        {
            throw new RuntimeException("Cannot set a target object on a functionality handle that does not specify a required input object.");
        }
        else
        {
            String parameterId;
            String dataObjectId;

            parameterId = requiredParameterObjectIds.keySet().iterator().next();
            dataObjectId = requiredParameterObjectIds.get(parameterId);
            if (targetObject.getId().equals(dataObjectId))
            {
                this.parameters.put(parameterId, targetObject.getIid());
            }
            else throw new RuntimeException("Cannot set target object " + targetObject + " on functionality handle that specifies a single required object: " + dataObjectId);
        }
    }

    @Override
    public String toString()
    {
        return "T8FunctionalityHandle{" + "id=" + id + ", parameters=" + parameters + '}';
    }
}
