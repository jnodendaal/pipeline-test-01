package com.pilog.t8.definition.file.context;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8ServerFileContextDefinition extends T8FileContextDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FILE_CONTEXT_SERVER";
    public static final String DISPLAY_NAME = "Server File Context";
    public static final String DESCRIPTION = "A file context on the T8 Server.";
    public enum Datum {FILE_PATH};
    // -------- Definition Meta-Data -------- //

    public T8ServerFileContextDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FILE_PATH.toString(), "File Path", "The file path opn the server where this file context is located."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8FileContext createNewFileContextInstance(T8Context context, T8FileManager fileManager, String contextInstanceID) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.file.context.T8ServerFileContext").getConstructor(T8Context.class, T8FileManager.class, this.getClass(), String.class);
        return (T8FileContext)constructor.newInstance(context, fileManager, this, contextInstanceID);
    }

    public String getFilePath()
    {
        return (String)getDefinitionDatum(Datum.FILE_PATH.toString());
    }

    public void setFilePath(String filePath)
    {
        setDefinitionDatum(Datum.FILE_PATH.toString(), filePath);
    }
}
