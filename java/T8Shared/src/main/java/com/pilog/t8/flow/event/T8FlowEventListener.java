package com.pilog.t8.flow.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowEventListener extends EventListener
{
    public void flowCompleted(T8FlowCompletedEvent event);
    public void flowStopped(T8FlowStoppedEvent event);
    public void flowFinalized(T8FlowFinalizedEvent event);
    public void taskClaimed(T8FlowTaskClaimedEvent event);
    public void taskUnclaimed(T8FlowTaskUnclaimedEvent event);
    public void taskCompleted(T8FlowTaskCompletedEvent event);
    public void signalReceived(T8FlowSignalReceivedEvent event);
}
