package com.pilog.t8.definition.flow.task.script;

import com.pilog.t8.definition.flow.task.script.T8FlowScriptTaskDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowTaskDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowScriptTaskDefinition extends T8FlowScriptTaskDefinition implements T8WorkFlowTaskDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_TASK_SCRIPT";
    public static final String DISPLAY_NAME = "Script Task";
    public static final String DESCRIPTION = "A task that executes a script wihtin the server context.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //
    
    public T8WorkFlowScriptTaskDefinition(String identifier)
    {
        super(identifier);
    }
}
