package com.pilog.t8.definition.ui.dialog;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Dialog and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8GlobalDialogAPIHandler
{
    public static ArrayList<T8ComponentEventDefinition> events;
    public static ArrayList<T8ComponentOperationDefinition> operations;

    // These should NOT be changed, only added to.
    public static final String EVENT_DIALOG_STARTED = "$CE_DIALOG_STARTED";
    public static final String EVENT_DIALOG_OPENED = "$CE_DIALOG_OPENED";
    public static final String EVENT_DIALOG_CLOSED = "$CE_DIALOG_CLOSED";
    public static final String EVENT_DIALOG_CLOSE_BUTTON_PRESSED = "$CE_DIALOG_CLOSE_BUTTON_PRESSED";
    public static final String OPERATION_OPEN_DIALOG = "$CO_OPEN_DIALOG";
    public static final String OPERATION_CLOSE_DIALOG = "$CO_CLOSE_DIALOG";
    public static final String OPERATION_SET_TITLE = "$CO_SET_TITLE";
    public static final String PARAMETER_TITLE = "$P_TITLE";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_OUTPUT_PARAMETERS = "$P_OUTPUT_PARAMETERS";

    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DIALOG_STARTED);
        newEventDefinition.setMetaDisplayName("Dialog Started");
        newEventDefinition.setMetaDescription("This event occurs when the dialog and all of its content components have been initialized and started.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DIALOG_OPENED);
        newEventDefinition.setMetaDisplayName("Dialog Opened");
        newEventDefinition.setMetaDescription("This event occurs when the dialog is opened.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DIALOG_CLOSED);
        newEventDefinition.setMetaDisplayName("Dialog Closed");
        newEventDefinition.setMetaDescription("This event occurs when the dialog is closed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OUTPUT_PARAMETERS, "Output Parameters", "The output parameters of the dialog.", T8DataType.MAP));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DIALOG_CLOSE_BUTTON_PRESSED);
        newEventDefinition.setMetaDisplayName("Dialog Close Button Pressed");
        newEventDefinition.setMetaDescription("This event occurs when the dialog's close button is pressed.");
        events.add(newEventDefinition);
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_OPEN_DIALOG);
        newOperationDefinition.setMetaDisplayName("Open Dialog");
        newOperationDefinition.setMetaDescription("This operations opens the dialog so that it becomes visible for user input.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLOSE_DIALOG);
        newOperationDefinition.setMetaDisplayName("Disable Button");
        newOperationDefinition.setMetaDescription("This operations closes the dialog.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OUTPUT_PARAMETERS, "Output Parameters", "The output parameters of the dialog.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TITLE);
        newOperationDefinition.setMetaDisplayName("Set Title");
        newOperationDefinition.setMetaDescription("This operation sets the title of the dialog.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TITLE, "Title", "The title to set on the dialog.", T8DataType.STRING));
        operations.add(newOperationDefinition);
    }

    public static ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    public static ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}
