package com.pilog.t8.flow;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowExecutionKey
{
    public String getFlowIdentifier();
    public String getFlowInstanceIdentifier();
    public String getNodeIdentifier();
    public String getNodeInstanceIdentifier();
}
