package com.pilog.t8.definition.data.source;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataSourceDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_SOURCE";
    public static final String GROUP_NAME = "Data Sources";
    public static final String GROUP_DESCRIPTION = "Definitions of various sources from which data can be extracted and, where applicable, to which data can be written.";
    public static final String STORAGE_PATH = "/data_sources";
    public static final String IDENTIFIER_PREFIX = "DS_";
    public enum Datum {FIELD_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    private Map<String, String> fieldToSourceMapping;
    private Map<String, String> sourceToFieldMapping;

    public T8DataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Field Definitions", "The definitions of the fields provided by this data source."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataSourceFieldDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.data.source.T8DataSourceDefinitionActionHandler", new Class<?>[]{T8Context.class, T8DataSourceDefinition.class}, context, this);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        fieldToSourceMapping = createFieldToSourceIdentifierMapping(false);
        sourceToFieldMapping = T8IdentifierUtilities.createReverseIdentifierMap(fieldToSourceMapping);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;
        T8DataSource dataSource = null;

        // Do the default definition validation.
        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        // Test the data source.
        try
        {
            List<T8DataSourceFieldDefinition> existingFieldDefinitions;
            T8DataTransaction tx;
            T8DataSession ds;

            // Get the current data session and from it, the instant transaction to use for data source testing.
            ds = serverContext.getDataManager().getCurrentSession();
            tx = ds.instantTransaction();

            // Open the data source and simple retrieve its field definitions.
            dataSource = getNewDataSourceInstance(tx);
            dataSource.open();
            existingFieldDefinitions = dataSource.retrieveFieldDefinitions();
            dataSource.close();

            // Verify that each field in the data source actually exists.
            for (T8DataSourceFieldDefinition fieldDefinition : getFieldDefinitions())
            {
                boolean fieldExists;

                fieldExists = false;
                for (T8DataSourceFieldDefinition existingFieldDefinition : existingFieldDefinitions)
                {
                    if (Objects.equals(fieldDefinition.getSourceIdentifier(), existingFieldDefinition.getSourceIdentifier()))
                    {
                        fieldExists = true;
                        break;
                    }
                }

                // If we could not find the field in the existing collection, add a validation error.
                if (!fieldExists)
                {
                    validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.FIELD_DEFINITIONS.toString(), "Source field not found: " + fieldDefinition.getIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
                }
            }
        }
        catch (Exception e)
        {
            try
            {
                if (dataSource != null) dataSource.close();
            }
            catch (Exception e2)
            {
                e2.printStackTrace();
            }

            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), null, e.getMessage(), T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        // Return all errors identified.
        return validationErrors;
    }

    /**
     * Factory method for data source instances.  Data Sources get all access to
     * the server-environment through the data access provider.
     * @param accessProvider
     * @return
     * @throws java.lang.Exception
     */
    public abstract T8DataSource getNewDataSourceInstance(T8DataTransaction accessProvider) throws Exception;

    public Map<String, String> getFieldToSourceIdentifierMapping()
    {
        return fieldToSourceMapping;
    }

    public Map<String, String> getSourceToFieldIdentifierMapping()
    {
        return sourceToFieldMapping;
    }

    public String mapFieldToSourceIdentifier(String fieldIdentifier)
    {
        return fieldToSourceMapping.get(fieldIdentifier);
    }

    public String mapSourceToFieldIdentifier(String fieldIdentifier)
    {
        return sourceToFieldMapping.get(fieldIdentifier);
    }

    public boolean containsField(String fieldIdentifier)
    {
        for (T8DataFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if ((fieldDefinition.getIdentifier().equals(fieldIdentifier)) || (fieldDefinition.getPublicIdentifier().equals(fieldIdentifier))) return true;
        }

        return false;
    }

    public T8DataSourceFieldDefinition getFieldDefinition(String fieldIdentifier)
    {
        for (T8DataSourceFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if ((fieldDefinition.getIdentifier().equals(fieldIdentifier)) || (fieldDefinition.getPublicIdentifier().equals(fieldIdentifier))) return fieldDefinition;
        }

        return null;
    }

    public List<T8DataSourceFieldDefinition> getFieldDefinitions()
    {
        return (List<T8DataSourceFieldDefinition>)getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8DataSourceFieldDefinition> fieldDefinitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), fieldDefinitions);
    }

    public void addFieldDefinition(T8DataSourceFieldDefinition fieldDefinition)
    {
        String fieldIdentifier;

        fieldIdentifier = fieldDefinition.getIdentifier();
        if (!containsField(fieldIdentifier))
        {
            addSubDefinition(Datum.FIELD_DEFINITIONS.toString(), fieldDefinition);
        }
        else throw new RuntimeException("Attempt to add duplicate field: " + fieldIdentifier);
    }

    public T8DataType getFieldDataType(String fieldIdentifier)
    {
        T8DataFieldDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldIdentifier);
        if (fieldDefinition == null) throw new RuntimeException("Field not found: " + fieldIdentifier);
        else return fieldDefinition.getDataType();
    }

    public List<String> getFieldIdentifiers()
    {
        return getFieldDefinitions().stream().map(T8DataFieldDefinition::getIdentifier).collect(Collectors.toCollection(ArrayList::new));
    }

    public List<String> getKeyFieldIdentifiers()
    {
        return getFieldDefinitions().stream().filter(T8DataFieldDefinition::isKey).map(T8DataFieldDefinition::getIdentifier).collect(Collectors.toCollection(ArrayList::new));
    }

    public List<String> getRequiredFieldIdentifiers()
    {
        return getFieldDefinitions().stream().filter(T8DataFieldDefinition::isRequired).map(T8DataFieldDefinition::getIdentifier).collect(Collectors.toCollection(ArrayList::new));
    }

    private Map<String, String> createFieldToSourceIdentifierMapping(boolean localFieldIdentifiers)
    {
        List<T8DataSourceFieldDefinition> fieldDefinitions;

        fieldDefinitions = getFieldDefinitions();
        if (fieldDefinitions != null)
        {
            Map<String, String> identifierMap;

            identifierMap = new HashMap<>();
            for (T8DataFieldDefinition fieldDefinition : fieldDefinitions)
            {
                identifierMap.put(localFieldIdentifiers ? fieldDefinition.getIdentifier() : fieldDefinition.getPublicIdentifier(), fieldDefinition.getSourceIdentifier());
            }

            return identifierMap;
        }
        else return null;
    }
}
