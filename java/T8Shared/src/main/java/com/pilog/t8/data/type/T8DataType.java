package com.pilog.t8.data.type;

import com.pilog.t8.datatype.T8DtBigInteger;
import com.pilog.t8.datatype.T8DtByteArray;
import com.pilog.t8.datatype.T8DtBoolean;
import com.pilog.t8.datatype.T8DtCustomObject;
import com.pilog.t8.datatype.T8DtBigDecimal;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtImage;
import com.pilog.t8.datatype.T8DtUserDetails;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.datatype.T8DtLong;
import com.pilog.t8.datatype.T8DtDate;
import com.pilog.t8.datatype.T8DtFloat;
import com.pilog.t8.datatype.T8DtInteger;
import com.pilog.t8.datatype.T8DtTime;
import com.pilog.t8.datatype.T8DtTimestamp;
import com.pilog.t8.datatype.T8DtDefinition;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtUndefined;
import com.pilog.t8.datatype.T8DtDouble;
import com.pilog.t8.datatype.T8DtDataFilter;
import com.pilog.t8.datatype.T8DtDataEntity;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.datatype.T8DtString.T8StringType;
import java.io.Serializable;
import java.util.EnumSet;

/**
 * Example:  MAP<STRING(TYPE=IDENTIFIER),STRING(TYPE=EPIC_EXPRESSION)>
 * @author Bouwer du Preez
 */
public interface T8DataType extends Serializable
{
    // Common Data Types.
    public static final T8DataType UNDEFINED = new T8DtUndefined(); // Any java object that does not fall under one of the other explicitly defined types.
    /** Any java object that does not fall under one of the other explicitly defined types.*/
    public static final T8DataType CUSTOM_OBJECT = new T8DtCustomObject();
    public static final T8DataType USER_DETAILS = new T8DtUserDetails();
    public static final T8DataType DATA_FILTER = new T8DtDataFilter();
    public static final T8DataType STRING = new T8DtString(); // A regular String value that will not be translated at any stage and which has a length shorter than 4000 characters.
    public static final T8DataType BOOLEAN = new T8DtBoolean();
    public static final T8DataType INTEGER = new T8DtInteger();
    public static final T8DataType BIG_INTEGER = new T8DtBigInteger();
    public static final T8DataType LONG = new T8DtLong();
    public static final T8DataType FLOAT = new T8DtFloat();
    public static final T8DataType DOUBLE = new T8DtDouble();
    public static final T8DataType BIG_DECIMAL = new T8DtBigDecimal();
    public static final T8DataType TIME = new T8DtTime();
    public static final T8DataType DATE = new T8DtDate();
    public static final T8DataType TIMESTAMP = new T8DtTimestamp(); // Equivalent to DATE_TIME.
    public static final T8DataType DATE_TIME = new T8DtTimestamp(); // Equivalent to TIMESTAMP.
    public static final T8DataType DEFINITION = new T8DtDefinition(); // A T8Definition.
    public static final T8DataType MAP = new T8DtMap(UNDEFINED, UNDEFINED);
    public static final T8DataType LIST = new T8DtList(UNDEFINED);
    public static final T8DataType DATA_ENTITY = new T8DtDataEntity();
    public static final T8DataType GUID = new T8DtString(T8StringType.GUID);
    public static final T8DataType BYTE_ARRAY = new T8DtByteArray();
    public static final T8DataType IMAGE = new T8DtImage();
    public static final T8DataType LONG_STRING = new T8DtString(T8StringType.LONG_STRING); // A String value that can potentially be longer than 4000 characters.  For this type alternative storage methods are used e.g. CLOBs.
    public static final T8DataType DISPLAY_STRING = new T8DtString(T8StringType.TRANSLATED_STRING); // A String value that will be visible to the User on the UI and that needs to be translated if required.
    public static final T8DataType IDENTIFIER_STRING = new T8DtString(); // A String value containing one or more T8Definition identifiers that need to be included in any developer refactoring.
    public static final T8DataType EPIC_SCRIPT = new T8DtString(T8StringType.EPIC_SCRIPT);
    public static final T8DataType JAVA = new T8DtString(T8StringType.JAVA);
    public static final T8DataType EPIC_EXPRESSION = new T8DtString(T8StringType.EPIC_EXPRESSION);
    public static final T8DataType EPIC_EXPRESSION_LIST = new T8DtList(EPIC_EXPRESSION);
    public static final T8DataType EPIC_EXPRESSION_VALUE_MAP = new T8DtMap(STRING, EPIC_EXPRESSION);
    public static final T8DataType EPIC_EXPRESSION_DISPLAY_STRING_MAP = new T8DtMap(EPIC_EXPRESSION, DISPLAY_STRING);
    public static final T8DataType PASSWORD_HASH = new T8DtString(T8StringType.PASSWORD);
    public static final T8DataType COLOR_HEX = new T8DtString(T8StringType.COLOR_HEX);
    public static final T8DataType DEFINITION_IDENTIFIER = new T8DtString(T8StringType.DEFINITION_IDENTIFIER); // A single global T8Definition identifier String.
    public static final T8DataType DEFINITION_INSTANCE_IDENTIFIER = new T8DtString(T8StringType.DEFINITION_INSTANCE_IDENTIFIER); // A single T8Definition instance identifier String.
    public static final T8DataType DEFINITION_TYPE_IDENTIFIER = new T8DtString(T8StringType.DEFINITION_TYPE_IDENTIFIER); // A single global T8Definition type identifier String.
    public static final T8DataType DEFINITION_GROUP_IDENTIFIER = new T8DtString(T8StringType.DEFINITION_GROUP_IDENTIFIER); // A single global T8Definition group identifier String.
    public static final T8DataType DEFINITION_LIST = new T8DtList(DEFINITION); // A List of T8Definition objects.
    public static final T8DataType DEFINITION_IDENTIFIER_LIST = new T8DtList(DEFINITION_IDENTIFIER); // A List of T8Definition identifier Strings.
    public static final T8DataType DEFINITION_IDENTIFIER_MAP = new T8DtMap(DEFINITION_IDENTIFIER, DEFINITION_IDENTIFIER); // A Map with T8Definition identifier String key-value pairs.
    public static final T8DataType DEFINITION_IDENTIFIER_EXPRESSION_MAP = new T8DtMap(DEFINITION_IDENTIFIER, EPIC_EXPRESSION); // A Map with T8Definition identifiers as keys and EPIC Expressions as values.
    public static final T8DataType DEFINITION_IDENTIFIER_STRING_MAP = new T8DtMap(DEFINITION_IDENTIFIER, STRING); // A Map with T8Definition identifier String keys.
    public static final T8DataType DEFINITION_IDENTIFIER_KEY_MAP = new T8DtMap(DEFINITION_IDENTIFIER, UNDEFINED); // A Map with T8Definition identifier String keys.
    public static final T8DataType DEFINITION_IDENTIFIER_VALUE_MAP = new T8DtMap(UNDEFINED, DEFINITION_IDENTIFIER); // A Map with T8Definition identifier String values.
    public static final T8DataType DEFINITION_GROUP_IDENTIFIER_LIST = new T8DtList(DEFINITION_GROUP_IDENTIFIER); // A list of global T8Definition group identifier String.

    /**
     * Returns the globally unique identifier of this data type.
     * @return The globally unique identifier of this data type.
     */
    public String getDataTypeIdentifier();

    /**
     * Returns true if the specified data type has the same identifier as this
     * type.
     * @param type The type to test against this type.
     * @return Boolean true if the specified type has the same identifier as
     * this type.
     */
    public boolean isType(T8DataType type);

    /**
     * Returns true if the specified data type has the same identifier as the
     * specified type.
     * @param typeIdentifier The type identifier to test against this type.
     * @return Boolean true if the specified type identifier is the same as this
     * type.
     */
    public boolean isType(String typeIdentifier);

    /**
     * Creates a String representation of this data type, containing the
     * complete descendant type structure.
     * @param includeAttributes A flag to indicate whether to functional
     * attributes of the data type should be included in the string
     * representation.
     * @return A String representation of this data type and its content types.
     */
    public String getDataTypeStringRepresentation(boolean includeAttributes);

    /**
     * Returns the canonical name of the class from which objects of this data
     * type is created.
     * @return The canonical name of the class from which objects of this data
     * type is created.
     */
    public String getDataTypeClassName();

    /**
     * Returns the JSON object representation of the supplied object.  The valid
     * object types that may be returned by this method are:
     * - String
     * - Boolean
     * - Integer
     * - Long
     * - Double
     * - BigInteger
     * - BigDecimal
     * - JsonArrayBuilder
     * - JsonObjectBuilder
     * @param object The object from which to create the JSON object.
     * @return The JSON representation of the supplied object.
     */
    public JsonValue serialize(Object object);

    /**
     * Extracts the data type value from the supplied JSON value.
     * @param jsonValue The JSON value from which to extract the data.
     * @return The data value extracted from the JSON value.
     */
    public Object deserialize(JsonValue jsonValue);

    /**
     * Returns an {@code EnumSet} of the {@code DataFilterOperator}s which are
     * applicable to the current data type.<br/>
     * <br/>
     * By default, there should be a standard set of included operators for each
     * data type.
     *
     * @return The {@code EnumSet} of {@code DataFilterOperator}s which are
     *      applicable to the current {@code T8DataType}
     */
    default EnumSet<DataFilterOperator> getApplicableOperators()
    {
        return T8DataType.getDefaultOperatorSet();
    }

    /**
     * The default set of {@code DataFilterOperators} which are applicable to
     * {@code T8DataType}s. This is the same set which is returned if the data
     * type does not specify its own set of applicable operators.
     *
     * @return The default set of applicable data filter operators
     */
    public static EnumSet<DataFilterOperator> getDefaultOperatorSet()
    {
        EnumSet<DataFilterOperator> excludedByDefault;

        // Once the default set becomes smaller than the exclusion set, this
        // should be reversed
        excludedByDefault = EnumSet.of(DataFilterOperator.THIS_DAY, DataFilterOperator.EQUAL_TO_FIELD);

        return EnumSet.complementOf(excludedByDefault);
    }
}
