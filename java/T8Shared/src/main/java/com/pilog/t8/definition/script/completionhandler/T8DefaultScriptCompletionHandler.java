package com.pilog.t8.definition.script.completionhandler;

import com.pilog.epic.rsyntax.BasicEpicCompletion;
import com.pilog.epic.rsyntax.MethodParameter;
import com.pilog.epic.rsyntax.MethodParameterChoicesProvider;
import com.pilog.epic.rsyntax.RegexCompletionProvider;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.communication.T8CommunicationDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.key.T8DataKeyGeneratorDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.definition.process.operation.T8ServerOperationProcessDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultScriptCompletionHandler implements T8ScriptCompletionHandler, MethodParameterChoicesProvider
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8DefaultScriptCompletionHandler.class.getName());

    private final Map<String, String> parameterGroupId;
    protected List<T8ScriptCompletionResolver> resolvers;
    protected T8DefinitionManager definitionManager;
    protected T8Context context;
    protected T8Definition parentDefinition;

    public T8DefaultScriptCompletionHandler(T8Context context, T8Definition parentDefinition)
    {
        resolvers = new ArrayList<>();
        this.context = context;
        this.definitionManager = context.getClientContext().getDefinitionManager();
        this.parentDefinition = parentDefinition;
        parameterGroupId = new HashMap<>();
        parameterGroupId.put("flowIdentifier", T8WorkFlowDefinition.GROUP_IDENTIFIER);
        parameterGroupId.put("serverProcessIdentifier", T8ServerOperationProcessDefinition.GROUP_IDENTIFIER);
        parameterGroupId.put("serverOperationIdentifier", T8ServerOperationDefinition.GROUP_IDENTIFIER);
        parameterGroupId.put("clientOperationIdentifier", T8ClientOperationDefinition.GROUP_IDENTIFIER);
        parameterGroupId.put("communicationIdentifier", T8CommunicationDefinition.GROUP_IDENTIFIER);
        parameterGroupId.put("keyIdentifier", T8DataKeyGeneratorDefinition.GROUP_IDENTIFIER);
        parameterGroupId.put("entityIdentifier", T8DataEntityDefinition.GROUP_IDENTIFIER);
    }

    public void addResolver(T8ScriptCompletionResolver resolver)
    {
        resolvers.add(resolver);
    }

    @Override
    public List<T8ScriptCompletionResolver> getResolvers()
    {
        return resolvers;
    }

    @Override
    public MethodParameterChoicesProvider getMethodParameterChoicesProvider()
    {
        return this;
    }

    @Override
    public List<BasicEpicCompletion> getParameterChoices(JTextComponent textComponent, MethodParameter parameter)
    {
        List<T8DefinitionMetaData> definitionMetaDatas;
        List<BasicEpicCompletion> returnList;

        returnList = new ArrayList<>();
        definitionMetaDatas = new ArrayList<>();
        returnList.addAll(getVariableChoices(textComponent.getText()));
        returnList.add(new BasicEpicCompletion("null"));
        if (parameter.getName().equals("paramMap"))
        {
            List<String> identifiers;

            identifiers = getIdentifiers(getJTextAreaCurrentLine((JTextArea) textComponent));
            return identifiers.isEmpty() ? new ArrayList<>() : createParamMapString(textComponent, identifiers.get(0));
        }
        else if (parameter.getName().equals("paramMapKey"))
        {
            return getParamMapKeyChoices(textComponent, getLastIdentifier(getTextUpToCarret((JTextArea) textComponent)));
        }
        else if (parameterGroupId.containsKey(parameter.getName()))
        {
            try
            {
                definitionMetaDatas = definitionManager.getGroupDefinitionMetaData(parentDefinition.getRootProjectId(), parameterGroupId.get(parameter.getName()));
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to get identifiers for auto complete parameter " + parameter, ex);
            }
        }

        for (T8DefinitionMetaData t8DefinitionTypeMetaData : definitionMetaDatas)
        {
            returnList.add(new BasicEpicCompletion("\"" + t8DefinitionTypeMetaData.getId() + "\"", t8DefinitionTypeMetaData.getDisplayName()));
        }
        return returnList;
    }

    @Override
    public List<RegexCompletionProvider> getRegexCompletionProviders()
    {
        List<RegexCompletionProvider> completionProviders;

        completionProviders = new ArrayList<>();
        completionProviders.add(new T8DefinitionRegexScriptCompletionProvider(context, definitionManager, null));
        return completionProviders;
    }

    protected List<BasicEpicCompletion> getParamMapKeyChoices(JTextComponent textComponent, String identifier)
    {
        try
        {
            T8Definition definition = definitionManager.getRawDefinition(context, parentDefinition.getRootProjectId(), identifier);
            List<T8DataParameterDefinition> parameterDefinitions;
            if (definition instanceof T8ServerOperationProcessDefinition)
            {
                parameterDefinitions = ((T8ServerOperationProcessDefinition) definition).getOutputParameterDefinitions();
            }
            else if (definition instanceof T8ServerOperationDefinition)
            {
                parameterDefinitions = ((T8ServerOperationDefinition) definition).getOutputParameterDefinitions();
            }
            else if (definition instanceof T8ClientOperationDefinition)
            {
                parameterDefinitions = ((T8ClientOperationDefinition) definition).getOutputParameterDefinitions();
            }
            else return new ArrayList<>(0);

            if (parameterDefinitions == null) return new ArrayList<>(0);

            List<BasicEpicCompletion> choices = new ArrayList<>();
            for (T8DataParameterDefinition t8DataParameterDefinition : parameterDefinitions)
            {
                choices.add(new BasicEpicCompletion("\"" + t8DataParameterDefinition.getPublicIdentifier() + "\""));
            }
            return choices;
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to create param hash map for " + identifier, ex);
        }

        return new ArrayList<>();
    }

    private List<BasicEpicCompletion> getVariableChoices(String scriptText)
    {
        List<BasicEpicCompletion> choices;

        choices = new ArrayList<>();
        for (String variable : getVariables(scriptText))
        {
            choices.add(new BasicEpicCompletion(variable));
        }

        return choices;
    }

    private List<String> getVariables(String scriptText)
    {
        Pattern pattern;
        Matcher matcher;
        List<String> matches;

        pattern = Pattern.compile("var [a-zA-Z0-9]+[ |,|;]");
        matcher = pattern.matcher(scriptText);
        matches = new ArrayList<>();
        while (matcher.find())
        {
            String match = matcher.group();
            match = match.replaceAll("var ", "");
            match = match.replaceAll(",", "");
            match = match.replaceAll(" ", "");
            match = match.replaceAll(";", "");

            matches.add(match);
        }
        return matches;
    }

    protected List<BasicEpicCompletion> createParamMapString(JTextComponent textComponent, String identifier)
    {
        try
        {
            T8Definition definition = definitionManager.getRawDefinition(context, parentDefinition.getRootProjectId(), identifier);
            List<T8DataParameterDefinition> parameterDefinitions;
            if (definition instanceof T8WorkFlowDefinition)
            {
                parameterDefinitions = ((T8WorkFlowDefinition) definition).getFlowParameterDefinitions();
            }
            else if (definition instanceof T8ServerOperationProcessDefinition)
            {
                parameterDefinitions = ((T8ServerOperationProcessDefinition) definition).getInputParameterDefinitions();
            }
            else if (definition instanceof T8ServerOperationDefinition)
            {
                parameterDefinitions = ((T8ServerOperationDefinition) definition).getInputParameterDefinitions();
            }
            else if (definition instanceof T8ClientOperationDefinition)
            {
                parameterDefinitions = ((T8ClientOperationDefinition) definition).getInputParameterDefinitions();
            }
            else if (definition instanceof T8CommunicationDefinition)
            {
                parameterDefinitions = ((T8CommunicationDefinition) definition).getInputParameters();
            }
            else return new ArrayList<>(0);

            if (parameterDefinitions == null) return Arrays.asList(new BasicEpicCompletion("null"));

            StringBuilder hashMapString = new StringBuilder("new [");
            if (parameterDefinitions.size() > 2)
            {
                hashMapString.append("\n");
            }
            Iterator<T8DataParameterDefinition> dataParamaterIterator = parameterDefinitions.iterator();
            while (dataParamaterIterator.hasNext())
            {
                T8DataParameterDefinition dataParameterDefinition = dataParamaterIterator.next();
                hashMapString.append("\"");
                hashMapString.append(dataParameterDefinition.getPublicIdentifier());
                hashMapString.append("\":");
                if (dataParamaterIterator.hasNext())
                {
                    hashMapString.append(",");
                    if (parameterDefinitions.size() > 2)
                    {
                        hashMapString.append("\n");
                    }
                }
            }
            // Make sure to add the ":" if an empty map is required
            hashMapString.append(parameterDefinitions.isEmpty() ? ":]" : "]");

            if (parameterDefinitions.size() > 2)
            {
                hashMapString.append("\n");
            }
            return Arrays.asList(new BasicEpicCompletion(hashMapString.toString()));
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to create param hash map for " + identifier, ex);
        }
        return new ArrayList<>();
    }

    protected List<String> getIdentifiers(String textToCheck)
    {
        List<String> matches;

        matches = StringUtilities.findStrings("\"", "[@|$][a-zA-Z0-9_]+", "\"", new StringBuffer(textToCheck));
        return matches;
    }

    protected String getLastIdentifier(String textToCheck)
    {
        List<String> identifiers = getIdentifiers(textToCheck);
        return identifiers.get(identifiers.size() - 1);
    }

    protected String getJTextAreaCurrentLine(JTextArea textArea)
    {
        try
        {
            int lineNumber = textArea.getLineOfOffset(textArea.getCaretPosition());
            int lineStart = textArea.getLineStartOffset(lineNumber);
            int lineEnd = textArea.getLineEndOffset(lineNumber);
            return textArea.getText(lineStart, lineEnd - lineStart);
        }
        catch (BadLocationException ex)
        {
            LOGGER.log("Failed to get text for current line", ex);
            return "";
        }
    }

    protected String getTextUpToCarret(JTextArea textArea)
    {
        try
        {
            return textArea.getText(0, textArea.getCaretPosition());
        }
        catch (BadLocationException ex)
        {
            LOGGER.log("Failed to get text up to carret location", ex);
            return "";
        }
    }
}
