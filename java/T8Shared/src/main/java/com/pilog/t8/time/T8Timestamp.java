package com.pilog.t8.time;

import static com.pilog.t8.utilities.date.Dates.STD_SYS_TS_FORMATTER;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Bouwer du Preez
 */
public class T8Timestamp implements Serializable, Comparable<T8Timestamp>
{
    private long milliseconds;

    public T8Timestamp(long milliseconds)
    {
        this.milliseconds = milliseconds;
    }

    public long getMilliseconds()
    {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds)
    {
        this.milliseconds = milliseconds;
    }

    @Override
    public int hashCode()
    {
        return 61 * 7 + (int) (this.milliseconds ^ (this.milliseconds >>> 32));
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null || T8Timestamp.class != obj.getClass()) return false;

        return this.milliseconds == ((T8Timestamp)obj).milliseconds;
    }

    @Override
    public String toString()
    {
        return STD_SYS_TS_FORMATTER.format(new Date(milliseconds));
    }

    @Override
    public int compareTo(T8Timestamp other)
    {
        return Long.compare(this.milliseconds, other.milliseconds);
    }

    /**
     * Returns a new instance of {@code T8Timestamp} which defines a
     * representation of the time in milliseconds passed as parameter. This
     * method is {@code null}-safe.
     *
     * @param milliseconds The {@code long} time in milliseconds
     *
     * @return A {@code T8Timestamp} representation of the specified time.
     *      {@code null} if the milliseconds parameter is {@code null}
     */
    public static T8Timestamp fromTime(Long milliseconds)
    {
        if (milliseconds == null) return null;
        else return new T8Timestamp(milliseconds);
    }

    public static T8Timestamp now()
    {
        return new T8Timestamp(System.currentTimeMillis());
    }
}
