/**
 * Created on Sep 2, 2015, 7:44:50 PM
 */
package com.pilog.t8.time;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Gavin Boshoff
 */
public class T8TimestampFormatter extends Format
{
    private static final long serialVersionUID = 3005630828156253474L;

    private final DateFormat dateFormatter;

    public T8TimestampFormatter(String formatPattern)
    {
        this.dateFormatter = SimpleDateFormat.getDateTimeInstance();
        ((SimpleDateFormat)this.dateFormatter).applyPattern(formatPattern);
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos)
    {
        if (obj == null) return new StringBuffer();
        Date toFormat;

        if (obj instanceof Long) toFormat = new Date((long)obj);
        else if (obj instanceof String) toFormat = new Date(Long.parseUnsignedLong((String)obj));
        else if (obj instanceof java.util.Date) toFormat = (Date)obj;
        else if (obj instanceof T8Timestamp) toFormat = new Date(((T8Timestamp)obj).getMilliseconds());
        else return new StringBuffer();

        return new StringBuffer(this.dateFormatter.format(toFormat));
    }

    @Override
    public Object parseObject(String source, ParsePosition pos)
    {
        throw new UnsupportedOperationException("Parsing not supported.");
    }
}