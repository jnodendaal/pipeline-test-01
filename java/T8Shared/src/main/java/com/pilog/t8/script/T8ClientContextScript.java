package com.pilog.t8.script;

/**
 * @author Bouwer du Preez
 */
public interface T8ClientContextScript extends T8Script
{
    // No methods at this stage, but added for future extension purposes.
}
