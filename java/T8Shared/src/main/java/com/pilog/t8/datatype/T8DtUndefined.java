package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.time.T8Date;
import com.pilog.t8.time.T8Timestamp;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtUndefined extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@UNDEFINED";

    private transient T8DefinitionManager context;

    public T8DtUndefined()
    {
    }

    public T8DtUndefined(T8DefinitionManager context)
    {
        this.context = context;
    }

    public T8DtUndefined(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
        this.context = context;
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return null;
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object == null) return JsonValue.NULL;
        else if (object instanceof String)
        {
            return T8DataType.STRING.serialize(object);
        }
        else if (object instanceof Boolean)
        {
            return T8DataType.BOOLEAN.serialize(object);
        }
        else if (object instanceof T8Timestamp)
        {
            return T8DataType.TIMESTAMP.serialize(object);
        }
        else if (object instanceof T8Date)
        {
            return T8DataType.DATE.serialize(object);
        }
        else if (object instanceof Integer)
        {
            return T8DataType.INTEGER.serialize(object);
        }
        else if (object instanceof List)
        {
            return new T8DtList(context).serialize(object);
        }
        else if (object instanceof Map)
        {
            return new T8DtMap(context).serialize(object);
        }
        else if (context != null)
        {
            T8DataType type;

            type = context.getObjectDataType(object);
            if (type != null)
            {
                return type.serialize(object);
            }
            else throw new UnsupportedOperationException(IDENTIFIER + " - Cannot serialize Object : " + object + " - of class : " + object.getClass());
        }
        else throw new UnsupportedOperationException(IDENTIFIER + " - Cannot serialize Object : " + object + " - of class : " + object.getClass());
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if (jsonValue == null || jsonValue == JsonValue.NULL) return null;
        else if (jsonValue.isString())
        {
            return T8DataType.STRING.deserialize(jsonValue);
        }
        else if (jsonValue.isBoolean())
        {
            return T8DataType.BOOLEAN.deserialize(jsonValue);
        }
        else if (jsonValue.isNumber())
        {
            return tryGetNumeric(jsonValue);
        }
        else if (jsonValue.isObject())
        {
            JsonObject object;
            String type;

            // Cast to object.
            object = jsonValue.asObject();
            type = object.getString("type");
            if ((type != null) && (T8IdentifierUtilities.isPublicId(type)))
            {
                // Make sure the context for this data type is set.
                if (context != null)
                {
                    T8DataType dt;

                    // Find the correct data type mathcing the type specification of the json object.
                    dt = context.createDataType(type);
                    if (dt != null)
                    {
                        return dt.deserialize(object);
                    }
                    else throw new RuntimeException("Unsupported json parameter type: " + type);
                }
                else throw new RuntimeException("Cannot deserialize json parameter type when no context is set: " + type);
            }
            else return T8DataType.MAP.deserialize(jsonValue);
        }
        else if (jsonValue.isArray())
        {
            return T8DataType.LIST.deserialize(jsonValue);
        }
        else throw new UnsupportedOperationException(IDENTIFIER + " - Cannot deserialize JSON value : " + jsonValue);
    }

    private Object tryGetNumeric(JsonValue jsonValue)
    {
        try
        {
            // Try as an integer
            return T8DataType.INTEGER.deserialize(jsonValue);
        }
        catch (NumberFormatException nfe)
        {
            try
            {
                // Try as a long
                return T8DataType.TIMESTAMP.deserialize(jsonValue);
            }
            catch (NumberFormatException nfeLong)
            {
                try
                {
                    // Try as double
                    return T8DataType.DOUBLE.deserialize(jsonValue);
                }
                catch (NumberFormatException nfeDouble)
                {
                    // Try as BigDecimal
                    return T8DataType.BIG_DECIMAL.deserialize(jsonValue);
                }
            }
        }
    }
}
