package com.pilog.t8.data;

import java.util.Iterator;

/**
 * This interface extends the default Iterator interface to allow iteration over
 * data element that are retrieved from a data access provider.
 * 
 * @author Bouwer du Preez
 */
public interface T8DataIterator<E> extends Iterator<E>
{
    public T8DataTransaction getDataAccessProvider();
    public void close();
}
