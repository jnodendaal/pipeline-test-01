package com.pilog.t8.definition.gfx.painter.effect.area;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import com.pilog.t8.ui.T8PainterEffect;
import com.pilog.t8.definition.gfx.painter.effect.T8PainterEffectDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8AreaPainterEffectDefinition extends T8PainterEffectDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_EFFECT_AREA";
    public static final String DISPLAY_NAME = "Path Painter Effect";
    public static final String DESCRIPTION = "A painter effect that uses a shape or path as input.";
    public enum Datum {BRUSH_COLOR,
                       BRUSH_STEPS, 
                       EFFECT_WIDTH, 
                       RENDER_INSIDE_SHAPE, 
                       FILL_INSIDE_SHAPE, 
                       SHAPE_MASKED};
    // -------- Definition Meta-Data -------- //
    
    public T8AreaPainterEffectDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.COLOR_HEX, Datum.BRUSH_COLOR.toString(), "Brush Color", "The color of the brush to use when rendering the effect.", "ffffffff"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.BRUSH_STEPS.toString(), "Brush Steps", "The inset on the bottom side of the painted area (in pixels).", 10));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.EFFECT_WIDTH.toString(), "Effect Width", "The inset on the left side of the painted area (in pixels).", 10));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RENDER_INSIDE_SHAPE.toString(), "Render Inside Shape", "Experimental datum.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FILL_INSIDE_SHAPE.toString(), "Fill Inside Shape", "Experimental datum.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SHAPE_MASKED.toString(), "Shape Masked", "Experimental datum.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
 
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8PainterEffect getNewPainterEffectInstance()
    {
        try
        {
            T8PainterEffect painterEffect;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.gfx.painter.effect.area.T8AreaPainterEffect").getConstructor(this.getClass());
            painterEffect = (T8PainterEffect)constructor.newInstance(this);
            return painterEffect;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public Color getBrushColor()
    {
        return T8DefinitionDatumUtilities.getColor((String)getDefinitionDatum(Datum.BRUSH_COLOR.toString()));
    }
    
    public void setBrushColor(Color brushColor)
    {
        setDefinitionDatum(Datum.BRUSH_COLOR.toString(), T8DefinitionDatumUtilities.getColorHex(brushColor));
    }
   
    public Integer getBrushSteps()
    {
        return (Integer)getDefinitionDatum(Datum.BRUSH_STEPS.toString());
    }
    
    public void setBrushSteps(int inset)
    {
        setDefinitionDatum(Datum.BRUSH_STEPS.toString(), inset);
    }
    
    public Integer getEffectWidth()
    {
        return (Integer)getDefinitionDatum(Datum.EFFECT_WIDTH.toString());
    }
    
    public void setEffectWidth(int inset)
    {
        setDefinitionDatum(Datum.EFFECT_WIDTH.toString(), inset);
    }
    
    public boolean getRenderInsideShape()
    {
        Boolean value;
        
        value = (Boolean)getDefinitionDatum(Datum.RENDER_INSIDE_SHAPE.toString());
        return value != null ? value : false;
    }
    
    public void setRenderInsideShape(boolean renderInsideShape)
    {
        setDefinitionDatum(Datum.RENDER_INSIDE_SHAPE.toString(), renderInsideShape);
    }
    
    public boolean getFillInsideShape()
    {
        Boolean value;
        
        value = (Boolean)getDefinitionDatum(Datum.FILL_INSIDE_SHAPE.toString());
        return value != null ? value : false;
    }
    
    public void setFillInsideShape(boolean fillInsideShape)
    {
        setDefinitionDatum(Datum.FILL_INSIDE_SHAPE.toString(), fillInsideShape);
    }
    
    public boolean getShapeMasked()
    {
        Boolean value;
        
        value = (Boolean)getDefinitionDatum(Datum.SHAPE_MASKED.toString());
        return value != null ? value : false;
    }
    
    public void setShapeMasked(boolean shapeMasked)
    {
        setDefinitionDatum(Datum.SHAPE_MASKED.toString(), shapeMasked);
    }
}
