package com.pilog.t8.report;

import com.pilog.t8.time.T8Timestamp;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ReportDetails implements Serializable
{
    private String projectId;
    private String id;
    private String iid;
    private String name;
    private String description;
    private String thumbnailImageId;
    private String userId;
    private String initiatorId;
    private String initiatorIid;
    private T8Timestamp startTime;
    private T8Timestamp endTime;
    private T8Timestamp expirationTime;
    private String fileContextId;
    private String filePath;

    public T8ReportDetails()
    {
    }

    public T8ReportDetails(String projectId, String reportId, String reportIid)
    {
        this.projectId = projectId;
        this.id = reportId;
        this.iid = reportIid;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getIid()
    {
        return iid;
    }

    public void setIid(String iid)
    {
        this.iid = iid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getThumbnailImageId()
    {
        return thumbnailImageId;
    }

    public void setThumbnailImageId(String thumbnailImageId)
    {
        this.thumbnailImageId = thumbnailImageId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getFileContextId()
    {
        return fileContextId;
    }

    public void setFileContextId(String fileContextId)
    {
        this.fileContextId = fileContextId;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId)
    {
        this.initiatorId = initiatorId;
    }

    public String getInitiatorIid()
    {
        return initiatorIid;
    }

    public void setInitiatorIid(String initiatorIid)
    {
        this.initiatorIid = initiatorIid;
    }

    public T8Timestamp getStartTime()
    {
        return startTime;
    }

    public void setStartTime(T8Timestamp startTime)
    {
        this.startTime = startTime;
    }

    public T8Timestamp getEndTime()
    {
        return endTime;
    }

    public void setEndTime(T8Timestamp endTime)
    {
        this.endTime = endTime;
    }

    public T8Timestamp getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(T8Timestamp expirationTime)
    {
        this.expirationTime = expirationTime;
    }
}
