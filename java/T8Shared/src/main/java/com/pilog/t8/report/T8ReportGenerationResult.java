package com.pilog.t8.report;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ReportGenerationResult implements Serializable
{
    private String fileContextId;
    private String filepath;
    private String reportIid;

    public T8ReportGenerationResult(String reportIid, String fileContextId, String filepath)
    {
        this.reportIid = reportIid;
        this.fileContextId = fileContextId;
        this.filepath = filepath;
    }

    public String getFileContextId()
    {
        return fileContextId;
    }

    public void setFileContextId(String fileContextId)
    {
        this.fileContextId = fileContextId;
    }

    public String getFilepath()
    {
        return filepath;
    }

    public void setFilepath(String filepath)
    {
        this.filepath = filepath;
    }

    public String getReportIid()
    {
        return reportIid;
    }

    public void setReportIid(String reportIid)
    {
        this.reportIid = reportIid;
    }

    @Override
    public String toString()
    {
        return "T8ReportGenerationResult{" + "fileContextId=" + fileContextId + ", filepath=" + filepath + ", reportIid=" + reportIid + '}';
    }
}
