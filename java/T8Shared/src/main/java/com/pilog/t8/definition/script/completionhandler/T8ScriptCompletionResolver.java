package com.pilog.t8.definition.script.completionhandler;

import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ScriptCompletionResolver
{
    public List<String> getMatchPatterns();
    public Map<String, String> getCodeCompletionSuggestions(final String matchedText);
}
