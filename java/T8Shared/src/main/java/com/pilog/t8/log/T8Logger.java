package com.pilog.t8.log;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class  T8Logger
{
    public enum Level
    {
        PERFORMANCE,
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        FATAL
    }

    protected final String callerName;

    T8Logger(String callerName)
    {
        this.callerName = callerName;
    }

    public void log(String message)
    {
        log(Level.DEBUG,message);
    }

    public void log(LogMessage message)
    {
        log(Level.DEBUG, message);
    }

    public void log(Throwable exception)
    {
        log(Level.ERROR, exception.getLocalizedMessage(), exception);
    }

    public void log(String message, Throwable exception)
    {
        log(Level.ERROR, message, exception);
    }

    public void log(LogMessage message, Throwable exception)
    {
        log(Level.ERROR, message, exception);
    }

    public abstract void log(Level level, String message);

    public abstract void log(Level level, LogMessage message);

    public abstract void log(Level level, String message, Throwable exception);

    public abstract void log(Level level, LogMessage message, Throwable exception);
}
