package com.pilog.t8.definition.flow.node.gateway;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowForkConditionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_FORK_CONDITION";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_FORK_CONDITION";
    public static final String IDENTIFIER_PREFIX = "FFC_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Fork Condition";
    public static final String DESCRIPTION = "A condition evaluated within the context of a flow gateway in order to determine the applicability of a specific flow path.";
    public enum Datum {CONDITION_EXPRESSION,
                       NODE_IDENTIFIER};
    // -------- Definition Meta-Data -------- //
    
    public T8FlowForkConditionDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NODE_IDENTIFIER.toString(), "Node", "The identifier of the node to which this fork condition applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression to be evaluated."));
        return datumTypes;
    }
    
    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.NODE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8WorkFlowNodeDefinition nodeDefinition;
            
            nodeDefinition = (T8WorkFlowNodeDefinition)getParentDefinition();
            if (nodeDefinition != null)
            {
                T8WorkFlowDefinition flowDefinition;
                List<String> nodeIdentifiers;
                
                flowDefinition = (T8WorkFlowDefinition)nodeDefinition.getParentDefinition();
                nodeIdentifiers = flowDefinition.getChildNodeIdentifiers(nodeDefinition.getIdentifier());

                return createStringOptions(nodeIdentifiers);
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else return new ArrayList<T8DefinitionDatumOption>();
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }
    
    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }
    
    public String getNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.NODE_IDENTIFIER.toString());
    }
    
    public void setNodeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.NODE_IDENTIFIER.toString(), identifier);
    }
}
