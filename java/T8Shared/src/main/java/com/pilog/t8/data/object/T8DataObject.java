package com.pilog.t8.data.object;

import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectFieldDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataObject implements Serializable
{
    private final String id;
    private final String keyFieldId;
    private final TreeMap<String, Object> fieldValues;
    private final Map<String, Integer> fieldDisplayPriority;
    private final Map<String, Integer> fieldDisplaySequence;
    private final Map<String, String> fieldDisplayNames;
    private final Map<String, String> fieldDisplayDescriptions;
    private T8DataObjectState state;

    public static final String ID_FIELD = "$ID";

    public T8DataObject(T8DataObjectDefinition definition, String iid)
    {
        this.fieldValues = new TreeMap<>();
        this.id = definition.getIdentifier();
        this.keyFieldId = definition.getKeyFieldId();
        this.fieldDisplaySequence = new HashMap<>();
        this.fieldDisplayPriority = new HashMap<>();
        this.fieldDisplayNames = new HashMap<>();
        this.fieldDisplayDescriptions = new HashMap<>();

        for (T8DataObjectFieldDefinition fieldDefinition : definition.getFieldDefinitions())
        {
            String displayName;
            String displayDescription;
            Integer sequence;
            Integer priority;
            String fieldId;

            fieldId = fieldDefinition.getIdentifier();

            sequence = fieldDefinition.getDisplaySequence();
            if (sequence != null) fieldDisplaySequence.put(fieldId, sequence);

            priority = fieldDefinition.getDisplayPriority();
            if (priority != null) fieldDisplayPriority.put(fieldId, priority);

            displayName = fieldDefinition.getDisplayName();
            if (displayName != null) fieldDisplayNames.put(fieldId, displayName);

            displayDescription = fieldDefinition.getDisplayDescription();
            if (displayDescription != null) fieldDisplayDescriptions.put(fieldId, displayDescription);
        }

        setIid(iid);
    }

    /**
     * Only used by copy method.
     */
    private T8DataObject(String id, String keyFieldId)
    {
        this.fieldValues = new TreeMap<>();
        this.id = id;
        this.keyFieldId = keyFieldId;
        this.fieldDisplaySequence = new HashMap<>();
        this.fieldDisplayPriority = new HashMap<>();
        this.fieldDisplayNames = new HashMap<>();
        this.fieldDisplayDescriptions = new HashMap<>();
    }

    public final String getKey()
    {
        return (String)getFieldValue(keyFieldId);
    }

    public final String getIid()
    {
        return (String)getFieldValue(keyFieldId);
    }

    public final void setIid(String iid)
    {
        setFieldValue(keyFieldId, iid);
    }

    public String getId()
    {
        return id;
    }

    /**
     * Returns the id of this data object.
     * @return The id of this data object.
     * @see getId()
     * @deprecated
     */
    @Deprecated
    public String getIdentifier()
    {
        return getId();
    }

    public T8DataObjectState getState()
    {
        return state;
    }

    public void setState(T8DataObjectState state)
    {
        this.state = state;
    }

    public boolean containsField(String fieldId)
    {
        return fieldValues.containsKey(fieldId);
    }

    public List<String> getFieldIds()
    {
        return new ArrayList<>(fieldValues.keySet());
    }

    public Object getFieldValue(String fieldId)
    {
        if (fieldId.startsWith(id))
        {
            return fieldValues.get(fieldId);
        }
        else if (fieldId.startsWith(T8Definition.getPublicIdPrefix()) || fieldId.startsWith(T8Definition.getPrivateIdPrefix()))
        {
            throw new IllegalArgumentException("Field id " + fieldId + " does not use the correct data object id: " + id);
        }
        else
        {
            return fieldValues.get(id + fieldId);
        }
    }

    public Map<String, Object> getFieldValues()
    {
        return new TreeMap<>(fieldValues);
    }

    public Object setFieldValue(String fieldId, Object fieldValue)
    {
        if (fieldId.startsWith(id))
        {
            return fieldValues.put(fieldId, fieldValue);
        }
        else if (fieldId.startsWith(T8Definition.getPublicIdPrefix()) || fieldId.startsWith(T8Definition.getPrivateIdPrefix()))
        {
            throw new IllegalArgumentException("Field id " + fieldId + " does not use the correct data object id: " + id);
        }
        else
        {
            return fieldValues.put(id + fieldId, fieldValue);
        }
    }

    public void setFieldValues(Map<String, Object> fieldValues)
    {
        this.fieldValues.clear();
        if (fieldValues != null)
        {
            for (String fieldId : fieldValues.keySet())
            {
                setFieldValue(fieldId, fieldValues.get(fieldId));
            }
        }
    }

    public void setMatchingFieldValues(Map<String, Object> fieldValues)
    {
        List<String> fieldIds;

        fieldIds = getFieldIds();
        for (String fieldId : fieldValues.keySet())
        {
            if (fieldIds.contains(fieldId))
            {
                this.fieldValues.put(fieldId, fieldValues.get(fieldId));
            }
        }
    }

    public Object clearField(String fieldId)
    {
        return fieldValues.remove(fieldId);
    }

    public void clearAllFields()
    {
        fieldValues.clear();
    }

    public List<String> getNonEmptyFieldIds()
    {
        return new ArrayList<>(fieldValues.keySet());
    }

    public Integer getFieldDisplaySequence(String fieldId)
    {
        return fieldDisplaySequence.get(fieldId);
    }

    public Integer getFieldDisplayPriority(String fieldId)
    {
        return fieldDisplayPriority.get(fieldId);
    }

    public String getFieldDisplayName(String fieldId)
    {
        return fieldDisplayNames.get(fieldId);
    }

    public String getFieldDisplayDescription(String fieldId)
    {
        return fieldDisplayDescriptions.get(fieldId);
    }

    public List<String> getDisplayFieldIds(Integer priorityFilter)
    {
        List<String> fieldIds;
        List<Pair<String, Integer>> fieldSequence;

        fieldSequence = new ArrayList<>();
        for (String fieldId : fieldDisplaySequence.keySet())
        {
            fieldSequence.add(new Pair<>(fieldId, fieldDisplaySequence.get(fieldId)));
        }

        // Sort the pairs according to sequence value.
        fieldSequence.sort((Pair<String, Integer> pair1, Pair<String, Integer> pair2) ->
        {
            return pair1.getValue2().compareTo(pair2.getValue2());
        });

        // Add the ordererd field ids to the output list.
        fieldIds = new ArrayList<>();
        for (Pair<String, Integer> pair : fieldSequence)
        {
            String fieldId;

            fieldId = pair.getValue1();
            if (priorityFilter != null)
            {
                Integer priority;

                priority = fieldDisplayPriority.get(fieldId);
                if ((priority != null) && (priority.compareTo(priorityFilter) <= 0))
                {
                    fieldIds.add(fieldId);
                }
            }
            else
            {
                fieldIds.add(fieldId);
            }
        }

        // Return the list of field ids sorted by sequence and filtered according to priority.
        return fieldIds;
    }

    public T8DataObject copy()
    {
        T8DataObject newObject;

        newObject = new T8DataObject(id, keyFieldId);
        newObject.setFieldValues(fieldValues);
        return newObject;
    }

    @Override
    public String toString()
    {
        StringBuffer string;

        string = new StringBuffer(getId());
        string.append(getFieldValues());
        return string.toString();
    }
}
