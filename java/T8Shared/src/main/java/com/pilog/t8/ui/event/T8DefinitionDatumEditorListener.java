package com.pilog.t8.ui.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionDatumEditorListener extends EventListener
{
    public void definitionLinkActivated(T8DefinitionLinkEvent event);
}
