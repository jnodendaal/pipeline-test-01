package com.pilog.t8.definition.graph;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8GraphManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public abstract class T8GraphDefinition extends T8Definition
{
    protected enum Datum {NODE_DEFINITIONS, EDGE_DEFINITIONS, START_NODE_IDENTIFIER};

    public T8GraphDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.START_NODE_IDENTIFIER.toString(), "Start Node", "The logical first node of the graph (if one exists)."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.NODE_DEFINITIONS.toString(), "Nodes", "All of the nodes contained by the graph."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EDGE_DEFINITIONS.toString(), "Edges", "All of the edges contained by the graph."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8GraphDefinition.Datum.START_NODE_IDENTIFIER.toString().equals(datumIdentifier)) return createStringOptions(getNodeIdentifiers());
        else return null;
    }

    public ArrayList<T8DefinitionDatumOption> getNodeDatumOptions(T8DefinitionContext definitionContext) throws Exception
    {
        return getDatumOptions(definitionContext, Datum.NODE_DEFINITIONS.toString());
    }

    public String getNodeDatumIdentifier()
    {
        return Datum.NODE_DEFINITIONS.toString();
    }

    public String getEdgeDatumIdentifier()
    {
        return Datum.EDGE_DEFINITIONS.toString();
    }

    public ArrayList<T8GraphNodeDefinition> getNodeDefinitions()
    {
        if (getDefinitionDatum(Datum.NODE_DEFINITIONS.toString()) == null) setDefinitionDatum(Datum.NODE_DEFINITIONS.toString(), new ArrayList<T8Definition>());
        return (ArrayList<T8GraphNodeDefinition>)getDefinitionDatum(Datum.NODE_DEFINITIONS.toString());
    }

    public T8Definition getStartNodeDefinition()
    {
        return getNodeDefinition(getStartNodeIdentifier());
    }

    public String getStartNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.START_NODE_IDENTIFIER.toString());
    }

    public void setStartNodeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.START_NODE_IDENTIFIER.toString(), identifier);
    }

    public void setNodeDefinitions(ArrayList<T8Definition> definitions)
    {
        setDefinitionDatum(Datum.NODE_DEFINITIONS.toString(), definitions);
    }

    public void addNodeDefinition(T8GraphNodeDefinition definition)
    {
        addSubDefinition(Datum.NODE_DEFINITIONS.toString(), definition);
    }

    public T8GraphNodeDefinition getNodeDefinition(String identifier)
    {
        if (identifier != null)
        {
            for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
            {
                if (nodeDefinition.getIdentifier().equals(identifier)) return nodeDefinition;
            }
        }

        return null;
    }

    public List<String> getNodeIdentifiers()
    {
        List<String> nodeIdentifiers;

        nodeIdentifiers = new ArrayList<String>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            nodeIdentifiers.add(nodeDefinition.getIdentifier());
        }

        return nodeIdentifiers;
    }

    public ArrayList<T8GraphEdgeDefinition> getEdgeDefinitions()
    {
        return (ArrayList<T8GraphEdgeDefinition>)getDefinitionDatum(Datum.EDGE_DEFINITIONS.toString());
    }

    public void setEdgeDefinitions(ArrayList<T8GraphEdgeDefinition> edgeDefinitions)
    {
        setDefinitionDatum(Datum.EDGE_DEFINITIONS.toString(), edgeDefinitions);
    }

    public void addEdgeDefinition(T8GraphEdgeDefinition edgeDefinition)
    {
        addSubDefinition(Datum.EDGE_DEFINITIONS.toString(), edgeDefinition);
    }

    public void addEdgeDefinition(String parentIdentifier, String childIdentifier)
    {
        addEdgeDefinition(createEdgeDefinition(parentIdentifier, childIdentifier));
    }

    public boolean removeNodeDefinition(String nodeIdentifier)
    {
        T8Definition nodeDefinition;

        nodeDefinition = getNodeDefinition(nodeIdentifier);
        if (nodeDefinition != null) return removeSubDefinition(nodeDefinition);
        else return false;
    }

    public boolean removeNodeDefinition(T8GraphNodeDefinition nodeDefinition)
    {
        return removeSubDefinition(nodeDefinition);
    }

    public void removeLooseNodes()
    {
        ArrayList<T8GraphNodeDefinition> nodeDefinitions;

        nodeDefinitions = new ArrayList<T8GraphNodeDefinition>(getNodeDefinitions());
        for (T8GraphNodeDefinition nodeDefinition : nodeDefinitions)
        {
            List<T8GraphEdgeDefinition> edgeDefinitions;

            edgeDefinitions = getEdgeDefinitions(nodeDefinition.getIdentifier());
            if (edgeDefinitions.size() == 0) removeNodeDefinition(nodeDefinition);
        }
    }

    public void removeLooseEdges()
    {
        ArrayList<T8GraphEdgeDefinition> edgeDefinitions;

        edgeDefinitions = new ArrayList<T8GraphEdgeDefinition>(getEdgeDefinitions());
        for (T8GraphEdgeDefinition edgeDefinition : edgeDefinitions)
        {
            T8Definition parentNodeDefinition;
            T8Definition childNodeDefinition;

            parentNodeDefinition = getNodeDefinition(edgeDefinition.getParentNodeIdentifier());
            childNodeDefinition = getNodeDefinition(edgeDefinition.getChildNodeIdentifier());

            if ((parentNodeDefinition == null) || (childNodeDefinition == null))
            {
                removeEdgeDefinition(edgeDefinition);
            }
        }
    }

    public List<T8GraphEdgeDefinition> getEdgeDefinitions(String nodeIdentifier)
    {
        ArrayList<T8GraphEdgeDefinition> edgeDefinitions;

        edgeDefinitions = new ArrayList<T8GraphEdgeDefinition>();
        for (T8GraphEdgeDefinition edgeDefinition : getEdgeDefinitions())
        {
            if ((nodeIdentifier.equals(edgeDefinition.getParentNodeIdentifier())) || (nodeIdentifier.equals(edgeDefinition.getChildNodeIdentifier())))
            {
                edgeDefinitions.add(edgeDefinition);
            }
        }

        return edgeDefinitions;
    }

    public boolean removeEdgeDefinition(T8GraphEdgeDefinition edgeDefinition)
    {
        return removeSubDefinition(edgeDefinition);
    }

    public void removeEdgeDefinitions(String nodeIdentifier)
    {
        Iterator<T8GraphEdgeDefinition> edgeIterator;

        edgeIterator = getEdgeDefinitions().iterator();
        while (edgeIterator.hasNext())
        {
            T8GraphEdgeDefinition edgeDefinition;

            edgeDefinition = edgeIterator.next();
            if (edgeDefinition.getParentNodeIdentifier().equals(nodeIdentifier))
            {
                removeEdgeDefinition(edgeDefinition);
            }
            else if (edgeDefinition.getChildNodeIdentifier().equals(nodeIdentifier))
            {
                removeEdgeDefinition(edgeDefinition);
            }
        }
    }

    public void removeEdgeDefinitions(String node1Identifier, String node2Identifier, boolean bothDirections)
    {
        Iterator<T8GraphEdgeDefinition> edgeIterator;

        edgeIterator = getEdgeDefinitions().iterator();
        while (edgeIterator.hasNext())
        {
            T8GraphEdgeDefinition edgeDefinition;
            String parentIdentifier;
            String childIdentifier;

            edgeDefinition = edgeIterator.next();
            parentIdentifier = edgeDefinition.getParentNodeIdentifier();
            childIdentifier = edgeDefinition.getChildNodeIdentifier();
            if ((parentIdentifier.equals(node1Identifier)) && (childIdentifier.equals(node2Identifier)))
            {
                removeEdgeDefinition(edgeDefinition);
            }
            else if ((bothDirections) && (parentIdentifier.equals(node2Identifier)) && (childIdentifier.equals(node1Identifier)))
            {
                removeEdgeDefinition(edgeDefinition);
            }
        }
    }

    public ArrayList<String> getParentNodeIdentifiers(String nodeIdentifier)
    {
        ArrayList<String> parentNodeIdentifiers;

        parentNodeIdentifiers = new ArrayList<String>();
        for (T8GraphEdgeDefinition edgeDefinition : getEdgeDefinitions())
        {
            if (Objects.equals(edgeDefinition.getChildNodeIdentifier(), nodeIdentifier))
            {
                parentNodeIdentifiers.add(edgeDefinition.getParentNodeIdentifier());
            }
        }

        return parentNodeIdentifiers;
    }

    public ArrayList<String> getChildNodeIdentifiers(String nodeIdentifier)
    {
        ArrayList<String> childNodeIdentifiers;

        childNodeIdentifiers = new ArrayList<String>();
        for (T8GraphEdgeDefinition edgeDefinition : getEdgeDefinitions())
        {
            if (Objects.equals(edgeDefinition.getParentNodeIdentifier(), nodeIdentifier))
            {
                childNodeIdentifiers.add(edgeDefinition.getChildNodeIdentifier());
            }
        }

        return childNodeIdentifiers;
    }

    public abstract T8GraphManager getGraphManager(T8Context context) throws Exception;
    public abstract T8GraphEdgeDefinition createEdgeDefinition(String parentIdentifier, String childIdentifier);
}
