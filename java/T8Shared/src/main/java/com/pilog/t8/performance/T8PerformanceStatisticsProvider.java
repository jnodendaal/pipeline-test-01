package com.pilog.t8.performance;

/**
 * @author Bouwer du Preez
 */
public interface T8PerformanceStatisticsProvider
{
    public void setPerformanceStatisticsEnabled(boolean enabled);
    public void setPerformanceStatistics(T8PerformanceStatistics statistics);
    public T8PerformanceStatistics getPerformanceStatistics();
}
