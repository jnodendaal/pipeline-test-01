package com.pilog.t8.definition.user.password;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public abstract class T8PasswordPolicyRuleDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PASSWORD_POLICY_RULE";
    public static final String STORAGE_PATH = "/password_policy_rules";
    public static final String IDENTIFIER_PREFIX = "PASSWORD_RULE_";

    public enum Datum {FAILRE_MESSAGE};
    // -------- Definition Meta-Data -------- //

    public T8PasswordPolicyRuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.FAILRE_MESSAGE.toString(), "Failure Message", "The failure message that will be displayed to the user if validation fails."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {}

    /**
     * Returns a new instance of the {@code T8PasswordRuleValidator} for the
     * current {@code T8PasswordPolicyRule} implementation. The validator should
     * always only be available on the server side to improve
     * security-by-abstraction.
     *
     * @return The {@code T8PasswordRuleValidator} to be used to validate a
     *      given password
     */
    public abstract T8PasswordRuleValidator getValidator();

    public void setFailureMessage(String text)
    {
        setDefinitionDatum(Datum.FAILRE_MESSAGE, text);
    }

    public String getFailureMessage()
    {
        return getDefinitionDatum(Datum.FAILRE_MESSAGE);
    }
}
