package com.pilog.t8.definition;

import java.awt.Color;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDatumUtilities
{
    public static Color getColor(String colorHex)
    {
        if (colorHex != null)
        {
            long colorLong;
            int red;
            int green;
            int blue;
            int alpha;

            // Get the Hex representation of the color and parse a long value from it.  Parsing to int may mess things up, since the hex is unsigned and int is not.
            colorLong = Long.parseLong(colorHex, 16);

            // Get the individual color components from the long value.  The long cannot be cast to int for sRGB value since it may be out of range (int is signed and we need unsigned).
            alpha = (int)((colorLong >> 24) & 0x000000ff);
            red = (int)((colorLong >> 16) & 0x000000ff);
            green = (int)((colorLong >> 8) & 0x000000ff);
            blue = (int)((colorLong) & 0x000000ff);

            return new Color(red, green, blue, alpha);
        }
        else return null;
    }
    
    public static String getColorHex(Color color)
    {
        return color != null ? Integer.toHexString(color.getRGB()) : null;
    }
}
