package com.pilog.t8.definition.data.source;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataSourceFieldDefinition extends T8DataFieldDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_SOURCE_FIELD";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Source Field";
    public static final String DESCRIPTION = "A field contained by a data source.";
    public enum Datum {SOURCE_DATA_TYPE};
    // -------- Definition Meta-Data -------- //

    private T8DataType sourceDataType;

    public T8DataSourceFieldDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataSourceFieldDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, T8DataType dataType)
    {
        super(identifier, sourceIdentifier, key, required, dataType);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        // Get the super class datum types.
        datumTypes = super.getDatumTypes();

        // Remove the SOURCE_IDENTIFIER datum type.
        for (T8DefinitionDatumType datumType : datumTypes)
        {
            if (datumType.getIdentifier().equals(T8DataFieldDefinition.Datum.SOURCE_IDENTIFIER.toString()))
            {
                datumTypes.remove(datumType);
                break;
            }
        }

        // Add a new SOURCE_IDENTIFIER type that uses a STRING data type and not DEFINITION_IDENTIFIER.
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, T8DataFieldDefinition.Datum.SOURCE_IDENTIFIER.toString(), "Source", "The source from which this field gets its value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SOURCE_DATA_TYPE.toString(), "Source Data Type", "The data type of the source from which this field gets its value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String dataTypeString;

        super.initializeDefinition(context, inputParameters, configurationSettings);

        dataTypeString = getSourceDataTypeString();
        if (dataTypeString != null)
        {
            T8DefinitionManager definitionManager;

            definitionManager = context.getServerContext().getDefinitionManager();
            sourceDataType = definitionManager.createDataType(dataTypeString);
        }
        else sourceDataType = null;
    }

    public T8DataType getSourceDataType()
    {
        return sourceDataType;
    }

    public String getSourceDataTypeString()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_DATA_TYPE.toString());
    }

    public void setSourceDataTypeString(String dataTypeString)
    {
        setDefinitionDatum(Datum.SOURCE_DATA_TYPE.toString(), dataTypeString);
    }
}
