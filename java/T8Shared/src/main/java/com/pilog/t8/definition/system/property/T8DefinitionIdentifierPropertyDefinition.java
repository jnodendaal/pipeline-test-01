package com.pilog.t8.definition.system.property;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionIdentifierPropertyDefinition extends T8SystemPropertyDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_PROPERTY_DEFINITION_IDENTIFIER";
    public static final String DISPLAY_NAME = "Definition Identifier Type System Property";
    public static final String DESCRIPTION = "A System Property with a Definition Identifier data type.";
    public enum Datum { DEFINITION_GROUP_IDENTIFIER,
                        PROPERTY_VALUE};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionIdentifierPropertyDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_GROUP_IDENTIFIER, Datum.DEFINITION_GROUP_IDENTIFIER.toString(), "Definition Group", "The definition group from which the value of this property may be selected."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROPERTY_VALUE.toString(), "Definition Identifier", "The property value (definition identifier)."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DEFINITION_GROUP_IDENTIFIER.toString().equals(datumIdentifier)) return createStringOptions(definitionContext.getDefinitionGroupIdentifiers());
        else if (Datum.PROPERTY_VALUE.toString().equals(datumIdentifier))
        {
            String groupIdentifier;

            groupIdentifier = getDefinitionGroupIdentifier();
            if (groupIdentifier != null) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), groupIdentifier));
            else return new ArrayList<>();
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getDefinitionGroupIdentifier()
    {
        return getDefinitionDatum(Datum.DEFINITION_GROUP_IDENTIFIER);
    }

    public void setDefinitionGroupIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DEFINITION_GROUP_IDENTIFIER, identifier);
    }

    @Override
    public String getPropertyValue()
    {
        return getDefinitionDatum(Datum.PROPERTY_VALUE);
    }

    public void setPropertyValue(String value)
    {
        setDefinitionDatum(Datum.PROPERTY_VALUE, value);
    }
}
