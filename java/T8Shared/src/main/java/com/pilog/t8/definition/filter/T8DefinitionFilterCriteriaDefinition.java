package com.pilog.t8.definition.filter;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.filter.T8DefinitionFilterClause;
import com.pilog.t8.definition.filter.T8DefinitionFilterCriteria;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFilterCriteriaDefinition extends T8DefinitionFilterClauseDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_FILTER_CRITERIA";
    public static final String IDENTIFIER_PREFIX = "FILTER_CRITERIA_";
    public static final String DISPLAY_NAME = "Definition Filter Criteria";
    public static final String DESCRIPTION = "An object that specifies filter criteria to be used for filtering T8 Definitions.";
    private enum Datum {FILTER_CLAUSE_DEFINITIONS};
    // -------- Definition Meta-Data -------- //
    
    public T8DefinitionFilterCriteriaDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FILTER_CLAUSE_DEFINITIONS.toString(), "Filter Clauses", "The filter clauses contained by this criteria definition."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILTER_CLAUSE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData((T8DefinitionFilterClauseDefinition.GROUP_IDENTIFIER)));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8DefinitionFilterClause getNewDefinitionFilterClauseInstance(T8SessionContext sessionContext, Map<String, Object> filterParameters)
    {
        return getNewDefinitionFilterCriteriaInstance(sessionContext, filterParameters);
    }

    public T8DefinitionFilterCriteria getNewDefinitionFilterCriteriaInstance(T8SessionContext sessionContext, Map<String, Object> filterParameters)
    {
        T8DefinitionFilterCriteria filterCriteria;
        
        filterCriteria = new T8DefinitionFilterCriteria(this);
        for (T8DefinitionFilterClauseDefinition filterClauseDefinition : getFilterClauseDefinitions())
        {
            filterCriteria.addFilterClause(filterClauseDefinition.getConjunction(), filterClauseDefinition.getNewDefinitionFilterClauseInstance(sessionContext, filterParameters));
        }
        
        return filterCriteria;
    }
    
    public ArrayList<T8DefinitionFilterClauseDefinition> getFilterClauseDefinitions()
    {
        return (ArrayList<T8DefinitionFilterClauseDefinition>)getDefinitionDatum(Datum.FILTER_CLAUSE_DEFINITIONS.toString());
    }

    public void setFilterClauseDefinitions(ArrayList<T8DefinitionFilterClauseDefinition> definitions)
    {
        getFilterClauseDefinitions().clear();
        getFilterClauseDefinitions().addAll(definitions);
    }
}
