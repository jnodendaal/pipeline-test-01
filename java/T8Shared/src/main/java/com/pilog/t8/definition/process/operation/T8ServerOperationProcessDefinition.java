package com.pilog.t8.definition.process.operation;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.process.T8Process;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerOperationProcessDefinition extends T8ProcessDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SERVER_PROCESS_OPERATION";
    public static final String DISPLAY_NAME = "Server Operation Process";
    public static final String DESCRIPTION = "A long running server-side process.";
    public enum Datum
    {
        OPERATION_IDENTIFIER,
        INPUT_PARAMETER_MAPPING,
        OUTPUT_PARAMETER_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public T8ServerOperationProcessDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.OPERATION_IDENTIFIER.toString(), "Operation", "The operation that will be invoked when this process is executed."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Process Input to Operation Input", "A mapping of Process input parameters to the corresponding Operation input parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Operation Output to Process Ouput", "A mapping of Operation output parameters to the corresponding Process output parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.OPERATION_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            operationId = getOperationIdentifier();
            if (operationId != null)
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> processParameterDefinitions;
                    List<T8DataParameterDefinition> operationParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    processParameterDefinitions = getInputParameterDefinitions();
                    operationParameterDefinitions = operationDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((processParameterDefinitions != null) && (operationParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition processParameterDefinition : processParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition operationParameterDefinition : operationParameterDefinitions)
                            {
                                identifierList.add(operationParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(processParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            operationId = getOperationIdentifier();
            if (operationId != null)
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> processParameterDefinitions;
                    List<T8DataParameterDefinition> operationParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    processParameterDefinitions = getOutputParameterDefinitions();
                    operationParameterDefinitions = operationDefinition.getOutputParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((processParameterDefinitions != null) && (operationParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition taskParameterDefinition : operationParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition flowParameterDefinition : processParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(taskParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Process getNewProcessInstance(T8Context context, String processIid)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.process.operation.T8ServerOperationProcess").getConstructor(T8Context.class, T8ServerOperationProcessDefinition.class, String.class);
            return (T8Process)constructor.newInstance(context, this, processIid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getOperationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString());
    }

    public void setOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> inputParameterMapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), inputParameterMapping);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOutputParameterMapping(Map<String, String> outputParameterMapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString(), outputParameterMapping);
    }
}
