package com.pilog.t8.definition.patch;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionStringPatch
{
    private String prePattern;
    private String targetPattern;
    private String postPattern;
    private String replacementString;

    public T8DefinitionStringPatch(String prePattern, String targetPattern, String postPattern, String replacementString)
    {
        this.prePattern = prePattern;
        this.targetPattern = targetPattern;
        this.postPattern = postPattern;
        this.replacementString = replacementString;
    }

    public String getPrePattern()
    {
        return prePattern;
    }

    public void setPrePattern(String prePattern)
    {
        this.prePattern = prePattern;
    }

    public String getTargetPattern()
    {
        return targetPattern;
    }

    public void setTargetPattern(String targetPattern)
    {
        this.targetPattern = targetPattern;
    }

    public String getPostPattern()
    {
        return postPattern;
    }

    public void setPostPattern(String postPattern)
    {
        this.postPattern = postPattern;
    }

    public String getReplacementString()
    {
        return replacementString;
    }

    public void setReplacementString(String replacementString)
    {
        this.replacementString = replacementString;
    }
}
