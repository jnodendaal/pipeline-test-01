package com.pilog.t8.performance;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultThreadPerformanceStatistics implements T8ThreadPerformanceStatistics, Serializable
{
    private final String identifier;
    private final Map<String, T8PerformanceEventStatistics> eventStatistics;
    private final Map<String, T8ExecutionPerformanceStatistics> executionStatistics;
    private T8ExecutionPerformanceStatistics currentlyStartedExecution;

    public T8DefaultThreadPerformanceStatistics(String identifier)
    {
        this.identifier = identifier;
        this.currentlyStartedExecution = null;
        this.eventStatistics = new TreeMap<String, T8PerformanceEventStatistics>();
        this.executionStatistics = new TreeMap<String, T8ExecutionPerformanceStatistics>();
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public void reset()
    {
        executionStatistics.clear();
    }

    @Override
    public void logEvent(String eventIdentifier)
    {
        T8PerformanceEventStatistics stats;

        stats = eventStatistics.get(eventIdentifier);
        if (stats == null)
        {
            stats = new T8DefaultPerformanceEventStatistics(eventIdentifier);
            eventStatistics.put(eventIdentifier, stats);
        }

        stats.logEvent();
    }

    @Override
    public void logExecutionStart(String executionIdentifier)
    {
        if (currentlyStartedExecution != null)
        {
            if (currentlyStartedExecution.getIdentifier().equals(executionIdentifier))
            {
                currentlyStartedExecution.logStart();
            }
            else
            {
                currentlyStartedExecution.logSubExecutionStart(executionIdentifier);
            }
        }
        else
        {
            currentlyStartedExecution = executionStatistics.get(executionIdentifier);
            if (currentlyStartedExecution == null)
            {
                currentlyStartedExecution = new T8DefaultExecutionPerformanceStatistics(executionIdentifier, null);
                executionStatistics.put(executionIdentifier, currentlyStartedExecution);
            }

            currentlyStartedExecution.logStart();
        }
    }

    @Override
    public void logExecutionEnd(String executionIdentifier)
    {
        if (currentlyStartedExecution != null)
        {
            if (currentlyStartedExecution.getIdentifier().equals(executionIdentifier))
            {
                currentlyStartedExecution.logEnd();
                if (!currentlyStartedExecution.hasInstances()) currentlyStartedExecution = null;
            }
            else
            {
                currentlyStartedExecution.logSubExecutionEnd(executionIdentifier);
            }
        }
        else throw new RuntimeException("Invalid attempt to log end of execution that has not started: " + executionIdentifier);
    }

    public T8ExecutionPerformanceStatistics findExecutionStatistics(String identifier)
    {
        for (T8ExecutionPerformanceStatistics stats : executionStatistics.values())
        {
            if (stats.getIdentifier().equals(identifier))
            {
                return stats;
            }
            else
            {
                T8ExecutionPerformanceStatistics subStats;

                subStats = stats.findExecutionStatistics(identifier);
                if (subStats != null) return subStats;
            }
        }

        return null;
    }

    @Override
    public void printStatistics(OutputStream stream)
    {
        PrintWriter writer;

        writer = new PrintWriter(stream);
        writer.println(identifier);
        writer.flush();

        // Print all event statistics.
        for (T8PerformanceEventStatistics stats : eventStatistics.values())
        {
            stats.printStatistics(stream);
        }

        // Print all execution statistics.
        for (T8ExecutionPerformanceStatistics stats : executionStatistics.values())
        {
            stats.printStatistics(stream);
        }
    }
}
