package com.pilog.t8.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityStructure implements Serializable
{
    private List<T8DataEntityStructureNode> baseNodes;
    
    public T8DataEntityStructure()
    {
        this.baseNodes = new ArrayList<T8DataEntityStructureNode>();
    }
    
    public void clear()
    {
        baseNodes.clear();
    }
    
    public List<String> getBaseEntityIdentifierList()
    {
        List<String> identifierList;
        
        identifierList = new ArrayList<String>();
        for (T8DataEntityStructureNode node : baseNodes)
        {
            identifierList.add(node.getID());
        }
        
        return identifierList;
    }
    
    public List<T8DataEntityStructureNode> getBaseEntityNodes()
    {
        return new ArrayList<T8DataEntityStructureNode>(baseNodes);
    }
    
    public T8DataEntityStructureNode getBaseEntityNode(String entityIdentifier)
    {
        for (T8DataEntityStructureNode node : baseNodes)
        {
            if (entityIdentifier.equals(node.getID()))
            {
                return node;
            }
        }
        
        return null;
    }
    
    public void addBaseEntityNode(T8DataEntityStructureNode node)
    {
        baseNodes.add(node);
    }
    
    public boolean removeBaseEntityNode(T8DataEntityStructureNode node)
    {
        return baseNodes.remove(node);
    }
    
    public boolean containsNode(String entityIdentifier)
    {
        return getNode(entityIdentifier) != null;
    }
    
    public List<String> getEntityIdentifierList()
    {
        List<T8DataEntityStructureNode> nodes;
        
        nodes = getNodes();
        if (nodes != null)
        {
            List<String> idList;
            
            idList = new ArrayList<String>();
            for (T8DataEntityStructureNode node : nodes)
            {
                idList.add(node.getID());
            }
            
            return idList;
        }
        else return null;
    }
    
    /**
     * This method returns the list of entity ID's from the root to the
     * specified node.
     * @param entityIdentifier The entity ID for which to retrieve the 
     * path ID set.
     * @return The path set of entity ID's to which the specified entity
     * belongs.
     */
    public List<String> getEntityPathIdentifierList(String entityIdentifier)
    {
        T8DataEntityStructureNode node;
        List<String> idList;
        
        idList = new ArrayList<String>();
        node = getNode(entityIdentifier);
        if (node != null)
        {
            for (T8DataEntityStructureNode pathNode : node.getPathNodes())
            {
                idList.add(pathNode.getID());
            }
        }
        
        return idList;
    }
    
    /**
     * This method returns the list of entity ID's that are descendants of
     * the specified organization node.
     * @param entityIdentifier The entity ID for which to retrieve the 
     * descendant ID set.
     * @return The set of entity ID's that are descendants of the 
     * specified entity.
     */
    public List<String> getEntityDescendantIdentifierList(String entityIdentifier)
    {
        T8DataEntityStructureNode node;
        List<String> idList;
        
        idList = new ArrayList<String>();
        node = getNode(entityIdentifier);
        if (node != null)
        {
            for (T8DataEntityStructureNode descendantNode : node.getDescendentNodes())
            {
                idList.add(descendantNode.getID());
            }
        }
        
        return idList;
    }
    
    /**
     * This method returns the list of entity ID's from the root to the
     * specified node and also includes the descendants of the specified node
     * but not its siblings or any of their descendants.
     * @param entityIdentifier The entity ID for which to retrieve the 
     * lineage ID set.
     * @return The lineage set of entity ID's to which the specified
     * organization belongs.
     */
    public List<String> getEntityLineageIdentifierList(String entityIdentifier)
    {
        List<String> idList;
        
        idList = getEntityPathIdentifierList(entityIdentifier);
        idList.addAll(getEntityDescendantIdentifierList(entityIdentifier));
        return idList;
    }
    
    public List<T8DataEntityStructureNode> getNodes()
    {
        List<T8DataEntityStructureNode> nodes;
        
        nodes = new ArrayList<T8DataEntityStructureNode>();
        for (T8DataEntityStructureNode baseNode : baseNodes)
        {
            nodes.add(baseNode);
            nodes.addAll(baseNode.getDescendentNodes());
        }
        
        return nodes;
    }
    
    public T8DataEntityStructureNode getNode(String entityIdentifier)
    {
        LinkedList<T8DataEntityStructureNode> nodeList;

        nodeList = new LinkedList<T8DataEntityStructureNode>();
        nodeList.addAll(baseNodes);
        while (nodeList.size() > 0)
        {
            T8DataEntityStructureNode nextNode;

            nextNode = nodeList.pop();
            if (nextNode.getID().equals(entityIdentifier))
            {
                return nextNode;
            }
            else
            {
                nodeList.addAll(nextNode.getChildNodes());
            }
        }
        
        return null;
    }
    
    public int getSize()
    {
        if (baseNodes != null)
        {
            int count;
            
            count = 0;
            for (T8DataEntityStructureNode baseNode : baseNodes)
            {
                count += baseNode.countDescendants();
                count += 1;
            }
            
            return count;
        }
        else return 0;
    }
}
