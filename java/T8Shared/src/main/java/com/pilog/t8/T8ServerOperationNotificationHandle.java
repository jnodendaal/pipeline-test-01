package com.pilog.t8;

import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;

/**
 * This class is simply used as a synchronization handle.  Whenever the
 * status of an executing operation changes, notifyAll() is invoked on the
 * notification handle of the operation.
 * @author Bouwer du Preez
 */
public class T8ServerOperationNotificationHandle
{
    private final String operationIid;
    private T8ServerOperationStatus operationStatus;

    public T8ServerOperationNotificationHandle(String operationIid)
    {
        this.operationIid = operationIid;
    }

    public String getOperationIid()
    {
        return operationIid;
    }

    public T8ServerOperationStatus getOperationStatus()
    {
        return operationStatus;
    }

    public synchronized void setOperationStatus(T8ServerOperationStatus operationStatus)
    {
        this.operationStatus = operationStatus;
    }
}
