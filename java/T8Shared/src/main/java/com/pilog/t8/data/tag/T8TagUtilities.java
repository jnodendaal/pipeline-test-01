package com.pilog.t8.data.tag;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8TagUtilities
{
    public static final String OPEN_SQUARE_BRACKET_REPLACEMENT = "<<-OSB->>";
    public static final String CLOSE_SQUARE_BRACKET_REPLACEMENT = "<<-CSB->>";
    public static final String COLON_REPLACEMENT = "<<-COL->>";
    
    /**
     * Returns only the Identifier of the supplied tag (in String format).
     * 
     * @param tag The tag String from which the identifier will be extracted.
     * @return The identifier extracted from the supplied tag String or null if
     * no valid identifier was found.
     */
    private static String getTagIdentifier(String tag)
    {
        int colonIndex;
        
        colonIndex = tag.lastIndexOf(":");
        if (colonIndex > -1)
        {
            int prefixIndex;
            
            prefixIndex = tag.indexOf("[");
            if ((prefixIndex > -1) && (prefixIndex < colonIndex))
            {
                return tag.substring(1, colonIndex);
            }
            else return null;
        }
        else
        {
            int prefixIndex;
            int suffixIndex;
            
            prefixIndex = tag.indexOf("[");
            suffixIndex = tag.indexOf("]");
            if ((prefixIndex > -1) && (suffixIndex > -1) && (prefixIndex + 1 < suffixIndex))
            {
                return tag.substring(prefixIndex + 1, suffixIndex);
            }
            else return null;
        }
    }
    
    /**
     * Returns only the Value of the supplied tag (in String format).
     * 
     * @param tag The tag String from which the value will be extracted.
     * @return The value extracted from the supplied tag or null if no value was
     * found.
     */
    private static String getTagValue(String tag)
    {
        int colonIndex;
        
        colonIndex = tag.lastIndexOf(":");
        if (colonIndex > -1)
        {
            return resetSpecialCharacters(tag.substring(colonIndex + 1, tag.length()-1));
        }
        else
        {
            return null; // If no colon in the tag, it means the tag is an identifier only (no value).
        }
    }
    
    /**
     * Returns only the Value of the specified tag extracted from the supplied
     * tag String.
     * 
     * @param tagString The tag String (may contain multiple tags).
     * @param tagIdentifier The Identifier of the tag in the tag String for 
     * which the value will be extracted.
     * @return The value of the specified tag as extracted from the supplied
     * tag String.  Returns null if the tag was not found or if the tag has a
     * null value (no colon in the tag).
     */
    public static String getTagValue(String tagString, String tagIdentifier)
    {
        if (tagString == null)
        {
            return null;
        }
        else
        {
            int colonIndex;
            int tagEndIndex;

            colonIndex = tagString.lastIndexOf("[" + tagIdentifier + ":");
            tagEndIndex = tagString.indexOf("]", colonIndex);
            if (colonIndex > -1)
            {
                // Use substring to extract only the value part of the tag.
                return tagString.substring(colonIndex + 2 + tagIdentifier.length(), tagEndIndex);
            }
            else
            {
                return null; // If no colon in the tag String, it means the tag is not present at all.
            }
        }
    }
    
    /**
     * Parses all tag Strings in the supplied list (each String may only contain
     * one tag) and then returns a set of only those tags that have the 
     * specified identifier.
     * 
     * @param tags The list of tag Strings to parse.
     * @param tagIdentifier The Identifier of the tags for which values will be
     * returned in the result set.
     * @return The result set containing only the values of tags from the 
     * supplied input list that have the specified identifier.
     */
    public static Set<String> getTagValueSet(List<String> tags, String tagIdentifier)
    {
        Set<String> tagValues;
        
        tagValues = new HashSet<String>();
        for (String tag : tags)
        {
            String identifier;
            
            identifier = getTagIdentifier(tag);
            if (tagIdentifier.equals(identifier))
            {
                tagValues.add(getTagValue(tag));
            }
        }
        
        return tagValues;
    }
    
    /**
     * Returns a list of all tag values from the input collection for tags with
     * a specified identifier.
     * 
     * @param tags The list of tag Strings to parse.
     * @param tagIdentifier The Identifier of the tags for which values will be
     * returned in the result list.
     * @return The result list containing only the values of tags from the 
     * supplied input collection that have the specified identifier.
     */
    public static List<String> getTagValueList(Collection<T8Tag> tags, String tagIdentifier)
    {
        List<String> tagValues;
        
        tagValues = new ArrayList<String>();
        for (T8Tag tag : tags)
        {
            String identifier;
            
            identifier = tag.getTagIdentifier();
            if (tagIdentifier.equals(identifier))
            {
                tagValues.add(tag.getTagValue());
            }
        }
        
        return tagValues;
    }
    
    /**
     * Creates a Pattern that can be used to match all Strings of the correct
     * tag format.
     * 
     * @return The newly created Pattern.
     */
    public static Pattern createTagPattern()
    {
        return Pattern.compile("\\[[^\\]]*\\]");
    }
    
    private static String replaceSpecialCharacters(String inputString)
    {
        String formattedString;
            
        formattedString = inputString.replaceAll(Pattern.quote("["), OPEN_SQUARE_BRACKET_REPLACEMENT);
        formattedString = formattedString.replaceAll(Pattern.quote("]"), CLOSE_SQUARE_BRACKET_REPLACEMENT);
        formattedString = formattedString.replaceAll(Pattern.quote(":"), COLON_REPLACEMENT);
        return formattedString;
    }
    
    private static String resetSpecialCharacters(String inputString)
    {
        String formattedString;
            
        formattedString = inputString.replaceAll(Pattern.quote(OPEN_SQUARE_BRACKET_REPLACEMENT), "[");
        formattedString = formattedString.replaceAll(Pattern.quote(CLOSE_SQUARE_BRACKET_REPLACEMENT), "]");
        formattedString = formattedString.replaceAll(Pattern.quote(COLON_REPLACEMENT), ":");
        return formattedString;
    }
    
    /**
     * Returns the properly formatted String representation of a single tag with
     * the specified identifier and value.
     * 
     * @param tagIdentifier The tag identifier.
     * @param tagValue The tag value.
     * @return The formal String representation of the supplied tag Identifier 
     * and value.
     */
    private static String getTagString(String tagIdentifier, String tagValue)
    {
        if (tagIdentifier != null)
        {
            StringBuffer tagString;

            tagString = new StringBuffer();
            tagString.append("[");
            tagString.append(tagIdentifier);
            if (tagValue != null)
            {
                tagString.append(":");
                tagString.append(replaceSpecialCharacters(tagValue));
            }
            tagString.append("]");
            return tagString.toString();
        }
        else return null;
    }
    
    /**
     * Returns a properly formatted String containing all the tags from the
     * supplied map, appended in order of map iteration.
     * 
     * @param tagMap The map of tags to be appended to the tag String.
     * @return The tag String consisting of all tags from the supplied Map.
     */
    public static String getTagString(Map<String, String> tagMap)
    {
        if ((tagMap != null) && (tagMap.size() > 0))
        {
            StringBuffer tagString;
            
            tagString = new StringBuffer();
            for (String tagIdentifier : tagMap.keySet())
            {
                String tag;
                
                tag = getTagString(tagIdentifier, tagMap.get(tagIdentifier));
                if (tag != null)
                {
                    tagString.append(" "); // Always prepend a space if a tag is added to the tag String (standard format in T8).
                    tagString.append(tag);
                }
            }
            
            return tagString.length() > 0 ? tagString.toString() : null;
        }
        else return null;
    }
    
    /**
     * Parses a single T8Tag object from the supplied String.  If the supplied
     * tag String contains no valid tag data or is null, a null result will be
     * returned.
     * 
     * @param tagString The String from which the T8Tag will be parsed.  Only
     * the first valid tag from the string will be parsed.
     * @return The T8Tag object parsed from the supplied String or null if no
     * valid tag data was found in the String.
     */
    public static T8Tag parseTag(String tagString)
    {
        if ((tagString == null) || (tagString.length() < 3))
        {
            return null;
        }
        else
        {
            String tagIdentifier;
            
            tagIdentifier = getTagIdentifier(tagString);
            if (tagIdentifier != null)
            {
                return new T8Tag(tagIdentifier, getTagValue(tagString));
            }
            else return null;
        }
    }
    
    /**
     * Parses an ordered list of T8Tag objects from the supplied input String
     * and collects the list in a T8TagSet.
     * 
     * @param multipleTagString The String of tags from which the map will be
     * parsed.
     * @return A T8TagSet containing the tags parsed from the supplied string.
     */
    public static T8TagSet parseTags(String multipleTagString)
    {
        T8TagSet tagSet;
        
        tagSet = new T8TagSet();
        if (multipleTagString != null)
        {
            Pattern tagPattern;
            Matcher matcher;

            tagPattern = createTagPattern();
            matcher = tagPattern.matcher(multipleTagString);
            while (matcher.find())
            {
                String tagString;

                tagString = matcher.group();
                if ((tagString != null) && (tagString.length() > 1))
                {
                    tagSet.addTag(new T8Tag(getTagIdentifier(tagString), getTagValue(tagString)), false);
                }
            }

            return tagSet;
        }
        else return tagSet;
    }
    
    /**
     * Parses an ordered map of tag identifier:value pairs from the supplied
     * input String.  If the input String is null or contains no valid tag data
     * an empty map will be returned.
     * 
     * @param multipleTagString The String of tags from which the map will be
     * parsed.
     * @return A map of tag identifier:value pairs.
     */
    public static Map<String, String> parseTagMap(String multipleTagString)
    {
        Map<String, String> tagMap;
        
        tagMap = new LinkedHashMap<String, String>();
        if (multipleTagString != null)
        {
            Pattern tagPattern;
            Matcher matcher;

            tagPattern = createTagPattern();
            matcher = tagPattern.matcher(multipleTagString);
            while (matcher.find())
            {
                String tagString;

                tagString = matcher.group();
                if ((tagString != null) && (tagString.length() > 1))
                {
                    tagMap.put(getTagIdentifier(tagString), getTagValue(tagString));
                }
            }

            return tagMap;
        }
        else return tagMap;
    }
}
