package com.pilog.t8.definition;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8IdentifierUtilities
{
    public static final String CONCEPT_ID_PATTERN = "[0-9a-fA-F]{32,32}";

    private static final Pattern COMPILED_CONCEPT_ID_PATTERN = Pattern.compile(CONCEPT_ID_PATTERN, 0);

    /** Private constructor to prevent instantiation of utility class. */
    private T8IdentifierUtilities() {}

    /**
     * This method takes a valid T8 Identifier and converts it to a strict camel case format.
     * This method can be used as the inverse of the {@code toIdCase} method.
     * The format requirements for the input identifier are:
     * - Only Capital letters, digits and underscores allowed.
     * - First character must be a letter.
     * - No leading or trailing underscores.
     * - No double underscores.
     * - No underscores followed directly preceding a digit.
     *
     * @param identifier The identifier to convert to camel case.
     * @return The camel case identifier equivalent of the input identifier.
     */
    public static String toCamelCase(String identifier)
    {
        StringBuilder buffer;

        buffer = new StringBuilder();
        for (String section : identifier.split("_"))
        {
            if (buffer.length() > 0)
            {
                buffer.append(Character.toUpperCase(section.charAt(0)));
                buffer.append(section.substring(1, section.length()).toLowerCase());
            }
            else buffer.append(section.toLowerCase());
        }

        return buffer.toString();
    }

    /**
     * This method takes a strict camel case identifier and converts it to a T8 identifier format.
     * This method can be used as the inverse of the {@code toCamelCase} method.
     * The format requirements of the input identifier are:
     *
     * @param identifier
     * @return
     */
    public static String toIdCase(String identifier)
    {
        return identifier.replaceAll("(.)(\\p{Upper})", "$1_$2").toUpperCase();
    }

    /**
     * @deprecated Use {@link #isConceptId(java.lang.String)} instead
     */
    @Deprecated
    public static boolean isConceptID(String identifier)
    {
        if (identifier != null)
        {
            return identifier.matches(CONCEPT_ID_PATTERN);
        }
        else return false;
    }

    /**
     * Uses a precompiled pattern to verify that the specified value is in a
     * valid format as to constitute a Concept ID.
     *
     * @param identifier The {@code String} value to test
     *
     * @return {@code true} if the value is determined to be a valid Concept ID,
     *      {@code false} otherwise
     */
    public static boolean isConceptId(String identifier)
    {
        if (!Strings.isNullOrEmpty(identifier))
        {
            return COMPILED_CONCEPT_ID_PATTERN.matcher(identifier).matches();
        } else return false;
    }

    public static boolean isLocalId(String id)
    {
        return T8Definition.getLocalIdPattern().matcher(id).matches();
    }

    public static boolean isPublicId(String id)
    {
        return T8Definition.getPublicIdPattern().matcher(id).matches();
    }

    public static boolean isPrivateId(String id)
    {
        return T8Definition.getPrivateIdPattern().matcher(id).matches();
    }

    public static boolean isQualifiedLocalId(String id)
    {
        return T8Definition.getQualifiedLocalIdPattern().matcher(id).matches();
    }

    public static boolean isInNamespace(String id, String namespace)
    {
        if (namespace == null) throw new IllegalArgumentException("Null namespace supplied.");
        else if (id.startsWith(namespace))
        {
            if (id.length() == namespace.length()) return true;
            else
            {
                // If the identifier has a local part, then it has to be directly after the namespace.
                return (id.charAt(namespace.length()) == '$');
            }
        }
        else return false;
    }

    public static String getGlobalIdentifierPart(String identifier)
    {
        // The global identifier part is equivalent to the namespace of the identifier, so just return the namespace.
        return getNamespace(identifier);
    }

    public static String getLocalIdentifierPart(String identifier)
    {
        int localIdentifierIndex;

        localIdentifierIndex = identifier.indexOf(T8Definition.getLocalIdPrefix());
        if (localIdentifierIndex == -1) // There is no local part to the identifier, so the entire identifier defines the namespace.
        {
            return null;
        }
        else
        {
            return identifier.substring(localIdentifierIndex);
        }
    }

    /**
     * Returns the fixed part of the supplied definition's identifier i.e.
     * the global/local prefix and the type prefix.
     *
     * @param definition The definition from which the identifier part will be
     * fetched.
     * @return The fixed part of the supplied definition's identifier.
     */
    public static String getFixedIdentifierPart(T8Definition definition)
    {
        String identifier;
        String prefix;

        if (definition == null) throw new IllegalArgumentException("Null definition found.");

        identifier = definition.getIdentifier();
        prefix = definition.getTypeMetaData().getIdPrefix();
        if (isLocalId(identifier))
        {
            return T8Definition.getLocalIdPrefix() + prefix;
        }
        else
        {
            return (T8Definition.getPublicIdPrefix() + prefix);
        }
    }

    /**
     * Returns the non-fixed part of the supplied definition's identifier i.e.
     * the part that comes after the global/local prefix and the type prefix.
     *
     * If the definition's identifier does not start with the standard prefix,
     * the part of the identifier after the global/local prefix is returned.
     *
     * @param definition The definition from which the identifier part will be
     * fetched.
     * @return The non-fixed part of the supplied definition's identifier.
     */
    public static String getNonFixedIdentifierPart(T8Definition definition)
    {
        String identifier;
        String prefix;

        if (definition == null) throw new IllegalArgumentException("Null definition found.");

        identifier = definition.getIdentifier();
        prefix = definition.getTypeMetaData().getIdPrefix();
        if (isLocalId(identifier))
        {
            prefix = (T8Definition.getLocalIdPrefix() + prefix);
        }
        else
        {
            prefix = (T8Definition.getPublicIdPrefix() + prefix);
        }

        if (identifier.startsWith(prefix)) return identifier.substring(prefix.length());
        else return identifier.substring(1);
    }

    public static String getNamespace(String identifier)
    {
        if (identifier.startsWith(T8Definition.getPublicIdPrefix()))
        {
            int localIdIndex;

            localIdIndex = identifier.indexOf(T8Definition.getLocalIdPrefix());
            if (localIdIndex == -1) // There is no local part to the identifier, so the entire identifier defines the namespace.
            {
                return identifier;
            }
            else
            {
                return identifier.substring(0, localIdIndex);
            }
        }
        else if (identifier.startsWith(T8Definition.getPrivateIdPrefix()))
        {
            int localIdIndex;

            localIdIndex = identifier.indexOf(T8Definition.getLocalIdPrefix());
            if (localIdIndex == -1) // There is no local part to the identifier, so the entire identifier defines the namespace.
            {
                return identifier;
            }
            else
            {
                return identifier.substring(0, localIdIndex);
            }
        }
        else return null;
    }

    public static String setNamespace(String identifier, String namespace)
    {
        int localIdentifierIndex;

        localIdentifierIndex = identifier.indexOf(T8Definition.getLocalIdPrefix());
        if (localIdentifierIndex == -1) // There is no local part to the identifier, so the entire identifier defines the namespace.
        {
            throw new IllegalArgumentException("Supplied identifier '" + identifier + "' does not have a local part.");
        }
        else
        {
            return namespace + identifier.substring(localIdentifierIndex);
        }
    }

    public static String changeNamespace(String identifier, String namespace)
    {
        return namespace + stripNamespace(identifier);
    }

    public static ArrayList<String> stripNamespace(List<String> identifiers)
    {
        ArrayList<String> strippedIdentifiers;

        strippedIdentifiers = new ArrayList<>();
        for (String identifier : identifiers)
        {
            strippedIdentifiers.add(stripNamespace(identifier));
        }

        return strippedIdentifiers;
    }

    public static String stripNamespace(String identifier)
    {
        String namespace;

        namespace = getNamespace(identifier);
        return namespace != null ? identifier.substring(namespace.length()) : identifier;
    }

    public static String stripNamespace(String identifier, String namespace, boolean strictChecking)
    {
        if (identifier == null) throw new IllegalArgumentException("Cannot strip namespace '" + namespace + "' from null identifier.");
        else if (identifier == null) throw new IllegalArgumentException("Cannot strip null namespace from identifier.");
        else if (isInNamespace(identifier, namespace))
        {
            return identifier.substring(namespace.length());
        }
        else
        {
            if (strictChecking)
            {
                throw new RuntimeException("Identifier '" + identifier + "' not in required namespace '" + namespace + "'.");
            }
            else return identifier;
        }
    }

    public static String stripVowels(String identifier, boolean keepFirstVowels)
    {
        StringBuffer result;
        int index = 0;

        result = new StringBuffer(identifier);
        if (identifier.startsWith(T8Definition.getPublicIdPrefix())) index = T8Definition.getPublicIdPrefix().length();
        else if (identifier.startsWith(T8Definition.getLocalIdPrefix())) index = T8Definition.getLocalIdPrefix().length();

        while (index < result.length())
        {
            Character precedingCharacter;
            char character;

            character = result.charAt(index);
            precedingCharacter = index > 0 ? result.charAt(index-1) : null;

            character = Character.toUpperCase(character);
            if ((character == 'A') || (character == 'E') || (character == 'I') || (character == 'O') || (character == 'U'))
            {
                if ((keepFirstVowels) && (precedingCharacter != null) && (precedingCharacter == '_'))
                {
                    index++;
                    continue;
                }
                else
                {
                    result.deleteCharAt(index);
                }
            }
            else index++;
        }

        return result.toString();
    }

    public static String createLocalIdentifier(String inputString)
    {
        return T8Definition.getLocalIdPrefix() + createIdentifierString(inputString);
    }

    public static String createGlobalIdentifier(String inputString)
    {
        return T8Definition.getPublicIdPrefix() + createIdentifierString(inputString);
    }

    /**
     * Creates a String that can be used in a valid T8 identifier.  That means
     * that all characters except letters, digits and underscores have been
     * stripped, all whitespace converted to underscores and all letters
     * switched to upper case.
     * @param inputString The input String from which to create the identifier
     * String.
     * @return The converted String that can be used in a T8 identifier.
     */
    public static String createIdentifierString(String inputString)
    {
        StringBuffer identifier;

        identifier = new StringBuffer();
        for (int index = 0; index < inputString.length(); index++)
        {
            char character;

            character = inputString.charAt(index);
            if (Character.isLetter(character))
            {
                identifier.append(Character.toUpperCase(character));
            }
            else if (((character == '_') || (Character.isWhitespace(character))) && (identifier.charAt(identifier.length()-1) != '_')) // Make sure not to add more than one underscore next to each other.
            {
                identifier.append('_');
            }
            else if (Character.isDigit(character))
            {
                identifier.append(character);
            }
        }

        return identifier.toString();
    }

    /**
     * This method takes a valid T8 identifier in the form ABC_DE_FGHIJK_L_MN and shortens it by
     * prioritizing the longest terms first as targets for reduced length.  Once all terms have been
     * shortened to a length of 1 character, the string will simply be shortened by reducing the length.
     * The above mentioned identifier will be shortened as follows:
     * ABC_DE_FGIJK_L_MN
     * ABC_DE_FGJK_L_MN
     * ABC_DE_FJK_L_MN
     * ABC_DE_FK_L_MN
     * AC_DE_FK_L_MN
     * AC_DE_FK_L_M
     * AC_DE_F_L_M
     * AC_D_F_L_M
     * A_D_F_L_M
     * A_D_F_L_
     *
     * @param inputString The identifier to shorten.
     * @param maximumLength The maximum length to which the string will be shortened.
     * @return The shortened identifier.
     */
    public static String shortenIdentifier(String inputString, int maximumLength)
    {
        StringBuffer identifier;

        identifier = new StringBuffer(inputString);
        while (identifier.length() > maximumLength)
        {
            int lastUnderscoreIndex;
            int targetStartIndex;
            int targetEndIndex;
            int longest;

            longest = 0;
            lastUnderscoreIndex = 0;
            targetStartIndex = 0;
            targetEndIndex = 0;
            for (int index = 0; index < identifier.length(); index++)
            {
                if (index == identifier.length()-1)
                {
                    if (lastUnderscoreIndex > 0)
                    {
                        int length;

                        length = index  - lastUnderscoreIndex;
                        if ((length > 1) && (length > longest))
                        {
                            longest = length;
                            targetStartIndex = lastUnderscoreIndex + 1;
                            targetEndIndex = index;
                        }
                    }
                    else
                    {
                        int length;

                        length = index;
                        if ((length > 1) && (length > longest))
                        {
                            longest = length;
                            targetStartIndex = 0;
                            targetEndIndex = index;
                        }
                    }
                }
                else
                {
                    char character;

                    character = identifier.charAt(index);
                    if (character == '_')
                    {
                        if (lastUnderscoreIndex > 0)
                        {
                            int length;

                            length = index - 1 - lastUnderscoreIndex;
                            if ((length > 1) && (length >= longest)) // The greater than *or* equal is important to allow terms of equal length to be shortend from last to first.
                            {
                                longest = length;
                                targetStartIndex = lastUnderscoreIndex + 1;
                                targetEndIndex = index - 1;
                            }
                        }
                        else
                        {
                            int length;

                            length = index;
                            if ((length > 1) && (length > longest))
                            {
                                longest = length;
                                targetStartIndex = 0;
                                targetEndIndex = index - 1;
                            }
                        }

                        lastUnderscoreIndex = index;
                    }
                }
            }

            // If we found a target area to shorten do it.
            if (longest > 0)
            {
                // If the length of the target term to shorten is longer than 2 characters, we find the character in the middle and remove it.
                if (longest > 2)
                {
                    int targetIndex;

                    targetIndex = targetStartIndex + ((targetEndIndex - targetStartIndex) / 2);
                    identifier.deleteCharAt(targetIndex);
                }
                else
                {
                    // The length of the target term is only 2 characters so remove the last one.
                    identifier.deleteCharAt(targetEndIndex);
                }
            }
            else // No target area left, so we have only one option: shorten the string.
            {
                identifier.setLength(maximumLength);
                return identifier.toString();
            }
        }

        return identifier.toString();
    }

    public static String createShortPrefix(String identifier)
    {
        StringBuffer prefix;
        int index = 0;

        prefix = new StringBuffer(identifier);
        if (identifier.startsWith(T8Definition.getPublicIdPrefix())) prefix.delete(0, T8Definition.getPublicIdPrefix().length());
        else if (identifier.startsWith(T8Definition.getLocalIdPrefix())) prefix.delete(0, T8Definition.getLocalIdPrefix().length());

        while (index < prefix.length())
        {
            char character;

            character = prefix.charAt(index);
            if (character != '_')
            {
                index++; // Skip the first letter, then delete all other characters until an underscore is reached.
                while ((prefix.length() > index) && (prefix.charAt(index) != '_'))
                {
                    prefix.deleteCharAt(index);
                }
            }
            else prefix.deleteCharAt(index);
        }

        return prefix.toString();
    }

    public static String removeApplicableNamespace(String identifier, List<String> namespaces)
    {
        String applicableNamespace;

        applicableNamespace = getApplicableNamespace(identifier, namespaces);
        if (applicableNamespace != null)
        {
            return identifier.substring(applicableNamespace.length());
        }
        else return identifier;
    }

    public static String getApplicableNamespace(String identifier, List<String> namespaces)
    {
        String applicableNamespace;

        applicableNamespace = null;
        for (String namespace : namespaces)
        {
            if ((identifier.startsWith(namespace)) && (applicableNamespace == null || (namespace.length() > applicableNamespace.length())))
            {
                applicableNamespace = namespace;
            }
        }

        return applicableNamespace;
    }

    /**
     * Prepends the specified namespace to each of the parameter identifiers in
     * the supplied list.  If a parameter in the supplied list already has a
     * prefixed namespace other than the one specified, an exception is thrown
     * if the strictChecking flag is set or else the identifier's existing
     * namespace is removed and replaced with the new namespace.
     * @param namespace The namespace to prepend.
     * @param identifiers The identifiers to which the namespace will be
     * prepended.
     * @param strictChecking If set, an exception will be thrown if an
     * identifier with any namespace other than the one specified is
     * encountered.
     */
    public static void prependNamespace(String namespace, List<String> identifiers, boolean strictChecking)
    {
        if (identifiers != null)
        {
            for (int index = 0; index < identifiers.size(); index++)
            {
                String parameterIdentifier;

                parameterIdentifier = identifiers.remove(index);
                if (isLocalId(parameterIdentifier))
                {
                    identifiers.add(index, namespace + parameterIdentifier);
                }
                else if (!isInNamespace(parameterIdentifier, namespace))
                {
                    if (!strictChecking)
                    {
                        identifiers.add(index, namespace + stripNamespace(parameterIdentifier));
                    }
                    else throw new RuntimeException("Invalid parameter identifier '" + parameterIdentifier + "' found while prepending namespace:" + namespace);
                }
            }
        }
    }

    /**
     * Prepends the specified namespace to each of the parameter identifiers in
     * the supplied map.  If a parameter in the supplied map already has a
     * prefixed namespace other than the one specified, an exception is thrown.
     * @param namespace The namespace to prepend.
     * @param parameters The parameters to which the namespace will be
     * prepended.
     * @return A map containing all of the supplied input parameters, each of
     * which will have the specified namespace prefixed to the parameter
     * identifier.
     */
    public static Map<String, Object> prependNamespace(String namespace, Map<String, Object> parameters)
    {
        return prependNamespace(namespace, parameters, true);
    }

    /**
     * Prepends the specified namespace to each of the parameter identifiers in
     * the supplied map.  If a parameter in the supplied map already has a
     * prefixed namespace other than the one specified, an exception is thrown
     * if the strictChecking flag is set or else the parameter is just left in
     * its existing namespace.
     * @param namespace The namespace to prepend.
     * @param parameters The parameters to which the namespace will be
     * prepended.
     * @param strictChecking If set,
     * @return A map containing all of the supplied input parameters, each of
     * which will have the specified namespace prefixed to the parameter
     * identifier.
     */
    public static Map<String, Object> prependNamespace(String namespace, Map<String, Object> parameters, boolean strictChecking)
    {
        if (parameters != null)
        {
            LinkedHashMap<String, Object> resultMap;

            resultMap = new LinkedHashMap<>();
            for (String parameterIdentifier : parameters.keySet())
            {
                Object parameterValue;

                parameterValue = parameters.get(parameterIdentifier);
                if (isLocalId(parameterIdentifier))
                {
                    resultMap.put(namespace + parameterIdentifier, parameterValue);
                }
                else if (isInNamespace(parameterIdentifier, namespace))
                {
                    resultMap.put(parameterIdentifier, parameterValue);
                }
                else if (!strictChecking)
                {
                    resultMap.put(parameterIdentifier, parameterValue);
                }
                else throw new RuntimeException("Invalid parameter identifier '" + parameterIdentifier + "' found while prepending namespace:" + namespace);
            }

            return resultMap;
        }
        else return null;
    }

    /**
     * Prepends the specified namespace to each of the parameter identifiers in
     * the supplied map.  If a parameter in the supplied map already has a
     * prefixed namespace it will be replaced with the specified namespace.
     * @param namespace The namespace to prepend.
     * @param parameters The parameters to which the namespace will be
     * prepended.
     * @return A map containing all of the supplied input parameters, each of
     * which will have the specified namespace prefixed to the parameter
     * identifier.
     */
    public static Map<String, Object> setNamespace(String namespace, Map<String, Object> parameters)
    {
        if (parameters != null)
        {
            LinkedHashMap<String, Object> resultMap;

            resultMap = new LinkedHashMap<>();
            for (String parameterIdentifier : parameters.keySet())
            {
                Object parameterValue;

                parameterValue = parameters.get(parameterIdentifier);
                if (isLocalId(parameterIdentifier))
                {
                    resultMap.put(namespace + parameterIdentifier, parameterValue);
                }
                else if (isInNamespace(parameterIdentifier, namespace))
                {
                    resultMap.put(parameterIdentifier, parameterValue);
                }
                else
                {
                    resultMap.put(namespace + stripNamespace(parameterIdentifier), parameterValue);
                }
            }

            return resultMap;
        }
        else return null;
    }

    /**
     * Strips all namespaces from the identifiers in the input map and returns
     * the output.  It is possible that the output map will contain fewer
     * parameters than the input map if, in the process of stripping the
     * namespace, some of the keys become identical.
     *
     * @param parameters The parameter map from which to strip any namespaces.
     * @return A map containing the parameters from the input map will any
     * namespace removed from the identifiers.
     */
    public static Map<String, Object> stripNamespace(Map<String, ? extends Object> parameters)
    {
        if (parameters != null)
        {
            LinkedHashMap<String, Object> resultMap;

            resultMap = new LinkedHashMap<>();
            for (String parameterId : parameters.keySet())
            {
                Object parameterValue;

                parameterValue = parameters.get(parameterId);
                resultMap.put(stripNamespace(parameterId), parameterValue);
            }

            return resultMap;
        }
        else return null;
    }

    public static Map<String, Object> stripNamespace(String namespace, Map<String, ? extends Object> parameters, boolean strictChecking)
    {
        if (parameters != null)
        {
            LinkedHashMap<String, Object> resultMap;

            resultMap = new LinkedHashMap<>();
            for (String parameterIdentifier : parameters.keySet())
            {
                Object parameterValue;

                parameterValue = parameters.get(parameterIdentifier);
                resultMap.put(stripNamespace(parameterIdentifier, namespace, strictChecking), parameterValue);
            }

            return resultMap;
        }
        else return null;
    }

    public static Map<String, Object> stripNamespaces(List<String> namespaces, Map<String, Object> parameters)
    {
        if (parameters != null)
        {
            if ((namespaces != null) && (namespaces.size() > 0))
            {
                LinkedHashMap<String, Object> resultMap;

                resultMap = new LinkedHashMap<>();
                for (String parameterIdentifier : parameters.keySet())
                {
                    Object parameterValue;
                    String applicableNamespace;

                    parameterValue = parameters.get(parameterIdentifier);
                    applicableNamespace = getApplicableNamespace(parameterIdentifier, namespaces);
                    if (applicableNamespace != null)
                    {
                        resultMap.put(stripNamespace(parameterIdentifier, applicableNamespace, false), parameterValue);
                    }
                    else
                    {
                        resultMap.put(parameterIdentifier, parameterValue);
                    }
                }

                return resultMap;
            }
            else return parameters;
        }
        else return null;
    }

    /**
     * Changes the namespace of all key identifiers in the supplied map to the
     * specified namespace.
     *
     * @param values The input values that will be mapped to a new namespace.
     * @param entityIdentifier The identifier to which all keys will be mapped.
     */
    public static void changeNamespace(Map<String, Object> values, String entityIdentifier)
    {
        if ((values != null) && (values.size() > 0))
        {
            Map<String, Object> mappedValues;

            mappedValues = new LinkedHashMap<>();
            for (String key : values.keySet())
            {
                mappedValues.put(T8IdentifierUtilities.changeNamespace(key, entityIdentifier), values.get(key));
            }

            values.clear();
            values.putAll(mappedValues);
        }
    }

    /**
     * This method will discard all of the parameters in the input map that
     * have identifiers that do not correspond to one of the specified
     * identifiers.  The specified identifiers to retain may either have
     * namespaces or not.
     *
     * @param parameters The parameter map that will be culled.
     * @param identifiers The identifier to retain in the map.  All others will
     * be removed.
     */
    public static void retainParameters(Map<String, Object> parameters, List<String> identifiers)
    {
        if ((parameters != null) && (parameters.size() > 0))
        {
            Iterator<String> keyIterator;

            keyIterator = parameters.keySet().iterator();
            while (keyIterator.hasNext())
            {
                String key;
                String keyWithoutNamespace;
                boolean found;

                key = keyIterator.next();
                keyWithoutNamespace = T8IdentifierUtilities.stripNamespace(key);
                found = false;

                // Try to find a match for the parameter identifier from the list of identifiers to retain.
                for (String identifier : identifiers)
                {
                    if (key.equals(identifier)) // Direct match.
                    {
                        found = true;
                        break;
                    }
                    else if (keyWithoutNamespace.equals(identifier)) // Match of the key without its namespace to the identifier (don't remove namespace from the identifier only the key).
                    {
                        found = true;
                        break;
                    }
                }

                // If now match was found, remove the key.
                if (!found) keyIterator.remove();
            }
        }
    }

    /**
     * This method maps the parameter keys in the input map to corresponding
     * output parameter keys in an output map.  Key keys are mapped through a
     * chain of 1 or more identifier mappings.  Each of these mappings contains
     * the input keys and corresponding output keys to which to map the inputs.
     * This method ALWAYS returns a map, even if the inputs are null.
     *
     * @param parameters The parameters to map.
     * @param identifierMaps The list of identifier mappings to use.
     * @return A Map of all parameters for which keys were mapped through the
     * mapping chain.
     */
    @SafeVarargs
    public static Map<String, Object> mapParameters(Map<String, Object> parameters, Map<String, String>... identifierMaps)
    {
        LinkedHashMap<String, Object> outputParameters;

        outputParameters = new LinkedHashMap<>();
        if ((parameters != null) && (identifierMaps != null))
        {
            for (String parameterIdentifier : parameters.keySet())
            {
                String mappedIdentifier;

                // Apply all of the supplied mappings to find the resultant identifier.
                mappedIdentifier = parameterIdentifier;
                for (Map<String, String> identifierMap : identifierMaps)
                {
                    if (identifierMap != null)
                    {
                        mappedIdentifier = identifierMap.get(mappedIdentifier);
                    }
                    else
                    {
                        // If one of the maps is null it means the mapping chain is broken so there is no need to continue the loop.
                        mappedIdentifier = null;
                        break;
                    }
                }

                // If a valid identifier has been mapped, then add it to the result.
                if (mappedIdentifier != null)
                {
                    outputParameters.put(mappedIdentifier, parameters.get(parameterIdentifier));
                }
            }
        }

        return outputParameters;
    }

    @SafeVarargs
    public static List<String> mapIdentifiers(List<String> identifiers, Map<String, String>... identifierMaps)
    {
        if (identifierMaps != null)
        {
            List<String> outputIdentifiers;

            outputIdentifiers = new ArrayList<>();
            if (identifiers != null)
            {
                for (String identifier : identifiers)
                {
                    String mappedIdentifier;

                    // Apply all of the supplied mappings to find the resultant identifier.
                    mappedIdentifier = identifier;
                    for (Map<String, String> identifierMap : identifierMaps)
                    {
                        if (identifierMap != null)
                        {
                            mappedIdentifier = identifierMap.get(mappedIdentifier);
                        }
                        else
                        {
                            // If one of the maps is null it means the mapping chain is broken so there is no need to continue the loop.
                            mappedIdentifier = null;
                            break;
                        }
                    }

                    // If a valid identifier has been mapped, then add it to the result.
                    if (mappedIdentifier != null)
                    {
                        outputIdentifiers.add(mappedIdentifier);
                    }
                }
            }

            return outputIdentifiers;
        }
        else return identifiers;
    }

    public static Map<String, String> createReverseIdentifierMap(Map<String, String> identifierMap)
    {
        LinkedHashMap<String, String> reverseMap;

        reverseMap = new LinkedHashMap<>();
        if (identifierMap != null)
        {
            for (String key : identifierMap.keySet())
            {
                String value;

                value = identifierMap.get(key);
                reverseMap.put(value, key);
            }
        }

        return reverseMap;
    }

    public static String generateLocalIdentifier()
    {
        return T8Definition.getLocalIdPrefix() + createNewGUID();
    }

    public static String createNewGUID()
    {
        UUID id;

        id = UUID.randomUUID();
        return id.toString().replace("-", "").toUpperCase();
    }

    public static String createNewGlobalDefinitionInstanceIdentifier()
    {
        return T8Definition.getPublicIdPrefix() + createNewGUID();
    }

    public static boolean isDefinitionIdentifierType(T8DataType dataType)
    {
        if (dataType.isType(T8DtString.IDENTIFIER))
        {
            return (((T8DtString)dataType).getType() == T8DtString.T8StringType.DEFINITION_IDENTIFIER);
        }
        else return false;
    }

    public static String convertIdentifierToTitleCase(String identifier)
    {
        StringBuilder titleCase;
        String localIdentifierPrefix;
        boolean isPreviousCharSpace;
        boolean isFirstChar;

        titleCase = new StringBuilder();
        localIdentifierPrefix = T8Definition.getLocalIdPrefix();

        isPreviousCharSpace = false;
        isFirstChar = true;
        for (char c : identifier.toCharArray())
        {
            if(localIdentifierPrefix.equals(String.valueOf(c))) continue;

            if(Character.valueOf(c).equals('_'))
            {
                isPreviousCharSpace = true;
                titleCase.append(" ");
            }
            else
            {
                if(isPreviousCharSpace || isFirstChar) titleCase.append(Character.toTitleCase(c));
                else titleCase.append(Character.toLowerCase(c));

                isPreviousCharSpace = false;
            }
            isFirstChar = false;
        }

        return titleCase.toString();
    }

    /**
     * This method should be used rather than the
     * {@link #convertIdentifierToTitleCase(java.lang.String)}, since it is
     * twice as fast and works with any identifiers (i.e. Global, local and
     * combination).<br/>
     * <br/>
     * <b>Example:</b><br/>
     * @NOTIF_SERVER_SHUTDOWN$NOTIFICATION<br/>
     * is converted to<br/>
     * Notif Server Shutdown Notification<br/>
     * <br/>
     * <b>Note:</b> Currently underscores following one another will give an
     * unwanted result.
     *
     * @param identifier The identifier for which to get a readable
     *      {@code String} value
     *
     * @return The {@code String} readable identifier
     */
    public static String readableIdentifier(String identifier)
    {
        StringBuilder titleBuilder;
        char localIdentifier;
        int idIdx;
        char c;

        localIdentifier = T8Definition.getLocalIdPrefix().charAt(0);

        idIdx = (identifier.startsWith(T8Definition.getPublicIdPrefix()) || identifier.startsWith(T8Definition.getLocalIdPrefix())) ? 1 : 0;

        titleBuilder = new StringBuilder();
        titleBuilder.append(identifier.charAt(idIdx++));
        for (;idIdx < identifier.length(); idIdx++)
        {
            c = identifier.charAt(idIdx);

            if (c == '_' || c == localIdentifier) titleBuilder.append(' ').append(identifier.charAt(++idIdx));
            else titleBuilder.append(Character.toLowerCase(c));
        }

        return titleBuilder.toString();
    }
}
