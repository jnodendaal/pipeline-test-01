package com.pilog.t8.definition.ui;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentEventDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_EVENT";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_EVENT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Component Event";
    public static final String DESCRIPTION = "An event that may be triggered on a UI component.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {EVENT_PARAMETER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ComponentEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.derivedDefinitionList(Datum.EVENT_PARAMETER_DEFINITIONS.toString(), "Event Parameters", "The data parametes available when this event occurs."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EVENT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public List<T8DataParameterDefinition> getParameterDefinitions()
    {
        return (List<T8DataParameterDefinition>)getDefinitionDatum(Datum.EVENT_PARAMETER_DEFINITIONS.toString());
    }

    public void setParameterDefinitions(List<T8DataParameterResourceDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.EVENT_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }
    
    public void addParameterDefinition(T8DataParameterResourceDefinition parameterDefinition)
    {
        addSubDefinition(Datum.EVENT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    @Override
    public String toString()
    {
        return "Event[" + getIdentifier() + "]";
    }
}
