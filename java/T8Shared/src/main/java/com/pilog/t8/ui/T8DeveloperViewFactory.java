package com.pilog.t8.ui;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.graph.T8GraphDefinition;

/**
 * @author Bouwer du Preez
 */
public interface T8DeveloperViewFactory
{
    public T8DeveloperView createReadOnlyDefinitionNavigator(T8DeveloperView parentView, String projectId, String definitionId);
    public T8DeveloperView createReadOnlyDefinitionNavigator(T8DeveloperView parentView, T8Definition newDefinition);
    public T8DeveloperView createDefinitionNavigator(T8DeveloperView parentView, String projectId, String definitionId);
    public T8DeveloperView createDefinitionNavigator(T8DeveloperView parentView, T8Definition newDefinition);
    public T8DeveloperView createDefinitionCompositionView(T8DeveloperView parentView, String projectId, String definitionId);
    public T8DeveloperView createDefinitionGraph(T8DeveloperView parentView, T8GraphDefinition newDefinition);
    public T8DeveloperView createDefinitionGraphManager(T8DeveloperView parentView, String groupId);
    public T8DeveloperView createDefinitionGroupManager(T8DeveloperView parentView, String groupId);
    public T8DeveloperView createProjectEditor(T8DeveloperView parentView, String projectId);
    public T8DeveloperView createProjectManager(T8DeveloperView parentView);
    public T8DeveloperView createDefinitionUsagesManager(T8DeveloperView parentView, T8Definition definition);
    public T8DeveloperView createDefinitionSearchManager(T8DeveloperView parentView);
    public T8DeveloperView createDefinitionSearchManager(T8DeveloperView parentView, String groupId);
    public T8DeveloperView createDefinitionJsonManager(T8DeveloperView parentView);
}
