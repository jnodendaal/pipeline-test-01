package com.pilog.t8.definition.operation.java;

import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * This class defines the definition of a specific operation type.
 *
 * @author Bouwer du Preez
 */
public class T8JavaServerOperationDefinition extends T8ServerOperationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_SERVER_OPERATION_JAVA";
    public static final String DISPLAY_NAME = "Java Server Operation";
    public static final String DESCRIPTION = "A server-side pre-compiled Java operation.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {CLASS_NAME};
    // -------- Definition Meta-Data -------- //

    public T8JavaServerOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_NAME.toString(), "Class Name", "The class name to use when instantiating this an operation of this type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid)
    {
        return T8Reflections.getFastFailInstance(getClassName(), new Class<?>[]{T8Context.class, this.getClass(), String.class}, context, this, operationIid);
    }

    public String getClassName()
    {
        return getDefinitionDatum(Datum.CLASS_NAME);
    }

    public void setClassName(String clazzName)
    {
        setDefinitionDatum(Datum.CLASS_NAME, clazzName);
    }
}

