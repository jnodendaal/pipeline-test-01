package com.pilog.t8.definition.procedure;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.procedure.T8Procedure;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.remote.server.connection.T8SoapConnectionDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.xml.XmlDocument;
import com.pilog.xml.XmlElement;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SoapDialogProcedureDefinition extends T8ProcedureDefinition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_PROCEDURE_SOAP_DIALOG";
    public static final String DISPLAY_NAME = "SOAP Dialog Procedure";
    public static final String DESCRIPTION = "A server-side EPIC procedure that conducts a SOAP request-response dialog.";
    public enum Datum {CONNECTION_ID,
                       SCRIPT};
    // -------- Definition Meta-Data -------- //

    public T8SoapDialogProcedureDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_ID.toString(), "Endpoint Connection", "The id of the connection to the endpoint to which the SOAP request from this procedure will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.SCRIPT.toString(), "Script", "The script code."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8SoapConnectionDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8Procedure getNewProcedureInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.procedure.T8SoapDialogProcedure").getConstructor(T8Context.class, this.getClass());
            return (T8Procedure)constructor.newInstance(context, this);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while instatiating T8SoapDialogProcedure from definition: " + this, e);
        }
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultServerContextScript").getConstructor(T8Context.class, T8ServerContextScriptDefinition.class);
        return (T8ServerContextScript)constructor.newInstance(context, this);
    }

    @Override
    public String getScript()
    {
        return (String)getDefinitionDatum(Datum.SCRIPT.toString());
    }

    public void setScript(String script)
    {
        setDefinitionDatum(Datum.SCRIPT.toString(), script);
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        return getInputParameterDefinitions();
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return getOutputParameterDefinitions();
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<>();
        classImports.add(new T8ScriptClassImport(XmlDocument.class.getSimpleName(), XmlDocument.class));
        classImports.add(new T8ScriptClassImport(XmlElement.class.getSimpleName(), XmlElement.class));
        return classImports;
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        return null;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    public String getConnectionId()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_ID.toString());
    }

    public void setConnectionId(String id)
    {
        setDefinitionDatum(Datum.CONNECTION_ID.toString(), id);
    }
}

