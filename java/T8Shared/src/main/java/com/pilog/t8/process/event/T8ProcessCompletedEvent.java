package com.pilog.t8.process.event;

import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.process.T8ProcessDetails;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessCompletedEvent extends EventObject
{
    private final T8ProcessDetails processDetails;
    private final Map<String, Object> outputParameters;

    public T8ProcessCompletedEvent(T8ProcessManager processManager, T8ProcessDetails processDetails, Map<String, Object> outputParameters)
    {
        super(processManager);
        this.processDetails = processDetails;
        this.outputParameters = outputParameters;
    }

    public T8ProcessDetails getProcessDetails()
    {
        return processDetails;
    }

    public Map<String, Object> getOutputParameters()
    {
        return new HashMap<>(outputParameters);
    }
}
