package com.pilog.t8.definition.data.source.cte.sql;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.source.cte.T8CTEDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8SQLCTEDefinition extends T8CTEDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMON_TABLE_EXPRESSION_SQL";
    public static final String DISPLAY_NAME = "SQL Common Table Expression";
    public static final String DESCRIPTION = "A Common Table Expression (With Clause) that can be used in other applicable data sources";
    public enum Datum
    {
        CTE,
        SQL_PARAMETER_EXPRESSION_MAP
    };
    // -------- Definition Meta-Data -------- //

    public T8SQLCTEDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CTE.toString(), "CTE", "The common table expression"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION_VALUE_MAP, Datum.SQL_PARAMETER_EXPRESSION_MAP.toString(), "SQL Parameter Expressions", "The parameters used in the SQL Strings of this data source and their corresponding value expressions."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if ((Datum.CTE.toString().equals(datumId)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.sql.T8SQLDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumId));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        } return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.data.source.cte.T8SQLCTEDefinitionActionHandler").getConstructor(T8Context.class, T8SQLCTEDefinition.class);
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public String getSQLCTEString(T8DataFilter filter, int pageOffset, int pageSize)
    {
        return (String)getDefinitionDatum(Datum.CTE.toString());
    }

    public void setSQLCTEtring(String sql)
    {
        setDefinitionDatum(Datum.CTE.toString(), sql);
    }

    @Override
    public Map<String, String> getSQLParameterExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.SQL_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setSQLParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.SQL_PARAMETER_EXPRESSION_MAP.toString(), expressionMap);
    }
}
