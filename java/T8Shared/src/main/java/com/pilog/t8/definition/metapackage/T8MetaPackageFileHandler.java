package com.pilog.t8.definition.metapackage;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import java.io.File;
import java.io.RandomAccessFile;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Bouwer du Preez
 */
public class T8MetaPackageFileHandler
{
    private final T8DefinitionSerializer serializer;

    private boolean dataAdded;
    private RandomAccessFile dataFile;
    private SecretKeySpec secretKey;
    private T8MetaPackageFileIndex index;
    private Cipher encryptCipher;
    private Cipher decryptCipher;

    public T8MetaPackageFileHandler(T8DefinitionSerializer serializer)
    {
        this.dataFile = null;
        this.index = new T8MetaPackageFileIndex();
        this.serializer = serializer;
    }

    public T8MetaPackageDescriptor addPackage(String packageIdentifier) throws Exception
    {
        return index.addPackage(packageIdentifier);
    }

    public int getDefinitionCount(String packageName)
    {
        return index.getPackageDefinitionCount(packageName);
    }

    public boolean containsPackage(String packageIdentifier)
    {
        return index.containsPackage(packageIdentifier);
    }

    public boolean openFile(File file, boolean createNewFile) throws Exception
    {
        // Check that an encryption key has been set.
        if (secretKey == null) throw new Exception("No encryption key set.");

        if ((file.exists()) && (!createNewFile))
        {
            // Read the data file index from the opened data file.
            dataFile = new RandomAccessFile(file, "rw");
            readIndex();
            return true;
        }
        else
        {
            // Write a Long value to the start of the new file, to reserve a location where the data file index offset will later be stored.
            dataFile = new RandomAccessFile(file, "rw");
            dataFile.setLength(0);
            dataFile.seek(0);
            dataFile.writeLong(100);
            return true;
        }
    }

    private void readIndex() throws Exception
    {
        long dataFileIndexOffset;
        int dataFileIndexSize;
        byte[] dataBytes;

        dataFile.seek(0);
        dataFileIndexOffset = dataFile.readLong();
        dataFile.seek(dataFileIndexOffset);

        dataFileIndexSize = (int)(dataFile.length() - dataFileIndexOffset);
        dataBytes = new byte[dataFileIndexSize];
        dataFile.read(dataBytes);
        dataBytes = decryptBytes(dataBytes);

        dataFile.seek(dataFileIndexOffset); // Return to the position where the index starts, so that any addition of data rows will occur from there.

        index = (T8MetaPackageFileIndex)SerializationUtilities.deserializeObject(dataBytes);
    }

    private void writeIndex() throws Exception
    {
        long dataFileIndexOffset;
        byte[] dataBytes;

        // Write the data file index to the data file and record its location.
        dataFileIndexOffset = dataFile.getFilePointer();
        dataBytes = SerializationUtilities.serializeObject(index);
        dataBytes = encryptBytes(dataBytes);
        dataFile.write(dataBytes);

        // Write the offset of the dataFileDefintion at the start of the file.
        dataFile.seek(0);
        dataFile.writeLong(dataFileIndexOffset);
    }

    public void writeDefinition(String packageIdentifier, T8Definition definition) throws Exception
    {
        String definitionString;
        byte[] dataBytes;
        long startOffset;
        int dataSize;

        definitionString = serializer.serializeDefinition(definition);
        dataBytes = definitionString.getBytes("UTF-8");
        dataBytes = encryptBytes(dataBytes);
        startOffset = dataFile.getFilePointer();
        dataSize = dataBytes.length;
        dataFile.write(dataBytes);
        dataAdded = true;

        index.addDefinitionDataLocation(packageIdentifier, definition.getIdentifier(), startOffset, dataSize);
    }

    public T8Definition readDefinition(String packageIdentifier, int definitionNumber) throws Exception
    {
        T8MetaPackageDefinitionDataLocation dataLocation;

        dataLocation = index.getPackageDataLocation(packageIdentifier, definitionNumber);
        if (dataLocation != null)
        {
            return readDefinition(dataLocation);
        }
        else return null;
    }

    public T8Definition readDefinition(String packageIdentifier, String definitionIdentifier) throws Exception
    {
        T8MetaPackageDefinitionDataLocation dataLocation;

        dataLocation = index.getPackageDataLocation(packageIdentifier, definitionIdentifier);
        if (dataLocation != null)
        {
            return readDefinition(dataLocation);
        }
        else return null;
    }

    private T8Definition readDefinition(T8MetaPackageDefinitionDataLocation dataLocation) throws Exception
    {
        T8Definition definition;
        String definitionString;
        byte[] dataBytes;

        dataFile.seek(dataLocation.getOffset());
        dataBytes = new byte[dataLocation.getSize()];
        dataFile.read(dataBytes);
        dataBytes = decryptBytes(dataBytes);
        definitionString = new String(dataBytes, "UTF-8");

        definition = serializer.deserializeDefinition(definitionString);
        return definition;
    }

    public void closeFile() throws Exception
    {
        // If data was added to the file, write the updated file index to the file.
        if (dataAdded)
        {
            writeIndex();
        }

        dataFile.close();
        index = null;
    }

    public boolean isFileOpen()
    {
        return dataFile != null;
    }

    public ArrayList<String> getPackageIdentifiers()
    {
        return index.getPackageIdentifierList();
    }

    public ArrayList<String> getPackageDefinitionIdentifiers(String packageIdentifier)
    {
        return index.getPackageDefinitionIdentifierList(packageIdentifier);
    }

    public T8MetaPackageFileIndex getIndex()
    {
        return index;
    }

    public void setSecretKey(byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
        secretKey = new SecretKeySpec(keyBytes, "AES");

        encryptCipher = Cipher.getInstance("AES");
        encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);

        decryptCipher = Cipher.getInstance("AES");
        decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
    }

    private byte[] encryptBytes(byte[] unencryptedBytes) throws IllegalBlockSizeException, BadPaddingException
    {
        return encryptCipher.doFinal(unencryptedBytes);
    }

    private byte[] decryptBytes(byte[] encryptedBytes) throws IllegalBlockSizeException, BadPaddingException
    {
        return decryptCipher.doFinal(encryptedBytes);
    }
}
