package com.pilog.t8;

import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.remote.server.connection.T8RemoteConnectionUser;
import com.pilog.t8.definition.user.T8AdministratorUserDefinition;
import com.pilog.t8.definition.user.T8ClientUserDefinition;
import com.pilog.t8.definition.user.T8DeveloperUserDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.security.T8PasswordChangeResponse;
import com.pilog.t8.security.T8SessionDetails;
import com.pilog.t8.security.T8UserDetails;
import com.pilog.t8.security.T8UserLoginResponse;
import com.pilog.t8.security.T8UserUpdateResponse;
import com.pilog.t8.security.event.T8SessionEventListener;
import java.util.Arrays;
import java.util.List;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8UserProfileDetails;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;

/**
 * @author Bouwer du Preez
 */
public interface T8SecurityManager extends T8ServerModule
{
    /**
     * An enum that defines the types of responses that can occur when a client
     * sends a login request to the security manager.
     */
    public enum T8LoginResponseType
    {
        /**
         * Login was successful.
         */
        SUCCESS(true),

        /**
         * Login was successful but the current user needs to update his/her
         * password because it has been reset.
         */
        SUCCESS_NEW_PASSWORD(true),

        /**
         * Login was not successful.
         */
        FAILURE(false),

        /**
         * The login failed because the password expired
         */
        PASSWORD_EXPIRED(true),

        /**
         * The login failed because the user expired
         */
        USER_EXPIRED(false),

        /** The login failed because the user is inactive  **/
        USER_INACTIVE(false),

        /**
         * The current session is already logged in.
         */
        SESSION_ALREADY_LOGGED_IN(true),

        /**
         * The current user is already logged in.
         */
        USER_ALREADY_LOGGED_IN(false),

        /**
         * The maximum number of concurrent sessions allowed for the requesting user has been reached.
         */
        MAXIMUM_CONCURRENT_SESSION_LIMIT_REACHED(false),

        /**
         * The login was successful but the password is about to expire
         */
        PASSWORD_EXPIRY_WARNING(true),

        /**
         * If the user account is set to a locked state
         */
        USER_ACCOUNT_LOCKED(false),

        /** If the user attempts to use an inappropriate entry point. */
        USER_NOT_ALLOWED(false);

        private final boolean sessionAuthenticated;

        private T8LoginResponseType(boolean sessionAuthenticated)
        {
            this.sessionAuthenticated = sessionAuthenticated;
        }

        public boolean isSessionAuthenticated()
        {
            return sessionAuthenticated;
        }
    };

    /**
     * The {@code LoginLocation} will determine the types of users which can
     * log in at the specified entry point.
     */
    public enum LoginLocation
    {
        /** Allows for only Developer and System users to log in. */
        DEVELOPER(T8AdministratorUserDefinition.class, T8DeveloperUserDefinition.class),
        /** Allows for Client, Developer and System users to log in. */
        CLIENT(T8AdministratorUserDefinition.class, T8DeveloperUserDefinition.class, T8ClientUserDefinition.class),
        /** Allows for only Developer and System users to log in. */
        CONFIGURATOR(T8AdministratorUserDefinition.class, T8DeveloperUserDefinition.class),
        /** Allows for Client, Developer and System users to log in. */
        WEB_SERVICE(T8AdministratorUserDefinition.class, T8DeveloperUserDefinition.class, T8ClientUserDefinition.class);

        private final List<Class<? extends T8UserDefinition>> validUserTypes;

        @SafeVarargs
        private LoginLocation(Class<? extends T8UserDefinition>... validUserTypes)
        {
            this.validUserTypes = Arrays.asList(validUserTypes);
        }

        /**
         * Determines whether or not the specified user definition is of a valid
         * type for the current entry point.
         *
         * @param userDefinition The {@code T8UserDefinition} to be checked
         *
         * @return {@code true} if the user type is valid for this entry point.
         *      {@code false} otherwise
         */
        public boolean isValidUserType(T8UserDefinition userDefinition)
        {
            return this.validUserTypes.contains(userDefinition.getClass());
        }
    };

    /**
     * Returns all current session details from the server.
     * @param context The session context from which the operation is
     * invoked.
     * @return
     * @throws Exception
     */
    public List<T8SessionDetails> getAllSessionDetails(T8Context context) throws Exception;

    /**
     * Returns current session details from the server.
     * @param context The session context for which details will be
     * returned.
     * @return
     * @throws Exception
     */
    public T8SessionDetails getSessionDetails(T8Context context) throws Exception;

    /**
     * Returns a Server Session Context to be used internally when the server
     * executes functionality where a session context is required.  This method
     * can only be used by the server from which this security manager was
     * constructed.
     * @return The session context constructed for use internal to the server.
     */
    public T8Context getMainServerContext();

    /**
     * Creates a new context to be used by one of the server manager objects.
     * @param module The manager module for which a new context is to be created.
     * @return The newly created context.
     */
    public T8Context createServerModuleContext(T8ServerModule module);

    /**
     * Creates a context containing the default settings for the specified organization.
     * @param context The parent context from which the new context is requested.
     * @param orgId The id of the organization for which a context will be constructed.
     * @return The new organization context.
     */
    public T8Context createOrganizationContext(T8Context context, String orgId);

    /**
     * Creates a new session context that can be used to log in a new user.
     * @return
     * @throws Exception
     */
    public T8SessionContext createNewSessionContext() throws Exception;

    /**
     * Returns the details of the specified user if security protocol allows.
     * @param context The session from which this operation is executed.
     * @param userId The username/user identifier of the user to fetch.
     * @return The details of the specified user.
     * @throws java.lang.Exception
     */
    public T8UserDetails getUserDetails(T8Context context, String userId) throws Exception;

    /**
     * Locks the user, preventing the specified user from login in to the system, when the user attempts to log in to the
     * system, the login will be blocked and the provided message will be displayed
     * @param context The session content of the current user
     * @param userId The user identifier/username of the user to lock
     * @param lockReason The lock reason that will be displayed to the user
     * @throws Exception
     */
    public void lockUser(T8Context context, String userId, String lockReason) throws Exception;

    /**
     *
     * @param context The session content of the current user
     * @param userId The user identifier/username of the user to unlock
     * @throws Exception
     */
    public void unlockUser(T8Context context, String userId) throws Exception;

    /**
     * Uses the supplied username and password to log a user into the system.
     * If successful, the supplied session context will be updated with the
     * users information.
     * @param context The session context from which the operation is
     * invoked.
     * @param userId
     * @param password
     * @param loginLocation
     * @param closeExistingSession
     * @return
     * @throws Exception
     */
    public T8UserLoginResponse login(T8Context context, LoginLocation loginLocation, String userId, char[] password, boolean closeExistingSession) throws Exception;

    /**
     * Logs the specified session context out of the system.
     * @param context The session context from which the operation is
     * invoked.
     * @return
     * @throws Exception
     */
    public boolean logout(T8Context context) throws Exception;

    /**
     * Authenticates the supplied request object and returns a response containing the details of the result.
     * @param request The request to authenticate.
     * @return The session context associated with the supplied request.
     */
    public T8AuthenticationResponse authenticate(T8HttpServletRequest request);

    /**
     * Changes the password for the supplied session user to the new password
     * supplied.
     * @param context The session context from which the operation is
     * invoked.
     * @param password
     * @return A response object with the success/failure flag of the operation
     * as well as any additional information or messages relevant to the result.
     * @throws Exception
     */
    public T8PasswordChangeResponse changePassword(T8Context context, char[] password) throws Exception;

    /**
     * Changes the password for the specified session user to the new password
     * supplied.
     *
     * @param context The session context from which the operation is
     * invoked.
     * @param userId The identifier of the user whose password will be
     * changed.  This parameter is not required if the username is specified.
     * @param password The new password to set.
     * @return A response object with the success/failure flag of the operation
     * as well as any additional information or messages relevant to the result.
     * @throws Exception
     */
    public T8PasswordChangeResponse changePassword(T8Context context, String userId, char[] password) throws Exception;

    /**
     * This method returns a boolean flag indicating whether or not a password
     * change request is currently in progress for the specified user.
     * @param context The session context from which the operation is
     * invoked.
     * @param userId The identifier of the user for which to check the
     * password change status.  This parameter is not required if the username
     * is specified.
     * @return A boolean flag indicating whether or not a password change
     * request is currently in progress for the specified user.
     * @throws Exception
     */
    public boolean isPasswordChangeAvailable(T8Context context, String userId) throws Exception;

    /**
     * Returns a Boolean flag indicating whether or not the supplied username is
     * available for registration.
     * @param context
     * @param username The username to check.
     * @return A Boolean value indicating whether or not the specified username
     * is available.
     * @throws Exception
     */
    public boolean isUsernameAvailable(T8Context context, String username) throws Exception;

    /**
     * Returns a Boolean flag indicating whether or not the specified user is
     * registered in this system.
     * @param context
     * @param userId
     * @return The user to check (can be either a user identifier or username).
     * @throws Exception
     */
    public boolean isUserRegistered(T8Context context, String userId) throws Exception;

    /**
     * Checks whether the specified session is active or not.
     * @param context The session context from which the operation is
     * invoked.
     * @param sessionId
     * @return
     * @throws Exception
     */
    public boolean isSessionActive(T8Context context, String sessionId) throws Exception;

    /**
     * Returns a list of all user profile identifiers that may be used by the
     * currently logged in user.
     * @param context The session context from which the operation is
     * invoked.
     * @return
     * @throws Exception
     */
    public List<String> getAccessibleUserProfileIds(T8Context context) throws Exception;

    /**
     * Returns a list of details of all user profiles that may be used by the
     * currently logged in user.
     * @param context The session context from which the operation is
     * invoked.
     * @return The list of details of the accessible user profiles.
     * @throws Exception
     */
    public List<T8UserProfileDetails> getAccessibleUserProfileDetails(T8Context context) throws Exception;

    /**
     * Returns a list of definition meta data for all user profiles that may be
     * used by the currently logged in user.
     * @param context The session context from which the operation is
     * invoked.
     * @return
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getAccessibleUserProfileMetaData(T8Context context) throws Exception;

    /**
     * Returns a set of the system functionality identifiers that may be used by
     * the currently logged in user.
     * @param context The session context from which the operation is
     * invoked.
     * @return
     * @throws Exception
     */
    public List<String> getAccessibleFunctionalityIds(T8Context context) throws Exception;

    /**
     * Switches the profile of the currently logged in user to the profile
     * specified.
     * @param context The session context from which the operation is
     * invoked.
     * @param userProfileId
     * @return
     * @throws Exception
     */
    public boolean switchUserProfile(T8Context context, String userProfileId) throws Exception;

    /**
     * Switches the current user session language to the one provided in the parameter
     * @param context The session context of the current user
     * @param languageId the language identifier to switch to
     * @return true if the language has been switched
     * @throws Exception
     */
    public T8SessionContext switchSessionLanguage(T8Context context, String languageId) throws Exception;

    /**
     * Sets the active indicator for the specified user identifier. If the
     * active indicator is the same as the current indicator set for the user,
     * no change is made.
     *
     * @param context The {@code T8Context} of the executing user
     * @param userId The {@code String} user identifier of the user to
     *      make active/inactive
     * @param active The activity level to be set for the specified user
     *
     * @throws Exception If the user is not found or some other error during
     *      the change process
     */
    public void setUserActive(T8Context context, String userId, boolean active) throws Exception;

    /**
     * Creates a new user in the system.
     * @param context The session context from which the operation is
     * invoked.
     * @param details The user details according to which the user will be
     * created.
     * @return The response from the security manager indicating operation
     * success and and supplying an additional message on failure.
     * @throws Exception
     */
    public T8UserUpdateResponse createNewUser(T8Context context, T8UserDetails details) throws Exception;

    /**
     * Updates the specified user with the supplied details.
     * @param context The session context from which the operation is
     * invoked.
     * @param details The user details according to which the user will be
     * created.
     * @return The response from the security manager indicating operation
     * success and and supplying an additional message on failure.
     * @throws Exception
     */
    public T8UserUpdateResponse updateUserDetails(T8Context context, T8UserDetails details) throws Exception;

    /**
     * Deletes the specified user from the system.
     * @param context The session context from which the operation is
     * invoked.
     * @param userId
     * @return
     * @throws Exception
     */
    public boolean deleteUser(T8Context context, String userId) throws Exception;

    /**
     * Resets the specified user's password so that the user will have to enter
     * a new password at next login.
     * @param context The session context from which the operation is
     * invoked.
     * @param userId The identifier of the user for which the password
     * will be reset.
     * @return A boolean value indicating the success of the operation.
     * @throws Exception
     */
    public boolean resetUserPassword(T8Context context, String userId) throws Exception;

    /**
     * This method requests that a password change email be sent to the
     * specified user.  Once invoked, the email is sent to the specified user's
     * email address and a password change verification token is added to the
     * user definition.  The token is also supplied to the user in the email
     * that is sent out so that the token can be used to verify a subsequent
     * password change operation.
     * @param context The session context from which the operation is
     * invoked.
     * @param userId The identifier of the user to which the email will
     * be sent.  This parameter is not required if the username is specified.
     * @return A boolean flag indicating whether or not the operation has
     * completed successfully.
     * @throws Exception
     */
    public boolean requestPasswordChange(T8Context context, String userId) throws Exception;

    /**
     * Request the Web Service API Key for the user of the supplied session context.
     * If no current API Key exists for the user one will be generated.
     * @param context The session context of the user for which to retrieve the API Key.
     * @return The API Key of the user
     * @throws Exception if the user of the session context is not a valid web service user
     */
    public String getWebServiceUserApiKey(T8Context context) throws Exception;

    /**
     * Generates a new API Key for the web service user of the supplied session context.
     * If the user is not a valid web service user then nothing will happen.
     * Calling this method will invalidate the current API Key and all new request with the old API Key will
     * become invalid.
     * @param context The session context of the user for which to generate a new API Key.
     */
    public void generateWebServiceUserApiKey(T8Context context);

    /**
     *  Gets the preferred remote connection user, for the current session user, for the specified connection identifier.
     * @param context The session context of the current user.
     * @param conectionId The connection identifier for which the remote user must be retrieved.
     * @return The remote connection user, if available, else null
     */
    public T8RemoteConnectionUser getRemoteConnectionUser(T8Context context, String conectionId);

    /**
     * Adds a listener to the security manager that will be notified of all
     * events that occur.
     * @param listener
     */
    public void addSessionEventListener(T8SessionEventListener listener);

    /**
     * Removes the specified listener from the security manager.
     * @param listener
     */
    public void removeSessionEventListener(T8SessionEventListener listener);

    /**
     * Refreshes the session expiry time. The purpose of this method is to reset
     * the session expiration time to a full session duration.
     *
     * @param context The {@code T8Context} of the session to be
     *      extended
     *
     * @throws Exception If there is any error during the resetting of the
     *      session expiry
     */
    public void extendSession(T8Context context) throws Exception;

    /**
     * Creates a new access context associated with the current thread and the
     * specified session.
     * @param sessionContext The session context to associate with the current thread and the newly creates access context.
     * @return The newly created access context.
     */
    public T8Context createContext(T8SessionContext sessionContext);

    /**
     * Returns the access context currently associated with the invoking thread.
     * @return The access context currently associated with the invoking thread.
     */
    public T8Context getContext();

    /**
     * Returns the new context after access has been approved.
     * @param context The context to be accessed.
     * @return The new context.
     */
    public T8Context accessContext(T8Context context);

    /**
     * Destroys the access context associated with the current thread.
     */
    public void destroyContext();

    /**
     * Enables/disables performance tracking on a specific session.
     * @param context  The context of the requesting session.
     * @param sessionId The id of the session for which to enabled/disable performance tracking.
     * @param enabled A Boolean flag indicating whether or not performance tracking should be enabled (true) or disabled (false).
     * @throws java.lang.Exception
     */
    public void setSessionPerformanceStatisticsEnabled(T8Context context, String sessionId, boolean enabled) throws Exception;

    /**
     * Returns the performance statistics gathered for a specific session.
     * @param context The context of the requesting session.
     * @param sessionId The id of the session for which to fetch performance statistics.
     * @return The performance statistics gathered for the specified session.
     * @throws java.lang.Exception
     */
    public T8PerformanceStatistics getSessionPerformanceStatistics(T8Context context, String sessionId) throws Exception;
}
