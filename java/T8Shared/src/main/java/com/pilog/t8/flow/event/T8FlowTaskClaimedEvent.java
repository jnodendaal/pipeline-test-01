package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey.WaitKeyIdentifier;
import com.pilog.t8.flow.key.T8TaskClaimExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskClaimedEvent extends T8FlowEvent
{
    private final String taskIid;
    private final String claimedByUserId;

    public static final String TYPE_ID = "TASK_CLAIMED";

    public T8FlowTaskClaimedEvent(String taskInstanceIdentifier, String claimedByUserIdentifier)
    {
        super(taskInstanceIdentifier);
        this.taskIid = taskInstanceIdentifier;
        this.claimedByUserId = claimedByUserIdentifier;
    }

    public T8FlowTaskClaimedEvent(JsonObject object)
    {
        super(object.getString("taskIid"));
        this.taskIid = object.getString("taskIid");
        this.claimedByUserId = object.getString("claimedByUserId");
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskIid;
    }

    public String getClaimedByUserIdentifier()
    {
        return claimedByUserId;
    }

    @Override
    public T8FlowExecutionKey getExecutionKey(T8WorkFlowNodeDefinition waitingNodeDefinition, T8FlowNodeState waitingNodeState)
    {
        T8FlowTaskState taskState;

        taskState = waitingNodeState.getTaskState();
        if (taskState != null)
        {
            // Check that the task IID's match.
            if (taskState.getTaskIid().equals(taskIid))
            {
                String flowIdentifier;
                String flowInstanceIdentifier;
                String nodeIdentifier;
                String nodeInstanceIdentifier;

                flowIdentifier = taskState.getFlowId();
                flowInstanceIdentifier = taskState.getFlowIid();
                nodeIdentifier = taskState.getNodeId();
                nodeInstanceIdentifier = taskState.getNodeIid();
                return new T8TaskClaimExecutionKey(flowIdentifier, flowInstanceIdentifier, nodeIdentifier, nodeInstanceIdentifier, taskIid, claimedByUserId);
            }
            else return null;
        }
        else throw new RuntimeException("No task state found in node state waiting for task '" + taskIid + "' claim:" + waitingNodeState);
    }

    @Override
    public T8DataFilterCriteria getWaitKeyFilterCriteria()
    {
        Map<String, Object> filterMap;

        filterMap = new HashMap<String, Object>();
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_ID", WaitKeyIdentifier.TASK_CLAIM.toString());
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_TASK_IID", taskIid);
        return new T8DataFilterCriteria(filterMap);
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        JsonObject eventObject;

        eventObject = new JsonObject();
        eventObject.add("taskIid", taskIid);
        eventObject.add("claimedByUserId", claimedByUserId);
        return eventObject;
    }
}
