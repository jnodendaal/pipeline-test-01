package com.pilog.t8.definition.flow.task.object;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectGroupHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.object.T8DataObjectGroupDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskObjectGroupDefinition extends T8DataObjectGroupDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_GROUP_TASK";
    public static final String DISPLAY_NAME = "Task Object Group";
    public static final String DESCRIPTION = "A group of task objects.";
    public enum Datum
    {
        OBJECT_IDS
    };
    // -------- Definition Meta-Data -------- //

    public T8TaskObjectGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.OBJECT_IDS.toString(), "Task Objects", "The ids of the objects belonging to this group."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.OBJECT_IDS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8TaskObjectDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DataObjectGroupHandler getDataObjectGroupHandler(T8DataTransaction tx)
    {
        return T8Reflections.getInstance("com.pilog.t8.flow.task.object.T8TaskObjectGroupHandler", new Class<?>[]{T8TaskObjectGroupDefinition.class, T8DataTransaction.class}, this, tx);
    }

    public List<String> getObjectIds()
    {
        List<String> ids;

        ids = (List<String>)getDefinitionDatum(Datum.OBJECT_IDS);
        return ids != null ? ids : new ArrayList<String>();
    }

    public void setObjectIds(List<String> ids)
    {
        setDefinitionDatum(Datum.OBJECT_IDS, ids);
    }
}
