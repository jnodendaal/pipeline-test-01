package com.pilog.t8.data;

import com.pilog.t8.definition.data.source.listener.T8DataSourceListenerDefinition;

/**
 * @author Bouwer du Preez
 */
public interface T8DataSourceListener
{
    public T8DataSourceListenerDefinition getDefinition();

    public void open() throws Exception;
    public void close() throws Exception;
    public void commit() throws Exception;
    public void rollback() throws Exception;
    
    public void entityInserted(T8DataEntity dataEntity);
    public void entityUpdated(T8DataEntity oldEntity, T8DataEntity newEntity);
    public void entityDeleted(T8DataEntity dataEntity);
}
