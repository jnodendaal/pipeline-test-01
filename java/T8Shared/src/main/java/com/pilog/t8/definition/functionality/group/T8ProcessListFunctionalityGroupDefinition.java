package com.pilog.t8.definition.functionality.group;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.process.T8ProcessSummary;
import com.pilog.t8.process.T8ProcessTypeSummary;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectFieldDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessListFunctionalityGroupDefinition extends T8DefaultFunctionalityGroupDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_GROUP_SYSTEM_PROCESS_LIST";
    public static final String DISPLAY_NAME = "Process List Functionality Group";
    public static final String DESCRIPTION = "A grouping used to access system process list functionalities.";
    public static final String IDENTIFIER_PREFIX = "FNC_GRP_";
    public enum Datum
    {
        FUNCTIONALITY_IDENTIFIER,
        TARGET_OBJECT_IDENTIFIER,
        PROCESS_TYPE_FIELD_IDENTIFIER,
        INCLUDE_EXECUTING_FIELD_IDENTIFIER,
        INCLUDE_IDLE_FIELD_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    private T8DataObjectDefinition targetObjectDefinition;

    public T8ProcessListFunctionalityGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_IDENTIFIER.toString(), "Functionality", "The identifier of the module functionlity that will be displayed when one of the functionalities in this group is invoked.  This module takes responsibility for display of the selected process type list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_OBJECT_IDENTIFIER.toString(), "Target Object", "The identifier of the target object that will be created for execution of the functionality."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROCESS_TYPE_FIELD_IDENTIFIER.toString(), "Process Type Field", "The identifier of the target object field to which the process type identifier will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INCLUDE_EXECUTING_FIELD_IDENTIFIER.toString(), "'Include Executing' Field", "The identifier of the target object field to which the boolean flag 'Include Executing' (indicating whether or not executing processes should be included) will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INCLUDE_IDLE_FIELD_IDENTIFIER.toString(), "'Include Idle' Field", "The identifier of the target object field to which the boolean flag 'Include Idle' (indicating whether or not executing processes should be included) will be sent."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TARGET_OBJECT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if ((Datum.PROCESS_TYPE_FIELD_IDENTIFIER.toString().equals(datumIdentifier)) || (Datum.INCLUDE_EXECUTING_FIELD_IDENTIFIER.toString().equals(datumIdentifier)) || (Datum.INCLUDE_IDLE_FIELD_IDENTIFIER.toString().equals(datumIdentifier)))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String targetObjectId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            optionList.add(new T8DefinitionDatumOption("No mapping.", null));

            targetObjectId = getTargetObjectIdentifier();
            if (targetObjectId != null)
            {
                T8DataObjectDefinition dataObjectDefinition;

                dataObjectDefinition = (T8DataObjectDefinition)definitionContext.getRawDefinition(getRootProjectId(), targetObjectId);
                if (dataObjectDefinition != null)
                {
                    List<T8DataObjectFieldDefinition> objectFieldDefinitions;

                    objectFieldDefinitions = dataObjectDefinition.getFieldDefinitions();
                    optionList.addAll(createPublicIdentifierOptionsFromDefinitions((List)objectFieldDefinitions));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String definitionId;

        // Call the super-initialization.
        super.initializeDefinition(context, inputParameters, configurationSettings);

        // Pre-load the target object definition if one is specified.
        definitionId = getTargetObjectIdentifier();
        if (definitionId != null)
        {
            targetObjectDefinition = (T8DataObjectDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    @Override
    public T8FunctionalityGroupHandle getNewFunctionalityGroupHandle(T8Context context, T8FunctionalityGroupHandle parentGroup) throws Exception
    {
        try
        {
            T8ConfigurationManager configurationManager;
            T8ProcessManager processManager;
            T8FunctionalityGroupHandle groupHandle;
            T8FunctionalityHandle handle;
            T8ProcessSummary processListSummary;
            String functionalityId;

            // Create the new handle.
            groupHandle = super.getNewFunctionalityGroupHandle(context, parentGroup);
            functionalityId = getFunctionalityIdentifier();

            // Get a configuration and process managers.
            configurationManager = context.getServerContext().getConfigurationManager();
            processManager = context.getServerContext().getProcessManager();

            // Get the process list summary.
            processListSummary = processManager.getProcessSummary(context);

            // Create the 'All Processes' Functionality.
            handle = new T8FunctionalityHandle(functionalityId);
            handle.setDisplayName(configurationManager.getUITranslation(context, "All Processes") + " (" + (processListSummary.getTotalProcessCount()) + ")");
            handle.setDescription(configurationManager.getUITranslation(context, "All Processes available for administration."));
            handle.setIcon(getIcon());
            handle.addParameter(functionalityId + "$P_PROCESS_ID", null);

            // Add the completed handle to the group.
            groupHandle.addFunctionality(handle);

            // Add all the process type functionalities.
            for (T8ProcessTypeSummary processTypeSummary : processListSummary.getProcessTypeSummaries())
            {
                if (processTypeSummary.getTotalCount() > 0)
                {
                    // Create the Functionality.
                    handle = new T8FunctionalityHandle(functionalityId);
                    handle.setDisplayName(configurationManager.getUITranslation(context, processTypeSummary.getDisplayName()) + " (" + (processTypeSummary.getTotalCount() - processTypeSummary.getExecutingCount()) + ")");
                    handle.setDescription(processTypeSummary.getDescription());
                    handle.setIcon(getIcon());
                    handle.addParameter(functionalityId + "$P_PROCESS_ID", processTypeSummary.getProcessId());

                    // Add the completed handle to the group.
                    groupHandle.addFunctionality(handle);
                }
            }

            // Return the constructed handle.
            return groupHandle;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while initializing functionality group handle: " + getIdentifier(), e);
        }
    }

    public String getFunctionalityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER.toString());
    }

    public void setFunctionalityIdentifier(String functionalityIdentifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER.toString(), functionalityIdentifier);
    }

    public String getTargetObjectIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_OBJECT_IDENTIFIER.toString());
    }

    public void setTargetObjectIdentifier(String functionalityIdentifier)
    {
        setDefinitionDatum(Datum.TARGET_OBJECT_IDENTIFIER.toString(), functionalityIdentifier);
    }

    public String getProcessTypeFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROCESS_TYPE_FIELD_IDENTIFIER.toString());
    }

    public void setProcessTypeFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROCESS_TYPE_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getIncludeExecutingFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INCLUDE_EXECUTING_FIELD_IDENTIFIER.toString());
    }

    public void setIncludeExecutingFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INCLUDE_EXECUTING_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getIncludeIdleFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INCLUDE_IDLE_FIELD_IDENTIFIER.toString());
    }

    public void setIncludeIdleFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INCLUDE_IDLE_FIELD_IDENTIFIER.toString(), identifier);
    }

    public T8DataObjectDefinition getTargetObjectDefinition()
    {
        return targetObjectDefinition;
    }
}
