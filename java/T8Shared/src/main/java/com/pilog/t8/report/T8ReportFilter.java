package com.pilog.t8.report;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.pilog.t8.definition.report.T8ReportManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ReportFilter implements Serializable
{
    private String userId;
    private final Set<String> reportIds; // If no ids are added, all types are considered to be included.
    private ReportOrdering reportOrdering;

    public enum ReportOrdering
    {
        TIME_STARTED_ASCENDING(F_GENERATION_START_TIME, T8DataFilter.OrderMethod.ASCENDING),
        TIME_STARTED_DESCENDING(F_GENERATION_START_TIME, T8DataFilter.OrderMethod.DESCENDING),
        TIME_COMPLETED_ASCENDING(F_GENERATION_END_TIME, T8DataFilter.OrderMethod.ASCENDING),
        TIME_COMPLETED_DESCENDING(F_GENERATION_END_TIME, T8DataFilter.OrderMethod.DESCENDING);

        private final String fieldId;
        private final T8DataFilter.OrderMethod orderMethod;

        private ReportOrdering(String fieldId, T8DataFilter.OrderMethod orderMethod)
        {
            this.fieldId = fieldId;
            this.orderMethod = orderMethod;
        }

        private String getFieldId(String entityId)
        {
            return entityId + fieldId;
        }
    };

    public T8ReportFilter()
    {
        reportIds = new HashSet<>();
        reportOrdering = ReportOrdering.TIME_COMPLETED_DESCENDING;
    }

    public T8ReportFilter copy()
    {
        T8ReportFilter newFilter;

        newFilter = new T8ReportFilter();
        newFilter.setUserId(userId);
        newFilter.setReportOrdering(reportOrdering);
        newFilter.setIncludedReportIds(reportIds);
        return newFilter;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public void clearIncludedReportId()
    {
        reportIds.clear();
    }

    public void addIncludedReportId(String reportId)
    {
        reportIds.add(reportId);
    }

    public boolean isReportIdIncluded(String reportId)
    {
        return reportIds.isEmpty() || reportIds.contains(reportId);
    }

    public Set<String> getIncludedReportIds()
    {
        return new HashSet<>(reportIds);
    }

    public void setIncludedReportIds(Collection<String> reportIds)
    {
        this.reportIds.clear();
        if (reportIds != null)
        {
            this.reportIds.addAll(reportIds);
        }
    }

    public ReportOrdering getReportOrdering()
    {
        return reportOrdering;
    }

    public void setReportOrdering(ReportOrdering ordering)
    {
        this.reportOrdering = ordering;
    }

    public T8DataFilter getDataFilter(T8Context context, String entityId)
    {
        T8DataFilterCriteria filterCriteria;
        T8DataFilter filter;

        // Create the process filter.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityId, filterCriteria);
        filter.addFieldOrdering(this.reportOrdering.getFieldId(entityId), this.reportOrdering.orderMethod);

        // Add user criteria.
        if (this.userId != null)
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + F_USER_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, this.userId);
        }

        // Add report id filter criteria.
        if (!this.reportIds.isEmpty())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + F_REPORT_ID, T8DataFilterCriterion.DataFilterOperator.IN, this.reportIds);
        }

        // Return the final criteria.
        return filter;
    }
}
