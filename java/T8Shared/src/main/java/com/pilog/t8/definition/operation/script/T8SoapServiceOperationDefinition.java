package com.pilog.t8.definition.operation.script;

import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.datatype.T8DtString.T8StringType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.security.T8Context;
import com.pilog.xml.XmlDocument;
import com.pilog.xml.XmlElement;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SoapServiceOperationDefinition extends T8ScriptServerOperationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_OPERATION_SOAP_SERVICE";
    public static final String DISPLAY_NAME = "EPIC SOAP Server Operation";
    public static final String DESCRIPTION = "A server-side EPIC Script operation that implements the endpoints of a SOAP-based web service.";
    public static final String IDENTIFIER_PREFIX = ""; // We override the default operation prefix, so that the URL for the operation is not restricted.
    public enum Datum {WSDL};
    // -------- Definition Meta-Data -------- //

    public static final String XML_INPUT_PARAMETER_ID = "$P_XML_INPUT";
    public static final String XML_OUTPUT_PARAMETER_ID = "$P_XML_OUTPUT";

    public T8SoapServiceOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(new T8DtString(T8StringType.XML), Datum.WSDL.toString(), "WSDL", "The WSDL that defines this service and the endpoints it contains."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.operation.script.T8SoapServiceOperation").getConstructor(T8Context.class, this.getClass(), String.class);
            return (T8ServerOperation)constructor.newInstance(context, this, operationIid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<>();
        classImports.add(new T8ScriptClassImport(XmlDocument.class.getSimpleName(), XmlDocument.class));
        classImports.add(new T8ScriptClassImport(XmlElement.class.getSimpleName(), XmlElement.class));
        return classImports;
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitionList;

        definitionList = new ArrayList();
        definitionList.add(new T8DataParameterDefinition(XML_INPUT_PARAMETER_ID, "XML Input", "The XML input object contained in the SOAP request envelope.", T8DataType.CUSTOM_OBJECT));
        return definitionList;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitionList;

        definitionList = new ArrayList();
        definitionList.add(new T8DataParameterDefinition(XML_OUTPUT_PARAMETER_ID, "XML Input", "The XML output object to return in the SOAP response envelope.", T8DataType.CUSTOM_OBJECT));
        return definitionList;
    }

    public String getWsdl()
    {
        return (String)getDefinitionDatum(Datum.WSDL);
    }

    public void setWsdl(String wsdl)
    {
        setDefinitionDatum(Datum.WSDL, wsdl);
    }
}

