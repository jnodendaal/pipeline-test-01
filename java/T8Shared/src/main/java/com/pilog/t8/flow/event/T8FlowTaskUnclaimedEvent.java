package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.task.T8TaskDetails;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskUnclaimedEvent extends T8FlowEvent
{
    public static final String TYPE_ID = "TASK_UNCLAIMED";
    
    public T8FlowTaskUnclaimedEvent(T8TaskDetails taskDetails)
    {
        super(taskDetails);
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public T8TaskDetails getTaskDetails()
    {
        return (T8TaskDetails)source;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
