package com.pilog.t8.process.event;

import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.process.T8ProcessDetails;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessFailedEvent extends EventObject
{
    private final T8ProcessDetails processDetails;

    public T8ProcessFailedEvent(T8ProcessManager processManager, T8ProcessDetails processDetails)
    {
        super(processManager);
        this.processDetails = processDetails;
    }

    public T8ProcessDetails getProcessDetails()
    {
        return processDetails;
    }
}
