package com.pilog.t8.definition.security.sso;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8SingleSignOnHandler;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8SingleSignOnHandlerDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SSO_HANDLER";
    public static final String GROUP_NAME = "Single-Sign-On Handlers";
    public static final String GROUP_DESCRIPTION = "Client-side security handlers that can be used to facilitate a single-sign-on authentication operation.";
    public static final String DISPLAY_NAME = "Single-Sign-On Handler";
    public static final String DESCRIPTION = "A definition of a client-side security handler that can be used to facilitate a single-sign-on authentication operation.";
    public static final String IDENTIFIER_PREFIX = "SSO_";
    public static final String STORAGE_PATH = "/single_sign_on_handlers";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8SingleSignOnHandlerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8SingleSignOnHandler createSingleSignOnHandler(T8ClientContext clientContext) throws Exception;
}
