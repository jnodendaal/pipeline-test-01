package com.pilog.t8.definition.filter;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFilterCriterion implements T8DefinitionFilterClause, Serializable
{
    private T8DefinitionFilterCriterionDefinition definition;
    private String datumIdentifier;
    private DefinitionFilterOperator operator;
    private Object filterValue;
    private boolean caseInsensitive;

    public enum DefinitionFilterOperator {EQUAL, NOT_EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL, LIKE, MATCHES, NOT_LIKE, IN, IS_NULL, IS_NOT_NULL};
    
    public T8DefinitionFilterCriterion(String datumIdentifier, DefinitionFilterOperator operator, Object filterValue)
    {
        this.datumIdentifier = datumIdentifier;
        this.operator = operator;
        this.filterValue = filterValue;
    }
    
    public T8DefinitionFilterCriterion(String datumIdentifier, DefinitionFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(datumIdentifier, operator, filterValue);
        this.caseInsensitive = caseInsensitive;
    }
    
    public T8DefinitionFilterCriterion(T8DefinitionFilterCriterionDefinition definition, String fieldIdentifier, DefinitionFilterOperator operator, Object filterValue)
    {
        this(fieldIdentifier, operator, filterValue);
        this.definition = definition;
    }

    public T8DefinitionFilterCriterionDefinition getDefinition()
    {
        return definition;
    }
    
    @Override
    public T8DefinitionFilterCriterion copy()
    {
        T8DefinitionFilterCriterion copiedCriterion;
        
        copiedCriterion = new T8DefinitionFilterCriterion(datumIdentifier, operator, filterValue);
        return copiedCriterion;
    }
    
    @Override
    public Set<String> getFilterDatumIdentifiers()
    {
        Set<String> datumIdentifierSet;
        
        datumIdentifierSet = new HashSet<String>();
        datumIdentifierSet.add(datumIdentifier);
        
        return datumIdentifierSet;
    }
    
    @Override
    public List<T8DefinitionFilterCriterion> getFilterCriterionList()
    {
        return ArrayLists.newArrayList(this);
    }
    
    public String getDatumIdentifier()
    {
        return datumIdentifier;
    }

    public void setDatumIdentifier(String datumIdentifier)
    {
        this.datumIdentifier = datumIdentifier;
    }

    public Object getFilterValue()
    {
        return filterValue;
    }

    public void setFilterValue(Object filterValue)
    {
        this.filterValue = filterValue;
    }

    public DefinitionFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(DefinitionFilterOperator operator)
    {
        this.operator = operator;
    }

    public boolean isCaseInsensitive()
    {
        return caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        this.caseInsensitive = caseInsensitive;
    }
    
    @Override
    public boolean includesDefinition(T8Definition definition)
    {
        Object value;
        
        value = definition.getDefinitionDatum(datumIdentifier);
        if (value == null)
        {
            if (operator == DefinitionFilterOperator.EQUAL) return filterValue == null;
            else if (operator == DefinitionFilterOperator.NOT_EQUAL) return filterValue != null;
            else if (operator == DefinitionFilterOperator.GREATER_THAN_OR_EQUAL) return filterValue == null;
            else if (operator == DefinitionFilterOperator.LESS_THAN_OR_EQUAL) return filterValue == null;
            else if (operator == DefinitionFilterOperator.LIKE) return filterValue == null;
            else if (operator == DefinitionFilterOperator.LIKE) return filterValue == null;
            else if (operator == DefinitionFilterOperator.IN) return filterValue == null;
            else if (operator == DefinitionFilterOperator.IS_NULL) return true;
            else if (operator == DefinitionFilterOperator.IS_NOT_NULL) return false;
            else return false;
        }
        else
        {
            if (operator == DefinitionFilterOperator.EQUAL) return equal(value, filterValue);
            else if (operator == DefinitionFilterOperator.NOT_EQUAL) return !equal(value, filterValue);
            else if (operator == DefinitionFilterOperator.GREATER_THAN_OR_EQUAL) return greaterThanOrEqual(value, filterValue);
            else if (operator == DefinitionFilterOperator.LESS_THAN_OR_EQUAL) return lessThanOrEqual(value, filterValue);
            else if (operator == DefinitionFilterOperator.LIKE) return like(value, (String)filterValue);
            else if (operator == DefinitionFilterOperator.NOT_LIKE) return !like(value, (String)filterValue);
            else if (operator == DefinitionFilterOperator.IN)
            {
                Iterator valueIterator;
                List valueList;

                valueList = (List)filterValue;
                valueIterator = valueList.iterator();

                while (valueIterator.hasNext())
                {
                    Object inObject;
                    
                    inObject = valueIterator.next();
                    if (value.equals(inObject)) return true;
                }
                
                return false;
            }
            else if (operator == DefinitionFilterOperator.IS_NULL) return false;
            else if (operator == DefinitionFilterOperator.IS_NOT_NULL) return true;
            else return false;
        }
    }
    
    @Override
    public T8DefinitionFilterCriterion getFilterCriterion(String identifier)
    {
        if ((definition != null) && (definition.getIdentifier().equals(identifier))) return this;
        else return null;
    }
    
    private boolean like(Object value, String filterValue)
    {
        if (filterValue != null)
        {
            boolean endsWildcard;
            boolean startsWildcard;
            String valueString;
            String filterString;

            valueString = "" + value;
            filterString = filterValue.replace("%", "");
            
            if (caseInsensitive)
            {
                valueString = valueString.toUpperCase();
                filterString = filterString.toUpperCase();
            }
            
            endsWildcard = filterValue.endsWith("%");
            startsWildcard = filterValue.startsWith("%");
            if (startsWildcard && endsWildcard)
            {
                return valueString.contains(filterString);
            }
            else if (startsWildcard)
            {
                return valueString.endsWith(filterString);
            }
            else if (endsWildcard)
            {
                return valueString.startsWith(filterString);
            }
            else
            {
                return valueString.contains(filterString);
            }
        }
        else return false;
    }
    
    private boolean equal(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            String valueString;
            String filterString;

            valueString = "" + value;
            filterString = "" + filterValue;
            
            if (caseInsensitive)
            {
                valueString = valueString.toUpperCase();
                filterString = filterString.toUpperCase();
            }
            
            return value.equals((String)filterValue);
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) == ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) == 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }
    
    private boolean greaterThanOrEqual(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            if (filterValue instanceof String)
            {
                return ((String)value).compareTo((String)filterValue) >= 0;
            }
            else return ((String)value).compareTo("" + filterValue) >= 0;
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) >= ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) >= 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }
    
    private boolean lessThanOrEqual(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            if (filterValue instanceof String)
            {
                return ((String)value).compareTo((String)filterValue) <= 0;
            }
            else return ((String)value).compareTo("" + filterValue) <= 0;
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) <= ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) <= 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }
}
