package com.pilog.t8.data;

import com.pilog.t8.cache.T8DataRequirementCache;
import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.data.connection.pool.T8DataConnectionPoolDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.cache.T8DataRecordCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache;

/**
 * @author Bouwer du Preez
 */
public interface T8DataTransaction extends T8PerformanceStatisticsProvider
{
    public String getIdentifier();
    public long getLastActivityTime();
    public Thread getParentThread();
    public T8DataSession getDataSession();
    public T8TerminologyCache getTerminologyCache();
    public T8DataRecordCache getDataRecordCache();
    public T8DataRequirementInstanceCache getDataRequirementInstanceCache();
    public T8DataRequirementCache getDataRequirementCache();
    public T8DataConnectionPoolDefinition getDataConnectionPoolDefinition(String connectionPoolId);
    public T8DataSourceDefinition getDataSourceDefinition(String dataSourceId);
    public T8DataEntityDefinition getDataEntityDefinition(String dataEntityId);
    public T8DataConnection getDataConnection(String connectionId);
    public <A extends T8Api> A getApi(String apiId);

    public void refreshDataSource(String dataSourceIdentifier, Map<String, Object> refreshParameters) throws Exception;

    public T8DataEntity create(String entityId, Map<String, Object> keyMap) throws Exception;
    public int count(String entityId, T8DataFilter filter) throws Exception;
    public boolean exists(String entityId, Map<String, Object> keyMap) throws Exception;
    public void insert(T8DataEntity dataEntity) throws Exception;
    public void insert(List<T8DataEntity> dataEntities) throws Exception;
    public boolean update(T8DataEntity dataEntity) throws Exception;
    public boolean update(List<T8DataEntity> dataEntities) throws Exception;
    public int update(String entityId, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception;
    public boolean delete(T8DataEntity dataEntity) throws Exception;
    public int delete(List<T8DataEntity> dataEntity) throws Exception;
    public int delete(String entityId, T8DataFilter filter) throws Exception;
    public int deleteCascade(T8DataEntity dataEntity) throws Exception;
    public int deleteCascade(String entityId, T8DataFilter filter) throws Exception;

    /**
     * Returns the new context after access has been approved.
     * @param context The context to be accessed.
     * @return The new context.
     */
    public T8Context accessContext(T8Context context);
    public T8Context getContext();

    /**
     * Retrieves a single record from the access provider as a
     * {@code T8DataEntity} object. If more than one record is returned, the
     * first in the set is returned. If no records are returned by the data
     * source, the result will be {@code null}.
     *
     * @param entityId The {@code String} entity identifier to be used
     *      for the data retrieval
     * @param keyMap The {@code Map} containing the key values pairs which
     *      define the filter to be applied for the record to be retrieved
     *
     * @return The first record as a {@code T8DataEntity} from the returned
     *      results. {@code null} if no records are returned
     *
     * @throws Exception If there is any error during the retrieval of the
     *      required data
     */
    public T8DataEntity retrieve(String entityId, Map<String, Object> keyMap) throws Exception;
    public List<T8DataEntity> select(String entityId, Map<String, Object> keyMap) throws Exception;
    public List<T8DataEntity> select(String entityId, T8DataFilter filter) throws Exception;
    public List<T8DataEntity> select(String entityId, Map<String, Object> keyMap, int pageOffset, int pageSize) throws Exception;
    public List<T8DataEntity> select(String entityId, T8DataFilter filter, int pageOffset, int pageSize) throws Exception;
    public T8DataEntityResults scroll(String entityId, T8DataFilter filter) throws Exception;
    public T8DataIterator<T8DataEntity> iterate(String entityId, T8DataFilter filter) throws Exception;

    public T8DataTransactionLog getLog();

    public void setDataSourceDefinition(T8DataSourceDefinition sourceDefinition);
    public void setDataEntityDefinition(T8DataEntityDefinition entityDefinition);

    /**
     * Indicates whether or not the transaction is active. Active to mean it has
     * not been committed or rolled-back.
     *
     * @return {@code true} if the transaction is still active. {@code false}
     *      otherwise
     */
    public boolean isOpen();

    /**
     * Indicates whether or not the transaction is currently executing an active
     * operation that has not finished yet.
     *
     * @return {@code true} if actively executing. {@code false} otherwise
     */
    public boolean isActive();
    public void rollback();
    public void commit();
    public void suspend();
    public void resume();
}
