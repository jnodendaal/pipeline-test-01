package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NgComponentFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private Map<String, Object> componentParameters;
    private String componentUri;
    private String iconUri;

    public T8NgComponentFunctionalityAccessHandle(String functionalityId, String functionalityIid, T8NgComponentDefinition componentDefinition, Map<String, Object> inputParameters, String displayName, String description)
    {
        super(functionalityId, functionalityIid, displayName, description, null);
        this.componentUri = componentDefinition.getComponentUri();
        this.componentParameters = inputParameters;
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        return null;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return true;
    }

    public String getComponentUri()
    {
        return componentUri;
    }

    public void setComponentUri(String componentUri)
    {
        this.componentUri = componentUri;
    }

    public Map<String, Object> getComponentParameters()
    {
        return componentParameters;
    }

    public void setComponentParameters(Map<String, Object> componentParameters)
    {
        this.componentParameters = componentParameters;
    }

    public String getIconUri()
    {
        return iconUri;
    }

    public void setIconUri(String iconUri)
    {
        this.iconUri = iconUri;
    }
}
