package com.pilog.t8.definition.communication;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8Communication;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public abstract class T8CommunicationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_COMMUNICATIONS";
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION";
    public static final String STORAGE_PATH = "/communication";
    public static final String DISPLAY_NAME = "Communication";
    public static final String DESCRIPTION = "A Communication Packet";
    public static final String IDENTIFIER_PREFIX = "COMM_";
    public enum Datum {
        ACTIVE,
        INPUT_PARAMETER_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8CommunicationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "Sets whether this communication is active or not.", true));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters", "The input data parameters required by this communication."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        } else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8CommunicationTestHarness", new Class<?>[]{});
    }

    public abstract T8Communication getCommunicationInstance(T8CommunicationManager communicationManager);

    public Boolean isActive()
    {
        return getDefinitionDatum(Datum.ACTIVE);
    }

    public void setActive(Boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE, active);
    }

    public List<T8DataParameterDefinition> getInputParameters()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameters(List<T8DataParameterDefinition> inputParameters)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, inputParameters);
    }
}
