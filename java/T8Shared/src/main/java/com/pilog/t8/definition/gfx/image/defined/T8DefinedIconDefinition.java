package com.pilog.t8.definition.gfx.image.defined;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Image;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinedIconDefinition extends T8IconDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_ICON_DEFINED";
    public static final String DISPLAY_NAME = "Defined Icon";
    public static final String DESCRIPTION = "An icon image that is stored using the T8Image format.";
    public enum Datum {IMAGE};
    // -------- Definition Meta-Data -------- //

    public T8DefinedIconDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.IMAGE, Datum.IMAGE.toString(), "Image Data", "The encoded image data."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Image getImage()
    {
        return (T8Image)getDefinitionDatum(Datum.IMAGE.toString());
    }
    
    public void setImage(T8Image image)
    {
        setDefinitionDatum(Datum.IMAGE.toString(), image);
    }
}
