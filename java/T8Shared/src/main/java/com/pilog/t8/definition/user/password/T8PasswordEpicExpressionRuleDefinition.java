package com.pilog.t8.definition.user.password;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8PasswordEpicExpressionRuleDefinition extends T8PasswordPolicyRuleDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_PASSWORD_EPIC_EXPRESSION_RULE";
    public static final String DISPLAY_NAME = "Password Epic Expression Rules";
    public static final String DESCRIPTION = "A rule to be applied when validating password using epic expression.";

    public enum Datum {EPIC_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8PasswordEpicExpressionRuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.EPIC_EXPRESSION.toString(), "Epic Expression", "The epic expression that will be used to validate the user password against."));

        return datumTypes;
    }

    @Override
    public T8PasswordRuleValidator getValidator()
    {
        return T8Reflections.getInstance("com.pilog.t8.security.rulevalidator.T8PasswordEpicExpressionRuleValidator", new Class<?>[]{T8PasswordEpicExpressionRuleDefinition.class}, this);
    }

    public void setEpicExpression(String text)
    {
        setDefinitionDatum(Datum.EPIC_EXPRESSION, text);
    }

    public String getEpicExpression()
    {
        return getDefinitionDatum(Datum.EPIC_EXPRESSION);
    }
}
