/**
 * Created on 08 Feb 2016, 3:14:20 PM
 */
package com.pilog.t8.cache;

/**
 * A {@code T8CacheDataProvider} in the form of a {@code FunctionalInterface}
 * which simply allows a more dynamic use for implementation requirements.<br/>
 * <br/>
 * A {@code T8CacheDataProvider} is intended to be used in association with a
 * {@code T8SLRUProviderCache}, but of course it is not restricted to be used
 * in this manner.<br/>
 * <br/>
 * Whether or not a provider will allow {@code null} to be returned is up to the
 * implementer.
 *
 * @author Gavin Boshoff
 *
 * @param <K> The type of the key specified for retrieval from the provider
 * @param <V> The type of the value returned from the provider
 */
@FunctionalInterface
public interface T8CacheDataProvider<K, V>
{
    V retrieve(K key);
}