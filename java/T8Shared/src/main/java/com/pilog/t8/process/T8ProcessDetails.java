package com.pilog.t8.process;

import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.process.T8Process.T8ProcessStatus;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessDetails implements Serializable
{
    private String projectId;
    private String processId;
    private String processIid;
    private String name;
    private String description;
    private String initiatorId;
    private String initiatorName;
    private String restrictionUserId;
    private String restrictionUserProfileId;
    private String failureMessage;
    private T8ProgressReport progressReport;
    private transient List<String> visibilityRestrictionUserIds;
    private transient List<String> visibilityRestrictionProfileIds;
    private transient List<String> visibilityRestrictionWorkflowProfileIds;
    private transient T8ProcessNotificationHandle notificationHandle;
    private transient boolean finalizeOnCompletion = false;
    private T8ProcessStatus status;
    private boolean executing;
    private double progress;
    private long startTime;
    private long endTime;

    public T8ProcessDetails(String processIid)
    {
        this.processIid = processIid;
    }

    public void setProcessDetails(T8ProcessDetails details)
    {
        this.projectId = details.getProjectId();
        this.processId = details.getProcessId();
        this.processIid = details.getProcessIid();
        this.name = details.getName();
        this.description = details.getProcessDescription();
        this.initiatorId = details.getInitiatorId();
        this.restrictionUserId = details.getRestrictionUserId();
        this.restrictionUserProfileId = details.getRestrictionUserProfileId();
        this.notificationHandle = details.getNotificationHandle();
        this.status = details.getStatus();
        this.executing = details.isExecuting();
        this.progress = details.getProgress();
        this.startTime = details.getStartTime();
        this.endTime = details.getEndTime();
        this.visibilityRestrictionUserIds = details.getVisibilityRestrictionUserIds();
        this.visibilityRestrictionProfileIds = details.getVisibilityRestrictionProfileIds();
        this.visibilityRestrictionWorkflowProfileIds = details.getVisibilityRestrictionWorkflowProfileIds();
        this.progressReport = details.getProgressReport();
    }

    public T8ProcessStatus getStatus()
    {
        return status;
    }

    public void setProcessStatus(T8ProcessStatus status)
    {
        this.status = status;
    }

    public String getProcessIid()
    {
        return processIid;
    }

    public void setProcessIid(String processIid)
    {
        this.processIid = processIid;
    }

    public boolean isExecuting()
    {
        return executing;
    }

    public void setExecuting(boolean executing)
    {
        this.executing = executing;
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId)
    {
        this.initiatorId = initiatorId;
    }

    public String getInitiatorName()
    {
        return initiatorName;
    }

    public void setInitiatorName(String initiatorName)
    {
        this.initiatorName = initiatorName;
    }

    public String getRestrictionUserId()
    {
        return restrictionUserId;
    }

    public void setRestrictionUserId(String restrictionUserId)
    {
        this.restrictionUserId = restrictionUserId;
    }

    public String getRestrictionUserProfileId()
    {
        return restrictionUserProfileId;
    }

    public void setRestrictionUserProfileId(String restrictionUserProfileId)
    {
        this.restrictionUserProfileId = restrictionUserProfileId;
    }

    public String getProcessDescription()
    {
        return description;
    }

    public void setProcessDescription(String processDescription)
    {
        this.description = processDescription;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFailureMessage()
    {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage)
    {
        this.failureMessage = failureMessage;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public void setStartTime(long startTime)
    {
        this.startTime = startTime;
    }

    public long getEndTime()
    {
        return endTime;
    }

    public void setEndTime(long endTime)
    {
        this.endTime = endTime;
    }

    public double getProgress()
    {
        return progress;
    }

    public void setProgress(double progress)
    {
        this.progress = progress;
    }

    public T8ProgressReport getProgressReport()
    {
        return progressReport;
    }

    public void setProgressReport(T8ProgressReport progressReport)
    {
        this.progressReport = progressReport;
    }

    public T8ProcessNotificationHandle getNotificationHandle()
    {
        return notificationHandle;
    }

    public void setNotificationHandle(T8ProcessNotificationHandle notificationHandle)
    {
        this.notificationHandle = notificationHandle;
    }

    public boolean isFinalizeOnCompletion()
    {
        return finalizeOnCompletion;
    }

    public void setFinalizeOnCompletion(boolean finalizeOnCompletion)
    {
        this.finalizeOnCompletion = finalizeOnCompletion;
    }

    public void setVisibilityRestrictionWorkflowProfileIds(List<String> workflowProfileIds)
    {
        this.visibilityRestrictionWorkflowProfileIds = workflowProfileIds;
    }

    public List<String> getVisibilityRestrictionWorkflowProfileIds()
    {
        return visibilityRestrictionWorkflowProfileIds == null ? new ArrayList<String>(0) : visibilityRestrictionWorkflowProfileIds;
    }

    public void setVisibilityRestrictionProfileIds(List<String> profileIds)
    {
        this.visibilityRestrictionProfileIds = profileIds;
    }

    public List<String> getVisibilityRestrictionProfileIds()
    {
        return visibilityRestrictionProfileIds == null ? new ArrayList<String>(0) : visibilityRestrictionProfileIds;
    }

    public void setVisibilityRestrictionUserIds(List<String> userIds)
    {
        this.visibilityRestrictionUserIds = userIds;
    }

    public List<String> getVisibilityRestrictionUserIds()
    {
        return visibilityRestrictionUserIds == null ? new ArrayList<String>(0) : visibilityRestrictionUserIds;
    }

    @Override
    public String toString()
    {
        return "T8ProcessDetails{" + "projectId=" + projectId + ", processId=" + processId + ", processIid=" + processIid + '}';
    }
}
