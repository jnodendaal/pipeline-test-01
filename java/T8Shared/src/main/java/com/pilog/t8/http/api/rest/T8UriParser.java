package com.pilog.t8.http.api.rest;

/**
 * @author Bouwer du Preez
 */
public interface T8UriParser
{
    public boolean isApplicable(String urlString);
    public T8UriParseResult parseUri(T8UriParseResult precedingParseResult, String method, String uri);
}
