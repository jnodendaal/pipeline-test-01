package com.pilog.t8.exception;

/**
 * @author Hennie Brink
 */
public class T8DefaultUserException extends Exception implements T8UserException
{

    public T8DefaultUserException()
    {
    }

    public T8DefaultUserException(String message)
    {
        super(message);
    }

    public T8DefaultUserException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public T8DefaultUserException(Throwable cause)
    {
        super(cause);
    }

    public T8DefaultUserException(String message, Throwable cause,
                                  boolean enableSuppression,
                                  boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getUserMessage()
    {
        return getMessage();
    }

}
