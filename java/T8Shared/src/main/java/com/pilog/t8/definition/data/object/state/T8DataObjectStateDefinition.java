package com.pilog.t8.definition.data.object.state;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectStateDefinition extends T8StateGraphNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_STATE";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Object State";
    public static final String DESCRIPTION = "A state in which a data object in the system can exist.";
    public static final String IDENTIFIER_PREFIX = "O_STATE_";
    public enum Datum
    {
        CONCEPT_ID,
        CREATOR_FUNCTIONALITY_IDS,
        STATE_TRANSITION_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8DataObjectStateDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.CONCEPT_ID.toString(), "Concept Id", "The Concept identifier linked to this state."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.CREATOR_FUNCTIONALITY_IDS.toString(), "Creator Functionalities", "The functionalities (if any) that may created objects with this as its initial state."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STATE_TRANSITION_DEFINITIONS.toString(), "State Transitions", "The state transitions that may occur from this state."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.STATE_TRANSITION_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataObjectStateTransitionDefinition.TYPE_IDENTIFIER));
        else if (Datum.CREATOR_FUNCTIONALITY_IDS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.STATE_TRANSITION_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8DataObjectState getNewStateInstance(T8ServerContext serverContext, T8SessionContext sessionContext, String graphId) throws Exception
    {
        T8DataObjectState state;

        state = new T8DataObjectState(graphId, getIdentifier(), getConceptId());
        state.setCreatorFunctionalityIds(getCreatorFunctionalityIds());
        for (T8DataObjectStateTransitionDefinition transitionDefinition : getStateTransitionDefinitions())
        {
            state.addTransition(transitionDefinition.getNewStateTransition());
        }

        return state;
    }

    public String getConceptId()
    {
        return getDefinitionDatum(Datum.CONCEPT_ID);
    }

    public void setConceptId(String conceptId)
    {
        setDefinitionDatum(Datum.CONCEPT_ID, conceptId);
    }

    public List<String> getCreatorFunctionalityIds()
    {
        List<String> value;

        value = getDefinitionDatum(Datum.CREATOR_FUNCTIONALITY_IDS);
        return value != null ? value : new ArrayList<>();
    }

    public void setCreatorFunctionalityIds(List<String> ids)
    {
        setDefinitionDatum(Datum.CREATOR_FUNCTIONALITY_IDS, ids);
    }

    public List<T8DataObjectStateTransitionDefinition> getStateTransitionDefinitions()
    {
        return getDefinitionDatum(Datum.STATE_TRANSITION_DEFINITIONS);
    }

    public void setStateTransitionDefinitions(List<T8DataObjectStateTransitionDefinition> definitions)
    {
        setDefinitionDatum(Datum.STATE_TRANSITION_DEFINITIONS, definitions);
    }
}
