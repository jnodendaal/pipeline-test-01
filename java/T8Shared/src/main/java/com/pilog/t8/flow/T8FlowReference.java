package com.pilog.t8.flow;

import com.pilog.t8.flow.T8Flow.FlowStatus;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowReference implements Serializable
{
    private String flowInstanceIdentifier;
    private FlowStatus flowStatus;
    private String initiatorNodeIdentifier;
    private String initiatorUserIdentifier;
    private String initiatorProfileIdentifier;
    private boolean updated;
    private boolean inserted;
    
    public T8FlowReference(String flowInstanceIdentifier)
    {
        this.flowInstanceIdentifier = flowInstanceIdentifier;
        this.updated = false;
        this.inserted = true;
    }
    
    public T8FlowReference(String flowInstanceIdentifier, String initiatorNodeIdentifier, String initiatorUserIdentifier, String initiatorUserProfileIdentifier, FlowStatus flowStatus)
    {
        this(flowInstanceIdentifier);
        this.initiatorNodeIdentifier = initiatorNodeIdentifier;
        this.initiatorUserIdentifier = initiatorUserIdentifier;
        this.initiatorProfileIdentifier = initiatorUserProfileIdentifier;
        this.flowStatus = flowStatus;
    }
    
    public boolean isUpdated()
    {
        return updated;
    }
    
    public void resetFlags()
    {
        inserted = false;
        updated = false;
    }

    public String getFlowInstanceIdentifier()
    {
        return flowInstanceIdentifier;
    }

    public void setFlowInstanceIdentifier(String flowInstanceIdentifier)
    {
        this.flowInstanceIdentifier = flowInstanceIdentifier;
    }

    public FlowStatus getFlowStatus()
    {
        return flowStatus;
    }

    public void setFlowStatus(FlowStatus flowStatus)
    {
        this.flowStatus = flowStatus;
    }

    public String getInitiatorNodeIdentifier()
    {
        return initiatorNodeIdentifier;
    }

    public void setInitiatorNodeIdentifier(String initiatorNodeIdentifier)
    {
        this.initiatorNodeIdentifier = initiatorNodeIdentifier;
    }

    public String getInitiatorUserIdentifier()
    {
        return initiatorUserIdentifier;
    }

    public void setInitiatorUserIdentifier(String initiatorUserIdentifier)
    {
        this.initiatorUserIdentifier = initiatorUserIdentifier;
    }

    public String getInitiatorProfileIdentifier()
    {
        return initiatorProfileIdentifier;
    }

    public void setInitiatorProfileIdentifier(String initiatorProfileIdentifier)
    {
        this.initiatorProfileIdentifier = initiatorProfileIdentifier;
    }
    
    
}
