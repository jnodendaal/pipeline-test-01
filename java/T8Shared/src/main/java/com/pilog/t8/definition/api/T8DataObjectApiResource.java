package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_OBJ_RETRIEVE = "@OS_API_OBJ_RETRIEVE";
    public static final String OPERATION_API_OBJ_SEARCH = "@OS_API_OBJ_SEARCH";
    public static final String OPERATION_API_OBJ_SEARCH_GROUP = "@OS_API_OBJ_SEARCH_GROUP";

    public static final String PARAMETER_OBJECT_ID = "$P_OBJECT_ID";
    public static final String PARAMETER_OBJECT_IID = "$P_OBJECT_IID";
    public static final String PARAMETER_OBJECT_FILTER = "$P_OBJECT_FILTER";
    public static final String PARAMETER_GROUP_ID = "$P_GROUP_ID";
    public static final String PARAMETER_SEARCH_EXPRESSION = "$P_SEARCH_EXPRESSION";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";
    public static final String PARAMETER_OBJECT = "$P_OBJECT";
    public static final String PARAMETER_OBJECTS = "$P_OBJECTS";
    public static final String PARAMETER_INCLUDE_STATE = "$P_INCLUDE_STATE";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;

            definition = new T8JavaServerOperationDefinition(OPERATION_API_OBJ_RETRIEVE);
            definition.setMetaDisplayName("Retrieve Data Object");
            definition.setMetaDescription("Returns the entire T8DataObject for the given input paramaters.");
            definition.setClassName("com.pilog.t8.api.T8DataObjectApiOperations$ApiRetrieve");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECT_ID, "Data Object Id", "The data object id for the object that needs to be retrieved.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECT_IID, "Data Object Iid", "The data object iid for the object that needs to be retrieved.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_STATE, "Include State Flag", "If set to true, the state information for each of the retrieved objects will also be retrieved and attached.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECT, "Data Object", "The T8DataObject that was retrieved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_OBJ_SEARCH);
            definition.setMetaDisplayName("Search Data Objects");
            definition.setMetaDescription("Returns the list of T8DataObjects retrieved using the supplied search criteria.");
            definition.setClassName("com.pilog.t8.api.T8DataObjectApiOperations$ApiSearch");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECT_ID, "Data Object Id", "The data object id for the object that needs to be retrieved.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECT_FILTER, "Data Object Filter", "The data object filter to apply to search results retrieved.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_EXPRESSION, "Search Expression", "The expression to use for the object search.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_STATE, "Include State Flag", "If set to true, the state information for each of the retrieved objects will also be retrieved and attached.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page to retrieve.", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The number of retrieved to return.", T8DataType.INTEGER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECTS, "Data Objects", "The list of retrieved objects.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_OBJ_SEARCH_GROUP);
            definition.setMetaDisplayName("Search Data Object Grous");
            definition.setMetaDescription("Returns the list of T8DataObjects retrieved from the specified group using the supplied search criteria.");
            definition.setClassName("com.pilog.t8.api.T8DataObjectApiOperations$ApiSearchGroup");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_GROUP_ID, "Data Object Group Id", "The id for the data object group from which data objects will be retrieved retrieved.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECT_FILTER, "Data Object Filter", "The data object filter to apply to search results retrieved.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_EXPRESSION, "Search Expression", "The expression to use for the object search.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_STATE, "Include State Flag", "If set to true, the state information for each of the retrieved objects will also be retrieved and attached.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page to retrieve.", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The number of retrieved to return.", T8DataType.INTEGER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OBJECTS, "Data Objects", "The list of retrieved objects.", T8DataType.LIST));
            definitions.add(definition);
        }

        return definitions;
    }

}
