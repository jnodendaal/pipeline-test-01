package com.pilog.t8.data.filter;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterCriterionDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.date.Dates;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilterCriterion implements T8DataFilterClause, Serializable
{
    private String identifier;
    private String fieldIdentifier;
    private DataFilterOperator operator;
    private Object filterValue;
    private boolean caseInsensitive;
    private String entityIdentifier;

    private transient String cachedWhereClause;
    private transient List<Object> cachedWhereClauseParameters;

    public enum DataFilterOperator
    {
        EQUAL("Equal To"),
        NOT_EQUAL("Not Equal To"),
        GREATER_THAN("Greater Than"),
        GREATER_THAN_OR_EQUAL("Greater Than or Equal To"),
        LESS_THAN("Less than"),
        LESS_THAN_OR_EQUAL("Less Than or Equal To"),
        LIKE("Containing"),
        STARTS_WITH("Beginning With"),
        ENDS_WITH("Ending With"),
        MATCHES("Matching"),
        NOT_LIKE("Not Containing"),
        /**
         * The {@code T8DataFilterClause} contract allows for {@code Collection}
         * types to be used as the filter value, in which case the IN clause is
         * built from the collection.
         **/
        IN("In"),
        /** See {@link #IN} **/
        NOT_IN("Not In"),
        IS_NULL("Is Empty"),
        IS_NOT_NULL("Is Not Empty"),
        THIS_DAY("This Day"),
        CONTAINS("Full-Text"),
        /** Special operator when one column is checked for equality to another rather than a parameter value. */
        EQUAL_TO_FIELD("Equal To Field");

        private final String displayString;

        DataFilterOperator(String displayString)
        {
            this.displayString = displayString;
        }

        public String getDisplayString()
        {
            return this.displayString;
        }
    };

    public T8DataFilterCriterion(String fieldIdentifier, DataFilterOperator operator, Object filterValue)
    {
        this.fieldIdentifier = fieldIdentifier;
        this.operator = operator;
        this.filterValue = filterValue;
    }

    public T8DataFilterCriterion(String fieldIdentifier, DataFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(fieldIdentifier, operator, filterValue);
        this.caseInsensitive = caseInsensitive;
    }

    public T8DataFilterCriterion(String identifier, String fieldIdentifier, DataFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(fieldIdentifier, operator, filterValue, caseInsensitive);
        this.identifier = identifier;
    }

    public T8DataFilterCriterion(T8DataFilterCriterionDefinition definition, String fieldIdentifier, DataFilterOperator operator, Object filterValue)
    {
        this(fieldIdentifier, operator, filterValue);
        this.identifier = definition.getIdentifier();
    }

    public T8DataFilterCriterion(T8DataFilterCriterionDefinition definition, String fieldIdentifier, DataFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(fieldIdentifier, operator, filterValue, caseInsensitive);
        this.identifier = definition.getIdentifier();
    }

    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public T8DataFilterCriterion copy()
    {
        T8DataFilterCriterion copiedCriterion;

        copiedCriterion = new T8DataFilterCriterion((String)identifier, fieldIdentifier, operator, filterValue, caseInsensitive);
        return copiedCriterion;
    }

    @Override
    public Set<String> getFilterFieldIdentifiers()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<String>();
        fieldSet.add(fieldIdentifier);

        return fieldSet;
    }

    @Override
    public List<T8DataFilterCriterion> getFilterCriterionList()
    {
        return ArrayLists.typeSafeList(this);
    }

    public String getFieldIdentifier()
    {
        return fieldIdentifier;
    }

    public void setFieldIdentifier(String fieldIdentifier)
    {
        clearCache();
        this.fieldIdentifier = fieldIdentifier;
    }

    public Object getFilterValue()
    {
        return filterValue;
    }

    public void setFilterValue(Object filterValue)
    {
        clearCache();
        this.filterValue = filterValue;
    }

    public DataFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(DataFilterOperator operator)
    {
        clearCache();
        this.operator = operator;
    }

    public boolean isCaseInsensitive()
    {
        return caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        clearCache();
        this.caseInsensitive = caseInsensitive;
    }

    private void clearCache()
    {
        cachedWhereClause = null;
        cachedWhereClauseParameters = null;
    }

    @Override
    public String getEntityIdentifier()
    {
        return entityIdentifier;
    }

    @Override
    public void setEntityIdentifier(String entityIdentifier)
    {
        clearCache();
        this.entityIdentifier = entityIdentifier;
        if (fieldIdentifier != null)
        {
            fieldIdentifier = T8IdentifierUtilities.setNamespace(fieldIdentifier, entityIdentifier);
        }
    }

    @Override
    public boolean hasCriteria()
    {
        if (operator == DataFilterOperator.IS_NULL) return true; // This operator does not require a filter value.
        else if (operator == DataFilterOperator.IS_NOT_NULL) return true; // This operator does not require a filter value.
        else if (filterValue == null) return false; // All other operators require a filter value.
        else if (filterValue instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return !Strings.isNullOrEmpty((String)filterValue);
        }
        else return true;
    }


    @Override
    public StringBuffer getWhereClause(T8DataTransaction dataAccessProvider, String sqlIdentifier)
    {
        if (cachedWhereClause != null)
        {
            return new StringBuffer(cachedWhereClause);
        }
        else
        {
            StringBuffer whereClause;
            String dsFieldIdentifier;
            String fieldSourceIdentifier;
            T8DataSourceFieldDefinition dsFieldDefinition;
            T8DataConnection dataConnection;
            T8DataEntityDefinition dataEntityDefinition;
            T8DataSourceDefinition dataSourceDefinition;
            T8DatabaseAdaptor adaptor;

            // Fetch the data access definitions required.
            dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(getEntityIdentifier());
            dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
            dataConnection = (dataAccessProvider.getDataConnection(((T8DataConnectionBasedSourceDefinition)dataSourceDefinition).getDataConnectionIdentifier()));
            adaptor = dataConnection.getDatabaseAdaptor();

            // Get the data source field mapped to the entity field.
            dsFieldIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
            if (dsFieldIdentifier == null) throw new RuntimeException("Entity '" + dataEntityDefinition + "' does not contain a valid source mapping for field '" + fieldIdentifier + "' as required by filter criterion: " + this);

            // Get the source identifier mapped to the data source field.
            dsFieldDefinition = dataSourceDefinition.getFieldDefinition(dsFieldIdentifier);
            fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(dsFieldIdentifier);
            if (fieldSourceIdentifier == null) throw new RuntimeException("Data Source '" + dataSourceDefinition + "' does not contain a valid source mapping for field '" + dsFieldIdentifier + "' as requied by filter criterion: " + this);

            // Construct the WHERE clause.
            whereClause = new StringBuffer();
            if (operator == DataFilterOperator.MATCHES) // Regular expression matching is handled in a DB specific way using DB Adaptor.
            {
                whereClause.append(adaptor.getRegularExpressionLike(fieldSourceIdentifier, "" + filterValue));
            }
            else if (operator == DataFilterOperator.CONTAINS)
            {
                whereClause.append(adaptor.getSQLParameterizedColumnContains(fieldSourceIdentifier));
            }
            else // All other operators can be added using generic SQL.
            {
                boolean convertToUpperCase;
                boolean guidField;

                guidField = T8DataUtilities.isGUIDDataType(dsFieldDefinition.getDataType()); // GUID fields are a special case of comparison.
                convertToUpperCase = ((!guidField) && caseInsensitive);
                whereClause.append(convertToUpperCase ? "UPPER(" + fieldSourceIdentifier + ")" : fieldSourceIdentifier);
                if (operator == DataFilterOperator.EQUAL)
                {
                    String parameterString;

                    parameterString = guidField ? adaptor.getSQLParameterizedHexToGUID() : "?";
                    whereClause.append(" = ").append(parameterString);
                }
                else if (operator == DataFilterOperator.NOT_EQUAL)
                {
                    String parameterString;

                    parameterString = guidField ? adaptor.getSQLParameterizedHexToGUID() : "?";
                    whereClause.append(" <> ").append(parameterString);
                }
                else if (operator == DataFilterOperator.GREATER_THAN)
                {
                    whereClause.append(" > ?");
                }
                else if (operator == DataFilterOperator.GREATER_THAN_OR_EQUAL)
                {
                    whereClause.append(" >= ?");
                }
                else if (operator == DataFilterOperator.LESS_THAN)
                {
                    whereClause.append(" < ?");
                }
                else if (operator == DataFilterOperator.LESS_THAN_OR_EQUAL)
                {
                    whereClause.append(" <= ?");
                }
                else if (operator == DataFilterOperator.LIKE)
                {
                    whereClause.append(" LIKE ?");
                    if ((filterValue != null) && (adaptor.requiresSqlLikeEscape(filterValue.toString())))
                    {
                        whereClause.append(" ESCAPE '\\'");
                    }
                }
                else if (operator == DataFilterOperator.STARTS_WITH)
                {
                    whereClause.append(" LIKE ?");
                    if ((filterValue != null) && (adaptor.requiresSqlLikeEscape(filterValue.toString())))
                    {
                        whereClause.append(" ESCAPE '\\'");
                    }
                }
                else if (operator == DataFilterOperator.ENDS_WITH)
                {
                    whereClause.append(" LIKE ?");
                    if ((filterValue != null) && (adaptor.requiresSqlLikeEscape(filterValue.toString())))
                    {
                        whereClause.append(" ESCAPE '\\'");
                    }
                }
                else if (operator == DataFilterOperator.NOT_LIKE)
                {
                    whereClause.append(" NOT LIKE ?");
                    if ((filterValue != null) && (adaptor.requiresSqlLikeEscape(filterValue.toString())))
                    {
                        whereClause.append(" ESCAPE '\\'");
                    }
                }
                else if (operator == DataFilterOperator.IS_NULL)
                {
                    whereClause.append(" IS NULL");
                }
                else if (operator == DataFilterOperator.IS_NOT_NULL)
                {
                    whereClause.append(" IS NOT NULL");
                }
                else if ((operator == DataFilterOperator.IN) || (operator == DataFilterOperator.NOT_IN))
                {
                    if (filterValue instanceof Collection)
                    {
                        Iterator<?> valueIterator;
                        Collection<?> valueCollection;
                        String parameterString;

                        parameterString = guidField ? adaptor.getSQLParameterizedHexToGUID() : "?";
                        valueCollection = (Collection)filterValue;
                        valueIterator = valueCollection.iterator();

                        whereClause.append((operator == DataFilterOperator.IN) ? " IN (" : " NOT IN (");
                        while (valueIterator.hasNext())
                        {
                            valueIterator.next();
                            whereClause.append(parameterString);
                            if (valueIterator.hasNext()) whereClause.append(", ");
                        }
                        whereClause.append(")");
                    }
                    else if (filterValue instanceof T8DataFilter) // A sub-query is used.
                    {
                        T8DataFilter subDataFilter;
                        Map<String, String> fieldMapping;

                        subDataFilter = (T8DataFilter)filterValue;

                        fieldMapping = new HashMap<>();
                        fieldMapping.put(fieldIdentifier, fieldIdentifier);

                        whereClause.append((operator == DataFilterOperator.IN) ? " IN (SELECT " : " NOT IN (SELECT ");
                        whereClause.append(fieldSourceIdentifier);
                        whereClause.append(" FROM (");
                        whereClause.append(((T8SQLQueryDataSourceDefinition)dataSourceDefinition).createQuery(dataAccessProvider, subDataFilter, fieldMapping));
                        whereClause.append(") subFilter )");
                    }
                    else if (filterValue instanceof T8SubDataFilter) // A sub-query is used.
                    {
                        T8DataSourceDefinition subFilterSourceDefinition;
                        T8DataEntityDefinition subFilterEntityDefinition;
                        T8SubDataFilter subDataFilter;
                        T8DataFilter subQueryFilter;
                        String subFilterEntityIdentifier;

                        subDataFilter = (T8SubDataFilter)filterValue;
                        subQueryFilter = subDataFilter.getDataFilter();
                        subFilterEntityIdentifier = subQueryFilter.getEntityIdentifier();

                        // Fetch the data access definitions required for the sub-query.
                        subFilterEntityDefinition = dataAccessProvider.getDataEntityDefinition(subFilterEntityIdentifier);
                        subFilterSourceDefinition = dataAccessProvider.getDataSourceDefinition(subFilterEntityDefinition.getDataSourceIdentifier());

                        // Append the sub-query.
                        if (subFilterSourceDefinition instanceof T8SQLQueryDataSourceDefinition)
                        {
                            whereClause.append((operator == DataFilterOperator.IN) ? " IN (SELECT " : " NOT IN (SELECT ");
                            whereClause.append(fieldSourceIdentifier);
                            whereClause.append(" FROM (");
                            whereClause.append(((T8SQLQueryDataSourceDefinition)subFilterSourceDefinition).createQuery(dataAccessProvider, subQueryFilter, subDataFilter.getSubFilterFieldMapping()));
                            whereClause.append(") subFilter )");
                        }
                        else throw new RuntimeException("Cannot create sub-query from data source: " + dataSourceDefinition);
                    }
                    else throw new RuntimeException("Invalid parameter type used with IN clause: " + filterValue);
                }
                else if (operator == DataFilterOperator.THIS_DAY)
                {
                    whereClause.append(" BETWEEN ? AND ?");
                }
                else if (com.pilog.t8.utilities.objects.Objects.in(operator, DataFilterOperator.EQUAL_TO_FIELD)) // Just add on any new additions for field matching
                {
                    // This match won't have a parameter, instead the matching column for the
                    // identifier stored in the filter value is used
                    whereClause.append(buildMatchColumnFilter(dataEntityDefinition, dataSourceDefinition));
                }
            }

            // Cache the constructed where clause and return it.
            cachedWhereClause = whereClause.toString();
            return whereClause;
        }
    }

    private String buildMatchColumnFilter(T8DataEntityDefinition dataEntityDefinition, T8DataSourceDefinition dataSourceDefinition)
    {
        String matchColumnFieldSourceIdentifier;
        String matchColumnDSFieldIdentifier;

        // Check that the value is a string, since it needs to be an identifier
        if (!(this.filterValue instanceof String)) throw new RuntimeException("Invalid filter value type found for column match. Filter value : {"+this.filterValue+"}");

        // Check that there is a valid entity-DS mapping for the identifier
        matchColumnDSFieldIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier((String)this.filterValue);
        if (matchColumnDSFieldIdentifier == null) throw new RuntimeException("Entity '" + dataEntityDefinition + "' does not contain a valid source mapping for field '" + this.filterValue + "' as required by match column filter criterion: " + this);

        // Check that there is a valid DS-source mapping for the identifier
        matchColumnFieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(matchColumnDSFieldIdentifier);
        if (matchColumnFieldSourceIdentifier == null) throw new RuntimeException("Data Source '" + dataSourceDefinition + "' does not contain a valid source mapping for field '" + matchColumnDSFieldIdentifier + "' as requied by match column filter criterion: " + this);

        // Return the actual string to be used in the where clause
        // Add cases for any additional field matching operators
        switch (this.operator)
        {
            case EQUAL_TO_FIELD:
                return " = " + matchColumnFieldSourceIdentifier;
            default:
                throw new RuntimeException("Unhandled column matching operator found : " + this.operator);
        }
    }

    @Override
    public List<Object> getWhereClauseParameters(T8DataTransaction dataAccessProvider)
    {
        if (cachedWhereClauseParameters != null)
        {
            return cachedWhereClauseParameters;
        }
        else
        {
            T8DataSourceFieldDefinition dsFieldDefinition;
            String fieldSourceIdentifier;
            ArrayList<Object> parameterList;
            T8DataEntityDefinition dataEntityDefinition;
            T8DataSourceDefinition dataSourceDefinition;
            T8DataConnection dataConnection;
            T8DatabaseAdaptor dbAdaptor;
            boolean convertToUpperCase;
            boolean guidField;

            // Fetch the data access definitions required.
            dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(getEntityIdentifier());
            dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
            dataConnection = (dataAccessProvider.getDataConnection(((T8DataConnectionBasedSourceDefinition)dataSourceDefinition).getDataConnectionIdentifier()));
            fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
            dsFieldDefinition = dataSourceDefinition.getFieldDefinition(fieldSourceIdentifier);
            fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
            dbAdaptor = dataConnection.getDatabaseAdaptor();
            guidField = T8DataUtilities.isGUIDDataType(dsFieldDefinition.getDataType()); // GUID fields are a special case of comparison.
            convertToUpperCase = ((!guidField) && caseInsensitive);

            // Check the operator to handle specific cases of filter parameter conversion.
            parameterList = new ArrayList<>();
            if ((operator == DataFilterOperator.LIKE) || (operator == DataFilterOperator.NOT_LIKE))
            {
                String parameterString;

                parameterString = convertToUpperCase ? ((String)filterValue).toUpperCase() : filterValue.toString();
                if (dbAdaptor.requiresSqlLikeEscape(parameterString)) parameterString = dbAdaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                parameterString = "%" + parameterString + "%";

                parameterList.add(parameterString);
            }
            else if (operator == DataFilterOperator.STARTS_WITH)
            {
                String parameterString;

                parameterString = convertToUpperCase ? ((String)filterValue).toUpperCase() : filterValue.toString();
                if (dbAdaptor.requiresSqlLikeEscape(parameterString)) parameterString = dbAdaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                parameterString = parameterString + "%";

                parameterList.add(parameterString);
            }
            else if (operator == DataFilterOperator.ENDS_WITH)
            {
                String parameterString;

                parameterString = convertToUpperCase ? ((String)filterValue).toUpperCase() : filterValue.toString();
                if (dbAdaptor.requiresSqlLikeEscape(parameterString)) parameterString = dbAdaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                parameterString = "%" + parameterString;

                parameterList.add(parameterString);
            }
            else if ((operator == DataFilterOperator.IN) || (operator == DataFilterOperator.NOT_IN))
            {
                if (filterValue != null)
                {
                    if (filterValue instanceof Collection)
                    {
                        Collection valueList;

                        // Get the value list.
                        valueList = (Collection)filterValue;

                        // If its a guid field, we have to convert the filter value because of database vendor dependent implementations.
                        if (T8DataUtilities.isGUIDDataType(dsFieldDefinition.getDataType()))
                        {
                            // Iterate over all values in the list and if a value is not null, convert it.
                            for (Object value : valueList)
                            {
                                if (value != null)
                                {
                                    parameterList.add(dbAdaptor.convertHexStringToSQLGUIDString(value.toString()));
                                }
                                else
                                {
                                    parameterList.add(null);
                                }
                            }
                        }
                        else
                        {
                            parameterList.addAll(valueList);
                        }
                    }
                    else if (filterValue instanceof T8DataFilter) // A sub-query is used.
                    {
                        T8DataFilter subDataFilter;

                        subDataFilter = (T8DataFilter)filterValue;
                        parameterList.addAll(subDataFilter.getWhereClauseParameters(dataAccessProvider));
                    }
                    else if (filterValue instanceof T8SubDataFilter) // A sub-query is used.
                    {
                        T8SubDataFilter subDataFilter;

                        subDataFilter = (T8SubDataFilter)filterValue;
                        parameterList.addAll(subDataFilter.getDataFilter().getWhereClauseParameters(dataAccessProvider));
                    }
                }
            }
            else if (operator == DataFilterOperator.THIS_DAY)
            {
                if (filterValue == null)
                {
                    // Do nothing, since we do not want to handle null dates for this operator
                }
                else if (filterValue instanceof String || filterValue instanceof T8Timestamp)
                {
                    String thisDay;

                    thisDay = Dates.stripTime(filterValue.toString());
                    parameterList.add(thisDay);
                    parameterList.add(Dates.nextDay(thisDay, Dates.STD_SYS_DT_FORMAT));
                } else throw new UnsupportedOperationException("Value type {"+filterValue.getClass()+"} not yet handled by THIS_DAY operator.");
            }
            else if ((operator == DataFilterOperator.IS_NULL) || (operator == DataFilterOperator.IS_NOT_NULL))
            {
                // Do nothing, since no filter value is required for this operator type.
            }
            else if (operator == DataFilterOperator.MATCHES)
            {
                // Do nothing, since no filter value is required for this operator type.
            }
            else if (operator == DataFilterOperator.CONTAINS)
            {
                parameterList.add(filterValue);
            }
            else if (com.pilog.t8.utilities.objects.Objects.in(operator, DataFilterOperator.EQUAL_TO_FIELD)) // Just add on any new additions for column matching
            {
                // Do nothing, since the filter value is already added in the form of a column name match
            }
            else if (filterValue instanceof String)
            {
                // If its a guid field, we have to convert the filter value because of database vendor dependent implementations.
                if (T8DataUtilities.isGUIDDataType(dsFieldDefinition.getDataType()) && (filterValue != null))
                {
                    parameterList.add(dbAdaptor.convertHexStringToSQLGUIDString(filterValue.toString()));
                }
                else
                {
                    parameterList.add(convertToUpperCase ? ((String)filterValue).toUpperCase() : filterValue);
                }
            }
            else
            {
                // If its a guid field, we have to convert the filter value because of database vendor dependent implementations.
                if (T8DataUtilities.isGUIDDataType(dsFieldDefinition.getDataType()) && (filterValue != null))
                {
                    parameterList.add(dbAdaptor.convertHexStringToSQLGUIDString(filterValue.toString()));
                }
                else
                {
                    parameterList.add(filterValue);
                }
            }

            // Cache the constructed list and return it.
            cachedWhereClauseParameters = Collections.unmodifiableList(parameterList);
            return parameterList;
        }
    }

    @Override
    public boolean includesEntity(T8DataEntity entity)
    {
        Object value;

        value = entity.getFieldValue(fieldIdentifier);
        if (value == null)
        {
            if (operator == DataFilterOperator.EQUAL) return filterValue == null;
            else if (operator == DataFilterOperator.NOT_EQUAL) return filterValue != null;
            else if (operator == DataFilterOperator.GREATER_THAN) return filterValue == null;
            else if (operator == DataFilterOperator.GREATER_THAN_OR_EQUAL) return filterValue == null;
            else if (operator == DataFilterOperator.LESS_THAN) return filterValue == null;
            else if (operator == DataFilterOperator.LESS_THAN_OR_EQUAL) return filterValue == null;
            else if (operator == DataFilterOperator.LIKE) return filterValue == null;
            else if (operator == DataFilterOperator.NOT_LIKE) return filterValue == null;
            else if (operator == DataFilterOperator.IN) return filterValue == null;
            else if (operator == DataFilterOperator.NOT_IN) return filterValue == null;
            else if (operator == DataFilterOperator.IS_NULL) return true;
            else if (operator == DataFilterOperator.IS_NOT_NULL) return false;
            else return false;
        }
        else
        {
            if (operator == DataFilterOperator.EQUAL) return equal(value, filterValue);
            else if (operator == DataFilterOperator.NOT_EQUAL) return !equal(value, filterValue);
            else if (operator == DataFilterOperator.GREATER_THAN) return greaterThan(value, filterValue);
            else if (operator == DataFilterOperator.GREATER_THAN_OR_EQUAL) return greaterThanOrEqual(value, filterValue);
            else if (operator == DataFilterOperator.LESS_THAN) return lessThan(value, filterValue);
            else if (operator == DataFilterOperator.LESS_THAN_OR_EQUAL) return lessThanOrEqual(value, filterValue);
            else if (operator == DataFilterOperator.LIKE) return like(value, (String)filterValue);
            else if (operator == DataFilterOperator.NOT_LIKE) return !like(value, (String)filterValue);
            else if (operator == DataFilterOperator.STARTS_WITH) return startsWith(value, (String)filterValue);
            else if (operator == DataFilterOperator.ENDS_WITH) return endsWith(value, (String)filterValue);
            else if (operator == DataFilterOperator.IN)
            {
                Iterator<?> valueIterator;
                Collection<?> valueList;

                valueList = (Collection)filterValue;
                valueIterator = valueList.iterator();

                while (valueIterator.hasNext())
                {
                    Object inObject;

                    inObject = valueIterator.next();
                    if (value.equals(inObject)) return true;
                }

                return false;
            }
            else if (operator == DataFilterOperator.NOT_IN)
            {
                Iterator<?> valueIterator;
                List<?> valueList;

                valueList = (List)filterValue;
                valueIterator = valueList.iterator();

                while (valueIterator.hasNext())
                {
                    Object inObject;

                    inObject = valueIterator.next();
                    if (value.equals(inObject)) return false;
                }

                return true;
            }
            else if (operator == DataFilterOperator.IS_NULL) return false;
            else if (operator == DataFilterOperator.IS_NOT_NULL) return true;
            else return false;
        }
    }

    @Override
    public T8DataFilterCriterion getFilterCriterion(String identifier)
    {
        if ((this.identifier != null) && (this.identifier.equals(identifier))) return this;
        else return null;
    }

    private boolean startsWith(Object value, String filterValue)
    {
        if (filterValue != null)
        {
            if (filterValue.endsWith("%"))
            {
                return like(value, filterValue);
            }
            else
            {
                return like(value, filterValue + "%");
            }
        }
        else return false;
    }

    private boolean endsWith(Object value, String filterValue)
    {
        if (filterValue != null)
        {
            if (filterValue.startsWith("%"))
            {
                return like(value, filterValue);
            }
            else
            {
                return like(value, "%" + filterValue);
            }
        }
        else return false;
    }

    private boolean like(Object value, String filterValue)
    {
        if (filterValue != null)
        {
            boolean endsWildcard;
            boolean startsWildcard;
            String valueString;
            String filterString;

            valueString = "" + value;
            filterString = filterValue.replace("%", "");

            if (caseInsensitive)
            {
                valueString = valueString.toUpperCase();
                filterString = filterString.toUpperCase();
            }

            endsWildcard = filterValue.endsWith("%");
            startsWildcard = filterValue.startsWith("%");
            if (startsWildcard && endsWildcard)
            {
                return valueString.contains(filterString);
            }
            else if (startsWildcard)
            {
                return valueString.endsWith(filterString);
            }
            else if (endsWildcard)
            {
                return valueString.startsWith(filterString);
            }
            else
            {
                return valueString.contains(filterString);
            }
        }
        else return false;
    }

    private boolean equal(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            String valueString;
            String filterString;

            valueString = "" + value;
            filterString = "" + filterValue;

            if (caseInsensitive)
            {
                valueString = valueString.toUpperCase();
                filterString = filterString.toUpperCase();
            }

            return valueString.equals(filterString);
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) == ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) == 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Long)
        {
            if (filterValue instanceof Long)
            {
                return Long.compare((Long)value, (Long)filterValue) == 0;
            } else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if(value instanceof Boolean)
        {
            if (filterValue instanceof Boolean)
            {
                return Objects.equals(value, filterValue);
            } else if (filterValue instanceof String)
            {
                return Objects.equals(value, Boolean.parseBoolean((String) filterValue));
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }

    private boolean greaterThan(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            if (filterValue instanceof String)
            {
                return ((String)value).compareTo((String)filterValue) > 0;
            }
            else return ((String)value).compareTo("" + filterValue) > 0;
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) > ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) > 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Long)
        {
            if (filterValue instanceof Long)
            {
                return Long.compare((Long)value, (Long)filterValue) > 0;
            } else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Date)
        {
            if (filterValue instanceof Date)
            {
                return ((Date)value).after((Date)filterValue);
            }
            else if (filterValue instanceof T8Timestamp)
            {
                return ((Date)value).after(new Date(((T8Timestamp)filterValue).getMilliseconds()));
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }

    private boolean greaterThanOrEqual(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            if (filterValue instanceof String)
            {
                return ((String)value).compareTo((String)filterValue) >= 0;
            }
            else return ((String)value).compareTo("" + filterValue) >= 0;
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) >= ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) >= 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Long)
        {
            if (filterValue instanceof Long)
            {
                return Long.compare((Long)value, (Long)filterValue) >= 0;
            } else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Date)
        {
            if (filterValue instanceof Date)
            {
                return ((Date)value).after((Date)filterValue) || ((Date)value).equals(filterValue);
            }
            else if (filterValue instanceof T8Timestamp)
            {
                return ((Date)value).after(new Date(((T8Timestamp)filterValue).getMilliseconds())) || ((Date)value).equals(new Date(((T8Timestamp)filterValue).getMilliseconds()));
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }

    private boolean lessThan(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            if (filterValue instanceof String)
            {
                return ((String)value).compareTo((String)filterValue) < 0;
            }
            else return ((String)value).compareTo("" + filterValue) < 0;
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) < ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) < 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Long)
        {
            if (filterValue instanceof Long)
            {
                return Long.compare((Long)value, (Long)filterValue) < 0;
            } else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Date)
        {
            if (filterValue instanceof Date)
            {
                return ((Date)value).before((Date)filterValue);
            }
            else if (filterValue instanceof T8Timestamp)
            {
                return ((Date)value).before(new Date(((T8Timestamp)filterValue).getMilliseconds()));
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }

    private boolean lessThanOrEqual(Object value, Object filterValue)
    {
        if (value instanceof String)
        {
            if (filterValue instanceof String)
            {
                return ((String)value).compareTo((String)filterValue) <= 0;
            }
            else return ((String)value).compareTo("" + filterValue) <= 0;
        }
        else if (value instanceof Integer)
        {
            if (filterValue instanceof Integer)
            {
                return ((Integer)value) <= ((Integer)filterValue);
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Double)
        {
            if (filterValue instanceof Double)
            {
                return Double.compare((Double)value, (Double)filterValue) <= 0;
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Long)
        {
            if (filterValue instanceof Long)
            {
                return Long.compare((Long)value, (Long)filterValue) <= 0;
            } else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else if (value instanceof Date)
        {
            if (filterValue instanceof Date)
            {
                return ((Date)value).before((Date)filterValue) || ((Date)value).equals(filterValue);
            }
            else if (filterValue instanceof T8Timestamp)
            {
                return ((Date)value).before(new Date(((T8Timestamp)filterValue).getMilliseconds())) || ((Date)value).equals(new Date(((T8Timestamp)filterValue).getMilliseconds()));
            }
            else throw new RuntimeException("Incompatible filter value type '" + filterValue.getClass() + "' for value type '" + value.getClass() + "'.");
        }
        else return false; // TODO:  Add the other possible data type comparisons.
    }

    public boolean equalsFilterCriterion(T8DataFilterCriterion other)
    {
        if (other == null)
        {
            return false;
        }
        else if (!Objects.equals(identifier, other.identifier))
        {
            return false;
        }
        else if ((this.fieldIdentifier == null) ? (other.fieldIdentifier != null) : !this.fieldIdentifier.equals(other.fieldIdentifier))
        {
            return false;
        }
        else if (this.operator != other.operator)
        {
            return false;
        }
        else if (this.filterValue != other.filterValue && (this.filterValue == null || !this.filterValue.equals(other.filterValue)))
        {
            return false;
        }
        else if (this.caseInsensitive != other.caseInsensitive)
        {
            return false;
        }
        else return true;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder();
        toStringBuilder.append("[");

        // Append a descriptor.
        if (identifier != null)
        {
            toStringBuilder.append(identifier);
            toStringBuilder.append(":");
        }
        else
        {
            toStringBuilder.append("Filter Criterion:");
        }

        // Append the criterion specifics.
        toStringBuilder.append(fieldIdentifier);
        toStringBuilder.append(" ");
        toStringBuilder.append(operator);
        toStringBuilder.append(" ");
        toStringBuilder.append(filterValue);
        toStringBuilder.append("]");
        return toStringBuilder.toString();
    }
}
