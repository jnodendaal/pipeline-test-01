package com.pilog.t8.flow.task;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskValidationResult
{
    private String taskIid;
    private boolean valid;
    private Map<String, Object> outputParameters;

    public T8TaskValidationResult(String taskIid, boolean valid, Map<String, Object> outputParameters)
    {
        this.taskIid = taskIid;
        this.valid = valid;
        this.outputParameters = outputParameters;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public void setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
    }

    public boolean isValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public Map<String, Object> getOutputParameters()
    {
        return outputParameters;
    }

    public void setOutputParameters(Map<String, Object> outputParameters)
    {
        this.outputParameters = outputParameters;
    }
}
