package com.pilog.t8.definition.script;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskModuleDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskModuleInputParameterScriptDefinition extends T8DefaultScriptDefinition implements T8ClientContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_TASK_MODULE_INPUT_PARAMETERS";
    public static final String GROUP_IDENTIFIER = "@DT_SCRIPT_FLOW_TASK_MODULE_INPUT_PARAMETERS";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Module Task Input Parameter Script";
    public static final String DESCRIPTION = "A script that compiles a set of module input parameters to be used during execution of a specific flow task.";
    // -------- Definition Meta-Data -------- //

    public T8FlowTaskModuleInputParameterScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8WorkFlowUserTaskModuleDefinition taskModuleDefinition;

        taskModuleDefinition = (T8WorkFlowUserTaskModuleDefinition)getParentDefinition();
        if (taskModuleDefinition != null)
        {
            T8WorkFlowUserTaskDefinition taskDefinition;

            taskDefinition = (T8WorkFlowUserTaskDefinition)taskModuleDefinition.getParentDefinition();
            return taskDefinition.getInputParameterDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8WorkFlowUserTaskModuleDefinition taskModuleUIDefinition;
        T8DefinitionManager definitionManager;

        definitionManager = context.getServerContext().getDefinitionManager();
        taskModuleUIDefinition = (T8WorkFlowUserTaskModuleDefinition)getParentDefinition();
        if (taskModuleUIDefinition != null)
        {
            T8ModuleDefinition moduleDefinition;

            moduleDefinition = (T8ModuleDefinition)definitionManager.getRawDefinition(context, getRootProjectId(), taskModuleUIDefinition.getModuleIdentifier());
            if (moduleDefinition != null)
            {
                return moduleDefinition.getInputParameterDefinitions();
            }
            else return null;
        }
        else return null;
    }

    @Override
    public T8ClientContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultClientContextScript").getConstructor(T8Context.class, T8ClientContextScriptDefinition.class);
        return (T8ClientContextScript)constructor.newInstance(context, this);
    }
}
