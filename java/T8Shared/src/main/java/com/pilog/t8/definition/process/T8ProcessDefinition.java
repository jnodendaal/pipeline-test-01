package com.pilog.t8.definition.process;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.process.T8Process;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowProfileDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.definition.user.T8UserProfileDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ProcessDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SERVER_PROCESS";
    public static final String GROUP_NAME = "Server Process";
    public static final String GROUP_DESCRIPTION = "Long running processes that are managed by the server and logged in a persistence store so that they can be monitored even after completion.";
    public static final String STORAGE_PATH = "/server_processes";
    public static final String DISPLAY_NAME = "Server Process";
    public static final String DESCRIPTION = "A long running server-side process.";
    public static final String IDENTIFIER_PREFIX = "PRC_";
    public enum Datum
    {
        PROCESS_DISPLAY_NAME,
        PROCESS_DESCRIPTION,
        INPUT_PARAMETER_DEFINITIONS,
        OUTPUT_PARAMETER_DEFINITIONS,
        FAILURE_SCRIPT,
        FINALIZE_ON_COMPLETION,
        SEND_NOTIFICATION,
        PROCESS_USER_VISIBILITY_RESCRICTIONS,
        PROCESS_USER_PROFILE_VISIBILITY_RESTRICTIONS,
        PROCESS_WORKFLOW_PROFILE_VISIBILITY_RESTRICTIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8ProcessDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.PROCESS_DISPLAY_NAME.toString(), "Display Name", "The name of this type of process that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.PROCESS_DESCRIPTION.toString(), "Description", "The description of this type of process that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FINALIZE_ON_COMPLETION.toString(), "Finalize on Completion", "If this option is checked then the process will be finalized from the process history list once the process has completed SUCCESSFULLY.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SEND_NOTIFICATION.toString(), "Send Notification", "If this option is checked a notification will be sent to all applicable users when this process is started.", false));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this process."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters", "The output data parameters returned by this process after execution."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.PROCESS_USER_VISIBILITY_RESCRICTIONS.toString(), "User Visibility Restrictions", "All of the users defined in this list will be able to see and control the process."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.PROCESS_USER_PROFILE_VISIBILITY_RESTRICTIONS.toString(), "User Profile Visibility Restrictions", "All of the users currently in the profile defined in this list will be able to see and control the process."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.PROCESS_WORKFLOW_PROFILE_VISIBILITY_RESTRICTIONS.toString(), "Workflow Profile Visibility Restrictions", "All of the users with the workflow profile defined in this list will be able to see and control the process."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FAILURE_SCRIPT.toString(), "Failure Script", "The failure script that will be run when a exception occurs on the process that will stop the process from executing."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.FAILURE_SCRIPT.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ProcessFailureScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.PROCESS_USER_VISIBILITY_RESCRICTIONS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8UserDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROCESS_USER_PROFILE_VISIBILITY_RESTRICTIONS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8UserProfileDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROCESS_WORKFLOW_PROFILE_VISIBILITY_RESTRICTIONS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowProfileDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) || (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumId));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8Process getNewProcessInstance(T8Context context, String processIid);

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8ProcessTestHarness", new Class<?>[]{});
    }

    public String getProcessDisplayName()
    {
        return (String)getDefinitionDatum(Datum.PROCESS_DISPLAY_NAME);
    }

    public void setProcessDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.PROCESS_DISPLAY_NAME, displayName);
    }

    public String getProcessDescription()
    {
        return (String)getDefinitionDatum(Datum.PROCESS_DESCRIPTION);
    }

    public void setProcessDescription(String description)
    {
        setDefinitionDatum(Datum.PROCESS_DESCRIPTION, description);
    }

    public Boolean isFinalizeOnCompletionEnabled()
    {
        return (Boolean)getDefinitionDatum(Datum.FINALIZE_ON_COMPLETION);
    }

    public void setFinalizeOnCompletion(Boolean finalizeOnCompletion)
    {
        setDefinitionDatum(Datum.FINALIZE_ON_COMPLETION, finalizeOnCompletion);
    }

    public Boolean isSendNotification()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.SEND_NOTIFICATION);
        return value != null && value;
    }

    public void setSendNotification(Boolean sendNotification)
    {
        setDefinitionDatum(Datum.SEND_NOTIFICATION, sendNotification);
    }

    public T8ProcessFailureScriptDefinition getFailureScriptDefinition()
    {
        return (T8ProcessFailureScriptDefinition)getDefinitionDatum(Datum.FAILURE_SCRIPT);
    }

    public void setFailureScriptDefinition(T8ProcessFailureScriptDefinition failureScriptDefinition)
    {
        setDefinitionDatum(Datum.FAILURE_SCRIPT, failureScriptDefinition);
    }

    public void addInputParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public void addOutputParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public ArrayList<String> getUserVisibilityResctrictions()
    {
        return (ArrayList<String>)getDefinitionDatum(Datum.PROCESS_USER_VISIBILITY_RESCRICTIONS.toString());
    }

    public void setUserVisibilityResctrictions(ArrayList<String> restrictions)
    {
        setDefinitionDatum(Datum.PROCESS_USER_VISIBILITY_RESCRICTIONS, restrictions);
    }

    public ArrayList<String> getUserProfileVisibilityResctrictions()
    {
        return (ArrayList<String>)getDefinitionDatum(Datum.PROCESS_USER_PROFILE_VISIBILITY_RESTRICTIONS);
    }

    public void setUserProfileVisibilityResctrictions(ArrayList<String> restrictions)
    {
        setDefinitionDatum(Datum.PROCESS_USER_PROFILE_VISIBILITY_RESTRICTIONS, restrictions);
    }

    public ArrayList<String> getWorkFlowProfileVisibilityResctrictions()
    {
        return (ArrayList<String>)getDefinitionDatum(Datum.PROCESS_WORKFLOW_PROFILE_VISIBILITY_RESTRICTIONS);
    }

    public void setWorkFlowProfileVisibilityResctrictions(ArrayList<String> restrictions)
    {
        setDefinitionDatum(Datum.PROCESS_WORKFLOW_PROFILE_VISIBILITY_RESTRICTIONS, restrictions);
    }
}
