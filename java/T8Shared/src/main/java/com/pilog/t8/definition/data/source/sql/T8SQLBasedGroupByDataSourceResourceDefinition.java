/**
 * Created on 08 Oct 2015, 2:29:01 PM
 */
package com.pilog.t8.definition.data.source.sql;

/**
 * @author Gavin Boshoff
 */
public class T8SQLBasedGroupByDataSourceResourceDefinition extends T8SQLBasedGroupByDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_SQL_GROUP_BY_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8SQLBasedGroupByDataSourceResourceDefinition(String identifier)
    {
        super(identifier);
    }
}