package com.pilog.t8.definition.data.source.epic;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8EpicSqlDataSourceScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_DATA_SOURCE_EPIC_SQL";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_DATA_SOURCE_EPIC_SQL";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "EPIC SQL Data Source Script";
    public static final String DESCRIPTION = "A script that compiles SQL to be used by a data source.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_ENTITY_DEFINITION = "$P_ENTITY_DEFINITION";
    public static final String PARAMETER_DML_OPERATION_TYPE = "$P_DML_OPERATION_TYPE";
    public static final String PARAMETER_DML_CLAUSE_TYPE = "$P_DML_CLAUSE_TYPE";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";
    public static final String PARAMETER_SQL_STRING = "$P_SQL_STRING";

    public enum DmlOperationType {SELECT, COUNT, INSERT, UPDATE, DELETE};
    public enum DmlClauseType {WITH, SELECT, WHERE, ORDER_BY};

    public T8EpicSqlDataSourceScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_ENTITY_DEFINITION, "Entity Definition", "The entity definition referencing this data source.", T8DataType.DEFINITION));
        definitions.add(new T8DataParameterDefinition(PARAMETER_SQL_STRING, "SQL String", "The string to which this script must append additional SQL.", T8DataType.CUSTOM_OBJECT));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DML_OPERATION_TYPE, "DML Operation", "The operation type to generate SQL for.  Values: " + DmlOperationType.values(), T8DataType.STRING));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DML_CLAUSE_TYPE, "DML Clause", "The clause type to generate SQL for.  Values: " + DmlClauseType.values(), T8DataType.STRING));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter for which to generate SQL.", T8DataType.CUSTOM_OBJECT));
        definitions.add(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Time Issued", "The time at which the task was issued.", T8DataType.INTEGER));
        definitions.add(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Time Claimed", "The time at which the task was claimed.", T8DataType.INTEGER));
        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<T8DataParameterDefinition>();
        return definitions;
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<T8ScriptClassImport>();
        classImports.add(new T8ScriptClassImport(DmlOperationType.class.getSimpleName(), DmlOperationType.class));
        classImports.add(new T8ScriptClassImport(DmlClauseType.class.getSimpleName(), DmlClauseType.class));
        return classImports;
    }
}
