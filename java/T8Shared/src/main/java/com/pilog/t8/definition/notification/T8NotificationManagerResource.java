package com.pilog.t8.definition.notification;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLAggregateFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationManagerResource implements T8DefinitionResource
{
    // Operation identifiers.
    public static final String OPERATION_SEND_NOTIFICATION = "@OS_SEND_NOTIFICATION";
    public static final String OPERATION_GET_USER_NOTIFICATIONS = "@OS_GET_USER_NOTIFICATIONS";
    public static final String OPERATION_GET_USER_NOTIFICATION_SUMMARY = "@OS_GET_USER_NOTIFICATION_SUMMARY";
    public static final String OPERATION_CLOSE_NOTIFICATION = "@OS_CLOSE_NOTIFICATION";
    public static final String OPERATION_CLOSE_ALL_NOTIFICATIONS = "@OP_CLOSE_ALL_NOTIFICATIONS";
    public static final String OPERATION_NOTIFICATION_MARK_AS = "@OS_NOTIFICATION_MARK_AS";
    public static final String OPERATION_COUNT_NOTIFICATIONS = "@OP_COUNT_NOTIFICATIONS";

    // Parameter identifiers.
    public static final String PARAMETER_NOTIFICATION_IDENTIFIER = "$P_NOTIFICATION_IDENTIFIER";
    public static final String PARAMETER_NOTIFICATION_PARAMETERS = "$P_NOTIFICATION_PARAMETERS";
    public static final String PARAMETER_NOTIFICATION_LIST = "$P_NOTIFICATION_LIST";
    public static final String PARAMETER_NOTIFICATION_FILTER = "$P_NOTIFICATION_FILTER";
    public static final String PARAMETER_NOTIFICATION_SUMMARY = "$P_NOTIFICATION_SUMMARY";
    public static final String PARAMETER_NOTIFICATION_IID = "$P_NOTIFICATION_IID";
    public static final String PARAMETER_NOTIFICATION_STATUS = "$P_NOTIFICATION_STATUS";
    public static final String PARAMETER_NOTIFICATION_COUNT = "$P_NOTIFICATION_COUNT";

    // Data Source Identifiers.
    public static final String STATE_NOTIFICATION_DS_IDENTIFIER = "@DS_STATE_NOTIFICATION";
    public static final String STATE_NOTIFICATION_PAR_DS_IDENTIFIER = "@DS_STATE_NOTIFICATION_PAR";
    public static final String HISTORY_NOTIFICATION_DS_IDENTIFIER = "@DS_HISTORY_NOTIFICATION";
    public static final String HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER = "@DS_HISTORY_NOTIFICATION_PAR";
    public static final String STATE_NOTIFICATION_SUMMARY_DS_IDENTIFIER = "@DS_STATE_NOTIFICATION_SUMMARY";

    // Data Entity Identifiers.
    public static final String STATE_NOTIFICATION_DE_IDENTIFIER = "@E_STATE_NOTIFICATION";
    public static final String STATE_NOTIFICATION_PAR_DE_IDENTIFIER = "@E_STATE_NOTIFICATION_PAR";
    public static final String HISTORY_NOTIFICATION_DE_IDENTIFIER = "@E_HISTORY_NOTIFICATION";
    public static final String HISTORY_NOTIFICATION_PAR_DE_IDENTIFIER = "@E_HISTORY_NOTIFICATION_PAR";
    public static final String STATE_NOTIFICATION_SUMMARY_DE_IDENTIFIER = "@E_STATE_NOTIFICATION_SUMMARY";

    // Table Names.
    private static final String STATE_NOTIFICATION = "NTF";
    private static final String STATE_NOTIFICATION_PAR = "NTF_PAR";
    private static final String HISTORY_NOTIFICATION = "HIS_NTF";
    private static final String HISTORY_NOTIFICATION_PAR = "HIS_NTF_PAR";

    // Field identifiers.
    public static final String F_USER_ID = "$USER_ID";
    public static final String F_STATUS = "$STATUS";
    public static final String F_NOTIFICATION_COUNT = "$NOTIFICATION_COUNT";
    public static final String F_NOTIFICATION_ID = "$NOTIFICATION_ID";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8ProcessNotificationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getProcessNotificationDefinitions());
        if (typeIdentifiers.contains(T8JasperReportNotificationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getJasperNotificationDefinitions());
        if (typeIdentifiers.contains(T8SystemNotificationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getSystemNotificationDefinitions());
        if (typeIdentifiers.contains(T8MessageNotificationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getMessageNotificationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions());

        return definitions;
    }

    private List<T8JavaServerOperationDefinition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8JavaServerOperationDefinition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_SEND_NOTIFICATION);
        definition.setMetaDisplayName("Send Notification");
        definition.setMetaDescription("Sends a specified notification using the supplied parameters.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$SendNotificationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_IDENTIFIER, "Notification ID", "The ID of the notification to be sent.", T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_USER_NOTIFICATIONS);
        definition.setMetaDisplayName("Get User Notifications");
        definition.setMetaDescription("Gets a list of all the notifications to display to the current session user.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$GetUserNotificationsOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_FILTER, "Notification Filter", "The filter to be applied when retrieving the notification set.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_LIST, "Notification List", "A list containing all of the notifications intended for the current session user.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CLOSE_NOTIFICATION);
        definition.setMetaDisplayName("Close Notification");
        definition.setMetaDescription("Dismisses a specific notification instance.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$CloseNotificationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_IID, "Notification Instance ID", "The instance ID for the notification to be dismissed.", T8DataType.GUID));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CLOSE_ALL_NOTIFICATIONS);
        definition.setMetaDisplayName("Close All Notifications");
        definition.setMetaDescription("Close all notifications sent to the current session context user.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$CloseAllNotificationsOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_USER_NOTIFICATION_SUMMARY);
        definition.setMetaDisplayName("Get User Notification Summary");
        definition.setMetaDescription("Gets the summary of the notification details for a specific user.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$GetUserNotificationSummaryOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_FILTER, "Notification Filter", "The filter to be applied when retrieving the details for the notification set.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_SUMMARY, "Notification Summary", "The summary containing details about the user notifications available.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_NOTIFICATION_MARK_AS);
        definition.setMetaDisplayName("Change Notification Instance Status");
        definition.setMetaDescription("Changes the status for a specific notification instance.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$MarkNotificationAsOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_IID, "Notification Instance ID", "The instance ID for the notification for which the status should be updated.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_STATUS, "Notification Status", "Status to which the notification instance should be updated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_COUNT_NOTIFICATIONS);
        definition.setMetaDisplayName("Count Notifications");
        definition.setMetaDescription("Counts the number of notifications that adhere to the specified filter criteria.");
        definition.setClassName("com.pilog.t8.notification.T8NotificationOperations$CountNotificationsOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_FILTER, "Notification Filter", "The filter to be applied when retrieving the details for the notification set.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_COUNT, "Notification Count", "The number of notifications that adhere to the specified filter criteria.", T8DataType.INTEGER));
        definitions.add(definition);

        return definitions;
    }

    private List<T8ProcessNotificationDefinition> getProcessNotificationDefinitions()
    {
        return ArrayLists.typeSafeList(new T8ProcessNotificationDefinition(null));
    }

    private List<T8JasperReportNotificationDefinition> getJasperNotificationDefinitions()
    {
        return ArrayLists.typeSafeList(new T8JasperReportNotificationDefinition(null));
    }

    private List<T8SystemNotificationDefinition> getSystemNotificationDefinitions()
    {
        return ArrayLists.typeSafeList(new T8SystemNotificationDefinition(null));
    }

    private List<T8MessageNotificationDefinition> getMessageNotificationDefinitions()
    {
        return ArrayLists.typeSafeList(new T8MessageNotificationDefinition(null));
    }

    private List<T8DataSourceDefinition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8SQLBasedGroupByDataSourceDefinition sqlGroupByDataSourceDefinition;
        T8TableDataSourceResourceDefinition tableDataSourceDefinition;
        List<T8DataSourceDefinition> definitions;

        definitions = new ArrayList<>();

        // Notification History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_NOTIFICATION_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(HISTORY_NOTIFICATION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NOTIFICATION_IID", "NOTIFICATION_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NOTIFICATION_ID", "NOTIFICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$USER_ID", "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PRIORITY", "PRIORITY", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_ID", "SENDER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_IID", "SENDER_IID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_ORG_ID", "SENDER_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT", "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_PARAMETERS", "EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Notification History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(HISTORY_NOTIFICATION_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_ID", "PARAMETER_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ID", "IDENTIFIER", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // State Notification Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_NOTIFICATION_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(STATE_NOTIFICATION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NOTIFICATION_IID", "NOTIFICATION_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NOTIFICATION_ID", "NOTIFICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$USER_ID", "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PRIORITY", "PRIORITY", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME_SENT", "TIME_SENT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$STATUS", "STATUS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_ID", "SENDER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_IID", "SENDER_IID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_ORG_ID", "SENDER_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // State Notification Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_NOTIFICATION_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(STATE_NOTIFICATION_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NOTIFICATION_IID", "NOTIFICATION_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_IID", "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARENT_PARAMETER_IID", "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE", "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_KEY", "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // State Notification Summary SQL Group By Data Source
        sqlGroupByDataSourceDefinition = new T8SQLBasedGroupByDataSourceResourceDefinition(STATE_NOTIFICATION_SUMMARY_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.setOriginatingDataSourceIdentifier(STATE_NOTIFICATION_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_NOTIFICATION_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_STATUS);
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NOTIFICATION_ID, "NOTIFICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STATUS, "STATUS", false, false, T8DataType.STRING));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8SQLAggregateFieldResourceDefinition(F_NOTIFICATION_COUNT, "NOTIFICATION_COUNT", false, false, T8DataType.INTEGER, "COUNT(NOTIFICATION_ID)"));
        definitions.add(sqlGroupByDataSourceDefinition);

        return definitions;
    }

    private List<T8DataEntityResourceDefinition> getDataEntityDefinitions()
    {
        List<T8DataEntityResourceDefinition> definitions;
        T8DataEntityResourceDefinition entityDefinition;

        definitions = new ArrayList<>();

        // Notification History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_NOTIFICATION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_NOTIFICATION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$EVENT_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NOTIFICATION_IID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$NOTIFICATION_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NOTIFICATION_ID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$NOTIFICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$USER_ID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PRIORITY", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$PRIORITY", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_ID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$SENDER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_IID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$SENDER_IID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_ORG_ID", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$SENDER_ORG_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$EVENT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$TIME", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_PARAMETERS", HISTORY_NOTIFICATION_DS_IDENTIFIER + "$EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        definitions.add(entityDefinition);

        // Notification History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_NOTIFICATION_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER + "$EVENT_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_ID", HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER + "$PARAMETER_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ID", HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER + "$ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", HISTORY_NOTIFICATION_PAR_DS_IDENTIFIER + "$VALUE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // State Notification Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_NOTIFICATION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_NOTIFICATION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NOTIFICATION_IID", STATE_NOTIFICATION_DS_IDENTIFIER + "$NOTIFICATION_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NOTIFICATION_ID", STATE_NOTIFICATION_DS_IDENTIFIER + "$NOTIFICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$USER_ID", STATE_NOTIFICATION_DS_IDENTIFIER + "$USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PRIORITY", STATE_NOTIFICATION_DS_IDENTIFIER + "$PRIORITY", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME_SENT", STATE_NOTIFICATION_DS_IDENTIFIER + "$TIME_SENT", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$STATUS", STATE_NOTIFICATION_DS_IDENTIFIER + "$STATUS", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_ID", STATE_NOTIFICATION_DS_IDENTIFIER + "$SENDER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_IID", STATE_NOTIFICATION_DS_IDENTIFIER + "$SENDER_IID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_ORG_ID", STATE_NOTIFICATION_DS_IDENTIFIER + "$SENDER_ORG_ID", false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // State Notification Property Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_NOTIFICATION_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_NOTIFICATION_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NOTIFICATION_IID", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$NOTIFICATION_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_IID", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$PARAMETER_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARENT_PARAMETER_IID", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$TYPE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_KEY", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$PARAMETER_KEY", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$SEQUENCE", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", STATE_NOTIFICATION_PAR_DS_IDENTIFIER + "$VALUE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // State Notification Summary Data Entity
        entityDefinition = new T8DataEntityResourceDefinition(STATE_NOTIFICATION_SUMMARY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_NOTIFICATION_SUMMARY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NOTIFICATION_ID, STATE_NOTIFICATION_SUMMARY_DS_IDENTIFIER + F_NOTIFICATION_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, STATE_NOTIFICATION_SUMMARY_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NOTIFICATION_COUNT, STATE_NOTIFICATION_SUMMARY_DS_IDENTIFIER + F_NOTIFICATION_COUNT, false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        return definitions;
    }
}
