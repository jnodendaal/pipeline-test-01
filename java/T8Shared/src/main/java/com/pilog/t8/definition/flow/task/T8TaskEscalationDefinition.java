package com.pilog.t8.definition.flow.task;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskEscalationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_TASK_ESCALATION";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_TASK_ESCALATION";
    public static final String STORAGE_PATH = "/task_escalations";
    public static final String DISPLAY_NAME = "Workflow Task Escalation";
    public static final String DESCRIPTION = "The setup of a routine to follow when workflow tasks are escalated according to preset triggers.";
    public static final String IDENTIFIER_PREFIX = "TASK_ESC_";
    private enum Datum {SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8TaskEscalationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SCRIPT_DEFINITION.toString(), "Script", "The script to execute when this escalation occurs."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TaskEscalationScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8TaskEscalationScriptDefinition getScriptDefinition()
    {
        return (T8TaskEscalationScriptDefinition)getDefinitionDatum(Datum.SCRIPT_DEFINITION.toString());
    }

    public void setScriptDefinition(T8TaskEscalationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.SCRIPT_DEFINITION.toString(), definition);
    }
}
