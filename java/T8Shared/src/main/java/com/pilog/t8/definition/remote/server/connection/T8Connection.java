/**
 * Created on 09 Mar 2016, 9:38:01 AM
 */
package com.pilog.t8.definition.remote.server.connection;

/**
 * This currently only serves the purpose of a marker interface, since there is
 * no easy way to standardize the way in which the different connectors
 * implementing this interface should work.
 *
 * @author Gavin Boshoff
 */
public interface T8Connection {}