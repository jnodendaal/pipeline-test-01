package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDescendentMetaDataChangeEvent extends EventObject
{
    private final T8DefinitionMetaData oldValue;
    private final T8DefinitionMetaData newValue;
    private final Object actor;
    
    public T8DefinitionDescendentMetaDataChangeEvent(T8Definition definition, T8DefinitionMetaData oldValue, T8DefinitionMetaData newValue, Object actor)
    {
        super(definition);
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.actor = actor;
    }
    
    public T8Definition getDescendentDefinition()
    {
        return (T8Definition)source;
    }

    public T8DefinitionMetaData getNewValue()
    {
        return newValue;
    }

    public T8DefinitionMetaData getOldValue()
    {
        return oldValue;
    }
    
    public Object getActor()
    {
        return actor;
    }
}
