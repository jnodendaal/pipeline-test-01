package com.pilog.t8.definition.operation;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultServerOperation implements T8ServerOperation
{
    protected final T8ServerContext serverContext;
    protected final T8Context context;
    protected final T8JavaServerOperationDefinition definition;

    protected T8DataTransaction tx;
    protected volatile boolean stopFlag = false;
    protected volatile boolean cancelFlag = false;
    protected volatile boolean pauseFlag = false;

    public T8DefaultServerOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.context = context;
    }

    @Override
    public void init()
    {
        this.tx = serverContext.getDataManager().getCurrentSession().getTransaction();
    }

    @Override
    public double getProgress(T8Context context)
    {
        return -1;
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return null;
    }

    @Override
    public void stop(T8Context context)
    {
        stopFlag = true;
    }

    @Override
    public void cancel(T8Context context)
    {
        cancelFlag = true;
    }

    @Override
    public void pause(T8Context context)
    {
        pauseFlag = true;
    }

    @Override
    public void resume(T8Context context)
    {
        pauseFlag = false;
    }
}
