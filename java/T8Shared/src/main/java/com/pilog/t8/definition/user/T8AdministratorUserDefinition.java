package com.pilog.t8.definition.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8AdministratorUserDefinition extends T8UserDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_USER_ADMINISTRATOR";
    public static final String STORAGE_PATH = "/administrators";
    public static final String DISPLAY_NAME = "Administrator";
    public static final String DESCRIPTION = "A registered system Administrator user.";
    public static final String IDENTIFIER_PREFIX = "UA_";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.SYSTEM;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8AdministratorUserDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();

        //Hide some datums that will not be configurable for this user type
        for (T8DefinitionDatumType t8DefinitionDatumType : datumTypes)
        {
            if(t8DefinitionDatumType.getIdentifier().equals(T8UserDefinition.Datum.USER_PROFILE_IDENTIFIERS.toString()))t8DefinitionDatumType.setHidden(true);
            else if(t8DefinitionDatumType.getIdentifier().equals(T8UserDefinition.Datum.WORK_FLOW_PROFILE_IDENTIFIERS.toString()))t8DefinitionDatumType.setHidden(true);
        }

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    //Ensure that we only ever return the adminstrator profile identifier
    @Override
    public List<String> getProfileIdentifiers()
    {
        return Arrays.asList("@UP_ADMINISTRATOR");
    }

    //Ensure that we never return any workflow profile identifiers
    @Override
    public List<String> getWorkFlowProfileIdentifiers()
    {
        return new ArrayList<String>();
    }
}
