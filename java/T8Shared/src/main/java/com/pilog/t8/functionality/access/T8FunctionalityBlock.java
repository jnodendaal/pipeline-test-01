package com.pilog.t8.functionality.access;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.translation.T8TranslatableString;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityBlock
{
    private final String functionalityIdentifier;
    private final String dataObjectIdentifier;
    private final String dataObjectKey;
    private final String rootOrgID;
    private final String orgID;
    private final String userID;
    private final String profileID;
    private final T8TranslatableString message;

    public T8FunctionalityBlock(String rootOrgId, String orgId, String userId, String profileId, String functionalityId, String dataObjectId, String dataObjectIid, T8TranslatableString message)
    {
        this.functionalityIdentifier = functionalityId;
        this.dataObjectIdentifier = dataObjectId;
        this.dataObjectKey = dataObjectIid;
        this.rootOrgID = rootOrgId;
        this.orgID = orgId;
        this.userID = userId;
        this.profileID = profileId;
        this.message = message;
    }

    public String getFunctionalityIdentifier()
    {
        return functionalityIdentifier;
    }

    public String getDataObjectIdentifier()
    {
        return dataObjectIdentifier;
    }

    public String getDataObjectKey()
    {
        return dataObjectKey;
    }

    public String getRootOrgID()
    {
        return rootOrgID;
    }

    public String getOrgID()
    {
        return orgID;
    }

    public String getUserIdentifier()
    {
        return userID;
    }

    public String getProfileIdentifier()
    {
        return profileID;
    }

    public T8TranslatableString getMessage()
    {
        return message;
    }

    public boolean blocksFunctionality(String functionalityIdentifier, T8DataObject dataObject)
    {
        // If the functionality identifiers are not the same, no block.
        if (!this.functionalityIdentifier.equals(functionalityIdentifier))
        {
            // Data Objects are not the same, so no block.
            return false;
        }
        else if (this.dataObjectIdentifier == null)
        {
            // If blocking is not on the data object, a block has occurred because the functionality identifiers are the same.
            return true;
        }
        else // Blocking is on data object.
        {
            // Check that the input data object is the same as the blocked data object.
            return ((this.dataObjectIdentifier.equals(dataObject.getId())) && (this.dataObjectKey.equals(dataObject.getKey())));
        }
    }

    @Override
    public String toString()
    {
        return "T8FunctionalityBlock{" + "functionalityIdentifier=" + functionalityIdentifier + ", dataObjectIdentifier=" + dataObjectIdentifier + ", dataObjectKey=" + dataObjectKey + '}';
    }
}
