package com.pilog.t8.definition.user;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.user.T8UserProfile;
import com.pilog.t8.definition.flow.T8WorkFlowProfileDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.user.password.T8PasswordPolicyDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8UserProfileDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_USER_PROFILE";
    public static final String GROUP_IDENTIFIER = "@DG_USER_PROFILE";
    public static final String GROUP_NAME = "User Profiles";
    public static final String GROUP_DESCRIPTION = "Configuration settings that describe the access rights and settings applicable to a specific type of user in the system.";
    public static final String STORAGE_PATH = "/user_profiles";
    public static final String DISPLAY_NAME = "User Profile";
    public static final String DESCRIPTION = "A user profile that defines the configuration and setup for a specific user type.";
    public static final String IDENTIFIER_PREFIX = "UP_";
    public enum Datum {
        DISPLAY_NAME,
        INCLUDED_PROFILE_IDENTIFIERS,
        ASSOCIATED_WF_PROFILE_IDENTIFIERS,
        FUNCTIONALITY_IDENTIFIERS,
        PASSWORD_POLICY_DEFINITION_IDENTIFIER,
        REMOTE_SERVER_CONNECTIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8UserProfileDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The text to use when this profile is displayed on the UI."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.INCLUDED_PROFILE_IDENTIFIERS.toString(), "Included Profiles", "The list of profiles included in this profile i.e. inherited by this profile."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.ASSOCIATED_WF_PROFILE_IDENTIFIERS.toString(), "Associated Workflow Profiles", "The list of workflow profiles associated with this profile."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.FUNCTIONALITY_IDENTIFIERS.toString(), "System Functionalities", "The list of system functionalities available to this profile."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PASSWORD_POLICY_DEFINITION_IDENTIFIER.toString(), "Password Policy Identifier", "The policy containing password configuration and requirement setting for this profile."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REMOTE_SERVER_CONNECTIONS.toString(), "Remote Server Connections", "The remote server connections that this profile can connect to."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.INCLUDED_PROFILE_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8UserProfileDefinition.GROUP_IDENTIFIER));
        else if (Datum.ASSOCIATED_WF_PROFILE_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowProfileDefinition.GROUP_IDENTIFIER));
        else if (Datum.PASSWORD_POLICY_DEFINITION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PasswordPolicyDefinition.GROUP_IDENTIFIER), true, "None");
        else if (Datum.REMOTE_SERVER_CONNECTIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8UserRemoteServerConnectionDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8UserProfile getNewUserProfileInstance(T8Context context) throws Exception
    {
        List<String> includedProfileIdentifiers;
        T8DefinitionManager definitionManager;
        T8UserProfile userProfile;

        // Create the new profile object.
        userProfile = new T8UserProfile(getIdentifier());

        // Set the functionalities of the user profile.
        userProfile.setFunctionalityIdentifiers(getFunctionalityIdentifiers());

        // We have to load and include all the required-sub-profiles.
        definitionManager = context.getServerContext().getDefinitionManager();
        includedProfileIdentifiers = getIncludedProfileIdentifiers();
        if (includedProfileIdentifiers != null)
        {
            String projectId;

            projectId = getRootProjectId();
            for (String includedProfileIds : includedProfileIdentifiers)
            {
                T8UserProfileDefinition includedProfileDefinition;

                includedProfileDefinition = (T8UserProfileDefinition)definitionManager.getInitializedDefinition(context, projectId, includedProfileIds, null);
                userProfile.addIncludedProfile(includedProfileDefinition.getNewUserProfileInstance(context));
            }
        }

        // Return the completed user profile.
        return userProfile;
    }

    public String getUserProfileDisplayName()
    {
        return getDefinitionDatum(Datum.DISPLAY_NAME);
    }

    public void setUserProfileDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME, displayName);
    }

    public List<String> getIncludedProfileIdentifiers()
    {
        return getDefinitionDatum(Datum.INCLUDED_PROFILE_IDENTIFIERS);
    }

    public void setIncludedProfileIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.INCLUDED_PROFILE_IDENTIFIERS, identifiers);
    }

    public List<String> getAssociatedWorkFlowProfileIdentifiers()
    {
        return getDefinitionDatum(Datum.ASSOCIATED_WF_PROFILE_IDENTIFIERS);
    }

    public void setAssociatedWorkFlowProfileIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.ASSOCIATED_WF_PROFILE_IDENTIFIERS, identifiers);
    }

    public List<String> getFunctionalityIdentifiers()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIERS);
    }

    public void setFunctionalityIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIERS, identifiers);
    }

    public String getPasswordPolicyDefinitionIdentifier()
    {
        return getDefinitionDatum(Datum.PASSWORD_POLICY_DEFINITION_IDENTIFIER);
    }

    public void setPasswordPolcyDefinitionIdentifier(String definitionIdentifier)
    {
        setDefinitionDatum(Datum.PASSWORD_POLICY_DEFINITION_IDENTIFIER, definitionIdentifier);
    }

    public List<T8UserRemoteServerConnectionDefinition> getRemoteServerConnectionDefinitions()
    {
        return getDefinitionDatum(Datum.REMOTE_SERVER_CONNECTIONS);
    }

    public void setRemoteServerConnectionDefinitions(List<T8UserRemoteServerConnectionDefinition> remoteServerConnectionDefinitions)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_CONNECTIONS, remoteServerConnectionDefinitions);
    }
}
