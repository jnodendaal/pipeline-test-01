package com.pilog.t8.flow.key;

/**
 * @author Bouwer du Preez
 */
public class T8TaskReassignedExecutionKey extends T8DefaultFlowExecutionKey
{
    private final String taskInstanceIdentifier;
    private final String userIdentifier;
    private final String profileIdentifier;

    public T8TaskReassignedExecutionKey(String flowIdentifier, String flowInstanceIdentifier, String nodeIdentifier, String nodeInstanceIdentifier, String taskInstanceIdentifier, String userIdentifier, String profileIdentifier)
    {
        super(flowIdentifier, flowInstanceIdentifier, nodeIdentifier, nodeInstanceIdentifier);
        this.taskInstanceIdentifier = taskInstanceIdentifier;
        this.userIdentifier = userIdentifier;
        this.profileIdentifier = profileIdentifier;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskInstanceIdentifier;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public String getProfileIdentifier()
    {
        return profileIdentifier;
    }
}
