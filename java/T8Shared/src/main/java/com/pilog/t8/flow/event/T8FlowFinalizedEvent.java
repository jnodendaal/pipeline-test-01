package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowStatus;

/**
 * @author Bouwer du Preez
 */
public class T8FlowFinalizedEvent extends T8FlowEvent
{
    public static final String TYPE_ID = "FLOW_FINALIZED";
    
    public T8FlowFinalizedEvent(T8FlowStatus flowStatus)
    {
        super(flowStatus);
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public T8FlowStatus getFlowStatus()
    {
        return (T8FlowStatus)source;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
