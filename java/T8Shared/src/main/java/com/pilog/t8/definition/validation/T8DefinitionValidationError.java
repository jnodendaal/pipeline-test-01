package com.pilog.t8.definition.validation;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionValidationError implements Serializable
{
    private String projectIdentifier;
    private String definitionIdentifier;
    private String datumIdentifier;
    private String message;
    private ErrorType errorType;
    
    public enum ErrorType {CRITICAL, WARNING};
    
    public T8DefinitionValidationError(String projectIdentifier, String definitionIdentifier, String datumIdentifier, String message, ErrorType errorType)
    {
        this.projectIdentifier = projectIdentifier;
        this.definitionIdentifier = definitionIdentifier;
        this.datumIdentifier = datumIdentifier;
        this.message = message;
        this.errorType = errorType;
    }

    public String getProjectIdentifier()
    {
        return projectIdentifier;
    }

    public void setProjectIdentifier(String projectIdentifier)
    {
        this.projectIdentifier = projectIdentifier;
    }
    
    public String getDefinitionIdentifier()
    {
        return definitionIdentifier;
    }

    public String getDatumIdentifier()
    {
        return datumIdentifier;
    }

    public void setDatumIdentifier(String datumIdentifier)
    {
        this.datumIdentifier = datumIdentifier;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
    
    public ErrorType getErrorType()
    {
        return errorType;
    }
}
