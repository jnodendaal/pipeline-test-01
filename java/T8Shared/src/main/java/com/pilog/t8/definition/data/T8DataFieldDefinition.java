package com.pilog.t8.definition.data;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataFieldDefinition extends T8DataValueDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum
    {
        SOURCE_IDENTIFIER,
        KEY,
        REQUIRED
    };
    // -------- Definition Meta-Data -------- //

    public T8DataFieldDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataFieldDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, T8DataType dataType)
    {
        this(identifier, sourceIdentifier, key, required, dataType.getDataTypeStringRepresentation(true));
    }

    public T8DataFieldDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, String dataType)
    {
        this(identifier);

        setDefinitionDatum(Datum.SOURCE_IDENTIFIER.toString(), sourceIdentifier, false, this);
        setDefinitionDatum(Datum.KEY.toString(), key, false, this);
        setDefinitionDatum(Datum.REQUIRED.toString(), required, false, this);
        setDefinitionDatum(T8DataValueDefinition.Datum.DATA_TYPE.toString(), dataType, false, this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SOURCE_IDENTIFIER.toString(), "Source", "The source from which this field gets its value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.KEY.toString(), "Key", "A boolean value indicating whether this field is part of a primary key.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REQUIRED.toString(), "Required", "A boolean value indicating whether this field is required (false indicates it is optional).", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    public String getSourceIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_IDENTIFIER.toString());
    }

    public void setSourceIdentifier(String sourceIdentifier)
    {
        setDefinitionDatum(Datum.SOURCE_IDENTIFIER.toString(), sourceIdentifier);
    }

    public boolean isKey()
    {
        return (Boolean)getDefinitionDatum(Datum.KEY.toString());
    }

    public void setKey(boolean key)
    {
        setDefinitionDatum(Datum.KEY.toString(), key);
    }

    public boolean isRequired()
    {
        return (Boolean)getDefinitionDatum(Datum.REQUIRED.toString());
    }

    public void setRequired(boolean required)
    {
        setDefinitionDatum(Datum.REQUIRED.toString(), required);
    }
}
