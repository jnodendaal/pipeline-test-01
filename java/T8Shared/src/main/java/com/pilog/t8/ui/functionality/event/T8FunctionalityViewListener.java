package com.pilog.t8.ui.functionality.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityViewListener extends EventListener
{
    public void functionalityViewUpdated(T8FunctionalityViewUpdatedEvent event);
    public void functionalityViewClosed(T8FunctionalityViewClosedEvent event);
}
