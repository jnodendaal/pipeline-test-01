/**
 * Created on 30 Mar 2016, 7:15:02 AM
 */
package com.pilog.t8.definition.data.connection.pool;

import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Gavin Boshoff
 */
public class T8HikariCPDataConnectionPoolDefinition extends T8DataConnectionPoolDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_CONNECTION_POOL_HIKARICP";
    public static final String DISPLAY_NAME = "HikariCP Connection Pool";
    public static final String DESCRIPTION = "A HikariCP connection pool.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8HikariCPDataConnectionPoolDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return super.getDatumTypes();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataConnectionPool createNewConnectionPoolInstance(T8DataTransaction dataAccessProvider) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.data.connection.pool.T8HikariCPDataConnectionPool", new Class<?>[]{this.getClass(), T8DataTransaction.class}, this, dataAccessProvider);
    }
}