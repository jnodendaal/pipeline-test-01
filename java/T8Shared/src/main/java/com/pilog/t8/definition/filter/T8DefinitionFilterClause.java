package com.pilog.t8.definition.filter;

import com.pilog.t8.definition.T8Definition;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionFilterClause
{
    public enum DefinitionFilterConjunction {AND, OR};
    
    public T8DefinitionFilterClause copy();
    public T8DefinitionFilterCriterion getFilterCriterion(String identifier);
    public List<T8DefinitionFilterCriterion> getFilterCriterionList();
    public boolean includesDefinition(T8Definition definition);
    public Set<String> getFilterDatumIdentifiers();
}
