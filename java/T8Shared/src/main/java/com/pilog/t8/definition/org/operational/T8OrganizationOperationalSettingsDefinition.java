/**
 * Created on 15 Jul 2016, 7:18:28 AM
 */
package com.pilog.t8.definition.org.operational;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8OrganizationOperationalSettingsDefinition extends T8Definition
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OrganizationOperationalSettingsDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_ORG_OPERATIONAL_SETTINGS";
    public static final String GROUP_IDENTIFIER = "@DG_ORG_OPERATIONAL_SETTINGS";
    public static final String STORAGE_PATH = "/operational";
    public static final String DISPLAY_NAME = "Organization Operational Settings";
    public static final String DESCRIPTION = "Defines the operational details of an organization.";
    public static final String IDENTIFIER_PREFIX = "OO_";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.SYSTEM;
    public enum Datum { OPERATIONAL_INTERVAL_DEFINITION_LIST,
                        NON_WORK_DAY_DEFINITION_LIST};
    // -------- Definition Meta-Data -------- //

    public enum OperatingDay {
        MONDAY(Calendar.MONDAY),
        TUESDAY(Calendar.TUESDAY),
        WEDNESDAY(Calendar.WEDNESDAY),
        THURSDAY(Calendar.THURSDAY),
        FRIDAY(Calendar.FRIDAY),
        SATURDAY(Calendar.SATURDAY),
        SUNDAY(Calendar.SUNDAY);

        public static OperatingDay fromCalendarDayOfWeek(int calDayOfWeek)
        {
            for (OperatingDay operatingDay : values())
            {
                if (operatingDay.getCalendarDayOfWeek() == calDayOfWeek) return operatingDay;
            }

            throw new RuntimeException("Invalid Calendar Day of Week value received: DOW = " + calDayOfWeek);
        }

        private final int calendarDayOfWeek;

        private OperatingDay(int calendarDayOfWeek)
        {
            this.calendarDayOfWeek = calendarDayOfWeek;
        }

        public int getCalendarDayOfWeek()
        {
            return this.calendarDayOfWeek;
        }
    };

    public T8OrganizationOperationalSettingsDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), "Operational Intervals", "Operational/Working Intervals, based on working days, within the organization."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.NON_WORK_DAY_DEFINITION_LIST.toString(), "Non Work Days", "Non working days, such as Public holidays observed during normal operational hours."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8OrganizationOperationalIntervalDefinition.TYPE_IDENTIFIER));
        if (Datum.NON_WORK_DAY_DEFINITION_LIST.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8OrganizationOperationalNonWorkDayDefinition.TYPE_IDENTIFIER));
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception {}

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = new ArrayList<>();
        validationErrors.addAll(validateIntervalDefinitions(getOperationalIntervalDefinitions()));
        validationErrors.addAll(validateNonWorkDayDefinitions(getNonWorkDayDefinitions()));

        return validationErrors;
    }

    private List<T8DefinitionValidationError> validateIntervalDefinitions(List<T8OrganizationOperationalIntervalDefinition> intervalDefinitions)
    {
        List<T8DefinitionValidationError> validationErrors;
        int thisStartHour;
        int thisEndHour;
        int otherHour;

        validationErrors = new ArrayList<>();
        try
        {
            for (T8OrganizationOperationalIntervalDefinition intervalDefinition : intervalDefinitions)
            {
                thisStartHour = intervalDefinition.getOperatingHoursStart();
                thisEndHour = intervalDefinition.getOperatingHoursEnd();

                for (T8OrganizationOperationalIntervalDefinition otherIntervalDefinition : intervalDefinitions)
                {
                    // We can skip it if we've reached the same instance in the inner loop
                    if (intervalDefinition == otherIntervalDefinition) continue;

                    if (intervalDefinition.getOperatingDay() == otherIntervalDefinition.getOperatingDay())
                    {
                        otherHour = otherIntervalDefinition.getOperatingHoursStart();
                        if (otherHour >= thisStartHour && otherHour <= thisEndHour)
                        {
                            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), "Operational Hour intervals within the same day cannot overlap. Day: {"+intervalDefinition.getOperatingDayString()+"}" + intervalDefinition.getOperatingDay(), T8DefinitionValidationError.ErrorType.CRITICAL));
                        }

                        otherHour = otherIntervalDefinition.getOperatingHoursEnd();
                        if (otherHour >= thisStartHour && otherHour <= thisEndHour)
                        {
                            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), "Operational Hour intervals within the same day cannot overlap. Day: {"+intervalDefinition.getOperatingDayString()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                        }
                    }
                }

                if (thisStartHour > thisEndHour)
                {
                    validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), "The start time for a working day cannot be before the end time. Day: {"+intervalDefinition.getOperatingDayString()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                }
                else if (thisStartHour == thisEndHour)
                {
                    if (intervalDefinition.getOperatingMinutesEnd() <= intervalDefinition.getOperatingMinutesStart())
                    {
                        validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), "The start time for a working interval cannot be before or equal to the end time. Day: {"+intervalDefinition.getOperatingDayString()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                    }
                }
            }
        }
        catch (NullPointerException npe)
        {
            LOGGER.log("It seems like and integer value has not been filled in.", npe);
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), "Please ensure that all hour and minute values have been completed for all intervals.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    private List<T8DefinitionValidationError> validateNonWorkDayDefinitions(List<T8OrganizationOperationalNonWorkDayDefinition> nwdDefinitions)
    {
        List<T8DefinitionValidationError> validationErrors;
        boolean otherRepeated;
        boolean thisRepeated;
        Calendar otherNwdCal;
        Calendar nwdCal;

        validationErrors = new ArrayList<>();
        otherNwdCal = Calendar.getInstance();
        nwdCal = Calendar.getInstance();

        for (T8OrganizationOperationalNonWorkDayDefinition nwdDefinition : nwdDefinitions)
        {
            nwdCal.setTimeInMillis(nwdDefinition.getNonWorkingDayDateMillis());
            thisRepeated = nwdDefinition.isRepeatYearly();

            for (T8OrganizationOperationalNonWorkDayDefinition otherNwdDefinition : nwdDefinitions)
            {
                // We can skip it if we've reached the same instance in the inner loop
                if (nwdDefinition == otherNwdDefinition) continue;

                otherNwdCal.setTimeInMillis(otherNwdDefinition.getNonWorkingDayDateMillis());
                otherRepeated = otherNwdDefinition.isRepeatYearly();

                // 4 combinations. 1. This repeated; 2. Other repeated; 3. Both repeated; 4. Neither repeated
                if (thisRepeated && otherRepeated)
                {
                    if (nwdCal.get(Calendar.MONTH) == otherNwdCal.get(Calendar.MONTH))
                    {
                        if (nwdCal.get(Calendar.DAY_OF_MONTH) == otherNwdCal.get(Calendar.DAY_OF_MONTH))
                        {
                            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.NON_WORK_DAY_DEFINITION_LIST.toString(), "Two non work days that repeat yearly cannot fall on the same day of the same month. Non Work Day 1: {"+nwdDefinition.getNonWorkingDayDescription()+"} - Non Work Day 2: {"+otherNwdDefinition.getNonWorkingDayDescription()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                        }
                    }
                }
                else if (thisRepeated)
                {
                    if (nwdCal.get(Calendar.YEAR) <= otherNwdCal.get(Calendar.YEAR))
                    {
                        if (nwdCal.get(Calendar.MONTH) == otherNwdCal.get(Calendar.MONTH))
                        {
                            if (nwdCal.get(Calendar.DAY_OF_MONTH) == otherNwdCal.get(Calendar.DAY_OF_MONTH))
                            {
                                validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.NON_WORK_DAY_DEFINITION_LIST.toString(), "{"+otherNwdDefinition.getNonWorkingDayDescription()+"} falls on a day already covered by the repeated Non Work Day {"+nwdDefinition.getNonWorkingDayDescription()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                            }
                        }
                    }
                }
                else if (otherRepeated)
                {
                    if (otherNwdCal.get(Calendar.YEAR) <= nwdCal.get(Calendar.YEAR))
                    {
                        if (nwdCal.get(Calendar.MONTH) == otherNwdCal.get(Calendar.MONTH))
                        {
                            if (nwdCal.get(Calendar.DAY_OF_MONTH) == otherNwdCal.get(Calendar.DAY_OF_MONTH))
                            {
                                validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.NON_WORK_DAY_DEFINITION_LIST.toString(), "{"+nwdDefinition.getNonWorkingDayDescription()+"} falls on a day already covered by the repeated Non Work Day {"+otherNwdDefinition.getNonWorkingDayDescription()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                            }
                        }
                    }
                }
                else
                {
                    if (otherNwdCal.get(Calendar.YEAR) == nwdCal.get(Calendar.YEAR))
                    {
                        if (nwdCal.get(Calendar.DAY_OF_YEAR) == otherNwdCal.get(Calendar.DAY_OF_YEAR))
                        {
                            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.NON_WORK_DAY_DEFINITION_LIST.toString(), "Cannot have 2 Non Work days on the same date. Non Work Day 1: {"+nwdDefinition.getNonWorkingDayDescription()+"} - Non Work Day 2: {"+otherNwdDefinition.getNonWorkingDayDescription()+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                        }
                    }
                }
            }
        }

        return validationErrors;
    }

    public <T extends T8OperationalHoursCalculator> T getOrganizationOperationalHoursCalculator()
    {
        return T8Reflections.getSingletonInstance("com.pilog.t8.org.operational.T8OrganizationOperationalHoursCalculator", "getInstance", new Class<?>[]{T8OrganizationOperationalSettingsDefinition.class}, this);
    }

    public ArrayList<T8OrganizationOperationalIntervalDefinition> getOperationalIntervalDefinitions()
    {
        return getDefinitionDatum(Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST);
    }

    public void addOperationalIntervalDefinition(T8OrganizationOperationalIntervalDefinition operationalIntervalDefinition)
    {
        addSubDefinition(Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), operationalIntervalDefinition);
    }

    public void setOperationalIntervalDefinitions(ArrayList<T8OrganizationOperationalIntervalDefinition> operationalIntervalDefinitions)
    {
        setDefinitionDatum(Datum.OPERATIONAL_INTERVAL_DEFINITION_LIST.toString(), operationalIntervalDefinitions);
    }

    public void replaceOperationalIntervalDefinition(T8OrganizationOperationalIntervalDefinition operationalIntervalDefinition)
    {
        Iterator<T8OrganizationOperationalIntervalDefinition> operationalIntervalDefinitionIter;
        ArrayList<T8OrganizationOperationalIntervalDefinition> operationalIntervalDefinitions;
        String replacementIdentifier;

        replacementIdentifier = operationalIntervalDefinition.getIdentifier();
        operationalIntervalDefinitions = getOperationalIntervalDefinitions();
        operationalIntervalDefinitionIter = operationalIntervalDefinitions.iterator();

        while (operationalIntervalDefinitionIter.hasNext())
        {
            if (operationalIntervalDefinitionIter.next().getIdentifier().equals(replacementIdentifier))
            {
                operationalIntervalDefinitionIter.remove();
                break;
            }
        }

        operationalIntervalDefinitions.add(operationalIntervalDefinition);
        setOperationalIntervalDefinitions(operationalIntervalDefinitions);
    }

    public ArrayList<T8OrganizationOperationalNonWorkDayDefinition> getNonWorkDayDefinitions()
    {
        return getDefinitionDatum(Datum.NON_WORK_DAY_DEFINITION_LIST);
    }

    public void addNonWorkDayDefinition(T8OrganizationOperationalNonWorkDayDefinition nonWorkDayDefinition)
    {
        addSubDefinition(Datum.NON_WORK_DAY_DEFINITION_LIST.toString(), nonWorkDayDefinition);
    }

    public void setNonWorkDayDefinitions(ArrayList<T8OrganizationOperationalNonWorkDayDefinition> nonWorkDayDefinitions)
    {
        setDefinitionDatum(Datum.NON_WORK_DAY_DEFINITION_LIST, nonWorkDayDefinitions);
    }

    public void replaceNonWorkDayDefinition(T8OrganizationOperationalNonWorkDayDefinition nonWorkDayDefinition)
    {
        Iterator<T8OrganizationOperationalNonWorkDayDefinition> nonWorkDayDefinitionIter;
        ArrayList<T8OrganizationOperationalNonWorkDayDefinition> nonWorkDayDefinitions;
        String replacementIdentifier;

        replacementIdentifier = nonWorkDayDefinition.getIdentifier();
        nonWorkDayDefinitions = getNonWorkDayDefinitions();
        nonWorkDayDefinitionIter = nonWorkDayDefinitions.iterator();

        while (nonWorkDayDefinitionIter.hasNext())
        {
            if (nonWorkDayDefinitionIter.next().getIdentifier().equals(replacementIdentifier))
            {
                nonWorkDayDefinitionIter.remove();
                break;
            }
        }

        nonWorkDayDefinitions.add(nonWorkDayDefinition);
        setNonWorkDayDefinitions(nonWorkDayDefinitions);
    }
}