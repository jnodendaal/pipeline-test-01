package com.pilog.t8.data;

/**
 * @author Bouwer du Preez
 */
public interface T8DataKeyGenerator
{
    public void init() throws Exception;
    public void destroy();
    
    public String generateKey() throws Exception;
}
