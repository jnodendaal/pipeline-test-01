package com.pilog.t8.data.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Bouwer du Preez
 * @param <S> The type of the stateful object to which this state history is applicable.
 */
public class T8StateStore<S> implements Serializable
{
    private final LinkedHashMap<String, S> states;

    public T8StateStore()
    {
        this.states = new LinkedHashMap<>();
    }

    public void clear()
    {
        states.clear();
    }

    public List<S> getStates()
    {
        return new ArrayList<S>(states.values());
    }

    public S putState(String stateId, S state)
    {
        return states.put(stateId, state);
    }

    public S getState(String stateId)
    {
        return states.get(stateId);
    }
}
