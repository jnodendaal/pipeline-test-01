/**
 * Created on 15 Dec 2015, 8:23:41 AM
 */
package com.pilog.t8.definition.data.source.sql;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Gavin Boshoff
 */
public class T8SQLPostOrderedDataSourceDefinition extends T8SQLDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_SQL_PO";
    public static final String DISPLAY_NAME = "SQL Post Ordered Data Source";
    public static final String DESCRIPTION = "A data source that reads using custom SQL and orders the final result set after retrieval.";
    public enum Datum {
        NULLS_FIRST;
    };
    // -------- Definition Meta-Data -------- //

    public T8SQLPostOrderedDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.NULLS_FIRST.toString(), "Nulls First", "Whether null values should be added to the beginning or end of the result set.", false));

        return datumTypes;
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction accessProvider)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.source.sql.T8SQLPostOrderedDataSource", new Class<?>[]{T8SQLPostOrderedDataSourceDefinition.class, T8DataTransaction.class}, this, accessProvider);
    }

    public boolean isNullsFirst()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.NULLS_FIRST));
    }

    public void setNullsFirst(boolean nullsFirst)
    {
        setDefinitionDatum(Datum.NULLS_FIRST, nullsFirst);
    }
}