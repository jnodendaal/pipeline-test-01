package com.pilog.t8.data.entity;

import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityValidationException extends Exception
{
    private final List<T8DataEntityValidationReport> validationReports;

    public T8DataEntityValidationException(String message, List<T8DataEntityValidationReport> validationReports)
    {
        super(message);
        this.validationReports = validationReports;
    }   

    public List<T8DataEntityValidationReport> getValidationReports()
    {
        return validationReports;
    }
}
