package com.pilog.t8.data.object.filter;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.data.filter.T8DataFilterCriteriaDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriterion.ObjectFilterOperator;

/**
 * @author Bouwer du Preez
 */
public class T8ObjectFilterCriteria implements T8ObjectFilterClause, Serializable
{
    private final LinkedHashMap<T8ObjectFilterClause, ObjectFilterConjunction> filterClauses;
    private String objectId;
    private String identifier;

    public T8ObjectFilterCriteria()
    {
        filterClauses = new LinkedHashMap<T8ObjectFilterClause, ObjectFilterConjunction>();
    }

    public T8ObjectFilterCriteria(String identifier)
    {
        this();
        this.identifier = identifier;
    }

    public T8ObjectFilterCriteria(T8DataFilterCriteriaDefinition definition)
    {
        this();
        this.identifier = definition.getIdentifier();
    }

    public T8ObjectFilterCriteria(Map<String, Object> keyMap)
    {
        this();
        addKeyFilter(keyMap, false);
    }

    public T8ObjectFilterCriteria(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        this();
        addKeyFilter(keyMap, caseInsensitive);
    }

    public T8ObjectFilterCriteria(ObjectFilterConjunction conjunction, T8ObjectFilterClause... filterClauses)
    {
        this();
        addFilterClauses(conjunction, filterClauses);
    }

    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public T8ObjectFilterCriteria copy()
    {
        T8ObjectFilterCriteria copiedCriteria;

        copiedCriteria = new T8ObjectFilterCriteria(identifier);
        for (T8ObjectFilterClause filterClause : filterClauses.keySet())
        {
            copiedCriteria.addFilterClause(filterClauses.get(filterClause), filterClause.copy());
        }

        return copiedCriteria;
    }

    public void clearClauses()
    {
        filterClauses.clear();
    }

    public void addKeyFilter(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        for (String key : keyMap.keySet())
        {
            Object value;

            value = keyMap.get(key);
            if (value == null)
            {
                addFilterClause(ObjectFilterConjunction.AND, key, ObjectFilterOperator.IS_NULL, value, caseInsensitive);
            }
            else if (value instanceof Collection)
            {
                addFilterClause(ObjectFilterConjunction.AND, key, ObjectFilterOperator.IN, value, caseInsensitive);
            }
            else
            {
                addFilterClause(ObjectFilterConjunction.AND, key, ObjectFilterOperator.EQUAL, value, caseInsensitive);
            }
        }
    }

    public void addClause(String conjunction, T8ObjectFilterClause filterClause)
    {
        addFilterClause(ObjectFilterConjunction.valueOf(conjunction), filterClause);
    }

    public void addFilterClause(ObjectFilterConjunction conjunction, T8ObjectFilterClause filterClause)
    {
        if (filterClause == null) throw new IllegalArgumentException("Invalid Filter Clause: The filter clause cannot be null");
        else
        {
            filterClauses.put(filterClause, conjunction);
        }
    }

    public void setFilterClauses(LinkedHashMap<T8ObjectFilterClause, ObjectFilterConjunction> filterClauses)
    {
        this.filterClauses.clear();
        for (T8ObjectFilterClause clause : filterClauses.keySet())
        {
            addFilterClause(filterClauses.get(clause), clause);
        }
    }

    public void addFilterClauses(ObjectFilterConjunction conjunction, T8ObjectFilterClause... filterClauses)
    {
        for (T8ObjectFilterClause filterClause : filterClauses)
        {
            addFilterClause(conjunction, filterClause);
        }
    }

    public void addFilterClause(ObjectFilterConjunction conjunction, String columnName, ObjectFilterOperator operator, Object filterValue)
    {
        addFilterClause(conjunction, new T8ObjectFilterCriterion(columnName, operator, filterValue, false));
    }

    public void addFilterClause(ObjectFilterConjunction conjunction, String columnName, ObjectFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(conjunction, new T8ObjectFilterCriterion(columnName, operator, filterValue, caseInsensitive));
    }

    public void addFilterCriterion(ObjectFilterConjunction conjunction, String columnName, ObjectFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(conjunction, new T8ObjectFilterCriterion(columnName, operator, filterValue, caseInsensitive));
    }

    public void addFilterCriterion(String conjunction, String columnName, String operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(ObjectFilterConjunction.valueOf(conjunction), new T8ObjectFilterCriterion(columnName, ObjectFilterOperator.valueOf(operator), filterValue, caseInsensitive));
    }

    public ObjectFilterConjunction getConjunction(T8ObjectFilterClause clause)
    {
        return filterClauses.get(clause);
    }

    public LinkedHashMap<T8ObjectFilterClause, ObjectFilterConjunction> getFilterClauses()
    {
        return filterClauses;
    }

    public void setFilterClauses(Map<T8ObjectFilterClause, ObjectFilterConjunction> clauses)
    {
        this.filterClauses.clear();
        for (T8ObjectFilterClause clause : clauses.keySet())
        {
            addFilterClause(clauses.get(clause), clause);
        }
    }

    @Override
    public boolean hasCriteria()
    {
        for (T8ObjectFilterClause clause : filterClauses.keySet())
        {
            if (clause.hasCriteria()) return true;
        }

        return false;
    }

    @Override
    public T8ObjectFilterCriterion getFilterCriterion(String identifier)
    {
        for (T8ObjectFilterClause clause : filterClauses.keySet())
        {
            T8ObjectFilterCriterion filterCriterion;

            filterCriterion = clause.getFilterCriterion(identifier);
            if (filterCriterion != null) return filterCriterion;
        }

        return null;
    }

    @Override
    public Set<String> getFilterFieldIds()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<String>();
        for (T8ObjectFilterClause filterClause : filterClauses.keySet())
        {
            fieldSet.addAll(filterClause.getFilterFieldIds());
        }

        return fieldSet;
    }

    /**
     * Removes the {@code T8ObjectFilterCriterion} objects from the associated {@code T8ObjectFilterCriteria} by matching it up
     * with the field identifier supplied.
     *
     * @param filterCriterion The {@code String} field identifier associated with the {@code T8ObjectFilterCriterion} objects to be removed.
     */
    public void removeFilterCriterionByField(String fieldIdentifier)
    {
        // Loops through the Criterion List and finds the criterion's that needs to be removed.
        for (T8ObjectFilterCriterion filterCriterion : getFilterCriterionList())
        {
            if (filterCriterion.getFieldIdentifier().equals(fieldIdentifier))
            {
                removeFilterClause(filterCriterion);
            }
        }
    }

    /**
     * Removes the given {@code T8ObjectFilterClause} from the clause list.
     *
     * @param filterClause The {@code T8ObjectFilterClause} that needs to be removed.
     */
    public void removeFilterClause(T8ObjectFilterClause filterClause)
    {
        List<T8ObjectFilterClause> filterClauseRemovalList;

        filterClauseRemovalList = new ArrayList<T8ObjectFilterClause>();
        // If the Filter clause is not found at this level check in the sub clauses
        if (filterClauses.remove(filterClause) == null)
        {
            for (T8ObjectFilterClause filterClauseFromCriteria : filterClauses.keySet())
            {
                if (filterClauseFromCriteria instanceof T8ObjectFilterCriteria)
                {
                    ((T8ObjectFilterCriteria)filterClauseFromCriteria).removeFilterClause(filterClause);

                    if (filterClauseFromCriteria.getFilterCriterionList().isEmpty())
                    {
                        filterClauseRemovalList.add(filterClauseFromCriteria);
                    }
                }
            }
        }

        for (T8ObjectFilterClause removeFilterClause : filterClauseRemovalList)
        {
            filterClauses.remove(removeFilterClause);
        }
    }

    @Override
    public List<T8ObjectFilterCriterion> getFilterCriterionList()
    {
        ArrayList<T8ObjectFilterCriterion> criterionList;

        criterionList = new ArrayList<T8ObjectFilterCriterion>();
        for (T8ObjectFilterClause filterClause : filterClauses.keySet())
        {
            criterionList.addAll(filterClause.getFilterCriterionList());
        }

        return criterionList;
    }

    public boolean equalsFilterCriteria(T8ObjectFilterCriteria other)
    {
        if (other == null)
        {
            return false;
        }
        else if (!Objects.equals(identifier, other.identifier))
        {
            return false;
        }
        else if((this.filterClauses == null && other.filterClauses != null) || (this.filterClauses != null && other.filterClauses == null))
        {
            return false;
        }
        else if (this.filterClauses != null && other.filterClauses != null)
        {
            if((this.filterClauses.isEmpty() && !other.filterClauses.isEmpty()) || (!this.filterClauses.isEmpty() && other.filterClauses.isEmpty()))
            {
                return false;
            }

            for (T8ObjectFilterClause t8DataFilterClause : filterClauses.keySet())
            {
                if(!other.filterClauses.containsKey(t8DataFilterClause))
                {
                    return false;
                }
            }

            return true;
        }
        else return true;
    }

    @Override
    public T8DataFilterClause getDataFilterClause(String entityId, Map<String, String> objectToEntityFieldMap)
    {
        return getDataFilterCriteria(entityId, objectToEntityFieldMap);
    }

    public T8DataFilterCriteria getDataFilterCriteria(String entityId, Map<String, String> objectToEntityFieldMap)
    {
        T8DataFilterCriteria criteria;

        criteria = new T8DataFilterCriteria();
        for (T8ObjectFilterClause clause : this.filterClauses.keySet())
        {
            criteria.addFilterClause(filterClauses.get(clause).getDataFilterConjunction(), clause.getDataFilterClause(entityId, objectToEntityFieldMap));
        }

        return criteria;
    }

    @Override
    public String toString()
    {
        return "T8DataObjectFilterCriteria{" + "objectId=" + objectId + ", identifier=" + identifier + '}';
    }
}
