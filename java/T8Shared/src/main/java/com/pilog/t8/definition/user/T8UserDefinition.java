package com.pilog.t8.definition.user;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.flow.T8WorkFlowProfileDefinition;
import com.pilog.t8.definition.language.T8LanguageDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8UserDefinition extends T8Definition
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8UserDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_USER";
    public static final String DISPLAY_NAME = "User";
    public static final String DESCRIPTION = "A registered system user.";
    public static final String IDENTIFIER_PREFIX = "U_";
    public enum Datum
    {
        USERNAME,
        PASSWORD,
        RESET_PASSWORD_FLAG,
        USER_PROFILE_IDENTIFIERS,
        WORK_FLOW_PROFILE_IDENTIFIERS,
        MAXIMUM_CONCURRENT_SESSION_LIMIT,
        NAME,
        SURNAME,
        EMAIL_ADDRESS,
        LANGUAGE_IDENTIFIER,
        ORGANIZATION_IDENTIFIER,
        MOBILE_NUMBER,
        WORK_TELEPHONE_NUMBER,
        TEMP_PW_HASH,
        TEMP_PW_EXPIRY_TIME,
        API_KEY,
        WINDOWS_SID,
        ACTIVE,
        LOCK_INDICATOR,
        LOCK_REASON,
        USER_ACTIVATION_DATE,
        USER_EXPIRY_DATE,
        LAST_LOGIN_DATE,
        PASSWORD_ACTIVATION_DATE,
        PASSWORD_HISTORY_LIST,
        INCORRECT_PASSWORD_ENTRY_COUNT,
        REMOTE_SERVER_CONNECTIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8UserDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "If the user is active.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOCK_INDICATOR.toString(), "Lock Indicator", "If the user is locked and not allowed to log in.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.LOCK_REASON.toString(), "Lock Reason", "The reason for the lock (if available)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MAXIMUM_CONCURRENT_SESSION_LIMIT.toString(), "Maximum Concurrent Session Limit", "The maximum number of concurrent sessions allowed for this user.", 1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.NAME.toString(), "Name", "The name of the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SURNAME.toString(), "Surname", "The surname of the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.EMAIL_ADDRESS.toString(), "E-mail Address", "The e-mail address of the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MOBILE_NUMBER.toString(), "Mobile number", "The mobile number of the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.WORK_TELEPHONE_NUMBER.toString(), "Work telephone number", "The work telephone number of the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.USERNAME.toString(), "Username", "The username to be used for authentication purposes."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.PASSWORD_HASH, Datum.PASSWORD.toString(), "Password", "The password to be used for authentication purposes.").setHidden(true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.API_KEY.toString(), "Webservice API Key", "The api key that will be used when accessing application from web service.", T8IdentifierUtilities.createNewGUID()));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.WINDOWS_SID.toString(), "Windows Sid", "The windows security identifier associated with this user on the local domain."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RESET_PASSWORD_FLAG.toString(), "Reset Password", "If this boolean value is set to true, the user will be prompted to enter a new password during the next login."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.USER_EXPIRY_DATE.toString(), "User Expiry Date", "The date when this user's access to the system will expire."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.LAST_LOGIN_DATE.toString(), "Last Login Date", "The date when this user was last logged in to the system."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.PASSWORD_ACTIVATION_DATE.toString(), "Password Activation Date", "The date when this users password was made active.").setHidden(true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.USER_ACTIVATION_DATE.toString(), "User Activation Date", "The date when this user was made active.", new T8Timestamp(new Date().getTime())).setHidden(true));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.USER_PROFILE_IDENTIFIERS.toString(), "Profiles", "The profiles to which this user has access."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.WORK_FLOW_PROFILE_IDENTIFIERS.toString(), "Workflow Profiles", "The workflow profiles to which this user belongs."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.LANGUAGE_IDENTIFIER.toString(), "Language", "The language to which the UI text will be translated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.ORGANIZATION_IDENTIFIER.toString(), "Organization", "The Organization to which this user belongs."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TEMP_PW_HASH.toString(), "Temporary Password Hash", "A temporary password hash used for user password reset functions.").setHidden(true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.TEMP_PW_EXPIRY_TIME.toString(), "Temporary Password Expiry Time", "The time at which the remporary password is no longer considered valid.").setHidden(true));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.PASSWORD_HASH), Datum.PASSWORD_HISTORY_LIST.toString(), "Password History List", "A List containing the the users previous passwords.").setHidden(true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.INCORRECT_PASSWORD_ENTRY_COUNT.toString(), "Incorrect Password Entry Count", "The amount of time the user has entered its password incorrectly in one session.", 0).setHidden(true));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REMOTE_SERVER_CONNECTIONS.toString(), "Remote Server Connections", "The remote server connections that this user can connect to."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.USER_PROFILE_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8UserProfileDefinition.GROUP_IDENTIFIER));
        else if (Datum.WORK_FLOW_PROFILE_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowProfileDefinition.GROUP_IDENTIFIER));
        else if (Datum.LANGUAGE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8LanguageDefinition.GROUP_IDENTIFIER));
        else if (Datum.REMOTE_SERVER_CONNECTIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8UserRemoteServerConnectionDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.user.T8UserDefinitionActionHandler", new Class<?>[]{T8Context.class, T8UserDefinition.class}, context, this);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;
        T8UserProfileDefinition userProfileDefinition;
        List<String> workFlowProfileIdentifiers;
        T8DefinitionManager definitionManager;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        // Get the definition manager instance to retrieve raw definitions
        definitionManager = serverContext.getDefinitionManager();

        // Create a local instance of the work flow profile identifier list
        workFlowProfileIdentifiers = getWorkFlowProfileIdentifiers();

        // If the user has no profiles linked, just ignore it
        if (this.getProfileIdentifiers() == null) return validationErrors;

        // We want to ensure that each of the Work Flow Profiles associated
        // with a user profile have been linked to the current user definition
        for (String profileId : this.getProfileIdentifiers())
        {
            try
            {
                userProfileDefinition = definitionManager.getRawDefinition(null, getRootProjectId(), profileId);
                if (userProfileDefinition.getAssociatedWorkFlowProfileIdentifiers() == null) continue;

                for (String associatedWorkFlowProfileIdentifier : userProfileDefinition.getAssociatedWorkFlowProfileIdentifiers())
                {
                    if (workFlowProfileIdentifiers == null || !workFlowProfileIdentifiers.contains(associatedWorkFlowProfileIdentifier))
                    {
                        validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.WORK_FLOW_PROFILE_IDENTIFIERS.toString(), "Missing Associated WorkFlow Profile {"+associatedWorkFlowProfileIdentifier+"}", T8DefinitionValidationError.ErrorType.CRITICAL));
                    }
                }
            }
            catch (Exception ex)
            {
                LOGGER.log(() -> "Validation error. Definition {"+getIdentifier()+"} validating Profile {"+profileId+"}");
                validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), "N/A", "Error during validation check. " + ex.getMessage(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        return validationErrors;
    }

    @Override
    public String getDisplayIdentifier()
    {
        StringBuffer displayID;
        String name;
        String surname;

        name = getName();
        surname = getSurname();
        displayID = new StringBuffer();
        if (name != null)
        {
            displayID.append(name);
            displayID.append(" ");
        }

        if (surname != null)
        {
            displayID.append(surname);
            displayID.append(" ");
        }

        displayID.append("(");
        displayID.append(getIdentifier());
        displayID.append(")");
        return displayID.toString();
    }

    public int getMaximumConcurrentSessionLimit()
    {
        Integer value;

        value = getDefinitionDatum(Datum.MAXIMUM_CONCURRENT_SESSION_LIMIT);
        return value != null ? value : 1;
    }

    public void setMaximumConcurrentSessionLimit(int limit)
    {
        setDefinitionDatum(Datum.MAXIMUM_CONCURRENT_SESSION_LIMIT, limit);
    }

    public String getUsername()
    {
        return getDefinitionDatum(Datum.USERNAME);
    }

    public void setUsername(String username)
    {
        setDefinitionDatum(Datum.USERNAME, username);
    }

    public String getPassword()
    {
        return getDefinitionDatum(Datum.PASSWORD);
    }

    public void setPassword(String password)
    {
        setDefinitionDatum(Datum.PASSWORD, password);
        setPasswordActivationDate(new Date());
        addPasswordToHistoryList(password);
    }

    public String getApiKey()
    {
        return getDefinitionDatum(Datum.API_KEY);
    }

    public void generateApiKey()
    {
        setDefinitionDatum(Datum.API_KEY, T8IdentifierUtilities.createNewGUID());
    }

    public String getWindowsSid()
    {
        return getDefinitionDatum(Datum.WINDOWS_SID);
    }

    public void setWindowsSid(String sid)
    {
        setDefinitionDatum(Datum.WINDOWS_SID, sid);
    }

    public List<String> getProfileIdentifiers()
    {
        return getDefinitionDatum(Datum.USER_PROFILE_IDENTIFIERS);
    }

    public void setProfileIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.USER_PROFILE_IDENTIFIERS, identifiers);
    }

    public List<String> getWorkFlowProfileIdentifiers()
    {
        return getDefinitionDatum(Datum.WORK_FLOW_PROFILE_IDENTIFIERS);
    }

    public void setWorkFlowProfileIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.WORK_FLOW_PROFILE_IDENTIFIERS, identifiers);
    }

    public String getLanguageIdentifier()
    {
        return getDefinitionDatum(Datum.LANGUAGE_IDENTIFIER);
    }

    public void setLanguageIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.LANGUAGE_IDENTIFIER, identifier);
    }

    public String getOrganizationIdentifier()
    {
        return getDefinitionDatum(Datum.ORGANIZATION_IDENTIFIER);
    }

    public void setOrganizationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ORGANIZATION_IDENTIFIER, identifier);
    }

    public String getName()
    {
        return getDefinitionDatum(Datum.NAME);
    }

    public void setName(String name)
    {
        setDefinitionDatum(Datum.NAME, name);
    }

    public String getSurname()
    {
        return getDefinitionDatum(Datum.SURNAME);
    }

    public void setSurname(String surname)
    {
        setDefinitionDatum(Datum.SURNAME, surname);
    }

    public String getEmailAddress()
    {
        return getDefinitionDatum(Datum.EMAIL_ADDRESS);
    }

    public void setEmailAddress(String address)
    {
        setDefinitionDatum(Datum.EMAIL_ADDRESS, address);
    }

    public String getMobileNumber()
    {
        return getDefinitionDatum(Datum.MOBILE_NUMBER);
    }

    public void setMobileNumber(String address)
    {
        setDefinitionDatum(Datum.MOBILE_NUMBER, address);
    }

    public String getWorkTelephoneNumber()
    {
        return getDefinitionDatum(Datum.WORK_TELEPHONE_NUMBER);
    }

    public void setWorkTelephoneNumber(String address)
    {
        setDefinitionDatum(Datum.WORK_TELEPHONE_NUMBER, address);
    }

    public boolean isResetPasswordFlag()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.RESET_PASSWORD_FLAG));
    }

    public void setResetPasswordFlag(boolean reset)
    {
        setDefinitionDatum(Datum.RESET_PASSWORD_FLAG, reset);
    }

    public boolean isActive()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.ACTIVE));
    }

    public void setActive(boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE, active);
    }

    public boolean isUserLocked()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.LOCK_INDICATOR));
    }

    public void setUserLocked(boolean locked)
    {
        setDefinitionDatum(Datum.LOCK_INDICATOR, locked);
    }

    public String getUserLockReason()
    {
        return getDefinitionDatum(Datum.LOCK_REASON);
    }

    public void setUserLockReason(String reason)
    {
        setDefinitionDatum(Datum.LOCK_REASON, reason);
    }

    public String getTemporaryPasswHash()
    {
        return getDefinitionDatum(Datum.TEMP_PW_HASH);
    }

    public void setTempPasswHash(String tempPasswHash)
    {
        setDefinitionDatum(Datum.TEMP_PW_HASH, tempPasswHash);
    }

    public T8Timestamp getTempPasswExpiryTime()
    {
        return T8Timestamp.fromTime(getDefinitionDatum(Datum.TEMP_PW_EXPIRY_TIME));
    }

    public void setTempPasswExpiryTime(Date date)
    {
        setDefinitionDatum(Datum.TEMP_PW_EXPIRY_TIME, date != null ? date.getTime() : null);
    }

    public T8Timestamp getPasswordActivationDate()
    {
        return T8Timestamp.fromTime(getDefinitionDatum(Datum.PASSWORD_ACTIVATION_DATE));
    }

    public void setPasswordActivationDate(Date date)
    {
        setDefinitionDatum(Datum.PASSWORD_ACTIVATION_DATE, date != null ? date.getTime() : null);
    }

    public T8Timestamp getUserActivationDate()
    {
        return T8Timestamp.fromTime((Long)getDefinitionDatum(Datum.USER_ACTIVATION_DATE));
    }

    public void setUserActivationDate(T8Timestamp date)
    {
        setDefinitionDatum(Datum.USER_ACTIVATION_DATE, date != null ? date.getMilliseconds() : null);
    }

    public T8Timestamp getUserExpiryDate()
    {
        return T8Timestamp.fromTime((Long)getDefinitionDatum(Datum.USER_EXPIRY_DATE));
    }

    public void setUserExpiryDate(T8Timestamp date)
    {
        setDefinitionDatum(Datum.USER_EXPIRY_DATE, date != null ? date.getMilliseconds() : null);
    }

    public Date getLastLoginDate()
    {
        Long millis;

        millis = getDefinitionDatum(Datum.LAST_LOGIN_DATE);
        return millis != null ? new Date(millis) : null;
    }

    public void setLastLoginDate(Date date)
    {
        setDefinitionDatum(Datum.LAST_LOGIN_DATE, date != null ? date.getTime() : null);
    }

    public Integer getIncorrectPasswordEntryCount()
    {
        return getDefinitionDatum(Datum.INCORRECT_PASSWORD_ENTRY_COUNT);
    }

    public void setIncorrectPasswordEntryCount(Integer count)
    {
        setDefinitionDatum(Datum.INCORRECT_PASSWORD_ENTRY_COUNT, count);
    }

    public void addPasswordToHistoryList(String password)
    {
        List<String> passwordHistory;

        passwordHistory = getUserPasswordHistoryList();
        if(passwordHistory == null) passwordHistory = new LinkedList<>();
        passwordHistory.add(password);

        setUserPasswordHistoryList(passwordHistory);
    }

    public List<String> getUserPasswordHistoryList()
    {
        return getDefinitionDatum(Datum.PASSWORD_HISTORY_LIST);
    }

    public void setUserPasswordHistoryList(List<String> history)
    {
        setDefinitionDatum(Datum.PASSWORD_HISTORY_LIST, history);
    }

    public List<T8UserRemoteServerConnectionDefinition> getUserRemoteServerConnectionDefinitions()
    {
        return getDefinitionDatum(Datum.REMOTE_SERVER_CONNECTIONS);
    }

    public void setUserRemoteServerConnectionDefinitions(List<T8UserRemoteServerConnectionDefinition> userRemoteServerConnectionDefinitions)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_CONNECTIONS, userRemoteServerConnectionDefinitions);
    }
}
