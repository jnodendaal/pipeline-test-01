package com.pilog.t8.data.filter;

import com.pilog.epic.annotation.EPICMethod;
import com.pilog.epic.annotation.EPICParameter;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilter implements Serializable
{
    private String filterIdentifier; // Only applicable if the filter does not reference its definition.
    private String entityId;
    private T8DataFilterCriteria filterCriteria; // Must never be null.
    private final LinkedHashMap<String, OrderMethod> fieldOrdering;
    private final List<String> fieldGrouping;
    private final List<String> fieldIdentifiers; // Identifiers of fields to include (null indicates that all fields must be included).

    public enum OrderMethod {ASCENDING, DESCENDING};

    public T8DataFilter(String entityId)
    {
        this.entityId = entityId;
        this.filterCriteria = new T8DataFilterCriteria();
        this.filterCriteria.setEntityIdentifier(entityId);
        this.fieldOrdering = new LinkedHashMap<>();
        this.fieldGrouping = new ArrayList<>();
        this.fieldIdentifiers = new ArrayList<>();
    }

    public T8DataFilter(T8DataFilterDefinition definition)
    {
        this(definition.getDataEntityIdentifier());
        this.filterIdentifier = definition.getIdentifier();
    }

    public T8DataFilter(String entityIdentifier, Map<String, Object> keyMap)
    {
        this(entityIdentifier);
        filterCriteria.addKeyFilter(keyMap, false);
    }

    public T8DataFilter(String entityIdentifier, Map<String, Object> keyMap, boolean caseInsensitive)
    {
        this(entityIdentifier);
        filterCriteria.addKeyFilter(keyMap, caseInsensitive);
    }

    public T8DataFilter(String entityIdentifier, T8DataFilterCriteria filterCriteria)
    {
        this(entityIdentifier);
        setFilterCriteria(filterCriteria);
    }

    public T8DataFilter(String entityIdentifier, List<Map<String, Object>> keyList)
    {
        this(entityIdentifier);
        for (Map<String, Object> key : keyList)
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR, new T8DataFilterCriteria(key, false));
        }
    }

    public T8DataFilter copy()
    {
        T8DataFilter copiedFilter;

        copiedFilter = new T8DataFilter(entityId);
        copiedFilter.setFieldOrdering(new LinkedHashMap<>(fieldOrdering));
        copiedFilter.setFilterCriteria(filterCriteria.copy());
        copiedFilter.setFieldGrouping(new ArrayList<>(fieldGrouping));

        return copiedFilter;
    }

    public String getIdentifier()
    {
        return filterIdentifier;
    }

    public void setIdentifier(String filterIdentifier)
    {
        this.filterIdentifier = filterIdentifier;
    }

    @EPICMethod(Description = "Gets the Entity Identifier associated with this data filter.")
    public String getEntityIdentifier()
    {
        return entityId;
    }

    /**
     * Sets the entity identifier for this filter.  All of the filter criteria
     * current belonging to the filter are also updated to reflect the use of
     * the new entity identifier.  This means that all current field identifiers
     * used in the filter will have their namespace set to the new entity
     * identifier.
     * @param identifier The new entity identifier to set on the filter.
     */
    public void setEntityIdentifier(String identifier)
    {
        LinkedHashMap<String, OrderMethod> oldFieldOrdering;

        // Set the entity identifier of the filter.
        entityId = identifier;

        // Update all filter criteria.
        filterCriteria.setEntityIdentifier(entityId);

        // Update field ordering.
        oldFieldOrdering = new LinkedHashMap<>(fieldOrdering);
        fieldOrdering.clear();
        for (String fieldIdentifier : oldFieldOrdering.keySet())
        {
            fieldOrdering.put(T8IdentifierUtilities.changeNamespace(fieldIdentifier, entityId), oldFieldOrdering.get(fieldIdentifier));
        }
    }

    public boolean hasFilterCriteria()
    {
        return filterCriteria != null && filterCriteria.hasCriteria();
    }

    public T8DataFilterCriteria getFilterCriteria()
    {
        return filterCriteria;
    }

    public final void setFilterCriteria(T8DataFilterCriteria filterCriteria)
    {
        if (filterCriteria != null)
        {
            this.filterCriteria = filterCriteria;
            this.filterCriteria.setEntityIdentifier(entityId);
        }
    }

    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param dataFilter The data filter from which the criteria to add to this
     * filter will be retrieved.
     * @return
     */
    @EPICMethod(Description = "Adds a data filter to this filter as a new filter criteria using the supplied conjuction.")
    public T8DataFilter addFilterCriteria(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "dataFilter") T8DataFilter dataFilter)
    {
        return addFilterCriteria(DataFilterConjunction.valueOf(conjunction), dataFilter);
    }

    public T8DataFilter addFilterCriteria(DataFilterConjunction conjunction, T8DataFilter dataFilter)
    {
        // Check that we do not add null criteria, some filters only contain order by information.
        if ((dataFilter != null) & (dataFilter.hasFilterCriteria()))
        {
            filterCriteria.addFilterClause(conjunction, dataFilter.getFilterCriteria());
        }

        return this;
    }

    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param filterCriteria The data filter criteria to add to this filter.
     * @return
     */
    @EPICMethod(Description = "Adds a new filter criteria to this data filter using the provided conjunction.")
    public T8DataFilter addFilterCriteria(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "filterCriteria") T8DataFilterCriteria filterCriteria)
    {
        return addFilterCriteria(DataFilterConjunction.valueOf(conjunction), filterCriteria);
    }

    public T8DataFilter addFilterCriteria(DataFilterConjunction conjunction, T8DataFilterCriteria filterCriteria)
    {
        this.filterCriteria.addFilterClause(conjunction, filterCriteria);
        return this;
    }

    /**
     * This is a convenience method for use from script.
     * @param keyMap The map of key identifier-value pairs that will be added
     * as additional filter criteria.
     * @return
     */
    @EPICMethod(Description = "Adds a new filter criteria based on the identifier-value pairs.")
    public T8DataFilter addFilterCriteria(@EPICParameter(VariableName = "keyMap") Map<String, Object> keyMap)
    {
        this.filterCriteria.addKeyFilter(keyMap, false);
        return this;
    }

    /**
     * This is a convenience method for use from script.
     *
     * @param conjunction The String representation of a valid conjunction
     * @param fieldIdentifier The field identifier to which the criteria are
     *      applicable
     * @param valueList The collection of values that form the criteria to be
     *      added
     *
     * @return A reference to the current {@code T8DataFilter}
     */
    @EPICMethod(Description = "Adds a new filter criteria for the specified field containing all of the supplied values.")
    public T8DataFilter addFilterCriteria(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "fieldIdentifier") String fieldIdentifier, @EPICParameter(VariableName = "valueList") Collection<Object> valueList)
    {
        return addFilterCriteria(DataFilterConjunction.valueOf(conjunction), fieldIdentifier, valueList);
    }

    public T8DataFilter addFilterCriteria(DataFilterConjunction conjunction, String fieldIdentifier, Collection<? extends Object> valueList)
    {
        filterCriteria.addFilterClause(conjunction, new T8DataFilterCriterion(fieldIdentifier, DataFilterOperator.IN, valueList));
        return this;
    }

    public T8DataFilter addFilterCriterion(DataFilterConjunction conjunction, T8DataFilterCriterion dataFilterCriterion)
    {
        filterCriteria.addFilterClause(conjunction, dataFilterCriterion);
        return this;
    }

    public T8DataFilter addFilterCriterion(DataFilterConjunction conjunction, String fieldIdentifier, DataFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        filterCriteria.addFilterClause(conjunction, fieldIdentifier, operator, filterValue, caseInsensitive);
        return this;
    }

    public T8DataFilter addFilterCriterion(String fieldIdentifier, Object filterValue)
    {
        return this.addFilterCriterion(DataFilterConjunction.AND, fieldIdentifier, DataFilterOperator.EQUAL, filterValue, false);
    }

    public T8DataFilter addFilterCriterion(String fieldIdentifier, String operator, Object filterValue)
    {
        return this.addFilterCriterion(DataFilterConjunction.AND, fieldIdentifier, DataFilterOperator.valueOf(operator), filterValue, false);
    }

    public T8DataFilter addFilterCriterion(String fieldIdentifier, DataFilterOperator operator, Object filterValue)
    {
        return this.addFilterCriterion(DataFilterConjunction.AND, fieldIdentifier, operator, filterValue, false);
    }

    public T8DataFilter addFilterCriterion(String conjunction, String fieldIdentifier, String operator, Object filterValue, boolean caseInsensitive)
    {
        return this.addFilterCriterion(DataFilterConjunction.valueOf(conjunction), fieldIdentifier, DataFilterOperator.valueOf(operator), filterValue, caseInsensitive);
    }

    public void addSubFilterCriterion(String conjunction, String fieldId, String operator, T8DataFilter subFilter, Map<String, String> fieldMapping)
    {
        addSubFilterCriterion(DataFilterConjunction.valueOf(conjunction), fieldId, DataFilterOperator.valueOf(operator), subFilter, fieldMapping);
    }

    public void addSubFilterCriterion(DataFilterConjunction conjunction, String fieldIdentifier, DataFilterOperator operator, T8DataFilter subFilter, Map<String, String> fieldMapping)
    {
        T8SubDataFilter subDataFilter;

        subDataFilter = new T8SubDataFilter(subFilter);
        subDataFilter.setSubFilterFieldMapping(fieldMapping);
        addFilterCriterion(conjunction, fieldIdentifier, operator, subDataFilter, false);
    }

    public StringBuffer getWhereClause(T8DataTransaction tx, String sqlId)
    {
        if (hasFilterCriteria())
        {
            StringBuffer whereClause;

            whereClause = new StringBuffer("WHERE ");
            whereClause.append(filterCriteria.getWhereClause(tx, sqlId));
            return whereClause;
        }
        else return null;
    }

    public ArrayList<Object> getWhereClauseParameters(T8DataTransaction tx)
    {
        return filterCriteria != null ? filterCriteria.getWhereClauseParameters(tx) : null;
    }

    public StringBuffer getOrderByClause(T8DataConnection dataConnection, T8DataEntityDefinition entityDefinition, T8DataSourceDefinition dataSourceDefinition)
    {
        if (hasFieldOrdering())
        {
            StringBuffer orderByClause;
            Iterator<String> orderingIterator;

            orderingIterator = fieldOrdering.keySet().iterator();
            orderByClause = new StringBuffer("ORDER BY");
            while (orderingIterator.hasNext())
            {
                String fieldId;

                fieldId = orderingIterator.next();
                if (entityDefinition.canOrderByField(fieldId))
                {
                    String sourceFieldIdentifier;
                    OrderMethod orderMethod;

                    orderMethod = fieldOrdering.get(fieldId);
                    sourceFieldIdentifier = entityDefinition.mapFieldToSourceIdentifier(fieldId);
                    sourceFieldIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(sourceFieldIdentifier);

                    // If we have already added stuff to the clause, prefix a comma to separate the following addition.
                    if (orderByClause.length() > 8) orderByClause.append(",");
                    orderByClause.append(" ");
                    orderByClause.append(sourceFieldIdentifier);
                    orderByClause.append(orderMethod == OrderMethod.ASCENDING ? " ASC" : " DESC");
                }
            }

            // If we haven't added any valid fields to the clause, set the String to 0 length (clear it).
            if (orderByClause.length() == 8) orderByClause.setLength(0);
            return orderByClause;
        }
        else return null;
    }

    public StringBuffer getGroupByClause(T8DataConnection dataConnection, T8DataEntityDefinition entityDefinition, T8DataSourceDefinition dataSourceDefinition)
    {
        if (hasFieldGrouping())
        {
            StringBuffer groupByClause;
            Iterator<String> groupingIterator;

            groupingIterator = fieldGrouping.iterator();
            groupByClause = new StringBuffer("GROUP BY");
            while (groupingIterator.hasNext())
            {
                String fieldIdentifier;

                fieldIdentifier = groupingIterator.next();
                if (entityDefinition.canGroupByField(fieldIdentifier))
                {
                    String sourceFieldIdentifier;

                    sourceFieldIdentifier = entityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                    sourceFieldIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(sourceFieldIdentifier);

                    // If we have already added stuff to the clause, prefix a comma to separate the following addition.
                    if (groupByClause.length() > 8) groupByClause.append(",");
                    groupByClause.append(" ");
                    groupByClause.append(sourceFieldIdentifier);
                }
            }

            // If we haven't added any valid fields to the clause, set the String to 0 length (clear it).
            if (groupByClause.length() == 8) groupByClause.setLength(0);
            return groupByClause;
        }
        else return null;
    }

    public boolean includesEntity(T8DataEntity entity)
    {
        return filterCriteria.includesEntity(entity);
    }

    public T8DataFilterCriterion getFilterCriterion(String identifier)
    {
        return filterCriteria.getFilterCriterion(identifier);
    }

    /**
     * Returns a boolean value indicating whether or not any field ordering is
     * set on this filter.  It is possible for this method to return true
     * without any of the ordering settings taking any affect on the results
     * returned using this filter.  Successful ordering is data source
     * dependent.
     *
     * @return Boolean true if any field ordering is set on this filter.
     */
    public boolean hasFieldOrdering()
    {
        return fieldOrdering.size() > 0;
    }

    public void clearFieldOrdering()
    {
        fieldOrdering.clear();
    }

    public T8DataFilter addFieldOrdering(String fieldIdentifier, OrderMethod method)
    {
        fieldOrdering.remove(fieldIdentifier); // First remove the field (if it was present) so that we know the new entry will be added ad the end of the linked map.
        fieldOrdering.put(fieldIdentifier, method);
        return this;
    }

    /**
     * This is a convenience method that can be easily used from EPIC script
     * because it takes String parameter types.
     *
     * @param fieldIdentifier Identifier of the field to order.
     * @param method The method 'ASCENDING' or 'DESCENDING' to order by.
     * @return This filter for use in method chaining.
     */
    public T8DataFilter addFieldOrdering(String fieldIdentifier, String method)
    {
        fieldOrdering.remove(fieldIdentifier); // First remove the field (if it was present) so that we know the new entry will be added ad the end of the linked map.
        fieldOrdering.put(fieldIdentifier, OrderMethod.valueOf(method));
        return this;
    }

    public OrderMethod removeFieldOrdering(String fieldIdentifier)
    {
        return fieldOrdering.remove(fieldIdentifier);
    }

    public void setFieldOrdering(String fieldIdentifier, OrderMethod method)
    {
        fieldOrdering.put(fieldIdentifier, method);
    }

    public OrderMethod getFieldOrderMethod(String fieldIdentifier)
    {
        return fieldOrdering.get(fieldIdentifier);
    }

    public LinkedHashMap<String, OrderMethod> getFieldOrdering()
    {
        return new LinkedHashMap<>(fieldOrdering);
    }

    public void setFieldOrdering(LinkedHashMap<String, OrderMethod> fieldOrdering)
    {
        this.fieldOrdering.clear();
        if (fieldOrdering != null)
        {
            this.fieldOrdering.putAll(fieldOrdering);
        }
    }

    /**
     * Returns a boolean value indicating whether or not any field grouping is
     * set on this filter.  It is possible for this method to return true
     * without any of the grouping settings taking any affect on the results
     * returned using this filter.  Successful grouping is data source
     * dependent.
     *
     * @return Boolean true if any field grouping is set on this filter.
     */
    public boolean hasFieldGrouping()
    {
        return fieldGrouping.size() > 0;
    }

    public void clearFieldGrouping()
    {
        fieldGrouping.clear();
    }

    public void addFieldGrouping(String fieldIdentifier)
    {
        fieldGrouping.remove(fieldIdentifier); // First remove the field (if it was present) so that we know the new entry will be added ad the end of the list.
        fieldGrouping.add(fieldIdentifier);
    }

    public boolean removeFieldGrouping(String fieldIdentifier)
    {
        return fieldGrouping.remove(fieldIdentifier);
    }

    public List<String> getFieldGrouping()
    {
        return fieldGrouping;
    }

    public void setFieldGrouping(List<String> fieldGrouping)
    {
        this.fieldGrouping.clear();
        if (fieldGrouping != null)
        {
            this.fieldGrouping.addAll(fieldGrouping);
        }
    }

    public Set<String> getFilterFieldIdentifiers()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<>();
        fieldSet.addAll(filterCriteria.getFilterFieldIdentifiers());
        return fieldSet;
    }

    public void setFieldIdentifiers(List<String> fieldIdentifiers)
    {
        this.fieldIdentifiers.clear();
        this.fieldIdentifiers.addAll(fieldIdentifiers);
    }

    public void addFieldIdentifier(String fieldIdentifier)
    {
        fieldIdentifiers.add(fieldIdentifier);
    }

    public void clearFieldIdentifiers()
    {
        fieldIdentifiers.clear();
    }

    public List<String> getFieldIdentifiers()
    {
        return new ArrayList<>(fieldIdentifiers);
    }

    /**
     * This method will use the field indicator to find the {@code T8DataFilterCriterion}'s to be removed from the
     * {@code T8DataFilter}.
     *
     * @param fieldIdentifier The {@code String} field identifier associated with the {@code T8DatafilterCriterion} to be removed.
     * @return Returns instance of self
     */
    public T8DataFilter removeFilterCriterionByField(String fieldIdentifier)
    {
        filterCriteria.removeFilterCriterionByField(fieldIdentifier);
        return this;
    }

    /**
     * Remove the {@code T8DataFilterClause} from the {@code T8DataFilter}.
     *
     * @param filterClause The {@code T8DataFilterClause} to be removed.
     */
    public void removeFilterClause(T8DataFilterClause filterClause)
    {
        filterCriteria.removeFilterClause(filterClause);
    }

    public List<T8DataFilterCriterion> getFilterCriterionList()
    {
        ArrayList<T8DataFilterCriterion> criterionList;

        criterionList = new ArrayList<>();
        criterionList.addAll(filterCriteria.getFilterCriterionList());
        return criterionList;
    }

    @Override
    public String toString()
    {
        return "T8DataFilter{" + "filterIdentifier=" + filterIdentifier + ", entityIdentifier=" + entityId + '}';
    }

    public void printStructure()
    {
        T8DataFilterPrinter.printStructure(System.out, this);
    }
}
