package com.pilog.t8.security;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8UserUpdateResponse implements Serializable
{
    private String userId;
    private boolean success;
    private String message;
    private T8SessionDetails sessionDetails;

    public T8UserUpdateResponse(String userId, boolean success, String message, T8SessionDetails sessionDetails)
    {
        this.userId = userId;
        this.success = success;
        this.message = message;
        this.sessionDetails = sessionDetails;
    }

    public String getIdentifier()
    {
        return userId;
    }

    public void setIdentifier(String identifier)
    {
        this.userId = identifier;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public T8SessionDetails getSessionDetails()
    {
        return sessionDetails;
    }

    public void setSessionDetails(T8SessionDetails sessionDetails)
    {
        this.sessionDetails = sessionDetails;
    }
}
