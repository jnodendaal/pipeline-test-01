package com.pilog.t8.definition.data.object;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectFieldDefinition extends T8DataValueDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT_FIELD";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Object Field";
    public static final String DESCRIPTION = "A field contained by a data object.";
    public enum Datum
    {
        DISPLAY_NAME,
        DISPLAY_DESCRIPTION,
        DISPLAY_SEQUENCE,
        DISPLAY_PRIORITY
    };
    // -------- Definition Meta-Data -------- //

    public T8DataObjectFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The name of the field, when displayed on a UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DISPLAY_DESCRIPTION.toString(), "Display Description", "The description of the field, when displayed on a UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.DISPLAY_SEQUENCE.toString(), "Display Sequence", "The sequence in which this field will be ordered relative to other fields of the object, when displayed on a UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.DISPLAY_PRIORITY.toString(), "Display Priority", "The priority of this field relative to other fields of the object, when displayed on a UI."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public Integer getDisplaySequence()
    {
        return getDefinitionDatum(Datum.DISPLAY_SEQUENCE);
    }

    public void setDisplaySequence(Integer sequence)
    {
        setDefinitionDatum(Datum.DISPLAY_SEQUENCE, sequence);
    }

    public Integer getDisplayPriority()
    {
        return getDefinitionDatum(Datum.DISPLAY_PRIORITY);
    }

    public void setDisplayPriortiy(Integer priority)
    {
        setDefinitionDatum(Datum.DISPLAY_PRIORITY, priority);
    }

    public String getDisplayName()
    {
        return getDefinitionDatum(Datum.DISPLAY_NAME);
    }

    public void setDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME, displayName);
    }

    public String getDisplayDescription()
    {
        return getDefinitionDatum(Datum.DISPLAY_DESCRIPTION);
    }

    public void setDisplayDescription(String description)
    {
        setDefinitionDatum(Datum.DISPLAY_DESCRIPTION, description);
    }
}
