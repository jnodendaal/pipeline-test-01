/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.entity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8GroupByDataEntityDefinition extends T8DataEntityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ENTITY_GROUP_BY";
    public static final String DISPLAY_NAME = "Group By Entity";
    public static final String DESCRIPTION = "A Group by Data entity";
    public static final String IDENTIFIER_PREFIX = "E_GROUP_BY_";
    public enum Datum
    {
        REFERENCE_ENTITY_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public T8GroupByDataEntityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REFERENCE_ENTITY_IDENTIFIER.toString(), "Reference Entity", "The referencing entity that will that will be used by this group by entity."));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REFERENCE_ENTITY_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    public String getReferenceEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.REFERENCE_ENTITY_IDENTIFIER.toString());
    }

    public void setReferenceEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.REFERENCE_ENTITY_IDENTIFIER.toString(), identifier);
    }
}
