package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.security.T8UserDetails;
import com.pilog.t8.security.T8UserProfileDetails;
import com.pilog.t8.security.T8WorkflowProfileDetails;

/**
 * @author Gavin Boshoff
 */
public class T8DtUserDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@USER_DETAILS";

    public T8DtUserDetails() {}

    public T8DtUserDetails(T8DefinitionManager context) {}

    public T8DtUserDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8UserDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtWorkflowProfileDetails dtWorkflowProfileDetails;
            T8DtUserProfileDetails dtUserProfileDetails;
            JsonArray userProfileArray;
            JsonArray workflowProfileArray;
            JsonObject jsonUserDetails;
            T8UserDetails userDetails;
            T8Timestamp expiryDate;

            // Create the JSON object.
            userDetails = (T8UserDetails)object;
            expiryDate = userDetails.getExpiryDate();
            userProfileArray = new JsonArray();
            workflowProfileArray = new JsonArray();

            jsonUserDetails = new JsonObject();
            jsonUserDetails.add("id", userDetails.getIdentifier());
            jsonUserDetails.add("username", userDetails.getUsername());
            jsonUserDetails.add("name", userDetails.getName());
            jsonUserDetails.add("surname", userDetails.getSurname());
            jsonUserDetails.add("emailAddress", userDetails.getEmailAddress());
            jsonUserDetails.add("mobileNumber", userDetails.getMobileNumber());
            jsonUserDetails.add("workTelephoneNumber", userDetails.getWorkTelephoneNumber());
            jsonUserDetails.add("languageId", userDetails.getLanguageIdentifier());
            jsonUserDetails.add("orgId", userDetails.getOrganizationIdentifier());
            jsonUserDetails.add("active", userDetails.isActive());
            jsonUserDetails.add("expiryDate", expiryDate != null ? expiryDate.getMilliseconds() : null);
            jsonUserDetails.add("userProfileDetails", userProfileArray);
            jsonUserDetails.add("workflowProfileDetails", workflowProfileArray);

            // Add all user profile details.
            dtUserProfileDetails = new T8DtUserProfileDetails();
            for (T8UserProfileDetails profileDetails : userDetails.getUserProfileDetails())
            {
                userProfileArray.add(dtUserProfileDetails.serialize(profileDetails));
            }

            // Add all workflow profile details.
            dtWorkflowProfileDetails = new T8DtWorkflowProfileDetails();
            for (T8WorkflowProfileDetails profileDetails : userDetails.getWorkflowProfileDetails())
            {
                workflowProfileArray.add(dtWorkflowProfileDetails.serialize(profileDetails));
            }

            // Return the final JSON object.
            return jsonUserDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8UserDetails userDetails;
            JsonObject jsonUserDetails;
            JsonArray userProfileArray;
            JsonArray workflowProfileArray;
            Long expiryDate;

            // Create the file details object.
            jsonUserDetails = jsonValue.asObject();
            expiryDate = jsonUserDetails.getLong("expiryDate");

            userDetails = new T8UserDetails();
            userDetails.setIdentifier(jsonUserDetails.getString("id"));
            userDetails.setUsername(jsonUserDetails.getString("username"));
            userDetails.setName(jsonUserDetails.getString("name"));
            userDetails.setSurname(jsonUserDetails.getString("surname"));
            userDetails.setEmailAddress(jsonUserDetails.getString("emailAddress"));
            userDetails.setMobileNumber(jsonUserDetails.getString("mobileNumber"));
            userDetails.setWorkTelephoneNumber(jsonUserDetails.getString("workTelephoneNumber"));
            userDetails.setLanguageIdentifier(jsonUserDetails.getString("languageId"));
            userDetails.setOrganizationIdentifier(jsonUserDetails.getString("orgId"));
            userDetails.setActive(jsonUserDetails.getBoolean("active"));
            userDetails.setExpiryDate(expiryDate != null ? new T8Timestamp(expiryDate) : null);

            // Add the user profile details.
            userProfileArray = jsonUserDetails.getJsonArray("userProfileDetails");
            if (userProfileArray != null)
            {
                T8DtUserProfileDetails dtUserProfileDetails;

                dtUserProfileDetails = new T8DtUserProfileDetails();
                for (JsonValue userProfileDetails : userProfileArray)
                {
                    userDetails.addUserProfileDetails(dtUserProfileDetails.deserialize(userProfileDetails));
                }
            }

            // Add the workflow profile details.
            workflowProfileArray = jsonUserDetails.getJsonArray("workflowProfileDetails");
            if (workflowProfileArray != null)
            {
                T8DtWorkflowProfileDetails dtWorkflowProfileDetails;

                dtWorkflowProfileDetails = new T8DtWorkflowProfileDetails();
                for (JsonValue workflowProfileDetails : workflowProfileArray)
                {
                    userDetails.addWorkflowProfileDetails(dtWorkflowProfileDetails.deserialize(workflowProfileDetails));
                }
            }

            // Return the completed object.
            return userDetails;
        }
        else return null;
    }
}