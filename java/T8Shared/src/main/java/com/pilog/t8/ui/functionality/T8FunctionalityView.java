package com.pilog.t8.ui.functionality;

import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityView
{
    public T8FunctionalityAccessHandle getExecutionHandle();
    public void initialize() throws Exception;
    public void startComponent();
    public void stopComponent();
    
    public void addFunctionalityViewListener(T8FunctionalityViewListener listener);
    public void removeFunctionalityViewListener(T8FunctionalityViewListener listener);
}
