package com.pilog.t8.definition.patch;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.patch.T8DefinitionPatch;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PatchDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PATCH";
    public static final String TYPE_IDENTIFIER = "@DT_PATCH";
    public static final String IDENTIFIER_PREFIX = "PATCH_";
    public static final String STORAGE_PATH = "/patches";
    public static final String DISPLAY_NAME = "T8 Patch Definition";
    public static final String DESCRIPTION = "A definition containing settings for updating existing definitions to a new format.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    public enum Datum
    {
        ENABLED,
        TYPE_PATCHES,
        STRING_REPLACEMENTS,
        STRING_MAPS
    };
    // -------- Definition Meta-Data -------- //

    public T8PatchDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ENABLED.toString(), "Enabled", "A flag indicating whether this patch is enabled or not (disabled patches are not applied).", false));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TYPE_PATCHES.toString(), "Definition Type Patches", "The patches applicable to various definition types in the system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STRING_REPLACEMENTS.toString(), "Definition String Patches", "The string patches applicable to all definition types in the system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STRING_MAPS.toString(), "String Maps", "The string replacement maps to apply as part of this patch."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TYPE_PATCHES.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DefinitionTypePatchDefinition.TYPE_IDENTIFIER));
        else if (Datum.STRING_REPLACEMENTS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DefinitionStringPatchDefinition.TYPE_IDENTIFIER));
        else if (Datum.STRING_MAPS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8StringMappingPatchDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.patch.T8PatchDefinitionActionHandler", new Class<?>[]{T8Context.class, T8PatchDefinition.class}, context, this);
    }

    public T8DefinitionPatch getNewPatchInstance() throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.definition.patch.T8DefinitionPatch").getConstructor(T8PatchDefinition.class);
        return (T8DefinitionPatch)constructor.newInstance(this);
    }

    public boolean isEnabled()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ENABLED);
        return value != null && value;
    }

    public void setEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.ENABLED, enabled);
    }

    public List<T8DefinitionTypePatchDefinition> getTypePatches()
    {
        return (List<T8DefinitionTypePatchDefinition>)getDefinitionDatum(Datum.TYPE_PATCHES);
    }

    public void setTypePatches(List<T8DefinitionTypePatchDefinition> definitions)
    {
        setDefinitionDatum(Datum.TYPE_PATCHES, definitions);
    }

    public List<T8DefinitionStringPatchDefinition> getStringReplacements()
    {
        return (List<T8DefinitionStringPatchDefinition>)getDefinitionDatum(Datum.STRING_REPLACEMENTS);
    }

    public void setStringReplacements(List<T8DefinitionStringPatchDefinition> definitions)
    {
        setDefinitionDatum(Datum.STRING_REPLACEMENTS, definitions);
    }

    public void addStringMap(T8StringMappingPatchDefinition definition)
    {
        this.addSubDefinition(Datum.STRING_MAPS.toString(), definition);
    }

    public List<T8StringMappingPatchDefinition> getStringMaps()
    {
        return (List<T8StringMappingPatchDefinition>)getDefinitionDatum(Datum.STRING_MAPS);
    }

    public void setStringMaps(List<T8StringMappingPatchDefinition> definitions)
    {
        setDefinitionDatum(Datum.STRING_MAPS, definitions);
    }
}

