package com.pilog.t8.communication;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationMessageState implements Serializable
{
    private String messageIdentifier;
    private String messageInstanceIdentifier;
    private String communicationIdentifier;
    private String communicationInstanceIdentifier;
    private String serviceIdentifier;
    private String senderIdentifier;
    private String senderInstanceIdentifier;
    private String recipientDisplayName;
    private String recipientAddress;
    private int priority;
    private String subject;
    private String content;
    private int failureCount;
    private long timeQueued;
    private final Map<String, Object> parameters;

    public T8CommunicationMessageState(String messageIdentifier, String messageInstanceIdentifier)
    {
        this.messageIdentifier = messageIdentifier;
        this.messageInstanceIdentifier = messageInstanceIdentifier;
        this.parameters = new HashMap<String, Object>();
        this.timeQueued = -1;
    }

    public String getServiceIdentifier()
    {
        return serviceIdentifier;
    }

    public void setServiceIdentifier(String serviceIdentifier)
    {
        this.serviceIdentifier = serviceIdentifier;
    }

    public String getMessageInstanceIdentifier()
    {
        return messageInstanceIdentifier;
    }

    public void setMessageInstanceIdentifier(String messageInstanceIdentifier)
    {
        this.messageInstanceIdentifier = messageInstanceIdentifier;
    }

    public String getMessageIdentifier()
    {
        return messageIdentifier;
    }

    public void setMessageIdentifier(String messageIdentifier)
    {
        this.messageIdentifier = messageIdentifier;
    }

    public String getCommunicationIdentifier()
    {
        return communicationIdentifier;
    }

    public void setCommunicationIdentifier(String communicationIdentifier)
    {
        this.communicationIdentifier = communicationIdentifier;
    }

    public String getCommunicationInstanceIdentifier()
    {
        return communicationInstanceIdentifier;
    }

    public void setCommunicationInstanceIdentifier(String communicationInstanceIdentifier)
    {
        this.communicationInstanceIdentifier = communicationInstanceIdentifier;
    }

    public String getSenderIdentifier()
    {
        return senderIdentifier;
    }

    public void setSenderIdentifier(String senderIdentifier)
    {
        this.senderIdentifier = senderIdentifier;
    }

    public String getSenderInstanceIdentifier()
    {
        return senderInstanceIdentifier;
    }

    public void setSenderInstanceIdentifier(String senderInstanceIdentifier)
    {
        this.senderInstanceIdentifier = senderInstanceIdentifier;
    }

    public String getRecipientDisplayName()
    {
        return recipientDisplayName;
    }

    public void setRecipientDisplayName(String recipientDisplayName)
    {
        this.recipientDisplayName = recipientDisplayName;
    }

    public String getRecipientAddress()
    {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress)
    {
        this.recipientAddress = recipientAddress;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public int getFailureCount()
    {
        return failureCount;
    }

    public void setFailureCount(int failureCount)
    {
        this.failureCount = failureCount;
    }

    public long getTimeQueued()
    {
        return timeQueued;
    }

    public void setTimeQueued(long timeQueued)
    {
        this.timeQueued = timeQueued;
    }

    public Map<String, Object> getParameters()
    {
        return new HashMap<String, Object>(parameters);
    }

    public void setParameters(Map<String, Object> parameters)
    {
        this.parameters.clear();
        if (parameters != null)
        {
            this.parameters.putAll(parameters);
        }
    }

    public void addParameter(String parameterIdentifier, Object parameterValue)
    {
        parameters.put(parameterIdentifier, parameterValue);
    }
}
