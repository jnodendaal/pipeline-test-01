package com.pilog.t8.definition.user.password;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public final class T8PasswordFormatRuleDefinition extends T8PasswordPolicyRuleDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_PASSWORD_FORMAT_RULE";
    public static final String DISPLAY_NAME = "Password Format Rules";
    public static final String DESCRIPTION = "A rule to be applied when validating password formats.";

    public enum Datum {REGULAR_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8PasswordFormatRuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REGULAR_EXPRESSION.toString(), "Regular Expression", "The regular expression that will be used to validate the user password against."));

        return datumTypes;
    }

    @Override
    public T8PasswordRuleValidator getValidator()
    {
        return T8Reflections.getInstance("com.pilog.t8.security.rulevalidator.T8PasswordFormatRuleValidator", new Class<?>[]{T8PasswordFormatRuleDefinition.class}, this);
    }

    public void setRegularExpression(String text)
    {
        setDefinitionDatum(Datum.REGULAR_EXPRESSION, text);
    }

    public String getRegularExpression()
    {
        return getDefinitionDatum(Datum.REGULAR_EXPRESSION);
    }
}
