package com.pilog.t8.definition.http.api.rest;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.http.api.rest.T8UriParser;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public abstract class T8UriParserDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_URI_PARSER";
    public static final String GROUP_NAME = "URL Parser";
    public static final String GROUP_DESCRIPTION = "Parsers used to interpret segments of a URI.";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8UriParserDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8UriParser getUriParser(T8Context context);
}
