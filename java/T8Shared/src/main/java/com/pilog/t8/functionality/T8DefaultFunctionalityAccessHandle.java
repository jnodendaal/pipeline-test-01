package com.pilog.t8.functionality;

import com.pilog.t8.ui.functionality.event.T8FunctionalityExecutionEndedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityExecutionListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityExecutionUpdatedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import javax.swing.Icon;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    protected final String functionalityId;
    protected final String functionalityIid;
    protected String displayName;
    protected String description;
    protected Icon icon;
    private T8FunctionalityAccessType accessType;
    private String accessMessage;
    protected transient EventListenerList handleListeners;
    protected transient T8FunctionalityViewListener viewListener;

    public T8DefaultFunctionalityAccessHandle(String functionalityId, String functionalityIid, String displayName, String description, Icon icon)
    {
        this.functionalityId = functionalityId;
        this.functionalityIid = functionalityIid;
        this.displayName = displayName;
        this.description = description;
        this.icon = icon;
        this.handleListeners = new EventListenerList();
        this.viewListener = new FunctionalityViewListener();
    }

    /**
     * Returns the type of access granted to the requester.
     * @return The type of access granted tot he requester.
     */
    @Override
    public T8FunctionalityAccessType getAccessType()
    {
        return T8FunctionalityAccessType.ACCESS;
    }

    /**
     * Returns the access message (if any) applicable to the access type granted to the requester.
     * @return The access message (if any) applicable to the access type granted to the requester.
     */
    @Override
    public String getAccessMessage()
    {
        // The default access message is null as no message is required.
        return null;
    }

    @Override
    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public Icon getIcon()
    {
        return icon;
    }

    @Override
    public String getFunctionalityId()
    {
        return functionalityId;
    }

    @Override
    public String getFunctionalityIid()
    {
        return functionalityIid;
    }

    @Override
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();
        buffer.append(getClass().getSimpleName());
        buffer.append("[functionality:");
        buffer.append(getFunctionalityId());
        buffer.append(",instance:");
        buffer.append(functionalityIid);
        buffer.append("]");
        return buffer.toString();
    }

    @Override
    public void addFunctionalityExecutionListener(T8FunctionalityExecutionListener listener)
    {
        if (handleListeners == null) throw new RuntimeException("Cannot add listener to handle in this context.");
        handleListeners.add(T8FunctionalityExecutionListener.class, listener);
    }

    @Override
    public void removeFunctionalityExecutionListener(T8FunctionalityExecutionListener listener)
    {
        if (handleListeners == null) throw new RuntimeException("Cannot add listener to handle in this context.");
        handleListeners.remove(T8FunctionalityExecutionListener.class, listener);
    }

    private void fireExecutionUpdatedEvent()
    {
        if (handleListeners != null)
        {
            T8FunctionalityExecutionUpdatedEvent event;

            event = new T8FunctionalityExecutionUpdatedEvent(this);
            for (T8FunctionalityExecutionListener listener : handleListeners.getListeners(T8FunctionalityExecutionListener.class))
            {
                listener.functionalityExecutionUpdated(event);
            }
        }
    }

    private void fireExecutionEndedEvent()
    {
        if (handleListeners != null)
        {
            T8FunctionalityExecutionEndedEvent event;

            event = new T8FunctionalityExecutionEndedEvent(this);
            for (T8FunctionalityExecutionListener listener : handleListeners.getListeners(T8FunctionalityExecutionListener.class))
            {
                listener.functionalityExecutionEnded(event);
            }
        }
    }

    /**
     * This method is called by functionality views created by this handle when
     * functionality execution ends.
     */
    public void functionalityExecutionEnded()
    {
       fireExecutionEndedEvent();
    }

    /**
     * This method is called by functionality views created by this handle when
     * the functionality execution status has to be updated.
     */
    public void functionalityExecutionUpdated()
    {
       fireExecutionUpdatedEvent();
    }

    private class FunctionalityViewListener implements T8FunctionalityViewListener
    {
        @Override
        public void functionalityViewUpdated(T8FunctionalityViewUpdatedEvent event)
        {
            fireExecutionUpdatedEvent();
        }

        @Override
        public void functionalityViewClosed(T8FunctionalityViewClosedEvent event)
        {
            fireExecutionEndedEvent();
        }
    }
}
