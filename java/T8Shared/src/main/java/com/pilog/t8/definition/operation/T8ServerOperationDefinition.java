package com.pilog.t8.definition.operation;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ServerOperationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SYSTEM_SERVER_OPERATION";
    public static final String GROUP_NAME = "Server Operations";
    public static final String GROUP_DESCRIPTION = "Server-side operations that make up the API accessed by all client-side applications when interacting with the system.";
    public static final String STORAGE_PATH = "/server_operations";
    public static final String DISPLAY_NAME = "Server Operation";
    public static final String DESCRIPTION = "A T8 server-side operation.";
    public static final String IDENTIFIER_PREFIX = "OP_";
    public enum Datum
    {
        OPERATION_DISPLAY_NAME,
        OPERATION_DESCRIPTION,
        CONTAINER_MANAGED_TRANSACTION,
        INPUT_PARAMETER_DEFINITIONS,
        OUTPUT_PARAMETER_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8ServerOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.OPERATION_DISPLAY_NAME.toString(), "Display Name", "The name of this type of operation that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.OPERATION_DESCRIPTION.toString(), "Description", "The description of this type of operation that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CONTAINER_MANAGED_TRANSACTION.toString(), "Container Managed Transaction", "A flag to indicate whether or not this operation will execute within a data transaction managed externally.", true));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this operation."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters", "The output data parameters returned by this operation after execution."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8OperationTaskTestHarness", new Class<?>[]{});
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionDocumentationProvider createDocumentationProvider(T8DefinitionContext definitionContext)
    {
        return new T8ServerOperationDocumentationProvider(definitionContext);
    }

    public abstract T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid);

    public boolean isContainerManagedTransaction()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.CONTAINER_MANAGED_TRANSACTION);
        return value == null || value;
    }

    public void setContainerManagedTransaction(boolean containerManagedTransaction)
    {
        setDefinitionDatum(Datum.CONTAINER_MANAGED_TRANSACTION, containerManagedTransaction);
    }

    public String getOperationDisplayName()
    {
        return getDefinitionDatum(Datum.OPERATION_DISPLAY_NAME);
    }

    public void setOperationDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.OPERATION_DISPLAY_NAME, displayName);
    }

    public String getOperationDescription()
    {
        return getDefinitionDatum(Datum.OPERATION_DESCRIPTION);
    }

    public void setOperationDescription(String description)
    {
        setDefinitionDatum(Datum.OPERATION_DESCRIPTION, description);
    }

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public void addInputParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public void addOutputParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }
}
