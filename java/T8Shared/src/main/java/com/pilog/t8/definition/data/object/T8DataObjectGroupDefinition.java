package com.pilog.t8.definition.data.object;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectGroupHandler;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataObjectGroupDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT_GROUP";
    public static final String GROUP_NAME = "Data Object Groups";
    public static final String GROUP_DESCRIPTION = "Definitions of the various objects that are maintained by the system as content data.  Objects are abstractions of underlying data entities and server as handles to the data while being process in the system.";
    public static final String STORAGE_PATH = "/data_objects";
    public static final String IDENTIFIER_PREFIX = "OG_";
    public enum Datum
    {
        KEY_FIELD_ID
    };
    // -------- Definition Meta-Data -------- //

    public T8DataObjectGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DataObjectGroupHandler getDataObjectGroupHandler(T8DataTransaction tx);
}
