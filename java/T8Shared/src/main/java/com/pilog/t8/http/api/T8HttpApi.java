package com.pilog.t8.http.api;

import com.pilog.json.JsonObject;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8HttpApi
{
    /**
     * Returns the name of the API, as it appears in URIs.
     * @return the name of the API, as it appears in URIs.
     */
    public String getName();

    /**
     * Processes an HTTP request to the API and returns the result of the operation.
     * @param context The context of the request, as authenticated by the security manager.
     * @param method Returns the name of the HTTP method with which this request was
     * made, for example, GET, POST, or PUT. Same as the value of the CGI variable REQUEST_METHOD.
     * @param pathString The URI to be resolved by the API.
     * @param parameters The request parameters.
     * @param input The JSON context received as input with the request.
     * @return The result of the request, containing output content and status code.
     */
    public T8HttpApiResult handleRequest(T8Context context, String method, String pathString, Map<String, List<String>> parameters, JsonObject input);
}
