package com.pilog.t8.time;

/**
 * @author Bouwer du Preez
 */
public class T8Second implements T8TimeUnit
{
    private final long amount;

    public T8Second(long amount)
    {
        this.amount = amount;
    }

    @Override
    public long getMilliseconds()
    {
        return 1000 * amount;
    }

    public long getAmount()
    {
        return amount;
    }
}
