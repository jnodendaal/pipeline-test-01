package com.pilog.t8.definition.functionality.access;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.functionality.access.T8FunctionalityAccessBlocker;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FunctionalityAccessBlockerDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FUNCTIONALITY_ACCESS_BLOCKER";
    public static final String DISPLAY_NAME = "System Functionality Access Blocker";
    public static final String DESCRIPTION = "A functionality access blocker.";
    public static final String IDENTIFIER_PREFIX = "FUNC_BLOCK_";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8FunctionalityAccessBlockerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8FunctionalityAccessBlocker createNewAccessBlockerInstance(T8Context context) throws Exception;
}
