/**
 * Created on 15 Jul 2016, 8:26:29 AM
 */
package com.pilog.t8.definition.org.operational;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Gavin Boshoff
 */
public class T8OrganizationOperationalIntervalDefinition extends T8Definition implements Comparable<T8OrganizationOperationalIntervalDefinition>
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_ORG_OPERATIONAL_INTERVAL";
    public static final String GROUP_IDENTIFIER = "@DG_ORG_OPERATIONAL_INTERVAL";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Operational Interval";
    public static final String DESCRIPTION = "A definition of a single operational day, including its start and end time.";
    public enum Datum { OPERATING_DAY,
                        OPERATING_HOURS_START,
                        OPERATING_MINUTES_START,
                        OPERATING_HOURS_END,
                        OPERATING_MINUTES_END,
                        /** Mainly used during runtime for filler intervals. */
                        ACTIVE};
    // -------- Definition Meta-Data -------- //

    public T8OrganizationOperationalIntervalDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8Definition copy()
    {
        T8OrganizationOperationalIntervalDefinition operationalIntervalCopy;

        operationalIntervalCopy = new T8OrganizationOperationalIntervalDefinition(getIdentifier());
        operationalIntervalCopy.setOperatingDay(getOperatingDay());
        operationalIntervalCopy.setOperatingHoursStart(getOperatingHoursStart());
        operationalIntervalCopy.setOperatingMinutesStart(getOperatingMinutesStart());
        operationalIntervalCopy.setOperatingHoursEnd(getOperatingHoursEnd());
        operationalIntervalCopy.setOperatingMinutesEnd(getOperatingMinutesEnd());
        operationalIntervalCopy.setActive(isActive());

        return operationalIntervalCopy;
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATING_DAY.toString(), "Operating Day", "The day of the week considered to be a working day.", T8OrganizationOperationalSettingsDefinition.OperatingDay.MONDAY.toString(), T8DefinitionDatumType.T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.OPERATING_HOURS_START.toString(), "Operating Start Hour (24 Hour Format)", "The hour of day at which the operational day starts.", 8));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.OPERATING_MINUTES_START.toString(), "Operating Start Minute", "The minutes during the start hour at which the operational day starts.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.OPERATING_HOURS_END.toString(), "Operating End Hour (24 Hour Format)", "The hour of day at which the operational day ends.", 17));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.OPERATING_MINUTES_END.toString(), "Operating End Minute", "The minutes during the end hour at which the operational day ends.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "An active indicator used for indicating whether or not the interval is taken into consideration.", true).setHidden(true));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        switch (Datum.valueOf(datumIdentifier))
        {
            case OPERATING_DAY:
                return createStringOptions(T8OrganizationOperationalSettingsDefinition.OperatingDay.values());
            case OPERATING_HOURS_START:
            case OPERATING_HOURS_END:
                return createStringOptions(IntStream.range(0, 24).mapToObj(String::valueOf).collect(Collectors.toCollection(ArrayList::new)));
            case OPERATING_MINUTES_START:
            case OPERATING_MINUTES_END:
                return createStringOptions(IntStream.range(0, 60).mapToObj(String::valueOf).collect(Collectors.toCollection(ArrayList::new)));
            default:
                return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception {}

    @Override
    public int compareTo(T8OrganizationOperationalIntervalDefinition otherInterval)
    {
        T8OrganizationOperationalSettingsDefinition.OperatingDay otherOperatingDay;
        T8OrganizationOperationalSettingsDefinition.OperatingDay thisOperatingDay;

        otherOperatingDay = otherInterval.getOperatingDay();
        thisOperatingDay = this.getOperatingDay();

        if (thisOperatingDay == null && otherOperatingDay == null) return 0;
        if (thisOperatingDay == null) return 1;
        if (otherOperatingDay == null) return -1;

        if (thisOperatingDay.ordinal() < otherOperatingDay.ordinal()) return -1;
        if (thisOperatingDay.ordinal() > otherOperatingDay.ordinal()) return 1;

        if (this.getOperatingHoursStart() < otherInterval.getOperatingHoursStart()) return -1;
        if (this.getOperatingHoursStart() > otherInterval.getOperatingHoursStart()) return 1;

        if (this.getOperatingMinutesStart() < otherInterval.getOperatingMinutesStart()) return -1;
        if (this.getOperatingMinutesStart() > otherInterval.getOperatingMinutesStart()) return 1;

        return 0;
    }

    public String getOperatingDayString()
    {
        return getDefinitionDatum(Datum.OPERATING_DAY);
    }

    public T8OrganizationOperationalSettingsDefinition.OperatingDay getOperatingDay()
    {
        return T8OrganizationOperationalSettingsDefinition.OperatingDay.valueOf(getOperatingDayString());
    }

    public void setOperatingDay(String operatingDay)
    {
        setDefinitionDatum(Datum.OPERATING_DAY, operatingDay != null ? operatingDay.toUpperCase() : null);
    }

    public void setOperatingDay(T8OrganizationOperationalSettingsDefinition.OperatingDay operatingDay)
    {
        setOperatingDay(operatingDay != null ? operatingDay.toString() : (String)null); // String cast is required, otherwise ambiguous method call
    }

    public int getOperatingHoursStart()
    {
        return getDefinitionDatum(Datum.OPERATING_HOURS_START);
    }

    public void setOperatingHoursStart(int operatingHoursStart)
    {
        setDefinitionDatum(Datum.OPERATING_HOURS_START, operatingHoursStart);
    }

    public int getOperatingHoursEnd()
    {
        return getDefinitionDatum(Datum.OPERATING_HOURS_END);
    }

    public void setOperatingHoursEnd(int operatingHoursEnd)
    {
        setDefinitionDatum(Datum.OPERATING_HOURS_END, operatingHoursEnd);
    }

    public int getOperatingMinutesStart()
    {
        return getDefinitionDatum(Datum.OPERATING_MINUTES_START);
    }

    public void setOperatingMinutesStart(int operatingMinutesStart)
    {
        setDefinitionDatum(Datum.OPERATING_MINUTES_START, operatingMinutesStart);
    }

    public int getOperatingMinutesEnd()
    {
        return getDefinitionDatum(Datum.OPERATING_MINUTES_END);
    }

    public void setOperatingMinutesEnd(int operatingMinutesEnd)
    {
        setDefinitionDatum(Datum.OPERATING_MINUTES_END, operatingMinutesEnd);
    }

    public boolean isActive()
    {
        Boolean activeInd;

        activeInd = getDefinitionDatum(Datum.ACTIVE);
        return (activeInd == null || activeInd); // null is considered true value
    }

    public void setActive(boolean activeInd)
    {
        setDefinitionDatum(Datum.ACTIVE, activeInd);
    }
}