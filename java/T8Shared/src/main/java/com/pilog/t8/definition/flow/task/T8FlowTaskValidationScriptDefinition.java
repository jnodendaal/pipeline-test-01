package com.pilog.t8.definition.flow.task;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskValidationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_TASK_VALIDATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FLOW_TASK_VALIDATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Task Validation Script";
    public static final String DESCRIPTION = "A script that is executed in order to evaluate whether or not all requirements for a task's completion have been met.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_VALID = "$P_VALID";

    public T8FlowTaskValidationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8FlowTaskDefinition taskDefinition;

        taskDefinition = (T8FlowTaskDefinition)getParentDefinition();
        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.addAll(taskDefinition.getInputParameterDefinitions());
        definitions.addAll(taskDefinition.getOutputParameterDefinitions());
        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8FlowTaskDefinition taskDefinition;

        taskDefinition = (T8FlowTaskDefinition)getParentDefinition();
        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.addAll(taskDefinition.getOutputParameterDefinitions());
        definitions.addAll(taskDefinition.getValidationOutputParameterDefinitions());
        definitions.add(new T8DataParameterDefinition(PARAMETER_VALID, "Valid Flag", "The boolean result of the evaluation in this script, indicating a valid task completion if true.", T8DataType.BOOLEAN));
        return definitions;
    }
}
