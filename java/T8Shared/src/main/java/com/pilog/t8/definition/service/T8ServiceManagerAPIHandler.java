package com.pilog.t8.definition.service;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ServiceManagerAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_EXECUTE_SERVICE_OPERATION = "@OS_EXECUTE_SERVICE_OPERATION";

    public static final String PARAMETER_SERVICE_IDENTIFIER = "$P_SERVICE_IDENTIFIER";
    public static final String PARAMETER_OPERATION_IDENTIFIER = "$P_OPERATION_IDENTIFIER";
    public static final String PARAMETER_INPUT_PARAMETER_MAP = "$P_INPUT_PARAMETER_MAP";
    public static final String PARAMETER_OUTPUT_PARAMETER_MAP = "$P_OUTPUT_PARAMETER_MAP";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();

            definition = new T8JavaServerOperationDefinition(OPERATION_EXECUTE_SERVICE_OPERATION);
            definition.setMetaDescription("Execute Service Operation");
            definition.setClassName("com.pilog.t8.service.T8ServiceManagerOperations$ExecuteServiceOperationOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SERVICE_IDENTIFIER, "Service Identifier", "The identifier of the service on which the operation will be invoked.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OPERATION_IDENTIFIER, "Operation Identifier", "The identifier of the service operation that will be invoked.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETER_MAP, "Input Parameters", "A map containing the input parameters that will be passed to the operation.", T8DataType.MAP));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OUTPUT_PARAMETER_MAP, "Output Parameters", "A map containing the output parameters returned by the operation after execution.", T8DataType.MAP));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
