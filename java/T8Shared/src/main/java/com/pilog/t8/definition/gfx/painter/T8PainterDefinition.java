package com.pilog.t8.definition.gfx.painter;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Painter;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8PainterDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_GFX_PAINTER";
    public static final String GROUP_NAME = "Painters";
    public static final String GROUP_DESCRIPTION = "Configurations used to describe how parts of the user interfaces in the system must be painted or renderer.  Typically used for backgrounds and borders of UI components.";
    public static final String IDENTIFIER_PREFIX = "GFX_PAINTER_";
    public static final String STORAGE_PATH = "/painters";
    // -------- Definition Meta-Data -------- //

    public T8PainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return new ArrayList<T8DefinitionDatumType>();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public abstract T8Painter getNewPainterInstance();
}
