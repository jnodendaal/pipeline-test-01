package com.pilog.t8.ui.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8ComponentEventListener extends EventListener
{
    public void componentEvent(T8ComponentEvent event);
}
