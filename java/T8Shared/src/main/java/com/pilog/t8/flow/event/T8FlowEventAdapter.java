/**
 * Created on 12 Jun 2015, 12:58:45 PM
 */
package com.pilog.t8.flow.event;

/**
 * @author Gavin Boshoff
 */
@SuppressWarnings("NoopMethodInAbstractClass")
public abstract class T8FlowEventAdapter implements T8FlowEventListener
{
    @Override
    public void flowCompleted(T8FlowCompletedEvent event) {}

    @Override
    public void flowStopped(T8FlowStoppedEvent event) {}

    @Override
    public void flowFinalized(T8FlowFinalizedEvent event) {}

    @Override
    public void taskClaimed(T8FlowTaskClaimedEvent event) {}

    @Override
    public void taskUnclaimed(T8FlowTaskUnclaimedEvent event) {}

    @Override
    public void taskCompleted(T8FlowTaskCompletedEvent event) {}

    @Override
    public void signalReceived(T8FlowSignalReceivedEvent event) {}
}