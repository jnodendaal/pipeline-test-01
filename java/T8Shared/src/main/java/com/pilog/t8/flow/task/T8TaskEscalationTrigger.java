package com.pilog.t8.flow.task;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8TaskEscalationTrigger implements Serializable
{
    private String triggerIid;
    private String flowId;
    private String taskId;
    private String groupId;
    private TriggerEvent event;
    private Integer timeElapsed; // Minutes.
    private Integer priority;
    private Integer priorityIncrease;
    private boolean recurring;
    private String escalationId;
    private Integer sequence;

    public enum TriggerEvent{GROUP_DURATION, ISSUED, ISSUED_OR_CLAIMED, CLAIMED};

    public T8TaskEscalationTrigger(String triggerIid)
    {
        this.triggerIid = triggerIid;
    }

    public String getTriggerIid()
    {
        return triggerIid;
    }

    public void setTriggerIid(String triggerIid)
    {
        this.triggerIid = triggerIid;
    }

    public String getFlowId()
    {
        return flowId;
    }

    public void setFlowId(String flowId)
    {
        this.flowId = flowId;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public void setTaskId(String taskId)
    {
        this.taskId = taskId;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public TriggerEvent getEvent()
    {
        return event;
    }

    public void setEvent(TriggerEvent event)
    {
        this.event = event;
    }

    public Integer getTimeElapsed()
    {
        return timeElapsed;
    }

    public void setTimeElapsed(Integer timeElapsed)
    {
        this.timeElapsed = timeElapsed;
    }

    public Integer getPriority()
    {
        return priority;
    }

    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }

    public Integer getPriorityIncrease()
    {
        return priorityIncrease;
    }

    public void setPriorityIncrease(Integer priorityIncrease)
    {
        this.priorityIncrease = priorityIncrease;
    }

    public boolean isRecurring()
    {
        return recurring;
    }

    public void setRecurring(boolean recurring)
    {
        this.recurring = recurring;
    }

    public String getEscalationId()
    {
        return escalationId;
    }

    public void setEscalationId(String escalationId)
    {
        this.escalationId = escalationId;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    @Override
    public String toString()
    {
        return "T8TaskEscalationTrigger{" + "triggerIid=" + triggerIid + ", flowId=" + flowId + ", taskId=" + taskId + ", event=" + event + ", timeElapsed=" + timeElapsed + ", priority=" + priority + ", priorityIncrease=" + priorityIncrease + ", recurring=" + recurring + ", escalationId=" + escalationId + '}';
    }
}
