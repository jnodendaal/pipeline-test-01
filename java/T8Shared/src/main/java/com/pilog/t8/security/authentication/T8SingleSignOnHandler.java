package com.pilog.t8.security.authentication;

/**
 * @author Bouwer du Preez
 */
public interface T8SingleSignOnHandler
{
    /**
     * Signs the current user onto the system using the configured SSO authentication setup.
     * @return A Boolean result indicating successful sign-on if true.
     */
    public boolean signOn();
}
