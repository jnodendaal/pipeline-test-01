package com.pilog.t8.flow;

import com.pilog.t8.flow.event.T8FlowSignalReceivedEvent;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowSignalStartEventNode
{
    public void setStartEvent(T8FlowSignalReceivedEvent event);
    public void setAttachedToNodeInstanceIdentifier(String nodeInstanceIdentifier);
}
