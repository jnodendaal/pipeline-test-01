package com.pilog.t8.definition.script;

import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8ScriptDefinition
{
    public String getIdentifier();
    public String getNamespace();
    public String getScript();
    public boolean containsScript();
    public String getRootProjectId();
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception;
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception;
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception;
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception;
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context);
}
