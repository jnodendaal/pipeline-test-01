package com.pilog.t8.functionality.access;

import com.pilog.t8.security.T8DataAccess;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityAccessRights implements Serializable
{
    private String functionalityIid;
    private String functionalityId;
    private String dataObjectIid;
    private String dataObjectId;
    private String projectId;
    private T8DataAccess dataAccess;
    private final List<String> operationIds;
    private final List<T8FunctionalityDataObjectAccessRights> objectAccessRights;

    public T8FunctionalityAccessRights()
    {
        this.operationIds = new ArrayList<>();
        this.objectAccessRights = new ArrayList<>();
    }

    public String getFunctionalityIid()
    {
        return functionalityIid;
    }

    public void setFunctionalityIid(String functionalityIid)
    {
        this.functionalityIid = functionalityIid;
    }

    public String getFunctionalityId()
    {
        return functionalityId;
    }

    public void setFunctionalityId(String functionalityId)
    {
        this.functionalityId = functionalityId;
    }

    public String getDataObjectIid()
    {
        return dataObjectIid;
    }

    public void setDataObjectIid(String dataObjectIid)
    {
        this.dataObjectIid = dataObjectIid;
    }

    public String getDataObjectId()
    {
        return dataObjectId;
    }

    public void setDataObjectId(String dataObjectId)
    {
        this.dataObjectId = dataObjectId;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public T8DataAccess getDataAccess()
    {
        return dataAccess;
    }

    public void setDataAccess(T8DataAccess dataAccess)
    {
        this.dataAccess = dataAccess;
    }

    public List<String> getOperationIds()
    {
        return new ArrayList<>(operationIds);
    }

    public void setOperationIds(Collection<String> operationIds)
    {
        this.operationIds.clear();
        if (operationIds != null)
        {
            this.operationIds.addAll(operationIds);
        }
    }

    public List<T8FunctionalityDataObjectAccessRights> getObjectAccessRights()
    {
        return new ArrayList<>(this.objectAccessRights);
    }

    public void setObjectAccessRights(List<T8FunctionalityDataObjectAccessRights> accessRights)
    {
        this.objectAccessRights.clear();
        if (accessRights != null)
        {
            this.objectAccessRights.addAll(accessRights);
        }
    }

    public void addObjectAccessRights(T8FunctionalityDataObjectAccessRights rights)
    {
        this.objectAccessRights.add(rights);
    }
}
