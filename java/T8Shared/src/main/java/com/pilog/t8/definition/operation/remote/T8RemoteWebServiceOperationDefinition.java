package com.pilog.t8.definition.operation.remote;

import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8RemoteWebServiceOperationDefinition extends T8ServerOperationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REMOTE_OPERATION_T8_WEB_SERVICE_REST";
    public static final String DISPLAY_NAME = "Remote T8 Web Service Operation";
    public static final String DESCRIPTION = "An remote T8 Web Service Operation that is executed from within the local T8 Server context.";
    public enum Datum {REMOTE_SERVER_CONNECTION_DEFINITION_IDENTIFIER,
                       REMOTE_OPERATION_IDENTIFIER,
                       REMOTE_OPERATION_INPUT_PARAMETER_MAPPING,
                       REMOTE_OPERATION_OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8RemoteWebServiceOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REMOTE_SERVER_CONNECTION_DEFINITION_IDENTIFIER.toString(), "Remote Server Connection Details", "The definition identifier of the remote server connection details that will be used to make the connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.IDENTIFIER_STRING, Datum.REMOTE_OPERATION_IDENTIFIER.toString(), "Remote Operation Identifier", "The Operation that will be called on the remote Tech 8 Instance. This is the Server Operation Definition Identifier on the remote system."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.DEFINITION_IDENTIFIER), Datum.REMOTE_OPERATION_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping: Operation Input to Remote Input", "The Input Parameter Mapping that maps the parameters between this operation and the remote operation."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.DEFINITION_IDENTIFIER, T8DataType.STRING), Datum.REMOTE_OPERATION_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping: Remote Output to Ouput", "The Output Parameter Mapping that maps the ouput parameters of the remote operation to the output parameters of this operation."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REMOTE_SERVER_CONNECTION_DEFINITION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ConnectionDefinition.GROUP_IDENTIFIER));
        } else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid)
    {
        return T8Reflections.getInstance("com.pilog.t8.operation.remote.t8.T8RemoteWebServiceOperation", new Class<?>[]{T8Context.class, T8RemoteWebServiceOperationDefinition.class, String.class}, context, this, operationIid);
    }

    public String getRemoteServerConnectionDefinitionIdentifier()
    {
        return getDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_DEFINITION_IDENTIFIER);
    }

    public void setRemoteServerConnectionDefinitionIdentifier(String remoteServerConnectionDefinitionIdentifier)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_DEFINITION_IDENTIFIER, remoteServerConnectionDefinitionIdentifier);
    }

    public String getRemoteOperationIdentifier()
    {
        return getDefinitionDatum(Datum.REMOTE_OPERATION_IDENTIFIER);
    }

    public void setRemoteOperationIdentifier(String remoteOperationInstanceIdentifier)
    {
        setDefinitionDatum(Datum.REMOTE_OPERATION_IDENTIFIER, remoteOperationInstanceIdentifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return getDefinitionDatum(Datum.REMOTE_OPERATION_INPUT_PARAMETER_MAPPING);
    }

    public void setInputParameterMapping(Map<String, String> inputParameterMapping)
    {
        setDefinitionDatum(Datum.REMOTE_OPERATION_INPUT_PARAMETER_MAPPING, inputParameterMapping);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return getDefinitionDatum(Datum.REMOTE_OPERATION_OUTPUT_PARAMETER_MAPPING);
    }

    public void setOutputParameterMapping(Map<String, String> outputParameterMapping)
    {
        setDefinitionDatum(Datum.REMOTE_OPERATION_OUTPUT_PARAMETER_MAPPING, outputParameterMapping);
    }
}
