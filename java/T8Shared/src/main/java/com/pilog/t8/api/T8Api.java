package com.pilog.t8.api;

/**
 * @author Bouwer du Preez
 */
public interface T8Api
{
    // No methods yet - just a marker interface at this stage.
}
