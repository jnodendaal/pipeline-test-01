package com.pilog.t8.definition;

import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.utilities.codecs.HexCodec;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeSet;

/**t
 * This class handles a few operations that are not possible from elsewhere in
 * the framework since some of the methods involved are package protected due to
 * security and consistency requirements.
 *
 * @author Bouwer du Preez
 */
public class T8DefinitionUtilities
{
    public static final String CHECKSUM_REPLACEMENT_STRING = "-####T8_CHECKSUM####-";

    public static String computeChecksumHex(T8Definition definition) throws IOException, NoSuchAlgorithmException, Exception
    {
        byte[] checksumBytes;

        checksumBytes = computeChecksum(definition);
        return HexCodec.convertBytesToHexString(checksumBytes);
    }

    public static byte[] computeChecksum(T8Definition definition) throws IOException, NoSuchAlgorithmException, Exception
    {
        T8DefinitionSerializer serializer;
        MessageDigest messageDigest;
        String definitionString;
        String existingChecksum;

        // Record the definition's existing checksum and then set it to null because it must not be included when the new checksum is computed.
        existingChecksum = definition.getChecksum();
        definition.setChecksum(CHECKSUM_REPLACEMENT_STRING);

        // Serialize the definition.
        serializer = new T8DefinitionSerializer();
        definitionString = serializer.serializeDefinition(definition);

        // Compute the new checksum.
        messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(definitionString.getBytes("UTF-16"));

        // Reset the definitions existing checksum.
        definition.setChecksum(existingChecksum);

        // Return the checksum bytes.
        return messageDigest.digest();
    }

    public static String computeChecksumHex(String serializedDefinition) throws IOException, NoSuchAlgorithmException, Exception
    {
        byte[] checksumBytes;

        checksumBytes = computeChecksum(serializedDefinition);
        return HexCodec.convertBytesToHexString(checksumBytes);
    }

    public static byte[] computeChecksum(String serializedDefinition) throws IOException, NoSuchAlgorithmException, Exception
    {
        MessageDigest messageDigest;

        // Compute the new checksum.
        messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(serializedDefinition.getBytes("UTF-16"));

        // Return the checksum bytes.
        return messageDigest.digest();
    }

    public static final T8DefinitionTypeMetaData getDefinitionTypeMetaData(T8Definition definition)
    {
        return getDefinitionTypeMetaData(definition.getClass());
    }

    public static final T8DefinitionTypeMetaData getDefinitionTypeMetaData(Class definitionClass)
    {
        try
        {
            T8DefinitionTypeMetaData metaData;

            metaData = new T8DefinitionTypeMetaData();
            metaData.setTypeId((String)definitionClass.getField("TYPE_IDENTIFIER").get(null));
            metaData.setGroupId((String)definitionClass.getField("GROUP_IDENTIFIER").get(null));
            metaData.setDisplayName((String)definitionClass.getField("DISPLAY_NAME").get(null));
            metaData.setDescription((String)definitionClass.getField("DESCRIPTION").get(null));
            metaData.setStoragePath((String)getOptionalFieldValue(definitionClass, "STORAGE_PATH"));
            metaData.setIdPrefix((String)getOptionalFieldValue(definitionClass, "IDENTIFIER_PREFIX"));
            metaData.setDefinitionLevel(getDefinitionLevel(definitionClass));
            metaData.setClassName(definitionClass.getName());

            return metaData;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Invalid type meta data found in definition class: " + definitionClass, e);
        }
    }

    public static final T8DefinitionGroupMetaData getDefinitionGroupMetaData(Class definitionClass)
    {

        try
        {
            String groupId;

            groupId = (String)getOptionalFieldValue(definitionClass, "GROUP_IDENTIFIER");
            if (groupId != null)
            {
                T8DefinitionGroupMetaData metaData;

                metaData = new T8DefinitionGroupMetaData();
                metaData.setGroupId(groupId);
                metaData.setName((String)getOptionalFieldValue(definitionClass, "GROUP_NAME"));
                metaData.setDescription((String)getOptionalFieldValue(definitionClass, "GROUP_DESCRIPTION"));
                return metaData;
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Invalid type meta data found in definition class: " + definitionClass, e);
        }
    }

    public static final T8DefinitionLevel getDefinitionLevel(T8Definition definition)
    {
        return getDefinitionLevel(definition.getClass());
    }

    public static final T8DefinitionLevel getDefinitionLevel(Class definitionClass)
    {
        try
        {
            T8DefinitionLevel definitionLevel;

            definitionLevel = (T8DefinitionLevel)getOptionalFieldValue(definitionClass, "DEFINITION_LEVEL");
            return definitionLevel != null ? definitionLevel : T8DefinitionLevel.PROJECT;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Invalid system type flag meta data found in definition class: " + definitionClass, e);
        }
    }

    public static final boolean isProjectLevelDefinition(T8Definition definition)
    {
        return (getDefinitionLevel(definition) == T8DefinitionLevel.PROJECT);
    }

    public static final boolean isSystemLevelDefinition(T8Definition definition)
    {
        return (getDefinitionLevel(definition) == T8DefinitionLevel.SYSTEM);
    }

    public static final boolean isResourceLevelDefinition(T8Definition definition)
    {
        return (getDefinitionLevel(definition) == T8DefinitionLevel.RESOURCE);
    }

    public static final String getDefinitionTypeIdentifier(T8Definition definition)
    {
        try
        {
            return (String)definition.getClass().getField("TYPE_IDENTIFIER").get(null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Invalid type identifier meta data found in definition: " + definition, e);
        }
    }

    public static final String getDefinitionGroupIdentifier(T8Definition definition)
    {
        try
        {
            return (String)definition.getClass().getField("GROUP_IDENTIFIER").get(null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Invalid group identifier meta data found in definition: " + definition, e);
        }
    }

    private static Object getOptionalFieldValue(Class definitionClass, String fieldId) throws Exception
    {
        Field[] fields;

        fields = definitionClass.getFields();
        for (Field field : fields)
        {
            if (field.getName().equals(fieldId))
            {
                return field.get(null);
            }
        }

        return null;
    }

    public static List<T8DefinitionDatumType> getDatumTypes(Class definitionClass)
    {
        List<T8DefinitionDatumType> datumTypeList;
        Class[] classes;
        Class superClass;

        datumTypeList = new ArrayList<T8DefinitionDatumType>();

        classes = definitionClass.getDeclaredClasses();
        for (Class decClass : classes)
        {
            if (decClass.getSimpleName().equals("DatumType"))
            {
                Object[] enumConstants;

                enumConstants = decClass.getEnumConstants();
                for (Object enumConstant : enumConstants)
                {
                    datumTypeList.add((T8DefinitionDatumType)enumConstant);
                }
            }
        }

        // Add all super class datum types.
        superClass = definitionClass.getSuperclass();
        if (superClass != null)
        {
            datumTypeList.addAll(getDatumTypes(superClass));
        }

        return datumTypeList;
    }

    public static final void assignDefinitionParent(T8Definition parentDefinition, T8Definition childDefinition)
    {
        if (childDefinition.getParentDefinition() == null)
        {
            childDefinition.setParentDefinition(parentDefinition);
        }
        else throw new RuntimeException("Cannot assign a definition to a parent if it already has one.");
    }

    public static final boolean isDefinitionDatumMutable(T8Definition definition)
    {
        T8Definition parentDefinition;

        parentDefinition = definition.getParentDefinition();
        if (parentDefinition == null)
        {
            return true;
        }
        else
        {
            T8DefinitionDatumType datumType;

            datumType = parentDefinition.getSubDefinitionDatumType(definition);
            return datumType.isMutable();
        }
    }

    public static final boolean isDefinitionDatumPersisted(T8Definition definition)
    {
        T8Definition parentDefinition;

        parentDefinition = definition.getParentDefinition();
        if (parentDefinition == null)
        {
            return true;
        }
        else
        {
            T8DefinitionDatumType datumType;

            datumType = parentDefinition.getSubDefinitionDatumType(definition);
            return datumType.isPersisted();
        }
    }

    public static List<String> getIdentifierList(List<T8DefinitionMetaData> definitionDetailsList)
    {
        ArrayList<String> identifierList;

        identifierList = new ArrayList<String>();
        for (T8DefinitionMetaData definitionDetails : definitionDetailsList)
        {
            identifierList.add(definitionDetails.getId());
        }

        return identifierList;
    }

    public static T8Definition getDefinition(List<T8Definition> definitionList, String identifier)
    {
        if (definitionList != null)
        {
            for (T8Definition definition : definitionList)
            {
                if (definition.getIdentifier().equals(identifier))
                {
                    return definition;
                }
            }

            return null;
        }
        else return null;
    }

    public static List<T8DefinitionMetaData> getDefinitionMetaData(List<T8Definition> definitions)
    {
        ArrayList<T8DefinitionMetaData> metaData;

        metaData = new ArrayList<T8DefinitionMetaData>();
        if (definitions != null)
        {
            for (T8Definition definition : definitions)
            {
                metaData.add(definition.getMetaData());
            }
        }

        return metaData;
    }

    public static LinkedHashMap<String, T8DefinitionTypeMetaData> getDefinitionTypeMetaData(List<T8Definition> definitions)
    {
        LinkedHashMap<String, T8DefinitionTypeMetaData> typeMetaData;

        typeMetaData = new LinkedHashMap<String, T8DefinitionTypeMetaData>();
        for (T8Definition definition : definitions)
        {
            T8DefinitionTypeMetaData metaData;

            metaData = definition.getTypeMetaData();
            if (!typeMetaData.containsKey(metaData.getTypeId())) // If it already exists, don't add it again because we want to maintain the order according to the list of input definitions.
            {
                typeMetaData.put(metaData.getTypeId(), metaData);
            }
        }

        return typeMetaData;
    }

    /**
     * This method assigns the specified identifier to the supplied definition.
     * This method is inherently unsafe within the context of the larger
     * operational system because the identifier change is not propagated on
     * a global level, only within the local context.
     * @param definition The definition to rename.
     * @param identifier The new identifier that will be assigned to the
     * definition.
     * @param propagateLocalChange A boolean flag indicating whether or not to
     * propagate the identifier change to other definitions locally linked to
     * the supplied renamed definition.
     */
    public static void unsafeRenameLocalDefinition(T8Definition definition, String identifier, boolean propagateLocalChange)
    {
        if (getDefinitionLevel(definition) == T8DefinitionLevel.RESOURCE) throw new RuntimeException("Cannot rename resource definition: " + definition);
        else definition.setIdentifier(identifier, propagateLocalChange);
    }

    public static T8Definition clearAuditingData(T8Definition definition)
    {
        definition.setDefinitionStatus(T8Definition.T8DefinitionStatus.UPDATED);
        definition.setRevision(0);
        definition.setCreatedTime(null);
        definition.setCreatedUserIdentifier(null);
        definition.setUpdatedTime(null);
        definition.setUpdatedUserIdentifier(null);
        return definition;
    }

    public static <T extends T8Definition> ArrayList<com.pilog.t8.definition.T8DefinitionDatumOption> createLocalIdOptionsFromDefinitions(List<T> definitions, boolean allowNull, String nullDisplayName)
    {
        ArrayList<com.pilog.t8.definition.T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedIdentifiers;

        orderedIdentifiers = new TreeSet<>();
        for (T8Definition definition : definitions)
        {
            orderedIdentifiers.add(definition.getIdentifier());
        }

        identifierOptions = new ArrayList<>();
        if (allowNull) identifierOptions.add(new com.pilog.t8.definition.T8DefinitionDatumOption(nullDisplayName, null));
        for (String definitionIdentifier : orderedIdentifiers)
        {
            identifierOptions.add(new com.pilog.t8.definition.T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }
}
