package com.pilog.t8.data.tag;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataTagFilter extends T8DataFilter
{
    public T8DataTagFilter(String entityIdentifier, List<T8Tag> includedTags, List<T8Tag> excludedTags)
    {
        super(entityIdentifier);
        setFilterCriteria(includedTags, excludedTags);
    }
    
    private void setFilterCriteria(List<T8Tag> includedTags, List<T8Tag> excludedTags)
    {
        T8DataFilterCriteria filterCriteria;
        String tagFieldIdentifier;
        
        tagFieldIdentifier = getEntityIdentifier() + "$TAGS";
        
        // Create the main filter criteria.
        filterCriteria = new T8DataFilterCriteria();
        
        // Add tag inclusion criteria if applicable.
        if ((includedTags != null) && (includedTags.size() > 0))
        {
            T8DataFilterCriteria inclusionCriteria;
            
            inclusionCriteria = new T8DataFilterCriteria();
            for (T8Tag tag : includedTags)
            {
                inclusionCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(tagFieldIdentifier, T8DataFilterCriterion.DataFilterOperator.LIKE, tag.getTagString()));
            }
            
            filterCriteria.addFilterClause(DataFilterConjunction.AND, inclusionCriteria);
        }
        
        // Add tag exclusion criteria if applicable.
        if ((excludedTags != null) && (excludedTags.size() > 0))
        {
            T8DataFilterCriteria exclusionCriteria;
            
            exclusionCriteria = new T8DataFilterCriteria();
            for (T8Tag tag : excludedTags)
            {
                exclusionCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(tagFieldIdentifier, T8DataFilterCriterion.DataFilterOperator.NOT_LIKE, tag.getTagString()));
            }
            
            filterCriteria.addFilterClause(DataFilterConjunction.AND, exclusionCriteria);
        }
        
        // Set the filter criteria on the filter.
        setFilterCriteria(filterCriteria);
    }
}
