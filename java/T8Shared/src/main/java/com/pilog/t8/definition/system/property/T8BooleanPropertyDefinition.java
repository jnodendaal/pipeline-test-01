/**
 * Created on 22 Jun 2016, 2:19:09 PM
 */
package com.pilog.t8.definition.system.property;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8BooleanPropertyDefinition extends T8SystemPropertyDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_PROPERTY_BOOL";
    public static final String DISPLAY_NAME = "Boolean Type System Property";
    public static final String DESCRIPTION = "A System Property with a Boolean data type.";
    public static final String VERSION = "0";
    public enum Datum {PROPERTY_VALUE};
    // -------- Definition Meta-Data -------- //

    public T8BooleanPropertyDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.PROPERTY_VALUE.toString(), "Property Value", "The Boolean value of the property.", false));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public Boolean getPropertyValue()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.PROPERTY_VALUE));
    }

    public void setPropertyValue(boolean value)
    {
        setDefinitionDatum(Datum.PROPERTY_VALUE, value);
    }
}