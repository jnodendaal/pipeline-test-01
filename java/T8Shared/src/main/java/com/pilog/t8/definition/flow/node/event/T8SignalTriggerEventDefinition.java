package com.pilog.t8.definition.flow.node.event;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This node triggers a specific flow signal.
 *
 * @author Bouwer du Preez
 */
public class T8SignalTriggerEventDefinition extends T8FlowEventDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_EVENT_SIGNAL_TRIGGER";
    public static final String DISPLAY_NAME = "Flow Signal Trigger Event";
    public static final String DESCRIPTION = "An event that triggers a specific workflow signal.";
    private enum Datum {SIGNAL_IDENTIFIER, PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8SignalTriggerEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SIGNAL_IDENTIFIER.toString(), "Signal", "The identifier of the signal to trigger."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow to Signal", "A mapping of Flow Parameters to the corresponding Signal Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SIGNAL_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), getSignalDefinitionGroupIdentifier()));
        else if (Datum.PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String signalId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            signalId = getSignalIdentifier();
            if (signalId != null)
            {
                T8FlowSignalDefinition signalDefinition;
                T8WorkFlowDefinition flowDefinition;

                signalDefinition = (T8FlowSignalDefinition)definitionContext.getRawDefinition(getRootProjectId(), signalId);
                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();

                if ((signalDefinition != null) && (flowDefinition != null))
                {
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    List<T8DataParameterDefinition> signalParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();
                    signalParameterDefinitions = signalDefinition.getParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((flowParameterDefinitions != null) && (signalParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition signalParameterDefinition : signalParameterDefinitions)
                            {
                                identifierList.add(signalParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(flowParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.event.T8SignalTriggerEventNode").getConstructor(T8Context.class, T8Flow.class, T8SignalTriggerEventDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    public String getSignalDefinitionGroupIdentifier()
    {
        return T8FlowSignalDefinition.GROUP_IDENTIFIER;
    }

    public String getSignalIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SIGNAL_IDENTIFIER.toString());
    }

    public void setSignalIdentifier(String taskIdentifier)
    {
        setDefinitionDatum(Datum.SIGNAL_IDENTIFIER.toString(), taskIdentifier);
    }

    public Map<String, String> getParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.PARAMETER_MAPPING.toString());
    }

    public void setParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.PARAMETER_MAPPING.toString(), mapping);
    }
}
