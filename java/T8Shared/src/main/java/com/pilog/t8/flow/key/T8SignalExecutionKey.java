package com.pilog.t8.flow.key;

import com.pilog.t8.flow.state.T8FlowNodeState;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SignalExecutionKey extends T8DefaultFlowExecutionKey
{
    private final String signalIdentifier;
    private final Map<String, Object> signalParameters;

    public T8SignalExecutionKey(T8FlowNodeState nodeState, String signalIdentifier, Map<String, Object> signalParameters)
    {
        super(nodeState);
        this.signalIdentifier = signalIdentifier;
        this.signalParameters = signalParameters;
    }

    public String getSignalIdentifier()
    {
        return signalIdentifier;
    }
    
    public Map<String, Object> getSignalParameters()
    {
        return signalParameters;
    }
}
