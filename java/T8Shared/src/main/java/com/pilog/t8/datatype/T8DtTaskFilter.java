package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.flow.task.T8TaskFilter;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DtTaskFilter extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TASK_FILTER";

    public T8DtTaskFilter() {}

    public T8DtTaskFilter(T8DefinitionManager context) {}

    public T8DtTaskFilter(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8TaskFilter.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject taskFilterObject;
            T8TaskFilter taskFilter;
            Set<String> taskIds;

            // Create the concept JSON object.
            taskFilter = (T8TaskFilter)object;
            taskFilterObject = new JsonObject();
            taskFilterObject.add("flowIid", taskFilter.getFlowInstanceIdentifier());
            taskFilterObject.add("searchString", taskFilter.getDescriptivePropertySearchString());
            taskFilterObject.add("includeClaimedTasks", taskFilter.isIncludeClaimedTasks());
            taskFilterObject.add("includeUnclaimedTasks", taskFilter.isIncludeUnclaimedTasks());
            taskFilterObject.add("pageOffset", taskFilter.getPageOffset());
            taskFilterObject.add("pageSize", taskFilter.getPageSize());

            // Add the task ids to filter by.
            taskIds = taskFilter.getIncludedTaskTypes();
            if ((taskIds != null) && (!taskIds.isEmpty()))
            {
                JsonArray taskIdArray;

                taskIdArray = new JsonArray();
                taskFilterObject.add("taskIds", taskIdArray);
                for (String taskId : taskIds)
                {
                    taskIdArray.add(taskId);
                }
            }

            // Return the final task list object.
            return taskFilterObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject taskFilterObject;
            JsonArray taskIdArray;
            T8TaskFilter taskFilter;

            // Get the JSON values.
            taskFilterObject = jsonValue.asObject();
            taskIdArray = taskFilterObject.getJsonArray("taskIds");

            // Create the task filter object.
            taskFilter = new T8TaskFilter();
            taskFilter.setFlowInstanceIdentifier(taskFilterObject.getString("flowIid"));
            taskFilter.setDescriptivePropertySearchString(taskFilterObject.getString("searchString"));
            taskFilter.setIncludeClaimedTasks(taskFilterObject.getBoolean("includeClaimedTasks", false));
            taskFilter.setIncludeClaimedTasks(taskFilterObject.getBoolean("includeUnclaimedTasks", true));
            taskFilter.setPageOffset(taskFilterObject.getInt("pageOffset", 0));
            taskFilter.setPageSize(taskFilterObject.getInt("pageSize", 0));

            // Add the filter task id list if specified.
            if (taskIdArray != null)
            {
                for (JsonValue taskIdValue : taskIdArray.values())
                {
                    taskFilter.addIncludedTaskType(taskIdValue.asString());
                }
            }

            // Return the completed object.
            return taskFilter;
        }
        else return null;
    }
}