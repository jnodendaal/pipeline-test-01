package com.pilog.t8.definition.flow;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.definition.flow.phase.T8FlowPhaseDefinition;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8WorkFlowNodeDefinition extends T8GraphNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_WORK_FLOW_NODE";
    public static final String STORAGE_PATH = null;
    public enum Datum {PHASE_IDENTIFIER,
                       DISPLAY_NAME};
    // -------- Definition Meta-Data -------- //

    public enum FlowNodeType {EVENT, ACTIVITY, GATEWAY};

    public T8WorkFlowNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PHASE_IDENTIFIER.toString(), "Phase", "The work flow phase to which this node belongs."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The name of this node when displayed on a client UI."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PHASE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createLocalIdentifierOptionsFromDefinitions(getLocalDefinitionsOfGroup(T8FlowPhaseDefinition.GROUP_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public abstract FlowNodeType getNodeType();
    public abstract T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String nodeIid) throws Exception;

    public String getPhaseIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PHASE_IDENTIFIER.toString());
    }

    public void setPhaseIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PHASE_IDENTIFIER.toString(), identifier);
    }

    public String getDisplayName()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_NAME);
    }

    public void setDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME, displayName);
    }
}
