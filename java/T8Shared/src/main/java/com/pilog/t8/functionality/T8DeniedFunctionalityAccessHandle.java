package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;

/**
 * @author Bouwer du Preez
 */
public class T8DeniedFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle
{
    private final T8FunctionalityAccessType accessType;
    private final String accessMessage;

    public T8DeniedFunctionalityAccessHandle(T8FunctionalityHandle functionalityHandle, T8FunctionalityAccessType accessType, String accessMessage)
    {
        super(functionalityHandle.getId(), null, functionalityHandle.getDisplayName(), functionalityHandle.getDescription(), functionalityHandle.getIcon());
        this.accessType = accessType;
        this.accessMessage = accessMessage;
    }

    /**
     * Returns the type of access granted to the requester.
     * @return The type of access granted tot he requester.
     */
    @Override
    public T8FunctionalityAccessType getAccessType()
    {
        return accessType;
    }

    /**
     * Returns the access message (if any) applicable to the access type granted to the requester.
     * @return The access message (if any) applicable to the access type granted to the requester.
     */
    @Override
    public String getAccessMessage()
    {
        return accessMessage;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        throw new UnsupportedOperationException("Functionality access denied.  Use of this method not supported.");
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        throw new UnsupportedOperationException("Functionality access denied.  Use of this method not supported.");
    }
}
