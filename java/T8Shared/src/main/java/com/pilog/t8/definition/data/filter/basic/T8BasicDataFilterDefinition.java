package com.pilog.t8.definition.data.filter.basic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * @author Bouwer du Preez
 */
public class T8BasicDataFilterDefinition extends T8DataFilterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_FILTER_BASIC";
    public static final String DISPLAY_NAME = "Basic Data Filter";
    public static final String DESCRIPTION = "An object that specifies basic filter criteria to be used for filtering data entities.";
    private enum Datum {FIELD_CRITERIA_EXPRESSION_MAP,
                        FIELD_ODERING_MAP};
    // -------- Definition Meta-Data -------- //

    public T8BasicDataFilterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.FIELD_CRITERIA_EXPRESSION_MAP.toString(), "Expression Map:  Field Identifier to Filter Value Expression", "The expressions that will be evaluated in order to determine the filter values for each of the specified entity fields.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_STRING_MAP, Datum.FIELD_ODERING_MAP.toString(), "Data Ordering", "The ordering that will be applied to data retrieved by this filter.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString())));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FIELD_CRITERIA_EXPRESSION_MAP.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String dataEntityId;

            optionList = new ArrayList<>();

            dataEntityId = getDataEntityIdentifier();
            if (dataEntityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), dataEntityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new LinkedHashMap<>();
                    if (fieldDefinitions != null)
                    {
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            // Add the field identifier with a null value (in order to allow any String to be entered as an expression).
                            identifierMap.put(fieldDefinition.getPublicIdentifier(), null);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.FIELD_ODERING_MAP.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String dataEntityId;

            optionList = new ArrayList<>();

            dataEntityId = getDataEntityIdentifier();
            if (dataEntityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), dataEntityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new LinkedHashMap<>();
                    if (fieldDefinitions != null)
                    {
                        ArrayList<String> options;

                        options = new ArrayList<>();
                        options.add(OrderMethod.ASCENDING.toString());
                        options.add(OrderMethod.DESCENDING.toString());
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            identifierMap.put(fieldDefinition.getPublicIdentifier(), options);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (getFieldCriteriaExpressionMap() != null)
        {
            for (String fieldIdentifier : getFieldCriteriaExpressionMap().keySet())
            {
                if (!Objects.equals(getDataEntityIdentifier(), T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier)))
                {
                    validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.FIELD_CRITERIA_EXPRESSION_MAP.toString(), "Invalid Field Identifier " + fieldIdentifier, T8DefinitionValidationError.ErrorType.CRITICAL));
                }
            }
        }

        if (getFieldOrderingMap() != null)
        {
            for (String fieldIdentifier : getFieldOrderingMap().keySet())
            {
                if (!Objects.equals(getDataEntityIdentifier(), T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier)))
                {
                    validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.FIELD_ODERING_MAP.toString(), "Invalid Field Identifier " + fieldIdentifier, T8DefinitionValidationError.ErrorType.CRITICAL));
                }
            }
        }

        return validationErrors;
    }

    @Override
    public T8DataFilter getNewDataFilterInstance(T8Context context, Map<String, Object> filterParameters)
    {
        Map<String, String> expressionMap;
        T8DataFilter dataFilter;

        // Create the new data filter.
        dataFilter = new T8DataFilter(getDataEntityIdentifier());
        dataFilter.setFieldOrdering(getFieldOrderingMap());

        // Evaluate all field filter value expressions.
        expressionMap = getFieldCriteriaExpressionMap();
        if (expressionMap != null)
        {
            Map<String, Object> expressionParameters;
            Map<String, Object> filterValues;
            T8ExpressionEvaluator evaluator;

            // Create the expression evaluator to evaluate field value expressions.
            evaluator = context.isServer() ? context.getServerContext().getExpressionEvaluator(context) : context.getClientContext().getExpressionEvaluator();

            // Create the map to hold all of the expression parameters that may be used during expression evaluation.
            expressionParameters = new HashMap<>();
            expressionParameters.putAll(context.getSessionContext().getSessionParameterMap());
            if (filterParameters != null) expressionParameters.putAll(filterParameters);

            // Create a map to hold the fields and each field's filter value.
            filterValues = new HashMap<>();
            for (String fieldIdentifier : expressionMap.keySet())
            {
                String expression;

                // Get the expression for the field.
                expression = expressionMap.get(fieldIdentifier);
                if (!Strings.isNullOrEmpty(expression))
                {
                    // Evaluate the field value expression.
                    try
                    {
                        Object filterValue;

                        filterValue = evaluator.evaluateExpression(expression, expressionParameters, null);
                        filterValues.put(fieldIdentifier, filterValue);
                    }
                    catch (EPICSyntaxException | EPICRuntimeException e)
                    {
                        throw new RuntimeException("Exception while executing filter criterion value expression: " + expressionMap, e);
                    }
                } else filterValues.put(fieldIdentifier, null);
            }

            // Add the filter values as criteria to the filter object.
            dataFilter.addFilterCriteria(filterValues);
            return dataFilter;
        }
        return dataFilter;
    }

    public Map<String, String> getFieldCriteriaExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.FIELD_CRITERIA_EXPRESSION_MAP.toString());
    }

    public void setFieldCriteriaExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.FIELD_CRITERIA_EXPRESSION_MAP.toString(), expressionMap);
    }

    public void setFieldOrderingMap(Map<String, OrderMethod> orderingMap)
    {
        setDefinitionDatum(Datum.FIELD_ODERING_MAP.toString(), HashMaps.toStringValueMap(orderingMap));
    }

    public LinkedHashMap<String, OrderMethod> getFieldOrderingMap()
    {
        Map<String, String> orderMapDatum;

        orderMapDatum = (Map<String, String>)getDefinitionDatum(Datum.FIELD_ODERING_MAP.toString());
        if (orderMapDatum != null)
        {
            LinkedHashMap<String, OrderMethod> orderMap;

            orderMap = new LinkedHashMap<>();
            for (String fieldIdentifier : orderMapDatum.keySet())
            {
                String orderMethod;

                orderMethod = orderMapDatum.get(fieldIdentifier);
                if (orderMethod != null)
                {
                    orderMap.put(fieldIdentifier, OrderMethod.valueOf(orderMethod));
                }
            }

            return orderMap;
        } else return null;
    }
}
