package com.pilog.t8.data.object;

import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DataObjectStateGraph
{
    public String getId();
    public List<String> getDataObjectIds();
    public T8DataObjectState getState(String stateId);
    public T8DataObjectState getStateCreatedBy(String functionalityId);
    public T8DataObjectState getNextState(String functionalityId, String fromStateId, String toStateId);
    public T8DataObjectState getStateByConceptId(String conceptId);
}
