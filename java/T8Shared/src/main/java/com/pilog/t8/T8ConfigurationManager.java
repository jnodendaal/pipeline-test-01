package com.pilog.t8;

import com.pilog.t8.ui.T8LookAndFeelManager;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8ConfigurationManager extends T8ServerModule
{
    public T8UserManagerConfiguration getUserManagerConfiguration();

    /**
     * Retrieves the API associated with the specified API identifier
     * {@code String}. If the API does not exist or cannot be initialized, an
     * error will be raised. This means that the returning value will always be
     * an instantiated {@code APIFacade} instance.
     *
     * @param <A> The {@code T8Api} instance type
     *
     * @param context The {@code T8Context} which will be used to
     *      retrieve and instantiate the specified API
     * @param apiIdentifier The {@code String} identifier of the required API
     *
     * @return The {@code T8Api} instance. Never {@code null}
     *
     * @throws RuntimeException If the API does not exist or cannot be
     *      instantiated
     */
    public <A extends T8Api> A getAPI(T8Context context, String apiIdentifier);
    public Map<String, String> getAPIClassMap(T8Context context);
    public void requestUITranslations(T8Context context, String languageIdentifier, Set<String> phrases);
    public Map<String, String> getUITranslationMap(T8Context context, String languageIdentifier);
    public String getUITranslation(T8Context context, String languageIdentifier, String phrase);
    public String getUITranslation(T8Context context, String phrase);
    /**
     * Gets the value of a system property as set up in the definitions. The
     * returned value type is dependent on the property definition type. The
     * system properties should be subtypes of the
     * {@code T8SystemPropertyDefinition} where these sub-types define the type
     * of the property value.<br/>
     * <br/>
     * The generic return type allows for implicit casting of the returning
     * value for the property.
     *
     * @param <P> The property type which will be returned
     * @param context The {@code T8Context} which defines the
     *      current session and its related attributes
     * @param propertyIdentifier The definition identifier for the specific
     *      property to be retrieved
     *
     * @return The property of type {@code P}
     *
     * @throws RuntimeException If the specified property does not exist
     */
    public <P> P getProperty(T8Context context, String propertyIdentifier);
    /**
     * @see #getProperty(com.pilog.t8.T8Context, java.lang.String).<br/>
     * <br/>
     * Performs exactly the same function as above method, with the exception
     * that if the property is not found, the default value specified will be
     * returned. This method should be limited to cases where it makes sense to
     * use a default value.
     *
     * @param <P> The property type which will be returned
     * @param context The {@code T8Context} which defines the
     *      current session and its related attributes
     * @param propertyIdentifier The definition identifier for the specific
     *      property to be retrieved
     * @param defaultValue The default value of type {@code P} to be returned
     *      if the property does not exist
     *
     * @return The property value of type {@code P} or the default value if the
     *      property does not exist
     */
    public <P> P getProperty(T8Context context, String propertyIdentifier, P defaultValue);
    public T8LookAndFeelManager getLookAndFeelManager(T8Context context);

    <V extends Object> void setUserProperty(T8Context context, String identifier, V value);
    <V extends Object> V getUserProperty(T8Context context, String identifier, V defaultValue);

    public T8OperationalHoursCalculator getOperationalHoursCalculator(T8Context context) throws Exception;
}
