package com.pilog.t8.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.Serializable;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public class T8Image implements Serializable
{
    // The ID used to verify serialized object version.
    static final long serialVersionUID = 1L;
    
    private int[] pixels;
    private int width;
    private int height;

    public T8Image(int[] pixels, int width, int height)
    {
        this.pixels = pixels;
        this.width = width;
        this.height = height;
    }
    
    public T8Image(Image image) throws Exception
    {
        setImage(image);
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
    
    public int[] getPixels()
    {
        return pixels;
    }

    public void setImage(int[] pixels, int width, int height)
    {
        this.pixels = pixels;
        this.width = width;
        this.height = height;
    }
    
    public void setImage(Image image) throws InterruptedException
    {
        PixelGrabber pixelGrabber;

        width = image.getWidth(null);
        height = image.getHeight(null);
        pixels = new int[width * height];
        pixelGrabber = new PixelGrabber(image, 0, 0, width, height, pixels, 0, width);
        pixelGrabber.grabPixels();
    }

    public Image getImage()
    {
        if ((pixels != null) && (pixels.length > 0))
        {
            MemoryImageSource imageSource;
            Toolkit toolkit;

            imageSource = new MemoryImageSource(width, height, pixels, 0, width);
            toolkit = Toolkit.getDefaultToolkit();
            return toolkit.createImage(imageSource);
        }
        else return null;
    }
    
    public BufferedImage getBufferedImage()
    {
        BufferedImage bufferedImage;
        Graphics bufferedImageG;

        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        bufferedImageG = bufferedImage.getGraphics();
        bufferedImageG.drawImage(getImage(), 0, 0, null);
        return bufferedImage;
    }
    
    public ImageIcon getImageIcon()
    {
        Image image;
        
        image = getImage();
        return image != null ? new ImageIcon(image) : new ImageIcon();
    }
}
