package com.pilog.t8.definition.data;

import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DataParameterResourceDefinition extends T8DataParameterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RESOURCE_DATA_PARAMETER";
    public static final String DISPLAY_NAME = "Data Parameter Resource";
    public static final String DESCRIPTION = "A parameter used as input or output for a particular operation.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8DataParameterResourceDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataParameterResourceDefinition(String valueId, String displayName, String valueDescription, T8DataType dataType)
    {
        this(valueId, displayName, valueDescription, dataType, false);
    }

    public T8DataParameterResourceDefinition(String valueId, String displayName, String valueDescription, T8DataType dataType, boolean optional)
    {
        super(valueId, displayName, valueDescription, dataType, optional);
    }

    public T8DataParameterResourceDefinition(String valueId, String displayName, String valueDescription, String dataTypeId)
    {
        this(valueId, displayName, valueDescription, dataTypeId, false);
    }

    public T8DataParameterResourceDefinition(String valueId, String displayName, String valueDescription, String dataTypeId, boolean optional)
    {
        super(valueId, displayName, valueDescription, dataTypeId, optional);
    }
}
