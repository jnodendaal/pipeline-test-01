package com.pilog.t8.ui;

import com.pilog.graph.model.GraphModel;
import com.pilog.graph.view.GraphVertexRenderer;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import java.awt.Component;
import java.awt.Dimension;

/**
 * @author Bouwer du Preez
 */
public interface T8GraphManager
{
    public GraphVertexRenderer getVertextRenderer();
    public Dimension getPreferredSize();
    public GraphModel buildGraphModel(T8GraphDefinition graphDefinition);
    public Component getConfigurationComponent();
}
