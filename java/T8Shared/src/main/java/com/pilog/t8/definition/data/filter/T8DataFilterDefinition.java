package com.pilog.t8.definition.data.filter;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataFilterDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_FILTER";
    public static final String IDENTIFIER_PREFIX = "FILTER_";
    public static final String DISPLAY_NAME = "Data Filter";
    public static final String DESCRIPTION = "An object that specifies filter criteria to be used for filtering data entities.";
    public enum Datum {DATA_ENTITY_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DataFilterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity to which this data filter applies."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DataFilter getNewDataFilterInstance(T8Context context, Map<String, Object> filterParameters);

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }
}
