package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DialogFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_DIALOG";
    public static final String DISPLAY_NAME = "Dialog Functionality";
    public static final String DESCRIPTION = "A T8 functionality that opens a specific global dialog.";
    public static final String IDENTIFIER_PREFIX = "FNC_D_";
    public enum Datum
    {
        DIALOG_IDENTIFIER,
        INPUT_PARAMETER_MAPPING,
        INPUT_PARAMETER_EXPRESSION_MAP
    };
    // -------- Definition Meta-Data -------- //

    public T8DialogFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DIALOG_IDENTIFIER.toString(), "Dialog Identifier", "The identifier of the dialog linked to this functionality."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Target Object Fields to Dialog Input", "A mapping of target object fields to the corresponding input parameters of the dialog."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), "Input Parameter Expression Map", "The parameters the will be sent to the dialog as inputs."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DIALOG_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8GlobalDialogDefinition dialogDefinition;
            String projectId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            projectId = getRootProjectId();

            dialogDefinition = (T8GlobalDialogDefinition)definitionContext.getRawDefinition(projectId, getDialogIdentifier());
            if (dialogDefinition != null)
            {
                List<T8DataParameterDefinition> dialogInputParameterDefinitions;
                List<T8DataParameterDefinition> fuctionalityParameterDefinitions;
                HashMap<String, List<String>> identifierMap;

                dialogInputParameterDefinitions = dialogDefinition.getInputParameterDefinitions();
                fuctionalityParameterDefinitions = getInputParameterDefinitions();

                identifierMap = new HashMap<String, List<String>>();
                if ((dialogInputParameterDefinitions != null) && (fuctionalityParameterDefinitions != null))
                {
                    for (T8DataParameterDefinition functionalityParameterDefinition : fuctionalityParameterDefinitions)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<String>();
                        for (T8DataParameterDefinition dialogInputParameterDefinition : dialogInputParameterDefinitions)
                        {
                            identifierList.add(dialogInputParameterDefinition.getPublicIdentifier());
                        }

                        identifierMap.put(functionalityParameterDefinition.getPublicIdentifier(), identifierList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId))
        {
            if(Strings.isNullOrEmpty(getDialogIdentifier()))
            {
                return new ArrayList<T8DefinitionDatumOption>(0);
            }
            else
            {
                Map<String, List<String>> identifiers = new HashMap<String, List<String>>();
                ArrayList<T8DefinitionDatumOption> optionList = new ArrayList<T8DefinitionDatumOption>(1);
                T8GlobalDialogDefinition dialogDefinition;

                dialogDefinition = (T8GlobalDialogDefinition)definitionContext.getRawDefinition(getRootProjectId(), getDialogIdentifier());
                for (T8DataParameterDefinition parameterDefinition : dialogDefinition.getInputParameterDefinitions())
                {
                    identifiers.put(parameterDefinition.getPublicIdentifier(), null);
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifiers));
                return optionList;
            }
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumIdentifier)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8DialogFunctionalityInstance").getConstructor(T8Context.class, T8DialogFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8DialogFunctionalityInstance").getConstructor(T8Context.class, T8DialogFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }

    public String getDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DIALOG_IDENTIFIER.toString());
    }

    public void setDialogIdentifier(String dialogIdentifier)
    {
        setDefinitionDatum(Datum.DIALOG_IDENTIFIER.toString(), dialogIdentifier);
    }

    public Map<String, String> getInputParameterExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setInputParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), expressionMap);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
