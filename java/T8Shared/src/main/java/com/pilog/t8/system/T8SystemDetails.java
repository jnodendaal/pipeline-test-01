package com.pilog.t8.system;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8SystemDetails implements Serializable
{
    private String productConfigurationIconIdentifier;
    private String productDeveloperIconIdentifier;
    private String productClientIconIdentifier;
    private String productSplashIconIdentifier;
    private String externalBinding;
    private String displayName;
    private String identifier;

    public T8SystemDetails(String identifier)
    {
        this.identifier = identifier;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getProductConfigurationIconIdentifier()
    {
        return productConfigurationIconIdentifier;
    }

    public void setProductConfigurationIconIdentifier(String productConfigurationIconIdentifier)
    {
        this.productConfigurationIconIdentifier = productConfigurationIconIdentifier;
    }

    public String getProductDeveloperIconIdentifier()
    {
        return productDeveloperIconIdentifier;
    }

    public void setProductDeveloperIconIdentifier(String productDeveloperIconIdentifier)
    {
        this.productDeveloperIconIdentifier = productDeveloperIconIdentifier;
    }

    public String getProductClientIconIdentifier()
    {
        return productClientIconIdentifier;
    }

    public void setProductClientIconIdentifier(String productClientIconIdentifier)
    {
        this.productClientIconIdentifier = productClientIconIdentifier;
    }

    public String getProductSplashIconIdentifier()
    {
        return this.productSplashIconIdentifier;
    }

    public void setProductSplashIconIdentifier(String productSplashIconIdentifier)
    {
        this.productSplashIconIdentifier = productSplashIconIdentifier;
    }

    public String getExternalBinding()
    {
        return this.externalBinding;
    }

    public void setExternalBinding(String externalBinding)
    {
        this.externalBinding = externalBinding;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8SystemDetails{");
        toStringBuilder.append("identifier=").append(this.identifier);
        toStringBuilder.append(",displayName=").append(this.displayName);
        toStringBuilder.append(",productDeveloperIconIdentifier=").append(this.productDeveloperIconIdentifier);
        toStringBuilder.append(",productClientIconIdentifier=").append(this.productClientIconIdentifier);
        toStringBuilder.append(",productConfigurationIconIdentifier=").append(this.productConfigurationIconIdentifier);
        toStringBuilder.append(",productSplashIconIdentifier=").append(this.productSplashIconIdentifier);
        toStringBuilder.append(",externalBinding=").append(this.externalBinding);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
