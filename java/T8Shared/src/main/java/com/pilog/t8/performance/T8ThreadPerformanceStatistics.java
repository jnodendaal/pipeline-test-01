package com.pilog.t8.performance;

import java.io.OutputStream;

/**
 * @author Bouwer du Preez
 */
public interface T8ThreadPerformanceStatistics
{
    public String getIdentifier();
    public void reset();
    public void logEvent(String eventIdentifier);
    public void logExecutionStart(String executionIdentifier);
    public void logExecutionEnd(String executionIdentifier);
    public void printStatistics(OutputStream stream);
}
