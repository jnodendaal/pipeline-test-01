package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowStatus;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowCompletedEvent extends T8FlowEvent
{
    private final Map<String, Object> outputParameters;

    public static final String TYPE_ID = "FLOW_COMPLETED";

    public T8FlowCompletedEvent(T8FlowStatus flow, Map<String, Object> outputParameters)
    {
        super(flow);
        this.outputParameters = outputParameters;
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public T8FlowStatus getFlowStatus()
    {
        return (T8FlowStatus)source;
    }

    public Map<String, Object> getOutputParameters()
    {
        return outputParameters;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
