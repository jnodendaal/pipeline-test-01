package com.pilog.t8.data;

/**
 * @author Bouwer du Preez
 */
public interface T8DataEntityResults
{
    public String getEntityIdentifier();
    public enum StreamType {INPUT, OUTPUT};
    public void setStreamType(String fieldIdentifier, StreamType streamType);
    public boolean next() throws Exception;
    public T8DataEntity get();
    public void close();

    public void addListener(T8DataEntityResultListener listener);
    public void removeListener(T8DataEntityResultListener listener);
}
