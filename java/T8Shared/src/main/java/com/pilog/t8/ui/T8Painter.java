package com.pilog.t8.ui;

import java.awt.Graphics2D;

/**
 * @author Bouwer du Preez
 */
public interface T8Painter
{
    /**
     * Paints to the specified graphics context.
     * 
     * @param g2 The Graphics2D to render to. This must not be null.
     * @param object An optional configuration parameter. This may be null.
     * @param width Width of the area to paint.
     * @param height Height of the area to paint.
     */
    public void paint(Graphics2D g2, Object object, int width, int height);
}
