package com.pilog.t8.definition.functionality.group;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectFunctionalityGroupDefinition extends T8DefaultFunctionalityGroupDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_GROUP_PROJECT";
    public static final String DISPLAY_NAME = "Project Functionality Group";
    public static final String DESCRIPTION = "A grouping for a set of functionalities available in a T8 Project.";
    public static final String IDENTIFIER_PREFIX = "FNC_GRP_";
    public static final String STORAGE_PATH = "/functionality_groups";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.PROJECT;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8ProjectFunctionalityGroupDefinition(String identifier)
    {
        super(identifier);
    }
}
