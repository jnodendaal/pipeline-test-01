package com.pilog.t8.user.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8UserManagerListener extends EventListener
{
    public void userProfilePictureUpdated(T8UserProfilePictureUpdatedEvent event);
    public void userDetailsUpdated(T8UserDetailsUpdatedEvent event);
}
