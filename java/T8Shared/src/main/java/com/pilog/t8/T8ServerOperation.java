package com.pilog.t8;

import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * This interface must be implemented by all classes representing operations
 * that can be executed on the T8MainServer.
 *
 * @author Bouwer du Preez
 */
public interface T8ServerOperation
{
    public enum T8ServerOperationExecutionType {SYNCHRONOUS, ASYNCHRONOUS};
    public enum T8ServerOperationStatus {NOT_STARTED, COMPLETED, IN_PROGRESS, STOPPED, CANCELLED, PAUSED, FAILED, STOPPING, CANCELLING, PAUSING};

    public void init();
    public double getProgress(T8Context context); // Returns the progress of the operation execution.
    public T8ProgressReport getProgressReport(T8Context context); // Returns a status report detailing the progress and current status of the operation.
    public Map<String, ? extends Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception;
    public void stop(T8Context context); // Stops execution without cancelling already completed progress.
    public void cancel(T8Context context); // Cancels all completed progress, rolling back changes where possible.
    public void pause(T8Context context); // Pauses execution of the operation.
    public void resume(T8Context context); // Resumes a previously paused operation.
}
