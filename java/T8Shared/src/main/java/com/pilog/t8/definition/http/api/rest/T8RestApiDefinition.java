package com.pilog.t8.definition.http.api.rest;

import com.pilog.t8.T8Server;
import com.pilog.t8.definition.http.api.T8HttpApiDefinition;
import com.pilog.t8.http.api.T8HttpApi;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8RestApiDefinition extends T8HttpApiDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_HTTP_API_REST";
    public static final String DISPLAY_NAME = "RESTful HTTP API";
    public static final String DESCRIPTION = "A definition of a RESTful API accessible via HTTP request using URI and JSON content.";
    public enum Datum
    {
        API_NAME,
        METHOD_PARAMETER_ID,
        REQUEST_BODY_PARAMETER_ID,
        RESPONSE_BODY_PARAMETER_ID,
        QUERY_PARAMETER_ID,
        PARAMETER_DEFINITIONS,
        URI_PARSERS
    };
    // -------- Definition Meta-Data -------- //

    public T8RestApiDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.API_NAME.toString(), "API Name", "The name of the API as it will be used in a request URI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.METHOD_PARAMETER_ID.toString(), "Http Method Parameter Id", "The id of the parameter in which to store the HTTP method of the request, eg. PUT, POST, GET."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.QUERY_PARAMETER_ID.toString(), "Query Parameter Id", "The id of the parameter in which to store the query parameters received as part of the URI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REQUEST_BODY_PARAMETER_ID.toString(), "Request Body Parameter Id", "The id of the parameter in which to store the JSON body as extracted from the HTTP request."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RESPONSE_BODY_PARAMETER_ID.toString(), "Response Body Parameter Id", "The id of the parameter from which to retrieve the JSON body to be sent with HTTP resonse after the request has been serviced."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PARAMETER_DEFINITIONS.toString(), "Parameters", "The parameters applicable to the URIs of this API."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.URI_PARSERS.toString(), "URI Parsers",  "The parsers to use when parsing URIs addressed to this API."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.URI_PARSERS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8UriParserDefinition.GROUP_IDENTIFIER));
        else if (Datum.METHOD_PARAMETER_ID.toString().equals(datumId))
        {
            List<T8DataParameterDefinition> uriParameterDefinitions;

            uriParameterDefinitions = getParameterDefinitions();
            return createLocalIdentifierOptionsFromDefinitions(uriParameterDefinitions, true, "Not Captured");
        }
        else if (Datum.QUERY_PARAMETER_ID.toString().equals(datumId))
        {
            List<T8DataParameterDefinition> uriParameterDefinitions;

            uriParameterDefinitions = getParameterDefinitions();
            return createLocalIdentifierOptionsFromDefinitions(uriParameterDefinitions, true, "Not Captured");
        }
        else if (Datum.REQUEST_BODY_PARAMETER_ID.toString().equals(datumId))
        {
            List<T8DataParameterDefinition> uriParameterDefinitions;

            uriParameterDefinitions = getParameterDefinitions();
            return createLocalIdentifierOptionsFromDefinitions(uriParameterDefinitions, true, "Not Set");
        }
        else if (Datum.RESPONSE_BODY_PARAMETER_ID.toString().equals(datumId))
        {
            List<T8DataParameterDefinition> uriParameterDefinitions;

            uriParameterDefinitions = getParameterDefinitions();
            return createLocalIdentifierOptionsFromDefinitions(uriParameterDefinitions, true, "Not Set");
        }
        else if (Datum.QUERY_PARAMETER_ID.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            List<T8DataParameterDefinition> apiParameterDefinitions;
            TreeMap<String, List<String>> idMap;
            ArrayList<String> parameterIdList;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            apiParameterDefinitions = getParameterDefinitions();
            idMap = new TreeMap<String, List<String>>();

            parameterIdList = new ArrayList<String>();
            for (T8DataParameterDefinition parameterDefinition : apiParameterDefinitions)
            {
                parameterIdList.add(parameterDefinition.getIdentifier());
            }

            idMap.put(null, parameterIdList);
            optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", idMap));
            return optionList;
        }
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.PARAMETER_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8HttpApi getHttpApi(T8Server server, T8Context context) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.http.api.rest.T8RestHttpApi", new Class<?>[]{T8Server.class, T8Context.class, T8RestApiDefinition.class}, server, context, this);
    }

    public String getApiName()
    {
        return getDefinitionDatum(Datum.API_NAME);
    }

    public void setApiName(String name)
    {
        setDefinitionDatum(Datum.API_NAME, name);
    }

    public String getMethodParameterId()
    {
        return getDefinitionDatum(Datum.METHOD_PARAMETER_ID);
    }

    public void setMethodParameterId(String id)
    {
        setDefinitionDatum(Datum.METHOD_PARAMETER_ID, id);
    }

    public String getQueryParameterId()
    {
        return getDefinitionDatum(Datum.QUERY_PARAMETER_ID);
    }

    public void setQueryParameterId(String id)
    {
        setDefinitionDatum(Datum.QUERY_PARAMETER_ID, id);
    }

    public String getRequestBodyParameterId()
    {
        return getDefinitionDatum(Datum.REQUEST_BODY_PARAMETER_ID);
    }

    public void setRequestBodyParameterId(String id)
    {
        setDefinitionDatum(Datum.REQUEST_BODY_PARAMETER_ID, id);
    }

    public String getResponseBodyParameterId()
    {
        return getDefinitionDatum(Datum.RESPONSE_BODY_PARAMETER_ID);
    }

    public void setResponseBodyParameterId(String id)
    {
        setDefinitionDatum(Datum.RESPONSE_BODY_PARAMETER_ID, id);
    }

    public ArrayList<T8DataParameterDefinition> getParameterDefinitions()
    {
        return getDefinitionDatum(Datum.PARAMETER_DEFINITIONS);
    }

    public void setParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public List<T8UriParserDefinition> getUriParserDefinitions()
    {
        return getDefinitionDatum(Datum.URI_PARSERS);
    }

    public void setUriParserDefinitions(List<T8UriParserDefinition> parserDefinitions)
    {
        setDefinitionDatum(Datum.URI_PARSERS, parserDefinitions);
    }
}
