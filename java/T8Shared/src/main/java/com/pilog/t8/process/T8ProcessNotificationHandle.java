package com.pilog.t8.process;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessNotificationHandle
{
    // This class is simply used as a synchronization handle.  Whenever the 
    // status of an executing process changes, notifyAll() is invoked on the
    // notification handle of the operation.
}
