package com.pilog.t8.definition.gfx.painter;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.gfx.painter.effect.T8PainterEffectDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AbstractAreaPainterDefinition extends T8AbstractLayoutPainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {FILL_PAINT_DEFINITION,
                       PAINT_STRETCHED, 
                       BORDER_PAINT_DEFINITION, 
                       BORDER_WIDTH, 
                       AREA_EFFECT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //
    
    public T8AbstractAreaPainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FILL_PAINT_DEFINITION.toString(), "Fill Paint", "The paint that is used to fill the painted area."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.PAINT_STRETCHED.toString(), "Stretch Paint", "A boolean value indicating whether or not this painter should attemp to resize the paint to fit the painter area.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.BORDER_PAINT_DEFINITION.toString(), "Border Paint", "The paint to use for stroking the shape (painting the outline)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.BORDER_WIDTH.toString(), "Border Width", "The width of the border (in pixels).", 0));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.AREA_EFFECT_DEFINITIONS.toString(), "Area Effects", "The area effects to apply to this painter."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILL_PAINT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PaintDefinition.GROUP_IDENTIFIER));
        else if (Datum.BORDER_PAINT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PaintDefinition.GROUP_IDENTIFIER));
        else if (Datum.AREA_EFFECT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PainterEffectDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    public T8PaintDefinition getFillPaintDefinition()
    {
        return (T8PaintDefinition)getDefinitionDatum(Datum.FILL_PAINT_DEFINITION.toString());
    }
    
    public void setFillPaintDefinition(T8PaintDefinition definition)
    {
        setDefinitionDatum(Datum.FILL_PAINT_DEFINITION.toString(), definition);
    }
    
    public T8PaintDefinition getBorderPaintDefinition()
    {
        return (T8PaintDefinition)getDefinitionDatum(Datum.BORDER_PAINT_DEFINITION.toString());
    }
    
    public void setBorderPaintDefinition(T8PaintDefinition definition)
    {
        setDefinitionDatum(Datum.BORDER_PAINT_DEFINITION.toString(), definition);
    }
    
    public int getBorderWidth()
    {
        return (Integer)getDefinitionDatum(Datum.BORDER_WIDTH.toString());
    }
    
    public void setBorderWidth(int width)
    {
        setDefinitionDatum(Datum.BORDER_WIDTH.toString(), width);
    }
    
    public Boolean isPaintStreched()
    {
        return (Boolean)getDefinitionDatum(Datum.PAINT_STRETCHED.toString());
    }
    
    public void setPaintStretched(boolean stretched)
    {
        setDefinitionDatum(Datum.PAINT_STRETCHED.toString(), stretched);
    }
    
    public List<T8PainterEffectDefinition> getAreaEffectDefinitions()
    {
        return (List<T8PainterEffectDefinition>)getDefinitionDatum(Datum.AREA_EFFECT_DEFINITIONS.toString());
    }
    
    public void setAreaEffectDefinitions(List<T8PainterEffectDefinition> definitions)
    {
        setDefinitionDatum(Datum.AREA_EFFECT_DEFINITIONS.toString(), definitions);
    }
}
