package com.pilog.t8.notification;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationFilter implements Serializable
{
    private final Set<String> includeNotificationTypes;
    private String userIdentifier;
    private boolean includeReceivedNotifications;
    private boolean includeNewNotifications;
    private NotificationOrdering notificationOrdering;
    private int pageOffset;
    private int pageSize;

    public enum NotificationOrdering
    {
        TIME_SENT_ASCENDING("$TIME_SENT", T8DataFilter.OrderMethod.ASCENDING),
        TIME_SENT_DESCENDING("$TIME_SENT", T8DataFilter.OrderMethod.DESCENDING);

        private final String fieldId;
        private final T8DataFilter.OrderMethod orderMethod;

        private NotificationOrdering(String fieldIdentifier, T8DataFilter.OrderMethod orderMethod)
        {
            this.fieldId = fieldIdentifier;
            this.orderMethod = orderMethod;
        }

        private String getFieldIdentifier(String entityId)
        {
            return entityId + fieldId;
        }
    };

    public T8NotificationFilter()
    {
        includeReceivedNotifications = false;
        includeNewNotifications = true;
        notificationOrdering = NotificationOrdering.TIME_SENT_DESCENDING;
        pageOffset = 0;
        pageSize = 25;

        this.includeNotificationTypes = new HashSet<>();
    }

    /**
     * Specifies a notification type to be inclusive based on the filter.
     * Notification types are defined by the notification identifiers.
     *
     * @param notificationIdentifier The {@code String} notification identifier
     *      defining a notification type to be included
     */
    public void addIncludedNotificationType(String notificationIdentifier)
    {
        if (notificationIdentifier == null) return;
        this.includeNotificationTypes.add(notificationIdentifier);
    }

    /**
     * Checks whether or not the specified notification identifier (which
     * defines a notification type) is included in the list of included types.
     * Exclusions only occur once there are types specified to be included. This
     * means that if the included notification type set is empty, all
     * notification types are considered to be included.
     *
     * @param notificationIdentifier The {@code String} notification identifier
     *      to check whether or not included
     *
     * @return {@code true} if the specified identifier is in the inclusive set,
     *      or if no included notification types have been specified.
     *      {@code false} if included notification types have been specified and
     *      the specified identifier is not part of them
     */
    public boolean isNotificationTypeIncluded(String notificationIdentifier)
    {
        //We only start excluding types when there are actually types populated
        if (this.includeNotificationTypes.isEmpty()) return true;
        else return this.includeNotificationTypes.contains(notificationIdentifier);
    }

    public void clearIncludedNotificationTypes()
    {
        this.includeNotificationTypes.clear();
    }

    /**
     * Specifies whether or not old notifications are included. The default if
     * not set is {@code false}.
     *
     * @return {@code true} if old notifications are included. {@code false}
     *      otherwise
     */
    public boolean isIncludeReceivedNotifications()
    {
        return includeReceivedNotifications;
    }

    /**
     * Specifies whether or not old notifications should be included. The
     * default is not set is {@code false}.
     *
     * @param includeOldNotifications {@code true} to include old notifications
     */
    public void setIncludeReceivedNotifications(boolean includeOldNotifications)
    {
        this.includeReceivedNotifications = includeOldNotifications;
    }

    /**
     * Specifies whether or not new notifications are included. The default if
     * not set is {@code true}.
     *
     * @return {@code true} if new notifications are included. {@code false} if
     *      not
     */
    public boolean isIncludeNewNotifications()
    {
        return includeNewNotifications;
    }

    /**
     * Specifies whether or not new notifications should be included. The
     * default if not set is {@code true}.
     *
     * @param incudeNewNotifications {@code true} to include new notifications
     */
    public void setIncludeNewNotifications(boolean incudeNewNotifications)
    {
        this.includeNewNotifications = incudeNewNotifications;
    }

    public NotificationOrdering getNotificationOrdering()
    {
        return notificationOrdering;
    }

    public void setTaskOrdering(NotificationOrdering notificationOrdering)
    {
        this.notificationOrdering = notificationOrdering;
    }

    public int getPageOffset()
    {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset)
    {
        this.pageOffset = pageOffset;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userIdentifier = userIdentifier;
    }

    public T8DataFilter getDataFilter(T8Context context, String entityId)
    {
        T8DataFilterCriteria filterCriteria;
        T8DataFilter filter;

        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityId, filterCriteria);

        filter.addFieldOrdering(this.notificationOrdering.getFieldIdentifier(entityId), this.notificationOrdering.orderMethod);

        if (this.includeNewNotifications && this.includeReceivedNotifications) {/*Do nothing, cause we want everything.*/}
        else if (this.includeNewNotifications) filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId+"$STATUS", T8DataFilterCriterion.DataFilterOperator.EQUAL, T8Notification.NotificationStatus.SENT.toString());
        else if (this.includeReceivedNotifications) filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId+"$STATUS", T8DataFilterCriterion.DataFilterOperator.EQUAL, T8Notification.NotificationStatus.RECEIVED.toString());

        if (!Strings.isNullOrEmpty(this.userIdentifier))
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId+"$USER_ID", T8DataFilterCriterion.DataFilterOperator.EQUAL, this.userIdentifier);
        }

        if (!this.includeNotificationTypes.isEmpty())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId+"$NOTIFICATION_ID", T8DataFilterCriterion.DataFilterOperator.IN, this.includeNotificationTypes);
        }

        return filter;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;
        toStringBuilder = new StringBuilder("T8NotificationFilter{");
        toStringBuilder.append("includeNotificationTypes=").append(this.includeNotificationTypes);
        toStringBuilder.append(",userIdentifier=").append(this.userIdentifier);
        toStringBuilder.append(",includeOldNotifications=").append(this.includeReceivedNotifications);
        toStringBuilder.append(",includeNewNotifications=").append(this.includeNewNotifications);
        toStringBuilder.append(",notificationOrdering=").append(this.notificationOrdering);
        toStringBuilder.append(",pageOffset=").append(this.pageOffset);
        toStringBuilder.append(",pageSize=").append(this.pageSize);
        toStringBuilder.append('}');
        return toStringBuilder.toString();
    }
}
