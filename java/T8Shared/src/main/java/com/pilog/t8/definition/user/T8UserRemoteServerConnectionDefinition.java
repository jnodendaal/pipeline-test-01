package com.pilog.t8.definition.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.remote.server.connection.T8RemoteConnectionUser;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Gavin Boshoff
 */
public class T8UserRemoteServerConnectionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_T8_USER_REMOTE_SERVER_CONNECTION";
    public static final String TYPE_IDENTIFIER = "@DT_T8_USER_REMOTE_SERVER_CONNECTION";
    public static final String DISPLAY_NAME = "Remote Server Connection";
    public static final String DESCRIPTION = "A Definition specifying the remote connection and remote user for this user";
    public static final String IDENTIFIER_PREFIX = "RSC_";
    public enum Datum {REMOTE_SERVER_CONNECTION_IDENTIFIER,
                       REMOTE_SERVER_USER_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public T8UserRemoteServerConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {}

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString(), "Remote Server Connection Identifier", "The identifier of the remote server connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REMOTE_SERVER_USER_IDENTIFIER.toString(), "Remote Server User Identifier", "The identifier of the user that will be used when login in to the remote server."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ConnectionDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.REMOTE_SERVER_USER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            if (!Strings.isNullOrEmpty(getRemoteServerConnectionIdentifier()))
            {
                T8ConnectionDefinition remoteServerConnectionDefinition;
                List<String> remoteUserIdentifiers;

                remoteServerConnectionDefinition = (T8ConnectionDefinition) definitionContext.getRawDefinition(getRootProjectId(), getRemoteServerConnectionIdentifier());
                if (remoteServerConnectionDefinition.getRemoteUserDefinitions() != null)
                {
                    remoteUserIdentifiers = remoteServerConnectionDefinition.getRemoteUserDefinitions().stream().map(T8RemoteConnectionUser::getPublicIdentifier).collect(Collectors.toCollection(ArrayList::new));
                }
                else remoteUserIdentifiers = new ArrayList<>();

                return createStringOptions(remoteUserIdentifiers);
            }
            else return new ArrayList<>();
        }
        else return null;
    }

    public String getRemoteServerConnectionIdentifier()
    {
        return getDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER);
    }

    public void setRemoteServerConnectionIdentifier(String remoteServerConnectionIdentifier)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER, remoteServerConnectionIdentifier);
    }

    public String getRemoteServerUserIdentifier()
    {
        return getDefinitionDatum(Datum.REMOTE_SERVER_USER_IDENTIFIER);
    }

    public void setRemoteServerUserIdentifier(String remoteServerUserIdentifier)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_USER_IDENTIFIER, remoteServerUserIdentifier);
    }
}
