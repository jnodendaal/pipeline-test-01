package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NgComponentFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_NG_COMPONENT";
    public static final String DISPLAY_NAME = "Angular Component Functionality";
    public static final String DESCRIPTION = "A functionality that provides access to an Angular HTML component.";
    public static final String IDENTIFIER_PREFIX = "FNC_NG_";
    public enum Datum
    {
        COMPONENT_ID,
        INPUT_PARAMETER_MAPPING,
        INPUT_PARAMETER_EXPRESSION_MAP
    };
    // -------- Definition Meta-Data -------- //

    public T8NgComponentFunctionalityDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.COMPONENT_ID.toString(), "Component Id", "The identifier of the Angular component linked to this functionality."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Functionality Input to Component Input", "A mapping of functionality input parameters to the corresponding input parameters of the component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), "Input Parameter Expression Map", "The parameters the will be sent to the component as inputs."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.COMPONENT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8NgComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8NgComponentDefinition componentDefinition;
            String projectId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            projectId = getRootProjectId();

            componentDefinition = (T8NgComponentDefinition)definitionContext.getRawDefinition(projectId, getComponentId());
            if (componentDefinition != null)
            {
                List<T8DataParameterDefinition> componentInputParameterDefinitions;
                List<T8DataParameterDefinition> fuctionalityParameterDefinitions;
                HashMap<String, List<String>> identifierMap;

                componentInputParameterDefinitions = componentDefinition.getInputParameterDefinitions();
                fuctionalityParameterDefinitions = getInputParameterDefinitions();

                identifierMap = new HashMap<String, List<String>>();
                if ((componentInputParameterDefinitions != null) && (fuctionalityParameterDefinitions != null))
                {
                    for (T8DataParameterDefinition functionalityParameterDefinition : fuctionalityParameterDefinitions)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<String>();
                        for (T8DataParameterDefinition componentInputParameterDefinition : componentInputParameterDefinitions)
                        {
                            identifierList.add(componentInputParameterDefinition.getPublicIdentifier());
                        }

                        identifierMap.put(functionalityParameterDefinition.getPublicIdentifier(), identifierList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId))
        {
            if (Strings.isNullOrEmpty(getComponentId()))
            {
                return new ArrayList<T8DefinitionDatumOption>(0);
            }
            else
            {
                Map<String, List<String>> identifiers;
                ArrayList<T8DefinitionDatumOption> optionList;
                T8NgComponentDefinition componentDefinition;

                identifiers = new HashMap<String, List<String>>();
                optionList = new ArrayList<T8DefinitionDatumOption>(1);
                componentDefinition = (T8NgComponentDefinition)definitionContext.getRawDefinition(getRootProjectId(), getComponentId());
                for (T8DataParameterDefinition parameterDefinition : componentDefinition.getInputParameterDefinitions())
                {
                    identifiers.put(parameterDefinition.getPublicIdentifier(), null);
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifiers));
                return optionList;
            }
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if ((Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumId));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8NgComponentFunctionalityInstance").getConstructor(T8Context.class, T8NgComponentFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8NgComponentFunctionalityInstance").getConstructor(T8Context.class, T8NgComponentFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }

    public String getComponentId()
    {
        return (String)getDefinitionDatum(Datum.COMPONENT_ID.toString());
    }

    public void setComponentId(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.COMPONENT_ID.toString(), moduleIdentifier);
    }

    public Map<String, String> getInputParameterExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setInputParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), expressionMap);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
