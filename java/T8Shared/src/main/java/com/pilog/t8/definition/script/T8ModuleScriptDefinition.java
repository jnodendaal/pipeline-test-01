package com.pilog.t8.definition.script;

import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleScriptDefinition extends T8ComponentControllerScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_MODULE";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_MODULE";
    public static final String IDENTIFIER_PREFIX = "SM_";
    public static final String DISPLAY_NAME = "Module Script";
    public static final String DESCRIPTION = "A script that is executed within the context of a UI module.";
    // -------- Definition Meta-Data -------- //

    public T8ModuleScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8ComponentControllerScriptCompletionHandler(context, this);
    }
}
