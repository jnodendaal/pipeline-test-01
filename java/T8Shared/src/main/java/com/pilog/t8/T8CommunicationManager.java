package com.pilog.t8;

import com.pilog.t8.communication.event.T8CommunicationListener;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationManager extends T8ServerModule
{
    /**
     * Adds a communication listener to the manager.
     * @param listener
     */
    public void addCommunicationListener(T8CommunicationListener listener);

    /**
     * Removes a communication listener from the manager.
     * @param listener
     */
    public void removeCommunicationListener(T8CommunicationListener listener);

    /**
     * Sends the specified communication using the supplied input
     * parameters.
     * @param context The session from which the communication is sent.
     * @param communicationId The identifier of the communication to send.
     * @param communicationParameters The input parameter to use when compiling the communication.
     * @throws Exception
     */
    public void sendCommunication(T8Context context, String communicationId, Map<String, Object> communicationParameters) throws Exception;

    /**
     * Queues the specified communication using the supplied input parameters.  Messages will be sent asynchronously on a priority basis as soon as possible.
     * @param context The session from which the communication is sent.
     * @param communicationId The identifier of the communication to queue.
     * @param communicationParameters The input parameter to use when compiling the communication.
     * @throws Exception
     */
    public void queueCommunication(T8Context context, String communicationId, Map<String, Object> communicationParameters) throws Exception;

    /**
     * Shuts down the specified service and reinitializes it with the latest definition.
     * @param serviceId The id of the service to restart.
     * @throws java.lang.Exception
     */
    public void restartService(String serviceId) throws Exception;

    /**
     * This method will return the number of all messages currently queued but not yet sent on the specified service.
     * @param serviceId The id of the service for which to count queued messages.  If null, the total count across all services will be returned.
     * @return An unmodifiable list containing all of the communication messages
     * @throws java.lang.Exception
     */
    public int getQueuedMessageCount(String serviceId) throws Exception;

    /**
     * Removes the specified communication from the queue
     * @param context The session Context of the canceling user
     * @param messageIid The messageInstanceIdentifier of the message to cancel
     * @throws Exception
     */
    public void cancelMessage(T8Context context, String messageIid) throws Exception;
}
