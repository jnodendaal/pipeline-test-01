package com.pilog.t8.definition.communication;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationManagerResource implements T8DefinitionResource
{
    // Operation identifiers.
    public static final String OPERATION_SEND_COMMUNICATION = "@OS_SEND_COMMUNICATION";
    public static final String OPERATION_QUEUE_COMMUNICATION = "@OS_QUEUE_COMMUNICATION";
    public static final String OPERATION_GET_QUEUED_MESSAGE_COUNT = "@OS_GET_QUEUED_MESSAGE_COUNT";
    public static final String OPERATION_CANCEL_COMMUNICATION_MESSAGE = "@OS_CANCEL_COMMUNICATION_MESSAGE";

    // Parameter identifiers.
    public static final String PARAMETER_COMMUNICATION_IDENTIFIER = "$P_COMMUNICATION_IDENTIFIER";
    public static final String PARAMETER_COMMUNICATION_DEFINITION = "$P_COMMUNICATION_DEFINITION";
    public static final String PARAMETER_COMMUNICATION_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_MESSAGE_COUNT = "$P_MESSAGE_COUNT";
    public static final String PARAMETER_SERVICE_ID = "$P_SERVICE_ID";
    public static final String PARAMETER_MESSAGE_INSTANCE_IDENTIFIER = "$P_MESSAGE_INSTANCE_IDENTIFIER";

    // Entity & Source Identifiers.
    public static final String COMMUNICATION_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_COMMUNICATION";
    public static final String COMMUNICATION_HISTORY_DE_IDENTIFIER = "@E_HISTORY_COMMUNICATION";
    public static final String COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER = "@DS_HISTORY_COMMUNICATION_PAR";
    public static final String COMMUNICATION_HISTORY_PAR_DE_IDENTIFIER = "@E_HISTORY_COMMUNICATION_PAR";
    public static final String MESSAGE_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_MESSAGE";
    public static final String MESSAGE_HISTORY_DE_IDENTIFIER = "@E_HISTORY_MESSAGE";
    public static final String MESSAGE_STATE_DS_IDENTIFIER = "@DS_STATE_COMM_MESSAGE";
    public static final String MESSAGE_STATE_DE_IDENTIFIER = "@E_STATE_COMM_MESSAGE";
    public static final String MESSAGE_STATE_PAR_DS_IDENTIFIER = "@DS_STATE_COMM_MESSAGE_PAR";
    public static final String MESSAGE_STATE_PAR_DE_IDENTIFIER = "@E_STATE_COMM_MESSAGE_PAR";

    // Table Names.
    public static final String TABLE_HISTORY_COMMUNICATION = "HIS_COM";
    public static final String TABLE_HISTORY_COMMUNICATION_PAR = "HIS_COM_PAR";
    public static final String TABLE_HISTORY_MESSAGE = "HIS_COM_MSG";
    public static final String TABLE_COMMUNICATION_MESSAGE = "COM_MSG";
    public static final String TABLE_COMMUNICATION_MESSAGE_PAR = "COM_MSG_PAR";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Communication History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(COMMUNICATION_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_COMMUNICATION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMMUNICATION_IDENTIFIER", "COMMUNICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMMUNICATION_INSTANCE_IDENTIFIER", "COMMUNICATION_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_USER_IDENTIFIER", "INITIATOR_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_USER_PROFILE_IDENTIFIER", "INITIATOR_USER_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT", "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.DATE_TIME));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_PARAMETERS", "EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Communication History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_COMMUNICATION_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_ID", "PARAMETER_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ID", "IDENTIFIER", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Message History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(MESSAGE_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_MESSAGE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMMUNICATION_IDENTIFIER", "COMMUNICATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMMUNICATION_INSTANCE_IDENTIFIER", "COMMUNICATION_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_IDENTIFIER", "MESSAGE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_INSTANCE_IDENTIFIER", "MESSAGE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECIPIENT_IDENTIFIER", "RECIPIENT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECIPIENT_RECORD_ID", "RECIPIENT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECIPIENT_DISPLAY_NAME", "RECIPIENT_DISPLAY_NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECIPIENT_ADDRESS", "RECIPIENT_ADDRESS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_SUBJECT", "MESSAGE_SUBJECT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_CONTENT", "MESSAGE_CONTENT", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT", "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Message State Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(MESSAGE_STATE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_COMMUNICATION_MESSAGE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_ID", "MESSAGE_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_IID", "MESSAGE_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMMUNICATION_ID", "COMMUNICATION_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMMUNICATION_IID", "COMMUNICATION_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SERVICE_ID", "SERVICE_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_ID", "SENDER_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SENDER_IID", "SENDER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECIPIENT_DISPLAY_NAME", "RECIPIENT_DISPLAY_NAME", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECIPIENT_ADDRESS", "RECIPIENT_ADDRESS", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_SUBJECT", "MESSAGE_SUBJECT", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_CONTENT", "MESSAGE_CONTENT", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_PRIORITY", "MESSAGE_PRIORITY", false, true, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FAILURE_COUNT", "FAILURE_COUNT", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME_QUEUED", "TIME_QUEUED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Message State Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(MESSAGE_STATE_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_COMMUNICATION_MESSAGE_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MESSAGE_IID", "MESSAGE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_IID", "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARENT_PARAMETER_IID", "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE", "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_KEY", "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CHARACTER_DATA", "CHARACTER_DATA", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BINARY_DATA", "BINARY_DATA", false, false, T8DataType.BYTE_ARRAY));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Communication History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(COMMUNICATION_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(COMMUNICATION_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$EVENT_INSTANCE_IDENTIFIER", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMMUNICATION_IDENTIFIER", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$COMMUNICATION_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMMUNICATION_INSTANCE_IDENTIFIER", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$COMMUNICATION_INSTANCE_IDENTIFIER", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INITIATOR_USER_IDENTIFIER", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$INITIATOR_USER_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INITIATOR_USER_PROFILE_IDENTIFIER", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$INITIATOR_USER_PROFILE_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$EVENT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$TIME", false, false, T8DataType.DATE_TIME));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_PARAMETERS", COMMUNICATION_HISTORY_DS_IDENTIFIER + "$EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        definitions.add(entityDefinition);

        // Communication History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(COMMUNICATION_HISTORY_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER + "$EVENT_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_ID", COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER + "$PARAMETER_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ID", COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER + "$ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", COMMUNICATION_HISTORY_PAR_DS_IDENTIFIER + "$VALUE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Message History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(MESSAGE_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(MESSAGE_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", MESSAGE_HISTORY_DS_IDENTIFIER + "$EVENT_INSTANCE_IDENTIFIER", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMMUNICATION_IDENTIFIER", MESSAGE_HISTORY_DS_IDENTIFIER + "$COMMUNICATION_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMMUNICATION_INSTANCE_IDENTIFIER", MESSAGE_HISTORY_DS_IDENTIFIER + "$COMMUNICATION_INSTANCE_IDENTIFIER", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_IDENTIFIER", MESSAGE_HISTORY_DS_IDENTIFIER + "$MESSAGE_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_INSTANCE_IDENTIFIER", MESSAGE_HISTORY_DS_IDENTIFIER + "$MESSAGE_INSTANCE_IDENTIFIER", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECIPIENT_IDENTIFIER", MESSAGE_HISTORY_DS_IDENTIFIER + "$RECIPIENT_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECIPIENT_RECORD_ID", MESSAGE_HISTORY_DS_IDENTIFIER + "$RECIPIENT_RECORD_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECIPIENT_DISPLAY_NAME", MESSAGE_HISTORY_DS_IDENTIFIER + "$RECIPIENT_DISPLAY_NAME", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECIPIENT_ADDRESS", MESSAGE_HISTORY_DS_IDENTIFIER + "$RECIPIENT_ADDRESS", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_SUBJECT", MESSAGE_HISTORY_DS_IDENTIFIER + "$MESSAGE_SUBJECT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_CONTENT", MESSAGE_HISTORY_DS_IDENTIFIER + "$MESSAGE_CONTENT", false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT", MESSAGE_HISTORY_DS_IDENTIFIER + "$EVENT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME", MESSAGE_HISTORY_DS_IDENTIFIER + "$TIME", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // Message State Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(MESSAGE_STATE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(MESSAGE_STATE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_ID", MESSAGE_STATE_DS_IDENTIFIER + "$MESSAGE_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_IID", MESSAGE_STATE_DS_IDENTIFIER + "$MESSAGE_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMMUNICATION_ID", MESSAGE_STATE_DS_IDENTIFIER + "$COMMUNICATION_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMMUNICATION_IID", MESSAGE_STATE_DS_IDENTIFIER + "$COMMUNICATION_IID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SERVICE_ID", MESSAGE_STATE_DS_IDENTIFIER + "$SERVICE_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_ID", MESSAGE_STATE_DS_IDENTIFIER + "$SENDER_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SENDER_IID", MESSAGE_STATE_DS_IDENTIFIER + "$SENDER_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECIPIENT_DISPLAY_NAME", MESSAGE_STATE_DS_IDENTIFIER + "$RECIPIENT_DISPLAY_NAME", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECIPIENT_ADDRESS", MESSAGE_STATE_DS_IDENTIFIER + "$RECIPIENT_ADDRESS", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_SUBJECT", MESSAGE_STATE_DS_IDENTIFIER + "$MESSAGE_SUBJECT", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_CONTENT", MESSAGE_STATE_DS_IDENTIFIER + "$MESSAGE_CONTENT", false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_PRIORITY", MESSAGE_STATE_DS_IDENTIFIER + "$MESSAGE_PRIORITY", false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FAILURE_COUNT", MESSAGE_STATE_DS_IDENTIFIER + "$FAILURE_COUNT", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME_QUEUED", MESSAGE_STATE_DS_IDENTIFIER + "$TIME_QUEUED", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // Message State Parameter Data Source.
        entityDefinition = new T8DataEntityResourceDefinition(MESSAGE_STATE_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(MESSAGE_STATE_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MESSAGE_IID", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$MESSAGE_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_IID", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$PARAMETER_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARENT_PARAMETER_IID", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$TYPE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_KEY", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$PARAMETER_KEY", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$SEQUENCE", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CHARACTER_DATA", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$CHARACTER_DATA", false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BINARY_DATA", MESSAGE_STATE_PAR_DS_IDENTIFIER + "$BINARY_DATA", false, false, T8DataType.BYTE_ARRAY));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_SEND_COMMUNICATION);
        definition.setMetaDisplayName("Send Communication");
        definition.setMetaDescription("Sends the specified communication using the supplied input parameters.");
        definition.setClassName("com.pilog.t8.communication.T8CommunicationOperations$SendCommunicationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMUNICATION_IDENTIFIER, "Communication Identifier", "The Identifier for the communication to be sent.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMUNICATION_PARAMETERS, "Communication Parameters", "The parameters to be used by the specified communication.", T8DataType.MAP));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_QUEUE_COMMUNICATION);
        definition.setMetaDisplayName("Queue Communication");
        definition.setMetaDescription("Queues the specified communication using the supplied input parameters.  Messages will be sent asynchronously on a priority basis as soon as possible.");
        definition.setClassName("com.pilog.t8.communication.T8CommunicationOperations$QueueCommunicationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMUNICATION_IDENTIFIER, "Communication Identifier", "The Identifier for the communication to be queued.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMUNICATION_PARAMETERS, "Communication Parameters", "The parameters to be used by the specified communication.", T8DataType.MAP));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_QUEUED_MESSAGE_COUNT);
        definition.setMetaDisplayName("Get Queued Message Count");
        definition.setMetaDescription("This operation will return the number of all messages currently queued but not yet sent on the specified service.");
        definition.setClassName("com.pilog.t8.communication.T8CommunicationOperations$GetQueuedCommunicationsOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SERVICE_ID, "Service Id", "The id of the service for which to count queued messages.  If null, the total count across all services will be returned.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE_COUNT, "Message Count", "The number of messages queued.", T8DataType.INTEGER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CANCEL_COMMUNICATION_MESSAGE);
        definition.setMetaDisplayName("Cancel Communication Message");
        definition.setMetaDescription("Cancels the specified communication using the supplied input parameters.");
        definition.setClassName("com.pilog.t8.communication.T8CommunicationOperations$CancelCommunicationMessageOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE_INSTANCE_IDENTIFIER, "Message Instance Identifier", "The instance identifier of the message to cancel", T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(definition);

        return definitions;
    }
}
