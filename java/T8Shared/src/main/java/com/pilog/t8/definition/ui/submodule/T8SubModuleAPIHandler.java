package com.pilog.t8.definition.ui.submodule;

import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8SubModule and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are 
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8SubModuleAPIHandler
{
    public static ArrayList<T8ComponentEventDefinition> events;
    public static ArrayList<T8ComponentOperationDefinition> operations;

    // These should NOT be changed, only added to.
    public static final String OPERATION_UNLOAD_MODULE = "$CO_UNLOAD_MODULE";
    public static final String OPERATION_RELOAD_MODULE = "$CO_RELOAD_MODULE";
    
    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_RELOAD_MODULE);
        newOperationDefinition.setMetaDisplayName("Reload Module");
        newOperationDefinition.setMetaDescription("This operations reloads the sub-module.");
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_UNLOAD_MODULE);
        newOperationDefinition.setMetaDisplayName("Unload Module");
        newOperationDefinition.setMetaDescription("This operations unloads the currently loaded sub-module (if any is loaded) and clears are used by the module.");
        operations.add(newOperationDefinition);
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}
