package com.pilog.t8.definition.flow.task.object;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectFieldDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TaskObjectConstructorScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_TASK_OBJECT_CONSTRUCTOR_SCRIPT";
    public static final String GROUP_IDENTIFIER = "@DG_TASK_OBJECT_CONSTRUCTOR_SCRIPT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Task Object Constructor Script";
    public static final String DESCRIPTION = "A script that is executed in order to compile a collection of workflow task list field values.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_TASK_EVENT_ID = "$P_TASK_EVENT_ID";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_TASK_STATE = "$P_TASK_STATE";
    public static final String PARAMETER_TASK_DEFINITION = "$P_TASK_DEFINITION";

    public T8TaskObjectConstructorScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8TaskObjectDefinition viewDefinition;

        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_TASK_STATE, "Task State", "The state of the task that is the target of this object.", T8DataType.CUSTOM_OBJECT));
        definitions.add(new T8DataParameterDefinition(PARAMETER_TASK_DEFINITION, "Task Definition", "The definition of the task that is the target of this object.", T8DataType.DEFINITION));
        definitions.add(new T8DataParameterDefinition(PARAMETER_TASK_EVENT_ID, "Task Event Id", "The event that triggered the refresh of the task list.", T8DataType.STRING));
        definitions.add(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Langauge Id", "The id of the language to be used when generating field values.", T8DataType.GUID));

        viewDefinition = (T8TaskObjectDefinition)getParentDefinition();
        if (viewDefinition != null)
        {
            definitions.addAll(viewDefinition.getInputParameterDefinitions());
        }

        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8TaskObjectDefinition objectDefinition;

        definitions = new ArrayList<T8DataParameterDefinition>();
        objectDefinition = (T8TaskObjectDefinition)getParentDefinition();
        if (objectDefinition != null)
        {
            for (T8DataObjectFieldDefinition fieldDefinition : objectDefinition.getFieldDefinitions())
            {
                T8DataParameterDefinition parameterDefinition;

                parameterDefinition = new T8DataParameterDefinition(fieldDefinition.getIdentifier());
                parameterDefinition.setMetaDisplayName(fieldDefinition.getMetaDisplayName());
                parameterDefinition.setMetaDescription(fieldDefinition.getMetaDescription());
                parameterDefinition.setDataType(fieldDefinition.getDataType());
                definitions.add(parameterDefinition);
            }
        }

        return definitions;
    }
}
