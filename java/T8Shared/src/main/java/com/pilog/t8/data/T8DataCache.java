package com.pilog.t8.data;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DataCache
{
    public boolean cachesData(String dataIdentifier);
    public Object recacheData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception;
    public Object getCachedData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception;
    
    public boolean isInitialized();
    public void init() throws Exception;
    public void clear();
    public void destroy();
}
