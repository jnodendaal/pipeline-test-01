package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DtList extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@LIST";

    private final T8DataType elementDataType;

    public T8DtList(T8DataType elementDataType)
    {
        this.elementDataType = elementDataType;
    }

    public T8DtList(T8DefinitionManager context)
    {
        this(T8DataType.UNDEFINED);
    }

    public T8DtList(T8DefinitionManager context, T8DataType elementDataType)
    {
        this.elementDataType = elementDataType;
    }

    public T8DtList(T8DefinitionManager context, String stringRepresentation)
    {
        if (stringRepresentation.startsWith(IDENTIFIER))
        {
            String elementStringRepresentation;
            int openBracketIndex;
            int closeBracketIndex;

            // Get the indices of the delimiters.
            openBracketIndex = stringRepresentation.indexOf('<');
            closeBracketIndex = stringRepresentation.lastIndexOf('>');

            // Get the required substring.
            elementStringRepresentation = stringRepresentation.substring(openBracketIndex + 1, closeBracketIndex);

            // Construct the element type.
            elementDataType = context.createDataType(elementStringRepresentation);
        }
        else throw new RuntimeException("Cannot parse @LIST data type from string representation: " + stringRepresentation);
    }

    public T8DataType getElementDataType()
    {
        return elementDataType;
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER + "<" + elementDataType.getDataTypeStringRepresentation(includeAttributes) + ">";
    }

    @Override
    public String getDataTypeClassName()
    {
        return ArrayList.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        List list;

        list = (List)object;
        if (list != null)
        {
            JsonArray jsonArray;

            jsonArray = new JsonArray();
            for (Object listElement : list)
            {
                jsonArray.add(elementDataType.serialize(listElement));
            }

            return jsonArray;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            ArrayList list;

            list = new ArrayList();
            for (JsonValue value : ((JsonArray)jsonValue))
            {
                if (value == null) list.add(value);
                else list.add(elementDataType.deserialize(value));
            }

            return list;
        }
        else return null;
    }
}
