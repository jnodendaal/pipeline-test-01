package com.pilog.t8.definition.communication;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8CommunicationServiceDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_COMMUNICATION_SERVICES";
    public static final String GROUP_NAME = "Communication Services";
    public static final String GROUP_DESCRIPTION = "Services maintained by the system communication manager and that are employed when communications are sent.  Each service is responsible for specific communication types.";
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_SERVICE";
    public static final String STORAGE_PATH = "/communication";
    public static final String DISPLAY_NAME = "Communication Service";
    public static final String DESCRIPTION = "Communication Service";
    public static final String IDENTIFIER_PREFIX = "COMM_SERV_";
    public static final String VERSION = "0";
    public enum Datum
    {
        ACTIVE,
        SEND_AMOUNT,
        QUEUE_POLLING_INTERVAL,
        MAX_RETRIES_ALLOWED
    };
    // -------- Definition Meta-Data -------- //

    public T8CommunicationServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "Sets this communication service to active/inactive."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.SEND_AMOUNT.toString(), "Send Amount", "The maximum amount of communications that are allowed to be sent at one time. -1 = All"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.QUEUE_POLLING_INTERVAL.toString(), "Queue Polling Interval", "The interval (in seconds) at which the message queue will be polled for messages to be sent.", 30));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MAX_RETRIES_ALLOWED.toString(), "Maximum Retries Allowed", "The maximum number of times that a message can be retried before being failed.", 1));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8CommunicationService getServiceInstance(T8Context context, T8CommunicationManager manager);
    public abstract T8CommunicationType getType();

    public boolean isActive()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.ACTIVE));
    }

    public void setActive(boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE, active);
    }

    public void setSendAmount(Integer sendAmount)
    {
        setDefinitionDatum(Datum.SEND_AMOUNT, sendAmount);
    }

    public Integer getSendAmount()
    {
        return getDefinitionDatum(Datum.SEND_AMOUNT);
    }

    public void setQueuePollingInterval(Integer sendInterval)
    {
        setDefinitionDatum(Datum.QUEUE_POLLING_INTERVAL, sendInterval);
    }

    public Integer getQueuePollingInterval()
    {
        return getDefinitionDatum(Datum.QUEUE_POLLING_INTERVAL);
    }

    public void setMaxRetriesAllowed(int maxRetries)
    {
        setDefinitionDatum(Datum.MAX_RETRIES_ALLOWED, maxRetries);
    }

    public int getMaxRetriesAllowed()
    {
        return getDefinitionDatum(Datum.MAX_RETRIES_ALLOWED);
    }
}
