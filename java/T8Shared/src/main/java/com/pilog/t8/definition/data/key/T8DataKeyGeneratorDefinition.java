package com.pilog.t8.definition.data.key;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataKeyGenerator;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataKeyGeneratorDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_KEY_GENERATOR";
    public static final String GROUP_NAME = "Data Key Generators";
    public static final String DISPLAY_NAME = "Data Key Generator";
    public static final String DESCRIPTION = "A definition specifying the characteristics of a key generation algorithm.";
    public static final String STORAGE_PATH = "/data_key_generators";
    public static final String IDENTIFIER_PREFIX = "KEY_";
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8DataKeyGeneratorDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DataKeyGenerator createNewDataKeyGenerator(T8Context context) throws Exception;
}
