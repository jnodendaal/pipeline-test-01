package com.pilog.t8.definition;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.filter.T8DataFilterClauseDefinition;
import com.pilog.t8.definition.editor.T8DefinitionEditorSetupDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTypeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_TYPE";
    public static final String GROUP_NAME = "Definition Types";
    public static final String GROUP_DESCRIPTION = "Configurations applicable to types of definitions in the system.";
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_TYPE";
    public static final String DISPLAY_NAME = "Definition Type";
    public static final String DESCRIPTION = "An object that defines a definition type..";
    public static final String STORAGE_PATH = "/definition_types";
    public static final String VERSION = "0";
    public enum Datum {DATUM_DEFINITIONS, DEFINITION_EDITOR_SETUP_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DEFINITION_EDITOR_SETUP_IDENTIFIER.toString(), "Definition Editor", "The editor to use for editing of this definition type."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATUM_DEFINITIONS.toString(), "Datums", "The specific datums defined for this type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATUM_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterClauseDefinition.GROUP_IDENTIFIER));
        else if (Datum.DEFINITION_EDITOR_SETUP_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DefinitionEditorSetupDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getDefinitionEditorSetupIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DEFINITION_EDITOR_SETUP_IDENTIFIER.toString());
    }

    public void setDefinitionEditorSetupIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DEFINITION_EDITOR_SETUP_IDENTIFIER.toString(), identifier);
    }
}
