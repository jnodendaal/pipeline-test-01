package com.pilog.t8.definition.data.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityRelationshipDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ENTITY_RELATIONSHIP";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_ENTITY_RELATIONSHIP";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Entity Relationship";
    public static final String DESCRIPTION = "An object that defines the relationship between two types of data entities.";
    public static final String VERSION = "0";
    public enum Datum {CONTEXT_ENTITY_IDENTIFIER,
                       RELATED_ENTITY_IDENTIFIER,
                       FIELD_RELATIONSHIPS};
    // -------- Definition Meta-Data -------- //

    public enum EntityRelationType {PARENT, CHILD};

    public T8DataEntityRelationshipDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.CONTEXT_ENTITY_IDENTIFIER.toString(), "Context Entity", "The context entity from which the relationship is defined."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.RELATED_ENTITY_IDENTIFIER.toString(), "Related Entity", "The entity that is related to the context entity."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.FIELD_RELATIONSHIPS.toString(), "Field Relationships", "A mapping of the context entity field identifiers to the corresponding fields of the related entity."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONTEXT_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.RELATED_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_RELATIONSHIPS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String contextEntityId;
            String relatedEntityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            contextEntityId = getContextEntityIdentifier();
            relatedEntityId = getRelatedEntityIdentifier();
            if ((relatedEntityId != null) && (contextEntityId != null))
            {
                T8DataEntityDefinition contextEntityDefinition;
                T8DataEntityDefinition relatedEntityDefinition;

                contextEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), contextEntityId);
                relatedEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), relatedEntityId);

                if ((contextEntityDefinition != null) && (relatedEntityDefinition != null))
                {
                    List<T8DataEntityFieldDefinition> contextFieldDefinitions;
                    List<T8DataEntityFieldDefinition> relatedFieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    contextFieldDefinitions = contextEntityDefinition.getFieldDefinitions();
                    relatedFieldDefinitions = relatedEntityDefinition.getFieldDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((contextFieldDefinitions != null) && (relatedFieldDefinitions != null))
                    {
                        for (T8DataFieldDefinition contextFieldDefinition : contextFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataFieldDefinition relatedFieldDefinition : relatedFieldDefinitions)
                            {
                                identifierList.add(relatedFieldDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(contextFieldDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getRelatedEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RELATED_ENTITY_IDENTIFIER.toString());
    }

    public void setRelatedEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.RELATED_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getContextEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTEXT_ENTITY_IDENTIFIER.toString());
    }

    public void setContextEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONTEXT_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getFieldRelationships()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.FIELD_RELATIONSHIPS.toString());
    }

    public void setFieldRelationships(Map<String, String> relationships)
    {
        setDefinitionDatum(Datum.FIELD_RELATIONSHIPS.toString(), relationships);
    }
}
