package com.pilog.t8.definition.gfx.paint;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionEditor;
import java.awt.Color;
import java.awt.MultipleGradientPaint.CycleMethod;
import java.awt.Paint;
import java.awt.RadialGradientPaint;
import java.awt.geom.Point2D;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author Bouwer du Preez
 */
public class T8RadialGradientPaintDefinition extends T8PaintDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINT_RADIAL_GRADIENT";
    public static final String DISPLAY_NAME = "Radial Gradient Paint";
    public static final String DESCRIPTION = "A paint that draws a radial gradient color scale.  The gradient is defined by a collection of colors, each of which is assigned a corresponding fraction of the paint range.";
    public enum Datum {CENTER_X,
                       CENTER_Y,
                       FOCUS_X,
                       FOCUS_Y,
                       RADIUS,
                       CYCLE_METHOD,
                       FRACTIONS,
                       COLORS};
    // -------- Definition Meta-Data -------- //

    public T8RadialGradientPaintDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.CENTER_X.toString(), "Center X", "The x-coordinate of the center point of the radial gradient.", 0f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.CENTER_Y.toString(), "Center Y", "The y-coordinate of the center point of the radial gradient.", 0f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.FOCUS_X.toString(), "Focus X", "The x-coordinate of the focus point of the radial gradient.", 1f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.FOCUS_Y.toString(), "Focus Y", "The y-coordinate of the focus point of the radial gradient.", 0f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.RADIUS.toString(), "Radius", "The radius of the radial gradient.", 50f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CYCLE_METHOD.toString(), "Cycle Method", "The method by which the gradient will be repeated when additional space is available.", CycleMethod.NO_CYCLE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.FLOAT), Datum.FRACTIONS.toString(), "Fractions", "The points on the gradient radius where color changes occur."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.COLOR_HEX), Datum.COLORS.toString(), "Colors", "The colors of the gradient scale, cooresponding to the fractions when color changes occur."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CYCLE_METHOD.toString().equals(datumIdentifier)) return createStringOptions(CycleMethod.values());
        else return null;
    }

    @Override
    public T8DefinitionEditor getDefinitionEditor(T8DefinitionContext definitionContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.developer.definitions.gfx.paint.T8RadialGradientPaintDefinitionEditor").getConstructor(T8DefinitionContext.class, this.getClass());
        return (T8DefinitionEditor)constructor.newInstance(definitionContext, this);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public Paint getNewPaintInstance()
    {
        return new RadialGradientPaint(getCenterX(), getCenterY(), getRadius(), getFocusX(), getFocusY(), getFractions(), getColors(), getCycleMethod());
    }

    public void setPaint(RadialGradientPaint paint)
    {
        if (paint != null)
        {
            Point2D centerPoint;
            Point2D focusPoint;

            centerPoint = paint.getCenterPoint();
            focusPoint = paint.getFocusPoint();

            setCenterX((float)centerPoint.getX());
            setCenterY((float)centerPoint.getY());
            setFocusX((float)focusPoint.getX());
            setFocusY((float)focusPoint.getY());
            setRadius(paint.getRadius());
            setFractions(paint.getFractions());
            setColors(paint.getColors());
            setCycleMethod(paint.getCycleMethod());
        }
    }

    public float getCenterX()
    {
        return (Float)getDefinitionDatum(Datum.CENTER_X.toString());
    }

    public void setCenterX(float centerX)
    {
        setDefinitionDatum(Datum.CENTER_X.toString(), centerX);
    }

    public float getCenterY()
    {
        return (Float)getDefinitionDatum(Datum.CENTER_Y.toString());
    }

    public void setCenterY(float centerY)
    {
        setDefinitionDatum(Datum.CENTER_Y.toString(), centerY);
    }

    public float getFocusX()
    {
        return (Float)getDefinitionDatum(Datum.FOCUS_X.toString());
    }

    public void setFocusX(float endX)
    {
        setDefinitionDatum(Datum.FOCUS_X.toString(), endX);
    }

    public float getFocusY()
    {
        return (Float)getDefinitionDatum(Datum.FOCUS_Y.toString());
    }

    public void setFocusY(float focusY)
    {
        setDefinitionDatum(Datum.FOCUS_Y.toString(), focusY);
    }

    public float getRadius()
    {
        return (Float)getDefinitionDatum(Datum.RADIUS.toString());
    }

    public void setRadius(float radius)
    {
        setDefinitionDatum(Datum.RADIUS.toString(), radius);
    }

    public CycleMethod getCycleMethod()
    {
        return CycleMethod.valueOf((String)getDefinitionDatum(Datum.CYCLE_METHOD.toString()));
    }

    public void setCycleMethod(CycleMethod cycleMethod)
    {
        setDefinitionDatum(Datum.CYCLE_METHOD.toString(), cycleMethod.toString());
    }

    public float[] getFractions()
    {
        List<Float> fractionList;
        float[] fractions;

        fractionList = (List<Float>)getDefinitionDatum(Datum.FRACTIONS.toString());
        if (fractionList != null)
        {
            fractions = new float[fractionList.size()];
            for (int fractionIndex = 0; fractionIndex < fractionList.size(); fractionIndex++)
            {
                fractions[fractionIndex] = fractionList.get(fractionIndex);
            }

            return fractions;
        }
        else
        {
            return new float[]{0f, 1f};
        }
    }

    public void setFractions(float[] fractions)
    {
        ArrayList<Float> fractionList;

        fractionList = new ArrayList<Float>();
        for (float fraction : fractions) fractionList.add(fraction);
        setDefinitionDatum(Datum.FRACTIONS.toString(), fractionList);
    }

    public Color[] getColors()
    {
        List<String> colorHexList;
        Color[] colors;

        colorHexList = (List<String>)getDefinitionDatum(Datum.COLORS.toString());
        if (colorHexList != null)
        {
            colors = new Color[colorHexList.size()];
            for (int colorIndex = 0; colorIndex < colorHexList.size(); colorIndex++)
            {
                colors[colorIndex] = T8DefinitionDatumUtilities.getColor(colorHexList.get(colorIndex));
            }

            return colors;
        }
        else
        {
            return new Color[]{Color.WHITE, Color.BLACK};
        }
    }

    public void setColors(Color[] colors)
    {
        ArrayList<String> colorHexList;

        colorHexList = new ArrayList<String>();
        if (colors != null)
        {
            for (Color color : colors)
            {
                colorHexList.add(Integer.toHexString(color.getRGB()));
            }
        }

        setDefinitionDatum(Datum.COLORS.toString(), colorHexList);
    }
}
