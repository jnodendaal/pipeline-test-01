package com.pilog.t8.ui.functionality.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityInstanceListener extends EventListener
{
    public void functionalityEnded(T8FunctionalityInstanceEndedEvent event);
}
