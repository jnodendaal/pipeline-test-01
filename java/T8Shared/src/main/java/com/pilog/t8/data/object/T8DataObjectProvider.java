package com.pilog.t8.data.object;

/**
 * @author Bouwer du
 */
public interface T8DataObjectProvider
{
    public T8DataObject getDataObject(String dataObjectId, String dataObjectIid, boolean includeState);
    public T8DataObjectState getDataObjectState(String dataObjectIid);
}
