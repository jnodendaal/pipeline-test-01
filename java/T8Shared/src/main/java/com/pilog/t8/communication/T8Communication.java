package com.pilog.t8.communication;

import com.pilog.t8.data.T8DataTransaction;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8Communication
{
    // History event in the lifecycle of a communication.
    public enum CommunicationHistoryEvent
    {
        SENT,
        QUEUED,
        FAILED
    }

    /**
     * Returns the identifier of the communication setup from which this
     * communication was created.
     * @return The identifier of the communication setup from which this
     * communication was created.
     */
    public String getIdentifier();

    /**
     * Returns the instance identifier of this communication setup.
     * @return The instance identifier of this communication setup.
     */
    public String getInstanceIdentifier();

    /**
     * Initializes this communication using the supplied parameters as context.
     * @param tx The data transaction to user for construction of the communication messages.
     * @param communicationParameters The Input Parameters that will be used to determine the message template and communication recipients.
     */
    public void init(T8DataTransaction tx, Map<String,Object> communicationParameters);

    /**
     * Destroys this communication, releasing all acquired resources.
     */
    public void destroy();

    /**
     * Returns the next generated message or null if no more messages are available.
     * @return The next generated message or null if no more messages are available.
     * @throws Exception
     */
    public T8CommunicationMessage generateNextMessage() throws Exception;
}
