package com.pilog.t8.definition.functionality.access;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.functionality.access.T8FunctionalityAccessBlocker;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8StandardFunctionalityAccessBlockerDefinition extends T8FunctionalityAccessBlockerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_ACCESS_BLOCKER_STANDARD";
    public static final String DISPLAY_NAME = "Standard Functionality Access Blocker";
    public static final String DESCRIPTION = "A standard functionality access blocker.";
    public enum Datum {BLOCKING_FUNCTIONALITY_IDENTIFIER,
                       BLOCK_ON_DATA_OBJECT,
                       BLOCK_ON_USER,
                       BLOCK_ON_USER_PROFILE,
                       BLOCKED_FUNCTIONALITY_IDENTIFIERS,
                       BLOCK_MESSAGE};
    // -------- Definition Meta-Data -------- //

    public T8StandardFunctionalityAccessBlockerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BLOCKING_FUNCTIONALITY_IDENTIFIER.toString(), "Blocking Functionality", "The functionality that will trigger this blocker when accessed."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.BLOCKED_FUNCTIONALITY_IDENTIFIERS.toString(), "Blocked Functionalities", "The list of functionalities that will be blocked when this blocker is activated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.BLOCK_ON_DATA_OBJECT.toString(), "Block on Data Object", "If set to true, blocking will be applied in context of the data object.  If false, blocking of functionality regardless of data object is applied."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.BLOCK_ON_USER.toString(), "Block on User", "If set to true, blocking will be applied in context of the user.  If false, blocking of functionality regardless of user is applied."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.BLOCK_ON_USER_PROFILE.toString(), "Block on User Profile", "If set to true, blocking will be applied in context of the user profile.  If false, blocking of functionality regardless of user profile is applied."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BLOCK_MESSAGE.toString(), "Blocked Message", "The user message that will be displayed to indicate why the functionalities are blocked."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BLOCKING_FUNCTIONALITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.BLOCKED_FUNCTIONALITY_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FunctionalityAccessBlocker createNewAccessBlockerInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.access.T8StandardFunctionalityAccessBlocker").getConstructor(T8Context.class, T8StandardFunctionalityAccessBlockerDefinition.class);
        return (T8FunctionalityAccessBlocker)constructor.newInstance(context, this);
    }

    public String getBlockingFunctionalityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BLOCKING_FUNCTIONALITY_IDENTIFIER.toString());
    }

    public void setBlockingFunctionalityIdentifier(String functionalityIdentifier)
    {
        setDefinitionDatum(Datum.BLOCKING_FUNCTIONALITY_IDENTIFIER.toString(), functionalityIdentifier);
    }

    public List<String> getBlockedFunctionalityIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.BLOCKED_FUNCTIONALITY_IDENTIFIERS.toString());
    }

    public void setBlockedFunctionalityIdentifiers(List<String> functionalityIdentifiers)
    {
        setDefinitionDatum(Datum.BLOCKED_FUNCTIONALITY_IDENTIFIERS.toString(), functionalityIdentifiers);
    }

    public boolean isBlockOnDataObject()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.BLOCK_ON_DATA_OBJECT.toString());
        return value != null ? value : false;
    }

    public void setBlockOnDataObject(boolean blockOnDataObject)
    {
        setDefinitionDatum(Datum.BLOCK_ON_DATA_OBJECT.toString(), blockOnDataObject);
    }

    public boolean isBlockOnUser()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.BLOCK_ON_USER.toString());
        return value != null ? value : false;
    }

    public void setBlockOnUser(boolean blockOnUser)
    {
        setDefinitionDatum(Datum.BLOCK_ON_USER.toString(), blockOnUser);
    }

    public boolean isBlockOnUserProfile()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.BLOCK_ON_USER_PROFILE.toString());
        return value != null ? value : false;
    }

    public void setBlockOnUserProfile(boolean blockOnUserProfile)
    {
        setDefinitionDatum(Datum.BLOCK_ON_USER_PROFILE.toString(), blockOnUserProfile);
    }

    public String getBlockMessage()
    {
        return (String)getDefinitionDatum(Datum.BLOCK_MESSAGE.toString());
    }

    public void setBlockMessage(String message)
    {
        setDefinitionDatum(Datum.BLOCK_MESSAGE.toString(), message);
    }
}
