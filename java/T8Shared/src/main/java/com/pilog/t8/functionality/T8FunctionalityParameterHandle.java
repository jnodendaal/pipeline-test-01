package com.pilog.t8.functionality;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityParameterHandle implements Serializable
{
    private String parameterId;
    private String dataObjectId;
    private boolean required;

    public T8FunctionalityParameterHandle()
    {
        this.required = false;
    }

    public T8FunctionalityParameterHandle(String parameterId, String dataObjectId, boolean required)
    {
        this.parameterId = parameterId;
        this.dataObjectId = dataObjectId;
        this.required = required;
    }

    public String getParameterId()
    {
        return parameterId;
    }

    public void setParameterId(String parameterId)
    {
        this.parameterId = parameterId;
    }

    public String getDataObjectId()
    {
        return dataObjectId;
    }

    public void setDataObjectId(String dataObjectId)
    {
        this.dataObjectId = dataObjectId;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }
}
