package com.pilog.t8.definition.security.authentication;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8AuthenticationHandler;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AuthenticationHandlerDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_AUTHENTICATION_HANDLER";
    public static final String GROUP_NAME = "Authentication Handlers";
    public static final String GROUP_DESCRIPTION = "Server-side security handlers that may be used to authenticate client requests.";
    public static final String DISPLAY_NAME = "Authentication Handler";
    public static final String DESCRIPTION = "A definition of a server-side security handler that may be used to authenticate client requests.";
    public static final String IDENTIFIER_PREFIX = "AUTH_";
    public static final String STORAGE_PATH = "/authentication_handlers";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8AuthenticationHandlerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8AuthenticationHandler createAuthenticationHandler(T8ServerContext serverContext) throws Exception;
}
