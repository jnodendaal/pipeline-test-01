package com.pilog.t8.data.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityValidationReport implements Serializable
{
    private String entityGUID;
    private boolean validationSuccess;
    private List<T8DataEntityValidationError> validationErrors;

    public T8DataEntityValidationReport(String entityGUID, boolean validationSuccess, List<T8DataEntityValidationError> validationErrors)
    {
        this.entityGUID = entityGUID;
        this.validationSuccess = validationSuccess;
        this.validationErrors = validationErrors;
    }

    public boolean isValidationSuccess()
    {
        return validationSuccess;
    }

    public void setValidationSuccess(boolean validationSuccess)
    {
        this.validationSuccess = validationSuccess;
    }

    public String getEntityGUID()
    {
        return entityGUID;
    }

    public void setEntityGUID(String entityGUID)
    {
        this.entityGUID = entityGUID;
    }

    public int getErrorCount()
    {
        return this.validationErrors == null ? 0 : this.validationErrors.size();
    }

    public List<T8DataEntityValidationError> getValidationErrors()
    {
        return validationErrors;
    }

    public void setValidationErrors(List<T8DataEntityValidationError> validationErrors)
    {
        this.validationErrors = validationErrors;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8DataEntityValidationReport{");
        toStringBuilder.append("entityGUID=").append(this.entityGUID);
        toStringBuilder.append(",validationSuccess=").append(this.validationSuccess);
        toStringBuilder.append(",validationErrors=").append(this.validationErrors);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
