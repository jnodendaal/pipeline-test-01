package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.report.T8ReportDetails;
import com.pilog.t8.time.T8Timestamp;
import java.io.File;

/**
 *
 * @author Pieter Strydom
 */
public class T8DtReportDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@REPORT_DETAILS";

    public T8DtReportDetails() {}

    public T8DtReportDetails(T8DefinitionManager context) {}

    public T8DtReportDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8ReportDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject jsonReportDetails;
            T8ReportDetails reportDetails;
            T8Timestamp startTime;
            T8Timestamp endTime;
            String filePath;

            reportDetails = (T8ReportDetails)object;
            startTime = reportDetails.getStartTime();
            endTime = reportDetails.getEndTime();
            filePath = reportDetails.getFilePath();

            // Create the JSON object.
            jsonReportDetails = new JsonObject();
            jsonReportDetails.add("id", reportDetails.getId());
            jsonReportDetails.add("iid", reportDetails.getIid());
            jsonReportDetails.add("name", reportDetails.getName());
            jsonReportDetails.add("description", reportDetails.getDescription());
            jsonReportDetails.add("thumbnailImageId", reportDetails.getThumbnailImageId());
            jsonReportDetails.add("initiatorId", reportDetails.getInitiatorId());
            jsonReportDetails.add("userId", reportDetails.getUserId());
            jsonReportDetails.add("startTime", startTime != null ? startTime.getMilliseconds() : null);
            jsonReportDetails.add("endTime", endTime != null ? endTime.getMilliseconds() : null);
            jsonReportDetails.add("fileName", filePath != null ? new File(filePath).getName() : null);
            jsonReportDetails.add("filePath", filePath);
            jsonReportDetails.add("fileContextId", reportDetails.getFileContextId());

            // Return the final JSON object.
            return jsonReportDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8ReportDetails deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8ReportDetails reportDetails;
            JsonObject jsonReportDetails;
            Long startTime;
            Long endTime;

            jsonReportDetails = jsonValue.asObject();
            startTime = jsonReportDetails.getLong("startTime");
            endTime = jsonReportDetails.getLong("endTime");

            // Create the file details object.
            reportDetails = new T8ReportDetails();
            reportDetails.setProjectId(jsonReportDetails.getString("projectId"));
            reportDetails.setId(jsonReportDetails.getString("id"));
            reportDetails.setIid(jsonReportDetails.getString("iid"));
            reportDetails.setName(jsonReportDetails.getString("name"));
            reportDetails.setDescription(jsonReportDetails.getString("description"));
            reportDetails.setThumbnailImageId(jsonReportDetails.getString("thumbnailImageId"));
            reportDetails.setInitiatorId(jsonReportDetails.getString("initiatorId"));
            reportDetails.setUserId(jsonReportDetails.getString("userId"));
            reportDetails.setStartTime(startTime != null ? new T8Timestamp(startTime) : null);
            reportDetails.setEndTime(endTime != null ? new T8Timestamp(endTime) : null);
            reportDetails.setFilePath(jsonReportDetails.getString("filePath"));
            reportDetails.setFileContextId(jsonReportDetails.getString("fileContextId"));

            // Return the completed object.
            return reportDetails;
        }
        else return null;
    }
}
