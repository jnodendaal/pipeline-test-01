package com.pilog.t8.flow.state;

import com.pilog.t8.flow.state.data.T8FlowStateDataSet;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.flow.T8FlowReference;
import com.pilog.t8.flow.T8Flow.FlowStatus;
import com.pilog.t8.flow.state.data.T8NodeStateDataSet;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8FlowState implements Serializable
{
    private String projectId;
    private String flowIid;
    private String flowId;
    private FlowStatus flowStatus;
    private String initiatorId;
    private String initiatorIid;
    private String initiatorOrgId;
    private String initiatorRootOrgId;
    private T8Timestamp timeQueued;
    private T8Timestamp timeStarted;
    private final T8FlowStateParameterMap inputParameters;
    private final Map<String, T8FlowNodeState> nodeStates;
    private final Map<String, T8FlowReference> subFlowReferences;
    private final Map<String, T8FlowDataObjectState> dataObjectStates;
    private long constructionTime;
    private long constructedTime;
    private long retrievalTime;
    private long retrievedTime;
    private boolean updated;
    private boolean inserted;

    public T8FlowState(String flowIid)
    {
        this.flowIid = flowIid;
        this.dataObjectStates = new HashMap<>();
        this.nodeStates = new HashMap<String, T8FlowNodeState>();
        this.inputParameters = new T8FlowStateParameterMap(null, flowIid, null, STATE_FLOW_INPUT_PAR_DE_IDENTIFIER, null);
        this.subFlowReferences = new HashMap<String, T8FlowReference>();
        this.inserted = true;
        this.updated = false;
    }

    public T8FlowState(String flowIid, T8FlowStateDataSet dataSet)
    {
        T8DataEntity flowEntity;

        // Construct the flow state.
        flowEntity = dataSet.getFlowEntity();
        if (flowEntity == null) throw new IllegalArgumentException("Data for construction of flow state '" + flowIid + "' not found.");
        else
        {
            this.projectId = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_PROJECT_ID);
            this.flowIid = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_FLOW_IID);
            this.flowId = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_FLOW_ID);
            this.flowStatus = FlowStatus.valueOf((String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_STATUS));
            this.initiatorId = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_INITIATOR_ID);
            this.initiatorIid = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_INITIATOR_IID);
            this.initiatorOrgId = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_INITIATOR_ORG_ID);
            this.initiatorRootOrgId = (String)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_INITIATOR_ROOT_ORG_ID);
            this.timeStarted = (T8Timestamp)flowEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER + F_TIME_STARTED);
            this.inserted = false;
            this.updated = false;

            // Construct the node states.
            this.nodeStates = new HashMap<String, T8FlowNodeState>();
            for (T8NodeStateDataSet nodeStateDataSet : dataSet.getNodeStateDataSets())
            {
                addNodeState(new T8FlowNodeState(this, nodeStateDataSet));
            }

            // Construct the data object states.
            this.dataObjectStates = new HashMap<>();
            for (T8DataEntity dataObjectEntity : dataSet.getDataObjectStateEntities())
            {
                addDataObjectState(new T8FlowDataObjectState(this, dataObjectEntity));
            }

            // Construct sub-flow references.
            this.subFlowReferences = new HashMap<String, T8FlowReference>();

            // Construct the input parameters.
            this.inputParameters = new T8FlowStateParameterMap(null, flowIid, null, STATE_FLOW_INPUT_PAR_DE_IDENTIFIER, dataSet.getInputParameterDataSet());
        }
    }

    public synchronized T8DataEntity createEntity(T8DataEntityDefinition entityDefinition)
    {
        String entityId;
        T8DataEntity newEntity;

        entityId = entityDefinition.getIdentifier();
        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityId + F_PROJECT_ID, projectId);
        newEntity.setFieldValue(entityId + F_FLOW_IID, flowIid);
        newEntity.setFieldValue(entityId + F_FLOW_ID, flowId);
        newEntity.setFieldValue(entityId + F_STATUS, flowStatus.toString());
        newEntity.setFieldValue(entityId + F_INITIATOR_ID, initiatorId);
        newEntity.setFieldValue(entityId + F_INITIATOR_IID, initiatorIid);
        newEntity.setFieldValue(entityId + F_INITIATOR_ORG_ID, initiatorOrgId);
        newEntity.setFieldValue(entityId + F_INITIATOR_ROOT_ORG_ID, initiatorRootOrgId);
        newEntity.setFieldValue(entityId + F_TIME_STARTED, timeStarted);
        return newEntity;
    }

    public boolean isUpdated()
    {
        return updated;
    }

    public synchronized void statePersisted()
    {
        // Reset this state's flags.
        inserted = false;
        updated = false;

        // Reset input parameter flags.
        inputParameters.statePersisted();

        // Reset node state flags.
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            nodeState.statePersisted();
        }

        // Reset sub-flow reference flags.
        for (T8FlowReference subFlowReference : subFlowReferences.values())
        {
            subFlowReference.resetFlags();
        }

        // Reset data object state flags.
        for (T8FlowDataObjectState dataObjectState : dataObjectStates.values())
        {
            dataObjectState.statePersisted();
        }
    }

    public synchronized void addUpdates(T8FlowStateUpdates updates)
    {
        // Add this flow state's updates.
        if (inserted) updates.addInsert(createEntity(updates.getFlowStateEntityDefinition()));
        else if (updated) updates.addUpdate(createEntity(updates.getFlowStateEntityDefinition()));

        // Add all node state updates.
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            nodeState.addUpdates(updates);
        }

        // Add input parameter updates.
        inputParameters.addUpdates(updates.getFlowStateInputParameterEntityDefinition(), updates);

        // Add the data object updates.
        for (T8FlowDataObjectState dataObjectState : dataObjectStates.values())
        {
            dataObjectState.addUpdates(updates);
        }
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        if (!Objects.equals(this.projectId, projectId))
        {
            updated = true;
            this.projectId = projectId;
        }
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String flowIid)
    {
        if (!Objects.equals(this.flowIid, flowIid))
        {
            updated = true;
            this.flowIid = flowIid;
        }
    }

    public String getFlowId()
    {
        return flowId;
    }

    public void setFlowId(String flowId)
    {
        if (!Objects.equals(this.flowId, flowId))
        {
            updated = true;
            this.flowId = flowId;
        }
    }

    public FlowStatus getFlowStatus()
    {
        return flowStatus;
    }

    public void setFlowStatus(FlowStatus status)
    {
        if (!Objects.equals(this.flowStatus, status))
        {
            updated = true;
            this.flowStatus = status;
        }
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId)
    {
        if (!Objects.equals(this.initiatorId, initiatorId))
        {
            updated = true;
            this.initiatorId = initiatorId;
        }
    }

    public String getInitiatorIid()
    {
        return initiatorIid;
    }

    public void setInitiatorIid(String initiatorIid)
    {
        if (!Objects.equals(this.initiatorIid, initiatorIid))
        {
            updated = true;
            this.initiatorIid = initiatorIid;
        }
    }

    public String getInitiatorOrgId()
    {
        return initiatorOrgId;
    }

    public void setInitiatorOrgId(String initiatorOrgId)
    {
        if (!Objects.equals(this.initiatorOrgId, initiatorOrgId))
        {
            updated = true;
            this.initiatorOrgId = initiatorOrgId;
        }
    }

    public String getInitiatorRootOrgId()
    {
        return initiatorRootOrgId;
    }

    public void setInitiatorRootOrgId(String rootOrgId)
    {
        if (!Objects.equals(this.initiatorRootOrgId, rootOrgId))
        {
            updated = true;
            this.initiatorRootOrgId = rootOrgId;
        }
    }

    public long getConstructionTime()
    {
        return constructionTime;
    }

    public void setConstructionTime(long constructionTime)
    {
        this.constructionTime = constructionTime;
    }

    public long getConstructedTime()
    {
        return constructedTime;
    }

    public void setConstructedTime(long constructedTime)
    {
        this.constructedTime = constructedTime;
    }

    public long getRetrievalTime()
    {
        return retrievalTime;
    }

    public void setRetrievalTime(long retrievalTime)
    {
        this.retrievalTime = retrievalTime;
    }

    public long getRetrievedTime()
    {
        return retrievedTime;
    }

    public void setRetrievedTime(long retrievedTime)
    {
        this.retrievedTime = retrievedTime;
    }

    public T8Timestamp getTimeQueued()
    {
        return timeQueued;
    }

    public Long getTimeQueuedMilliseconds()
    {
        return timeQueued != null ? timeQueued.getMilliseconds() : null;
    }

    public void setTimeQueued(T8Timestamp timeQueued)
    {
        if (!Objects.equals(this.timeQueued, timeQueued))
        {
            updated = true;
            this.timeQueued = timeQueued;
        }
    }

    public T8Timestamp getTimeStarted()
    {
        return timeStarted;
    }

    public Long getTimeStartedMilliseconds()
    {
        return timeStarted != null ? timeStarted.getMilliseconds() : null;
    }

    public void setTimeStarted(T8Timestamp timeStarted)
    {
        if (!Objects.equals(this.timeStarted, timeStarted))
        {
            updated = true;
            this.timeStarted = timeStarted;
        }
    }

    public T8FlowStateParameterMap getInputParameters()
    {
        return inputParameters;
    }

    public void setInputParameters(Map<String, Object> inputParameters)
    {
        this.inputParameters.clear();
        if (inputParameters != null)
        {
            this.inputParameters.putAll(inputParameters);
        }
    }

    public T8FlowNodeState getNodeState(String nodeInstanceIdentifier)
    {
        return nodeStates.get(nodeInstanceIdentifier);
    }

    public List<T8FlowNodeState> getNodeStateList()
    {
        return new ArrayList<T8FlowNodeState>(nodeStates.values());
    }

    public List<T8FlowNodeState> getNodeStatesByType(String nodeIdentifier)
    {
        List<T8FlowNodeState> stateList;

        stateList = new ArrayList<T8FlowNodeState>();
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            if (nodeState.getNodeId().equals(nodeIdentifier))
            {
                stateList.add(nodeState);
            }
        }

        return stateList;
    }

    public Map<String, T8FlowNodeState> getNodeStates()
    {
        return new HashMap<String, T8FlowNodeState>(nodeStates);
    }

    public final void addNodeState(T8FlowNodeState nodeState)
    {
        nodeStates.put(nodeState.getNodeIid(), nodeState);
    }

    public List<T8FlowTaskState> getTaskStates(String userId, List<String> profileIds)
    {
        List<T8FlowTaskState> taskStates;

        taskStates = new ArrayList<>();
        for (T8FlowNodeState nodeState : getNodeStateList())
        {
            T8FlowTaskState taskState;

            // Get the task state from this node (if any).
            taskState = nodeState.getTaskState();
            if (taskState != null)
            {
                // Make sure that the task has been issued but not yet completed.
                if ((taskState.isIssued()) && (!taskState.isCompleted()))
                {
                    String restrictionUserId;

                    // Make sure the user restriction applies.
                    restrictionUserId = taskState.getRestrictionUserId();
                    if ((restrictionUserId == null) || (restrictionUserId.equals(userId)))
                    {
                        String restrictionProfileId;

                        // Make sure the profile restriction applies.
                        restrictionProfileId = taskState.getRestrictionProfileId();
                        if ((restrictionProfileId == null) || (profileIds.contains(restrictionProfileId)))
                        {
                            String claimedByUserId;

                            // Make sure the claimed-by-user applies.
                            claimedByUserId = taskState.getClaimedByUserId();
                            if ((claimedByUserId == null) || (claimedByUserId.equals(userId)))
                            {
                                taskStates.add(taskState);
                            }
                        }
                    }
                }
            }
        }

        return taskStates;
    }

    public List<T8FlowTaskState> getPrecedingTaskStates(String taskIid)
    {
        T8FlowTaskState targetState;

        targetState = getTaskState(taskIid);
        if (targetState != null)
        {
            List<T8FlowTaskState> states;
            T8Timestamp timeIssued;

            states = new ArrayList<>();
            timeIssued = targetState.getTimeIssued();
            for (T8FlowTaskState taskState : getTaskStates())
            {
                if (taskState != targetState)
                {
                    T8Timestamp timeCompleted;

                    timeCompleted = taskState.getTimeCompleted();
                    if ((timeCompleted != null) && (timeIssued.compareTo(timeCompleted) > 0))
                    {
                        states.add(taskState);
                    }
                }
            }

            return states;
        }
        else throw new IllegalArgumentException("Task state " + taskIid + " not found in flow " + flowIid);
    }

    public T8FlowTaskState getPrecedingTaskState(String taskIid)
    {
        T8FlowTaskState targetState;

        targetState = getTaskState(taskIid);
        if (targetState != null)
        {
            T8FlowTaskState precedingState;
            T8Timestamp maxTimeCompleted;
            T8Timestamp timeIssued;

            precedingState = null;
            maxTimeCompleted = null;
            timeIssued = targetState.getTimeIssued();
            for (T8FlowTaskState taskState : getTaskStates())
            {
                if (taskState != targetState)
                {
                    T8Timestamp timeCompleted;

                    timeCompleted = taskState.getTimeCompleted();
                    if ((timeCompleted != null) && (timeIssued.compareTo(timeCompleted) > 0))
                    {
                        if ((maxTimeCompleted == null) || (timeCompleted.compareTo(maxTimeCompleted) > 0))
                        {
                            maxTimeCompleted = timeCompleted;
                            precedingState = taskState;
                        }
                    }
                }
            }

            return precedingState;
        }
        else throw new IllegalArgumentException("Task state " + taskIid + " not found in flow " + flowIid);
    }

    public List<T8FlowTaskState> getTaskStates()
    {
        List<T8FlowTaskState> taskStates;

        taskStates = new ArrayList<>();
        for (T8FlowNodeState nodeState : getNodeStateList())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if (taskState != null)
            {
                taskStates.add(taskState);
            }
        }

        return taskStates;
    }

    public List<T8FlowTaskState> getTaskStatesByGroup(String groupId)
    {
        List<T8FlowTaskState> taskStates;

        taskStates = new ArrayList<>();
        for (T8FlowNodeState nodeState : getNodeStateList())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if ((taskState != null) && (groupId.equals(taskState.getGroupId())))
            {
                taskStates.add(taskState);
            }
        }

        return taskStates;
    }

    public List<T8FlowTaskState> getTaskStatesByGroupIteration(String groupId, int iteration)
    {
        List<T8FlowTaskState> taskStates;

        taskStates = new ArrayList<>();
        for (T8FlowNodeState nodeState : getNodeStateList())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if ((taskState != null) && (groupId.equals(taskState.getGroupId())) && (taskState.getGroupIteration() == iteration))
            {
                taskStates.add(taskState);
            }
        }

        return taskStates;
    }

    public T8FlowTaskState getTaskState(String taskIid)
    {
        for (T8FlowNodeState nodeState : getNodeStateList())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if ((taskState != null) && (taskState.getTaskIid().equals(taskIid)))
            {
                return taskState;
            }
        }

        return null;
    }

    public Map<String, T8FlowReference> getSubFlowReferences()
    {
        return new HashMap<String, T8FlowReference>(subFlowReferences);
    }

    public T8FlowReference removeSubFlowReference(String instanceIdentifier)
    {
        return subFlowReferences.remove(instanceIdentifier);
    }

    public void addSubFlowReference(T8FlowReference reference)
    {
        subFlowReferences.put(reference.getFlowInstanceIdentifier(), reference);
    }

    public void setSubFlowReferences(Map<String, T8FlowReference> subFlowReferences)
    {
        if (!Objects.equals(this.subFlowReferences, subFlowReferences))
        {
            updated = true;
            this.subFlowReferences.clear();
            if (subFlowReferences != null) this.subFlowReferences.putAll(subFlowReferences);
        }
    }

    public int getNodeCount()
    {
        return nodeStates.size();
    }

    public int getNodeInputParameterCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            count += nodeState.getTotalInputParameterCount();
        }

        return count;
    }

    public int getNodeExecutionParameterCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            count += nodeState.getTotalExecutionParameterCount();
        }

        return count;
    }

    public int getNodeOutputParameterCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            count += nodeState.getTotalOutputParameterCount();
        }

        return count;
    }

    public int getOutputNodeMappingCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            count += nodeState.getOutputNodeMappingCount();
        }

        return count;
    }

    public int getTaskCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if (taskState != null) count++;
        }

        return count;
    }

    public int getTaskInputParameterCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if (taskState != null) count += taskState.getTotalInputParameterCount();
        }

        return count;
    }

    public int getTaskOutputParameterCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            T8FlowTaskState taskState;

            taskState = nodeState.getTaskState();
            if (taskState != null) count += taskState.getTotalOutputParameterCount();
        }

        return count;
    }

    public int getWaitKeyCount()
    {
        int count;

        count = 0;
        for (T8FlowNodeState nodeState : nodeStates.values())
        {
            count += nodeState.getWaitKeyCount();
        }

        return count;
    }

    public boolean containsDataObjectState(String objectIid)
    {
        return dataObjectStates.containsKey(objectIid);
    }

    public T8FlowDataObjectState getDataObjectState(String objectIid)
    {
        return dataObjectStates.get(objectIid);
    }

    public List<T8FlowDataObjectState> getDataObjectStateList()
    {
        return new ArrayList<T8FlowDataObjectState>(dataObjectStates.values());
    }

    public List<T8FlowDataObjectState> getDataObjectStatesByType(String objectIdentifier)
    {
        List<T8FlowDataObjectState> stateList;

        stateList = new ArrayList<T8FlowDataObjectState>();
        for (T8FlowDataObjectState objectState : dataObjectStates.values())
        {
            if (objectState.getDataObjectId().equals(objectIdentifier))
            {
                stateList.add(objectState);
            }
        }

        return stateList;
    }

    public Map<String, T8FlowDataObjectState> getDataObjectStates()
    {
        return new HashMap<String, T8FlowDataObjectState>(dataObjectStates);
    }

    public final void addDataObjectState(T8FlowDataObjectState dataObjectState)
    {
        dataObjectStates.put(dataObjectState.getDataObjectIid(), dataObjectState);
    }

    public int getDataObjectStateCount()
    {
        return dataObjectStates.size();
    }

    public String getSummary()
    {
        StringBuilder buffer;

        buffer = new StringBuilder();
        buffer.append("T8FlowState:");
        buffer.append(flowIid);
        buffer.append("\n");
        buffer.append("Retrieval Time:");
        buffer.append(retrievalTime);
        buffer.append("ms\n");
        buffer.append("Construction Time:");
        buffer.append(constructionTime);
        buffer.append("ms\n");
        buffer.append("Flow Input Parameters:");
        buffer.append(inputParameters.getTotalParameterCount());
        buffer.append("\n");
        buffer.append("Nodes:");
        buffer.append(getNodeCount());
        buffer.append("\n");
        buffer.append("Node Input Parameters:");
        buffer.append(getNodeInputParameterCount());
        buffer.append("\n");
        buffer.append("Node Execution Parameters:");
        buffer.append(getNodeExecutionParameterCount());
        buffer.append("\n");
        buffer.append("Node Output Parameters:");
        buffer.append(getNodeOutputParameterCount());
        buffer.append("\n");
        buffer.append("Node Output Mappings:");
        buffer.append(getOutputNodeMappingCount());
        buffer.append("\n");
        buffer.append("Tasks:");
        buffer.append(getTaskCount());
        buffer.append("\n");
        buffer.append("Task Input Parameters:");
        buffer.append(getTaskInputParameterCount());
        buffer.append("\n");
        buffer.append("Task Output Parameters:");
        buffer.append(getTaskOutputParameterCount());
        buffer.append("\n");
        buffer.append("Wait Keys:");
        buffer.append(getWaitKeyCount());
        buffer.append("\n");
        buffer.append("Data Object States:");
        buffer.append(getDataObjectStateCount());
        return buffer.toString();
    }

    @Override
    public String toString()
    {
        return "T8FlowState{" + "flowInstanceIdentifier=" + flowIid + '}';
    }
}
