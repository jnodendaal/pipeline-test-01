package com.pilog.t8.definition.service;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.service.T8Service;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ServiceDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SYSTEM_SERVICE";
    public static final String GROUP_NAME = "System Services";
    public static final String GROUP_DESCRIPTION = "Configuration settings of global services provided by the system.";
    public static final String STORAGE_PATH = "/system_services";
    public static final String DISPLAY_NAME = "System Service";
    public static final String DESCRIPTION = "A server-side Java service with a predefined API.";
    public static final String IDENTIFIER_PREFIX = "SERVICE_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8ServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public abstract T8Service getNewServiceInstance(T8ServerContext serverContext);

    public abstract ArrayList<T8ServiceOperationDefinition> getOperationDefinitions();
}
