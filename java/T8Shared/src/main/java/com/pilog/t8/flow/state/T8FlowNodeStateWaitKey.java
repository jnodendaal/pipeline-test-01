package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowNodeStateWaitKey implements Serializable
{
    protected String flowIid;
    protected String nodeIid;
    protected WaitKeyIdentifier keyId;
    protected String keyProjectId;
    protected String keyFlowId;
    protected String keyFlowIid;
    protected String keyNodeId;
    protected String keyNodeIid;
    protected String keyTaskId;
    protected String keyTaskIid;
    protected String keyDataObjectId;
    protected String keyDataObjectIid;
    protected String keySignalId;
    protected String keySignalParameter;
    protected boolean inserted;
    protected boolean updated;

    public enum WaitKeyIdentifier {NODE_COMPLETION, TASK_CLAIM, TASK_COMPLETION, SIGNAL};

    public T8FlowNodeStateWaitKey(T8FlowNodeState nodeState)
    {
        this.flowIid = nodeState.getFlowIid();
        this.nodeIid = nodeState.getNodeIid();
        this.inserted = true;
        this.updated = false;
    }

    public T8FlowNodeStateWaitKey(T8DataEntity waitKeyEntity, T8FlowNodeState nodeState)
    {
        this.flowIid = (String)waitKeyEntity.getFieldValue("$FLOW_IID");
        this.nodeIid = (String)waitKeyEntity.getFieldValue("$NODE_IID");
        this.keyId = WaitKeyIdentifier.valueOf((String)waitKeyEntity.getFieldValue("$KEY_ID"));
        this.keyFlowIid = (String)waitKeyEntity.getFieldValue("$KEY_FLOW_IID");
        this.keyFlowId = (String)waitKeyEntity.getFieldValue("$KEY_FLOW_ID");
        this.keyNodeIid = (String)waitKeyEntity.getFieldValue("$KEY_NODE_IID");
        this.keyNodeId = (String)waitKeyEntity.getFieldValue("$KEY_NODE_ID");
        this.keyTaskIid = (String)waitKeyEntity.getFieldValue("$KEY_TASK_IID");
        this.keyTaskId = (String)waitKeyEntity.getFieldValue("$KEY_TASK_ID");
        this.keyDataObjectIid = (String)waitKeyEntity.getFieldValue("$KEY_DATA_OBJECT_IID");
        this.keyDataObjectId = (String)waitKeyEntity.getFieldValue("$KEY_DATA_OBJECT_ID");
        this.keySignalId = (String)waitKeyEntity.getFieldValue("$KEY_SIGNAL_ID");
        this.keySignalParameter = (String)waitKeyEntity.getFieldValue("$KEY_SIGNAL_PARAMETER");
        this.keyProjectId = (String)waitKeyEntity.getFieldValue("$KEY_PROJECT_ID");
        this.inserted = false;
        this.updated = false;
    }

    public T8DataEntity createEntity(T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;
        String entityIdentifier;

        entityIdentifier = entityDefinition.getIdentifier();
        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityIdentifier + "$FLOW_IID", flowIid);
        newEntity.setFieldValue(entityIdentifier + "$NODE_IID", nodeIid);
        newEntity.setFieldValue(entityIdentifier + "$KEY_ID", keyId.toString());
        newEntity.setFieldValue(entityIdentifier + "$KEY_FLOW_IID", keyFlowIid);
        newEntity.setFieldValue(entityIdentifier + "$KEY_FLOW_ID", keyFlowId);
        newEntity.setFieldValue(entityIdentifier + "$KEY_NODE_IID", keyNodeIid);
        newEntity.setFieldValue(entityIdentifier + "$KEY_NODE_ID", keyNodeId);
        newEntity.setFieldValue(entityIdentifier + "$KEY_TASK_IID", keyTaskIid);
        newEntity.setFieldValue(entityIdentifier + "$KEY_TASK_ID", keyTaskId);
        newEntity.setFieldValue(entityIdentifier + "$KEY_DATA_OBJECT_IID", keyDataObjectIid);
        newEntity.setFieldValue(entityIdentifier + "$KEY_DATA_OBJECT_ID", keyDataObjectId);
        newEntity.setFieldValue(entityIdentifier + "$KEY_SIGNAL_ID", keySignalId);
        newEntity.setFieldValue(entityIdentifier + "$KEY_SIGNAL_PARAMETER", keySignalParameter);
        newEntity.setFieldValue(entityIdentifier + "$KEY_PROJECT_ID", keyProjectId);
        return newEntity;
    }

    public void statePersisted()
    {
        updated = false;
        inserted = false;
    }

    public void addUpdates(T8FlowStateUpdates updates)
    {
        if (inserted)
        {
            updates.addInsert(createEntity(updates.getNodeStateWaitKeyEntityDefinition()));
        }
        else if (updated)
        {
            updates.addUpdate(createEntity(updates.getNodeStateWaitKeyEntityDefinition()));
        }
    }

    public WaitKeyIdentifier getKeyIdentifier()
    {
        return keyId;
    }

    public String getKeyProjectId()
    {
        return keyProjectId;
    }

    public void setKeyProjectId(String keyProjectId)
    {
        this.keyProjectId = keyProjectId;
    }

    public void setKeyIdentifier(WaitKeyIdentifier keyIdentifier)
    {
        this.keyId = keyIdentifier;
    }

    public String getKeyFlowIdentifier()
    {
        return keyFlowId;
    }

    public void setKeyFlowIdentifier(String keyFlowIdentifier)
    {
        this.keyFlowId = keyFlowIdentifier;
    }

    public String getKeyFlowInstanceIdentifier()
    {
        return keyFlowIid;
    }

    public void setKeyFlowInstanceIdentifier(String keyFlowInstanceIdentifier)
    {
        this.keyFlowIid = keyFlowInstanceIdentifier;
    }

    public String getKeyNodeIdentifier()
    {
        return keyNodeId;
    }

    public void setKeyNodeIdentifier(String keyNodeIdentifier)
    {
        this.keyNodeId = keyNodeIdentifier;
    }

    public String getKeyNodeInstanceIdentifier()
    {
        return keyNodeIid;
    }

    public void setKeyNodeInstanceIdentifier(String keyNodeInstanceIdentifier)
    {
        this.keyNodeIid = keyNodeInstanceIdentifier;
    }

    public String getKeyTaskIdentifier()
    {
        return keyTaskId;
    }

    public void setKeyTaskIdentifier(String keyTaskIdentifier)
    {
        this.keyTaskId = keyTaskIdentifier;
    }

    public String getKeyTaskInstanceIdentifier()
    {
        return keyTaskIid;
    }

    public void setKeyTaskInstanceIdentifier(String keyTaskInstanceIdentifier)
    {
        this.keyTaskIid = keyTaskInstanceIdentifier;
    }

    public String getKeyDataObjectIdentifier()
    {
        return keyDataObjectId;
    }

    public void setKeyDataObjectIdentifier(String keyDataObjectIdentifier)
    {
        this.keyDataObjectId = keyDataObjectIdentifier;
    }

    public String getKeyDataObjectInstanceIdentifier()
    {
        return keyDataObjectIid;
    }

    public void setKeyDataObjectInstanceIdentifier(String keyDataObjectInstanceIdentifier)
    {
        this.keyDataObjectIid = keyDataObjectInstanceIdentifier;
    }

    public String getKeySignalIdentifier()
    {
        return keySignalId;
    }

    public void setKeySignalIdentifier(String keySignalIdentifier)
    {
        this.keySignalId = keySignalIdentifier;
    }

    public String getKeySignalParameter()
    {
        return keySignalParameter;
    }

    public void setKeySignalParameter(String signalParameter)
    {
        this.keySignalParameter = signalParameter;
    }
}
