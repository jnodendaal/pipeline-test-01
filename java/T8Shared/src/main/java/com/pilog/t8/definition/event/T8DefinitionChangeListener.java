package com.pilog.t8.definition.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionChangeListener extends EventListener
{
    public void metaDataChanged(T8DefinitionMetaDataChangeEvent event);
    public void descendentMetaDataChanged(T8DefinitionDescendentMetaDataChangeEvent event);
    public void datumChanged(T8DefinitionDatumChangeEvent event);
    public void descendentDatumChanged(T8DefinitionDescendentDatumChangeEvent event);
}
