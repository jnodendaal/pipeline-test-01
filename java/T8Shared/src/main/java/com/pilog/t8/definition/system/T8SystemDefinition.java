package com.pilog.t8.definition.system;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessPolicyDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.language.T8LanguageDefinition;
import com.pilog.t8.definition.security.sso.T8SingleSignOnHandlerDefinition;
import com.pilog.t8.definition.ui.laf.T8LookAndFeelDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Bouwer du Preez
 */
public class T8SystemDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM";
    public static final String GROUP_IDENTIFIER = "@DG_SYSTEM";
    public static final String STORAGE_PATH = "/systems";
    public static final String DISPLAY_NAME = "T8 System Definition";
    public static final String DESCRIPTION = "A definition used by the T8 system for internal configuration and setup.";
    public static final String IDENTIFIER_PREFIX = "SYSTEM_";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    public enum Datum
    {
        SYSTEM_DISPLAY_NAME,
        EXTERNAL_BINDING,
        PRODUCT_CONFIGURATION_ICON_IDENTIFIER,
        PRODUCT_DEVELOPER_ICON_IDENTIFIER,
        PRODUCT_CLIENT_ICON_IDENTIFIER,
        PRODUCT_SPLASH_ICON_IDENTIFIER,
        LANGUAGE_IDENTIFIERS,
        LOOK_AND_FEEL_IDENTIFIER,
        SSO_HANDLER_ID,
        FUNCTIONALITY_ACCESS_POLICY_IDENTIFIER,
        USER_DATA_FILE_CONTEXT_ID,
        REPORT_FILE_CONTEXT_ID,
        SESSION_TIMEOUT,
        DEFAULT_CONNECTION_IDENTIFIER,
        TERMINOLOGY_CACHE_SIZE,
        DATA_REQUIREMENT_CACHE_SIZE,
        FLOW_MANAGER_ENABLED,
        COMMUNICATION_MANAGER_ENABLED,
        PROCESS_MANAGER_ENABLED,
        REPORT_MANAGER_ENABLED
    };
    // -------- Definition Meta-Data -------- //

    public static final String DEFAULT_CONNECTION_IDENTIFIER = "@DATA_CONNECTION_MAIN";
    public static final String DEFAULT_PRODUCT_ICON_IDENTIFIER = "@ICN_DEFAULT_INSTALL";
    public static final String DEFAULT_PRODUCT_SPLASH_ICON_IDENTIFIER = "@ICN_DEFAULT_SPLASH";

    public T8SystemDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SYSTEM_DISPLAY_NAME.toString(), "Display Name",  "The name of this system that will be used for all display purposes on the UI, communication, etc."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.EXTERNAL_BINDING.toString(), "External Binding",  "The Base URL which defines the connection url for the application to be added to the JNLP's as part of the context path.", "http://localhost"));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.PRODUCT_DEVELOPER_ICON_IDENTIFIER.toString(), "Developer Install Icon Identifier", "The identifier of the icon for the desktop icon to be created for the developer application."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.PRODUCT_CLIENT_ICON_IDENTIFIER.toString(), "Client Install Icon Identifier", "The name of the icon for the desktop icon to be created for the client application."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.PRODUCT_CONFIGURATION_ICON_IDENTIFIER.toString(), "Configuration Install Icon Identifier", "The name of the icon for the desktop icon to be created for the configuration application."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.PRODUCT_SPLASH_ICON_IDENTIFIER.toString(), "Product Splash Icon Identifier", "The name of the icon for the splash screen displayed during application jnlp launch."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.LANGUAGE_IDENTIFIERS.toString(), "Languages",  "The languages supported by the system."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.LOOK_AND_FEEL_IDENTIFIER.toString(), "Look and Feel", "The Look and Feel to use for this system."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.SSO_HANDLER_ID.toString(), "Single-Sign-On", "The single-sign-on handler to be used by clients."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.FUNCTIONALITY_ACCESS_POLICY_IDENTIFIER.toString(), "Functionality Access Policy", "The functionality access policy used by the system."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.USER_DATA_FILE_CONTEXT_ID.toString(), "User Data File Context", "The file context where user data such as preferences, profile pictures etc. are stored."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.REPORT_FILE_CONTEXT_ID.toString(), "Report File Context", "The file context where generated reports are be stored."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DEFAULT_CONNECTION_IDENTIFIER.toString(), "Default Connection", "The default connection to use for this system."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.LONG, Datum.SESSION_TIMEOUT.toString(), "Session Timeout(ms)",  "The default session timout in ms. Minimum is 1 minute", TimeUnit.MINUTES.toMillis(30)));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.TERMINOLOGY_CACHE_SIZE.toString(), "Terminology Cache Size",  "The maximum number of entries to store in the system-wide terminology cache.", 100_000));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.DATA_REQUIREMENT_CACHE_SIZE.toString(), "Data Requirement Cache Size",  "The maximum number of entries to store in the system-wide Data Requirement cache.", 1_000));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FLOW_MANAGER_ENABLED.toString(), "Workflow Manager Enabled",  "The master setting to enabled or disable workflow services in the system.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COMMUNICATION_MANAGER_ENABLED.toString(), "Communication Manager Enabled",  "The master setting to enabled or disable communication services in the system.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.PROCESS_MANAGER_ENABLED.toString(), "Process Manager Enabled",  "The master setting to enabled or disable long running process services in the system.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REPORT_MANAGER_ENABLED.toString(), "Report Manager Enabled",  "The master setting to enabled or disable report generation and maintenance services in the system.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.LANGUAGE_IDENTIFIERS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8LanguageDefinition.GROUP_IDENTIFIER));
        else if (Datum.PRODUCT_DEVELOPER_ICON_IDENTIFIER.toString().equals(datumId)
                || Datum.PRODUCT_CLIENT_ICON_IDENTIFIER.toString().equals(datumId)
                || Datum.PRODUCT_CONFIGURATION_ICON_IDENTIFIER.toString().equals(datumId)
                || Datum.PRODUCT_SPLASH_ICON_IDENTIFIER.toString().equals(datumId))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), false, DEFAULT_PRODUCT_ICON_IDENTIFIER);
        }
        else if (Datum.LOOK_AND_FEEL_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8LookAndFeelDefinition.GROUP_IDENTIFIER));
        else if (Datum.SSO_HANDLER_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8SingleSignOnHandlerDefinition.GROUP_IDENTIFIER), true, "SSO Disabled");
        else if (Datum.FUNCTIONALITY_ACCESS_POLICY_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityAccessPolicyDefinition.GROUP_IDENTIFIER), true, "No Access Policy");
        else if (Datum.USER_DATA_FILE_CONTEXT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FileContextDefinition.GROUP_IDENTIFIER), true, "No File Context");
        else if (Datum.REPORT_FILE_CONTEXT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FileContextDefinition.GROUP_IDENTIFIER), true, "No File Context");
        else if (Datum.DEFAULT_CONNECTION_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER), true, "No Default Connection");
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getSystemDisplayName()
    {
        return getDefinitionDatum(Datum.SYSTEM_DISPLAY_NAME);
    }

    public void setSystemDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.SYSTEM_DISPLAY_NAME, displayName);
    }

    public String getExternalBinding()
    {
        return getDefinitionDatum(Datum.EXTERNAL_BINDING);
    }

    public void setExternalBinding(String externalBinding)
    {
        setDefinitionDatum(Datum.EXTERNAL_BINDING, externalBinding);
    }

    public String getProductClientIconId()
    {
        return (getDefinitionDatum(Datum.PRODUCT_CLIENT_ICON_IDENTIFIER) == null ? DEFAULT_PRODUCT_ICON_IDENTIFIER : getDefinitionDatum(Datum.PRODUCT_CLIENT_ICON_IDENTIFIER));
    }

    public void setProductClientIconId(String iconId)
    {
        setDefinitionDatum(Datum.PRODUCT_CLIENT_ICON_IDENTIFIER, iconId);
    }

    public String getProductDeveloperIconId()
    {
        return (getDefinitionDatum(Datum.PRODUCT_DEVELOPER_ICON_IDENTIFIER) == null ? DEFAULT_PRODUCT_ICON_IDENTIFIER : getDefinitionDatum(Datum.PRODUCT_DEVELOPER_ICON_IDENTIFIER));
    }

    public void setProductDeveloperIconId(String iconId)
    {
        setDefinitionDatum(Datum.PRODUCT_DEVELOPER_ICON_IDENTIFIER, iconId);
    }

    public String getProductConfigurationIconId()
    {
        return (getDefinitionDatum(Datum.PRODUCT_CONFIGURATION_ICON_IDENTIFIER) == null ? DEFAULT_PRODUCT_ICON_IDENTIFIER : getDefinitionDatum(Datum.PRODUCT_CONFIGURATION_ICON_IDENTIFIER));
    }

    public void setProductConfigurationIconId(String iconId)
    {
        setDefinitionDatum(Datum.PRODUCT_CONFIGURATION_ICON_IDENTIFIER, iconId);
    }

    public String getProductSplashIconId()
    {
        return (getDefinitionDatum(Datum.PRODUCT_SPLASH_ICON_IDENTIFIER) == null ? DEFAULT_PRODUCT_ICON_IDENTIFIER : getDefinitionDatum(Datum.PRODUCT_SPLASH_ICON_IDENTIFIER));
    }

    public void setProductSplashIconId(String iconId)
    {
        setDefinitionDatum(Datum.PRODUCT_SPLASH_ICON_IDENTIFIER, iconId);
    }

    public List<String> getLanguageIds()
    {
        return getDefinitionDatum(Datum.LANGUAGE_IDENTIFIERS);
    }

    public void setLanguageIds(List<String> languageIds)
    {
        setDefinitionDatum(Datum.LANGUAGE_IDENTIFIERS, languageIds);
    }

    public String getLookAndFeelId()
    {
        return getDefinitionDatum(Datum.LOOK_AND_FEEL_IDENTIFIER);
    }

    public void setLookAndFeelId(String lafId)
    {
        setDefinitionDatum(Datum.LOOK_AND_FEEL_IDENTIFIER, lafId);
    }

    public String getSsoHandlerId()
    {
        return getDefinitionDatum(Datum.SSO_HANDLER_ID);
    }

    public void setSsoHandlerId(String id)
    {
        setDefinitionDatum(Datum.SSO_HANDLER_ID, id);
    }

    public String getFunctionalityAccessPolicyId()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_ACCESS_POLICY_IDENTIFIER);
    }

    public void setFunctionalityAccessPolicyId(String policyId)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_ACCESS_POLICY_IDENTIFIER, policyId);
    }

    public String getUserDataFileContextId()
    {
        return getDefinitionDatum(Datum.USER_DATA_FILE_CONTEXT_ID);
    }

    public void setUserDataFileContextId(String contextId)
    {
        setDefinitionDatum(Datum.USER_DATA_FILE_CONTEXT_ID, contextId);
    }

    public String getReportFileContextId()
    {
        return getDefinitionDatum(Datum.REPORT_FILE_CONTEXT_ID);
    }

    public void setReportFileContextId(String contextId)
    {
        setDefinitionDatum(Datum.REPORT_FILE_CONTEXT_ID, contextId);
    }

    public String getDefaultConnectionId()
    {
        String connectionId;

        connectionId = getDefinitionDatum(Datum.DEFAULT_CONNECTION_IDENTIFIER);
        return connectionId != null ? connectionId : DEFAULT_CONNECTION_IDENTIFIER;
    }

    public void setDefaultConnectionId(String conntextionId)
    {
        setDefinitionDatum(Datum.DEFAULT_CONNECTION_IDENTIFIER, conntextionId);
    }

    public Long getSessionTimeout()
    {
        return getDefinitionDatum(Datum.SESSION_TIMEOUT);
    }

    public void setSessionTimeout(Long sessionTimeout)
    {
        setDefinitionDatum(Datum.SESSION_TIMEOUT, sessionTimeout);
    }

    public int getTerminologyCacheSize()
    {
        Integer size;

        size = getDefinitionDatum(Datum.TERMINOLOGY_CACHE_SIZE);
        return size != null ? size : 1_000_000;
    }

    public void setTerminologyCacheSize(int size)
    {
        setDefinitionDatum(Datum.TERMINOLOGY_CACHE_SIZE, size);
    }

    public int getDataRequirementCacheSize()
    {
        Integer size;

        size = getDefinitionDatum(Datum.DATA_REQUIREMENT_CACHE_SIZE);
        return size != null ? size : 5_000;
    }

    public void setDataRequirementCacheSize(int size)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_CACHE_SIZE, size);
    }

    public boolean isFlowManagerEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.FLOW_MANAGER_ENABLED);
        return value == null || value;
    }

    public void setFlowManagerEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.FLOW_MANAGER_ENABLED, enabled);
    }

    public boolean isCommunicationManagerEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.COMMUNICATION_MANAGER_ENABLED);
        return value == null || value;
    }

    public void setCommunicationManagerEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.COMMUNICATION_MANAGER_ENABLED, enabled);
    }

    public boolean isProcessManagerEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.PROCESS_MANAGER_ENABLED);
        return value == null || value;
    }

    public void setProcessManagerEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.PROCESS_MANAGER_ENABLED, enabled);
    }

    public boolean isReportManagerEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.REPORT_MANAGER_ENABLED);
        return value == null || value;
    }

    public void setReportManagerEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.REPORT_MANAGER_ENABLED, enabled);
    }
}

