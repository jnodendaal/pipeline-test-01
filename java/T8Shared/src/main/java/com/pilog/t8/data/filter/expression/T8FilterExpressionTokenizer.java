package com.pilog.t8.data.filter.expression;

import com.pilog.t8.data.filter.expression.T8FilterExpressionToken.TokenType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8FilterExpressionTokenizer
{
    private final List<T8FilterExpressionToken> history;
    private final SafeStringReader reader;
    private T8FilterExpressionToken currentToken;
    private int lineNumber;
    private int index = 0;

    public T8FilterExpressionTokenizer(String expression)
    {
        this.lineNumber = 0;
        this.reader = new SafeStringReader(expression);
        this.history = new ArrayList<>();
    }

    public int getLineNumber()
    {
        return lineNumber;
    }

    public boolean moveNext()  throws T8SearchExpressionParseException
    {
        if (index < history.size())
        {
            currentToken = history.get(index);
        }
        else
        {
            currentToken = parseNextToken();
            history.add(currentToken);
        }
        index++;
        return currentToken != null;
    }

    public boolean movePrevious()
    {
        if (index > 0 && history.size() > 0)
        {
            index--;
            currentToken = history.get(index);

            return true;
        }
        else return false;
    }

    public T8FilterExpressionToken getToken()
    {
        return currentToken;
    }

    private T8FilterExpressionToken parseNextToken() throws T8SearchExpressionParseException
    {
        int readChar;

        while((readChar = reader.read()) != -1)
        {
            // If a white space is encountered, skip it.
            if (Character.isWhitespace(readChar))
            {
                if (readChar == '\n') lineNumber++;
                continue;
            }
            else
            {
                String word;

                // Check the defined token types.
                if (readChar == '(') return new T8FilterExpressionToken(TokenType.SYMBOL, '(');
                if (readChar == ')') return new T8FilterExpressionToken(TokenType.SYMBOL, ')');
                if (readChar == '=') return new T8FilterExpressionToken(TokenType.OPERATOR, '=');
                if (readChar == '<') return new T8FilterExpressionToken(TokenType.OPERATOR, '<');
                if (readChar == '>') return new T8FilterExpressionToken(TokenType.OPERATOR, '>');
                if (readChar == '%') return new T8FilterExpressionToken(TokenType.OPERATOR, '%');
                if (readChar == '!') return new T8FilterExpressionToken(TokenType.OPERATOR, '!');

                // Try and look ahead with A characters to see if it is and AND keyword.
                if (readChar == 'A')
                {
                    reader.mark(3);
                    if (reader.read() == 'N' && reader.read() == 'D') return new T8FilterExpressionToken(TokenType.KEYWORD, "AND");
                    else reader.reset(); // Reset the reader to the original position and continue checking for other matches.
                }
                // Try and look ahead with O characters to see if it as an OR keyword.
                if (readChar == 'O')
                {
                    reader.mark(2);
                    if (reader.read() == 'R') return new T8FilterExpressionToken(TokenType.KEYWORD, "OR");
                    else reader.reset(); // Reset the reader to the original position and continue checking for other matches.
                }

                // Check for string qoutes, and read all text following it as a string until the next string quote is identified.
                if (readChar == '"') return parsePhrase();

                // Skip back one so we can capture the next word.
                word =  parseWord((char)readChar);

                // If this identifier does not match any of the know identifiers report it as a string.
                if (T8IdentifierUtilities.isQualifiedLocalId(word)) return new T8FilterExpressionToken(TokenType.FIELD_IDENTIFIER, word);
                else return new T8FilterExpressionToken(TokenType.WORD, word);
            }
        }

        return null;
    }

    private String parseWord(char currentValue)
    {
        StringBuilder text;
        int character;

        character = currentValue;
        text = new StringBuilder();
        while (true)
        {
            text.append((char)character);
            reader.mark(1);
            character = reader.read();

            // Check for any valid identifier characters.
            if ((character == -1) || (Character.isWhitespace(character) || (character == '(') || (character == ')')))
            {
                // Reset the reader to the last valid position.
                reader.reset();
                break;
            }
        }

        return text.toString();
    }

    private T8FilterExpressionToken parsePhrase() throws T8SearchExpressionParseException
    {
        int character;
        StringBuilder text;

        character = reader.read();
        text = new StringBuilder();
        while (character != '"')
        {
            // If the end of the stream is reached and we haven't found the end qoute, notify the user
            if (character == -1) throw new T8SearchExpressionParseException("Ending qoute(\") expected");

            //append the next character until the end qoute is found
            text.append((char)character);
            character = reader.read();
        }

        // Advance the reader past the last quote.
        return new T8FilterExpressionToken(TokenType.PHRASE, text.toString());
    }

    private static class SafeStringReader extends StringReader
    {
        public SafeStringReader(String s)
        {
            super(s);
        }

        @Override
        public void reset()
        {
            try
            {
                super.reset();
            }
            catch (Exception e)
            {
                throw new RuntimeException("StringReader exception.", e);
            }
        }


        @Override
        public int read()
        {
            try
            {
                return super.read();
            }
            catch (Exception e)
            {
                throw new RuntimeException("StringReader exception.", e);
            }
        }

        @Override
        public void mark(int readAheadLimit)
        {
            try
            {
                super.mark(readAheadLimit);
            }
            catch (Exception e)
            {
                throw new RuntimeException("StringReader exception.", e);
            }
        }
    }
}
