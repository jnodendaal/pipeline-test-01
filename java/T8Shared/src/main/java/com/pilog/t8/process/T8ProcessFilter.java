package com.pilog.t8.process;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.pilog.t8.definition.process.T8ProcessManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessFilter implements Serializable
{
    private final Set<String> processIids; // If no identifiers are added, all types are considered to be included.
    private final Set<String> processIds; // If no identifiers are added, all types are considered to be included.
    private final Set<String> userIds;
    private final Set<String> profileIds;
    private ProcessOrdering processOrdering;
    private int pageOffset;
    private int pageSize;

    public enum ProcessOrdering
    {
        TIME_STARTED_ASCENDING("$TIME_STARTED", T8DataFilter.OrderMethod.ASCENDING),
        TIME_STARTED_DESCENDING("$TIME_STARTED", T8DataFilter.OrderMethod.DESCENDING),
        TIME_COMPLETED_ASCENDING("$TIME_COMPLETED", T8DataFilter.OrderMethod.ASCENDING),
        TIME_COMPLETED_DESCENDING("$TIME_COMPLETED", T8DataFilter.OrderMethod.DESCENDING);

        private final String fieldId;
        private final T8DataFilter.OrderMethod orderMethod;

        private ProcessOrdering(String fieldId, T8DataFilter.OrderMethod orderMethod)
        {
            this.fieldId = fieldId;
            this.orderMethod = orderMethod;
        }

        private String getFieldId(String entityId)
        {
            return entityId + fieldId;
        }
    };

    public T8ProcessFilter()
    {
        this.processIids = new HashSet<>();
        this.processIds = new HashSet<>();
        this.userIds = new HashSet<>();
        this.profileIds = new HashSet<>();
        this.processOrdering = ProcessOrdering.TIME_COMPLETED_ASCENDING;
        this.pageOffset = 0;
        this.pageSize = 0;
    }

    public T8ProcessFilter copy()
    {
        T8ProcessFilter copy;

        copy = new T8ProcessFilter();
        copy.setProcessIds(processIds);
        copy.setProcessIids(processIids);
        copy.setUserIds(userIds);
        copy.setProfileIds(profileIds);
        copy.setProcessOrdering(processOrdering);
        copy.setPageOffset(pageOffset);
        copy.setPageSize(pageSize);
        return copy;
    }

    public void clearProcessIds()
    {
        this.processIds.clear();
    }

    public void addProcessId(String processId)
    {
        this.processIds.add(processId);
    }

    public boolean isProcessIdIncluded(String processId)
    {
        return processIds.isEmpty() || processIds.contains(processId);
    }

    public Set<String> getProcessIds()
    {
        return new HashSet<>(processIds);
    }

    public void setProcessIds(Collection<String> ids)
    {
        this.processIds.clear();
        if (ids != null)
        {
            this.processIds.addAll(ids);
        }
    }

    public void clearProcessIids()
    {
        this.processIids.clear();
    }

    public void addProcessIid(String iid)
    {
        this.processIids.add(iid);
    }

    public Set<String> getProcessIids()
    {
        return new HashSet<>(processIids);
    }

    public void setProcessIids(Collection<String> iids)
    {
        this.processIids.clear();
        if (iids != null)
        {
            this.processIids.addAll(iids);
        }
    }

    public void clearUserIds()
    {
        this.userIds.clear();
    }

    public void addUserId(String id)
    {
        this.userIds.add(id);
    }

    public Set<String> getUserIds()
    {
        return new HashSet<>(userIds);
    }

    public void setUserIds(Collection<String> ids)
    {
        this.userIds.clear();
        if (ids != null)
        {
            this.userIds.addAll(ids);
        }
    }

    public void clearProfileIds()
    {
        this.profileIds.clear();
    }

    public void addProfileId(String id)
    {
        this.profileIds.add(id);
    }

    public Set<String> getProfileIds()
    {
        return new HashSet<>(profileIds);
    }

    public void setProfileIds(Collection<String> ids)
    {
        this.profileIds.clear();
        if (ids != null)
        {
            this.profileIds.addAll(ids);
        }
    }

    public ProcessOrdering getProcessOrdering()
    {
        return processOrdering;
    }

    public void setProcessOrdering(ProcessOrdering processOrdering)
    {
        this.processOrdering = processOrdering;
    }

    public int getPageOffset()
    {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset)
    {
        this.pageOffset = pageOffset;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public T8DataFilter getDataFilter(T8Context context, String entityId)
    {
        T8DataFilterCriteria filterCriteria;
        T8DataFilter filter;

        // Create the process filter.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityId, filterCriteria);
        filter.addFieldOrdering(this.processOrdering.getFieldId(entityId), this.processOrdering.orderMethod);

        // Add user criteria.
        if (!this.userIds.isEmpty())
        {
            T8DataFilterCriteria userCriteria;

            userCriteria = new T8DataFilterCriteria();
            userCriteria.addFilterClause(DataFilterConjunction.AND, entityId + F_RESTRICTION_USER_ID, T8DataFilterCriterion.DataFilterOperator.IN, this.userIds);
            userCriteria.addFilterClause(DataFilterConjunction.OR, entityId + F_RESTRICTION_USER_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null);
            filterCriteria.addFilterClause(DataFilterConjunction.AND, userCriteria);
        }

        // Add user profile criteria.
        if (!this.profileIds.isEmpty())
        {
            T8DataFilterCriteria profileCriteria;

            profileCriteria = new T8DataFilterCriteria();
            profileCriteria.addFilterClause(DataFilterConjunction.AND, entityId + F_RESTRICTION_USER_PROFILE_ID, T8DataFilterCriterion.DataFilterOperator.IN, this.profileIds);
            profileCriteria.addFilterClause(DataFilterConjunction.OR, entityId + F_RESTRICTION_USER_PROFILE_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null);
            filterCriteria.addFilterClause(DataFilterConjunction.AND, profileCriteria);
        }

        // Add process id filter criteria.
        if (!this.processIds.isEmpty())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + F_PROCESS_ID, T8DataFilterCriterion.DataFilterOperator.IN, this.processIds);
        }

        // Add process iid filter criteria.
        if (!this.processIids.isEmpty())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + F_PROCESS_IID, T8DataFilterCriterion.DataFilterOperator.IN, this.processIids);
        }

        // Return the final criteria.
        return filter;
    }
}
