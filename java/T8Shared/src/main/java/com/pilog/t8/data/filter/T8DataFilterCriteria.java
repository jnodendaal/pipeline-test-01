package com.pilog.t8.data.filter;

import com.pilog.epic.annotation.EPICMethod;
import com.pilog.epic.annotation.EPICParameter;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.data.filter.T8DataFilterCriteriaDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilterCriteria implements T8DataFilterClause, Serializable
{
    protected final LinkedHashMap<T8DataFilterClause, DataFilterConjunction> filterClauses;
    protected String entityId;
    protected String id;

    public T8DataFilterCriteria()
    {
        filterClauses = new LinkedHashMap<T8DataFilterClause, DataFilterConjunction>();
    }

    public T8DataFilterCriteria(String id)
    {
        this();
        this.id = id;
    }

    public T8DataFilterCriteria(T8DataFilterCriteriaDefinition definition)
    {
        this();
        this.id = definition.getIdentifier();
    }

    public T8DataFilterCriteria(Map<String, Object> keyMap)
    {
        this();
        addKeyFilter(keyMap, false);
    }

    public T8DataFilterCriteria(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        this();
        addKeyFilter(keyMap, caseInsensitive);
    }

    public T8DataFilterCriteria(DataFilterConjunction conjunction, T8DataFilterClause... filterClauses)
    {
        this();
        addFilterClauses(conjunction, filterClauses);
    }

    public String getIdentifier()
    {
        return id;
    }

    @Override
    public T8DataFilterCriteria copy()
    {
        T8DataFilterCriteria copiedCriteria;

        copiedCriteria = new T8DataFilterCriteria(id);
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            copiedCriteria.addFilterClause(filterClauses.get(filterClause), filterClause.copy());
        }

        return copiedCriteria;
    }

    public void clearClauses()
    {
        filterClauses.clear();
    }

    public void addKeyFilter(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        for (String key : keyMap.keySet())
        {
            Object value;

            value = keyMap.get(key);
            if (value == null)
            {
                addFilterClause(DataFilterConjunction.AND, key, DataFilterOperator.IS_NULL, value, caseInsensitive);
            }
            else if (value instanceof Collection)
            {
                addFilterClause(DataFilterConjunction.AND, key, DataFilterOperator.IN, value, caseInsensitive);
            }
            else
            {
                addFilterClause(DataFilterConjunction.AND, key, DataFilterOperator.EQUAL, value, caseInsensitive);
            }
        }
    }

    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param filterClause The clause to add to this filter criteria.
     */
    @EPICMethod(Description = "Adds a new filter clause criteria to this filter criteria using the provided conjunction.")
    public void addClause(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "filterClause") T8DataFilterClause filterClause)
    {
        addFilterClause(DataFilterConjunction.valueOf(conjunction), filterClause);
    }

    public void addFilterClause(DataFilterConjunction conjunction, T8DataFilterClause filterClause)
    {
        if (filterClause == null) throw new IllegalArgumentException("Invalid Filter Clause: The filter clause cannot be null");
        else
        {
            filterClause.setEntityIdentifier(entityId);
            filterClauses.put(filterClause, conjunction);
        }
    }

    public void setFilterClauses(LinkedHashMap<T8DataFilterClause, DataFilterConjunction> filterClauses)
    {
        this.filterClauses.clear();
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            addFilterClause(filterClauses.get(clause), clause);
        }
    }

    public void addFilterClauses(DataFilterConjunction conjunction, T8DataFilterClause... filterClauses)
    {
        for (T8DataFilterClause filterClause : filterClauses)
        {
            addFilterClause(conjunction, filterClause);
        }
    }

    public void addFilterClause(DataFilterConjunction conjunction, String columnName, DataFilterOperator operator, Object filterValue)
    {
        addFilterClause(conjunction, new T8DataFilterCriterion(columnName, operator, filterValue, false));
    }

    public void addFilterClause(DataFilterConjunction conjunction, String columnName, DataFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(conjunction, new T8DataFilterCriterion(columnName, operator, filterValue, caseInsensitive));
    }

    public void addFilterCriterion(DataFilterConjunction conjunction, String columnName, DataFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(conjunction, new T8DataFilterCriterion(columnName, operator, filterValue, caseInsensitive));
    }

    public void addFilterCriterion(String conjunction, String columnName, String operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(DataFilterConjunction.valueOf(conjunction), new T8DataFilterCriterion(columnName, DataFilterOperator.valueOf(operator), filterValue, caseInsensitive));
    }

    public DataFilterConjunction getConjunction(T8DataFilterClause clause)
    {
        return filterClauses.get(clause);
    }

    public LinkedHashMap<T8DataFilterClause, DataFilterConjunction> getFilterClauses()
    {
        return filterClauses;
    }

    public void setFilterClauses(Map<T8DataFilterClause, DataFilterConjunction> clauses)
    {
        this.filterClauses.clear();
        for (T8DataFilterClause clause : clauses.keySet())
        {
            addFilterClause(clauses.get(clause), clause);
        }
    }

    @Override
    public boolean hasCriteria()
    {
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            if (clause.hasCriteria()) return true;
        }

        return false;
    }

    @Override
    public String getEntityIdentifier()
    {
        return entityId;
    }

    @Override
    public void setEntityIdentifier(String identifier)
    {
        this.entityId = identifier;
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            filterClause.setEntityIdentifier(identifier);
        }
    }

    @Override
    public StringBuffer getWhereClause(T8DataTransaction dataAccessProvider, String sqlIdentifier)
    {
        if (!hasCriteria())
        {
            return null;
        }
        else
        {
            StringBuffer whereClause;
            Iterator<T8DataFilterClause> clauseIterator;
            boolean first;

            whereClause = new StringBuffer();
            whereClause.append("(");

            first = true;
            clauseIterator = filterClauses.keySet().iterator();
            while (clauseIterator.hasNext())
            {
                T8DataFilterClause clause;

                clause = clauseIterator.next();
                if (clause.hasCriteria())
                {
                    if (first) // If it's the first clause, don't append a space, nor a conjunction.
                    {
                        first = false;
                        whereClause.append(clause.getWhereClause(dataAccessProvider, sqlIdentifier));
                    }
                    else // For any clause except the first one, append the conjunction and then the clause.
                    {
                        whereClause.append(" ");
                        whereClause.append(filterClauses.get(clause));
                        whereClause.append(" ");
                        whereClause.append(clause.getWhereClause(dataAccessProvider, sqlIdentifier));
                    }
                }
            }

            whereClause.append(")");
            return whereClause;
        }
    }

    @Override
    public ArrayList<Object> getWhereClauseParameters(T8DataTransaction dataAccessProvider)
    {
        ArrayList<Object> parameters;

        parameters = new ArrayList<Object>();
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            if (clause.hasCriteria())
            {
                parameters.addAll(clause.getWhereClauseParameters(dataAccessProvider));
            }
        }

        return parameters;
    }

    @Override
    public boolean includesEntity(T8DataEntity entity)
    {
        boolean result;
        boolean firstIteration;

        result = filterClauses.keySet().isEmpty();
        //In order for the filter to work correctly in scenarios where only and operators are used, we need to force the modidifer to be true for the fist iteration.
        firstIteration = true;
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            result = includesEntity(entity, result || (firstIteration && filterClauses.get(clause) == DataFilterConjunction.AND), clause);
            firstIteration = false;
        }

        return result;
    }

    @Override
    public T8DataFilterCriterion getFilterCriterion(String identifier)
    {
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            T8DataFilterCriterion filterCriterion;

            filterCriterion = clause.getFilterCriterion(identifier);
            if (filterCriterion != null) return filterCriterion;
        }

        return null;
    }

    private boolean includesEntity(T8DataEntity entity, boolean modifier, T8DataFilterClause clause)
    {
        DataFilterConjunction conjunction;

        conjunction = filterClauses.get(clause);
        if (conjunction == DataFilterConjunction.AND) return modifier && clause.includesEntity(entity);
        else return modifier || clause.includesEntity(entity);
    }

    @Override
    public Set<String> getFilterFieldIdentifiers()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<String>();
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            fieldSet.addAll(filterClause.getFilterFieldIdentifiers());
        }

        return fieldSet;
    }

    /**
     * Removes the {@code T8DataFilterCriterion} objects from the associated {@code T8DataFilterCriteria} by matching it up
 with the field id supplied.
     *
     * @param filterCriterion The {@code String} field id associated with the {@code T8DataFilterCriterion} objects to be removed.
     */
    public void removeFilterCriterionByField(String fieldIdentifier)
    {
        // Loops through the Criterion List and finds the criterion's that needs to be removed.
        for (T8DataFilterCriterion filterCriterion : getFilterCriterionList())
        {
            if (filterCriterion.getFieldIdentifier().equals(fieldIdentifier))
            {
                removeFilterClause(filterCriterion);
            }
        }
    }

    /**
     * Removes the given {@code T8DataFilterClause} from the clause list.
     *
     * @param filterClause The {@code T8DataFilterClause} that needs to be removed.
     */
    public void removeFilterClause(T8DataFilterClause filterClause)
    {
        List<T8DataFilterClause> filterClauseRemovalList;

        filterClauseRemovalList = new ArrayList<T8DataFilterClause>();
        // If the Filter clause is not found at this level check in the sub clauses
        if (filterClauses.remove(filterClause) == null)
        {
            for (T8DataFilterClause filterClauseFromCriteria : filterClauses.keySet())
            {
                if (filterClauseFromCriteria instanceof T8DataFilterCriteria)
                {
                    ((T8DataFilterCriteria)filterClauseFromCriteria).removeFilterClause(filterClause);

                    if (filterClauseFromCriteria.getFilterCriterionList().isEmpty())
                    {
                        filterClauseRemovalList.add(filterClauseFromCriteria);
                    }
                }
            }
        }

        for (T8DataFilterClause removeFilterClause : filterClauseRemovalList)
        {
            filterClauses.remove(removeFilterClause);
        }
    }

    @Override
    public List<T8DataFilterCriterion> getFilterCriterionList()
    {
        ArrayList<T8DataFilterCriterion> criterionList;

        criterionList = new ArrayList<T8DataFilterCriterion>();
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            criterionList.addAll(filterClause.getFilterCriterionList());
        }

        return criterionList;
    }

    public boolean equalsFilterCriteria(T8DataFilterCriteria other)
    {
        if (other == null)
        {
            return false;
        }
        else if (!Objects.equals(id, other.id))
        {
            return false;
        }
        else if((this.filterClauses == null && other.filterClauses != null) || (this.filterClauses != null && other.filterClauses == null))
        {
            return false;
        }
        else if (this.filterClauses != null && other.filterClauses != null)
        {
            if((this.filterClauses.isEmpty() && !other.filterClauses.isEmpty()) || (!this.filterClauses.isEmpty() && other.filterClauses.isEmpty()))
            {
                return false;
            }

            for (T8DataFilterClause t8DataFilterClause : filterClauses.keySet())
            {
                if(!other.filterClauses.containsKey(t8DataFilterClause))
                {
                    return false;
                }
            }

            return true;
        }
        else return true;
    }

    @Override
    public String toString()
    {
        return "T8DataFilterCriteria{" + "entityIdentifier=" + entityId + ", identifier=" + id + '}';
    }

    public void printStructure()
    {
        T8DataFilterPrinter.printStructure(System.out, this);
    }
}
