package com.pilog.t8.communication.event;

import com.pilog.t8.communication.T8CommunicationMessage;

/**
 * @author Hennie Brink
 */
public class T8MessageCancelledEvent extends T8CommunicationEvent
{
    public T8MessageCancelledEvent(T8CommunicationMessage message)
    {
        super(message);
    }

    public T8CommunicationMessage getMessage()
    {
        return (T8CommunicationMessage)source;
    }
}
