package com.pilog.t8.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8StateParameterList extends ArrayList<Object>
{
    private final String collectionIid;
    private final String entityId;
    private final Map<Integer, String> contentParameterIids;
    private final Set<Integer> insertedIndices;
    private final Map<String, Object> entityKey;

    public T8StateParameterList(String collectionIid, String entityId, Map<String, Object> key, T8StateDataSet dataSet)
    {
        this.collectionIid = collectionIid;
        this.entityId = entityId;
        this.contentParameterIids = new HashMap<>();
        this.insertedIndices = new HashSet<>();
        this.entityKey = key;
        if (dataSet != null) addParameters(dataSet);
    }

    private synchronized void addParameters(T8StateDataSet dataSet)
    {
        List<T8DataEntity> entityList;

        entityList = dataSet.getEntities(entityId, entityKey);
        for (T8DataEntity parameterEntity : entityList)
        {
            String parameterIid;
            Integer parameterIndex;
            String parameterValue;
            String parameterType;

            parameterIid = (String)parameterEntity.getFieldValue(entityId + "$PARAMETER_IID");
            parameterType = (String)parameterEntity.getFieldValue(entityId + "$TYPE");
            parameterIndex = (Integer)parameterEntity.getFieldValue(entityId + "$SEQUENCE");
            parameterValue = (String)parameterEntity.getFieldValue(entityId + "$VALUE");

            add(createParameterValue(parameterIid, parameterType, parameterValue, entityId, dataSet));
            contentParameterIids.put(parameterIndex, parameterIid);
        }
    }

    public synchronized T8DataEntity createEntity(T8DataTransaction tx, int index) throws Exception
    {
        T8DataEntity newEntity;
        Object value;

        value = get(index);

        newEntity = tx.create(entityId, null);
        newEntity.setFieldValue(entityId + "$PARAMETER_IID", contentParameterIids.get(index));
        newEntity.setFieldValue(entityId + "$PARENT_PARAMETER_IID", collectionIid);
        newEntity.setFieldValue(entityId + "$TYPE", T8StateParameterMap.getParameterType(value));
        newEntity.setFieldValue(entityId + "$SEQUENCE", index);
        newEntity.setFieldValue(entityId + "$PARAMETER_KEY", index);
        newEntity.setFieldValue(entityId + "$VALUE", T8StateParameterMap.getParameterStringValue(value));

        for (String keyFieldId : entityKey.keySet())
        {
            newEntity.setFieldValue(keyFieldId, entityKey.get(keyFieldId));
        }

        return newEntity;
    }

    public synchronized void statePersisted()
    {
        // Clear this state's flags.
        insertedIndices.clear();

        // Check for sub-collections.
        for (Object value : this)
        {
            if (value instanceof T8StateParameterMap)
            {
                ((T8StateParameterMap)value).statePersisted();
            }
            else if (value instanceof T8StateParameterList)
            {
                ((T8StateParameterList)value).statePersisted();
            }
        }
    }

    public synchronized void addUpdates(T8DataTransaction tx, T8StateUpdates updates) throws Exception
    {
        // Add the updates for each inserted index.
        for (Integer insertedIndex : insertedIndices)
        {
            updates.addInsert(createEntity(tx, insertedIndex));
        }

        // Check for sub-collections.
        for (Object value : this)
        {
            if (value instanceof T8StateParameterMap)
            {
                ((T8StateParameterMap)value).addUpdates(tx, updates);
            }
            else if (value instanceof T8StateParameterList)
            {
                ((T8StateParameterList)value).addUpdates(tx, updates);
            }
        }
    }

    public String getIdentifier()
    {
        return collectionIid;
    }

    @Override
    public synchronized Object remove(int i)
    {
        throw new RuntimeException("Cannot remove parameter.");
    }

    @Override
    public synchronized boolean add(Object value)
    {
        String parameterIid;
        int index;

        index = size();
        parameterIid = T8IdentifierUtilities.createNewGUID();
        contentParameterIids.put(index, parameterIid);
        insertedIndices.add(index);

        if (value instanceof Map)
        {
            T8StateParameterMap newMap;
            Map<String, Object> subKey;

            subKey = new HashMap(entityKey);
            subKey.put(entityId + "$PARENT_PARAMETER_IID", collectionIid);
            newMap = new T8StateParameterMap(parameterIid, entityId, subKey, null);
            newMap.putAll((Map)value);
            return super.add(newMap);
        }
        else if (value instanceof List)
        {
            T8StateParameterList newList;
            Map<String, Object> subKey;

            subKey = new HashMap(entityKey);
            subKey.put(entityId + "$PARENT_PARAMETER_IID", collectionIid);
            newList = new T8StateParameterList(parameterIid, entityId, subKey, null);
            for (Object valueElement : (List)value)
            {
                newList.add(valueElement);
            }

            return super.add(newList);
        }
        else
        {
            return super.add(value);
        }
    }

    public synchronized Object createParameterValue(String parameterIid, String parameterType, String value, String entityId, T8StateDataSet dataSet)
    {
        switch (parameterType)
        {
            case "MAP":
            {
                return new T8StateParameterMap(parameterIid, entityId, HashMaps.<String, Object>create(entityId + "$PARENT_PARAMETER_IID", parameterIid), dataSet);
            }
            case "LIST":
            {
                return new T8StateParameterList(parameterIid, entityId, HashMaps.<String, Object>create(entityId + "$PARENT_PARAMETER_IID", parameterIid), dataSet);
            }
            case "INTEGER":
            {
                return Integer.parseInt(value);
            }
            case "LONG":
            {
                return Long.parseLong(value);
            }
            case "DOUBLE":
            {
                return Double.parseDouble(value);
            }
            case "FLOAT":
            {
                return Float.parseFloat(value);
            }
            case "BOOLEAN":
            {
                return Boolean.parseBoolean(value);
            }
            case "TIMESTAMP":
            {
                return T8Timestamp.fromTime(Long.parseLong(value));
            }
            default:
            {
                return value; // Just return the String value.
            }
        }
    }
}
