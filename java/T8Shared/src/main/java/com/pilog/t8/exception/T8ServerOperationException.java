package com.pilog.t8.exception;

import com.pilog.t8.T8SessionContext;

/**
 * @author Hennie Brink
 */
public class T8ServerOperationException extends Exception
{
    private transient T8SessionContext sessionContext;

    public T8ServerOperationException()
    {
    }

    public T8ServerOperationException(String message)
    {
        super(message);
    }

    public T8ServerOperationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public T8ServerOperationException(Throwable cause)
    {
        super(cause);
    }

    public T8ServerOperationException(String message, Throwable cause,
                                      boolean enableSuppression,
                                      boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public T8ServerOperationException(T8SessionContext sessionContext)
    {
        this.sessionContext = sessionContext;
    }

    public T8ServerOperationException(T8SessionContext sessionContext,
                                      String message)
    {
        super(message);
        this.sessionContext = sessionContext;
    }

    public T8ServerOperationException(T8SessionContext sessionContext,
                                      String message, Throwable cause)
    {
        super(message, cause);
        this.sessionContext = sessionContext;
    }

    public T8ServerOperationException(T8SessionContext sessionContext,
                                      Throwable cause)
    {
        super(cause);
        this.sessionContext = sessionContext;
    }

    public T8ServerOperationException(T8SessionContext sessionContext,
                                      String message, Throwable cause,
                                      boolean enableSuppression,
                                      boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        this.sessionContext = sessionContext;
    }

    @Override
    public String toString()
    {
        if(sessionContext == null)
            return super.toString();
        else return "[" + sessionContext.getSessionIdentifier() + "] " + super.toString();
    }
}
