package com.pilog.t8.definition.functionality.access;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.functionality.access.T8FunctionalityAccessPolicy;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityAccessPolicyDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FUNCTIONALITY_ACCESS_POLICY";
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_ACCESS_POLICY";
    public static final String DISPLAY_NAME = "System Functionality Access Policy";
    public static final String DESCRIPTION = "A functionality access policy.";
    public static final String IDENTIFIER_PREFIX = "FUNC_ACC_";
    public static final String STORAGE_PATH = "/functionality_access_policies";
    public enum Datum {ACCESS_BLOCKER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8FunctionalityAccessPolicyDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ACCESS_BLOCKER_DEFINITIONS.toString(), "Access Blockers", "The list of access blockers enforced by this policy."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ACCESS_BLOCKER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FunctionalityAccessBlockerDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8FunctionalityAccessPolicy createNewAccessPolicyInstance(T8Context context) throws Exception
    {
        return new T8FunctionalityAccessPolicy(context, this);
    }

    public List<T8FunctionalityAccessBlockerDefinition> getAccessBlockerDefinitions()
    {
        return (List<T8FunctionalityAccessBlockerDefinition>)getDefinitionDatum(Datum.ACCESS_BLOCKER_DEFINITIONS.toString());
    }

    public void setAccessBlockerDefinitions(List<T8FunctionalityAccessBlockerDefinition> definitions)
    {
        setDefinitionDatum(Datum.ACCESS_BLOCKER_DEFINITIONS.toString(), definitions);
    }
}
