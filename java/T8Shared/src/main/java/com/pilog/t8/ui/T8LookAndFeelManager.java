package com.pilog.t8.ui;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.laf.T8LookAndFeelDefinition;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8LookAndFeelManager
{
    // Notifications
    private T8Painter notificationBackgroundPainter;

    // T8Button
    private T8Painter buttonBackgroundPainter;
    private T8Painter buttonBackgroundFocusedPainter;
    private T8Painter buttonBackgroundSelectedPainter;
    private T8Painter buttonBackgroundDisabledPainter;

    // T8Button (Tool Bar)
    private T8Painter toolBarButtonBackgroundPainter;
    private T8Painter toolBarButtonBackgroundFocusedPainter;
    private T8Painter toolBarButtonBackgroundSelectedPainter;
    private T8Painter toolBarButtonBackgroundDisabledPainter;

    //T8DataSearchComboBox
    private T8Painter dataSearchCombBoxBackgroundPainter;
    private T8Painter dataSearchComboBoxActivatedPainter;

    // T8ToolBar
    private T8Painter toolBarBackgroundPainter;

    // T8ContentHeader
    private T8Painter contentHeaderBackgroundPainter;
    private T8Painter contentHeaderBackgroundSelectedPainter;

    public T8LookAndFeelManager(T8ClientContext clientContext, T8Context context)
    {
    }

    public void init(T8LookAndFeelDefinition definition)
    {
        T8PainterDefinition painterDefinition;

        // Notifications
        painterDefinition = definition.getNotificationBackgroundPainterDefinition();
        notificationBackgroundPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button
        painterDefinition = definition.getButtonBackgroundPainterDefinition();
        buttonBackgroundPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button : Focused
        painterDefinition = definition.getButtonBackgroundFocusedPainterDefinition();
        buttonBackgroundFocusedPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button : Selected
        painterDefinition = definition.getButtonBackgroundSelectedPainterDefinition();
        buttonBackgroundSelectedPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button : Disabled
        painterDefinition = definition.getButtonBackgroundDisabledPainterDefinition();
        buttonBackgroundDisabledPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8ToolBar
        painterDefinition = definition.getToolBarBackgroundPainterDefinition();
        toolBarBackgroundPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button (Tool Bar)
        painterDefinition = definition.getToolBarButtonBackgroundPainterDefinition();
        toolBarButtonBackgroundPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button (Tool Bar) : Focused
        painterDefinition = definition.getToolBarButtonBackgroundFocusedPainterDefinition();
        toolBarButtonBackgroundFocusedPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button (Tool Bar) : Selected
        painterDefinition = definition.getToolBarButtonBackgroundSelectedPainterDefinition();
        toolBarButtonBackgroundSelectedPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8Button (Tool Bar) : Disabled
        painterDefinition = definition.getToolBarButtonBackgroundDisabledPainterDefinition();
        toolBarButtonBackgroundDisabledPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8BDataSearchComboBox : Background
        painterDefinition = definition.getDataSearchComboBoxBackgroundPainterDefinition();
        dataSearchCombBoxBackgroundPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8BDataSearchComboBox : Activated
        painterDefinition = definition.getDataSearchComboBoxActivatedPainterDefinition();
        dataSearchComboBoxActivatedPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8ContentHeader
        painterDefinition = definition.getContentHeaderBackgroundPainterDefinition();
        contentHeaderBackgroundPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;

        // T8ContentHeader : Selected
        painterDefinition = definition.getContentHeaderBackgroundSelectedPainterDefinition();
        contentHeaderBackgroundSelectedPainter = painterDefinition != null ? painterDefinition.getNewPainterInstance() : null;
    }

    public void destroy()
    {
        // Nothing to do here yet.
    }

    public T8Painter getNotificationBackgroundPainter()
    {
        return notificationBackgroundPainter;
    }

    public T8Painter getButtonBackgroundPainter()
    {
        return buttonBackgroundPainter;
    }

    public T8Painter getButtonBackgroundFocusedPainter()
    {
        return buttonBackgroundFocusedPainter;
    }

    public T8Painter getButtonBackgroundSelectedPainter()
    {
        return buttonBackgroundSelectedPainter;
    }

    public T8Painter getButtonBackgroundDisabledPainter()
    {
        return buttonBackgroundDisabledPainter;
    }

    public T8Painter getToolBarBackgroundPainter()
    {
        return toolBarBackgroundPainter;
    }

    public T8Painter getToolBarButtonBackgroundPainter()
    {
        return toolBarButtonBackgroundPainter;
    }

    public T8Painter getToolBarButtonBackgroundFocusedPainter()
    {
        return toolBarButtonBackgroundFocusedPainter;
    }

    public T8Painter getToolBarButtonBackgroundSelectedPainter()
    {
        return toolBarButtonBackgroundSelectedPainter;
    }

    public T8Painter getToolBarButtonBackgroundDisabledPainter()
    {
        return toolBarButtonBackgroundDisabledPainter;
    }

    public T8Painter getDataSearchCombBoxBackgroundPainter()
    {
        return dataSearchCombBoxBackgroundPainter;
    }

    public T8Painter getDataSearchComboBoxActivatedPainter()
    {
        return dataSearchComboBoxActivatedPainter;
    }

    public T8Painter getContentHeaderBackgroundPainter()
    {
        return contentHeaderBackgroundPainter;
    }

    public T8Painter getContentHeaderBackgroundSelectedPainter()
    {
        return contentHeaderBackgroundSelectedPainter;
    }
}
