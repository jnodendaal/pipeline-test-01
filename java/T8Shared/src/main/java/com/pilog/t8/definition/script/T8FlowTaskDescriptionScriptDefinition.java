package com.pilog.t8.definition.script;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskDescriptionScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_TASK_DESCRIPTION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FLOW_TASK_DESCRIPTION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Task Description Script";
    public static final String DESCRIPTION = "A script that is executed in order to compile a collection of descriptive properties for a specific flow task.";
    // -------- Definition Meta-Data -------- //
    
    public static final String PARAMETER_INITIATOR_ORG_ID = "$P_INITIATOR_ORG_ID";
    
    public T8FlowTaskDescriptionScriptDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8FlowTaskDefinition taskDefinition;
        
        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_INITIATOR_ORG_ID, "Initiator Organization ID", "The ID of the organization to which the initiator of this flow belongs.", T8DataType.GUID));

        taskDefinition = (T8FlowTaskDefinition)getParentDefinition();
        if (taskDefinition != null)
        {
            definitions.addAll(taskDefinition.getInputParameterDefinitions());
        }

        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }
}
