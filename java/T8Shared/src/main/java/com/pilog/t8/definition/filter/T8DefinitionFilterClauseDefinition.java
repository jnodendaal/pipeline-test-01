package com.pilog.t8.definition.filter;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.filter.T8DefinitionFilterClause;
import com.pilog.t8.definition.filter.T8DefinitionFilterClause.DefinitionFilterConjunction;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefinitionFilterClauseDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_FILTER_CLAUSE";
    public static final String STORAGE_PATH = null;
    public enum Datum {CONJUNCTION};
    // -------- Definition Meta-Data -------- //
    
    public T8DefinitionFilterClauseDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONJUNCTION.toString(), "Conjunction", "The conjunction type to use when joining this filter clause to others.", DefinitionFilterConjunction.AND.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONJUNCTION.toString().equals(datumIdentifier)) return createStringOptions(DefinitionFilterConjunction.values());
        else return null;
    }    
    
    public DefinitionFilterConjunction getConjunction()
    {
        return DefinitionFilterConjunction.valueOf((String)getDefinitionDatum(Datum.CONJUNCTION.toString()));
    }
    
    public void setConjunction(DefinitionFilterConjunction conjunction)
    {
        setDefinitionDatum(Datum.CONJUNCTION.toString(), conjunction.toString());
    }
    
    public abstract T8DefinitionFilterClause getNewDefinitionFilterClauseInstance(T8SessionContext sessionContext, Map<String, Object> filterParameters);
}
