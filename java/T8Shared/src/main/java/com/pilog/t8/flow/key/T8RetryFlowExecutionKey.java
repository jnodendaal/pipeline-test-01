package com.pilog.t8.flow.key;

/**
 * @author Bouwer du Preez
 */
public class T8RetryFlowExecutionKey extends T8DefaultFlowExecutionKey
{
    public T8RetryFlowExecutionKey(String flowInstanceIdentifier, String nodeInstanceIdentifier)
    {
        super(null, flowInstanceIdentifier, null, nodeInstanceIdentifier);
    }
}
