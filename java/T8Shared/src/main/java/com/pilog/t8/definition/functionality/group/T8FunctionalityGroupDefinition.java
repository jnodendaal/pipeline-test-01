package com.pilog.t8.definition.functionality.group;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FunctionalityGroupDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FUNCTIONALITY_GROUP";
    public static final String GROUP_NAME = "Functionality Groups";
    public static final String GROUP_DESCRIPTION = "Sets of functionalities grouped together for a specific purpose such as the UI display of menu options in a functional area.";
    public static final String DISPLAY_NAME = "Functionality Group";
    public static final String DESCRIPTION = "A grouping for a set of functionalities available in a T8 System.";
    public static final String STORAGE_PATH = "/functionality_groups";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8FunctionalityGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8FunctionalityGroupHandle getNewFunctionalityGroupHandle(T8Context context, T8FunctionalityGroupHandle parentGroup) throws Exception;
}
