package com.pilog.t8.definition.process;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtProcessDetails;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLAggregateFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GET_PROCESS_DETAILS = "@OP_PRC_GET_PROCESS_DETAILS";
    public static final String OPERATION_GET_PROCESS_SUMMARY = "@OP_PRC_GET_PROCESS_SUMMARY";
    public static final String OPERATION_COUNT_PROCESSES = "@OP_PRC_COUNT_PROCESSES";
    public static final String OPERATION_SEARCH_PROCESSES = "@OP_PRC_SEARCH_PROCESSES";
    public static final String OPERATION_START_PROCESS = "@OP_PRC_START_PROCESS";
    public static final String OPERATION_STOP_PROCESS = "@OP_PRC_STOP_PROCESS";
    public static final String OPERATION_CANCEL_PROCESS = "@OP_PRC_CANCEL_PROCESS";
    public static final String OPERATION_PAUSE_PROCESS = "@OP_PRC_PAUSE_PROCESS";
    public static final String OPERATION_RESUME_PROCESS = "@OP_PRC_RESUME_PROCESS";
    public static final String OPERATION_FINALIZE_PROCESS = "@OP_PRC_FINALIZE_PROCESS";

    public static final String PARAMETER_PROCESS_COUNT = "$P_PROCESS_COUNT";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_OUTPUT_PARAMETERS = "$P_OUTPUT_PARAMETERS";
    public static final String PARAMETER_PROCESS_FILTER = "$P_PROCESS_FILTER";
    public static final String PARAMETER_PROCESS_DEFINITION = "$P_PROCESS_DEFINITION";
    public static final String PARAMETER_PROCESS_LIST = "$P_PROCESS_LIST";
    public static final String PARAMETER_PROCESS_SUMMARY = "$P_PROCESS_SUMMARY";
    public static final String PARAMETER_PROCESS_IID = "$P_PROCESS_IID";
    public static final String PARAMETER_PROCESS_STATE_DEFINITION = "$P_PROCESS_STATE_DEFINITION";
    public static final String PARAMETER_PROCESS_ID = "$P_PROCESS_ID";
    public static final String PARAMETER_PROCESS_DETAILS = "$P_PROCESS_DETAILS";
    public static final String PARAMETER_SEARCH_EXPRESSION = "$P_SEARCH_EXPRESSION";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";

    public static final String PROCESS_STATE_DS_IDENTIFIER = "@DS_STATE_PROCESS";
    public static final String PROCESS_STATE_DE_IDENTIFIER = "@E_STATE_PROCESS";
    public static final String PROCESS_STATE_PAR_DS_IDENTIFIER = "@DS_STATE_PROCESS_PAR";
    public static final String PROCESS_STATE_PAR_DE_IDENTIFIER = "@E_STATE_PROCESS_PAR";
    public static final String PROCESS_STATE_SUMMARY_DS_IDENTIFIER = "@DS_STATE_PROCESS_SUMMARY";
    public static final String PROCESS_STATE_SUMMARY_DE_IDENTIFIER = "@E_STATE_PROCESS_SUMMARY";
    public static final String PROCESS_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_PROCESS";
    public static final String PROCESS_HISTORY_DE_IDENTIFIER = "@E_HISTORY_PROCESS";
    public static final String PROCESS_HISTORY_PAR_DS_IDENTIFIER = "@DS_HISTORY_PROCESS_PAR";
    public static final String PROCESS_HISTORY_PAR_DE_IDENTIFIER = "@E_HISTORY_PROCESS_PAR";

    public static final String F_EVENT_IID = "$EVENT_IID";
    public static final String F_PROCESS_IID = "$PROCESS_IID";
    public static final String F_PROCESS_ID = "$PROCESS_ID";
    public static final String F_PROJECT_ID = "$PROJECT_ID";
    public static final String F_USER_ID = "$USER_ID";
    public static final String F_USER_PROFILE_ID = "$USER_PROFILE_ID";
    public static final String F_EVENT = "$EVENT";
    public static final String F_TIME = "$TIME";
    public static final String F_EVENT_PARAMETERS = "$EVENT_PARAMETERS";
    public static final String F_PARAMETER_ID = "$PARAMETER_ID";
    public static final String F_PARAMETER_IID = "$PARAMETER_IID";
    public static final String F_PARENT_PARAMETER_IID = "$PARENT_PARAMETER_IID";
    public static final String F_ID = "$ID";
    public static final String F_PROGRESS = "$PROGRESS";
    public static final String F_VALUE = "$VALUE";
    public static final String F_STATUS = "$STATUS";
    public static final String F_PROCESS_DISPLAY_NAME = "$PROCESS_DISPLAY_NAME";
    public static final String F_PROCESS_DESCRIPTION = "$PROCESS_DESCRIPTION";
    public static final String F_INITIATOR_ID = "$INITIATOR_ID";
    public static final String F_INITIATOR_IID = "$INITIATOR_IID";
    public static final String F_RESTRICTION_USER_ID = "$RESTRICTION_USER_ID";
    public static final String F_RESTRICTION_USER_PROFILE_ID = "$RESTRICTION_USER_PROFILE_ID";
    public static final String F_START_TIME = "$START_TIME";
    public static final String F_END_TIME = "$END_TIME";
    public static final String F_TYPE = "$TYPE";
    public static final String F_SEQUENCE = "$SEQUENCE";
    public static final String F_CHARACTER_DATA = "$CHARACTER_DATA";
    public static final String F_BINARY_DATA = "$BINARY_DATA";
    public static final String F_PARAMETER_KEY = "$PARAMETER_KEY";
    public static final String F_PROCESS_COUNT = "$PROCESS_COUNT";

    // Table Names.
    public static final String TABLE_HISTORY_PROCESS = "HIS_PRC";
    public static final String TABLE_HISTORY_PROCESS_PAR = "HIS_PRC_PAR";
    public static final String TABLE_STATE_PROCESS = "PRC";
    public static final String TABLE_STATE_PROCESS_PAR = "PRC_PAR";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        T8DefinitionManager definitionManager;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        definitionManager = serverContext.getDefinitionManager();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions(definitionManager));
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(definitionManager));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(definitionManager));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8DefinitionManager definitionManager)
    {
        T8SQLBasedGroupByDataSourceDefinition sqlGroupByDataSourceDefinition;
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Process History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(PROCESS_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(definitionManager.getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_PROCESS);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_ID, "PROCESS_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_IID, "PROCESS_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_PROFILE_ID, "USER_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME, "TIME", false, false, T8DataType.DATE_TIME));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_PARAMETERS, "EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Process History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(PROCESS_HISTORY_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(definitionManager.getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_PROCESS_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_IID, "PROCESS_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Process State Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(PROCESS_STATE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(definitionManager.getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_PROCESS);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_ID, "PROCESS_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_IID, "PROCESS_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_DISPLAY_NAME, "PROCESS_DISPLAY_NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_DESCRIPTION, "PROCESS_DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STATUS, "STATUS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROGRESS, "PROGRESS", false, false, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RESTRICTION_USER_ID, "RESTRICTION_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RESTRICTION_USER_PROFILE_ID, "RESTRICTION_USER_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_START_TIME, "START_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_END_TIME, "END_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Process State Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(PROCESS_STATE_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(definitionManager.getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_PROCESS_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_IID, "PROCESS_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHARACTER_DATA, "CHARACTER_DATA", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_BINARY_DATA, "BINARY_DATA", false, false, T8DataType.BYTE_ARRAY));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Process State Summary SQL Group By Data Source.
        sqlGroupByDataSourceDefinition = new T8SQLBasedGroupByDataSourceResourceDefinition(PROCESS_STATE_SUMMARY_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.setOriginatingDataSourceIdentifier(PROCESS_STATE_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_PROJECT_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_PROCESS_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_RESTRICTION_USER_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_RESTRICTION_USER_PROFILE_ID);
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROCESS_ID, "PROCESS_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RESTRICTION_USER_ID, "RESTRICTION_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RESTRICTION_USER_PROFILE_ID, "RESTRICTION_USER_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8SQLAggregateFieldResourceDefinition(F_PROCESS_COUNT, "PROCESS_COUNT", false, false, T8DataType.INTEGER, "COUNT(PROCESS_ID)"));
        definitions.add(sqlGroupByDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8DefinitionManager definitionManager)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Process History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(PROCESS_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(PROCESS_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, PROCESS_HISTORY_DS_IDENTIFIER + F_EVENT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, PROCESS_HISTORY_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_ID, PROCESS_HISTORY_DS_IDENTIFIER + F_PROCESS_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_IID, PROCESS_HISTORY_DS_IDENTIFIER + F_PROCESS_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, PROCESS_HISTORY_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_PROFILE_ID, PROCESS_HISTORY_DS_IDENTIFIER + F_USER_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, PROCESS_HISTORY_DS_IDENTIFIER + F_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, PROCESS_HISTORY_DS_IDENTIFIER + F_TIME, false, false, T8DataType.DATE_TIME));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_PARAMETERS, PROCESS_HISTORY_DS_IDENTIFIER + F_EVENT_PARAMETERS, false, false, T8DataType.LONG_STRING));
        definitions.add(entityDefinition);

        // Process History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(PROCESS_HISTORY_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(PROCESS_HISTORY_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_EVENT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_IID, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_PROCESS_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, PROCESS_HISTORY_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Process State Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(PROCESS_STATE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(PROCESS_STATE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, PROCESS_STATE_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_ID, PROCESS_STATE_DS_IDENTIFIER + F_PROCESS_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_IID, PROCESS_STATE_DS_IDENTIFIER + F_PROCESS_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_DISPLAY_NAME, PROCESS_STATE_DS_IDENTIFIER + F_PROCESS_DISPLAY_NAME, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_DESCRIPTION, PROCESS_STATE_DS_IDENTIFIER + F_PROCESS_DESCRIPTION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STATUS, PROCESS_STATE_DS_IDENTIFIER + F_STATUS, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROGRESS, PROCESS_STATE_DS_IDENTIFIER + F_PROGRESS, false, false, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, PROCESS_STATE_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, PROCESS_STATE_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RESTRICTION_USER_ID, PROCESS_STATE_DS_IDENTIFIER + F_RESTRICTION_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RESTRICTION_USER_PROFILE_ID, PROCESS_STATE_DS_IDENTIFIER + F_RESTRICTION_USER_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_START_TIME, PROCESS_STATE_DS_IDENTIFIER + F_START_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_END_TIME, PROCESS_STATE_DS_IDENTIFIER + F_END_TIME, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // Process State Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(PROCESS_STATE_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(PROCESS_STATE_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_IID, PROCESS_STATE_PAR_DS_IDENTIFIER + F_PROCESS_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, PROCESS_STATE_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, PROCESS_STATE_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, PROCESS_STATE_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, PROCESS_STATE_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, PROCESS_STATE_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, PROCESS_STATE_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHARACTER_DATA, PROCESS_STATE_PAR_DS_IDENTIFIER + F_CHARACTER_DATA, false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_BINARY_DATA, PROCESS_STATE_PAR_DS_IDENTIFIER + F_BINARY_DATA, false, false, T8DataType.BYTE_ARRAY));
        definitions.add(entityDefinition);

        // Process State Summary Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(PROCESS_STATE_SUMMARY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(PROCESS_STATE_SUMMARY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, PROCESS_STATE_SUMMARY_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_ID, PROCESS_STATE_SUMMARY_DS_IDENTIFIER + F_PROCESS_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RESTRICTION_USER_ID, PROCESS_STATE_SUMMARY_DS_IDENTIFIER + F_RESTRICTION_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RESTRICTION_USER_PROFILE_ID, PROCESS_STATE_SUMMARY_DS_IDENTIFIER + F_RESTRICTION_USER_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROCESS_COUNT, PROCESS_STATE_SUMMARY_DS_IDENTIFIER + F_PROCESS_COUNT, false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        //  Returns the complete list of definitions.
        return definitions;
    }

    public List<T8Definition> getOperationDefinitions(T8DefinitionManager definitionManager)
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_COUNT_PROCESSES);
        definition.setMetaDisplayName("Get Process Count");
        definition.setMetaDescription("Returns the number of executing processes of the specified type on the server.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$CountProcessesOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_FILTER, "Process Filter", "The process filter to apply when counting processes.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_EXPRESSION, "Search Expression", "The expression to be used as search criteria.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_COUNT, "Process Count", "A number of processes.", T8DataType.INTEGER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_PROCESS_SUMMARY);
        definition.setMetaDisplayName("Get Process List Summary");
        definition.setMetaDescription("Returns a summary of the process types and counts that are available within the specified session context.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$GetProcessSummaryOperation");
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_SUMMARY, "Process List Summary", "A summary of the content of the process list.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_SEARCH_PROCESSES);
        definition.setMetaDisplayName("Search Processes");
        definition.setMetaDescription("Returns a list of all processes that are accessable within the specified session context (using the specified process filter) and that meet the specified criteria.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$SearchProcessesOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_FILTER, "Process Filter", "The process filter to apply when searching for processes.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_EXPRESSION, "Search Expression", "The expression to be used as search criteria.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset index of the page of results to return.", T8DataType.INTEGER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The number of results to return from the specified offset.", T8DataType.INTEGER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_LIST, "Process List", "A list of process details.", new T8DtList(new T8DtProcessDetails(definitionManager))));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_PROCESS_DETAILS);
        definition.setMetaDisplayName("Get Process Details");
        definition.setMetaDescription("Returns the details of the specified currently executing process.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$GetProcessDetailsOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "An identifier of the process instance from which to fetch detail information.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_START_PROCESS);
        definition.setMetaDisplayName("Start Process");
        definition.setMetaDescription("Creates a new process instance from the specified process definition and starts execution using the supplied input parameters.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$StartProcessOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_ID, "Process Identifier", "An identifier of the process type to start.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETERS, "Input Parameters", "A Map containing the input parameters to be used for process execution.", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_STOP_PROCESS);
        definition.setMetaDisplayName("Stop Process");
        definition.setMetaDescription("Stops the execution of the specified process as soon as possible.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$StopProcessOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "An identifier of the process instance to stop.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CANCEL_PROCESS);
        definition.setMetaDisplayName("Cancel Process");
        definition.setMetaDescription("Cancels the execution of the specified process as soon as possible.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$CancelProcessOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "An identifier of the process instance to cancel.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_PAUSE_PROCESS);
        definition.setMetaDisplayName("Pause Process");
        definition.setMetaDescription("Pauses the execution of the specified process as soon as possible.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$PauseProcessOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "An identifier of the process instance to pause.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RESUME_PROCESS);
        definition.setMetaDisplayName("Resume Process");
        definition.setMetaDescription("Resumes the execution of the specified previously paused process.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$ResumeProcessOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "An identifier of the process instance to resume.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_FINALIZE_PROCESS);
        definition.setMetaDisplayName("Finalize Process");
        definition.setMetaDescription("Closes the specified process, finalizing its execution and removing it from the process list.");
        definition.setClassName("com.pilog.t8.process.T8ProcessOperations$FinalizeProcessOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "An identifier of the process instance to finalize.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCESS_DETAILS, "Process Details", "An object containing the details of the specified process.", T8DtProcessDetails.IDENTIFIER, false));
        definitions.add(definition);

        return definitions;
    }
}
