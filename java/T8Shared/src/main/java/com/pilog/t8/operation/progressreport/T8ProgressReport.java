package com.pilog.t8.operation.progressreport;

import java.io.Serializable;

/**
 * @author Hennie Brink
 */
public interface T8ProgressReport extends Serializable
{
    public String getTitle();

    public void setTitle(String title);

    Double getProgress();

    String getProgressMessage();

    void setProgress(Double progress);

    void setProgressMessage(String progressMessage);

}
