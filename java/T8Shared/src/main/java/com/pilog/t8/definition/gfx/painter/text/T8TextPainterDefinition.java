package com.pilog.t8.definition.gfx.painter.text;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.gfx.painter.T8AbstractAreaPainterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextPainterDefinition extends T8AbstractAreaPainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_TEXT";
    public static final String DISPLAY_NAME = "Text Painter";
    public static final String DESCRIPTION = "A painter that draws a text String on the paintable area.";
    public enum Datum {TEXT,
                       FONT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8TextPainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TEXT.toString(), "Text", "The text that will be painted by this painter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FONT_DEFINITION.toString(), "Font", "The font to use when painting the text."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FONT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FontDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.FONT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Painter getNewPainterInstance()
    {
        return T8Reflections.getInstance("com.pilog.t8.gfx.painter.text.T8TextPainter", new Class<?>[]{T8TextPainterDefinition.class}, this);
    }

    public String getText()
    {
        return getDefinitionDatum(Datum.TEXT);
    }

    public void setText(String text)
    {
         setDefinitionDatum(Datum.TEXT, text);
    }

    public T8FontDefinition getFontDefinition()
    {
        return getDefinitionDatum(Datum.FONT_DEFINITION);
    }

    public void setFontDefinition(T8FontDefinition fontDefinition)
    {
         setDefinitionDatum(Datum.FONT_DEFINITION, fontDefinition);
    }
}
