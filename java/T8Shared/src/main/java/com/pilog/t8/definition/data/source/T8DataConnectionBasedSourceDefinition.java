package com.pilog.t8.definition.data.source;

/**
 * @author Bouwer du Preez
 */
public interface T8DataConnectionBasedSourceDefinition
{
    public String getDataConnectionIdentifier();
}
