package com.pilog.t8.definition.gfx.painter.matte;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.T8AbstractAreaPainterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MattePainterDefinition extends T8AbstractAreaPainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_MATTE";
    public static final String DISPLAY_NAME = "Matte Painter";
    public static final String DESCRIPTION = "A painter that fills the entire paintable area with the selected paint.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8MattePainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Painter getNewPainterInstance()
    {
        try
        {
            T8Painter painter;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.gfx.painter.matte.T8MattePainter").getConstructor(T8MattePainterDefinition.class);
            painter = (T8Painter)constructor.newInstance(this);
            return painter;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
