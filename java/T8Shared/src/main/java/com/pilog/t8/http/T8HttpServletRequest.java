package com.pilog.t8.http;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8HttpServletRequest
{
    public static String AUTHORIZATION_HEADER = "Authorization";

    public String getAuthorizationHeader();
    public T8SessionContext getSessionContext();
    public void setSessionContext(T8SessionContext sessionContext);
    public T8Context getAccessContext();

    /**
     * This method returns a map of query parameters as received from the HTTP request.
     * Each parameter name is a key in the output map, with the values for the parameter as
     * a string list value mapped to the key.  If no values for the parameter were received,
     * the value will be an empty list.
     * @return A map of query parameters as received from the HTTP request.
     */
    public Map<String, List<String>> getParameterMap();
}
