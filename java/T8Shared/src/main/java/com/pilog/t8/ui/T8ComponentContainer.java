package com.pilog.t8.ui;

import com.pilog.t8.ui.task.T8ComponentTask;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public interface T8ComponentContainer
{
    public JComponent getComponent();
    public void setComponent(JComponent component);
    public void setMessage(String message);
    public void setProgressMessage(String message);
    public void setCategoryMessage(String message);
    public void setProgress(int progress);
    public void setCategoryProgress(int progress);
    public void setIndeterminate(boolean indeterminate);
    public void setProgressUpdateInterval(int milliseconds);
    public void lock();
    public void unlock();
    public void setLocked(final boolean locked);
    public boolean isLocked();
    public void addTask(T8ComponentTask task);
    public int getExecutingTaskCount();
}
