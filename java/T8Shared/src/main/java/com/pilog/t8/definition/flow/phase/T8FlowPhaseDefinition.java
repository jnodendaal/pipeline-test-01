package com.pilog.t8.definition.flow.phase;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.phase.T8FlowPhase;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhaseDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_PHASE";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_PHASE";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "FLOW_PHASE_";
    public static final String DISPLAY_NAME = "Workflow Phase";
    public static final String DESCRIPTION = "A phase of progress in a workflow process.";
    public enum Datum {DISPLAY_NAME};
    // -------- Definition Meta-Data -------- //

    public T8FlowPhaseDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The display name of this phase."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8FlowPhase createNewPhaseInstance(T8Context context) throws Exception
    {
        return new T8FlowPhase(this);
    }

    public String getDisplayName()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_NAME.toString());
    }

    public void setDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME.toString(), displayName);
    }
}
