package com.pilog.t8.definition.ui.dialog;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentControllerDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8GlobalDialogDefinition extends T8DialogDefinition implements T8ComponentControllerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DIALOG_GLOBAL";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_DIALOG_GLOBAL";
    public static final String GROUP_NAME = "UI Dialog";
    public static final String GROUP_DESCRIPTION = "A UI dialog that contains functionality common to many different component and which is therefore defined on its own in order to be reusable.";
    public static final String DISPLAY_NAME = "Global Dialog";
    public static final String DESCRIPTION = "A UI container to which other UI components can be added all of which will then be displayed in a separate window from the main application.";
    public static final String IDENTIFIER_PREFIX = "C_DIALOG_";
    private enum Datum {INPUT_PARAMETER_DEFINITIONS,
                        OUTPUT_PARAMETER_DEFINITIONS,
                        ADDITIONAL_COMPONENT_DEFINITIONS,
                        CONTROLLER_SCRIPT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8GlobalDialogDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this dialog."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ADDITIONAL_COMPONENT_DEFINITIONS.toString(), "Additional Components", "Additional components contained by this module that perform a support function, such as dialogs."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters",  "The output data parameters returned when the dialog is closed."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CONTROLLER_SCRIPT_DEFINITIONS.toString(), "Controller Scripts",  "Scripts that can be executed within the context of this controller (Dialog)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.ADDITIONAL_COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.CONTROLLER_SCRIPT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ComponentControllerScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8GlobalDialogTestHarness", new Class<?>[]{});
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.ui.dialog.T8GlobalDialog", new Class<?>[]{T8GlobalDialogDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8GlobalDialogAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public ArrayList<T8ComponentDefinition> getAdditionalComponentDefinitions()
    {
        return getDefinitionDatum(Datum.ADDITIONAL_COMPONENT_DEFINITIONS);
    }

    public void setAdditionalComponentDefinitions(ArrayList<T8ComponentDefinition> componentDefinitions)
    {
        setDefinitionDatum(Datum.ADDITIONAL_COMPONENT_DEFINITIONS, componentDefinitions);
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public ArrayList<T8ComponentControllerScriptDefinition> getControllerScriptDefinitions()
    {
        return getDefinitionDatum(Datum.CONTROLLER_SCRIPT_DEFINITIONS);
    }

    public void setControllerScriptDefinitions(ArrayList<T8ComponentControllerScriptDefinition> scriptDefinitions)
    {
        setDefinitionDatum(Datum.CONTROLLER_SCRIPT_DEFINITIONS, scriptDefinitions);
    }
}
