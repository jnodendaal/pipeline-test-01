package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.task.T8TaskDetails;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskPriorityChangedEvent extends T8FlowEvent
{
    private final int oldPriority;
    private final int newPriority;

    public static final String TYPE_ID = "TASK_PRIORITY_CHANGED";
    
    public T8FlowTaskPriorityChangedEvent(String taskInstanceIdentifier, int oldPriority, int newPriority)
    {
        super(taskInstanceIdentifier);
        this.oldPriority = oldPriority;
        this.newPriority = newPriority;
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public int getOldPriority()
    {
        return oldPriority;
    }

    public int getNewPriority()
    {
        return newPriority;
    }

    public T8TaskDetails getTaskDetails()
    {
        return (T8TaskDetails)source;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
