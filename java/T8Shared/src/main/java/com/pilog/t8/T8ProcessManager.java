package com.pilog.t8;

import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.process.T8ProcessFilter;
import com.pilog.t8.process.T8ProcessSummary;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.process.event.T8ProcessManagerListener;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ProcessManager extends T8ServerModule
{
    /**
     * Adds a listener that will be notified of process events that occur on this manager.
     * @param listener
     */
    public void addEventListener(T8ProcessManagerListener listener);

    /**
     * Removes the specified listener from this manager.
     * @param listener
     */
    public void removeEventListener(T8ProcessManagerListener listener);

    /**
     * Returns the details of the specified process.
     * @param context
     * @param processIid
     * @return
     * @throws Exception
     */
    public T8ProcessDetails getProcessDetails(T8Context context, String processIid) throws Exception;

    /**
     * Returns a summary of the process types and counts that are available within the specified session context.
     * @param context
     * @return
     * @throws Exception
     */
    public T8ProcessSummary getProcessSummary(T8Context context) throws Exception;

    /**
     * Returns the number of executing processes of the specified type currently active and executing on the server.
     * @param context
     * @param processFilter
     * @param searchExpression
     * @return
     * @throws Exception
     */
    public int countProcesses(T8Context context, T8ProcessFilter processFilter, String searchExpression) throws Exception;

    /**
     * Searches all process states, execute and idle, and returns those that meet the specified criteria.
     * @param context
     * @param processFilter
     * @param searchExpression
     * @param pageOffset
     * @param pageSize
     * @return
     * @throws Exception
     */
    public List<T8ProcessDetails> searchProcesses(T8Context context, T8ProcessFilter processFilter, String searchExpression, int pageOffset, int pageSize) throws Exception;

    /**
     * Loads the specified process definition, creates a new instance of the process from the definition and starts its execution using the supplied input parameters.
     * @param context
     * @param processId
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public T8ProcessDetails startProcess(T8Context context, String processId, Map<String, Object> inputParameters) throws Exception;

    /**
     * Creates a new process instance from the supplied process definition and starts execution using the supplied input parameters.
     * @param context
     * @param processDefinition
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public T8ProcessDetails startProcess(T8Context context, T8ProcessDefinition processDefinition, Map<String, Object> inputParameters) throws Exception;

    /**
     * Stops the execution of the specified process as soon as possible.
     * @param context
     * @param processIid
     * @return
     * @throws Exception
     */
    public T8ProcessDetails stopProcess(T8Context context, String processIid) throws Exception;

    /**
     * Stops the execution of the specified process as soon as possible.
     * @param context
     * @param processIid
     * @return
     * @throws Exception
     */
    public T8ProcessDetails cancelProcess(T8Context context, String processIid) throws Exception;

    /**
     * Pauses the execution of the specified process as soon as possible.
     * @param context
     * @param processIid
     * @return
     * @throws Exception
     */
    public T8ProcessDetails pauseProcess(T8Context context, String processIid) throws Exception;

    /**
     * Resumes the execution of the specified previously paused process.
     * @param context
     * @param processIid
     * @return
     * @throws Exception
     */
    public T8ProcessDetails resumeProcess(T8Context context, String processIid) throws Exception;

    /**
     * Closes the specified process, finalizing its execution and removing it from the process list.
     * @param context
     * @param processIid
     * @return
     * @throws Exception
     */
    public T8ProcessDetails finalizeProcess(T8Context context, String processIid) throws Exception;
}
