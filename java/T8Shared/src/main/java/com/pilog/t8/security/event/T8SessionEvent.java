package com.pilog.t8.security.event;

import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public abstract class T8SessionEvent extends EventObject
{
    public T8SessionEvent(Object source)
    {
        super(source);
    }
}
