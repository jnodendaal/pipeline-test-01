package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.state.data.T8TaskStateDataSet;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityDefinition;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskState implements Serializable
{
    private final T8FlowNodeState parentNodeState;
    private final String projectId;
    private final String flowIid;
    private final String flowId;
    private final String nodeIid;
    private final String nodeId;
    private final String taskIid;
    private final String taskId;
    private String groupId;
    private final String initiatorId;
    private final String initiatorIid;
    private final String initiatorOrgId;
    private final String initiatorRootOrgId;
    private String restrictionUserId;
    private String restrictionProfileId;
    private String claimedByUserId;
    private T8Timestamp timeIssued;
    private T8Timestamp timeClaimed;
    private T8Timestamp timeCompleted;
    private T8Timestamp timeEscalated;
    private Integer escalationLevel;
    private Long timespanIssued;
    private Long timespanClaimed;
    private Long timespanTotal;
    private Long timespanBusinessIssued;
    private Long timespanBusinessClaimed;
    private Long timespanBusinessTotal;
    private int priority;
    private int groupIteration;
    private final T8FlowStateTaskParameterMap inputParameters;
    private final T8FlowStateTaskParameterMap outputParameters;
    private boolean updated;
    private boolean inserted;

    public T8FlowTaskState(T8FlowNodeState parentNodeState, String taskId, String taskIid)
    {
        this.parentNodeState = parentNodeState;
        this.projectId = parentNodeState.getProjectId();
        this.flowId = parentNodeState.getFlowId();
        this.flowIid = parentNodeState.getFlowIid();
        this.nodeId = parentNodeState.getNodeId();
        this.nodeIid = parentNodeState.getNodeIid();
        this.initiatorId = parentNodeState.getFlowState().getInitiatorId();
        this.initiatorIid = parentNodeState.getFlowState().getInitiatorIid();
        this.initiatorOrgId = parentNodeState.getFlowState().getInitiatorOrgId();
        this.initiatorRootOrgId = parentNodeState.getFlowState().getInitiatorRootOrgId();
        this.taskId = taskId;
        this.taskIid = taskIid;
        this.inputParameters = new T8FlowStateTaskParameterMap(this, null, null, STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, null);
        this.outputParameters = new T8FlowStateTaskParameterMap(this, null, null, STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, null);
        this.groupIteration = 0;
        this.updated = false;
        this.inserted = true;
    }

    public T8FlowTaskState(T8FlowNodeState parentNodeState, T8TaskStateDataSet dataSet)
    {
        T8DataEntity taskEntity;

        // Construct the task state.
        taskEntity = dataSet.getTaskEntity();
        this.parentNodeState = parentNodeState;
        this.projectId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_PROJECT_ID);
        this.flowId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_FLOW_ID);
        this.flowIid = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_FLOW_IID);
        this.nodeId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_NODE_ID);
        this.nodeIid = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_NODE_IID);
        this.taskIid = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TASK_IID);
        this.taskId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TASK_ID);
        this.groupId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_GROUP_ID);
        this.restrictionUserId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_RESTRICTION_USER_ID);
        this.restrictionProfileId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_RESTRICTION_PROFILE_ID);
        this.initiatorId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_INITIATOR_ID);
        this.initiatorIid = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_INITIATOR_IID);
        this.initiatorOrgId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_INITIATOR_ORG_ID);
        this.initiatorRootOrgId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_INITIATOR_ROOT_ORG_ID);
        this.claimedByUserId = (String)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_CLAIMED_BY_USER_ID);
        this.priority = (Integer)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_PRIORITY);
        this.escalationLevel = (Integer)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_ESCALATION_LEVEL);
        this.timeIssued = (T8Timestamp)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ISSUED);
        this.timeClaimed = (T8Timestamp)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_CLAIMED);
        this.timeCompleted = (T8Timestamp)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_COMPLETED);
        this.timeEscalated = (T8Timestamp)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED);
        this.timespanIssued = (Long)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_ISSUED);
        this.timespanClaimed = (Long)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_CLAIMED);
        this.timespanTotal = (Long)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_TOTAL);
        this.timespanBusinessIssued = (Long)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_BUSINESS_ISSUED);
        this.timespanBusinessClaimed = (Long)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_BUSINESS_CLAIMED);
        this.timespanBusinessTotal = (Long)taskEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_BUSINESS_TOTAL);
        this.groupIteration = 0;
        this.updated = false;
        this.inserted = false;

        // Construct the input parameters.
        this.inputParameters = new T8FlowStateTaskParameterMap(this, null, null, STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, dataSet.getInputParameterDataSet());

        // Construct the output parameters.
        this.outputParameters = new T8FlowStateTaskParameterMap(this, null, null, STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, dataSet.getOutputParameterDataSet());
    }

    public T8TaskDetails getTaskDetails(String languageId, T8TaskActivityDefinition nodeDefinition, T8FlowTaskDefinition taskDefinition)
    {
        T8TaskDetails taskDetails;

        // Create a new task details object.
        taskDetails = new T8TaskDetails(getTaskIid());
        taskDetails.setRestrictionUserId(getRestrictionUserId());
        taskDetails.setRestrictionProfileId(getRestrictionProfileId());
        taskDetails.setProjectId(taskDefinition.getRootProjectId());
        taskDetails.setFlowId(getFlowId());
        taskDetails.setFlowIid(getFlowIid());
        taskDetails.setTaskId(getTaskId());
        taskDetails.setTaskDisplayName(taskDefinition.getName());
        taskDetails.setTaskDescription(taskDefinition.getDescription());
        taskDetails.setFunctionalityId(taskDefinition.getFunctionalityId());
        taskDetails.setClaimed(isClaimed());
        taskDetails.setClaimedByUserId(getClaimedByUserId());
        taskDetails.setTaskInputParameters(T8IdentifierUtilities.setNamespace(taskId, getInputParameters()));
        taskDetails.setFlowParameters(parentNodeState != null ? parentNodeState.getExecutionParameters() : null);
        taskDetails.setTimeIssued(getTimeIssuedMilliseconds());
        taskDetails.setTimeClaimed(getTimeClaimedMilliseconds());
        taskDetails.setTimeEscalated(getTimeEscalatedMilliseconds());
        taskDetails.setPriority(getPriority());
        taskDetails.setSubsequentFollowUp(nodeDefinition.isSubsequentFollowUp());

        // Return the compiled task details.
        return taskDetails;
    }

    public T8DataEntity createEntity(T8DataEntityDefinition entityDefinition)
    {
        String entityIdentifier;
        T8DataEntity newEntity;

        entityIdentifier = entityDefinition.getIdentifier();
        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityIdentifier + F_PROJECT_ID, parentNodeState != null ? parentNodeState.getProjectId() : this.projectId);
        newEntity.setFieldValue(entityIdentifier + F_FLOW_IID, parentNodeState != null ? parentNodeState.getFlowIid() : this.flowIid);
        newEntity.setFieldValue(entityIdentifier + F_FLOW_ID, parentNodeState != null ? parentNodeState.getFlowId() : this.flowId);
        newEntity.setFieldValue(entityIdentifier + F_NODE_IID, parentNodeState != null ? parentNodeState.getNodeIid() : this.nodeIid);
        newEntity.setFieldValue(entityIdentifier + F_NODE_ID, parentNodeState != null ? parentNodeState.getNodeId() : this.nodeId);
        newEntity.setFieldValue(entityIdentifier + F_TASK_IID, taskIid);
        newEntity.setFieldValue(entityIdentifier + F_TASK_ID, taskId);
        newEntity.setFieldValue(entityIdentifier + F_GROUP_ID, groupId);
        newEntity.setFieldValue(entityIdentifier + F_RESTRICTION_USER_ID, restrictionUserId);
        newEntity.setFieldValue(entityIdentifier + F_RESTRICTION_PROFILE_ID, restrictionProfileId);
        newEntity.setFieldValue(entityIdentifier + F_INITIATOR_ID, parentNodeState != null ? parentNodeState.getFlowState().getInitiatorId() : this.initiatorId);
        newEntity.setFieldValue(entityIdentifier + F_INITIATOR_IID, parentNodeState != null ? parentNodeState.getFlowState().getInitiatorIid() : this.initiatorIid);
        newEntity.setFieldValue(entityIdentifier + F_INITIATOR_ORG_ID, parentNodeState != null ? parentNodeState.getFlowState().getInitiatorOrgId() : this.initiatorOrgId);
        newEntity.setFieldValue(entityIdentifier + F_INITIATOR_ROOT_ORG_ID, parentNodeState != null ? parentNodeState.getFlowState().getInitiatorRootOrgId() : this.initiatorRootOrgId);
        newEntity.setFieldValue(entityIdentifier + F_CLAIMED_BY_USER_ID, claimedByUserId);
        newEntity.setFieldValue(entityIdentifier + F_PRIORITY, priority);
        newEntity.setFieldValue(entityIdentifier + F_ESCALATION_LEVEL, escalationLevel);
        newEntity.setFieldValue(entityIdentifier + F_TIME_ISSUED, timeIssued);
        newEntity.setFieldValue(entityIdentifier + F_TIME_CLAIMED, timeClaimed);
        newEntity.setFieldValue(entityIdentifier + F_TIME_COMPLETED, timeCompleted);
        newEntity.setFieldValue(entityIdentifier + F_TIME_ESCALATED, timeEscalated);
        newEntity.setFieldValue(entityIdentifier + F_TIME_SPAN_ISSUED, timespanIssued);
        newEntity.setFieldValue(entityIdentifier + F_TIME_SPAN_CLAIMED, timespanClaimed);
        newEntity.setFieldValue(entityIdentifier + F_TIME_SPAN_TOTAL, timespanTotal);
        newEntity.setFieldValue(entityIdentifier + F_TIME_SPAN_BUSINESS_ISSUED, timespanBusinessIssued);
        newEntity.setFieldValue(entityIdentifier + F_TIME_SPAN_BUSINESS_CLAIMED, timespanBusinessClaimed);
        newEntity.setFieldValue(entityIdentifier + F_TIME_SPAN_BUSINESS_TOTAL, timespanBusinessTotal);
        newEntity.setFieldValue(entityIdentifier + F_GROUP_ITERATION, groupIteration);
        return newEntity;
    }

    public T8FlowNodeState getParentNodeState()
    {
        return parentNodeState;
    }

    public boolean isUpdated()
    {
        return updated;
    }

    public void statePersisted()
    {
        // Reset this state's flags.
        inserted = false;
        updated = false;

        // Reset the input parameter map flags.
        inputParameters.statePersisted();

        // Reset the output parameter map flags.
        outputParameters.statePersisted();
    }

    public void addUpdates(T8FlowStateUpdates updates)
    {
        // Add this node states updates.
        if (inserted) updates.addInsert(createEntity(updates.getTaskStateEntityDefinition()));
        else if (updated) updates.addUpdate(createEntity(updates.getTaskStateEntityDefinition()));

        // Add input parameter updates.
        inputParameters.addUpdates(updates.getTaskStateInputParameterEntityDefinition(), updates);

        // Add output parameter updates.
        outputParameters.addUpdates(updates.getTaskStateOutputParameterEntityDefinition(), updates);
    }

    public String getProjectId()
    {
        return projectId;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public String getFlowId()
    {
        return flowId;
    }

    public String getNodeIid()
    {
        return nodeIid;
    }

    public String getNodeId()
    {
        return nodeId;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        if (!Objects.equals(this.groupId, groupId))
        {
            updated = true;
            this.groupId = groupId;
        }
    }

    public String getRestrictionUserId()
    {
        return restrictionUserId;
    }

    public void setRestrictionUserId(String restrictionUserId)
    {
        if (!Objects.equals(this.restrictionUserId, restrictionUserId))
        {
            updated = true;
            this.restrictionUserId = restrictionUserId;
        }
    }

    public String getRestrictionProfileId()
    {
        return restrictionProfileId;
    }

    public void setRestrictionProfileId(String restrictionProfileId)
    {
        if (!Objects.equals(this.restrictionProfileId, restrictionProfileId))
        {
            updated = true;
            this.restrictionProfileId = restrictionProfileId;
        }
    }

    public String getClaimedByUserId()
    {
        return claimedByUserId;
    }

    public void setClaimedByUserId(String claimedByUserId)
    {
        if (!Objects.equals(this.claimedByUserId, claimedByUserId))
        {
            updated = true;
            this.claimedByUserId = claimedByUserId;
        }
    }

    public boolean isClaimed()
    {
        return ((timeClaimed != null) && (timeClaimed.getMilliseconds() > 0));
    }

    public T8Timestamp getTimeIssued()
    {
        return timeIssued;
    }

    public Long getTimeIssuedMilliseconds()
    {
        return timeIssued != null ? timeIssued.getMilliseconds() : null;
    }

    public void setTimeIssued(T8Timestamp timeIssued)
    {
        if (!Objects.equals(this.timeIssued, timeIssued))
        {
            updated = true;
            this.timeIssued = timeIssued;
        }
    }

    public boolean isIssued()
    {
        return ((timeIssued != null) && (timeIssued.getMilliseconds() > 0));
    }

    public T8Timestamp getTimeClaimed()
    {
        return timeClaimed;
    }

    public Long getTimeClaimedMilliseconds()
    {
        return timeClaimed != null ? timeClaimed.getMilliseconds() : null;
    }

    public void setTimeClaimed(T8Timestamp timeClaimed)
    {
        if (!Objects.equals(this.timeClaimed, timeClaimed))
        {
            updated = true;
            this.timeClaimed = timeClaimed;
        }
    }

    public T8Timestamp getTimeCompleted()
    {
        return timeCompleted;
    }

    public void setTimeCompleted(T8Timestamp timeCompleted)
    {
        if (!Objects.equals(this.timeCompleted, timeCompleted))
        {
            updated = true;
            this.timeCompleted = timeCompleted;
        }
    }

    public T8Timestamp getTimeEscalated()
    {
        return timeEscalated;
    }

    public void setTimeEscalated(T8Timestamp timeEscalated)
    {
        if (!Objects.equals(this.timeEscalated, timeEscalated))
        {
            updated = true;
            this.timeEscalated = timeEscalated;
        }
    }

    public Long getTimeEscalatedMilliseconds()
    {
        return timeEscalated != null ? timeEscalated.getMilliseconds() : null;
    }

    public Integer getEscalationLevel()
    {
        return escalationLevel != null ? escalationLevel : null;
    }

    public void setEscalationLevel(Integer escalationLevel)
    {
        if (!Objects.equals(this.escalationLevel, escalationLevel))
        {
            updated = true;
            this.escalationLevel = escalationLevel;
        }
    }

    public Long getTimespanIssued()
    {
        return timespanIssued != null ? timespanIssued : null;
    }

    public void setTimespanIssued(Long timespanIssued)
    {
        if (!Objects.equals(this.timespanIssued, timespanIssued))
        {
            updated = true;
            this.timespanIssued = timespanIssued;
        }
    }

    public Long getTimespanClaimed()
    {
        return timespanClaimed != null ? timespanClaimed : null;
    }

    public void setTimespanClaimed(Long timespanClaimed)
    {
        if (!Objects.equals(this.timespanClaimed, timespanClaimed))
        {
            updated = true;
            this.timespanClaimed = timespanClaimed;
        }
    }

    public Long getTimespanTotal()
    {
        return timespanTotal != null ? timespanTotal : null;
    }

    public void setTimespanTotal(Long timespanTotal)
    {
        if (!Objects.equals(this.timespanTotal, timespanTotal))
        {
            updated = true;
            this.timespanTotal = timespanTotal;
        }
    }

    public Long getTimespanBusinessIssued()
    {
        return timespanBusinessIssued != null ? timespanBusinessIssued : null;
    }

    public void setTimespanBusinessIssued(Long timespanBusinessIssued)
    {
        if (!Objects.equals(this.timespanBusinessIssued, timespanBusinessIssued))
        {
            updated = true;
            this.timespanBusinessIssued = timespanBusinessIssued;
        }
    }

    public Long getTimespanBusinessClaimed()
    {
        return timespanBusinessClaimed != null ? timespanBusinessClaimed : null;
    }

    public void setTimespanBusinessClaimed(Long timespanBusinessClaimed)
    {
        if (!Objects.equals(this.timespanBusinessClaimed, timespanBusinessClaimed))
        {
            updated = true;
            this.timespanBusinessClaimed = timespanBusinessClaimed;
        }
    }

    public Long getTimespanBusinessTotal()
    {
        return timespanBusinessTotal != null ? timespanBusinessTotal : null;
    }

    public void setTimespanBusinessTotal(Long timespanBusinessTotal)
    {
        if (!Objects.equals(this.timespanBusinessTotal, timespanBusinessTotal))
        {
            updated = true;
            this.timespanBusinessTotal = timespanBusinessTotal;
        }
    }

    public boolean isCompleted()
    {
        return ((timeCompleted != null) && (timeCompleted.getMilliseconds() > 0));
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        if (!Objects.equals(this.priority, priority))
        {
            updated = true;
            this.priority = priority;
        }
    }

    public int getGroupIteration()
    {
        return groupIteration;
    }

    public void setGroupIteration(int groupIteration)
    {
        if (!Objects.equals(this.groupIteration, groupIteration))
        {
            updated = true;
            this.groupIteration = groupIteration;
        }
    }

    public T8FlowStateParameterMap getInputParameters()
    {
        return inputParameters;
    }

    public void setInputParameters(Map<String, Object> inputParameters)
    {
        this.inputParameters.clear();
        if (inputParameters != null)
        {
            this.inputParameters.putAll(inputParameters);
        }
    }

    public T8FlowStateParameterMap getOutputParameters()
    {
        return outputParameters;
    }

    public void setOutputParameters(Map<String, Object> outputParameters)
    {
        this.outputParameters.clear();
        if (outputParameters != null)
        {
            this.outputParameters.putAll(outputParameters);
        }
    }

    public int getTotalInputParameterCount()
    {
        return inputParameters.getTotalParameterCount();
    }

    public int getTotalOutputParameterCount()
    {
        return outputParameters.getTotalParameterCount();
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public String getInitiatorIid()
    {
        return initiatorIid;
    }

    public String getInitiatorOrgId()
    {
        return initiatorOrgId;
    }

    public String getInitiatorRootOrgId()
    {
        return initiatorRootOrgId;
    }

    /**
     * Returns the task that was completed at the latest instant but preceding the time that this task was issued.
     * @return The task that was completed at the latest instance preceding the time of issue for this task.
     */
    public T8FlowTaskState getPrecedingTaskState()
    {
        if (parentNodeState != null)
        {
            T8FlowState flowState;

            flowState = parentNodeState.getFlowState();
            return flowState != null ? flowState.getPrecedingTaskState(taskIid) : null;
        }
        else return null;
    }

    public void recalculateTimespans(T8OperationalHoursCalculator timeCalculator)
    {
        recalculateTimespanIssued(timeCalculator);
        recalculateTimespanClaimed(timeCalculator);
        recalculateTimespanTotal();
    }

    public void recalculateTimespanIssued(T8OperationalHoursCalculator timeCalculator)
    {
        // Compute and determine times and timespans applicable.
        if (timeClaimed != null)
        {
            timespanIssued = timeClaimed.getMilliseconds() - timeIssued.getMilliseconds();
            timespanBusinessIssued = timeCalculator != null ? timeCalculator.calculateOperationalTimeDifference(timeIssued.getMilliseconds(), timeClaimed.getMilliseconds()) : timespanIssued;
        }
        else
        {
            timespanIssued = null;
            timespanBusinessIssued = null;
        }
    }

    public void recalculateTimespanClaimed(T8OperationalHoursCalculator timeCalculator)
    {
        // Compute and determine times and timespans applicable.
        if (timeCompleted != null)
        {
            timespanClaimed = timeCompleted.getMilliseconds() - timeClaimed.getMilliseconds();
            timespanBusinessClaimed = timeCalculator != null ? timeCalculator.calculateOperationalTimeDifference(timeClaimed.getMilliseconds(), timeCompleted.getMilliseconds()) : timespanClaimed;
        }
        else
        {
            timespanClaimed = null;
            timespanBusinessClaimed = null;
        }
    }

    public void recalculateTimespanTotal()
    {
        // Compute and determine times and timespans applicable.
        timespanTotal = (timespanIssued + timespanClaimed);
        timespanBusinessTotal = (timespanBusinessIssued + timespanBusinessClaimed);
    }

    @Override
    public String toString()
    {
        return "T8FlowTaskState{" + "parentNodeState=" + parentNodeState + ", flowIid=" + flowIid + ", flowId=" + flowId + ", nodeIid=" + nodeIid + ", nodeId=" + nodeId + ", taskIid=" + taskIid + ", taskId=" + taskId + ", groupId=" + groupId + ", groupIteration=" + groupIteration + '}';
    }
}
