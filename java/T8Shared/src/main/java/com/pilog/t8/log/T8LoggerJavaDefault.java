package com.pilog.t8.log;

import com.pilog.t8.utilities.strings.Strings;
import java.util.logging.Logger;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8LoggerJavaDefault extends T8Logger
{
    @SuppressWarnings("NonConstantLogger")
    private final Logger LOG;

    public T8LoggerJavaDefault(String prefixID, String callerName, T8Logger.Level level)
    {
        super(callerName);
        LOG = Logger.getLogger("pilog"+(Strings.isNullOrEmpty(prefixID) ? "" : ":"+prefixID));
        LOG.setLevel(convertLoggingLevel(level));
    }

    @Override
    public void log(Level level, String message)
    {
        LOG.logp(convertLoggingLevel(level), callerName, "", message);
    }

    @Override
    public void log(Level level, String message, Throwable exception)
    {
        LOG.log(convertLoggingLevel(level), message, exception);
        if(exception.getCause() != null)
        {
            log(level, message,exception.getCause());
        }
    }

    @Override
    public void log(Level level, LogMessage message, Throwable exception)
    {
        LOG.log(convertLoggingLevel(level), exception, message);
        if(exception.getCause() != null)
        {
            log(level, message, exception.getCause());
        }
    }

    @Override
    public void log(Level level, LogMessage message)
    {
        LOG.log(convertLoggingLevel(level), message);
    }

    private java.util.logging.Level convertLoggingLevel(T8Logger.Level level)
    {
        switch (level)
        {
            case DEBUG:
            {
                return java.util.logging.Level.INFO;
            }
            case INFO:
            {
                return java.util.logging.Level.INFO;
            }
            case WARNING:
            {
                return java.util.logging.Level.WARNING;
            }
            case ERROR:
            {
                return java.util.logging.Level.SEVERE;
            }
            case FATAL:
            {
                return java.util.logging.Level.SEVERE;
            }
            default:
            {
                return java.util.logging.Level.FINEST;
            }
        }
    }
}
