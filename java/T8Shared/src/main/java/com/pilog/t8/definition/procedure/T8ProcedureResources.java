package com.pilog.t8.definition.procedure;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ProcedureResources implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_EXECUTE_PROCEDURE = "@OS_EXECUTE_PROCEDURE";

    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_PROCEDURE_IDENTIFIER = "$P_PROCEDURE_IDENTIFIER";
    public static final String PARAMETER_PROCEDURE_DEFINITION = "$P_PROCEDURE_DEFINITION";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_OUTPUT_PARAMETERS = "$P_OUTPUT_PARAMETERS";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        //  Returns the complete list of definitions.
        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_EXECUTE_PROCEDURE);
        definition.setMetaDisplayName("Execute Procedure");
        definition.setMetaDescription("Executes a server-side procedure and returns the results..");
        definition.setClassName("com.pilog.t8.procedure.T8ProcedureOperations$ExecuteProcedureOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the procedure to execute is located.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROCEDURE_IDENTIFIER, "Procedure Identifier", "The identifier of the procedure to execute.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETERS, "Input Parameters", "The input parameters to supply to the procedure.", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OUTPUT_PARAMETERS, "Output Parameters", "The output parametesr returned by the procedure.", T8DataType.MAP));
        definitions.add(definition);

        return definitions;
    }
}
