package com.pilog.t8.notification;

import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8NotificationGenerator
{
    public List<T8Notification> generateNotifications(T8Context context, Map<String, ? extends Object> inputParameters) throws Exception;
}
