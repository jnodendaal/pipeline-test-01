package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.file.T8FileData;
import com.pilog.t8.utilities.codecs.Base64Codec;

/**
 * @author Bouwer du Preez
 */
public class T8DtFileData extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FILE_DATA";

    public T8DtFileData() {}

    public T8DtFileData(T8DefinitionManager context) {}

    public T8DtFileData(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FileData.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject jsonFileData;
            T8FileData fileData;
            byte[] data;

            // Create the JSON object.
            fileData = (T8FileData)object;
            data = fileData.getData();
            jsonFileData = new JsonObject();
            jsonFileData.add("filename", fileData.getFilename());
            jsonFileData.add("data", data != null ? Base64Codec.encode(data) : null);

            // Return the final JSON object.
            return jsonFileData;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8FileData deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject jsonFileData;
            T8FileData fileData;
            String dataString;

            // Create the file details object.
            jsonFileData = jsonValue.asObject();
            dataString = jsonFileData.getString("data");
            fileData = new T8FileData();
            fileData.setFilename(jsonFileData.getString("filename"));
            fileData.setData(dataString != null ? Base64Codec.decode(dataString) : null);

            // Return the completed object.
            return fileData;
        }
        else return null;
    }
}
