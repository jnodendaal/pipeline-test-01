package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public abstract class T8ConnectionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_REMOTE_SERVER_CONNECTION_DETAILS";
    public static final String STORAGE_PATH = "/remote_server_connection";
    public static final String IDENTIFIER_PREFIX = "CONNECTION_";
    public enum Datum
    {
        CONNECTION_PROTOCOL,
        SERVER_HOST_NAME,
        SERVER_PORT,
        SERVER_CONTEXT_URL
    };
    // -------- Definition Meta-Data -------- //

    public static enum ConnectionProtocol
    {
        HTTP("http://"),
        HTTPS("https://");

        private final String protocolPrefix;

        private ConnectionProtocol(String protocolPrefix)
        {
            this.protocolPrefix = protocolPrefix;
        }

        public String getProtocolPrefix()
        {
            return protocolPrefix;
        }
    }

    public T8ConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract List<T8RemoteUserDefinition> getRemoteUserDefinitions();

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONNECTION_PROTOCOL.toString(), "Protocol", "The protocol to be used by this remote connection.", ConnectionProtocol.HTTP.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SERVER_HOST_NAME.toString(), "Host Name", "The hostname of the server on which the Tech 8 Instance is listening."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.SERVER_PORT.toString(), "Port", "The port number of the server on which the Tech 8 Instance is listening."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SERVER_CONTEXT_URL.toString(), "Context URL", "The context URL of the Tech 8 instance to call.", "/t8/api_operation"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.CONNECTION_PROTOCOL.toString().equals(datumIdentifier)) return createStringOptions(ConnectionProtocol.values());
        else return null;
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (getConnectionProtocol() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.CONNECTION_PROTOCOL.toString(), "Connection Protocol is Empty", T8DefinitionValidationError.ErrorType.CRITICAL));
        if (getServerHostName() == null || getServerHostName().isEmpty()) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.SERVER_HOST_NAME.toString(), "Hostname is Empty", T8DefinitionValidationError.ErrorType.CRITICAL));
        if (getServerContextURL()== null || getServerContextURL().isEmpty()) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.SERVER_CONTEXT_URL.toString(), "Context URL is Empty", T8DefinitionValidationError.ErrorType.CRITICAL));
        if (getServerContextURL() != null && !getServerContextURL().isEmpty())
        {
            if (getServerContextURL().startsWith("/")) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.SERVER_CONTEXT_URL.toString(), "Context URL should be without the \"/\" prefix", T8DefinitionValidationError.ErrorType.CRITICAL));
            if (getServerContextURL().endsWith("/")) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.SERVER_CONTEXT_URL.toString(), "Context URL should be without the \"/\" suffix", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    public abstract T8Connection getNewConnectionInstance(T8Context context);

    public String getURL()
    {
        StringBuilder urlBuilder;
        String contextURL;
        Integer port;

        // Get the details of the connection.
        port = getServerPort();
        contextURL = getServerContextURL();

        // Create the start of the URL.
        urlBuilder = new StringBuilder();
        urlBuilder.append(getConnectionProtocol().getProtocolPrefix());
        urlBuilder.append(getServerHostName());

        // Append the port if required.
        if (port != null)
        {
            urlBuilder.append(":");
            urlBuilder.append(port);
        }

        // Append the context URL if required.
        if (!Strings.isNullOrEmpty(contextURL))
        {
            urlBuilder.append("/");
            urlBuilder.append(contextURL);
        }

        return urlBuilder.toString();
    }

    public ConnectionProtocol getConnectionProtocol()
    {
        return ConnectionProtocol.valueOf(getDefinitionDatum(Datum.CONNECTION_PROTOCOL));
    }

    public void setConnectionProtocol(ConnectionProtocol protocol)
    {
        setDefinitionDatum(Datum.CONNECTION_PROTOCOL, protocol);
    }

    public String getServerHostName()
    {
        return getDefinitionDatum(Datum.SERVER_HOST_NAME);
    }

    public void setServerHostName(String serverHostName)
    {
        setDefinitionDatum(Datum.SERVER_HOST_NAME, serverHostName);
    }

    public Integer getServerPort()
    {
        return getDefinitionDatum(Datum.SERVER_PORT);
    }

    public void setServerPort(Integer serverPort)
    {
        setDefinitionDatum(Datum.SERVER_PORT, serverPort);
    }

    public String getServerContextURL()
    {
        return getDefinitionDatum(Datum.SERVER_CONTEXT_URL);
    }

    public void setServerContextURL(String serverContext)
    {
        setDefinitionDatum(Datum.SERVER_CONTEXT_URL, serverContext);
    }
}
