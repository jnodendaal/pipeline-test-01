package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.time.T8Hour;

/**
 * @author Bouwer du Preez
 */
public class T8DtHour extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@HOUR";

    public T8DtHour()
    {
    }

    public T8DtHour(T8DefinitionManager context)
    {
    }

    public T8DtHour(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8Hour.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject jsonMinute;
            T8Hour hour;

            hour = (T8Hour)object;
            jsonMinute = new JsonObject();
            jsonMinute.add("type", IDENTIFIER);
            jsonMinute.add("amount", hour.getAmount());
            return jsonMinute;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject jsonHour;

            jsonHour = jsonValue.asObject();
            return new T8Hour(jsonHour.getLong("amount", 0));
        }
        else return null;
    }
}
