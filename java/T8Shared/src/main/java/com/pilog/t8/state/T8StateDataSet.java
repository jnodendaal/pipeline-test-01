package com.pilog.t8.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8StateDataSet
{
    private final Map<String, List<T8DataEntity>> dataEntities;

    public T8StateDataSet()
    {
        dataEntities = new HashMap<String, List<T8DataEntity>>();
    }

    public synchronized int getEntityCount()
    {
        int count;

        count = 0;
        for (List<T8DataEntity> entityList : dataEntities.values())
        {
            count += entityList.size();
        }

        return count;
    }

    public synchronized void addData(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            addData(entity);
        }
    }

    public synchronized void addData(T8DataEntity entity)
    {
        List<T8DataEntity> entityList;
        String entityIdentifier;

        entityIdentifier = entity.getIdentifier();
        entityList = dataEntities.get(entityIdentifier);
        if (entityList == null)
        {
            entityList = new ArrayList<T8DataEntity>();
            entityList.add(entity);
            dataEntities.put(entityIdentifier, entityList);
        }
        else
        {
            entityList.add(entity);
        }
    }

    public synchronized int getEntityCount(String entityId)
    {
        List<T8DataEntity> entityList;

        entityList = dataEntities.get(entityId);
        return entityList != null ? entityList.size() : 0;
    }

    public synchronized List<T8DataEntity> getEntities(String entityId)
    {
        List<T8DataEntity> entities;

        entities = dataEntities.get(entityId);
        return entities != null ? new ArrayList<>(entities) : new ArrayList<>();
    }

    public synchronized List<T8DataEntity> getEntities(String entityId, Map<String, Object> key)
    {
        List<T8DataEntity> entityList;

        entityList = dataEntities.get(entityId);
        if (entityList != null)
        {
            return T8DataUtilities.findDataEntities(entityList, key);
        }
        else return new ArrayList<>();
    }

    public synchronized List<T8DataEntity> getEntities(String entityIdentifier, String keyFieldIdentifier, Object keyValue)
    {
        List<T8DataEntity> entityList;

        entityList = dataEntities.get(entityIdentifier);
        if (entityList != null)
        {
            return T8DataUtilities.findDataEntities(entityList, keyFieldIdentifier, keyValue);
        }
        else return new ArrayList<>();
    }

    public synchronized T8DataEntity getEntity(String entityIdentifier, Map<String, Object> key)
    {
        List<T8DataEntity> entityList;

        entityList = dataEntities.get(entityIdentifier);
        if (entityList != null)
        {
            List<T8DataEntity> entities;

            entities = T8DataUtilities.findDataEntities(entityList, key);
            return entities.size() > 0 ? entities.get(0) : null;
        }
        else return null;
    }

    public synchronized T8DataEntity getEntity(String entityIdentifier, String keyFieldIdentifier, Object keyValue)
    {
        List<T8DataEntity> entityList;

        entityList = dataEntities.get(entityIdentifier);
        if (entityList != null)
        {
            List<T8DataEntity> entities;

            entities = T8DataUtilities.findDataEntities(entityList, keyFieldIdentifier, keyValue);
            return entities.size() > 0 ? entities.get(0) : null;
        }
        else return null;
    }

    public synchronized <T> List<T> getFieldValues(String entityIdentifier, Map<String, Object> key, String fieldIdentifier)
    {
        List<T8DataEntity> entityList;

        entityList = dataEntities.get(entityIdentifier);
        if (entityList != null)
        {
            List<T8DataEntity> entities;

            entities = T8DataUtilities.findDataEntities(entityList, key);
            if (entities.size() > 0)
            {
                return T8DataUtilities.getEntityFieldValues(entities, fieldIdentifier);
            }
            else return new ArrayList<>();
        }
        else return new ArrayList<>();
    }
}
