package com.pilog.t8.flow;

import com.pilog.t8.flow.T8FlowNode.FlowNodeStatus;
import com.pilog.t8.flow.task.T8FlowTaskStatus;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowNodeStatus implements Serializable
{
    private String nodeIdentifier;
    private FlowNodeStatus status;
    private int progress;
    private String statusMessage;
    private T8FlowTaskStatus taskStatus;
    
    public T8FlowNodeStatus(String nodeIdentifier, FlowNodeStatus status, int progress, String statusMessage)
    {
        this.nodeIdentifier = nodeIdentifier;
        this.status = status;
        this.progress = progress;
        this.statusMessage = statusMessage;
    }

    public String getNodeIdentifier()
    {
        return nodeIdentifier;
    }

    public void setNodeIdentifier(String nodeIdentifier)
    {
        this.nodeIdentifier = nodeIdentifier;
    }

    public FlowNodeStatus getStatus()
    {
        return status;
    }

    public void setStatus(FlowNodeStatus status)
    {
        this.status = status;
    }

    public int getProgress()
    {
        return progress;
    }

    public void setProgress(int progress)
    {
        this.progress = progress;
    }

    public String getStatusMessage()
    {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage)
    {
        this.statusMessage = statusMessage;
    }

    public T8FlowTaskStatus getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(T8FlowTaskStatus taskStatus)
    {
        this.taskStatus = taskStatus;
    }
}
