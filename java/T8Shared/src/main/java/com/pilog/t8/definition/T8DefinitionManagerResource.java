package com.pilog.t8.definition;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GET_SYSTEM_DETAILS = "@OS_GET_SYSTEM_DETAILS";
    public static final String OPERATION_INITIALIZE_DEFINITION = "@OS_INITIALIZE_DEFINITION";
    public static final String OPERATION_GET_INITIALIZED_DEFINITION = "@OS_GET_INITIALIZED_DEFINITION";
    public static final String OPERATION_RENAME_DEFINITION = "@OS_RENAME_DEFINITION";
    public static final String OPERATION_REMOVE_DEFINITION_REFERENCES = "@OS_REMOVE_DEFINITION_REFERENCES";
    public static final String OPERATION_COPY_DEFINITION = "@OS_COPY_DEFINITION";
    public static final String OPERATION_CREATE_NEW_DEFINITION = "@OS_CREATE_NEW_DEFINITION";
    public static final String OPERATION_GET_RESOLVED_DEFINITION = "@OS_GET_RESOLVED_DEFINITION";
    public static final String OPERATION_GET_RAW_DEFINITION = "@OS_GET_RAW_DEFINITION";
    public static final String OPERATION_GET_RAW_GROUP_DEFINITIONS = "@OS_GET_RAW_GROUP_DEFINITIONS";
    public static final String OPERATION_GET_INITIALIZED_GROUP_DEFINITIONS = "@OS_GET_INITIALIZED_GROUP_DEFINITIONS";
    public static final String OPERATION_DELETE_DEFINITION = "@OS_DELETE_DEFINITION";
    public static final String OPERATION_SAVE_DEFINITION = "@OS_SAVE_DEFINITION";
    public static final String OPERATION_FINALIZE_DEFINITION = "@OS_FINALIZE_DEFINITION";
    public static final String OPERATION_RELOAD_DEFINITIONS = "@OS_RELOAD_DEFINITIONS";
    public static final String OPERATION_TAG_DEFINITIONS = "@OS_TAG_DEFINITIONS";
    public static final String OPERATION_UNTAG_DEFINITIONS = "@OS_UNTAG_DEFINITIONS";
    public static final String OPERATION_MOVE_DEFINITION_TO_PROJECT = "@OS_MOVE_DEFINITION_TO_PROJECT";
    public static final String OPERATION_COPY_DEFINITION_TO_PROJECT = "@OS_COPY_DEFINITION_TO_PROJECT";
    public static final String OPERATION_CHECK_ID_AVAILABILITY = "@OS_CHECK_ID_AVAILABILITY";
    public static final String OPERATION_GET_DEFINITION_IDS = "@OS_GET_DEFINITION_IDS";
    public static final String OPERATION_GET_GROUP_DEFINITION_IDS = "@OS_GET_GROUP_DEFINITION_IDS";
    public static final String OPERATION_GET_DEFINITION_GROUP_IDS = "@OS_GET_DEFINITION_GROUP_IDS";
    public static final String OPERATION_GET_DEFINITION_TYPE_IDS = "@OS_GET_DEFINITION_TYPE_IDS";
    public static final String OPERATION_GET_DEFINITION_COMPOSITION = "@OS_GET_DEFINITION_COMPOSITION";
    public static final String OPERATION_GET_DEFINITION_META_DATA = "@OS_GET_DEFINITION_META_DATA";
    public static final String OPERATION_GET_ALL_DEFINITION_META_DATA = "@OS_GET_ALL_DEFINITION_META_DATA";
    public static final String OPERATION_GET_PROJECT_DEFINITION_META_DATA = "@OS_OPERATION_GET_PROJECT_DEFINITION_META_DATA";
    public static final String OPERATION_GET_TYPE_DEFINITION_META_DATA = "@OS_OPERATION_GET_TYPE_DEFINITION_META_DATA";
    public static final String OPERATION_GET_GROUP_DEFINITION_META_DATA = "@OS_OPERATION_GET_GROUP_DEFINITION_META_DATA";
    public static final String OPERATION_GET_DEFINITION_TYPE_META_DATA = "@OS_GET_DEFINITION_TYPE_META_DATA";
    public static final String OPERATION_GET_GROUP_DEFINITION_TYPE_META_DATA = "@OS_GET_GROUP_DEFINITION_TYPE_META_DATA";
    public static final String OPERATION_GET_ALL_DEFINITION_GROUP_META_DATA = "@OS_GET_ALL_DEFINITION_GROUP_META_DATA";
    public static final String OPERATION_GET_ALL_DEFINITION_TYPE_META_DATA = "@OS_GET_ALL_DEFINITION_TYPE_META_DATA";
    public static final String OPERATION_FIND_DEFINITIONS_BY_TEXT = "@OS_FIND_DEFINITIONS_BY_TEXT";
    public static final String OPERATION_FIND_DEFINITIONS_BY_REGULAR_EXPRESSION = "@OS_FIND_DEFINITIONS_BY_REGULAR_EXPRESSION";
    public static final String OPERATION_FIND_DEFINITIONS_BY_ID = "@OS_FIND_DEFINITIONS_BY_ID";
    public static final String OPERATION_GET_ALL_DEFINITION_HANDLES = "@OS_GET_ALL_DEFINITION_HANDLES";
    public static final String OPERATION_IMPORT_DEFINITION = "@OS_IMPORT_DEFINITION";
    public static final String OPERATION_LOCK_DEFINITION = "@OS_LOCK_DEFINITION";
    public static final String OPERATION_UNLOCK_DEFINITION = "@OS_UNLOCK_DEFINITION";
    public static final String OPERATION_GET_DEFINITION_LOCK_DETAILS = "@OS_GET_DEFINITION_LOCK_DETAILS";
    public static final String OPERATION_TRANSFER_DEFINITION_REFERENCES = "@OS_TRANSFER_DEFINITION_REFERENCES";
    public static final String OPERATION_CREATE_DATA_TYPE = "@OS_CREATE_DATA_TYPE";
    public static final String OPERATION_EXPORT_PROJECTS = "@OP_DEF_EXPORT_PROJECTS";
    public static final String OPERATION_IMPORT_PROJECTS = "@OP_DEF_IMPORT_PROJECTS";
    public static final String OPERATION_DELETE_PROJECTS = "@OP_DEF_DELETE_PROJECTS";

    public static final String PARAMETER_SYSTEM_DETAILS = "$P_SYSTEM_DETAILS";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_SAFE_DELETION = "$P_SAFE_DELETION";
    public static final String PARAMETER_TEXT = "$P_TEXT";
    public static final String PARAMETER_INCLUDE_DESCENDANTS = "$P_INCLUDE_DESCENDANTS";
    public static final String PARAMETER_INCLUDE_REFERENCES_ONLY = "$P_INCLUDE_REFERENCES_ONLY";
    public static final String PARAMETER_REGULAR_EXPRESSION = "$P_REGULAR_EXPRESSION";
    public static final String PARAMETER_DATA_TYPE = "$P_DATA_TYPE";
    public static final String PARAMETER_DATUM_TYPE_ID = "$P_DATUM_TYPE_ID";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_DEFINITION_CONSTRUCTOR_PARAMETER_LIST = "$P_DEFINITION_CONSTRUCTOR_PARAMETER_LIST";
    public static final String PARAMETER_KEY_ID = "$P_KEY_ID";
    public static final String PARAMETER_PROJECT_IDS = "$P_PROJECT_IDS";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_PROJECT_IMPORT_REPORT = "$P_PROJECT_IMPORT_REPORT";
    public static final String PARAMETER_NEW_PROJECT_ID = "$P_NEW_PROJECT_ID";
    public static final String PARAMETER_DEFINITION_ID = "$P_DEFINITION_ID";
    public static final String PARAMETER_DEFINITION_TYPE_ID = "$P_DEFINITION_TYPE_ID";
    public static final String PARAMETER_DEFINITION_GROUP_ID = "$P_DEFINITION_GROUP_ID";
    public static final String PARAMETER_OLD_DEFINITION_ID = "$P_OLD_DEFINITION_ID";
    public static final String PARAMETER_NEW_DEFINITION_ID = "$P_NEW_DEFINITION_ID";
    public static final String PARAMETER_DEFINITION_FILTER = "$P_DEFINITION_FILTER";
    public static final String PARAMETER_DEFINITION = "$P_DEFINITION";
    public static final String PARAMETER_DEFINITION_LOCK_DETAILS = "$P_DEFINITION_LOCK_DETAILS";
    public static final String PARAMETER_DEFINITION_HANDLE = "$P_DEFINITION_HANDLE";
    public static final String PARAMETER_DEFINITION_HANDLE_LIST = "$P_DEFINITION_HANDLE_LIST";
    public static final String PARAMETER_DEFINITION_LIST = "$P_DEFINITION_LIST";
    public static final String PARAMETER_DEFINITION_ID_LIST = "$P_DEFINITION_ID_LIST";
    public static final String PARAMETER_DEFINITION_TYPE_ID_LIST = "$P_DEFINITION_TYPE_ID_LIST";
    public static final String PARAMETER_DEFINITION_GROUP_ID_LIST = "$P_DEFINITION_GROUP_ID_LIST";
    public static final String PARAMETER_DEFINITION_COMPOSITION = "$P_DEFINITION_COMPOSITION";
    public static final String PARAMETER_DEFINITION_META_DATA = "$P_DEFINITION_META_DATA";
    public static final String PARAMETER_DEFINITION_TYPE_META_DATA = "$P_DEFINITION_TYPE_META_DATA";
    public static final String PARAMETER_DEFINITION_META_DATA_LIST = "$P_DEFINITION_META_DATA_LIST";
    public static final String PARAMETER_DEFINITION_TYPE_META_DATA_LIST = "$P_DEFINITION_TYPE_META_DATA_LIST";
    public static final String PARAMETER_DEFINITION_GROUP_META_DATA = "$P_DEFINITION_GROUP_META_DATA";
    public static final String PARAMETER_DATA_TYPE_STRING = "$P_DATA_TYPE_STRING";
    public static final String PARAMETER_COMMENT = "$P_COMMENT";
    public static final String PARAMETER_FILE_CONTEXT_IID = "$P_FILE_CONTEXT_IID";
    public static final String PARAMETER_FILE_PATH = "$P_FILE_PATH";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_SYSTEM_DETAILS);
            definition.setMetaDisplayName("Get System Details");
            definition.setMetaDescription("Returns the details of the currently loaded system.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetSystemDetailsOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SYSTEM_DETAILS, "System Details", "The details of the currently loaded system.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_INITIALIZE_DEFINITION);
            definition.setMetaDisplayName("Initialize Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$InitializeDefinitionOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_IDS);
            definition.setMetaDisplayName("Get Definition Identifier");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionIdentifiersOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project context for this operation.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_TYPE_ID, "Definition Type Id", "The type id for which to retrieve definition ids.", T8DataType.DEFINITION_TYPE_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID_LIST, "Definition Id List", "The list of definition ids of the specified type.", T8DataType.DEFINITION_IDENTIFIER_LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_GROUP_DEFINITION_IDS);
            definition.setMetaDisplayName("Get Group Definition Identifier");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetGroupDefinitionIdentifiersOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project context for this operation.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_GROUP_ID, "Definition Group Id", "The group id for which to retrieve definition ids.", T8DataType.DEFINITION_GROUP_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID_LIST, "Definition Id List", "The list of definition ids belonging to the specified group.", T8DataType.DEFINITION_IDENTIFIER_LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_GROUP_IDS);
            definition.setMetaDisplayName("Get Definition Group Identifier");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionGroupIdentifiersOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_TYPE_IDS);
            definition.setMetaDisplayName("Get Definition Type Identifiers");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionTypeIdentifiersOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_GROUP_ID, "Definition Group Identifier", "The group identifier for a set of type identifiers to be retrieved.", T8DataType.DEFINITION_GROUP_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_TYPE_ID_LIST, "Definition Type Identifier List", "The list of type identifiers retrieved, possibly filtered by the group identifier input.", T8DataType.DEFINITION_IDENTIFIER_LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_COMPOSITION);
            definition.setMetaDisplayName("Get Definition Composition");
            definition.setMetaDescription("Gets the composition break-down of the specified definition.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionCompositionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier of the definition for which to retrieve the composition.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DESCENDANTS, "Include Descendants", "A boolean flag to indicate whether or not descendant compositions should be included.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_COMPOSITION, "Definition Composition", "The compsition break-down of the requested definition.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_META_DATA);
            definition.setMetaDisplayName("Get Definition Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionMetaDataOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the definition is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Id", "The identifier of the definition for which to retrieve meta data.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID_LIST, "Definition Id List", "The identifiers of the definitions for which to retrieve meta data.", T8DataType.DEFINITION_IDENTIFIER_LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA, "Definition Meta Data", "The meta data of the definition requested.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA_LIST, "Definition Meta Data List", "The list of meta data objects for the definitions requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_MOVE_DEFINITION_TO_PROJECT);
            definition.setMetaDisplayName("Move Definition To Project");
            definition.setMetaDescription("Moves a Definition from one project to another, removing it from the source project completely.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$MoveDefinitionToProjectOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the definition is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier of the definition to move.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NEW_PROJECT_ID, "Project Identifier", "The identifier of the project to which the definition is to be moved.", T8DataType.DEFINITION_IDENTIFIER));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_COPY_DEFINITION_TO_PROJECT);
            definition.setMetaDisplayName("Copy Definition To Project");
            definition.setMetaDescription("Copies a Definition from one project to another, leaving the source copy in its original project.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$CopyDefinitionToProjectOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the definition is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier of the definition to copy.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NEW_PROJECT_ID, "Project Identifier", "The identifier of the project to which the definition is to be copied.", T8DataType.DEFINITION_IDENTIFIER));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_PROJECT_DEFINITION_META_DATA);
            definition.setMetaDisplayName("Get Project Definition Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetProjectDefinitionMetaDataOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project for which to fetch all content definition meta data.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA_LIST, "Definition Meta Data List", "The list of meta data objects for the definitions requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_TYPE_DEFINITION_META_DATA);
            definition.setMetaDisplayName("Get Type Definition Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetTypeDefinitionMetaDataOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project context for this operation.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA_LIST, "Definition Meta Data List", "The list of meta data objects for the definitions requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_GROUP_DEFINITION_META_DATA);
            definition.setMetaDisplayName("Get Group Definition Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetGroupDefinitionMetaDataOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project context for this operation.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA_LIST, "Definition Meta Data List", "The list of meta data objects for the definitions requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_ALL_DEFINITION_META_DATA);
            definition.setMetaDisplayName("Get All Definition Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetAllDefinitionMetaDataOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA_LIST, "Definition Meta Data List", "The list of meta data objects for the definitions requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_TYPE_META_DATA);
            definition.setMetaDisplayName("Get Definition Type Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionTypeMetaDataOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_TYPE_ID, "Definition Type Identifier", "The definition type identifier to use to retrieve the definition type meta data for the specified definition type.", T8DataType.DEFINITION_TYPE_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA_LIST, "Definition Meta Data List", "The list containing the meta data for the specified type identifier.", T8DataType.LIST));

            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_GROUP_DEFINITION_TYPE_META_DATA);
            definition.setMetaDisplayName("Get Group Definition Type Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetGroupDefinitionTypeMetaDataOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_ALL_DEFINITION_GROUP_META_DATA);
            definition.setMetaDisplayName("Get All Definition Group Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetAllDefinitionGroupMetaDataOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_GROUP_META_DATA, "Definition Group Meta Data List", "The list of group meta data objects requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_ALL_DEFINITION_TYPE_META_DATA);
            definition.setMetaDisplayName("Get All Definition Type Meta Data");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetAllDefinitionTypeMetaDataOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_TYPE_META_DATA_LIST, "Definition Type Meta Data List", "The list of type meta data objects requested.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_INITIALIZED_DEFINITION);
            definition.setMetaDisplayName("Get Initialized Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetInitializedDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the definition is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier of the definition to return.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition to return.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition loaded from the meta data manager.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_RENAME_DEFINITION);
            definition.setMetaDisplayName("Get Raw Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$RenameDefinitionOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE_LIST, "Afffected Definitions", "The list of handles to definitions affected by this operation.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_COPY_DEFINITION);
            definition.setMetaDisplayName("Get Raw Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$CopyDefinitionOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CREATE_NEW_DEFINITION);
            definition.setMetaDisplayName("Create New Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$CreateNewDefinitionOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_RAW_DEFINITION);
            definition.setMetaDisplayName("Get Raw Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetRawDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the definition is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier of the definition to return.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition to return.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition loaded from the meta data manager.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_RESOLVED_DEFINITION);
            definition.setMetaDisplayName("Get Resolved Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetResolvedDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_TYPE_ID, "Definition Type Identifier", "The type identifier of the definition to resolve and return.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition loaded from the meta data manager.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_RAW_GROUP_DEFINITIONS);
            definition.setMetaDisplayName("Get Raw Group Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetRawGroupDefinitionsOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_INITIALIZED_GROUP_DEFINITIONS);
            definition.setMetaDisplayName("Get Initialized Group Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetInitializedGroupDefinitionsOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_DEFINITION);
            definition.setMetaDisplayName("Delete Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$DeleteDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier of the definition to delete.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition to delete.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SAFE_DELETION, "Safe Deletion", "If set to Boolean TRUE, the definition will not be deleted if any other definitions in the system reference it.  Default = TRUE.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_REMOVE_DEFINITION_REFERENCES);
            definition.setMetaDisplayName("Remove Definition References");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$RemoveDefinitionReferencesOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The identifier to remove from all other definitions.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_KEY_ID, "Key Identifier", "The key to use if any currently locked definitions are affected by this operation.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_SAVE_DEFINITION);
            definition.setMetaDisplayName("Save Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$SaveDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition to save.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_FINALIZE_DEFINITION);
            definition.setMetaDisplayName("Finalize Definition");
            definition.setMetaDescription("Finalizes in-progress updates on the definition, marking its status accordingly.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$FinalizeDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition to finalize.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMENT, "Comment", "The comment that will be added to the definition is finalization is successful.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_META_DATA, "Definition Meta Data", "The updated meta data of the finalized definition.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_RELOAD_DEFINITIONS);
            definition.setMetaDisplayName("Reload Definitions");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$ReloadDefinitionsOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CHECK_ID_AVAILABILITY);
            definition.setMetaDisplayName("Check Identifier Availability");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$CheckIdentifierAvailabilityOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project context in which to check availability.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The id to check for availability.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "Boolean true if the identifier is available.", T8DataType.BOOLEAN));
            definitions.add(definition);

            //Find Definition Operations.
            definition = new T8JavaServerOperationDefinition(OPERATION_FIND_DEFINITIONS_BY_TEXT);
            definition.setMetaDisplayName("Find Definitions by Text");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$FindDefinitionsByTextOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_FIND_DEFINITIONS_BY_REGULAR_EXPRESSION);
            definition.setMetaDisplayName("Find Definitions by Regular Expression");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$FindDefinitionsByRegularExpressionOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_FIND_DEFINITIONS_BY_ID);
            definition.setMetaDisplayName("Find Definitions by Identifier");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$FindDefinitionsByIdentifierOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_REFERENCES_ONLY, "Include References Only", "If set to true, only definitions referencing the specified identifier will be included and not the actual definitions to which the identifier belongs.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_ALL_DEFINITION_HANDLES);
            definition.setMetaDisplayName("Get All Definition Handles");
            definition.setMetaDescription("Returns the handles of all definitions from all projects and system caches that meet the specified filter criteria.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetAllDefinitionHandlesOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_FILTER, "Definition Filter", "The filter that will be used to identify definitions for which to return handles.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE_LIST, "Definition Handle List", "The list of definition handles identified by the input filter parameter.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_IMPORT_DEFINITION);
            definition.setMetaDisplayName("Import Definition");
            definition.setMetaDescription("Imports the supplied definition into the system while keeping its meta-data unaffected.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$ImportDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition to import.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_LOCK_DEFINITION);
            definition.setMetaDisplayName("Lock Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$LockDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition to lock.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_KEY_ID, "Key Identifier", "The identifier of the definition lock key.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition loaded from the meta data manager.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_UNLOCK_DEFINITION);
            definition.setMetaDisplayName("Unlock Definition");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$UnlockDefinitionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition to unlock.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition to unlock (if supplied, this definition will also be saved before it is unlocked).", T8DataType.DEFINITION));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_KEY_ID, "Key Identifier", "The identifier of the definition lock key.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition loaded from the meta data manager.", T8DataType.DEFINITION));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_DEFINITION_LOCK_DETAILS);
            definition.setMetaDescription("Returns the details of a lock held on a specified definition.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$GetDefinitionLockDetailsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_HANDLE, "Definition Handle", "The handle of the definition for which to fetch lock details.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_LOCK_DETAILS, "Definition Lock Details", "The details of the definition lock.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_TRANSFER_DEFINITION_REFERENCES);
            definition.setMetaDisplayName("Transfer Definition References");
            definition.setMetaDescription("Transfers all references from the specified old identifier to the specified new identifier.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$TransferDefinitionReferencesOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OLD_DEFINITION_ID, "Old Definition Identifier", "The old identifier from which references will be transferred.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NEW_DEFINITION_ID, "New Definition Identifier", "The new identifier to which references will be transferred.", T8DataType.DEFINITION_IDENTIFIER));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CREATE_DATA_TYPE);
            definition.setMetaDisplayName("Create Data Type");
            definition.setMetaDescription("Returns the T8DataType object matching the specified string representation.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$CreateDataTypeOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_TYPE_STRING, "Data Type String", "The string representation of the data type to create.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_TYPE, "Data Type", "The newly created Data Type.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_EXPORT_PROJECTS);
            definition.setMetaDisplayName("Export Projects");
            definition.setMetaDescription("Exports the specified list of projects to the target location.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$ExportProjectsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_IDS, "Project Ids", "The list of ids of projects to export.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_CONTEXT_IID, "File Context Iid", "The file context to which projects will be exported.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The file path in the specified context to which the projects will be exported.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_IMPORT_PROJECTS);
            definition.setMetaDisplayName("Import Projects");
            definition.setMetaDescription("Import the specified list of projects from the target file.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$ImportProjectsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_IDS, "Project Ids", "The list of ids of projects to import from the target file.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_CONTEXT_IID, "File Context Iid", "The file context where the file to be imported is located.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The file path where the file to be imported is located.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_IMPORT_REPORT, "Import Report", "The report detailing the result of the requested import operation.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_PROJECTS);
            definition.setMetaDisplayName("Delete Projects");
            definition.setMetaDescription("Delete the specified projects and their content.");
            definition.setClassName("com.pilog.t8.definition.T8DefinitionManagerOperations$DeleteProjectsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_IDS, "Project Ids", "The list of ids of projects to be deleted.", T8DataType.LIST));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
