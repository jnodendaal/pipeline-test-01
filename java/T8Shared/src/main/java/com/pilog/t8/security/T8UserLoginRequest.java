package com.pilog.t8.security;

import com.pilog.t8.T8SecurityManager.LoginLocation;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8UserLoginRequest implements Serializable
{
    private final String userIdentifier;
    private final char[] password;
    private final LoginLocation loginLocation;
    private final boolean endExistingSession;

    public T8UserLoginRequest(LoginLocation loginLocation, String userIdentifier, char[] password, boolean endExistingSession)
    {
        this.loginLocation = loginLocation;
        this.userIdentifier = userIdentifier;
        this.password = password;
        this.endExistingSession = endExistingSession;
    }

    public char[] getPassword()
    {
        return password;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public LoginLocation getLoginLocation()
    {
        return loginLocation;
    }

    public boolean isEndExistingSession()
    {
        return endExistingSession;
    }
}
