package com.pilog.t8.definition.data.cache;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataCache;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataCacheDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_CACHE";
    public static final String GROUP_NAME = "Data Caches";
    public static final String GROUP_DESCRIPTION = "Caches employed be the system to optimize retrieval from certain often used data stores.";
    public static final String STORAGE_PATH = "/data_caches";
    public static final String IDENTIFIER_PREFIX = "DATA_CACHE_";
    private enum Datum{LAZY_INITIALIZATION};
    // -------- Definition Meta-Data -------- //

    public T8DataCacheDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LAZY_INITIALIZATION.toString(), "Lazy Initialization",  "If true, this cache will not be initialized during system startup, only when first needed.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DataCache createNewDataCacheInstance(T8Context context) throws Exception;

    public boolean isLazyInitialization()
    {
        Boolean lazyInitialization;

        lazyInitialization = (Boolean)getDefinitionDatum(Datum.LAZY_INITIALIZATION.toString());
        return (lazyInitialization == null) || lazyInitialization;
    }

    public void setLazyInitialization(boolean lazyInitialization)
    {
        setDefinitionDatum(Datum.LAZY_INITIALIZATION.toString(), lazyInitialization);
    }
}
