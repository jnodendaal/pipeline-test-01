package com.pilog.t8.time;

/**
 * @author Bouwer du Preez
 */
public class T8Minute implements T8TimeUnit
{
    private final long amount;

    public T8Minute(long amount)
    {
        this.amount = amount;
    }

    @Override
    public long getMilliseconds()
    {
        return 60000 * amount;
    }

    public long getAmount()
    {
        return amount;
    }
}
