package com.pilog.t8.data;

import com.pilog.t8.data.entity.T8DefaultDataEntityComparator;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.utilities.collections.MapComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Bouwer du Preez
 */
public class T8DataUtilities
{
    public static final boolean isGUIDDataType(T8DataType dataType)
    {
        if (dataType.isType(T8DtString.IDENTIFIER))
        {
            return (((T8DtString)dataType).getType() == T8DtString.T8StringType.GUID);
        }
        else return false;
    }

    public static boolean isEqual(T8DataEntity entity1, T8DataEntity entity2)
    {
        return entity1.getFieldValues().equals(entity2.getFieldValues());
    }

    /**
     * Convenience method mostly used from scripts to fetch they field values of
     * the first entity in a list quickly and easily.  If the list is empty,
     * null is returned.
     *
     * @param entityList The list of entities from which the first entity's
     * fields will be retrieved.
     * @return A map containing the field values of the first entity in the
     * supplied list or null if the list is empty.
     */
    public static Map<String, Object> getFirstEntityFieldValues(List<T8DataEntity> entityList)
    {
        if ((entityList != null) && (entityList.size() > 0))
        {
            return entityList.get(0).getFieldValues();
        }
        else return null;
    }

    /**
     * Returns a new HashMap created from the supplied HashMap but containing
     * only the keys specified in the supplied list.
     *
     * @param entity The entity from which the new HashMap will be created.
     * @param keys The list of keys to include in the new map.
     * @return A new HashMap created from the input map, but containing only the
     * specified keys.
     */
    public static final HashMap<String, Object> getHashMapValues(T8DataEntity entity, String[] keys)
    {
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<String, Object>();
        for (String key : keys)
        {
            filterValues.put(key, entity.getFieldValue(key));
        }

        return filterValues;
    }

    public static Map<String, Object> getUpdatedFieldValues(T8DataEntity oldEntity, T8DataEntity newEntity)
    {
        Map<String, Object> oldValues;
        Map<String, Object> updatedValues;

        updatedValues = new HashMap<String, Object>();
        oldValues = oldEntity.getFieldValues();
        for (String fieldIdentifier : oldValues.keySet())
        {
            Object newValue;
            Object oldValue;

            oldValue = oldEntity.getFieldValue(fieldIdentifier);
            newValue = newEntity.getFieldValue(fieldIdentifier);

            if (!Objects.equals(oldValue, newValue))
            {
                updatedValues.put(fieldIdentifier, newValue);
            }
        }

        return updatedValues;
    }

    /**
     * This method will loop through all of the supplied data entities and return
     * only those that contain filter values.
     *
     * @param dataEntities The data entity to filter.
     * @param filterKeyName The name of the key in the data entity, that will be
     * used to filter on.
     * @param filterValue The data value that will be used to filter with.
     * @return The subset of the supplied data entity collection that contain the
     * filter values supplied.
     */
    public static final ArrayList<T8DataEntity> getFilteredDataEntities(List<T8DataEntity> dataEntities, String filterKeyName, Object filterValue)
    {
        ArrayList<T8DataEntity> filteredDataEntities;

        filteredDataEntities = new ArrayList<>();
        for (T8DataEntity dataEntity : dataEntities)
        {
            Object dataValue;

            dataValue = dataEntity.getFieldValue(filterKeyName);

            // If the data value does not correspond to the key value, return false.
            if (filterValue == null)
            {
                if (dataValue == null)
                {
                    filteredDataEntities.add(dataEntity);
                }
            }
            else if (filterValue.equals(dataValue))
            {
                filteredDataEntities.add(dataEntity);
            }
        }

        return filteredDataEntities;
    }

    /**
     * This method will loop through all of the supplied data entities and return
     * only those that contain filter values.
     *
     * @param dataEntities The data rows to filter.
     * @param filterColumnValues The column values that will be used to filter
     * the supplied data rows.
     * @return The subset of the supplied data entity collection that contain the
     * filter values supplied.
     */
    public static final ArrayList<T8DataEntity> getFilteredDataEntities(List<T8DataEntity> dataEntities, Map<String, Object> filterColumnValues)
    {
        ArrayList<T8DataEntity> filteredDataEntities;

        filteredDataEntities = new ArrayList<>();
        for (T8DataEntity dataEntity : dataEntities)
        {
            if (T8DataUtilities.dataEntityMatchesKey(dataEntity, filterColumnValues))
            {
                filteredDataEntities.add(dataEntity);
            }
        }

        return filteredDataEntities;
    }

    /**
     * Compares the supplied data entity to the specified key values and returns
     * a boolean value indicating whether or not the data values in the entity
     * correspond to the key values.
     *
     * @param dataEntity A data entity containing a data row in the form of
     * (columnName, columnValue) pairs.
     * @param keyValues A HashMap containing the key in the form of
     * (keyName, keyValue) pairs.
     * @return Returns true if all of the key values are equal to the
     * corresponding values in the supplied data entity, else returns false.
     */
    public static final boolean dataEntityMatchesKey(T8DataEntity dataEntity, Map<String, Object> keyValues)
    {
        Iterator<String> keyIterator;

        keyIterator = keyValues.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String keyName;
            Object keyValue;
            Object dataValue;

            // Read the key value and the data value.
            keyName = keyIterator.next();
            keyValue = keyValues.get(keyName);
            dataValue = dataEntity.getFieldValue(keyName);

            // If the data value does not correspond to the key value, return false.
            if (!Objects.equals(keyValue, dataValue)) return false;
        }

        // The data rows fits the key.
        return true;
    }

    /**
     * Iterates over the supplied list of data entities and returns the first
     * one matching the specified key values.
     *
     * @param entityList The data entities to evaluate.
     * @param keyValues The key values to use for evaluation.
     * @return The first row from the specified collection that matches the
     * filter values.
     */
    public static final T8DataEntity findDataEntity(List<T8DataEntity> entityList, Map<String, Object> keyValues)
    {
        for (T8DataEntity dataEntity : entityList)
        {
            if (T8DataUtilities.dataEntityMatchesKey(dataEntity, keyValues))
            {
                return dataEntity;
            }
        }

        return null;
    }

    /**
     * Iterates over the supplied list of data entities and returns all entities
     * matching the specified key values.
     *
     * @param entityList The data entities to evaluate.
     * @param keyValues The key values to use for evaluation.
     * @return The list of entities from the specified collection that matches
     * the filter values.
     */
    public static final List<T8DataEntity> findDataEntities(List<T8DataEntity> entityList, Map<String, Object> keyValues)
    {
        List<T8DataEntity> resultList;

        resultList = new ArrayList<T8DataEntity>();
        for (T8DataEntity dataEntity : entityList)
        {
            if (T8DataUtilities.dataEntityMatchesKey(dataEntity, keyValues))
            {
                resultList.add(dataEntity);
            }
        }

        return resultList;
    }

    /**
     * Iterates over the supplied list of data entities and returns all entities
     * matching the specified key value.
     *
     * @param entityList The data entities to evaluate.
     * @param fieldIdentifier The entity field to evaluate.
     * @param fieldValue The value to check for.
     * @return The list of entities from the specified collection that match the
     * the specified filter value.
     */
    public static final List<T8DataEntity> findDataEntities(List<T8DataEntity> entityList, String fieldIdentifier, Object fieldValue)
    {
        List<T8DataEntity> resultList;

        resultList = new ArrayList<T8DataEntity>();
        for (T8DataEntity dataEntity : entityList)
        {
            if (Objects.equals(fieldValue, dataEntity.getFieldValue(fieldIdentifier)))
            {
                resultList.add(dataEntity);
            }
        }

        return resultList;
    }

    /**
     * Returns a list containing the field value specified from each of the
     * entities in the input list.
     *
     * @param <T> The field type which represents the type of the values in the
     *      returned list
     * @param entityList The list of entities from which to extract the values.
     * @param fieldIdentifier The field from which to extract the values.
     *
     * @return The list of values extracted from the entities.  The index of
     *      each value in the list will correspond to the index of the entity in
     *      the input list from which it was extracted.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getEntityFieldValues(List<T8DataEntity> entityList, String fieldIdentifier)
    {
        List<T> fieldValues;

        fieldValues = new ArrayList<>();
        if (entityList != null)
        {
            for (T8DataEntity entity : entityList)
            {
                fieldValues.add((T)entity.getFieldValue(fieldIdentifier));
            }
        }

        return fieldValues;
    }

    /**
     * Returns a set containing the distinct field values specified from each of
     * the entities in the input list.
     *
     * @param <T>
     * @param entityList The list of entities from which to extract the values.
     * @param fieldIdentifier The field from which to extract the values.
     * @return The set of distinct values extracted from the entities.
     */
    @SuppressWarnings("unchecked")
    public static <T> Set<T> getEntityFieldValueSet(List<T8DataEntity> entityList, String fieldIdentifier)
    {
        if (entityList != null)
        {
            return entityList.stream().map((entity) -> (T)entity.getFieldValue(fieldIdentifier)).collect(Collectors.toCollection(HashSet::new));
        } else return new HashSet<>();
    }

    /**
     * This method will evaluate all entities in the newEntities collection and return
     * a list of entities that are present in the newEntities collection but not in the
     * oldEntities collection.  Entities are evaluated based on the values of their key
     * columns, as supplied in the keys list.
     *
     * @param oldEntities The 'old' collection of data Entities.
     * @param newEntities The 'new' collection of data Entities.
     * @param keys The list of key column names to use for evaluation.
     * @return A list of data entities that are in the newEntities collection but not in
     * the oldEntities collection.
     */
    public static final ArrayList<T8DataEntity> getAddedEntities(List<T8DataEntity> oldEntities, List<T8DataEntity> newEntities, String[] keys)
    {
        ArrayList<T8DataEntity> addedEntities;

        addedEntities = new ArrayList<>();
        for (T8DataEntity newEntity : newEntities)
        {
            T8DataEntity oldEntity;

            oldEntity = findDataEntity(oldEntities, getHashMapValues(newEntity, keys));
            if (oldEntity == null)
            {
                addedEntities.add(newEntity);
            }
        }

        return addedEntities;
    }

    /**
     * Returns a {@code List} containing the distinct field values retrieved from the entity {@code List}.
     *
     * @param entityList The {@code List} of entities from which to extract the distinct values.
     * @param fieldIdentifier The entity field from which to extract the values.
     * @return The {@code List} of distinct values extracted from the entities. The index will be
     * the order in which the distinct values are retrieved from the entity {@code List}.
     */
    public static <V> List<V> getDistinctEntityFieldValues(List<T8DataEntity> entityList, String fieldIdentifier)
    {
        Set<V> distinctValueSet;
        List<V> fieldValues;

        distinctValueSet = new HashSet<>();
        fieldValues = new ArrayList<>();
        if (entityList != null)
        {
            for (T8DataEntity entity : entityList)
            {
                distinctValueSet.add((V)entity.getFieldValue(fieldIdentifier));
            }
        }
        fieldValues.addAll(distinctValueSet);
        return fieldValues;
    }

    public static T8DataEntityDefinition createEntityDefinition(String entityIdentifier, T8DataSourceDefinition sourceDefinition) throws Exception
    {
        T8DataEntityDefinition newEntityDefinition = new T8DataEntityDefinition(entityIdentifier);
        newEntityDefinition.setDataSourceIdentifier(sourceDefinition.getPublicIdentifier());
        for (T8DataSourceFieldDefinition t8DataSourceFieldDefinition : sourceDefinition.getFieldDefinitions())
        {
            T8DataEntityFieldDefinition newFieldDefinition = new T8DataEntityFieldDefinition(t8DataSourceFieldDefinition.getIdentifier());
            newFieldDefinition.setDataType(t8DataSourceFieldDefinition.getDataType());
            newFieldDefinition.setKey(t8DataSourceFieldDefinition.isKey());
            newFieldDefinition.setRequired(t8DataSourceFieldDefinition.isRequired());
            newFieldDefinition.setSourceIdentifier(t8DataSourceFieldDefinition.getPublicIdentifier());
            newEntityDefinition.addFieldDefinition(newFieldDefinition);
        }
        newEntityDefinition.initializeDefinition(null, null, null);
        return newEntityDefinition;
    }

    /**
     * Converts the supplied data entity to a different type.  All fields that
     * correspond between the old and new entity definitions are retained.  The
     * retained field values are added to the new entity but the old entity is
     * not altered.
     * @param tx The data transaction to use.
     * @param dataEntity The data entity to convert.
     * @param newIdentifier The identifier of the data entity to convert to.
     * @return The newly created entity (containing all matching fields from the
     * source entity).
     * @throws Exception
     */
    public static T8DataEntity convertDataEntity(T8DataTransaction tx, T8DataEntity dataEntity, String newIdentifier) throws Exception
    {
        T8DataEntity newEntity;

        newEntity = tx.create(newIdentifier, null);
        for (String fieldIdentifier : dataEntity.getDefinition().getFieldIdentifiers(true))
        {
            if (newEntity.containsField(fieldIdentifier))
            {
                newEntity.setFieldValue(newIdentifier + fieldIdentifier, dataEntity.getFieldValue(dataEntity.getIdentifier() + fieldIdentifier));
            }
        }

        return newEntity;
    }

    /**
     * Sorts the supplied list of data entities according to the ordering method
     * specified.  This method requires that each of the fields specified in the
     * ordering method, must contain a value that implements the
     * <code>Comparable</code> interface.
     * @param entityList The list of data entities to sort.
     * @param fieldOrdering The field ordering method to use.
     */
    public static void sort(List<T8DataEntity> entityList, Map<String, OrderMethod> fieldOrdering)
    {
        Collections.sort(entityList, new T8DefaultDataEntityComparator(fieldOrdering));
    }

    /**
     * Sorts the supplied list of maps according to the ordering method
     * specified.  This method requires that each of the fields specified in the
     * key ordering map, must contain be mapped to a value that implements the
     * <code>Comparable</code> interface.
     *
     * @param mapList The list of Map collections to sort.
     * @param keyOrdering The ordering methods to use for each key.  This map
     * must contain value pairs in the form <mapKey,orderMethod> where orderMethod
     * is a Boolean true value for ascending ordering and boolean false for descending ordering.
     */
    public static void sortMaps(List<Map> mapList, Map<String, Boolean> keyOrdering)
    {
        Map<String, MapComparator.OrderMethod> orderMap;

        orderMap = new HashMap<>();
        for (String key : keyOrdering.keySet())
        {
            Boolean ascending;

            ascending = keyOrdering.get(key);
            orderMap.put(key, ascending ? MapComparator.OrderMethod.ASCENDING : MapComparator.OrderMethod.DESCENDING);
        }

        Collections.sort(mapList, new MapComparator(orderMap));
    }
}
