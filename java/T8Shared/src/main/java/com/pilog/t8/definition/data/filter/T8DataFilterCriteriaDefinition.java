package com.pilog.t8.definition.data.filter;

import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilterCriteriaDefinition extends T8DataFilterClauseDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_FILTER_CRITERIA";
    public static final String IDENTIFIER_PREFIX = "FILTER_CRITERIA_";
    public static final String DISPLAY_NAME = "Data Filter Criteria";
    public static final String DESCRIPTION = "An object that specifies filter criteria to be used for filtering data entities.";
    public static final String VERSION = "0";
    private enum Datum {FILTER_CLAUSE_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DataFilterCriteriaDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FILTER_CLAUSE_DEFINITIONS.toString(), "Filter Clauses", "The filter clauses contained by this criteria definition."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILTER_CLAUSE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData((T8DataFilterClauseDefinition.GROUP_IDENTIFIER)));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DataFilterCriteria getNewDataFilterClauseInstance(T8Context context, Map<String, Object> filterParameters)
    {
        T8DataFilterCriteria filterCriteria;

        filterCriteria = new T8DataFilterCriteria(this);
        for (T8DataFilterClauseDefinition filterClauseDefinition : getFilterClauseDefinitions())
        {
            filterCriteria.addFilterClause(filterClauseDefinition.getConjunction(), filterClauseDefinition.getNewDataFilterClauseInstance(context, filterParameters));
        }

        return filterCriteria;
    }

    public ArrayList<T8DataFilterClauseDefinition> getFilterClauseDefinitions()
    {
        return (ArrayList<T8DataFilterClauseDefinition>)getDefinitionDatum(Datum.FILTER_CLAUSE_DEFINITIONS.toString());
    }

    public void setFilterClauseDefinitions(ArrayList<T8DataFilterClauseDefinition> definitions)
    {
        getFilterClauseDefinitions().clear();
        getFilterClauseDefinitions().addAll(definitions);
    }
}
