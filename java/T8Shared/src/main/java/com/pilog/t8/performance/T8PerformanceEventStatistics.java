package com.pilog.t8.performance;

import java.io.OutputStream;

/**
 * @author Bouwer du Preez
 */
public interface T8PerformanceEventStatistics
{
    public String getIdentifier();
    public void reset();
    public void logEvent();
    public void printStatistics(OutputStream stream);
}
