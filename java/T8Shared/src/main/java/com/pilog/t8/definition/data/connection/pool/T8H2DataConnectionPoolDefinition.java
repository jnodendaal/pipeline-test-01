package com.pilog.t8.definition.data.connection.pool;

import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8H2DataConnectionPoolDefinition extends T8DataConnectionPoolDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_CONNECTION_POOL_H2";
    public static final String DISPLAY_NAME = "H2 Connection Pool";
    public static final String DESCRIPTION = "A H2 connection pool.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8H2DataConnectionPoolDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataConnectionPool createNewConnectionPoolInstance(T8DataTransaction dataAccessProvider) throws Exception
    {
        Constructor constructor;
            
        constructor = Class.forName("com.pilog.t8.data.connection.pool.T8H2DataConnectionPool").getConstructor(this.getClass(), T8DataTransaction.class);
        return (T8DataConnectionPool)constructor.newInstance(this, dataAccessProvider);
    }
}
