package com.pilog.t8.performance;

import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.numbers.Numbers;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultExecutionPerformanceStatistics implements T8ExecutionPerformanceStatistics, Serializable
{
    private final T8ExecutionPerformanceStatistics parentExecutionStatistics;
    private T8ExecutionPerformanceStatistics currentlyStartedExecution;
    private final LinkedList<Long> instances;
    private final Map<String, T8ExecutionPerformanceStatistics> subExecutionStatistics;
    private String identifier; // The identifier of the execution.
    private long iterations; // The number of times this execution has been performed.
    private long firstStartTime; // The time when this execution was first started.
    private long firstEndTime; // The time when the first execution of this type ended.
    private long lastStartTime; // The time when the last execution of this type was started.
    private long lastEndTime; // The time when the last execution of this type was ended.
    private long longestIterationDuration; // The longest duration of a single execution of this type.
    private long totalDuration; // The total duration of all executions of this type.

    public T8DefaultExecutionPerformanceStatistics(String identifier, T8ExecutionPerformanceStatistics parent)
    {
        this.identifier = identifier;
        this.iterations = 0;
        this.firstStartTime = -1;
        this.firstEndTime = -1;
        this.lastStartTime = -1;
        this.lastEndTime = -1;
        this.longestIterationDuration = 0;
        this.totalDuration = 0;
        this.currentlyStartedExecution = null;
        this.parentExecutionStatistics = parent;
        this.instances = new LinkedList<Long>();
        this.subExecutionStatistics = new TreeMap<String, T8ExecutionPerformanceStatistics>();
    }

    @Override
    public T8ExecutionPerformanceStatistics getParentExecutionStatistics()
    {
        return parentExecutionStatistics;
    }

    @Override
    public T8ExecutionPerformanceStatistics findExecutionStatistics(String identifier)
    {
        for (T8ExecutionPerformanceStatistics stats : subExecutionStatistics.values())
        {
            if (stats.getIdentifier().equals(identifier))
            {
                return stats;
            }
            else
            {
                T8ExecutionPerformanceStatistics subStats;

                subStats = stats.findExecutionStatistics(identifier);
                if (subStats != null) return subStats;
            }
        }

        return null;
    }

    @Override
    public long getIterations()
    {
        return iterations;
    }

    @Override
    public long getFirstStartTime()
    {
        return firstStartTime;
    }

    @Override
    public long getFirstEndTime()
    {
        return firstEndTime;
    }

    @Override
    public long getLastStartTime()
    {
        return lastStartTime;
    }

    @Override
    public long getLastEndTime()
    {
        return lastEndTime;
    }

    @Override
    public long getLongestIterationDuration()
    {
        return longestIterationDuration;
    }

    @Override
    public long getTotalDuration()
    {
        return totalDuration;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public void reset()
    {
        this.iterations = 0;
        this.firstStartTime = -1;
        this.firstEndTime = -1;
        this.lastStartTime = -1;
        this.lastEndTime = -1;
        this.longestIterationDuration = 0;
        this.totalDuration = 0;
    }

    @Override
    public boolean hasInstances()
    {
        return instances.size() > 0;
    }

    @Override
    public void logStart()
    {
        long time;

        // Incement the number of iterations.
        iterations++;

        // Log the start time.
        time = System.currentTimeMillis();
        lastStartTime = time;
        if (firstStartTime == -1) firstStartTime = time;

        // Log the instance start time.
        instances.push(time);
    }

    @Override
    public void logEnd()
    {
        long time;
        long duration;

        // Log the end time.
        time = System.currentTimeMillis();
        lastEndTime = time;
        if (firstEndTime == -1) firstEndTime = time;

        // Log the duration since the instance start time.
        duration = (lastEndTime - instances.pop());
        if (instances.size() == 0) totalDuration += duration; // Only add the root durations te prevent nested invocations from being counted multiple times.
        if (duration > longestIterationDuration) longestIterationDuration = duration;
    }

    @Override
    public void logSubExecutionStart(String executionIdentifier)
    {
        if (currentlyStartedExecution != null)
        {
            if (currentlyStartedExecution.getIdentifier().equals(executionIdentifier))
            {
                currentlyStartedExecution.logStart();
            }
            else
            {
                currentlyStartedExecution.logSubExecutionStart(executionIdentifier);
            }
        }
        else
        {
            currentlyStartedExecution = subExecutionStatistics.get(executionIdentifier);
            if (currentlyStartedExecution == null)
            {
                currentlyStartedExecution = new T8DefaultExecutionPerformanceStatistics(executionIdentifier, this);
                subExecutionStatistics.put(executionIdentifier, currentlyStartedExecution);
            }

            currentlyStartedExecution.logStart();
        }
    }

    @Override
    public void logSubExecutionEnd(String executionIdentifier)
    {
        if (currentlyStartedExecution != null)
        {
            if (currentlyStartedExecution.getIdentifier().equals(executionIdentifier))
            {
                currentlyStartedExecution.logEnd();
                if (!currentlyStartedExecution.hasInstances()) currentlyStartedExecution = null;
            }
            else
            {
                currentlyStartedExecution.logSubExecutionEnd(executionIdentifier);
            }
        }
        else throw new RuntimeException("Invalid attempt to log end of execution that has not started: " + executionIdentifier);
    }

    @Override
    public boolean isExecuting(String executionIdentifier)
    {
        if (currentlyStartedExecution != null)
        {
            if (currentlyStartedExecution.getIdentifier().equals(executionIdentifier))
            {
                return true;
            }
            else return currentlyStartedExecution.isExecuting(executionIdentifier);
        }
        else return false;
    }

    private int getDepth()
    {
        T8ExecutionPerformanceStatistics parent;
        int depth;

        depth = 0;
        parent = this;
        while ((parent = parent.getParentExecutionStatistics()) != null)
        {
            depth++;
        }

        return depth;
    }

    private long getUnaccountedTime()
    {
        long subExecutionTime;

        subExecutionTime = 0;
        for (T8ExecutionPerformanceStatistics subStats : subExecutionStatistics.values())
        {
            subExecutionTime += subStats.getTotalDuration();
        }

        if (subExecutionTime > 0) return totalDuration - subExecutionTime;
        else return 0;
    }

    @Override
    public void printStatistics(OutputStream stream)
    {
        PrintWriter writer;
        String indentation;
        Long parentDuration;
        Double percentage;

        // Create an indentation String based on the depth of this exection statistics object.
        indentation = "\t";
        for (int indent = 0; indent < getDepth(); indent++)
        {
            indentation += "\t";
        }

        // Get the duration of the parent execution statistics if there are any.
        parentDuration = parentExecutionStatistics != null ? parentExecutionStatistics.getTotalDuration() : null;
        if (parentDuration != null)
        {
            // Determine the percentage of the parent's time for which this execution accounts.
            percentage =((double)totalDuration) / ((double)parentDuration) * 100.00;
            if ((!Double.isInfinite(percentage) && (!Double.isNaN(percentage))))
            {
                percentage = Numbers.round(percentage, 2);
            }
            else percentage = null;
        }
        else percentage = null;

        // Write the statistics to the output stream.
        writer = new PrintWriter(stream);
        writer.println(indentation + "* " + identifier + (percentage != null ? (" " + percentage + "%") : ""));
        writer.println(indentation + "\tIterations:            " + iterations);
        writer.println(indentation + "\tTotal Duration:        " + totalDuration + "ms");
        writer.println(indentation + "\tUnaccounted Time:      " + getUnaccountedTime() + "ms");
        writer.println(indentation + "\tLongest Iteration:     " + longestIterationDuration + "ms");
        writer.println(indentation + "\tAverage Iteration:     " + ((double)totalDuration/(double)iterations) + "ms");
        writer.println(indentation + "\tIterations Per Second: " + ((double)iterations*1000.00/(double)totalDuration));
        writer.println(indentation + "\tFirst Start Time:      " + new T8Timestamp(firstStartTime) + "ms");
        writer.println(indentation + "\tFirst End Time:        " + new T8Timestamp(firstEndTime) + "ms");
        writer.println(indentation + "\tLast Start Time:       " + new T8Timestamp(lastStartTime) + "ms");
        writer.println(indentation + "\tLast End Time:         " + new T8Timestamp(lastEndTime) + "ms");
        writer.flush();

        // Print all sub-execution statistics.
        for (T8ExecutionPerformanceStatistics stats : subExecutionStatistics.values())
        {
            stats.printStatistics(stream);
        }
    }
}
