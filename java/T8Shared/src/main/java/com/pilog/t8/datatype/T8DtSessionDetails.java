package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.security.T8SessionDetails;
import com.pilog.t8.security.T8WorkflowProfileDetails;

/**
 * @author Gavin Boshoff
 */
public class T8DtSessionDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@SESSION_DETAILS";

    public T8DtSessionDetails() {}

    public T8DtSessionDetails(T8DefinitionManager context) {}

    public T8DtSessionDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8SessionDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtWorkflowProfileDetails dtWorkflowProfileDetails;
            T8DtUserProfileDetails dtUserProfileDetails;
            JsonArray workflowProfileArray;
            JsonObject jsonSessionDetails;
            T8SessionDetails sessionDetails;

            // Create the JSON object.
            dtUserProfileDetails = new T8DtUserProfileDetails();
            sessionDetails = (T8SessionDetails)object;
            workflowProfileArray = new JsonArray();

            jsonSessionDetails = new JsonObject();
            jsonSessionDetails.add("sessionId", sessionDetails.getSessionId());
            jsonSessionDetails.add("userId", sessionDetails.getUserId());
            jsonSessionDetails.add("userName", sessionDetails.getUserName());
            jsonSessionDetails.add("userSurname", sessionDetails.getUserSurname());
            jsonSessionDetails.add("userProfileDetails", dtUserProfileDetails.serialize(sessionDetails.getUserProfileDetails()));
            jsonSessionDetails.add("workflowProfileDetails", workflowProfileArray);
            jsonSessionDetails.add("performanceStatisticsEnabled", sessionDetails.isPerformanceStatisticsEnabled());

            // Add all workflow profile details.
            dtWorkflowProfileDetails = new T8DtWorkflowProfileDetails();
            for (T8WorkflowProfileDetails profileDetails : sessionDetails.getWorkflowProfileDetails())
            {
                workflowProfileArray.add(dtWorkflowProfileDetails.serialize(profileDetails));
            }

            // Return the final JSON object.
            return jsonSessionDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8SessionDetails deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DtUserProfileDetails dtUserProfileDetails;
            T8SessionDetails sessionDetails;
            JsonObject jsonSessionDetails;
            JsonArray workflowProfileArray;

            // Create the file details object.
            jsonSessionDetails = jsonValue.asObject();
            dtUserProfileDetails = new T8DtUserProfileDetails();

            sessionDetails = new T8SessionDetails();
            sessionDetails.setSessionId(jsonSessionDetails.getString("sessionId"));
            sessionDetails.setUserId(jsonSessionDetails.getString("userId"));
            sessionDetails.setUserName(jsonSessionDetails.getString("userName"));
            sessionDetails.setUserSurname(jsonSessionDetails.getString("userSurname"));
            sessionDetails.setUserProfileDetails(dtUserProfileDetails.deserialize(jsonSessionDetails.getJsonObject("userProfileDetails")));
            sessionDetails.setPerformanceStatisticsEnabled(jsonSessionDetails.getBoolean("performanceStatisticsEnabled"));

            // Add the workflow profile details.
            workflowProfileArray = jsonSessionDetails.getJsonArray("workflowProfileDetails");
            if (workflowProfileArray != null)
            {
                T8DtWorkflowProfileDetails dtWorkflowProfileDetails;

                dtWorkflowProfileDetails = new T8DtWorkflowProfileDetails();
                for (JsonValue workflowProfileDetails : workflowProfileArray)
                {
                    sessionDetails.addWorkflowProfileDetails(dtWorkflowProfileDetails.deserialize(workflowProfileDetails));
                }
            }

            // Return the completed object.
            return sessionDetails;
        }
        else return null;
    }
}