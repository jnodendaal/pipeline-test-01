package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.report.T8ReportFilter;
import com.pilog.t8.report.T8ReportFilter.ReportOrdering;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DtReportFilter extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@REPORT_FILTER";

    public T8DtReportFilter() {}

    public T8DtReportFilter(T8DefinitionManager context) {}

    public T8DtReportFilter(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8ReportFilter.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject jsonReportFilter;
            T8ReportFilter reportFilter;
            Set<String> reportIds;

            // Create the concept JSON object.
            reportFilter = (T8ReportFilter)object;
            jsonReportFilter = new JsonObject();
            jsonReportFilter.add("userId", reportFilter.getUserId());
            jsonReportFilter.add("ordering", reportFilter.getReportOrdering().toString());

            // Add the report ids to filter by.
            reportIds = reportFilter.getIncludedReportIds();
            if ((reportIds != null) && (!reportIds.isEmpty()))
            {
                JsonArray reportIdArray;

                reportIdArray = new JsonArray();
                jsonReportFilter.add("reportIds", reportIdArray);
                for (String reportId : reportIds)
                {
                    reportIdArray.add(reportId);
                }
            }

            // Return the final report filter object.
            return jsonReportFilter;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject jsonReportFilter;
            T8ReportFilter reportFilter;
            JsonArray reportIdArray;
            String ordering;

            // Get the JSON values.
            jsonReportFilter = jsonValue.asObject();
            reportIdArray = jsonReportFilter.getJsonArray("reportIds");
            ordering = jsonReportFilter.getString("ordering");

            // Create the report filter object.
            reportFilter = new T8ReportFilter();
            reportFilter.setUserId(jsonReportFilter.getString("userId"));
            reportFilter.setReportOrdering(ordering != null ? ReportOrdering.valueOf(ordering) : ReportOrdering.TIME_COMPLETED_DESCENDING);

            // Add the filter report id list if specified.
            if (reportIdArray != null)
            {
                for (JsonValue reportIdValue : reportIdArray.values())
                {
                    reportFilter.addIncludedReportId(reportIdValue.asString());
                }
            }

            // Return the completed object.
            return reportFilter;
        }
        else return null;
    }
}