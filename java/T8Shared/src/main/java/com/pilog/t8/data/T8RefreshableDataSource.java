package com.pilog.t8.data;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8RefreshableDataSource extends T8DataSource
{
    public void refresh(Map<String, Object> parameters) throws Exception;
}
