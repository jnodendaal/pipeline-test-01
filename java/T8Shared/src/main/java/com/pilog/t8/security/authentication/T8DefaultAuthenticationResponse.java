package com.pilog.t8.security.authentication;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8SessionContext;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultAuthenticationResponse implements T8AuthenticationResponse, Serializable
{
    private final T8SessionContext sessionContext;
    private final ResponseType responseType;

    public T8DefaultAuthenticationResponse(ResponseType responseType, T8SessionContext sessionContext)
    {
        this.responseType = responseType;
        this.sessionContext = sessionContext;
    }

    @Override
    public ResponseType getResponseType()
    {
        return responseType;
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    @Override
    public JsonObject serialize()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUserId()
    {
        return sessionContext != null ? sessionContext.getUserIdentifier() : null;
    }
}
