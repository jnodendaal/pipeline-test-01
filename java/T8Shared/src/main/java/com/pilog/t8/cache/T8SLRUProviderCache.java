/**
 * Created on 08 Feb 2016, 2:46:55 PM
 */
package com.pilog.t8.cache;

import com.pilog.t8.utilities.cache.SLRUCache;

/**
 * A {@code T8SLRUProviderCache} is a dynamic SLRU cache which allows for any
 * form of data to be cached, with a provider which can be used to supply the
 * data required to fill the cache, as and when requested.<br/>
 * <br/>
 * The basis of the cache consists of 2 things.
 * <ol>
 * <li>The {@code T8CacheDataProvider}, responsible for the provision of values
 * not yet available within the cache.</li>
 * <li>The underlying {@link SLRUCache} with a size (as specified during
 * instantiation) + 25%.</li>
 * </ol>
 * The {@code T8CacheDataProvider} is a supplier taking a single parameter to
 * return the required value to be cached, if it exists. {@code null} safe
 * caches are the responsibility of the implementer.
 *
 * @author Gavin Boshoff
 *
 * @param <K> The key type for the cache
 * @param <V> The value type for values within the cache
 */
public class T8SLRUProviderCache<K, V>
{
    private static final long serialVersionUID = -3511338993151510522L;

    private final T8CacheDataProvider<K, V> provider;
    private final int cacheSize;

    private SLRUCache<K, V> slruCache;

    public T8SLRUProviderCache(T8CacheDataProvider<K, V> provider, int cacheSize)
    {
        this.cacheSize = cacheSize;
        this.provider = provider;
        this.slruCache = new SLRUCache<>(cacheSize);
    }

    public V get(K key)
    {
        if (this.slruCache.isCached(key))
        {
            return this.slruCache.getCached(key);
        }
        else
        {
            V value;

            this.slruCache.cacheNew(key, (value = this.provider.retrieve(key)));

            return value;
        }
    }

    public void flush()
    {
        this.slruCache = new SLRUCache<>(this.cacheSize);
    }
}