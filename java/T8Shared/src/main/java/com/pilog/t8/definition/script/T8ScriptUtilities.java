package com.pilog.t8.definition.script;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptUtilities
{
    private static final String PREFIX_EXCLUSION = "^|[^@#\\$0-9a-zA-Z_]";
    private static final String SUFFIX_EXCLUSION = "[^0-9a-zA-Z_]|$";

    public static List<String> getReferencedIds(String script, boolean includeLocalIds, boolean includePrivateIds, boolean includePublicIds)
    {
        StringBuffer scriptBuffer;
        List<String> ids;

        ids = new ArrayList<>();
        scriptBuffer = new StringBuffer(script);

        if (includeLocalIds)
        {
            ids.addAll(StringUtilities.findStrings(PREFIX_EXCLUSION, T8Definition.getLocalIdRegex(), SUFFIX_EXCLUSION, scriptBuffer));
        }

        if (includePrivateIds)
        {
            return StringUtilities.findStrings(PREFIX_EXCLUSION, T8Definition.getPrivateIdRegex(), SUFFIX_EXCLUSION, scriptBuffer);
        }

        if (includePublicIds)
        {
            return StringUtilities.findStrings(PREFIX_EXCLUSION, T8Definition.getPublicIdRegex(), SUFFIX_EXCLUSION, scriptBuffer);
        }

        return ids;
    }

    public static boolean containsIdentifier(String script, String identifier)
    {
        return StringUtilities.findStrings(PREFIX_EXCLUSION, Pattern.quote(identifier), SUFFIX_EXCLUSION, new StringBuffer(script)).size() > 0;
    }

    public static String replaceIdentifier(String script, String oldIdentifier, String newIdentifier, String namespace)
    {
        if (namespace != null)
        {
            ArrayList<String> namespaces;

            namespaces = new ArrayList<String>();
            namespaces.add(namespace);
            return replaceIdentifier(script, oldIdentifier, newIdentifier, namespaces);
        }
        else return replaceIdentifier(script, oldIdentifier, newIdentifier, (List<String>)null);
    }

    public static String replaceIdentifier(String script, String oldId, String newId, List<String> namespaces)
    {
        StringBuffer valueString;

        valueString = new StringBuffer(script);

        if (T8IdentifierUtilities.isPublicId(oldId))
        {
            StringUtilities.replaceAll(PREFIX_EXCLUSION, Pattern.quote(oldId), SUFFIX_EXCLUSION, valueString, newId);
        }
        else if (T8IdentifierUtilities.isPrivateId(oldId))
        {
            StringUtilities.replaceAll(PREFIX_EXCLUSION, Pattern.quote(oldId), SUFFIX_EXCLUSION, valueString, newId);
        }
        else if (T8IdentifierUtilities.isLocalId(oldId))
        {
            StringUtilities.replaceAll(PREFIX_EXCLUSION, Pattern.quote(oldId), SUFFIX_EXCLUSION, valueString, newId);
        }
        else if (T8IdentifierUtilities.isQualifiedLocalId(oldId))
        {
            // First replace it just as it is, for incase the fully qualified old identifier is used in the script.
            StringUtilities.replaceAll(PREFIX_EXCLUSION, Pattern.quote(oldId), SUFFIX_EXCLUSION, valueString, newId);

            // Now remove the applicable namespace and then replace the local part.
            if (namespaces != null)
            {
                String targetIdentifier;
                String replacementIdentifier;

                targetIdentifier = T8IdentifierUtilities.removeApplicableNamespace(oldId, namespaces);
                replacementIdentifier = T8IdentifierUtilities.removeApplicableNamespace(newId, namespaces);
                StringUtilities.replaceAll(PREFIX_EXCLUSION, Pattern.quote(targetIdentifier), SUFFIX_EXCLUSION, valueString, replacementIdentifier);
            }
        }
        else throw new RuntimeException("Old Identifier that is to be renamed does not fit any valid identifier patterns: " + oldId);

        return valueString.toString();
    }
}
