package com.pilog.t8.definition.data.filter.advanced;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterCriteriaDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8AdvancedDataFilterDefinition extends T8DataFilterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_FILTER";
    public static final String DISPLAY_NAME = "Advanced Data Filter";
    public static final String DESCRIPTION = "An object that specifies filter criteria to be used for filtering data entities.";
    private enum Datum {FILTER_CRITERIA_DEFINITION,
                        FIELD_ODERING_MAP};
    // -------- Definition Meta-Data -------- //

    public T8AdvancedDataFilterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.FILTER_CRITERIA_DEFINITION.toString(), "Filter Criteria", "The filter criteria specified by this data filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_STRING_MAP, Datum.FIELD_ODERING_MAP.toString(), "Data Ordering", "The ordering that will be applied to data retrieved by this filter.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString())));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILTER_CRITERIA_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataFilterCriteriaDefinition.TYPE_IDENTIFIER));
        else if (Datum.FIELD_ODERING_MAP.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new LinkedHashMap<>();
                    if (fieldDefinitions != null)
                    {
                        ArrayList<String> options;

                        options = new ArrayList<>();
                        options.add(OrderMethod.ASCENDING.toString());
                        options.add(OrderMethod.DESCENDING.toString());
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            identifierMap.put(fieldDefinition.getPublicIdentifier(), options);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(getFieldOrderingMap() != null)
        {
            for (String fieldIdentifier : getFieldOrderingMap().keySet())
            {
                if (!Objects.equals(getDataEntityIdentifier(), T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier)))
                {
                    validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.FIELD_ODERING_MAP.toString(), "Invalid Field Identifier " + fieldIdentifier, T8DefinitionValidationError.ErrorType.CRITICAL));
                }
            }
        }

        return validationErrors;
    }

    @Override
    public T8DataFilter getNewDataFilterInstance(T8Context context, Map<String, Object> filterParameters)
    {
        T8DataFilterCriteriaDefinition filterCriteriaDefinition;
        T8DataFilter dataFilter;

        dataFilter = new T8DataFilter(this);
        dataFilter.setFieldOrdering(getFieldOrderingMap());
        filterCriteriaDefinition = getFilterCriteriaDefinition();
        if (filterCriteriaDefinition != null) dataFilter.setFilterCriteria(filterCriteriaDefinition.getNewDataFilterClauseInstance(context, filterParameters));

        return dataFilter;
    }

    public T8DataFilterCriteriaDefinition getFilterCriteriaDefinition()
    {
        return (T8DataFilterCriteriaDefinition)getDefinitionDatum(Datum.FILTER_CRITERIA_DEFINITION.toString());
    }

    public void setFilterCriteriaDefinition(T8DataFilterCriteriaDefinition definition)
    {
        setDefinitionDatum(Datum.FILTER_CRITERIA_DEFINITION.toString(), definition);
    }

    public void setFieldOrderingMap(Map<String, OrderMethod> orderingMap)
    {
        setDefinitionDatum(Datum.FIELD_ODERING_MAP.toString(), orderingMap);
    }

    public LinkedHashMap<String, OrderMethod> getFieldOrderingMap()
    {
        Map<String, String> orderMapDatum;

        orderMapDatum = (Map<String, String>)getDefinitionDatum(Datum.FIELD_ODERING_MAP.toString());
        if (orderMapDatum != null)
        {
            LinkedHashMap<String, OrderMethod> orderMap;

            orderMap = new LinkedHashMap<>();
            for (String fieldIdentifier : orderMapDatum.keySet())
            {
                String orderMethod;

                orderMethod = orderMapDatum.get(fieldIdentifier);
                if (orderMethod != null)
                {
                    orderMap.put(fieldIdentifier, OrderMethod.valueOf(orderMethod));
                }
            }

            return orderMap;
        } else return null;
    }
}
