package com.pilog.t8.time;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Bouwer du Preez
 */
public class T8Date implements Serializable, Comparable<T8Date>
{
    public static final DateFormat STANDARD_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private long milliseconds;

    public static T8Date fromTime(Long milliseconds)
    {
        if (milliseconds == null) return null;
        else return new T8Date(milliseconds);
    }

    public T8Date(long milliseconds)
    {
        this.milliseconds = normalizeMilliseconds(milliseconds);
    }

    public long getMilliseconds()
    {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds)
    {
        this.milliseconds = normalizeMilliseconds(milliseconds);
    }

    /**
     * Normalizes the millisecond value to represent the contained date to the
     * same day, but to 11 AM of that day with no smaller date/time part.<br>
     * <br>
     * This is not full-proof since we rely on the fact that areas of the
     * UTC-12:00 time-zone are considered uninhabited. We also rely on the
     * assumption that the areas falling within the UTC+12:00 or greater
     * time-zone will most likely never be worked with.
     *
     * @param milliseconds The time-zone independent millisecond value
     *
     * @return A millisecond value representing the original Date and Time as
     *      at 11AM of that day, regardless of the original time section
     */
    private long normalizeMilliseconds(long milliseconds)
    {
        // TODO: GBO - Look into LocalDate API
        Calendar msCal;

        // We normalize the time-zone to 0 so that we can ignore it
        msCal = Calendar.getInstance();
        msCal.setTimeInMillis(milliseconds);

        // Complete the normalization
        msCal.set(Calendar.HOUR_OF_DAY, 11);
        msCal.set(Calendar.MINUTE, 0);
        msCal.set(Calendar.SECOND, 0);
        msCal.set(Calendar.MILLISECOND, 0);

        // We compensate for the current time zone
        msCal.add(Calendar.MILLISECOND, (-1*msCal.getTimeZone().getRawOffset()));
        msCal.setTimeZone(TimeZone.getTimeZone("GMT"));

        return msCal.getTimeInMillis();
    }

    @Override
    public int hashCode()
    {
        return 29 * 3 + (int) (this.milliseconds ^ (this.milliseconds >>> 32));
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        final T8Date other = (T8Date) obj;
        return this.milliseconds == other.milliseconds;
    }

    @Override
    public String toString()
    {
        return STANDARD_FORMAT.format(new Date(milliseconds));
    }

    @Override
    public int compareTo(T8Date other)
    {
        return Long.compare(this.milliseconds, other.milliseconds);
    }
}
