package com.pilog.t8.security;

/**
 * @author Bouwer du Preez
 */
public interface T8ContextCode
{
    public Object execute(T8Context context) throws Exception;
}