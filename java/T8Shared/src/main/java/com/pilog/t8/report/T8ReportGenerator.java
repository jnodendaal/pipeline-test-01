package com.pilog.t8.report;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ReportGenerator
{
    public T8ReportDescription generateReportDescription(String reportIid, Map<String, Object> parameters);
    public T8ReportGenerationResult generateReport(String reportIid, Map<String, Object> parameters, String fileContextId, String filepath) throws Exception;
    public double getProgress();
    public void cancel();
}
