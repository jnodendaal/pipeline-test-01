package com.pilog.t8.definition;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.definition.documentation.T8DefaultDefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.definition.event.T8DefinitionChangeListener;
import com.pilog.t8.definition.event.T8DefinitionDatumChangeEvent;
import com.pilog.t8.definition.event.T8DefinitionDescendentDatumChangeEvent;
import com.pilog.t8.definition.event.T8DefinitionDescendentMetaDataChangeEvent;
import com.pilog.t8.definition.event.T8DefinitionMetaDataChangeEvent;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.script.T8ScriptUtilities;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.utilities.threads.ThreadUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8Definition implements Serializable
{
    // The ID used to verify serialized object revision.
    static final long serialVersionUID = 1L;

    private String identifier; // The identifier of this definition, uniquely identifying it within its project context.
    private String description; // The description of this definition used for development purposes.
    private String displayName; // The display name of this definition used for development purposes.
    private String projectId; // The identifier of the project to which this definition belongs.
    private String versionId; // The identifier of the project revision to which this definition belongs belongs.
    private String createdUserIdentifier; // Identifier of the user that first created this definition.  Only stored on global definition.
    private Long createdTime; // Time that this definition was first created.  Only stored on global definition.
    private String updatedUserIdentifier; // Identifier of user that last saved changes to this definition.  Only stored on global definition.
    private Long updatedTime; // Time that changes were last saved to this definition.  Only stored on global definition.
    private String finalizedUserIdentifier; // Identifier of user that last finalized changes on this definition.  Only stored on global definition.
    private Long finalizedTime; // Time that changes were last finalized on this definition.  Only stored on global definition.
    private String checksum; // Checksum computed for this definition when last authenticated revision was saved.  Only stored on root definitions.
    private Long revision; // The revision of this definition.  This value is incremented by the definition manager every time changes are saved to this definition.
    private T8DefinitionStatus status; // The status of this definition as loaded form meta data manager.
    private final LinkedHashMap<String, Object> definitionData; // The map holding the datum values of this definition.
    private final List<T8DefinitionComment> comments; // A list of text comments linked to this definition by contributors.
    private boolean locked; // A boolean flag indicating whether or not this definition is locked (updates prohibited).
    private boolean patched; // A boolean flag indicating whether or not this definition currently has a patch applied to it.
    private T8Definition parentDefinition;  // The parent of this definition within the local hierarchy to which it belongs.

    private transient EventListenerList definitionListeners;

    private static final String LOCAL_ID_PREFIX = "$";
    private static final String PRIVATE_ID_PREFIX = "#";
    private static final String PUBLIC_ID_PREFIX = "@";
    private static final String LOCAL_ID_REGEX = "\\$[0-9a-zA-Z_]+";
    private static final String PRIVATE_ID_REGEX = "#[0-9a-zA-Z_]+";
    private static final String PUBLIC_ID_REGEX = "@[0-9a-zA-Z_]+";
    private static final String QUALIFIED_LOCAL_ID_REGEX = "(" + PUBLIC_ID_REGEX + "|" + PRIVATE_ID_REGEX + ")" + LOCAL_ID_REGEX;
    private static final String ID_REGEX = "(@|#|\\$)[0-9a-zA-Z_]+";

    // Having the patterns pre-compiled prevents compilation on every use.
    private static final Pattern ID_PATTERN = Pattern.compile(ID_REGEX);
    private static final Pattern LOCAL_ID_PATTERN = Pattern.compile(LOCAL_ID_REGEX);
    private static final Pattern PRIVATE_ID_PATTERN = Pattern.compile(PRIVATE_ID_REGEX);
    private static final Pattern PUBLIC_ID_PATTERN = Pattern.compile(PUBLIC_ID_REGEX);
    private static final Pattern QUALIFIED_LOCAL_ID_PATTERN = Pattern.compile(QUALIFIED_LOCAL_ID_REGEX);

    public enum DefinitionValidationType {LOCAL, GLOBAL};
    public enum T8DefinitionStatus {UPDATED, FINALIZED};
    public enum T8DefinitionLevel {ROOT, SYSTEM, PROJECT, RESOURCE};

    public T8Definition(String identifier)
    {
        this.identifier = identifier;
        this.comments = new ArrayList<>();
        this.definitionData = new LinkedHashMap<>();
        this.createdTime = System.currentTimeMillis();
        this.updatedTime = null;
        this.finalizedTime = null;
        this.createdUserIdentifier = null;
        this.updatedUserIdentifier = null;
        this.finalizedUserIdentifier = null;
        this.status = null;
        this.locked = false;
        this.patched = false;
        setDefaultValues();
    }

    public T8DefinitionHandle getHandle()
    {
        T8DefinitionHandle handle;

        handle = new T8DefinitionHandle(getLevel(), identifier, projectId);
        handle.setTypeIdentifier(T8DefinitionUtilities.getDefinitionTypeIdentifier(this));
        handle.setGroupIdentifier(T8DefinitionUtilities.getDefinitionGroupIdentifier(this));
        return handle;
    }

    public T8DefinitionMetaData getMetaData()
    {
        T8DefinitionMetaData metaData;

        metaData = new T8DefinitionMetaData();
        metaData.setId(getIdentifier());
        metaData.setDefinitionLevel(T8DefinitionUtilities.getDefinitionLevel(this));
        metaData.setTypeId(T8DefinitionUtilities.getDefinitionTypeIdentifier(this));
        metaData.setGroupId(T8DefinitionUtilities.getDefinitionGroupIdentifier(this));
        metaData.setDisplayName(getMetaDisplayName());
        metaData.setDescription(getMetaDescription());
        metaData.setVersionId(versionId);
        metaData.setProjectId(getProjectIdentifier());
        metaData.setComments(comments);
        metaData.setCreatedTime(createdTime);
        metaData.setCreatedUserId(createdUserIdentifier);
        metaData.setUpdatedTime(updatedTime);
        metaData.setUpdatedUserId(updatedUserIdentifier);
        metaData.setChecksum(checksum);
        metaData.setStatus(status);
        metaData.setRevision(revision);
        metaData.setPatched(patched);
        metaData.setPatchedContent(containsPatchedContent());
        metaData.setPublic(isPublic());
        return metaData;
    }

    public T8DefinitionTypeMetaData getTypeMetaData()
    {
        return T8DefinitionUtilities.getDefinitionTypeMetaData(this);
    }

    public T8Definition copy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void setDefaultValues()
    {
        List<T8DefinitionDatumType> datumTypes;

        datumTypes = getDatumTypes();
        if (datumTypes != null)
        {
            for (T8DefinitionDatumType datumType : getDatumTypes())
            {
                Object defaultValue;

                defaultValue = datumType.getDefaultValue();
                if (defaultValue != null)
                {
                    setDefinitionDatum(datumType.getIdentifier(), defaultValue, false, null);
                }
                else
                {
                    if (datumType.isDefinitionListType())
                    {
                        setDefinitionDatum(datumType.getIdentifier(), new ArrayList<>(), false, null);
                    }
                }
            }
        }
    }

    void setParentDefinition(T8Definition definition)
    {
        this.parentDefinition = definition;
    }

    /**
     * Returns the definition to which this definition logically belongs.
     *
     * @return The definition that contains this definition.  null if this
     * definition is a root definition and therefore has not parent.
     */
    public T8Definition getParentDefinition()
    {
        return parentDefinition;
    }

    /**
     * Returns a list of all the definitions that are local ancestors of this
     * definition.
     *
     * @return A list of the local ancestors of this definition.  The first
     * definition in the list will be this definition's parent and the last will
     * be the root.  If this definition is the root, the list will be empty.
     */
    public List<T8Definition> getAncestorDefinitions()
    {
        ArrayList<T8Definition> ancestors;
        T8Definition parent;

        ancestors = new ArrayList<>();
        parent = getParentDefinition();
        while (parent != null)
        {
            ancestors.add(parent);
            parent = parent.getParentDefinition();
        }

        return ancestors;
    }

    /**
     * Returns a list of ancestor definitions of this definition matching the
     * specified definition type.
     *
     * @param definitionTypeIdentifier The definition type identifier to match.
     * @return A list of this definition's ancestors matching the specified
     * type.
     */
    public List<T8Definition> getAncestorDefinitions(String definitionTypeIdentifier)
    {
        ArrayList<T8Definition> ancestors;
        T8Definition parent;

        ancestors = new ArrayList<>();
        parent = getParentDefinition();
        while (parent != null)
        {
            T8DefinitionTypeMetaData metaData;

            // Only add the parent to the list if it is of the specified type.
            metaData = parent.getTypeMetaData();
            if (metaData.getTypeId().equals(definitionTypeIdentifier)) ancestors.add(parent);

            parent = parent.getParentDefinition();
        }

        return ancestors;
    }

    /**
     * Returns the first parent definition of this definition matching the
     * specified class type.
     *
     * @param <D> The subtype of {@code T8Definition} to be returned
     * @param classType The definition type identifier to match.
     *
     * @return The first parent definition matching the required type.
     */
    @SuppressWarnings("unchecked")
    public <D extends T8Definition> D getAncestorDefinition(Class<D> classType)
    {
        T8Definition matchingDefinition;

        matchingDefinition = parentDefinition;
        while (matchingDefinition != null)
        {
            if (classType.isInstance(matchingDefinition))
            {
                return (D)matchingDefinition;
            }
            else matchingDefinition = matchingDefinition.getParentDefinition();
        }

        return null;
    }

    /**
     * Returns the first parent definition of this definition matching the
     * specified definition type.
     *
     * @param definitionTypeIdentifier The definition type identifier to match.
     * @return The first parent definition matching the required type.
     */
    public T8Definition getAncestorDefinition(String definitionTypeIdentifier)
    {
        T8Definition matchingDefinition;

        matchingDefinition = parentDefinition;
        while (matchingDefinition != null)
        {
            T8DefinitionTypeMetaData metaData;

            metaData = matchingDefinition.getTypeMetaData();
            if (metaData.getTypeId().equals(definitionTypeIdentifier))
            {
                return matchingDefinition;
            }
            else matchingDefinition = matchingDefinition.getParentDefinition();
        }

        return null;
    }

    /**
     * Returns a boolean value indicating whether or not this definition is publicly accessible i.e. outside of its parent project.
     * @return A boolean value indicating whether or not this definition is publicly accessible i.e. outside of its parent project.
     */
    public boolean isPublic()
    {
        // Currently, the check for this is only on the identifier but a more specific check may be implemented in future.
        return this.identifier.startsWith(PUBLIC_ID_PREFIX);
    }

    public Object getAncestorDefinitionDatum(String definitionTypeIdentifier, String datumIdentifier)
    {
        T8Definition matchingDefinition;

        matchingDefinition = parentDefinition;
        while (matchingDefinition != null)
        {
            if (definitionTypeIdentifier == null)
            {
                if (matchingDefinition.containsDefinitionDatum(datumIdentifier))
                {
                    return matchingDefinition.getDefinitionDatum(datumIdentifier);
                }
                else matchingDefinition = matchingDefinition.getParentDefinition();
            }
            else
            {
                T8DefinitionTypeMetaData metaData;

                metaData = matchingDefinition.getTypeMetaData();
                if (metaData.getTypeId().equals(definitionTypeIdentifier))
                {
                    return matchingDefinition.getDefinitionDatum(datumIdentifier);
                }
                else matchingDefinition = matchingDefinition.getParentDefinition();
            }
        }

        return null;
    }

    public List<T8Definition> getLocalDefinitionsOfType(String typeIdentifier)
    {
        List<T8Definition> localDefinitions;
        Iterator<T8Definition> definitionIterator;

        localDefinitions = getLocalDefinitions();
        definitionIterator = localDefinitions.iterator();
        while (definitionIterator.hasNext())
        {
            if (!Objects.equals(definitionIterator.next().getTypeMetaData().getTypeId(), typeIdentifier))
            {
                definitionIterator.remove();
            }
        }

        return localDefinitions;
    }

    public List<T8Definition> getLocalDefinitionsOfGroup(String groupIdentifier)
    {
        List<T8Definition> localDefinitions;
        Iterator<T8Definition> definitionIterator;

        localDefinitions = getLocalDefinitions();
        definitionIterator = localDefinitions.iterator();
        while (definitionIterator.hasNext())
        {
            if (!Objects.equals(definitionIterator.next().getTypeMetaData().getGroupId(), groupIdentifier))
            {
                definitionIterator.remove();
            }
        }

        return localDefinitions;
    }

    public List<T8Definition> getLocalDefinitions()
    {
        T8Definition rootDefinition;
        List<T8Definition> localDefinitions;

        rootDefinition = getRootDefinition();
        localDefinitions = rootDefinition.getDescendentDefinitions();
        localDefinitions.add(rootDefinition);
        return localDefinitions;
    }

    public T8Definition getLocalDefinition(String identifier)
    {
        T8Definition rootDefinition;
        List<T8Definition> localDefinitions;

        rootDefinition = getRootDefinition();
        localDefinitions = rootDefinition.getDescendentDefinitions();
        for (T8Definition localDefinition : localDefinitions)
        {
            if (localDefinition.getIdentifier().equals(identifier))
            {
                return localDefinition;
            }
        }

        return null;
    }

    public T8Definition getRootDefinition()
    {
        T8Definition rootDefinition;

        rootDefinition = this;
        while (rootDefinition.getParentDefinition() != null)
        {
            rootDefinition = rootDefinition.getParentDefinition();
        }

        return rootDefinition;
    }

    public String getRootProjectId()
    {
        T8Definition rootDefinition;

        rootDefinition = getRootDefinition();
        return rootDefinition != null ? rootDefinition.getProjectIdentifier() : null;
    }

    public boolean isUpdated()
    {
        if (isGlobalDefinition())
        {
            try
            {
                String currentChecksum;

                currentChecksum = T8DefinitionUtilities.computeChecksumHex(this);
                return !currentChecksum.equals(checksum);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while checking definition update status.", e);
                return true;
            }
        }
        else throw new RuntimeException("Updates are only tracked for global definitions.");
    }

    void setIdentifier(String newIdentifier, boolean propagateChange)
    {
        if (!this.identifier.equals(newIdentifier))
        {
            String oldIdentifier;

            oldIdentifier = this.identifier;
            this.identifier = newIdentifier;
            if (propagateChange)
            {
                getRootDefinition().changeReferenceId(oldIdentifier, newIdentifier, true);
            }
        }
    }

    public boolean containsPatchedContent()
    {
        if (patched) return true;
        else
        {
            for (T8Definition localDefinition : getLocalDefinitions())
            {
                if (localDefinition.isPatched()) return true;
            }
        }

        return false;
    }

    public boolean containsIdentifier(String identifier)
    {
        List<T8Definition> subDefinitions;

        // If the identifier is a project identifier, compare it with this definition's project identifier and do the update if required.
        if (Objects.equals(identifier, projectId)) return true;

        // Check all the datums in this definition and ensure that the identifier change is applied where applicable.
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            T8DataType datumDataType;

            datumDataType = datumType.getDataType();
            if (datumDataType.isType(T8DtString.IDENTIFIER))
            {
                String datumIdentifier;
                String stringValue;

                datumIdentifier = datumType.getIdentifier();
                stringValue = (String)getDefinitionDatum(datumIdentifier);
                if (stringValue != null)
                {
                    if (T8ScriptUtilities.containsIdentifier(stringValue, identifier)) return true;
                }
            }
            else if (datumDataType.isType(T8DtList.IDENTIFIER))
            {
                String datumIdentifier;
                List list;

                datumIdentifier = datumType.getIdentifier();
                list = (List)getDefinitionDatum(datumIdentifier);
                if (list != null)
                {
                    for (Object listElement : list)
                    {
                        if (listElement instanceof String)
                        {
                            if (T8ScriptUtilities.containsIdentifier((String)listElement, identifier)) return true;
                        }
                    }
                }
            }
            else if (datumDataType.isType(T8DtMap.IDENTIFIER))
            {
                String datumIdentifier;
                Map map;

                datumIdentifier = datumType.getIdentifier();
                map = (Map)getDefinitionDatum(datumIdentifier);
                if (map != null)
                {
                    for (Object key : map.keySet())
                    {
                        Object value;

                        value = map.get(key);

                        // Check the key.
                        if (key instanceof String)
                        {
                            if (T8ScriptUtilities.containsIdentifier((String)key, identifier)) return true;
                        }

                        // Check the value.
                        if (value instanceof String)
                        {
                            if (T8ScriptUtilities.containsIdentifier((String)value, identifier)) return true;
                        }
                    }
                }
            }
        }

        // Check all sub-definitions.
        subDefinitions = getSubDefinitions();
        for (T8Definition subDefinition : subDefinitions)
        {
            if (subDefinition.containsIdentifier(identifier)) return true;
        }

        // No reference found.
        return false;
    }

    /**
     * This method changes all references contained by this definition and its sub-definitions
     * from the specified old identifier to the specified new identifier.
     * @param oldIdentifier The old referenced ID to be changed.
     * @param newIdentifier The new reference to change to.
     * @param isLocalChange A boolean flag to indicate whether or not this is a local change.
     */
    public void changeReferenceId(String oldIdentifier, String newIdentifier, boolean isLocalChange)
    {
        List<T8Definition> subDefinitions;
        List<String> identifierNamespaces;

        // Get the namespaces of the script.
        identifierNamespaces = ArrayLists.newArrayList(getNamespace());

        // Do a check to ensure that a fully qualified old identifier is supplied for non-local changes.
        if ((T8IdentifierUtilities.isLocalId(oldIdentifier)) && (!isLocalChange)) throw new RuntimeException("Local identifier '" + oldIdentifier + "' found during non-local identifier propagation.");

        // If the identifier is a project identifier, compare it with this definition's project identifier and do the update if required.
        if (Objects.equals(oldIdentifier, projectId)) setProjectIdentifier(newIdentifier);

        // Propagate the identifier change to all sub-definitions.
        subDefinitions = getSubDefinitions();
        for (T8Definition subDefinition : subDefinitions)
        {
            subDefinition.changeReferenceId(oldIdentifier, newIdentifier, isLocalChange);
        }

        // Check all the datums in this definition and ensure that the identifier change is applied where applicable.
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            T8DataType datumDataType;

            datumDataType = datumType.getDataType();
            if (datumDataType.isType(T8DtString.IDENTIFIER))
            {
                String datumIdentifier;
                String identifierString;

                datumIdentifier = datumType.getIdentifier();
                identifierString = (String)getDefinitionDatum(datumIdentifier);
                if (identifierString != null)
                {
                    setDefinitionDatum(datumIdentifier, T8ScriptUtilities.replaceIdentifier(identifierString, oldIdentifier, newIdentifier, identifierNamespaces));
                }
            }
            else if (datumDataType.isType(T8DtList.IDENTIFIER))
            {
                String datumIdentifier;
                List oldList;

                datumIdentifier = datumType.getIdentifier();
                oldList = (List)getDefinitionDatum(datumIdentifier);
                if (oldList != null)
                {
                    ArrayList newList;

                    newList = new ArrayList();
                    for (Object oldListValue : oldList)
                    {
                        if (oldListValue instanceof String)
                        {
                            newList.add(T8ScriptUtilities.replaceIdentifier((String)oldListValue, oldIdentifier, newIdentifier, identifierNamespaces));
                        }
                        else
                        {
                            newList.add(oldListValue);
                        }
                    }

                    setDefinitionDatum(datumIdentifier, newList);
                }
            }
            else if (datumDataType.isType(T8DtMap.IDENTIFIER))
            {
                String datumIdentifier;
                Map oldMap;

                datumIdentifier = datumType.getIdentifier();
                oldMap = (Map)getDefinitionDatum(datumIdentifier);
                if (oldMap != null)
                {
                    HashMap newMap;

                    newMap = new HashMap();
                    for (Object oldKey : oldMap.keySet())
                    {
                        Object oldValue;
                        Object newKey;
                        Object newValue;

                        // Re-factor the key.
                        if (oldKey instanceof String)
                        {
                            newKey = T8ScriptUtilities.replaceIdentifier((String)oldKey, oldIdentifier, newIdentifier, identifierNamespaces);
                        }
                        else newKey = oldKey;

                        // Re-factor the value.
                        oldValue = oldMap.get(oldKey);
                        if (oldValue instanceof String)
                        {
                            newValue = T8ScriptUtilities.replaceIdentifier((String)oldValue, oldIdentifier, newIdentifier, identifierNamespaces);
                        }
                        else newValue = oldValue;

                        // Add the new key and value to the new map.
                        newMap.put(newKey, newValue);
                    }

                    // Set the new datum value.
                    setDefinitionDatum(datumIdentifier, newMap);
                }
            }
        }
    }

    public void removeReferenceId(String identifierToBeRemoved, boolean includeDescendants)
    {
        String identifierReplacementString;
        List<String> identifierNamespaces;

        // Get the namespace to be used for replacements.
        identifierNamespaces = ArrayLists.newArrayList(getNamespace());

        // Set the string to be used as replacement when the identifier is removed from a script or expression.
        identifierReplacementString = "***REFERENCE_REMOVED:" + identifierToBeRemoved.substring(1) + "***";

        // Remove references from descendants if required.
        if (includeDescendants)
        {
            List<T8Definition> subDefinitions;

            subDefinitions = getSubDefinitions();
            for (T8Definition subDefinition : subDefinitions)
            {
                subDefinition.removeReferenceId(identifierToBeRemoved, includeDescendants);
            }
        }

        // Check all the datums in this definition and ensure that the identifier is removed where applicable.
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            T8DataType datumDataType;

            datumDataType = datumType.getDataType();
            if (datumDataType.isType(T8DtString.IDENTIFIER))
            {
                T8DtString stringDataType;
                String datumIdentifier;
                String identifierString;

                datumIdentifier = datumType.getIdentifier();
                identifierString = (String)getDefinitionDatum(datumIdentifier);
                stringDataType = (T8DtString)datumDataType;
                if (!Strings.isNullOrEmpty(identifierString))
                {
                    switch (stringDataType.getType())
                    {
                        case DEFINITION_IDENTIFIER:
                            if (identifierString.equals(identifierToBeRemoved))
                            {
                                setDefinitionDatum(datumType.getIdentifier(), null);
                            }
                            break;
                        case EPIC_EXPRESSION:
                            setDefinitionDatum(datumIdentifier, T8ScriptUtilities.replaceIdentifier(identifierString, identifierToBeRemoved, identifierReplacementString, identifierNamespaces));
                            break;
                        case EPIC_SCRIPT:
                            setDefinitionDatum(datumIdentifier, T8ScriptUtilities.replaceIdentifier(identifierString, identifierToBeRemoved, identifierReplacementString, identifierNamespaces));
                            break;
                        default:
                            break;
                    }
                }
            }
            else if (datumDataType.isType(T8DtList.IDENTIFIER))
            {
                String datumIdentifier;
                boolean changed;
                List list;

                changed = false;
                datumIdentifier = datumType.getIdentifier();
                list = (List)getDefinitionDatum(datumIdentifier);
                if (list != null)
                {
                    Iterator valueIterator;

                    valueIterator = list.iterator();
                    while (valueIterator.hasNext())
                    {
                        Object listValue;

                        listValue = valueIterator.next();
                        if ((listValue instanceof String) && (listValue.equals(identifierToBeRemoved)))
                        {
                            changed = true;
                            valueIterator.remove();
                        }
                    }
                }

                if (changed) setDefinitionDatum(datumIdentifier, list);
            }
            else if (datumDataType.isType(T8DtMap.IDENTIFIER))
            {
                String datumIdentifier;
                Map map;

                datumIdentifier = datumType.getIdentifier();
                map = (Map)getDefinitionDatum(datumIdentifier);
                if (map != null)
                {
                    for (Object key : new HashSet(map.keySet()))
                    {
                        if ((key instanceof String) && ((key.equals(identifierToBeRemoved)) || (((String)key).startsWith(identifierToBeRemoved + '$'))))
                        {
                            map.remove(key);
                        }
                        else
                        {
                            Object value;

                            value = map.get(key);
                            if (value instanceof String)
                            {
                                if ((key.equals(identifierToBeRemoved)) || (((String)key).startsWith(identifierToBeRemoved + '$')))
                                {
                                    map.remove(key);
                                }
                                else
                                {
                                    map.put(key, T8ScriptUtilities.replaceIdentifier((String)value, identifierToBeRemoved, identifierReplacementString, identifierNamespaces));
                                }
                            }
                        }
                    }

                    setDefinitionDatum(datumIdentifier, map);
                }
            }
        }
    }

    /**
     * Returns a list of the definition references contained by this definition and, if specified,
     * all descendant definitions.
     * @param includeLocalReferences
     * @param includePrivateReferences
     * @param includePublicReferences
     * @param includeDescendants
     * @return
     */
    public Set<String> getReferenceIds(boolean includeLocalReferences, boolean includePrivateReferences, boolean includePublicReferences, boolean includeDescendants)
    {
        Set<String> identifierSet;

        // Create a set to hold the references.
        identifierSet = new HashSet<String>();

        // Set references from descendants if required.
        if (includeDescendants)
        {
            List<T8Definition> subDefinitions;

            subDefinitions = getSubDefinitions();
            for (T8Definition subDefinition : subDefinitions)
            {
                identifierSet.addAll(subDefinition.getReferenceIds(includeLocalReferences, includePrivateReferences, includePublicReferences, includeDescendants));
            }
        }

        // Check all the datums in this definition and ensure that the identifier change is applied where applicable.
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            T8DataType datumDataType;

            datumDataType = datumType.getDataType();
            if (datumDataType.isType(T8DtString.IDENTIFIER))
            {
                String datumIdentifier;
                String identifierString;

                datumIdentifier = datumType.getIdentifier();
                identifierString = (String)getDefinitionDatum(datumIdentifier);
                if (identifierString != null)
                {
                    identifierSet.addAll(T8ScriptUtilities.getReferencedIds(identifierString, includePrivateReferences, includeLocalReferences, includePublicReferences));
                }
            }
            else if (datumDataType.isType(T8DtList.IDENTIFIER))
            {
                String datumIdentifier;
                List list;

                datumIdentifier = datumType.getIdentifier();
                list = (List)getDefinitionDatum(datumIdentifier);
                if (list != null)
                {
                    for (Object listValue : list)
                    {
                        if (listValue instanceof String)
                        {
                            identifierSet.addAll(T8ScriptUtilities.getReferencedIds((String)listValue, includeLocalReferences, includePrivateReferences, includePublicReferences));
                        }
                    }
                }
            }
            else if (datumDataType.isType(T8DtMap.IDENTIFIER))
            {
                String datumIdentifier;
                Map map;

                datumIdentifier = datumType.getIdentifier();
                map = (Map)getDefinitionDatum(datumIdentifier);
                if (map != null)
                {
                    for (Object key : map.keySet())
                    {
                        Object value;

                        if (key instanceof String)
                        {
                            identifierSet.addAll(T8ScriptUtilities.getReferencedIds((String)key, includeLocalReferences, includePrivateReferences, includePublicReferences));
                        }

                        value = map.get(key);
                        if (value instanceof String)
                        {
                            identifierSet.addAll(T8ScriptUtilities.getReferencedIds((String)value, includeLocalReferences, includePrivateReferences, includePublicReferences));
                        }
                    }
                }
            }
        }

        // Return the complete set of reference ID's.
        return identifierSet;
    }

    public void addChangeListener(T8DefinitionChangeListener listener)
    {
        // Lazy initialization of definition listener list.
        if (definitionListeners == null) definitionListeners = new EventListenerList();
        definitionListeners.add(T8DefinitionChangeListener.class, listener);
        if (definitionListeners.getListenerCount() > 5)
        {
            T8Log.log("Warning:  Suspicious definition listener count (" + definitionListeners.getListenerCount() + ") in definition: " + identifier + ".  Last listener added by: " + ThreadUtilities.getInvokerStackTraceElement(Thread.currentThread()));
        }
    }

    public void removeChangeListener(T8DefinitionChangeListener listener)
    {
        if (definitionListeners != null)
        {
            definitionListeners.remove(T8DefinitionChangeListener.class, listener);
        }
    }

    private void fireMetaDataChangeEvent(T8DefinitionMetaData oldValue, T8DefinitionMetaData newValue, Object actor)
    {
        // Only fire the event if the two values are different.
        if (!Objects.equals(oldValue, newValue))
        {
            // Only fire the event on this definition if there are actually some listeners registered.
            if ((definitionListeners != null) && (definitionListeners.getListenerCount() > 0))
            {
                T8DefinitionMetaDataChangeEvent event;

                // Fire an event signalling a datum change on this definition.
                event = new T8DefinitionMetaDataChangeEvent(this, oldValue, newValue, actor);
                for (T8DefinitionChangeListener listener : definitionListeners.getListeners(T8DefinitionChangeListener.class))
                {
                    listener.metaDataChanged(event);
                }
            }

            // If this definition has a parent, fire the descendent definition change event on the parent.
            if (parentDefinition != null)
            {
                parentDefinition.fireDescendentMetaDataChangeEvent(this, oldValue, newValue, actor);
            }
        }
    }

    protected void fireDescendentMetaDataChangeEvent(T8Definition descendentDefinition, T8DefinitionMetaData oldValue, T8DefinitionMetaData newValue, Object actor)
    {
        // Only fire the event on this definition if there are actually some listeners registered.
        if ((definitionListeners != null) && (definitionListeners.getListenerCount() > 0))
        {
            T8DefinitionDescendentMetaDataChangeEvent event;

            // Fire an event signalling a datum change on one of the descendents of this definition.
            event = new T8DefinitionDescendentMetaDataChangeEvent(descendentDefinition, oldValue, newValue, actor);
            for (T8DefinitionChangeListener listener : definitionListeners.getListeners(T8DefinitionChangeListener.class))
            {
                listener.descendentMetaDataChanged(event);
            }
        }

        // If this definition has a parent, fire the descendent definition change event on the parent.
        if (parentDefinition != null)
        {
            parentDefinition.fireDescendentMetaDataChangeEvent(descendentDefinition, oldValue, newValue, actor);
        }
    }

    private void fireDatumChangeEvent(T8DefinitionDatumType datum, Object oldValue, Object newValue, Object actor)
    {
        // Only fire the event if the two values are different.
        if (!Objects.equals(oldValue, newValue))
        {
            // Only fire the event on this definition if there are actually some listeners registered.
            if ((definitionListeners != null) && (definitionListeners.getListenerCount() > 0))
            {
                T8DefinitionDatumChangeEvent event;

                // Fire an event signalling a datum change on this definition.
                event = new T8DefinitionDatumChangeEvent(this, datum, oldValue, newValue, actor);
                for (T8DefinitionChangeListener listener : definitionListeners.getListeners(T8DefinitionChangeListener.class))
                {
                    listener.datumChanged(event);
                }
            }

            // If this definition has a parent, fire the descendent definition change event on the parent.
            if (parentDefinition != null)
            {
                parentDefinition.fireDescendentDatumChangeEvent(this, datum, oldValue, newValue, actor);
            }
        }
    }

    protected void fireDescendentDatumChangeEvent(T8Definition descendentDefinition, T8DefinitionDatumType datum, Object oldValue, Object newValue, Object actor)
    {
        // Only fire the event on this definition if there are actually some listeners registered.
        if ((definitionListeners != null) && (definitionListeners.getListenerCount() > 0))
        {
            T8DefinitionDescendentDatumChangeEvent event;

            // Fire an event signalling a datum change on one of the descendents of this definition.
            event = new T8DefinitionDescendentDatumChangeEvent(descendentDefinition, datum, oldValue, newValue, actor);
            for (T8DefinitionChangeListener listener : definitionListeners.getListeners(T8DefinitionChangeListener.class))
            {
                listener.descendentDatumChanged(event);
            }
        }

        // If this definition has a parent, fire the descendent definition change event on the parent.
        if (parentDefinition != null)
        {
            parentDefinition.fireDescendentDatumChangeEvent(descendentDefinition, datum, oldValue, newValue, actor);
        }
    }

    public boolean isGlobalDefinition()
    {
        return parentDefinition == null;
    }

    public static String getPublicIdPrefix()
    {
        return PUBLIC_ID_PREFIX;
    }

    public static String getPrivateIdPrefix()
    {
        return PRIVATE_ID_PREFIX;
    }

    public static String getLocalIdPrefix()
    {
        return LOCAL_ID_PREFIX;
    }

    public static Pattern getIdPattern()
    {
        return ID_PATTERN;
    }

    public static String getIdRegex()
    {
        return ID_REGEX;
    }

    public static Pattern getPublicIdPattern()
    {
        return PUBLIC_ID_PATTERN;
    }

    public static String getPublicIdRegex()
    {
        return PUBLIC_ID_REGEX;
    }

    public static String getPrivateIdRegex()
    {
        return PRIVATE_ID_REGEX;
    }

    public static Pattern getPrivateIdPattern()
    {
        return PRIVATE_ID_PATTERN;
    }

    public static Pattern getQualifiedLocalIdPattern()
    {
        return QUALIFIED_LOCAL_ID_PATTERN;
    }

    public static String getQualifiedLocalIdRegex()
    {
        return QUALIFIED_LOCAL_ID_REGEX;
    }

    public static Pattern getLocalIdPattern()
    {
        return LOCAL_ID_PATTERN;
    }

    public static String getLocalIdRegex()
    {
        return LOCAL_ID_REGEX;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public String getNamespace()
    {
        T8Definition rootDefinition;

        rootDefinition = getRootDefinition();
        if ((rootDefinition != null) && (rootDefinition != this)) return rootDefinition.getIdentifier();
        else return getIdentifier();
    }

    public String getPublicIdentifier()
    {
        T8Definition rootDefinition;

        rootDefinition = getRootDefinition();
        if ((rootDefinition != null) && (rootDefinition != this)) return rootDefinition.getIdentifier() + getIdentifier();
        else return getIdentifier();
    }

    /**
     * Returns the identifier to use for display purposes on the UI.
     * The default behavior of this method is to simply return the regular
     * definition identifier but sub-classes may override this method to return
     * a more useful identifier in some special cases (e.g. when the actual
     * identifier of the definition is a generated String).
     *
     * @return A String that uniquely identifies this definition which will be
     * used for display purposes on the developer UI.
     */
    public String getDisplayIdentifier()
    {
        return identifier;
    }

    public final String getMetaDescription()
    {
        return description;
    }

    public final void setMetaDescription(String description)
    {
        if (!Objects.equals(this.description, description))
        {
            T8DefinitionMetaData oldValue;

            oldValue = this.getMetaData();
            this.description = description;
            fireMetaDataChangeEvent(oldValue, getMetaData(), null);
        }
    }

    public final String getMetaDisplayName()
    {
        return displayName;
    }

    public final void setMetaDisplayName(String displayName)
    {
        if (!Objects.equals(this.displayName, displayName))
        {
            T8DefinitionMetaData oldValue;

            oldValue = this.getMetaData();
            this.displayName = displayName;
            fireMetaDataChangeEvent(oldValue, getMetaData(), null);
        }
    }

    public final String getVersionId()
    {
        return versionId;
    }

    public final void setVersionId(String versionId)
    {
        if (!Objects.equals(this.versionId, versionId))
        {
            T8DefinitionMetaData oldValue;

            oldValue = this.getMetaData();
            this.versionId = versionId;
            fireMetaDataChangeEvent(oldValue, getMetaData(), null);
        }
    }

    public final String getProjectIdentifier()
    {
        return projectId;
    }

    public final void setProjectIdentifier(String projectIdentifier)
    {
        if (!Objects.equals(this.projectId, projectIdentifier))
        {
            T8DefinitionMetaData oldValue;

            oldValue = this.getMetaData();
            this.projectId = projectIdentifier;
            fireMetaDataChangeEvent(oldValue, getMetaData(), null);
        }
    }

    /**
     * Specifies whether or not the current raw definition instance is locked.
     * If locked, the current instance cannot be modified.<br/>
     * <br/>
     * If set to {@code false}, this is usually an indication that the
     * definition is opened for editing and should be locked for all other
     * users/processes, to prevent simultaneous editing.
     *
     * @return {@code true} if the current instance is locked. {@code false}
     *      otherwise
     */
    public boolean isLocked()
    {
        return locked;
    }

    /**
     * Specifies whether or not the current raw definition instance is locked.
     * If locked, the current instance cannot be modified.<br/>
     * <br/>
     * If set to {@code false}, this is usually an indication that the
     * definition is opened for editing and should be locked for all other
     * users/processes, to prevent simultaneous editing.
     *
     * @param locked {@code true} to lock the current instance
     */
    final void setLocked(boolean locked)
    {
        this.locked = locked;
    }

    public boolean isPatched()
    {
        return patched;
    }

    final void setPatched(boolean patched)
    {
        this.patched = patched;
    }

    final void clearContentPatched()
    {
        this.patched = false;
        for (T8Definition localDefinition : getLocalDefinitions())
        {
            localDefinition.setPatched(false);
        }
    }

    public final T8DefinitionStatus getDefinitionStatus()
    {
        return status;
    }

    final void setDefinitionStatus(T8DefinitionStatus status)
    {
        this.status = status;
    }

    public List<T8DefinitionComment> getComments()
    {
        return new ArrayList<>(comments);
    }

    public void setComments(List<T8DefinitionComment> comments)
    {
        this.comments.clear();
        if (comments != null)
        {
            this.comments.addAll(comments);
        }
    }

    public void addComment(T8DefinitionComment comment)
    {
        this.comments.add(comment);
    }

    public final String getChecksum()
    {
        return checksum;
    }

    public final void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }

    public final Long getUpdatedTime()
    {
        return updatedTime;
    }

    final void setUpdatedTime(Long time)
    {
        this.updatedTime = time;
    }

    public final Long getCreatedTime()
    {
        return createdTime;
    }

    final void setCreatedTime(Long time)
    {
        this.createdTime = time;
    }

    public final Long getFinalizedTime()
    {
        return finalizedTime;
    }

    final void setFinalizedTime(Long time)
    {
        this.finalizedTime = time;
    }

    public final String getUpdatedUserIdentifier()
    {
        return updatedUserIdentifier;
    }

    final void setUpdatedUserIdentifier(String identifier)
    {
        if ((identifier != null) && (identifier.trim().length() == 0)) updatedUserIdentifier = null;
        else this.updatedUserIdentifier = identifier;
    }

    public final String getCreatedUserIdentifier()
    {
        return createdUserIdentifier;
    }

    final void setCreatedUserIdentifier(String identifier)
    {
        if ((identifier != null) && (identifier.trim().length() == 0)) createdUserIdentifier = null;
        else this.createdUserIdentifier = identifier;
    }

    public final String getFinalizedUserIdentifier()
    {
        return finalizedUserIdentifier;
    }

    final void setFinalizedUserIdentifier(String identifier)
    {
        if ((identifier != null) && (identifier.trim().length() == 0)) finalizedUserIdentifier = null;
        else this.finalizedUserIdentifier = identifier;
    }

    public final Long getRevision()
    {
        return revision;
    }

    final void setRevision(long revision)
    {
        this.revision = revision;
    }

    final void incrementRevision()
    {
        if (revision == null) revision = new Long(0);
        else revision++;
    }

    public T8DefinitionLevel getLevel()
    {
        return T8DefinitionUtilities.getDefinitionLevel(this);
    }

    public void setMetaData(String displayName, String description)
    {
        // If any real changes were made to the meta data, update it now with the new values.
        if ((!Objects.equals(this.description, description)) || !Objects.equals(this.displayName, displayName))
        {
            T8DefinitionMetaData oldValue;

            oldValue = this.getMetaData();

            this.description = description;
            this.displayName = displayName;

            fireMetaDataChangeEvent(oldValue, getMetaData(), null);
        }
    }

    public Object getCompleteDefinitionData()
    {
        return getCompleteDefinitionData(definitionData);
    }

    public Object getCompleteDefinitionData(Object definitionDataObject)
    {
        if (definitionDataObject instanceof T8Definition)
        {
            return ((T8Definition)definitionDataObject).getCompleteDefinitionData();
        }
        else if (definitionDataObject instanceof Map)
        {
            Map<String, Object> dataMap;

            dataMap = (Map<String, Object>)definitionDataObject;
            for (String key : dataMap.keySet())
            {
                dataMap.put(key, getCompleteDefinitionData(dataMap.get(key)));
            }

            return dataMap;
        }
        else if (definitionDataObject instanceof List)
        {
            List<Object> dataList;

            dataList = (List<Object>)definitionDataObject;
            for (int index = 0; index < dataList.size(); index++)
            {
                dataList.set(index, getCompleteDefinitionData(dataList.get(index)));
            }

            return dataList;
        }
        else return definitionDataObject;
    }

    /**
     * This method is only used by the definition serializer and is therefore
     * package protected.
     *
     * @return This definition's data collection.
     */
    HashMap<String, Object> getDefinitionData()
    {
        return definitionData;
    }

    /**
     * This method is only used by the definition serializer and is therefore
     * package protected.
     *
     * @param definitionData The definition data to add to this definition.
     */
    void setDefinitionData(Map<String, Object> definitionData)
    {
        for (String datumId : definitionData.keySet())
        {
            T8DefinitionDatumType datumType;

            datumType = getDatumType(datumId);
            if ((datumType != null) && (datumType.isPersisted()))
            {
                Object value;

                value = definitionData.get(datumId);
                setDefinitionDatum(datumId, value, false, null);
            }
        }
    }

    /**
     * This method is only used by the definition serializer and is therefore
     * package protected.
     */
    void setDefinitionMetaData(T8DefinitionMetaData metaData, boolean fireEvent)
    {
        T8DefinitionMetaData existingMetaData;
        T8DefinitionLevel level;

        // Get the existing meta data and elvel.
        existingMetaData = getMetaData();
        level = getLevel();

        this.displayName = metaData.getDisplayName();
        this.description = metaData.getDescription();
        this.versionId = (level == T8DefinitionLevel.PROJECT) ? metaData.getVersionId() : null;
        this.projectId = (level == T8DefinitionLevel.PROJECT) ? metaData.getProjectId() : null;
        this.checksum = metaData.getChecksum();
        this.status = metaData.getStatus();
        this.createdTime = metaData.getCreatedTime();
        this.updatedTime = metaData.getUpdatedTime();
        this.createdUserIdentifier = metaData.getCreatedUserId();
        this.updatedUserIdentifier = metaData.getUpdatedUserId();
        this.revision = metaData.getRevision();
        this.patched = metaData.isPatched();

        // Do some validation.
        if (T8DefinitionUtilities.getDefinitionLevel(this) == T8DefinitionLevel.SYSTEM)
        {
            projectId = null;
        }

        // Add the tags if any were supplied.
        this.comments.clear();
        this.comments.addAll(metaData.getComments());

        // Fire the change event if required.
        if (fireEvent)
        {
            fireMetaDataChangeEvent(existingMetaData, getMetaData(), null);
        }
    }

    public boolean containsDefinitionDatum(String key)
    {
        return definitionData.containsKey(key);
    }

    protected <D> D getDefinitionDatum(Enum<?> key)
    {
        return getDefinitionDatum(key.toString());
    }

    @SuppressWarnings("unchecked")
    public <D, K, V> D getDefinitionDatum(String key)
    {
        D value;

        // Get the datum value.
        value = (D)definitionData.get(key);

        // Now check if the datum value is a collection and if so, make a copy so that we protect the definition content from exterior modification.
        if (value instanceof ArrayList)
        {
            return (D)new ArrayList<>((Collection<V>)value);
        }
        else if (value instanceof LinkedHashMap)
        {
            return (D)new LinkedHashMap<>((Map<K, V>)value);
        }
        else return value;
    }

    public Map<String, Object> getDefinitionDatums()
    {
        return new HashMap<>(definitionData);
    }

    protected void setDefinitionDatum(String datumId, Object data, boolean triggerEvents, Object actor)
    {
        T8DefinitionDatumType datumType;

        datumType = getDatumType(datumId);
        if (datumType != null)
        {
            Object oldValue;

            oldValue = definitionData.get(datumId);
            definitionData.put(datumId, data);
            if (data != null)
            {
                if (datumType.isDefinitionListType())
                {
                    List<T8Definition> definitionList;

                    definitionList = (List<T8Definition>)data;
                    for (T8Definition definition : definitionList)
                    {
                        definition.setParentDefinition(this);
                    }
                }
                else if (datumType.isSingleDefinitionType())
                {
                    ((T8Definition)data).setParentDefinition(this);
                }
            }
            else if (datumType.isListType()) // Make sure that a list is never null.
            {
                definitionData.put(datumId, new ArrayList<>());
            }

            // Fire the change event.
            if (triggerEvents) fireDatumChangeEvent(datumType, oldValue, data, actor);
        }
        else throw new RuntimeException("Cannot set datum '" + datumId + "' on definition '" + identifier + "' because it does not contain the specified datum type.");
    }

    public void setDefinitionDatum(String datumId, Object data, Object actor)
    {
        setDefinitionDatum(datumId, data, true, actor);
    }

    public void setDefinitionDatum(String datumId, Object data)
    {
        setDefinitionDatum(datumId, data, true, null);
    }

    protected void setDefinitionDatum(Enum<?> datumId, Object data)
    {
        setDefinitionDatum(datumId.toString(), data);
    }

    public ArrayList<T8Definition> getSubDefinitions()
    {
        ArrayList<T8Definition> definitionList;

        definitionList = new ArrayList<>();
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            if (datumType.isSubDefinitionType())
            {
                Object definitionObject;

                definitionObject = getDefinitionDatum(datumType.getIdentifier());
                if (definitionObject != null)
                {
                    if (definitionObject instanceof List)
                    {
                        definitionList.addAll((List)definitionObject);
                    }
                    else definitionList.add((T8Definition)definitionObject);
                }
            }
        }

        return definitionList;
    }

    public T8Definition getSubDefinition(String identifier)
    {
        for (T8Definition subDefinition : getSubDefinitions())
        {
            if ((subDefinition.getIdentifier().equals(identifier)) || (subDefinition.getPublicIdentifier().equals(identifier)))
            {
                return subDefinition;
            }
        }

        return null;
    }

    public ArrayList<String> getDescendentDefinitionIdentifiers()
    {
        ArrayList<T8Definition> descendentDefinitions;
        ArrayList<String> identifierList;

        identifierList = new ArrayList<>();
        descendentDefinitions = getDescendentDefinitions();
        for (T8Definition descendentDefinition : descendentDefinitions)
        {
            identifierList.add(descendentDefinition.getIdentifier());
        }

        return identifierList;
    }

    public ArrayList<T8Definition> getDescendentDefinitions()
    {
        ArrayList<T8Definition> definitionList;
        ArrayList<T8Definition> subDefinitionDescendents;

        definitionList = new ArrayList<>();
        definitionList.addAll(getSubDefinitions());

        subDefinitionDescendents = new ArrayList<>();
        for (T8Definition subDefinition : definitionList)
        {
            subDefinitionDescendents.addAll(subDefinition.getDescendentDefinitions());
        }

        definitionList.addAll(subDefinitionDescendents);
        return definitionList;
    }

    public <D extends T8Definition> D getDescendentDefinition(String identifier)
    {
        Stack<T8Definition> definitions;
        T8Definition subDefinition;

        definitions = new Stack<>();
        definitions.addAll(getSubDefinitions());

        while (!definitions.isEmpty())
        {
            subDefinition = definitions.pop();
            if ((subDefinition.getIdentifier().equals(identifier)) || (subDefinition.getPublicIdentifier().equals(identifier)))
            {
                return (D)subDefinition;
            } else definitions.addAll(subDefinition.getSubDefinitions());
        }

        return null;
    }

    public int getSubDefinitionIndex(T8Definition subDefinition)
    {
        T8DefinitionDatumType datumType;

        datumType = getSubDefinitionDatumType(subDefinition);
        if (datumType != null)
        {
            if (datumType.isDefinitionListType())
            {
                List definitionList;

                definitionList = (List)getDefinitionDatum(datumType.getIdentifier());
                if ((definitionList != null) && (definitionList.contains(subDefinition)))
                {
                    return definitionList.indexOf(subDefinition);
                }
                else return -1;
            }
            else if (getDefinitionDatum(datumType.getIdentifier()) == subDefinition)
            {
                return 0;
            }
            else return -1;
        }
        else throw new RuntimeException("Definition datum type not found: " + subDefinition);
    }

    public void setSubDefinitionIndex(T8Definition subDefinition, int index)
    {
        T8DefinitionDatumType datumType;

        datumType = getSubDefinitionDatumType(subDefinition);
        if (datumType != null)
        {
            if (datumType.isDefinitionListType())
            {
                List definitionList;

                definitionList = (List)getDefinitionDatum(datumType.getIdentifier());
                if ((definitionList != null) && (definitionList.contains(subDefinition)))
                {
                    if ((index > -1) && (index < definitionList.size()))
                    {
                        definitionList.remove(subDefinition);
                        definitionList.add(index, subDefinition);
                        setDefinitionDatum(datumType.getIdentifier(), definitionList);
                    }
                    else throw new ArrayIndexOutOfBoundsException(index);
                }
            }
            else throw new IllegalArgumentException("Index not applicable to single definition datum types.");
        }
        else throw new RuntimeException("Definition datum type not found: " + subDefinition);
    }

    public void addSubDefinition(String datumIdentifier, T8Definition subDefinition)
    {
        addSubDefinition(datumIdentifier, subDefinition, -1);
    }

    public void addSubDefinition(String datumIdentifier, T8Definition subDefinition, int index)
    {
        addSubDefinition(datumIdentifier, subDefinition, index, true);
    }

    protected void addSubDefinition(String datumIdentifier, T8Definition subDefinition, int index, boolean triggerEvents)
    {
        T8DefinitionDatumType datumType;

        datumType = getDatumType(datumIdentifier);
        if (datumType != null)
        {
            if (datumType.isDefinitionListType())
            {
                List<T8Definition> definitionList;

                definitionList = (List<T8Definition>)getDefinitionDatum(datumType.getIdentifier());
                if (definitionList == null)
                {
                    definitionList = new ArrayList();
                    setDefinitionDatum(datumType.getIdentifier(), definitionList, triggerEvents, null);
                }
                else definitionList = new ArrayList<>(definitionList); // Create a new List so that we do not alter the old value.

                if ((datumType.getMaximumElements() == -1) || (definitionList.size() < datumType.getMaximumElements()))
                {
                    if (index > -1) definitionList.add(index, subDefinition);
                    else definitionList.add(subDefinition);

                    // Now set the new datum value.
                    setDefinitionDatum(datumType.getIdentifier(), definitionList, triggerEvents, null);
                }
                else throw new RuntimeException("Maximum elements reached.  More data cannot be added.");
            }
            else
            {
                setDefinitionDatum(datumType.getIdentifier(), subDefinition, triggerEvents, null);
            }
        }
        else throw new RuntimeException("Invalid definition datum: " + datumIdentifier + " Definition: " + getIdentifier());
    }

    public boolean removeSubDefinition(T8Definition subDefinition)
    {
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            if (datumType.isSubDefinitionType())
            {
                String datumIdentifier;

                datumIdentifier = datumType.getIdentifier();
                if (datumType.isDefinitionListType())
                {
                    List<T8Definition> definitionList;

                    definitionList = (List)getDefinitionDatum(datumIdentifier);
                    if ((definitionList != null) && (definitionList.contains(subDefinition)))
                    {
                        subDefinition.setParentDefinition(null);
                        definitionList = new ArrayList<>(definitionList); // Create a new list so that we do not alter the old value.
                        definitionList.remove(subDefinition);
                        setDefinitionDatum(datumIdentifier, definitionList);
                        return true;
                    }
                }
                else if (subDefinition == getDefinitionDatum(datumIdentifier))
                {
                    subDefinition.setParentDefinition(null);
                    setDefinitionDatum(datumIdentifier, null);
                    return true;
                }
            }
        }

        return false;
    }

    public T8DefinitionDatumType getDatumType(String datumIdentifier)
    {
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            if (datumType.getIdentifier().equals(datumIdentifier))
            {
                return datumType;
            }
        }

        return null;
    }

    public int getDatumSize(String datumIdentifier)
    {
        T8DefinitionDatumType datumType;

        datumType = getDatumType(datumIdentifier);
        if (datumType != null)
        {
            Object datumObject;

            datumObject = getDefinitionDatum(datumIdentifier);
            if (datumObject == null)
            {
                return 0;
            }
            else if (datumObject instanceof Collection)
            {
                return ((Collection)datumObject).size();
            }
            else return 1;
        }
        else throw new RuntimeException("Datum Type not found: " + datumIdentifier);
    }

    public List<Pair<T8Definition, T8DefinitionDatumType>> getDefinitionPath()
    {
        if (parentDefinition != null)
        {
            List<Pair<T8Definition, T8DefinitionDatumType>> path;
            Pair<T8Definition, T8DefinitionDatumType> lastPathStep;

            path = parentDefinition.getDefinitionPath();
            lastPathStep = path.get(path.size()-1);
            lastPathStep.setValue2(parentDefinition.getSubDefinitionDatumType(this));
            path.add(new Pair<>(this, null));
            return path;
        }
        else
        {
            List<Pair<T8Definition, T8DefinitionDatumType>> path;

            path = new ArrayList<>();
            path.add(new Pair<>(this, null));
            return path;
        }
    }

    /**
     * Returns the datum type in this definition that contains the specified
     * sub-definition.
     * @param subDefinition The sub-definition to search for.
     * @return The datum type containing the specified sub-definition.
     */
    public T8DefinitionDatumType getSubDefinitionDatumType(T8Definition subDefinition)
    {
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            if (datumType.isDefinitionListType())
            {
                List definitionList;

                definitionList = (List)getDefinitionDatum(datumType.getIdentifier());
                if (definitionList != null)
                {
                    if (definitionList.contains(subDefinition)) return datumType;
                }
            }
            else if (getDefinitionDatum(datumType.getIdentifier()) == subDefinition)
            {
                return datumType;
            }
        }

        return null;
    }

    public ArrayList<T8DefinitionDatumType> getSubDefinitionDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        for (T8DefinitionDatumType datumType : getDatumTypes())
        {
            if (datumType.isSubDefinitionType())
            {
                datumTypes.add(datumType);
            }
        }

        return datumTypes;
    }

    protected static ArrayList<T8DefinitionDatumOption> createStringOptions(List<String> stringList)
    {
        ArrayList<T8DefinitionDatumOption> stringOptions;

        stringOptions = new ArrayList<>();
        for (String string : stringList)
        {
            if (string == null) stringOptions.add(new T8DefinitionDatumOption(null, null));
            else stringOptions.add(new T8DefinitionDatumOption(string, string));
        }

        return stringOptions;
    }

    protected static ArrayList<T8DefinitionDatumOption> createStringOptions(Object[] objectArray)
    {
        ArrayList<T8DefinitionDatumOption> stringOptions;

        Arrays.sort(objectArray);
        stringOptions = new ArrayList<>();
        for (Object object : objectArray)
        {
            if (object == null) stringOptions.add(new T8DefinitionDatumOption(null, null));
            else stringOptions.add(new T8DefinitionDatumOption(object.toString(), object.toString()));
        }

        return stringOptions;
    }

    protected static ArrayList<T8DefinitionDatumOption> createIntegerOptions(Integer[] objectArray)
    {
        ArrayList<T8DefinitionDatumOption> integerOptions;

        integerOptions = new ArrayList<>();
        for (Integer intValue : objectArray)
        {
            if (intValue == null) integerOptions.add(new T8DefinitionDatumOption(null, null));
            else integerOptions.add(new T8DefinitionDatumOption(intValue.toString(), intValue));
        }

        return integerOptions;
    }

    protected static ArrayList<T8DefinitionDatumOption> createDefinitionOption(T8DefinitionTypeMetaData definitionMetaData)
    {
        ArrayList<T8DefinitionDatumOption> definitionOptions;
        T8DefinitionConstructorParameters constructorParameters;

        constructorParameters = new T8DefinitionConstructorParameters(null, definitionMetaData.getTypeId(), null);

        definitionOptions = new ArrayList<>();
        definitionOptions.add(new T8DefinitionDatumOption(definitionMetaData.getDisplayName(), constructorParameters));
        return definitionOptions;
    }

    protected static ArrayList<T8DefinitionDatumOption> createDefinitionOptions(List<T8DefinitionTypeMetaData> definitionMetaDataList)
    {
        ArrayList<T8DefinitionDatumOption> definitionOptions;

        definitionOptions = new ArrayList<>();
        for (T8DefinitionTypeMetaData definitionMetaData : definitionMetaDataList)
        {
            T8DefinitionConstructorParameters constructorParameters;

            constructorParameters = new T8DefinitionConstructorParameters(null, definitionMetaData.getTypeId(), null);
            definitionOptions.add(new T8DefinitionDatumOption(definitionMetaData.getDisplayName(), constructorParameters));
        }

        return definitionOptions;
    }

    protected static ArrayList<T8DefinitionDatumOption> createIdentifierOptions(List<T8DefinitionMetaData> definitionMetaDataList)
    {
        return createIdentifierOptions(definitionMetaDataList, true, null);
    }

    protected static ArrayList<T8DefinitionDatumOption> createIdentifierOptions(List<T8DefinitionMetaData> definitionMetaDataList, boolean allowNull, String nullDisplayName)
    {
        ArrayList<T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedIdentifiers;

        orderedIdentifiers = new TreeSet<>();
        for (T8DefinitionMetaData definitionMetaData : definitionMetaDataList)
        {
            orderedIdentifiers.add(definitionMetaData.getId());
        }

        identifierOptions = new ArrayList<>();
        if (allowNull) identifierOptions.add(new T8DefinitionDatumOption(nullDisplayName, null));
        for (String definitionIdentifier : orderedIdentifiers)
        {
            identifierOptions.add(new T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }

    protected static ArrayList<T8DefinitionDatumOption> createStringOptions(List<String> strings, boolean allowNull, String nullDisplayName)
    {
        ArrayList<T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedStrings;

        orderedStrings = new TreeSet<>();
        for (String string : strings)
        {
            orderedStrings.add(string);
        }

        identifierOptions = new ArrayList<>();
        if (allowNull) identifierOptions.add(new T8DefinitionDatumOption(nullDisplayName, null));
        for (String definitionIdentifier : orderedStrings)
        {
            identifierOptions.add(new T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }

    protected static <T extends T8Definition> ArrayList<T8DefinitionDatumOption> createLocalIdentifierOptionsFromDefinitions(List<T> definitions)
    {
        ArrayList<T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedIdentifiers;

        orderedIdentifiers = new TreeSet<>();
        for (T8Definition definition : definitions)
        {
            orderedIdentifiers.add(definition.getIdentifier());
        }

        identifierOptions = new ArrayList<>();
        for (String definitionIdentifier : orderedIdentifiers)
        {
            identifierOptions.add(new T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }

    protected static <T extends T8Definition> ArrayList<T8DefinitionDatumOption> createLocalIdentifierOptionsFromDefinitions(List<T> definitions, boolean allowNull, String nullDisplayName)
    {
        ArrayList<T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedIdentifiers;

        orderedIdentifiers = new TreeSet<>();
        for (T8Definition definition : definitions)
        {
            orderedIdentifiers.add(definition.getIdentifier());
        }

        identifierOptions = new ArrayList<>();
        if (allowNull) identifierOptions.add(new T8DefinitionDatumOption(nullDisplayName, null));
        for (String definitionIdentifier : orderedIdentifiers)
        {
            identifierOptions.add(new T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }

    protected static <T extends T8Definition> ArrayList<T8DefinitionDatumOption> createPublicIdentifierOptionsFromDefinitions(List<T> definitions)
    {
        ArrayList<T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedIdentifiers;

        orderedIdentifiers = new TreeSet<>();
        for (T8Definition definition : definitions)
        {
            orderedIdentifiers.add(definition.getPublicIdentifier());
        }

        identifierOptions = new ArrayList<>();
        for (String definitionIdentifier : orderedIdentifiers)
        {
            identifierOptions.add(new T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }

    protected static <T extends T8Definition> ArrayList<T8DefinitionDatumOption> createPublicIdentifierOptionsFromDefinitions(List<T> definitions, boolean allowNull, String nullDisplayName)
    {
        ArrayList<T8DefinitionDatumOption> identifierOptions;
        TreeSet<String> orderedIdentifiers;

        orderedIdentifiers = new TreeSet<>();
        for (T8Definition definition : definitions)
        {
            orderedIdentifiers.add(definition.getPublicIdentifier());
        }

        identifierOptions = new ArrayList<>();
        if (allowNull) identifierOptions.add(new T8DefinitionDatumOption(nullDisplayName, null));
        for (String definitionIdentifier : orderedIdentifiers)
        {
            identifierOptions.add(new T8DefinitionDatumOption(definitionIdentifier, definitionIdentifier));
        }

        return identifierOptions;
    }

    @Override
    public String toString()
    {
        return "T8Definition{" + "id=" + identifier + ", projectId=" + getRootProjectId() + "}";
    }

    /**
     * Returns a Test Harness that can be used to test this definition.  The
     * default implementation of this method returns null, indicating that no
     * definition-specific test harness is available.
     *
     * @param clientContext The client context from which the testing will be
     * performed.
     * @param sessionContext The session context from which the testing will be
     * performed.
     * @return A test harness object that provides facilities for testing this
     * definition.  Returns null if no test harness is available.
     */
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return null;
    }

    /**
     * This method is used by the framework to validate a definition from a
     * server context.  This will typically be used when a system validity check
     * is performed.
     *
     * @param serverContext The developer context from which this validation
     * is performed.
     * @param sessionContext The session context to use during definition
     * validation.
     * @param validationType The type of validation to perform.  A local
     * validation only checks that the datum values internal to the definition
     * are valid in terms of type and content whereas a global validation also
     * does reference checking.
     * @return The validation errors (if any).  A null value or empty list
     * indicates not errors were found.
     */
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        T8DefinitionValidationHandler validationHandler;

        validationHandler = new T8DefinitionValidationHandler(serverContext, sessionContext);
        return validationHandler.validateDefinition(this, validationType);
    }

    /**
     * This method is used by the T8Developer to retrieve a documentation
     * provider for this definition.  The documentation provider is then queried
     * for information that may be displayed to the user in order to describe
     * the functionality and content of this definition.
     * @param definitionContext Definition context.
     * @return The definition documentation provider to use when querying
     * definition documentation information.
     */
    public T8DefinitionDocumentationProvider createDocumentationProvider(T8DefinitionContext definitionContext)
    {
        return new T8DefaultDefinitionDocumentationProvider(definitionContext);
    }

    /**
     * This method is called whenever this definition is opened for editing by
     * the user.  If this method returns a null value a default definition
     * editor will be constructed when this definition is edited.
     *
     * @param definitionEditorContext The context wherein the editor will be
     * used.
     * @return The definition editor to use for editing of this definition.
     * @throws java.lang.Exception
     */
    public T8DefinitionEditor getDefinitionEditor(T8DefinitionContext definitionEditorContext) throws Exception
    {
        // The default definition behavior is to return no editor.  It is then
        // up to the editor enivronment to create a default editor to be used.
        return null;
    }

    /**
     * This method is called by the default definition editor for each of the
     * definition datum types contained in this definition in order to create an
     * applicable component for editing of the datum value.  If a null value is
     * returned a default datum editor will be constructed for the specified
     * datum type.
     *
     * @param definitionEditorContext The context where the datum editor will be used.
     * @param datumIdentifier The datum for which an editor is requested.
     * @return The component to use for editing of the specified datum.
     */
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionEditorContext, String datumIdentifier)
    {
        return null;
    }

    /**
     * This method is called by the default definition editor when this
     * definition is opened for editing by the user.  The method must return the
     * applicable action handler which can take case of actions to be performed
     * on this definition during development.
     *
     * @param context  The context within which the action handle will be used.
     * @return  The action handler applicable to this definition.
     */
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return null;
    }

    /**
     * This method is always called directly after a definition is loaded from
     * its persisted state.  This method must perform any construction related
     * operations required by the definition.  This method differs from the
     * definition initialization in that it is always performed, not only when
     * the definition is used in a runtime environment.
     *
     * @param serverContext The server context from which this definition is
     * constructed.
     * @throws java.lang.Exception
     */
    public void constructDefinition(T8ServerContext serverContext) throws Exception
    {
        // Default construction does nothing.
    }

    /**
     * This method is called whenever a definition is loaded on the server side.
     *
     * @param context
     * @param inputParameters The input parameters to use when initializing this
     * definition.
     * @param configurationSettings The configuration settings to use when
     * initializing this definition.
     * @throws Exception
     */
    public abstract void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception;

    /**
     * Returns the type of each datum that is stored in this definition's data
     * collection.
     *
     * @return The type of each datum in this definition's data collection.
     */
    public abstract ArrayList<T8DefinitionDatumType> getDatumTypes();

    /**
     * Resolves the allowable values for the specified datum within the
     * specified context.
     *
     * @param definitionContext The context in which this operation is used.
     * @param datumId The datum identifier for which to resolve
     * allowable values.
     * @return A list of possible values for the specified datum within the
     * specified context.
     * @throws java.lang.Exception
     */
    public abstract ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception;
}
