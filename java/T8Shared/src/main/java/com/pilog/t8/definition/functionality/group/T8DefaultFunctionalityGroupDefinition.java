package com.pilog.t8.definition.functionality.group;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.ui.T8Image;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultFunctionalityGroupDefinition extends T8FunctionalityGroupDefinition
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultFunctionalityGroupDefinition.class);

    // -------- Definition Meta-Data -------- //
    public enum Datum
    {
        DISPLAY_NAME_EXPRESSION,
        DESCRIPTION,
        ICON_IDENTIFIER,
        GROUP_OVERVIEW_FUNCTIONALITY_IDENTIFIER,
        FUNCTIONALITY_IDENTIFIERS,
        SUB_GROUP_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;
    private T8FunctionalityDefinition overviewFunctionalityDefinition;
    private Map<String, T8FunctionalityDefinition> functionalityDefinitions;

    public T8DefaultFunctionalityGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DISPLAY_NAME_EXPRESSION.toString(), "Display Name Expresion", "The expression that is evaluated to determine the display name of this functionality group."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DESCRIPTION.toString(), "Description", "The text that describes this functionality and which can be used for display on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this functionality when displayed on the UI."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.GROUP_OVERVIEW_FUNCTIONALITY_IDENTIFIER.toString(), "Overview Functionality", "The functionality that can be invoked to display an overview of the functionality group."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.FUNCTIONALITY_IDENTIFIERS.toString(), "Functionalities", "The list of functionalities and sub-groups belonging to this group (sequence is important)."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SUB_GROUP_DEFINITIONS.toString(), "Sub-Groups", "The list of sub-groups belonging to this group."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.ICON_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else if (Datum.GROUP_OVERVIEW_FUNCTIONALITY_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER), true, "None");
        else if (Datum.FUNCTIONALITY_IDENTIFIERS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.SUB_GROUP_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DefaultFunctionalityGroupDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        List<String> functionalityIds;
        T8DefinitionManager definitionManager;
        List<String> accessibleFunctionalityIds;
        T8ServerContext serverContext;
        String definitionId;
        String iconId;

        // Call the super-initialization.
        serverContext = context.getServerContext();
        super.initializeDefinition(context, inputParameters, configurationSettings);

        // Pre-load the icon definition if one is specified.
        iconId = getIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }

        // Load each of the functionality definitions.
        definitionManager = serverContext.getDefinitionManager();
        functionalityDefinitions = new LinkedHashMap<>();
        accessibleFunctionalityIds = serverContext.getSecurityManager().getAccessibleFunctionalityIds(context);
        functionalityIds = getFunctionalityIdentifiers();
        if (accessibleFunctionalityIds != null && functionalityIds != null)
        {
            for (String functionalityId : functionalityIds)
            {
                if (accessibleFunctionalityIds.contains(functionalityId))
                {
                    T8FunctionalityDefinition functionalityDefinition;

                    functionalityDefinition = (T8FunctionalityDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), functionalityId, inputParameters);
                    if ((functionalityDefinition != null) && (functionalityDefinition.isActive()))
                    {
                        functionalityDefinitions.put(functionalityId, functionalityDefinition);
                    }
                }
            }
        }

        // Load the overview functionality definition.
        definitionId = getGroupOverviewFunctionalityIdentifier();
        if (definitionId != null)
        {
            T8FunctionalityDefinition functionalityDefinition;

            functionalityDefinition = (T8FunctionalityDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
            if (functionalityDefinition.isActive())
            {
                overviewFunctionalityDefinition = functionalityDefinition;
            }
        }
    }

    @Override
    public T8FunctionalityGroupHandle getNewFunctionalityGroupHandle(T8Context context, T8FunctionalityGroupHandle parentGroup) throws Exception
    {
        try
        {
            T8ConfigurationManager configurationManager;
            String displayNameExpression;
            T8FunctionalityGroupHandle handle;

            // Create the new handle.
            handle = new T8FunctionalityGroupHandle(getIdentifier(), parentGroup);
            handle.setDescription(getMetaDescription());
            handle.setIcon(getIcon());

            // Get a configuration manager for translations.
            configurationManager = context.getServerContext().getConfigurationManager();

            // Initialize the display name.
            displayNameExpression = getDisplayNameExpression();
            if (!Strings.isNullOrEmpty(displayNameExpression))
            {
                try
                {
                    T8ExpressionEvaluator expressionEvaluator;

                    expressionEvaluator = context.getExpressionEvaluator();
                    handle.setDisplayName((String)expressionEvaluator.evaluateExpression(displayNameExpression, null, null));
                }
                catch (EPICSyntaxException | EPICRuntimeException ex)
                {
                    LOGGER.log("Failed to evaluate functionality group '" + getIdentifier() + "' display name expression: " + displayNameExpression, ex);
                    handle.setDisplayName(configurationManager.getUITranslation(context, "Error"));
                }
            }
            else
            {
                handle.setDisplayName(getIdentifier());
            }

            // Add all functionalities.
            for (T8FunctionalityDefinition functionalityDefinition : getFunctionalityDefinitions())
            {
                try
                {
                    handle.addFunctionality(functionalityDefinition.getNewFunctionalityHandle(context));
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while initializing functionality: " + functionalityDefinition, e);
                }
            }

            // Add all sub-groups.
            for (T8FunctionalityGroupDefinition functionalityGroupDefinition : getSubGroupDefinitions())
            {
                try
                {
                    T8FunctionalityGroupHandle subGroup;

                    subGroup = functionalityGroupDefinition.getNewFunctionalityGroupHandle(context, handle);
                    handle.addSubGroup(subGroup);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while initializing functionality group: " + functionalityGroupDefinition, e);
                }
            }

            // Add the group overview functionality.
            if (getGroupOverviewFunctionalityDefinition() != null)
            {
                T8FunctionalityDefinition overviewDefinition;

                overviewDefinition = getGroupOverviewFunctionalityDefinition();

                try
                {
                    handle.setOverviewFunctionality(overviewDefinition.getNewFunctionalityHandle(context));
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while initializing group overview functionality: " + overviewDefinition, e);
                }
            }

            // Return the constructed handle.
            return handle;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while initializing functionality group handle: " + getIdentifier(), e);
        }
    }

    public boolean containsFunctionalityDefinitions()
    {
        if ((functionalityDefinitions != null) && (functionalityDefinitions.size() > 0))
        {
            return true;
        }
        else
        {
            for (T8DefaultFunctionalityGroupDefinition subGroupDefinition : getSubGroupDefinitions())
            {
                if (subGroupDefinition.containsFunctionalityDefinitions())
                {
                    return true;
                }
            }

            return false;
        }
    }

    public List<T8FunctionalityDefinition> getFunctionalityDefinitions()
    {
        List<T8FunctionalityDefinition> definitionList;

        definitionList = new ArrayList<>();
        if (functionalityDefinitions != null) definitionList.addAll(functionalityDefinitions.values());
        return definitionList;
    }

    public T8FunctionalityDefinition getGroupOverviewFunctionalityDefinition()
    {
        return overviewFunctionalityDefinition;
    }

    public String getDisplayNameExpression()
    {
        return getDefinitionDatum(Datum.DISPLAY_NAME_EXPRESSION);
    }

    public void setDisplayNameExpression(String text)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME_EXPRESSION, text);
    }

    public String getGroupDescription()
    {
        return getDefinitionDatum(Datum.DESCRIPTION);
    }

    public void setGroupDescription(String text)
    {
        setDefinitionDatum(Datum.DESCRIPTION, text);
    }

    public String getIconIdentifier()
    {
        return getDefinitionDatum(Datum.ICON_IDENTIFIER);
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER, identifier);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }

    public ImageIcon getIcon()
    {
        if (iconDefinition != null)
        {
            T8Image iconImage;

            iconImage = iconDefinition.getImage();
            return iconImage != null ? iconImage.getImageIcon() : null;
        }
        else return null;
    }

    public String getGroupOverviewFunctionalityIdentifier()
    {
        return getDefinitionDatum(Datum.GROUP_OVERVIEW_FUNCTIONALITY_IDENTIFIER);
    }

    public void setGroupOverviewFunctionalityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.GROUP_OVERVIEW_FUNCTIONALITY_IDENTIFIER, identifier);
    }

    public List<String> getFunctionalityIdentifiers()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIERS);
    }

    public void setFunctionalityIdentifier(List<String> identifiers)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIERS, identifiers);
    }

    public List<T8DefaultFunctionalityGroupDefinition> getSubGroupDefinitions()
    {
        return getDefinitionDatum(Datum.SUB_GROUP_DEFINITIONS);
    }

    public void setSubGroupDefinitions(List<T8DefaultFunctionalityGroupDefinition> groupDefinitions)
    {
        setDefinitionDatum(Datum.SUB_GROUP_DEFINITIONS, groupDefinitions);
    }
}
