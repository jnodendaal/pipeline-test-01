package com.pilog.t8.definition.help;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.help.T8HelpDisplay;
import java.util.ArrayList;

/**
 * @author Gavin Boshoff
 */
public abstract class T8HelpDisplayDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_HELP";
    public static final String STORAGE_PATH = "/help";
    public static final String IDENTIFIER_PREFIX = "HELP_";
    // -------- Definition Meta-Data -------- //

    public T8HelpDisplayDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public abstract T8HelpDisplay getHelpDisplayInstance();
}