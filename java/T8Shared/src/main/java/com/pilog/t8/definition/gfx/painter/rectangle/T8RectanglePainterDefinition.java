package com.pilog.t8.definition.gfx.painter.rectangle;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.T8AbstractAreaPainterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Bouwer.duPreez
 */
public class T8RectanglePainterDefinition extends T8AbstractAreaPainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_RECTANGLE";
    public static final String DISPLAY_NAME = "Rectangle Painter";
    public static final String DESCRIPTION = "A painter that paints square and rounded rectangles.";
    public enum Datum {WIDTH,
                       HEIGHT,
                       ROUND_WIDTH, 
                       ROUND_HEIGHT};
    // -------- Definition Meta-Data -------- //

    public T8RectanglePainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.WIDTH.toString(), "Width", "The width of the rectangle.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HEIGHT.toString(), "Height", "The height of the rectangle.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROUND_WIDTH.toString(), "Round Width", "The width of the retangle's rounded corners.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROUND_HEIGHT.toString(), "Round Height", "The height of the retangle's round corners.", 0));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Painter getNewPainterInstance()
    {
        try
        {
            T8Painter painter;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.gfx.painter.rectangle.T8RectanglePainter").getConstructor(T8RectanglePainterDefinition.class);
            painter = (T8Painter)constructor.newInstance(this);
            return painter;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public int getWidth()
    {
        return (Integer)getDefinitionDatum(Datum.WIDTH.toString());
    }
    
    public void setWidth(Integer top)
    {
        setDefinitionDatum(Datum.WIDTH.toString(), top);
    }
    
    public int getHeight()
    {
        return (Integer)getDefinitionDatum(Datum.HEIGHT.toString());
    }
    
    public void setHeight(Integer left)
    {
        setDefinitionDatum(Datum.HEIGHT.toString(), left);
    }
    
    public int getRoundWidth()
    {
        return (Integer)getDefinitionDatum(Datum.ROUND_WIDTH.toString());
    }
    
    public void setRoundWidth(Integer roundWidth)
    {
        setDefinitionDatum(Datum.ROUND_WIDTH.toString(), roundWidth);
    }
    
    public int getRoundHeight()
    {
        return (Integer)getDefinitionDatum(Datum.ROUND_HEIGHT.toString());
    }
    
    public void setRoundHeight(Integer roundHeight)
    {
        setDefinitionDatum(Datum.ROUND_HEIGHT.toString(), roundHeight);
    }
}
