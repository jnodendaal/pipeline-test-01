package com.pilog.t8.data.database;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import java.io.Serializable;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DatabaseAdaptor extends Serializable
{
    /**
     * Returns the T8DataType that matches the specified SQL Column type and
     * width.
     * @param sqlType
     * @param width
     * @return
     */
    public T8DataType getDataType(int sqlType, int width);

    /**
     * This method will be used when a tag with a specific identifier needs to
     * be replaced with a new tag value irrespective of the existing tag's
     * value.  A method implementing this method must return a valid SQL string
     * that can be used in an UPDATE statement as part of the SET clause. A
     * valid example of a String returned by this method would be:
     *
     *      TAGS = REGEXP_REPLACE(TAGS, ' \[TEST_TAG_1:[^].]+\]', '')
     *
     * @param tagIdentifier The identifier of the tag that must be replaced.
     * @return The SQL replacement string that will replace all tags of the
     * specified identifier with a new tag (prepared statement placeholder ?).
     */
    public String getSQLTagReplaceByIdentifier(String tagIdentifier, String replacementTag);
    public String getSequenceNextValueQuery(T8DataConnection dataConnection, String sequenceName);
    public String getSQLStringConcatenationOperator();

    /**
     * Returns a String that can be used in a SQL WHERE clause to match the
     * contents of the specified column with the specified regular expression.
     * @param columnName The name of the colum on which the regular expression
     * will be applied.
     * @param regularExpression The regular expression to apply.
     * @return A String that can be appended to a SQL WHERE clause to match the
     * specified regular expression on the supplied column.
     */
    public String getRegularExpressionLike(String columnName, String regularExpression);

    /**
     * Checks the supplied input string and returns a boolean true if the input string contains
     * special characters that require an escape mechanism.
     * @param inputString The input string to check.
     * @return boolean true if the input string contains special characters to be escaped.
     */
    public boolean requiresSqlLikeEscape(String inputString);

    /**
     * Escapes all special characters in the supplied input String using the
     * specified escape character so that the resultant String can be used as
     * a literal operand in a LIKE comparison.
     * @param inputString The input String to which the escape characters will
     * be added.
     * @param escapeCharacter The escape character to use.
     * @return The input String with all special characters escaped.
     */
    public String getSQLEscapedLikeOperand(String inputString, char escapeCharacter);

    /**
     * Converts the specified input hex String (16 bytes) to the equivalent
     * String representation of the GUID.
     * @param inputString The input hex String representing a GUID.
     * @return A String representation of the input GUID that can be used in an
     * SQL statement.
     */
    public String convertHexStringToSQLGUIDString(String inputString);

    /**
     * Return the SQL predicate expression to be used in a WHERE clause where
     * a full-text evaluation on a column is specified.
     * @param columnName The name of the column to which the full-text predicate
     * will be applied.
     * @return The SQL predicate expression.
     */
    public String getSQLParameterizedColumnContains(String columnName);

    /**
     * Converts the specified column used in an SQL query from a hex String
     * (16 bytes) to the equivalent String representation of the GUID.
     * @param columnIdentifier The identifier of the column to convert, as used
     * in the SQL query where the String returned by this method will be
     * used.
     * @return The String representation of a valid SQL function to convert the
     * specified column to a valid GUID.
     */
    public String getSQLColumnHexToGUID(String columnIdentifier);
    public String getSQLParameterizedHexToGUID();
    public String getSQLParameterizedMillisecondsToTimestamp();
    public String getSQLColumnMillisecondsToTimestamp(String columnName);
    public String getSQLColumnTimestampToMilliseconds(String columnName);
    public String getSQLColumnTimestampAddMilliseconds(String timestampColumnName, String millisecondExpression);

    /**
     * Creates and returns an expression that converts the supplied Globally
     * Unique Identifier hex String (16 bytes) to a format that can be used in
     * an SQL statement.
     * @param guid The ID to be converted.
     * @return An expression converting the supplied GUID String to the
     * appropriate type for use in an SQL statement.
     */
    public String getSQLHexToGUID(String guid);
    public String getSQLCreateNewGUID();

    /**
     * Retrieves the database specific query to retrieve all of the database table
     * names. The query is specified as to return only the table names with the
     * {@code ResultSet} column header {@code TABLE_NAME}.<br>
     * <br>
     * <b>Note:</b> The query returned is in a format which allows for a where
     * clause to be appended to it by the calling method.
     *
     * @return The {@code String} for the query to retrieve the database table
     *      names only, with the column header {@code TABLE_NAME}
     */
    public String getTableSelectionQuery();
    public String getTableCreationStatement(T8TableDataSourceDefinition dataSourceDefinition);
    public String getAddTableColumnStatement(String tableName, T8DataFieldDefinition dataFieldDefinition);
    public boolean addColumnToPrimaryKey(String tableName, String columnName, T8DataConnection dataConnection);
    public void logPrimaryKeyDifferences(String tableName, List<T8DataSourceFieldDefinition> dataFieldDefinitions, T8DataConnection dataConnection);

    /**
     * Returns the Meta/DML escape character which is to be used to escape the
     * start of column and table names. This method would always return a
     * constant {@code char} but enforces the availability of it within each
     * adaptor.<br/>
     * <br/>
     * Some databases use an escape character set, such as {@literal []}, while
     * others use a single character, such as {@literal "}
     *
     * @return The Meta/DML start escape {@code char}
     */
    char getMetaEscapeCharacterStart();

    /**
     * Returns the Meta/DML escape character which is to be used to escape the
     * end of column and table names. This method would always return a constant
     * {@code char} but enforces the availability of it within each adaptor.<br/>
     * <br/>
     * Some databases use an escape character set, such as {@literal []}, while
     * others use a single character, such as {@literal "}
     *
     * @return The Meta/DML end escape {@code char}
     */
    char getMetaEscapeCharacterEnd();

    /**
     * Escapes the specified {@code Meta String} using the start and end escape
     * characters provided by {@link #getMetaEscapeCharacterStart()} and
     * {@link #getMetaEscapeCharacterEnd()}.
     *
     * @param metaString The {@code Meta String} value to be escaped
     *
     * @return The escaped {@code Meta String} value
     */
    String escapeMetaString(String metaString);

    /**
     * Gets the Database specific function used to retrieve the current date and
     * time value as defined by the running database instance.<br/>
     * <br/>
     * <b>Note:</b> The value returned by the function could be inconsistent to
     * that of the application server if the database instance and the
     * application server are running on different servers which do not manage
     * the system date and time values consistently.
     *
     * @return The database specific {@code String} function to return a current
     *      date time value
     */
    String getSqlCurrentDateTimeFunction();
}
