package com.pilog.t8.ui;

import java.awt.Graphics2D;
import java.awt.Shape;
import org.jdesktop.swingx.painter.effects.AreaEffect;

/**
 * This class is a simple adapter that allows a T8PainterEffect to be used in 
 * all instances where a regular Painter object is required.  Having this 
 * adapter also reduces the dependency on the SwingX specific interface Painter 
 * within the T8Shared project.
 * 
 * @author Bouwer du Preez
 */
public class T8PainterEffectAdapter implements AreaEffect
{
    private T8PainterEffect painterEffect;
    
    public T8PainterEffectAdapter(T8PainterEffect painterEffect)
    {
        this.painterEffect = painterEffect;
    }

    @Override
    public void apply(Graphics2D graphicsContext, Shape clipShape, int width, int height)
    {
        painterEffect.apply(graphicsContext, clipShape, width, height);
    }
}
