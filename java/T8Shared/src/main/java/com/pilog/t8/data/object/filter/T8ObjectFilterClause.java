package com.pilog.t8.data.object.filter;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface T8ObjectFilterClause
{
    public enum ObjectFilterConjunction
    {
        AND,
        OR;

        public DataFilterConjunction getDataFilterConjunction()
        {
            return this == ObjectFilterConjunction.AND ? DataFilterConjunction.AND : DataFilterConjunction.OR;
        }
    };

    /**
     * Returns true if this clause contains active criteria that will affect the
     * filter (i.e. is not empty).
     * @return true if this clause contains active criteria that will affect the
     * filter (i.e. is not empty).
     */
    public boolean hasCriteria();
    public T8ObjectFilterClause copy();
    public T8ObjectFilterCriterion getFilterCriterion(String id);
    public List<T8ObjectFilterCriterion> getFilterCriterionList();
    public Set<String> getFilterFieldIds();
    public T8DataFilterClause getDataFilterClause(String entityId, Map<String, String> objectToEntityFieldMap);
}
