package com.pilog.t8.data.entity;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import java.util.Comparator;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataEntityComparator implements Comparator
{
    private final String[] fieldIdentifiers;
    private final OrderMethod[] orderMethods;

    public T8DefaultDataEntityComparator(Map<String, OrderMethod> fieldOrdering)
    {
        int index;

        this.fieldIdentifiers = new String[fieldOrdering.size()];
        this.orderMethods = new OrderMethod[fieldOrdering.size()];

        index = 0;
        for (String fieldIdentifier : fieldOrdering.keySet())
        {
            fieldIdentifiers[index] = fieldIdentifier;
            orderMethods[index] = fieldOrdering.get(fieldIdentifier);
            index++;
        }
    }

    @Override
    public int compare(Object o1, Object o2)
    {
        T8DataEntity entity1;
        T8DataEntity entity2;

        // Get the entities to compare.
        entity1 = (T8DataEntity)o1;
        entity2 = (T8DataEntity)o2;

        // Evaluate each of the field values to be compared in sequence.
        for (int orderSequence = 0; orderSequence < fieldIdentifiers.length; orderSequence++)
        {
            String fieldIdentifier;
            OrderMethod orderMethod;
            Comparable value1;
            Comparable value2;

            // Get the identifier of the field to compare and the order method to use.
            fieldIdentifier = fieldIdentifiers[orderSequence];
            orderMethod = orderMethods[orderSequence];

            // Get the two field values to compare.
            value1 = (Comparable)entity1.getFieldValue(fieldIdentifier);
            value2 = (Comparable)entity2.getFieldValue(fieldIdentifier);

            // Compare the field value depending on the method to use.
            if (orderMethod == OrderMethod.ASCENDING)
            {
                // If value 1 is null and value 2 is not, it means that value 1 is less than value 2 (ascending).
                if (value1 == null)
                {
                    if (value2 != null) return -1;
                }
                else return value1.compareTo(value2); // Value 1 is not null, so just do a normal compare with value 2 (ascending).
            }
            else
            {
                // If value 1 is null and value 2 is not, it means that value 1 is greater than value 2 (descending).
                if (value1 == null)
                {
                    if (value2 != null) return 1;
                }
                else return value1.compareTo(value2) * -1; // Value 1 is not null, so just do a normal compare with value 2 (descending).
            }
        }

        // If we reached this point, it means all of the comparisons returned a 0 value.
        return 0;
    }
}
