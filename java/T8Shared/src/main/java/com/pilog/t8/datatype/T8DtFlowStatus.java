package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.task.T8TaskDetails;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DtFlowStatus extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FLOW_STATUS";

    public T8DtFlowStatus() {}

    public T8DtFlowStatus(T8DefinitionManager context) {}

    public T8DtFlowStatus(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FlowStatus.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtTaskDetails dtTaskDetails;
            JsonArray jsonTaskArray;
            T8FlowStatus flowStatus;
            JsonObject jsonFlowStatus;

            // Create the JSON object.
            flowStatus = (T8FlowStatus)object;

            jsonFlowStatus = new JsonObject();
            jsonFlowStatus.add("flowIid", flowStatus.getFlowIid());
            jsonFlowStatus.add("flowId", flowStatus.getFlowId());
            jsonFlowStatus.add("initiatorUserDisplayName", flowStatus.getInitiatorUserDisplayName());

            // Serialize the available tasks.
            jsonTaskArray = new JsonArray();
            jsonFlowStatus.add("availableTasks", jsonTaskArray);
            dtTaskDetails = new T8DtTaskDetails();
            for (T8TaskDetails taskDetails : flowStatus.getAvailableTasks())
            {
                jsonTaskArray.add(dtTaskDetails.serialize(taskDetails));
            }

            // Return the final JSON object.
            return jsonFlowStatus;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8FlowStatus deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8FlowStatus flowStatus;
            JsonObject jsonFlowStatus;
            JsonArray jsonTaskArray;

            // Create the java object.
            jsonFlowStatus = jsonValue.asObject();
            flowStatus = new T8FlowStatus();
            flowStatus.setFlowIid(jsonFlowStatus.getString("flowIid"));
            flowStatus.setFlowId(jsonFlowStatus.getString("flowId"));
            flowStatus.setInitiatorUserDisplayName(jsonFlowStatus.getString("initiatorUserDisplayName"));

            // Deserialize tasks.
            jsonTaskArray = jsonFlowStatus.getJsonArray("availableTasks");
            if (jsonTaskArray != null)
            {
                List<T8TaskDetails> availableTasks;
                T8DtTaskDetails dtTaskDetails;

                dtTaskDetails = new T8DtTaskDetails();
                availableTasks = new ArrayList<>();
                for (JsonObject jsonTask : jsonTaskArray.objects())
                {
                    availableTasks.add(dtTaskDetails.deserialize(jsonTask));
                }
            }

            // Return the completed object.
            return flowStatus;
        }
        else return null;
    }
}