package com.pilog.t8.communication.event;

import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public abstract class T8CommunicationEvent extends EventObject
{
    public T8CommunicationEvent(Object source)
    {
        super(source);
    }
}
