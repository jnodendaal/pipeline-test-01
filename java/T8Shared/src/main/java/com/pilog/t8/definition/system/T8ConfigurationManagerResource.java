package com.pilog.t8.definition.system;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ConfigurationManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GET_UI_TRANSLATION_MAP = "@OS_GET_UI_TRANSLATION_MAP";
    public static final String OPERATION_REQUEST_UI_TRANSLATIONS = "@OS_REQUEST_UI_TRANSLATIONS";
    public static final String OPERATION_GET_API_CLASS_MAP = "@OS_GET_API_CLASS_MAP";
    public static final String OPERATION_GET_LOOK_AND_FEEL_DEFINITION = "@OS_GET_LOOK_AND_FEEL_DEFINITION";

    public static final String PARAMETER_SUCCESS = "$SUCCESS";
    public static final String PARAMETER_UI_TRANSLATION_MAP = "$P_UI_TRANSLATION_MAP";
    public static final String PARAMETER_UI_UNTRANSLATED_PHRASE_SET = "$P_UI_UNTRANSLATED_PHRASE_SET";
    public static final String PARAMETER_LANGUAGE_IDENTIFIER = "$P_LANGUAGE_IDENTIFIER";
    public static final String PARAMETER_API_CLASS_MAP = "$P_API_CLASS_MAP";
    public static final String PARAMETER_LOOK_AND_FEEL_DEFINITION = "$P_LOOK_AND_FEEL_DEFINITION";

    public static final String PROPERTY_DEFAULT_LANGUAGE_IDENTIFIER = "@SYS_PROP_DEFAULT_LANGUAGE_IDENTIFIER";
    public static final String PROPERTY_DEFAULT_ORG_DATA_MODEL_IDENTIFIER = "@SYS_PROP_DEFAULT_ORG_DATA_MODEL_IDENTIFIER";
    public static final String PROPERTY_MAXIMUM_GLOBAL_FLOW_COUNT = "@SYS_PROP_MAXIMUM_GLOBAL_FLOW_COUNT";
    public static final String PROPERTY_MAXIMUM_CONNECTION_POOL_SIZE = "@SYS_PROP_MAXIMUM_CONNECTION_POOL_SIZE";
    public static final String PROPERTY_SERVER_URL = "@SYS_PROP_SERVER_URL";
    public static final String PROPERTY_TEMP_PASSW_VALID_HOURS = "@SYS_PROP_TEMP_PASSW_VALID_HOURS";

    public static final String USR_PROP_VALIDATE_ON_SAVE = "@USR_PROP_VALIDATE_ON_SAVE";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            // UI Translation Operations.
            definition = new T8JavaServerOperationDefinition(OPERATION_GET_UI_TRANSLATION_MAP);
            definition.setMetaDescription("Get UI Translation Map");
            definition.setClassName("com.pilog.t8.config.T8ConfigurationManagerOperations$GetUITranslationMapOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_REQUEST_UI_TRANSLATIONS);
            definition.setMetaDescription("Request UI Translations");
            definition.setClassName("com.pilog.t8.config.T8ConfigurationManagerOperations$RequestUITranslationsOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_API_CLASS_MAP);
            definition.setMetaDescription("Get API Class Map");
            definition.setClassName("com.pilog.t8.config.T8ConfigurationManagerOperations$GetAPIClassMapOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_API_CLASS_MAP, "API Class Map", "The map of API classes used by the client-side system.", T8DataType.DEFINITION_IDENTIFIER_KEY_MAP));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_LOOK_AND_FEEL_DEFINITION);
            definition.setMetaDescription("Get Look and Feel Definition");
            definition.setClassName("com.pilog.t8.config.T8ConfigurationManagerOperations$GetLookAndFeelDefinitionOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LOOK_AND_FEEL_DEFINITION, "Look and Feel Definition", "The Look and Feel Definition specified for this system context.", T8DataType.DEFINITION));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
