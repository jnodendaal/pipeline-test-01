package com.pilog.t8.data.type;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.util.EnumSet;
import java.util.Map;

/**
 * An abstract class that provides functionality common to all data types.
 *
 * @author Bouwer du Preez
 */
public abstract class T8AbstractDataType implements T8DataType
{
    protected Map<String, String> parseAttributes(String stringRepresentation)
    {
        int openIndex;

        openIndex = stringRepresentation.indexOf('(');
        if (openIndex > -1)
        {
            int closeIndex;

            closeIndex = stringRepresentation.indexOf(')');
            if (closeIndex > 1)
            {
                String attributeString;

                attributeString = stringRepresentation.substring(openIndex + 1, closeIndex);
                return StringUtilities.buildParameterMap(attributeString, ",");
            }
            else throw new RuntimeException("Invalid Data Type String. Missing ')': " + stringRepresentation);
        }
        else return null;
    }

    @Override
    public boolean isType(T8DataType type)
    {
        if (type != null)
        {
            return getDataTypeIdentifier().equals(type.getDataTypeIdentifier());
        }
        return false;
    }

    @Override
    public boolean isType(String typeIdentifier)
    {
        if (typeIdentifier != null)
        {
            return getDataTypeIdentifier().equals(typeIdentifier);
        }
        return false;
    }

    /**
     * Data Type attributes are not taken into account when equality is
     * determined nor when a hash code is calculated.
     * @return
     */
    @Override
    public int hashCode()
    {
        return getDataTypeStringRepresentation(false).hashCode();
    }

    /**
     * Data Type attributes are not taken into account when equality is
     * determined.
     * @param object The object to test for equality to this object.
     * @return A boolean flag indicating equality if true.
     */
    @Override
    public boolean equals(Object object)
    {
        if (object instanceof T8DataType)
        {
            return getDataTypeStringRepresentation(false).equals(((T8DataType)object).getDataTypeStringRepresentation(false));
        }
        else return false;
    }

    @Override
    public EnumSet<DataFilterOperator> getApplicableOperators()
    {
        return T8DataType.getDefaultOperatorSet();
    }

    @Override
    public String toString()
    {
        return getDataTypeStringRepresentation(true);
    }
}
