package com.pilog.t8.flow.task;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskDetails implements Serializable, Comparable<T8TaskDetails>
{
    @SuppressWarnings("PublicInnerClass")
    public enum PriorityLevel
    {
        VERY_LOW(0),
        LOW(200),
        NORMAL(400),
        HIGH(600),
        VERY_HIGH(800);

        private final int priority;

        PriorityLevel(int priority)
        {
            this.priority = priority;
        }

        public int getPriority()
        {
            return this.priority;
        }

        public static PriorityLevel getPriorityLevel(T8TaskDetails taskDetails)
        {
            return getPriorityLevel(taskDetails.getPriority());
        }

        public static PriorityLevel getPriorityLevel(int priority)
        {
            PriorityLevel[] levels;

            levels = PriorityLevel.values();
            // Reverse loop cannot be replaced with for-each
            for (int i = levels.length-1; i >= 0; i--)
            {
                if (priority >= levels[i].getPriority()) return levels[i];
            }

            throw new IllegalStateException("Priority does not fall within the valid range.");
        }
    }

    private String projectId;
    private String flowId;
    private String flowIid;
    private String taskId;
    private String taskIid;
    private String taskDisplayName;
    private String taskDescription;
    private String restrictionUserId;
    private String restrictionProfileId;
    private String functionalityId;
    private boolean subsequentFollowUp;
    private boolean claimed;
    private String claimedByUserId;
    private Long timeIssued;
    private Long timeClaimed;
    private Long timeEscalated;
    private int priority;
    private Map<String, Object> flowParameters;
    private Map<String, Object> taskInputParameters;

    public T8TaskDetails(String taskInstanceIdentifier)
    {
        this.taskIid = taskInstanceIdentifier;
        this.subsequentFollowUp = false;
        this.timeClaimed = -1l;
        this.timeIssued = -1l;
        this.timeEscalated = -1l;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public void setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
    }

    public String getFunctionalityId()
    {
        return functionalityId;
    }

    public void setFunctionalityId(String functionalityId)
    {
        this.functionalityId = functionalityId;
    }

    public boolean isClaimed()
    {
        return claimed;
    }

    public void setClaimed(boolean claimed)
    {
        this.claimed = claimed;
    }

    public boolean isSubsequentFollowUp()
    {
        return subsequentFollowUp;
    }

    public void setSubsequentFollowUp(boolean subsequentFollowUp)
    {
        this.subsequentFollowUp = subsequentFollowUp;
    }

    public String getClaimedByUserIdentifier()
    {
        return claimedByUserId;
    }

    public void setClaimedByUserId(String claimedByUserIdentifier)
    {
        this.claimedByUserId = claimedByUserIdentifier;
    }

    public String getFlowId()
    {
        return flowId;
    }

    public void setFlowId(String flowIdentifier)
    {
        this.flowId = flowIdentifier;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String flowIid)
    {
        this.flowIid = flowIid;
    }

    public String getRestrictionUserId()
    {
        return restrictionUserId;
    }

    public void setRestrictionUserId(String restrictionUserId)
    {
        this.restrictionUserId = restrictionUserId;
    }

    public String getRestrictionProfileId()
    {
        return restrictionProfileId;
    }

    public void setRestrictionProfileId(String restrictionProfileId)
    {
        this.restrictionProfileId = restrictionProfileId;
    }

    public String getTaskDescription()
    {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription)
    {
        this.taskDescription = taskDescription;
    }

    public String getTaskDisplayName()
    {
        return taskDisplayName;
    }

    public void setTaskDisplayName(String taskDisplayName)
    {
        this.taskDisplayName = taskDisplayName;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public void setTaskId(String taskId)
    {
        this.taskId = taskId;
    }

    public Map<String, Object> getFlowParameters()
    {
        return flowParameters;
    }

    public void setFlowParameters(Map<String, Object> parameters)
    {
        this.flowParameters = parameters;
    }

    public Map<String, Object> getTaskInputParameters()
    {
        return taskInputParameters;
    }

    public void setTaskInputParameters(Map<String, Object> taskInputParameters)
    {
        this.taskInputParameters = taskInputParameters;
    }

    public Long getTimeClaimed()
    {
        return timeClaimed;
    }

    public void setTimeClaimed(Long timeClaimed)
    {
        this.timeClaimed = timeClaimed;
    }

    public Long getTimeEscalated()
    {
        return timeEscalated;
    }

    public void setTimeEscalated(Long timeEscalated)
    {
        this.timeEscalated = timeEscalated;
    }

    public Long getTimeIssued()
    {
        return timeIssued;
    }

    public void setTimeIssued(Long timeIssued)
    {
        this.timeIssued = timeIssued;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        if (priority < 0 || priority > 1000) throw new IllegalArgumentException("Priority range has to be >= 0 and <= 1000. Priority: " + priority);
        this.priority = priority;
    }

    @Override
    public String toString()
    {
        StringBuffer stringBuffer;

        stringBuffer = new StringBuffer();
        stringBuffer.append("T8TaskDetails{");
        stringBuffer.append("taskIdentifier=");
        stringBuffer.append(taskId);
        stringBuffer.append(", taskIid=");
        stringBuffer.append(taskIid);
        stringBuffer.append(", flowId=");
        stringBuffer.append(flowId);
        stringBuffer.append(", flowIid=");
        stringBuffer.append(flowIid);
        stringBuffer.append("}");

        return stringBuffer.toString();
    }

    @Override
    public int compareTo(T8TaskDetails o)
    {
        if(o == null) return -1;
        if(o == this) return 0;

        // The priority determines which is greater
        return Integer.compare(o.getPriority(), getPriority());
    }
}
