package com.pilog.t8.definition.data.key;

import com.pilog.t8.data.T8DataKeyGenerator;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8SequentialDataKeyGeneratorDefinition extends T8DataKeyGeneratorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_KEY_GENERATOR_SEQUENTIAL";
    public static final String DISPLAY_NAME = "Sequential Data Key Generator";
    public static final String DESCRIPTION = "A Configurable Sequential Data Key Generator";
    public enum Datum
    {
        PREFIX,
        SUFFIX,
        LENGTH,
        PADDING_CHAR,
        PADDING_DIRECTION,
        FIRST_VALUE,
        LAST_VALUE,
        FETCH_SIZE
    };
    // -------- Definition Meta-Data -------- //

    public enum PaddingDirection
    {
        BEFORE,
        AFTER
    }

    public T8SequentialDataKeyGeneratorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PREFIX.toString(), "Prefix", "The prefix that will be prepended to the generated key."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SUFFIX.toString(), "Suffix", "The suffix that will be appended to the generated key."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.LENGTH.toString(), "Length", "The prefered length of the key, the key will be padded with the padding character to the desired length. Use -1 if padding must be disabled.", -1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PADDING_CHAR.toString(), "Padding Character", "The padding character that will be used when padding the key to the desired length.", "0"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PADDING_DIRECTION.toString(), "Padding Direction", "Wether the padding must be done before or after the generated key.", "BEFORE", T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.FIRST_VALUE.toString(), "First Value", "The initial value that will be used when generation starts.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.LAST_VALUE.toString(), "Last Value", "The last value that will be generated. After this value is reached the system will throw an exception on the next request. This functionality can be disabled by setting the value to -1.", -1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.FETCH_SIZE.toString(), "Pre-Fetch Size", "To improve system performance keys can be pre-fetched in batches. This indicates the size of the batches that will be prefetched, when the system recieved a new batch and an unexpected shutdown occurs those keys will be lost.", 50));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PADDING_DIRECTION.toString().equals(datumIdentifier))
        {
            return createStringOptions(PaddingDirection.values());
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataKeyGenerator createNewDataKeyGenerator(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.key.T8SequentialDataKeyGenerator").getConstructor(T8Context.class, T8SequentialDataKeyGeneratorDefinition.class);
        return (T8DataKeyGenerator)constructor.newInstance(context, this);
    }

    public String getPrefix()
    {
        return (String)getDefinitionDatum(Datum.PREFIX.toString());
    }

    public void setPrefix(String prefix)
    {
        setDefinitionDatum(Datum.PREFIX.toString(), prefix);
    }

    public String getSuffix()
    {
        return (String)getDefinitionDatum(Datum.SUFFIX.toString());
    }

    public void setSuffix(String suffix)
    {
        setDefinitionDatum(Datum.SUFFIX.toString(), suffix);
    }

    public Integer getLength()
    {
        return (Integer)getDefinitionDatum(Datum.LENGTH.toString());
    }

    public void setLength(Integer length)
    {
        setDefinitionDatum(Datum.LENGTH.toString(), length);
    }

    public String getPaddingCharacter()
    {
        return (String)getDefinitionDatum(Datum.PADDING_CHAR.toString());
    }

    public void setPaddingCharacter(String paddingCharacter)
    {
        setDefinitionDatum(Datum.PADDING_CHAR.toString(), paddingCharacter);
    }

    public PaddingDirection getPaddingDirection()
    {
        return PaddingDirection.valueOf((String)getDefinitionDatum(Datum.PADDING_DIRECTION.toString()));
    }

    public void setPaddingDirection(PaddingDirection paddingDirection)
    {
        setDefinitionDatum(Datum.PADDING_DIRECTION.toString(), paddingDirection.toString());
    }

    public Integer getFirstValue()
    {
        return (Integer)getDefinitionDatum(Datum.FIRST_VALUE.toString());
    }

    public void setFirstValue(Integer firstValue)
    {
        setDefinitionDatum(Datum.FIRST_VALUE.toString(), firstValue);
    }

    public Integer getLastValue()
    {
        return (Integer)getDefinitionDatum(Datum.LAST_VALUE.toString());
    }

    public void setLastValue(Integer lastValue)
    {
        setDefinitionDatum(Datum.LAST_VALUE.toString(), lastValue);
    }

    public Integer getFetchSize()
    {
        return (Integer)getDefinitionDatum(Datum.FETCH_SIZE.toString());
    }

    public void setFetchSize(Integer lastValue)
    {
        setDefinitionDatum(Datum.FETCH_SIZE.toString(), lastValue);
    }

    public Boolean isLengthLimited()
    {
        if (getLength() == null)
        {
            return false;
        }
        if (getLength().compareTo(-1) == 0)
        {
            return false;
        }
        return true;
    }

    public Boolean isLastValueEnabled()
    {
        if (getLastValue() == null)
        {
            return false;
        }
        if (getLastValue().compareTo(-1) == 0)
        {
            return false;
        }

        return true;
    }
}
