package com.pilog.t8.definition.flow.node.event;

import com.pilog.t8.definition.script.*;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowEndScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_END";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FLOW_END";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow End Script";
    public static final String DESCRIPTION = "A script that is executed on execution of a flow end event.";
    // -------- Definition Meta-Data -------- //

    public T8WorkflowEndScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8EndEventDefinition endEventDefinition;

        endEventDefinition = (T8EndEventDefinition)getParentDefinition();
        if (endEventDefinition != null)
        {
            T8WorkFlowDefinition flowDefinition;

            flowDefinition = (T8WorkFlowDefinition)endEventDefinition.getParentDefinition();
            return flowDefinition.getFlowParameterDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8EndEventDefinition endEventDefinition;

        endEventDefinition = (T8EndEventDefinition)getParentDefinition();
        if (endEventDefinition != null)
        {
            T8WorkFlowDefinition flowDefinition;

            flowDefinition = (T8WorkFlowDefinition)endEventDefinition.getParentDefinition();
            return flowDefinition.getFlowParameterDefinitions();
        }
        else return null;
    }
}
