package com.pilog.t8.report;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ReportTypeSummary implements Serializable
{
    private String processId;
    private String name;
    private String description;
    private int totalCount;

    public T8ReportTypeSummary(String taskIdentifier)
    {
        this.processId = taskIdentifier;
    }

    public String getProcessIdentifier()
    {
        return processId;
    }

    public void setTaskIdentifier(String taskIdentifier)
    {
        this.processId = taskIdentifier;
    }

    public String getDisplayName()
    {
        return name;
    }

    public void setDisplayName(String displayName)
    {
        this.name = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }
}
