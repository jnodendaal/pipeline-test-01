package com.pilog.t8.definition.gfx.painter.gloss;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Bouwer.duPreez
 */
public class T8GlossPainterDefinition extends T8PainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_GLOSS";
    public static final String DISPLAY_NAME = "Gloss Painter";
    public static final String DESCRIPTION = "A painter that simulates a gloss effect on the painted area.";
    public enum Datum {GLOSS_PAINT_DEFINITION,
                       GLOSS_POSITION};
    // -------- Definition Meta-Data -------- //

    public enum GlossPosition {TOP, BOTTOM};
    
    public T8GlossPainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.GLOSS_PAINT_DEFINITION.toString(), "Gloss Paint", "The paint that is used to fill the glossy area."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.GLOSS_POSITION.toString(), "Gloss Position", "If position where the glossy area will be painted.", GlossPosition.TOP.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.GLOSS_PAINT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PaintDefinition.GROUP_IDENTIFIER));
        else if (Datum.GLOSS_POSITION.toString().equals(datumIdentifier)) return createStringOptions(GlossPosition.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Painter getNewPainterInstance()
    {
        try
        {
            T8Painter painter;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.gfx.painter.gloss.T8GlossPainter").getConstructor(T8GlossPainterDefinition.class);
            painter = (T8Painter)constructor.newInstance(this);
            return painter;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
  
    public GlossPosition getGlossPosition()
    {
        return GlossPosition.valueOf((String)getDefinitionDatum(Datum.GLOSS_POSITION.toString()));
    }

    public void setGlossPosition(GlossPosition position)
    {
         setDefinitionDatum(Datum.GLOSS_POSITION.toString(), position.toString());
    }
    
    public T8PaintDefinition getGlossPaintDefinition()
    {
        return (T8PaintDefinition)getDefinitionDatum(Datum.GLOSS_PAINT_DEFINITION.toString());
    }
    
    public void setGlossPaintDefinition(T8PaintDefinition definition)
    {
        setDefinitionDatum(Datum.GLOSS_PAINT_DEFINITION.toString(), definition);
    }
}
