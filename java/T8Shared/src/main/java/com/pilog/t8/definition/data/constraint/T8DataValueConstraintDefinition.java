package com.pilog.t8.definition.data.constraint;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public abstract class T8DataValueConstraintDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_VALUE_CONSTRAINTS";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_VALUE_CONSTRAINT";
    public static final String DISPLAY_NAME = "Data Value Constraint";
    public static final String DESCRIPTION = "A Data Value Constraint that specified the format constraint of allowed data";
    public static final String IDENTIFIER_PREFIX = "VAL_CONSTR_";

    public enum Datum
    {
        ENFORCE_VALIDATION
    };
// -------- Definition Meta-Data -------- //
    public T8DataValueConstraintDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract void validate(String fieldIdentifier, Object value);

    public abstract String getSQLRepresentation();

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ENFORCE_VALIDATION.toString(), "Enforce Validation", "Validate this field when new values get inserted.", true));
        return datumTypes;
    }

    public Boolean enforceValidation()
    {
        Boolean enforce = (Boolean)getDefinitionDatum(Datum.ENFORCE_VALIDATION.toString());
        if(enforce == null)
            return true;
        return enforce;
    }

    public void setEnforceValidation(Boolean validate)
    {
        setDefinitionDatum(Datum.ENFORCE_VALIDATION.toString(), validate);
    }
}
