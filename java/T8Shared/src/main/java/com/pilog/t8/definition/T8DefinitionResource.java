package com.pilog.t8.definition;

import com.pilog.t8.T8ServerContext;
import java.util.List;
import java.util.Set;

/**
 * This interface identifies classes that supply definitions to the framework
 * during startup.  All classes implementing this interface will be queried for
 * definitions and all supplied definitions will be cached during the 
 * initialization sequence of the framework.
 * 
 * @author Bouwer du Preez
 */
public interface T8DefinitionResource
{
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers);
}
