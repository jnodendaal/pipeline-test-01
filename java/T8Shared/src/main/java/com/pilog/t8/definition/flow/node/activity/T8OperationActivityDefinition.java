package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OperationActivityDefinition extends T8FlowActivityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_ACTIVITY_SERVER_OPERATION";
    public static final String DISPLAY_NAME = "Server Operation Activity";
    public static final String DESCRIPTION = "An activity that executes a server operation.";
    private enum Datum {OPERATION_IDENTIFIER,
                        OPERATION_INPUT_PARAMETER_MAPPING,
                        OPERATION_OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8OperationActivityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.OPERATION_IDENTIFIER.toString(), "Server Operation", "The identifier of the Server Operation to execute."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OPERATION_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow to Server Operation", "A mapping of Flow Parameters to the corresponding Server Operation Input Parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OPERATION_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Server Operation to Flow", "A mapping of Server Operation Output Parameters to the corresponding Flow Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPERATION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8JavaServerOperationDefinition.GROUP_IDENTIFIER));
        else if (Datum.OPERATION_INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            operationId = getOperationIdentifier();
            if (operationId != null)
            {
                T8JavaServerOperationDefinition operationDefinition;
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
                operationDefinition = (T8JavaServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> operationInputParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    operationInputParameterDefinitions = operationDefinition.getInputParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((operationInputParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition operationInputParameterDefinition : operationInputParameterDefinitions)
                            {
                                identifierList.add(operationInputParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(flowParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.OPERATION_OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            operationId = getOperationIdentifier();
            if (operationId != null)
            {
                T8JavaServerOperationDefinition operationDefinition;
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
                operationDefinition = (T8JavaServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> operationOutputParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    operationOutputParameterDefinitions = operationDefinition.getOutputParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((operationOutputParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition operationOutputParameterDefinition : operationOutputParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(operationOutputParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.activity.T8ServerOperationActivityNode").getConstructor(T8Context.class, T8Flow.class, T8OperationActivityDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    @Override
    public String getProfileIdentifier()
    {
        return null;
    }

    public String getOperationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString());
    }

    public void setOperationIdentifier(String taskIdentifier)
    {
        setDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString(), taskIdentifier);
    }

    public Map<String, String> getOperationInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OPERATION_INPUT_PARAMETER_MAPPING.toString());
    }

    public void setOperationInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OPERATION_INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getOperationOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OPERATION_OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOperationOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OPERATION_OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
