package com.pilog.t8.data;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataSessionDetails implements Serializable
{
    private String sessionIdentifier;
    private String parentSessionIdentifier;
    private String parentThreadName;
    private String userIdentifier;
    private String userProfileIdentifier;
    private int activeTransactionCount;
    private long creationTime;
    private long lastActivityTime;

    public T8DataSessionDetails(String sessionIdentifier)
    {
        this.sessionIdentifier = sessionIdentifier;
    }

    public String getParentThreadName()
    {
        return parentThreadName;
    }

    public void setParentThreadName(String parentThreadName)
    {
        this.parentThreadName = parentThreadName;
    }

    public String getParentSessionIdentifier()
    {
        return parentSessionIdentifier;
    }

    public void setParentSessionIdentifier(String parentSessionIdentifier)
    {
        this.parentSessionIdentifier = parentSessionIdentifier;
    }

    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(long lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public long getCreationTime()
    {
        return creationTime;
    }

    public void setCreationTime(long creationTime)
    {
        this.creationTime = creationTime;
    }

    public String getSessionIdentifier()
    {
        return sessionIdentifier;
    }

    public void setSessionIdentifier(String sessionIdentifier)
    {
        this.sessionIdentifier = sessionIdentifier;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userIdentifier = userIdentifier;
    }

    public String getUserProfileIdentifier()
    {
        return userProfileIdentifier;
    }

    public void setUserProfileIdentifier(String userProfileIdentifier)
    {
        this.userProfileIdentifier = userProfileIdentifier;
    }

    public int getActiveTransactionCount()
    {
        return activeTransactionCount;
    }

    public void setActiveTransactionCount(int activeTransactionCount)
    {
        this.activeTransactionCount = activeTransactionCount;
    }
}
