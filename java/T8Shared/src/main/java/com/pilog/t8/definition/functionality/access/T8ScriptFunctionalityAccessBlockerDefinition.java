package com.pilog.t8.definition.functionality.access;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.functionality.access.T8FunctionalityAccessBlocker;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptFunctionalityAccessBlockerDefinition extends T8FunctionalityAccessBlockerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_ACCESS_BLOCKER_SCRIPT";
    public static final String DISPLAY_NAME = "Script Functionality Access Blocker";
    public static final String DESCRIPTION = "A scripted functionality access blocker.";
    public enum Datum {SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8ScriptFunctionalityAccessBlockerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SCRIPT_DEFINITION.toString(), "Script",  "The script the will be executed to create functionality access blocks."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FunctionalityAccessBlockerScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8FunctionalityAccessBlocker createNewAccessBlockerInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.access.T8ScriptFunctionalityAccessBlocker").getConstructor(T8Context.class, T8ScriptFunctionalityAccessBlockerDefinition.class);
        return (T8FunctionalityAccessBlocker)constructor.newInstance(context, this);
    }

    public T8FunctionalityAccessBlockerScriptDefinition getScriptDefinition()
    {
        return (T8FunctionalityAccessBlockerScriptDefinition)getDefinitionDatum(Datum.SCRIPT_DEFINITION.toString());
    }

    public void setScriptDefinition(T8FunctionalityAccessBlockerScriptDefinition scriptDefinition)
    {
        setDefinitionDatum(Datum.SCRIPT_DEFINITION.toString(), scriptDefinition);
    }
}
