package com.pilog.t8.definition.ui;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentInterfaceDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_INTERFACE";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_INTERFACE";
    public static final String DISPLAY_NAME = "Component Interface";
    public static final String DESCRIPTION = "An interface that defines a set of API events an operations implemented by all components that implement the interface.";
    public static final String IDENTIFIER_PREFIX = "CI_";
    public enum Datum {EVENT_DEFINITIONS,
                       OPERATION_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ComponentInterfaceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EVENT_DEFINITIONS.toString(), "Interface Events", "The events defined by the interface of this container."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OPERATION_DEFINITIONS.toString(), "Interface Operations", "The operations defined by the interface of this container."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EVENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ComponentInterfaceEventDefinition.TYPE_IDENTIFIER));
        else if (Datum.OPERATION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ComponentInterfaceOperationDefinition.TYPE_IDENTIFIER));
        else return null;
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public T8ComponentInterfaceEventDefinition getEventDefinition(String identifier)
    {
        return (T8ComponentInterfaceEventDefinition)getSubDefinition(identifier);
    }

    public List<T8ComponentInterfaceEventDefinition> getEventDefinitions()
    {
        return (List<T8ComponentInterfaceEventDefinition>)getDefinitionDatum(Datum.EVENT_DEFINITIONS.toString());
    }
    
    public void setEventDefinitions(List<T8ComponentInterfaceEventDefinition> eventDefinitions)
    {
        setDefinitionDatum(Datum.EVENT_DEFINITIONS.toString(), eventDefinitions);
    }
    
    public T8ComponentInterfaceOperationDefinition getOperationDefinition(String identifier)
    {
        return (T8ComponentInterfaceOperationDefinition)getSubDefinition(identifier);
    }
    
    public List<T8ComponentInterfaceOperationDefinition> getOperationDefinitions()
    {
        return (List<T8ComponentInterfaceOperationDefinition>)getDefinitionDatum(Datum.OPERATION_DEFINITIONS.toString());
    }
    
    public void setOperationDefinitions(List<T8ComponentInterfaceOperationDefinition> operationDefinitions)
    {
        setDefinitionDatum(Datum.OPERATION_DEFINITIONS.name(), operationDefinitions);
    }
}
