package com.pilog.t8.ui;

/**
 * @author Hennie Brink
 */
public interface T8RootPane extends T8Component
{
    public void lockUI(String message);
    public void unlockUI();
}
