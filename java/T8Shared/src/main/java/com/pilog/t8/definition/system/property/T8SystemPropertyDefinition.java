package com.pilog.t8.definition.system.property;

import com.pilog.t8.definition.T8Definition;

/**
 * @author Bouwer du Preez
 */
public abstract class T8SystemPropertyDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SYSTEM_PROPERTY";
    public static final String GROUP_NAME = "System Properties";
    public static final String GROUP_DESCRIPTION = "Properties that are configured on a global level, setting flags and options that govern the behavior or specific functionalities.";
    public static final String IDENTIFIER_PREFIX = "SYS_PROP_";
    public static final String STORAGE_PATH = "/system_properties";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8SystemPropertyDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract Object getPropertyValue();
}
