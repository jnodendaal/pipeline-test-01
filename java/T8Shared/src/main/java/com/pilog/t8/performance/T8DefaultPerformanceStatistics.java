package com.pilog.t8.performance;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultPerformanceStatistics implements T8PerformanceStatistics, Serializable
{
    private final Map<String, T8ThreadPerformanceStatistics> threadStatistics;
    private final String identifier;
    private boolean enabled;

    public T8DefaultPerformanceStatistics(String identifier)
    {
        this.identifier = identifier;
        this.threadStatistics = new HashMap<String, T8ThreadPerformanceStatistics>();
        this.enabled = true;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public boolean isEnabled()
    {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public void reset()
    {
        threadStatistics.clear();
    }

    @Override
    public void logEvent(String eventIdentifier)
    {
        if (enabled)
        {
            T8ThreadPerformanceStatistics stats;
            String threadName;

            threadName = Thread.currentThread().getName();
            stats = threadStatistics.get(threadName);
            if (stats == null)
            {
                stats = new T8DefaultThreadPerformanceStatistics(threadName);
                threadStatistics.put(threadName, stats);
            }

            stats.logEvent(eventIdentifier);
        }
    }

    @Override
    public void logExecutionStart(String executionIdentifier)
    {
        if (enabled)
        {
            T8ThreadPerformanceStatistics stats;
            String threadName;

            threadName = Thread.currentThread().getName();
            stats = threadStatistics.get(threadName);
            if (stats == null)
            {
                stats = new T8DefaultThreadPerformanceStatistics(threadName);
                threadStatistics.put(threadName, stats);
            }

            stats.logExecutionStart(executionIdentifier);
        }
    }

    @Override
    public void logExecutionEnd(String executionIdentifier)
    {
        if (enabled)
        {
            T8ThreadPerformanceStatistics category;
            String threadName;

            threadName = Thread.currentThread().getName();
            category = threadStatistics.get(threadName);
            if (category != null)
            {
                category.logExecutionEnd(executionIdentifier);
            }
            else throw new RuntimeException("Logging of category execution end not possible because the execution has not been logged as started: " + threadName);
        }
    }

    @Override
    public void printStatistics(OutputStream stream)
    {
        PrintWriter writer;

        writer = new PrintWriter(stream);
        writer.println("Performance Statistics: " + identifier);
        writer.println("===============================================================================================================");
        writer.flush();
        for (T8ThreadPerformanceStatistics stats : threadStatistics.values())
        {
            stats.printStatistics(stream);
        }
    }

    @Override
    public List<T8ThreadPerformanceStatistics> getThreadPerformanceStatistics()
    {
        return new ArrayList<T8ThreadPerformanceStatistics>(threadStatistics.values());
    }

    public void addPerformanceStatistics(T8PerformanceStatistics stats)
    {
        if (stats != null)
        {
            for (T8ThreadPerformanceStatistics catStats : stats.getThreadPerformanceStatistics())
            {
                String catIdentifier;

                catIdentifier = catStats.getIdentifier();
                if (threadStatistics.containsKey(catIdentifier)) catIdentifier = (catIdentifier + "*");
                threadStatistics.put(catIdentifier, catStats);
            }
        }
    }
}
