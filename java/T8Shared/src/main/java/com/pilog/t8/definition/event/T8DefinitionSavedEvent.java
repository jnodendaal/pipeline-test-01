package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.security.T8Context;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionSavedEvent extends EventObject
{
    private final T8Context context;
    private final T8Definition definition;

    public T8DefinitionSavedEvent(T8Context context, T8Definition definition)
    {
        super(definition);
        this.context = context;
        this.definition = definition;
    }

    public T8Context getContext()
    {
        return context;
    }

    public T8Definition getDefinition()
    {
        return definition;
    }
}
