package com.pilog.t8.definition.report;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.gfx.image.T8ImageDefinition;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer dy Preez
 */
public abstract class T8ReportDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_REPORT";
    public static final String GROUP_NAME = "Reports";
    public static final String GROUP_DESCRIPTION = "Predefined reports that can be requested for generation by clients or system processes.";
    public static final String STORAGE_PATH = "/reports";
    public static final String DISPLAY_NAME = "T8 Report";
    public static final String DESCRIPTION = "Definition to set up a Tech 8 report.";
    public static final String IDENTIFIER_PREFIX = "REPORT_";
    public enum Datum
    {
        THUMBNAIL_IMAGE_ID,
        TYPE_NAME,
        TYPE_DESCRIPTION,
        DESCRIPTION_SCRIPT,
        REPORT_PARAMETERS
    };
    // -------- Definition Meta-Data -------- //

    public T8ReportDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.THUMBNAIL_IMAGE_ID.toString(), "Thumbnail Image", "The image that will be used as a thumbnail representing this report on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TYPE_NAME.toString(), "Type Name", "The name to use when describing reports of this type on a UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TYPE_DESCRIPTION.toString(), "Type Description", "The description to use for reports of this type on a UI."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.DESCRIPTION_SCRIPT.toString(), "Discription Script", "The script to execute when determining the display name and description of the report."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REPORT_PARAMETERS.toString(), "Report Parameters", "The parameters required to generate this report."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.THUMBNAIL_IMAGE_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ImageDefinition.GROUP_IDENTIFIER), true, "No Thumbnail");
        else if (Datum.REPORT_PARAMETERS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.DESCRIPTION_SCRIPT.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ReportDescriptionScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if ((Datum.REPORT_PARAMETERS.toString().equals(datumId)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    public abstract T8ReportGenerator getReportGenerator(T8Context context);

    public void addReportParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.REPORT_PARAMETERS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getReportParameterDefinitions()
    {
        return getDefinitionDatum(Datum.REPORT_PARAMETERS);
    }

    public void setReportParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.REPORT_PARAMETERS, parameterDefinitions);
    }

    public String getThumbnailImageId()
    {
        return getDefinitionDatum(Datum.THUMBNAIL_IMAGE_ID);
    }

    public void setThumbnailImageId(String id)
    {
        setDefinitionDatum(Datum.THUMBNAIL_IMAGE_ID, id);
    }

    public String getTypeName()
    {
        return getDefinitionDatum(Datum.TYPE_NAME);
    }

    public void setTypeName(String name)
    {
        setDefinitionDatum(Datum.TYPE_NAME, name);
    }

    public String getTypeDescription()
    {
        return getDefinitionDatum(Datum.TYPE_DESCRIPTION);
    }

    public void setTypeDescription(String description)
    {
        setDefinitionDatum(Datum.TYPE_DESCRIPTION, description);
    }

    public T8ReportDescriptionScriptDefinition getDescriptionScript()
    {
        return getDefinitionDatum(Datum.DESCRIPTION_SCRIPT);
    }

    public void setDescriptionScript(T8ReportDescriptionScriptDefinition definition)
    {
        setDefinitionDatum(Datum.DESCRIPTION_SCRIPT, definition);
    }
}
