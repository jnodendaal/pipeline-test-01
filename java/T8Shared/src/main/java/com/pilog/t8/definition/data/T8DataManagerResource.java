package com.pilog.t8.definition.data;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GET_DATA_SESSION_DETAILS = "@OS_GET_DATA_SESSION_DETAILS";
    public static final String OPERATION_KILL_DATA_SESSION = "@OS_KILL_DATA_SESSION";
    public static final String OPERATION_RETRIEVE_DATA_SOURCE_FIELD_DEFINITIONS = "@OS_RETRIEVE_DATA_SOURCE_FIELD_DEFINITIONS";
    public static final String OPERATION_RETRIEVE_DATA_ENTITY = "@OS_RETRIEVE_DATA_ENTITY";
    public static final String OPERATION_INSERT_DATA_ENTITY = "@OS_INSERT_DATA_ENTITY";
    public static final String OPERATION_UPDATE_DATA_ENTITY = "@OS_UPDATE_DATA_ENTITY";
    public static final String OPERATION_CREATE_DATA_ENTITY = "@OS_CREATE_DATA_ENTITY";
    public static final String OPERATION_DELETE_DATA_ENTITY = "@OS_DELETE_DATA_ENTITY";
    public static final String OPERATION_SELECT_DATA_ENTITIES = "@OS_SELECT_DATA_ENTITIES";
    public static final String OPERATION_COUNT_DATA_ENTITIES = "@OS_COUNT_DATA_ENTITIES";
    public static final String OPERATION_VALIDATE_DATA_ENTITIES = "@OS_VALIDATE_DATA_ENTITIES";
    public static final String OPERATION_GET_SQL_QUERY_STRING = "@OS_DATA_MANAGER_GET_SQL_QUERY_STRING";
    public static final String OPERATION_TEST_DIRECT_CONNECTION = "@OS_TEST_DIRECT_CONNECTION";
    public static final String OPERATION_GET_CACHE_SIZES = "@OP_DAT_GET_CACHE_SIZES";
    public static final String OPERATION_CLEAR_CACHE = "@OP_DAT_CLEAR_CACHE";

    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_FAILURE_MESSAGE = "$P_FAILURE_MESSAGE";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_FIELD_VALUE_MAP = "$P_FIELD_VALUE_MAP";
    public static final String PARAMETER_FIELD_DEFINITION_LIST = "$P_FIELD_DEFINITION_LIST";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";
    public static final String PARAMETER_DATA_ENTITY_COUNT = "$P_DATA_ENTITY_COUNT";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_DATA_SOURCE_IDENTIFIER = "$P_DATA_SOURCE_IDENTIFIER";
    public static final String PARAMETER_DATA_ENTITY_IDENTIFIER = "$P_DATA_ENTITY_IDENTIFIER";
    public static final String PARAMETER_DATA_ENTITY_DEFINITION = "$P_DATA_ENTITY_DEFINITION";
    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";
    public static final String PARAMETER_DATA_ENTITY_LIST = "$P_DATA_ENTITY_LIST";
    public static final String PARAMETER_DATA_ENTITY_VALIDATION_SCRIPT_DEFINITION = "$P_DATA_ENTITY_VALIDATION_SCRIPT_DEFINITION";
    public static final String PARAMETER_DATA_ENTITY_VALIDATION_REPORT_LIST = "$P_DATA_ENTITY_VALIDATION_REPORT_LIST";
    public static final String PARAMETER_DATA_SESSION_DETAILS_LIST = "$P_DATA_SESSION_DETAILS_LIST";
    public static final String PARAMETER_DATA_SESSION_IDENTIFIER = "$P_DATA_SESSION_IDENTIFIER";
    public static final String PARAMETER_SQL_QUERY_STRING = "$P_SQL_QUERY_STRING";
    public static final String PARAMETER_CONNECTION_IDENTIFIER = "$P_CONNECTION_IDENTIFIER";
    public static final String PARAMETER_CACHE_SIZE_MAP = "$P_CACHE_SIZE_MAP";
    public static final String PARAMETER_CACHE_ID = "$P_CACHE_ID";

    public static final String DATA_KEY_STATE_DS_IDENTIFIER = "@DS_STATE_DATA_KEY";
    public static final String DATA_KEY_STATE_DE_IDENTIFIER = "@E_STATE_DATA_KEY";

    public static final String TABLE_DATA_KEY = "DAT_KEY";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Data Key State Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_KEY_STATE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_KEY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_KEY_IDENTIFIER", "DATA_KEY_ID", true, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEED_VALUE", "SEED_VALUE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Data Key State Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_KEY_STATE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_KEY_STATE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_KEY_IDENTIFIER", DATA_KEY_STATE_DS_IDENTIFIER + "$DATA_KEY_IDENTIFIER", true, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEED_VALUE", DATA_KEY_STATE_DS_IDENTIFIER + "$SEED_VALUE", false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_DATA_SESSION_DETAILS);
        definition.setMetaDescription("Get Data Session Details");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$GetDataSessionDetailsOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_KILL_DATA_SESSION);
        definition.setMetaDescription("Kills a Data Session");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$KillDataSessionOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_SESSION_IDENTIFIER, "Data Session Identifier", "The unique identifier of the data session to be killed.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RETRIEVE_DATA_SOURCE_FIELD_DEFINITIONS);
        definition.setMetaDescription("Retrieve Data Source Field Definitions");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$RetrieveDataSourceFieldDefinitionsOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the data source is located.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_SOURCE_IDENTIFIER, "Data Source Identifier", "The Identifier of the data source for which to retrieve field definitions.", T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RETRIEVE_DATA_ENTITY);
        definition.setMetaDescription("Retrieve Data Entity");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$RetrieveDataEntityOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY_IDENTIFIER, "Data Record Document Entity Identifier", "The unique identifier of the entity from which the document will be retrieved.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FIELD_VALUE_MAP, "Field Value Map", "A Map containing the Field Values that will be used as keys to retrieve the data record document.", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "A data entity retrieved (null if not found).", T8DataType.DATA_ENTITY));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_INSERT_DATA_ENTITY);
        definition.setMetaDescription("Insert Data Entity");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$InsertDataEntityOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY, "Data Record Document Entity", "The entity to insert.", T8DataType.DATA_ENTITY));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_UPDATE_DATA_ENTITY);
        definition.setMetaDescription("Update Data Entity");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$UpdateDataEntityOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY, "Data Record Document Entity", "The entity to update.", T8DataType.DATA_ENTITY));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Data Entity", "A boolean value indicating whether or not the supplied entity was successfully updated.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CREATE_DATA_ENTITY);
        definition.setMetaDescription("Create Data Entity");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$CreateDataEntityOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY_IDENTIFIER, "Data Record Document Entity Identifier", "The entity identifier to create.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FIELD_VALUE_MAP, "Data Record Document Entity Field Values Map", "The entity field values map.", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The newly created data entity.", T8DataType.DATA_ENTITY));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_DATA_ENTITY);
        definition.setMetaDescription("Delete Data Entity");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$DeleteDataEntityOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_SELECT_DATA_ENTITIES);
        definition.setMetaDisplayName("Select Data Entities");
        definition.setOperationDisplayName("Select Data Entities");
        definition.setMetaDescription("Selects the Data Entities based on the entity identifier and other parameters provided.");
        definition.setOperationDescription("Selects the Data Entities based on the entity identifier and other parameters provided.");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$SelectDataEntitiesOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY_IDENTIFIER, "Data Entity Identifier", "The entity for which data will be retrieved.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The Data Filter to be applied when the data is queried.", T8DataType.DATA_FILTER, true));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Start Offset", "The Offset from where .", T8DataType.INTEGER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The total number of records to be retrieved as a page.", T8DataType.INTEGER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity Results", "The list of Data Entity objects as the result of the executed query.", new T8DtList(T8DataType.DATA_ENTITY)));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_COUNT_DATA_ENTITIES);
        definition.setMetaDescription("Count Data Entities");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$CountDataEntitiesOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY_IDENTIFIER, "Data Entity Identifier", "The identifier of the data entity on which the count will be performed.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter to use when performing the count operation.", T8DataType.DATA_FILTER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY_COUNT, "Data Entity Count", "The number of data entities counted.", T8DataType.INTEGER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_VALIDATE_DATA_ENTITIES);
        definition.setMetaDescription("Validate Data Entities");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$ValidateDataEntitiesOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_SQL_QUERY_STRING);
        definition.setMetaDescription("Get SQL Query String");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$GetSQLQueryString");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the data source is located.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_SOURCE_IDENTIFIER, "Data Source Identifier", "The Identifier of the data source for which the query string must be generated.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SQL_QUERY_STRING, "SQL Query String", "The SQL Query String that will get executed by the data source.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_TEST_DIRECT_CONNECTION);
        definition.setMetaDescription("Test Direct Connection");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$TestDirectConnectionConnectionOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the connection is located.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONNECTION_IDENTIFIER, "Connection Identifier", "The Identifier of the connection to test.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "If the connection succeeded.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAILURE_MESSAGE, "Failure Message", "If the connection test failed, the exception message will be used as the failure message.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_CACHE_SIZES);
        definition.setMetaDisplayName("Get Cache Size");
        definition.setMetaDescription("Returns a map containing the sizes of the caches that are currently active.");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$GetCacheSizesOperation");
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CACHE_SIZE_MAP, "Cache Size Map", "A map containing the names of caches as key set and for each key an integer value indicating the number of entries in the cache.", T8DataType.MAP));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CLEAR_CACHE);
        definition.setMetaDisplayName("Clear Cache");
        definition.setMetaDescription("Clears the specified cache, removing all entries it contains.  This method should be used carefully and only when no concurrent access to the cache can be guaranteed.");
        definition.setClassName("com.pilog.t8.data.T8DataManagerOperations$ClearCacheOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CACHE_ID, "Cache Id", "The id of the cache to clear.", T8DataType.STRING));
        definitions.add(definition);

        return definitions;
    }
}
