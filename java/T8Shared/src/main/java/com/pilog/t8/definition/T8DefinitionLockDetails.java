package com.pilog.t8.definition;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionLockDetails implements Serializable
{
    private String definitionIdentifier;
    private String projectIdentifier;
    private String userIdentifier;
    private String userProfileIdentifier;
    private boolean locked;
    
    public T8DefinitionLockDetails(String definitionIdentifier, String projectIdentifier, String userIdentifier, String userProfileIdentifier, boolean locked)
    {
        this.definitionIdentifier = definitionIdentifier;
        this.projectIdentifier = projectIdentifier;
        this.userIdentifier = userIdentifier;
        this.userProfileIdentifier = userProfileIdentifier;
        this.locked = locked;
    }

    public String getDefinitionIdentifier()
    {
        return definitionIdentifier;
    }

    public void setDefinitionIdentifier(String definitionIdentifier)
    {
        this.definitionIdentifier = definitionIdentifier;
    }

    public String getProjectIdentifier()
    {
        return projectIdentifier;
    }

    public void setProjectIdentifier(String projectIdentifier)
    {
        this.projectIdentifier = projectIdentifier;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userIdentifier = userIdentifier;
    }

    public String getUserProfileIdentifier()
    {
        return userProfileIdentifier;
    }

    public void setUserProfileIdentifier(String userProfileIdentifier)
    {
        this.userProfileIdentifier = userProfileIdentifier;
    }

    public boolean isLocked()
    {
        return locked;
    }

    public void setLocked(boolean locked)
    {
        this.locked = locked;
    }
}
