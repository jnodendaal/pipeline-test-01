package com.pilog.t8.ui;

import com.pilog.t8.ui.event.T8DefinitionEditorListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionEditor
{
    /**
     * This method is called from a non-EDT thread and must perform all heavy
     * initialization of the editor before it can be used.
     */
    public void initializeComponent();
    
    /**
     * This method is called when the editor has been made visible within the
     * context where it is used and the definition is ready to be edited.  This
     * method installs listeners and does all the required setup for the editor.
     */
    public void startComponent();
    
    /**
     * This method is called before the definition editor is discarded or hidden
     * and should ALWAYS be called once the editor is no longer usable.  This
     * method will perform any cleanup operations that are required.  
     */
    public void stopComponent();
    
    /**
     * Commits all outstanding changes on the editor.
     */
    public void commitChanges();
    
    /**
     * This method is called whenever the context in which the editor is 
     * currently executing requires that the content of the editor needs to be
     * refreshed in order to reflect updates that result from related definition
     * changes.
     */
    public void refreshEditor();
    
    public void addDefinitionEditorListener(T8DefinitionEditorListener listener);
    public void removeDefinitionEditorListener(T8DefinitionEditorListener listener);
    
    public void setDefinitionDetailsVisible(boolean identifierEnabled, boolean displayNameEnabled, boolean descriptionEnabled);
}
