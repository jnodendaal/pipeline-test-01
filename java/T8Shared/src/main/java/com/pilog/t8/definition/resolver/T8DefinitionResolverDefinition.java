package com.pilog.t8.definition.resolver;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionResolver;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefinitionResolverDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_RESOLVER";
    public static final String DISPLAY_NAME = "Definition Resolver";
    public static final String DESCRIPTION = "A definition of a resolver that is used to determine the most applicable definition of a specific type within a specific environment.";
    public static final String IDENTIFIER_PREFIX = "DRESOLVE_";
    public static final String STORAGE_PATH = "/definition_resolvers";
    public enum Datum {DEFINITION_TYPE_IDENTIFIERS};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionResolverDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_LIST, Datum.DEFINITION_TYPE_IDENTIFIERS.toString(), "Definition Types", "The definition types to store in this cache.  If a type is not found, the definition identifiers will be checked."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DEFINITION_TYPE_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            List<String> typeIdentifiers;

            typeIdentifiers = definitionContext.getDefinitionTypeIdentifiers();
            Collections.sort(typeIdentifiers);
            return createStringOptions(typeIdentifiers);
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DefinitionResolver getNewDefinitionResolverInstance(T8Context context) throws Exception;

    public List<String> getDefinitionTypeIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIERS.toString());
    }

    public void setDefinitionTypeIdentifiers(List<String> typeIdentifiers)
    {
        setDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIERS.toString(), typeIdentifiers);
    }
}
