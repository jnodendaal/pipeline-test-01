package com.pilog.t8.security;

import java.io.Serializable;

/**
 * This class defines the heartbeat response returned by the server when a
 * heartbeat request is received from a client.  This response will contain
 * standard data related to the current state of the client's session.
 *
 * @author Bouwer du Preez
 */
public class T8HeartbeatResponse implements Serializable
{
    private long serverTime;
    private long lastActivityTime;
    private long expirationTime;
    private double serverLoad;
    private double serverRequestsPerSecond;
    private int maximumConcurrency;
    private boolean hasPendingNotifications;
    private int taskCount;
    private int reportCount;
    private int processCount;

    public T8HeartbeatResponse()
    {
        lastActivityTime = -1;
        expirationTime = -1;
        serverTime = -1;
        serverLoad = -1;
        serverRequestsPerSecond = -1;
        maximumConcurrency = -1;
        taskCount = -1;
        reportCount = -1;
        processCount = -1;
    }

    public long getServerTime()
    {
        return serverTime;
    }

    public void setServerTime(long serverTime)
    {
        this.serverTime = serverTime;
    }

    public double getServerLoad()
    {
        return serverLoad;
    }

    public void setServerLoad(double serverLoad)
    {
        this.serverLoad = serverLoad;
    }

    public double getServerRequestsPerSecond()
    {
        return serverRequestsPerSecond;
    }

    public void setServerRequestsPerSecond(double serverRequestsPerSecond)
    {
        this.serverRequestsPerSecond = serverRequestsPerSecond;
    }

    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(long lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public boolean hasPendingNotifications()
    {
        return hasPendingNotifications;
    }

    public void setHasPendingNotifications(boolean hasPendingNotifications)
    {
        this.hasPendingNotifications = hasPendingNotifications;
    }

    public int getMaximumConcurrency()
    {
        return maximumConcurrency;
    }

    public void setMaximumConcurrency(int maximumConcurrency)
    {
        this.maximumConcurrency = maximumConcurrency;
    }

    public int getTaskCount()
    {
        return taskCount;
    }

    public void setTaskCount(int taskCount)
    {
        this.taskCount = taskCount;
    }

    public int getReportCount()
    {
        return reportCount;
    }

    public void setReportCount(int reportCount)
    {
        this.reportCount = reportCount;
    }

    public int getProcessCount()
    {
        return processCount;
    }

    public void setProcessCount(int processCount)
    {
        this.processCount = processCount;
    }
}
