package com.pilog.t8.flow.state.data;

import com.pilog.t8.data.T8DataEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NodeStateDataSet implements Serializable
{
    private final Map<String, T8ParameterStateDataSet> inputParameterDataSets; // Key: NODE_ID, Value: The parameter data set for the input node.
    private final T8ParameterStateDataSet executionParameterDataSet;
    private final T8ParameterStateDataSet outputParameterDataSet;
    private final T8ParameterStateDataSet outputNodeDataSet;
    private final List<T8DataEntity> waitKeyEntities;
    private final T8DataEntity nodeEntity;
    private final String nodeIID;
    private T8TaskStateDataSet taskStateDataSet;

    public T8NodeStateDataSet(String nodeIID, T8DataEntity nodeEntity)
    {
        this.nodeIID = nodeIID;
        this.nodeEntity = nodeEntity;
        this.executionParameterDataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
        this.outputParameterDataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
        this.outputNodeDataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
        this.inputParameterDataSets = new HashMap<String, T8ParameterStateDataSet>();
        this.waitKeyEntities = new ArrayList<T8DataEntity>();
    }

    public String getNodeIID()
    {
        return nodeIID;
    }

    public T8DataEntity getNodeEntity()
    {
        return nodeEntity;
    }

    public T8ParameterStateDataSet getExecutionParameterDataSet()
    {
        return executionParameterDataSet;
    }

    public T8ParameterStateDataSet getOutputParameterDataSet()
    {
        return outputParameterDataSet;
    }

    public T8ParameterStateDataSet getOutputNodeDataSet()
    {
        return outputNodeDataSet;
    }

    public T8TaskStateDataSet getTaskStateDataSet()
    {
        return taskStateDataSet;
    }

    public List<String> getInputNodeIDList()
    {
        return new ArrayList<String>(inputParameterDataSets.keySet());
    }

    public T8ParameterStateDataSet getInputParameterDataSet(String nodeID)
    {
        return inputParameterDataSets.get(nodeID);
    }

    public List<T8ParameterStateDataSet> getInputParameterDataSets()
    {
        return new ArrayList<T8ParameterStateDataSet>(inputParameterDataSets.values());
    }

    public List<T8DataEntity> getWaitKeyEntities()
    {
        return waitKeyEntities;
    }

    public int getEntityCount()
    {
        int count;

        count = nodeEntity != null ? 1 : 0;
        count += executionParameterDataSet.getEntityCount();
        count += outputParameterDataSet.getEntityCount();
        count += outputNodeDataSet.getEntityCount();
        count += waitKeyEntities.size();

        if (taskStateDataSet != null) count += taskStateDataSet.getEntityCount();

        for (T8ParameterStateDataSet inputParameterDataSet : getInputParameterDataSets())
        {
            count += inputParameterDataSet.getEntityCount();
        }

        return count;
    }

    public void addInputParameterData(T8DataEntity entity)
    {
        T8ParameterStateDataSet dataSet;
        String nodeID;

        nodeID = (String)entity.getFieldValue("$INPUT_NODE_ID"); // The node from which the input data came.
        dataSet = inputParameterDataSets.get(nodeID);
        if (dataSet == null)
        {
            dataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
            dataSet.addParameterData(entity);
            inputParameterDataSets.put(nodeID, dataSet);
        }
        else
        {
            dataSet.addParameterData(entity);
        }
    }

    public void addExecutionParameterData(T8DataEntity entity)
    {
        executionParameterDataSet.addParameterData(entity);
    }

    public void addOutputParameterData(T8DataEntity entity)
    {
        outputParameterDataSet.addParameterData(entity);
    }

    public void addOutputNodeData(T8DataEntity entity)
    {
        outputNodeDataSet.addParameterData(entity);
    }

    public void setTaskData(T8DataEntity entity)
    {
        taskStateDataSet = new T8TaskStateDataSet(entity);
    }

    public void addTaskInputParameterData(T8DataEntity entity)
    {
        taskStateDataSet.addInputParameterData(entity);
    }

    public void addTaskOutputParameterData(T8DataEntity entity)
    {
        taskStateDataSet.addOutputParameterData(entity);
    }

    public void addTaskPropertyData(T8DataEntity entity)
    {
        taskStateDataSet.addPropertyData(entity);
    }

    public void addWaitKeyData(T8DataEntity entity)
    {
        waitKeyEntities.add(entity);
    }
}
