package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final T8ModuleDefinition moduleDefinition;
    private final Map<String, Object> inputParameters;
    private final List<String> finalizationEventIdentifiers;

    public T8ModuleFunctionalityAccessHandle(String functionalityId, String functionalityIid, T8ModuleDefinition moduleDefinition, Map<String, Object> inputParameters, List<String> finalizationEventIdentifiers, String displayName, String description, Icon icon)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.moduleDefinition = moduleDefinition;
        this.inputParameters = inputParameters;
        this.finalizationEventIdentifiers = finalizationEventIdentifiers;
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        T8FunctionalityView view;

        constructor = Class.forName("com.pilog.t8.functionality.view.T8ModuleFunctionalityView").getConstructor(T8ComponentController.class, T8ModuleFunctionalityAccessHandle.class, T8ModuleDefinition.class, Map.class, List.class);
        view = (T8FunctionalityView)constructor.newInstance(controller, this, moduleDefinition, inputParameters, finalizationEventIdentifiers);
        view.addFunctionalityViewListener(viewListener);
        return view;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return true;
    }
}
