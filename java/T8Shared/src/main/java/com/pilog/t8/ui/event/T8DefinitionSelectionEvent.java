package com.pilog.t8.ui.event;

import com.pilog.t8.definition.T8Definition;
import java.awt.Component;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionSelectionEvent extends EventObject
{
    private final T8Definition newSelection;

    public T8DefinitionSelectionEvent(Component source, T8Definition newSelection)
    {
        super(source);
        this.newSelection = newSelection;
    }

    public T8Definition getNewSelection()
    {
        return newSelection;
    }
}
