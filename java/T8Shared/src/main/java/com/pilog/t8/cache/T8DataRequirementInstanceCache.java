package com.pilog.t8.cache;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRequirementInstanceCache
{
    /**
     * Returns the TransactionManger responsible for transactions involving this terminology cache.
     * This method currently returns an object only to avoid having to include J2EE libraries in this project.
     * In future this interface will be moved and will return the property type: javax.transaction.TransactionManager
     * @return javax.transaction.TransactionManager responsible for transactions on this cache.
     */
    public Object getTransactionManager();

    public void put(DataRequirementInstanceData drInstance);
    public void remove(DataRequirementInstanceData drInstance);
    public DataRequirementInstanceData remove(String drInstanceId);
    public DataRequirementInstanceData get(String drInstanceId);
    public boolean containsKey(String drInstanceId);

    public class DataRequirementInstanceData implements Serializable
    {
        private String drInstanceId;
        private String drId;
        private boolean independentIndicator;
        private String recordOntologyClass;

        public DataRequirementInstanceData(String drInstanceId, String drId, boolean independentIndicator, String recordOntologyClass)
        {
            this.drInstanceId = drInstanceId;
            this.drId = drId;
            this.independentIndicator = independentIndicator;
            this.recordOntologyClass = recordOntologyClass;
        }

        public String getDrInstanceId()
        {
            return drInstanceId;
        }

        public void setDrInstanceId(String drInstanceId)
        {
            this.drInstanceId = drInstanceId;
        }

        public String getDrId()
        {
            return drId;
        }

        public void setDrId(String drId)
        {
            this.drId = drId;
        }

        public boolean isIndependentIndicator()
        {
            return independentIndicator;
        }

        public void setIndependentIndicator(boolean independentIndicator)
        {
            this.independentIndicator = independentIndicator;
        }

        public String getRecordOntologyClass()
        {
            return recordOntologyClass;
        }

        public void setRecordOntologyClass(String recordOntologyClass)
        {
            this.recordOntologyClass = recordOntologyClass;
        }
    }
}
