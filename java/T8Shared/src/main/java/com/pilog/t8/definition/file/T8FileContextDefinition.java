package com.pilog.t8.definition.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FileContextDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FILE_CONTEXT";
    public static final String GROUP_NAME = "File Contexts";
    public static final String GROUP_DESCRIPTION = "Definitions of the various contexts in the system where files can be read or stored.  Each context definition contains configuration specific to the location it pertains to.";
    public static final String STORAGE_PATH = "/file_contexts";
    public static final String IDENTIFIER_PREFIX = "FILE_CONTEXT_";
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8FileContextDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8FileContext createNewFileContextInstance(T8Context context, T8FileManager fileManager, String contextInstanceID) throws Exception;
}
