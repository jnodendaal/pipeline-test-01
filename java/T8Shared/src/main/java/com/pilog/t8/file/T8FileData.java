package com.pilog.t8.file;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FileData implements Serializable
{
    private String filename;
    private byte[] data;

    public T8FileData()
    {
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }
}
