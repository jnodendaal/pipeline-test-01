package com.pilog.t8.security;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.script.T8ExpressionEvaluator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The {@code T8Context} defines the environmental, data, functional and security context
 * within which a path of execution is conducted.
 *
 * @author Bouwer du Preez
 */
public class T8Context implements Serializable
{
    private transient T8ServerContext serverContext;
    private transient T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8Context parentContext;
    private String sessionId;
    private String systemAgentId;
    private String systemAgentIid;
    private String userId;
    private String userProfileId;
    private String languageId;
    private String projectId;
    private String flowId;
    private String flowIid;
    private String taskId;
    private String taskIid;
    private String processId;
    private String processIid;
    private String functionalityId;
    private String functionalityIid;
    private String operationId;
    private String operationIid;
    private String organizationId;
    private String rootOrganizationId;
    private final List<String> workflowProfileIds;
    private final Map<String, String> dataAccessIds;
    private final List<T8OperationAccessRights> operationAccessRights;

    public T8Context(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.operationAccessRights = new ArrayList<>();
        this.dataAccessIds = new HashMap<>();
        this.workflowProfileIds = new ArrayList<>();
        setSessionContext(sessionContext);
    }

    public T8Context(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        this.operationAccessRights = new ArrayList<>();
        this.dataAccessIds = new HashMap<>();
        this.workflowProfileIds = new ArrayList<>();
        setSessionContext(sessionContext);
    }

    public T8Context(T8Context inputContext)
    {
        this.sessionContext = inputContext.getSessionContext();
        this.serverContext = inputContext.getServerContext();
        this.clientContext = inputContext.getClientContext();
        this.operationAccessRights = new ArrayList<>();
        this.dataAccessIds = new HashMap<>();
        this.workflowProfileIds = new ArrayList<>();
        setContext(inputContext);
    }

    /**
     * Sets the access variables of this instance to those of the input context.
     * This method does not affect the T8ClientContext, T8ServerContext or T8SessionContext
     * contained by this instance.
     * @param inputContext
     */
    public void setContext(T8Context inputContext)
    {
        // Clear all collections.
        this.operationAccessRights.clear();
        this.dataAccessIds.clear();
        this.workflowProfileIds.clear();

        // Set all content to the input context's content.
        this.languageId = inputContext.getLanguageId();
        this.userId = inputContext.getUserId();
        this.userProfileId = inputContext.getUserProfileId();
        this.systemAgentId = inputContext.getSystemAgentId();
        this.systemAgentIid = inputContext.getSystemAgentIid();
        this.projectId = inputContext.getProjectId();
        this.flowId = inputContext.getFlowId();
        this.flowIid = inputContext.getFlowIid();
        this.taskId = inputContext.getTaskId();
        this.taskIid = inputContext.getTaskIid();
        this.processId = inputContext.getProcessId();
        this.processIid = inputContext.getProcessIid();
        this.functionalityId = inputContext.getFunctionalityId();
        this.functionalityIid = inputContext.getFunctionalityIid();
        this.operationId = inputContext.getOperationId();
        this.operationIid = inputContext.getOperationIid();
        this.organizationId = inputContext.getOrganizationId();
        this.rootOrganizationId = inputContext.getRootOrganizationId();
        this.workflowProfileIds.addAll(inputContext.getWorkflowProfileIds());
        this.operationAccessRights.addAll(inputContext.getOperationAccessRights());
        this.dataAccessIds.putAll(inputContext.getDataAccessIds());
        this.operationAccessRights.addAll(inputContext.getOperationAccessRights());
    }

    /**
     * Resets the context variables of this instance that can be obtained from the local
     * T8SessionContext.
     */
    public void resetSessionContext()
    {
        this.setSessionContext(sessionContext);
    }

    private void setSessionContext(T8SessionContext sessionContext)
    {
        this.sessionContext = sessionContext;
        this.sessionId = sessionContext.getSessionIdentifier();
        this.systemAgentId = sessionContext.getSystemAgentIdentifier();
        this.systemAgentIid = sessionContext.getSystemAgentInstanceIdentifier();
        this.userId = sessionContext.getUserIdentifier();
        this.userProfileId = sessionContext.getUserProfileIdentifier();
        this.organizationId = sessionContext.getOrganizationIdentifier();
        this.rootOrganizationId = sessionContext.getRootOrganizationIdentifier();
        this.languageId = sessionContext.getContentLanguageIdentifier();
        this.workflowProfileIds.clear();
        this.workflowProfileIds.addAll(sessionContext.getWorkFlowProfileIdentifiers());
    }

    public void add(T8Context inputContext)
    {
        if (inputContext.getProjectId() != null) this.projectId = inputContext.getProjectId();
        if (inputContext.getFlowId() != null) this.flowId = inputContext.getFlowId();
        if (inputContext.getFlowIid() != null) this.flowIid = inputContext.getFlowIid();
        if (inputContext.getTaskId() != null) this.taskId = inputContext.getTaskId();
        if (inputContext.getTaskIid() != null) this.taskIid = inputContext.getTaskIid();
        if (inputContext.getProcessId() != null) this.processId = inputContext.getProcessId();
        if (inputContext.getProcessIid() != null) this.processIid = inputContext.getProcessIid();
        if (inputContext.getFunctionalityId() != null) this.functionalityId = inputContext.getFunctionalityId();
        if (inputContext.getFunctionalityIid() != null) this.functionalityIid = inputContext.getFunctionalityIid();
        if (inputContext.getOperationId() != null) this.operationId = inputContext.getOperationId();
        if (inputContext.getOperationIid() != null) this.operationIid = inputContext.getOperationIid();
        if (inputContext.getOrganizationId() != null) this.organizationId = inputContext.getOrganizationId();
        if (inputContext.getRootOrganizationId() != null) this.rootOrganizationId = inputContext.getRootOrganizationId();
        this.operationAccessRights.addAll(inputContext.getOperationAccessRights());
        this.dataAccessIds.putAll(inputContext.getDataAccessIds());
    }

    public T8Context copy()
    {
        return new T8Context(this);
    }

    T8Context getParentContext()
    {
        return parentContext;
    }

    void setParentContext(T8Context context)
    {
        this.parentContext = context;
    }

    public T8Context getRootContext()
    {
        T8Context context;

        context = this;
        while (context.getParentContext() != null)
        {
            context = context.getParentContext();
        }

        return context;
    }

    public boolean isEquivalent(T8Context other)
    {
        if (!Objects.equals(this.projectId, other.projectId)) return false;
        else if (!Objects.equals(this.flowId, other.flowId)) return false;
        else if (!Objects.equals(this.flowIid, other.flowIid)) return false;
        else if (!Objects.equals(this.taskId, other.taskId)) return false;
        else if (!Objects.equals(this.taskIid, other.taskIid)) return false;
        else if (!Objects.equals(this.processId, other.processId)) return false;
        else if (!Objects.equals(this.processIid, other.processIid)) return false;
        else if (!Objects.equals(this.functionalityId, other.functionalityId)) return false;
        else if (!Objects.equals(this.functionalityIid, other.functionalityIid)) return false;
        else if (!Objects.equals(this.operationId, other.operationId)) return false;
        else if (!Objects.equals(this.operationIid, other.operationIid)) return false;
        else if (!Objects.equals(this.organizationId, other.organizationId)) return false;
        else if (!Objects.equals(this.rootOrganizationId, other.rootOrganizationId)) return false;
        else if (this.sessionContext != other.sessionContext) return false;
        else return Objects.equals(this.dataAccessIds, other.dataAccessIds);
    }

    public boolean isServer()
    {
        return this.serverContext != null;
    }

    public boolean isClient()
    {
        return this.clientContext != null;
    }

    public boolean isSystem()
    {
        return this.sessionContext.isSystemSession();
    }

    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return serverContext != null ? serverContext.getExpressionEvaluator(this) : clientContext.getExpressionEvaluator();
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public void setServerContext(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
    }

    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    public void setClientContext(T8ClientContext clientContext)
    {
        this.clientContext = clientContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public String getSessionId()
    {
        return this.sessionId;
    }

    public String getUserId()
    {
        return this.userId;
    }

    public String getUserProfileId()
    {
        return this.userProfileId;
    }

    public List<String> getWorkflowProfileIds()
    {
        return this.workflowProfileIds;
    }

    public String getLanguageId()
    {
        return this.languageId;
    }

    public String getOrganizationId()
    {
        return this.organizationId;
    }

    public T8Context setOrganizationId(String orgId)
    {
        this.organizationId = orgId;
        return this;
    }

    public String getRootOrganizationId()
    {
        return this.rootOrganizationId;
    }

    public T8Context setRootOrganizationId(String rootOrgId)
    {
        this.rootOrganizationId = rootOrgId;
        return this;
    }

    /**
     * Returns the id of the agent to which this context is applicable.
     * This method will return the user id if available or else the system agent id.
     * @return The user id if available or else the system agent id.
     */
    public String getAgentId()
    {
        if (this.userId != null) return this.userId;
        else return this.systemAgentId;
    }

    /**
     * Returns the instance id of the agent to which this context is applicable.
     * This method will return the system agent iid if available.
     * @return The system agent iid if available else null.
     */
    public String getAgentIid()
    {
        return this.systemAgentIid;
    }

    public String getSystemAgentId()
    {
        return this.systemAgentId;
    }

    public String getSystemAgentIid()
    {
        return this.systemAgentIid;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public T8Context setProjectId(String projectId)
    {
        this.projectId = projectId;
        return this;
    }

    public String getFlowId()
    {
        return flowId;
    }

    public T8Context setFlowId(String flowId)
    {
        this.flowId = flowId;
        return this;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public T8Context setFlowIid(String flowIid)
    {
        this.flowIid = flowIid;
        return this;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public T8Context setTaskId(String taskId)
    {
        this.taskId = taskId;
        return this;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public T8Context setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
        return this;
    }

    public String getProcessId()
    {
        return processId;
    }

    public T8Context setProcessId(String processId)
    {
        this.processId = processId;
        return this;
    }

    public String getProcessIid()
    {
        return processIid;
    }

    public T8Context setProcessIid(String processIid)
    {
        this.processIid = processIid;
        return this;
    }

    public String getFunctionalityId()
    {
        return functionalityId;
    }

    public T8Context setFunctionalityId(String functionalityId)
    {
        this.functionalityId = functionalityId;
        return this;
    }

    public String getFunctionalityIid()
    {
        return functionalityIid;
    }

    public T8Context setFunctionalityIid(String functionalityIid)
    {
        this.functionalityIid = functionalityIid;
        return this;
    }

    public String getDataAccessId(String drInstanceId)
    {
        String accessId;

        accessId = dataAccessIds.get(drInstanceId);
        return accessId != null ? accessId : dataAccessIds.get(null); // Return the access id if found, or else fetch and return the default id.
    }

    public Map<String, String> getDataAccessIds()
    {
        return new HashMap<>(dataAccessIds);
    }

    public T8Context setDataAccessId(String accessId)
    {
        dataAccessIds.put(null, accessId);
        return this;
    }

    public void setDataAccessId(String drInstanceId, String accessId)
    {
        dataAccessIds.put(drInstanceId, accessId);
    }

    public void setDataAcccess(T8DataAccess dataAccess)
    {
        this.dataAccessIds.clear();
        if (dataAccess != null)
        {
            this.dataAccessIds.putAll(dataAccess.getDataAccessMap());
        }
    }

    public String getOperationId()
    {
        return operationId;
    }

    public T8Context setOperationId(String operationId)
    {
        this.operationId = operationId;
        return this;
    }

    public String getOperationIid()
    {
        return operationIid;
    }

    public T8Context setOperationIid(String operationIid)
    {
        this.operationIid = operationIid;
        return this;
    }

    public List<T8OperationAccessRights> getOperationAccessRights()
    {
        return new ArrayList<>(operationAccessRights);
    }

    public T8Context setOperationAccessRights(List<T8OperationAccessRights> rights)
    {
        this.operationAccessRights.clear();
        if (rights != null)
        {
            this.operationAccessRights.addAll(rights);
        }

        return this;
    }

    public T8OperationAccessRights getOperationAccessRights(String operationId)
    {
        for (T8OperationAccessRights rights : this.operationAccessRights)
        {
            if (operationId.equals(rights.getOperationId()))
            {
                return rights;
            }
        }

        return null;
    }

    public T8Context addOperationAccessRights(T8OperationAccessRights rights)
    {
        removeOperationAccessRights(rights.getOperationId());
        this.operationAccessRights.add(rights);
        return this;
    }

    public T8OperationAccessRights removeOperationAccessRights(String operationId)
    {
        for (T8OperationAccessRights rights : this.operationAccessRights)
        {
            if (operationId.equals(rights.getOperationId()))
            {
                operationAccessRights.remove(rights);
                return rights;
            }
        }

        return null;
    }

    public void clearOperationAccessRights()
    {
        this.operationAccessRights.clear();
    }

    @Override
    public String toString()
    {
        StringBuilder builder;

        builder = new StringBuilder();
        builder.append("T8Context{");
        builder.append("projectId=");
        builder.append(projectId);
        builder.append(", operationId=");
        builder.append(operationId);
        builder.append(", operationIid=");
        builder.append(operationIid);
        builder.append("flowId=");
        builder.append(flowId);
        builder.append(", flowIid=");
        builder.append(flowIid);
        builder.append(", taskId=");
        builder.append(taskId);
        builder.append(", taskIid=");
        builder.append(taskIid);
        builder.append(", processId=");
        builder.append(processId);
        builder.append(", processIid=");
        builder.append(processIid);
        builder.append(", functionalityId=");
        builder.append(functionalityId);
        builder.append(", functionalityIid=");
        builder.append(functionalityIid);
        builder.append(", organizationId=");
        builder.append(organizationId);
        builder.append(", dataAccessIds=");
        builder.append(dataAccessIds);
        builder.append('}');
        return builder.toString();
    }

    public static class T8OperationContext extends T8Context
    {
        public T8OperationContext(T8Context parentContext, String orgId, String flowId, String flowIid)
        {
            super(parentContext);
            super.setOrganizationId(orgId);
            super.setFlowId(flowId);
            super.setFlowIid(flowIid);
        }
    }

    public static class T8WorkflowContext extends T8Context
    {
        public T8WorkflowContext(T8Context parentContext, String flowId, String flowIid)
        {
            super(parentContext);
            super.setFlowId(flowId);
            super.setFlowIid(flowIid);
        }
    }

    public static class T8WorkflowTaskContext extends T8Context
    {
        public T8WorkflowTaskContext(T8Context parentContext, String flowId, String flowIid, String taskId, String taskIid)
        {
            super(parentContext);
            super.setFlowId(flowId);
            super.setFlowIid(flowIid);
            super.setTaskId(taskId);
            super.setTaskIid(taskIid);
        }
    }

    public static class T8ProjectContext extends T8Context
    {
        public T8ProjectContext(T8Context parentContext, String projectId)
        {
            super(parentContext);
            super.setProjectId(projectId);
        }
    }

    public static class T8FunctionalityContext extends T8Context
    {
        public T8FunctionalityContext(T8Context parentContext, String projectId, String functionalityId, String functionalityIid)
        {
            super(parentContext);
            super.setProjectId(projectId);
            super.setFunctionalityId(functionalityId);
            super.setFunctionalityIid(functionalityIid);
        }

        public T8FunctionalityContext(T8Context parentContext, String projectId, T8FunctionalityAccessHandle accessHandle)
        {
            super(parentContext);
            super.setProjectId(projectId);
            super.setFunctionalityId(accessHandle.getFunctionalityId());
            super.setFunctionalityIid(accessHandle.getFunctionalityIid());
        }
    }
}
