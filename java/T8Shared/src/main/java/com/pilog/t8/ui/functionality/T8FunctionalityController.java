package com.pilog.t8.ui.functionality;

import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.ui.T8ComponentController;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityController
{
    /**
     * Returns the component controller to be used by new functionalities
     * invoked within the context of this controller.
     * @return
     */
    public T8ComponentController getFunctionalityComponentController();

    /**
     * Returns the specified functionality view from the functionality
     * controller.  If no view for the specified functionality instance exists,
     * null is returned.
     * @param functionalityIid The instance identifier of the
     * functionality for which to return the view component from the
     * functionality controller.
     * @return The view component associated with the specified functionality
     * instance.
     */
    public T8FunctionalityView getFunctionalityView(String functionalityIid);

    /**
     * Invokes the specified functionality within this context.
     * @param functionalityId The identifier of the functionality to
     * invoke.
     * @param parameters The functionality parameters used for access.
     * @return The execution handle of the executing functionality.
     * @throws java.lang.Exception
     */
    public T8FunctionalityAccessHandle accessFunctionality(String functionalityId, Map<String, Object> parameters) throws Exception;
}
