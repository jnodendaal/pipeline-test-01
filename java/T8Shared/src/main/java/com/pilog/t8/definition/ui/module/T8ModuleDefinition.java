package com.pilog.t8.definition.ui.module;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.script.T8ComponentEventHandlerScriptDefinition;
import com.pilog.t8.definition.script.T8ModuleOperationScriptDefinition;
import com.pilog.t8.definition.script.T8ModuleScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentControllerDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleDefinition extends T8ComponentDefinition implements T8ComponentControllerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MODULE";
    public static final String GROUP_NAME = "Modules";
    public static final String GROUP_DESCRIPTION = "Containers to which other UI components can be added.  Modules act as collections of UI components with a shared purpose and encapsulated functionality.";
    public static final String STORAGE_PATH = "/modules";
    public static final String DISPLAY_NAME = "Module";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  The module acts as a collection of UI components with a shared purpose and encapsulated functionality.";
    public static final String IDENTIFIER_PREFIX = "M_";
    public enum Datum
    {
        INPUT_PARAMETER_DEFINITIONS,
        VARIABLE_DEFINITIONS,
        ROOT_COMPONENT_DEFINITION,
        ADDITIONAL_COMPONENT_DEFINITIONS,
        MODULE_EVENT_DEFINITIONS,
        MODULE_OPERATION_SCRIPT_DEFINITIONS,
        MODULE_SCRIPT_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    private List<T8ComponentEventDefinition> predefinedEventDefinitions;
    private List<T8ComponentOperationDefinition> predefinedOperationDefinitions;

    public T8ModuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this module."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.VARIABLE_DEFINITIONS.toString(), "Variables",  "The variables used in this module accessible via setVar() and getVar() methods."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.ROOT_COMPONENT_DEFINITION.toString(), "Root Component", "The main content component displayed by this module."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ADDITIONAL_COMPONENT_DEFINITIONS.toString(), "Additional Components", "Additional components contained by this module that perform a support function, such as dialogs."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MODULE_EVENT_DEFINITIONS.toString(), "Module Events", "The events that may occur in this module."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MODULE_OPERATION_SCRIPT_DEFINITIONS.toString(), "Module Operations", "The operations that can be executed by this module."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MODULE_SCRIPT_DEFINITIONS.toString(), "Module Scripts", "Scripts used in this module by it's internal operations."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext context, String datumId) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(context.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.VARIABLE_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(context.getDefinitionTypeMetaData(T8ModuleVariableDefinition.TYPE_IDENTIFIER));
        else if (Datum.ROOT_COMPONENT_DEFINITION.toString().equals(datumId)) return createDefinitionOptions(context.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.ADDITIONAL_COMPONENT_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(context.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.MODULE_EVENT_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(context.getDefinitionTypeMetaData(T8ModuleEventDefinition.TYPE_IDENTIFIER));
        else if (Datum.MODULE_OPERATION_SCRIPT_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(context.getDefinitionTypeMetaData(T8ModuleOperationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.MODULE_SCRIPT_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(context.getDefinitionTypeMetaData(T8ModuleScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(context, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext context, String datumId)
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, context, this, getDatumType(datumId));
        }
        else if (Datum.VARIABLE_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, context, this, getDatumType(datumId));
        }
        else return null;
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8ModuleTestHarness", new Class<?>[]{});
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (getRootComponentDefinition()== null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.ROOT_COMPONENT_DEFINITION.toString(), "No Root Component Definition set.", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.module.T8Module", new Class<?>[]{T8ComponentController.class, T8ModuleDefinition.class}, controller, this);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        ArrayList<T8DataValueDefinition> definitions;

        definitions = new ArrayList<>();
        return definitions;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> definitions;

        // Get the pre-defined module event definitions.
        definitions = new ArrayList<>();

        definitions.addAll(getDefaultModuleEventDefinitions());

        // Add all user-defined module events.
        definitions.addAll(convertModuleEventDefinitions());
        return definitions;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> definitions;

        // Get the pre-defined module operation definitions.
        definitions = new ArrayList<>();
        definitions.addAll(getDefaultModuleOperationefinitions());

        // Add the user-defined module operations.
        definitions.addAll(convertModuleOperationDefinitions());
        return definitions;
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }

    public ArrayList<T8ModuleVariableDefinition> getVariableDefinitions()
    {
        return getDefinitionDatum(Datum.VARIABLE_DEFINITIONS);
    }

    public void setVariableDefinitions(List<T8ModuleVariableDefinition> variableDefinitions)
    {
        setDefinitionDatum(Datum.VARIABLE_DEFINITIONS, variableDefinitions);
    }

    public T8ComponentDefinition getRootComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.ROOT_COMPONENT_DEFINITION.toString());
    }

    public void setRootComponentDefinition(T8ComponentDefinition rootComponentDefinition)
    {
         setDefinitionDatum(Datum.ROOT_COMPONENT_DEFINITION.toString(), rootComponentDefinition);
    }

    public ArrayList<T8ComponentDefinition> getAdditionalComponentDefinitions()
    {
        return (ArrayList<T8ComponentDefinition>)getDefinitionDatum(Datum.ADDITIONAL_COMPONENT_DEFINITIONS.toString());
    }

    public void setAdditionalComponentDefinitions(ArrayList<T8ComponentDefinition> componentDefinitions)
    {
        setDefinitionDatum(Datum.ADDITIONAL_COMPONENT_DEFINITIONS.toString(), componentDefinitions);
    }

    public ArrayList<T8ComponentEventDefinition> getDefaultModuleEventDefinitions()
    {
        if (predefinedEventDefinitions == null)  predefinedEventDefinitions = T8ModuleAPIHandler.getEventDefinitions(this);
        return new ArrayList<>(predefinedEventDefinitions);
    }

    public ArrayList<T8ComponentOperationDefinition> getDefaultModuleOperationefinitions()
    {
        if (predefinedOperationDefinitions == null)  predefinedOperationDefinitions = T8ModuleAPIHandler.getOperationDefinitions(this);
        return new ArrayList<>(predefinedOperationDefinitions);
    }

    public T8ModuleEventDefinition getModuleEventDefinition(String identifier)
    {
        return (T8ModuleEventDefinition)getSubDefinition(identifier);
    }

    public ArrayList<T8ModuleEventDefinition> getModuleEventDefinitions()
    {
        return (ArrayList<T8ModuleEventDefinition>)getDefinitionDatum(Datum.MODULE_EVENT_DEFINITIONS.toString());
    }

    public void setModuleEventDefinitions(ArrayList<T8ModuleEventDefinition> eventDefinitions)
    {
        setDefinitionDatum(Datum.MODULE_EVENT_DEFINITIONS.toString(), eventDefinitions);
    }

    public T8ModuleOperationScriptDefinition getModuleOperationScriptDefinition(String identifier)
    {
        return (T8ModuleOperationScriptDefinition)getSubDefinition(identifier);
    }

    public ArrayList<T8ModuleOperationScriptDefinition> getModuleOperationScriptDefinitions()
    {
        return (ArrayList<T8ModuleOperationScriptDefinition>)getDefinitionDatum(Datum.MODULE_OPERATION_SCRIPT_DEFINITIONS.toString());
    }

    public void setModuleOperationDefinitions(ArrayList<T8ModuleOperationScriptDefinition> scriptDefinitions)
    {
        setDefinitionDatum(Datum.MODULE_OPERATION_SCRIPT_DEFINITIONS.toString(), scriptDefinitions);
    }

    public ArrayList<T8ModuleScriptDefinition> getModuleScriptDefinitions()
    {
        return (ArrayList<T8ModuleScriptDefinition>)getDefinitionDatum(Datum.MODULE_SCRIPT_DEFINITIONS.toString());
    }

    public void setModuleScriptDefinitions(ArrayList<T8ModuleScriptDefinition> scriptDefinitions)
    {
        setDefinitionDatum(Datum.MODULE_SCRIPT_DEFINITIONS.toString(), scriptDefinitions);
    }

    public T8ModuleScriptDefinition getModuleScriptDefinition(String identifier)
    {
        for (T8ModuleScriptDefinition scriptDefinition : getModuleScriptDefinitions())
        {
            if (scriptDefinition.getIdentifier().equals(identifier))
            {
                return scriptDefinition;
            }
        }

        return null;
    }

    public void addEventHandlerScriptDefinitions(ArrayList<T8ComponentEventHandlerScriptDefinition> definitions)
    {
        if (definitions != null)
        {
            for (T8ComponentEventHandlerScriptDefinition definition : definitions)
            {
                addSubDefinition(T8ComponentDefinition.Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString(), definition);
            }
        }
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        // The module very specifically does not return any child component
        // defintions, because the entire module is considered a single
        // component and its internal components are managed internally.
        return null;
    }

    /**
     * Converts all module event definitions to the more generic component event
     * type.
     *
     * @return A list of all module events converted to component events.
     */
    private ArrayList<T8ComponentEventDefinition> convertModuleEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> eventDefinitions;
        List<T8ModuleEventDefinition> moduleEventDefinitions;

        eventDefinitions = new ArrayList<>();
        moduleEventDefinitions = getModuleEventDefinitions();

        if (moduleEventDefinitions != null)
        {
            for (T8ModuleEventDefinition moduleEventDefinition : moduleEventDefinitions)
            {
                T8ComponentEventDefinition eventCopy;

                // Copy the event definition.
                eventCopy = new T8ComponentEventDefinition(moduleEventDefinition.getIdentifier());
                eventCopy.setMetaDisplayName(moduleEventDefinition.getMetaDisplayName());
                eventCopy.setMetaDescription(moduleEventDefinition.getMetaDescription());
                T8DefinitionUtilities.assignDefinitionParent(this, eventCopy);

                // Copy all event parameters to the new event.
                for (T8DataParameterDefinition eventParameterDefinition : moduleEventDefinition.getParameterDefinitions())
                {
                    T8DataParameterResourceDefinition parameterCopy;

                    parameterCopy = new T8DataParameterResourceDefinition(eventParameterDefinition.getIdentifier());
                    parameterCopy.setMetaDisplayName(eventParameterDefinition.getMetaDisplayName());
                    parameterCopy.setMetaDescription(eventParameterDefinition.getMetaDescription());
                    parameterCopy.setDataType(eventParameterDefinition.getDataType());
                    eventCopy.addParameterDefinition(parameterCopy);
                }

                // Add the copied event to the list that will be returned.
                eventDefinitions.add(eventCopy);
            }
        }

        return eventDefinitions;
    }

    /**
     * Converts all module operation script definitions to the more generic
     * component operation type.
     *
     * @return A list of all module operations converted to component
     * operations.
     */
    public ArrayList<T8ComponentOperationDefinition> convertModuleOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operationDefinitions;
        List<T8ModuleOperationScriptDefinition> moduleOperationDefinitions;

        operationDefinitions = new ArrayList<>();
        moduleOperationDefinitions = getModuleOperationScriptDefinitions();

        if (moduleOperationDefinitions != null)
        {
            for (T8ModuleOperationScriptDefinition operationScriptDefinition : moduleOperationDefinitions)
            {
                T8ComponentOperationDefinition operationCopy;

                // Copy the operation definition.
                operationCopy = new T8ComponentOperationDefinition(operationScriptDefinition.getIdentifier());
                operationCopy.setMetaDisplayName(operationScriptDefinition.getMetaDisplayName());
                operationCopy.setMetaDescription(operationScriptDefinition.getMetaDescription());
                T8DefinitionUtilities.assignDefinitionParent(this, operationCopy);

                // Copy all operation input parameters to the new operation.
                for (T8DataParameterDefinition parameterDefinition : operationScriptDefinition.getInputParameterDefinitions())
                {
                    T8DataParameterResourceDefinition parameterCopy;

                    parameterCopy = new T8DataParameterResourceDefinition(parameterDefinition.getIdentifier());
                    parameterCopy.setMetaDisplayName(parameterDefinition.getMetaDisplayName());
                    parameterCopy.setMetaDescription(parameterDefinition.getMetaDescription());
                    parameterCopy.setDataType(parameterDefinition.getDataType());
                    operationCopy.addInputParameterDefinition(parameterCopy);
                }

                // Copy all operation output parameters to the new operation.
                for (T8DataParameterDefinition parameterDefinition : operationScriptDefinition.getOutputParameterDefinitions())
                {
                    T8DataParameterResourceDefinition parameterCopy;

                    parameterCopy = new T8DataParameterResourceDefinition(parameterDefinition.getIdentifier());
                    parameterCopy.setMetaDisplayName(parameterDefinition.getMetaDisplayName());
                    parameterCopy.setMetaDescription(parameterDefinition.getMetaDescription());
                    parameterCopy.setDataType(parameterDefinition.getDataType());
                    operationCopy.addOutputParameterDefinition(parameterCopy);
                }

                // Add the copied operation definition to the list that will be returned.
                operationDefinitions.add(operationCopy);
            }
        }

        return operationDefinitions;
    }
}
