package com.pilog.t8.definition.notification;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationGenerator;
import com.pilog.t8.ui.notification.T8NotificationDisplayComponent;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * @author Gavin Boshoff
 */
public class T8ProcessNotificationDefinition extends T8NotificationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String IDENTIFIER = "@NOTIF_PROCESS";
    public static final String TYPE_IDENTIFIER = "@DT_NOTIFICATION_PROCESS_RESOURCE";
    public static final String DISPLAY_NAME = "Process Notification Generator";
    public static final String DESCRIPTION = "A definition of a notification generator for Process messages.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    // Notification input parameters.
    public static final String PARAMETER_PROCESS_ID = "$P_PROCESS_ID";
    public static final String PARAMETER_PROCESS_IID = "$P_PROCESS_IID";
    public static final String PARAMETER_PROCESS_DISPLAY_NAME = "$P_NAME";
    public static final String PARAMETER_PROCESS_DESCRIPTION = "$P_DESCRIPTION";
    public static final String PARAMETER_PROCESS_INITIATOR_ID = "$P_INITIATOR_ID";
    public static final String PARAMETER_PROCESS_INITIATOR_DISPLAY_NAME = "$P_INITIATOR_DISPLAY_NAME";

    public T8ProcessNotificationDefinition(String identifier)
    {
        super(IDENTIFIER);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "The instance identifier of the process to which the notification refers.", T8DataType.GUID));
        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getNotificationParameterDefinitions()
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_IID, "Process Instance Identifier", "The instance identifier of the process to which the notification refers.", T8DataType.GUID));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_ID, "Process Identifier", "The identifier for the process to which the notification refers.", T8DataType.DEFINITION_IDENTIFIER));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_DISPLAY_NAME, "Process Name", "The the display name of the process.", T8DataType.DISPLAY_STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_DESCRIPTION, "Process Description", "The description of the process.", T8DataType.DISPLAY_STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_INITIATOR_ID, "Initiator Identifier", "The identifier of the agent that initiated the process.", T8DataType.DEFINITION_IDENTIFIER));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_PROCESS_INITIATOR_DISPLAY_NAME, "Initiator Display Name", "The display name of the agent that initiated the process.", T8DataType.DISPLAY_STRING));
        return parameterList;
    }

    @Override
    public String getNotificationDisplayName()
    {
        return "Process";
    }

    @Override
    public Icon getNotificationIcon()
    {
        return new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/gearIcon.png"));
    }

    @Override
    public T8NotificationGenerator getNotificationGeneratorInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.notification.T8ProcessNotificationGenerator", new Class<?>[]{T8Context.class, T8ProcessNotificationDefinition.class}, context, this);
    }

    @Override
    public T8NotificationDisplayComponent getDisplayComponentInstance(T8Context context, T8Notification notification)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.notification.T8ProcessNotificationDisplayComponent", new Class<?>[]{T8Context.class, T8ProcessNotificationDefinition.class, T8Notification.class}, context, this, notification);
    }
}