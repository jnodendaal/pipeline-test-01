package com.pilog.t8.ui.functionality.event;

import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityExecutionUpdatedEvent extends EventObject
{
    private final T8FunctionalityAccessHandle executionHandle;
    
    public T8FunctionalityExecutionUpdatedEvent(T8FunctionalityAccessHandle executionHandle)
    {
        super(executionHandle);
        this.executionHandle = executionHandle;
    }

    public T8FunctionalityAccessHandle getFunctionalityExecutionHandle()
    {
        return executionHandle;
    }
}
