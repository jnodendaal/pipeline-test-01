package com.pilog.t8.definition.data.source.sql;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8SQLBasedGroupByDataSourceDefinition extends T8DataSourceDefinition implements T8DataConnectionBasedSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_SQL_GROUP_BY";
    public static final String DISPLAY_NAME = "SQL Group By Data Source";
    public static final String DESCRIPTION = "A data source that performs group by operations on a sql based data source.";
    public enum Datum
    {
        ORIGINATING_DATA_SOURCE_DEFINITION_IDENTIFIER,
        GROUP_BY_FIELD_IDENTIFIERS
    };
    // -------- Definition Meta-Data -------- //

    private T8DataConnectionBasedSourceDefinition wrappedConnectionBasedSourceDefinition;

    public T8SQLBasedGroupByDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction accessProvider)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.source.sql.T8SQLBasedGroupByDataSource", new Class<?>[]{T8SQLBasedGroupByDataSourceDefinition.class, T8DataTransaction.class}, this, accessProvider);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ORIGINATING_DATA_SOURCE_DEFINITION_IDENTIFIER.toString(), "Originating Data Source", "The data source on which the group by will be performed."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.DEFINITION_IDENTIFIER), Datum.GROUP_BY_FIELD_IDENTIFIERS.toString(), "Group By Fields", "The Field definitions that will be used to group by."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ORIGINATING_DATA_SOURCE_DEFINITION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8DefinitionMetaData> validDefinitions = new ArrayList<>();
            List<T8Definition> rawDefinitions = definitionContext.getRawGroupDefinitions(getRootProjectId(), T8DataSourceDefinition.GROUP_IDENTIFIER);
            for (T8Definition t8Definition : rawDefinitions)
            {
                if (t8Definition instanceof T8DataConnectionBasedSourceDefinition && t8Definition instanceof T8SQLQueryDataSourceDefinition)
                    validDefinitions.add(t8Definition.getMetaData());
            }
            return createIdentifierOptions(validDefinitions);
        }
        else if (Datum.GROUP_BY_FIELD_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            if (!Strings.isNullOrEmpty(getOriginatingDataSourceIdentifier()))
            {
                return createStringOptions(((T8DataSourceDefinition)definitionContext.getRawDefinition(getRootProjectId(), getOriginatingDataSourceIdentifier())).getFieldIdentifiers());
            }
            else return new ArrayList<>();
        }
        else if (T8DataSourceDefinition.Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataSourceFieldDefinition.GROUP_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
        wrappedConnectionBasedSourceDefinition = (T8DataConnectionBasedSourceDefinition)context.getServerContext().getDefinitionManager().getRawDefinition(context, getRootProjectId(), getOriginatingDataSourceIdentifier());

        // Validates the definition for completeness.
        validateDefinition();
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        //TODO This should be temporary until someone fixed the table datum editor to adapt to the extra datum of the aggregate field definition
        return null;
    }

    private void validateDefinition() throws Exception
    {
        if (Strings.isNullOrEmpty(getOriginatingDataSourceIdentifier()))
        {
            throw new Exception("Group by identifier list may not be empty.");
        }

        if (Strings.isNullOrEmpty(getOriginatingDataSourceIdentifier()))
        {
            throw new Exception("The originating data source identifier may not be null.");
        }
    }

    /**
     * Sets the original {@code T8DataConnectionBasedSourceDefinition}. This is used when the Data Source only exists on
     * the {@code T8DataTransaction} and not on the {@code T8DefinitionManager}.
     *
     * @param wrappedConnectionBasedSourceDefinition The {@code T8DataConnectionBasedSourceDefinition} to be used by this definition.
     */
    public void setOriginatingDataSourceDefinition(T8DataConnectionBasedSourceDefinition wrappedConnectionBasedSourceDefinition)
    {
        this.wrappedConnectionBasedSourceDefinition = wrappedConnectionBasedSourceDefinition;
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return wrappedConnectionBasedSourceDefinition.getDataConnectionIdentifier();
    }

    public String getOriginatingDataSourceIdentifier()
    {
        return getDefinitionDatum(Datum.ORIGINATING_DATA_SOURCE_DEFINITION_IDENTIFIER);
    }

    public void setOriginatingDataSourceIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.ORIGINATING_DATA_SOURCE_DEFINITION_IDENTIFIER, connectionIdentifier);
    }

    public List<String> getGroupByFieldIdentifiers()
    {
        return getDefinitionDatum(Datum.GROUP_BY_FIELD_IDENTIFIERS);
    }

    public void setGroupByFieldIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.GROUP_BY_FIELD_IDENTIFIERS, identifiers);
    }

    public void addGroupByFieldIdentifier(String identifier)
    {
        List<String> identifiers = getGroupByFieldIdentifiers();

        if (identifiers == null) identifiers = new ArrayList<>();
        identifiers.remove(identifier);
        identifiers.add(identifier);
        setGroupByFieldIdentifiers(identifiers);
    }

    public void removeGroupByFieldIdentifier(String identifier)
    {
        List<String> identifiers = getGroupByFieldIdentifiers();
        if (identifiers != null)
        {
            identifiers.remove(identifier);
            setGroupByFieldIdentifiers(identifiers);
        }
    }
}
