package com.pilog.t8.definition.operation.java;

import com.pilog.t8.T8ClientOperation;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8JavaClientOperationDefinition extends T8ClientOperationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_CLIENT_OPERATION_JAVA";
    public static final String DISPLAY_NAME = "Java Client Operation";
    public static final String DESCRIPTION = "A client-side pre-compiled Java operation.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {CLASS_NAME};
    // -------- Definition Meta-Data -------- //

    public T8JavaClientOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_NAME.toString(), "Class Name", "The class name to use when instantiating this an operation of this type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ClientOperation getNewOperationInstance(T8ComponentController controller, String operationIid) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName(getClassName()).getConstructor(T8ComponentController.class, T8JavaClientOperationDefinition.class, String.class);
        return (T8ClientOperation)constructor.newInstance(controller, this, operationIid);
    }

    public String getClassName()
    {
        return (String)getDefinitionDatum(Datum.CLASS_NAME.toString());
    }

    public void setClassName(String propertyValue)
    {
        setDefinitionDatum(Datum.CLASS_NAME.toString(), propertyValue);
    }

}
