package com.pilog.t8.definition.file.context;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8RemoteFileContextDefinition extends T8FileContextDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FILE_CONTEXT_T8_REMOTE";
    public static final String DISPLAY_NAME = "Remote T8 File Context";
    public static final String DESCRIPTION = "A file context on a remote T8 Server.";
    public enum Datum {REMOTE_SERVER_CONNECTION_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8RemoteFileContextDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString(), "Remote Server Connection Identifier", "The remote server connection that will be used to retrieve files from."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ConnectionDefinition.GROUP_IDENTIFIER));
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8FileContext createNewFileContextInstance(T8Context context, T8FileManager fileManager, String contextInstanceID) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.file.context.T8RemoteFileContext").getConstructor(T8Context.class, T8FileManager.class, T8RemoteFileContextDefinition.class, String.class);
        return (T8FileContext)constructor.newInstance(context, fileManager, this, contextInstanceID);
    }

    public String getRemoteServerConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString());
    }

    public void setRemoteServerConnectionIdentifier(String remoteServerConnectionIdentifier)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString(), remoteServerConnectionIdentifier);
    }
}
