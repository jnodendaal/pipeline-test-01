package com.pilog.t8.ui.functionality.event;

import com.pilog.t8.functionality.T8FunctionalityInstance;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityInstanceEndedEvent extends EventObject
{
    private final T8FunctionalityInstance functionalityInstance;
    
    public T8FunctionalityInstanceEndedEvent(T8FunctionalityInstance functionalityInstance)
    {
        super(functionalityInstance);
        this.functionalityInstance = functionalityInstance;
    }

    public T8FunctionalityInstance getFunctionalityInstance()
    {
        return functionalityInstance;
    }
}
