/**
 * Created on Jun 23, 2017, 7:58:56 AM
 */
package com.pilog.t8.definition.system.property;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8ListPropertyDefinition extends T8SystemPropertyDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_PROPERTY_LIST_STR";
    public static final String DISPLAY_NAME = "List (String) Type System Property";
    public static final String DESCRIPTION = "A System Property with a List (String) data type.";
    public static final String VERSION = "0";
    public enum Datum {PROPERTY_VALUE};
    // -------- Definition Meta-Data -------- //

    public T8ListPropertyDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.PROPERTY_VALUE.toString(), "Property Value", "The List value of the property, containing String values for list items."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<String> getPropertyValue()
    {
        return getDefinitionDatum(Datum.PROPERTY_VALUE);
    }

    public void setPropertyValue(List<String> value)
    {
        setDefinitionDatum(Datum.PROPERTY_VALUE, value);
    }
}