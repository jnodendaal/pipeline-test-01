package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey.WaitKeyIdentifier;
import com.pilog.t8.flow.key.T8NodeCompletionExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowNodeCompletedEvent extends T8FlowEvent
{
    private final Map<String, Object> outputParameters;
    private final T8Flow flow;

    public static final String TYPE_ID = "NODE_COMPLETED";

    public T8FlowNodeCompletedEvent(T8Flow flow, T8FlowNode node, Map<String, Object> outputParameters)
    {
        super(node);
        this.flow = flow;
        this.outputParameters = outputParameters;
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public T8Flow getFlow()
    {
        return flow;
    }

    public T8FlowNode getNode()
    {
        return (T8FlowNode)source;
    }

    public Map<String, Object> getOutputParameters()
    {
        return outputParameters;
    }

    @Override
    public T8FlowExecutionKey getExecutionKey(T8WorkFlowNodeDefinition waitingNodeDefinition, T8FlowNodeState waitingNodeState)
    {
        // Check that the flow IID's match.
        if (waitingNodeState.getFlowIid().equals(flow.getInstanceIdentifier()))
        {
            // Check that the node ID's match.
            if (waitingNodeState.getNodeId().equals(getNode().getState().getNodeId()))
            {
                return new T8NodeCompletionExecutionKey(waitingNodeState, getNode().getState().getNodeId());
            }
            else return null;
        }
        else return null;
    }

    @Override
    public T8DataFilterCriteria getWaitKeyFilterCriteria()
    {
        Map<String, Object> filterMap;

        filterMap = new HashMap<String, Object>();
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_ID", WaitKeyIdentifier.NODE_COMPLETION.toString());
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_FLOW_IID", flow.getInstanceIdentifier());
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_NODE_ID", getNode().getState().getNodeId());
        return new T8DataFilterCriteria(filterMap);
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
