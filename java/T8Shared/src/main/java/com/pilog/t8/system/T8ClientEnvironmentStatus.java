package com.pilog.t8.system;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ClientEnvironmentStatus implements Serializable
{
    private int networkLatency;
    private int maximumConcurrency;
    private double serverLoad;
    private double serverActivity;
    private double networkThroughput;

    public T8ClientEnvironmentStatus()
    {
    }

    public int getNetworkLatency()
    {
        return networkLatency;
    }

    public void setNetworkLatency(int networkLatency)
    {
        this.networkLatency = networkLatency;
    }

    public double getNetworkThroughput()
    {
        return networkThroughput;
    }

    public void setNetworkThroughput(double networkThroughput)
    {
        this.networkThroughput = networkThroughput;
    }

    public double getServerLoad()
    {
        return serverLoad;
    }

    public void setServerLoad(double serverLoad)
    {
        this.serverLoad = serverLoad;
    }

    public double getServerActivity()
    {
        return serverActivity;
    }

    public void setServerActivity(double serverActivity)
    {
        this.serverActivity = serverActivity;
    }

    public int getMaximumConcurrency()
    {
        return maximumConcurrency;
    }

    public void setMaximumConcurrency(int maximumConcurrency)
    {
        this.maximumConcurrency = maximumConcurrency;
    }
}
