package com.pilog.t8.definition.patch;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTypePatch implements Serializable
{
    private String typeIdentifier;
    private String newTypeIdentifier;
    private Map<String, String> datumIdentifierMap;
    
    public T8DefinitionTypePatch(String typeIdentifier, String newTypeIdentifier, Map<String, String> datumIdentifierMap)
    {
        this.typeIdentifier = typeIdentifier;
        this.newTypeIdentifier = newTypeIdentifier;
        this.datumIdentifierMap = datumIdentifierMap;
    }

    public String getTypeIdentifier()
    {
        return typeIdentifier;
    }

    public void setTypeIdentifier(String typeIdentifier)
    {
        this.typeIdentifier = typeIdentifier;
    }

    public String getNewTypeIdentifier()
    {
        return newTypeIdentifier;
    }

    public void setNewTypeIdentifier(String newTypeIdentifier)
    {
        this.newTypeIdentifier = newTypeIdentifier;
    }

    public Map<String, String> getDatumIdentifierMap()
    {
        return datumIdentifierMap;
    }

    public void setDatumIdentifierMap(Map<String, String> datumIdentifierMap)
    {
        this.datumIdentifierMap = datumIdentifierMap;
    }
}
