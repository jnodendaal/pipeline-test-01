package com.pilog.t8.notification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationSummary implements Serializable
{
    private final Map<String, T8NotificationTypeSummary> notificationTypeSummaries;

    public T8NotificationSummary()
    {
        notificationTypeSummaries = new LinkedHashMap<>();
    }

    public List<T8NotificationTypeSummary> getNotificationTypeSummaries()
    {
        return new ArrayList<>(notificationTypeSummaries.values());
    }

    public T8NotificationTypeSummary getNotificationTypeSummary(String notificationIdentifier)
    {
        return notificationTypeSummaries.get(notificationIdentifier);
    }

    public void putNotificationTypeSummary(T8NotificationTypeSummary summary)
    {
        notificationTypeSummaries.put(summary.getNotificationIdentifier(), summary);
    }

    public int getNotificationCount(T8NotificationFilter notificationFilter)
    {
        int count;

        count = 0;
        for (T8NotificationTypeSummary typeSummary : notificationTypeSummaries.values())
        {
            if (notificationFilter.isNotificationTypeIncluded(typeSummary.getNotificationIdentifier()))
            {
                if (notificationFilter.isIncludeNewNotifications())
                {
                    count += typeSummary.getNewCount();
                }

                if (notificationFilter.isIncludeReceivedNotifications())
                {
                    count += (typeSummary.getTotalCount() - typeSummary.getNewCount());
                }
            }
        }

        return count;
    }

    public int getNewNotificationCount()
    {
        return notificationTypeSummaries.values().stream().map((typeSummary) -> typeSummary.getNewCount()).reduce(0, Integer::sum);
    }

    public int getTotalNotificationCount()
    {
        return notificationTypeSummaries.values().stream().map((typeSummary) -> typeSummary.getTotalCount()).reduce(0, Integer::sum);
    }
}
