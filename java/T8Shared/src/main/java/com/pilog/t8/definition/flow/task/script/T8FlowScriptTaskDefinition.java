package com.pilog.t8.definition.flow.task.script;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.script.T8FlowTaskScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FlowScriptTaskDefinition extends T8FlowTaskDefinition
{
    private enum Datum {SCRIPT_DEFINITION};

    public T8FlowScriptTaskDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SCRIPT_DEFINITION.toString(), "Script", "The script to execute."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FlowTaskScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8FlowTaskScriptDefinition getScriptDefinition()
    {
        return getDefinitionDatum(Datum.SCRIPT_DEFINITION);
    }

    public void setScriptDefinition(T8FlowTaskScriptDefinition definition)
    {
        setDefinitionDatum(Datum.SCRIPT_DEFINITION, definition);
    }

    @Override
    public TaskExecutionType getExecutionType()
    {
        return TaskExecutionType.INTERNAL;
    }

    @Override
    public T8FlowTask getNewTaskInstance(T8TaskDetails taskDetails) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.flow.task.script.T8FlowScriptTask", new Class<?>[]{T8FlowScriptTaskDefinition.class, T8TaskDetails.class}, this, taskDetails);
    }
}
