package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefinitionEventHandlerDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_EVENT_HANDLER";
    public static final String GROUP_NAME = "Definition Event Handlers";
    public static final String GROUP_DESCRIPTION = "Event handlers that react and process event that occur on definitions in the system.";
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_EVENT_HANDLER";
    public static final String IDENTIFIER_PREFIX = "DEF_EH_";
    public static final String STORAGE_PATH = "/definition_event_handlers";
    public static final String DISPLAY_NAME = "T8 Definition Event Handler";
    public static final String DESCRIPTION = "A definition that defines a handler that will react to specific definition events.";
    public enum Datum {DEFINITION_TYPE_PATCH_DEFINITIONS,
                       DEFINITION_STRING_PATCH_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionEventHandlerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DefinitionEventHandler getNewEventHandlerInstance(T8Context context) throws Exception;
}

