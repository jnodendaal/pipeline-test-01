package com.pilog.t8.state;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8StateParameterCollection
{
    public T8StateParameterMap createNewParameterMap(String mapIdentifier, Map<String, Object> key, T8StateDataSet dataSet);
    public T8StateParameterList createNewParameterList(String listIdentifier, Map<String, Object> key, T8StateDataSet dataSet);
}
