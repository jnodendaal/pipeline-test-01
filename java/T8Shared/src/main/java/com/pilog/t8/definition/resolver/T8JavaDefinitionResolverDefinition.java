package com.pilog.t8.definition.resolver;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionResolver;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * This class defines the definition of a specific operation type.
 *
 * @author Bouwer du Preez
 */
public class T8JavaDefinitionResolverDefinition extends T8DefinitionResolverDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_RESOLVER_JAVA";
    public static final String DISPLAY_NAME = "Java Definition Resolver";
    public static final String DESCRIPTION = "A server-side pre-compiled Java definition resolver.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {CLASS_NAME};
    // -------- Definition Meta-Data -------- //

    public T8JavaDefinitionResolverDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_NAME.toString(), "Class Name", "The class name to use when instantiating a resolver of this type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionResolver getNewDefinitionResolverInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName(getClassName()).getConstructor(T8Context.class, this.getClass());
            return (T8DefinitionResolver)constructor.newInstance(context, this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getClassName()
    {
        return (String)getDefinitionDatum(Datum.CLASS_NAME.toString());
    }

    public void setClassName(String propertyValue)
    {
        setDefinitionDatum(Datum.CLASS_NAME.toString(), propertyValue);
    }
}

