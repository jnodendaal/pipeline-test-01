package com.pilog.t8.definition.script;

import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultServerContextScriptDefinition extends T8DefaultScriptDefinition implements T8ServerContextScriptDefinition
{
    public T8DefaultServerContextScriptDefinition(String id)
    {
        super(id);
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.script.T8DefaultServerContextScript", new Class<?>[]{T8Context.class, T8ServerContextScriptDefinition.class}, context, this);
    }
}
