package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SubFlowActivityDefinition extends T8FlowActivityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_ACTIVITY_SUB_FLOW";
    public static final String DISPLAY_NAME = "Sub-Flow Activity";
    public static final String DESCRIPTION = "An activity that executes a sub-flow.";
    private enum Datum {SUB_FLOW_IDENTIFIER,
                        SUB_FLOW_INPUT_PARAMETER_MAPPING,
                        SUB_FLOW_OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public static final String NODE_STATE_SUB_FLOW_INSTANCE_IDENTIFIER = "$SUB_FLOW_INSTANCE_IDENTIFIER";

    public T8SubFlowActivityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SUB_FLOW_IDENTIFIER.toString(), "Sub Flow", "The identifier of the sub-flow to execute."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.SUB_FLOW_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow to Sub-Flow", "A mapping of Flow Parameters to the corresponding Sub-Flow Input Parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.SUB_FLOW_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Sub-Flow to Flow", "A mapping of Sub-Flow Output Parameters to the corresponding Flow Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SUB_FLOW_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowDefinition.GROUP_IDENTIFIER));
        else if (Datum.SUB_FLOW_INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String subFlowId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            subFlowId = getSubFlowIdentifier();
            if (subFlowId != null)
            {
                T8WorkFlowDefinition subFlowDefinition;
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
                subFlowDefinition = (T8WorkFlowDefinition)definitionContext.getRawDefinition(getRootProjectId(), subFlowId);
                if (subFlowDefinition != null)
                {
                    List<T8DataParameterDefinition> subFlowParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    subFlowParameterDefinitions = subFlowDefinition.getFlowParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((subFlowParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition subFlowParameterDefinition : subFlowParameterDefinitions)
                            {
                                identifierList.add(subFlowParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(flowParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.SUB_FLOW_OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String subFlowId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            subFlowId = getSubFlowIdentifier();
            if (subFlowId != null)
            {
                T8WorkFlowDefinition subFlowDefinition;
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
                subFlowDefinition = (T8WorkFlowDefinition)definitionContext.getRawDefinition(getRootProjectId(), subFlowId);
                if (subFlowDefinition != null)
                {
                    List<T8DataParameterDefinition> subFlowParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    subFlowParameterDefinitions = subFlowDefinition.getFlowParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((subFlowParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition subFlowParameterDefinition : subFlowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(subFlowParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.activity.T8SubFlowActivityNode").getConstructor(T8Context.class, T8Flow.class, T8SubFlowActivityDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    @Override
    public String getProfileIdentifier()
    {
        return null;
    }

    public String getSubFlowIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SUB_FLOW_IDENTIFIER.toString());
    }

    public void setSubFlowIdentifier(String taskIdentifier)
    {
        setDefinitionDatum(Datum.SUB_FLOW_IDENTIFIER.toString(), taskIdentifier);
    }

    public Map<String, String> getSubFlowInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.SUB_FLOW_INPUT_PARAMETER_MAPPING.toString());
    }

    public void setSubFlowInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.SUB_FLOW_INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getSubFlowOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.SUB_FLOW_OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setSubFlowOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.SUB_FLOW_OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
