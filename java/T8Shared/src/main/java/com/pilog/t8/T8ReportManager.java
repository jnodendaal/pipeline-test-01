package com.pilog.t8;

import com.pilog.t8.report.T8ReportDetails;
import com.pilog.t8.report.T8ReportFilter;
import com.pilog.t8.report.T8ReportSummary;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ReportManager extends T8ServerModule
{
    public T8ReportDetails getReportDetails(T8Context context, String reportIid) throws Exception;
    public T8ReportSummary getReportSummary(T8Context context) throws Exception;
    public int countReports(T8Context context, T8ReportFilter reportFilter, String searchExpression) throws Exception;
    public List<T8ReportDetails> searchReports(T8Context context, T8ReportFilter reportFilter, String searchExpression, int pageOffset, int pageSize) throws Exception;
    public boolean deleteReport(T8Context context, String reportIid) throws Exception;
    public String generateReport(T8Context context, String reportId, Map<String, Object> inputParameters) throws Exception;
}
