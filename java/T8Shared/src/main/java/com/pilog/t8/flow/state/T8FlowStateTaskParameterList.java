package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateTaskParameterList extends T8FlowStateParameterList
{
    protected final T8FlowTaskState parentTaskState;

    public T8FlowStateTaskParameterList(T8FlowTaskState parentTaskState, T8FlowStateParameterCollection parentCollection, String parameterIID, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        super(parentCollection, parentTaskState.getFlowIid(), parameterIID, entityIdentifier, null);
        this.parentTaskState = parentTaskState;
        if (dataSet != null) addParameters(dataSet);
    }

    @Override
    public synchronized T8DataEntity createEntity(int index, T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;

        newEntity = super.createEntity(index, entityDefinition);
        newEntity.setFieldValue(entityIdentifier + "$TASK_IID", parentTaskState.getTaskIid());
        return newEntity;
    }

    @Override
    public synchronized T8FlowStateParameterMap createNewParameterMap(T8FlowStateParameterCollection newMapParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        return new T8FlowStateTaskParameterMap(parentTaskState, newMapParentCollection, parameterIID, entityIdentifier, dataSet);
    }

    @Override
    public synchronized T8FlowStateParameterList createNewParameterList(T8FlowStateParameterCollection newListParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        return new T8FlowStateTaskParameterList(parentTaskState, newListParentCollection, parameterIID, entityIdentifier, dataSet);
    }
}
