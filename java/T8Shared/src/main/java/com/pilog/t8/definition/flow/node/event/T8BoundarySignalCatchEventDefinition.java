package com.pilog.t8.definition.flow.node.event;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.definition.flow.T8FlowBoundaryNodeDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This node catches a specific flow signal only if the activity that it is
 * attached to is active when the event is triggered.
 *
 * @author Bouwer du Preez
 */
public class T8BoundarySignalCatchEventDefinition extends T8SignalCatchEventDefinition implements T8FlowBoundaryNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_EVENT_BOUNDARY_SIGNAL_CATCH";
    public static final String DISPLAY_NAME = "Flow Boundary Signal Catch Event";
    public static final String DESCRIPTION = "An event that catches a specific workflow signal if the activity it is attached to is active when the signal is triggered.";
    private enum Datum {ATTACHED_TO_NODE_IDENTIFIER, CANCEL_ACTIVITY};
    // -------- Definition Meta-Data -------- //

    public T8BoundarySignalCatchEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ATTACHED_TO_NODE_IDENTIFIER.toString(), "Activity", "The identifier of the activity to which this event is attached."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CANCEL_ACTIVITY.toString(), "Cancel Activity", "Denotes whether the Activity to which this node is attached should be cancelled or not when the event is caught."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ATTACHED_TO_NODE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8WorkFlowDefinition flowDefinition;

            flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
            if (flowDefinition != null)
            {
                List<String> nodeIdentifiers;

                nodeIdentifiers = flowDefinition.getActivityNodeIdentifiers();
                return createStringOptions(nodeIdentifiers);
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.event.T8BoundarySignalCatchEventNode").getConstructor(T8Context.class, T8Flow.class, T8BoundarySignalCatchEventDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    @Override
    public String getAttachedToNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ATTACHED_TO_NODE_IDENTIFIER.toString());
    }

    public void setAttachedToNodeIdentifier(String nodeIdentifier)
    {
        setDefinitionDatum(Datum.ATTACHED_TO_NODE_IDENTIFIER.toString(), nodeIdentifier);
    }

    @Override
    public boolean isCancelActivity()
    {
        Boolean cancel;

        cancel = (Boolean)getDefinitionDatum(Datum.CANCEL_ACTIVITY.toString());
        return cancel != null && cancel;
    }

    public void setCancelActivity(boolean cancel)
    {
        setDefinitionDatum(Datum.CANCEL_ACTIVITY.toString(), cancel);
    }
}
