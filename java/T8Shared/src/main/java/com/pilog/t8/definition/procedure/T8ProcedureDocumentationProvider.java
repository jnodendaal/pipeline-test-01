package com.pilog.t8.definition.procedure;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.documentation.T8DefaultDefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationType;
import com.pilog.t8.definition.data.T8DataValueDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8ProcedureDocumentationProvider extends T8DefaultDefinitionDocumentationProvider
{
    public static final String PROCEDURE_API_DOCUMENTATION_TYPE_IDENTIFIER = "PROCEDURE_API";

    public T8ProcedureDocumentationProvider(T8DefinitionContext definitionContext)
    {
        super(definitionContext);
        documentationTypes.put(PROCEDURE_API_DOCUMENTATION_TYPE_IDENTIFIER, new T8DefinitionDocumentationType(PROCEDURE_API_DOCUMENTATION_TYPE_IDENTIFIER, "Procedure API", "Documentation describing the API of the T8 Server Operation."));
    }

    @Override
    public StringBuffer getHTMLDocumentation(T8Definition definition, String typeIdentifier)
    {
        if (PROCEDURE_API_DOCUMENTATION_TYPE_IDENTIFIER.equals(typeIdentifier))
        {
            return createProcedureAPIDocumentationString(definitionContext, (T8ProcedureDefinition)definition);
        }
        else return super.getHTMLDocumentation(definition, typeIdentifier);
    }

    protected StringBuffer createProcedureAPIDocumentationString(T8DefinitionContext definitionContext, T8ProcedureDefinition procedureDefinition)
    {
        StringBuffer documentation;

        documentation = new StringBuffer();
        documentation.append("<h1>Procedure API: " + procedureDefinition.getIdentifier() + "</h1>");
        documentation.append("</br>");
        documentation.append(createDefinitionDetailsString(definitionContext, procedureDefinition));
        documentation.append("</br>");
        documentation.append(createOperationDocumentationString(definitionContext, procedureDefinition));
        return documentation;
    }

    protected StringBuffer createOperationDocumentationString(T8DefinitionContext definitionContext, T8ProcedureDefinition procedureDefinition)
    {
        StringBuffer documentation;

        documentation = new StringBuffer();
        documentation.append("<h2>Procedure Details</h2>");
        documentation.append("<table border=\"1\">");
        documentation.append("<tr><td>Operation Identifier</td><td>" + procedureDefinition.getIdentifier() + "</td></tr>");
        documentation.append("<tr><td>Operation Name</td><td>" + procedureDefinition.getMetaDisplayName() + "</td></tr>");
        documentation.append("<tr><td>Description</td><td>" + procedureDefinition.getMetaDescription() + "</td></tr>");

        // Add the input parameter section.
        documentation.append("<tr>");
        documentation.append("<td>Input Parameters</td>");
        documentation.append("<td>");
        documentation.append("<ul>");
        for (T8DataValueDefinition parameterDefinition : procedureDefinition.getInputParameterDefinitions())
        {
            documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
        }
        documentation.append("</ul>");
        documentation.append("</td>");
        documentation.append("</tr>");

        // Add the output parameter section.
        documentation.append("<tr>");
        documentation.append("<td>Output Parameters</td>");
        documentation.append("<td>");
        documentation.append("<ul>");
        for (T8DataValueDefinition parameterDefinition : procedureDefinition.getOutputParameterDefinitions())
        {
            documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
        }
        documentation.append("</ul>");
        documentation.append("</td>");
        documentation.append("</tr>");
        documentation.append("</table>");
        documentation.append("</br>");

        return documentation;
    }
}
