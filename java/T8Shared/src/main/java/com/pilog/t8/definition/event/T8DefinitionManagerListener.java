package com.pilog.t8.definition.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionManagerListener extends EventListener
{
    public void definitionImported(T8DefinitionImportedEvent event);
    public void definitionSaved(T8DefinitionSavedEvent event);
    public void definitionDeleted(T8DefinitionDeletedEvent event);
    public void definitionLocked(T8DefinitionLockedEvent event);
    public void definitionUnlocked(T8DefinitionUnlockedEvent event);
    public void definitionRenamed(T8DefinitionRenamedEvent event);
    public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent event);
}
