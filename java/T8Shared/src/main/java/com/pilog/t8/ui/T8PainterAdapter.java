package com.pilog.t8.ui;

import java.awt.Graphics2D;
import org.jdesktop.swingx.painter.Painter;

/**
 * This class is a simple adapter that allows a T8Painter to be used in all
 * instances where a regular Painter object is required.  Having this adapter
 * also reduces the dependency on the SwingX specific interface Painter within
 * the T8Shared project.
 * 
 * @author Bouwer du Preez
 */
public class T8PainterAdapter implements Painter
{
    private T8Painter painter;
    
    public T8PainterAdapter(T8Painter painter)
    {
        this.painter = painter;
    }
    
    @Override
    public void paint(Graphics2D graphicsContext, Object object, int width, int height)
    {
        painter.paint(graphicsContext, object, width, height);
    }
}
