package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtPassword extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@PASSWORD";

    public T8DtPassword()
    {
    }

    public T8DtPassword(T8DefinitionManager context)
    {
    }

    public T8DtPassword(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return char[].class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonArray jsonArray;
            char[] charArray;

            charArray = (char[])object;
            jsonArray = new JsonArray();
            for (int i = 0; i < charArray.length; i++)
            {
                jsonArray.add(JsonValue.valueOf(charArray[i]));
            }

            return jsonArray;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                JsonArray jsonArray;
                char[] charArray;

                jsonArray = jsonValue.asArray();
                charArray = new char[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++)
                {
                    JsonValue value;

                    value = jsonArray.get(i);
                    charArray[i] = value != null ? value.asString().charAt(0) : null;
                }

                return charArray;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deserializing char array from JSON.", e);
            }
        }
        else return null;
    }
}
