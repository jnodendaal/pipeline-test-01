package com.pilog.t8.definition.user.password;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8PasswordPolicyDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_PASSWORD_POLICY";
    public static final String GROUP_IDENTIFIER = "@DG_PASSWORD_POLICY";
    public static final String GROUP_NAME = "Password Policies";
    public static final String GROUP_DESCRIPTION = "Policies defining the rules that are applied to user passwords such as the format of permissable passwords, validity periods etc.";
    public static final String STORAGE_PATH = "/password_policies";
    public static final String DISPLAY_NAME = "Password Policy";
    public static final String DESCRIPTION = "A policy that defines various password related configurations such as the rules to be applied when validating password formats.";
    public static final String IDENTIFIER_PREFIX = "PASSWORD_POLICY_";
    public enum Datum {PASSWORD_FORMAT_RULE_DEFINITIONS,
                       PASSWORD_VALIDITY_TIME,
                       PASSWORD_RETRIES_ALLOWED};
    // -------- Definition Meta-Data -------- //

    public T8PasswordPolicyDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PASSWORD_FORMAT_RULE_DEFINITIONS.toString(), "Password Rules", "Rules that will be applied when validating passwords."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER ,Datum.PASSWORD_VALIDITY_TIME.toString(), "Password Validity Time", "The amount of days before the password will be deemed invalid. -1 can be used to deactivate.", 30));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER ,Datum.PASSWORD_RETRIES_ALLOWED.toString(), "Password Retries Allowed", "The amount of times the user is allowed to incorrectly enter its password before the user definition will be locked and revent any additional log in's.", 10));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PASSWORD_FORMAT_RULE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PasswordPolicyRuleDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public void setPasswordFormatRuleDefinitions(List<T8PasswordPolicyRuleDefinition> definitions)
    {
        setDefinitionDatum(Datum.PASSWORD_FORMAT_RULE_DEFINITIONS, definitions);
    }

    public List<T8PasswordPolicyRuleDefinition> getPasswordFormatRuleDefinitions()
    {
        return getDefinitionDatum(Datum.PASSWORD_FORMAT_RULE_DEFINITIONS);
    }

    public void setPasswordValidityTime(Integer days)
    {
        setDefinitionDatum(Datum.PASSWORD_VALIDITY_TIME, days);
    }

    public Integer getPasswordValidityTime()
    {
        return getDefinitionDatum(Datum.PASSWORD_VALIDITY_TIME);
    }

    public void setPasswordRetriesAllowed(Integer retries)
    {
        setDefinitionDatum(Datum.PASSWORD_RETRIES_ALLOWED, retries);
    }

    public Integer getPasswordRetriesAllowed()
    {
        return getDefinitionDatum(Datum.PASSWORD_RETRIES_ALLOWED);
    }
}
