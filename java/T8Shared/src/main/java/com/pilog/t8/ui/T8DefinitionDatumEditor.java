package com.pilog.t8.ui;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionDatumEditor
{
    public T8Context getContext();
    public T8ClientContext getClientContext();
    public T8DefinitionContext getDefinitionContext();
    public T8SessionContext getSessionContext();
    public String getDatumIdentifier();
    public T8DefinitionDatumType getDatumType();
    public T8Definition getDefinition();

    /**
     * This method is called from a non-EDT thread and must perform all heavy
     * initialization of the editor before it can be used.
     */
    public void initializeComponent();

    /**
     * This method is called once the editor has been initialized and added to
     * the UI.  This method must not perform heavy operations that should rather
     * be performed in the initialize method.
     *
     * When this method is called, the latest datum value is read from the
     * underlying definition end updated on the datum editor.
     */
    public void startComponent();

    /**
     * Stops the component, committing all outstanding changes and releasing all
     * acquired resources.
     */
    public void stopComponent();

    /**
     * When this method is called, the latest changes on the editor are
     * committed to the underlying definition.
     */
    public void commitChanges();

    /**
     * When this method is called, the latest datum value is read from the
     * underlying definition end updated on the datum editor.
     */
    public void refreshEditor();

    public void setEditable(boolean editable);
    public void addDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener);
    public void removeDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener);
}
