package com.pilog.t8.http.api.rest;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8UriParseResult
{
    private final Map<String, Object> parameters;
    private final Map<String, String> operationInputParameterMapping;
    private final Map<String, String> operationOutputParameterMapping;
    private String operationId;
    private boolean success;

    public T8UriParseResult()
    {
        this.parameters = new HashMap<>();
        this.operationInputParameterMapping = new HashMap<>();
        this.operationOutputParameterMapping = new HashMap<>();
    }

    public T8UriParseResult(T8UriParseResult input)
    {
        this();
        if (input != null) T8UriParseResult.this.add(input);
    }

    public void add(T8UriParseResult result)
    {
        this.success = result.isSuccess();
        this.operationId = result.getOperationId();
        this.parameters.putAll(result.getParameters());
        this.operationInputParameterMapping.clear();
        this.operationInputParameterMapping.putAll(result.getOperationInputParameterMapping());
        this.operationOutputParameterMapping.clear();
        this.operationOutputParameterMapping.putAll(result.getOperationOutputParameterMapping());
    }

    public void setParameter(String parameterId, Object value)
    {
        this.parameters.put(parameterId, value);
    }

    public Map<String, Object> getParameters()
    {
        return new HashMap<>(parameters);
    }

    public Map<String, String> getOperationInputParameterMapping()
    {
        return new HashMap<>(operationInputParameterMapping);
    }

    public void setOperationInputParameterMapping(Map<String, String> mapping)
    {
        this.operationInputParameterMapping.clear();
        if (mapping != null)
        {
            this.operationInputParameterMapping.putAll(mapping);
        }
    }

    public Map<String, String> getOperationOutputParameterMapping()
    {
        return new HashMap<>(operationOutputParameterMapping);
    }

    public void setOperationOutputParameterMapping(Map<String, String> mapping)
    {
        this.operationOutputParameterMapping.clear();
        if (mapping != null)
        {
            this.operationOutputParameterMapping.putAll(mapping);
        }
    }

    public String getOperationId()
    {
        return operationId;
    }

    public void setOperationId(String id)
    {
        this.operationId = id;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }
}
