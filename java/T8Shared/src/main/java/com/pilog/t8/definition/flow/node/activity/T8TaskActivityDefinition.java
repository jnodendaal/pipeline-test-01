package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8FlowTaskGroupDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowTaskDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskActivityDefinition extends T8FlowActivityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_ACTIVITY_TASK";
    public static final String DISPLAY_NAME = "Task Activity";
    public static final String DESCRIPTION = "An activity that executes a task.";
    private enum Datum
    {
        TASK_IDENTIFIER,
        TASK_GROUP_ID,
        TASK_GROUP_RESET,
        SUBSEQUENT_FOLLOW_UP,
        USER_RESTRICTION_PARAMETER_IDENTIFIER,
        USER_ID_OUTPUT_PARAMETER_IDENTIFIER,
        PROFILE_ID_OUTPUT_PARAMETER_IDENTIFIER,
        TASK_INPUT_PARAMETER_MAPPING,
        TASK_OUTPUT_PARAMETER_MAPPING,
        TASK_INPUT_PARAMETER_ASSIGNMENT_DEFINITIONS,
        FLOW_PARAMETER_ASSIGNMENT_MAP,
        TASK_PRIORITY_PARAMETER_IDENTIFIER,
        TASK_ISSUED_COMMUNICATION_DEFINITION,
        TASK_ISSUED_COMMUNICATION_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public static final String NODE_STATE_TASK_INSTANCE_IDENTIFIER = "$TASK_INSTANCE_IDENTIFIER";
    public static final String NODE_STATE_TASK_CLAIMED_BY_USER_IDENTIFIER = "$TASK_CLAIMED_BY_USER_IDENTIFIER";
    public static final String NODE_STATE_TASK_ISSUED_TIME = "$TASK_ISSUED_TIME";
    public static final String NODE_STATE_TASK_CLAIMED_TIME = "$TASK_CLAIMED_TIME";

    public T8TaskActivityDefinition(String identifier)
    {
        super(identifier);
    }

    // TEMPORARY HACK TO FIX EXISTING FLOWS.
    @Override
    public void constructDefinition(T8ServerContext serverContext) throws Exception
    {
        T8TaskActivityCommunicationDefinition existingComm;

        existingComm = getTaskIssuedCommunicationDefinition();
        if (existingComm != null)
        {
            List<T8TaskActivityCommunicationDefinition> comms;

            comms = getTaskIssuedCommunicationDefinitions();
            comms.add(existingComm);
            setTaskIssuedCommunicationDefinitions(comms);
            setTaskIssuedCommunicationDefinition(null);
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_IDENTIFIER.toString(), "Task", "The identifier of the task to execute."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_GROUP_ID.toString(), "Task Group", "The group to which the task spawned by this node will be assigned for reporting and escalation purposes."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TASK_GROUP_RESET.toString(), "Task Group Reset", "A flag to indicate that the group to which this task belongs must be reset i.e. the next iteration of the group must start when this node is reached.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SUBSEQUENT_FOLLOW_UP.toString(), "Subsequent Follow-up", "A flag to indicate whether a subsequent task is a direct follow-up on this task i.e. will be performed by the same user in a sequence of steps.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.USER_RESTRICTION_PARAMETER_IDENTIFIER.toString(), "User Restriction Parameter", "The flow parameter to use as a user restriction identifier for this activity (if applicable)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.USER_ID_OUTPUT_PARAMETER_IDENTIFIER.toString(), "User ID Output Parameter", "The flow parameter to which the ID of the user that completes this task will be stored."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROFILE_ID_OUTPUT_PARAMETER_IDENTIFIER.toString(), "Workflow Profile ID Output Parameter", "The flow parameter to which the ID of the profile of the user that completes this task will be stored."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_PRIORITY_PARAMETER_IDENTIFIER.toString(), "Task Priority Parameter", "The flow parameter to use as the initialization priority for this activity (if applicable)."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.TASK_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow to Task Input", "A mapping of Flow Parameters to the corresponding Task Input Parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.TASK_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Task Output to Flow", "A mapping of Task Output Parameters to the corresponding Flow Parameters."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TASK_INPUT_PARAMETER_ASSIGNMENT_DEFINITIONS.toString(), "Task Input Parameter Assignments", "Assignment/evaluation of specific values to be used as task input parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.FLOW_PARAMETER_ASSIGNMENT_MAP.toString(), "Flow Parameter Assignments", "Assignment/evaluation of specific values to be used as activity output parameters."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.TASK_ISSUED_COMMUNICATION_DEFINITION.toString(), "Task Issued Communication",  "The communication to send when a task is issued."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString(), "Task Issued Communications",  "The communications to send when a task is issued."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TASK_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowTaskDefinition.GROUP_IDENTIFIER));
        else if (Datum.TASK_GROUP_ID.toString().equals(datumIdentifier)) return T8DefinitionUtilities.createLocalIdOptionsFromDefinitions(getLocalDefinitionsOfGroup(T8FlowTaskGroupDefinition.GROUP_IDENTIFIER), true, "No Group");
        else if (Datum.TASK_INPUT_PARAMETER_ASSIGNMENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TaskParameterAssignmentDefinition.TYPE_IDENTIFIER));
        else if (Datum.TASK_ISSUED_COMMUNICATION_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TaskActivityCommunicationDefinition.GROUP_IDENTIFIER));
        else if (Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TaskActivityCommunicationDefinition.GROUP_IDENTIFIER));
        else if (Datum.USER_RESTRICTION_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return getFlowParametersAsDatumOptions("No Restriction");
        }
        else if (Datum.TASK_PRIORITY_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return getFlowParametersAsDatumOptions("No Preset");
        }
        else if ((Datum.USER_ID_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) || (Datum.PROFILE_ID_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)))
        {
            return getFlowParametersAsDatumOptions("No Output");
        }
        else if (Datum.TASK_INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String taskId;

            optionList = new ArrayList<>();

            taskId = getTaskIdentifier();
            if (taskId != null)
            {
                T8FlowTaskDefinition taskDefinition;
                T8WorkFlowDefinition flowDefinition;

                taskDefinition = (T8FlowTaskDefinition)definitionContext.getRawDefinition(getRootProjectId(), taskId);
                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();

                if ((taskDefinition != null) && (flowDefinition != null))
                {
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    List<T8DataParameterDefinition> taskParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();
                    taskParameterDefinitions = taskDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<>();
                    if ((flowParameterDefinitions != null) && (taskParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataParameterDefinition taskParameterDefinition : taskParameterDefinitions)
                            {
                                identifierList.add(taskParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(flowParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.TASK_OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String taskId;

            optionList = new ArrayList<>();

            taskId = getTaskIdentifier();
            if (taskId != null)
            {
                T8FlowTaskDefinition taskDefinition;
                T8WorkFlowDefinition flowDefinition;

                taskDefinition = (T8FlowTaskDefinition)definitionContext.getRawDefinition(getRootProjectId(), taskId);
                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();

                if ((taskDefinition != null) && (flowDefinition != null))
                {
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    List<T8DataParameterDefinition> taskParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();
                    taskParameterDefinitions = taskDefinition.getOutputParameterDefinitions();

                    identifierMap = new HashMap<>();
                    if ((flowParameterDefinitions != null) && (taskParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition taskParameterDefinition : taskParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(taskParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.FLOW_PARAMETER_ASSIGNMENT_MAP.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8WorkFlowDefinition flowDefinition;

            optionList = new ArrayList<>();

            flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
            if (flowDefinition != null)
            {
                List<T8DataParameterDefinition> flowParameterDefinitions;
                HashMap<String, List<String>> identifierMap;

                flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                identifierMap = new HashMap<>();
                if (flowParameterDefinitions != null)
                {
                    for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                    {
                        identifierMap.put(flowParameterDefinition.getIdentifier(), null);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<>();
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    private ArrayList<T8DefinitionDatumOption> getFlowParametersAsDatumOptions(String defaultOption)
    {
        ArrayList<T8DefinitionDatumOption> datumOptions;
        T8WorkFlowDefinition flowDefinition;

        datumOptions = new ArrayList<>();
        datumOptions.add(new T8DefinitionDatumOption(defaultOption, null));
        flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
        if (flowDefinition != null)
        {
            List<T8DataParameterDefinition> flowParameterDefinitions;

            flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();
            if (flowParameterDefinitions != null)
            {
                flowParameterDefinitions.stream().forEach((flowParameterDefinition) ->
                {
                    datumOptions.add(new T8DefinitionDatumOption(flowParameterDefinition.getIdentifier(), flowParameterDefinition.getIdentifier()));
                });
            }
        }

        return datumOptions;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.TASK_INPUT_PARAMETER_ASSIGNMENT_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.activity.T8TaskActivityNode").getConstructor(T8Context.class, T8Flow.class, T8TaskActivityDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    public String getTaskIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_IDENTIFIER.toString());
    }

    public void setTaskIdentifier(String taskId)
    {
        setDefinitionDatum(Datum.TASK_IDENTIFIER.toString(), taskId);
    }

    public String getTaskGroupId()
    {
        return getDefinitionDatum(Datum.TASK_GROUP_ID);
    }

    public void setTaskGroupId(String groupId)
    {
        setDefinitionDatum(Datum.TASK_GROUP_ID, groupId);
    }

    public boolean isTaskGroupReset()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.TASK_GROUP_RESET);
        return value != null && value;
    }

    public void setTaskGroupReset(boolean reset)
    {
        setDefinitionDatum(Datum.TASK_GROUP_RESET, reset);
    }

    public boolean isSubsequentFollowUp()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.SUBSEQUENT_FOLLOW_UP.toString());
        return value != null ? value : false;
    }

    public void setSubsequentFollowUp(boolean followUp)
    {
        setDefinitionDatum(Datum.SUBSEQUENT_FOLLOW_UP.toString(), followUp);
    }

    public String getUserRestrictionParameterId()
    {
        return (String)getDefinitionDatum(Datum.USER_RESTRICTION_PARAMETER_IDENTIFIER.toString());
    }

    public void setUserRestrictionParameterId(String id)
    {
        setDefinitionDatum(Datum.USER_RESTRICTION_PARAMETER_IDENTIFIER.toString(), id);
    }

    public String getUserIdOutputParameterId()
    {
       return (String)getDefinitionDatum(Datum.USER_ID_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setUserIdOutputParameterId(String id)
    {
        setDefinitionDatum(Datum.USER_ID_OUTPUT_PARAMETER_IDENTIFIER.toString(), id);
    }

    public String getProfileIdOutputParameterId()
    {
        return (String)getDefinitionDatum(Datum.PROFILE_ID_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setProfileIdOutputParameterId(String id)
    {
        setDefinitionDatum(Datum.PROFILE_ID_OUTPUT_PARAMETER_IDENTIFIER.toString(), id);
    }

    public Map<String, String> getTaskInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.TASK_INPUT_PARAMETER_MAPPING.toString());
    }

    public void setTaskInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.TASK_INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getTaskOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.TASK_OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setTaskOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.TASK_OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public List<T8TaskParameterAssignmentDefinition> getTaskInputParameterAssignmentDefinitions()
    {
        return (List<T8TaskParameterAssignmentDefinition>)getDefinitionDatum(Datum.TASK_INPUT_PARAMETER_ASSIGNMENT_DEFINITIONS.toString());
    }

    public void setTaskInputParameterAssignmentDefinitions(List<T8TaskParameterAssignmentDefinition> definitions)
    {
        setDefinitionDatum(Datum.TASK_INPUT_PARAMETER_ASSIGNMENT_DEFINITIONS.toString(), definitions);
    }

    public Map<String, String> getFlowParameterAssignmentMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.FLOW_PARAMETER_ASSIGNMENT_MAP.toString());
    }

    public void setFlowParameterAssignmentMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.FLOW_PARAMETER_ASSIGNMENT_MAP.toString(), expressionMap);
    }

    public String getTaskPriorityParameterId()
    {
        return getDefinitionDatum(Datum.TASK_PRIORITY_PARAMETER_IDENTIFIER);
    }

    public void setTaskPriorityParameterId(String identifier)
    {
        setDefinitionDatum(Datum.TASK_PRIORITY_PARAMETER_IDENTIFIER, identifier);
    }

    private T8TaskActivityCommunicationDefinition getTaskIssuedCommunicationDefinition()
    {
        return (T8TaskActivityCommunicationDefinition)getDefinitionDatum(Datum.TASK_ISSUED_COMMUNICATION_DEFINITION.toString());
    }

    private void setTaskIssuedCommunicationDefinition(T8TaskActivityCommunicationDefinition definition)
    {
        setDefinitionDatum(Datum.TASK_ISSUED_COMMUNICATION_DEFINITION.toString(), definition);
    }

    public List<T8TaskActivityCommunicationDefinition> getTaskIssuedCommunicationDefinitions()
    {
        return (List<T8TaskActivityCommunicationDefinition>)getDefinitionDatum(Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString());
    }

    public void setTaskIssuedCommunicationDefinitions(List<T8TaskActivityCommunicationDefinition> definitions)
    {
        setDefinitionDatum(Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString(), definitions);
    }
}
