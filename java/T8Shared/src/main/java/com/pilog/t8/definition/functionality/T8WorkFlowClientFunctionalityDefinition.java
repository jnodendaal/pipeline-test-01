package com.pilog.t8.definition.functionality;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowClientFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_WORKFLOW_CLIENT";
    public static final String DISPLAY_NAME = "Workflow Client Functionality";
    public static final String DESCRIPTION = "A T8 functionality that opens user tasks in a specific workflow on the UI.";
    public static final String IDENTIFIER_PREFIX = "FNC_WORKFLOW_CLIENT_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowClientFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8WorkFlowClientFunctionalityInstance").getConstructor(T8Context.class, T8WorkFlowClientFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8WorkFlowClientFunctionalityInstance").getConstructor(T8Context.class, T8WorkFlowClientFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }
}
