package com.pilog.t8.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordCache
{
    /**
     * Returns the TransactionManger responsible for transactions involving this terminology cache.
     * This method currently returns an object only to avoid having to include J2EE libraries in this project.
     * In future this interface will be moved and will return the property type: javax.transaction.TransactionManager
     * @return javax.transaction.TransactionManager responsible for transactions on this cache.
     */
    public Object getTransactionManager();

    public void put(RecordData record);
    public void remove(RecordData record);
    public RecordData remove(String recordId);
    public RecordData get(String recordId);
    public boolean containsKey(String recordId);

    public class RecordData implements Serializable
    {
        private final List<Map<String, Object>> documentData;
        private final List<Map<String, Object>> propertyData;
        private final List<Map<String, Object>> valueData;
        private final List<Map<String, Object>> attributeData;
        private final List<Map<String, Object>> attachmentData;
        private String recordId;

        public RecordData(String recordId, List<Map<String, Object>> documentData, List<Map<String, Object>> propertyData, List<Map<String, Object>> valueData, List<Map<String, Object>> attributeData, List<Map<String, Object>> attachmentData)
        {
            this.recordId = recordId;
            this.documentData = new ArrayList<>();
            this.propertyData = new ArrayList<>();
            this.valueData = new ArrayList<>();
            this.attributeData = new ArrayList<>();
            this.attachmentData = new ArrayList<>();
            setDocumentData(documentData);
            setPropertyData(propertyData);
            setValueData(valueData);
            setAttributeData(attributeData);
            setAttachmentData(attachmentData);
        }

        public String getRecordId()
        {
            return recordId;
        }

        public void setRecordId(String recordId)
        {
            this.recordId = recordId;
        }

        public List<Map<String, Object>> getDocumentData()
        {
            return documentData;
        }

        public void setDocumentData(List<Map<String, Object>> documentData)
        {
            this.documentData.clear();
            if (documentData != null) this.documentData.addAll(documentData);
        }

        public List<Map<String, Object>> getPropertyData()
        {
            return propertyData;
        }

        public void setPropertyData(List<Map<String, Object>> propertyData)
        {
            this.propertyData.clear();
            if (propertyData != null) this.propertyData.addAll(propertyData);
        }

        public List<Map<String, Object>> getValueData()
        {
            return valueData;
        }

        public void setValueData(List<Map<String, Object>> valueData)
        {
            this.valueData.clear();
            if (valueData != null) this.valueData.addAll(valueData);
        }

        public List<Map<String, Object>> getAttributeData()
        {
            return attributeData;
        }

        public void setAttributeData(List<Map<String, Object>> attributeData)
        {
            this.attributeData.clear();
            if (attributeData != null) this.attributeData.addAll(attributeData);
        }

        public List<Map<String, Object>> getAttachmentData()
        {
            return attachmentData;
        }

        public void setAttachmentData(List<Map<String, Object>> attachmentData)
        {
            this.attachmentData.clear();
            if (attachmentData != null) this.attachmentData.addAll(attachmentData);
        }

        public void deleteAttributeData(List<String> attributeIds)
        {
            Iterator<Map<String, Object>> rowIterator;

            rowIterator = this.attributeData.iterator();
            while (rowIterator.hasNext())
            {
                Map<String, Object> nextRow;

                nextRow = rowIterator.next();
                if (attributeIds.contains((String)nextRow.get("ATTRIBUTE_ID")))
                {
                    rowIterator.remove();
                }
            }
        }

        public boolean updateAttributeValue(String attributeId, String type, String value)
        {
            Iterator<Map<String, Object>> rowIterator;

            rowIterator = this.attributeData.iterator();
            while (rowIterator.hasNext())
            {
                Map<String, Object> nextRow;

                nextRow = rowIterator.next();
                if (attributeId.equals((String)nextRow.get("ATTRIBUTE_ID")))
                {
                    nextRow.put("TYPE", type);
                    nextRow.put("VALUE", value);
                    return true;
                }
            }

            // Not found.
            return false;
        }

        public void insertAttributeValue(String rootRecordId, String recordId, String attributeId, String type, String value)
        {
            Map<String, Object> attributeRow;

            attributeRow = new HashMap<>();
            attributeRow.put("ROOT_RECORD_ID", rootRecordId);
            attributeRow.put("RECORD_ID", recordId);
            attributeRow.put("ATTRIBUTE_ID", attributeId);
            attributeRow.put("TYPE", type);
            attributeRow.put("VALUE", value);
            this.attributeData.add(attributeRow);
        }
    }
}
