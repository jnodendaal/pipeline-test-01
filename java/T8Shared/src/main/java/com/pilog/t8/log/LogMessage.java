/**
 * Created on 12 Aug 2015, 11:47:33 AM
 */
package com.pilog.t8.log;

import java.util.function.Supplier;

/**
 * This {@code FunctionalInterface} allows for the evaluation of messages to
 * be printed iff the current logging level is set for the message to be
 * printed. Up to the point where the logging level has not been evaluated, the
 * message hasn't been either.<br/>
 * <br/>
 * This is useful when the message includes concatenation, or method calls to
 * build the message, as these actions will not happen unless the message is in
 * actual fact being output to the log files.
 *
 * @author Gavin Boshoff
 */
@FunctionalInterface
@SuppressWarnings("MarkerInterface")
public interface LogMessage extends Supplier<String> {}