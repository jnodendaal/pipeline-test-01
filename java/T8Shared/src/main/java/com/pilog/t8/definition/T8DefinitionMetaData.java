package com.pilog.t8.definition;

import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8Definition.T8DefinitionStatus;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionMetaData implements Serializable
{
    private String id;
    private String typeId;
    private String groupId;
    private String displayName;
    private String description;
    private String versionId;
    private String projectId;
    private String createdUserId;
    private Long createdTime;
    private String updatedUserId;
    private Long updatedTime;
    private String checksum;
    private Long revision;
    private boolean publicAccess; // Indicates the access of this definition.
    private boolean patched; // Indicates whether or not this definition has been patched.
    private boolean patchedContent; // Indicates whether or not this definition or one of its descendants have been patched.
    private T8DefinitionLevel definitionLevel;
    private T8DefinitionStatus status;
    private List<String> versionContributors;
    private final List<T8DefinitionComment> comments;

    public T8DefinitionMetaData()
    {
        this.comments = new ArrayList<>();
    }

    public T8DefinitionHandle getDefinitionHandle()
    {
        T8DefinitionHandle handle;

        handle = new T8DefinitionHandle(definitionLevel, id, projectId);
        handle.setGroupIdentifier(groupId);
        handle.setTypeIdentifier(typeId);
        return handle;
    }

    public boolean matchesDefinitionHandle(T8DefinitionHandle definitionHandle)
    {
        if (definitionHandle != null)
        {
            return ((Objects.equals(id, definitionHandle.getDefinitionIdentifier())) && (Objects.equals(projectId, definitionHandle.getProjectIdentifier())));
        }
        else return false;
    }

    public boolean isPublic()
    {
        return publicAccess;
    }

    public void setPublic(boolean publicAccess)
    {
        this.publicAccess = publicAccess;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTypeId()
    {
        return typeId;
    }

    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getVersionId()
    {
        return versionId;
    }

    public void setVersionId(String versionId)
    {
        this.versionId = versionId;
    }

    public String getCreatedUserId()
    {
        return createdUserId;
    }

    public void setCreatedUserId(String userId)
    {
        if ((userId != null) && (userId.trim().length() == 0)) createdUserId = null;
        else this.createdUserId = userId;
    }

    public Long getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getUpdatedUserId()
    {
        return updatedUserId;
    }

    public void setUpdatedUserId(String userId)
    {
        if ((userId != null) && (userId.trim().length() == 0)) updatedUserId = null;
        else this.updatedUserId = userId;
    }

    public Long getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(Long lastUpdateTime)
    {
        this.updatedTime = lastUpdateTime;
    }

    public String getChecksum()
    {
        return checksum;
    }

    public void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }

    public T8DefinitionStatus getStatus()
    {
        return status;
    }

    public void setStatus(T8DefinitionStatus status)
    {
        this.status = status;
    }

    public Long getRevision()
    {
        return revision;
    }

    public void setRevision(Long revision)
    {
        this.revision = revision;
    }

    public T8DefinitionLevel getDefinitionLevel()
    {
        return definitionLevel;
    }

    public void setDefinitionLevel(T8DefinitionLevel definitionLevel)
    {
        this.definitionLevel = definitionLevel;
    }

    public boolean isPatched()
    {
        return patched;
    }

    public void setPatched(boolean patched)
    {
        this.patched = patched;
    }

    public boolean isPatchedContent()
    {
        return patchedContent;
    }

    public void setPatchedContent(boolean patchedContent)
    {
        this.patchedContent = patchedContent;
    }

    public List<T8DefinitionComment> getComments()
    {
        return new ArrayList<>(comments);
    }

    public void setComments(List<T8DefinitionComment> comments)
    {
        this.comments.clear();
        if (comments != null)
        {
            this.comments.addAll(comments);
        }
    }

    public void addComment(T8DefinitionComment comment)
    {
        this.comments.add(comment);
    }

    @Override
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();
        buffer.append("[Definition=");
        buffer.append(id);
        buffer.append(", Display Name=");
        buffer.append(displayName);
        buffer.append(", Checksum=");
        buffer.append(checksum);
        buffer.append(", Created Time=");
        buffer.append(createdTime);
        buffer.append(", Updated Time=");
        buffer.append(updatedTime);
        buffer.append("]");

        return buffer.toString();
    }
}
