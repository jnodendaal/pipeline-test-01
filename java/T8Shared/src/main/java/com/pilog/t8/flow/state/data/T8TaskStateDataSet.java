package com.pilog.t8.flow.state.data;

import com.pilog.t8.data.T8DataEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskStateDataSet implements Serializable
{
    private final T8ParameterStateDataSet inputParameterDataSet;
    private final T8ParameterStateDataSet outputParameterDataSet;
    private final Map<String, T8ParameterStateDataSet> propertyParameterDataSets; // Key: LANGUAGE_ID, Value: The parameter data set for the language.
    private final T8DataEntity taskEntity;
    private final String taskIID;

    public T8TaskStateDataSet(T8DataEntity taskEntity)
    {
        this.taskIID = (String)taskEntity.getFieldValue("$TASK_IID");
        this.taskEntity = taskEntity;
        this.inputParameterDataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
        this.outputParameterDataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
        this.propertyParameterDataSets = new HashMap<>();
    }

    public String getTaskIID()
    {
        return taskIID;
    }

    public T8DataEntity getTaskEntity()
    {
        return taskEntity;
    }

    public T8ParameterStateDataSet getInputParameterDataSet()
    {
        return inputParameterDataSet;
    }

    public T8ParameterStateDataSet getOutputParameterDataSet()
    {
        return outputParameterDataSet;
    }

    public List<String> getLanguageIDList()
    {
        return new ArrayList<String>(propertyParameterDataSets.keySet());
    }

    public T8ParameterStateDataSet getLanguageParameterDataSet(String languageID)
    {
        return propertyParameterDataSets.get(languageID);
    }

    public List<T8ParameterStateDataSet> getPropertyDataSets()
    {
        return new ArrayList<T8ParameterStateDataSet>(propertyParameterDataSets.values());
    }

    public int getEntityCount()
    {
        int count;

        count = taskEntity != null ? 1 : 0;
        count += inputParameterDataSet.getEntityCount();
        count += outputParameterDataSet.getEntityCount();
        for (T8ParameterStateDataSet propertyDataSet : getPropertyDataSets())
        {
            count += propertyDataSet.getEntityCount();
        }

        return count;
    }

    public void addInputParameterData(T8DataEntity entity)
    {
        inputParameterDataSet.addParameterData(entity);
    }

    public void addInputParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            inputParameterDataSet.addParameterData(entity);
        }
    }

    public void addOutputParameterData(T8DataEntity entity)
    {
        outputParameterDataSet.addParameterData(entity);
    }

    public void addOutputParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            outputParameterDataSet.addParameterData(entity);
        }
    }

    public void addPropertyData(T8DataEntity entity)
    {
        T8ParameterStateDataSet dataSet;
        String languageID;

        languageID = (String)entity.getFieldValue("$LANGUAGE_ID");
        dataSet = propertyParameterDataSets.get(languageID);
        if (dataSet == null)
        {
            dataSet = new T8ParameterStateDataSet("$PARENT_PROPERTY_ID");
            dataSet.addParameterData(entity);
            propertyParameterDataSets.put(languageID, dataSet);
        }
        else
        {
            dataSet.addParameterData(entity);
        }
    }

    public void addPropertyData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            addPropertyData(entity);
        }
    }
}
