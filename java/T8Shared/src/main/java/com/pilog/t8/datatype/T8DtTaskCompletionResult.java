package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.flow.task.T8TaskCompletionResult.T8TaskCompletionResultType;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtTaskCompletionResult extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TASK_COMPLETION_RESULT";

    public T8DtTaskCompletionResult() {}

    public T8DtTaskCompletionResult(T8DefinitionManager context) {}

    public T8DtTaskCompletionResult(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FunctionalityHandle.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8TaskCompletionResult result;
            Map<String, Object> resultParameters;
            T8TaskCompletionResultType resultType;
            JsonObject jsonResult;

            // Create the concept JSON object.
            result = (T8TaskCompletionResult)object;
            resultType = result.getResultType();
            resultParameters = result.getResultParameters();
            jsonResult = new JsonObject();
            jsonResult.add("taskIid", result.getTaskIid());
            jsonResult.add("resultType", resultType != null ? resultType.toString() : null);
            if (resultParameters != null) jsonResult.addIfNotNull("resultParameters", new T8DtMap(null).serialize(resultParameters));

            // Return the handle object.
            return jsonResult;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8TaskCompletionResult result;
            JsonObject jsonResult;
            String resultType;

            jsonResult = (JsonObject)jsonValue;
            resultType = jsonResult.getString("resultType");
            
            result = new T8TaskCompletionResult();
            result.setTaskIid(jsonResult.getString("taskIid"));
            result.setResultType(resultType != null ? T8TaskCompletionResultType.valueOf(resultType) : null);
            result.setResultParameters(new T8DtMap(null).deserialize(jsonResult.getJsonObject("resultParameters")));
            return result;
        }
        else return null;
    }
}