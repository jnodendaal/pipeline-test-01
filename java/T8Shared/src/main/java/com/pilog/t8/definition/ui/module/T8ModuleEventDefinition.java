package com.pilog.t8.definition.ui.module;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleEventDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_EVENT";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MODULE_EVENT";
    public static final String DISPLAY_NAME = "Module Event";
    public static final String DESCRIPTION = "An event that may occur inside of the module and which may be listened for by components outside of the module context.";
    public static final String IDENTIFIER_PREFIX = "ME_";
    public enum Datum {EVENT_PARAMETER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ModuleEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EVENT_PARAMETER_DEFINITIONS.toString(), "Event Parameters", "The data parameters available when this event occurs."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EVENT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public ArrayList<T8DataParameterDefinition> getParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.EVENT_PARAMETER_DEFINITIONS.toString());
    }

    public void addParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.EVENT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public void setParameterDefinitions(ArrayList<T8DataParameterDefinition> eventParameterDefinitions)
    {
        setDefinitionDatum(Datum.EVENT_PARAMETER_DEFINITIONS.toString(), eventParameterDefinitions);
    }
}
