package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8StateParameterPersistenceHandler
{
    private final T8DataEntityDefinition entityDefinition;
    private final String entityIdentifier;
    private final String foreignKeyFieldIdentifier;

    public enum ParameterType{};

    public T8StateParameterPersistenceHandler(T8DataEntityDefinition entityDefinition, String foreignKeyFieldIdentifier)
    {
        this.entityDefinition = entityDefinition;
        this.entityIdentifier = entityDefinition.getIdentifier();
        this.foreignKeyFieldIdentifier = T8IdentifierUtilities.stripNamespace(foreignKeyFieldIdentifier);
    }

    public Map<String, Object> retrieveParameters(T8DataTransaction tx, String foreignKeyFieldValue) throws Exception
    {
        List<T8DataEntity> parameterEntities;

        parameterEntities = tx.select(entityIdentifier, HashMaps.newHashMap(entityIdentifier + foreignKeyFieldIdentifier, foreignKeyFieldValue));
        return buildParameterMap(parameterEntities, null);
    }

    private Map<String, Object> buildParameterMap(List<T8DataEntity> parameterEntities, String parentParameterIID)
    {
        Map<String, Object> parameterMap;
        List<T8DataEntity> mapEntities;

        parameterMap = new HashMap<String, Object>();
        mapEntities = T8DataUtilities.findDataEntities(parameterEntities, "$PARENT_PARAMETER_IID" , parentParameterIID);
        for (T8DataEntity mapEntity : mapEntities)
        {
            String key;

            key = (String)mapEntity.getFieldValue("$KEY");
            parameterMap.put(key, buildParameterValue(parameterEntities, mapEntity));
        }

        return parameterMap;
    }

    private List<Object> buildParameterList(List<T8DataEntity> parameterEntities, String parentParameterIID)
    {
        List<T8DataEntity> listEtities;
        List<Object> parameters;

        parameters = new ArrayList<Object>();
        listEtities = T8DataUtilities.findDataEntities(parameterEntities, "$PARENT_PARAMETER_IID" , parentParameterIID);
        Collections.sort(listEtities, new EntitySequenceComparator());
        for (T8DataEntity listEntity : listEtities)
        {
            parameters.add(buildParameterValue(parameterEntities, listEntity));
        }

        return parameters;
    }

    private Object buildParameterValue(List<T8DataEntity> parameterEntities, T8DataEntity parameterEntity)
    {
        String parameterIID;
        String type;
        String value;
        String clob;
        byte[] blob;

        // Get the field values used for value construction.
        parameterIID = (String)parameterEntity.getFieldValue("$PARAMETER_IID");
        value = (String)parameterEntity.getFieldValue("$VALUE");
        type = (String)parameterEntity.getFieldValue("$TYPE");
        clob = (String)parameterEntity.getFieldValue("$CHARACTER_DATA");
        blob = (byte[])parameterEntity.getFieldValue("$BINARY_DATA");

        // Construct the appropriate parameter value based on the type specified.
        switch (type)
        {
            case "@MAP":
                return buildParameterMap(parameterEntities, parameterIID);
            case "@LIST":
                return buildParameterList(parameterEntities, parameterIID);
            case "@BYTE_ARRAY":
                return blob;
            case "@LONG_STRING":
                return clob;
            case "@INTEGER":
                return Integer.parseInt(value);
            case "@LONG":
                return Long.parseLong(value);
            case "@DOUBLE":
                return Double.parseDouble(value);
            case "@FLOAT":
                return Float.parseFloat(value);
            case "@BOOLEAN":
                return Boolean.parseBoolean(value);
            case "@STRING":
                return value;
            case "@UNDEFINED":
                return null;
            default:
                throw new RuntimeException("Invalid parameter type found: " + type);
        }
    }

    public void insertParameters(T8DataTransaction tx, String foreignKeyFieldValue, String parentParameterIID, Map<String, Object> parameters) throws Exception
    {
        int index;

        index = 0;
        for (String key : parameters.keySet())
        {
            T8DataEntity newEntity;
            String parameterIID;
            Object value;

            // Get the parameter value.
            value = parameters.get(key);
            parameterIID = T8IdentifierUtilities.createNewGUID();

            // Create the new parameter entity.
            newEntity = entityDefinition.getNewDataEntityInstance();
            newEntity.setFieldValue(entityIdentifier + foreignKeyFieldIdentifier, foreignKeyFieldValue);
            newEntity.setFieldValue(entityIdentifier + "$PARAMETER_IID", parameterIID);
            newEntity.setFieldValue(entityIdentifier + "$PARENT_PARAMETER_IID", parentParameterIID);
            newEntity.setFieldValue(entityIdentifier + "$TYPE", getParameterType(value));
            newEntity.setFieldValue(entityIdentifier + "$SEQUENCE", index++);
            newEntity.setFieldValue(entityIdentifier + "$PARAMETER_KEY", key);
            newEntity.setFieldValue(entityIdentifier + "$VALUE", getParameterStringValue(value));
            if (value instanceof byte[]) newEntity.setFieldValue(entityIdentifier + "$BINARY_DATA", value);

            // Insert the entity.
            tx.insert(newEntity);

            // Insert sub-collections.
            if (value instanceof Map)
            {
                insertParameters(tx, foreignKeyFieldValue, parameterIID, (Map<String, Object>)value);
            }
            else if (value instanceof List)
            {
                insertParameters(tx, foreignKeyFieldValue, parameterIID, (List<Object>)value);
            }
        }
    }

    public void insertParameters(T8DataTransaction tx, String foreignKeyFieldValue, String parentParameterIID, List<Object> parameters) throws Exception
    {
        for (int index = 0; index < parameters.size(); index++)
        {
            T8DataEntity newEntity;
            String parameterIID;
            Object value;

            // Get the parameter value.
            value = parameters.get(index);
            parameterIID = T8IdentifierUtilities.createNewGUID();

            // Create the new parameter entity.
            newEntity = entityDefinition.getNewDataEntityInstance();
            newEntity.setFieldValue(entityIdentifier + foreignKeyFieldIdentifier, foreignKeyFieldValue);
            newEntity.setFieldValue(entityIdentifier + "$PARAMETER_IID", parameterIID);
            newEntity.setFieldValue(entityIdentifier + "$PARENT_PARAMETER_IID", parentParameterIID);
            newEntity.setFieldValue(entityIdentifier + "$TYPE", getParameterType(value));
            newEntity.setFieldValue(entityIdentifier + "$SEQUENCE", index);
            newEntity.setFieldValue(entityIdentifier + "$PARAMETER_KEY", index);
            newEntity.setFieldValue(entityIdentifier + "$VALUE", getParameterStringValue(value));

            // Insert the entity.
            tx.insert(newEntity);

            // Insert sub-collections.
            if (value instanceof Map)
            {
                insertParameters(tx, foreignKeyFieldValue, parameterIID, (Map<String, Object>)value);
            }
            else if (value instanceof List)
            {
                insertParameters(tx, foreignKeyFieldValue, parameterIID, (List<Object>)value);
            }
        }
    }

    private static String getParameterStringValue(Object value)
    {
        if (value == null)
        {
            return null;
        }
        else if (value instanceof Map)
        {
            return null;
        }
        else if (value instanceof List)
        {
            return null;
        }
        else
        {
            return value.toString();
        }
    }

    private static String getParameterType(Object parameterValue)
    {
        if (parameterValue == null)
        {
            return "@NULL";
        }
        else if (parameterValue instanceof Map)
        {
            return T8DataType.MAP.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof List)
        {
            return T8DataType.LIST.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof byte[])
        {
            return T8DataType.BYTE_ARRAY.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof Integer)
        {
            return T8DataType.INTEGER.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof Long)
        {
            return T8DataType.LONG.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof Double)
        {
            return T8DataType.DOUBLE.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof Float)
        {
            return T8DataType.FLOAT.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof Boolean)
        {
            return T8DataType.BOOLEAN.getDataTypeIdentifier();
        }
        else if (parameterValue instanceof String)
        {
            return T8DataType.STRING.getDataTypeIdentifier();
        }
        else throw new RuntimeException("Invalid parameter type: " + parameterValue.getClass());
    }

    private static class EntitySequenceComparator implements Comparator
    {
        @Override
        public int compare(Object o1, Object o2)
        {
            int sequence1;
            int sequence2;

            sequence1 = (int)((T8DataEntity)o1).getFieldValue("$SEQUENCE");
            sequence2 = (int)((T8DataEntity)o1).getFieldValue("$SEQUENCE");
            return Integer.compare(sequence1, sequence2);
        }
    }
}
