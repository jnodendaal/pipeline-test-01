package com.pilog.t8.definition.data.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityDataObjectDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_OBJECT";
    public static final String GROUP_IDENTIFIER = "@DG_FUNCTIONALITY_OBJECT";
    public static final String GROUP_NAME = "Functionality Objects";
    public static final String GROUP_DESCRIPTION = "The mapping information between parent functionalities and the data objects on which they act.";
    public static final String IDENTIFIER_PREFIX = "FO_";
    public static final String DISPLAY_NAME = "Functionality Object";
    public static final String DESCRIPTION = "The setup of a data object as it relates to a parent functionality.";
    public enum Datum
    {
        DATA_OBJECT_ID,
        FUNCTIONALITY_INPUT_PARAMETER_ID,
        REQUIRED
    };
    // -------- Definition Meta-Data -------- //

    public T8FunctionalityDataObjectDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_OBJECT_ID.toString(), "Data Object", "The data object to which this mapping applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_INPUT_PARAMETER_ID.toString(), "Key Parameter", "The functionality parameter to be used as key to the specified data object."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REQUIRED.toString(), "Required", "A Boolean flag that determines whether or not this object is required as input to the functionlity."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_OBJECT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if (Datum.FUNCTIONALITY_INPUT_PARAMETER_ID.toString().equals(datumId))
        {
            T8FunctionalityDefinition functionalityDefinition;

            functionalityDefinition = (T8FunctionalityDefinition)getParentDefinition();
            return createLocalIdentifierOptionsFromDefinitions(functionalityDefinition.getInputParameterDefinitions(), true, "No Input Parameter");
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getDataObjectId()
    {
        return getDefinitionDatum(Datum.DATA_OBJECT_ID);
    }

    public void setDataObjectId(String id)
    {
        setDefinitionDatum(Datum.DATA_OBJECT_ID, id);
    }

    public String getFunctionalityInputParameterId()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_INPUT_PARAMETER_ID);
    }

    public void setFunctionalityInputParameterId(String id)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_INPUT_PARAMETER_ID, id);
    }

    public boolean isRequired()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.REQUIRED);
        return value != null && value;
    }

    public void setRequired(Boolean required)
    {
        setDefinitionDatum(Datum.REQUIRED, required);
    }
}
