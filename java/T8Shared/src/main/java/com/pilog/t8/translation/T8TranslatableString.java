package com.pilog.t8.translation;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.ParameterizedString;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.io.Serializable;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TranslatableString extends ParameterizedString implements Serializable
{
    public T8TranslatableString(String stringValue)
    {
        super(stringValue);
    }

    public T8TranslatableString(String stringValue, Object... parameterValues)
    {
        super(stringValue, parameterValues);
    }

    public String translateString(T8ConfigurationManager configurationManager, T8Context context)
    {
        StringBuffer string;

        string = new StringBuffer(configurationManager.getUITranslation(context, stringBuffer.toString()));
        StringUtilities.replaceAll("[^\\\\]", "\\?", null, string, (List)parameterValues);
        //StringUtilities.replaceAll(null, "\\\\?", null, string, "?"); // TODO: Investigate this - caused memory problems for some reason.

        return string.toString();
    }

    @Override
    public void addParameter(Object parameter)
    {
        parameterValues.add(parameter != null ? parameter.toString() : null);
    }
}
