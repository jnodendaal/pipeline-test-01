package com.pilog.t8.definition;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.event.T8DefinitionContextListener;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * This interface defines a context in which a definition's life-cycle takes
 * place i.e. where the definition is loaded, initialized edited and
 * saved/deleted.
 *
 * @author Bouwer du Preez
 */
public interface T8DefinitionContext
{
    public T8Context getContext();
    public T8ClientContext getClientContext();
    public T8SessionContext getSessionContext();
    public void addContextListener(T8DefinitionContextListener listener);
    public void removeContextListener(T8DefinitionContextListener listener);

    public T8Definition createNewDefinition(String definitionId, String typeId) throws Exception;
    public T8Definition createNewDefinition(String definitionId, String typeId, List parameters) throws Exception;
    public List<T8DefinitionHandle> renameDefinition(T8Definition definition, String newDefinitionId) throws Exception;
    public void removeDefinitionReferences(String definitionId, String keyId) throws Exception;
    public boolean checkIdentifierAvailability(String projectId, String definitionId) throws Exception;

    public T8DefinitionMetaData saveDefinition(T8Definition definition, String keyIdentifier) throws Exception;
    public void deleteDefinition(String projectId, String definitionId, boolean safeCheck, String keyIdentifier) throws Exception;
    public boolean deleteDefinition(T8DefinitionHandle definitionHandle, boolean safeDeletion, String keyIdentifier) throws Exception;
    public T8Definition lockDefinition(T8DefinitionHandle definitionHandle, String keyId) throws Exception;
    public T8Definition unlockDefinition(T8DefinitionHandle definitionHandle, String keyId) throws Exception;
    public T8Definition unlockDefinition(T8Definition definition, String keyId) throws Exception;
    public T8DefinitionLockDetails getDefinitionLockDetails(T8DefinitionHandle definitionHandle) throws Exception;
    public T8DefinitionMetaData finalizeDefinition(T8DefinitionHandle definitionHandle, String comment) throws Exception;

    public T8Definition copyDefinition(T8Definition definition, String newDefinitionId) throws Exception;
    public boolean copyDefinitionToProject(String projectId, String definitionId, String newProjectId) throws Exception;
    public boolean moveDefinitionToProject(String projectId, String definitionId, String newProjectId) throws Exception;
    public void transferDefinitionReferences(String oldIdentifier, String newIdentifier) throws Exception;

    public T8Definition getRawDefinition(String projectId, String definitionId) throws Exception;
    public T8Definition getRawDefinition(T8DefinitionHandle definitionHandle) throws Exception;
    public T8Definition getInitializedDefinition(String projectId, String definitionId, Map<String, Object> inputParameters) throws Exception;
    public T8DefinitionComposition getDefinitionComposition(String projectId, String definitionId, boolean includeDescendants) throws Exception;
    public List<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupId) throws Exception;
    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception;
    public List<T8DefinitionMetaData> getProjectDefinitionMetaData(String projectId) throws Exception;
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String projectId, String groupId) throws Exception;
    public List<T8DefinitionTypeMetaData> getAllDefinitionTypeMetaData() throws Exception;
    public List<T8DefinitionGroupMetaData> getAllDefinitionGroupMetaData() throws Exception;
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeId) throws Exception;
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String projectId, String typeId) throws Exception;
    public List<String> getGroupDefinitionIdentifiers(String projectId, String groupId) throws Exception;
    public List<String> getDefinitionIdentifiers(String projectId, String typeId) throws Exception;
    public List<String> getDefinitionGroupIdentifiers() throws Exception;
    public List<String> getDefinitionTypeIdentifiers() throws Exception;
    public List<String> getDefinitionTypeIdentifiers(String groupId) throws Exception;
    public List<T8Definition> getRawGroupDefinitions(String projectId, String groupId) throws Exception;

    public List<T8DefinitionHandle> findDefinitionsByText(String searchString) throws Exception;
    public List<T8DefinitionHandle> findDefinitionsByRegularExpression(String expression) throws Exception;
    public List<T8DefinitionHandle> findDefinitionsByIdentifier(String definitionId, boolean includeReferencesOnly) throws Exception;
}
