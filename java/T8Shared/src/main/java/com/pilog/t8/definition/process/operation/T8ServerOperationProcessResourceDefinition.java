package com.pilog.t8.definition.process.operation;

/**
 * @author Hennie Brink
 */
public class T8ServerOperationProcessResourceDefinition extends T8ServerOperationProcessDefinition
{
    public static final String TYPE_IDENTIFIER = "@DT_SERVER_PROCESS_OPERATION_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;

    public T8ServerOperationProcessResourceDefinition(String identifier)
    {
        super(identifier);
    }
}
