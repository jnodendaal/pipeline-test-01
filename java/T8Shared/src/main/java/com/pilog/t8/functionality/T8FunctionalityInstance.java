package com.pilog.t8.functionality;

import com.pilog.t8.ui.functionality.T8TargetObjectValidationResult;
import com.pilog.t8.ui.functionality.event.T8FunctionalityInstanceListener;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.time.T8Timestamp;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityInstance
{
    /**
     * Returns the identifier of the functionality from which this instance was
     * created.
     * @return The identifier of the functionality from which this instance was
     * created.
     */
    public String getFunctionalityId();

    /**
     * Returns the unique identifier of this functionality instance.
     * @return The unique identifier of this functionality instance.
     */
    public String getFunctionalityIid();

    /**
     * Returns the Functionality definition from which this instance was
     * created.
     * @return The Functionality definition from which this instance was
     * created.
     */
    public T8FunctionalityDefinition getDefinition();

    /**
     * When this method is invoked, the functionality must initialize its state
     * in order to be ready for execution.  On the client-side this method will
     * be invoked in a background thread.
     */
    public void initialize();

    /**
     * Runs validations to ensure that this functionality instance can be
     * applied to the supplied target object.
     * @param parameters The functionality parameters supplied for access.
     * @return The validation result.
     */
    public T8TargetObjectValidationResult validateTargetObjects(Map<String, Object> parameters);

    /**
     * This executes the functionality.  The instance is responsible for
     * functionality execution in whatever way it requires.  The progress of the
     * functionality can be obtained via separate methods.
     * @param parameters The functionality parameters supplied for access.
     * @return The functionality execution handle that can be used to monitor
     * execution of the functionality.
     * @throws java.lang.Exception
     */
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception;

    /**
     * Returns the time at which this functionality instance was accessed of null if it has not yet been accessed.
     * @return The time at which this functionality instance was accessed of null if it has not yet been accessed.
     */
    public T8Timestamp getTimeAccessed();

    /**
     * This method must release all resources acquired by the functionality
     * during its execution.
     * @return A boolean flag indicating whether or not this functionality was
     * successfully released.  Some functionalities always return true if they
     * are of a type that can be released at any time.  Other functionality
     * types such as workflows only return true if this method is called after
     * the workflow has completed execution i.e. the stop() method must be
     * called first before the functionality can be released.
     */
    public boolean release();

    /**
     * This method stop the execution of the functionality if it is still in
     * progress.  This method must always return immediately and returns a
     * boolean value indicating whether or not the functionality can currently
     * be stopped.  If the functionality is not in progress, this method has no
     * effect and will return true.  Calling this method multiple times is
     * allowed.
     * @return A Boolean flag indicating whether or not the functionality could
     * be stopped.
     */
    public boolean stop();

    /**
     * Returns the state of this functionality that can be persisted to storage.
     * @return The state of this functionality instance.
     */
    public T8FunctionalityState getFunctionalityState();

    public void addFunctionalityInstanceListener(T8FunctionalityInstanceListener listener);
    public void removeFunctionalityInstanceListener(T8FunctionalityInstanceListener listener);
}
