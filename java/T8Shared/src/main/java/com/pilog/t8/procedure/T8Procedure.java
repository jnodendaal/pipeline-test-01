package com.pilog.t8.procedure;

import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * This interface must be implemented by all classes representing procedures
 * that can be executed on the T8MainServer.  Procedures differ from T8Operation
 * in that they are optimized for re-use within a T8Operation context and without
 * restrictions incurred due to external API exposure.
 *
 * @author Bouwer du Preez
 */
public interface T8Procedure
{
    public void init();
    public double getProgress(); // Returns the progress of the procedure execution.
    public T8ProgressReport getProgressReport(); // Returns a status report detailing the progress and current status of the procedure.
    public Map<String, ? extends Object> execute(T8Context accessContext, Map<String, Object> procedureParameters) throws Exception;
    public void stop(); // Stops execution without cancelling already completed progress.
    public void cancel(); // Cancels all completed progress, rolling back changes where possible.
    public void pause(); // Pauses execution of the procedure.
    public void resume(); // Resumes a previously paused procedure.
}
