package com.pilog.t8.definition.flow.signal;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowSignalDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_SIGNAL";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_WORK_SIGNAL";
    public static final String IDENTIFIER_PREFIX = "FW_SIG_";
    public static final String STORAGE_PATH = "/work_flow_signals";
    public static final String DISPLAY_NAME = "Work Flow Signal";
    public static final String DESCRIPTION = "A signal that may be sent to all executing work flows.";
    public enum Datum {PARAMETER_DEFINITIONS,
                       KEY_PARAMETER_ID};
    // -------- Definition Meta-Data -------- //

    public T8FlowSignalDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PARAMETER_DEFINITIONS.toString(), "Parameters",  "The data parameters carried by the signal."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.KEY_PARAMETER_ID.toString(), "Key Parameter", "The key parameter (has to be a GUID) of this signal."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.KEY_PARAMETER_ID.toString().equals(datumIdentifier)) return createLocalIdentifierOptionsFromDefinitions(getParameterDefinitions());
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getKeyParameterId()))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.KEY_PARAMETER_ID.toString(), "No Signal Key Parameter set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public ArrayList<T8DataParameterDefinition> getParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.PARAMETER_DEFINITIONS.toString());
    }

    public void setParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }

    public String getKeyParameterId()
    {
        return (String)getDefinitionDatum(Datum.KEY_PARAMETER_ID.toString());
    }

    public void setKeyParameterId(String id)
    {
        setDefinitionDatum(Datum.KEY_PARAMETER_ID.toString(), id);
    }
}
