package com.pilog.t8.data.tag;

import java.io.Serializable;
import java.util.Objects;

/**
 * This is an immutable class representing a T8 data tag that comprises an
 * identifier:value combination.
 *
 * @author Bouwer du Preez
 */
public class T8Tag implements Comparable<T8Tag>, Serializable
{
    private final String tagIdentifier;
    private final String tagValue;

    public T8Tag(String identifier, String value)
    {
        this.tagIdentifier = identifier;
        this.tagValue =value;
    }

    public String getTagIdentifier()
    {
        return tagIdentifier;
    }

    public String getTagValue()
    {
        return tagValue;
    }

    public String getTagString()
    {
        StringBuffer tagString;

        tagString = new StringBuffer();
        tagString.append("[");
        tagString.append(tagIdentifier);

        if (tagValue != null)
        {
            tagString.append(":");
            tagString.append(tagValue);
        }

        tagString.append("]");
        return tagString.toString();
    }

    public boolean isEqual(T8Tag tag)
    {
        if (tag == null) return false;
        else return ((Objects.equals(this.tagIdentifier, tag.getTagIdentifier())) && (Objects.equals(this.tagValue, tag.getTagValue())));
    }

    @Override
    public String toString()
    {
        return getTagString();
    }

    @Override
    public int compareTo(T8Tag o)
    {
        if (o == null) return 1;
        else return o.getTagString().compareTo(getTagString());
    }
}
