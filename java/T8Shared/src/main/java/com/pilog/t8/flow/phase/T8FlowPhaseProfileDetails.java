package com.pilog.t8.flow.phase;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhaseProfileDetails implements Serializable
{
    private final String identifier;
    private String displayName;

    public T8FlowPhaseProfileDetails(String identifier, String displayName)
    {
        this.identifier = identifier;
        this.displayName = displayName;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
}
