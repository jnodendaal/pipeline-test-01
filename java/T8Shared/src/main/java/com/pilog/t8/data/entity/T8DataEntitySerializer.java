package com.pilog.t8.data.entity;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntitySerializer
{
    public static JsonValue serializeEntityCount(String entityIdentifier, int count)
    {
        JsonObject countResult;

        countResult = new JsonObject();
        countResult.add("entityId", entityIdentifier);
        countResult.add("count", count);

        return countResult;
    }

    public static JsonValue serializeToJSON(T8DataEntity entity, boolean includeNullFields)
    {
        T8DataEntityDefinition entityDefinition;
        List<String> fieldIds;
        JsonObject entityObject;

        // Get the field identifiers.
        entityDefinition = entity.getDefinition();
        fieldIds = entityDefinition.getFieldIdentifiers(true);

        // Create the record JSON object.
        entityObject = new JsonObject();
        entityObject.add("id", entity.getIdentifier());
        for (String fieldId : fieldIds)
        {
            T8DataEntityFieldDefinition fieldDefinition;
            T8DataType fieldDataType;

            fieldDefinition = entityDefinition.getFieldDefinition(fieldId);
            fieldDataType = fieldDefinition.getDataType();
            entityObject.add(fieldId, fieldDataType.serialize(entity.getFieldValue(fieldId)));
        }

        return entityObject;
    }

    public static JsonArray serializeToJSON(List<String> fieldIdList, List<T8DataEntity> entityList)
    {
        JsonArray dataArray;

        dataArray = new JsonArray();
        if ((entityList != null) && (entityList.size() > 0))
        {
            T8DataEntityDefinition entityDefinition;
            List<String> fieldIds;

            // Get the entity details.
            entityDefinition = entityList.get(0).getDefinition();
            fieldIds = fieldIdList != null ? fieldIdList : entityDefinition.getFieldIdentifiers(true);

            // Loop through all entities and add them to the array.
            for (T8DataEntity entity : entityList)
            {
                JsonObject entityObject;

                // Create the entity object.
                entityObject = new JsonObject();
                for (String fieldId : fieldIds)
                {
                    T8DataEntityFieldDefinition fieldDefinition;

                    fieldDefinition = entityDefinition.getFieldDefinition(fieldId);
                    if (fieldDefinition != null)
                    {
                        T8DataType fieldDataType;

                        fieldDataType = fieldDefinition.getDataType();
                        entityObject.add(fieldId, fieldDataType.serialize(entity.getFieldValue(fieldId)));
                    }
                    else throw new IllegalArgumentException("Invalid field: " + fieldId);
                }

                // Add the entity object to the array.
                dataArray.add(entityObject);
            }
        }

        return dataArray;
    }

    public static JsonArray serializeToJSONTable(List<String> fieldIdList, List<T8DataEntity> entityList)
    {
        JsonArray rowArray;

        // Create the row array.
        rowArray = new JsonArray();
        if ((entityList != null) && (entityList.size() > 0))
        {
            T8DataEntityDefinition entityDefinition;
            List<String> fieldIdentifiers;

            // Get the entity details.
            entityDefinition = entityList.get(0).getDefinition();
            fieldIdentifiers = fieldIdList != null ? fieldIdList : entityDefinition.getFieldIdentifiers(true);

            // Loop through all entities and add them to the array.
            for (T8DataEntity entity : entityList)
            {
                JsonArray valueArray;

                // Create the value array.
                valueArray = new JsonArray();
                for (String fieldIdentifier : fieldIdentifiers)
                {
                    T8DataEntityFieldDefinition fieldDefinition;
                    T8DataType fieldDataType;

                    fieldDefinition = entityDefinition.getFieldDefinition(fieldIdentifier);
                    fieldDataType = fieldDefinition.getDataType();
                    valueArray.add(fieldDataType.serialize(entity.getFieldValue(fieldIdentifier)));
                }

                // Add the entity object to the array.
                rowArray.add(valueArray);
            }
        }

        return rowArray;
    }
}
