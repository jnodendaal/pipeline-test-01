package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.security.T8WorkflowProfileDetails;

/**
 * @author Gavin Boshoff
 */
public class T8DtWorkflowProfileDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@WORKFLOW_PROFILE_DETAILS";

    public T8DtWorkflowProfileDetails() {}

    public T8DtWorkflowProfileDetails(T8DefinitionManager context) {}

    public T8DtWorkflowProfileDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8WorkflowProfileDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8WorkflowProfileDetails profileDetails;
            JsonObject jsonProfileDetails;

            // Create the JSON object.
            profileDetails = (T8WorkflowProfileDetails)object;

            jsonProfileDetails = new JsonObject();
            jsonProfileDetails.add("id", profileDetails.getId());
            jsonProfileDetails.add("name", profileDetails.getName());
            jsonProfileDetails.add("description", profileDetails.getDescription());

            // Return the final JSON object.
            return jsonProfileDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8WorkflowProfileDetails deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8WorkflowProfileDetails profileDetails;
            JsonObject jsonFileDetails;

            // Create the profile details object.
            jsonFileDetails = jsonValue.asObject();
            profileDetails = new T8WorkflowProfileDetails();
            profileDetails.setId(jsonFileDetails.getString("id"));
            profileDetails.setName(jsonFileDetails.getString("name"));
            profileDetails.setDescription(jsonFileDetails.getString("description"));

            // Return the completed object.
            return profileDetails;
        }
        else return null;
    }
}