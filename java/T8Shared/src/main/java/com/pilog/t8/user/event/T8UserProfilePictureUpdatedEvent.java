package com.pilog.t8.user.event;

/**
 * @author Bouwer du Preez
 */
public class T8UserProfilePictureUpdatedEvent
{
    private final String pictureUrl;
    private final String thumbnailUrl;

    public T8UserProfilePictureUpdatedEvent(String pictureUrl, String thumbnailUrl)
    {
        this.pictureUrl = pictureUrl;
        this.thumbnailUrl = thumbnailUrl;
    }
}