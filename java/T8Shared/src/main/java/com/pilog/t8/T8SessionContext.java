package com.pilog.t8;

import com.pilog.t8.performance.T8PerformanceStatistics;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8SessionContext extends Serializable
{
    public String getSessionIdentifier();
    public String getUserIdentifier();
    public String getUserProfileIdentifier();
    public String getLanguageIdentifier();
    public String getContentLanguageIdentifier();
    public String getUserName();
    public String getUserSurname();
    public String getOrganizationIdentifier();
    public String getRootOrganizationIdentifier();
    public String getServerURL();
    public List<String> getWorkFlowProfileIdentifiers();
    public Map<String, Object> getSessionParameterMap();
    public T8PerformanceStatistics getPerformanceStatistics();
    public boolean isLoggedIn();

    public Serializable getSessionData(String dataId);
    public Serializable setSessionData(String dataId, Serializable data);

    // Applicable to system sessions.
    public boolean isSystemSession();
    public String getSystemAgentIdentifier();
    public String getSystemAgentInstanceIdentifier();

    // Applicable to anonymous sessions.
    /**
     * A convenience method to define whether or not the instance of
     * {@code T8SessionContext} is that of an anonymous session representation.
     *
     * @return {@code true} if the session is anonymous. {@code false} otherwise
     */
    public boolean isAnonymousSession();
}
