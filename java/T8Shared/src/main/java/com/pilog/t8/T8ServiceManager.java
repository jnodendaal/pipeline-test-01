package com.pilog.t8;

import com.pilog.t8.security.T8Context;
import com.pilog.t8.service.T8Service;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ServiceManager extends T8ServerModule
{
    public T8Service getService(T8Context context, String serviceId);
    public Map<String, Object> executeServiceOperation(T8Context context, String serviceId, String serviceOperationIdentifier, Map<String, Object> operationParameters) throws Exception;
}
