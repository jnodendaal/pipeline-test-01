package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ParameterAssignmentActivityDefinition extends T8FlowActivityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_ACTIVITY_PARAMETER_ASSIGNMENT";
    public static final String DISPLAY_NAME = "Flow Parameter Assignment Activity";
    public static final String DESCRIPTION = "An activity that executes a list of expressions in order to assign specific values to flow parameters.";
    private enum Datum {PARAMETER_EXPRESSION_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ParameterAssignmentActivityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PARAMETER_EXPRESSION_DEFINITIONS.toString(), "Flow Parameter Expressions", "The parameter assignment expressions that will be executed in sequence when this activity is performed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARAMETER_EXPRESSION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ParameterAssignmentExpressionDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.PARAMETER_EXPRESSION_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.activity.T8FlowParameterAssignmentActivityNode").getConstructor(T8Context.class, T8Flow.class, T8ParameterAssignmentActivityDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    @Override
    public String getProfileIdentifier()
    {
        return null;
    }

    public List<T8ParameterAssignmentExpressionDefinition> getParameterExpressionDefinitions()
    {
        return (List<T8ParameterAssignmentExpressionDefinition>)getDefinitionDatum(Datum.PARAMETER_EXPRESSION_DEFINITIONS.toString());
    }

    public void setParameterExpressionDefinitions(List<T8ParameterAssignmentExpressionDefinition> expressionDefinitions)
    {
        setDefinitionDatum(Datum.PARAMETER_EXPRESSION_DEFINITIONS.toString(), expressionDefinitions);
    }
}
