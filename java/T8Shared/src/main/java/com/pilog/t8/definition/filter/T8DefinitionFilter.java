package com.pilog.t8.definition.filter;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.filter.T8DefinitionFilterClause.DefinitionFilterConjunction;
import com.pilog.t8.definition.filter.T8DefinitionFilterCriterion.DefinitionFilterOperator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFilter implements Serializable
{
    private T8DefinitionFilterDefinition definition;
    private String filterIdentifier; // Only applicable if the filter does not reference its definition.
    private T8DefinitionFilterCriteria filterCriteria;
    
    public enum OrderMethod {ASCENDING, DESCENDING};
    
    public T8DefinitionFilter()
    {
        this.filterCriteria = null;
    }
    
    public T8DefinitionFilter(T8DefinitionFilterDefinition definition)
    {
        this.definition = definition;
    }
    
    public T8DefinitionFilter copy()
    {
        T8DefinitionFilter copiedFilter;
        
        copiedFilter = new T8DefinitionFilter(definition);
        if (filterCriteria != null) copiedFilter.setFilterCriteria(filterCriteria.copy());
        return copiedFilter;
    }

    public String getIdentifier()
    {
        if (definition == null)
        {
            return filterIdentifier;
        }
        else return definition.getIdentifier();
    }

    public void setIdentifier(String filterIdentifier)
    {
        if (definition == null)
        {
            this.filterIdentifier = filterIdentifier;
        }
        else throw new RuntimeException("Cannot set identifier of a data filter that is predefined.");
    }
    
    public T8DefinitionFilterDefinition getDefinition()
    {
        return definition;
    }
    
    public boolean hasFilterCriteria()
    {
        return filterCriteria != null && filterCriteria.hasCriteria();
    }

    public T8DefinitionFilterCriteria getFilterCriteria()
    {
        return filterCriteria;
    }

    public void setFilterCriteria(T8DefinitionFilterCriteria filterCriteria)
    {
        this.filterCriteria = filterCriteria;
    }
    
    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param definitionFilter The data filter from which the criteria to add to this
     * filter will be retrieved.
     */
    public void addFilterCriteria(String conjunction, T8DefinitionFilter definitionFilter)
    {
        addFilterCriteria(DefinitionFilterConjunction.valueOf(conjunction), definitionFilter);
    }
    
    public void addFilterCriteria(DefinitionFilterConjunction conjunction, T8DefinitionFilter definitionFilter)
    {
        if (filterCriteria == null)
        {
            filterCriteria = definitionFilter.getFilterCriteria();
        }
        else
        {
            filterCriteria.addFilterClause(conjunction, definitionFilter.getFilterCriteria());
        }
    }
    
    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param filterCriteria The data filter criteria to add to this filter.
     */
    public void addFilterCriteria(String conjunction, T8DefinitionFilterCriteria filterCriteria)
    {
        addFilterCriteria(DefinitionFilterConjunction.valueOf(conjunction), filterCriteria);
    }
    
    public void addFilterCriteria(DefinitionFilterConjunction conjunction, T8DefinitionFilterCriteria filterCriteria)
    {
        if (this.filterCriteria == null)
        {
            this.filterCriteria = filterCriteria;
        }
        else
        {
            this.filterCriteria.addFilterClause(conjunction, filterCriteria);
        }
    }
    
    /**
     * This is a convenience method for use from script.
     * @param keyMap The map of key identifier-value pairs that will be added
     * as additional filter criteria.
     */
    public void addFilterCriteria(Map<String, Object> keyMap)
    {
        if (filterCriteria == null)
        {
            filterCriteria = new T8DefinitionFilterCriteria(keyMap);
        }
        else
        {
            filterCriteria.addKeyFilter(keyMap, false);
        }
    }
    
    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param fieldIdentifier The field identifier to which the criteria are
     * applicable.
     * @param valueList The list of values that form the criteria to be added.
     */
    public void addFilterCriteria(String conjunction, String fieldIdentifier, List<Object> valueList)
    {
        addFilterCriteria(DefinitionFilterConjunction.valueOf(conjunction), fieldIdentifier, valueList);
    }
    
    public void addFilterCriteria(DefinitionFilterConjunction conjunction, String datumIdentifier, List<Object> valueList)
    {
        if (filterCriteria == null) filterCriteria = new T8DefinitionFilterCriteria();
        filterCriteria.addFilterClause(conjunction, new T8DefinitionFilterCriterion(datumIdentifier, DefinitionFilterOperator.IN, valueList));
    }
    
    public void addFilterCriterion(DefinitionFilterConjunction conjunction, T8DefinitionFilterCriterion dataFilterCriterion)
    {
        if (filterCriteria == null)
        {
            filterCriteria = new T8DefinitionFilterCriteria();
        }

        filterCriteria.addFilterClause(conjunction, dataFilterCriterion);
    }
    
    public void addFilterCriterion(DefinitionFilterConjunction conjunction, String datumIdentifier, DefinitionFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        if (filterCriteria == null)
        {
            filterCriteria = new T8DefinitionFilterCriteria();
        }

        filterCriteria.addFilterClause(conjunction, datumIdentifier, operator, filterValue, caseInsensitive);
    }
    
    public boolean includesDefinition(T8Definition definition)
    {
        if (filterCriteria != null)
        {
            return filterCriteria.includesDefinition(definition);
        }
        else return true;
    }
    
    public T8DefinitionFilterCriterion getFilterCriterion(String identifier)
    {
        if (filterCriteria != null)
        {
            return filterCriteria.getFilterCriterion(identifier);
        }
        else return null;
    }
    
    public Set<String> getFilterDatumIdentifiers()
    {
        Set<String> datumIdentifierSet;
        
        datumIdentifierSet = new HashSet<String>();
        if (filterCriteria != null) datumIdentifierSet.addAll(filterCriteria.getFilterDatumIdentifiers());
        return datumIdentifierSet;
    }
    
    public List<T8DefinitionFilterCriterion> getFilterCriterionList()
    {
        ArrayList<T8DefinitionFilterCriterion> criterionList;
        
        criterionList = new ArrayList<T8DefinitionFilterCriterion>();
        if (filterCriteria != null)
        {
            criterionList.addAll(filterCriteria.getFilterCriterionList());
        }
        
        return criterionList;
    }
}
