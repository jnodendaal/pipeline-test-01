package com.pilog.t8.data.entity;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityValidationError implements Serializable
{
    private String entityFieldIdentifier;
    private String errorMessage;

    public T8DataEntityValidationError(String entityFieldIdentifier, String errorMessage)
    {
        this.entityFieldIdentifier = entityFieldIdentifier;
        this.errorMessage = errorMessage;
    }

    public String getEntityFieldIdentifier()
    {
        return entityFieldIdentifier;
    }

    public void setEntityFieldIdentifier(String entityFieldIdentifier)
    {
        this.entityFieldIdentifier = entityFieldIdentifier;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8DataEntityValidationError{");
        toStringBuilder.append("entityFieldIdentifier=").append(this.entityFieldIdentifier);
        toStringBuilder.append(",errorMessage=").append(this.errorMessage);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
