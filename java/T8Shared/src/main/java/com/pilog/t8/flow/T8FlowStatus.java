package com.pilog.t8.flow;

import com.pilog.t8.flow.T8Flow.FlowStatus;
import com.pilog.t8.flow.phase.T8FlowPhase;
import com.pilog.t8.flow.phase.T8FlowPhaseNodeDetails;
import com.pilog.t8.flow.phase.T8FlowPhaseProfileDetails;
import com.pilog.t8.flow.phase.T8FlowPhaseUserDetails;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition.FlowNodeType;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStatus implements Serializable
{
    private String flowIid;
    private String flowId;
    private String flowName;
    private FlowStatus status;
    private int progress;
    private long startTime;
    private long endTime;
    private String initiatorUserId;
    private String initiatorUserDisplayName;
    private String initiatorProfileId;
    private String initiatorProfileDisplayName;
    private String initiatorOrganizationId;
    private String initiatorOrganizationDisplayName;
    private final List<T8FlowNodeStatus> executingNodeStatuses;
    private String parentFlowIid;
    private final List<T8TaskDetails> availableTasks;
    private T8FlowPhase phases;

    public T8FlowStatus()
    {
        this.availableTasks = new ArrayList<>();
        this.executingNodeStatuses = new ArrayList<>();
    }

    public long getEndTime()
    {
        return endTime;
    }

    public void setEndTime(long endTime)
    {
        this.endTime = endTime;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String identifier)
    {
        this.flowIid = identifier;
    }

    public String getFlowId()
    {
        return flowId;
    }

    public void setFlowId(String flowIdentifier)
    {
        this.flowId = flowIdentifier;
    }

    public int getProgress()
    {
        return progress;
    }

    public void setProgress(int progress)
    {
        this.progress = progress;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public void setStartTime(long startTime)
    {
        this.startTime = startTime;
    }

    public FlowStatus getStatus()
    {
        return status;
    }

    public void setStatus(FlowStatus status)
    {
        this.status = status;
    }

    public String getFlowDisplayName()
    {
        return flowName;
    }

    public void setFlowDisplayName(String flowDisplayName)
    {
        this.flowName = flowDisplayName;
    }

    public String getInitiatorUserDisplayName()
    {
        return initiatorUserDisplayName;
    }

    public void setInitiatorUserDisplayName(String initiatorUserDisplayName)
    {
        this.initiatorUserDisplayName = initiatorUserDisplayName;
    }

    public String getInitiatorProfileDisplayName()
    {
        return initiatorProfileDisplayName;
    }

    public void setInitiatorProfileDisplayName(String initiatorProfileDisplayName)
    {
        this.initiatorProfileDisplayName = initiatorProfileDisplayName;
    }

    public String getInitiatorOrganizationDisplayName()
    {
        return initiatorOrganizationDisplayName;
    }

    public void setInitiatorOrganizationDisplayName(String initiatorOrganizationDisplayName)
    {
        this.initiatorOrganizationDisplayName = initiatorOrganizationDisplayName;
    }

    public String getInitiatorUserIdentifier()
    {
        return initiatorUserId;
    }

    public void setInitiatorUserIdentifier(String initiatorUserIdentifier)
    {
        this.initiatorUserId = initiatorUserIdentifier;
    }

    public String getInitiatorProfileIdentifier()
    {
        return initiatorProfileId;
    }

    public void setInitiatorProfileIdentifier(String initiatorProfileIdentifier)
    {
        this.initiatorProfileId = initiatorProfileIdentifier;
    }

    public String getInitiatorOrganizationIdentifier()
    {
        return initiatorOrganizationId;
    }

    public void setInitiatorOrganizationIdentifier(String initiatorOrganizationIdentifier)
    {
        this.initiatorOrganizationId = initiatorOrganizationIdentifier;
    }

    public List<T8FlowNodeStatus> getExecutingNodeStatuses()
    {
        return new ArrayList<>(executingNodeStatuses);
    }

    public void setExecutingNodeStatuses(ArrayList<T8FlowNodeStatus> executingNodeStatuses)
    {
        this.executingNodeStatuses.clear();
        if (executingNodeStatuses != null)
        {
            this.executingNodeStatuses.addAll(executingNodeStatuses);
        }
    }

    public String getParentFlowIid()
    {
        return parentFlowIid;
    }

    public void setParentFlowIid(String parentFlowIid)
    {
        this.parentFlowIid = parentFlowIid;
    }

    public T8FlowPhase getPhases()
    {
        return phases;
    }

    public void setPhases(T8FlowPhase phases)
    {
        this.phases = phases;
    }

    public List<T8TaskDetails> getAvailableTasks()
    {
        return new ArrayList<>(availableTasks);
    }

    public void setAvailableTasks(List<T8TaskDetails> availableTasks)
    {
        this.availableTasks.clear();
        if (availableTasks != null)
        {
            this.availableTasks.addAll(availableTasks);
        }
    }

    public String getCurrentPhaseDisplayString()
    {
        if (phases != null)
        {
            List<T8FlowPhase> inProgressPhases;
            Iterator<T8FlowPhase> phaseIterator;
            StringBuilder buffer;

            buffer = new StringBuilder();
            inProgressPhases = phases.getSubsequentInProgressPhases();
            phaseIterator = inProgressPhases.iterator();
            while (phaseIterator.hasNext())
            {
                T8FlowPhase phase;

                phase = phaseIterator.next();
                buffer.append(phase.getDisplayName());
                if (phaseIterator.hasNext()) buffer.append(", ");
            }

            return buffer.toString();
        }
        else return null;
    }

    public String getCurrentStepsDisplayString()
    {
        if (phases != null)
        {
            List<T8FlowPhase> inProgressPhases;
            Iterator<T8FlowPhase> phaseIterator;
            Iterator<String> stringIterator;
            List<String> strings;
            StringBuilder buffer;

            buffer = new StringBuilder();
            strings = new ArrayList<>();
            inProgressPhases = phases.getSubsequentInProgressPhases();
            phaseIterator = inProgressPhases.iterator();
            while (phaseIterator.hasNext())
            {
                T8FlowPhase phase;

                phase = phaseIterator.next();
                for (T8FlowPhaseNodeDetails nodeDetails : phase.getNodeDetails())
                {
                    if (nodeDetails.getNodeType() == FlowNodeType.ACTIVITY)
                    {
                        String displayName;

                        displayName = nodeDetails.getDisplayName();
                        if ((!Strings.isNullOrEmpty(displayName)) && (!strings.contains(displayName)))
                        {
                            strings.add(displayName);
                        }
                    }
                }
            }

            stringIterator = strings.iterator();
            while (stringIterator.hasNext())
            {
                buffer.append(stringIterator.next());
                if (stringIterator.hasNext()) buffer.append(", ");
            }

            return buffer.toString();
        }
        else return null;
    }

    public String getCurrentResponsiblePartiesDisplayString()
    {
        if (phases != null)
        {
            List<T8FlowPhase> inProgressPhases;
            Iterator<T8FlowPhase> phaseIterator;
            Iterator<String> stringIterator;
            List<String> strings;
            StringBuilder buffer;

            buffer = new StringBuilder();
            strings = new ArrayList<>();
            inProgressPhases = phases.getSubsequentInProgressPhases();
            phaseIterator = inProgressPhases.iterator();
            while (phaseIterator.hasNext())
            {
                T8FlowPhase phase;

                phase = phaseIterator.next();
                for (T8FlowPhaseUserDetails userDetails : phase.getUserDetails())
                {
                    String displayName;

                    displayName = userDetails.getDisplayName();
                    if ((!Strings.isNullOrEmpty(displayName)) && (!strings.contains(displayName)))
                    {
                        strings.add(displayName);
                    }
                }

                for (T8FlowPhaseProfileDetails profileDetails : phase.getProfileDetails())
                {
                    String displayName;

                    displayName = profileDetails.getDisplayName();
                    if ((!Strings.isNullOrEmpty(displayName)) && (!strings.contains(displayName)))
                    {
                        strings.add(displayName);
                    }
                }
            }

            // If not responsible parties were found, the server is responsible.
            if (strings.isEmpty())
            {
                strings.add("System");
            }

            stringIterator = strings.iterator();
            while (stringIterator.hasNext())
            {
                buffer.append(stringIterator.next());
                if (stringIterator.hasNext()) buffer.append(", ");
            }


            return buffer.toString();
        }
        else return null;
    }

    @Override
    public String toString()
    {
        return "T8FlowStatus{" + "flowIid=" + flowIid + ", flowId=" + flowId + '}';
    }
}
