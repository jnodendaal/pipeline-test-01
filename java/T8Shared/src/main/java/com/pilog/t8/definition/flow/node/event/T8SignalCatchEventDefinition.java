package com.pilog.t8.definition.flow.node.event;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This node catches a specific flow signal.
 *
 * @author Bouwer du Preez
 */
public class T8SignalCatchEventDefinition extends T8FlowEventDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_EVENT_SIGNAL_CATCH";
    public static final String DISPLAY_NAME = "Flow Signal Catch Event";
    public static final String DESCRIPTION = "An event that catches a specific workflow signal.";
    private enum Datum
    {
        SIGNAL_IDENTIFIER,
        KEY_PARAMETER_ID,
        PARAMETER_MAPPING,
        CONDITION_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public static String FLOW_IID_PARAMETER_ID = "$P_FLOW_IID";
    public static String FLOW_ID_PARAMETER_ID = "$P_FLOW_ID";

    public T8SignalCatchEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SIGNAL_IDENTIFIER.toString(), "Signal", "The identifier of the signal to catch."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.KEY_PARAMETER_ID.toString(), "Key Signal Parameter", "The key parameter (has to be a GUID) to use as the key that identifies the signal to catch."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.PARAMETER_MAPPING.toString(), "Parameter Mapping:  Signal to Flow", "A mapping of Signal Parameters to the corresponding Flow Parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated before the signal node is activated.  Use this to check signal parameters against flow parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SIGNAL_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FlowSignalDefinition.GROUP_IDENTIFIER));
        else if (Datum.KEY_PARAMETER_ID.toString().equals(datumIdentifier))
        {
            T8WorkFlowDefinition flowDefinition;

            flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
            if (flowDefinition != null)
            {
                ArrayList<T8DefinitionDatumOption> options;

                options = new ArrayList<>();
                options.add(new T8DefinitionDatumOption(FLOW_IID_PARAMETER_ID, FLOW_IID_PARAMETER_ID));
                options.add(new T8DefinitionDatumOption(FLOW_ID_PARAMETER_ID, FLOW_ID_PARAMETER_ID));
                options.addAll(createLocalIdentifierOptionsFromDefinitions(flowDefinition.getFlowParameterDefinitions()));
                return options;
            }
            else return new ArrayList<>();
        }
        else if (Datum.PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String signalId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            signalId = getSignalIdentifier();
            if (signalId != null)
            {
                T8FlowSignalDefinition signalDefinition;
                T8WorkFlowDefinition flowDefinition;

                signalDefinition = (T8FlowSignalDefinition)definitionContext.getRawDefinition(getRootProjectId(), signalId);
                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();

                if ((signalDefinition != null) && (flowDefinition != null))
                {
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    List<T8DataParameterDefinition> signalParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();
                    signalParameterDefinitions = signalDefinition.getParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((flowParameterDefinitions != null) && (signalParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition signalParameterDefinition : signalParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(signalParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.CONDITION_EXPRESSION.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.TextAreaDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getKeyParameterId()))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.KEY_PARAMETER_ID.toString(), "No Signal Key Parameter set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.event.T8SignalCatchEventNode").getConstructor(T8Context.class, T8Flow.class, T8SignalCatchEventDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    public String getSignalIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SIGNAL_IDENTIFIER.toString());
    }

    public void setSignalIdentifier(String taskIdentifier)
    {
        setDefinitionDatum(Datum.SIGNAL_IDENTIFIER.toString(), taskIdentifier);
    }

    public Map<String, String> getParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.PARAMETER_MAPPING.toString());
    }

    public void setParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.PARAMETER_MAPPING.toString(), mapping);
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public boolean evaluateConditionExpression(Map<String, Object> flowParameters, Map<String, Object> signalParameters) throws Exception
    {
        String conditionExpression;

        conditionExpression = getConditionExpression();
        if ((conditionExpression != null) && (conditionExpression.length() > 0))
        {
            Map<String, Object> expressionParameters;
            ExpressionEvaluator expressionEvaluator;

            expressionParameters = new HashMap<String, Object>();
            if (flowParameters != null) expressionParameters.putAll(flowParameters);
            if (signalParameters != null) expressionParameters.putAll(signalParameters);

            expressionEvaluator = new ExpressionEvaluator();
            return expressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null);
        }
        else return true;
    }

    public String getKeyParameterId()
    {
        return (String)getDefinitionDatum(Datum.KEY_PARAMETER_ID.toString());
    }

    public void setKeyParameterId(String id)
    {
        setDefinitionDatum(Datum.KEY_PARAMETER_ID.toString(), id);
    }
}
