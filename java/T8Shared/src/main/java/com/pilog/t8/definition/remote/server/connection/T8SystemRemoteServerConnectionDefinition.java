/**
 * Created on 09 Mar 2016, 12:03:08 PM
 */
package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8SystemRemoteServerConnectionDefinition extends T8ConnectionDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REMOTE_SERVER_CONNECTION_DETAIL_SYSTEM";
    public static final String DISPLAY_NAME = "Remote Server System Connection";
    public static final String DESCRIPTION = "This definition contains the information neccessary to make a system based connection to a remote server.";
    public enum Datum {
        REMOTE_USER_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public T8SystemRemoteServerConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.REMOTE_USER_DEFINITION.toString(), "Remote User Definition", "The definition of the remote user to be used when this connection is used by a call."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.REMOTE_USER_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RemoteConnectionUser.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {}

    @Override
    public T8Connection getNewConnectionInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.webservice.T8SystemRemoteWebServiceConnector", new Class<?>[]{T8Context.class, T8SystemRemoteServerConnectionDefinition.class}, context, this);
    }

    @Override
    public List<T8RemoteUserDefinition> getRemoteUserDefinitions()
    {
        return ArrayLists.typeSafeList(getRemoteUserDefinition());
    }

    public T8RemoteUserDefinition getRemoteUserDefinition()
    {
        return getDefinitionDatum(Datum.REMOTE_USER_DEFINITION);
    }

    public void setRemoteUserDefinition(T8RemoteUserDefinition remoteUserDefinition)
    {
        setDefinitionDatum(Datum.REMOTE_USER_DEFINITION, remoteUserDefinition);
    }
}