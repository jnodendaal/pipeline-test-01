package com.pilog.t8.definition.user.password;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8PasswordHistoryRuleDefinition extends T8PasswordPolicyRuleDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_PASSWORD_HISTORY_RULE";
    public static final String DISPLAY_NAME = "Password History Rules";
    public static final String DESCRIPTION = "A rule to be applied when validating password history.";

    public enum Datum {CHECK_LAST_X_PASSWORDS};
    // -------- Definition Meta-Data -------- //

    public T8PasswordHistoryRuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.CHECK_LAST_X_PASSWORDS.toString(), "Check last X passwords", "Validates the new password against the last x amount of passwords where x is the amount specified."));

        return datumTypes;
    }

    @Override
    public T8PasswordRuleValidator getValidator()
    {
        return T8Reflections.getInstance("com.pilog.t8.security.rulevalidator.T8PasswordHistoryRuleValidator", new Class<?>[]{T8PasswordHistoryRuleDefinition.class}, this);
    }

    public void setLastXAmount(Integer amount)
    {
        setDefinitionDatum(Datum.CHECK_LAST_X_PASSWORDS, amount);
    }

    public Integer getLastXAmount()
    {
        return getDefinitionDatum(Datum.CHECK_LAST_X_PASSWORDS);
    }
}
