package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtInteger extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@INTEGER";

    public T8DtInteger()
    {
    }

    public T8DtInteger(T8DefinitionManager context)
    {
    }

    public T8DtInteger(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return Integer.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        return JsonValue.valueOf(object);
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            return jsonValue.asInt();
        }
        else return null;
    }
}
