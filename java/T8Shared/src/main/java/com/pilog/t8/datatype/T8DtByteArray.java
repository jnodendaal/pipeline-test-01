package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.utilities.codecs.Base64Codec;

/**
 * @author Bouwer du Preez
 */
public class T8DtByteArray extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@BYTE_ARRAY";

    public T8DtByteArray()
    {
    }

    public T8DtByteArray(T8DefinitionManager context)
    {
    }

    public T8DtByteArray(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return byte[].class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            byte[] array;

            array = (byte[])object;
            return JsonValue.valueOf(Base64Codec.encode(array));
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                return Base64Codec.decode(jsonValue.asString());
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deserializing byte array from JSON.", e);
            }
        }
        else return null;
    }
}
