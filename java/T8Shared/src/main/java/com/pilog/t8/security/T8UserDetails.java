package com.pilog.t8.security;

import com.pilog.t8.time.T8Timestamp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8UserDetails implements Serializable
{
    private String id;
    private String username;
    private String name;
    private String surname;
    private String emailAddress;
    private String mobileNumber;
    private String workTelephoneNumber;
    private String languageId;
    private String organizationId;
    private Boolean active;
    private T8Timestamp expiryDate;
    private final List<T8UserProfileDetails> userProfileDetails;
    private final List<T8WorkflowProfileDetails> workflowProfileDetails;

    public T8UserDetails()
    {
        this.userProfileDetails = new ArrayList<>();
        this.workflowProfileDetails = new ArrayList<>();
    }

    public String getIdentifier()
    {
        return id;
    }

    public void setIdentifier(String identifier)
    {
        this.id = identifier;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getWorkTelephoneNumber()
    {
        return workTelephoneNumber;
    }

    public void setWorkTelephoneNumber(String workTelephoneNumber)
    {
        this.workTelephoneNumber = workTelephoneNumber;
    }

    public String getLanguageIdentifier()
    {
        return languageId;
    }

    public void setLanguageIdentifier(String languageIdentifier)
    {
        this.languageId = languageIdentifier;
    }

    public String getOrganizationIdentifier()
    {
        return organizationId;
    }

    public void setOrganizationIdentifier(String organizationIdentifier)
    {
        this.organizationId = organizationIdentifier;
    }

    public Boolean isActive()
    {
        return active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public List<T8UserProfileDetails> getUserProfileDetails()
    {
        return new ArrayList<>(this.userProfileDetails);
    }

    public List<String> getUserProfileIds()
    {
        List<String> profileIds;

        profileIds = new ArrayList<>();
        for (T8UserProfileDetails profileDetails : this.userProfileDetails)
        {
            profileIds.add(profileDetails.getId());
        }

        return profileIds;
    }

    public void setUserProfileIds(List<String> profileIds)
    {
        this.userProfileDetails.clear();
        for (String profileId : profileIds)
        {
            addUserProfileDetails(new T8UserProfileDetails(profileId, null, null));
        }
    }

    public void addUserProfileDetails(T8UserProfileDetails details)
    {
        this.userProfileDetails.add(details);
    }

    public List<T8WorkflowProfileDetails> getWorkflowProfileDetails()
    {
        return new ArrayList<>(this.workflowProfileDetails);
    }

    public List<String> getWorkFlowProfileIds()
    {
        List<String> profileIds;

        profileIds = new ArrayList<>();
        for (T8WorkflowProfileDetails profileDetails : this.workflowProfileDetails)
        {
            profileIds.add(profileDetails.getId());
        }

        return profileIds;
    }

    public void setWorkflowProfileIds(List<String> profileIds)
    {
        this.workflowProfileDetails.clear();
        for (String profileId : profileIds)
        {
            addWorkflowProfileDetails(new T8WorkflowProfileDetails(profileId, null, null));
        }
    }

    public void addWorkflowProfileDetails(T8WorkflowProfileDetails details)
    {
        this.workflowProfileDetails.add(details);
    }

    public T8Timestamp getExpiryDate()
    {
        return expiryDate;
    }

    public void setExpiryDate(T8Timestamp expiryDate)
    {
        this.expiryDate = expiryDate;
    }
}
