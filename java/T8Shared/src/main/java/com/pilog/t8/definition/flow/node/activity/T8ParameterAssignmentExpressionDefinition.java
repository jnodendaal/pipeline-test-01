package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ParameterAssignmentExpressionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_PARAMETER_ASSIGNMENT_EXPRESSION";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_PARAMETER_ASSIGNMENT_EXPRESSION";
    public static final String IDENTIFIER_PREFIX = "FPA_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Parameter Assignment Expression";
    public static final String DESCRIPTION = "An expression that will be evaluated in order to determine the value of a flow parameter.";
    public enum Datum {VALUE_EXPRESSION,
                       PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //
    
    public T8ParameterAssignmentExpressionDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PARAMETER_IDENTIFIER.toString(), "Parameter", "The identifier of the flow parameter to which the result of the expression will be assigned."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.VALUE_EXPRESSION.toString(), "Value Expression", "The expression to be evaluated."));
        return datumTypes;
    }
    
    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8WorkFlowDefinition flowDefinition;
            
            flowDefinition = (T8WorkFlowDefinition)this.getAncestorDefinition(T8WorkFlowDefinition.TYPE_IDENTIFIER);
            if (flowDefinition != null)
            {
                List<T8Definition> parameterDefinitions;
                
                parameterDefinitions = (List)flowDefinition.getFlowParameterDefinitions();
                return this.createLocalIdentifierOptionsFromDefinitions(parameterDefinitions);
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else return new ArrayList<T8DefinitionDatumOption>();
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getValueExpression()
    {
        return (String)getDefinitionDatum(Datum.VALUE_EXPRESSION.toString());
    }
    
    public void setValueExpression(String expression)
    {
        setDefinitionDatum(Datum.VALUE_EXPRESSION.toString(), expression);
    }
    
    public String getParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PARAMETER_IDENTIFIER.toString());
    }
    
    public void setParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
