package com.pilog.t8.definition.gfx.paint;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionEditor;
import java.awt.Color;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint.CycleMethod;
import java.awt.Paint;
import java.awt.geom.Point2D;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author Bouwer du Preez
 */
public class T8LinearGradientPaintDefinition extends T8PaintDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINT_LINEAR_GRADIENT";
    public static final String DISPLAY_NAME = "Linear Gradient Paint";
    public static final String DESCRIPTION = "A paint that draws a linear gradient color scale.  The gradient is defined by a collection of colors, each of which is assigned a corresponding fraction of the paint range.";
    public enum Datum {START_X,
                       START_Y,
                       END_X,
                       END_Y,
                       CYCLE_METHOD,
                       FRACTIONS,
                       COLORS};
    // -------- Definition Meta-Data -------- //

    public T8LinearGradientPaintDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.START_X.toString(), "Start X", "The relative gradient start x-coordinate.", 0f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.START_Y.toString(), "Start Y", "The relative gradient start y-coordinate.", 0f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.END_X.toString(), "End X", "The relative gradient end x-coordinate.", 0f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.FLOAT, Datum.END_Y.toString(), "End Y", "The relative gradient end y-coordinate.", 1f));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CYCLE_METHOD.toString(), "Cycle Method", "The method by which the gradient will be repeated when additional space is available.", CycleMethod.NO_CYCLE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.FLOAT), Datum.FRACTIONS.toString(), "Fractions", "The points on the gradient scale defined by the start and end coordinates where color changes occur."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.COLOR_HEX), Datum.COLORS.toString(), "Colors", "The colors of the gradient scale, cooresponding to the fractions when color changes occur."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CYCLE_METHOD.toString().equals(datumIdentifier)) return createStringOptions(CycleMethod.values());
        else return null;
    }

    @Override
    public T8DefinitionEditor getDefinitionEditor(T8DefinitionContext definitionContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.developer.definitions.gfx.paint.T8LinearGradientPaintDefinitionEditor").getConstructor(T8DefinitionContext.class, this.getClass());
        return (T8DefinitionEditor)constructor.newInstance(definitionContext, this);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public Paint getNewPaintInstance()
    {
        return new LinearGradientPaint(getStartX(), getStartY(), getEndX(), getEndY(), getFractions(), getColors(), getCycleMethod());
    }

    public void setPaint(LinearGradientPaint paint)
    {
        if (paint != null)
        {
            Point2D startPoint;
            Point2D endPoint;

            startPoint = paint.getStartPoint();
            endPoint = paint.getEndPoint();

            setStartX((float)startPoint.getX());
            setStartY((float)startPoint.getY());
            setEndX((float)endPoint.getX());
            setEndY((float)endPoint.getY());
            setFractions(paint.getFractions());
            setColors(paint.getColors());
            setCycleMethod(paint.getCycleMethod());
        }
    }

    public float getStartX()
    {
        return (Float)getDefinitionDatum(Datum.START_X.toString());
    }

    public void setStartX(float startX)
    {
        setDefinitionDatum(Datum.START_X.toString(), startX);
    }

    public float getStartY()
    {
        return (Float)getDefinitionDatum(Datum.START_Y.toString());
    }

    public void setStartY(float startY)
    {
        setDefinitionDatum(Datum.START_Y.toString(), startY);
    }

    public float getEndX()
    {
        return (Float)getDefinitionDatum(Datum.END_X.toString());
    }

    public void setEndX(float endX)
    {
        setDefinitionDatum(Datum.END_X.toString(), endX);
    }

    public float getEndY()
    {
        return (Float)getDefinitionDatum(Datum.END_Y.toString());
    }

    public void setEndY(float endY)
    {
        setDefinitionDatum(Datum.END_Y.toString(), endY);
    }

    public CycleMethod getCycleMethod()
    {
        return CycleMethod.valueOf((String)getDefinitionDatum(Datum.CYCLE_METHOD.toString()));
    }

    public void setCycleMethod(CycleMethod cycleMethod)
    {
        setDefinitionDatum(Datum.CYCLE_METHOD.toString(), cycleMethod.toString());
    }

    public float[] getFractions()
    {
        List<Float> fractionList;
        float[] fractions;

        fractionList = (List<Float>)getDefinitionDatum(Datum.FRACTIONS.toString());
        if (fractionList != null)
        {
            fractions = new float[fractionList.size()];
            for (int fractionIndex = 0; fractionIndex < fractionList.size(); fractionIndex++)
            {
                fractions[fractionIndex] = fractionList.get(fractionIndex);
            }

            return fractions;
        }
        else
        {
            return new float[]{0f, 1f};
        }
    }

    public void setFractions(float[] fractions)
    {
        ArrayList<Float> fractionList;

        fractionList = new ArrayList<Float>();
        for (float fraction : fractions) fractionList.add(fraction);
        setDefinitionDatum(Datum.FRACTIONS.toString(), fractionList);
    }

    public Color[] getColors()
    {
        List<String> colorHexList;
        Color[] colors;

        colorHexList = (List<String>)getDefinitionDatum(Datum.COLORS.toString());
        if (colorHexList != null)
        {
            colors = new Color[colorHexList.size()];
            for (int colorIndex = 0; colorIndex < colorHexList.size(); colorIndex++)
            {
                colors[colorIndex] = T8DefinitionDatumUtilities.getColor(colorHexList.get(colorIndex));
            }

            return colors;
        }
        else
        {
            return new Color[]{Color.WHITE, Color.BLACK};
        }
    }

    public void setColors(Color[] colors)
    {
        ArrayList<String> colorHexList;

        colorHexList = new ArrayList<String>();
        if (colors != null)
        {
            for (Color color : colors)
            {
                colorHexList.add(Integer.toHexString(color.getRGB()));
            }
        }

        setDefinitionDatum(Datum.COLORS.toString(), colorHexList);
    }
}
