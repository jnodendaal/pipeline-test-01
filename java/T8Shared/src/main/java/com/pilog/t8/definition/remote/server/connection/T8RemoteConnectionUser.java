package com.pilog.t8.definition.remote.server.connection;

/**
 * @author Hennie Brink
 */
public interface T8RemoteConnectionUser
{
    public static final String GROUP_IDENTIFIER = "@DG_REMOTE_SERVER_CONNECTION_USER";
    public static final String IDENTIFIER_PREFIX = "RU_";

    public String getPublicIdentifier();

    public String getApiKey();
}
