package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final Map<String, Object> componentParameters;
    private String flowIid;
    private String taskIid;
    private String componentUri;
    private String iconUri;
    private boolean createViewComponent;

    public T8WorkflowFunctionalityAccessHandle(String functionalityId, String functionalityIid, String flowIid, String taskIid, String displayName, String description, Icon icon, boolean createViewComponent)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.flowIid = flowIid;
        this.taskIid = taskIid;
        this.createViewComponent = createViewComponent;
        this.componentParameters = new HashMap<>();
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        if (createViewComponent)
        {
            T8FunctionalityView view;

            view = T8Reflections.getFastFailInstance("com.pilog.t8.functionality.view.T8WorkFlowFunctionalityView", new Class<?>[]{T8ComponentController.class, T8WorkflowFunctionalityAccessHandle.class, String.class, String.class}, controller, this, flowIid, taskIid);
            view.addFunctionalityViewListener(viewListener);

            return view;
        }
        else return null;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return false;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String flowIid)
    {
        this.flowIid = flowIid;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public void setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
    }

    public String getComponentUri()
    {
        return componentUri;
    }

    public void setComponentUri(String componentUri)
    {
        this.componentUri = componentUri;
    }

    public String getIconUri()
    {
        return iconUri;
    }

    public void setIconUri(String iconUri)
    {
        this.iconUri = iconUri;
    }

    public boolean isCreateViewComponent()
    {
        return createViewComponent;
    }

    public void setCreateViewComponent(boolean createViewComponent)
    {
        this.createViewComponent = createViewComponent;
    }

    public Map<String, Object> getComponentParameters()
    {
        return new HashMap<>(componentParameters);
    }

    public void setComponentParameters(Map<String, Object> parameters)
    {
        this.componentParameters.clear();
        if (parameters != null)
        {
            this.componentParameters.putAll(parameters);
        }
    }
}
