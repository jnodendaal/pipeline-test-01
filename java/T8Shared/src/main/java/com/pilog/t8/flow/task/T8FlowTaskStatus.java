package com.pilog.t8.flow.task;

import com.pilog.t8.flow.task.T8FlowTask.TaskStatus;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskStatus implements Serializable
{
    private String taskIdentifier;
    private String taskInstanceIdentifier;
    private TaskStatus status;
    private int progress;
    private String statusMessage;
    
    public T8FlowTaskStatus(String taskIdentifier, String taskInstanceIdentifier, TaskStatus status, int progress, String statusMessage)
    {
        this.taskIdentifier = taskIdentifier;
        this.taskInstanceIdentifier = taskInstanceIdentifier;
        this.status = status;
        this.progress = progress;
        this.statusMessage = statusMessage;
    }

    public String getTaskIdentifier()
    {
        return taskIdentifier;
    }

    public void setTaskIdentifier(String taskIdentifier)
    {
        this.taskIdentifier = taskIdentifier;
    }
    
    public TaskStatus getStatus()
    {
        return status;
    }

    public void setStatus(TaskStatus status)
    {
        this.status = status;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskInstanceIdentifier;
    }

    public void setTaskInstanceIdentifier(String taskInstanceIdentifier)
    {
        this.taskInstanceIdentifier = taskInstanceIdentifier;
    }

    public int getProgress()
    {
        return progress;
    }

    public void setProgress(int progress)
    {
        this.progress = progress;
    }

    public String getStatusMessage()
    {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage)
    {
        this.statusMessage = statusMessage;
    }
}
