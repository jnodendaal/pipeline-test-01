/**
 * Created on 04 May 2015, 2:25:21 PM
 */
package com.pilog.t8.definition.help;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.help.T8HelpDisplay;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8HTMLHelpDisplayDefinition extends T8HelpDisplayDefinition
{
    private static final T8Logger logger = T8Log.getLogger(T8HTMLHelpDisplayDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_HELP_HTML";
    public static final String DISPLAY_NAME = "HTML Help Resource";
    public static final String DESCRIPTION = "A help resource in the form of a link to be called.";
    public enum Datum {URL};
    // -------- Definition Meta-Data -------- //

    public T8HTMLHelpDisplayDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8HelpDisplay getHelpDisplayInstance()
    {
        return T8Reflections.getInstance("com.pilog.t8.help.T8HTMLHelpDisplay", new Class<?>[]{T8HTMLHelpDisplayDefinition.class}, this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>(1);
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.URL.toString(), "Help URL", "The URL link which will be used to display relevant Help Data."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public URL getHelpURL()
    {
        String urlString = null;

        try
        {
            urlString = getHelpURLString();

            if (urlString == null) return null;
            else return new URL(getEncodedQueryURL(urlString));
        }
        catch (UnsupportedEncodingException | MalformedURLException ex)
        {
            logger.log("Invalid URL details specified.", ex);
            throw new IllegalStateException("The definition URL : " + urlString + " is an invalid URL, or the encoding used is invalid.", ex);
        }
    }

    private String getEncodedQueryURL(String urlString) throws UnsupportedEncodingException
    {
        String newUrlString;

        newUrlString = urlString.substring(urlString.lastIndexOf('/')+1);
        newUrlString = URLEncoder.encode(newUrlString, "UTF-8");
        newUrlString = urlString.substring(0, urlString.lastIndexOf('/')+1).concat(newUrlString);

        return newUrlString;
    }

    public void setHelpURL(URL url)
    {
        setHelpURLString(url.toString());
    }

    /**
     * A convenience method to {@link #getHelpURL()} when required to work with
     * a {@code String} value rather than an actual {@code URL} object.
     *
     * @return The {@code String} URL representation
     */
    public String getHelpURLString()
    {
        return getDefinitionDatum(Datum.URL);
    }

    /**
     * A convenience method to {@link #setHelpURL(java.net.URL)} when required
     * to work with a {@code String} rather than an actual {@code URL} object.
     *
     * @param url The {@code String} URL representation
     */
    public void setHelpURLString(String url)
    {
        setDefinitionDatum(Datum.URL, url);
    }
}