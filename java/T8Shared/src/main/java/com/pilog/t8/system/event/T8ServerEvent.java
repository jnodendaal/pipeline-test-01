package com.pilog.t8.system.event;

import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ServerEvent extends EventObject
{
    public T8ServerEvent(Object source)
    {
        super(source);
    }
}
