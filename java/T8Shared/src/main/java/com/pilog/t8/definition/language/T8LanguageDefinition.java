package com.pilog.t8.definition.language;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LanguageDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_LANGUAGE";
    public static final String GROUP_IDENTIFIER = "@DG_LANGUAGE";
    public static final String STORAGE_PATH = "/languages";
    public static final String DISPLAY_NAME = "Language";
    public static final String DESCRIPTION = "A language as defined by the T8 framework.";
    public static final String IDENTIFIER_PREFIX = "L_";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.SYSTEM;
    public enum Datum {BCP47_LANGUAGE_TAG, CONTENT_LANGUAGE_ID};
    // -------- Definition Meta-Data -------- //
    
    public T8LanguageDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BCP47_LANGUAGE_TAG.toString(), "BCP47 Language Tag", "The language tag defined by the IETF uniquely identifying this language."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONTENT_LANGUAGE_ID.toString(), "Content Language ID", "The language ID uniquely identifying this language as used in the content data of the system."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getBCP47LanguageTag()
    {
        return (String)getDefinitionDatum(Datum.BCP47_LANGUAGE_TAG.toString());
    }
    
    public void setBCP47LanguageTag(String tag)
    {
        setDefinitionDatum(Datum.BCP47_LANGUAGE_TAG.toString(), tag);
    }
    
    public String getContentLanguageID()
    {
        return (String)getDefinitionDatum(Datum.CONTENT_LANGUAGE_ID.toString());
    }
    
    public void setContentLanguageID(String identifier)
    {
        setDefinitionDatum(Datum.CONTENT_LANGUAGE_ID.toString(), identifier);
    }
}
