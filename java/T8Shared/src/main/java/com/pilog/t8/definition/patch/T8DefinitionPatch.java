package com.pilog.t8.definition.patch;

import com.pilog.t8.utilities.strings.StringUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionPatch implements Serializable
{
    private final T8PatchDefinition definition;
    private final List<T8DefinitionStringPatch> definitionStringPatches;
    private final Map<String, T8DefinitionTypePatch> definitionTypePatches;

    public T8DefinitionPatch(T8PatchDefinition definition)
    {
        this.definition = definition;
        this.definitionStringPatches = new ArrayList<T8DefinitionStringPatch>();
        this.definitionTypePatches = new HashMap<String, T8DefinitionTypePatch>();
        createStringPatches();
        createTypePatches();
    }

    public boolean isEnabled()
    {
        return definition.isEnabled();
    }

    private void createStringPatches()
    {
        definitionStringPatches.clear();

        // Add string replacements.
        for (T8DefinitionStringPatchDefinition stringPatchDefinition : definition.getStringReplacements())
        {
            String prePattern;
            String postPattern;
            String targetPattern;
            String replacementString;

            prePattern = stringPatchDefinition.getPrePattern();
            postPattern = stringPatchDefinition.getPostPattern();
            targetPattern = stringPatchDefinition.getTargetPattern();
            replacementString = stringPatchDefinition.getReplacementString();
            definitionStringPatches.add(new T8DefinitionStringPatch(prePattern, targetPattern, postPattern, replacementString));
        }

        // Add String mappings.
        for (T8StringMappingPatchDefinition stringMap : definition.getStringMaps())
        {
            Map<String, String> replacementMap;

            replacementMap = stringMap.getStringMap();
            if (replacementMap != null)
            {
                for (String targetString : replacementMap.keySet())
                {
                    String prePattern;
                    String postPattern;
                    String targetPattern;
                    String replacementString;

                    prePattern = null;
                    postPattern = null;
                    targetPattern = targetString;
                    replacementString = replacementMap.get(targetString);
                    definitionStringPatches.add(new T8DefinitionStringPatch(prePattern, targetPattern, postPattern, replacementString));
                }
            }
        }
    }

    private void createTypePatches()
    {
        definitionTypePatches.clear();
        for (T8DefinitionTypePatchDefinition typePatchDefinition : definition.getTypePatches())
        {
            String typeIdentifier;
            String newTypeIdentifier;
            Map<String, String> datumIdentifierMap;

            typeIdentifier = typePatchDefinition.getDefinitionTypeIdentifier();
            newTypeIdentifier = typePatchDefinition.getNewDefinitionTypeIdentifier();
            datumIdentifierMap = typePatchDefinition.getDefinitionDatumMap();
            definitionTypePatches.put(typeIdentifier, new T8DefinitionTypePatch(typeIdentifier, newTypeIdentifier, datumIdentifierMap));
        }
    }

    public String patchTypeIdentifier(String typeIdentifier)
    {
        T8DefinitionTypePatch definitionTypePatch;

        definitionTypePatch = definitionTypePatches.get(typeIdentifier);
        if (definitionTypePatch != null)
        {
            String newTypeIdentifier;

            newTypeIdentifier = definitionTypePatch.getNewTypeIdentifier();
            if (newTypeIdentifier != null) return newTypeIdentifier;
            else return typeIdentifier;
        }
        else return typeIdentifier;
    }

    public boolean patchDefinitionData(String typeIdentifier, Map<String, Object> definitionData)
    {
        T8DefinitionTypePatch definitionTypePatch;
        boolean patched;

        patched = false;
        definitionTypePatch = definitionTypePatches.get(typeIdentifier);
        if (definitionTypePatch != null)
        {
            Map<String, String> datumIdentifierMap;

            datumIdentifierMap = definitionTypePatch.getDatumIdentifierMap();
            if (datumIdentifierMap != null)
            {
                for (String datumIdentifier : datumIdentifierMap.keySet())
                {
                    if (definitionData.containsKey(datumIdentifier))
                    {
                        String newDatumIdentifier;

                        patched = true;
                        newDatumIdentifier = datumIdentifierMap.get(datumIdentifier);
                        definitionData.put(newDatumIdentifier, definitionData.get(datumIdentifier));
                    }
                }
            }

            return patched;
        }
        else return false;
    }

    public String patchDefinitionString(String definitionString)
    {
        StringBuffer stringBuffer;

        stringBuffer = new StringBuffer(definitionString);
        for (T8DefinitionStringPatch stringPatch : definitionStringPatches)
        {
            StringUtilities.replaceAll(stringPatch.getPrePattern(), stringPatch.getTargetPattern(), stringPatch.getPostPattern(), stringBuffer, stringPatch.getReplacementString());
        }

        return stringBuffer.toString();
    }
}
