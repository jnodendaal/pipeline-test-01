package com.pilog.t8.flow.state;

import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey.WaitKeyIdentifier;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowNodeStateWaitKeyList implements Serializable
{
    private final List<T8FlowNodeStateWaitKey> waitKeys;
    private final List<T8FlowNodeStateWaitKey> deletedKeys;
    private final T8FlowNodeState nodeState;

    public T8FlowNodeStateWaitKeyList(T8FlowNodeState nodeState)
    {
        this.nodeState = nodeState;
        this.waitKeys = new ArrayList<T8FlowNodeStateWaitKey>();
        this.deletedKeys = new ArrayList<T8FlowNodeStateWaitKey>();
    }

    public int size()
    {
        return waitKeys.size();
    }

    public void addUpdates(T8FlowStateUpdates updates)
    {
        // Add key updates.
        for (T8FlowNodeStateWaitKey waitKey : waitKeys)
        {
            waitKey.addUpdates(updates);
        }

        // Add key deletions.
        for (T8FlowNodeStateWaitKey waitKey : deletedKeys)
        {
            updates.addDeletion(waitKey.createEntity(updates.getNodeStateWaitKeyEntityDefinition()));
        }
    }

    public void statePersisted()
    {
        for (T8FlowNodeStateWaitKey waitKey : waitKeys)
        {
            waitKey.statePersisted();
        }

        deletedKeys.clear();
    }

    public void clear()
    {
        for (T8FlowNodeStateWaitKey waitKey : waitKeys)
        {
            deletedKeys.add(waitKey);
        }

        waitKeys.clear();
    }

    protected void add(T8FlowNodeStateWaitKey waitKey)
    {
        T8FlowNodeStateWaitKey keyToUpdate;

        // First try to find and existing key that key that can be updated.
        keyToUpdate = getKey(waitKey.getKeyIdentifier());
        if (keyToUpdate == null)
        {
            keyToUpdate = getDeletedKey(waitKey.getKeyIdentifier());
            if (keyToUpdate != null)
            {
                deletedKeys.remove(keyToUpdate);
                waitKeys.add(keyToUpdate);
            }
        }

        // If we found a key to update, do it.
        if (keyToUpdate != null)
        {
            keyToUpdate.setKeyFlowInstanceIdentifier(waitKey.getKeyFlowInstanceIdentifier());
            keyToUpdate.setKeyFlowIdentifier(waitKey.getKeyFlowIdentifier());
            keyToUpdate.setKeyNodeInstanceIdentifier(waitKey.getKeyNodeInstanceIdentifier());
            keyToUpdate.setKeyNodeIdentifier(waitKey.getKeyNodeIdentifier());
            keyToUpdate.setKeyTaskInstanceIdentifier(waitKey.getKeyTaskInstanceIdentifier());
            keyToUpdate.setKeyTaskIdentifier(waitKey.getKeyTaskIdentifier());
            keyToUpdate.setKeyDataObjectInstanceIdentifier(waitKey.getKeyDataObjectInstanceIdentifier());
            keyToUpdate.setKeyDataObjectIdentifier(waitKey.getKeyDataObjectIdentifier());
            keyToUpdate.setKeySignalIdentifier(waitKey.getKeySignalIdentifier());
            keyToUpdate.setKeySignalParameter(waitKey.getKeySignalParameter());
        }
        else
        {
            // If we could not find a key to update, insert a new one.
            waitKeys.add(waitKey);
        }
    }

    public T8FlowNodeStateWaitKey getKey(WaitKeyIdentifier identifier)
    {
        for (T8FlowNodeStateWaitKey waitKey : waitKeys)
        {
            if (waitKey.getKeyIdentifier().equals(identifier))
            {
                return waitKey;
            }
        }

        return null;
    }

    private T8FlowNodeStateWaitKey getDeletedKey(WaitKeyIdentifier identifier)
    {
        for (T8FlowNodeStateWaitKey waitKey : waitKeys)
        {
            if (waitKey.getKeyIdentifier().equals(identifier))
            {
                return waitKey;
            }
        }

        return null;
    }
}
