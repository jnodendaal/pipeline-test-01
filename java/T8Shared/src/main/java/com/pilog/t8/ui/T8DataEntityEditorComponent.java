package com.pilog.t8.ui;

import com.pilog.t8.data.T8DataEntity;

/**
 * @author Bouwer du Preez
 */
public interface T8DataEntityEditorComponent extends T8Component
{
    public String getEditorDataEntityIdentifier();
    public String getParentEditorIdentifier();
    public void setEditorDataEntity(T8DataEntity dataEntity);
    public T8DataEntity getEditorDataEntity();
    public boolean commitEditorChanges();
}
