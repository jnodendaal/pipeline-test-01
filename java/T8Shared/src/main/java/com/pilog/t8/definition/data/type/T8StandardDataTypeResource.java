package com.pilog.t8.definition.data.type;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.datatype.T8DtBoolean;
import com.pilog.t8.datatype.T8DtCustomObject;
import com.pilog.t8.datatype.T8DtDate;
import com.pilog.t8.datatype.T8DtDefinition;
import com.pilog.t8.datatype.T8DtDouble;
import com.pilog.t8.datatype.T8DtFloat;
import com.pilog.t8.datatype.T8DtImage;
import com.pilog.t8.datatype.T8DtInteger;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtLong;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtUndefined;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.datatype.T8DtTime;
import com.pilog.t8.datatype.T8DtTimestamp;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8StandardDataTypeResource implements T8DefinitionResource
{
    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<T8Definition>();
        if (typeIdentifiers.contains(T8DataTypeDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataTypeDefinitions());
        return definitions;
    }

    public List<T8Definition> getDataTypeDefinitions()
    {
        T8DataTypeDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<T8Definition>();

        // Data Type Definitions.
        definition = new T8DataTypeDefinition(T8DtUndefined.IDENTIFIER);
        definition.setMetaDisplayName("Null");
        definition.setMetaDescription("A special case data type used to identify a scenario where no data type is defined.");
        definition.setClassName("com.pilog.t8.data.type.T8NullDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtCustomObject.IDENTIFIER);
        definition.setMetaDisplayName("Custom Object");
        definition.setMetaDescription("A data type used for data that does not map to any of the other types.");
        definition.setClassName("com.pilog.t8.data.type.T8CustomObjectDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtDefinition.IDENTIFIER);
        definition.setMetaDisplayName("Definition");
        definition.setMetaDescription("A data type used to identify T8Definition objects.");
        definition.setClassName("com.pilog.t8.data.type.T8DefinitionDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtString.IDENTIFIER);
        definition.setMetaDisplayName("String");
        definition.setMetaDescription("A data type corresponding to the Java String primitive type.");
        definition.setClassName("com.pilog.t8.data.type.T8StringDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtBoolean.IDENTIFIER);
        definition.setMetaDisplayName("Boolean");
        definition.setMetaDescription("A data type corresponding to the Java Boolean primitive type.");
        definition.setClassName("com.pilog.t8.data.type.T8BooleanDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtInteger.IDENTIFIER);
        definition.setMetaDisplayName("Integer");
        definition.setMetaDescription("A data type corresponding to the Java Integer primitive type.");
        definition.setClassName("com.pilog.t8.data.type.T8IntegerDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtLong.IDENTIFIER);
        definition.setMetaDisplayName("Long");
        definition.setMetaDescription("A data type corresponding to the Java Long primitive type.");
        definition.setClassName("com.pilog.t8.data.type.T8LongDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtFloat.IDENTIFIER);
        definition.setMetaDisplayName("Float");
        definition.setMetaDescription("A data type corresponding to the Java Float primitive type.");
        definition.setClassName("com.pilog.t8.data.type.T8FloatDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtDouble.IDENTIFIER);
        definition.setMetaDisplayName("Double");
        definition.setMetaDescription("A data type corresponding to the Java Double primitive type.");
        definition.setClassName("com.pilog.t8.data.type.T8DoubleDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtTime.IDENTIFIER);
        definition.setMetaDisplayName("Time");
        definition.setMetaDescription("A data type used for a Time-of-day value.");
        definition.setClassName("com.pilog.t8.data.type.T8TimeDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtDate.IDENTIFIER);
        definition.setMetaDisplayName("Date");
        definition.setMetaDescription("A data type used for a calendar date value (withtout time of day).");
        definition.setClassName("com.pilog.t8.data.type.T8DateDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtTimestamp.IDENTIFIER);
        definition.setMetaDisplayName("Timestamp");
        definition.setMetaDescription("A data type used for a calendar date with time-of-day value.");
        definition.setClassName("com.pilog.t8.data.type.T8TimestampDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtImage.IDENTIFIER);
        definition.setMetaDisplayName("Image");
        definition.setMetaDescription("A data type used for a T8Image value.");
        definition.setClassName("com.pilog.t8.data.type.T8ImageDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtList.IDENTIFIER);
        definition.setMetaDisplayName("List");
        definition.setMetaDescription("A data type used for a List collection.");
        definition.setClassName("com.pilog.t8.data.type.T8ListDataType");
        definitions.add(definition);

        definition = new T8DataTypeDefinition(T8DtMap.IDENTIFIER);
        definition.setMetaDisplayName("Map");
        definition.setMetaDescription("A data type used for a Map collection.");
        definition.setClassName("com.pilog.t8.data.type.T8MapDataType");
        definitions.add(definition);

        return definitions;
    }
}
