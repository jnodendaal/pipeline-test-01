package com.pilog.t8.definition.gfx.image;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8IconDefinition extends T8ImageDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_GFX_ICON";
    public static final String GROUP_NAME = "Icons";
    public static final String GROUP_DESCRIPTION = "Definitions of icons used by the vairous user interfaces in the system.";
    public static final String STORAGE_PATH = "/icon_definitions";
    public static final String IDENTIFIER_PREFIX = "ICN_";
    // -------- Definition Meta-Data -------- //

    public T8IconDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return new ArrayList<T8DefinitionDatumType>();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }
}
