package com.pilog.t8.definition.operation;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.documentation.T8DefaultDefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationType;
import com.pilog.t8.definition.data.T8DataValueDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8ServerOperationDocumentationProvider extends T8DefaultDefinitionDocumentationProvider
{
    public static final String OPERATION_API_DOCUMENTATION_TYPE_IDENTIFIER = "OPERATION_API";
    
    public T8ServerOperationDocumentationProvider(T8DefinitionContext definitionContext)
    {
        super(definitionContext);
        documentationTypes.put(OPERATION_API_DOCUMENTATION_TYPE_IDENTIFIER, new T8DefinitionDocumentationType(OPERATION_API_DOCUMENTATION_TYPE_IDENTIFIER, "Operation API", "Documentation describing the API of the T8 Server Operation."));
    }
    
    @Override
    public StringBuffer getHTMLDocumentation(T8Definition definition, String typeIdentifier)
    {
        if (OPERATION_API_DOCUMENTATION_TYPE_IDENTIFIER.equals(typeIdentifier))
        {
            return createOperationAPIDocumentationString(definitionContext, (T8ServerOperationDefinition)definition);
        }
        else return super.getHTMLDocumentation(definition, typeIdentifier);
    }
    
    protected StringBuffer createOperationAPIDocumentationString(T8DefinitionContext definitionContext, T8ServerOperationDefinition operationDefinition)
    {
        StringBuffer documentation;
        
        documentation = new StringBuffer();
        documentation.append("<h1>Operation API: " + operationDefinition.getIdentifier() + "</h1>");
        documentation.append("</br>");
        documentation.append(createDefinitionDetailsString(definitionContext, operationDefinition));
        documentation.append("</br>");
        documentation.append(createOperationDocumentationString(definitionContext, operationDefinition));
        return documentation;
    }
    
    protected StringBuffer createOperationDocumentationString(T8DefinitionContext definitionContext, T8ServerOperationDefinition operationDefinition)
    {
        StringBuffer documentation;
        
        documentation = new StringBuffer();
        documentation.append("<h2>Operation Details</h2>");
        documentation.append("<table border=\"1\">");
        documentation.append("<tr><td>Operation Identifier</td><td>" + operationDefinition.getIdentifier() + "</td></tr>");
        documentation.append("<tr><td>Operation Name</td><td>" + operationDefinition.getMetaDisplayName() + "</td></tr>");
        documentation.append("<tr><td>Description</td><td>" + operationDefinition.getMetaDescription() + "</td></tr>");

        // Add the input parameter section.
        documentation.append("<tr>");
        documentation.append("<td>Input Parameters</td>");
        documentation.append("<td>");
        documentation.append("<ul>");
        for (T8DataValueDefinition parameterDefinition : operationDefinition.getInputParameterDefinitions())
        {
            documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
        }
        documentation.append("</ul>");
        documentation.append("</td>");
        documentation.append("</tr>");

        // Add the output parameter section.
        documentation.append("<tr>");
        documentation.append("<td>Output Parameters</td>");
        documentation.append("<td>");
        documentation.append("<ul>");
        for (T8DataValueDefinition parameterDefinition : operationDefinition.getOutputParameterDefinitions())
        {
            documentation.append("<li>" + parameterDefinition.getIdentifier() + ": " + parameterDefinition.getMetaDescription() + "</li>");
        }
        documentation.append("</ul>");
        documentation.append("</td>");
        documentation.append("</tr>");
        documentation.append("</table>");
        documentation.append("</br>");
        
        return documentation;
    }
}
