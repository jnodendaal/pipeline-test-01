/**
 * Created on 24 Jun 2016, 11:55:46 AM
 */
package com.pilog.t8.exception;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;

/**
 * @author Gavin Boshoff
 */
public class T8ThrowableSerializer
{
    public static JsonValue serializeToJson(Throwable error, boolean includeStack, boolean includeCause)
    {
        JsonObject errorObject;
        Throwable cause;

        errorObject = new JsonObject();
        errorObject.add("message", error.getMessage());
        if (includeStack)
        {
            StringBuilder stackBuilder;

            stackBuilder = new StringBuilder();
            for (StackTraceElement stackElement : error.getStackTrace())
            {
                stackBuilder.append(stackElement.getClassName());
                stackBuilder.append(":(").append(stackElement.getLineNumber()).append(")\n");
            }
            errorObject.add("stack", stackBuilder.toString());
        }

        if (includeCause && (cause = error.getCause()) != null)
        {
            errorObject.add("cause", serializeToJson(cause, includeStack, includeCause));
        }

        return errorObject;
    }
}