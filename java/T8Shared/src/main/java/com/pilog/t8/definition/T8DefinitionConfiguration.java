package com.pilog.t8.definition;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionConfiguration
{
    private Map<String, Object> configurationMap;
    private String definitionIdentifier;
    
    public T8DefinitionConfiguration(String definitionIdentifier)
    {
        this.definitionIdentifier = definitionIdentifier;
    }
    
    public String getDefinitionIdentifier()
    {
        return definitionIdentifier;
    }
    
    public void setConfigurationMap(Map<String, Object> configurationMap)
    {
        this.configurationMap = configurationMap;
    }
    
    public Map<String, Object> getConfigurationMap()
    {
        return configurationMap;
    }
    
    public void addDatumConfiguration(String datumIdentifier, Object datumValue)
    {
        if (configurationMap == null) configurationMap = new HashMap<String, Object>();
        configurationMap.put(datumIdentifier, datumValue);
    }
    
    public void configureDefinition(T8Definition definition)
    {
        if (configurationMap != null)
        {
            if (definition.getPublicIdentifier().equals(definitionIdentifier))
            {
                definition.setDefinitionData(configurationMap);
            }
            else throw new RuntimeException("Input definition '" + definition + "' does not match configuration: " + definitionIdentifier);
        }
    }
}
