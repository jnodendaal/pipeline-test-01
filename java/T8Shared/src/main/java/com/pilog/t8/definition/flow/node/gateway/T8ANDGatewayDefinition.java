package com.pilog.t8.definition.flow.node.gateway;

import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ANDGatewayDefinition extends T8FlowGatewayDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_GATEWAY_AND";
    public static final String DISPLAY_NAME = "AND Gateway";
    public static final String DESCRIPTION = "A gateway that splits execution into parallel paths that are execution simultaneously.";
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8ANDGatewayDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.gateway.T8ANDGatewayNode").getConstructor(T8Context.class, T8Flow.class, T8ANDGatewayDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }
}
