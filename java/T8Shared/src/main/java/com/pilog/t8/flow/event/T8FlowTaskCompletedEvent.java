package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.T8JsonParameterSerializer;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey;
import com.pilog.t8.flow.key.T8TaskCompletionExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskCompletedEvent extends T8FlowEvent
{
    private T8FlowTaskDefinition taskDefinition;
    private final String projectId;
    private final String taskIid;
    private final Map<String, Object> taskOutputParameters;

    public static final String TYPE_ID = "TASK_COMPLETED";

    public T8FlowTaskCompletedEvent(T8FlowTaskDefinition taskDefinition, String taskInstanceIdentifier, Map<String, Object> taskOutputParameters)
    {
        super(taskInstanceIdentifier);
        this.taskDefinition = taskDefinition;
        this.projectId = taskDefinition.getRootProjectId();
        this.taskIid = taskInstanceIdentifier;
        this.taskOutputParameters = taskOutputParameters;
    }

    public T8FlowTaskCompletedEvent(T8FlowController controller, JsonObject jsonObject)
    {
        super(jsonObject.getString("taskId"));

        try
        {
            T8JsonParameterSerializer parameterSerializer;

            parameterSerializer = new T8JsonParameterSerializer();
            this.projectId = jsonObject.getString("projectId");
            this.taskIid = jsonObject.getString("taskIid");
            this.taskDefinition = controller.getTaskDefinition(projectId, jsonObject.getString("taskId"));
            this.taskOutputParameters = parameterSerializer.deserializeParameters(taskDefinition.getOutputParameterDefinitions(), jsonObject.getJsonObject("parameters"));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while deserializing task completion event.", e);
        }
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskIid;
    }

    public Map<String, Object> getTaskOutputParameters()
    {
        return taskOutputParameters;
    }

    @Override
    public T8FlowExecutionKey getExecutionKey(T8WorkFlowNodeDefinition waitingNodeDefinition, T8FlowNodeState waitingNodeState)
    {
        T8FlowTaskState taskState;

        taskState = waitingNodeState.getTaskState();
        if (taskState != null)
        {
            // Check that the task IID's match.
            if (taskState.getTaskIid().equals(taskIid))
            {
                return new T8TaskCompletionExecutionKey(waitingNodeState, taskIid, taskOutputParameters);
            }
            else return null;
        }
        else throw new RuntimeException("No task state found in node state waiting for task '" + taskIid + "' claim:" + waitingNodeState);
    }

    @Override
    public T8DataFilterCriteria getWaitKeyFilterCriteria()
    {
        Map<String, Object> filterMap;

        filterMap = new HashMap<>();
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_ID", T8FlowNodeStateWaitKey.WaitKeyIdentifier.TASK_COMPLETION.toString());
        filterMap.put(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_TASK_IID", taskIid);
        return new T8DataFilterCriteria(filterMap);
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        try
        {
            T8JsonParameterSerializer parameterSerializer;
            JsonObject object;

            parameterSerializer = new T8JsonParameterSerializer();

            object = new JsonObject();
            object.add("type", TYPE_ID);
            object.add("projectId", taskDefinition.getRootProjectId());
            object.add("taskId", taskDefinition.getIdentifier());
            object.add("taskIid", taskIid);
            object.add("parameters", parameterSerializer.serializeParameters(taskDefinition.getOutputParameterDefinitions(), taskOutputParameters));
            return object;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while serializing event " + this + ": " + e);
        }
    }
}
