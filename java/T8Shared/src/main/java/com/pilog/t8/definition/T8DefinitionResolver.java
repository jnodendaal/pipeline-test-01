package com.pilog.t8.definition;

import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionResolver
{
    /**
     * Initializes the resolver to a state that is ready for operational use.
     * @throws Exception
     */
    public void init() throws Exception;

    /**
     * Destroys the resolver, clearing any cached data and releasing any
     * acquired resources.
     */
    public void destroy();

    /**
     * Resets the resolver, clearing any cached data that may have been
     * assimilated.
     */
    public void reset();

    /**
     * Returns a boolean value indicating whether or not the specified
     * definition type can be resolved.
     * @param typeId The definition type identifier to check.
     * @return
     */
    public boolean resolvesType(String typeId);

    /**
     * Returns the most applicable definition of the specified type, within the
     * specified session context, given the specified input parameters.
     * @param typeId The definition type to resolve.
     * @param inputParameters The input parameters to use for resolution of the
     * definition as well as initialization of any retrieved definition.
     * @return
     */
    public T8Definition getResolvedDefinition(T8Context context, String typeId, Map<String, Object> inputParameters) throws Exception;
}
