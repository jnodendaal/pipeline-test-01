package com.pilog.t8.definition.security.sso;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8SingleSignOnHandler;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WaffleSsoHandlerDefinition extends T8SingleSignOnHandlerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SSO_HANDLER_WAFFLE";
    public static final String DISPLAY_NAME = "Waffle Single-Sign-On Handler";
    public static final String DESCRIPTION = "A client-side handler that facilitates single-sign-on using Windows Authentication.";
    public enum Datum
    {
        SECURITY_PACKAGE,
        TARGET_NAME
    };
    // -------- Definition Meta-Data -------- //

    public T8WaffleSsoHandlerDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SECURITY_PACKAGE.toString(), "Security Package (SSPI)", "The Security Support Provider Interface (SSPI) to use for authentication.", "Negotiate"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TARGET_NAME.toString(), "Target Name", "The SSPI-specific target name to be used (if any)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8SingleSignOnHandler createSingleSignOnHandler(T8ClientContext clientContext) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.security.sso.T8WaffleSsoHandler", new Class<?>[]{T8ClientContext.class, T8WaffleSsoHandlerDefinition.class}, clientContext, this);
    }

    public String getSecurityPackage()
    {
        return getDefinitionDatum(Datum.SECURITY_PACKAGE);
    }

    public void setSecurityPackage(String securityPackage)
    {
        setDefinitionDatum(Datum.SECURITY_PACKAGE, securityPackage);
    }

    public String getTargetName()
    {
        return getDefinitionDatum(Datum.TARGET_NAME);
    }

    public void setTargetName(String spn)
    {
        setDefinitionDatum(Datum.TARGET_NAME, spn);
    }
}

