package com.pilog.t8.report;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ReportDescription implements Serializable
{
    private String name;
    private String description;

    public T8ReportDescription(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
