package com.pilog.t8.definition.http.api.rest;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.http.api.rest.T8UriParser;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.Collections;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8UriSegmentParserDefinition extends T8UriParserDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_URI_PARSER_SEGMENT";
    public static final String DISPLAY_NAME = "URI Segment Parser";
    public static final String DESCRIPTION = "A definition of a URI parser that interprets a segment of a URI demarcated by '/' separators.";
    public enum Datum
    {
        MATCH_REGEX,
        CAPTURE_REGEX,
        CAPTURE_PARAMETER_ID,
        OPERATION_ID,
        OPERATION_INPUT_PARAMETER_MAPPING,
        OPERATION_OUTPUT_PARAMETER_MAPPING,
        SUB_PARSERS
    };
    // -------- Definition Meta-Data -------- //

    public T8UriSegmentParserDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MATCH_REGEX.toString(), "Match Regex", "The regular expression to use when matching a specific part of a URI to be processed by this parser."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CAPTURE_REGEX.toString(), "Capture Regex", "If regular expression to use in order to capture part of a URI string as a parameter value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CAPTURE_PARAMETER_ID.toString(), "Capture Parameter Id", "The id of the parameter in which to store the value captured by this parser."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.OPERATION_ID.toString(), "Operation Id", "The id of the operation that will be invoked for URIs ending at this segment."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OPERATION_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  API to Operation Input", "A mapping of parameters captured from the URI to the corresponding operation input parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OPERATION_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping: Operation Output to API", "A mapping of operation output parameters to the corresponding API parameters."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SUB_PARSERS.toString(), "Sub-parsers",  "Parsers that follow up from the point in the input URI where the scope of this parser ends."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.SUB_PARSERS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8UriSegmentParserDefinition.GROUP_IDENTIFIER));
        else if (Datum.OPERATION_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER), true, "No Operation");
        else if (Datum.CAPTURE_PARAMETER_ID.toString().equals(datumId))
        {
            List<T8DataParameterDefinition> uriParameterDefinitions;

            uriParameterDefinitions = ((T8RestApiDefinition)getAncestorDefinition(T8RestApiDefinition.TYPE_IDENTIFIER)).getParameterDefinitions();
            return createLocalIdentifierOptionsFromDefinitions(uriParameterDefinitions, true, "Not Captured");
        }
        else if (Datum.OPERATION_INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            operationId = getOperationId();
            if (operationId != null)
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> uriParameterDefinitions;
                    List<T8DataParameterDefinition> operationParameterDefinitions;
                    TreeMap<String, List<String>> idMap;

                    uriParameterDefinitions = ((T8RestApiDefinition)getAncestorDefinition(T8RestApiDefinition.TYPE_IDENTIFIER)).getParameterDefinitions();
                    operationParameterDefinitions = operationDefinition.getInputParameterDefinitions();

                    idMap = new TreeMap<String, List<String>>();
                    if (operationParameterDefinitions != null)
                    {
                        ArrayList<String> fieldIdList;

                        fieldIdList = new ArrayList<String>();
                        for (T8DataParameterDefinition parameterDefinition : operationParameterDefinitions)
                        {
                            fieldIdList.add(parameterDefinition.getPublicIdentifier());
                        }

                        Collections.sort(fieldIdList);
                        for (T8DataParameterDefinition parameterDefinition : uriParameterDefinitions)
                        {
                            idMap.put(parameterDefinition.getIdentifier(), fieldIdList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", idMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.OPERATION_OUTPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            operationId = getOperationId();
            if (operationId != null)
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> uriParameterDefinitions;
                    List<T8DataParameterDefinition> operationParameterDefinitions;
                    TreeMap<String, List<String>> idMap;

                    uriParameterDefinitions = ((T8RestApiDefinition)getAncestorDefinition(T8RestApiDefinition.TYPE_IDENTIFIER)).getParameterDefinitions();
                    operationParameterDefinitions = operationDefinition.getOutputParameterDefinitions();

                    idMap = new TreeMap<String, List<String>>();
                    if (operationParameterDefinitions != null)
                    {
                        ArrayList<String> idList;

                        idList = new ArrayList<String>();
                        for (T8DataParameterDefinition parameterDefinition : uriParameterDefinitions)
                        {
                            idList.add(parameterDefinition.getIdentifier());
                        }

                        Collections.sort(idList);
                        for (T8DataParameterDefinition parameterDefinition : operationParameterDefinitions)
                        {
                            idMap.put(parameterDefinition.getPublicIdentifier(), idList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", idMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8UriParser getUriParser(T8Context context)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.http.api.rest.T8UriSegmentParser", new Class<?>[]{T8Context.class, T8UriSegmentParserDefinition.class}, context, this);
    }

    public String getMatchRegex()
    {
        return getDefinitionDatum(Datum.MATCH_REGEX);
    }

    public void setMatchRegex(String regex)
    {
        setDefinitionDatum(Datum.MATCH_REGEX, regex);
    }

    public String getCaptureRegex()
    {
        return getDefinitionDatum(Datum.CAPTURE_REGEX);
    }

    public void setCaptureRegex(String regex)
    {
        setDefinitionDatum(Datum.CAPTURE_REGEX, regex);
    }

    public String getCaptureParameterId()
    {
        return getDefinitionDatum(Datum.CAPTURE_PARAMETER_ID);
    }

    public void setCaptureParameterId(String id)
    {
        setDefinitionDatum(Datum.CAPTURE_PARAMETER_ID, id);
    }

    public String getOperationId()
    {
        return getDefinitionDatum(Datum.OPERATION_ID);
    }

    public void setOperationId(String operationId)
    {
        setDefinitionDatum(Datum.OPERATION_ID, operationId);
    }

    public Map<String, String> getOperationInputParameterMapping()
    {
        return getDefinitionDatum(Datum.OPERATION_INPUT_PARAMETER_MAPPING);
    }

    public void setOperationInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OPERATION_INPUT_PARAMETER_MAPPING, mapping);
    }

    public Map<String, String> getOperationOutputParameterMapping()
    {
        return getDefinitionDatum(Datum.OPERATION_OUTPUT_PARAMETER_MAPPING);
    }

    public void setOperationOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OPERATION_OUTPUT_PARAMETER_MAPPING, mapping);
    }

    public List<T8UriSegmentParserDefinition> getSubParsers()
    {
        return getDefinitionDatum(Datum.SUB_PARSERS);
    }

    public void setSubParsers(List<T8UriSegmentParserDefinition> subParsers)
    {
        setDefinitionDatum(Datum.SUB_PARSERS, subParsers);
    }
}
