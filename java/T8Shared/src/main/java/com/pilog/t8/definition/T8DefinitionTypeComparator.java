package com.pilog.t8.definition;

import java.util.Comparator;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTypeComparator implements Comparator
{
    private CompareMethod method;
    private CompareType type;

    public enum CompareMethod {LEXICOGRAPHIC};
    public enum CompareType {IDENTIFIER, DISPLAY_NAME, DESCRIPTION, DATUM};

    public T8DefinitionTypeComparator()
    {
        this(CompareMethod.LEXICOGRAPHIC, CompareType.IDENTIFIER);
    }
    public T8DefinitionTypeComparator(CompareMethod method, CompareType type)
    {
        this.method = method;
        this.type = type;
    }

    @Override
    public int compare(Object o1, Object o2)
    {
        T8DefinitionTypeMetaData type1;
        T8DefinitionTypeMetaData typ2;

        type1 = (T8DefinitionTypeMetaData)o1;
        typ2 = (T8DefinitionTypeMetaData)o2;
        if (type == CompareType.IDENTIFIER)
        {
            String value1;
            String value2;

            value1 = type1.getTypeId();
            value2 = typ2.getTypeId();
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                return value1.compareTo(value2);
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else if (type == CompareType.DISPLAY_NAME)
        {
            String value1;
            String value2;

            value1 = type1.getDisplayName();
            value2 = typ2.getDisplayName();
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                return value1.compareTo(value2);
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else if (type == CompareType.DESCRIPTION)
        {
            String value1;
            String value2;

            value1 = type1.getDescription();
            value2 = typ2.getDescription();
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                return value1.compareTo(value2);
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else throw new RuntimeException("No valid compare type set: " + type);
    }
}
