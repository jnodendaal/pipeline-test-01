package com.pilog.t8.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8StateParameterMap extends LinkedHashMap<String, Object>
{
    private final String collectionIid;
    private final String entityId;
    private final Map<String, Object> entityKey;
    private final Map<String, String> contentParameterIids;
    private final Set<String> insertedKeys;
    private final Set<String> updatedKeys;

    public T8StateParameterMap(String collectionIid, String entityId, Map<String, Object> key, T8StateDataSet dataSet)
    {
        this.collectionIid = collectionIid;
        this.entityId = entityId;
        this.entityKey = key;
        this.contentParameterIids = new HashMap<>();
        this.insertedKeys = new HashSet<>();
        this.updatedKeys = new HashSet<>();
        if (dataSet != null) addParameters(dataSet);
    }

    private synchronized void addParameters(T8StateDataSet dataSet)
    {
        List<T8DataEntity> entityList;

        entityList = dataSet.getEntities(entityId, entityKey);
        for (T8DataEntity parameterEntity : entityList)
        {
            String parameterIdentifier;
            String parameterKey;
            String parameterValue;
            String parameterType;

            parameterIdentifier = (String)parameterEntity.getFieldValue(entityId + "$PARAMETER_IID");
            parameterType = (String)parameterEntity.getFieldValue(entityId + "$TYPE");
            parameterKey = (String)parameterEntity.getFieldValue(entityId + "$PARAMETER_KEY");
            parameterValue = (String)parameterEntity.getFieldValue(entityId + "$VALUE");

            put(parameterKey, createParameterValue(parameterIdentifier, parameterType, parameterValue, entityId, dataSet));
            contentParameterIids.put(parameterKey, parameterIdentifier);
        }
    }

    public String getIdentifier()
    {
        return collectionIid;
    }

    public synchronized T8DataEntity createEntity(T8DataTransaction tx, int index, String key) throws Exception
    {
        T8DataEntity newEntity;
        Object value;

        value = get(key);

        newEntity = tx.create(entityId, null);
        newEntity.setFieldValue(entityId + "$PARAMETER_IID", contentParameterIids.get(key));
        newEntity.setFieldValue(entityId + "$PARENT_PARAMETER_IID", collectionIid);
        newEntity.setFieldValue(entityId + "$TYPE", getParameterType(value));
        newEntity.setFieldValue(entityId + "$SEQUENCE", index);
        newEntity.setFieldValue(entityId + "$PARAMETER_KEY", key);
        newEntity.setFieldValue(entityId + "$VALUE", getParameterStringValue(value));

        for (String keyFieldId : entityKey.keySet())
        {
            newEntity.setFieldValue(keyFieldId, entityKey.get(keyFieldId));
        }

        return newEntity;
    }

    public synchronized void statePersisted()
    {
        // Clear this state's flags.
        insertedKeys.clear();
        updatedKeys.clear();

        // Check for sub-collections.
        for (Object value : values())
        {
            if (value instanceof T8StateParameterMap)
            {
                ((T8StateParameterMap)value).statePersisted();
            }
            else if (value instanceof T8StateParameterList)
            {
                ((T8StateParameterList)value).statePersisted();
            }
        }
    }

    private synchronized int getKeyIndex(String key)
    {
        int index;

        index = 0;
        for (String nextKey : keySet())
        {
            if (nextKey.equals(key)) return index;
            index++;
        }

        return -1;
    }

    public synchronized void addUpdates(T8DataTransaction tx, T8StateUpdates updates) throws Exception
    {
        // Add the updates for each inserted key.
        for (String insertedKey : insertedKeys)
        {
            updates.addInsert(createEntity(tx, getKeyIndex(insertedKey), insertedKey));
        }

        // Add the updates for each updated key.
        for (String updatedKey : updatedKeys)
        {
            updates.addUpdate(createEntity(tx, getKeyIndex(updatedKey), updatedKey));
        }

        // Check for sub-collections.
        for (Object value : values())
        {
            if (value instanceof T8StateParameterMap)
            {
                ((T8StateParameterMap)value).addUpdates(tx, updates);
            }
            else if (value instanceof T8StateParameterList)
            {
                ((T8StateParameterList)value).addUpdates(tx, updates);
            }
        }
    }

    @Override
    public synchronized Object remove(Object key)
    {
        throw new RuntimeException("Cannot remove parameter.");
    }

    @Override
    public synchronized void putAll(Map<? extends String, ? extends Object> map)
    {
        for (Map.Entry<? extends String, ? extends Object> entrySet : map.entrySet())
        {
            put(entrySet.getKey(), entrySet.getValue());
        }
    }

    @Override
    public synchronized Object put(String key, Object value)
    {
        if (!containsKey(key))
        {
            if (value != null)
            {
                String parameterIid;

                parameterIid = T8IdentifierUtilities.createNewGUID();
                contentParameterIids.put(key, parameterIid);
                insertedKeys.add(key);

                if (value instanceof Map)
                {
                    T8StateParameterMap newMap;
                    Map<String, Object> subKey;

                    subKey = new HashMap(entityKey);
                    subKey.put(entityId + "$PARENT_PARAMETER_IID", collectionIid);
                    newMap = new T8StateParameterMap(parameterIid, entityId, subKey, null);
                    newMap.putAll((Map)value);
                    return super.put(key, newMap);
                }
                else if (value instanceof List)
                {
                    T8StateParameterList newList;
                    Map<String, Object> subKey;

                    subKey = new HashMap(entityKey);
                    subKey.put(entityId + "$PARENT_PARAMETER_IID", collectionIid);
                    newList = new T8StateParameterList(parameterIid, entityId, subKey, null);
                    for (Object valueElement : (List)value)
                    {
                        newList.add(valueElement);
                    }

                    return super.put(key, newList);
                }
                else
                {
                    return super.put(key, value);
                }
            }
            else return null;
        }
        else
        {
            Object oldValue;

            oldValue = get(key);
            if ((oldValue instanceof List) || (oldValue instanceof Map))
            {
                if (!Objects.equals(oldValue, value))
                {
                    throw new RuntimeException("Attempt to update an existing collection value in state parameter map " + collectionIid + " to a new value using key: " + key + ". Old Value: " + oldValue + ", New Value: " + value);
                }
                else return oldValue;
            }
            else // Updated value is not a collection.
            {
                if (!Objects.equals(oldValue, value))
                {
                    updatedKeys.add(key);
                    return super.put(key, value);
                }
                else return oldValue;
            }
        }
    }

    public synchronized Object createParameterValue(String parameterIid, String parameterType, String value, String entityId, T8StateDataSet dataSet)
    {
        switch (parameterType)
        {
            case "MAP":
            {
                return new T8StateParameterMap(parameterIid, entityId, HashMaps.<String, Object>create(entityId + "$PARENT_PARAMETER_IID", parameterIid), dataSet);
            }
            case "LIST":
            {
                return new T8StateParameterList(parameterIid, entityId, HashMaps.<String, Object>create(entityId + "$PARENT_PARAMETER_IID", parameterIid), dataSet);
            }
            case "INTEGER":
            {
                return Integer.parseInt(value);
            }
            case "LONG":
            {
                return Long.parseLong(value);
            }
            case "DOUBLE":
            {
                return Double.parseDouble(value);
            }
            case "FLOAT":
            {
                return Float.parseFloat(value);
            }
            case "BOOLEAN":
            {
                return Boolean.parseBoolean(value);
            }
            case "TIMESTAMP":
            {
                return T8Timestamp.fromTime(Long.parseLong(value));
            }
            default:
            {
                return value; // Just return the String value.
            }
        }
    }

    public static String getParameterStringValue(Object value)
    {
        if (value instanceof Map)
        {
            return ((T8StateParameterMap)value).getIdentifier();
        }
        else if (value instanceof List)
        {
            return ((T8StateParameterList)value).getIdentifier();
        }
        else if (value instanceof T8Timestamp)
        {
            return String.valueOf(((T8Timestamp)value).getMilliseconds());
        }
        else
        {
            return value.toString();
        }
    }

    public static String getParameterType(Object parameterValue)
    {
        if (parameterValue instanceof Map)
        {
            return "MAP";
        }
        else if (parameterValue instanceof List)
        {
            return "LIST";
        }
        else if (parameterValue instanceof Integer)
        {
            return "INTEGER";
        }
        else if (parameterValue instanceof Long)
        {
            return "LONG";
        }
        else if (parameterValue instanceof Double)
        {
            return "DOUBLE";
        }
        else if (parameterValue instanceof Float)
        {
            return "FLOAT";
        }
        else if (parameterValue instanceof Boolean)
        {
            return "BOOLEAN";
        }
        else if (parameterValue instanceof String)
        {
            return "STRING";
        }
        else if (parameterValue instanceof T8Timestamp)
        {
            return "TIMESTAMP";
        }
        else throw new RuntimeException("Invalid parameter type: " + parameterValue.getClass());
    }
}
