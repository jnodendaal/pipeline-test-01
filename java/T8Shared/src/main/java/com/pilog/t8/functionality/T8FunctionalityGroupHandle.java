package com.pilog.t8.functionality;

import com.pilog.t8.data.object.T8DataObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityGroupHandle implements Serializable
{
    protected final List<T8FunctionalityHandle> functionalities;
    protected final List<T8FunctionalityGroupHandle> subGroups;
    protected T8FunctionalityHandle overviewFunctionality;
    protected T8FunctionalityGroupHandle parentGroup;
    protected String id;
    protected String displayName;
    protected String description;
    protected String iconUri;
    protected Icon icon;

    public T8FunctionalityGroupHandle(String id, T8FunctionalityGroupHandle parentGroup)
    {
        this.id = id;
        this.parentGroup = parentGroup;
        this.functionalities = new ArrayList<T8FunctionalityHandle>();
        this.subGroups = new ArrayList<T8FunctionalityGroupHandle>();
    }

    public String getIdentifier()
    {
        return id;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Icon getIcon()
    {
        return icon;
    }

    public void setIcon(Icon icon)
    {
        this.icon = icon;
    }

    public String getIconUri()
    {
        return iconUri;
    }

    public void setIconUri(String iconUri)
    {
        this.iconUri = iconUri;
    }

    public T8FunctionalityGroupHandle getParentGroup()
    {
        return parentGroup;
    }

    void setParentGroup(T8FunctionalityGroupHandle parent)
    {
        this.parentGroup = parent;
    }

    public void clearSubGroups()
    {
        subGroups.clear();
    }

    /**
     * Returns the functionality group with the specified id from the
 content of this group.  If this group has the specified id, this
 object is returned.
     * @param groupIdentifier The id of the functionality group handle
 to return.
     * @return The functionality group handle with the specified id.
     */
    public T8FunctionalityGroupHandle getFunctionalityGroup(String groupIdentifier)
    {
        if (id.equals(groupIdentifier)) return this;
        else
        {
            for (T8FunctionalityGroupHandle subGroup : subGroups)
            {
                if (subGroup.getIdentifier().equals(groupIdentifier))
                {
                    return subGroup;
                }
                else
                {
                    T8FunctionalityGroupHandle foundGroup;

                    foundGroup = subGroup.getFunctionalityGroup(groupIdentifier);
                    if (foundGroup != null) return foundGroup;
                }
            }

            return null;
        }
    }

    public List<T8FunctionalityGroupHandle> getSubGroups()
    {
        return new ArrayList<T8FunctionalityGroupHandle>(subGroups);
    }

    public void addSubGroup(T8FunctionalityGroupHandle subGroup)
    {
        if (subGroup != null)
        {
            subGroup.setParentGroup(this);
            subGroups.add(subGroup);
        }
    }

    public T8FunctionalityHandle getOverviewFunctionality()
    {
        return overviewFunctionality;
    }

    public void setOverviewFunctionality(T8FunctionalityHandle functionality)
    {
        this.overviewFunctionality = functionality;
    }

    public void clearFunctionalities()
    {
        functionalities.clear();
    }

    public List<T8FunctionalityHandle> getFunctionalities()
    {
        return new ArrayList<T8FunctionalityHandle>(functionalities);
    }

    /**
     * Returns all functionality handles hooked to this group and its descendant
     * groups.
     * @return All functionality handles hooked to this group and its descendant
     * groups.
     */
    public List<T8FunctionalityHandle> getAllFunctionalities()
    {
        List<T8FunctionalityHandle> handles;

        handles = new ArrayList<T8FunctionalityHandle>(functionalities);
        for (T8FunctionalityGroupHandle subGroup : subGroups)
        {
            handles.addAll(subGroup.getAllFunctionalities());
        }

        return handles;
    }

    public void addFunctionality(T8FunctionalityHandle functionality)
    {
        functionalities.add(functionality);
    }

    public boolean removeFunctionality(T8FunctionalityHandle functionality)
    {
        return functionalities.remove(functionality);
    }

    public boolean containsAvailableFunctionality()
    {
        if (functionalities.size() > 0) return true;
        else
        {
            for (T8FunctionalityGroupHandle subGroup : subGroups)
            {
                if (subGroup.containsAvailableFunctionality()) return true;
            }

            return false;
        }
    }

    public void setTargetDataObject(T8DataObject targetObject)
    {
        // Remove any functionalities that are applicable to data objects other than the one specified.
        for (T8FunctionalityHandle functionalityHandle : getFunctionalities())
        {
            Set<String> requiredObjectIds;

            requiredObjectIds = functionalityHandle.getRequiredObjectIds();
            if (requiredObjectIds.size() > 1) // More than one required object, so this functionality must be removed.
            {
                removeFunctionality(functionalityHandle);
            }
            else if (requiredObjectIds.size() == 1)
            {
                if (targetObject != null)
                {
                    String dataObjectId;

                    dataObjectId = targetObject.getId();
                    if (requiredObjectIds.contains(dataObjectId))
                    {
                        functionalityHandle.setTargetDataObject(targetObject);
                    }
                    else // One required object but not the same one, so functionalit must be removed.
                    {
                        removeFunctionality(functionalityHandle);
                    }
                }
                else
                {
                    removeFunctionality(functionalityHandle);
                }
            }
        }

        // Filter the sub-groups.
        for (T8FunctionalityGroupHandle subGroup : subGroups)
        {
            subGroup.setTargetDataObject(targetObject);
        }
    }

    public void setFunctionalityEnabled(Set<String> functionalityIds, boolean includeIndependentFunctionalities,  boolean enabled, Boolean inverseEnabled, String disabledMessage)
    {
        // Enable the specified functionalities.
        for (T8FunctionalityHandle functionalityHandle : functionalities)
        {
            if (functionalityHandle.getRequiredObjectIds().isEmpty())
            {
                // The functionality is independent because it does not required a data object (or provides one that is predefined).
                if (includeIndependentFunctionalities)
                {
                    functionalityHandle.setEnabled(enabled);
                    if (!enabled) functionalityHandle.setDisabledMessage(disabledMessage);
                    else functionalityHandle.setDisabledMessage(null);
                }
            }
            else if ((functionalityIds != null) && (functionalityIds.contains(functionalityHandle.getId())))
            {
                // The functionality is not independent, so only enable it if the id is one of those specified.
                functionalityHandle.setEnabled(enabled);
                if (!enabled) functionalityHandle.setDisabledMessage(disabledMessage);
                else functionalityHandle.setDisabledMessage(null);
            }
            else if (inverseEnabled != null)
            {
                // If an inverse is specified, apply it to this handle because it does not fall under one of the preceding clauses.
                functionalityHandle.setEnabled(inverseEnabled);
                if (!inverseEnabled) functionalityHandle.setDisabledMessage(disabledMessage);
                else functionalityHandle.setDisabledMessage(null);
            }
        }

        // Enable functionalities in the sub-groups.
        for (T8FunctionalityGroupHandle subGroup : subGroups)
        {
            subGroup.setFunctionalityEnabled(functionalityIds, includeIndependentFunctionalities, enabled, inverseEnabled, disabledMessage);
        }
    }

    @Override
    public String toString()
    {
        return "T8FunctionalityGroupHandle{" + "id=" + id + '}';
    }
}
