package com.pilog.t8.process;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessSummary implements Serializable
{
    private final Map<String, T8ProcessTypeSummary> processTypeSummaries;

    public T8ProcessSummary()
    {
        processTypeSummaries = new LinkedHashMap<String, T8ProcessTypeSummary>();
    }

    public List<T8ProcessTypeSummary> getProcessTypeSummaries()
    {
        return new ArrayList<T8ProcessTypeSummary>(processTypeSummaries.values());
    }

    public T8ProcessTypeSummary getProcessTypeSummary(String processId)
    {
        return processTypeSummaries.get(processId);
    }

    public void putProcessTypeSummary(T8ProcessTypeSummary summary)
    {
        processTypeSummaries.put(summary.getProcessId(), summary);
    }

    public int getProcessCount(T8ProcessFilter processFilter)
    {
        int count;

        count = 0;
        for (T8ProcessTypeSummary processTypeSummary : processTypeSummaries.values())
        {
            if (processFilter.isProcessIdIncluded(processTypeSummary.getProcessId()))
            {
                count += processTypeSummary.getExecutingCount();
            }
        }

        return count;
    }

    public int getTotalProcessCount()
    {
        int count;

        count = 0;
        for (T8ProcessTypeSummary processTypeSummary : processTypeSummaries.values())
        {
            count += processTypeSummary.getTotalCount();
        }

        return count;
    }
}
