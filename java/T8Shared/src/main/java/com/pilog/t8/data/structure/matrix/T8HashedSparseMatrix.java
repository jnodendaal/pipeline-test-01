package com.pilog.t8.data.structure.matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An implementation of a sparse matrix using a single HashMap.  The matrix
 * grows according to data added but does not shrink when data is removed i.e.
 * the actual data size of the matrix decreases but the logical dimensions do
 * not.
 *
 * @author Bouwer du Preez
 * @param <T> Type of the elements in the matrix.
 */
public class T8HashedSparseMatrix<T>
{
    private final Map<String, T> data; // The map that stores all the data contained by the matrix.
    private int rowSize;
    private int columnSize;

    public T8HashedSparseMatrix()
    {
        data = new HashMap<String, T>();
        rowSize = 0;
        columnSize = 0;
    }

    public void clear()
    {
        rowSize = 0;
        columnSize = 0;
        data.clear();
    }

    public int getRowSize()
    {
        return rowSize;
    }

    public int getColumnSize()
    {
        return columnSize;
    }

    private String getKey(Integer row, Integer column)
    {
        return row.toString() + column.toString();
    }

    public T get(int row, int column)
    {
        return data.get(getKey(row, column));
    }

    public T remove(int row, int column)
    {
        return data.remove(getKey(row, column));
    }

    public void put(int rowIndex, int columnIndex, T value)
    {
        // Increase the row size if necessary.
        if (rowIndex >= rowSize) rowSize = rowIndex + 1;
        if (columnIndex >= columnSize) columnSize = columnIndex + 1;
        data.put(getKey(rowIndex, columnIndex), value);
    }

    public List<T> getRow(int rowIndex)
    {
        if (rowIndex < rowSize)
        {
            List<T> row;

            row = new ArrayList<T>();
            for (int columnIndex = 0; columnIndex < columnSize; columnIndex++)
            {
                row.add(get(rowIndex, columnIndex));
            }

            return row;
        }
        else return null;
    }

    public void putRow(int rowIndex, List<T> rowData)
    {
        if (rowData != null)
        {
            for (int columnIndex = 0; columnIndex < rowData.size(); columnIndex++)
            {
                put(rowIndex, columnIndex, rowData.get(columnIndex));
            }
        }
        else
        {
            removeRow(rowIndex);
        }
    }

    public void removeRow(int rowIndex)
    {
        if (rowIndex < rowSize)
        {
            for (int columnIndex = 0; columnIndex < columnSize; columnIndex++)
            {
                remove(rowIndex, columnIndex);
            }
        }
    }

    public void addRow(List<T> rowData)
    {
        putRow(rowSize, rowData);
    }

    public List<T> getColumn(int columnIndex)
    {
        if (columnIndex < columnSize)
        {
            List<T> row;

            row = new ArrayList<T>();
            for (int rowIndex = 0; rowIndex < rowSize; rowIndex++)
            {
                row.add(get(rowIndex, columnIndex));
            }

            return row;
        }
        else return null;
    }

    public void putColumn(int columnIndex, List<T> columnData)
    {
        if (columnData != null)
        {
            for (int rowIndex = 0; rowIndex < columnData.size(); rowIndex++)
            {
                put(rowIndex, columnIndex, columnData.get(rowIndex));
            }
        }
        else
        {
            removeColumn(columnIndex);
        }
    }

    public void removeColumn(int columnIndex)
    {
        if (columnIndex < columnSize)
        {
            for (int rowIndex = 0; rowIndex < rowSize; rowIndex++)
            {
                remove(rowIndex, columnIndex);
            }
        }
    }

    public void addColumn(List<T> columnData)
    {
        putColumn(columnSize, columnData);
    }
}
