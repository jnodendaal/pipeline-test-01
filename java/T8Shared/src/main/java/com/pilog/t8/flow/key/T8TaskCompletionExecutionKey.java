package com.pilog.t8.flow.key;

import com.pilog.t8.flow.state.T8FlowNodeState;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskCompletionExecutionKey extends T8DefaultFlowExecutionKey
{
    private final String taskInstanceIdentifier;
    private final Map<String, Object> taskOutputParameters;

    public T8TaskCompletionExecutionKey(T8FlowNodeState nodeState, String taskInstanceIdentifier, Map<String, Object> taskOutputParameters)
    {
        super(nodeState);
        this.taskInstanceIdentifier = taskInstanceIdentifier;
        this.taskOutputParameters = taskOutputParameters;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskInstanceIdentifier;
    }

    public Map<String, Object> getTaskOutputParameters()
    {
        return taskOutputParameters;
    }
}
