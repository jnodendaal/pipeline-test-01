package com.pilog.t8.data;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8DataSession extends T8PerformanceStatisticsProvider
{
    public String getId();
    public T8Context getContext();
    public Thread getParentThread();
    public long getLastActivityTime();
    public boolean isActive(); // Returns a boolean flag indicating whether or not the data session contains a currently active transaction.
    public T8DataSessionDetails getDetails();
    public T8DefinitionManager getDefinitionManager(); // Returns the definition manager that is used within context of this data session to fetch meta data definitions.
    public T8DataManager getDataManager(); // Returns the data manager from which this session was created.

    /**
     * Creates a new open transaction associated with the invoking thread.
     * Starting a new transaction from a thread already associated with an
     * existing transaction is an illegal operation.<br/>
     *
     * @return The {@code T8DataTransaction} new transaction associated with
     * the invoking thread.
     */
    public T8DataTransaction beginTransaction();

    /**
     * Returns the transaction associated with the caller thread.  This method
     * returns null if no transaction has been started by the invoking thread.
     *
     * @return The transaction associated with the caller thread.
     */
    public T8DataTransaction getTransaction();

    /**
     * Resume the transaction context association of the calling thread
     * with the transaction represented by the supplied Transaction object.
     * When this method returns, the calling thread is associated with the
     * transaction context specified.
     *
     * @param tx The <code>T8DataTransaction</code> object that represents
     * the transaction to be resumed.
     */
    public void resume(T8DataTransaction tx);

    /**
     * Suspend the transaction currently associated with the calling
     * thread and return a Transaction object that represents the
     * transaction context being suspended. If the calling thread is
     * not associated with a transaction, the method returns a null
     * object reference. When this method returns, the calling thread
     * is not associated with a transaction.
     *
     * @return <code>T8DataTransaction</code> object representing the
     * suspended transaction.
     */
    public T8DataTransaction suspend();

    /**
     * Commits all transactions and closes all data sources.
     */
    public void closeSession();

    /**
     * Indicates whether or not an instant transaction is currently used/open
     * in this session.
     * @return Boolean true/false
     */
    public boolean hasInstantTransaction();

    /**
     * Replaces the existing instant transaction of the session (if any) with a
     * new one.
     */
    public void refreshInstantTransaction();

    /**
     * Returns the shared thread-safe transaction used by all processes within
     * this data session.  Any operations performed on the instant transaction
     * are committed immediately.
     * @return
     */
    public T8DataTransaction instantTransaction();
}
