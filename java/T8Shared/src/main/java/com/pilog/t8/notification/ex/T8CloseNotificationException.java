/**
 * Created on 11 Jun 2015, 3:55:59 PM
 */
package com.pilog.t8.notification.ex;

/**
 * @author Gavin Boshoff
 */
public class T8CloseNotificationException extends RuntimeException
{
    private final String notificationInstanceIdentifier;

    public T8CloseNotificationException(String notificationInstanceIdentifier)
    {
        super("Notification To Be Closed : " + notificationInstanceIdentifier);
        this.notificationInstanceIdentifier = notificationInstanceIdentifier;
    }

    public T8CloseNotificationException(String message, String notificationInstanceIdentifier)
    {
        super(message);
        this.notificationInstanceIdentifier = notificationInstanceIdentifier;
    }

    public String getNotificationInstanceIdentifier()
    {
        return this.notificationInstanceIdentifier;
    }
}