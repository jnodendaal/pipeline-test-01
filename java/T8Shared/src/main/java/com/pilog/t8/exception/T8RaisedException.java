package com.pilog.t8.exception;

/**
 * @author Hennie Brink
 */
public class T8RaisedException extends Exception implements T8UserException
{
    private String technicalMessage;
    private String code;

    public T8RaisedException()
    {
    }

    public T8RaisedException(String code, String message)
    {
        super(message);
        this.code = code;
    }

    public T8RaisedException(String code, String message, String technicalMessage)
    {
        super(message);
        this.technicalMessage = technicalMessage;
        this.code = code;
    }

    public T8RaisedException(String code, String message, String technicalMessage, Throwable cause)
    {
        super(message, cause);
        this.technicalMessage = technicalMessage;
        this.code = code;
    }

    @Override
    public String getUserMessage()
    {
        return getMessage();
    }

    public String getCode()
    {
        return code;
    }

    public String getTechnicalMessage()
    {
        return technicalMessage;
    }

}
