package com.pilog.t8.definition.report;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLAggregateFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.process.report.T8ReportGenerationProcessDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ReportManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GENERATE_REPORT = "@OP_RPT_GENERATE_REPORT";
    public static final String OPERATION_DELETE_REPORT = "@OP_RPT_DELETE_REPORT";
    public static final String PROCESS_GENERATE_REPORT = "@PRC_RPT_GENERATE_REPORT";

    public static final String PARAMETER_FILE_CONTEXT_ID = "$P_FILE_CONTEXT_ID";
    public static final String PARAMETER_FILE_PATH = "$P_FILE_PATH";
    public static final String PARAMETER_DIRECTORY = "$P_DIRECTORY";
    public static final String PARAMETER_PROCESS_IID = "$P_PROCESS_IID";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_REPORT_ID = "$P_REPORT_ID";
    public static final String PARAMETER_REPORT_IID = "$P_REPORT_IID";
    public static final String PARAMETER_REPORT_PARAMETERS = "$P_REPORT_PARAMETERS";
    public static final String PARAMETER_RESULT = "$P_RESULT";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";

    // STATE_REPORT.
    public static final String STATE_REPORT_DS_IDENTIFIER = "@DS_STATE_REPORT";
    public static final String STATE_REPORT_DE_IDENTIFIER = "@E_STATE_REPORT";

    // STATE_REPORT_SUMMARY.
    public static final String STATE_REPORT_SUMMARY_DS_IDENTIFIER = "@DS_STATE_REPORT_SUMMARY";
    public static final String STATE_REPORT_SUMMARY_DE_IDENTIFIER = "@E_STATE_REPORT_SUMMARY";

    // HISTORY_REPORT.
    public static final String HISTORY_REPORT_DS_IDENTIFIER = "@DS_HISTORY_REPORT";
    public static final String HISTORY_REPORT_DE_IDENTIFIER = "@E_HISTORY_REPORT";

    // HISTORY_REPORT_PAR.
    public static final String HISTORY_REPORT_PAR_DS_IDENTIFIER = "@DS_HISTORY_REPORT_PAR";
    public static final String HISTORY_REPORT_PAR_DE_IDENTIFIER = "@E_HISTORY_REPORT_PAR";

    // Field ids.
    public static final String F_REPORT_IID = "$REPORT_IID";
    public static final String F_REPORT_ID = "$REPORT_ID";
    public static final String F_PROJECT_ID = "$PROJECT_ID";
    public static final String F_NAME = "$NAME";
    public static final String F_DESCRIPTION = "$DESCRIPTION";
    public static final String F_STATUS = "$STATUS";
    public static final String F_USER_ID = "$USER_ID";
    public static final String F_INITIATOR_ID = "$INITIATOR_ID";
    public static final String F_INITIATOR_IID = "$INITIATOR_IID";
    public static final String F_EVENT = "$EVENT";
    public static final String F_TIME = "$TIME";
    public static final String F_GENERATION_START_TIME = "$GENERATION_START_TIME";
    public static final String F_GENERATION_END_TIME = "$GENERATION_END_TIME";
    public static final String F_EXPIRATION_TIME = "$EXPIRATION_TIME";
    public static final String F_FILE_CONTEXT_ID = "$FILE_CONTEXT_ID";
    public static final String F_FILE_PATH = "$FILE_PATH";
    public static final String F_PARAMETER_IID = "$PARAMETER_IID";
    public static final String F_PARENT_PARAMETER_IID = "$PARENT_PARAMETER_IID";
    public static final String F_TYPE = "$TYPE";
    public static final String F_VALUE = "$VALUE";
    public static final String F_PARAMETER_KEY = "$PARAMETER_KEY";
    public static final String F_SEQUENCE = "$SEQUENCE";
    public static final String F_REPORT_COUNT = "$REPORT_COUNT";

    // Table Names.
    public static final String TABLE_STATE_REPORT = "RPT";
    public static final String TABLE_HISTORY_REPORT = "HIS_RPT";
    public static final String TABLE_HISTORY_REPORT_PAR = "HIS_RPT_PAR";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        if (typeIdentifiers.contains(T8ReportGenerationProcessDefinition.TYPE_IDENTIFIER)) definitions.addAll(getProcessDefinitions());
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8SQLBasedGroupByDataSourceDefinition sqlGroupByDataSourceDefinition;
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Report State Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_REPORT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_REPORT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_REPORT_IID, "REPORT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_REPORT_ID, "REPORT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NAME, "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DESCRIPTION, "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GENERATION_START_TIME, "GENERATION_START_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GENERATION_END_TIME, "GENERATION_END_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EXPIRATION_TIME, "EXPIRATION_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FILE_CONTEXT_ID, "FILE_CONTEXT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FILE_PATH, "FILE_PATH", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Report History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_REPORT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_REPORT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_REPORT_IID, "REPORT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_REPORT_ID, "REPORT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NAME, "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DESCRIPTION, "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STATUS, "STATUS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GENERATION_START_TIME, "GENERATION_START_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GENERATION_END_TIME, "GENERATION_END_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EXPIRATION_TIME, "EXPIRATION_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FILE_CONTEXT_ID, "FILE_CONTEXT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FILE_PATH, "FILE_PATH", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Report Parameter History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_REPORT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_REPORT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_REPORT_IID, "REPORT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Report State Summary SQL Group By Data Source.
        sqlGroupByDataSourceDefinition = new T8SQLBasedGroupByDataSourceResourceDefinition(STATE_REPORT_SUMMARY_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.setOriginatingDataSourceIdentifier(STATE_REPORT_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_PROJECT_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_REPORT_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_USER_ID);
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_REPORT_ID, "REPORT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8SQLAggregateFieldResourceDefinition(F_REPORT_COUNT, "REPORT_COUNT", false, false, T8DataType.INTEGER, "COUNT(REPORT_ID)"));
        definitions.add(sqlGroupByDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Report State Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_REPORT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_REPORT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_IID, STATE_REPORT_DS_IDENTIFIER + F_REPORT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_ID, STATE_REPORT_DS_IDENTIFIER + F_REPORT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, STATE_REPORT_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NAME, STATE_REPORT_DS_IDENTIFIER + F_NAME, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DESCRIPTION, STATE_REPORT_DS_IDENTIFIER + F_DESCRIPTION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, STATE_REPORT_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, STATE_REPORT_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, STATE_REPORT_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GENERATION_START_TIME, STATE_REPORT_DS_IDENTIFIER + F_GENERATION_START_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GENERATION_END_TIME, STATE_REPORT_DS_IDENTIFIER + F_GENERATION_END_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EXPIRATION_TIME, STATE_REPORT_DS_IDENTIFIER + F_EXPIRATION_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FILE_CONTEXT_ID, STATE_REPORT_DS_IDENTIFIER + F_FILE_CONTEXT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FILE_PATH, STATE_REPORT_DS_IDENTIFIER + F_FILE_PATH, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Report History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_REPORT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_REPORT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_IID, HISTORY_REPORT_DS_IDENTIFIER + F_REPORT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_ID, HISTORY_REPORT_DS_IDENTIFIER + F_REPORT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, HISTORY_REPORT_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NAME, HISTORY_REPORT_DS_IDENTIFIER + F_NAME, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DESCRIPTION, HISTORY_REPORT_DS_IDENTIFIER + F_DESCRIPTION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STATUS, HISTORY_REPORT_DS_IDENTIFIER + F_STATUS, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, HISTORY_REPORT_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, HISTORY_REPORT_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, HISTORY_REPORT_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GENERATION_START_TIME, HISTORY_REPORT_DS_IDENTIFIER + F_GENERATION_START_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GENERATION_END_TIME, HISTORY_REPORT_DS_IDENTIFIER + F_GENERATION_END_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EXPIRATION_TIME, HISTORY_REPORT_DS_IDENTIFIER + F_EXPIRATION_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FILE_CONTEXT_ID, HISTORY_REPORT_DS_IDENTIFIER + F_FILE_CONTEXT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FILE_PATH, HISTORY_REPORT_DS_IDENTIFIER + F_FILE_PATH, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Report Parameter History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_REPORT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_REPORT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_IID, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_REPORT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, HISTORY_REPORT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Report State Summary Data Entity
        entityDefinition = new T8DataEntityResourceDefinition(STATE_REPORT_SUMMARY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_REPORT_SUMMARY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, STATE_REPORT_SUMMARY_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_ID, STATE_REPORT_SUMMARY_DS_IDENTIFIER + F_REPORT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, STATE_REPORT_SUMMARY_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_REPORT_COUNT, STATE_REPORT_SUMMARY_DS_IDENTIFIER + F_REPORT_COUNT, false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_GENERATE_REPORT);
        definition.setMetaDisplayName("Generate Report");
        definition.setMetaDescription("Generates the specified report using the supplied input parameters.");
        definition.setClassName("com.pilog.t8.report.T8ReportManagerOperations$ApiGenerateReportOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROJECT_ID, "Project Id", "The identifier of the project where the report to be generated is located.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_ID, "Report Id", "The identifier of the report to generate.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_PARAMETERS, "Report Parameters", "The parameters to use as input to the report generation", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROCESS_IID, "Process Iid", "A instance identifier of the process handling the generation of the requested report.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_REPORT);
        definition.setMetaDisplayName("Delete Report");
        definition.setMetaDescription("Deletes the specified report from the current user's basket.");
        definition.setClassName("com.pilog.t8.report.T8ReportManagerOperations$ApiDeleteReportOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_IID, "Report Iid", "The instance identifier of the report to delete.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "A boolean result indicating whether or not the specified report was deleted.", T8DataType.BOOLEAN));
        definitions.add(definition);

        return definitions;
    }

    public List<T8Definition> getProcessDefinitions()
    {
        T8ReportGenerationProcessDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8ReportGenerationProcessDefinition(PROCESS_GENERATE_REPORT);
        definition.setMetaDisplayName("Generate Report");
        definition.setMetaDescription("Generates the specified report using the supplied input parameters.");
        definition.setProcessDisplayName("Report Generation");
        definition.setProcessDescription("Generates a specific report using supplied input parameters.");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROJECT_ID, "Project Id", "The identifier of the project where the report to be generated is located.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_ID, "Report Id", "The identifier of the report to generate.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_IID, "Report Iid", "The instance identifier of the report to generate.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_CONTEXT_ID, "File Context Id", "The identifier of the file context where the report output file will be created.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DIRECTORY, "Directry", "The directory in the specified file context to which the generated report will be written.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_PARAMETERS, "Report Parameters", "The parameters to use as input to the report generation.", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_PATH, "Report Filepath", "The context relative path of the report file that was generated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        return definitions;
    }
}
