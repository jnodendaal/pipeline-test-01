package com.pilog.t8.service;

import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8Service
{
    public void init() throws Exception;
    public void destroy();

    /**
     * Returns the service definition.
     *
     * @return The meta definition of the service.
     */
    public T8ServiceDefinition getDefinition();

    /**
     * This method executes one of the T8Service operations using the supplied
     * operation parameters and returns the output parameters.
     *
     * @param context
     * @param operationIdentifier The identifier of the operation to invoke.
     * @param operationParameters The input parameters that will be passed to
     * the operation.
     * @return The output parameters returned after operation completion.
     */
    public Map<String, Object> executeOperation(T8Context context, String operationIdentifier, Map<String, Object> operationParameters);
}
