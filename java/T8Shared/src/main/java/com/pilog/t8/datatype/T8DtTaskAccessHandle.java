package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.functionality.T8TaskFunctionalityAccessHandle;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtTaskAccessHandle extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FUNC_ACCESS_HANDLE_TASK";

    public T8DtTaskAccessHandle() {}

    public T8DtTaskAccessHandle(T8DefinitionManager context) {}

    public T8DtTaskAccessHandle(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FunctionalityHandle.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8TaskFunctionalityAccessHandle accessHandle;
            Map<String, Object> componentParameters;
            JsonObject handleObject;

            // Create the concept JSON object.
            accessHandle = (T8TaskFunctionalityAccessHandle)object;
            componentParameters = accessHandle.getComponentParameters();
            handleObject = new JsonObject();
            handleObject.add("accessType", accessHandle.getAccessType().toString());
            handleObject.add("accessMessage", accessHandle.getAccessMessage());
            handleObject.add("id", accessHandle.getFunctionalityId());
            handleObject.add("iid", accessHandle.getFunctionalityIid());
            handleObject.add("name", accessHandle.getDisplayName());
            handleObject.addIfNotNull("description", accessHandle.getDescription());
            handleObject.addIfNotNull("componentUri", accessHandle.getComponentUri());
            handleObject.addIfNotNull("iconUri", accessHandle.getIconUri());
            if (componentParameters != null) handleObject.addIfNotNull("componentParameters", new T8DtMap(null).serialize(componentParameters));

            // Return the handle object.
            return handleObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}