package com.pilog.t8.functionality;

import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import java.lang.reflect.Constructor;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8OperationFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final T8ServerOperationStatusReport operationStatusReport;

    public T8OperationFunctionalityAccessHandle(String functionalityId, String functionalityIid, String displayName, String description, Icon icon, T8ServerOperationStatusReport statusReport)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.operationStatusReport = statusReport;
    }

    public T8ServerOperationStatusReport getOperationStatusReport()
    {
        return operationStatusReport;
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        T8FunctionalityView view;

        constructor = Class.forName("com.pilog.t8.functionality.view.T8OperationFunctionalityView").getConstructor(T8ComponentController.class, T8OperationFunctionalityAccessHandle.class);
        view = (T8FunctionalityView)constructor.newInstance(controller, this);
        view.addFunctionalityViewListener(viewListener);
        return view;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return false;
    }
}
