package com.pilog.t8.definition.operation;

import com.pilog.t8.T8ClientOperation;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * This class defines the definition of a specific operation type.
 *
 * @author Bouwer du Preez
 */
public abstract class T8ClientOperationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_SYSTEM_CLIENT_OPERATION";
    public static final String STORAGE_PATH = "/client_operations";
    public static final String DISPLAY_NAME = "Client Operation";
    public static final String DESCRIPTION = "A client-side pre-compiled Java operation.";
    public static final String IDENTIFIER_PREFIX = "OC_";
    public enum Datum {INPUT_PARAMETER_DEFINITIONS,
                       OUTPUT_PARAMETER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ClientOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this operation."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters", "The output data parameters returned by this operation after execution."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8ClientOperation getNewOperationInstance(T8ComponentController controller, String operationIid) throws Exception;

    public void addInputParameterDefinition(T8DataParameterDefinition inputParameterDefinition)
    {
        addSubDefinition(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), inputParameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }
}

