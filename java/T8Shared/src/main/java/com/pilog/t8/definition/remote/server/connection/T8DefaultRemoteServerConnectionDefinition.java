package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8DefaultRemoteServerConnectionDefinition extends T8ConnectionDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REMOTE_SERVER_CONNECTION_DETAIL_DEFAULT";
    public static final String DISPLAY_NAME = "Default Remote Server Connection";
    public static final String DESCRIPTION = "This definition contains the information neccessary to make a connection to a remote server.";
    public enum Datum {
        REMOTE_USER_DEFINITIONS,
        DEFAULT_REMOTE_USER_DEFINITION_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public T8DefaultRemoteServerConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REMOTE_USER_DEFINITIONS.toString(), "Remote Users", "All of the remote users that are available on this remote connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DEFAULT_REMOTE_USER_DEFINITION_IDENTIFIER.toString(), "Default Remote User", "The default remote user that will be used by system sessions."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.REMOTE_USER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RemoteConnectionUser.GROUP_IDENTIFIER));
        else if(Datum.DEFAULT_REMOTE_USER_DEFINITION_IDENTIFIER.toString().equals(datumIdentifier)) return createLocalIdentifierOptionsFromDefinitions(getLocalDefinitionsOfType(T8RemoteUserDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Connection getNewConnectionInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.webservice.T8RemoteWebServiceConnector", new Class<?>[]{T8Context.class, T8DefaultRemoteServerConnectionDefinition.class}, context, this);
    }

    public String getServiceURL()
    {
        return getURL() + "/Rest";
    }

    @Override
    public List<T8RemoteUserDefinition> getRemoteUserDefinitions()
    {
        return getDefinitionDatum(Datum.REMOTE_USER_DEFINITIONS);
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.remote.T8RemoteServerConnectionTestHarness", new Class<?>[]{});
    }

    public void setRemoteUserDefinitions(List<T8RemoteUserDefinition> remoteUserDefinitions)
    {
        setDefinitionDatum(Datum.REMOTE_USER_DEFINITIONS, remoteUserDefinitions);
    }

    public T8RemoteUserDefinition getDefaultRemoteUserDefinition()
    {
        T8RemoteUserDefinition defaultDefinition;

        defaultDefinition = (T8RemoteUserDefinition) getSubDefinition(getDefaultRemoteUserDefinitionIdentifier());
        if (defaultDefinition == null) defaultDefinition = (T8RemoteUserDefinition) getLocalDefinitionsOfType(T8RemoteUserDefinition.TYPE_IDENTIFIER).get(0);

        return defaultDefinition;
    }

    public String getDefaultRemoteUserDefinitionIdentifier()
    {
        return getDefinitionDatum(Datum.DEFAULT_REMOTE_USER_DEFINITION_IDENTIFIER);
    }

    public void setDefaultRemoteUserDefinitionIdentifier(String remoteUserDefinition)
    {
        setDefinitionDatum(Datum.DEFAULT_REMOTE_USER_DEFINITION_IDENTIFIER, remoteUserDefinition);
    }
}
