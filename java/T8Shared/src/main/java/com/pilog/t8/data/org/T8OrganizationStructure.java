package com.pilog.t8.data.org;

import com.pilog.t8.utilities.collections.ArrayLists;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationStructure implements Serializable
{
    private final T8Organization structure;
    private final String ontologyStructureID;

    public enum OrganizationSetType
    {
        LINEAGE,    // From a specific organization, all ancestors, all descendants including the organization itself.
        ANCESTORS,  // From a specific organization, all ancestors, excluding the organization itself.
        LINE,       // From a specific organization, all ancestors, including the organization itself.
        SINGLE,     // From a specific organization, only the specific organization itself.
        HOUSE,      // From a specific organization, all organizations with the same parent, including the organization itself and its parent.
        SIBLINGS,   // From a specific organization, all organizations with the same parent, excluding the organization itself.
        BROOD,      // From a specific organization, all organizations with the same parent, including the organization itself.
        FAMILY,     // From a specific organization, all descendants, including the organization itself.
        DESCENDANTS // From a specific organization, all descendants, excluding the organization itself.
    };

    public T8OrganizationStructure(T8Organization rootNode, String ontologyStructureID)
    {
        this.structure = rootNode;
        this.ontologyStructureID = ontologyStructureID;
    }

    public String getRootOrganizationID()
    {
        return structure.getID();
    }

    public T8Organization getRootNode()
    {
        return structure;
    }

    /**
     * @deprecated Use {@code getOntologyStructureID()}.
     */
    @Deprecated
    public String getOrganizationDataStructureID()
    {
        return ontologyStructureID;
    }

    public String getOntologyStructureID()
    {
        return ontologyStructureID;
    }

    public boolean containsNode(String organizationID)
    {
        return getNode(organizationID) != null;
    }

    public List<String> getOrganizationIDList()
    {
        List<T8Organization> nodes;

        nodes = getNodes();
        if (nodes != null)
        {
            List<String> idList;

            idList = new ArrayList<String>();
            for (T8Organization node : nodes)
            {
                idList.add(node.getID());
            }

            return idList;
        }
        else return null;
    }

    /**
     * Returns the level in the organization structure where the specified
     * organization is currently attached.  The root organization is at level 0.
     * @param organizationID The organization for which to return the level.
     * @return The level of the specified organization in the organization
     * structure.
     */
    public int getOrganizationLevel(String organizationID)
    {
        return getOrganizationPathIDList(organizationID).size() - 1;
    }

    /**
     * This method returns the list of organization ID's from the root to the
     * specified node (inclusive).
     * @param organizationID The organization ID for which to retrieve the
     * path ID set.
     * @return The path set of organization ID's to which the specified
     * organization belongs.
     */
    public List<String> getOrganizationPathIDList(String organizationID)
    {
        T8Organization node;
        List<String> idList;

        idList = new ArrayList<String>();
        node = getNode(organizationID);
        if (node != null)
        {
            for (T8Organization pathNode : node.getPathNodes())
            {
                idList.add(pathNode.getID());
            }
        }

        return idList;
    }

    /**
     * This method returns the list of organization ID's that are siblings of
     * the specified organization node.
     * @param organizationID The organization ID for which to retrieve the
     * descendant ID set.
     * @return The set of organization ID's that are siblings of the
     * specified organization.
     */
    public List<String> getOrganizationSiblingIdList(String organizationID)
    {
        T8Organization organization;
        List<String> idList;

        idList = new ArrayList<String>();
        organization = getNode(organizationID);
        if (organization != null)
        {
            for (T8Organization siblingOrganization : organization.getSiblingOrganizations())
            {
                idList.add(siblingOrganization.getID());
            }
        }

        return idList;
    }

    /**
     * This method returns the list of organization ID's that are part of
     * the same house as the specified organization node.
     * @param organizationID The organization ID for which to retrieve the
     * descendant ID set.
     * @return The set of organization ID's that are part of
     * the same house as the specified organization node.
     */
    public List<String> getOrganizationHouseIdList(String organizationID)
    {
        T8Organization organization;
        List<String> idList;

        idList = new ArrayList<String>();
        organization = getNode(organizationID);
        if (organization != null)
        {
            for (T8Organization houseOrganization : organization.getHouseOrganizations())
            {
                idList.add(houseOrganization.getID());
            }
        }

        return idList;
    }

    /**
     * This method returns the list of organization ID's that are part of
     * the same brood as the specified organization node.
     * @param organizationID The organization ID for which to retrieve the
     * descendant ID set.
     * @return The set of organization ID's that are part of
     * the same brood as the specified organization node.
     */
    public List<String> getOrganizationBroodIdList(String organizationID)
    {
        T8Organization organization;
        List<String> idList;

        idList = new ArrayList<String>();
        organization = getNode(organizationID);
        if (organization != null)
        {
            for (T8Organization broodOrganization : organization.getBroodOrganizations())
            {
                idList.add(broodOrganization.getID());
            }
        }

        return idList;
    }

    /**
     * This method returns the list of organization ID's that are ancestors of
     * the specified organization node.
     * @param organizationID The organization ID for which to retrieve the
     * descendant ID set.
     * @return The set of organization ID's that are ancestors of the
     * specified organization.
     */
    public List<String> getOrganizationAncestorIdList(String organizationID)
    {
        T8Organization node;
        List<String> idList;

        idList = new ArrayList<String>();
        node = getNode(organizationID);
        if (node != null)
        {
            for (T8Organization ancestorOrganization : node.getAncestorOrganizations())
            {
                idList.add(ancestorOrganization.getID());
            }
        }

        return idList;
    }

    /**
     * This method returns the list of organization ID's that are descendants of
     * the specified organization node.
     * @param organizationID The organization ID for which to retrieve the
     * descendant ID set.
     * @return The set of organization ID's that are descendants of the
     * specified organization.
     */
    public List<String> getOrganizationDescendantIDList(String organizationID)
    {
        T8Organization node;
        List<String> idList;

        idList = new ArrayList<String>();
        node = getNode(organizationID);
        if (node != null)
        {
            for (T8Organization descendantNode : node.getDescendentNodes())
            {
                idList.add(descendantNode.getID());
            }
        }

        return idList;
    }

    public List<String> getOrganizationIdSet(OrganizationSetType setType, String organizationId)
    {
        List<String> idList;

        switch (setType)
        {
            case LINEAGE:
                return getOrganizationLineageIDList(organizationId);
            case FAMILY:
                idList = getOrganizationDescendantIDList(organizationId);
                idList.add(0, organizationId);
                return idList;
            case DESCENDANTS:
                return getOrganizationDescendantIDList(organizationId);
            case ANCESTORS:
                return getOrganizationAncestorIdList(organizationId);
            case LINE:
                return getOrganizationPathIDList(organizationId);
            case SINGLE:
                return ArrayLists.newArrayList(organizationId);
            case SIBLINGS:
                return getOrganizationSiblingIdList(organizationId);
            case HOUSE:
                return getOrganizationHouseIdList(organizationId);
            case BROOD:
                return getOrganizationBroodIdList(organizationId);
            default:
                throw new IllegalArgumentException("Unsupported Organization Set Type: " + setType);
        }
    }

    /**
     * This method returns the list of organization ID's from the root to the
     * specified node and also includes the descendants of the specified node
     * but not its siblings or any of their descendants.
     * @param organizationId The organization ID for which to retrieve the
     * lineage ID set.
     * @return The lineage set of organization ID's to which the specified
     * organization belongs.
     */
    public List<String> getOrganizationLineageIDList(String organizationId)
    {
        List<String> idList;

        idList = getOrganizationPathIDList(organizationId);
        idList.addAll(getOrganizationDescendantIDList(organizationId));
        return idList;
    }

    public List<T8Organization> getNodes()
    {
        if (structure != null)
        {
            List<T8Organization> structureNodes;

            structureNodes = structure.getDescendentNodes();
            structureNodes.add(0, structure);
            return structureNodes;
        }
        else return null;
    }

    public T8Organization getNode(String organizationId)
    {
        LinkedList<T8Organization> nodeList;

        nodeList = new LinkedList<T8Organization>();
        nodeList.add(structure);
        while (nodeList.size() > 0)
        {
            T8Organization nextNode;

            nextNode = nodeList.pop();
            if (nextNode.getID().equals(organizationId))
            {
                return nextNode;
            }
            else
            {
                nodeList.addAll(nextNode.getChildNodes());
            }
        }

        return null;
    }

    public int getSize()
    {
        if (structure != null)
        {
            return structure.countDescendants() + 1;
        }
        else return 0;
    }

    public List<String> getNestedIdList()
    {
        List<String> nestedList;

        nestedList = new ArrayList<String>();
        if (structure != null) structure.addNestedIdList(nestedList);
        return nestedList;
    }
}
