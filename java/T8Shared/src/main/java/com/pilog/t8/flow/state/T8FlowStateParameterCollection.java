package com.pilog.t8.flow.state;

import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowStateParameterCollection
{
    public String getParameterIid();
    public T8FlowStateParameterMap createNewParameterMap(T8FlowStateParameterCollection parentCollection, String parameterIid, T8ParameterStateDataSet dataSet);
    public T8FlowStateParameterList createNewParameterList(T8FlowStateParameterCollection parentCollection, String parameterIid, T8ParameterStateDataSet dataSet);
}
