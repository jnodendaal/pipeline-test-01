package com.pilog.t8.ui.functionality.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityExecutionListener extends EventListener
{
    public void functionalityExecutionEnded(T8FunctionalityExecutionEndedEvent event);
    public void functionalityExecutionUpdated(T8FunctionalityExecutionUpdatedEvent event);
}
