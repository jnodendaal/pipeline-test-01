package com.pilog.t8.definition.data.filter;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataFilterClauseDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_FILTER_CLAUSE";
    public static final String STORAGE_PATH = null;
    public enum Datum {CONJUNCTION};
    // -------- Definition Meta-Data -------- //

    public T8DataFilterClauseDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONJUNCTION.toString(), "Conjunction", "The conjunction type to use when joining this filter clause to others.", DataFilterConjunction.AND.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONJUNCTION.toString().equals(datumIdentifier)) return createStringOptions(DataFilterConjunction.values());
        else return null;
    }

    public DataFilterConjunction getConjunction()
    {
        return DataFilterConjunction.valueOf((String)getDefinitionDatum(Datum.CONJUNCTION.toString()));
    }

    public void setConjunction(DataFilterConjunction conjunction)
    {
        setDefinitionDatum(Datum.CONJUNCTION.toString(), conjunction.toString());
    }

    public abstract T8DataFilterClause getNewDataFilterClauseInstance(T8Context context, Map<String, Object> filterParameters);
}
