package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtFunctionalityHandle extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FUNCTIONALITY_HANDLE";

    public T8DtFunctionalityHandle() {}

    public T8DtFunctionalityHandle(T8DefinitionManager context) {}

    public T8DtFunctionalityHandle(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FunctionalityHandle.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8FunctionalityHandle functionalityHandle;
            Map<String, Object> parameters;
            JsonObject handleObject;

            // Create the concept JSON object.
            functionalityHandle = (T8FunctionalityHandle)object;
            handleObject = new JsonObject();
            handleObject.add("id", functionalityHandle.getId());
            handleObject.add("name", functionalityHandle.getDisplayName());
            handleObject.addIfNotNull("description", functionalityHandle.getDescription());
            handleObject.addIfNotNull("iconUri", functionalityHandle.getIconUri());
            handleObject.add("enabled", functionalityHandle.isEnabled());
            handleObject.addIfNotNull("confirmationMessage", functionalityHandle.getConfirmationMessage());
            handleObject.addIfNotNull("disabledMessage", functionalityHandle.getDisabledMessage());

            // Serialize the functionality parameters.
            parameters = functionalityHandle.getParameters();
            if (!parameters.isEmpty())
            {
                T8DtMap dtMap;
                
                dtMap = new T8DtMap(null);
                handleObject.add("parameters", dtMap.serialize(functionalityHandle.getParameters()));
            }

            // Return the handle object.
            return handleObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8FunctionalityHandle deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8FunctionalityHandle handle;
            JsonObject jsonParameters;
            JsonObject jsonHandle;
            String functionalityId;
            String name;
            String description;
            String iconUri;
            Boolean enabled;
            String confirmationMessage;
            String disabledMessage;

            // Get the JSON values.
            jsonHandle = jsonValue.asObject();
            functionalityId = jsonHandle.getString("id");
            name = jsonHandle.getString("name");
            description = jsonHandle.getString("description");
            iconUri = jsonHandle.getString("iconUri");
            enabled = jsonHandle.getBoolean("enabled");
            confirmationMessage = jsonHandle.getString("confirmationMessage");
            disabledMessage = jsonHandle.getString("disabledMessage");
            jsonParameters = jsonHandle.getJsonObject("parameters");

            // Create the functionality handle object.
            handle = new T8FunctionalityHandle(functionalityId);
            handle.setDisplayName(name);
            handle.setDescription(description);
            handle.setIconUri(iconUri);
            handle.setEnabled(enabled != null && enabled);
            handle.setConfirmationMessage(confirmationMessage);
            handle.setDisabledMessage(disabledMessage);

            // Deserialize the functionality parameters (if any).
            if (jsonParameters != null)
            {
                T8DtMap dtMap;

                dtMap = new T8DtMap(null);
                handle.setParameters(dtMap.deserialize(jsonParameters));
            }

            // Return the completed object.
            return handle;
        }
        else return null;
    }
}