package com.pilog.t8.definition;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionGroupMetaData implements Serializable
{
    private String groupId;
    private String name;
    private String description;

    public T8DefinitionGroupMetaData()
    {
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
