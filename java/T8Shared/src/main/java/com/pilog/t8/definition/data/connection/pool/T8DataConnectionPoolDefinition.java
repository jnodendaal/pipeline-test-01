package com.pilog.t8.definition.data.connection.pool;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataConnectionPoolDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_CONNECTION_POOL";
    public static final String GROUP_NAME = "Data Connection Pools";
    public static final String GROUP_DESCRIPTION = "Configuration settings for connection pools used when creating and maintaining connections to various data stores.";
    public static final String STORAGE_PATH = "/data_connection_pools";
    public static final String IDENTIFIER_PREFIX = "DATA_CONNECTION_POOL_";
    private enum Datum
    {
        MIN_POOL_SIZE,
        MAX_POOL_SIZE,
        RETRY_ON_CONNECTION_FAILURE
    };
    // -------- Definition Meta-Data -------- //

    public T8DataConnectionPoolDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MIN_POOL_SIZE.toString(), "Min Connection Pool Size", "The minimum connections this pool will keep alive.", 5));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MAX_POOL_SIZE.toString(), "Max Connection Pool Size", "The maximum amount of connections this connection pool will allow to be opened to the database", 200));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RETRY_ON_CONNECTION_FAILURE.toString(), "Retry On Connection Failure", "If this option is set to false, the system will not try to reastablish a connection to the database if it failed when the pool is created.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8DataConnectionPool createNewConnectionPoolInstance(T8DataTransaction dataAccessProvider) throws Exception;

    public int getMinPoolSize()
    {
        Integer size;

        size = (Integer) getDefinitionDatum(Datum.MIN_POOL_SIZE.toString());
        return size != null ? size : 5;
    }

    public void SetMinPoolSize(int size)
    {
        setDefinitionDatum(Datum.MIN_POOL_SIZE.toString(), size);
    }

    public int getMaxPoolSize()
    {
        Integer size;

        size = (Integer) getDefinitionDatum(Datum.MAX_POOL_SIZE.toString());
        return size != null ? size : 200;
    }

    public void SetMaxPoolSize(int size)
    {
        setDefinitionDatum(Datum.MAX_POOL_SIZE.toString(), size);
    }

    public boolean retryOnConnectionFailure()
    {
        Boolean retry;

        retry = (Boolean) getDefinitionDatum(Datum.RETRY_ON_CONNECTION_FAILURE.toString());
        return retry != null ? retry : true;
    }

    public void setRetryOnConnectionFailure(boolean retry)
    {
        setDefinitionDatum(Datum.RETRY_ON_CONNECTION_FAILURE.toString(), retry);
    }
}
