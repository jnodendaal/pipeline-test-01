package com.pilog.t8.definition.editor;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.ui.datumeditor.T8DefinitionDatumEditorDefinition;
import com.pilog.t8.definition.ui.editor.T8DefinitionEditorDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionEditorSetupDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_EDITOR_SETUP";
    public static final String GROUP_NAME = "Definition Editor Setup";
    public static final String GROUP_DESCRIPTION = "Configuraitons of the editor(s) to use when editing a specific definition type.";
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_EDITOR_SETUP";
    public static final String DISPLAY_NAME = "Definition Editor Setup";
    public static final String DESCRIPTION = "A definition of the editor(s) to use when editing a specific definition type.";
    public static final String STORAGE_PATH = "/definition_editor_setups";
    public static final String VERSION = "0";
    public enum Datum {DEFINITION_TYPE_IDENTIFIER, DEFINITION_EDITOR_DEFINITION, DATUM_EDITOR_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionEditorSetupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DEFINITION_TYPE_IDENTIFIER.toString(), "Definition Type", "The definition type to which this editor setup applies."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.DEFINITION_EDITOR_DEFINITION.toString(), "Definition Editor", "An editor that will replace the default definition editor when definitions of this type is edited."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATUM_EDITOR_DEFINITIONS.toString(), "Datum Editor", "The editors to use for specific definition datums."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DEFINITION_TYPE_IDENTIFIER.toString().equals(datumIdentifier)) return createStringOptions(definitionContext.getDefinitionTypeIdentifiers());
        else if (Datum.DATUM_EDITOR_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DefinitionDatumEditorDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getDefinitionTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIER.toString());
    }

    public void setDefinitionTypeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIER.toString(), identifier);
    }

    public T8DefinitionEditorDefinition getDefinitionEditorDefinition()
    {
        return (T8DefinitionEditorDefinition)getDefinitionDatum(Datum.DEFINITION_EDITOR_DEFINITION.toString());
    }

    public void setDefinitionEditorDefinition(T8DefinitionEditorDefinition definition)
    {
        setDefinitionDatum(Datum.DEFINITION_EDITOR_DEFINITION.toString(), definition);
    }

    public List<T8DefinitionDatumEditorDefinition> getDefinitionDatumEditorDefinitions()
    {
        return (List<T8DefinitionDatumEditorDefinition>)getDefinitionDatum(Datum.DATUM_EDITOR_DEFINITIONS.toString());
    }

    public void setDefinitionDatumEditorDefinitions(List<T8DefinitionDatumEditorDefinition> definitions)
    {
        setDefinitionDatum(Datum.DATUM_EDITOR_DEFINITIONS.toString(), definitions);
    }

    public T8DefinitionDatumEditorDefinition getDefinitionDatumEditorDefinition(String datumIdentifier)
    {
        for (T8DefinitionDatumEditorDefinition datumEditorDefinition : getDefinitionDatumEditorDefinitions())
        {
            if (datumEditorDefinition.getDatumIdentifier().equals(datumIdentifier))
            {
                return datumEditorDefinition;
            }
        }

        return null;
    }
}
