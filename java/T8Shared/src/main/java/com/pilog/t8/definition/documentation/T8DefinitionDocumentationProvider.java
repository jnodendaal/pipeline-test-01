package com.pilog.t8.definition.documentation;

import com.pilog.t8.definition.T8Definition;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionDocumentationProvider
{
    public boolean providesDocumentationType(String typeIdentifier);
    public T8DefinitionDocumentationType getDocumentationType(String typeIdentifier);
    public List<T8DefinitionDocumentationType> getDocumentationTypes();
    public StringBuffer getHTMLDocumentation(T8Definition definition, String typeIdentifier);
}
