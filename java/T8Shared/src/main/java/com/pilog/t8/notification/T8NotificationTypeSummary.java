package com.pilog.t8.notification;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationTypeSummary implements Serializable
{
    private final String notificationIdentifier;

    private String displayName;
    private String description;
    private int totalCount;
    private int newCount;

    public T8NotificationTypeSummary(String notificationIdentifier)
    {
        this.notificationIdentifier = notificationIdentifier;
    }

    public String getNotificationIdentifier()
    {
        return notificationIdentifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public int getNewCount()
    {
        return newCount;
    }

    public void setNewCount(int newCount)
    {
        this.newCount = newCount;
    }
}
