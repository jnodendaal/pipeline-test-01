package com.pilog.t8.definition;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionConstructorParameters implements Serializable
{
    private String identifier;
    private String typeIdentifier;
    private ArrayList<Object> parameters;

    public T8DefinitionConstructorParameters(String identifier, String typeIdentifier, ArrayList<Object> parameters)
    {
        this.identifier = identifier;
        this.typeIdentifier = typeIdentifier;
        this.parameters = parameters;
    }

    public ArrayList<Object> getParameters()
    {
        return parameters;
    }

    public String getTypeIdentifier()
    {
        return typeIdentifier;
    }

    public String getIdentifier()
    {
        return identifier;
    }
}
