package com.pilog.t8.definition.script;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.ui.submodule.T8SubModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentEventHandlerScriptDefinition extends T8DefaultScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_COMPONENT_EVENT_HANDLER";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_COMPONENT_EVENT_HANDLER";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Component Event Handler Script";
    public static final String DESCRIPTION = "A script that handles a component event.";
    public static final String VERSION = "0";
    public enum Datum {EVENT_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8ComponentEventHandlerScriptDefinition(String identifier)
    {
        super(identifier);
    }

    public T8ComponentEventHandlerScriptDefinition(String identifier, String eventIdentifier)
    {
        this(identifier);
        setEventIdentifier(eventIdentifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.EVENT_IDENTIFIER.toString(), "Event", "The Identifier of the Event that this script handles."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EVENT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8ComponentDefinition componentDefinition;

            componentDefinition = (T8ComponentDefinition)getParentDefinition();
            if (componentDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions(componentDefinition.getComponentEventDefinitions());
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8ComponentDefinition componentDefinition;

        componentDefinition = (T8ComponentDefinition)getParentDefinition();
        if (componentDefinition != null)
        {
            if (componentDefinition instanceof T8SubModuleDefinition)
            {
                T8ModuleDefinition moduleDefinition;
                T8SubModuleDefinition subModuleDefinition;

                subModuleDefinition = (T8SubModuleDefinition)componentDefinition;
                moduleDefinition = (T8ModuleDefinition)context.getServerContext().getDefinitionManager().getRawDefinition(context, getRootProjectId(), subModuleDefinition.getModuleIdentifier());
                if (moduleDefinition != null)
                {
                    T8ComponentEventDefinition eventDefinition;
                    String eventIdentifier;

                    eventIdentifier = getEventIdentifier();
                    eventIdentifier = T8IdentifierUtilities.stripNamespace(eventIdentifier, subModuleDefinition.getModuleIdentifier(), true);
                    eventDefinition = moduleDefinition.getComponentEventDefinition(eventIdentifier);
                    if (eventDefinition != null)
                    {
                        return eventDefinition.getParameterDefinitions();
                    }
                    else return null;
                }
                else return null;
            }
            else
            {
                T8ComponentEventDefinition eventDefinition;

                eventDefinition = componentDefinition.getComponentEventDefinition(getEventIdentifier());
                if (eventDefinition != null)
                {
                    return eventDefinition.getParameterDefinitions();
                }
                else return null;
            }
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8ComponentEventHandlerScriptCompletionHandler(context, this);
    }

    public String getEventIdentifier()
    {
        return (String)getDefinitionDatum(Datum.EVENT_IDENTIFIER.toString());
    }

    public void setEventIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.EVENT_IDENTIFIER.toString(), identifier);
    }
}
