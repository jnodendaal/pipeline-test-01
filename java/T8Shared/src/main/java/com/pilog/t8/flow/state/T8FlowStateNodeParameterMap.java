package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateNodeParameterMap extends T8FlowStateParameterMap
{
    protected final T8FlowNodeState parentNodeState;

    public T8FlowStateNodeParameterMap(T8FlowNodeState parentNodeState, T8FlowStateParameterCollection parentCollection, String parameterIID, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        super(parentCollection, parentNodeState.getFlowIid(), parameterIID, entityIdentifier, null);
        this.parentNodeState = parentNodeState;
        if (dataSet != null) addParameters(dataSet);
    }

    @Override
    public synchronized T8DataEntity createEntity(int index, String key, T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;

        newEntity = super.createEntity(index, key, entityDefinition);
        newEntity.setFieldValue(entityId + "$NODE_IID", parentNodeState.getNodeIid());
        return newEntity;
    }

    @Override
    public synchronized T8FlowStateParameterMap createNewParameterMap(T8FlowStateParameterCollection newMapParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        return new T8FlowStateNodeParameterMap(parentNodeState, newMapParentCollection, parameterIID, entityId, dataSet);
    }

    @Override
    public synchronized T8FlowStateParameterList createNewParameterList(T8FlowStateParameterCollection newListParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        return new T8FlowStateNodeParameterList(parentNodeState, newListParentCollection, parameterIID, entityId, dataSet);
    }
}
