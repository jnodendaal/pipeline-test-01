package com.pilog.t8.data;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Bouwer du Preez
 */
public interface T8DataConnectionPool
{
    public void setDataConnectionIdentifier(String identifier);
    public void setDriverClassName(String className) throws Exception;
    public void setConnectionURL(String connectionURL);
    public void setUsername(String username);
    public void setPassword(String password);

    public String getDataConnectionIdentifier();
    public Connection getConnection() throws SQLException;
    /**
     * The destroy method should leave the connection pool in a state where the
     * pool will be recreated on the next {@link #getConnection()} call, if the
     * configuration specified allows for this.
     */
    public void destroy();
}
