package com.pilog.t8.definition.data.entity;

import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public final class T8DataEntityFieldResourceDefinition extends T8DataEntityFieldDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ENTITY_FIELD_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8DataEntityFieldResourceDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataEntityFieldResourceDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, T8DataType dataType)
    {
        super(identifier, sourceIdentifier, key, required, dataType);
    }

    public T8DataEntityFieldResourceDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, String dataType)
    {
        super(identifier, sourceIdentifier, key, required, dataType);
    }
}
