package com.pilog.t8.definition.documentation;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDocumentationType implements Serializable
{
    private String identifier;
    private String displayName;
    private String description;
    
    public T8DefinitionDocumentationType(String identifier, String displayName, String description)
    {
        this.identifier = identifier;
        this.displayName = displayName;
        this.description = description;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getDescription()
    {
        return description;
    }
}
