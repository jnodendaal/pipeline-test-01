package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_MODULE";
    public static final String DISPLAY_NAME = "Module Functionality";
    public static final String DESCRIPTION = "A functionality that opens a specific module.";
    public static final String IDENTIFIER_PREFIX = "FNC_M_";
    public enum Datum
    {
        MODULE_IDENTIFIER,
        INPUT_PARAMETER_MAPPING,
        INPUT_PARAMETER_EXPRESSION_MAP,
        FINALIZATION_EVENT_IDENTIFIERS
    };
    // -------- Definition Meta-Data -------- //

    public T8ModuleFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MODULE_IDENTIFIER.toString(), "Module Identifier", "The identifier of the module linked to this functionality."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Functionality Parameters to Module Input", "A mapping of functionality parameters to the corresponding input parameters of the module."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), "Input Parameter Expression Map", "The parameters the will be sent to the module as inputs."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_LIST, Datum.FINALIZATION_EVENT_IDENTIFIERS.toString(), "Finalization Events", "The event that will signal the end of this functionality."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MODULE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ModuleDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8ModuleDefinition moduleDefinition;
            String projectId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            projectId = getRootProjectId();
            moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(projectId, getModuleIdentifier());
            if (moduleDefinition != null)
            {
                List<T8DataParameterDefinition> moduleInputParameterDefinitions;
                List<T8DataParameterDefinition> functionalityParameterDefinitions;
                HashMap<String, List<String>> identifierMap;

                moduleInputParameterDefinitions = moduleDefinition.getInputParameterDefinitions();
                functionalityParameterDefinitions = getInputParameterDefinitions();

                identifierMap = new HashMap<String, List<String>>();
                if ((moduleInputParameterDefinitions != null) && (functionalityParameterDefinitions != null))
                {
                    for (T8DataParameterDefinition functionalityParameterDefinition : functionalityParameterDefinitions)
                    {
                        ArrayList<String> idList;

                        idList = new ArrayList<String>();
                        for (T8DataParameterDefinition moduleInputParameterDefinition : moduleInputParameterDefinitions)
                        {
                            idList.add(moduleInputParameterDefinition.getPublicIdentifier());
                        }

                        identifierMap.put(functionalityParameterDefinition.getIdentifier(), idList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumIdentifier))
        {
            if(Strings.isNullOrEmpty(getModuleIdentifier()))
            {
                return new ArrayList<T8DefinitionDatumOption>(0);
            }
            else
            {
                Map<String, List<String>> ids;
                ArrayList<T8DefinitionDatumOption> optionList;
                T8ModuleDefinition moduleDefinition;

                ids = new HashMap<String, List<String>>();
                optionList = new ArrayList<T8DefinitionDatumOption>(1);
                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), getModuleIdentifier());
                for (T8DataParameterDefinition t8DataParameterDefinition : moduleDefinition.getInputParameterDefinitions())
                {
                    ids.put(t8DataParameterDefinition.getPublicIdentifier(), null);
                }
                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", ids));
                return optionList;
            }
        }
        else if (Datum.FINALIZATION_EVENT_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            String moduleId;

            moduleId = getModuleIdentifier();
            if (moduleId != null)
            {
                T8ModuleDefinition moduleDefinition;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                return createPublicIdentifierOptionsFromDefinitions((List)moduleDefinition.getModuleEventDefinitions());
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumIdentifier)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8ModuleFunctionalityInstance").getConstructor(T8Context.class, T8ModuleFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8ModuleFunctionalityInstance").getConstructor(T8Context.class, T8ModuleFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }

    public String getModuleIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_IDENTIFIER.toString());
    }

    public void setModuleIdentifier(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.MODULE_IDENTIFIER.toString(), moduleIdentifier);
    }

    public Map<String, String> getInputParameterExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setInputParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), expressionMap);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public List<String> getFinalizationEventIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.FINALIZATION_EVENT_IDENTIFIERS.toString());
    }

    public void setFinalizationEventIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.FINALIZATION_EVENT_IDENTIFIERS.toString(), identifiers);
    }
}
