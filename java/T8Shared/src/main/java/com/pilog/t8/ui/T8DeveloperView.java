package com.pilog.t8.ui;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DeveloperView
{
    public void startComponent();
    public void stopComponent();
    
    public String getHeader();

    public T8DeveloperViewFactory getViewFactory();
    public void setViewFactory(T8DeveloperViewFactory viewFactory);

    public void addView(T8DeveloperView view);
    public void addView(T8DeveloperView view, String viewHeader);
    public void removeView(T8DeveloperView view);
    public void definitionLinkActivated(String identifier);
    public void setSelectedDefinition(T8Definition definition);
    public T8Definition getSelectedDefinition();

    public void addDefinitionViewListener(T8DefinitionViewListener listener);
    public void removeDefinitionViewListener(T8DefinitionViewListener listener);

    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event);

    /**
     * This method will be called before a developer view is closed.
     * @return If this view is allowed to close it must return true, false if the view should stay open
     */
    public boolean canClose();
}
