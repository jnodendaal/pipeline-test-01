package com.pilog.t8.definition.flow;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowStateHandler;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8GraphManager;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.communication.T8TaskIssuedCommunicationDefinition;
import com.pilog.t8.definition.flow.phase.T8FlowPhaseDefinition;
import com.pilog.t8.definition.flow.edge.T8WorkFlowEdgeDefinition;
import com.pilog.t8.definition.flow.node.activity.T8FlowActivityDefinition;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityDefinition;
import com.pilog.t8.definition.flow.node.event.T8FlowEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalCatchEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8StartEventDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8FlowGatewayDefinition;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.definition.graph.T8GraphEdgeDefinition;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowDefinition extends T8GraphDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_WORK";
    public static final String GROUP_NAME = "Workflows";
    public static final String GROUP_DESCRIPTION = "Business processes that involve user participation and employ system operations to achieve predefined goals.";
    public static final String STORAGE_PATH = "/work_flows";
    public static final String DISPLAY_NAME = "Work Flow";
    public static final String DESCRIPTION = "A process to be executed by multiple users, each participating at a different stage of execution.";
    public static final String IDENTIFIER_PREFIX = "FLOW_";
    public enum Datum
    {
        FLOW_PARAMETER_DEFINITIONS,
        FLOW_DATA_OBJECT_DEFINITIONS,
        PHASE_DEFINITIONS,
        TASK_GROUP_DEFINITIONS,
        TASK_ISSUED_COMMUNICATION_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowDefinition(String identifier)
    {
        super(identifier);
        addDefaultStartNode();
    }

    private void addDefaultStartNode()
    {
        String nodeIdentifier;

        nodeIdentifier = getIdentifier().substring(1); // Remove the @.
        nodeIdentifier = "$START_" + nodeIdentifier;
        addSubDefinition(T8GraphDefinition.Datum.NODE_DEFINITIONS.toString(), new T8StartEventDefinition(nodeIdentifier), -1, false);
        setDefinitionDatum(T8GraphDefinition.Datum.START_NODE_IDENTIFIER.toString(), nodeIdentifier, false, this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FLOW_PARAMETER_DEFINITIONS.toString(), "Flow Parameters",  "The data parameters that will be used throughout the flow, passing between nodes and possibly being evaluated in gateway expressions.").addDatumDependency(new T8DefinitionDatumDependency(T8FlowDataObjectDefinition.TYPE_IDENTIFIER, T8FlowDataObjectDefinition.Datum.KEY_PARAMETER_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FLOW_DATA_OBJECT_DEFINITIONS.toString(), "Data Objects",  "The flow data objects used by this flow during execution."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PHASE_DEFINITIONS.toString(), "Flow Phases",  "The phases defined for this flow."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TASK_GROUP_DEFINITIONS.toString(), "Task Groups",  "The groups into which tasks in this workflow are organized for escalation and reporting purposes."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString(), "Task Issued Communications",  "The communications to send when specified tasks have been issued."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.FLOW_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.FLOW_DATA_OBJECT_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FlowDataObjectDefinition.GROUP_IDENTIFIER));
        else if (T8GraphDefinition.Datum.NODE_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8WorkFlowNodeDefinition.GROUP_IDENTIFIER));
        else if (Datum.PHASE_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FlowPhaseDefinition.GROUP_IDENTIFIER));
        else if (Datum.TASK_GROUP_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FlowTaskGroupDefinition.GROUP_IDENTIFIER));
        else if (Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TaskIssuedCommunicationDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.FLOW_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else if (Datum.FLOW_DATA_OBJECT_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8GraphEdgeDefinition createEdgeDefinition(String parentIdentifier, String childIdentifier)
    {
        return new T8WorkFlowEdgeDefinition(T8Definition.getLocalIdPrefix() + T8IdentifierUtilities.createNewGUID(), parentIdentifier, childIdentifier);
    }

    @Override
    public T8WorkFlowNodeDefinition getNodeDefinition(String nodeIdentifier)
    {
        return (T8WorkFlowNodeDefinition)super.getNodeDefinition(nodeIdentifier);
    }

    public T8Flow getNewFlowInstance(T8Context context, T8FlowController flowController, T8FlowStateHandler stateHandler, T8FlowState flowState) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.T8DefaultFlow").getConstructor(T8Context.class, T8FlowController.class, T8FlowStateHandler.class, T8WorkFlowDefinition.class, T8FlowState.class);
        return (T8Flow)constructor.newInstance(context, flowController, stateHandler, this, flowState);
    }

    @Override
    public T8GraphManager getGraphManager(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.developer.definitions.graphview.flow.T8WorkFlowGraphManager").getConstructor(T8Context.class);
        return (T8GraphManager)constructor.newInstance(context);
    }

    public List<T8FlowPhaseDefinition> getPhaseDefinitions()
    {
        return (List<T8FlowPhaseDefinition>)getDefinitionDatum(Datum.PHASE_DEFINITIONS.toString());
    }

    public void setPhaseDefinitions(List<T8FlowPhaseDefinition> definitions)
    {
        setDefinitionDatum(Datum.PHASE_DEFINITIONS.toString(), definitions);
    }

    public List<T8FlowTaskGroupDefinition> getTaskGroupDefinitions()
    {
        return (List<T8FlowTaskGroupDefinition>)getDefinitionDatum(Datum.TASK_GROUP_DEFINITIONS.toString());
    }

    public void setTaskGroupDefinitions(List<T8FlowTaskGroupDefinition> definitions)
    {
        setDefinitionDatum(Datum.TASK_GROUP_DEFINITIONS.toString(), definitions);
    }

    public void addFlowParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        getFlowParameterDefinitions().add(parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getFlowParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.FLOW_PARAMETER_DEFINITIONS.toString());
    }

    public void setFlowParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.FLOW_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }

    public List<T8FlowDataObjectDefinition> getFlowDataObjectDefinitions()
    {
        return (List<T8FlowDataObjectDefinition>)getDefinitionDatum(Datum.FLOW_DATA_OBJECT_DEFINITIONS.toString());
    }

    public void setFlowDataObjectDefinitions(List<T8FlowDataObjectDefinition> definitions)
    {
        setDefinitionDatum(Datum.FLOW_DATA_OBJECT_DEFINITIONS.toString(), definitions);
    }

    public T8FlowDataObjectDefinition getFlowDataObjectDefinition(String identifier)
    {
        for (T8FlowDataObjectDefinition objectDefinition : getFlowDataObjectDefinitions())
        {
            if (objectDefinition.getIdentifier().equals(identifier)) return objectDefinition;
        }

        return null;
    }

    public List<String> getActivityNodeIdentifiers()
    {
        List<String> nodeIdentifiers;

        nodeIdentifiers = new ArrayList<String>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8FlowActivityDefinition)
            {
                nodeIdentifiers.add(nodeDefinition.getIdentifier());
            }
        }

        return nodeIdentifiers;
    }

    public List<T8FlowActivityDefinition> getActivityNodeDefinitions()
    {
        List<T8FlowActivityDefinition> nodeDefinitions;

        nodeDefinitions = new ArrayList<T8FlowActivityDefinition>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8FlowActivityDefinition)
            {
                nodeDefinitions.add((T8FlowActivityDefinition)nodeDefinition);
            }
        }

        return nodeDefinitions;
    }

    public List<String> getEventNodeIdentifiers()
    {
        List<String> nodeIdentifiers;

        nodeIdentifiers = new ArrayList<String>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8FlowEventDefinition)
            {
                nodeIdentifiers.add(nodeDefinition.getIdentifier());
            }
        }

        return nodeIdentifiers;
    }

    public List<T8FlowEventDefinition> getEventNodeDefinitions()
    {
        List<T8FlowEventDefinition> nodeDefinitions;

        nodeDefinitions = new ArrayList<T8FlowEventDefinition>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8FlowEventDefinition)
            {
                nodeDefinitions.add((T8FlowEventDefinition)nodeDefinition);
            }
        }

        return nodeDefinitions;
    }

    public List<String> getGatewayNodeIdentifiers()
    {
        List<String> nodeIdentifiers;

        nodeIdentifiers = new ArrayList<String>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8FlowGatewayDefinition)
            {
                nodeIdentifiers.add(nodeDefinition.getIdentifier());
            }
        }

        return nodeIdentifiers;
    }

    public List<T8FlowGatewayDefinition> getGatewayNodeDefinitions()
    {
        List<T8FlowGatewayDefinition> nodeDefinitions;

        nodeDefinitions = new ArrayList<T8FlowGatewayDefinition>();
        for (T8GraphNodeDefinition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8FlowGatewayDefinition)
            {
                nodeDefinitions.add((T8FlowGatewayDefinition)nodeDefinition);
            }
        }

        return nodeDefinitions;
    }

    public Set<String> getCaughtSignalIdentifiers()
    {
        Set<String> signalIdentifiers;

        signalIdentifiers = new HashSet<String>();
        for (T8Definition nodeDefinition : getNodeDefinitions())
        {
            if (nodeDefinition instanceof T8SignalCatchEventDefinition)
            {
                signalIdentifiers.add(((T8SignalCatchEventDefinition)nodeDefinition).getSignalIdentifier());
            }
        }

        return signalIdentifiers;
    }

    public List<T8TaskIssuedCommunicationDefinition> getTaskIssuedCommunicationDefinitions()
    {
        return (List<T8TaskIssuedCommunicationDefinition>)getDefinitionDatum(Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString());
    }

    public void setTaskIssuedCommunicationDefinitions(List<T8TaskIssuedCommunicationDefinition> definitions)
    {
        setDefinitionDatum(Datum.TASK_ISSUED_COMMUNICATION_DEFINITIONS.toString(), definitions);
    }

    public List<String> getTaskActivityNodeIdentifiers()
    {
        return getNodeDefinitions().stream()
                .filter(nodeDefinition -> nodeDefinition instanceof T8TaskActivityDefinition)
                .map(T8GraphNodeDefinition::getIdentifier)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<T8TaskActivityDefinition> getTaskActivityNodeDefinitions()
    {
        return getNodeDefinitions().stream()
                .filter(nodeDefinition -> nodeDefinition instanceof T8TaskActivityDefinition)
                .map(nodeDefinition -> (T8TaskActivityDefinition)nodeDefinition)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
