package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Hennie Brink
 */
public class T8RemoteServerConnectionAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_TEST_REMOTE_SERVER_CONNECTION = "@OS_TEST_REMOTE_SERVER_CONNECTION"; // TODO: GBO - This operation should be removed or replaced with an appropriate method

    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_FAILURE_MESSAGE = "$P_FAILURE_MESSAGE";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_REMOTE_CONNECTION_IDENTIFIER = "$P_REMOTE_CONNECTION_IDENTIFIER";
    public static final String PARAMETER_REMOTE_CONNECTION_DEFINITION = "$P_REMOTE_CONNECTION_DEFINITION";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_TEST_REMOTE_SERVER_CONNECTION);
            definition.setMetaDescription("Test Remote Server Connection");
            definition.setClassName("com.pilog.t8.remote.server.connection.T8RemoteServerConnectionOperations$TestRemoteConnectionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project where the remote server connection to bet tested, is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REMOTE_CONNECTION_IDENTIFIER, "Remote Server Connection Identifier", "The Identifier of the remote server connection to test.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REMOTE_CONNECTION_DEFINITION, "Remote Server Connection Definition", "The definition that will be used to test the connection to the remote server.", T8DataType.DEFINITION));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "If the connection succeeded.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAILURE_MESSAGE, "Failure Message", "If the connection test failed, the exception message will be used as the failure message.", T8DataType.STRING));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
