package com.pilog.t8;

import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.system.event.T8ServerEventListener;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * The purpose of this interface is to act as a facade that exposes a selection of
 * server operations to clients.
 *
 * @author Bouwer du Preez
 */
public interface T8ServerContext
{
    // Server Manager access methods.
    public T8ServiceManager getServiceManager();
    public T8ProcessManager getProcessManager();
    public T8FlowManager getFlowManager();
    public T8FunctionalityManager getFunctionalityManager();
    public T8DefinitionManager getDefinitionManager();
    public T8SecurityManager getSecurityManager();
    public T8DataManager getDataManager();
    public T8FileManager getFileManager();
    public T8ConfigurationManager getConfigurationManager();
    public T8CommunicationManager getCommunicationManager();
    public T8NotificationManager getNotificationManager();
    public T8UserManager getUserManager();
    public T8ReportManager getReportManager();

    // Miscellaneous.
    public File getContextPath();
    public File getRealPath(String relativePath);
    public T8ExpressionEvaluator getExpressionEvaluator(T8Context context);

    // Server event handling.
    public void addServerEventListener(T8ServerEventListener listener);
    public void removeServerEventListener(T8ServerEventListener listener);

    // Server operation execution methods.
    public List<T8ServerOperationDefinition> getOperationDefinitions();
    public T8ServerOperationDefinition getOperationDefinition(T8Context context, String operationId);
    public T8ServerOperationStatusReport getOperationStatus(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport stopOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport cancelOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport pauseOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport resumeOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport executeAsynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception;
    public Map<String, Object> executeSynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception;
    public String queueOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception;
    public List<T8ServerOperationStatusReport> getServerOperationStatusReports(T8Context context);
}
