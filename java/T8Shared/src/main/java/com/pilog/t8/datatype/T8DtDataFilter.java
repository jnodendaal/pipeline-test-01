package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterSerializer;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Gavin Boshoff
 */
public class T8DtDataFilter extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILTER";

    public T8DtDataFilter()
    {
    }

    public T8DtDataFilter(T8DefinitionManager context)
    {
    }

    public T8DtDataFilter(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFilter.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof T8DataFilter)
            {
                return new T8DataFilterSerializer().serializeDataFilter((T8DataFilter)object);
            }
            else throw new IllegalArgumentException("Type expected: " + getDataTypeClassName() + " Type found: " + object.getClass().getCanonicalName());
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            return new T8DataFilterSerializer().deserializeDataFilter(jsonValue);
        }
        else return null;
    }
}