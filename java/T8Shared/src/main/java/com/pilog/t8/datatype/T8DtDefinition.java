package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;

/**
 * @author Bouwer du Preez
 */
public class T8DtDefinition extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DEFINITION";

    public T8DtDefinition()
    {
    }

    public T8DtDefinition(T8DefinitionManager context)
    {
    }

    public T8DtDefinition(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8Definition.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        throw new RuntimeException(IDENTIFIER + " does not allow serialization to JSON.");
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        throw new RuntimeException(IDENTIFIER + " does not allow de-serialization from JSON.");
    }
}
