package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.flow.T8FlowNode.FlowNodeStatus;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey.WaitKeyIdentifier;
import com.pilog.t8.flow.state.data.T8NodeStateDataSet;
import com.pilog.t8.flow.state.data.T8TaskStateDataSet;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition.FlowNodeType;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8FlowNodeState implements Serializable
{
    private final T8FlowState parentFlowState;
    private String projectId;
    private String flowIid;
    private String nodeIid;
    private String nodeId;
    private FlowNodeType nodeType;
    private FlowNodeStatus nodeStatus;
    private List<String> completedExecutionSteps;
    private T8FlowNodeStateWaitKeyList waitKeys;
    private Map<String, T8FlowStateNodeInputParameterMap> inputParameters;
    private T8FlowStateNodeParameterMap outputParameters;
    private T8FlowStateNodeParameterMap executionParameters;
    private T8FlowStateNodeOutputNodeMap outputNodeMap;
    private T8FlowTaskState taskState;
    private T8Timestamp timeActivated;
    private String information;
    private boolean updated;
    private boolean inserted;

    public T8FlowNodeState(T8FlowState parentFlowState, String nodeIid, String nodeId)
    {
        this.parentFlowState = parentFlowState;
        this.projectId = parentFlowState.getProjectId();
        this.flowIid = parentFlowState.getFlowIid();
        this.nodeIid = nodeIid;
        this.nodeId = nodeId;
        this.completedExecutionSteps = new ArrayList<>();
        this.waitKeys = new T8FlowNodeStateWaitKeyList(this);
        this.inputParameters = new LinkedHashMap<>();
        this.outputParameters = new T8FlowStateNodeParameterMap(this, null, null, STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER, null);
        this.executionParameters = new T8FlowStateNodeParameterMap(this, null, null, STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER, null);
        this.outputNodeMap = new T8FlowStateNodeOutputNodeMap(this, null, STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER, null);
        this.updated = false;
        this.inserted = true;
    }

    public T8FlowNodeState(T8FlowState parentFlowState, String nodeIid, String nodeId, FlowNodeType nodeType, FlowNodeStatus nodeStatus)
    {
        this(parentFlowState, nodeIid,nodeId);
        this.nodeType = nodeType;
        this.nodeStatus = nodeStatus;
    }

    public T8FlowNodeState(T8FlowState parentFlowState, T8NodeStateDataSet dataSet)
    {
        T8DataEntity nodeEntity;

        // Construct the flow state.
        nodeEntity = dataSet.getNodeEntity();
        this.parentFlowState = parentFlowState;
        this.projectId = (String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_PROJECT_ID);
        this.flowIid = (String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_FLOW_IID);
        this.nodeIid = (String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_NODE_IID);
        this.nodeId = (String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_NODE_ID);
        this.nodeType = FlowNodeType.valueOf((String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_TYPE));
        this.nodeStatus = FlowNodeStatus.valueOf((String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_STATUS));
        this.timeActivated = (T8Timestamp)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_TIME_ACTIVATED);
        this.information = (String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_INFORMATION);
        this.completedExecutionSteps = StringUtilities.buildCSVList((String)nodeEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER + F_COMPLETED_EXECUTION_STEPS));
        this.updated = false;
        this.inserted = false;

        // Construct the input parameters.
        this.inputParameters = new LinkedHashMap<>();
        for (String inputNodeID : dataSet.getInputNodeIDList())
        {
            inputParameters.put(inputNodeID, new T8FlowStateNodeInputParameterMap(this, null, inputNodeID, null, STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER, dataSet.getInputParameterDataSet(inputNodeID)));
        }

        // Construct the output parameters.
        this.outputParameters = new T8FlowStateNodeParameterMap(this, null, null, STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER, dataSet.getOutputParameterDataSet());

        // Construct the execution parameters.
        this.executionParameters = new T8FlowStateNodeParameterMap(this, null, null, STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER, dataSet.getExecutionParameterDataSet());

        // Construct the output node map.
        this.outputNodeMap = new T8FlowStateNodeOutputNodeMap(this, null, STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER, dataSet.getOutputNodeDataSet());

        // Construct the task state.
        addTaskState(dataSet);

        // Construct the node wait key.
        this.waitKeys = new T8FlowNodeStateWaitKeyList(this);
        for (T8DataEntity waitKeyEntity : dataSet.getWaitKeyEntities())
        {
            waitKeys.add(new T8FlowNodeStateWaitKey(waitKeyEntity, this));
        }
    }

    protected final void addTaskState(T8NodeStateDataSet dataSet)
    {
        T8TaskStateDataSet taskStateDataSet;

        taskStateDataSet = dataSet.getTaskStateDataSet();
        if (taskStateDataSet != null)
        {
            this.taskState = new T8FlowTaskState(this, taskStateDataSet);
        }
    }

    public T8FlowState getFlowState()
    {
        return parentFlowState;
    }

    public T8DataEntity createEntity(T8DataEntityDefinition entityDefinition)
    {
        String entityIdentifier;
        T8DataEntity newEntity;

        entityIdentifier = entityDefinition.getIdentifier();
        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityIdentifier + F_FLOW_IID, flowIid);
        newEntity.setFieldValue(entityIdentifier + F_NODE_IID, nodeIid);
        newEntity.setFieldValue(entityIdentifier + F_NODE_ID, nodeId);
        newEntity.setFieldValue(entityIdentifier + F_TYPE, nodeType.toString());
        newEntity.setFieldValue(entityIdentifier + F_STATUS, nodeStatus.toString());
        newEntity.setFieldValue(entityIdentifier + F_COMPLETED_EXECUTION_STEPS, StringUtilities.buildCSVString(completedExecutionSteps).toString());
        newEntity.setFieldValue(entityIdentifier + F_TIME_ACTIVATED, timeActivated);
        newEntity.setFieldValue(entityIdentifier + F_INFORMATION, information);
        return newEntity;
    }

    public void statePersisted()
    {
        inserted = false;
        updated = false;

        // Reset the input parameter maps' flags.
        for (T8FlowStateParameterMap inputParameterMap : inputParameters.values())
        {
            inputParameterMap.statePersisted();
        }

        // Reset the output parameter flags.
        outputParameters.statePersisted();

        // Reset the execution parameter flags.
        executionParameters.statePersisted();

        // Reset the output node map flags.
        outputNodeMap.statePersisted();

        // Reset the task state's flags.
        if (taskState != null) taskState.statePersisted();

        // Reset wait key state flags.
        waitKeys.statePersisted();
    }

    public boolean isUpdated()
    {
        return updated;
    }

    public void addUpdates(T8FlowStateUpdates updates)
    {
        // Add this node states updates.
        if (inserted) updates.addInsert(createEntity(updates.getNodeStateEntityDefinition()));
        else if (updated) updates.addUpdate(createEntity(updates.getNodeStateEntityDefinition()));

        // Add updates of all input parameter sets.
        for (T8FlowStateParameterMap inputParameterMap : inputParameters.values())
        {
            inputParameterMap.addUpdates(updates.getNodeStateInputParameterEntityDefinition(), updates);
        }

        // Add output parameter updates.
        outputParameters.addUpdates(updates.getNodeStateOutputParameterEntityDefinition(), updates);

        // Add execution parameter updates.
        executionParameters.addUpdates(updates.getNodeStateExecutionParameterEntityDefinition(), updates);

        // Add execution parameter updates.
        outputNodeMap.addUpdates(updates.getNodeStateOutputNodeEntityDefinition(), updates);

        // If a task state is attached to this node, add its updates.
        if (taskState != null) taskState.addUpdates(updates);

        // Add wait key updates.
        waitKeys.addUpdates(updates);
    }

    public String getProjectId()
    {
        return parentFlowState.getProjectId();
    }

    public String getFlowIid()
    {
        return parentFlowState.getFlowIid();
    }

    public String getFlowId()
    {
        return parentFlowState.getFlowId();
    }

    public String getNodeIid()
    {
        return nodeIid;
    }

    public void setNodeIid(String nodeIid)
    {
        if (!Objects.equals(this.nodeIid, nodeIid))
        {
            updated = true;
            this.nodeIid = nodeIid;
        }
    }

    public String getNodeId()
    {
        return nodeId;
    }

    public void setNodeId(String nodeId)
    {
        if (!Objects.equals(this.nodeId, nodeId))
        {
            updated = true;
            this.nodeId = nodeId;
        }
    }

    public FlowNodeType getNodeType()
    {
        return nodeType;
    }

    public void setNodeType(FlowNodeType nodeType)
    {
        if (!Objects.equals(this.nodeType, nodeType))
        {
            updated = true;
            this.nodeType = nodeType;
        }
    }

    public FlowNodeStatus getNodeStatus()
    {
        return nodeStatus;
    }

    public void setNodeStatus(FlowNodeStatus nodeStatus)
    {
        if (!Objects.equals(this.nodeStatus, nodeStatus))
        {
            updated = true;
            this.nodeStatus = nodeStatus;
        }
    }

    public List<String> getCompletedExecutionSteps()
    {
        return new ArrayList<String>(completedExecutionSteps);
    }

    public void setCompletedExecutionSteps(List<String> completedExecutionSteps)
    {
        if (!Objects.equals(this.completedExecutionSteps, completedExecutionSteps))
        {
            updated = true;
            this.completedExecutionSteps.clear();
            if (completedExecutionSteps != null)
            {
                this.completedExecutionSteps.addAll(completedExecutionSteps);
            }
        }
    }

    public T8Timestamp getTimeActivated()
    {
        return timeActivated;
    }

    public Long getTimeActivatedMilliseconds()
    {
        return timeActivated != null ? timeActivated.getMilliseconds() : null;
    }

    public void setTimeActivated(T8Timestamp timeActivated)
    {
        if (!Objects.equals(this.timeActivated, timeActivated))
        {
            updated = true;
            this.timeActivated = timeActivated;
        }
    }

    public String getInformation()
    {
        return information;
    }

    public void setInformation(String information)
    {
        if (!Objects.equals(this.information, information))
        {
            updated = true;
            this.information = information;
        }
    }

    public Map<String, T8FlowStateParameterMap> getInputParameters()
    {
        return new LinkedHashMap<String, T8FlowStateParameterMap>(inputParameters);
    }

    public void addInputParameters(T8FlowStateNodeInputParameterMap inputParameters)
    {
        this.inputParameters.put(inputParameters.getInputNodeIdentifier(), inputParameters);
    }

    public T8FlowStateParameterMap getOutputParameters()
    {
        return outputParameters;
    }

    public void addOutputParameters(Map<String, Object> outputParameters)
    {
        if (outputParameters != null)
        {
            this.outputParameters.putAll(outputParameters);
        }
    }

    public T8FlowStateParameterMap getExecutionParameters()
    {
        return executionParameters;
    }

    public void addExecutionParameters(Map<String, Object> executionParameters)
    {
        if (executionParameters != null)
        {
            this.executionParameters.putAll(executionParameters);
        }
    }

    public T8FlowStateNodeOutputNodeMap getOutputNodeMap()
    {
        return outputNodeMap;
    }

    public void addOutputNodeMappings(Map<String, String> outputNodes)
    {
        if (outputNodes != null)
        {
            this.outputNodeMap.putAll(outputNodes);
        }
    }

    public T8FlowTaskState getTaskState()
    {
        return taskState;
    }

    public void setTaskState(T8FlowTaskState taskState)
    {
        this.taskState = taskState;
    }

    public void clearWaitKeys()
    {
        waitKeys.clear();
    }

    public void addNodeCompletionWaitKey(String nodeIdentifier)
    {
        T8FlowNodeStateWaitKey key;

        key = new T8FlowNodeStateWaitKey(this);
        key.setKeyIdentifier(WaitKeyIdentifier.NODE_COMPLETION);
        key.setKeyFlowInstanceIdentifier(getFlowIid());
        key.setKeyFlowIdentifier(null);
        key.setKeyNodeInstanceIdentifier(null);
        key.setKeyNodeIdentifier(nodeIdentifier);
        key.setKeyTaskInstanceIdentifier(null);
        key.setKeyTaskIdentifier(null);
        key.setKeyDataObjectInstanceIdentifier (null);
        key.setKeyDataObjectIdentifier(null);
        key.setKeySignalIdentifier(null);
        key.setKeySignalParameter(null);
        waitKeys.add(key);
    }

    public void addTaskCompletionWaitKey(String taskIdentifier, String taskInstanceIdentifier)
    {
        T8FlowNodeStateWaitKey key;

        key = new T8FlowNodeStateWaitKey(this);
        key.setKeyIdentifier(WaitKeyIdentifier.TASK_COMPLETION);
        key.setKeyFlowInstanceIdentifier(null);
        key.setKeyFlowIdentifier(null);
        key.setKeyNodeInstanceIdentifier(null);
        key.setKeyNodeIdentifier(null);
        key.setKeyTaskInstanceIdentifier(taskInstanceIdentifier);
        key.setKeyTaskIdentifier(taskIdentifier);
        key.setKeyDataObjectInstanceIdentifier (null);
        key.setKeyDataObjectIdentifier(null);
        key.setKeySignalIdentifier(null);
        key.setKeySignalParameter(null);
        waitKeys.add(key);
    }

    public void addTaskClaimWaitKey(String taskIdentifier, String taskInstanceIdentifier)
    {
        T8FlowNodeStateWaitKey key;

        key = new T8FlowNodeStateWaitKey(this);
        key.setKeyIdentifier(WaitKeyIdentifier.TASK_CLAIM);
        key.setKeyFlowInstanceIdentifier(null);
        key.setKeyFlowIdentifier(null);
        key.setKeyNodeInstanceIdentifier(null);
        key.setKeyNodeIdentifier(null);
        key.setKeyTaskInstanceIdentifier(taskInstanceIdentifier);
        key.setKeyTaskIdentifier(taskIdentifier);
        key.setKeyDataObjectInstanceIdentifier (null);
        key.setKeyDataObjectIdentifier(null);
        key.setKeySignalIdentifier(null);
        key.setKeySignalParameter(null);
        waitKeys.add(key);
    }

    public void addSignalCatchWaitKey(String signalId, String signalParameter)
    {
        T8FlowNodeStateWaitKey key;

        key = new T8FlowNodeStateWaitKey(this);
        key.setKeyIdentifier(WaitKeyIdentifier.SIGNAL);
        key.setKeyFlowInstanceIdentifier(null);
        key.setKeyFlowIdentifier(null);
        key.setKeyNodeInstanceIdentifier(null);
        key.setKeyNodeIdentifier(null);
        key.setKeyTaskInstanceIdentifier(null);
        key.setKeyTaskIdentifier(null);
        key.setKeyDataObjectInstanceIdentifier (null);
        key.setKeyDataObjectIdentifier(null);
        key.setKeySignalIdentifier(signalId);
        key.setKeySignalParameter(signalParameter);
        waitKeys.add(key);
    }

    public int getTotalInputParameterCount()
    {
        int count;

        count = 0;
        for (T8FlowStateNodeInputParameterMap inputParameterMap : inputParameters.values())
        {
            count += inputParameterMap.getTotalParameterCount();
        }

        return count;
    }

    public int getTotalExecutionParameterCount()
    {
        return executionParameters.getTotalParameterCount();
    }

    public int getTotalOutputParameterCount()
    {
        return outputParameters.getTotalParameterCount();
    }

    public int getOutputNodeMappingCount()
    {
        return outputNodeMap.getTotalParameterCount();
    }

    public int getWaitKeyCount()
    {
        return waitKeys.size();
    }

    @Override
    public String toString()
    {
        return "T8FlowNodeState{" + "flowInstanceIdentifier=" + flowIid + ", nodeInstanceIdentifier=" + nodeIid + ", nodeIdentifier=" + nodeId + '}';
    }
}
