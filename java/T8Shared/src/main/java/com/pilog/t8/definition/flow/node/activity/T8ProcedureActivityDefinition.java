package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.procedure.T8ProcedureDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ProcedureActivityDefinition extends T8FlowActivityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_NODE_ACTIVITY_PROCEDURE";
    public static final String DISPLAY_NAME = "Procedure Activity";
    public static final String DESCRIPTION = "An activity that executes a procedure.";
    private enum Datum {PROCEDURE_ID,
                        PROCEDURE_INPUT_PARAMETER_MAPPING,
                        PROCEDURE_OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ProcedureActivityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROCEDURE_ID.toString(), "Procedure", "The identifier of the Procedure to execute."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.PROCEDURE_INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow to Procedure", "A mapping of Flow Parameters to the corresponding Procedure Input Parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.PROCEDURE_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Procedure to Flow", "A mapping of Procedure Output Parameters to the corresponding Flow Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PROCEDURE_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ProcedureDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROCEDURE_INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String procedureId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            procedureId = getProcedureId();
            if (procedureId != null)
            {
                T8ProcedureDefinition procedureDefinition;
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
                procedureDefinition = (T8ProcedureDefinition)definitionContext.getRawDefinition(getRootProjectId(), procedureId);
                if (procedureDefinition != null)
                {
                    List<T8DataParameterDefinition> operationInputParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    operationInputParameterDefinitions = procedureDefinition.getInputParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((operationInputParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition procedureInputParameterDefinition : operationInputParameterDefinitions)
                            {
                                identifierList.add(procedureInputParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(flowParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.PROCEDURE_OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String procedureId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            procedureId = getProcedureId();
            if (procedureId != null)
            {
                T8ProcedureDefinition procedureDefinition;
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)getParentDefinition();
                procedureDefinition = (T8ProcedureDefinition)definitionContext.getRawDefinition(getRootProjectId(), procedureId);
                if (procedureDefinition != null)
                {
                    List<T8DataParameterDefinition> operationOutputParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    operationOutputParameterDefinitions = procedureDefinition.getOutputParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((operationOutputParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition procedureOutputParameterDefinition : operationOutputParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(procedureOutputParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String nodeIid) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.activity.T8ProcedureActivityNode").getConstructor(T8Context.class, T8Flow.class, T8ProcedureActivityDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, nodeIid);
    }

    public String getProcedureId()
    {
        return (String)getDefinitionDatum(Datum.PROCEDURE_ID.toString());
    }

    public void setProcedureId(String id)
    {
        setDefinitionDatum(Datum.PROCEDURE_ID.toString(), id);
    }

    public Map<String, String> getProcedureInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.PROCEDURE_INPUT_PARAMETER_MAPPING.toString());
    }

    public void setProcedureInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.PROCEDURE_INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getProcedureOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.PROCEDURE_OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setProcedureOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.PROCEDURE_OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
