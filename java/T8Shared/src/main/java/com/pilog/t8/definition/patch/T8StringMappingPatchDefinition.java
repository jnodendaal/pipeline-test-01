package com.pilog.t8.definition.patch;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8StringMappingPatchDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PATCH_DEFINITION_STRING_MAP";
    public static final String TYPE_IDENTIFIER = "@DT_PATCH_DEFINITION_STRING_MAP";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "PATCH_DEF_STRING_MAP_";
    public static final String DISPLAY_NAME = "T8 Definition String Map Patch";
    public static final String DESCRIPTION = "A definition containing mapping of existing strings to their respective replacements.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    public enum Datum {STRING_MAP};
    // -------- Definition Meta-Data -------- //

    public T8StringMappingPatchDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.STRING), Datum.STRING_MAP.toString(), "String Map",  "A map of existing strings and their respective replacements."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public Map<String, String> getStringMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.STRING_MAP);
    }

    public void setStringMap(Map<String, String> stringMap)
    {
        setDefinitionDatum(Datum.STRING_MAP, stringMap);
    }
}

