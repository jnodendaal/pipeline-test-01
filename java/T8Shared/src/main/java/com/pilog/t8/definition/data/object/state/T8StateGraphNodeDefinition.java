package com.pilog.t8.definition.data.object.state;

import com.pilog.t8.definition.graph.T8GraphNodeDefinition;

/**
 * @author Bouwer du Preez
 */
public abstract class T8StateGraphNodeDefinition extends T8GraphNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT_STATE_GRAPH_NODE";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Object State Graph Node";
    public static final String DESCRIPTION = "A node in a data object state graph.";
    public static final String IDENTIFIER_PREFIX = "O_STATE_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //
    
    public T8StateGraphNodeDefinition(String identifier)
    {
        super(identifier);
    }
}
