package com.pilog.t8.system.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8ServerEventListener extends EventListener
{
    public void operationCompleted(T8OperationCompletedEvent event);
}
