package com.pilog.t8.definition.procedure;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.procedure.T8Procedure;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ProcedureDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PROCEDURE";
    public static final String GROUP_NAME = "Procedures";
    public static final String GROUP_DESCRIPTION = "Reusable scripts that implement standard logic to be shared by multiple server-side operations.";
    public static final String STORAGE_PATH = "/procedures";
    public static final String DISPLAY_NAME = "Procedure";
    public static final String DESCRIPTION = "A T8 server-side procedure that can be executed to perform a specific reusable task.";
    public static final String IDENTIFIER_PREFIX = "PROC_";
    public enum Datum
    {
        INPUT_PARAMETER_DEFINITIONS,
        OUTPUT_PARAMETER_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8ProcedureDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this procedure."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters", "The output data parameters returned by this procedure after execution."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8ProcedureTestHarness", new Class<?>[]{});
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionDocumentationProvider createDocumentationProvider(T8DefinitionContext definitionContext)
    {
        return new T8ProcedureDocumentationProvider(definitionContext);
    }

    public abstract T8Procedure getNewProcedureInstance(T8Context context);

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public void addInputParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public void addOutputParameterDefinition(T8DataParameterDefinition parameterDefinition)
    {
        addSubDefinition(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }
}
