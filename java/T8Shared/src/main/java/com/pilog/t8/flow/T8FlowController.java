package com.pilog.t8.flow;

import com.pilog.t8.flow.event.T8FlowEvent;
import com.pilog.t8.flow.event.T8FlowEventListener;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.T8TaskEscalationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowController
{
    // Releases all acquired resources.
    public void destroy();
    // Fires an event that will be dispatched to all current listeners.
    public void processFlowEvent(T8FlowEvent flowEvent);
    // Adds a flow listener to the controller.
    public void addFlowEventListener(T8Context context, T8FlowEventListener listener);
    // Removes a flow listener from the controller.
    public void removeFlowEventListener(T8Context context, T8FlowEventListener listener);

    // Fires a BPMN Signal event across the entire flow context.
    public void fireSignalEvent(T8Context context, String signalId, Map<String, Object> signalParameters);
    // Fires a BPMN Message event across the entire flow context.
    public void fireMessageEvent(T8Context context, String messageId, Map<String, Object> messageParameters);

    // Admin Only:  Attempts to retry the execution of a flow node that is currently in a failure state.
    public void retryExecution(T8Context context, String flowIid, String nodeIid);
    // Admin Only: Returns the active input identifiers for any specified node state within an executing flow state.
    public List<String> findActiveInputNodeInstanceIdentifiers(T8Context context, String flowIid, String nodeIid) throws Exception;
    // Returns the total number of flows currently active on the server.
    public int getFlowCount(T8Context context);
    // Returns the total number of flows currently cached on the server.
    public int getCachedFlowCount(T8Context context);
    // Clears all non-executing flows from the server cache.
    public int clearFlowCache(T8Context context);
    // Returns the number of the task types that are available within the specified session context, using the specified task filter.
    public int getTaskCount(T8Context context, T8TaskFilter taskFilter) throws Exception;
    // Returns a summary of the task types and counts that are available within the specified session context, using the specified task filter.
    public T8TaskListSummary getTaskListSummary(T8Context context, T8TaskFilter taskFilter) throws Exception;
    // Returns the details of the specified task.
    public T8TaskDetails getTaskDetails(T8Context context, String taskIid) throws Exception;
    // Returns the UI data of the specified task.
    public Map<String, Object> getTaskUiData(T8Context context, String taskIid) throws Exception;
    // Returns a list of all flow tasks that are available within the specified session context.
    public T8TaskList getTaskList(T8Context context, T8TaskFilter taskFilter) throws Exception;
    // Sets the priority of the task, the task priority will be used to sort tasks according to priority.
    public void setTaskPriority(T8Context context, String taskIid, int priority) throws Exception;
    // Decreases the task priority is by the specified amount, the minimum priority for a task is 0, and cannot be set to any lower.
    public void decreaseTaskPriority(T8Context context, String taskIid, int amount) throws Exception;
    // Increases the task priority is by the specified amount.
    public void increaseTaskPriority(T8Context context, String taskIid, int amount) throws Exception;
    // Reassigns the specified task to a new user and workflow profile.
    public void reassignTask(T8Context context, String taskIid, String userIdentifier, String profileId) throws Exception;
    // Issues a task that will be added to the global task list and made available for completion by external resources.
    public void issueTask(T8Context context, T8FlowTaskState taskState) throws Exception;
    // Revokes a task that was previously issued so that it is no longer available on the global task list.
    public void revokeTask(T8Context context, T8FlowTaskState taskState) throws Exception;
    // Cancels the specified task, meaning that it cannot be completed anymore even if it is already in progress.
    public void cancelTask(T8Context context, T8FlowTaskState taskState) throws Exception;
    // Claims the specified task from the list of available tasks for the specified session.  Once claimed a task cannot be executed in a different session.
    public void claimTask(T8Context context, String taskIid) throws Exception;
    // Unclaims a task so that it is returned to the list of available tasks.
    public void unclaimTask(T8Context context, String taskIid) throws Exception;
    // Completes the specified flow task, allowing to the flow engine to continue processing of subsequent nodes.
    public T8TaskCompletionResult completeTask(T8Context context, String taskIid, Map<String, Object> outputParameters) throws Exception;
    // Returns the current state of the specified executing flow.
    public T8FlowState getFlowState(T8Context context, String flowIid) throws Exception;
    // Returns the status of a currently executing flow.
    public T8FlowStatus getFlowStatus(T8Context context, String flowIid, boolean includeDisplayData) throws Exception;
    // Loads the specified flow definition, creates a new instance of the flow from the definition and queues its execution.
    public T8FlowStatus queueFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception;
    // Loads the specified flow definition, creates a new instance of the flow from the definition and starts its execution at the first node.
    public T8FlowStatus startFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception;
    // Kills the specified flow and all related flows, removing them from the execution stack and discarding their states.
    public void killFlow(T8Context context, String flowIid) throws Exception;

    // Returns the requested flow definition.
    public T8WorkFlowDefinition getFlowDefinition(String projectId, String flowId);
    // Returns the requested task definition.
    public T8FlowTaskDefinition getTaskDefinition(String projectId, String taskId);
    // Returns the requested signal definition.
    public T8FlowSignalDefinition getSignalDefinition(String projectId, String signalId);
    // Returns the requested escalation definition.
    public T8TaskEscalationDefinition getTaskEscalationDefinition(String projectId, String escalationId);

    // Refreshes the requested task view using the current state of the specified task.
    public void refreshTaskList(String taskIid) throws Exception;
    // Refreshes the the task list for all tasks linked to the specified flow instance.
    public void refreshFlowTaskList(String flowIid) throws Exception;

    // Reloads the task escalation trigger cache.
    public void recacheTaskEscalationTriggers(T8Context context) throws Exception;
}
