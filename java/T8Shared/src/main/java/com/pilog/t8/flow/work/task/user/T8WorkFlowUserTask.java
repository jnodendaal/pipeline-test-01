package com.pilog.t8.flow.work.task.user;

import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8FlowTaskStatus;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowUserTask implements T8FlowTask
{
    private T8WorkFlowUserTaskDefinition definition;
    private T8TaskDetails taskDetails;
    private TaskStatus status;
    private String statusMessage;
    private T8FlowTask uiTask;

    public T8WorkFlowUserTask(T8WorkFlowUserTaskDefinition definition, T8TaskDetails taskDetails)
    {
        this.definition = definition;
        this.taskDetails = taskDetails;
        this.status = TaskStatus.NOT_STARTED;
    }

    @Override
    public T8FlowTaskDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8TaskDetails getTaskDetails()
    {
        return taskDetails;
    }

    @Override
    public void stopTask()
    {
        if (uiTask != null)
        {
            uiTask.stopTask();
        }
    }

    @Override
    public void cancelTask()
    {
        if (uiTask != null)
        {
            uiTask.cancelTask();
        }
    }

    @Override
    public T8FlowTaskStatus getStatus()
    {
        if (uiTask != null)
        {
            return uiTask.getStatus();
        }
        else return new T8FlowTaskStatus(taskDetails.getTaskId(), taskDetails.getTaskIid(), status, -1, statusMessage);
    }

    @Override
    public Map<String, Object> doTask(T8ComponentController controller, Map<String, Object> inputParameters) throws Exception
    {
        uiTask = getFlowTask();
        if (uiTask != null)
        {
            return uiTask.doTask(controller, inputParameters);
        }
        else throw new Exception("Could not resolve UI for User Task: " + definition);
    }

    @Override
    public Map<String, Object> doTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception
    {
        throw new UnsupportedOperationException("Script Task cannot be executed from within a client context.");
    }

    @Override
    public Map<String, Object> resumeTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception
    {
        throw new UnsupportedOperationException("Script Task cannot be executed from within a client context.");
    }

    private T8FlowTask getFlowTask()
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.flow.work.task.user.T8WorkFlowModuleTask").getConstructor(definition.getClass(), T8TaskDetails.class);
            return (T8FlowTask)constructor.newInstance(definition, taskDetails);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
