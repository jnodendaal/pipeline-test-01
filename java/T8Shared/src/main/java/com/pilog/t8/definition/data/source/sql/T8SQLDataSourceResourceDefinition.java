/**
 * Created on 22 Apr 2015, 7:38:23 AM
 */
package com.pilog.t8.definition.data.source.sql;

/**
 * @author Gavin Boshoff
 */
public class T8SQLDataSourceResourceDefinition extends T8SQLDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_SQL_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8SQLDataSourceResourceDefinition(String identifier)
    {
        super(identifier);
    }
}