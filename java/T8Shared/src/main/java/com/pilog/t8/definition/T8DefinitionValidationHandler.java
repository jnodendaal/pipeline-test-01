package com.pilog.t8.definition;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.validation.T8DefinitionValidationError.ErrorType;
import com.pilog.t8.log.T8Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionValidationHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefinitionValidationHandler.class);

    private T8Definition definition;
    private List<T8Definition> localDefinitions;
    private final T8DefinitionManager definitionManager;
    private final List<T8DefinitionValidationError> validationErrors;
    private final List<String> checkedDefinitions;

    public T8DefinitionValidationHandler(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.definitionManager = serverContext.getDefinitionManager();
        this.checkedDefinitions = new ArrayList<>();
        this.validationErrors = new ArrayList<>();
    }

    public List<T8DefinitionValidationError> validateDefinition(T8Definition definition, T8Definition.DefinitionValidationType validationType)
    {
        this.localDefinitions = definition.getRootDefinition().getDescendentDefinitions();
        this.localDefinitions.add(definition);

        return validateLocalDefinition(definition, validationType);
    }

    private List<T8DefinitionValidationError> validateLocalDefinition(T8Definition definition, T8Definition.DefinitionValidationType validationType)
    {
        String projectId;

        this.definition = definition;
        projectId = definition.getRootProjectId();
        for (T8DefinitionDatumType datumType : definition.getDatumTypes())
        {
            T8DataType dataType;
            Object datumValue;

            dataType = datumType.getDataType();
            datumValue = definition.getDefinitionDatum(datumType.getIdentifier());
            if (T8IdentifierUtilities.isDefinitionIdentifierType(dataType))
            {
                String identifier;

                identifier = (String)datumValue;
                validateDefinitionIdentifier(validationType, projectId, datumType.getIdentifier(), identifier);
            }
            else if (dataType.equals(T8DataType.DEFINITION) && datumValue != null)
            {
                String identifier;

                identifier = ((T8Definition)datumValue).getIdentifier();
                validateDefinitionIdentifier(validationType, projectId, datumType.getIdentifier(), identifier);

                validateLocalDefinition(((T8Definition)datumValue), validationType);
            }
            else if (dataType.isType(T8DataType.LIST))
            {
                if (T8IdentifierUtilities.isDefinitionIdentifierType(((T8DtList)dataType).getElementDataType()))
                {
                    List<String> identifierList;

                    identifierList = (List<String>)datumValue;
                    if (identifierList != null)
                    {
                        for (String identifier : identifierList)
                        {
                            if(checkIdentifierValid(identifier)) validateDefinitionIdentifier(validationType, projectId, datumType.getIdentifier(), identifier);
                        }
                    }
                }
                else if (((T8DtList)dataType).getElementDataType().equals(T8DataType.DEFINITION))
                {
                    List<T8Definition> definitionList;

                    definitionList = (List<T8Definition>)datumValue;
                    if (definitionList != null)
                    {
                        for (T8Definition subDefinition : definitionList)
                        {
                            validateLocalDefinition(subDefinition, validationType);
                        }
                    }
                }
            }
            else if (dataType.isType(T8DataType.MAP))
            {
                T8DataType keyType;
                T8DataType valueType;
                T8DtMap mapType;

                mapType = (T8DtMap)dataType;
                keyType = mapType.getKeyDataType();
                valueType = mapType.getValueDataType();
                if (T8IdentifierUtilities.isDefinitionIdentifierType(keyType) || T8IdentifierUtilities.isDefinitionIdentifierType(valueType))
                {
                    Map<String, String> identifierMap;

                    identifierMap = (Map<String, String>)datumValue;
                    if (identifierMap != null)
                    {
                        for (String keyIdentifier : identifierMap.keySet())
                        {
                            if(T8IdentifierUtilities.isDefinitionIdentifierType(keyType) && checkIdentifierValid(keyIdentifier))
                            {
                                validateDefinitionIdentifier(validationType, projectId, datumType.getIdentifier(), keyIdentifier);
                            }

                            if(T8IdentifierUtilities.isDefinitionIdentifierType(valueType) && checkIdentifierValid(identifierMap.get(keyIdentifier)))
                            {
                                validateDefinitionIdentifier(validationType, projectId, datumType.getIdentifier(), identifierMap.get(keyIdentifier));
                            }
                        }
                    }
                }
            }
            else if(checkEpicType(dataType) && datumValue != null)
            {
                String script;
                Matcher matcher;

                script = (String) datumValue;
                matcher = Pattern.compile("(@[A-Z0-9_$]*)|(\\$[A-Z0-9_]*)").matcher(script);

                while(matcher.find())
                {
                    String identifier;

                    identifier = matcher.group();
                    //Make sure to check the definitions only once, and exclude web service and api definitions
                    if(!checkedDefinitions.contains(identifier) && checkIdentifierValid(identifier))
                    {
                        checkedDefinitions.add(identifier);
                        validateDefinitionIdentifier(validationType, projectId, datumType.getIdentifier(), identifier);
                    }
                }
            }
        }

        return validationErrors;
    }

    private void validateDefinitionIdentifier(T8Definition.DefinitionValidationType validationType, String projectId, String datumIdentifier, String identifier)
    {
        if (identifier != null)
        {
            String globalIdentifierPart;
            String localIdentifierPart;

            globalIdentifierPart = T8IdentifierUtilities.getGlobalIdentifierPart(identifier);
            localIdentifierPart = T8IdentifierUtilities.getLocalIdentifierPart(identifier);

            if (globalIdentifierPart != null && validationType == T8Definition.DefinitionValidationType.GLOBAL)
            {
                try
                {
                    T8Definition globalDefinition;

                    globalDefinition = definitionManager.getRawDefinition(null, projectId, globalIdentifierPart);
                    if (globalDefinition == null)
                    {
                        // ERROR:  The datum references a global definition that does not exist.
                        validationErrors.add(new T8DefinitionValidationError(definition.getRootDefinition().getProjectIdentifier(), definition.getPublicIdentifier(), datumIdentifier, "Referenced global definition not found: " + identifier, ErrorType.CRITICAL));
                    }
                    else if (localIdentifierPart != null)
                    {
                        T8Definition localDefinition;

                        localDefinition = T8DefinitionUtilities.getDefinition(globalDefinition.getDescendentDefinitions(), localIdentifierPart);
                        if (localDefinition == null)
                        {
                            // ERROR:  The datum references a local definition that does not exist.
                            validationErrors.add(new T8DefinitionValidationError(definition.getRootDefinition().getProjectIdentifier(), definition.getPublicIdentifier(), datumIdentifier, "Referenced definition not found: " + identifier, ErrorType.CRITICAL));
                        }
                    }
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while retrieving definition: " + globalIdentifierPart, e);

                    // ERROR:  The datum references a local definition that does not exist.
                    validationErrors.add(new T8DefinitionValidationError(definition.getRootDefinition().getProjectIdentifier(), definition.getPublicIdentifier(), datumIdentifier, "Referenced definition could not be retrieved: " + identifier, ErrorType.CRITICAL));
                }
            }
            else
            {
                T8Definition localDefinition;

                localDefinition = T8DefinitionUtilities.getDefinition(localDefinitions, identifier);
                if (localDefinition == null)
                {
                    // ERROR:  The datum references a local definition that does not exist.
                    validationErrors.add(new T8DefinitionValidationError(definition.getRootDefinition().getProjectIdentifier(), definition.getPublicIdentifier(), datumIdentifier, "Referenced definition not found: " + identifier, ErrorType.CRITICAL));
                }
            }
        }
    }

    private boolean checkIdentifierValid(String identifier)
    {
        if (identifier == null) return true;
        else if (identifier.startsWith("@API")) return false;
        else return !identifier.startsWith("@WS");
    }

    private boolean checkEpicType(T8DataType dataType)
    {
        if (dataType.isType(T8DataType.STRING))
        {
            return T8DataType.EPIC_EXPRESSION.getDataTypeStringRepresentation(true).equals(dataType.getDataTypeStringRepresentation(true)) ||
                   T8DataType.EPIC_SCRIPT.getDataTypeStringRepresentation(true).equals(dataType.getDataTypeStringRepresentation(true));
        } else return false;
    }
}
