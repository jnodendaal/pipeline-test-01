package com.pilog.t8.notification;

import com.pilog.t8.T8SessionContext;
import java.io.Serializable;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8Notification implements Serializable
{
    private final String projectId;
    private final String instanceIdentifier;

    private Map<String, ? extends Object> parameters;
    private String senderOrganizationIdentifier;
    private String senderInstanceIdentifier;
    private NotificationStatus status;
    private String senderIdentifier;
    private final String identifier;
    private String userIdentifier;
    private long sentTime;

    // Notification status.
    public enum NotificationStatus {SENT, RECEIVED};

    // Events used when logging notification history.
    public enum NotificationHistoryEvent {SENT, CLOSED};

    public T8Notification(String projectId, String notoficationId, String notificationIid)
    {
        this.projectId = projectId;
        this.sentTime = System.currentTimeMillis();
        this.identifier = notoficationId;
        this.instanceIdentifier = notificationIid;
    }

    /**
     * Returns the id of the project to which this notification belongs.
     * @return The id of the project to which this notification belongs.
     */
    public String getProjectId()
    {
        return projectId;
    }

    /**
     * Returns the globally unique instance identifier of the notification.
     * @return The globally unique instance identifier of the notification.
     */
    public String getInstanceIdentifier()
    {
        return instanceIdentifier;
    }

    /**
     * Returns the identifier of the notification type (definition).
     * @return The identifier of the notification type (definition).
     */
    public String getIdentifier()
    {
        return identifier;
    }

    /**
     * Returns the user identifier of the intended recipient of this
     * notification.
     * @return The user identifier of the intended recipient of this
     * notification.
     */
    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    /**
     * Sets the user identifier of the intended recipient of this notification.
     * @param userIdentifier The intended recipient of this notification.
     */
    public void setUserIdentifier(String userIdentifier)
    {
        this.userIdentifier = userIdentifier;
    }

    /**
     * Returns the time at which the notification was generated/sent.
     * @return The time at which the notification was generated/sent.
     */
    public long getSentTime()
    {
        return sentTime;
    }

    /**
     * Sets the time at which the notification was generated/sent.
     * @param creationTime The time at which the notification was
     * generated/sent.
     */
    public void setCreationTime(long creationTime)
    {
        this.sentTime = creationTime;
    }

    /**
     * Returns the status of this notification.
     * @return The current status of this notification.
     */
    public NotificationStatus getStatus()
    {
        return status;
    }

    /**
     * Sets the new status of this notification.
     * @param status The new status of this notification.
     */
    public void setStatus(NotificationStatus status)
    {
        this.status = status;
    }

    /**
     * Checks whether or not the current notification is considered new.
     * Notifications are considered new or old based on their status.
     *
     * @return {@code true} if the notification is considered new.
     *      {@code false} otherwise
     */
    public boolean isNew()
    {
        return NotificationStatus.SENT == this.status;
    }

    /**
     * Returns this notification's data parameters.
     * @return The data parameters of this notification.
     */
    public Map<String, ? extends Object> getParameters()
    {
        return parameters;
    }

    /**
     * Sets the data parameters of this notification.
     * @param parameters The new data parameters to set on this notification.
     */
    public void setParameters(Map<String, ? extends Object> parameters)
    {
        this.parameters = parameters;
    }

    /**
     * Sets the sender details based on the specified {@code T8SessionContext}.
     * The sender details is dependant on whether the specified session is a
     * system or user session.
     *
     * @param sessionContext The {@code T8SessionContext} defining the current
     *      session and its related details
     */
    public void setSenderDetail(T8SessionContext sessionContext)
    {
        this.senderOrganizationIdentifier = sessionContext.getOrganizationIdentifier();
        if (sessionContext.isSystemSession())
        {
            this.senderInstanceIdentifier = sessionContext.getSystemAgentInstanceIdentifier();
            this.senderIdentifier = sessionContext.getSystemAgentIdentifier();
        } else this.senderIdentifier = sessionContext.getUserIdentifier();
    }

    public String getSenderOrganizationIdentifier()
    {
        return this.senderOrganizationIdentifier;
    }

    public void setSenderOrganizationIdentifier(String senderOrganizationIdentifier)
    {
        this.senderOrganizationIdentifier = senderOrganizationIdentifier;
    }

    public String getSenderInstanceIdentifier()
    {
        return this.senderInstanceIdentifier;
    }

    public void setSenderInstanceIdentifier(String senderInstanceIdentifier)
    {
        this.senderInstanceIdentifier = senderInstanceIdentifier;
    }

    public String getSenderIdentifier()
    {
        return this.senderIdentifier;
    }

    public void setSenderIdentifier(String senderIdentifier)
    {
        this.senderIdentifier = senderIdentifier;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8Notification{");
        toStringBuilder.append("identifier=").append(this.identifier);
        toStringBuilder.append(",status=").append(this.status);
        toStringBuilder.append(",senderIdentifier=").append(this.senderIdentifier);
        toStringBuilder.append(",userIdentifier=").append(this.userIdentifier);
        toStringBuilder.append(",sentTime=").append(this.sentTime);
        toStringBuilder.append(",instanceIdentifier=").append(this.instanceIdentifier);
        toStringBuilder.append(",senderInstanceIdentifier=").append(this.senderInstanceIdentifier);
        toStringBuilder.append(",senderOrganizationIdentifier=").append(this.senderOrganizationIdentifier);
        toStringBuilder.append(",parameters=").append(this.parameters);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
