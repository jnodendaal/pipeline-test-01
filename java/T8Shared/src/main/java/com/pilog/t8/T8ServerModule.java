package com.pilog.t8;

/**
 * @author Bouwer du Preez
 */
public interface T8ServerModule
{
    /**
     * Initializes the server module to a functioning state that can be used by
     * other dependent modules.
     * @throws Exception 
     */
    public void init() throws Exception;
    
    /**
     * Starts services of the server module that are dependent on other modules.
     * All modules on the server will be initialized first.  After successful
     * initialization of all server modules, this method will be invoked on the
     * module to allow for a secondary sequence of start-up operations that are
     * dependent on an already initialized server state.
     * @throws Exception 
     */
    public void start() throws Exception;

    /**
     * Shuts down the server module, releasing all acquired resources and 
     * finalizing execution.
     */
    public void destroy();
}
