package com.pilog.t8.communication.event;

import com.pilog.t8.communication.T8CommunicationMessage;

/**
 * @author Bouwer du Preez
 */
public class T8MessageFailedEvent extends T8CommunicationEvent
{
    private final Exception failureException;

    public T8MessageFailedEvent(T8CommunicationMessage message, Exception failureException)
    {
        super(message);
        this.failureException = failureException;
    }

    public T8CommunicationMessage getMessage()
    {
        return (T8CommunicationMessage)source;
    }

    public Exception getException()
    {
        return failureException;
    }
}
