package com.pilog.t8.definition.data.source.java;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8JavaDataSourceDefinition extends T8DataSourceDefinition 
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_JAVA";
    public static final String DISPLAY_NAME = "Java Data Source";
    public static final String DESCRIPTION = "A server-side Java class that is used as a source for relational data.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {CLASS_NAME};
    // -------- Definition Meta-Data -------- //

    public T8JavaDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_NAME.toString(), "Class Name", "The class name to use when instantiating a data source of this type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction accessProvider)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName(getClassName()).getConstructor(this.getClass(), T8DataTransaction.class);
            return (T8DataSource)constructor.newInstance(this, accessProvider);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getClassName()
    {
        return (String)getDefinitionDatum(Datum.CLASS_NAME.toString());
    }

    public void setClassName(String propertyValue)
    {
        setDefinitionDatum(Datum.CLASS_NAME.toString(), propertyValue);
    }
}

