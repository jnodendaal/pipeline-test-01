package com.pilog.t8.definition.script;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptClassImport
{
    private final String reference;
    private final Class classToImport;
    
    public T8ScriptClassImport(String reference, Class classToImport)
    {
        this.reference = reference;
        this.classToImport = classToImport;
    }

    public String getReference()
    {
        return reference;
    }

    public Class getClassToImport()
    {
        return classToImport;
    }
}
