package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.time.T8Timestamp;
import java.util.EnumSet;

/**
 * @author Bouwer du Preez
 */
public class T8DtTimestamp extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TIMESTAMP";

    public T8DtTimestamp()
    {
    }

    public T8DtTimestamp(T8DefinitionManager context)
    {
    }

    public T8DtTimestamp(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8Timestamp.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            return JsonValue.valueOf(((T8Timestamp)object).getMilliseconds());
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8Timestamp deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            return new T8Timestamp(jsonValue.asLong());
        }
        else return null;
    }

    @Override
    public EnumSet<T8DataFilterCriterion.DataFilterOperator> getApplicableOperators()
    {
        EnumSet<T8DataFilterCriterion.DataFilterOperator> operators;

        operators = super.getApplicableOperators();
        operators.add(T8DataFilterCriterion.DataFilterOperator.THIS_DAY);

        return operators;
    }
}
