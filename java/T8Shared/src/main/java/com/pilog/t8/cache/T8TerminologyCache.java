package com.pilog.t8.cache;

/**
 * @author Bouwer du Preez
 */
public interface T8TerminologyCache
{
    /**
     * Returns the TransactionManger responsible for transactions involving this terminology cache.
     * This method currently returns an object only to avoid having to include J2EE libraries in this project.
     * In future this interface will be moved and will return the property type: javax.transaction.TransactionManager
     * @return javax.transaction.TransactionManager responsible for transactions on this cache.
     */
    public Object getTransactionManager();

    public void put(Object conceptTerminology);
    public void remove(Object conceptTerminology);
    public Object remove(String languageID, String conceptID);
    public Object get(String languageID, String conceptID);
    public boolean containsKey(String languageID, String conceptID);
}
