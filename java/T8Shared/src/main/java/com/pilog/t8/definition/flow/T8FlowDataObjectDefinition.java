package com.pilog.t8.definition.flow;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowDataObjectDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_DATA_OBJECT";
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_DATA_OBJECT";
    public static final String IDENTIFIER_PREFIX = "FO_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Data Object";
    public static final String DESCRIPTION = "A definition of a data object that is used during flow execution.";
    public enum Datum {DATA_OBJECT_IDENTIFIER,
                       KEY_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //
    
    public T8FlowDataObjectDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_OBJECT_IDENTIFIER.toString(), "Data Object",  "The Data Object used by the flow."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.KEY_PARAMETER_IDENTIFIER.toString(), "Key Parameter", "The flow parameter to which the key of this data object is mapped."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_OBJECT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if (Datum.KEY_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8WorkFlowDefinition flowDefinition;

            flowDefinition = (T8WorkFlowDefinition)getAncestorDefinition(T8WorkFlowDefinition.TYPE_IDENTIFIER);
            return createLocalIdentifierOptionsFromDefinitions(flowDefinition.getFlowParameterDefinitions());
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getDataObjectIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_OBJECT_IDENTIFIER.toString());
    }
    
    public void setDataObjectIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_OBJECT_IDENTIFIER.toString(), identifier);
    }

    public String getKeyParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.KEY_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setKeyParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.KEY_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
