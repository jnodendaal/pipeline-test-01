package com.pilog.t8.definition.event;

import com.pilog.t8.T8DefinitionManager;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8ConfigurationDefinitionsReloadedEvent extends EventObject
{
    private T8DefinitionManager definitionManager;
    
    public T8ConfigurationDefinitionsReloadedEvent(T8DefinitionManager definitionManager)
    {
        super(definitionManager);
        this.definitionManager = definitionManager;
    }

    public T8DefinitionManager getDefinitionManager()
    {
        return definitionManager;
    }
}
