package com.pilog.t8.flow.key;

/**
 * @author Bouwer du Preez
 */
public class T8TaskClaimExecutionKey extends T8DefaultFlowExecutionKey
{
    private final String taskInstanceIdentifier;
    private final String claimedByUserIdentifier;

    public T8TaskClaimExecutionKey(String flowIdentifier, String flowInstanceIdentifier, String nodeIdentifier, String nodeInstanceIdentifier, String taskInstanceIdentifier, String claimedByUserIdentifier)
    {
        super(flowIdentifier, flowInstanceIdentifier, nodeIdentifier, nodeInstanceIdentifier);
        this.taskInstanceIdentifier = taskInstanceIdentifier;
        this.claimedByUserIdentifier = claimedByUserIdentifier;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskInstanceIdentifier;
    }

    public String getClaimedByUserIdentifier()
    {
        return claimedByUserIdentifier;
    }
}
