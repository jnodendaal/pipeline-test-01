package com.pilog.t8.definition.script;

import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentControllerScriptCompletionHandler extends T8DefaultModuleCompletionHandler
{
    public T8ComponentControllerScriptCompletionHandler(T8Context context, T8CustomScriptDefinition definition)
    {
        super(context, definition);
    }
}
