package com.pilog.t8.flow;

import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.flow.state.T8FlowNodeState;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowNode
{
    public enum FlowNodeStatus
    {
        STATE_CREATED, // Node state has been created to store output from preceding nodes.
        NODE_CREATED, // The default state used when an instance of a Node state definition is created as part of node creation.
        STARTED, // Node execution has started.  The node will activate once all input data is available.
        ACTIVE, // All input data for the node has become available and the node has activated.
        COMPLETED, // Node execution has completed.
        STOPPED, // Node execution stopped.
        FAILED // Node execution failed.
    };

    public enum NodeHistoryEvent
    {
        STARTED,
        COMPLETED,
        STOPPED,
        FAILED
    };

    // Returns the definition of the node.
    public T8WorkFlowNodeDefinition getDefinition();
    // Returns the instance identifier of the node.
    public String getInstanceIdentifier();
    // Returns the state definition of the node.
    public T8FlowNodeState getState();
    // Starts execution of the node.
    public T8FlowNodeExecutionResult executeNode(T8FlowExecutionKey key) throws Exception;
    // Instruct the node to stop it's execution as soon as possible.
    public void stopNode() throws Exception;
    // Cancels execution of the node and attempts to roll back any transactional changes.
    public void cancelNode() throws Exception;
    // Returns the status of the node.
    public T8FlowNodeStatus getStatus();
}
