package com.pilog.t8.ui;

import java.awt.Graphics2D;
import java.awt.Shape;

/**
 * @author Bouwer du Preez
 */
public interface T8PainterEffect
{
    /**
     * Paints to the specified graphics context.
     * 
     * @param g2 The Graphics2D to render to. This must not be null.
     * @param clipShape An clip shape of the effect.
     * @param width Width of the area to paint.
     * @param height Height of the area to paint.
     */
    public void apply(Graphics2D g2, Shape clipShape, int width, int height);
}
