package com.pilog.t8.definition.data.connection.direct;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.connection.pool.T8DataConnectionPoolDefinition;
import java.sql.Connection;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DirectDataConnectionDefinition extends T8DataConnectionDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_CONNECTION_DIRECT";
    public static final String DISPLAY_NAME = "Direct Connection";
    public static final String DESCRIPTION = "A direct connection to a remote database.";
    public enum Datum {DRIVER_CLASS_NAME,
                       CONNECTION_STRING,
                       USERNAME,
                       PASSWORD,
                       DATA_CONNECTION_POOL_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DirectDataConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DRIVER_CLASS_NAME.toString(), "Driver Class Name", "The class name of the driver to use when creating a connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONNECTION_STRING.toString(), "Connection String", "The connection String to use when creating a connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.USERNAME.toString(), "Username", "The username to use for authentication when creating a connection"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PASSWORD.toString(), "Password", "The password to use for authentication when creating a connection"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_CONNECTION_POOL_IDENTIFIER.toString(), "Connection Pool", "The connection pool to use for this connection type."));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_CONNECTION_POOL_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionPoolDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataConnection createNewConnectionInstance(T8DataTransaction dataAccessProvider) throws Exception
    {
        T8DataConnectionPool connectionPool;
        T8DataManager dataManager;

        // Get the data manager and the required connection pool.
        dataManager = dataAccessProvider.getDataSession().getDataManager();
        synchronized (dataManager)
        {
            // If the connection pool does not exist, install it.
            connectionPool = dataManager.getDataConnectionPool(getIdentifier());
            if (connectionPool == null)
            {
                String connectionPoolIdentifier;

                connectionPoolIdentifier = getDataConnectionPoolIdentifier();
                if (connectionPoolIdentifier != null)
                {
                    T8DataConnectionPoolDefinition poolDefinition;

                    poolDefinition = dataAccessProvider.getDataConnectionPoolDefinition(getDataConnectionPoolIdentifier());
                    if (poolDefinition != null)
                    {
                        connectionPool = poolDefinition.createNewConnectionPoolInstance(dataAccessProvider);
                        connectionPool.setDataConnectionIdentifier(getIdentifier());
                        connectionPool.setDriverClassName(getDriverClassName());
                        connectionPool.setConnectionURL(getConnectionString());
                        connectionPool.setUsername(getUsername());
                        connectionPool.setPassword(getPassword());
                        dataManager.installDataConnectionPool(connectionPool);
                    }
                    else throw new Exception("Connection pool definition not found: " + connectionPoolIdentifier);
                }
                else throw new Exception("Connection pool not defined for connection: " + this);
            }
        }

        // Now return the connection.
        if (connectionPool != null)
        {
            Connection connection;

            connection = connectionPool.getConnection();
            if (connection == null) throw new Exception("The Data Connection could not be created.");

            return new T8DataConnection(this, connection);
        }
        else throw new Exception("Connection pool could not be found or installed: " + getIdentifier());
    }

    public String getDataConnectionPoolIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_CONNECTION_POOL_IDENTIFIER.toString());
    }

    public void setDataConnectionPoolIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_CONNECTION_POOL_IDENTIFIER.toString(), identifier);
    }

    public String getDriverClassName()
    {
        return (String)getDefinitionDatum(Datum.DRIVER_CLASS_NAME.toString());
    }

    public void setDriverClassName(String driverClassName)
    {
        setDefinitionDatum(Datum.DRIVER_CLASS_NAME.toString(), driverClassName);
    }

    public String getPassword()
    {
        return (String)getDefinitionDatum(Datum.PASSWORD.toString());
    }

    public void setPassword(String password)
    {
         setDefinitionDatum(Datum.PASSWORD.toString(), password);
    }

    public String getConnectionString()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_STRING.toString());
    }

    public void setConnectionString(String connectionString)
    {
         setDefinitionDatum(Datum.CONNECTION_STRING.toString(), connectionString);
    }

    public String getUsername()
    {
        return (String)getDefinitionDatum(Datum.USERNAME.toString());
    }

    public void setUsername(String username)
    {
         setDefinitionDatum(Datum.USERNAME.toString(), username);
    }
}
