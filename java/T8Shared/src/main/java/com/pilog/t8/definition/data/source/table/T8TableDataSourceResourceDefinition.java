package com.pilog.t8.definition.data.source.table;

/**
 * @author Bouwer du Preez
 */
public class T8TableDataSourceResourceDefinition extends T8TableDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATABASE_TABLE_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8TableDataSourceResourceDefinition(String identifier)
    {
        super(identifier);
    }
}
