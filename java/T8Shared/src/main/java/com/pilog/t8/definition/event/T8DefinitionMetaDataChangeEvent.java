package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionMetaDataChangeEvent extends EventObject
{
    private final T8DefinitionMetaData oldValue;
    private final T8DefinitionMetaData newValue;
    private final Object actor;
    
    public T8DefinitionMetaDataChangeEvent(T8Definition definition, T8DefinitionMetaData oldValue, T8DefinitionMetaData newValue, Object actor)
    {
        super(definition);
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.actor = actor;
    }
    
    public T8Definition getDefinition()
    {
        return (T8Definition)getSource();
    }

    public T8DefinitionMetaData getNewValue()
    {
        return newValue;
    }

    public T8DefinitionMetaData getOldValue()
    {
        return oldValue;
    }
    
    public Object getActor()
    {
        return actor;
    }
}
