package com.pilog.t8.definition.script.completionhandler;

import com.pilog.epic.rsyntax.BasicEpicCompletion;
import com.pilog.epic.rsyntax.RegexCompletionProvider;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import javax.swing.text.JTextComponent;

/**
 * @author Hennie Brink
 */
public class T8DefinitionRegexScriptCompletionProvider implements RegexCompletionProvider
{
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8Definition rootDefinition;

    public T8DefinitionRegexScriptCompletionProvider(T8Context context, T8DefinitionManager definitionManager, T8Definition rootDefinition)
    {
        this.context = context;
        this.definitionManager = definitionManager;
        this.rootDefinition = rootDefinition;
    }

    @Override
    public String getRegexPatternToMatch()
    {
        return "[@|\\$][0-9a-zA-Z_]+[\\$]*";
    }

    @Override
    public List<BasicEpicCompletion> getCompletionChoices(JTextComponent tc, String matchedString)
    {
        List<BasicEpicCompletion> completions;

        completions = new ArrayList<BasicEpicCompletion>();
        if(T8IdentifierUtilities.isPublicId(matchedString))
        {
            //Do nothing for now as this would be to intensive
        }
        else if(T8IdentifierUtilities.isLocalId(matchedString) && rootDefinition != null)
        {
            for (T8Definition t8Definition : rootDefinition.getLocalDefinitions())
            {
                if(t8Definition.getIdentifier().contains(matchedString))
                {
                    completions.add(new BasicEpicCompletion(t8Definition.getIdentifier()));
                }
            }
        }
        else if(T8IdentifierUtilities.isQualifiedLocalId(matchedString))
        {
            T8Definition definition;
            try
            {
                definition = definitionManager.getRawDefinition(context, rootDefinition.getRootProjectId(), T8IdentifierUtilities.getGlobalIdentifierPart(matchedString));
                for (T8Definition t8Definition : definition.getLocalDefinitions())
                {
                    if(t8Definition.getIdentifier().contains(matchedString))
                    {
                        completions.add(new BasicEpicCompletion(t8Definition.getIdentifier()));
                    }
                }
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to get completions for " + matchedString, ex);
            }
        }
        else if(matchedString.matches("@[0-9a-zA-Z_]+[\\$]*"))
        {
            T8Definition definition;
            try
            {
                definition = definitionManager.getRawDefinition(context, rootDefinition.getRootProjectId(), T8IdentifierUtilities.getGlobalIdentifierPart(matchedString));
                for (T8Definition t8Definition : definition.getLocalDefinitions())
                {
                    if(t8Definition.getPublicIdentifier().contains(matchedString))
                    {
                        completions.add(new BasicEpicCompletion(t8Definition.getIdentifier().replaceFirst("\\$", "")));
                    }
                }
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to get completions for " + matchedString, ex);
            }
        }

        return completions;
    }

}
