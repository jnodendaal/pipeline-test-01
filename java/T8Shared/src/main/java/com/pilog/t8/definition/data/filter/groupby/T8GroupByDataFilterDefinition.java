package com.pilog.t8.definition.data.filter.groupby;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.groupby.T8GroupByDataFilter;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.filter.T8DataFilterCriteriaDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.data.filter.advanced.T8AdvancedDataFilterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8GroupByDataFilterDefinition extends T8AdvancedDataFilterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_FILTER_GROUP_BY";
    public static final String DISPLAY_NAME = "Group By Filter";
    public static final String DESCRIPTION = "A Data Filter for Group by Entities";
    public static final String IDENTIFIER_PREFIX = "GROUP_BY_DF_";
    public static final String VERSION = "0";

    public enum Datum
    {
        REFERENCE_DE_FILTER
    };
// -------- Definition Meta-Data -------- //

    public T8GroupByDataFilterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.REFERENCE_DE_FILTER.toString(), "Reference Data Entity Filter", "The filter that will be passed to the referenced data entity."));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REFERENCE_DE_FILTER.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public T8DataFilter getNewDataFilterInstance(T8Context context, Map<String, Object> filterParameters)
    {
        T8DataFilterCriteriaDefinition filterCriteriaDefinition;
        T8DataFilter dataFilter;

        if(getReferencedDataEntityFilterDefinition() == null)
        {
            dataFilter = new T8GroupByDataFilter(this);
        }
        else
        {
            dataFilter = new T8GroupByDataFilter(this, getReferencedDataEntityFilterDefinition().getNewDataFilterInstance(context, filterParameters));
        }

        dataFilter.setFieldOrdering(getFieldOrderingMap());
        filterCriteriaDefinition = getFilterCriteriaDefinition();
        if (filterCriteriaDefinition != null) dataFilter.setFilterCriteria(filterCriteriaDefinition.getNewDataFilterClauseInstance(context, filterParameters));
        return dataFilter;
    }

    public T8DataFilterDefinition getReferencedDataEntityFilterDefinition()
    {
        return (T8DataFilterDefinition)getDefinitionDatum(Datum.REFERENCE_DE_FILTER.toString());
    }

    public void setReferencedDataEntityFilterDefinition(T8DataFilterDefinition innerFilterDefinition)
    {
        setDefinitionDatum(Datum.REFERENCE_DE_FILTER.toString(), innerFilterDefinition);
    }
}
