package com.pilog.t8.definition.system.monitor;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Hennie Brink
 */
public class T8ServerMonitorAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GET_SERVER_MONITOR_REPORT = "@OS_GET_SERVER_MONITOR_REPORT";

    public static final String PARAMETER_REPORT = "$REPORT";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_SERVER_MONITOR_REPORT);
            definition.setMetaDisplayName("Get Server Monitor Report");
            definition.setMetaDescription("Retrieves a server monitoring report.");
            definition.setClassName("com.pilog.t8.system.monitor.T8ServerMonitorOperations$GetT8ServerMonitorReport");
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT, "Report", "The monitor report created by this operation", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
