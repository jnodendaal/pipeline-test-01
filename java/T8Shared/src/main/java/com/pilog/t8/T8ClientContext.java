package com.pilog.t8;

import com.pilog.t8.definition.T8DefinitionEditorFactory;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Window;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ClientContext
{
    private final T8Client client;
    private final T8SessionContext sessionContext;

    public T8ClientContext(T8Client client, T8SessionContext sessionContext)
    {
        this.client = client;
        this.sessionContext = sessionContext;
    }

    public T8ClientEnvironmentStatus getEnvironmentStatus()
    {
        return client.getEnvironmentStatus();
    }

    public void refreshNotifications()
    {
        client.refreshNotifications();
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public void lockUI(String message)
    {
        client.lockUI(message);
    }

    public void unlockUI()
    {
        client.unlockUI();
    }

    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return client.getExpressionEvaluator();
    }

    public Window getParentWindow()
    {
        return client.getParentWindow();
    }

    public void showAdministrationConsole()
    {
        client.showAdministrationConsole();
    }

    public void dispayHelp(String helpIdentifier)
    {
        client.dispayHelp(helpIdentifier);
    }

    public T8DefinitionManager getDefinitionManager()
    {
        return client.getDefinitionManager();
    }

    public T8CommunicationManager getCommunicationManager()
    {
        return client.getCommunicationManager();
    }

    public T8NotificationManager getNotificationManager()
    {
        return client.getNotificationManager();
    }

    public T8ServiceManager getServiceManager()
    {
        return client.getServiceManager();
    }

    public T8ProcessManager getProcessManager()
    {
        return client.getProcessManager();
    }

    public T8SecurityManager getSecurityManager()
    {
        return client.getSecurityManager();
    }

    public T8FunctionalityManager getFunctionalityManager()
    {
        return client.getFunctionalityManager();
    }

    public T8FlowManager getFlowManager()
    {
        return client.getFlowManager();
    }

    public T8FileManager getFileManager()
    {
        return client.getFileManager();
    }

    public T8ConfigurationManager getConfigurationManager()
    {
        return client.getConfigurationManager();
    }

    public T8DefinitionEditorFactory getDefinitionEditorFactory(T8Context context)
    {
        return client.getDefinitionEditorFactory(context);
    }

    public T8ServerOperationStatusReport getServerOperationStatus(T8Context context, String operationIid) throws Exception
    {
        return client.getServerOperationStatus(context, operationIid);
    }

    public T8ServerOperationStatusReport stopServerOperation(T8Context context, String operationIid) throws Exception
    {
        return client.stopServerOperation(context, operationIid);
    }

    public T8ServerOperationStatusReport cancelServerOperation(T8Context context, String operationIid) throws Exception
    {
        return client.cancelServerOperation(context, operationIid);
    }

    public T8ServerOperationStatusReport pauseServerOperation(T8Context context, String operationIid) throws Exception
    {
        return client.pauseServerOperation(context, operationIid);
    }

    public T8ServerOperationStatusReport resumeServerOperation(T8Context context, String operationIid) throws Exception
    {
        return client.resumeServerOperation(context, operationIid);
    }

    public T8ServerOperationStatusReport executeAsynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        return client.executeAsynchronousOperation(context, operationId, operationParameters);
    }

    public Map<String, Object> executeAsynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters, String message) throws Exception
    {
        return client.executeAsynchronousServerOperation(context, operationId, operationParameters, message);
    }

    public Map<String, Object> executeSynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters, String message) throws Exception
    {
        return client.executeSynchronousServerOperation(context, operationId, operationParameters, message);
    }

    public Map<String, Object> executeClientOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        return client.executeClientOperation(context, operationId, operationParameters);
    }

    public Map<String, Object> executeClientScript(T8ScriptDefinition scriptDefinition, Map<String, Object> inputParameters) throws Exception
    {
        return client.executeClientScript(scriptDefinition, inputParameters);
    }
}
