package com.pilog.t8.definition.gfx.font;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FontDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_FONT";
    public static final String GROUP_IDENTIFIER = "@DG_GFX_FONT";
    public static final String STORAGE_PATH = "/fonts";
    public static final String DISPLAY_NAME = "Font";
    public static final String DESCRIPTION = "A type face to use when drawing text Strings in a graphics context.";
    public static final String VERSION = "0";
    public enum Datum {FONT_NAME,
                       FONT_STYLE,
                       FONT_SIZE};
    // -------- Definition Meta-Data -------- //

    public T8FontDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FONT_NAME.toString(), "Font Name", "The name of the font family to use e.g. Arial.", "Tahoma", T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.FONT_STYLE.toString(), "Font Style", "The style of the font e.g. Bold/Italic."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.FONT_SIZE.toString(), "Font Size", "The size of the font.", 12));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FONT_NAME.toString().equals(datumIdentifier))
        {
            GraphicsEnvironment env;

            env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            return createStringOptions(env.getAvailableFontFamilyNames());
        }
        else if (Datum.FONT_STYLE.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            optionList.add(new T8DefinitionDatumOption("Plain", Font.PLAIN));
            optionList.add(new T8DefinitionDatumOption("Bold", Font.BOLD));
            optionList.add(new T8DefinitionDatumOption("Italic", Font.ITALIC));
            optionList.add(new T8DefinitionDatumOption("Bold Italic", Font.BOLD & Font.ITALIC));
            return optionList;
        }
        else if (Datum.FONT_SIZE.toString().equals(datumIdentifier))
        {
            Integer[] intValues;

            intValues = new Integer[]{8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 36, 48, 72};
            return createIntegerOptions(intValues);
        }
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public Font getNewFontInstance()
    {
        try
        {
            return new Font(getFontName(), getFontStyle(), getFontSize());
        }
        catch (Exception e)
        {
            T8Log.log("While attempting to create font from Definition: " + getIdentifier(), e);
            return null;
        }
    }

    public void setFont(Font font)
    {
        if (font != null)
        {
            setFontName(font.getName());
            setFontStyle(font.getStyle());
            setFontSize(font.getSize());
        }
    }

    public String getFontName()
    {
        return (String)getDefinitionDatum(Datum.FONT_NAME.toString());
    }

    public void setFontName(String fontName)
    {
        setDefinitionDatum(Datum.FONT_NAME.toString(), fontName);
    }

    public Integer getFontStyle()
    {
        return (Integer)getDefinitionDatum(Datum.FONT_STYLE.toString());
    }

    public void setFontStyle(int fontStyle)
    {
        setDefinitionDatum(Datum.FONT_STYLE.toString(), fontStyle);
    }

    public Integer getFontSize()
    {
        return (Integer)getDefinitionDatum(Datum.FONT_SIZE.toString());
    }

    public void setFontSize(int fontSize)
    {
        setDefinitionDatum(Datum.FONT_SIZE.toString(), fontSize);
    }
}
