package com.pilog.t8.project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectImportReport implements Serializable
{
    private final List<T8ProjectImportResult> results;

    public T8ProjectImportReport()
    {
        this.results = new ArrayList<>();
    }

    public void addResults(T8ProjectImportResult result)
    {
        this.results.add(result);
    }

    public List<T8ProjectImportResult> getResults()
    {
        return new ArrayList<>(results);
    }

    public T8ProjectImportResult getProjectResult(String projectId)
    {
        for (T8ProjectImportResult result : results)
        {
            if (projectId.equals(result.getProjectId()))
            {
                return result;
            }
        }

        return null;
    }

    public static class T8ProjectImportResult implements Serializable
    {
        private final String projectId;
        private final String message;
        private final boolean success;

        public T8ProjectImportResult(String projectId, boolean success, String message)
        {
            this.projectId = projectId;
            this.success = success;
            this.message = message;
        }

        public String getProjectId()
        {
            return projectId;
        }

        public String getMessage()
        {
            return message;
        }

        public boolean isSuccess()
        {
            return success;
        }
    }
}
