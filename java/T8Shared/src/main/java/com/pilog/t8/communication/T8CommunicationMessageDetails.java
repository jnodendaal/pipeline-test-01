package com.pilog.t8.communication;

import java.io.Serializable;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8CommunicationMessageDetails implements Serializable
{
    private final String messageIdentifier;
    private final String messageInstanceIdentifier;
    private final String communicationIdentifier;
    private final String communicationInstanceIdentifier;
    private final String initiatorUserIdentifier;
    private final String initiatorUserProfileIdentifier;
    private final String recipientAddress;
    private final String recipientDisplayName;
    private final String serviceIdentifier;
    private final int priority;
    private final String subject;

    public T8CommunicationMessageDetails(String messageIdentifier,
                                         String messageInstanceIdentifier,
                                         String communicationIdentifier,
                                         String communicationInstanceIdentifier,
                                         String initiatorUserIdentifier,
                                         String initiatorUserProfileIdentifier,
                                         String recipientAddress,
                                         String recipientDisplayName,
                                         String serviceIdentifier,
                                         int priority,
                                         String subject)
    {
        this.messageIdentifier = messageIdentifier;
        this.messageInstanceIdentifier = messageInstanceIdentifier;
        this.communicationIdentifier = communicationIdentifier;
        this.communicationInstanceIdentifier = communicationInstanceIdentifier;
        this.initiatorUserIdentifier = initiatorUserIdentifier;
        this.initiatorUserProfileIdentifier = initiatorUserProfileIdentifier;
        this.recipientAddress = recipientAddress;
        this.recipientDisplayName = recipientDisplayName;
        this.serviceIdentifier = serviceIdentifier;
        this.priority = priority;
        this.subject = subject;
    }

    public String getCommunicationIdentifier()
    {
        return communicationIdentifier;
    }

    public String getCommunicationInstanceIdentifier()
    {
        return communicationInstanceIdentifier;
    }

    public String getInitiatorUserIdentifier()
    {
        return initiatorUserIdentifier;
    }

    public String getInitiatorUserProfileIdentifier()
    {
        return initiatorUserProfileIdentifier;
    }

    public String getMessageInstanceIdentifier()
    {
        return messageInstanceIdentifier;
    }

    public String getMessageTemplateIdentifier()
    {
        return messageIdentifier;
    }

    public int getPriority()
    {
        return priority;
    }

    public String getRecipientAddress()
    {
        return recipientAddress;
    }

    public String getRecipientDisplayName()
    {
        return recipientDisplayName;
    }

    public String getServiceIdentifier()
    {
        return serviceIdentifier;
    }

    public String getSubject()
    {
        return subject;
    }
}
