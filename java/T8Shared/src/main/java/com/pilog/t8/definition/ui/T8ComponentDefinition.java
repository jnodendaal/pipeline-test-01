package com.pilog.t8.definition.ui;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.script.T8ComponentEventHandlerScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Map;

/**
 * Each component that forms part of a module is initialized from a single
 * object model that holds all of the meta-data for the specific object.  Such
 * an object model is called a Component Definition, and this interface
 * specifies the behavior that all Component Definitions must comply to.
 *
 * When a component module is loaded the following sequence of steps are
 * executed:
 *  1.  Client-side:  A component controller requests the definition of a
 *      component from the server.
 *  2.  Server-side:  The requested Component Definition is loaded and then
 *      initialized.  During initialization of a definition each of its
 *      sub-component definitions are initialized, resulting in a recursive
 *      descent trough all descendent component definitions until all of them
 *      are initialized.  During a Component Definition initialization each
 *      Component Definition must perform any tasks or operations that are
 *      necessary to complete its state, before being sent to the client-side
 *      for component initialization.  This includes translation of displayed
 *      Strings, checking of security layer and retrieval of auxiliary data from
 *      the database.  Once a Component Definition has been initialized it MUST
 *      contain ALL required information to initialize the component which it
 *      defines.
 *  3.  Server-side:  The initialized Component Definition tree is returned to
 *      the requesting client-side controller.
 *  4.  Client-side:  The loaded and initialized Component Definition tree
 *      received from the server is now traversed by the parent controller.
 *      Each component definition in the tree is used to initialize a new
 *      instance of a component and each initialized component is added to its
 *      parent.  During this step NO further server requests may be performed by
 *      ANY of the component being initialized, since their individual
 *      definitions should now contain all the information they require.
 *
 * @author Bouwer du Preez
 */
public abstract class T8ComponentDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT";
    public static final String STORAGE_PATH = "/ui_components";
    public static final String IDENTIFIER_PREFIX = "C_";
    public enum Datum
    {
        VISIBLE,
        OPAQUE,
        COMPONENT_EVENT_DEFINITIONS,
        COMPONENT_OPERATION_DEFINITIONS,
        EVENT_HANDLER_SCRIPT_DEFINITIONS,
        TOOLTIP_TEXT
    };
    // -------- Definition Meta-Data -------- //

    public T8ComponentDefinition(String identifier)
    {
        super(identifier);
        setDerivedDatumValues();
    }

    private void setDerivedDatumValues()
    {
        setDefinitionDatum(Datum.VISIBLE.toString(), true, false, this);
        setDefinitionDatum(Datum.COMPONENT_EVENT_DEFINITIONS.toString(), getComponentEventDefinitions(), false, this);
        setDefinitionDatum(Datum.COMPONENT_OPERATION_DEFINITIONS.toString(), getComponentOperationDefinitions(), false, this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.VISIBLE.toString(), "Visible", "Sets the initial visibility of the component.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPAQUE.toString(), "Opaque", "Sets the initial opacity of the component.", true));
        datumTypes.add(T8DefinitionDatumType.derivedDefinitionList(Datum.COMPONENT_EVENT_DEFINITIONS.toString(), "Events", "The events types supported by this component."));
        datumTypes.add(T8DefinitionDatumType.derivedDefinitionList(Datum.COMPONENT_OPERATION_DEFINITIONS.toString(), "Operations", "The operation types supported by this component."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString(), "Event Handlers", "The event handlers registered to this component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TOOLTIP_TEXT.toString(), "Tooltip Text", "Sets teh tooltip text for this component."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString().equals(datumIdentifier))
        {
            ArrayList<T8ComponentEventDefinition> eventDefinitions;
            ArrayList<T8DefinitionDatumOption> options;

            options = new ArrayList<T8DefinitionDatumOption>();
            eventDefinitions = getComponentEventDefinitions();
            if (eventDefinitions != null)
            {
                for (T8ComponentEventDefinition eventDefinition : eventDefinitions)
                {
                    ArrayList<Object> parameters;
                    String eventHandlerIdentifier;
                    String displayName;

                    eventHandlerIdentifier = eventDefinition.getIdentifier();
                    eventHandlerIdentifier = eventHandlerIdentifier.replace("@", "");
                    eventHandlerIdentifier = eventHandlerIdentifier.replace("$", "");
                    eventHandlerIdentifier = eventHandlerIdentifier + "_" + T8IdentifierUtilities.createNewGUID();
                    eventHandlerIdentifier = "$CEH_" + eventHandlerIdentifier;

                    displayName = eventDefinition.getMetaDisplayName();
                    if (Strings.isNullOrEmpty(displayName)) displayName = "Handle Event: " + eventDefinition.getIdentifier();

                    parameters = new ArrayList<Object>();
                    parameters.add(eventDefinition.getIdentifier());
                    options.add(new T8DefinitionDatumOption(displayName, new T8DefinitionConstructorParameters(eventHandlerIdentifier, T8ComponentEventHandlerScriptDefinition.TYPE_IDENTIFIER, parameters)));
                }
            }

            return options;
        }
        else return null;
    }

    public T8ComponentControllerDefinition getControllerDefinition()
    {
        for (Object ancestorDefinition : getAncestorDefinitions())
        {
            if (ancestorDefinition instanceof T8ComponentControllerDefinition)
            {
                return (T8ComponentControllerDefinition)ancestorDefinition;
            }
        }

        return null;
    }

    public ArrayList<T8ComponentEventHandlerScriptDefinition> getEventHandlerScriptDefinitions()
    {
        return (ArrayList<T8ComponentEventHandlerScriptDefinition>)getDefinitionDatum(Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString());
    }

    public void setEventHandlerScriptDefinitions(ArrayList<T8ComponentEventHandlerScriptDefinition> newDefinitions)
    {
        setDefinitionDatum(Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString(), newDefinitions);
    }

    public void addEventHandlerScriptDefinition(T8ComponentEventHandlerScriptDefinition eventHandlerDefinition)
    {
        addSubDefinition(Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString(), eventHandlerDefinition);
    }

    public boolean removeEventHandlerScriptDefinition(T8ComponentEventHandlerScriptDefinition eventHandlerDefinition)
    {
        return removeSubDefinition(eventHandlerDefinition);
    }

    public T8ComponentEventDefinition getComponentEventDefinition(String eventIdentifier)
    {
        ArrayList<T8ComponentEventDefinition> eventDefinitions;

        eventDefinitions = getComponentEventDefinitions();
        if (eventDefinitions != null)
        {
            for (T8ComponentEventDefinition eventDefinition : eventDefinitions)
            {
                if (eventDefinition.getIdentifier().equals(eventIdentifier)) return eventDefinition;
            }
        }

        return null;
    }

    public T8ComponentOperationDefinition getComponentOperationDefinition(String operationIdentifier)
    {
        ArrayList<T8ComponentOperationDefinition> operationDefinitions;

        operationDefinitions = getComponentOperationDefinitions();
        if (operationDefinitions != null)
        {
            for (T8ComponentOperationDefinition operationDefinition : operationDefinitions)
            {
                if (operationDefinition.getIdentifier().equals(operationIdentifier)) return operationDefinition;
            }
        }

        return null;
    }

    public boolean isVisible()
    {
        return (Boolean)getDefinitionDatum(Datum.VISIBLE.toString());
    }

    public void setVisible(boolean visible)
    {
        setDefinitionDatum(Datum.VISIBLE.toString(), visible);
    }

    public boolean isOpaque()
    {
        Boolean opaque;

        opaque = (Boolean)getDefinitionDatum(Datum.OPAQUE.toString());
        return ((opaque == null) || (opaque));
    }

    public void setOpaque(boolean opaque)
    {
        setDefinitionDatum(Datum.OPAQUE.toString(), opaque);
    }

    public String getTooltipText()
    {
        return (String)getDefinitionDatum(Datum.TOOLTIP_TEXT.toString());
    }

    public void setTooltipText(String text)
    {
        setDefinitionDatum(Datum.TOOLTIP_TEXT.toString(), text);
    }

    @Override
    public T8DefinitionDocumentationProvider createDocumentationProvider(T8DefinitionContext definitionContext)
    {
        return new T8ComponentDefinitionDocumentationProvider(definitionContext);
    }

    /**
     * This method is always called by the T8 Server when this definition has
     * been freshly loaded.  This method must perform any and ALL operations
     * that are necessary to complete the state of the definition before being
     * used to initialize the component which it defines.  Any translations of
     * displayed Strings must be completed by this method.  Once this method has
     * completed, the Component Definition must be fully prepared to instantiate
     * a new instance of the Component which it defines, without that component
     * having to perform any other server or database request for meta data.
     *
     * @throws Exception Any un-handled Exception that occurs during the
     * initialization of this definition.
     */
    @Override
    public abstract void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception;

    /**
     * The method must return a new instance of the T8Component.
     * @param controller The component controller that will manage the created
     * component during its life-cycle.
     * @return The newly created component instance.
     * @throws java.lang.Exception
     */
    public abstract <C extends T8Component> C getNewComponentInstance(T8ComponentController controller) throws Exception;

    /**
     * Each T8Component will be queried by its parent controller using this
     * method for the valid input parameters that can be set on this
     * T8Component.
     * @return A list of input parameter definitions.
     */
    public abstract ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions();

    /**
     * Each T8Component will be queried by its parent controller using this
     * method for the available events, that may occur on this T8Component.
     * @return A list of component event definitions.
     */
    public abstract ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions();

    /**
     * Each T8Component will be queried by its parent controller using this
     * method for the available operations that can be executed on the
     * component.
     * @return A list of this component's operation definitions.
     */
    public abstract ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions();

    /**
     * Each T8Component will be queried by its parent controller using this
     * method for the available sub component definitions that belong to this
     * component and that must be logically added to this component when the UI
     * layout is constructed.
     * @return
     */
    public abstract ArrayList<? extends T8ComponentDefinition> getChildComponentDefinitions();
}
