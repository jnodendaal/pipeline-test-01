package com.pilog.t8.definition;

import java.util.Comparator;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDatumOption
{
    private final String displayName;
    private final Object value;

    public T8DefinitionDatumOption(String displayName, Object value)
    {
        this.displayName = displayName;
        this.value = value;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public Object getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return displayName;
    }

    /**
     * A comparator that can be used to organize datum options lexicographically
     * by display name.
     */
    public static class DisplayNameComparator implements Comparator<T8DefinitionDatumOption>
    {
        @Override
        public int compare(T8DefinitionDatumOption o1, T8DefinitionDatumOption o2)
        {
            if (o1 == null)
            {
                if (o2 == null) return 0;
                else return -1;
            }
            else if (o2 == null)
            {
                return 1;
            }
            else
            {
                String displayName1;
                String displayName2;

                displayName1 = o1.getDisplayName();
                displayName2 = o2.getDisplayName();

                if (displayName1 == null)
                {
                    if (displayName2 == null) return 0;
                    else return -1;
                }
                else if (displayName2 == null)
                {
                    return 1;
                }
                else return displayName1.compareTo(displayName2);
            }
        }
    }
}
