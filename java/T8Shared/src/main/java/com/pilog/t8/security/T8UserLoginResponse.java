package com.pilog.t8.security;

import com.pilog.t8.T8SecurityManager.T8LoginResponseType;
import com.pilog.t8.T8SessionContext;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8UserLoginResponse implements Serializable
{
    private final T8LoginResponseType responseType;
    private final T8SessionContext sessionContext;
    private T8SessionDetails sessionDetails;
    private String message;
    private String apiKey;

    public T8UserLoginResponse(T8Context context, T8LoginResponseType responseType)
    {
        this.responseType = responseType;
        this.sessionContext = context.getSessionContext();
    }

    public T8UserLoginResponse(T8Context context, String apiKey, T8LoginResponseType responseType, T8SessionDetails userDetails)
    {
        this(context, responseType);
        this.sessionDetails = userDetails;
        this.apiKey = apiKey;
    }

    public T8UserLoginResponse(T8Context context, T8LoginResponseType responseType, String message, T8SessionDetails userDetails)
    {
        this.responseType = responseType;
        this.sessionContext = context.getSessionContext();
        this.sessionDetails = userDetails;
        this.message = message;
    }

    public T8LoginResponseType getResponseType()
    {
        return this.responseType;
    }

    public T8SessionContext getSessionContext()
    {
        return this.sessionContext;
    }

    public String getMessage()
    {
        return this.message;
    }

    public String getAPIKey()
    {
        return this.apiKey;
    }

    public T8SessionDetails getSessionDetails()
    {
        return sessionDetails;
    }

    public void setSessionDetails(T8SessionDetails sessionDetails)
    {
        this.sessionDetails = sessionDetails;
    }
}
