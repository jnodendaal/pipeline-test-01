/**
 * Created on Jul 2, 2015, 11:03:33 AM
 */
package com.pilog.t8.definition.user.password;

import com.pilog.t8.definition.user.T8UserDefinition;

/**
 * An interface to be implemented for password rule checking. Each
 * {@code T8PasswordPolicyRule} implementation should have its own
 * implementation of this interface.<br/>
 * <br/>
 * It is extremely important to abstract all of the password rule checking in
 * such a way that it cannot be accessed through the code downloaded to the
 * client. This means that the actual validation functions should always occur
 * on the server side.
 *
 * @author Gavin Boshoff
 */
public interface T8PasswordRuleValidator
{
    /**
     * Validates the specified password for the user, based on configuration
     * which has been set up as a rule for the current system.<br/>
     * <br/>
     * It is always extremely important to keep to using the char[] for
     * passwords, as these values can be overwritten in memory, and will be as
     * soon as the value is no longer required.
     *
     * @param userDefinition The {@code T8UserDefinition} for the current user
     *      for which the password validation should occur
     * @param newPassw The {@code char[]} representation of the new password to
     *      be checked
     * @param newHash The newly generated password hash {@code String}
     *
     * @return {@code true} if the validation has succeeded and the rule accepts
     *      the new password. {@code false} if the rule rejects the new password
     */
    public boolean validate(T8UserDefinition userDefinition, char[] newPassw, String newHash);
}