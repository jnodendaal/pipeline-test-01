package com.pilog.t8.definition.notification;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationGenerator;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8Image;
import com.pilog.t8.ui.notification.T8NotificationDisplayComponent;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8EPICNotificationDefinition extends T8NotificationDefinition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_NOTIFICATION_EPIC";
    public static final String DISPLAY_NAME = "EPIC Scripted Notification";
    public static final String DESCRIPTION = "A definition of a notification generator that uses an EPIC script to generate notifications to be sent.";
    public enum Datum {NOTIFICATION_DISPLAY_NAME,
                       ICON_IDENTIFIER,
                       INPUT_PARAMETER_DEFINITIONS,
                       NOTIFICATION_PARAMETER_DEFINITIONS,
                       EPIC_SCRIPT};
    // -------- Definition Meta-Data -------- //

    // EPIC script input parameters.
    public static final String PARAMETER_NOTIFICATION_LIST = "$P_NOTIFICATION_LIST";

    // Notification icon definition added when this definition is initialized.
    private T8IconDefinition iconDefinition;

    public T8EPICNotificationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.NOTIFICATION_DISPLAY_NAME.toString(), "Display Name", "The name of this notification type to be used when displayed on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon that represents this notification type when displayed on the UI."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters", "The input parameters required to generate notifications of this type."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.NOTIFICATION_PARAMETER_DEFINITIONS.toString(), "Notification Parameters", "The parameters that will be contained by generated notifications and displayed on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.EPIC_SCRIPT.toString(), "EPIC Script",  "The script the will be executed to refresh the Record Access."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.NOTIFICATION_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8EPICNotificationParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconIdentifier;

        // Invoke the super implementation.
        super.initializeDefinition(context, inputParameters, configurationSettings);

        // Pre-load the icon definition if one is specified.
        iconIdentifier = getIconIdentifier();
        if (iconIdentifier != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconIdentifier, inputParameters);
        }
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else if (Datum.NOTIFICATION_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public String getNotificationDisplayName()
    {
        return (String)getDefinitionDatum(Datum.NOTIFICATION_DISPLAY_NAME.toString());
    }

    public void setNotificationDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.NOTIFICATION_DISPLAY_NAME.toString(), displayName);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }

    @Override
    public Icon getNotificationIcon()
    {
        if (iconDefinition != null)
        {
            T8Image iconImage;

            iconImage = iconDefinition.getImage();
            return iconImage != null ? iconImage.getImageIcon() : null;
        }
        else return null;
    }

    public String getIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return (List<T8DataParameterDefinition>)getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setInputParameterDefinitions(List<T8DataParameterDefinition> definitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), definitions);
    }

    @Override
    public List<T8EPICNotificationParameterDefinition> getNotificationParameterDefinitions()
    {
        return (List<T8EPICNotificationParameterDefinition>)getDefinitionDatum(Datum.NOTIFICATION_PARAMETER_DEFINITIONS.toString());
    }

    public void setNotificationParameterDefinitions(List<T8EPICNotificationParameterDefinition> definitions)
    {
        setDefinitionDatum(Datum.NOTIFICATION_PARAMETER_DEFINITIONS.toString(), definitions);
    }

    public String getEPICScript()
    {
        return (String)getDefinitionDatum(Datum.EPIC_SCRIPT.toString());
    }

    public void setEPICScript(String script)
    {
        setDefinitionDatum(Datum.EPIC_SCRIPT.toString(), script);
    }

    @Override
    public T8NotificationGenerator getNotificationGeneratorInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.notification.T8EPICNotificationGenerator", new Class<?>[]{T8Context.class, T8EPICNotificationDefinition.class}, context, this);
    }

    @Override
    public T8NotificationDisplayComponent getDisplayComponentInstance(T8Context context, T8Notification notification)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.notification.T8EPICNotificationDisplayComponent", new Class<?>[]{T8Context.class, T8EPICNotificationDefinition.class, T8Notification.class}, context, this, notification);
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultServerContextScript").getConstructor(T8Context.class, T8ServerContextScriptDefinition.class);
        return (T8ServerContextScript)constructor.newInstance(context, this);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        // Use the method implemented as part of the notification definition interface.
        return getInputParameterDefinitions();
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_NOTIFICATION_LIST, "Notification List", "The list of notifications generated using the supplied input parameters.", T8DataType.LIST));
        return parameterList;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<T8ScriptClassImport>();
        classImports.add(new T8ScriptClassImport(T8Notification.class.getSimpleName(), T8Notification.class));
        return classImports;
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        List<T8ScriptMethodImport> methodImports;

        methodImports = new ArrayList<T8ScriptMethodImport>();
        methodImports.add(new T8ScriptMethodImport("createNotification", this, T8EPICNotificationDefinition.class.getDeclaredMethod("createNotification", String.class, Map.class)));
        return methodImports;
    }

    @Override
    public String getScript()
    {
        return getEPICScript();
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }

    public T8Notification createNotification(String userIdentifier, Map<String, ? extends Object> parameters)
    {
        T8Notification notification;

        notification = new T8Notification(getRootProjectId(), getIdentifier(), T8IdentifierUtilities.createNewGUID());
        notification.setUserIdentifier(userIdentifier);
        notification.setParameters(parameters);
        return notification;
    }
}