package com.pilog.t8.definition.script.validation.entity;

import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;

/**
 * @author Bouwer du Preez
 */
public class T8ServerEntityValidationScriptDefinition extends T8EntityValidationScriptDefinition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_SERVER_ENTITY_VALIDATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_SERVER_ENTITY_VALIDATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Server Entity Validation Script";
    public static final String DESCRIPTION = "A script that is executed on the server-side to validate a data entity.";
    public static final String VERSION = "0";
    public enum Datum {DATA_ENTITY_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8ServerEntityValidationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultServerContextScript").getConstructor(T8Context.class, T8ServerContextScriptDefinition.class);
        return (T8ServerContextScript)constructor.newInstance(context, this);
    }
}
