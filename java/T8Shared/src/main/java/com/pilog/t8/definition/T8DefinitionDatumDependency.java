package com.pilog.t8.definition;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDatumDependency implements Serializable
{
    private final String definitionTypeIdentifier;
    private final String datumIdentifier;

    public T8DefinitionDatumDependency(String definitionTypeId, String datumId)
    {
        this.definitionTypeIdentifier = definitionTypeId;
        this.datumIdentifier = datumId;
    }

    public String getDatumIdentifier()
    {
        return datumIdentifier;
    }

    public String getDefinitionTypeIdentifier()
    {
        return definitionTypeIdentifier;
    }

    @Override
    public String toString()
    {
        return "T8DefinitionDatumDependency{" + "definitionTypeIdentifier=" + definitionTypeIdentifier + ", datumIdentifier=" + datumIdentifier + '}';
    }
}
