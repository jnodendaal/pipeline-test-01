package com.pilog.t8.flow.task;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskCompletionResult implements Serializable
{
    private String taskIid;
    private T8TaskCompletionResultType resultType;
    private final Map<String, Object> resultParameters;

    public enum T8TaskCompletionResultType
    {
        SUCCESS, // Successful task completion.
        FAILURE_ACCESS, // Task completion failure due access violation: user restricted from task.
        FAILURE_CLAIMED, // Task completion failure due to current task state: claimed by another user.
        FAILURE_COMPLETED, // Task completion failure due to current task state:  already completed.
        FAILURE_VALIDATION // Task completion failure due to end condition validation.
    }

    public T8TaskCompletionResult()
    {
        this.resultParameters = new HashMap<>();
    }

    public T8TaskCompletionResult(String taskIid, T8TaskCompletionResultType resultType, Map<String, Object> resultParameters)
    {
        this();
        this.taskIid = taskIid;
        this.resultType = resultType;
        T8TaskCompletionResult.this.setResultParameters(resultParameters);
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public void setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
    }

    public boolean isSuccess()
    {
        return resultType == T8TaskCompletionResultType.SUCCESS;
    }

    public T8TaskCompletionResultType getResultType()
    {
        return resultType;
    }

    public void setResultType(T8TaskCompletionResultType resultType)
    {
        this.resultType = resultType;
    }

    public Map<String, Object> getResultParameters()
    {
        return new HashMap<>(resultParameters);
    }

    public void setResultParameters(Map<String, Object> resultParameters)
    {
        this.resultParameters.clear();
        if (resultParameters != null)
        {
            this.resultParameters.putAll(resultParameters);
        }
    }
}
