package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.time.T8TimeUnit;

/**
 * @author Bouwer du Preez
 */
public class T8DtTimeUnit extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TIME_UNIT";

    public T8DtTimeUnit()
    {
    }

    public T8DtTimeUnit(T8DefinitionManager context)
    {
    }

    public T8DtTimeUnit(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8TimeUnit.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof T8Minute)
            {
                return new T8DtMinute().serialize(object);
            }
            else throw new RuntimeException("Unexpected type: " + object);
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject jsonObject;
            String typeId;

            jsonObject = jsonValue.asObject();
            typeId = jsonObject.getString("type");
            if (typeId != null)
            {
                switch (typeId)
                {
                    case T8DtMinute.IDENTIFIER:
                        return new T8DtMinute().deserialize(jsonValue);
                    case T8DtHour.IDENTIFIER:
                        return new T8DtHour().deserialize(jsonValue);
                    default:
                        throw new IllegalArgumentException("Unrecognized T8TimeUnit type: " + typeId);
                }
            }
            else throw new RuntimeException("Time unit type not found: " + jsonObject);
        }
        else return null;
    }
}
