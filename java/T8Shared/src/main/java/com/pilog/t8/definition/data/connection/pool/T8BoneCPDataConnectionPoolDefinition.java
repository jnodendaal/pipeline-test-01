package com.pilog.t8.definition.data.connection.pool;

import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8BoneCPDataConnectionPoolDefinition extends T8DataConnectionPoolDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_CONNECTION_POOL_BONECP";
    public static final String DISPLAY_NAME = "BoneCP Connection Pool";
    public static final String DESCRIPTION = "A BoneCP connection pool.";
    public enum Datum {DEBUG_UNCLOSED_CONNECTIONS};
    // -------- Definition Meta-Data -------- //

    public T8BoneCPDataConnectionPoolDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DEBUG_UNCLOSED_CONNECTIONS.toString(), "Debug Unclosed Connections (Decreases Performance)", "When BoneCP detects unclosed connections, the details surrounding this will be output when set to true.", false));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataConnectionPool createNewConnectionPoolInstance(T8DataTransaction dataAccessProvider) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.data.connection.pool.T8BoneCPDataConnectionPool", new Class<?>[]{this.getClass(), T8DataTransaction.class}, this, dataAccessProvider);
    }

    public boolean isDebugUnclosedConnections()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.DEBUG_UNCLOSED_CONNECTIONS));
    }

    public void setDebugUnclosedConnections(boolean debugUnclosedConnections)
    {
        setDefinitionDatum(Datum.DEBUG_UNCLOSED_CONNECTIONS, debugUnclosedConnections);
    }
}
