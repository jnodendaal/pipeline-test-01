package com.pilog.t8.performance;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultPerformanceEventStatistics implements T8PerformanceEventStatistics, Serializable
{
    private final String identifier; // The identifier of the event.
    private long occurrenceCount; // The number of times this execution has been performed.

    public T8DefaultPerformanceEventStatistics(String identifier)
    {
        this.identifier = identifier;
        this.occurrenceCount = 0;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public void reset()
    {
        this.occurrenceCount = 0;
    }

    @Override
    public void logEvent()
    {
        // Incement the number of occurrences.
        occurrenceCount++;
    }

    @Override
    public void printStatistics(OutputStream stream)
    {
        PrintWriter writer;

        writer = new PrintWriter(stream);
        writer.println("\t" + identifier);
        writer.println("\t\tOccurrences:           " + occurrenceCount);
        writer.flush();
    }
}
