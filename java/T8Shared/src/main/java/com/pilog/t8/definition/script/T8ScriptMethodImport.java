package com.pilog.t8.definition.script;

import java.lang.reflect.Method;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptMethodImport
{
    private final String procedureName;
    private final Object contextObject;
    private final Method method;
    
    public T8ScriptMethodImport(String procedureName, Object contextObject, Method method)
    {
        this.procedureName = procedureName;
        this.contextObject = contextObject;
        this.method = method;
    }

    public Object getContextObject()
    {
        return contextObject;
    }

    public Method getMethod()
    {
        return method;
    }

    public String getProcedureName()
    {
        return procedureName;
    }
}
