package com.pilog.t8.ui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.border.Border;

public interface T8CellRenderableComponent extends T8Component
{
    public Font getCellFont();
    public Color getSelectedCellBackgroundColor();
    public Color getSelectedCellForegroundColor();
    public Color getUnselectedCellBackgroundColor();
    public Color getUnselectedCellForegroundColor();
    public Border getSelectedBorder();
    public Border getUnselectedBorder();
    public Border getFocusedBorder();
    public Border getFocusedSelectedBorder();
}
