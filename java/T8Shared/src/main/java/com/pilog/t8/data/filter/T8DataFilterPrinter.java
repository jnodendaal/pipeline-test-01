package com.pilog.t8.data.filter;

import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilterPrinter
{
    private PrintWriter writer;

    public T8DataFilterPrinter(OutputStream stream)
    {
        writer = new PrintWriter(stream);
    }

    public static void printStructure(OutputStream outputStream, T8DataFilter dataFilter)
    {
        T8DataFilterPrinter printer;

        printer = new T8DataFilterPrinter(outputStream);
        printer.printStructure(dataFilter, 0);
        printer.flush();
    }

    public static void printStructure(OutputStream outputStream, T8DataFilterCriteria filterCriteria)
    {
        T8DataFilterPrinter printer;

        printer = new T8DataFilterPrinter(outputStream);
        printer.printStructure(filterCriteria, 0);
        printer.flush();
    }

    public void flush()
    {
        writer.flush();
    }

    public void printStructure(T8DataFilter dataFilter, int indent)
    {
        T8DataFilterCriteria filterCriteria;

        writer.println(createString("Data Filter: " + dataFilter.getIdentifier() + ":" + dataFilter.getEntityIdentifier(), indent));
        writer.println(createString("Field Ordering: " + dataFilter.getFieldOrdering(), indent));

        filterCriteria = dataFilter.getFilterCriteria();
        if (filterCriteria != null)
        {
            printStructure(filterCriteria, indent + 1);
        }
    }

    public void printStructure(T8DataFilterClause filterClause, int indent)
    {
        if (filterClause instanceof T8DataFilterCriteria) printStructure((T8DataFilterCriteria)filterClause, indent);
        else printStructure((T8DataFilterCriterion)filterClause, indent);
    }

    public void printStructure(T8DataFilterCriteria filterCriteria, int indent)
    {
        LinkedHashMap<T8DataFilterClause, DataFilterConjunction> clauses;

        clauses = filterCriteria.getFilterClauses();

        writer.println(createString("Filter Criteria: " + filterCriteria.getIdentifier(), indent));
        for (T8DataFilterClause filterClause : clauses.keySet())
        {
            writer.print(createString(clauses.get(filterClause) + " ", indent));
            printStructure(filterClause, indent + 1);
        }
    }

    public void printStructure(T8DataFilterCriterion filterCriterion, int indent)
    {
        DataFilterOperator operator;
        String fieldIdentifier;
        Object filterValue;

        fieldIdentifier = filterCriterion.getFieldIdentifier();
        operator = filterCriterion.getOperator();
        filterValue = filterCriterion.getFilterValue();

        if (filterValue instanceof T8SubDataFilter)
        {
            T8DataFilter subFilter;

            subFilter = ((T8SubDataFilter)filterValue).getDataFilter();
            writer.println(createString(fieldIdentifier + " " + operator + " " + ((T8SubDataFilter)filterValue).getSubFilterFieldMapping(), indent));
            printStructure(subFilter, indent + 1);
        }
        else if (filterValue instanceof T8DataFilter)
        {
            T8DataFilter subFilter;

            subFilter = (T8DataFilter)filterValue;
            writer.println(createString(fieldIdentifier + " " + operator, indent));
            printStructure(subFilter, indent + 1);
        }
        else
        {
            writer.println(createString(fieldIdentifier + " " + filterCriterion.getOperator() + " " + filterCriterion.getFilterValue(), indent));
        }
    }

    private StringBuffer createString(String inputString, int indent)
    {
        StringBuffer string;

        string = new StringBuffer();
        for (int i = 0; i < indent; i++)
        {
            string.append(" ");
        }

        string.append(inputString);
        return string;
    }
}
