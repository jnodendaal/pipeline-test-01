package com.pilog.t8.flow;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.flow.T8Flow.FlowHistoryEvent;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8FlowTask.TaskEvent;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowStateHandler
{
    public T8FlowState loadFlowState(String flowInstanceIdentifier);
    public List<T8FlowState> loadActiveFlowStates();

    public void saveFlowState(T8FlowState flowState);
    public void deleteFlowState(T8FlowState flowState);

    public void addWaitKey(T8FlowNodeStateWaitKey waitKey);
    public void deleteNodeWaitKeys(String nodeIID);
    public T8DataIterator<T8DataEntity> getWaitingFlowIterator(T8DataFilterCriteria filterCriteria);

    public void logFlowEvent(T8Context eventContext, FlowHistoryEvent event, T8Flow flow, Map<String, Object> eventParameters, String information);
    public void logTaskEvent(T8Context eventContext, TaskEvent event, T8FlowTaskState taskState, Map<String, Object> eventParameters);
}
