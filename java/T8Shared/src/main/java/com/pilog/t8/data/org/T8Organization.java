package com.pilog.t8.data.org;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8Organization implements Serializable
{
    private String id;
    private String name;
    private String typeID;
    private T8Organization parentNode;
    private final List<T8Organization> childNodes;
    private final List<String> ontologyStructureIDList;

    public T8Organization(String orgID, String name, String orgTypeID)
    {
        this.id = orgID;
        this.name = name;
        this.typeID = orgTypeID;
        this.parentNode = null;
        this.childNodes = new ArrayList<>();
        this.ontologyStructureIDList = new ArrayList<>();
    }

    /**
     * @deprecated Use {@link #getId()} instead
     */
    @Deprecated
    public String getID()
    {
        return id;
    }

    public String getId()
    {
        return id;
    }

    /**
     * @deprecated Use {@link #setId()} instead
     */
    public void setID(String id)
    {
        this.id = id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    void setParentNode(T8Organization parentNode)
    {
        this.parentNode = parentNode;
    }

    public T8Organization getParentNode()
    {
        return parentNode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTypeID()
    {
        return typeID;
    }

    public void setTypeID(String typeID)
    {
        this.typeID = typeID;
    }

    public int countDescendants()
    {
        if (childNodes != null)
        {
            int descendants;

            descendants = childNodes.size();
            for (T8Organization childNode : childNodes)
            {
                descendants += childNode.countDescendants();
            }

            return descendants;
        }
        else return 0;
    }

    /**
     * Returns the list of organization ID's from the root to this node.
     * @return The list of organization ID's from the root to this node.
     */
    public List<String> getOrganizationIDPath()
    {
        T8Organization parent;
        List<String> idList;

        idList = new ArrayList<String>();
        idList.add(getID());

        parent = this;
        while ((parent = parent.getParentNode()) != null)
        {
            idList.add(parent.getID());
        }

        Collections.reverse(idList);
        return idList;
    }

    public List<T8Organization> getChildNodes()
    {
        ArrayList<T8Organization> nodes;

        nodes = new ArrayList<T8Organization>();
        if (childNodes != null)
        {
            nodes.addAll(childNodes);
        }

        return nodes;
    }

    public List<T8Organization> getBroodOrganizations()
    {
        List<T8Organization> broodOrganizations;
        T8Organization parentOrganization;

        broodOrganizations = new ArrayList<T8Organization>();
        parentOrganization = getParentNode();
        if (parentOrganization != null)
        {
            for (T8Organization siblingOrganization : parentOrganization.getChildNodes())
            {
                broodOrganizations.add(siblingOrganization);
            }
        }

        return broodOrganizations;
    }

    public List<T8Organization> getHouseOrganizations()
    {
        List<T8Organization> houseOrganizations;
        T8Organization parentOrganization;

        houseOrganizations = new ArrayList<T8Organization>();
        parentOrganization = getParentNode();
        if (parentOrganization != null)
        {
            houseOrganizations.add(parentOrganization);
            for (T8Organization siblingOrganization : parentOrganization.getChildNodes())
            {
                houseOrganizations.add(siblingOrganization);
            }
        }

        return houseOrganizations;
    }

    public List<T8Organization> getSiblingOrganizations()
    {
        List<T8Organization> siblingOrganizations;
        T8Organization parentOrganization;

        siblingOrganizations = new ArrayList<T8Organization>();
        parentOrganization = getParentNode();
        if (parentOrganization != null)
        {
            for (T8Organization siblingOrganization : parentOrganization.getChildNodes())
            {
                if (siblingOrganization != this)
                {
                    siblingOrganizations.add(siblingOrganization);
                }
            }
        }

        return siblingOrganizations;
    }

    public List<T8Organization> getAncestorOrganizations()
    {
        List<T8Organization> organizations;
        T8Organization node;

        organizations = new ArrayList<T8Organization>();
        node = this;
        while ((node = node.getParentNode()) != null)
        {
            organizations.add(node);
        }

        Collections.reverse(organizations);
        return organizations;
    }

    public List<T8Organization> getPathNodes()
    {
        List<T8Organization> pathNodes;
        T8Organization node;

        pathNodes = new ArrayList<T8Organization>();
        pathNodes.add(this);
        node = this;
        while ((node = node.getParentNode()) != null)
        {
            pathNodes.add(node);
        }

        Collections.reverse(pathNodes);
        return pathNodes;
    }

    public List<T8Organization> getDescendentNodes()
    {
        LinkedList<T8Organization> nodeQueue;
        List<T8Organization> descendants;

        descendants = new ArrayList<T8Organization>();
        nodeQueue = new LinkedList<T8Organization>();
        nodeQueue.add(this);
        while(nodeQueue.size() > 0)
        {
            T8Organization nextNode;
            List<T8Organization> childNodesList;

            nextNode = nodeQueue.pop();
            childNodesList = nextNode.getChildNodes();

            descendants.addAll(childNodesList);
            nodeQueue.addAll(childNodesList);
        }

        return descendants;
    }

    public T8Organization findDescendantByID(String id)
    {
        if (this.id.equals(id)) return this;
        else
        {
            for (T8Organization child : childNodes)
            {
                T8Organization descendant;

                descendant = child.findDescendantByID(id);
                if (descendant != null) return descendant;
            }

            return null;
        }
    }

    public void addChildNode(T8Organization node)
    {
        node.setParentNode(this);
        childNodes.add(node);
    }

    public void addChildNodes(Collection<T8Organization> nodes)
    {
        for (T8Organization node : nodes)
        {
            addChildNode(node);
        }
    }

    public boolean removeChildNode(T8Organization node)
    {
        boolean deleted;

        deleted = childNodes.remove(node);
        if (deleted) node.setParentNode(null);
        return deleted;
    }

    public List<String> getOntologyStructureIDList()
    {
        return new ArrayList<String>(ontologyStructureIDList);
    }

    public void setOntologyStructureIDList(List<String> idList)
    {
        ontologyStructureIDList.clear();
        if (idList != null)
        {
            ontologyStructureIDList.addAll(idList);
        }
    }

    public void addOntologyStructureID(String ontologyStructureID)
    {
        ontologyStructureIDList.add(ontologyStructureID);
    }

    void addNestedIdList(List<String> indexList)
    {
        indexList.add(id);
        for (T8Organization childNode : childNodes)
        {
            childNode.addNestedIdList(indexList);
        }
        indexList.add(id);
    }

    @Override
    public int hashCode()
    {
        return 89 * 7 + Objects.hashCode(this.id);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        return Objects.equals(this.id, ((T8Organization)obj).id);
    }
}
