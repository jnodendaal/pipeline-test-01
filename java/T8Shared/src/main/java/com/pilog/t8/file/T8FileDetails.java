package com.pilog.t8.file;

import java.io.File;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FileDetails implements Serializable
{
    protected String contextIid;
    protected String contextId;
    protected String filePath;
    protected String fileName;
    protected String mediaType;
    protected long fileSize;
    protected boolean directory;
    protected String md5Checksum;

    public T8FileDetails()
    {
    }

    public T8FileDetails(String contextIid, String contextId, String filePath, long fileSize, boolean isDirectory, String md5Checksum)
    {
        this.contextIid = contextIid;
        this.contextId = contextId;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.directory = isDirectory;
        this.md5Checksum = md5Checksum;
    }

    public T8FileDetails(String contextIid, String contextId, String filePath, long fileSize, boolean isDirectory, String md5Checksum, String mediaType)
    {
        this(contextIid, contextId, filePath, fileSize, isDirectory, md5Checksum);
        this.mediaType = mediaType;
    }

    public String getContextIid()
    {
        return contextIid;
    }

    public void setContextIid(String contextIid)
    {
        this.contextIid = contextIid;
    }

    public String getContextId()
    {
        return contextId;
    }

    public void setContextId(String contextId)
    {
        this.contextId = contextId;
    }

    public String getFileName()
    {
        if (filePath != null)
        {
            File file;

            file = new File(filePath);
            return file.getName();
        }
        else return filePath;
    }

    public String getMediaType()
    {
        return mediaType;
    }

    public void setMediaType(String mediaType)
    {
        this.mediaType = mediaType;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public long getFileSize()
    {
        return fileSize;
    }

    public void setFileSize(long fileSize)
    {
        this.fileSize = fileSize;
    }

    public boolean isDirectory()
    {
        return directory;
    }

    public void setDirectory(boolean directory)
    {
        this.directory = directory;
    }

    public String getMD5Checksum()
    {
        return md5Checksum;
    }

    public void setMD5Checksum(String md5Checksum)
    {
        this.md5Checksum = md5Checksum;
    }

    public T8FileDetails copy()
    {
        T8FileDetails copy;

        copy = new T8FileDetails();
        copy.setDirectory(directory);
        copy.setContextId(contextId);
        copy.setContextIid(contextIid);
        copy.setMediaType(mediaType);
        copy.setFilePath(filePath);
        copy.setFileSize(fileSize);
        copy.setMD5Checksum(md5Checksum);
        return copy;
    }

    @Override
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();
        buffer.append("T8FileDetails[");
        buffer.append("fileContextInstanceID=");
        buffer.append(contextIid);
        buffer.append(", fileContextID=");
        buffer.append(contextId);
        buffer.append(", filePath=");
        buffer.append(filePath);
        buffer.append(", md5Checksum=");
        buffer.append(md5Checksum);
        buffer.append("]");

        return buffer.toString();
    }
}
