package com.pilog.t8.system.event;

/**
 * @author Bouwer du Preez
 */
public class T8OperationCompletedEvent extends T8ServerEvent
{
    private final String operationInstanceIdentifier;
    
    public T8OperationCompletedEvent(String operationInstanceIdentifier)
    {
        super(operationInstanceIdentifier); // The source of the event is currently the identifier of the session but this may change in future.
        this.operationInstanceIdentifier = operationInstanceIdentifier;
    }

    public String getOperationInstanceIdentifier()
    {
        return operationInstanceIdentifier;
    }
}
