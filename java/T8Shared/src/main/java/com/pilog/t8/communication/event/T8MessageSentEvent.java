package com.pilog.t8.communication.event;

import com.pilog.t8.communication.T8CommunicationMessage;

/**
 * @author Bouwer du Preez
 */
public class T8MessageSentEvent extends T8CommunicationEvent
{
    public T8MessageSentEvent(T8CommunicationMessage message)
    {
        super(message);
    }

    public T8CommunicationMessage getMessage()
    {
        return (T8CommunicationMessage)source;
    }
}
