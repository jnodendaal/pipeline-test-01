package com.pilog.t8.definition.data.source.task;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8TaskDataSourceDefinition extends T8DataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_TASK";
    public static final String DISPLAY_NAME = "Task Data Source";
    public static final String DESCRIPTION = "A data source that provides task details for the current session.";
    public enum Datum {TASK_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8TaskDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_IDENTIFIER.toString(), "Connection", "The connection to which this data source will connect to retrieve its data."));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TASK_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction dataAccessProvider) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.source.table.T8TableDataSource").getConstructor(T8TableDataSourceDefinition.class, T8DataTransaction.class);
        return (T8DataSource)constructor.newInstance(this, dataAccessProvider);
    }

    public String getTaskIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_IDENTIFIER.toString());
    }

    public void setTaskIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.TASK_IDENTIFIER.toString(), connectionIdentifier);
    }
}
