package com.pilog.t8.data.object;

import com.pilog.t8.data.object.filter.T8ObjectFilter;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DataObjectHandler
{
    /**
     * Returns the id of the data objects managed by this handler.
     * @return The id of the data objects managed by this handler.
     */
    public String getDataObjectId();

    /**
     * Retrieves the specified data object from the database.
     * @param <O>
     * @param objectIid The iid of the object to retrieve.
     * @return The retrieved data object or null if not found.
     * @throws Exception
     */
    public <O extends T8DataObject> O retrieve(String objectIid) throws Exception;

    /**
     * Searches for data objects matching the supplied search expression.
     * @param <O>
     * @param filter The filter to apply to the search results in conjunction with the search expression.
     * @param searchExpression The search expression to use.
     * @param pageOffset The offset of the page of results to return, starting from 0.
     * @param pageSize The number of results to return, starting from the specified page offset.
     * @return The list of results, matching the search expression.
     * @throws Exception
     */
    public <O extends T8DataObject> List<O> search(T8ObjectFilter filter, String searchExpression, int pageOffset, int pageSize) throws Exception;
}
