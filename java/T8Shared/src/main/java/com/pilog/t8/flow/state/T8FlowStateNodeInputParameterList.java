package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateNodeInputParameterList extends T8FlowStateNodeParameterList
{
    private final String inputNodeID;

    public T8FlowStateNodeInputParameterList(T8FlowNodeState parentNodeState, T8FlowStateParameterCollection parentCollection, String inputNodeID, String parameterIID, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        super(parentNodeState, parentCollection, parameterIID, entityIdentifier, null);
        this.inputNodeID = inputNodeID;
        if (dataSet != null) addParameters(dataSet);
    }

    public String getInputNodeIdentifier()
    {
        return inputNodeID;
    }

    @Override
    public synchronized T8DataEntity createEntity(int index, T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;

        newEntity = super.createEntity(index, entityDefinition);
        newEntity.setFieldValue(entityIdentifier + "$INPUT_NODE_ID", inputNodeID);
        return newEntity;
    }

    @Override
    public synchronized T8FlowStateParameterMap createNewParameterMap(T8FlowStateParameterCollection newMapParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        return new T8FlowStateNodeInputParameterMap(parentNodeState, newMapParentCollection, inputNodeID, parameterIID, entityIdentifier, dataSet);
    }

    @Override
    public synchronized T8FlowStateParameterList createNewParameterList(T8FlowStateParameterCollection newListParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        return new T8FlowStateNodeInputParameterList(parentNodeState, newListParentCollection, inputNodeID, parameterIID, entityIdentifier, dataSet);
    }
}
