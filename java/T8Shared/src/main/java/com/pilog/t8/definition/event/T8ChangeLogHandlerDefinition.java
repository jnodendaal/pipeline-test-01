package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ChangeLogHandlerDefinition extends T8DefinitionEventHandlerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_EVENT_HANDLER_CHANGE_LOG";
    public static final String DISPLAY_NAME = "Change Log Handler";
    public static final String DESCRIPTION = "A definition that defines a handler that will log definition changes to a persistence store.";
    public enum Datum {DATA_ENTITY_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8ChangeLogHandlerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier", "The data entity to which the definition history will be persisted."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionEventHandler getNewEventHandlerInstance(T8Context context) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.definition.event.T8ChangeLogHandler", new Class<?>[]{T8Context.class, T8ChangeLogHandlerDefinition.class}, context, this);
    }

    public String getDataEntityIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER);
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER, identifier);
    }
}

