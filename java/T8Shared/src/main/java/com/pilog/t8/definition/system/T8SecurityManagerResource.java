package com.pilog.t8.definition.system;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtPassword;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.datatype.T8DtSessionDetails;
import com.pilog.t8.datatype.T8DtUserDetails;
import com.pilog.t8.datatype.T8DtUserProfileDetails;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8SecurityManagerResource implements T8DefinitionResource
{
    public static final String OPERATION_LOGIN = "@OP_SEC_LOGIN";
    public static final String OPERATION_LOGOUT = "@OP_SEC_LOGOUT";
    public static final String OPERATION_SWITCH_USER_PROFILE = "@OP_SEC_SWITCH_USER_PROFILE";
    public static final String OPERATION_GET_ACCESSIBLE_USER_PROFILE_DETAILS = "@OP_SEC_GET_ACCESSIBLE_USER_PROFILE_DETAILS";
    public static final String OPERATION_CHANGE_PASSWORD = "@OP_SEC_CHANGE_USER_PASSWORD";
    public static final String OPERATION_GET_ALL_SESSION_DETAILS = "@OP_SEC_GET_ALL_SESSION_DETAILS";
    public static final String OPERATION_GET_SESSION_DETAILS = "@OP_SEC_GET_SESSION_DETAILS";
    public static final String OPERATION_IS_SESSION_ACTIVE = "@OP_SEC_IS_SESSION_ACTIVE";
    public static final String OPERATION_GET_USER_DETAILS = "@OP_SEC_GET_USER_DETAILS";
    public static final String OPERATION_GET_ACCESSIBLE_USER_PROFILE_IDS = "@OP_SEC_GET_ACCESSIBLE_USER_PROFILE_IDS";
    public static final String OPERATION_GET_ACCESSIBLE_USER_PROFILE_META_DATA = "@OP_SEC_GET_ACCESSIBLE_USER_PROFILE_META_DATA";
    public static final String OPERATION_CREATE_NEW_USER = "@OP_SEC_CREATE_NEW_USER";
    public static final String OPERATION_UPDATE_USER_DETAILS = "@OP_SEC_UPDATE_USER_DETAILS";
    public static final String OPERATION_SET_USER_ACTIVE = "@OP_SEC_SET_USER_ACTIVE";
    public static final String OPERATION_DELETE_USER = "@OP_SEC_DELETE_USER";
    public static final String OPERATION_RESET_USER_PASSWORD = "@OP_SEC_RESET_USER_PASSWORD";
    public static final String OPERATION_GENERATE_WEB_SERVICE_USER_API_KEY = "@OP_SEC_GENERATE_WEB_SERVICE_USER_API_KEY";
    public static final String OPERATION_GET_WEB_SERVICE_USER_API_KEY = "@OP_SEC_GET_WEB_SERVICE_USER_API_KEY";
    public static final String OPERATION_LOCK_USER = "@OP_SEC_LOCK_USER";
    public static final String OPERATION_UNLOCK_USER = "@OP_SEC_UNLOCK_USER";
    public static final String OPERATION_SWITCH_SESSION_USER_LANUAGE = "@OP_SEC_SWITCH_SESSION_USER_LANUAGE";
    public static final String OPERATION_EXTEND_USER_SESSION = "@OP_SEC_EXTEND_USER_SESSION";
    public static final String OPERATION_SET_SESSION_PERFORMANCE_STATISTICS_ENABLED = "@OP_SEC_SET_SESSION_PERFORMANCE_STATISTICS_ENABLED";
    public static final String OPERATION_GET_SESSION_PERFORMANCE_STATISTICS = "@OP_SEC_GET_SESSION_PERFORMANCE_STATISTICS";
    public static final String OPERATION_GET_SSO_HANDLER_DEFINITION = "@OP_SEC_GET_SSO_HANDLER_DEFINITION";

    public static final String PARAMETER_USER_DETAILS = "$P_USER_DETAILS";
    public static final String PARAMETER_USER_EXPIRY_DATE = "$P_USER_EXPIRY_DATE";
    public static final String PARAMETER_USERNAME = "$P_USERNAME";
    public static final String PARAMETER_NAME = "$P_NAME";
    public static final String PARAMETER_SURNAME = "$P_SURNAME";
    public static final String PARAMETER_EMAIL_ADDRESS = "$P_EMAIL_ADDRESS";
    public static final String PARAMETER_MOBILE_NUMBER = "$P_MOBILE_NUMBER";
    public static final String PARAMETER_WORK_TELEPHONE_NUMBER = "$P_WORK_TELEPHONE_NUMBER";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_ORGANIZATION_ID = "$P_ORGANIZATION_ID";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_RESPONSE_CODE = "$P_RESPONSE_CODE";
    public static final String PARAMETER_AVAILABLE = "$P_AVAILABLE";
    public static final String PARAMETER_MESSAGE = "$P_MESSAGE";
    public static final String PARAMETER_PASSWORD = "$P_PASSWORD";
    public static final String PARAMETER_USER_ID = "$P_USER_ID";
    public static final String PARAMETER_SESSION_CONTEXT = "$P_SESSION_CONTEXT";
    public static final String PARAMETER_SESSION_ID = "$P_SESSION_ID";
    public static final String PARAMETER_SESSION_DETAILS = "$P_SESSION_DETAILS";
    public static final String PARAMETER_SESSION_DETAILS_LIST = "$P_SESSION_DETAILS_LIST";
    public static final String PARAMETER_USER_PROFILE_ID = "$P_USER_PROFILE_ID";
    public static final String PARAMETER_USER_PROFILE_IDS = "$P_USER_PROFILE_IDS";
    public static final String PARAMETER_WORKFLOW_PROFILE_IDS = "$P_WORKFLOW_PROFILE_IDS";
    public static final String PARAMETER_USER_PROFILE_META_DATA = "$P_USER_PROFILE_META_DATA";
    public static final String PARAMETER_USER_PROFILE_DETAILS = "$P_USER_PROFILE_DETAILS";
    public static final String PARAMETER_USER_UPDATE_RESPONSE = "$P_USER_UPDATE_RESPONSE";
    public static final String PARAMETER_ACTIVE = "$P_ACTIVE";
    public static final String PARAMETER_API_KEY = "$P_API_KEY";
    public static final String PARAMETER_LOCK_REASON = "$P_LOCK_REASON";
    public static final String PARAMETER_END_EXISTING_SESSION = "$P_END_EXISTING_SESSION";
    public static final String PARAMETER_ENABLED = "$P_ENABLED";
    public static final String PARAMETER_PERFORMANCE_STATISTICS = "$P_PERFORMANCE_STATISTICS";
    public static final String PARAMETER_DEFINITION = "$P_DEFINITION";

    public static final String USER_HISTORY_DS_ID = "@DS_HISTORY_USER";
    public static final String USER_HISTORY_DE_ID = "@E_HISTORY_USER";
    public static final String USER_HISTORY_PAR_DS_ID = "@DS_HISTORY_USER_PAR";
    public static final String USER_HISTORY_PAR_DE_ID = "@E_HISTORY_USER_PAR";

    // Table Names.
    public static final String USER_HISTORY_TABLE_NAME = "HIS_USR";
    public static final String USER_HISTORY_PAR_TABLE_NAME = "HIS_USR_PAR";

    // Events used when logging user history.
    public enum UserHistoryEvent
    {
        LOG_IN,
        LOG_OUT,
        PROFILE_SWITCH,
        PASSWORD_CHANGE_REQUESTED,
        PASSWORD_CHANGED,
        LOG_IN_API,
        LOG_IN_SSO
    };

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIds)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIds.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIds.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIds.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Login History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(USER_HISTORY_DS_ID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(USER_HISTORY_TABLE_NAME);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SESSION_IDENTIFIER", "SESSION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$USER_IDENTIFIER", "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$USER_PROFILE_IDENTIFIER", "USER_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT", "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.DATE_TIME));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Login History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(USER_HISTORY_PAR_DS_ID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(USER_HISTORY_PAR_TABLE_NAME);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARAMETER_ID", "PARAMETER_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$IDENTIFIER", "IDENTIFIER", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Login History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(USER_HISTORY_DE_ID);
        entityDefinition.setDataSourceIdentifier(USER_HISTORY_DS_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", USER_HISTORY_DS_ID + "$EVENT_INSTANCE_IDENTIFIER", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SESSION_IDENTIFIER", USER_HISTORY_DS_ID + "$SESSION_IDENTIFIER", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$USER_IDENTIFIER", USER_HISTORY_DS_ID + "$USER_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$USER_PROFILE_IDENTIFIER", USER_HISTORY_DS_ID + "$USER_PROFILE_IDENTIFIER", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT", USER_HISTORY_DS_ID + "$EVENT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME", USER_HISTORY_DS_ID + "$TIME", false, false, T8DataType.DATE_TIME));
        definitions.add(entityDefinition);

        // Login History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(USER_HISTORY_PAR_DE_ID);
        entityDefinition.setDataSourceIdentifier(USER_HISTORY_PAR_DS_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_INSTANCE_IDENTIFIER", USER_HISTORY_PAR_DS_ID + "$EVENT_INSTANCE_IDENTIFIER", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARAMETER_ID", USER_HISTORY_PAR_DS_ID + "$PARAMETER_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$IDENTIFIER", USER_HISTORY_PAR_DS_ID + "$IDENTIFIER", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", USER_HISTORY_PAR_DS_ID + "$VALUE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_LOGIN);
        definition.setMetaDisplayName("Login");
        definition.setMetaDescription("Logs a user into the system, creating a new active session.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$LoginOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USERNAME, "Username", "The uique username identifying the user to be logged in.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PASSWORD, "Password", "The password used for log-in authentication.", T8DtPassword.IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_END_EXISTING_SESSION, "End Existing Session", "A boolean flag to indicate whether or not any existing session for this user should be ended.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RESPONSE_CODE, "Login Response Type", "A code indicating the server response to the login request.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE, "Login Response Message", "A message that describes the code returned as response type and suppies additional information if available.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_API_KEY, "API Key", "The API Key unique to the user for which the login was successful.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_DETAILS, "Session Details", "The details of the logged in session (only available after successful login).", new T8DtSessionDetails()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_LOGOUT);
        definition.setMetaDisplayName("Logout");
        definition.setMetaDescription("Logs the current user out of the system, ending the associated active session.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$LogoutOperation");
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "If result of the logout request.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_ALL_SESSION_DETAILS);
        definition.setMetaDisplayName("Get Session Details");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetSessionDetailsOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_IS_SESSION_ACTIVE);
        definition.setMetaDisplayName("Is Session Active");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$IsSessionActiveOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_ID, "Session Identifier", "The session identifier of the session to check.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ACTIVE, "Active", "If the session is active or not.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CHANGE_PASSWORD);
        definition.setMetaDisplayName("Change Password");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$ChangePasswordOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PASSWORD, "New Password", "The String to change the current password to.", T8DtPassword.IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Operation Success", "The result of the password change.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE, "Failure Message", "The failure message (only applicable if the operation failed).", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_ACCESSIBLE_USER_PROFILE_IDS);
        definition.setMetaDisplayName("Get Accessible User Profile Identifiers");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetAccessibleUserProfileIdentifiersOperation");
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_PROFILE_IDS, "User Profile Identifier List", "The list of accessible user profile identifier for the current session.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_ACCESSIBLE_USER_PROFILE_DETAILS);
        definition.setMetaDisplayName("Get Accessible User Profile Details");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$ApiGetAccessibleUserProfileDetails");
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_PROFILE_DETAILS, "User Profile Details List", "The list of details of the accessible user profiles for the current session.", new T8DtList(new T8DtUserProfileDetails())));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_ACCESSIBLE_USER_PROFILE_META_DATA);
        definition.setMetaDisplayName("Get Accessible User Profile Meta Data");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetAccessibleUserProfileMetaDataOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_USER_DETAILS);
        definition.setMetaDisplayName("Get User Details");
        definition.setMetaDescription("Get User Details.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetUserDetailsOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The user identifier/username for which to fetch details. Uses the user identifier from the session if not specified.", T8DataType.DEFINITION_IDENTIFIER, true));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_DETAILS, "User Details", "The details of the specified user.", new T8DtUserDetails()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CREATE_NEW_USER);
        definition.setMetaDisplayName("Create New User");
        definition.setMetaDescription("Creates a new user in the system.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$CreateNewUserOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_DETAILS, "User Details", "The user details object can be sent instead of the individual parameters.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USERNAME, "Username", "The username that will be used by the user for login purposes.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NAME, "Name", "The name of the user.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SURNAME, "Surname", "The surname of the user.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_EMAIL_ADDRESS, "Email Address", "The user's email address that will be used whenever notifications or administrative messages need to be sent.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MOBILE_NUMBER, "Mobile Number", "The user's mobile number that will be used whenever notifications or administrative messages need to be sent.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WORK_TELEPHONE_NUMBER, "Work Telephone Number", "The user's work telephone number that will be used whenever notifications or administrative messages need to be sent.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_LANGUAGE_ID, "Language Identifier", "The identifier of the user's preferred content language.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ORGANIZATION_ID, "Organization Identifier", "The identifier of the organization to which the user belongs.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_PROFILE_IDS, "Profile Identifier List", "The identifiers of the user profiles to which the user is linked.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WORKFLOW_PROFILE_IDS, "Work Flow Profile Identifier List", "The identifiers of the user Work Flow profiles to which the user is linked.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_EXPIRY_DATE, "User Expiry Date", "The date when this user's access to the system will expire.", T8DataType.DATE_TIME));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The identifier of the newly created user (null on creation failure).", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Operation Success", "The result of the user update request.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE, "Failure Message", "The failure message (only applicable if the operation failed).", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_UPDATE_USER_DETAILS);
        definition.setMetaDisplayName("Update User Details");
        definition.setMetaDescription("Updates the specified user's details.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$UpdateUserDetailsOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_DETAILS, "User Details", "The user details object can be sent instead of the individual parameters.", new T8DtUserDetails()));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The unique identifier of the user to update.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USERNAME, "Username", "The username that will be used by the user for login purposes.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NAME, "Name", "The name of the user.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SURNAME, "Surname", "The surname of the user.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_EMAIL_ADDRESS, "Email Address", "The user's email address that will be used whenever notifications or administrative messages need to be sent.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MOBILE_NUMBER, "Mobile Number", "The user's mobile number that will be used whenever notifications or administrative messages need to be sent.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WORK_TELEPHONE_NUMBER, "Work Telephone Number", "The user's work telephone number that will be used whenever notifications or administrative messages need to be sent.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_LANGUAGE_ID, "Language Identifier", "The identifier of the user's preferred content language.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ORGANIZATION_ID, "Organization Identifier", "The identifier of the organization to which the user belongs.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_PROFILE_IDS, "Profile Identifier List", "The identifiers of the user profiles to which the user is linked.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WORKFLOW_PROFILE_IDS, "Work Flow Profile Identifier List", "The identifiers of the user Work Flow profiles to which the user is linked.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_EXPIRY_DATE, "User Expiry Date", "The date when this user's access to the system will expire.", T8DataType.DATE_TIME));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Operation Success", "The result of the user update request.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE, "Failure Message", "The failure message (only applicable if the operation failed).", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_DETAILS, "Session Details", "The updated session details if the updated user is the currently logged-in user.", new T8DtSessionDetails()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_SET_USER_ACTIVE);
        definition.setMetaDisplayName("Set User Active");
        definition.setMetaDescription("Updates the user active indicator.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$SetUserActiveOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The identifier of the user to update.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ACTIVE, "Active", "If the user should be set active/inactive", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_USER);
        definition.setMetaDisplayName("Delete User");
        definition.setMetaDescription("Deletes the specified user from the system.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$DeleteUserOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The unique identifier of the user to delete.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Operation Success", "The result of the user update request.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MESSAGE, "Failure Message", "The failure message (only applicable if the operation failed).", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RESET_USER_PASSWORD);
        definition.setMetaDisplayName("Reset User Password");
        definition.setMetaDescription("Resets the specified user's password so that the user will have to enter a new password on next login.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$ResetUserPasswordOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The unique identifier of the user to update.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Operation Success", "The result of the user password reset request.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GENERATE_WEB_SERVICE_USER_API_KEY);
        definition.setMetaDisplayName("Generate Web Service User API Key");
        definition.setMetaDescription("Generates a new API key for the web service user.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GenerateWebServiceApiKeyOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_WEB_SERVICE_USER_API_KEY);
        definition.setMetaDisplayName("Get Web Service User API Key");
        definition.setMetaDescription("Gets the web service API key for the user.");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetWebServiceApiKeyOperation");
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_API_KEY, "API Key", "The API Key of the user.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_LOCK_USER);
        definition.setMetaDisplayName("Lock User");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$LockUser");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The user identifier of the user to lock.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_LOCK_REASON, "Lock Reason", "The reason why the user is locked.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_UNLOCK_USER);
        definition.setMetaDisplayName("Unlock User");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$UnlockUser");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The user identifier of the user to lock.", T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_SWITCH_SESSION_USER_LANUAGE);
        definition.setMetaDisplayName("Switch Session Language");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$SwitchSessionLanguage");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_LANGUAGE_ID, "Language Identifier", "The langauge identifier that will be switched to.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_CONTEXT, "Authentic Session Context", "The new authentic session context.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_SWITCH_USER_PROFILE);
        definition.setMetaDisplayName("Switch User Profile");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$ApiSwitchUserProfile");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_PROFILE_ID, "User Profile id", "The id of the user profile that will be switched to.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "A boolean result indicating whether or not the profile switch was successful.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_DETAILS, "Session Details", "The details of the updated session (only available after successful switch).", new T8DtSessionDetails()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_EXTEND_USER_SESSION);
        definition.setMetaDisplayName("Extend User Session");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$ExtendUserSession");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_SET_SESSION_PERFORMANCE_STATISTICS_ENABLED);
        definition.setMetaDisplayName("Set User Session Performance Tracking Enabled");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$SetSessionPerformanceStatisticsEnabled");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_ID, "Session id", "The id of the session on which to set the performance tracking flag.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ENABLED, "Enabled", "The Boolean flag to indicate the state of performance tracking to set.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_SESSION_PERFORMANCE_STATISTICS);
        definition.setMetaDisplayName("Get Session Performance Statistics");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetSessionPerformanceStatistics");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SESSION_ID, "Session id", "The id of the session for which to fetch performance statistics.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PERFORMANCE_STATISTICS, "Performance Statistics", "The performance statistics of the specified session.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_SSO_HANDLER_DEFINITION);
        definition.setMetaDisplayName("Get SSO Handler Definition");
        definition.setClassName("com.pilog.t8.security.T8SecurityManagerOperations$GetSsoHandlerDefinition");
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DEFINITION, "SSO Handler Definition", "The definition of the SSO handler configured for the system.", T8DataType.DEFINITION));
        definitions.add(definition);

        return definitions;
    }
}
