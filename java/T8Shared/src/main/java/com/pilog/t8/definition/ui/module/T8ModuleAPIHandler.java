package com.pilog.t8.definition.ui.module;

import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This class differs from other API handlers in that it does not store pre-
 * constructed event and operation definitions in a static list.  The reason for
 * this is that the operations and events of a module can be user defined as
 * opposed to other components for which events/operations are pre-defined.  In
 * order to make the user-defined definitions work properly along with the pre-
 * defined definitions of the module, both types have to be explicitly linked to
 * the parent module definition and of coarse this cannot be done if all modules
 * share a static list of events/operations.
 *
 * @author Bouwer du Preez
 */
public class T8ModuleAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_MODULE_STARTED = "$ME_MODULE_STARTED";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions(T8ModuleDefinition parentDefinition)
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_MODULE_STARTED);
        newEventDefinition.setMetaDisplayName("Module Started");
        newEventDefinition.setMetaDescription("This event occurs when all module components have been initialized and started.");
        T8DefinitionUtilities.assignDefinitionParent(parentDefinition, newEventDefinition); // This is the important part where module events differ from other component events definitions.
        events.add(newEventDefinition);
        
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions(T8ModuleDefinition parentDefinition)
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        
        operations = new ArrayList<T8ComponentOperationDefinition>();
        return operations;
    }
}
