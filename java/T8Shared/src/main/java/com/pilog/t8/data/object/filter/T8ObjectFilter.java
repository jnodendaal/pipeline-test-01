package com.pilog.t8.data.object.filter;

import com.pilog.epic.annotation.EPICMethod;
import com.pilog.epic.annotation.EPICParameter;
import com.pilog.t8.data.filter.T8DataFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.object.filter.T8ObjectFilterClause.ObjectFilterConjunction;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriterion.ObjectFilterOperator;

/**
 * @author Bouwer du Preez
 */
public class T8ObjectFilter implements Serializable
{
    private String filterId; // Only applicable if the filter does not reference its definition.
    private T8ObjectFilterCriteria filterCriteria; // Must never be null.
    private final LinkedHashMap<String, OrderMethod> fieldOrdering;
    private final List<String> fieldGrouping;
    private final List<String> fieldIds; // Identifiers of fields to include (null indicates that all fields must be included).

    public enum OrderMethod
    {
        ASCENDING,
        DESCENDING;

        public T8DataFilter.OrderMethod getDataFilterOrderMethod()
        {
            return this == OrderMethod.ASCENDING ? T8DataFilter.OrderMethod.ASCENDING : T8DataFilter.OrderMethod.DESCENDING;
        }
    };

    public T8ObjectFilter()
    {
        this.filterCriteria = new T8ObjectFilterCriteria();
        this.fieldOrdering = new LinkedHashMap<>();
        this.fieldGrouping = new ArrayList<>();
        this.fieldIds = new ArrayList<>();
    }

    public T8ObjectFilter(Map<String, Object> keyMap)
    {
        this();
        filterCriteria.addKeyFilter(keyMap, false);
    }

    public T8ObjectFilter(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        this();
        filterCriteria.addKeyFilter(keyMap, caseInsensitive);
    }

    public T8ObjectFilter(T8ObjectFilterCriteria filterCriteria)
    {
        this();
        setFilterCriteria(filterCriteria);
    }

    public T8ObjectFilter(List<Map<String, Object>> keyList)
    {
        this();
        for (Map<String, Object> key : keyList)
        {
            filterCriteria.addFilterClause(T8ObjectFilterClause.ObjectFilterConjunction.OR, new T8ObjectFilterCriteria(key, false));
        }
    }

    public T8ObjectFilter copy()
    {
        T8ObjectFilter copiedFilter;

        copiedFilter = new T8ObjectFilter();
        copiedFilter.setFieldOrdering(new LinkedHashMap<>(fieldOrdering));
        copiedFilter.setFilterCriteria(filterCriteria.copy());
        copiedFilter.setFieldGrouping(new ArrayList<>(fieldGrouping));

        return copiedFilter;
    }

    public String getIdentifier()
    {
        return filterId;
    }

    public void setIdentifier(String filterId)
    {
        this.filterId = filterId;
    }

    public boolean hasFilterCriteria()
    {
        return filterCriteria != null && filterCriteria.hasCriteria();
    }

    public T8ObjectFilterCriteria getFilterCriteria()
    {
        return filterCriteria;
    }

    public final void setFilterCriteria(T8ObjectFilterCriteria filterCriteria)
    {
        if (filterCriteria != null)
        {
            this.filterCriteria = filterCriteria;
        }
    }

    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param dataFilter The data filter from which the criteria to add to this
     * filter will be retrieved.
     * @return
     */
    @EPICMethod(Description = "Adds a data filter to this filter as a new filter criteria using the supplied conjuction.")
    public T8ObjectFilter addFilterCriteria(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "dataFilter") T8ObjectFilter dataFilter)
    {
        return addFilterCriteria(ObjectFilterConjunction.valueOf(conjunction), dataFilter);
    }

    public T8ObjectFilter addFilterCriteria(ObjectFilterConjunction conjunction, T8ObjectFilter dataFilter)
    {
        // Check that we do not add null criteria, some filters only contain order by information.
        if ((dataFilter != null) & (dataFilter.hasFilterCriteria()))
        {
            filterCriteria.addFilterClause(conjunction, dataFilter.getFilterCriteria());
        }

        return this;
    }

    /**
     * This is a convenience method for use from script.
     * @param conjunction The String representation of a valid conjunction.
     * @param filterCriteria The data filter criteria to add to this filter.
     * @return
     */
    @EPICMethod(Description = "Adds a new filter criteria to this data filter using the provided conjunction.")
    public T8ObjectFilter addFilterCriteria(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "filterCriteria") T8ObjectFilterCriteria filterCriteria)
    {
        return addFilterCriteria(ObjectFilterConjunction.valueOf(conjunction), filterCriteria);
    }

    public T8ObjectFilter addFilterCriteria(ObjectFilterConjunction conjunction, T8ObjectFilterCriteria filterCriteria)
    {
        this.filterCriteria.addFilterClause(conjunction, filterCriteria);
        return this;
    }

    /**
     * This is a convenience method for use from script.
     * @param keyMap The map of key identifier-value pairs that will be added
     * as additional filter criteria.
     * @return
     */
    @EPICMethod(Description = "Adds a new filter criteria based on the identifier-value pairs.")
    public T8ObjectFilter addFilterCriteria(@EPICParameter(VariableName = "keyMap") Map<String, Object> keyMap)
    {
        this.filterCriteria.addKeyFilter(keyMap, false);
        return this;
    }

    /**
     * This is a convenience method for use from script.
     *
     * @param conjunction The String representation of a valid conjunction
     * @param fieldId The field identifier to which the criteria are
     *      applicable
     * @param valueList The collection of values that form the criteria to be
     *      added
     *
     * @return A reference to the current {@code T8ObjectFilter}
     */
    @EPICMethod(Description = "Adds a new filter criteria for the specified field containing all of the supplied values.")
    public T8ObjectFilter addFilterCriteria(@EPICParameter(VariableName = "conjunction") String conjunction, @EPICParameter(VariableName = "fieldIdentifier") String fieldId, @EPICParameter(VariableName = "valueList") Collection<Object> valueList)
    {
        return addFilterCriteria(ObjectFilterConjunction.valueOf(conjunction), fieldId, valueList);
    }

    public T8ObjectFilter addFilterCriteria(ObjectFilterConjunction conjunction, String fieldId, Collection<? extends Object> valueList)
    {
        filterCriteria.addFilterClause(conjunction, new T8ObjectFilterCriterion(fieldId, ObjectFilterOperator.IN, valueList));
        return this;
    }

    public T8ObjectFilter addFilterCriterion(ObjectFilterConjunction conjunction, T8ObjectFilterCriterion dataFilterCriterion)
    {
        filterCriteria.addFilterClause(conjunction, dataFilterCriterion);
        return this;
    }

    public T8ObjectFilter addFilterCriterion(ObjectFilterConjunction conjunction, String fieldId, ObjectFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        filterCriteria.addFilterClause(conjunction, fieldId, operator, filterValue, caseInsensitive);
        return this;
    }

    public T8ObjectFilter addFilterCriterion(String fieldId, Object filterValue)
    {
        return this.addFilterCriterion(ObjectFilterConjunction.AND, fieldId, ObjectFilterOperator.EQUAL, filterValue, false);
    }

    public T8ObjectFilter addFilterCriterion(String fieldId, String operator, Object filterValue)
    {
        return this.addFilterCriterion(ObjectFilterConjunction.AND, fieldId, ObjectFilterOperator.valueOf(operator), filterValue, false);
    }

    public T8ObjectFilter addFilterCriterion(String fieldId, ObjectFilterOperator operator, Object filterValue)
    {
        return this.addFilterCriterion(ObjectFilterConjunction.AND, fieldId, operator, filterValue, false);
    }

    public T8ObjectFilter addFilterCriterion(String conjunction, String fieldId, String operator, Object filterValue, boolean caseInsensitive)
    {
        return this.addFilterCriterion(ObjectFilterConjunction.valueOf(conjunction), fieldId, ObjectFilterOperator.valueOf(operator), filterValue, caseInsensitive);
    }

    public T8ObjectFilterCriterion getFilterCriterion(String id)
    {
        return filterCriteria.getFilterCriterion(id);
    }

    /**
     * Returns a boolean value indicating whether or not any field ordering is
     * set on this filter.  It is possible for this method to return true
     * without any of the ordering settings taking any affect on the results
     * returned using this filter.  Successful ordering is data source
     * dependent.
     *
     * @return Boolean true if any field ordering is set on this filter.
     */
    public boolean hasFieldOrdering()
    {
        return fieldOrdering.size() > 0;
    }

    public void clearFieldOrdering()
    {
        fieldOrdering.clear();
    }

    public T8ObjectFilter addFieldOrdering(String fieldId, OrderMethod method)
    {
        fieldOrdering.remove(fieldId); // First remove the field (if it was present) so that we know the new entry will be added ad the end of the linked map.
        fieldOrdering.put(fieldId, method);
        return this;
    }

    /**
     * This is a convenience method that can be easily used from EPIC script
     * because it takes String parameter types.
     *
     * @param fieldId Identifier of the field to order.
     * @param method The method 'ASCENDING' or 'DESCENDING' to order by.
     * @return This filter object for method chaining.
     */
    public T8ObjectFilter addFieldOrdering(String fieldId, String method)
    {
        fieldOrdering.remove(fieldId); // First remove the field (if it was present) so that we know the new entry will be added ad the end of the linked map.
        fieldOrdering.put(fieldId, OrderMethod.valueOf(method));
        return this;
    }

    public OrderMethod removeFieldOrdering(String fieldId)
    {
        return fieldOrdering.remove(fieldId);
    }

    public void setFieldOrdering(String fieldIdentifier, OrderMethod method)
    {
        fieldOrdering.put(fieldIdentifier, method);
    }

    public OrderMethod getFieldOrderMethod(String fieldId)
    {
        return fieldOrdering.get(fieldId);
    }

    public LinkedHashMap<String, OrderMethod> getFieldOrdering()
    {
        return fieldOrdering;
    }

    public void setFieldOrdering(LinkedHashMap<String, OrderMethod> fieldOrdering)
    {
        this.fieldOrdering.clear();
        if (fieldOrdering != null)
        {
            this.fieldOrdering.putAll(fieldOrdering);
        }
    }

    /**
     * Returns a boolean value indicating whether or not any field grouping is
     * set on this filter.  It is possible for this method to return true
     * without any of the grouping settings taking any affect on the results
     * returned using this filter.  Successful grouping is data source
     * dependent.
     *
     * @return Boolean true if any field grouping is set on this filter.
     */
    public boolean hasFieldGrouping()
    {
        return fieldGrouping.size() > 0;
    }

    public void clearFieldGrouping()
    {
        fieldGrouping.clear();
    }

    public void addFieldGrouping(String fieldId)
    {
        fieldGrouping.remove(fieldId); // First remove the field (if it was present) so that we know the new entry will be added ad the end of the list.
        fieldGrouping.add(fieldId);
    }

    public boolean removeFieldGrouping(String fieldId)
    {
        return fieldGrouping.remove(fieldId);
    }

    public List<String> getFieldGrouping()
    {
        return fieldGrouping;
    }

    public void setFieldGrouping(List<String> fieldGrouping)
    {
        this.fieldGrouping.clear();
        if (fieldGrouping != null)
        {
            this.fieldGrouping.addAll(fieldGrouping);
        }
    }

    public Set<String> getFilterFieldIdentifiers()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<>();
        fieldSet.addAll(filterCriteria.getFilterFieldIds());
        return fieldSet;
    }

    public void setFieldIdentifiers(List<String> fieldIds)
    {
        this.fieldIds.clear();
        this.fieldIds.addAll(fieldIds);
    }

    public void addFieldIdentifier(String fieldIds)
    {
        this.fieldIds.add(fieldIds);
    }

    public void clearFieldIdentifiers()
    {
        fieldIds.clear();
    }

    public List<String> getFieldIdentifiers()
    {
        return new ArrayList<>(fieldIds);
    }

    /**
     * This method will use the field indicator to find the {@code T8ObjectFilterCriterion}'s to be removed from the
     * {@code T8ObjectFilter}.
     *
     * @param fieldId The {@code String} field identifier associated with the {@code T8DatafilterCriterion} to be removed.
     * @return Returns instance of self
     */
    public T8ObjectFilter removeFilterCriterionByField(String fieldId)
    {
        filterCriteria.removeFilterCriterionByField(fieldId);
        return this;
    }

    /**
     * Remove the {@code T8ObjectFilterClause} from the {@code T8ObjectFilter}.
     *
     * @param filterClause The {@code T8ObjectFilterClause} to be removed.
     */
    public void removeFilterClause(T8ObjectFilterClause filterClause)
    {
        filterCriteria.removeFilterClause(filterClause);
    }

    public List<T8ObjectFilterCriterion> getFilterCriterionList()
    {
        ArrayList<T8ObjectFilterCriterion> criterionList;

        criterionList = new ArrayList<>();
        criterionList.addAll(filterCriteria.getFilterCriterionList());
        return criterionList;
    }

    public T8DataFilter getDataFilter(String entityId, Map<String, String> objectToEntityFieldMap)
    {
        T8DataFilter filter;

        filter = new T8DataFilter(entityId);
        if (filterCriteria != null) filter.setFilterCriteria(filterCriteria.getDataFilterCriteria(entityId, objectToEntityFieldMap));
        for (String fieldId : fieldOrdering.keySet())
        {
            String entityFieldId;

            entityFieldId = objectToEntityFieldMap.get(fieldId);
            if (entityFieldId != null)
            {
                filter.addFieldOrdering(entityFieldId, fieldOrdering.get(fieldId).getDataFilterOrderMethod());
            }
            else throw new IllegalArgumentException("No entity field mapping found for data object field: " + fieldId);
        }

        return filter;
    }

    @Override
    public String toString()
    {
        return "T8DataFilter{" + "filterId=" + filterId + '}';
    }

    public void printStructure()
    {
        T8ObjectFilterPrinter.printStructure(System.out, this);
    }
}
