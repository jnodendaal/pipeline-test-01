package com.pilog.t8.definition.operation.java;

import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * This class defines the definition of a specific operation type.
 *
 * @author Bouwer du Preez
 */
public class T8DynamicJavaOperationDefinition extends T8ServerOperationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_OPERATION_DYNAMIC_JAVA";
    public static final String DISPLAY_NAME = "Dynamic Java Server Operation";
    public static final String DESCRIPTION = "A server-side Java operation that is compiled during runtime.";
    public enum Datum
    {
        SOURCE_CODE,
        BYTE_CODE
    };
    // -------- Definition Meta-Data -------- //

    public T8DynamicJavaOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.JAVA, Datum.SOURCE_CODE.toString(), "Java Source", "The java code implementing the operation logic."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.BYTE_ARRAY), Datum.BYTE_CODE.toString(), "Java Byte Code", "The compiled java byte code for this operation.").setHidden(true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.operation.java.T8DynamicJavaOperation", new Class<?>[]{T8Context.class, this.getClass(), String.class}, context, this, operationIid);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.operation.java.T8DynamicJavaOperationActionHandler", new Class<?>[]{T8Context.class, T8DynamicJavaOperationDefinition.class}, context, this);
    }

    public String getClassName()
    {
        String packageName;
        String className;

        // Get the class name byte formatting the operation definition.
        className = T8IdentifierUtilities.toCamelCase(getIdentifier().substring(1));
        className = (Character.toUpperCase(className.charAt(0)) + className.substring(1));

        // Append the project id as package.
        packageName = T8IdentifierUtilities.toCamelCase(getRootProjectId().substring(1));
        packageName = packageName.toLowerCase();

        // Prefix the class name with the package name.
        className = packageName + "." + className;
        return className;
    }

    public String getSourceCode()
    {
        return getDefinitionDatum(Datum.SOURCE_CODE);
    }

    public void setSourceCode(String code)
    {
        setDefinitionDatum(Datum.SOURCE_CODE, code);
    }

    public Map<String, byte[]> getByteCode()
    {
        return getDefinitionDatum(Datum.BYTE_CODE);
    }

    public void setByteCode(Map<String, byte[]> byteCode)
    {
        setDefinitionDatum(Datum.BYTE_CODE, byteCode);
    }
}

