package com.pilog.t8.security;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8PasswordChangeResponse implements Serializable
{
    private String identifier;
    private boolean success;
    private String message;
    
    public T8PasswordChangeResponse(String identifier, boolean success, String message)
    {
        this.identifier = identifier;
        this.success = success;
        this.message = message;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }
    
    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
