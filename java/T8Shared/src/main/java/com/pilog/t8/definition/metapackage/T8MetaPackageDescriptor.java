package com.pilog.t8.definition.metapackage;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8MetaPackageDescriptor implements Serializable
{
    // Standard ID to ensure compatible serialization.
    static final long serialVersionUID = 1L;
    
    private String identifier;
    private String description;
    private ArrayList<T8MetaPackageDefinitionDataLocation> definitionDataIndex;
    
    public T8MetaPackageDescriptor(String identifier)
    {
        this.identifier = identifier;
        this.definitionDataIndex = new ArrayList<T8MetaPackageDefinitionDataLocation>();
    }
    
    public String getIdentifier()
    {
        return identifier;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public int getDefinitionCount()
    {
        return definitionDataIndex.size();
    }
    
    public boolean containsDefinition(String definitionIdentifier)
    {
        for (T8MetaPackageDefinitionDataLocation dataLocation : definitionDataIndex)
        {
            if (dataLocation.getDefinitionIdentifier().equals(definitionIdentifier)) return true;
        }
        
        return false;
    }
    
    public T8MetaPackageDefinitionDataLocation getDefinitionDataLocation(String definitionIdentifier)
    {
        for (T8MetaPackageDefinitionDataLocation dataLocation : definitionDataIndex)
        {
            if (definitionIdentifier.equals(dataLocation.getDefinitionIdentifier()))
            {
                return dataLocation;
            }
        }
        
        return null;
    }
    
    public T8MetaPackageDefinitionDataLocation getDefinitionDataLocation(int definitionIndex)
    {
        return definitionIndex < definitionDataIndex.size() ? definitionDataIndex.get(definitionIndex) : null;
    }
    
    public void addDefinitionDataLocation(String definitionIdentifier, long offset, int size)
    {
        definitionDataIndex.add(new T8MetaPackageDefinitionDataLocation(definitionIdentifier, offset, size));
    }
    
    public ArrayList<String> getDefinitionIdentifierList()
    {
        ArrayList<String> identifierList;
        
        identifierList = new ArrayList<String>();
        for (T8MetaPackageDefinitionDataLocation dataLocation : definitionDataIndex)
        {
            identifierList.add(dataLocation.getDefinitionIdentifier());
        }
        
        return identifierList;
    }
}
