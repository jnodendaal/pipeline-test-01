package com.pilog.t8.definition.script;

import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public abstract class T8CustomClientContextScriptDefinition extends T8CustomScriptDefinition implements T8ClientContextScriptDefinition
{
    public T8CustomClientContextScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8ClientContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.script.T8DefaultClientContextScript", new Class<?>[]{T8Context.class, T8ClientContextScriptDefinition.class}, context, this);
    }
}
