package com.pilog.t8.flow.phase;

import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition.FlowNodeType;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhaseNodeDetails implements Serializable
{
    private String nodeId;
    private FlowNodeType nodeType;
    private String displayName;

    public T8FlowPhaseNodeDetails(String nodeId, FlowNodeType nodeType, String displayName)
    {
        this.nodeId = nodeId;
        this.nodeType = nodeType;
        this.displayName = displayName;
    }

    public String getNodeId()
    {
        return nodeId;
    }

    public void setNodeId(String nodeId)
    {
        this.nodeId = nodeId;
    }

    public FlowNodeType getNodeType()
    {
        return nodeType;
    }

    public void setNodeType(FlowNodeType nodeType)
    {
        this.nodeType = nodeType;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
}
