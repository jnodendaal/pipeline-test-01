package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.time.T8Time;

/**
 * @author Bouwer du Preez
 */
public class T8DtTime extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TIME";

    public T8DtTime()
    {
    }

    public T8DtTime(T8DefinitionManager context)
    {
    }

    public T8DtTime(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8Time.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        return JsonValue.valueOf(((T8Time)object).getMilliseconds());
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        return new T8Time(jsonValue.asLong());
    }
}
