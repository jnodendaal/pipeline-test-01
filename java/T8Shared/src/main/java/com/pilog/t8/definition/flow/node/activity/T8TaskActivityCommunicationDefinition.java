package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.communication.T8CommunicationDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskActivityCommunicationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_TASK_ACTIVITY_COMMUNICATION";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_TASK_ACTIVITY_COMMUNICATION";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "TASK_COMM_";
    public static final String DISPLAY_NAME = "Workflow Task Activity Communication";
    public static final String DESCRIPTION = "A communication sent during the lifecycle of a task activity.";
    public enum Datum {COMMUNICATION_IDENTIFIER,
                       COMMUNICATION_PARAMETER_MAPPING,
                       COMMUNICATION_PARAMETER_EXPRESSION_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8TaskActivityCommunicationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.COMMUNICATION_IDENTIFIER.toString(), "Communication", "The communication to use."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.COMMUNICATION_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow to Communication Input", "A mapping of Flow Parameters to the corresponding Communication Input Parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.COMMUNICATION_PARAMETER_EXPRESSION_MAPPING.toString(), "Parameter Mapping:  Communication Parameter to Expression", "A map containing communication input parameters and the expressions to be evaluated for each one (if applicable).  Expressions have access to flow parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.COMMUNICATION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CommunicationDefinition.GROUP_IDENTIFIER));
        else if (Datum.COMMUNICATION_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String communicationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            communicationId = getCommunicationIdentifier();
            if (communicationId != null)
            {
                T8CommunicationDefinition communicationDefinition;
                T8WorkFlowDefinition flowDefinition;

                communicationDefinition = (T8CommunicationDefinition)definitionContext.getRawDefinition(getRootProjectId(), communicationId);
                flowDefinition = (T8WorkFlowDefinition)getAncestorDefinition(T8WorkFlowDefinition.TYPE_IDENTIFIER);

                if ((communicationDefinition != null) && (flowDefinition != null))
                {
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    List<T8DataParameterDefinition> communicationParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();
                    communicationParameterDefinitions = communicationDefinition.getInputParameters();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((flowParameterDefinitions != null) && (communicationParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition communicationParameterDefinition : communicationParameterDefinitions)
                            {
                                identifierList.add(communicationParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(flowParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.COMMUNICATION_PARAMETER_EXPRESSION_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String communicationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            communicationId = getCommunicationIdentifier();
            if (communicationId != null)
            {
                T8CommunicationDefinition communicationDefinition;

                communicationDefinition = (T8CommunicationDefinition)definitionContext.getRawDefinition(getRootProjectId(), communicationId);
                if (communicationDefinition != null)
                {
                    List<T8DataParameterDefinition> communicationParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    communicationParameterDefinitions = communicationDefinition.getInputParameters();

                    identifierMap = new HashMap<String, List<String>>();
                    if (communicationParameterDefinitions != null)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<String>();
                        for (T8DataParameterDefinition communicationParameterDefinition : communicationParameterDefinitions)
                        {
                            identifierMap.put(communicationParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getCommunicationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.COMMUNICATION_IDENTIFIER.toString());
    }

    public void setCommunicationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.COMMUNICATION_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getCommunicationParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.COMMUNICATION_PARAMETER_MAPPING.toString());
    }

    public void setCommunicationParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.COMMUNICATION_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getCommunicationParameterExpressionMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.COMMUNICATION_PARAMETER_EXPRESSION_MAPPING.toString());
    }

    public void setCommunicationParameterExpressionMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.COMMUNICATION_PARAMETER_EXPRESSION_MAPPING.toString(), mapping);
    }
}
