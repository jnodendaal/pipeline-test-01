package com.pilog.t8.definition.flow.task;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectDefinition;
import com.pilog.t8.definition.functionality.T8TaskFunctionalityDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FlowTaskDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_NAME = "Tasks";
    public static final String GROUP_DESCRIPTION = "Tasks executed as part of a workflow process.";
    private enum Datum
    {
        TASK_TYPE_DISPLAY_NAME,
        TASK_TYPE_DESCRIPTION,
        TASK_DEFAULT_PRIORITY,
        VALIDATION_SCRIPT_DEFINITION,
        INPUT_PARAMETER_DEFINITIONS,
        OUTPUT_PARAMETER_DEFINITIONS,
        VALIDATION_OUTPUT_PARAMETER_DEFINITIONS,
        DATA_ACCESS_ID,
        FUNCTIONALITY_ID,
        TASK_OBJECT_ID,
        TASK_OBJECT_PARAMETER_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    /**
     * The enumerated options are:
     * 1.  INTERNAL:  The task is executed internally to the flow engine and no task is issued on the task list.
     * 2.  INTERNAL_CLAIM_DEPENDENT:  The task is issued on the task list, but is executed internally to the flow engine once the task is claimed by a user.
     * 3.  EXTERNAL:  The task is issued on the task list and is executed externally to the flow engine.  The flow engine simply waits for the task completion before proceeding.
     */
    public enum TaskExecutionType {INTERNAL, EXTERNAL};

    public T8FlowTaskDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TASK_TYPE_DISPLAY_NAME.toString(), "Task Type Display Name", "The name of this type of task that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TASK_TYPE_DESCRIPTION.toString(), "Task Type Description", "The description of this type of task that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.TASK_DEFAULT_PRIORITY.toString(), "Task Default Priority", "The default priority that will be assigned to this task initially. Priorities are assigned in brackets of 100, with 0-99 being very low, and 500-599 being very high.", 200));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Task Input Parameters", "The input parameters applicable to this task."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Task Output Parameters", "The output parameters returned by this task."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.VALIDATION_OUTPUT_PARAMETER_DEFINITIONS.toString(), "Task Validation Output Parameters", "The output parameters returned by the validation script of this task."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.VALIDATION_SCRIPT_DEFINITION.toString(), "Task Validation Script", "The script that executes when a request to complete the task is processed.  This script determines whether or not all requirements for task completion have been met."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ACCESS_ID.toString(), "Data Access Id", "The id of the data access layer applicable to this task."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_ID.toString(), "Task Functionality", "The functionality that will be accessed by the user to complete the task."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_OBJECT_ID.toString(), "Task Object", "The task list that will be updated when the details of this task change."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.TASK_OBJECT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Task to Object", "A mapping of task parameters to the corresponding task object parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.VALIDATION_OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.VALIDATION_SCRIPT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FlowTaskValidationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.DATA_ACCESS_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), "@DG_DATA_RECORD_ACCESS")); // GROUP_IDENTIFIER hard-coded due to T8DocumentShared not included in T8Shared.
        else if (Datum.FUNCTIONALITY_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8TaskFunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TASK_OBJECT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8TaskObjectDefinition.GROUP_IDENTIFIER));
        else if (Datum.TASK_OBJECT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String objectId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            objectId = getTaskObjectId();
            if (objectId != null)
            {
                T8TaskObjectDefinition objectDefinition;

                objectDefinition = (T8TaskObjectDefinition)definitionContext.getRawDefinition(getRootProjectId(), objectId);
                if (objectDefinition != null)
                {
                    List<T8DataParameterDefinition> taskParameterDefinitions;
                    List<T8DataParameterDefinition> objectParameterDefinitions;
                    TreeMap<String, List<String>> idMap;

                    taskParameterDefinitions = getInputParameterDefinitions();
                    taskParameterDefinitions.addAll(getValidationOutputParameterDefinitions());
                    objectParameterDefinitions = objectDefinition.getInputParameterDefinitions();

                    idMap = new TreeMap<String, List<String>>();
                    if (objectParameterDefinitions != null)
                    {
                        ArrayList<String> fieldIdList;

                        fieldIdList = new ArrayList<String>();
                        for (T8DataParameterDefinition parameterDefinition : objectParameterDefinitions)
                        {
                            fieldIdList.add(parameterDefinition.getPublicIdentifier());
                        }

                        Collections.sort(fieldIdList);
                        for (T8DataParameterDefinition parameterDefinition : taskParameterDefinitions)
                        {
                            idMap.put(parameterDefinition.getIdentifier(), fieldIdList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", idMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.VALIDATION_OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else if (Datum.VALIDATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return null;
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8WorkFlowTaskTestHarness", new Class<?>[]{});
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getName())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.TASK_TYPE_DISPLAY_NAME.toString(), "Display Name needs to be provided", T8DefinitionValidationError.ErrorType.CRITICAL));
        if (Strings.isNullOrEmpty(getDescription())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.TASK_TYPE_DESCRIPTION.toString(), "Display Description needs to be provided", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    public String getName()
    {
        return getDefinitionDatum(Datum.TASK_TYPE_DISPLAY_NAME);
    }

    public void setName(String displayName)
    {
        setDefinitionDatum(Datum.TASK_TYPE_DISPLAY_NAME, displayName);
    }

    public String getDescription()
    {
        return getDefinitionDatum(Datum.TASK_TYPE_DESCRIPTION);
    }

    public void setDescription(String description)
    {
        setDefinitionDatum(Datum.TASK_TYPE_DESCRIPTION, description);
    }

    public Integer getDefaultPriority()
    {
        return getDefinitionDatum(Datum.TASK_DEFAULT_PRIORITY);
    }

    public void setDefaultPriority(Integer taskDefaultPriority)
    {
        setDefinitionDatum(Datum.TASK_DEFAULT_PRIORITY, taskDefaultPriority);
    }

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public ArrayList<T8DataParameterDefinition> getValidationOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.VALIDATION_OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setValidationOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.VALIDATION_OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public T8FlowTaskValidationScriptDefinition getValidationScriptDefinition()
    {
        return getDefinitionDatum(Datum.VALIDATION_SCRIPT_DEFINITION);
    }

    public void setValidationScriptDefinition(T8FlowTaskValidationScriptDefinition scriptDefinition)
    {
        setDefinitionDatum(Datum.VALIDATION_SCRIPT_DEFINITION, scriptDefinition);
    }

    public String getDataAccessId()
    {
        return getDefinitionDatum(Datum.DATA_ACCESS_ID);
    }

    public void setDataAccessId(String id)
    {
        setDefinitionDatum(Datum.DATA_ACCESS_ID, id);
    }

    public Map<String, String> getTaskObjectParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.TASK_OBJECT_PARAMETER_MAPPING.toString());
    }

    public void setTaskObjectParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.TASK_OBJECT_PARAMETER_MAPPING.toString(), mapping);
    }

    public String getFunctionalityId()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_ID);
    }

    public void setFunctionalityId(String functionalityId)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_ID, functionalityId);
    }

    public String getTaskObjectId()
    {
        return getDefinitionDatum(Datum.TASK_OBJECT_ID);
    }

    public void setTaskObjectId(String taskId)
    {
        setDefinitionDatum(Datum.TASK_OBJECT_ID, taskId);
    }

    public abstract TaskExecutionType getExecutionType();
    public abstract T8FlowTask getNewTaskInstance(T8TaskDetails taskDetails) throws Exception;
}
