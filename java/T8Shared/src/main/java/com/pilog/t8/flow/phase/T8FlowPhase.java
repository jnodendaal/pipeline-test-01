package com.pilog.t8.flow.phase;

import com.pilog.t8.definition.flow.phase.T8FlowPhaseDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhase implements Serializable
{
    private String identifier;
    private String displayName;
    private String description;
    private T8FlowPhaseStatus status;
    private final List<T8FlowPhaseNodeDetails> nodeDetails;
    private final List<T8FlowPhaseUserDetails> userDetails;
    private final List<T8FlowPhaseProfileDetails> profileDetails;
    private final List<T8FlowPhase> inputPhases;
    private final List<T8FlowPhase> nextPhases;

    public enum T8FlowPhaseStatus
    {
        NOT_STARTED("Not Started"),
        IN_PROGRESS("In Progress"),
        COMPLETED("Completed");

        private final String displayName;

        T8FlowPhaseStatus(String displayName)
        {
            this.displayName = displayName;
        }

        public String getDisplayName()
        {
            return displayName;
        }
    };

    public T8FlowPhase(String identifier)
    {
        this.identifier = identifier;
        this.userDetails = new ArrayList<>();
        this.profileDetails = new ArrayList<>();
        this.inputPhases = new ArrayList<>();
        this.nextPhases = new ArrayList<>();
        this.status = T8FlowPhaseStatus.NOT_STARTED;
        this.nodeDetails = new ArrayList<>();
    }

    public T8FlowPhase(T8FlowPhaseDefinition definition)
    {
        this(definition.getIdentifier());
        this.displayName = definition.getDisplayName();
        this.description = definition.getMetaDescription();
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public T8FlowPhaseStatus getStatus()
    {
        return status;
    }

    public void setStatus(T8FlowPhaseStatus status)
    {
        this.status = status;
    }

    public void addNodeDetails(T8FlowPhaseNodeDetails nodeDetails)
    {
        this.nodeDetails.add(nodeDetails);
    }

    public List<T8FlowPhaseNodeDetails> getNodeDetails()
    {
        return new ArrayList<T8FlowPhaseNodeDetails>(nodeDetails);
    }

    public List<T8FlowPhase> getNextPhases()
    {
        return new ArrayList<T8FlowPhase>(nextPhases);
    }

    public void addNextPhase(T8FlowPhase nextPhase)
    {
        // Only add the phase if it is not null.
        if (nextPhase != null)
        {
            // Only add the phase if it is not part of the preceding phases (we have to prevent loops).
            if (!getPrecedingPhases().contains(nextPhase))
            {
                nextPhase.addInputPhase(this);
                nextPhases.add(nextPhase);
            }
            else throw new IllegalArgumentException("Cannot add " + nextPhase + " as next phase from " + this + " as this will create a loop.");
        }
        else throw new IllegalArgumentException("Cannot add null phase.");
    }

    void addInputPhase(T8FlowPhase inputPhase)
    {
        inputPhases.add(inputPhase);
    }

    public List<T8FlowPhaseUserDetails> getUserDetails()
    {
        return new ArrayList<T8FlowPhaseUserDetails>(userDetails);
    }

    public void addUserDetails(T8FlowPhaseUserDetails details)
    {
        userDetails.add(details);
    }

    public List<T8FlowPhaseProfileDetails> getProfileDetails()
    {
        return new ArrayList<T8FlowPhaseProfileDetails>(profileDetails);
    }

    public void addProfileDetails(T8FlowPhaseProfileDetails details)
    {
        profileDetails.add(details);
    }

    public boolean canAddNextPhase(T8FlowPhase nextPhase)
    {
        if (nextPhase == null) return false;
        else if (nextPhase == this) return false;
        else return (!getPrecedingPhases().contains(nextPhase));
    }

    public boolean containsNextPhase(String phaseIdentifier)
    {
        for (T8FlowPhase nextPhase : nextPhases)
        {
            if (nextPhase.getIdentifier().equals(phaseIdentifier))
            {
                return true;
            }
        }

        return false;
    }

    public boolean containsSubsequentPhase(String phaseIdentifier)
    {
        for (T8FlowPhase nextPhase : nextPhases)
        {
            if (nextPhase.getIdentifier().equals(phaseIdentifier))
            {
                return true;
            }
            else if (nextPhase.containsSubsequentPhase(phaseIdentifier))
            {
                return true;
            }
        }

        return false;
    }

    public List<T8FlowPhase> getPrecedingPhases()
    {
        List<T8FlowPhase> phases;

        phases = new ArrayList<T8FlowPhase>();
        phases.add(this);
        for (T8FlowPhase inputPhase : inputPhases)
        {
            for (T8FlowPhase precedingPhase : inputPhase.getPrecedingPhases())
            {
                if (!phases.contains(precedingPhase))
                {
                    phases.add(precedingPhase);
                }
            }
        }

        return phases;
    }

    public List<T8FlowPhase> getSubsequentPhases()
    {
        List<T8FlowPhase> phases;

        phases = new ArrayList<T8FlowPhase>();
        phases.add(this);
        for (T8FlowPhase nextPhase : nextPhases)
        {
            for (T8FlowPhase subsequentPhase : nextPhase.getSubsequentPhases())
            {
                if (!phases.contains(subsequentPhase))
                {
                    phases.add(subsequentPhase);
                }
            }
        }

        return phases;
    }

    public List<T8FlowPhase> getSubsequentInProgressPhases()
    {
        List<T8FlowPhase> phases;

        phases = new ArrayList<T8FlowPhase>();
        for (T8FlowPhase nextPhase : getSubsequentPhases())
        {
            if (nextPhase.getStatus() == T8FlowPhaseStatus.IN_PROGRESS)
            {
                phases.add(nextPhase);
            }
        }

        return phases;
    }

    public void setSubsequentStatus(T8FlowPhaseStatus status)
    {
        for (T8FlowPhase nextPhase : nextPhases)
        {
            nextPhase.setStatus(status);
            nextPhase.setSubsequentStatus(status);
        }
    }

    public String toStringRepresentation()
    {
        StringBuilder buffer;

        buffer = new StringBuilder();
        buffer.append("Phase:");
        buffer.append(identifier);

        for (T8FlowPhase nextPhase : nextPhases)
        {
            buffer.append("{");
            buffer.append(nextPhase.getIdentifier());
            buffer.append("}");
        }

        for (T8FlowPhase nextPhase : nextPhases)
        {
            buffer.append("\n");
            buffer.append(nextPhase.toStringRepresentation());
        }

        return buffer.toString();
    }

    @Override
    public String toString()
    {
        return "T8FlowPhase{" + "identifier=" + identifier + '}';
    }
}
