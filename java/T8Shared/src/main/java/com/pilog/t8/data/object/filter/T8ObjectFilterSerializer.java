package com.pilog.t8.data.object.filter;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.object.filter.T8ObjectFilter.OrderMethod;
import com.pilog.t8.data.object.filter.T8ObjectFilterClause.ObjectFilterConjunction;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriterion.ObjectFilterOperator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.LinkedHashMap;

/**
 * @author Bouwer du Preez
 */
public class T8ObjectFilterSerializer
{
    public T8ObjectFilterSerializer()
    {
    }

    public JsonValue serializeDataFilter(T8ObjectFilter filter)
    {
        LinkedHashMap<String, OrderMethod> fieldOrdering;
        T8ObjectFilterCriteria filterCriteria;
        JsonObject filterObject;
        JsonArray fieldArray;

        // Create a new Json object for the filter.
        filterObject = new JsonObject();

        // Add field ordering if required.
        fieldOrdering = filter.getFieldOrdering();
        if (fieldOrdering != null)
        {
            JsonArray orderArray;

            orderArray = new JsonArray();
            for (String fieldId : fieldOrdering.keySet())
            {
                JsonObject orderObject;

                orderObject = new JsonObject();
                orderObject.add("fieldId", fieldId);
                orderObject.add("method", fieldOrdering.get(fieldId).toString());
                orderArray.add(orderObject);
            }

            filterObject.add("order", orderArray);
        }

        // Add the filter criteria if required.
        filterCriteria = filter.getFilterCriteria();
        if (filterCriteria != null)
        {
            JsonObject jsonCriteria;

            jsonCriteria = serializeFilterCriteria(filterCriteria, ObjectFilterConjunction.AND);
            filterObject.add("criteria", jsonCriteria.getJsonArray("criteria"));
        }

        // Add the list of fields.
        fieldArray = new JsonArray();
        for (String fieldIdentifier : filter.getFieldIdentifiers())
        {
            fieldArray.add(fieldIdentifier);
        }
        filterObject.add("fields", fieldArray);

        // Return the Json representation of the data filter.
        return filterObject;
    }

    protected JsonObject serializeFilterCriteria(T8ObjectFilterCriteria filterCriteria, ObjectFilterConjunction conjunction)
    {
        LinkedHashMap<T8ObjectFilterClause, ObjectFilterConjunction> filterClauses;
        JsonObject criteriaObject;
        JsonArray clauseArray;

        // Create the criteria object.
        criteriaObject = new JsonObject();
        criteriaObject.add("conjunction", conjunction.toString());

        // Create the clause array.
        clauseArray = new JsonArray();
        filterClauses = filterCriteria.getFilterClauses();
        for (T8ObjectFilterClause clause : filterClauses.keySet())
        {
            clauseArray.add(serializeFilterClause(clause, filterClauses.get(clause)));
        }

        // Add the clauses to the criteria and return the object.
        criteriaObject.add("criteria", clauseArray);
        return criteriaObject;
    }

    protected JsonObject serializeFilterCriterion(T8ObjectFilterCriterion filterCriterion, ObjectFilterConjunction conjunction)
    {
        JsonObject criterionObject;

        // Create the criterion object.
        criterionObject = new JsonObject();
        criterionObject.add("conjunction", conjunction.toString());
        criterionObject.add("fieldId", filterCriterion.getFieldIdentifier());
        criterionObject.add("operator", filterCriterion.getOperator().toString());
        criterionObject.add("value", filterCriterion.getFilterValue());
        return criterionObject;
    }

    protected JsonObject serializeFilterClause(T8ObjectFilterClause filterClause, ObjectFilterConjunction conjunction)
    {
        if (filterClause instanceof T8ObjectFilterCriteria)
        {
            return serializeFilterCriteria((T8ObjectFilterCriteria)filterClause, conjunction);
        }
        else if (filterClause instanceof T8ObjectFilterCriterion)
        {
            return serializeFilterCriterion((T8ObjectFilterCriterion)filterClause, conjunction);
        }
        else throw new IllegalArgumentException("Invalid filter clause type: " + filterClause);
    }

    public T8ObjectFilter deserializeDataFilter(JsonValue value)
    {
        if (value != null)
        {
            if (value instanceof JsonObject)
            {
                T8ObjectFilterCriteria filterCriteria;
                JsonArray fieldArray;
                JsonArray orderArray;
                JsonObject jsonFilter;
                T8ObjectFilter filter;

                // Create the new data filter.
                jsonFilter = (JsonObject)value;
                filter = new T8ObjectFilter();

                // Add the specified ordering.
                orderArray = jsonFilter.getJsonArray("order");
                if (orderArray != null)
                {
                    for (JsonValue orderValue : orderArray)
                    {
                        JsonObject orderObject;
                        String fieldIdentifier;

                        orderObject = (JsonObject)orderValue;
                        fieldIdentifier = orderObject.getString("fieldId");
                        filter.addFieldOrdering(fieldIdentifier, orderObject.getString("method"));
                    }
                }

                // Add the filter criteria.
                if (jsonFilter.containsName("criteria"))
                {
                    filterCriteria = (T8ObjectFilterCriteria)deserializeFilterClause(jsonFilter);
                    if (filterCriteria != null) filter.setFilterCriteria(filterCriteria);
                }

                // Add the filter fields.
                fieldArray = jsonFilter.getJsonArray("fields");
                if (fieldArray != null)
                {
                    for (JsonValue field : fieldArray)
                    {
                        filter.addFieldIdentifier(field.asString());
                    }
                }

                // Return the complete filter.
                return filter;
            }
            else throw new IllegalArgumentException("Input value is not a Data Filter object: " + value);
        }
        else return null;
    }

    protected T8ObjectFilterClause deserializeFilterClause(JsonObject jsonClause)
    {
        if (jsonClause.containsName("criteria"))
        {
            return deserializeFilterCriteria(jsonClause);
        }
        else
        {
            return deserializeFilterCriterion(jsonClause);
        }
    }

    protected T8ObjectFilterCriteria deserializeFilterCriteria(JsonObject jsonCriteria)
    {
        T8ObjectFilterCriteria criteria;
        JsonArray clauseArray;

        criteria = new T8ObjectFilterCriteria();
        clauseArray = jsonCriteria.getJsonArray("criteria");
        if (clauseArray != null)
        {
            for (JsonValue clauseValue : clauseArray)
            {
                T8ObjectFilterClause clause;
                ObjectFilterConjunction conjunction;
                JsonObject clauseObject;
                String conjunctionString;

                clauseObject = (JsonObject)clauseValue;
                conjunctionString = clauseObject.getString("conjunction");
                conjunction = Strings.isNullOrEmpty(conjunctionString) ? ObjectFilterConjunction.AND : ObjectFilterConjunction.valueOf(conjunctionString);
                clause = deserializeFilterClause(clauseObject);
                if (clause != null) criteria.addFilterClause(conjunction, clause);
            }
        }

        return criteria;
    }

    protected T8ObjectFilterCriterion deserializeFilterCriterion(JsonObject jsonCriterion)
    {
        String fieldIdentifier;
        ObjectFilterOperator operator;
        Object filterValue;

        fieldIdentifier = jsonCriterion.getString("fieldId");
        operator = ObjectFilterOperator.valueOf(jsonCriterion.getString("operator"));
        filterValue = jsonCriterion.getString("value");
        return new T8ObjectFilterCriterion(fieldIdentifier, operator, filterValue);
    }
}
