package com.pilog.t8.definition.script;

import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultClientContextScriptDefinition extends T8DefaultScriptDefinition implements T8ClientContextScriptDefinition
{
    public T8DefaultClientContextScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8ClientContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultClientContextScript").getConstructor(T8Context.class, T8ClientContextScriptDefinition.class);
        return (T8ClientContextScript)constructor.newInstance(context, this);
    }
}
