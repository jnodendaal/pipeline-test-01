package com.pilog.t8.definition.notification;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationGenerator;
import com.pilog.t8.ui.notification.T8NotificationDisplayComponent;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * @author Gavin Boshoff
 */
public class T8MessageNotificationDefinition extends T8NotificationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String IDENTIFIER = "@NOTIF_MESSAGE";
    public static final String TYPE_IDENTIFIER = "@DT_NOTIFICATION_MESSAGE";
    public static final String DISPLAY_NAME = "Message Notification";
    public static final String DESCRIPTION = "A definition of a notification generator used for user messages.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    // Notification input parameters.
    public static final String PARAMETER_USER_IDENTIFIER_LIST = "$P_USER_IDENTIFIER_LIST";
    public static final String PARAMETER_MESSAGE = "$P_MESSAGE";

    public T8MessageNotificationDefinition(String identifier)
    {
        super(IDENTIFIER);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_USER_IDENTIFIER_LIST, "User Identifier List", "The identifiers of the recipient users of this message.", T8DataType.LIST));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_MESSAGE, "Message", "The message to be sent.", T8DataType.STRING));

        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getNotificationParameterDefinitions()
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_MESSAGE, "Message", "The message that was sent.", T8DataType.STRING));

        return parameterList;
    }

    @Override
    public String getNotificationDisplayName()
    {
        return "Message";
    }

    @Override
    public Icon getNotificationIcon()
    {
        return new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/speechBubbleIcon.png"));
    }

    @Override
    public T8NotificationGenerator getNotificationGeneratorInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.notification.T8MessageNotificationGenerator", new Class<?>[]{T8MessageNotificationDefinition.class}, this);
    }

    @Override
    public T8NotificationDisplayComponent getDisplayComponentInstance(T8Context context, T8Notification notification)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.notification.T8MessageNotificationDisplayComponent", new Class<?>[]{T8Context.class, T8MessageNotificationDefinition.class, T8Notification.class}, context, this, notification);
    }
}