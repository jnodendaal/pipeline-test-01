package com.pilog.t8.data.type;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

/**
 * @author Bouwer du Preez
 */
public class T8DataTypeUtilities
{
    public static boolean matchesAnyDataType(T8DataType typeToMatch, boolean includeAttributes, T8DataType... possibleMatches)
    {
        for (T8DataType possibleMatch : possibleMatches)
        {
            if(possibleMatch.getDataTypeStringRepresentation(includeAttributes).equals(typeToMatch.getDataTypeStringRepresentation(includeAttributes)))
                    return true;
        }

        return false;
    }

    public static void addJSONObject(JsonObjectBuilder objectBuilder, String objectIdentifier, Object javaObject)
    {
        if (javaObject == null)
        {
            objectBuilder.addNull(objectIdentifier);
        }
        else if (javaObject instanceof String)
        {
            objectBuilder.add(objectIdentifier, (String)javaObject);
        }
        else if (javaObject instanceof Boolean)
        {
            objectBuilder.add(objectIdentifier, (Boolean)javaObject);
        }
        else if (javaObject instanceof Boolean)
        {
            objectBuilder.add(objectIdentifier, (Boolean)javaObject);
        }
        else if (javaObject instanceof Integer)
        {
            objectBuilder.add(objectIdentifier, (Integer)javaObject);
        }
        else if (javaObject instanceof Long)
        {
            objectBuilder.add(objectIdentifier, (Long)javaObject);
        }
        else if (javaObject instanceof Double)
        {
            objectBuilder.add(objectIdentifier, (Double)javaObject);
        }
        else if (javaObject instanceof BigInteger)
        {
            objectBuilder.add(objectIdentifier, (BigInteger)javaObject);
        }
        else if (javaObject instanceof BigDecimal)
        {
            objectBuilder.add(objectIdentifier, (BigDecimal)javaObject);
        }
        else if (javaObject instanceof JsonArrayBuilder)
        {
            objectBuilder.add(objectIdentifier, (JsonArrayBuilder)javaObject);
        }
        else if (javaObject instanceof JsonObjectBuilder)
        {
            objectBuilder.add(objectIdentifier, (JsonObjectBuilder)javaObject);
        }
        else throw new RuntimeException("Invalid JSON object type '" + objectIdentifier + "': " + javaObject.getClass().getCanonicalName());
    }

    public static void addJSONObject(JsonArrayBuilder arrayBuilder, Object javaObject)
    {
        if (javaObject == null)
        {
            arrayBuilder.addNull();
        }
        else if (javaObject instanceof String)
        {
            arrayBuilder.add((String)javaObject);
        }
        else if (javaObject instanceof Boolean)
        {
            arrayBuilder.add((Boolean)javaObject);
        }
        else if (javaObject instanceof Boolean)
        {
            arrayBuilder.add((Boolean)javaObject);
        }
        else if (javaObject instanceof Integer)
        {
            arrayBuilder.add((Integer)javaObject);
        }
        else if (javaObject instanceof Long)
        {
            arrayBuilder.add((Long)javaObject);
        }
        else if (javaObject instanceof Double)
        {
            arrayBuilder.add((Double)javaObject);
        }
        else if (javaObject instanceof BigInteger)
        {
            arrayBuilder.add((BigInteger)javaObject);
        }
        else if (javaObject instanceof BigDecimal)
        {
            arrayBuilder.add((BigDecimal)javaObject);
        }
        else if (javaObject instanceof JsonArrayBuilder)
        {
            arrayBuilder.add((JsonArrayBuilder)javaObject);
        }
        else if (javaObject instanceof JsonObjectBuilder)
        {
            arrayBuilder.add((JsonObjectBuilder)javaObject);
        }
        else throw new RuntimeException("Invalid JSON object type: " + javaObject.getClass().getCanonicalName());
    }
}
