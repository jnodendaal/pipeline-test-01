package com.pilog.t8.definition.flow.task.user;

/**
 * @author Bouwer du Preez
 */
public interface T8WorkFlowUserTaskUIDefinition 
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_SYSTEM_TASK_USER_UI";
    // -------- Definition Meta-Data -------- //
}
