/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.exception;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hennie Brink
 */
public class T8ExceptionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_EXCEPTION_MESSAGE";
    public static final String TYPE_IDENTIFIER = "@DT_EXCEPTION_MESSAGE_DEFAULT";
    public static final String STORAGE_PATH = "/exception_messages";
    public static final String DISPLAY_NAME = "Default Exception";
    public static final String DESCRIPTION = "The details that will be displayed for this exception";
    public static final String IDENTIFIER_PREFIX = "EX_";

    public enum Datum
    {
        CODE,
        MESSAGE_EXPRESSION,
        INPUT_PARAMETER_DEFINITIONS
    };
// -------- Definition Meta-Data -------- //
    public T8ExceptionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters", "The input parameters required for this exception"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CODE.toString(), "Exception Code", "The unique code assigned for this exception"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.MESSAGE_EXPRESSION.toString(), "Exception Message Expression", "The expression that will build up the exception message, any translations that needs to occur must be done inside of this"));

        return datumTypes;
    }



    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    public String getCode()
    {
        return (String) getDefinitionDatum(Datum.CODE.toString());
    }

    public void setCode(String code)
    {
        setDefinitionDatum(Datum.CODE.toString(), code);
    }

    public String getMessageExpession()
    {
        return (String) getDefinitionDatum(Datum.MESSAGE_EXPRESSION.toString());
    }

    public void setMessageExpession(String expression)
    {
        setDefinitionDatum(Datum.MESSAGE_EXPRESSION.toString(), expression);
    }

    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return (List<T8DataParameterDefinition>) getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setInputParameterDefinitions(List<T8DataParameterDefinition> definitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), definitions);
    }
}
