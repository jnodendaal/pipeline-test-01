package com.pilog.t8.definition.user.property;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8UserPropertyDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_USER_PROPERTY";
    public static final String GROUP_NAME = "User Properties";
    public static final String GROUP_DESCRIPTION = "Properties that govern specific aspects of the system behavior specifically configured for each user.";
    public static final String TYPE_IDENTIFIER = "@DT_USER_PROPERTY";
    public static final String DISPLAY_NAME = "User Property";
    public static final String DESCRIPTION = "A User Property definition.";
    public static final String IDENTIFIER_PREFIX = "USR_PROP_";
    public static final String STORAGE_PATH = "/user_properties";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8UserPropertyDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {}

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return new ArrayList<>();
    }
}