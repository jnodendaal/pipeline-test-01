package com.pilog.t8.definition.data.source.listener.audit;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSourceListener;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.listener.T8DataSourceListenerDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * CREATE TABLE AUDIT_TRAIL
 *   (
 *       ID                          NUMBER NOT NULL,
 *       ENTITY_ID                   VARCHAR2(50) NOT NULL,
 *       ENTITY_NAME                 VARCHAR2(50) NOT NULL,
 *       ENTITY_PROPERTY             VARCHAR2(50) NOT NULL,
 *       ENTITY_PROPERTY_OLD_VALUE   VARCHAR2(4000),
 *       ENTITY_PROPERTY_NEW_VALUE   VARCHAR2(4000),
 *       OPERATION_TYPE              VARCHAR2(50),
 *       ACTOR_ID                    NUMBER NOT NULL,
 *       RELEVANCE_TIME              TIMESTAMP(6) NOT NULL,
 *       CONSTRAINT PK_AUDIT_TRAIL PRIMARY KEY (ID)
 *   )
 * 
 * @author Bouwer du Preez
 */
public class T8DataSourceAuditListenerDefinition extends T8DataSourceListenerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_LISTENER_AUDIT";
    public static final String DISPLAY_NAME = "Audit Listener";
    public static final String DESCRIPTION = "A listener that logs data changes to autiting records.";
    public static final String VERSION = "0";
    public enum Datum {DATA_SOURCE_IDENTIFIER, AUDIT_FIELD_IDENTIFIERS};
    // -------- Definition Meta-Data -------- //
    
    public T8DataSourceAuditListenerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_SOURCE_IDENTIFIER.toString(), "Data Source", "The Data Source"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_LIST, Datum.AUDIT_FIELD_IDENTIFIERS.toString(), "Connection", "The connection to which this data source will connect to retrieve its data."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_SOURCE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataSourceDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DataSourceListener getNewDataSourceListenerInstance(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        try
        {
            Constructor constructor;
            
            constructor = Class.forName("com.pilog.t8.data.source.listener.audit.T8DataSourceAuditListener").getConstructor(this.getClass(), T8ServerContext.class, T8SessionContext.class);
            return (T8DataSourceListener)constructor.newInstance(this, serverContext, sessionContext);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
