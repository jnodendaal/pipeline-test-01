package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import java.lang.reflect.Constructor;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowClientFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final String flowIid;
    private final String taskIid;

    public T8WorkFlowClientFunctionalityAccessHandle(String functionalityId, String functionalityIid, String flowIid, String taskIid, String displayName, String description, Icon icon)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.flowIid = flowIid;
        this.taskIid = taskIid;
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        T8FunctionalityView view;

        constructor = Class.forName("com.pilog.t8.functionality.view.T8WorkFlowClientFunctionalityView").getConstructor(T8ComponentController.class, T8WorkFlowClientFunctionalityAccessHandle.class, String.class, String.class);
        view = (T8FunctionalityView)constructor.newInstance(controller, this, flowIid, taskIid);
        view.addFunctionalityViewListener(viewListener);
        return view;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return true;
    }
}
