package com.pilog.t8.exception;

/**
 * @author Hennie Brink
 * <p>
 * This interface needs to be implemented by the exceptions that will display messages to the client,
 * the main purpose of this interface is so that the class implementing it can be found in a
 * stack trace so that the appropriate message can be shown to the user
 */
public interface T8UserException
{
    public String getUserMessage();
}
