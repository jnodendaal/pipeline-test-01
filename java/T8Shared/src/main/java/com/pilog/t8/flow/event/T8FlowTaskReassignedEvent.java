package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey.WaitKeyIdentifier;
import com.pilog.t8.flow.key.T8TaskReassignedExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskReassignedEvent extends T8FlowEvent
{
    private final String taskInstanceIdentifier;
    private final String userIdentifier;
    private final String profileIdentifier;

    public static final String TYPE_ID = "TASK_REASSIGNED";

    public T8FlowTaskReassignedEvent(String taskInstanceIdentifier, String userIdentifier, String profileIdentifier)
    {
        super(taskInstanceIdentifier);
        this.taskInstanceIdentifier = taskInstanceIdentifier;
        this.userIdentifier = userIdentifier;
        this.profileIdentifier = profileIdentifier;
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public String getTaskInstanceIdentifier()
    {
        return taskInstanceIdentifier;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public String getProfileIdentifier()
    {
        return profileIdentifier;
    }

    @Override
    public T8FlowExecutionKey getExecutionKey(T8WorkFlowNodeDefinition waitingNodeDefinition, T8FlowNodeState waitingNodeState)
    {
        T8FlowTaskState taskState;

        taskState = waitingNodeState.getTaskState();
        if (taskState != null)
        {
            // Check that the task IID's match.
            if (taskState.getTaskIid().equals(taskInstanceIdentifier))
            {
                String flowIdentifier;
                String flowInstanceIdentifier;
                String nodeIdentifier;
                String nodeInstanceIdentifier;

                flowIdentifier = waitingNodeState.getFlowId();
                flowInstanceIdentifier = waitingNodeState.getFlowIid();
                nodeIdentifier = waitingNodeState.getNodeId();
                nodeInstanceIdentifier = waitingNodeState.getNodeIid();
                return new T8TaskReassignedExecutionKey(flowIdentifier, flowInstanceIdentifier, nodeIdentifier, nodeInstanceIdentifier, taskInstanceIdentifier, userIdentifier, profileIdentifier);
            }
            else return null;
        }
        else throw new RuntimeException("No task state found in node state waiting for task '" + taskInstanceIdentifier + "' claim or completion: " + waitingNodeState);
    }

    @Override
    public T8DataFilterCriteria getWaitKeyFilterCriteria()
    {
        T8DataFilterCriteria keyIDCriteria;
        T8DataFilterCriteria filterCriteria;

        // Create the criteria for the key ID.
        keyIDCriteria = new T8DataFilterCriteria();
        keyIDCriteria.addFilterClause(DataFilterConjunction.AND, T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_ID", DataFilterOperator.EQUAL, WaitKeyIdentifier.TASK_CLAIM.toString());
        keyIDCriteria.addFilterClause(DataFilterConjunction.OR, T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_ID", DataFilterOperator.EQUAL, WaitKeyIdentifier.TASK_COMPLETION.toString());

        // Create the filter criteria.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$KEY_TASK_IID", DataFilterOperator.EQUAL, taskInstanceIdentifier);
        filterCriteria.addFilterClause(DataFilterConjunction.AND, keyIDCriteria);
        return filterCriteria;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
