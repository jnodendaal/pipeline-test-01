package com.pilog.t8.definition.ui.laf;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LookAndFeelDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_LOOK_AND_FEEL";
    public static final String GROUP_IDENTIFIER = "@DG_UI_LOOK_AND_FEEL";
    public static final String GROUP_NAME = "Look and Feel Configurations";
    public static final String GROUP_DESCRIPTION = "Settings that define the display properties of the user interface of the system.";
    public static final String IDENTIFIER_PREFIX = "LAF_";
    public static final String STORAGE_PATH = "/look_and_feels";
    public static final String DISPLAY_NAME = "UI Look & Feel";
    public static final String DESCRIPTION = "A definition that specifies the default look and feel settings for a T8 UI.";
    private enum Datum {NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER,
                        BUTTON_BACKGROUND_PAINTER_IDENTIFIER,
                        BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER,
                        BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER,
                        BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER,
                        TOOL_BAR_BUTTON_BACKGROUND_PAINTER_IDENTIFIER,
                        TOOL_BAR_BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER,
                        TOOL_BAR_BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER,
                        TOOL_BAR_BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER,
                        DATA_SEARCH_COMBOBOX_BACKGROUND_PAINTER,
                        DATA_SEARCH_COMBOBOX_ACTIVATED_PAINTER,
                        TOOL_BAR_BACKGROUND_PAINTER_IDENTIFIER,
                        CONTENT_HEADER_BACKGROUND_PAINTER_IDENTIFIER,
                        CONTENT_HEADER_BACKGROUND_SELECTED_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition notificationBackgroundPainterDefinition;
    private T8PainterDefinition buttonBackgroundPainterDefinition;
    private T8PainterDefinition buttonBackgroundFocusedPainterDefinition;
    private T8PainterDefinition buttonBackgroundSelectedPainterDefinition;
    private T8PainterDefinition buttonBackgroundDisabledPainterDefinition;
    private T8PainterDefinition toolBarButtonBackgroundPainterDefinition;
    private T8PainterDefinition toolBarButtonBackgroundFocusedPainterDefinition;
    private T8PainterDefinition toolBarButtonBackgroundSelectedPainterDefinition;
    private T8PainterDefinition toolBarButtonBackgroundDisabledPainterDefinition;
    private T8PainterDefinition dataSearchComboBoxBackgroundPainterDefinition;
    private T8PainterDefinition dataSearchComboBoxActivatedPainterDefinition;
    private T8PainterDefinition toolBarBackgroundPainterDefinition;
    private T8PainterDefinition contentHeaderBackgroundPainterDefinition;
    private T8PainterDefinition contentHeaderBackgroundSelectedPainterDefinition;

    public T8LookAndFeelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Notification Background Painter", "The painter to use for painting the background of popup notifications."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Button Background Painter", "The painter to use for painting the background of the button in its default state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString(), "Button Focused Background Painter", "The painter to use for painting the background of the button in its focused state (keyboard or mouse focus)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), "Button Selected Background Painter", "The painter to use for painting the background of the button in its selected state (when pressed)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString(), "Button Disabled Background Painter", "The painter to use for painting the background of the button in its disabled state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TOOL_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Tool Bar Background Painter", "The painter to use for painting the background of a Tool Bar."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TOOL_BAR_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Tool Bar Button Background Painter", "The painter to use for painting the background of the button in its default state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TOOL_BAR_BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString(), "Tool Bar Button Focused Background Painter", "The painter to use for painting the background of the button in its focused state (keyboard or mouse focus)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TOOL_BAR_BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), "Tool Bar Button Selected Background Painter", "The painter to use for painting the background of the button in its selected state (when pressed)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TOOL_BAR_BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString(), "Tool Bar Button Disabled Background Painter", "The painter to use for painting the background of the button in its disabled state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_SEARCH_COMBOBOX_BACKGROUND_PAINTER.toString(), "Data Search Combo Box Background Painter", "The painter to use for painting the background of the data search combo box."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_SEARCH_COMBOBOX_ACTIVATED_PAINTER.toString(), "Data Search Combo Box Activated Painter", "The painter to use for painting the background of the data search combo box in an activated state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONTENT_HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Content Header Background Painter", "The painter to use for painting the background of a content header."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONTENT_HEADER_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), "Content Header Selected Background Painter", "The painter to use for painting the background of a content header in its selected state."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TOOL_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TOOL_BAR_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TOOL_BAR_BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TOOL_BAR_BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TOOL_BAR_BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.DATA_SEARCH_COMBOBOX_BACKGROUND_PAINTER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.DATA_SEARCH_COMBOBOX_ACTIVATED_PAINTER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.CONTENT_HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.CONTENT_HEADER_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DefinitionManager definitionManager;
        String definitionId;

        definitionManager = context.getServerContext().getDefinitionManager();

        // Notification Background
        definitionId = getNotificationBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            notificationBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button
        definitionId = getButtonBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            buttonBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button : Focsed
        definitionId = getButtonBackgroundFocusedPainterIdentifier();
        if (definitionId != null)
        {
            buttonBackgroundFocusedPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button : Selected
        definitionId = getButtonBackgroundSelectedPainterIdentifier();
        if (definitionId != null)
        {
            buttonBackgroundSelectedPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button : Disabled
        definitionId = getButtonBackgroundDisabledPainterIdentifier();
        if (definitionId != null)
        {
            buttonBackgroundDisabledPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8ToolBar
        definitionId = getToolBarBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            toolBarBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button (Tool Bar)
        definitionId = getToolBarButtonBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            toolBarButtonBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button (Tool Bar) : Focsed
        definitionId = getToolBarButtonBackgroundFocusedPainterIdentifier();
        if (definitionId != null)
        {
            toolBarButtonBackgroundFocusedPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button (Tool Bar) : Selected
        definitionId = getToolBarButtonBackgroundSelectedPainterIdentifier();
        if (definitionId != null)
        {
            toolBarButtonBackgroundSelectedPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8Button (Tool Bar) : Disabled
        definitionId = getToolBarButtonBackgroundDisabledPainterIdentifier();
        if (definitionId != null)
        {
            toolBarButtonBackgroundDisabledPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8DataSearchComboBox : Background
        definitionId = getDataSearchComboBoxBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            dataSearchComboBoxBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8DataSearchComboBox : Activated
        definitionId = getDataSearchComboBoxActivatedPainterIdentifier();
        if (definitionId != null)
        {
            dataSearchComboBoxActivatedPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8ContentHeader
        definitionId = getContentHeaderBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            contentHeaderBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        // T8ContentHeader : Selected
        definitionId = getContentHeaderBackgroundSelectedPainterIdentifier();
        if (definitionId != null)
        {
            contentHeaderBackgroundSelectedPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    public T8PainterDefinition getNotificationBackgroundPainterDefinition()
    {
        return notificationBackgroundPainterDefinition;
    }

    public T8PainterDefinition getButtonBackgroundPainterDefinition()
    {
        return buttonBackgroundPainterDefinition;
    }

    public T8PainterDefinition getButtonBackgroundFocusedPainterDefinition()
    {
        return buttonBackgroundFocusedPainterDefinition;
    }

    public T8PainterDefinition getButtonBackgroundSelectedPainterDefinition()
    {
        return buttonBackgroundSelectedPainterDefinition;
    }

    public T8PainterDefinition getButtonBackgroundDisabledPainterDefinition()
    {
        return buttonBackgroundDisabledPainterDefinition;
    }

    public T8PainterDefinition getToolBarButtonBackgroundPainterDefinition()
    {
        return toolBarButtonBackgroundPainterDefinition;
    }

    public T8PainterDefinition getToolBarButtonBackgroundFocusedPainterDefinition()
    {
        return toolBarButtonBackgroundFocusedPainterDefinition;
    }

    public T8PainterDefinition getToolBarButtonBackgroundSelectedPainterDefinition()
    {
        return toolBarButtonBackgroundSelectedPainterDefinition;
    }

    public T8PainterDefinition getToolBarButtonBackgroundDisabledPainterDefinition()
    {
        return toolBarButtonBackgroundDisabledPainterDefinition;
    }

    public T8PainterDefinition getToolBarBackgroundPainterDefinition()
    {
        return toolBarBackgroundPainterDefinition;
    }

    public T8PainterDefinition getDataSearchComboBoxBackgroundPainterDefinition()
    {
        return dataSearchComboBoxBackgroundPainterDefinition;
    }

    public T8PainterDefinition getDataSearchComboBoxActivatedPainterDefinition()
    {
        return dataSearchComboBoxActivatedPainterDefinition;
    }

    public T8PainterDefinition getContentHeaderBackgroundPainterDefinition()
    {
        return contentHeaderBackgroundPainterDefinition;
    }

    public T8PainterDefinition getContentHeaderBackgroundSelectedPainterDefinition()
    {
        return contentHeaderBackgroundSelectedPainterDefinition;
    }

    public String getNotificationBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public String getButtonBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setButtonBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getButtonBackgroundFocusedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString());
    }

    public void setButtonBackgroundFocusedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getButtonBackgroundSelectedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString());
    }

    public void setButtonBackgroundSelectedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getButtonBackgroundDisabledPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString());
    }

    public void setButtonBackgroundDisabledPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getToolBarBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TOOL_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setToolBarBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TOOL_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getToolBarButtonBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setToolBarButtonBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getToolBarButtonBackgroundFocusedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString());
    }

    public void setToolBarButtonBackgroundFocusedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getToolBarButtonBackgroundSelectedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString());
    }

    public void setToolBarButtonBackgroundSelectedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getToolBarButtonBackgroundDisabledPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString());
    }

    public void setToolBarButtonBackgroundDisabledPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TOOL_BAR_BUTTON_BACKGROUND_DISABLED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getDataSearchComboBoxBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_SEARCH_COMBOBOX_BACKGROUND_PAINTER.toString());
    }

    public void setDataSearchComboBoxBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_SEARCH_COMBOBOX_BACKGROUND_PAINTER.toString(), identifier);
    }

    public String getDataSearchComboBoxActivatedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_SEARCH_COMBOBOX_ACTIVATED_PAINTER.toString());
    }

    public void setDataSearchComboBoxActivatedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_SEARCH_COMBOBOX_ACTIVATED_PAINTER.toString(), identifier);
    }

    public String getContentHeaderBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTENT_HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setContentHeaderBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONTENT_HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getContentHeaderBackgroundSelectedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTENT_HEADER_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString());
    }

    public void setContentHeaderBackgroundSelectedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONTENT_HEADER_BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), identifier);
    }
}
