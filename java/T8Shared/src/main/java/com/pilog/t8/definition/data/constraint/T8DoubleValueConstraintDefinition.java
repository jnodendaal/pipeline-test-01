package com.pilog.t8.definition.data.constraint;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8DoubleValueConstraintDefinition extends T8DataValueConstraintDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_VALUE_CONSTRAINT_DOUBLE";
    public static final String DISPLAY_NAME = "Double Value Constraint";
    public static final String DESCRIPTION = "A Double Value Constraint";
    public static final String IDENTIFIER_PREFIX = "CONSTR_DBL_";
    public enum Datum
    {
        SCALE,
        PRECISION
    };
    // -------- Definition Meta-Data -------- //
    public T8DoubleValueConstraintDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DoubleValueConstraintDefinition(String identifier, int precision, int scale)
    {
        super(identifier);
        setPrecision(precision);
        setScale(scale);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.SCALE.toString(), "Scale", "The scale of the value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PRECISION.toString(), "Precision", "The precision of the value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void validate(String fieldIdentifier, Object value)
    {
        if(!enforceValidation()) return;
        if(value == null) return;
        if(!(value instanceof Double) && !(value instanceof BigDecimal))
            throw new T8DataValidationException(value, "Value is not of type Double. The field " + fieldIdentifier + " requires a double value but found " + value.getClass().getCanonicalName());

        //All other precision information is either lost or will be handled later so lets just assume if the value is a double it is valid
    }

    @Override
    public String getSQLRepresentation()
    {
        return "(" + getPrecision() + "," + getScale() + ")";
    }

    public Integer getScale()
    {
        return getDefinitionDatum(Datum.SCALE);
    }

    public void setScale(Integer scale)
    {
        setDefinitionDatum(Datum.SCALE, scale);
    }

    public Integer getPrecision()
    {
        return getDefinitionDatum(Datum.PRECISION);
    }

    public void setPrecision(Integer precision)
    {
        setDefinitionDatum(Datum.PRECISION, precision);
    }
}
