package com.pilog.t8.data.object;

/**
 * @author Bouwer du Preez
 */
public interface T8ComponentDataObjectProvider
{
    public T8DataObject getDataObject(String objectIdentifier) throws Exception;
}
