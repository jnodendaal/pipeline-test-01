package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.flow.task.T8TaskDetails;

/**
 * @author Bouwer du Preez
 */
public class T8DtTaskDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TASK_DETAIL";

    public T8DtTaskDetails() {}

    public T8DtTaskDetails(T8DefinitionManager context) {}

    public T8DtTaskDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8TaskDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8TaskDetails taskDetails;
            JsonObject jsonTaskDetails;

            // Create the concept JSON object.
            taskDetails = (T8TaskDetails)object;
            jsonTaskDetails = new JsonObject();
            jsonTaskDetails.add("flowId", taskDetails.getFlowId());
            jsonTaskDetails.add("flowIid", taskDetails.getFlowIid());
            jsonTaskDetails.add("taskId", taskDetails.getTaskId());
            jsonTaskDetails.add("taskIid", taskDetails.getTaskIid());
            jsonTaskDetails.addIfNotNull("taskName", taskDetails.getTaskDisplayName());
            jsonTaskDetails.addIfNotNull("taskDescription", taskDetails.getTaskDescription());
            jsonTaskDetails.addIfNotNull("functionalityId", taskDetails.getFunctionalityId());
            jsonTaskDetails.addIfNotNull("restrictionUserId", taskDetails.getRestrictionUserId());
            jsonTaskDetails.addIfNotNull("restrictionProfileId", taskDetails.getRestrictionProfileId());
            jsonTaskDetails.add("claimed", taskDetails.isClaimed());
            jsonTaskDetails.addIfNotNull("claimedByUserId", taskDetails.getClaimedByUserIdentifier());
            jsonTaskDetails.add("timeIssued", taskDetails.getTimeIssued());
            jsonTaskDetails.addIfNotNull("timeClaimed", taskDetails.getTimeClaimed());
            jsonTaskDetails.add("priority", taskDetails.getPriority());

            // Return the final task object.
            return jsonTaskDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8TaskDetails deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8TaskDetails taskDetails;
            JsonObject taskObject;
            String flowIid;
            String flowId;
            String taskIid;
            String taskId;
            String taskName;
            String taskDescription;
            String functionalityId;
            String restrictionUserId;
            String restrictionProfileId;
            Boolean claimed;
            String claimedByUserId;
            Long timeIssued;
            Long timeClaimed;
            Integer priority;

            // Get the JSON values.
            taskObject = jsonValue.asObject();
            flowIid = taskObject.getString("flowIid");
            flowId = taskObject.getString("flowId");
            taskIid = taskObject.getString("taskIid");
            taskId = taskObject.getString("taskId");
            taskName = taskObject.getString("taskName");
            taskDescription = taskObject.getString("taskDescription");
            functionalityId = taskObject.getString("functionalityId");
            restrictionUserId = taskObject.getString("restrictionUserId");
            restrictionProfileId = taskObject.getString("restrictionProfileId");
            claimed = taskObject.getBoolean("claimed");
            claimedByUserId = taskObject.getString("claimedByUserId");
            timeIssued = taskObject.getLong("timeIssued");
            timeClaimed = taskObject.getLong("timeClaimed");
            priority = taskObject.getInteger("priority");

            // Create the task details object.
            taskDetails = new T8TaskDetails(taskIid);
            taskDetails.setTaskId(taskId);
            taskDetails.setFlowIid(flowIid);
            taskDetails.setFlowId(flowId);
            taskDetails.setTaskDisplayName(taskName);
            taskDetails.setTaskDescription(taskDescription);
            taskDetails.setFunctionalityId(functionalityId);
            taskDetails.setRestrictionUserId(restrictionUserId);
            taskDetails.setRestrictionProfileId(restrictionProfileId);
            taskDetails.setClaimed(claimed != null && claimed);
            taskDetails.setClaimedByUserId(claimedByUserId);
            taskDetails.setTimeIssued(timeIssued);
            taskDetails.setTimeClaimed(timeClaimed);
            taskDetails.setPriority(priority != null ? priority : 0);

            // Return the completed object.
            return taskDetails;
        }
        else return null;
    }
}