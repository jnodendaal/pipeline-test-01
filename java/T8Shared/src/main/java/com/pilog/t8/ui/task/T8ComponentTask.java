package com.pilog.t8.ui.task;

/**
 * @author Bouwer du Preez
 */
public interface T8ComponentTask
{
    public String getMessage();
    public boolean isIndeterminate();
    public double getProgress();
    public boolean isActive();
    public Object getProgressReportObject();
}
