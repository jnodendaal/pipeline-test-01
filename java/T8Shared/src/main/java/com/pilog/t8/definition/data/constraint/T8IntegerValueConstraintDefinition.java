/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.constraint;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8IntegerValueConstraintDefinition extends T8DataValueConstraintDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_VALUE_CONSTRAINT_INTEGER";
    public static final String DISPLAY_NAME = "Integer Value Constraint";
    public static final String DESCRIPTION = "A Integer Value Constraint";
    public static final String IDENTIFIER_PREFIX = "CONSTR_INT_";
    public enum Datum { PRECISION
    };
    // -------- Definition Meta-Data -------- //
    public T8IntegerValueConstraintDefinition(String identifier)
    {
        super(identifier);
    }

    public T8IntegerValueConstraintDefinition(String identifier, int precision)
    {
        super(identifier);
        setPrecision(precision);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PRECISION.toString(), "Precision", "The precision of the value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void validate(String fieldIdentifier, Object value)
    {
        if (enforceValidation() && value != null && !(value instanceof Integer))
        {
            throw new T8DataValidationException(value, "Value is not of type Integer. The field " + fieldIdentifier + " requires a Integer value but found " + value.getClass().getCanonicalName());
        }
    }

    @Override
    public String getSQLRepresentation()
    {
        return ""; // Integer does not allow for the specification of the precision. This is in fact handled by different INT types on the DB.
//        return "("+getPrecision()+")";
    }

    public Integer getPrecision()
    {
        return (Integer)getDefinitionDatum(Datum.PRECISION.toString());
    }

    public void setPrecision(Integer precision)
    {
        setDefinitionDatum(Datum.PRECISION.toString(), precision);
    }
}
