package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateNodeOutputNodeMap extends T8FlowStateParameterMap
{
    protected final T8FlowNodeState parentNodeState;

    public T8FlowStateNodeOutputNodeMap(T8FlowNodeState parentNodeState, String parameterIID, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        super(null, parentNodeState.getFlowIid(), parameterIID, entityIdentifier, null);
        this.parentNodeState = parentNodeState;
        if (dataSet != null) addOutputNodes(dataSet);
    }

    @Override
    public synchronized T8DataEntity createEntity(int index, String key, T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;
        Object value;

        value = get(key);

        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityId + "$FLOW_IID", flowIid);
        newEntity.setFieldValue(entityId + "$NODE_IID", parentNodeState.getNodeIid());
        newEntity.setFieldValue(entityId + "$OUTPUT_NODE_ID", key);
        newEntity.setFieldValue(entityId + "$OUTPUT_NODE_IID", value);
        return newEntity;
    }

    private synchronized void addOutputNodes(T8ParameterStateDataSet dataSet)
    {
        List<T8DataEntity> entityList;

        entityList = dataSet.getParameterEntitiesByParentIID(parentCollection != null ? parentCollection.getParameterIid() : null);
        for (T8DataEntity parameterEntity : entityList)
        {
            String outputNodeID;
            String outputNodeIID;

            outputNodeID = (String)parameterEntity.getFieldValue(entityId + "$OUTPUT_NODE_ID");
            outputNodeIID = (String)parameterEntity.getFieldValue(entityId + "$OUTPUT_NODE_IID");

            put(outputNodeID, outputNodeIID);
            parameterIids.put(outputNodeID, T8IdentifierUtilities.createNewGUID());
        }
    }
}
