/**
 * Created on 18 Jul 2016, 8:33:42 PM
 */
package com.pilog.t8.org.operational;

/**
 * This interface merely exists for the reflective instantiation of the concrete
 * class which would result in a recursive dependency.
 *
 * @author Gavin Boshoff
 */
public interface T8OperationalHoursCalculator
{
    String prettyPrintDuration(long millisecondDuration);

    /**
     * Calculates an approximate operationally valid end time given any start
     * time to work from and the number of operational milliseconds as the
     * duration between the operational start and end times.
     *
     * @param startTime The {@code long} start time. Does not have to be a
     *      valid operational time
     * @param duration The {@code long} number of operation milliseconds that
     *      should form the difference between the start and calculated end time
     *
     * @return The {@code long} timestamp representing a time falling within
     *      operational times
     */
    long calculateOperationalEndTime(long startTime, long duration);

    /**
     * Calculates an approximate operationally valid start time given any end
     * time to work from and the number of operational milliseconds as the
     * duration between the operational end and start times.
     *
     * @param endTime The {@code long} end time. Does not have to be a valid
     *      operational time
     * @param duration The {@code long} number of operation milliseconds that
     *      should form the difference between the end and calculated start time
     *
     * @return The {@code long} timestamp representing a time falling within
     *      operational times
     */
    long calculateOperationalStartTime(long endTime, long duration);

    long calculateTimeDifference(long startTime, long endTime);

    long calculateOperationalTimeDifference(long startTime, long endTime);

    /**
     * Determines whether or not the given timestamp instant falls within any
     * operational time-frame. Any factor, such as public holidays, are taken
     * into consideration when determining whether or not the instant falls
     * within the operational time.
     *
     * @param instant The {@code long} instant to be checked
     *
     * @return {@code true} if the instant falls within operational time,
     *      {@code false otherwise
     */
    boolean isOperationalInstant(long instant);
}