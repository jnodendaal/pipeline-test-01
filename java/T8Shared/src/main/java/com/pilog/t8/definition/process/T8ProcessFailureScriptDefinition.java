/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.process;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8ProcessFailureScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PROCESS_FAILURE_SCRIPT";
    public static final String TYPE_IDENTIFIER = "@DT_PROCESS_FAILURE_SCRIPT";
    public static final String DISPLAY_NAME = "Process Failure Script";
    public static final String DESCRIPTION = "A script that will be executed when a process failure occurs";
    public static final String IDENTIFIER_PREFIX = "PF_";

    public enum Datum
    {
    };
// -------- Definition Meta-Data -------- //
    public static final String PARAMETER_THROWABLE = "$P_THROWABLE";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_PROCESS_DEFINITION = "$P_PROCESS_DEFINITION";

    public T8ProcessFailureScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(
                                                                        T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> dataParameterDefinitions;

        dataParameterDefinitions = new ArrayList<T8DataParameterDefinition>(2);

        dataParameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_THROWABLE, "Throwable", "The exception that was thrown by the process", T8DataType.CUSTOM_OBJECT));
        dataParameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETERS, "Input Parameters", "The input parameters that was sent to this process.", T8DataType.MAP));
        dataParameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_PROCESS_DEFINITION, "Process Definition", "The definition of the failed process.", T8DataType.DEFINITION));

        return dataParameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(
                                                                         T8Context context) throws Exception
    {
        return null;
    }

}
