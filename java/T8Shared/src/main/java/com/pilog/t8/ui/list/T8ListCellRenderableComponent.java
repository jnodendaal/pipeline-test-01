package com.pilog.t8.ui.list;

import com.pilog.t8.ui.T8CellRenderableComponent;

/**
 * @author Bouwer du Preez
 */
public interface T8ListCellRenderableComponent extends T8CellRenderableComponent
{
    public String getDisplayFieldIdentifier();
}
