package com.pilog.t8;

import com.pilog.t8.definition.T8DefinitionEditorFactory;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Window;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8Client
{
    public void lockUI(String message);
    public void unlockUI();

    public T8ExpressionEvaluator getExpressionEvaluator();
    public Map<String, Object> executeClientScript(T8ScriptDefinition scriptDefinition, Map<String, Object> inputParameters) throws Exception;

    public T8ClientEnvironmentStatus getEnvironmentStatus();
    public Window getParentWindow();
    public void showAdministrationConsole();
    public void dispayHelp(String helpIdentifier);
    public void refreshNotifications();

    public T8DefinitionManager getDefinitionManager();
    public T8CommunicationManager getCommunicationManager();
    public T8NotificationManager getNotificationManager();
    public T8ProcessManager getProcessManager();
    public T8SecurityManager getSecurityManager();
    public T8FunctionalityManager getFunctionalityManager();
    public T8FlowManager getFlowManager();
    public T8FileManager getFileManager();
    public T8ConfigurationManager getConfigurationManager();
    public T8ServiceManager getServiceManager();

    public T8DefinitionEditorFactory getDefinitionEditorFactory(T8Context context);

    public T8ServerOperationStatusReport getServerOperationStatus(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport stopServerOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport cancelServerOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport pauseServerOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport resumeServerOperation(T8Context context, String operationIid) throws Exception;
    public T8ServerOperationStatusReport executeAsynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception;
    public Map<String, Object> executeAsynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters, String message) throws Exception;
    public Map<String, Object> executeSynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters, String message) throws Exception;
    public Map<String, Object> executeClientOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception;
}
