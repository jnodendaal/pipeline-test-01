package com.pilog.t8.definition.script;

import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentEventHandlerScriptCompletionHandler extends T8DefaultModuleCompletionHandler
{
    public T8ComponentEventHandlerScriptCompletionHandler(T8Context context, T8ComponentEventHandlerScriptDefinition definition)
    {
        super(context, definition);
    }
}
