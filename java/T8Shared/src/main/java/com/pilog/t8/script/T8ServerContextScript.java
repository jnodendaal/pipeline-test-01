package com.pilog.t8.script;

/**
 * @author Bouwer du Preez
 */
public interface T8ServerContextScript extends T8Script
{
    public T8ScriptExecutionHandle getScriptExecutionHandle();
}
