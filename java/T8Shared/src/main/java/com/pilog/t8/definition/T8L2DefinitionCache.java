package com.pilog.t8.definition;

import com.pilog.t8.data.type.T8DataType;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface T8L2DefinitionCache
{
    public void init() throws Exception;
    public void destroy();
    public void clearCache();
    public void cacheDefinitions() throws Exception;
    public void setContextPath(File contextPath) throws Exception;

    public boolean containsDefinition(String identifier) throws Exception;
    public boolean containsDefinitionGroup(String groupIdentifier) throws Exception;
    public boolean loadsType(T8DefinitionTypeMetaData typeMetaData);
    public boolean savesType(T8DefinitionTypeMetaData typeMetaData);

    public T8Definition loadDefinition(String identifier) throws Exception;
    public void saveDefinition(T8Definition definition) throws Exception;
    public void deleteDefinition(String identifier) throws Exception;
    public long getCachedCharacterSize();
    public int getCachedDefinitionCount();
    public Set<String> getDefinitionIdentifiers() throws Exception;
    public Set<String> getDefinitionIdentifiers(String typeIdentifier) throws Exception;
    public Set<String> getGroupDefinitionIdentifiers(String groupIdentifier) throws Exception;
    public Set<String> getGroupIdentifiers() throws Exception;
    public T8DefinitionMetaData getDefinitionMetaData(String identifier) throws Exception;
    public List<T8DefinitionMetaData> getAllDefinitionMetaData() throws Exception;
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String typeIdentifier) throws Exception;
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String groupIdentifier) throws Exception;
    public List<String> findDefinitionsByText(String searchText);
    public List<String> findDefinitionsByRegularExpression(String expression) throws Exception;
    public List<String> findDefinitionsByIdentifier(String identifier) throws Exception;
    public List<String> findDefinitionsByDatum(String datumIdentifier, T8DataType dataType, String content) throws Exception;
}
