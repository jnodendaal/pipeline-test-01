package com.pilog.t8.flow.task;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.data.object.filter.T8ObjectFilterClause.ObjectFilterConjunction;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriteria;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriterion;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriterion.ObjectFilterOperator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8TaskFilter implements Serializable
{
    private String flowIid;
    private String userId;
    private List<String> profileIds;
    private boolean includeClaimedTasks;
    private boolean includeUnclaimedTasks;
    private final Set<String> includedTaskTypes; // If no identifiers are added, all types are considered to be included.
    private TaskOrdering taskOrdering;
    private int pageOffset;
    private int pageSize;
    private String descriptivePropertySearchString;

    public enum TaskOrdering
    {
        TIME_ISSUED_ASCENDING,
        TIME_ISSUED_DESCENDING,
        TIME_CLAIMED_ASCENDING,
        TIME_CLAIMED_DESCENDING
    };

    public T8TaskFilter()
    {
        flowIid = null;
        includeClaimedTasks = false;
        includeUnclaimedTasks = true;
        includedTaskTypes = new HashSet<>();
        taskOrdering = TaskOrdering.TIME_CLAIMED_ASCENDING;
        pageOffset = 0;
        pageSize = 20;
    }

    public String getFlowInstanceIdentifier()
    {
        return flowIid;
    }

    public void setFlowInstanceIdentifier(String flowInstanceIdentifier)
    {
        this.flowIid = flowInstanceIdentifier;
    }

    public boolean isIncludeClaimedTasks()
    {
        return includeClaimedTasks;
    }

    public void setIncludeClaimedTasks(boolean includeClaimedTasks)
    {
        this.includeClaimedTasks = includeClaimedTasks;
    }

    public boolean isIncludeUnclaimedTasks()
    {
        return includeUnclaimedTasks;
    }

    public void setIncludeUnclaimedTasks(boolean includeUnclaimedTasks)
    {
        this.includeUnclaimedTasks = includeUnclaimedTasks;
    }

    public void clearIncludedTaskTypes()
    {
        includedTaskTypes.clear();
    }

    public void addIncludedTaskType(String taskTypeIdentifier)
    {
        includedTaskTypes.add(taskTypeIdentifier);
    }

    public boolean isTaskTypeIncluded(String taskTypeIdentifier)
    {
        return includedTaskTypes.isEmpty() || includedTaskTypes.contains(taskTypeIdentifier);
    }

    public Set<String> getIncludedTaskTypes()
    {
        return includedTaskTypes;
    }

    public TaskOrdering getTaskOrdering()
    {
        return taskOrdering;
    }

    public void setTaskOrdering(TaskOrdering taskOrdering)
    {
        this.taskOrdering = taskOrdering;
    }

    public int getPageOffset()
    {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset)
    {
        this.pageOffset = pageOffset;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public void setDescriptivePropertySearchString(String descriptivePropertySearchString)
    {
        this.descriptivePropertySearchString = descriptivePropertySearchString;
    }

    public String getDescriptivePropertySearchString()
    {
        return descriptivePropertySearchString;
    }

    public String getUserIdentifier()
    {
        return userId;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userId = userIdentifier;
    }

    public List<String> getProfileIdentifiers()
    {
        return profileIds;
    }

    public void setProfileIdentifiers(List<String> profileIdentifiers)
    {
        this.profileIds = profileIdentifiers;
    }

    public T8DataFilter getDataFilter(T8Context context, String entiyId)
    {
        T8DataFilterCriteria claimRestrictionFilterCriteria;
        T8DataFilterCriteria userRestrictionFilterCriteria;
        T8DataFilterCriteria subRestrictionFilterCriteria;
        T8DataFilterCriteria restrictionFilterCriteria;
        T8DataFilterCriteria filterCriteria;
        List<String> workflowProfileIds;
        T8DataFilter filter;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entiyId, filterCriteria);
        filter.addFieldOrdering(entiyId + "$PRIORITY", T8DataFilter.OrderMethod.DESCENDING);
        filter.addFieldOrdering(entiyId + "$TASK_ID", T8DataFilter.OrderMethod.ASCENDING);
        filter.addFieldOrdering(entiyId + "$TIME_ISSUED", T8DataFilter.OrderMethod.DESCENDING);

        restrictionFilterCriteria = new T8DataFilterCriteria();

        // Create a filter combining the user and workflow profile restriction
        subRestrictionFilterCriteria = new T8DataFilterCriteria();

        // Create the filter criteria for user restriction.
        userRestrictionFilterCriteria = new T8DataFilterCriteria();
        userRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entiyId + "$RESTRICTION_USER_ID", DataFilterOperator.EQUAL, context.getUserId()));
        userRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.OR, new T8DataFilterCriterion(entiyId + "$RESTRICTION_USER_ID", DataFilterOperator.IS_NULL, null));
        subRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, userRestrictionFilterCriteria);

        // Create the filter criteria for workflow profile restriction.
        workflowProfileIds = context.getSessionContext().getWorkFlowProfileIdentifiers();
        if (workflowProfileIds.isEmpty())
        {
            T8DataFilterCriteria profileRestrictionFilterCriteria;

            profileRestrictionFilterCriteria = new T8DataFilterCriteria();
            profileRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entiyId + "$RESTRICTION_PROFILE_ID", DataFilterOperator.IS_NULL, null));
            subRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, profileRestrictionFilterCriteria);
        }
        else
        {
            T8DataFilterCriteria profileRestrictionFilterCriteria;

            profileRestrictionFilterCriteria = new T8DataFilterCriteria();
            profileRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entiyId + "$RESTRICTION_PROFILE_ID", DataFilterOperator.IN, workflowProfileIds));
            profileRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.OR, new T8DataFilterCriterion(entiyId + "$RESTRICTION_PROFILE_ID", DataFilterOperator.IS_NULL, null));
            subRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, profileRestrictionFilterCriteria);
        }

        restrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, subRestrictionFilterCriteria);

        // Create the filter criteria for user restriction. Will include tasks
        // associated with workflow profiles not in the current user's workflow profile list.
        userRestrictionFilterCriteria = new T8DataFilterCriteria();
        userRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entiyId + "$RESTRICTION_USER_ID", DataFilterOperator.EQUAL, context.getUserId()));
        restrictionFilterCriteria.addFilterClause(DataFilterConjunction.OR, userRestrictionFilterCriteria);

        filterCriteria.addFilterClause(DataFilterConjunction.AND, restrictionFilterCriteria);

        // Create the filter criteria for claim restriction.
        claimRestrictionFilterCriteria = new T8DataFilterCriteria();
        claimRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entiyId + "$CLAIMED_BY_USER_ID", DataFilterOperator.EQUAL, context.getUserId()));
        claimRestrictionFilterCriteria.addFilterClause(DataFilterConjunction.OR, new T8DataFilterCriterion(entiyId + "$CLAIMED_BY_USER_ID", DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, claimRestrictionFilterCriteria);

        // Add the flow IID if specified.
        if (this.flowIid != null)
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$FLOW_IID", DataFilterOperator.EQUAL, this.flowIid);
        }

        // Add the task type criteria.
        if (CollectionUtilities.hasContent(this.includedTaskTypes))
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$TASK_ID", DataFilterOperator.IN, this.includedTaskTypes);
        }

        // Add the filter for claimed status.
        if (!includeClaimedTasks)
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$TIME_CLAIMED", DataFilterOperator.IS_NULL, null);
        }

        // Add the filter for unclaimed status.
        if (!includeUnclaimedTasks)
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$TIME_CLAIMED", DataFilterOperator.IS_NOT_NULL, null);
        }

        // Add the filter for issued status.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$TIME_ISSUED", DataFilterOperator.IS_NOT_NULL, null);

        // Add the filter for completed status.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$TIME_COMPLETED", DataFilterOperator.IS_NULL, null);

        //Add the descriptive search string filter if available
        if (!Strings.isNullOrEmpty(this.descriptivePropertySearchString))
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entiyId + "$VALUE", DataFilterOperator.LIKE, this.descriptivePropertySearchString);
        }

        return filter;
    }

    public T8ObjectFilter getObjectFilter(T8SessionContext sessionContext)
    {
        T8ObjectFilterCriteria claimRestrictionFilterCriteria;
        T8ObjectFilterCriteria userRestrictionFilterCriteria;
        T8ObjectFilterCriteria subRestrictionFilterCriteria;
        T8ObjectFilterCriteria restrictionFilterCriteria;
        T8ObjectFilterCriteria filterCriteria;
        List<String> workflowProfileIds;
        T8ObjectFilter filter;

        // Create the new filter object.
        filterCriteria = new T8ObjectFilterCriteria();
        filter = new T8ObjectFilter(filterCriteria);
        filter.addFieldOrdering("$PRIORITY", T8ObjectFilter.OrderMethod.DESCENDING);
        filter.addFieldOrdering("$TASK_ID", T8ObjectFilter.OrderMethod.ASCENDING);
        filter.addFieldOrdering("$TIME_ISSUED", T8ObjectFilter.OrderMethod.DESCENDING);

        restrictionFilterCriteria = new T8ObjectFilterCriteria();

        // Create a filter combining the user and workflow profile restriction
        subRestrictionFilterCriteria = new T8ObjectFilterCriteria();

        // Create the filter criteria for user restriction.
        userRestrictionFilterCriteria = new T8ObjectFilterCriteria();
        userRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, new T8ObjectFilterCriterion("$RESTRICTION_USER_ID", ObjectFilterOperator.EQUAL, sessionContext.getUserIdentifier()));
        userRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.OR, new T8ObjectFilterCriterion("$RESTRICTION_USER_ID", ObjectFilterOperator.IS_NULL, null));
        subRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, userRestrictionFilterCriteria);

        // Create the filter criteria for workflow profile restriction.
        workflowProfileIds = sessionContext.getWorkFlowProfileIdentifiers();
        if (workflowProfileIds.isEmpty())
        {
            T8ObjectFilterCriteria profileRestrictionFilterCriteria;

            profileRestrictionFilterCriteria = new T8ObjectFilterCriteria();
            profileRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, new T8ObjectFilterCriterion("$RESTRICTION_PROFILE_ID", ObjectFilterOperator.IS_NULL, null));
            subRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, profileRestrictionFilterCriteria);
        }
        else
        {
            T8ObjectFilterCriteria profileRestrictionFilterCriteria;

            profileRestrictionFilterCriteria = new T8ObjectFilterCriteria();
            profileRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, new T8ObjectFilterCriterion("$RESTRICTION_PROFILE_ID", ObjectFilterOperator.IN, sessionContext.getWorkFlowProfileIdentifiers()));
            profileRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.OR, new T8ObjectFilterCriterion("$RESTRICTION_PROFILE_ID", ObjectFilterOperator.IS_NULL, null));
            subRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, profileRestrictionFilterCriteria);
        }

        restrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, subRestrictionFilterCriteria);

        // Create the filter criteria for user restriction. Will include tasks
        // associated with workflow profiles not in the current user's workflow profile list.
        userRestrictionFilterCriteria = new T8ObjectFilterCriteria();
        userRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, new T8ObjectFilterCriterion("$RESTRICTION_USER_ID", ObjectFilterOperator.EQUAL, sessionContext.getUserIdentifier()));
        restrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.OR, userRestrictionFilterCriteria);

        filterCriteria.addFilterClause(ObjectFilterConjunction.AND, restrictionFilterCriteria);

        // Create the filter criteria for claim restriction.
        claimRestrictionFilterCriteria = new T8ObjectFilterCriteria();
        claimRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.AND, new T8ObjectFilterCriterion("$CLAIMED_BY_USER_ID", ObjectFilterOperator.EQUAL, sessionContext.getUserIdentifier()));
        claimRestrictionFilterCriteria.addFilterClause(ObjectFilterConjunction.OR, new T8ObjectFilterCriterion("$CLAIMED_BY_USER_ID", ObjectFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(ObjectFilterConjunction.AND, claimRestrictionFilterCriteria);

        // Add the flow IID if specified.
        if (this.flowIid != null)
        {
            filterCriteria.addFilterClause(ObjectFilterConjunction.AND, "$FLOW_IID", ObjectFilterOperator.EQUAL, this.flowIid);
        }

        // Add the task type criteria.
        if (CollectionUtilities.hasContent(this.includedTaskTypes))
        {
            filterCriteria.addFilterClause(ObjectFilterConjunction.AND, "$TASK_ID", ObjectFilterOperator.IN, this.includedTaskTypes);
        }

        // Add the filter for claimed status.
        if (!includeClaimedTasks)
        {
            filterCriteria.addFilterClause(ObjectFilterConjunction.AND, "$TIME_CLAIMED", ObjectFilterOperator.IS_NULL, null);
        }

        // Add the filter for unclaimed status.
        if (!includeUnclaimedTasks)
        {
            filterCriteria.addFilterClause(ObjectFilterConjunction.AND, "$TIME_CLAIMED", ObjectFilterOperator.IS_NOT_NULL, null);
        }

        // Add the filter for issued status.
        filterCriteria.addFilterClause(ObjectFilterConjunction.AND, "$TIME_ISSUED", ObjectFilterOperator.IS_NOT_NULL, null);

        // Add the filter for completed status.
        filterCriteria.addFilterClause(ObjectFilterConjunction.AND, "$TIME_COMPLETED", ObjectFilterOperator.IS_NULL, null);
        return filter;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;
        toStringBuilder = new StringBuilder("T8TaskFilter{");
        toStringBuilder.append("flowInstanceIdentifier=").append(this.flowIid);
        toStringBuilder.append(",userIdentifier=").append(this.userId);
        toStringBuilder.append(",profileIdentifiers=").append(this.profileIds);
        toStringBuilder.append(",includeClaimedTasks=").append(this.includeClaimedTasks);
        toStringBuilder.append(",includeUnclaimedTasks=").append(this.includeUnclaimedTasks);
        toStringBuilder.append(",includedTaskTypes=").append(this.includedTaskTypes);
        toStringBuilder.append(",taskOrdering=").append(this.taskOrdering);
        toStringBuilder.append(",pageOffset=").append(this.pageOffset);
        toStringBuilder.append(",pageSize=").append(this.pageSize);
        toStringBuilder.append(",descriptivePropertySearchString=").append(this.descriptivePropertySearchString);
        toStringBuilder.append('}');
        return toStringBuilder.toString();
    }
}
