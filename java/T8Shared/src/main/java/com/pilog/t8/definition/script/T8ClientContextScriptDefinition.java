package com.pilog.t8.definition.script;

import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8ClientContextScriptDefinition extends T8ScriptDefinition
{
    public T8Script getNewScriptInstance(T8Context context) throws Exception;
}
