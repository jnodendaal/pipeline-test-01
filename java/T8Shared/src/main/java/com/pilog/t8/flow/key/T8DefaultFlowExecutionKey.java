package com.pilog.t8.flow.key;

import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowNodeState;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultFlowExecutionKey implements T8FlowExecutionKey
{
    protected String flowIdentifier;
    protected String flowInstanceIdentifier;
    protected String nodeIdentifier;
    protected String nodeInstanceIdentifier;

    public T8DefaultFlowExecutionKey(T8FlowNodeState nodeState)
    {
        this.flowIdentifier = nodeState.getFlowId();
        this.flowInstanceIdentifier = nodeState.getFlowIid();
        this.nodeIdentifier = nodeState.getNodeId();
        this.nodeInstanceIdentifier = nodeState.getNodeIid();
    }
    
    public T8DefaultFlowExecutionKey(String flowIdentifier, String flowInstanceIdentifier, String nodeIdentifier, String nodeInstanceIdentifier)
    {
        this.flowIdentifier = flowIdentifier;
        this.flowInstanceIdentifier = flowInstanceIdentifier;
        this.nodeIdentifier = nodeIdentifier;
        this.nodeInstanceIdentifier = nodeInstanceIdentifier;
    }

    @Override
    public String getFlowIdentifier()
    {
        return flowIdentifier;
    }

    @Override
    public String getFlowInstanceIdentifier()
    {
        return flowInstanceIdentifier;
    }

    @Override
    public String getNodeIdentifier()
    {
        return nodeIdentifier;
    }

    @Override
    public String getNodeInstanceIdentifier()
    {
        return nodeInstanceIdentifier;
    }
}
