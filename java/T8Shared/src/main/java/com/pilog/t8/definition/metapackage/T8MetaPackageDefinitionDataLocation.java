package com.pilog.t8.definition.metapackage;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8MetaPackageDefinitionDataLocation implements Serializable
{
    // Standard ID to ensure compatible serialization.
    static final long serialVersionUID = 1L;
    
    private String definitionIdentifier;
    private long offset;
    private int size;

    public T8MetaPackageDefinitionDataLocation(String definitionIdentifier, long offset, int size)
    {
        this.definitionIdentifier = definitionIdentifier;
        this.offset = offset;
        this.size = size;
    }

    public String getDefinitionIdentifier()
    {
        return definitionIdentifier;
    }

    public void setDefinitionIdentifier(String definitionIdentifier)
    {
        this.definitionIdentifier = definitionIdentifier;
    }

    public long getOffset()
    {
        return offset;
    }

    public void setOffset(long offset)
    {
        this.offset = offset;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }
}
