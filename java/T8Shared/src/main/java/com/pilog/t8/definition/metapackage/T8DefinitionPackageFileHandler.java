package com.pilog.t8.definition.metapackage;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.json.WriterConfig;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionSerializer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Pieter Strydom
 */
public class T8DefinitionPackageFileHandler
{
    private final T8DefinitionSerializer serializer;

    private SecretKeySpec secretKey;
    private Cipher encryptCipher;
    private Cipher decryptCipher;
    private BufferedWriter outputWriter;
    private InputStreamReader inputReader;
    private JsonObject jsonRoot;

    public T8DefinitionPackageFileHandler(T8DefinitionSerializer serializer)
    {
        this.serializer = serializer;
    }

    public boolean openFile(String filePath, boolean createNewFile) throws Exception
    {
        // Check that an encryption key has been set.
        if (secretKey == null) throw new Exception("No encryption key set.");

        if (!createNewFile)
        {
            // Read the data file index from the opened data file.
            inputReader = new InputStreamReader(new FileInputStream(filePath));
            return true;
        }
        else
        {
            // Write a Long value to the start of the new file, to reserve a location where the data file index offset will later be stored.
            outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(filePath)), "UTF-8"));
            return true;
        }
    }

    public List<String> getPackageIdentifiers() throws IOException
    {
        List<String> packageIds;
        JsonArray jsonPackages;

        packageIds = new ArrayList<>();

        if (jsonRoot == null) this.jsonRoot = JsonObject.readFrom(inputReader);

        jsonPackages = jsonRoot.getJsonArray("packages");
        for (JsonValue jsonPackageValue : jsonPackages)
        {
            packageIds.add(jsonPackageValue.asObject().getString("packageId"));
        }

        return packageIds;
    }

    public List<String> getPackageDefinitionIdentifiers(String packageId) throws IOException
    {
        List<String> definitionIds;
        JsonArray jsonPackages;

        definitionIds = new ArrayList<>();

        if (jsonRoot == null) this.jsonRoot = JsonObject.readFrom(inputReader);

        jsonPackages = jsonRoot.getJsonArray("packages");

        for (JsonValue jsonPackageValue : jsonPackages)
        {
            String jsonPackageId;
            JsonObject packageObject;

            packageObject = jsonPackageValue.asObject();
            jsonPackageId = packageObject.getString("packageId");

            if (jsonPackageId != null && jsonPackageId.equals(packageId))
            {
                JsonArray packageDefinitions;

                packageDefinitions = packageObject.getJsonArray("content");

                for (JsonValue definitionValue : packageDefinitions)
                {
                    JsonObject metaObject;

                    metaObject = definitionValue.asObject().getJsonObject("META");

                    definitionIds.add(metaObject.getString("IDENTIFIER"));
                }
            }
        }

        return definitionIds;
    }

    public void writeDefinitions(T8DefinitionManager definitionManager, HashMap<String, List<T8Definition>> definitions) throws Exception
    {
        JsonArray jsonPackages;
        JsonObject jsonRootObject;

        // Read the root json object and process the content.
        jsonRootObject = new JsonObject();
        jsonPackages = new JsonArray();
        jsonRootObject.add("packages", jsonPackages);
        for (String key : definitions.keySet())
        {
            String packageIdentifier;
            JsonArray jsonContentDefinitions;
            JsonObject jsonPackage;

            packageIdentifier = key;

            // Add the json objects to the model.
            jsonPackage = new JsonObject();
            jsonPackage.add("packageId", packageIdentifier);
            jsonContentDefinitions = new JsonArray();
            jsonPackage.add("content", jsonContentDefinitions);
            jsonPackages.add(jsonPackage);

            // Serialize all of the definitions contained by the project.
            for (T8Definition contentDefinition : definitions.get(key))
            {
                JsonObject jsonContentDefinition;

                // Deserialize the content definition.
                jsonContentDefinition = serializer.serialize(contentDefinition);
                jsonContentDefinitions.add(jsonContentDefinition);
            }
        }

        // Write the root object to the output stream.
        jsonRootObject.writeTo(outputWriter, WriterConfig.PRETTY_PRINT);
    }

    public T8Definition readDefinition(T8DefinitionManager definitionManager, String packageId, String definitionId) throws Exception
    {
        JsonArray jsonPackages;
        T8Definition contentDefinition = null;

        if (jsonRoot == null) this.jsonRoot = JsonObject.readFrom(inputReader);

        jsonPackages = jsonRoot.getJsonArray("packages");

        for (JsonValue jsonPackageValue : jsonPackages)
        {
            String jsonPackageId;
            JsonObject packageObject;

            packageObject = jsonPackageValue.asObject();
            jsonPackageId = packageObject.getString("packageId");

            if (jsonPackageId != null && jsonPackageId.equals(packageId))
            {
                JsonArray packageDefinitions;

                packageDefinitions = packageObject.getJsonArray("content");

                for (JsonValue definitionValue : packageDefinitions)
                {
                    JsonObject metaObject;
                    JsonObject definitionObject;
                    String jsonDefinitionId;

                    definitionObject = definitionValue.asObject();
                    metaObject = definitionObject.getJsonObject("META");

                    jsonDefinitionId = metaObject.getString("IDENTIFIER");

                    if(jsonDefinitionId != null && jsonDefinitionId.equals(definitionId))
                    {
                        contentDefinition = serializer.deserializeDefinition(definitionObject);
                        return contentDefinition;
                    }
                }
            }
        }

        return contentDefinition;
    }

    public void closeFile() throws Exception
    {
        if (outputWriter != null) outputWriter.close();
        if (inputReader != null) inputReader.close();
    }

    public void setSecretKey(byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
        secretKey = new SecretKeySpec(keyBytes, "AES");

        encryptCipher = Cipher.getInstance("AES");
        encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);

        decryptCipher = Cipher.getInstance("AES");
        decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
    }

    private byte[] encryptBytes(byte[] unencryptedBytes) throws IllegalBlockSizeException, BadPaddingException
    {
        return encryptCipher.doFinal(unencryptedBytes);
    }

    private byte[] decryptBytes(byte[] encryptedBytes) throws IllegalBlockSizeException, BadPaddingException
    {
        return decryptCipher.doFinal(encryptedBytes);
    }
}