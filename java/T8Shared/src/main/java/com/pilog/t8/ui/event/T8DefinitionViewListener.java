package com.pilog.t8.ui.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionViewListener extends EventListener
{
    public void headerChanged(T8ViewHeaderChangedEvent event);
    public void selectionChanged(T8DefinitionSelectionEvent event);
    public void definitionLinkActivated(T8DefinitionLinkEvent event);
}
