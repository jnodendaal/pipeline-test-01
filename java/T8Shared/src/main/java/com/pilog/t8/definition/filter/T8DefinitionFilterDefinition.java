package com.pilog.t8.definition.filter;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.filter.T8DefinitionFilter;
import com.pilog.t8.definition.data.filter.T8DataFilterCriteriaDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFilterDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_FILTER";
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_FILTER";
    public static final String IDENTIFIER_PREFIX = "FILTER_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Definition Filter";
    public static final String DESCRIPTION = "An object that specifies filter criteria to be used for filtering T8 Definitions.";
    public static final String VERSION = "0";
    private enum Datum {FILTER_CRITERIA_DEFINITION};
    // -------- Definition Meta-Data -------- //
    
    public T8DefinitionFilterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.FILTER_CRITERIA_DEFINITION.toString(), "Filter Criteria", "The filter criteria specified by this data filter."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILTER_CRITERIA_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataFilterCriteriaDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public T8DefinitionFilter getNewDefinitionFilterInstance(T8SessionContext sessionContext)
    {
        return getNewDefinitionFilterInstance(sessionContext, null);
    }
    
    public T8DefinitionFilter getNewDefinitionFilterInstance(T8SessionContext sessionContext, Map<String, Object> filterParameters)
    {
        T8DefinitionFilterCriteriaDefinition filterCriteriaDefinition;
        T8DefinitionFilter definitionFilter;
        
        definitionFilter = new T8DefinitionFilter(this);
        filterCriteriaDefinition = getFilterCriteriaDefinition();
        if (filterCriteriaDefinition != null) definitionFilter.setFilterCriteria(filterCriteriaDefinition.getNewDefinitionFilterCriteriaInstance(sessionContext, filterParameters));
        
        return definitionFilter;
    }
    
    public T8DefinitionFilterCriteriaDefinition getFilterCriteriaDefinition()
    {
        return (T8DefinitionFilterCriteriaDefinition)getDefinitionDatum(Datum.FILTER_CRITERIA_DEFINITION.toString());
    }

    public void setFilterCriteriaDefinition(T8DefinitionFilterCriteriaDefinition definition)
    {
        setDefinitionDatum(Datum.FILTER_CRITERIA_DEFINITION.toString(), definition);
    }
}
