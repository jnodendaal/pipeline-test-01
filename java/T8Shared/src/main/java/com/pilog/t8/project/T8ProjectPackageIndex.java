package com.pilog.t8.project;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionSerializer;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectPackageIndex
{
    private final List<T8DefinitionMetaData> projectMetaData;
    private final List<T8DefinitionMetaData> contentMetaData;

    public T8ProjectPackageIndex()
    {
        this.projectMetaData = new ArrayList<>();
        this.contentMetaData = new ArrayList<>();
    }

    public List<String> getProjectIds()
    {
        List<String> projectIds;

        projectIds = new ArrayList<>();
        for (T8DefinitionMetaData metaData : projectMetaData)
        {
            projectIds.add(metaData.getId());
        }

        return projectIds;
    }

    public List<T8DefinitionMetaData> getProjectContentMetaData(String projectId)
    {
        List<T8DefinitionMetaData> metaData;

        metaData = new ArrayList<>();
        for (T8DefinitionMetaData contentMeta : contentMetaData)
        {
            if (projectId.equals(contentMeta.getProjectId()))
            {
                metaData.add(contentMeta);
            }
        }

        return metaData;
    }

    public void read(Reader reader) throws Exception
    {
        T8DefinitionSerializer serializer;
        JsonArray jsonProjects;
        JsonObject jsonRoot;

        // Clear the current index.
        projectMetaData.clear();
        contentMetaData.clear();

        // Get the serializer to use for reading of JSON definition data.
        serializer = new T8DefinitionSerializer();

        // Read the root json object and process the content.
        jsonRoot = JsonObject.readFrom(reader);
        jsonProjects = jsonRoot.getJsonArray("projects");
        for (JsonValue jsonProjectValue : jsonProjects)
        {
            T8DefinitionMetaData projectMeta;
            JsonObject jsonProjectDefinition;
            JsonArray jsonContentDefinitions;
            JsonObject jsonProject;

            // Get the json objects from the model.
            jsonProject = jsonProjectValue.asObject();
            jsonProjectDefinition = jsonProject.getJsonObject("definition");
            jsonContentDefinitions = jsonProject.getJsonArray("content");

            // Deserializer the project meta data and add it the collection.
            projectMeta = serializer.deserializeDefinitionMetaData(jsonProjectDefinition.getJsonObject("META"));
            projectMetaData.add(projectMeta);

            // Deserializer meta data for all content definitions.
            for (JsonValue jsonContentDefinition : jsonContentDefinitions)
            {
                T8DefinitionMetaData definitionMeta;

                definitionMeta = serializer.deserializeDefinitionMetaData(jsonContentDefinition.asObject().getJsonObject("META"));
                contentMetaData.add(definitionMeta);
            }
        }
    }
}
