package com.pilog.t8.definition.data.source.sql;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import com.pilog.t8.definition.data.source.cte.T8CTEDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8SQLDataSourceDefinition extends T8DataSourceDefinition implements T8SQLQueryDataSourceDefinition, T8DataConnectionBasedSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_SQL";
    public static final String DISPLAY_NAME = "SQL Data Source";
    public static final String DESCRIPTION = "A data source that reads using custom SQL.";
    public enum Datum {CONNECTION_IDENTIFIER,
                       QUERY_TIMEOUT,
                       SQL_WITH_STRING,
                       SQL_SELECT_STRING,
                       SQL_PARAMETER_EXPRESSION_MAP,
                       CTE_DEFINITION_IDENTIFIERS};
    // -------- Definition Meta-Data -------- //

    public T8SQLDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection to which this data source will connect to retrieve its data."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.CTE_DEFINITION_IDENTIFIERS.toString(), "CTE Definitions", "The CTE's that will be added to this query when it is executed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.QUERY_TIMEOUT.toString(), "Query Timeout", "The number of seconds allowed for queries on this data source to execute before being terminated.", 60));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SQL_WITH_STRING.toString(), "SQL With String", "The SQL With clause that will be prepended to the select string."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SQL_SELECT_STRING.toString(), "SQL Select String", "The SQL Select clause that will be used to retrieve the data source result set."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION_VALUE_MAP, Datum.SQL_PARAMETER_EXPRESSION_MAP.toString(), "SQL Parameter Expressions", "The parameters used in the SQL Strings of this data source and their corresponding value expressions."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else if (Datum.CTE_DEFINITION_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CTEDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.SQL_WITH_STRING.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.sql.T8SQLDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else if ((Datum.SQL_SELECT_STRING.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.sql.T8SQLDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction accessProvider)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.source.sql.T8SQLDataSource", new Class<?>[]{T8SQLDataSourceDefinition.class, T8DataTransaction.class}, this, accessProvider);
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return getConnectionIdentifier();
    }

    public String getConnectionIdentifier()
    {
        return getDefinitionDatum(Datum.CONNECTION_IDENTIFIER);
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER, connectionIdentifier);
    }

    public int getQueryTimeout()
    {
        Integer value;

        value = (Integer)getDefinitionDatum(Datum.QUERY_TIMEOUT.toString());
        return value != null ? value : 0;
    }

    public void setQueryTimeout(int timeout)
    {
        setDefinitionDatum(Datum.QUERY_TIMEOUT.toString(), timeout);
    }

    public String getSQLWithString()
    {
        return getDefinitionDatum(Datum.SQL_WITH_STRING);
    }

    public void setSQLWithString(String sql)
    {
        setDefinitionDatum(Datum.SQL_WITH_STRING, sql);
    }

    public String getSQLSelectString()
    {
        return getDefinitionDatum(Datum.SQL_SELECT_STRING);
    }

    public void setSQLSelectString(String sql)
    {
        setDefinitionDatum(Datum.SQL_SELECT_STRING, sql);
    }

    public Map<String, String> getSQLParameterExpressionMap()
    {
        return getDefinitionDatum(Datum.SQL_PARAMETER_EXPRESSION_MAP);
    }

    public void setSQLParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.SQL_PARAMETER_EXPRESSION_MAP, expressionMap);
    }

    public List<String> getCTEDefinitionIdentifiers()
    {
        return getDefinitionDatum(Datum.CTE_DEFINITION_IDENTIFIERS);
    }

    public void setCTEDefinitionIdentifiers(List<String> definitions)
    {
        setDefinitionDatum(Datum.CTE_DEFINITION_IDENTIFIERS, definitions);
    }

    @Override
    public StringBuilder createQuery(T8DataTransaction dataAccessProvider, T8DataFilter dataFilter, Map<String, String> fieldMapping)
    {
        StringBuilder subQuery;

        subQuery = new StringBuilder();
        subQuery.append("SELECT ");
        if ((fieldMapping != null) && (fieldMapping.size() > 0))
        {
            T8DataEntityDefinition dataEntityDefinition;
            T8DataSourceDefinition dataSourceDefinition;
            Iterator<String> fieldIterator;

            fieldIterator = fieldMapping.keySet().iterator();
            while (fieldIterator.hasNext())
            {
                String fieldIdentifier;
                String fieldSourceIdentifier;

                fieldIdentifier = fieldIterator.next();
                dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier));
                dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
                fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
                subQuery.append(fieldSourceIdentifier);
                subQuery.append(" AS ");

                fieldIdentifier = fieldMapping.get(fieldIdentifier);
                dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier));
                dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
                fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
                subQuery.append(fieldSourceIdentifier);

                if (fieldIterator.hasNext()) subQuery.append(", ");
            }
        }
        else // No Field mapping so just retrieve all fields using original identifiers.
        {
            subQuery.append("*");
        }

        subQuery.append(" FROM (");
        subQuery.append(getSQLSelectString());
        subQuery.append(") selectResults");
        if ((dataFilter != null) && (dataFilter.hasFilterCriteria()))
        {
            subQuery.append(" ");
            subQuery.append(dataFilter.getWhereClause(dataAccessProvider, "selectResults"));
        }

        return subQuery;
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        // Check the connection identifier
        if (Strings.isNullOrEmpty(getConnectionIdentifier()))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.CONNECTION_IDENTIFIER.toString(), "Connection identifier not specified.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }
}
