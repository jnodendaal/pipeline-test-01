package com.pilog.t8.security;

import com.pilog.t8.definition.security.T8DataAccessDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataAccess
{
    private final Map<String, String> dataRecordAccessMap;

    public T8DataAccess(T8DataAccessDefinition definition)
    {
        this.dataRecordAccessMap = definition.getDataRecordAccessMap();
    }

    public String getAccessLayerId(String drInstanceId)
    {
        return this.dataRecordAccessMap.get(drInstanceId);
    }

    /**
     * Temporary method to be removed once access maintenance in T8Context has been updated.
     */
    Map<String, String> getDataAccessMap()
    {
        return new HashMap<>(dataRecordAccessMap);
    }
}
