package com.pilog.t8.definition.event;

/**
 * @author Hennie Brink
 */
public class T8ChangeLogHandlerAPIHandler
{
    public static final String EF_EVENT_IID = "$EVENT_IID";
    public static final String EF_EVENT = "$EVENT";
    public static final String EF_DEFINITION_ID = "$DEFINITION_ID";
    public static final String EF_TIME = "$TIME";
    public static final String EF_USER_ID = "$USER_ID";
    public static final String EF_DATA = "$DATA";
}
