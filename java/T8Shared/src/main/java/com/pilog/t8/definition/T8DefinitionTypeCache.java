package com.pilog.t8.definition;

import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionTypeCache
{
    public void clearCache();
    public void cacheDefinitionTypes();
    public int getCachedDefinitionTypeCount();
    public List<String> getTypeIdentifiers();
    public List<String> getTypeIdentifiers(String groupIdentifier);
    public List<String> getGroupIdentifiers();
    public List<T8DefinitionGroupMetaData> getDefinitionGroupMetaData();
    public List<T8DefinitionTypeMetaData> getDefinitionTypeMetaData();
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeIdentifier);
    public List<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupIdentifier);
}
