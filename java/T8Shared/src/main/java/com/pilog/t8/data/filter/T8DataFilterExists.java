package com.pilog.t8.data.filter;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterExistsDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterExistsDefinition.ExistsOperator;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Hennie Brink
 */
public class T8DataFilterExists implements T8DataFilterClause, Serializable
{
    private String identifier;
    private String dataEntityIdentifier;
    private T8DataFilter existsFilter;
    private Map<String, String> existsMapping;
    private ExistsOperator operator;

    public T8DataFilterExists(String dataEntityIdentifier)
    {
        this.dataEntityIdentifier = dataEntityIdentifier;
        this.operator = ExistsOperator.EXISTS;
    }

    public T8DataFilterExists(String dataEntityIdentifier, T8DataFilter existsFilter, Map<String, String> existsMapping)
    {
        this(dataEntityIdentifier);
        this.existsFilter = existsFilter;
        this.existsMapping = existsMapping;
    }

    public T8DataFilterExists(String identifier, String dataEntityIdentifier)
    {
        this(dataEntityIdentifier);
        this.identifier = identifier;
    }

    public T8DataFilterExists(T8DataFilterExistsDefinition definition)
    {
        this(definition.getIdentifier(), definition.getDataEntityIdentifier());
        this.operator = definition.getOperator();
        this.existsMapping = definition.getExistsFilterMapping();
    }


    @Override
    public T8DataFilterClause copy()
    {
        T8DataFilterExists newFilter;

        newFilter = new T8DataFilterExists(identifier, dataEntityIdentifier);
        newFilter.existsFilter = existsFilter.copy();
        newFilter.existsMapping = new HashMap<>(existsMapping);
        newFilter.operator = operator;

        return newFilter;
    }

    @Override
    public T8DataFilterCriterion getFilterCriterion(String identifier)
    {
        return existsFilter.getFilterCriterion(identifier);
    }

    @Override
    public List<T8DataFilterCriterion> getFilterCriterionList()
    {
        return existsFilter.getFilterCriterionList();
    }

    @Override
    public boolean hasCriteria()
    {
        return true;
    }
    
    @Override
    public StringBuffer getWhereClause(T8DataTransaction dataAccessProvider, String sqlIdentifier)
    {
        T8DataEntityDefinition parentFilterEntityDefinition;
        T8DataSourceDefinition parentFilterSourceDefinition;
        T8DataSourceDefinition subFilterSourceDefinition;
        T8DataEntityDefinition subFilterEntityDefinition;
        StringBuffer whereClause;

        whereClause = new StringBuffer();
        try
        {
            parentFilterEntityDefinition = dataAccessProvider.getDataEntityDefinition(getEntityIdentifier());
            parentFilterSourceDefinition = dataAccessProvider.getDataSourceDefinition(parentFilterEntityDefinition.getDataSourceIdentifier());
            subFilterEntityDefinition = dataAccessProvider.getDataEntityDefinition(existsFilter.getEntityIdentifier());
            subFilterSourceDefinition = dataAccessProvider.getDataSourceDefinition(subFilterEntityDefinition.getDataSourceIdentifier());
        }
        catch (Exception e)
        {
            throw new RuntimeException("Unable to fetch data access definitions from access provider.", e);
        }

        if (subFilterSourceDefinition instanceof T8SQLQueryDataSourceDefinition)
        {
            whereClause.append((operator == ExistsOperator.EXISTS) ? " EXISTS (SELECT * " : " NOT EXISTS (SELECT * ");
            whereClause.append(" FROM (");
            whereClause.append(((T8SQLQueryDataSourceDefinition)subFilterSourceDefinition).createQuery(dataAccessProvider, existsFilter, null));
            whereClause.append(") existFilter ");
        }
        else throw new RuntimeException("Cannot create exist-query from data source: " + subFilterSourceDefinition);

        if(existsMapping != null && !existsMapping.isEmpty()) whereClause.append("WHERE ");

        for (Iterator<String> it = existsMapping.keySet().iterator(); it.hasNext();)
        {
            String soureFieldIdentifier = it.next();
            String parentFieldSourceIdentifier;
            String existsFieldSourceIdentifier;
            String destinationFieldIdentifier;

            destinationFieldIdentifier = existsMapping.get(soureFieldIdentifier);

            parentFieldSourceIdentifier = parentFilterSourceDefinition.mapFieldToSourceIdentifier(parentFilterEntityDefinition.mapFieldToSourceIdentifier(soureFieldIdentifier));
            existsFieldSourceIdentifier = subFilterSourceDefinition.mapFieldToSourceIdentifier(subFilterEntityDefinition.mapFieldToSourceIdentifier(destinationFieldIdentifier));

            whereClause.append(sqlIdentifier);
            whereClause.append(".");
            whereClause.append(parentFieldSourceIdentifier);
            whereClause.append(" = ");
            whereClause.append("existFilter.");
            whereClause.append(existsFieldSourceIdentifier);
            if(it.hasNext()) whereClause.append(" AND ");
        }

        whereClause.append(")");

        return whereClause;
    }

    @Override
    public List<Object> getWhereClauseParameters(T8DataTransaction dataAccessProvider)
    {
        return existsFilter.getWhereClauseParameters(dataAccessProvider);
    }

    @Override
    public boolean includesEntity(T8DataEntity entity)
    {
        return existsFilter.includesEntity(entity);
    }

    @Override
    public Set<String> getFilterFieldIdentifiers()
    {
        return existsFilter.getFilterFieldIdentifiers();
    }

    @Override
    public String getEntityIdentifier()
    {
        return dataEntityIdentifier;
    }

    @Override
    public void setEntityIdentifier(String entityIdentifier)
    {
        this.dataEntityIdentifier = entityIdentifier;
    }

    public T8DataFilter getExistsFilter()
    {
        return existsFilter;
    }

    public void setExistsFilter(T8DataFilter existsFilter)
    {
        this.existsFilter = existsFilter;
    }

    public Map<String, String> getExistsMapping()
    {
        return existsMapping;
    }

    public void setExistsMapping(Map<String, String> existsMapping)
    {
        this.existsMapping = existsMapping;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public ExistsOperator getOperator()
    {
        return operator;
    }

    public void setOperator(ExistsOperator operator)
    {
        this.operator = operator;
    }

}
