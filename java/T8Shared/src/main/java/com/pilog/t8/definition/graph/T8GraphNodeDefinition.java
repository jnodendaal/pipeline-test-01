package com.pilog.t8.definition.graph;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8GraphNodeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {X_COORDINATE, Y_COORDINATE};
    // -------- Definition Meta-Data -------- //
    
    public T8GraphNodeDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.X_COORDINATE.toString(), "X Coordinate", "A datum used for storing a x-coordinate that may be used when displaying this node on a visual representation of the graph.", -1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.Y_COORDINATE.toString(), "Y Coordinate", "A datum used for storing a y-coordinate that may be used when displaying this node on a visual representation of the graph.", -1));
        
        // Hide these datums, since they will be edited by the system and not the user.
        for (T8DefinitionDatumType datumType : datumTypes) datumType.setHidden(true);
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }
    
    public int getXCoordinate()
    {
        Integer coordinate;
        
        coordinate = (Integer)getDefinitionDatum(Datum.X_COORDINATE.toString());
        return coordinate != null ? coordinate : -1;
    }
    
    public void setXCoordinate(int coordinate)
    {
        setDefinitionDatum(Datum.X_COORDINATE.toString(), coordinate);
    }
    
    public int getYCoordinate()
    {
        Integer coordinate;
        
        coordinate = (Integer)getDefinitionDatum(Datum.Y_COORDINATE.toString());
        return coordinate != null ? coordinate : -1;
    }
    
    public void setYCoordinate(int coordinate)
    {
        setDefinitionDatum(Datum.Y_COORDINATE.toString(), coordinate);
    }
}
