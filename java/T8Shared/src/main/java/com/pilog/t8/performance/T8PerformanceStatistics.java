package com.pilog.t8.performance;

import java.io.OutputStream;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8PerformanceStatistics
{
    public String getIdentifier();
    public boolean isEnabled();
    public void setEnabled(boolean enabled);
    public void reset(); // Resets all recorded statistics.
    public void printStatistics(OutputStream stream);

    public List<T8ThreadPerformanceStatistics> getThreadPerformanceStatistics();

    public void logEvent(String eventIdentifier);
    public void logExecutionStart(String executionIdentifier);
    public void logExecutionEnd(String executionIdentifier);
}
