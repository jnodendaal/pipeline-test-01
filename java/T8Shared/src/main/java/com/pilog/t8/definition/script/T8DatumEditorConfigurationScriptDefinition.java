package com.pilog.t8.definition.script;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DatumEditorConfigurationScriptDefinition extends T8DefaultScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_DATUM_EDITOR_CONFIGURATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_DATUM_EDITOR_CONFIGURATION";
    public static final String IDENTIFIER_PREFIX = "SDC_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Datum Editor Configuration Script";
    public static final String DESCRIPTION = "A script that is executed within the context of a definition editor in order to configure a specific datum editor when it is initialized/refreshed.";
    public static final String VERSION = "0";
    // -------- Definition Meta-Data -------- //
    
    public static final String PARAMETER_DEFINITION = "$P_DEFINITION";
    public static final String PARAMETER_DEFINITION_IDENTIFIER = "$P_DEFINITION_IDENTIFIER";
    public static final String PARAMETER_DATUM_IDENTIFIER = "$P_DATUM_IDENTIFIER";
    public static final String PARAMETER_DATUM_TYPE = "$P_DATUM_TYPE";
    
    public T8DatumEditorConfigurationScriptDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        List<T8ScriptMethodImport> methodImports;

        methodImports = new ArrayList<T8ScriptMethodImport>();
        methodImports.add(new T8ScriptMethodImport("getDefinitionDatum", this, this.getClass().getDeclaredMethod("getDefinitionDatum", T8Definition.class, String.class)));
        methodImports.add(new T8ScriptMethodImport("getParentDefinitionDatum", this, this.getClass().getDeclaredMethod("getParentDefinitionDatum", T8Definition.class, String.class)));
        methodImports.add(new T8ScriptMethodImport("getAncestorDefinitionDatum", this, this.getClass().getDeclaredMethod("getAncestorDefinitionDatum", T8Definition.class, String.class, String.class)));
        return methodImports;
    }
    
    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        
        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_DEFINITION_IDENTIFIER, "Definition Identifier", "The identifier of the definition on which the datum editor is applied.", T8DataType.DEFINITION));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Definition", "The definition on which the datum editor is applied.", T8DataType.DEFINITION));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DATUM_IDENTIFIER, "Datum Identifier", "The identifier of the datum on which the datum editor is applied.", T8DataType.DEFINITION));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DATUM_TYPE, "Datum Type", "The datum type on which the datum editor is applied.", T8DataType.DEFINITION));
        return definitions;
    }
    
    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }
    
    public Object getDefinitionDatum(T8Definition definition, String datumIdentifier)
    {
        return definition.getDefinitionDatum(datumIdentifier);
    }
    
    public Object getParentDefinitionDatum(T8Definition definition, String datumIdentifier)
    {
        T8Definition parentDefinition;
        
        parentDefinition = definition.getParentDefinition();
        if (parentDefinition != null)
        {
            return parentDefinition.getDefinitionDatum(datumIdentifier);
        }
        else return null;
    }
    
    public Object getAncestorDefinitionDatum(T8Definition definition, String ancestorTypeIdentifier, String datumIdentifier)
    {
        return definition.getAncestorDefinitionDatum(ancestorTypeIdentifier, datumIdentifier);
    }
}
