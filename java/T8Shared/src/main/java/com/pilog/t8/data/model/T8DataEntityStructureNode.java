package com.pilog.t8.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityStructureNode implements Serializable
{
    private String id;
    private T8DataEntityStructureNode parentNode;
    private List<T8DataEntityStructureNode> childNodes;
    private Map<String, Map<String, String>> childFieldRelationships;
    
    public T8DataEntityStructureNode(String id)
    {
        this.id = id;
        this.parentNode = null;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    void setParentNode(T8DataEntityStructureNode parentNode)
    {
        this.parentNode = parentNode;
    }
    
    public T8DataEntityStructureNode getParentNode()
    {
        return parentNode;
    }
    
    public int countDescendants()
    {
        if (childNodes != null)
        {
            int descendants;

            descendants = childNodes.size();
            for (T8DataEntityStructureNode childNode : childNodes)
            {
                descendants += childNode.countDescendants();
            }

            return descendants;
        }
        else return 0;
    }
    
    /**
     * Returns the list of entity ID's from the root to this node.
     * @return The list of entity ID's from the root to this node.
     */
    public List<String> getEntityIdentifierPath()
    {
        T8DataEntityStructureNode parent;
        List<String> idList;
        
        idList = new ArrayList<String>();
        idList.add(getID());
        
        parent = this;
        while ((parent = parent.getParentNode()) != null)
        {
            idList.add(parent.getID());
        }
        
        Collections.reverse(idList);
        return idList;
    }
    
    public Map<String, String> getChildFieldRelationships(String childIdentifier)
    {
        return childFieldRelationships.get(childIdentifier);
    }
    
    public List<T8DataEntityStructureNode> getChildNodes()
    {
        ArrayList<T8DataEntityStructureNode> nodes;
        
        nodes = new ArrayList<T8DataEntityStructureNode>();
        if (childNodes != null)
        {
            nodes.addAll(childNodes);
        }
        
        return nodes;
    }
    
    public List<T8DataEntityStructureNode> getPathNodes()
    {
        List<T8DataEntityStructureNode> pathNodes;
        T8DataEntityStructureNode node;
        
        pathNodes = new ArrayList<T8DataEntityStructureNode>();
        pathNodes.add(this);
        node = this;
        while ((node = node.getParentNode()) != null)
        {
            pathNodes.add(node);
        }
        
        Collections.reverse(pathNodes);
        return pathNodes;
    }
    
    public List<T8DataEntityStructureNode> getDescendentNodes()
    {
        LinkedList<T8DataEntityStructureNode> nodeQueue;
        List<T8DataEntityStructureNode> descendants;
        
        descendants = new ArrayList<T8DataEntityStructureNode>();
        nodeQueue = new LinkedList<T8DataEntityStructureNode>();
        nodeQueue.add(this);
        while(nodeQueue.size() > 0)
        {
            T8DataEntityStructureNode nextNode;
            List<T8DataEntityStructureNode> childNodesList;
            
            nextNode = nodeQueue.pop();
            childNodesList = nextNode.getChildNodes();
            
            descendants.addAll(childNodesList);
            nodeQueue.addAll(childNodesList);
        }
        
        return descendants;
    }
    
    public T8DataEntityStructureNode findDescendantByID(String id)
    {
        if (this.id.equals(id)) return this;
        else if (childNodes != null)
        {
            for (T8DataEntityStructureNode child : childNodes)
            {
                T8DataEntityStructureNode descendant;
                
                descendant = child.findDescendantByID(id);
                if (descendant != null) return descendant;
            }
            
            return null;
        }
        else return null;
    }
    
    public void addChildNode(T8DataEntityStructureNode node, Map<String, String> fieldRelationships)
    {
        if (childNodes == null) childNodes = new ArrayList<T8DataEntityStructureNode>();
        if (childFieldRelationships == null) childFieldRelationships = new HashMap<String, Map<String, String>>();
        node.setParentNode(this);
        childNodes.add(node);
        childFieldRelationships.put(node.getID(), fieldRelationships);
    }
    
    public boolean removeChildNode(T8DataEntityStructureNode node)
    {
        if (childNodes != null)
        {
            boolean deleted;
            
            deleted = childNodes.remove(node);
            if (deleted)
            {
                node.setParentNode(null);
                childFieldRelationships.remove(node.getID());
            }
            
            return deleted;
        }
        else return false;
    }
}
