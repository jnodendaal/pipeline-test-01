package com.pilog.t8.definition;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonObject.Entry;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8Definition.T8DefinitionStatus;
import com.pilog.t8.definition.T8DefinitionComment.CommentType;
import com.pilog.t8.definition.patch.T8DefinitionPatch;
import com.pilog.t8.time.T8Date;
import com.pilog.t8.time.T8Time;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.ui.T8Image;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.patch.T8PatchDefinition;
import com.pilog.t8.utilities.codecs.Base64Codec;
import com.pilog.t8.utilities.compression.ByteCompressor;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionSerializer
{
    private final T8DefinitionManager definitionManager;
    private boolean serializeResourceDefinitions;
    private final List<T8DefinitionPatch> definitionPatches;

    public T8DefinitionSerializer()
    {
        this.definitionManager = null;
        this.serializeResourceDefinitions = false;
        this.definitionPatches = new ArrayList<T8DefinitionPatch>();
    }

    public T8DefinitionSerializer(T8DefinitionManager definitionManager)
    {
        this.definitionManager = definitionManager;
        this.serializeResourceDefinitions = false;
        this.definitionPatches = definitionManager.getDefinitionPatches();
    }

    public void setSerializeResourceDefinitions(boolean enabled)
    {
        serializeResourceDefinitions = enabled;
    }

    public String serializeDefinition(T8Definition definition) throws Exception
    {
        JsonObject definitionObject;

        // Construc the JSON object.
        definitionObject = serialize(definition);

        // Write the JSON object to string output.
        return definitionObject.toString();
    }

    public T8Definition deserializeDefinition(String definitionString) throws Exception
    {
        JsonObject definitionObject;
        String finalDefinitionString;
        T8Definition constructedDefinition;
        boolean patched = false;

        // Make sure that the definition manager was set.
        if (definitionManager == null) throw new Exception("Cannot deserialize definitions when a definition manager is not set.");

        // Apply Definition String patches.
        finalDefinitionString = definitionString;
        if (definitionPatches.size() > 0) // We want to avoid this section if possible for performance reasons.
        {
            // Make sure not to patch the patch definition itself :)
            if (!definitionString.contains(T8PatchDefinition.TYPE_IDENTIFIER))
            {
                for (T8DefinitionPatch patch : definitionPatches)
                {
                    finalDefinitionString = patch.patchDefinitionString(finalDefinitionString);
                }
            }

            // Compare the final definition String with the orginal to determine if any patch changes were performed.
            patched = (!finalDefinitionString.equals(definitionString));
        }

        // Read the JSON object from the input string.
        definitionObject = JsonObject.readFrom(finalDefinitionString);

        // Construct the definition from the patch definition String.
        constructedDefinition = deserializeDefinition(definitionObject);
        if (patched) constructedDefinition.setPatched(true);
        return constructedDefinition;
    }

    public T8Definition deserializeDefinition(JsonObject definitionObject) throws Exception
    {
        LinkedHashMap<String, Object> definitionData;
        T8DefinitionMetaData definitionMetaData;
        T8Definition newDefinition;
        Iterator entryIterator;
        JsonObject metaObject;
        String definitionId;
        String typeId;
        String unpatchedTypeId;
        boolean patched;

        // Make sure that the definition manager was set.
        if (definitionManager == null) throw new Exception("Cannot deserialize definitions when a definition manager is not set.");

        // Get the meta data from the definition object.
        metaObject = definitionObject.get("META").asObject();
        definitionMetaData = deserializeDefinitionMetaData(metaObject);

        // Apply any available patches.
        patched = false;
        definitionId = definitionMetaData.getId();
        typeId = definitionMetaData.getTypeId();
        unpatchedTypeId = typeId;
        for (T8DefinitionPatch definitionPatch : definitionPatches)
        {
            // Patch the definition data (datum identifiers etc.).
            //if (definitionPatch.patchDefinitionData(typeIdentifier, definitionData)) patched = true;

            // Now that the datums have been patched using the type identifier as reference, we can patch the type identifier itself.
            typeId = definitionPatch.patchTypeIdentifier(typeId);
        }

        // Make sure to set the patched flag if the type identifier was changed.
        if (!Objects.equals(unpatchedTypeId, typeId)) patched = true;

        // Construct the definition.
        definitionMetaData.setPatched(patched);
        newDefinition = definitionManager.createNewDefinition(definitionId, typeId);
        newDefinition.setDefinitionMetaData(definitionMetaData, false);

        // Construct the definition content data.
        definitionData = new LinkedHashMap<String, Object>();
        entryIterator = definitionObject.iterator();
        while (entryIterator.hasNext())
        {
            String datumId;
            Entry entry;

            // Get the entry key and value.
            entry = (Entry)entryIterator.next();
            datumId = entry.getName();

            // Exclude the 'META' entry because we've already processed it.
            if (!datumId.equals("META"))
            {
                JsonValue datumValue;
                T8DefinitionDatumType datumType;

                // Get the datum type.
                datumType = newDefinition.getDatumType(datumId);
                if (datumType != null)
                {
                    // Get the datum value and add it to the definition data map if not null.
                    datumValue = entry.getValue();
                    if (datumValue != null)
                    {
                        definitionData.put(datumId, deserialize(datumType.getDataType(), datumValue));
                    }
                }
                else
                {
                    // TODO: Try to patch the datum in order for it to fit the new definition.
                }
            }
        }

        // Set the definition content data.
        newDefinition.setDefinitionData(definitionData);
        return newDefinition;
    }

    public T8DefinitionMetaData deserializeDefinitionMetaData(JsonObject metaObject) throws Exception
    {
        T8DefinitionMetaData definitionMetaData;
        JsonArray jsonComments;
        String id;
        String description;
        String displayName;
        String typeId;
        String projectId;
        String versionId;
        String createdTimeString;
        String updatedTimeString;
        String createdUserId;
        String updatedUserId;
        String status;
        String checksum;
        String revision;

        // Get the meta data from the definition object.
        id = metaObject.getString("IDENTIFIER");
        description = metaObject.getString("DESCRIPTION");
        displayName = metaObject.getString("DISPLAY_NAME");
        typeId = metaObject.getString("TYPE");
        projectId = metaObject.getString("PROJECT");
        versionId = metaObject.getString("VERSION");
        checksum = metaObject.getString("CHECKSUM");
        createdTimeString = metaObject.getString("CREATED_TIME");
        updatedTimeString = metaObject.getString("UPDATED_TIME");
        createdUserId = metaObject.getString("CREATED_USER");
        updatedUserId = metaObject.getString("UPDATED_USER");
        status = metaObject.getString("STATUS");
        revision = metaObject.getString("REVISION");
        jsonComments = metaObject.getJsonArray("COMMENTS");

        // Construct the definition meta data.
        definitionMetaData =  new T8DefinitionMetaData();
        definitionMetaData.setId(id);
        definitionMetaData.setTypeId(typeId);
        definitionMetaData.setDisplayName(displayName);
        definitionMetaData.setDescription(description);
        definitionMetaData.setProjectId(Strings.isNullOrEmpty(projectId) ? null : projectId);
        definitionMetaData.setVersionId(Strings.isNullOrEmpty(versionId) ? null : Strings.isInteger(versionId) ? null : versionId); // A test for integer version ids is required to eliminate legacy version numbers.
        definitionMetaData.setChecksum(Strings.isNullOrEmpty(checksum) ? null : checksum);
        definitionMetaData.setRevision(Strings.isNullOrEmpty(revision) ? null : Long.parseLong(revision));
        definitionMetaData.setCreatedUserId(Strings.isNullOrEmpty(createdUserId) ? null : createdUserId);
        definitionMetaData.setUpdatedUserId(Strings.isNullOrEmpty(updatedUserId) ? null : updatedUserId);
        definitionMetaData.setCreatedTime(Strings.isNullOrEmpty(createdTimeString) ? null : Long.parseLong(createdTimeString));
        definitionMetaData.setUpdatedTime(Strings.isNullOrEmpty(updatedTimeString) ? null : Long.parseLong(updatedTimeString));
        definitionMetaData.setStatus(Strings.isNullOrEmpty(status) ? null : T8DefinitionStatus.valueOf(status));
        definitionMetaData.setPatched(false);

        // Deserialize the definition comments.
        if (jsonComments != null)
        {
            for (JsonObject jsonComment : jsonComments.objects())
            {
                T8DefinitionComment comment;

                comment = new T8DefinitionComment();
                comment.setUserId(jsonComment.getString("USER_ID"));
                comment.setText(jsonComment.getString("TEXT"));
                comment.setType(CommentType.valueOf(jsonComment.getString("TYPE")));
                comment.setTimestamp(new T8Timestamp(jsonComment.getLong("TIMESTAMP")));
                definitionMetaData.addComment(comment);
            }
        }

        // Return the final result.
        return definitionMetaData;
    }

    private Object deserialize(T8DataType dataType, JsonValue jsonValue) throws Exception
    {
        if (jsonValue == JsonValue.NULL)
        {
            return null;
        }
        else if (dataType.equals(T8DataType.UNDEFINED))
        {
            throw new RuntimeException("Cannot deserialize UNDEFINED data type.");
        }
        else if (dataType.equals(T8DataType.DEFINITION))
        {
            return deserializeDefinition((JsonObject)jsonValue);
        }
        else if (dataType.isType(T8DtList.IDENTIFIER))
        {
            ArrayList<Object> valueList;
            Iterator<JsonValue> listIterator;

            valueList = new ArrayList<Object>();
            listIterator = ((JsonArray)jsonValue).iterator();
            while (listIterator.hasNext())
            {
                valueList.add(deserialize(((T8DtList)dataType).getElementDataType(), listIterator.next()));
            }

            return valueList;
        }
        else if (dataType.isType(T8DtMap.IDENTIFIER))
        {
            LinkedHashMap<String, Object> mapData;
            Iterator<Entry> entryIterator;
            JsonObject mapObject;

            mapData = new LinkedHashMap<String, Object>();
            mapObject = (JsonObject)jsonValue;
            entryIterator = mapObject.iterator();
            while (entryIterator.hasNext())
            {
                Entry mapEntry;
                String key;

                mapEntry = entryIterator.next();
                key = mapEntry.getName();
                mapData.put(key, deserialize(((T8DtMap)dataType).getValueDataType(), mapEntry.getValue()));
            }

            return mapData;
        }
        else if (dataType.equals(T8DataType.DATA_ENTITY))
        {
            Iterator<Entry> fieldIterator;
            T8DataEntity entity;
            JsonObject entityObject;
            JsonObject fieldsObject;
            T8DataEntityDefinition entityDefinition;
            String entityId;
            String entityGUID;

            // Get the details of the entity.
            entityObject = (JsonObject)jsonValue;
            entityId = entityObject.getString("ENTITY_IDENTIFIER");
            entityGUID = entityObject.getString("ENTITY_GUID");
            fieldsObject = entityObject.get("FIELDS").asObject();

            // Retrieve the entity definition and construct the new entity object.
            entityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(null, null, entityId, null);
            entity = new T8DataEntity(entityDefinition);
            if ((entityGUID != null) && (entityGUID.length() > 0)) entity.setGUID(entityGUID);

            // Add all entity field values.
            fieldIterator = fieldsObject.iterator();
            while (fieldIterator.hasNext())
            {
                T8DataEntityFieldDefinition fieldDefinition;
                String fieldIdentifier;
                Entry fieldEntry;

                fieldEntry = fieldIterator.next();
                fieldIdentifier = fieldEntry.getName();

                fieldDefinition = entityDefinition.getFieldDefinition(fieldIdentifier);
                if (fieldDefinition != null)
                {
                    JsonValue fieldValue;

                    fieldValue = fieldEntry.getValue();
                    entity.setFieldValue(fieldIdentifier, deserialize(fieldDefinition.getDataType(), fieldValue));
                }
            }

            return entity;
        }
        else if (dataType.equals(T8DataType.DEFINITION_IDENTIFIER))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.DEFINITION_INSTANCE_IDENTIFIER))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.DEFINITION_TYPE_IDENTIFIER))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.EPIC_SCRIPT))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.IDENTIFIER_STRING))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.DATE))
        {
            return jsonValue.asLong();
        }
        else if (dataType.equals(T8DataType.DATE_TIME))
        {
            return jsonValue.asLong();
        }
        else if (dataType.equals(T8DataType.GUID))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.STRING))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.LONG_STRING))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.PASSWORD_HASH))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.COLOR_HEX))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.DISPLAY_STRING))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.EPIC_EXPRESSION))
        {
            return jsonValue.asString();
        }
        else if (dataType.equals(T8DataType.INTEGER))
        {
            return jsonValue.asInt();
        }
        else if (dataType.equals(T8DataType.FLOAT))
        {
            return jsonValue.asFloat();
        }
        else if (dataType.equals(T8DataType.DOUBLE))
        {
            return jsonValue.asDouble();
        }
        else if (dataType.equals(T8DataType.LONG))
        {
            return jsonValue.asLong();
        }
        else if (dataType.equals(T8DataType.BOOLEAN))
        {
            return jsonValue.asBoolean();
        }
        else if (dataType.equals(T8DataType.IMAGE))
        {
            return convertStringToImageData(jsonValue.asString());
        }
        else if (dataType.equals(T8DataType.BYTE_ARRAY))
        {
            return convertHexStringToBinaryData(jsonValue.asString());
        }
        else throw new RuntimeException("Invalid Definition Datum Dat Type encountered: '" + dataType);
    }

    public JsonObject serialize(T8Definition definition) throws Exception
    {
        JsonObject definitionObject;
        JsonObject metaObject;

        // Create the definition object.
        definitionObject = new JsonObject();

        // Add the meta object to the definition object.
        metaObject = T8DefinitionSerializer.this.serialize(definition.getMetaData());
        definitionObject.add("META", metaObject);

        // Add the rest of the definition datums to the definition object.
        for (T8DefinitionDatumType datumType : definition.getDatumTypes())
        {
            // Only serialize the datum if required.
            if (datumType.isPersisted())
            {
                String datumIdentifier;
                JsonValue datumObject;

                datumIdentifier = datumType.getIdentifier();
                datumObject = serializeToJSON(datumType.getDataType(), definition.getDefinitionDatum(datumIdentifier));
                if ((datumObject != null) && (datumObject != JsonValue.NULL))
                {
                    definitionObject.add(datumIdentifier, datumObject);
                }
            }
        }

        // Return the object.
        return definitionObject;
    }

    private JsonObject serialize(T8DefinitionMetaData metaData)
    {
        JsonObject jsonMetaData;
        String projectId;
        String versionId;
        Long revision;

        // Get some meta information from the definition.
        projectId = metaData.getProjectId();
        versionId = metaData.getVersionId();
        revision = metaData.getRevision();

        // Create the meta object.
        jsonMetaData = new JsonObject();
        jsonMetaData.add("IDENTIFIER", metaData.getId());
        jsonMetaData.add("TYPE", metaData.getTypeId());
        jsonMetaData.addIfNotNullOrEmpty("DISPLAY_NAME", metaData.getDisplayName());
        jsonMetaData.addIfNotNullOrEmpty("DESCRIPTION", metaData.getDescription());

        // Add meta data specific to global definitions.
        if (!T8IdentifierUtilities.isLocalId(metaData.getId()))
        {
            List<T8DefinitionComment> comments;

            jsonMetaData.addIfNotNullOrEmpty("VERSION", versionId);
            jsonMetaData.addIfNotNullOrEmpty("PROJECT", projectId);
            if (revision != null) jsonMetaData.add("REVISION", revision.toString());
            if (metaData.getStatus() != null) jsonMetaData.add("STATUS", metaData.getStatus().toString());
            if (metaData.getCreatedTime() != null) jsonMetaData.add("CREATED_TIME", metaData.getCreatedTime().toString());
            if (metaData.getUpdatedTime() != null) jsonMetaData.add("UPDATED_TIME", metaData.getUpdatedTime().toString());
            jsonMetaData.addIfNotNullOrEmpty("CREATED_USER", metaData.getCreatedUserId());
            jsonMetaData.addIfNotNullOrEmpty("UPDATED_USER", metaData.getUpdatedUserId());
            jsonMetaData.addIfNotNullOrEmpty("CHECKSUM", metaData.getChecksum());

            // Add comments.
            comments = metaData.getComments();
            if (!comments.isEmpty())
            {
                JsonArray jsonComments;

                jsonComments = new JsonArray();
                jsonMetaData.add("COMMENTS", jsonComments);
                for (T8DefinitionComment comment : comments)
                {
                    JsonObject jsonComment;

                    jsonComment = new JsonObject();
                    jsonComment.add("TIMESTAMP", comment.getTimestamp().getMilliseconds());
                    jsonComment.add("TYPE", comment.getType().toString());
                    jsonComment.add("USER_ID", comment.getUserId());
                    jsonComment.add("TEXT", comment.getText());
                    jsonComments.add(jsonComment);
                }
            }
        }

        // Return the final json version of the meta data object.
        return jsonMetaData;
    }

    private JsonValue serializeToJSON(T8DataType dataType, Object value) throws Exception
    {
        if (dataType.equals(T8DataType.UNDEFINED))
        {
            throw new RuntimeException("Cannot serialize UNDEFINED data type.");
        }
        else if (value == null)
        {
            return JsonValue.NULL;
        }
        else if (value instanceof T8Definition)
        {
            T8Definition subDefinition;

            // Only serialize non-resource definitions.
            subDefinition = (T8Definition)value;
            if ((subDefinition.getLevel() != T8DefinitionLevel.RESOURCE) || (serializeResourceDefinitions))
            {
                return serialize(subDefinition);
            }
            else return null;
        }
        else if (value instanceof Map)
        {
            Map<String, Object> dataMap;

            dataMap = (Map<String, Object>)value;
            if (dataMap.size() > 0)
            {
                JsonObject mapObject;
                T8DtMap mapDataType;
                T8DataType valueType;

                // Get the meta from the map pr
                mapDataType = (T8DtMap)dataType;
                valueType = mapDataType.getValueDataType();

                // Create the map object and add all of the entries to it.
                mapObject = new JsonObject();
                for (String key : dataMap.keySet())
                {
                    Object mapValue;

                    mapValue = dataMap.get(key);
                    mapObject.add(key, serializeToJSON(valueType != T8DataType.UNDEFINED ? valueType : determineDataType(mapValue), mapValue));
                }

                // Return the completed map object.
                return mapObject;
            }
            else return null; // Do not serialize empty maps.
        }
        else if (value instanceof List)
        {
            List<Object> dataList;

            dataList = (List<Object>)value;
            if (dataList.size() > 0)
            {
                JsonArray array;
                T8DtList listDataType;
                T8DataType elementType;

                // Get some meta from the list.
                listDataType = (T8DtList)dataType;
                elementType = listDataType.getElementDataType();

                // Create the array builder and add all elements from the list to it.
                array = new JsonArray();
                for (Object listValue : dataList)
                {
                    array.add(serializeToJSON(elementType != null ? elementType : determineDataType(listValue), listValue));
                }

                // Return the completed array.
                return array;
            }
            else return null; // Do not serialize empty lists.
        }
        else if (value instanceof T8DataEntity)
        {
            T8DataEntityDefinition entityDefinition;
            JsonObject entityObject;
            JsonObject fieldsObject;
            T8DataEntity entity;

            // Create the JSON objects.
            entity = (T8DataEntity)value;
            entityObject = new JsonObject();
            fieldsObject = new JsonObject();

            // Add the meta to the entity object.
            entityObject.add("ENTITY_IDENTIFIER", entity.getIdentifier());
            entityObject.add("ENTITY_GUID", entity.getGUID());

            // Add all field values to the fields object.
            entityDefinition = entity.getDefinition();
            for (String fieldIdentifier : entity.getFieldIdentifiers())
            {
                T8DataEntityFieldDefinition fieldDefinition;
                Object fieldValue;

                fieldDefinition = entityDefinition.getFieldDefinition(fieldIdentifier);
                fieldValue = entity.getFieldValue(fieldIdentifier);
                if (fieldValue != null)
                {

                    fieldsObject.add(fieldIdentifier, serializeToJSON(fieldDefinition.getDataType(), fieldValue));
                }
            }

            // Add the fields object to the entity object.
            entityObject.add("FIELDS", fieldsObject);
            return entityObject;
        }
        else if (value instanceof Date)
        {
            return JsonValue.valueOf(((Date)value).getTime());
        }
        else if (value instanceof T8Timestamp)
        {
            return JsonValue.valueOf(((T8Timestamp)value).getMilliseconds());
        }
        else if (value instanceof T8Time)
        {
            return JsonValue.valueOf(((T8Time)value).getMilliseconds());
        }
        else if (value instanceof T8Date)
        {
            return JsonValue.valueOf(((T8Date)value).getMilliseconds());
        }
        else if (value instanceof T8Image)
        {
            return JsonValue.valueOf(convertImageDataToString((T8Image)value));
        }
        else if (value instanceof byte[])
        {
            return JsonValue.valueOf(convertBinaryDataToString((byte[])value));
        }
        else
        {
            return JsonValue.valueOf(value);
        }
    }

    private T8Image convertStringToImageData(String imageData) throws Exception
    {
        byte[] data;
        T8Image image;

        data = convertHexStringToBinaryData(imageData);
        image = (T8Image)SerializationUtilities.deserializeObject(data);
        return image;
    }

    public String convertImageDataToString(T8Image image) throws Exception
    {
        byte[] byteArray;

        byteArray = SerializationUtilities.serializeObject(image);
        return convertBinaryDataToString(byteArray);
    }

    public String convertBinaryDataToString(byte[] binaryData) throws Exception
    {
        byte[] byteArray;

        byteArray = binaryData;
        byteArray = ByteCompressor.compressBytes(byteArray);
        return new String(Base64Codec.encode(byteArray));
    }

    private byte[] convertHexStringToBinaryData(String hexString) throws Exception
    {
        byte[] data;

        data = Base64Codec.decode(hexString);
        data = ByteCompressor.decompressBytes(data);
        return data;
    }

    private T8DataType determineDataType(Object value)
    {
        if (value == null) return T8DataType.UNDEFINED;
        else if (value instanceof Map) return T8DataType.MAP;
        else if (value instanceof List) return T8DataType.LIST;
        else if (value instanceof Boolean) return T8DataType.BOOLEAN;
        else if (value instanceof Integer) return T8DataType.INTEGER;
        else if (value instanceof Long) return T8DataType.LONG;
        else if (value instanceof Float) return T8DataType.FLOAT;
        else if (value instanceof Double) return T8DataType.DOUBLE;
        else if (value instanceof Date) return T8DataType.TIMESTAMP;
        else if (value instanceof T8Timestamp) return T8DataType.TIMESTAMP;
        else if (value instanceof T8Time) return T8DataType.TIME;
        else if (value instanceof T8Date) return T8DataType.DATE;
        else if (value instanceof T8Definition) return T8DataType.DEFINITION;
        else if (value instanceof T8DataEntity) return T8DataType.DATA_ENTITY;
        else if (value instanceof byte[]) return T8DataType.BYTE_ARRAY;
        else if (value instanceof T8Image) return T8DataType.IMAGE;
        else if (value instanceof String) return T8DataType.STRING;

        return T8DataType.CUSTOM_OBJECT;
    }
}
