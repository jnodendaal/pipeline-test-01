package com.pilog.t8;

import com.pilog.t8.notification.T8NotificationFilter;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationSummary;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8NotificationManager extends T8ServerModule
{
    /**
     * Sends the specified notification using the supplied input parameters.
     * @param context session context of the sender
     * @param notificationIdentifier The Identifier of the notification to send.
     * @param notificationParameters The parameters of the notification.
     * @throws Exception
     */
    public void sendNotification(T8Context context, String notificationIdentifier, Map<String, ? extends Object> notificationParameters) throws Exception;

    /**
     * Closes all of the notifications that have been sent to the current session context.
     * @param context
     * @throws Exception
     */
    public void closeAllNotifications(T8Context context) throws Exception;

    /**
     * Closes the specified notification, removing it from the active list and
     * archiving it in the history tables.
     * @param context
     * @param nodificationIid
     * @throws Exception
     */
    public void closeNotification(T8Context context, String nodificationIid) throws Exception;

    /**
     * Specifies that the status of a specific notification instance should be
     * updated. This will allow for notifications to be marked as read, etc.
     *
     * @param context The {@code T8Context} identifying the
     *      current session and its details
     * @param notificationInstanceIdentifier The {@code String} instance
     *      identifier which uniquely identifies the notification to be updated
     * @param status The status to which the notification should be updated
     *
     * @throws java.lang.Exception If there is any error during the updating of
     *      the notification status
     */
    void markWithStatus(T8Context context, String notificationInstanceIdentifier, T8Notification.NotificationStatus status) throws Exception;

    /**
     * Returns a boolean indication of whether or not there are any new
     * notifications for the session user.  A notification is deemed as 'new'
     * until it has been fetched at least once, using the
     * <code>getUserNotifications</code> method.
     * @param context The session context for which notifications will be
     * checked.
     * @return true is there are any notifications in the queue
     */
    public boolean hasNewNotifications(T8Context context);

    /**
     * Returns a summary of the notifications for the session user, filtered
     * according to the specified criteria.
     * @param context
     * @param filter
     * @return
     * @throws Exception
     */
    public T8NotificationSummary getUserNotificationSummary(T8Context context, T8NotificationFilter filter) throws Exception;

    /**
     * Counts the number of notifications that adhere to the specified filter criteria.
     * @param context
     * @param filter
     * @return
     * @throws Exception
     */
    public int countNotifications(T8Context context, T8NotificationFilter filter) throws Exception;

    /**
     * Returns the notifications for the session user, filtered according to the
     * supplied criteria.
     * @param context session context of the current user
     * @param filter The filter to use when fetching the notifications.
     * @return A list containing the requested notifications.
     * @throws Exception
     */
    public List<T8Notification> getUserNotifications(T8Context context, T8NotificationFilter filter) throws Exception;
}
