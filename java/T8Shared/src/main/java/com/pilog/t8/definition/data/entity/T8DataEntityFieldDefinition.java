package com.pilog.t8.definition.data.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityFieldDefinition extends T8DataFieldDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ENTITY_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_ENTITY_FIELD";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Entity Field";
    public static final String DESCRIPTION = "A field contained by a data entity.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8DataEntityFieldDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataEntityFieldDefinition(String identifier, String sourceId, boolean key, boolean required, T8DataType dataType)
    {
        super(identifier, sourceId, key, required, dataType);
    }

    public T8DataEntityFieldDefinition(String identifier, String sourceId, boolean key, boolean required, String dataType)
    {
        super(identifier, sourceId, key, required, dataType);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8DataFieldDefinition.Datum.SOURCE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = (T8DataEntityDefinition)getParentDefinition();
            if (entityDefinition != null)
            {
                String dataSourceId;

                dataSourceId = entityDefinition.getDataSourceIdentifier();
                if (dataSourceId != null)
                {
                    T8DataSourceDefinition dataSourceDefinition;

                    dataSourceDefinition = (T8DataSourceDefinition)definitionContext.getRawDefinition(getRootProjectId(), dataSourceId);
                    if (dataSourceDefinition != null)
                    {
                        return createPublicIdentifierOptionsFromDefinitions(dataSourceDefinition.getFieldDefinitions());
                    }
                    else return null;
                }
                else return null;
            }
            else return null;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }
}
