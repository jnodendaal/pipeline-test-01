package com.pilog.t8.process;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.process.T8Process.T8ProcessStatus;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessState implements Serializable
{
    private String projectId;
    private String id;
    private String iid;
    private String name;
    private String description;
    private T8ProcessStatus status;
    private double progress;
    private String initiatorId;
    private String initiatorIid;
    private String restrictionUserId;
    private String restrictionUserProfileId;
    private T8Timestamp startTime;
    private T8Timestamp endTime;
    private final Map<String, Object> parameters;

    public T8ProcessState(String projectId, String processId, String processIid)
    {
        this.projectId = projectId;
        this.id = processId;
        this.iid = processIid;
        this.parameters = new HashMap<String, Object>();
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getIid()
    {
        return iid;
    }

    public void setIid(String iid)
    {
        this.iid = iid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public T8ProcessStatus getStatus()
    {
        return status;
    }

    public void setStatus(T8ProcessStatus status)
    {
        this.status = status;
    }

    public double getProgress()
    {
        return progress;
    }

    public void setProgress(double progress)
    {
        this.progress = progress;
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId)
    {
        this.initiatorId = initiatorId;
    }

    public String getInitiatorIid()
    {
        return initiatorIid;
    }

    public void setInitiatorInstanceIdentifier(String initiatorIid)
    {
        this.initiatorIid = initiatorIid;
    }

    public String getRestrictionUserId()
    {
        return restrictionUserId;
    }

    public void setRestrictionUserId(String userId)
    {
        this.restrictionUserId = userId;
    }

    public String getRestrictionUserProfileId()
    {
        return restrictionUserProfileId;
    }

    public void setRestrictionUserProfileId(String userProfileId)
    {
        this.restrictionUserProfileId = userProfileId;
    }

    public T8Timestamp getStartTime()
    {
        return startTime;
    }

    public void setStartTime(T8Timestamp startTime)
    {
        this.startTime = startTime;
    }

    public T8Timestamp getEndTime()
    {
        return endTime;
    }

    public void setEndTime(T8Timestamp endTime)
    {
        this.endTime = endTime;
    }

    public Map<String, Object> getParameters()
    {
        return new HashMap<String, Object>(parameters);
    }

    public void setParameters(Map<String, Object> parameters)
    {
        this.parameters.clear();
        if (parameters != null)
        {
            this.parameters.putAll(parameters);
        }
    }

    public void setParameter(String parameterId, Object parameterValue)
    {
        this.parameters.put(parameterId, parameterValue);
    }

    public Object getParameter(String parameterId)
    {
        return this.parameters.get(parameterId);
    }

    public T8ProcessDetails getProcessDetails(T8ServerContext serverContext)
    {
        T8ProcessDetails processDetails;
        T8UserDefinition userDefinition;

        try
        {
            userDefinition = (T8UserDefinition)serverContext.getDefinitionManager().getRawDefinition(null, projectId, initiatorId);
        }
        catch (Exception ex)
        {
            userDefinition = null;
            T8Log.log("Failed to get user definition for initiator", ex);
        }

        processDetails = new T8ProcessDetails(iid);
        processDetails.setProjectId(projectId);
        processDetails.setProcessId(id);
        processDetails.setName(name);
        processDetails.setProcessDescription(description);
        processDetails.setProgress(progress);
        processDetails.setProcessStatus(status);
        processDetails.setRestrictionUserId(restrictionUserId);
        processDetails.setRestrictionUserProfileId(restrictionUserProfileId);
        processDetails.setInitiatorId(initiatorId);
        processDetails.setStartTime(startTime != null ? startTime.getMilliseconds() : -1);
        processDetails.setEndTime(endTime != null ? endTime.getMilliseconds() : -1);

        if (userDefinition != null)
        {
            processDetails.setInitiatorName((Strings.isNullOrEmpty(userDefinition.getName()) ? "" : userDefinition.getName()) + " " + (Strings.isNullOrEmpty(userDefinition.getSurname()) ? "" : userDefinition.getSurname()));
        }

        return processDetails;
    }
}
