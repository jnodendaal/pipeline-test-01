package com.pilog.t8.data.state;

/**
 * This interface defines the behavior of any object that acts as a data carrier in a context
 * where changes to the state of the object must be tracked.
 *
 * One example of a use-case where a T8Stateful object can be useful is in the following scenario:
 * - Data is retrieved from an arbitrary origin and loaded into a carrier object.
 * - The carrier object is transferred to a part of the system external to its origin, where changes are potentially applied.
 * - The carrier object is returned to its origin and any changes to it now have to be persisted.
 * In this scenario, the ability to identify if and how the state of the data carrier has been changed
 * becomes important in order to properly persist these changes without having to re-check the data
 * in the original persistence store against the current state of the object.
 *
 * A T8Stateful object therefore provides methods for saving its state at certain times so that the saved
 * states can later be accessed and used for comparison and the evaluation of changes that were applied.
 *
 * @author Bouwer du Preez
 * @param <S> The type of the stateful object.
 */
public interface T8Stateful<S>
{
    /**
     * Creates a complete copy of this object and returns it.  The copy must contain all of the state variables of the object.
     * @return An exact copy of this object.
     */
    public S copy();

    /**
     * Saves an exact copy of the current state of this object.  Repeated calls to this method will overwrite the saved state.
     * @return The state of this object as saved.
     */
    public S saveState();

    /**
     * Saves an exact copy of the current state of this object using the supplied id.  The id can subsequently be used to access the
     * saved state.  Repeated calls to this method using the same state id will overwrite the saved state.
     * @param stateId The id used as reference for the saved state.  This is may not be null.
     * @return The state of this object as saved.
     */
    public S saveState(String stateId);

    /**
     * Returns the state of this object as it existed the last time the {@code saveState()} method was invoked.
     * This method will return null if no state was previously saved.
     * @return The saved state of this object or null if no state was previously saved.
     */
    public S getSavedState();

    /**
     * Returns the state of this object as saved using the specified id.
     * This method will return null if no state was saved using the specified id.
     * @param stateId The unique state id for which to return the saved state.  This is may not be null.
     * @return The state of this object as saved using the specified id or null if no such state exists.
     */
    public S getSavedState(String stateId);

    /**
     * Clears all previously saved states from this object.
     */
    public void clearSavedStates();
}
