package com.pilog.t8.ui.event;

import java.awt.Component;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionLinkEvent extends EventObject
{
    private String projectId;
    private String definitionId;

    public T8DefinitionLinkEvent(Component source, String projectId, String definitionId)
    {
        super(source);
        this.projectId = projectId;
        this.definitionId = definitionId;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public String getDefinitionId()
    {
        return definitionId;
    }
}
