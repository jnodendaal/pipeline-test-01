package com.pilog.t8.definition.script;

import com.pilog.epic.rsyntax.BasicEpicCompletion;
import com.pilog.epic.rsyntax.MethodParameter;
import com.pilog.epic.rsyntax.MethodParameterChoicesProvider;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionResolver;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultModuleCompletionHandler extends T8DefaultScriptCompletionHandler
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8DefaultModuleCompletionHandler.class.getName());
    private final T8Definition definition;

    public T8DefaultModuleCompletionHandler(T8Context context, T8Definition definition)
    {
        super(context, definition);
        this.definition = definition;
        addResolver(new ComponentIdentifierResolver());
        addResolver(new ComponentOperationIdentifierResolver());
    }

    @Override
    public MethodParameterChoicesProvider getMethodParameterChoicesProvider()
    {
        return this;
    }

    @Override
    public List<BasicEpicCompletion> getParameterChoices(JTextComponent tc, MethodParameter param)
    {
        List<BasicEpicCompletion> suggestions;

        suggestions = new ArrayList<>();

        if (param.getName().equals("componentIdentifier"))
        {
            List<T8Definition> localDefinitions;

            localDefinitions = definition.getLocalDefinitions();
            for (T8Definition localDefinition : localDefinitions)
            {
                if (localDefinition instanceof T8ComponentDefinition)
                {
                    suggestions.add(new BasicEpicCompletion("\"" + localDefinition.getIdentifier() + "\"", localDefinition.getMetaDescription()));
                }
            }

            return suggestions;
        }
        else if (param.getName().equals("componentOperationIdentifier"))
        {
            String componentIdentifier;

            String text = null;
            if(tc instanceof JTextArea)
            {
                text = getJTextAreaCurrentLine((JTextArea)tc);
            }
            if(text == null)
            {
                text = tc.getText();
            }

            componentIdentifier = getIdentifiers(text).get(0);
            if (componentIdentifier != null)
            {
                T8Definition componentDefinition;

                componentDefinition = definition.getLocalDefinition(componentIdentifier);
                if ((componentDefinition != null) && (componentDefinition instanceof T8ComponentDefinition))
                {
                    List<T8ComponentOperationDefinition> operationDefinitions;

                    operationDefinitions = ((T8ComponentDefinition) componentDefinition).getComponentOperationDefinitions();
                    if (operationDefinitions != null)
                    {
                        for (T8ComponentOperationDefinition operationDefinition : operationDefinitions)
                        {
                            suggestions.add(new BasicEpicCompletion("\"" + operationDefinition.getIdentifier() + "\"", operationDefinition.getMetaDisplayName()));
                        }
                    }
                }
            }

            return suggestions;
        }
        else if("componentOperationOutputParameterIdentifier".equals(param.getName()))
        {
            String operationIdentifier;

            operationIdentifier = getLastIdentifier(getJTextAreaCurrentLine((JTextArea)tc));

            if(!Strings.isNullOrEmpty(operationIdentifier))
            {
                T8ComponentOperationDefinition operationDefinition;

                operationDefinition = (T8ComponentOperationDefinition) definition.getLocalDefinition(operationIdentifier);

                if(operationDefinition != null && operationDefinition.getOutputParameterDefinitions() != null)
                {
                    for (T8DataParameterResourceDefinition outputParameterDefinition : operationDefinition.getOutputParameterDefinitions())
                    {
                        suggestions.add(new BasicEpicCompletion("\"" + outputParameterDefinition.getIdentifier() + "\"", outputParameterDefinition.getMetaDisplayName()));
                    }
                }
            }

            return suggestions;
        }

        return super.getParameterChoices(tc, param);
    }

    @Override
    protected List<BasicEpicCompletion> createParamMapString(JTextComponent textComponent, String identifier)
    {
        try
        {
            T8Definition localDefinition = definition.getLocalDefinition(identifier);
            // if the returned definition is null then this probably wasnt a do/doget operation so let the super handle it
            if(localDefinition == null)
            {
                return super.createParamMapString(textComponent, identifier);
            }
            List<T8DataParameterDefinition> parameterDefinitions = new ArrayList<>();
            if(localDefinition instanceof T8ComponentDefinition)
            {
                String operationIdentifier = getIdentifiers(getJTextAreaCurrentLine((JTextArea)textComponent)).get(1);
                for (T8ComponentOperationDefinition t8ComponentOperationDefinition : ((T8ComponentDefinition)localDefinition).getComponentOperationDefinitions())
                {
                    if(t8ComponentOperationDefinition.getIdentifier().equals(operationIdentifier))
                    {
                        parameterDefinitions = t8ComponentOperationDefinition.getInputParameterDefinitions();
                    }
                }
            } else return super.createParamMapString(textComponent, identifier);

            if(parameterDefinitions == null || parameterDefinitions.isEmpty())
            {
                return Arrays.asList(new BasicEpicCompletion("null"));
            }
            StringBuilder hashMapString = new StringBuilder("new [");
            Iterator<T8DataParameterDefinition> dataParamaterIterator = parameterDefinitions.iterator();
            while(dataParamaterIterator.hasNext())
            {
                T8DataParameterDefinition dataParameterDefinition = dataParamaterIterator.next();
                hashMapString.append("\"");
                hashMapString.append(dataParameterDefinition.getIdentifier());
                hashMapString.append("\":");
                if(dataParamaterIterator.hasNext()) hashMapString.append(",");
            }
            hashMapString.append("]");
            return Arrays.asList(new BasicEpicCompletion(hashMapString.toString()));
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to create param hash map for " + identifier, ex);
        }
        return super.createParamMapString(textComponent, identifier);
    }

    @Override
    protected List<BasicEpicCompletion> getParamMapKeyChoices(JTextComponent textComponent, String identifier)
    {
        try
        {
            String lastComponentIdentifier;
            List<String> identifiers;

            identifiers = getIdentifiers(getTextUpToCarret((JTextArea)textComponent));
            //if identifiers is smaller or equal to one then there was no do/doget method
            if(identifiers.size() > 1)
            {
                //the component should be the second to last one
                lastComponentIdentifier = identifiers.get(identifiers.size()-2);
            }
            else return super.getParamMapKeyChoices(textComponent, identifier);
            T8Definition localDefinition = definition.getLocalDefinition(lastComponentIdentifier);
            // if the returned definition is null then this probably wasnt a do/doget operation so let the super handle it
            if(localDefinition == null)
            {
                return super.createParamMapString(textComponent, identifier);
            }
            List<T8DataParameterResourceDefinition> parameterDefinitions = new ArrayList<>();
            if(localDefinition instanceof T8ComponentDefinition)
            {
                for (T8ComponentOperationDefinition t8ComponentOperationDefinition : ((T8ComponentDefinition)localDefinition).getComponentOperationDefinitions())
                {
                    if(t8ComponentOperationDefinition.getIdentifier().equals(identifier))
                    {
                        parameterDefinitions = t8ComponentOperationDefinition.getOutputParameterDefinitions();
                    }
                }
            } else return super.createParamMapString(textComponent, identifier);

            if(parameterDefinitions == null)
            {
                return Arrays.asList(new BasicEpicCompletion("null"));
            }
            List<BasicEpicCompletion> choices = new ArrayList<>();
            for (T8DataParameterDefinition t8DataParameterDefinition : parameterDefinitions)
            {
                choices.add(new BasicEpicCompletion("\""+t8DataParameterDefinition.getPublicIdentifier()+"\""));
            }
            return choices;
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to create param hash map for " + identifier, ex);
        }
        return super.createParamMapString(textComponent, identifier);
    }

    private class ComponentIdentifierResolver implements T8ScriptCompletionResolver
    {
        @Override
        public List<String> getMatchPatterns()
        {
            List<String> patterns;

            patterns = new ArrayList<>();
            patterns.add("do\\(\"");
            patterns.add("doGet\\(\"");
            return patterns;
        }

        @Override
        public Map<String, String> getCodeCompletionSuggestions(String matchedText)
        {
            Map<String, String> suggestions;
            List<T8Definition> localDefinitions;

            suggestions = new TreeMap<>();
            localDefinitions = definition.getLocalDefinitions();
            for (T8Definition localDefinition : localDefinitions)
            {
                if (localDefinition instanceof T8ComponentDefinition)
                {
                    suggestions.put(localDefinition.getIdentifier(), localDefinition.getMetaDescription());
                }
            }

            return suggestions;
        }
    }

    private class ComponentOperationIdentifierResolver implements T8ScriptCompletionResolver
    {
        @Override
        public List<String> getMatchPatterns()
        {
            List<String> patterns;

            patterns = new ArrayList<>();
            patterns.add("do\\(\"[$A-Z_]+\",[ ]*\"");
            patterns.add("doGet\\(\"[$A-Z_]+\",[ ]*\"");
            return patterns;
        }

        @Override
        public Map<String, String> getCodeCompletionSuggestions(
                String matchedText)
        {
            Map<String, String> suggestions;
            String componentIdentifier;

            suggestions = new TreeMap<>();
            componentIdentifier = getIdentifiers(matchedText).get(0);
            if (componentIdentifier != null)
            {
                T8Definition componentDefinition;

                componentDefinition = definition.getLocalDefinition(componentIdentifier);
                if ((componentDefinition != null) && (componentDefinition instanceof T8ComponentDefinition))
                {
                    List<T8ComponentOperationDefinition> operationDefinitions;

                    operationDefinitions = ((T8ComponentDefinition) componentDefinition).getComponentOperationDefinitions();
                    if (operationDefinitions != null)
                    {
                        for (T8ComponentOperationDefinition operationDefinition : operationDefinitions)
                        {
                            suggestions.put(operationDefinition.getIdentifier(), operationDefinition.getMetaDisplayName());
                        }
                    }
                }
            }

            return suggestions;
        }
    }
}
