package com.pilog.t8.definition.data.connection;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.database.T8DatabaseAdaptorDefinition;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataConnectionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_CONNECTION";
    public static final String GROUP_NAME = "Data Connections";
    public static final String GROUP_DESCRIPTION = "Connections to a data store.";
    public static final String STORAGE_PATH = "/data_connections";
    public static final String IDENTIFIER_PREFIX = "DATA_CONNECTION_";
    private enum Datum {DATABASE_ADAPTOR_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8DatabaseAdaptorDefinition databaseAdaptorDefinition;

    public T8DataConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATABASE_ADAPTOR_IDENTIFIER.toString(), "Database Adaptor", "The Database Adaptor applicable to this connection type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATABASE_ADAPTOR_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DatabaseAdaptorDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String adaptorId;

        adaptorId = getDatabaseAdaptorIdentifier();
        if (adaptorId != null)
        {
            databaseAdaptorDefinition = (T8DatabaseAdaptorDefinition)context.getServerContext().getDefinitionManager().getRawDefinition(context, getRootProjectId(), adaptorId);
        }
    }

    public String getDatabaseAdaptorIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATABASE_ADAPTOR_IDENTIFIER.toString());
    }

    public void setDatabaseAdaptorIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.DATABASE_ADAPTOR_IDENTIFIER.toString(), connectionIdentifier);
    }

    public T8DatabaseAdaptorDefinition getDatabaseAdaptorDefinition()
    {
        return databaseAdaptorDefinition;
    }

    public abstract T8DataConnection createNewConnectionInstance(T8DataTransaction dataAccessProvider) throws Exception;
}
