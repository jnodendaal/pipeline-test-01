package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8DialogFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final String dialogIdentifier;
    private final Map<String, Object> inputParameters;

    public T8DialogFunctionalityAccessHandle(String functionalityId, String functionalityIid, String dialogId, Map<String, Object> inputParameters, String displayName, String description, Icon icon)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.dialogIdentifier = dialogId;
        this.inputParameters = inputParameters;
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        // This method is currently not in a good state as it is not implemented according to the intended contract.
        // TODO:  Update this method to no longer return a view component.  Also see the note on this update in the T8FunctionalityViewPane.
        controller.showDialog(dialogIdentifier, inputParameters, false);
        return null;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return false;
    }

    public String getDialogIdentifier()
    {
        return dialogIdentifier;
    }

    public Map<String, Object> getInputParameters()
    {
        return inputParameters;
    }
}
