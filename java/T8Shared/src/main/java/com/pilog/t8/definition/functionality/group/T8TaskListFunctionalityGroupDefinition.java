package com.pilog.t8.definition.functionality.group;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.task.T8TaskTypeSummary;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectFieldDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskListFunctionalityGroupDefinition extends T8DefaultFunctionalityGroupDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_GROUP_SYSTEM_TASK_LIST";
    public static final String DISPLAY_NAME = "Task List Functionality Group";
    public static final String DESCRIPTION = "A grouping used to access system task list functionalities.";
    public static final String IDENTIFIER_PREFIX = "FNC_GRP_";
    public enum Datum
    {
        FUNCTIONALITY_IDENTIFIER,
        TARGET_OBJECT_IDENTIFIER,
        TASK_TYPE_FIELD_IDENTIFIER,
        INCLUDE_CLAIMED_FIELD_IDENTIFIER,
        INCLUDE_UNCLAIMED_FIELD_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    private T8DataObjectDefinition targetObjectDefinition;

    public T8TaskListFunctionalityGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_IDENTIFIER.toString(), "Functionality", "The identifier of the module that will be displayed when one of the functionalities in this group is invoked.  This module takes responsibility for display of the selected task type list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_OBJECT_IDENTIFIER.toString(), "Target Object", "The identifier of the target object that will be created for execution of the functionality."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_TYPE_FIELD_IDENTIFIER.toString(), "Task Type Field", "The identifier of the target object field to which the task type identifier will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INCLUDE_CLAIMED_FIELD_IDENTIFIER.toString(), "'Include Claimed' Field", "The identifier of the target object field to which the boolean flag 'Include Claimed' (indicating whether or not claimed tasks should be included) will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INCLUDE_UNCLAIMED_FIELD_IDENTIFIER.toString(), "'Include Unclaimed' Field", "The identifier of the target object field to which the boolean flag 'Include Unclaimed' (indicating whether or not unclaimed tasks should be included) will be sent."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TARGET_OBJECT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if ((Datum.TASK_TYPE_FIELD_IDENTIFIER.toString().equals(datumIdentifier)) || (Datum.INCLUDE_CLAIMED_FIELD_IDENTIFIER.toString().equals(datumIdentifier)) || (Datum.INCLUDE_UNCLAIMED_FIELD_IDENTIFIER.toString().equals(datumIdentifier)))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String targetObjectId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            optionList.add(new T8DefinitionDatumOption("No mapping.", null));

            targetObjectId = getTargetObjectIdentifier();
            if (targetObjectId != null)
            {
                T8DataObjectDefinition dataObjectDefinition;

                dataObjectDefinition = (T8DataObjectDefinition)definitionContext.getRawDefinition(getRootProjectId(), targetObjectId);
                if (dataObjectDefinition != null)
                {
                    List<T8DataObjectFieldDefinition> objectFieldDefinitions;

                    objectFieldDefinitions = dataObjectDefinition.getFieldDefinitions();
                    optionList.addAll(createPublicIdentifierOptionsFromDefinitions((List)objectFieldDefinitions));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String definitionId;

        // Call the super-initialization.
        super.initializeDefinition(context, inputParameters, configurationSettings);

        // Pre-load the target object definition if one is specified.
        definitionId = getTargetObjectIdentifier();
        if (definitionId != null)
        {
            targetObjectDefinition = (T8DataObjectDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    @Override
    public T8FunctionalityGroupHandle getNewFunctionalityGroupHandle(T8Context context, T8FunctionalityGroupHandle parentGroup) throws Exception
    {
        try
        {
            T8ConfigurationManager configurationManager;
            T8FlowManager flowManager;
            T8FunctionalityGroupHandle groupHandle;
            T8FunctionalityHandle handle;
            T8TaskListSummary taskListSummary;
            String functionalityId;

            // Create the new handle.
            groupHandle = super.getNewFunctionalityGroupHandle(context, parentGroup);
            functionalityId = getFunctionalityIdentifier();

            // Get a configuration and flow managers.
            configurationManager = context.getServerContext().getConfigurationManager();
            flowManager = context.getServerContext().getFlowManager();

            // Get the task list summary.
            taskListSummary = flowManager.getTaskListSummary(context, null);

            // Create the 'All Tasks' Functionality.
            handle = new T8FunctionalityHandle(functionalityId);
            handle.setDisplayName(configurationManager.getUITranslation(context, "All Tasks") + " (" + (taskListSummary.getTotalTaskCount()) + ")");
            handle.setDescription(configurationManager.getUITranslation(context, "All Tasks currently available."));
            handle.setIcon(getIcon());
            handle.addParameter(functionalityId + "$P_TASK_ID", null);
            handle.addParameter(functionalityId + "$P_INCLUDE_CLAIMED_TASKS", true);
            handle.addParameter(functionalityId + "$P_INCLUDE_UNCLAIMED_TASKS", true);

            // Add the completed handle to the group.
            groupHandle.addFunctionality(handle);

            // Create the 'Claimed Tasks' Functionality.
            handle = new T8FunctionalityHandle(functionalityId);
            handle.setDisplayName(configurationManager.getUITranslation(context, "Tasks in Progress") + " (" + (taskListSummary.getClaimedTaskCount()) + ")");
            handle.setDescription(configurationManager.getUITranslation(context, "All Tasks that have been started but not yet completed."));
            handle.setIcon(getIcon());
            handle.addParameter(functionalityId + "$P_TASK_ID", null);
            handle.addParameter(functionalityId + "$P_INCLUDE_CLAIMED_TASKS", true);
            handle.addParameter(functionalityId + "$P_INCLUDE_UNCLAIMED_TASKS", false);

            // Add the completed handle to the group.
            groupHandle.addFunctionality(handle);

            // Add all the task type functionalities.
            for (T8TaskTypeSummary taskTypeSummary : taskListSummary.getTaskTypeSummaries())
            {
                if (taskTypeSummary.getTotalCount() > 0)
                {
                    // Create the Functionality.
                    handle = new T8FunctionalityHandle(functionalityId);
                    handle.setDisplayName(taskTypeSummary.getDisplayName() + " (" + taskTypeSummary.getTotalCount() + ")");
                    handle.setDescription(taskTypeSummary.getDescription());
                    handle.setIcon(getIcon());
                    handle.addParameter(functionalityId + "$P_TASK_ID", taskTypeSummary.getTaskIdentifier());
                    handle.addParameter(functionalityId + "$P_INCLUDE_CLAIMED_TASKS", true);
                    handle.addParameter(functionalityId + "$P_INCLUDE_UNCLAIMED_TASKS", true);

                    // Add the completed handle to the group.
                    groupHandle.addFunctionality(handle);
                }
            }

            // Return the constructed handle.
            return groupHandle;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while initializing functionality group handle: " + getIdentifier(), e);
        }
    }

    public String getFunctionalityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER.toString());
    }

    public void setFunctionalityIdentifier(String functionalityId)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER.toString(), functionalityId);
    }

    public String getTargetObjectIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_OBJECT_IDENTIFIER.toString());
    }

    public void setTargetObjectIdentifier(String functionalityIdentifier)
    {
        setDefinitionDatum(Datum.TARGET_OBJECT_IDENTIFIER.toString(), functionalityIdentifier);
    }

    public String getTaskTypeFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_TYPE_FIELD_IDENTIFIER.toString());
    }

    public void setTaskTypeFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TASK_TYPE_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getIncludeClaimedFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INCLUDE_CLAIMED_FIELD_IDENTIFIER.toString());
    }

    public void setIncludeClaimedFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INCLUDE_CLAIMED_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getIncludeUnclaimedFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INCLUDE_UNCLAIMED_FIELD_IDENTIFIER.toString());
    }

    public void setIncludeUnclaimedFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INCLUDE_UNCLAIMED_FIELD_IDENTIFIER.toString(), identifier);
    }

    public T8DataObjectDefinition getTargetObjectDefinition()
    {
        return targetObjectDefinition;
    }
}
