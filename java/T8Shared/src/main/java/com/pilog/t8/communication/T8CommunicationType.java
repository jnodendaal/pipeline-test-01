/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication;

/**
 * The communication type is a list of all the possible supported or future supported communication types that the system can send communications to.
 * @author hennie.brink@pilog.co.za
 */
public enum T8CommunicationType
{
    //This list should never be modified, only added to
    EMAIL,
    SMS,
    FAX,
    ANDROID_NOTIFICATION,
    IPHONE_NOTIFICATION
}
