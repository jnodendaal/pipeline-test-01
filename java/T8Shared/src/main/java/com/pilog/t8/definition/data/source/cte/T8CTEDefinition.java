package com.pilog.t8.definition.data.source.cte;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public abstract class T8CTEDefinition extends T8Definition
{
    public static final String GROUP_IDENTIFIER = "@DG_COMMON_TABLE_EXPRESSIONS";
    public static final String GROUP_NAME = "Common Table Expressions";
    public static final String GROUP_DESCRIPTION = "SQL clauses that are viewed as common to many different parts of the system and which are therefore extracted so as to be easily reusable.";
    public static final String STORAGE_PATH = "/common_table_expressions";
    public static final String IDENTIFIER_PREFIX = "CTE_";

    private List<T8CTEDefinition> includedCTEDefinitions;

    public enum Datum
    {
        NAME,
        DEPENDENT_CTES // This datum is misnamed.  These are no dependent CTE's but rather CTE's that this one is dependent on.
    };

    public T8CTEDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.NAME.toString(), "Name", "The SQL Name of the common table expression"));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.DEPENDENT_CTES.toString(), "Included CTE's", "Other CTE's that this one depends on."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DEPENDENT_CTES.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CTEDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        List<String> includedCTEIdentifiers;
        T8DefinitionManager definitionManager;

        definitionManager = context.getServerContext().getDefinitionManager();

        includedCTEDefinitions = new ArrayList<T8CTEDefinition>();
        includedCTEIdentifiers = getIncludedCTEDefinitionIdentifiers();
        if (includedCTEIdentifiers != null)
        {
            for (String cteIdentifier : includedCTEIdentifiers)
            {
                T8CTEDefinition includedDefinition;

                includedDefinition = (T8CTEDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), cteIdentifier, null);
                includedCTEDefinitions.add(includedDefinition);
            }
        }
    }

    /**
     * Returns a list of the CTE definitions included by this definition, from
     * the lowest level (i.e. the definition that should be added to the SQL
     * query first) to the highest level.
     * @return A list of the CTE definitions included by this definition.
     */
    public List<T8CTEDefinition> getIncludedCTEDefinitions()
    {
        List<T8CTEDefinition> definitionList;

        definitionList = new ArrayList<T8CTEDefinition>();
        if (includedCTEDefinitions != null)
        {
            for (T8CTEDefinition dependantDefinition : includedCTEDefinitions)
            {
                definitionList.addAll(dependantDefinition.getIncludedCTEDefinitions());
                definitionList.add(dependantDefinition);
            }
        }

        return definitionList;
    }

    public abstract String getSQLCTEString(T8DataFilter filter, int pageOffset, int pageSize);
    public abstract Map<String, String> getSQLParameterExpressionMap();

    public String getName()
    {
        return (String)getDefinitionDatum(Datum.NAME.toString());
    }

    public void setName(String name)
    {
        setDefinitionDatum(Datum.NAME.toString(), name);
    }

    public List<String> getIncludedCTEDefinitionIdentifiers()
    {
        return (List<String>) getDefinitionDatum(Datum.DEPENDENT_CTES.toString());
    }

    public void setIncludedCTEDefinitionIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DEPENDENT_CTES.toString(), identifiers);
    }
}
