package com.pilog.t8.definition;

import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionHandle implements Serializable
{
    private String definitionId;
    private String typeId;
    private String groupId;
    private String projectId;
    private T8DefinitionLevel definitionLevel;

    public T8DefinitionHandle(T8DefinitionLevel definitionLevel, String definitionId, String projectId)
    {
        if (definitionLevel == null) throw new RuntimeException("Null definition level.");
        this.definitionLevel = definitionLevel;
        this.definitionId = definitionId;
        setProjectIdentifier(projectId);
    }

    public String getHandleIdentifier()
    {
        // This method is used by equals of this class so be carefull.
        // The identifier returned by this method must identify the definition uniquely.
        if (definitionLevel == null) throw new RuntimeException("Null definition level.");
        else if (definitionLevel == T8DefinitionLevel.ROOT) return "@ROOT:" + definitionId;
        else if (definitionLevel == T8DefinitionLevel.PROJECT) return projectId + ":" + definitionId;
        else return "@RESOURCE:" + definitionId;
    }

    public String getDefinitionIdentifier()
    {
        return definitionId;
    }

    public void setDefinitionIdentifier(String identifier)
    {
        this.definitionId = identifier;
    }

    public T8DefinitionLevel getDefinitionLevel()
    {
        return definitionLevel;
    }

    public void setDefinitionLevel(T8DefinitionLevel definitionLevel)
    {
        if (definitionLevel == null) throw new RuntimeException("Null definition level.");
        this.definitionLevel = definitionLevel;
    }

    public String getProjectIdentifier()
    {
        return projectId;
    }

    public final void setProjectIdentifier(String projectIdentifier)
    {
        if ((projectIdentifier != null) && (projectIdentifier.trim().length() == 0)) throw new IllegalArgumentException("Empty String is now allowed as Project identifier.");
        this.projectId = projectIdentifier;
    }

    public String getTypeIdentifier()
    {
        return typeId;
    }

    public void setTypeIdentifier(String typeIdentifier)
    {
        this.typeId = typeIdentifier;
    }

    public String getGroupIdentifier()
    {
        return groupId;
    }

    public void setGroupIdentifier(String groupIdentifier)
    {
        this.groupId = groupIdentifier;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof T8DefinitionHandle)) return false;
        else
        {
            T8DefinitionHandle handle;

            handle = (T8DefinitionHandle)object;
            if (!Objects.equals(handle.getHandleIdentifier(), getHandleIdentifier())) return false;
            else return true;
        }
    }

    @Override
    public int hashCode()
    {
        return getHandleIdentifier().hashCode();
    }

    @Override
    public String toString()
    {
        return "[" + getHandleIdentifier() + "]";
    }
}
