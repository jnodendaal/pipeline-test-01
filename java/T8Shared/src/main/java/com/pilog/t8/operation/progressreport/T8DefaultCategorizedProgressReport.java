package com.pilog.t8.operation.progressreport;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8DefaultCategorizedProgressReport extends T8DefaultProgressReport implements T8CategorizedProgressReport
{
    private String categoryMessage;
    private String categoryTitle;
    private Double categoryProgress = -1d;

    public T8DefaultCategorizedProgressReport()
    {
    }

    public T8DefaultCategorizedProgressReport(String progressMessage,
                                       Double progress)
    {
        super(progressMessage, progress);
    }

    public T8DefaultCategorizedProgressReport(String progressMessage,
                                       Double progress,
                                       String categoryMessage,
                                       Double categoryProgress)
    {
        super(progressMessage, progress);
        this.categoryMessage = categoryMessage;
        this.categoryProgress = categoryProgress;
    }

    public T8DefaultCategorizedProgressReport(String progressTitle,
                                       String progressMessage,
                                       Double progress,
                                       String categoryTitle,
                                       String categoryMessage,
                                       Double categoryProgress)
    {
        super(progressTitle, progressMessage, progress);
        this.categoryMessage = categoryMessage;
        this.categoryProgress = categoryProgress;
        this.categoryTitle = categoryTitle;
    }

    @Override
    public String getCategoryMessage()
    {
        return categoryMessage;
    }

    @Override
    public Double getCategoryProgress()
    {
        return categoryProgress;
    }

    @Override
    public String getCategoryTitle()
    {
        return categoryTitle;
    }

    @Override
    public void setCategoryTitle(String categoryTitle)
    {
        this.categoryTitle = categoryTitle;
    }

    @Override
    public void setCategoryMessage(String categoryMessage)
    {
        this.categoryMessage = categoryMessage;
    }

    @Override
    public void setCategoryProgress(Double categoryProgress)
    {
        this.categoryProgress = categoryProgress;
    }
}
