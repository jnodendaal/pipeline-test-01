package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8RemoteUserDefinition extends T8Definition implements T8RemoteConnectionUser
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REMOTE_SERVER_CONNECTION_T8_USER";
    public static final String DISPLAY_NAME = "Remote T8 User";
    public static final String DESCRIPTION = "A Definition containing the authentication credentials for a remote T8 User";
    public enum Datum {
        USER_API_KEY
    };
// -------- Definition Meta-Data -------- //
    public T8RemoteUserDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {}

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.PASSWORD_HASH, Datum.USER_API_KEY.toString(), "API Key", "The API Key of this remote user."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public String getApiKey()
    {
        return getDefinitionDatum(Datum.USER_API_KEY);
    }

    public void setApiKey(String apiKey)
    {
        setDefinitionDatum(Datum.USER_API_KEY, apiKey);
    }
}
