package com.pilog.t8.security;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowProfileDetails implements Serializable
{
    private String id;
    private String name;
    private String description;

    public T8WorkflowProfileDetails()
    {
    }

    public T8WorkflowProfileDetails(String id, String name, String description)
    {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
