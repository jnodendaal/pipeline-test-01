package com.pilog.t8.definition.gfx.painter.image;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.image.T8ImageDefinition;
import com.pilog.t8.definition.gfx.painter.T8AbstractAreaPainterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ImagePainterDefinition extends T8AbstractAreaPainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_IMAGE";
    public static final String DISPLAY_NAME = "Image Painter";
    public static final String DESCRIPTION = "A painter that draws an image on the assigned area.";
    public enum Datum {IMAGE_IDENTIFIER,
                       FILL_MODE};
    // -------- Definition Meta-Data -------- //

    public enum FillMode {NONE,
                          REPEAT_HORIZONTAL,
                          REPEAT_VERTICAL,
                          REPEAT_HORIZONTAL_AND_VERTICAL,
                          SCALE_INSIDE_FIT,
                          SCALE_OUTSIDE_FIT,
                          SCALE_DISTORT};

    private T8ImageDefinition imageDefinition;

    public T8ImagePainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.IMAGE_IDENTIFIER.toString(), "Image", "The image to paint."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FILL_MODE.toString(), "Fill Mode", "The method by which the image will be resized if it does not fit into, or is larger than the available space.", FillMode.NONE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.IMAGE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ImageDefinition.GROUP_IDENTIFIER), false, null);
        else if (Datum.FILL_MODE.toString().equals(datumIdentifier)) return createStringOptions(FillMode.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String imageId;

        imageId = getImageIdentifier();
        if (imageId != null)
        {
            imageDefinition = (T8ImageDefinition)context.getServerContext().getDefinitionManager().getRawDefinition(context, getRootProjectId(), imageId);
        }
    }

    @Override
    public T8Painter getNewPainterInstance()
    {
        try
        {
            T8Painter painter;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.gfx.painter.image.T8ImagePainter").getConstructor(T8ImagePainterDefinition.class);
            painter = (T8Painter)constructor.newInstance(this);
            return painter;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public T8ImageDefinition getImageDefinition()
    {
        return imageDefinition;
    }

    public String getImageIdentifier()
    {
        return (String)getDefinitionDatum(Datum.IMAGE_IDENTIFIER.toString());
    }

    public void setImageIdentifier(String identifier)
    {
         setDefinitionDatum(Datum.IMAGE_IDENTIFIER.toString(), identifier);
    }

    public FillMode getFillMode()
    {
        return FillMode.valueOf((String)getDefinitionDatum(Datum.FILL_MODE.toString()));
    }

    public void setFillMode(FillMode fillMode)
    {
         setDefinitionDatum(Datum.FILL_MODE.toString(), fillMode.toString());
    }
}
