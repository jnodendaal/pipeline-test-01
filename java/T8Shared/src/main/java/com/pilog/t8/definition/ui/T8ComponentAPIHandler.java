package com.pilog.t8.definition.ui;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Component and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ComponentAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_SET_VISIBLE = "$CO_SET_VISIBLE";
    public static final String OPERATION_SET_ENABLED = "$CO_SET_ENABLED";
    public static final String OPERATION_SET_TOOLTIP_TEXT = "$CO_SET_TOOLTIP_TEXT";
    public static final String OPERATION_HIDE = "$CO_HIDE";
    public static final String OPERATION_SHOW = "$CO_SHOW";
    public static final String OPERATION_ENABLE = "$CO_ENABLE";
    public static final String OPERATION_DISABLE = "$CO_DISABLE";
    public static final String PARAMETER_VISIBLE = "$CP_VISIBLE";
    public static final String PARAMETER_ENABLED = "$CP_ENABLED";
    public static final String PARAMETER_TEXT = "$CP_TEXT";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();
        
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Component Visibility");
        newOperationDefinition.setMetaDescription("This operations sets the visibility of the component.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "The Boolean value that will be used to set the visibility of the component.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_HIDE);
        newOperationDefinition.setMetaDisplayName("Hide Component");
        newOperationDefinition.setMetaDescription("Sets the visibility of the component to false.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SHOW);
        newOperationDefinition.setMetaDisplayName("Show Component");
        newOperationDefinition.setMetaDescription("Sets the visibility of the component to true.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_ENABLED);
        newOperationDefinition.setMetaDisplayName("Set Component Enabled");
        newOperationDefinition.setMetaDescription("Sets whether the component should be enabled or not.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ENABLED, "Enabled", "The Boolean value that will be used to set the 'enabled' state of the component.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ENABLE);
        newOperationDefinition.setMetaDisplayName("Enable Component");
        newOperationDefinition.setMetaDescription("Sets the 'enabled' state of the component to true.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE);
        newOperationDefinition.setMetaDisplayName("Disable Component");
        newOperationDefinition.setMetaDescription("Sets the 'enabled' state of the component to false.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TOOLTIP_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Tooltip Text");
        newOperationDefinition.setMetaDescription("Sets the tooltip text of the component.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The translated text that should be displayed when the tooltip for this component is shown.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        return operations;
    }
}
