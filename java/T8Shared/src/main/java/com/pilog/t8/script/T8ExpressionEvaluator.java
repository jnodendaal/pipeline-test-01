package com.pilog.t8.script;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ExpressionEvaluator
{
    /**
     * Sets the language to use when resolving expressions involving terminology.
     * @param languageId The concept id of the language to set.
     */
    public void setLanguage(String languageId);

    /**
     * Compiles the specified expression and prepares it for evaluation.
     * @param expressionString The expression to compile.
     * @throws EPICSyntaxException Thrown if any part of the expression contains invalid syntax.
     */
    public void compileExpression(String expressionString) throws EPICSyntaxException;

    /**
     * Evaluates the Boolean expression currently pre-compiled.
     * @param inputVariables The input parameters to use during evaluation.
     * @param contextObject
     * @return The Boolean result of the evaluated expression.
     * @throws EPICRuntimeException Thrown if an exception occurs during evaluation of a valid expression.
     */
    public boolean evaluateBooleanExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException;

    /**
     * Evaluates the specified Boolean expression using the supplied input parameters.
     * @param expressionString The expression to evaluate.
     * @param inputVariables The input parameters to use during evaluation.
     * @param contextObject
     * @return The Boolean result of the evaluated expression.
     * @throws EPICSyntaxException Thrown if any part of the expression contains invalid syntax.
     * @throws EPICRuntimeException Thrown if an exception occurs during evaluation of a valid expression.
     */
    public boolean evaluateBooleanExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException;

    /**
     * Evaluates the expression currently pre-compiled.
     * @param inputVariables The input parameters to use during evaluation.
     * @param contextObject
     * @return The result of the evaluated expression.
     * @throws EPICRuntimeException
     */
    public Object evaluateExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException;

    /**
     * Compiles and evaluates the specified expression using the supplied input parameters.
     * @param expressionString The expression to evaluate.
     * @param inputVariables The input parameters to use during evaluation.
     * @param contextObject
     * @return The result of the evaluated expression.
     * @throws EPICSyntaxException Thrown if any part of the expression contains invalid syntax.
     * @throws EPICRuntimeException Thrown if an exception occurs during evaluation of a valid expression.
     */
    public Object evaluateExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException;
}
