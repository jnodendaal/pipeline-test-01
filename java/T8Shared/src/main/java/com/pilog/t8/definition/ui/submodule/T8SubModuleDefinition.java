package com.pilog.t8.definition.ui.submodule;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.script.T8ComponentEventHandlerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentControllerDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SubModuleDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_SUB_MODULE";
    public static final String DISPLAY_NAME = "Sub-Module";
    public static final String DESCRIPTION = "A container that holds a single module component.  The container acts as a relay between the parent- and sub-module.";
    public enum Datum {MODULE_IDENTIFIER,
                       INPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    private T8ModuleDefinition moduleDefinition;

    public T8SubModuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MODULE_IDENTIFIER.toString(), "Module Identifier", "The identifier of the module as a sub-module."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Input Parameter Mapping:  Module to Sub-Module", "A mapping of this module's input parameters to those of the sub-module."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MODULE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ModuleDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String subModuleId;

            optionList = new ArrayList<>();

            subModuleId = getModuleIdentifier();
            if (subModuleId != null)
            {
                T8ModuleDefinition subModuleDefinition;
                T8ComponentControllerDefinition controllerDefinition;

                subModuleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), subModuleId);
                controllerDefinition = getControllerDefinition();

                if ((subModuleDefinition != null) && (controllerDefinition != null))
                {
                    List<T8DataParameterDefinition> inputParameterDefinitions;
                    List<T8DataParameterDefinition> subParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    inputParameterDefinitions = controllerDefinition.getInputParameterDefinitions();
                    subParameterDefinitions = subModuleDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<>();
                    if ((inputParameterDefinitions != null) && (subParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition parentParameterDefinition : inputParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataParameterDefinition subParameterDefinition : subParameterDefinitions)
                            {
                                identifierList.add(subParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(parentParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (T8ComponentDefinition.Datum.EVENT_HANDLER_SCRIPT_DEFINITIONS.toString().equals(datumIdentifier))
        {
            String moduleId;

            moduleId = getModuleIdentifier();
            if (moduleId != null)
            {
                ArrayList<T8ComponentEventDefinition> eventDefinitions;
                ArrayList<T8DefinitionDatumOption> options;
                T8ComponentDefinition subModuleDefinition;

                subModuleDefinition = (T8ComponentDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);

                options = new ArrayList<>();
                eventDefinitions = subModuleDefinition.getComponentEventDefinitions();
                for (T8ComponentEventDefinition eventDefinition : eventDefinitions)
                {
                    ArrayList<Object> parameters;
                    String eventHandlerIdentifier;
                    String eventIdentifier;
                    String displayName;

                    eventIdentifier = eventDefinition.getPublicIdentifier();
                    displayName = eventDefinition.getMetaDisplayName();

                    eventHandlerIdentifier = getIdentifier() + "_" + eventIdentifier;
                    eventHandlerIdentifier = eventHandlerIdentifier.replace("@", "");
                    eventHandlerIdentifier = eventHandlerIdentifier.replace("$", "");
                    eventHandlerIdentifier = "$CEH_" + eventHandlerIdentifier;

                    parameters = new ArrayList<>();
                    parameters.add(eventIdentifier);
                    options.add(new T8DefinitionDatumOption((displayName != null) && (displayName.trim().length() > 0) ? displayName : eventIdentifier, new T8DefinitionConstructorParameters(eventHandlerIdentifier, T8ComponentEventHandlerScriptDefinition.TYPE_IDENTIFIER, parameters)));
                }

                return options;
            }
            else return null;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String moduleId;

        moduleId = getModuleIdentifier();
        if ((moduleId != null) && (moduleId.length() > 0))
        {
            T8DefinitionManager definitionManager;
            Map<String, Object> subModuleInputParameters;

            // Compile a map of the sub-module input parameters.
            subModuleInputParameters = T8IdentifierUtilities.stripNamespace(inputParameters);
            subModuleInputParameters = T8IdentifierUtilities.mapParameters(subModuleInputParameters, getInputParameterMapping());

            // Get the initialized sub-module definition.
            definitionManager = context.getServerContext().getDefinitionManager();
            moduleDefinition = (T8ModuleDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), moduleId, subModuleInputParameters);
            moduleDefinition.addEventHandlerScriptDefinitions(getEventHandlerScriptDefinitions());
        }
        else throw new Exception("No Module Identifier set in Sub Module Definition.");
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getModuleIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.MODULE_IDENTIFIER.toString(), "Module Identifier not set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.submodule.T8SubModule", new Class<?>[]{T8SubModuleDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        if (moduleDefinition != null)
        {
            return moduleDefinition.getComponentInputParameterDefinitions();
        } else return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        if (moduleDefinition != null)
        {
            return moduleDefinition.getComponentEventDefinitions();
        } else return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        if (moduleDefinition != null)
        {
            ArrayList<T8ComponentOperationDefinition> operations;

            operations = T8SubModuleAPIHandler.getOperationDefinitions();
            operations.addAll(moduleDefinition.getComponentOperationDefinitions());
            return operations;
        }
        return T8SubModuleAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public void setModuleDefinition(T8ModuleDefinition definition)
    {
        moduleDefinition = definition;
    }

    public T8ModuleDefinition getModuleDefinition()
    {
        return moduleDefinition;
    }

    public String getModuleIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_IDENTIFIER.toString());
    }

    public void setModuleIdentifier(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.MODULE_IDENTIFIER.toString(), moduleIdentifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
