package com.pilog.t8.time;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public interface T8TimeUnit extends Serializable
{
    public long getMilliseconds();
}
