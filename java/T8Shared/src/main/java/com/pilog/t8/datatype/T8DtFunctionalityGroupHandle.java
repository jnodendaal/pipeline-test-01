package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.functionality.T8FunctionalityHandle;

/**
 * @author Bouwer du Preez
 */
public class T8DtFunctionalityGroupHandle extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FUNCTIONALITY_GROUP_HANDLE";

    public T8DtFunctionalityGroupHandle() {}

    public T8DtFunctionalityGroupHandle(T8DefinitionManager context) {}

    public T8DtFunctionalityGroupHandle(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FunctionalityGroupHandle.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtFunctionalityHandle fhDataType;
            T8FunctionalityGroupHandle groupHandle;
            JsonObject groupHandleObject;
            JsonArray functionalityArray;

            // Create the concept JSON object.
            groupHandle = (T8FunctionalityGroupHandle)object;
            groupHandleObject = new JsonObject();
            functionalityArray = new JsonArray();
            groupHandleObject.add("id", groupHandle.getIdentifier());
            groupHandleObject.add("name", groupHandle.getDisplayName());
            groupHandleObject.addIfNotNull("description", groupHandle.getDescription());
            groupHandleObject.addIfNotNull("iconUri", groupHandle.getIconUri());
            groupHandleObject.add("functionalities", functionalityArray);

            // Serialize all functionalities in the group.
            fhDataType = new T8DtFunctionalityHandle(null);
            for (T8FunctionalityHandle functionalityHandle : groupHandle.getFunctionalities())
            {
                functionalityArray.add(fhDataType.serialize(functionalityHandle));
            }

            // Serialize all sub-groups.
            if (groupHandle.getSubGroups().size() > 0)
            {
                JsonArray subGroupArray;

                subGroupArray = new JsonArray();
                groupHandleObject.add("subGroups", subGroupArray);
                for (T8FunctionalityGroupHandle subGroupHandle : groupHandle.getSubGroups())
                {
                    subGroupArray.add(this.serialize(subGroupHandle));
                }
            }

            // Return the final task list object.
            return groupHandleObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8FunctionalityGroupHandle deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DtFunctionalityHandle fhDataType;
            JsonObject groupHandleObject;
            JsonArray functionalityArray;
            JsonArray subGroupArray;
            T8FunctionalityGroupHandle groupHandle;
            String name;
            String description;
            String iconUri;

            // Get the JSON values.
            groupHandleObject = jsonValue.asObject();
            name = groupHandleObject.getString("name");
            description = groupHandleObject.getString("description");
            iconUri = groupHandleObject.getString("iconUri");
            functionalityArray = groupHandleObject.getJsonArray("functionalities");

            // Deserialize all functionalities in this group
            groupHandle = new T8FunctionalityGroupHandle(groupHandleObject.getString("id"), null);
            groupHandle.setDisplayName(name);
            groupHandle.setDescription(description);
            groupHandle.setIconUri(iconUri);
            fhDataType = new T8DtFunctionalityHandle(null);
            for (JsonValue fhJson : functionalityArray.values())
            {
                groupHandle.addFunctionality(fhDataType.deserialize(fhJson));
            }

            // Deserialize all sub-groups.
            subGroupArray = groupHandleObject.getJsonArray("subGroups");
            if (subGroupArray != null)
            {
                for (JsonValue subGroupJson : subGroupArray.values())
                {
                    groupHandle.addSubGroup(this.deserialize(subGroupJson));
                }
            }

            // Return the completed object.
            return groupHandle;
        }
        else return null;
    }
}