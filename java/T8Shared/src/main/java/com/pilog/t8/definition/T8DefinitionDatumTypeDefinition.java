package com.pilog.t8.definition;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDatumTypeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DEFINITION_DATUM_TYPE";
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_DATUM_TYPE";
    public static final String DISPLAY_NAME = "Definition Datum Type";
    public static final String DESCRIPTION = "An object that defines a definition datum type..";
    public static final String STORAGE_PATH = null;
    public static final String VERSION = "0";
    public enum Datum {EDITOR_DEFINITION};
    // -------- Definition Meta-Data -------- //
    
    public T8DefinitionDatumTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.EDITOR_DEFINITION.toString(), "Datum Editor", "The datum editor that will replace the default when this definition datum is edited."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.EDITOR_DEFINITION.toString().equals(datumIdentifier)) return createStringOptions(DataFilterConjunction.values());
        else return null;
    }    

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
}
