package com.pilog.t8.definition.security.authentication;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8AuthenticationHandler;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WaffleAuthenticationHandlerDefinition extends T8AuthenticationHandlerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_AUTHENTICATION_HANDLER_WAFFLE";
    public static final String DISPLAY_NAME = "Waffle Authentication Handler";
    public static final String DESCRIPTION = "A client-side handler that facilitates single-sign-on using Windows Authentication.";
    public enum Datum
    {
        SECURITY_PACKAGE,
        AUTHORIZED_USER_GROUP_SIDS
    };
    // -------- Definition Meta-Data -------- //

    public T8WaffleAuthenticationHandlerDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SECURITY_PACKAGE.toString(), "Security Package (SSPI)", "The Security Support Provider Interface (SSPI) to use for authentication.", "Negotiate"));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.AUTHORIZED_USER_GROUP_SIDS.toString(), "Authorized User Groups", "The SIDs of the user groups authorized for access.  If no groups are specified, all groups are deemed valid."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8AuthenticationHandler createAuthenticationHandler(T8ServerContext serverContext) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.security.authentication.T8WaffleAuthenticationHandler", new Class<?>[]{T8ServerContext.class, T8WaffleAuthenticationHandlerDefinition.class}, serverContext, this);
    }

    public String getSecurityPackage()
    {
        return getDefinitionDatum(Datum.SECURITY_PACKAGE);
    }

    public void setSecurityPackage(String securityPackage)
    {
        setDefinitionDatum(Datum.SECURITY_PACKAGE, securityPackage);
    }

    public List<String> getAuthorizedUserGroupSids()
    {
        List<String> value;

        value = getDefinitionDatum(Datum.AUTHORIZED_USER_GROUP_SIDS);
        return value != null ? value : new ArrayList<>();
    }

    public void setAuthorizedUserGroupSids(List<String> groupSids)
    {
        setDefinitionDatum(Datum.AUTHORIZED_USER_GROUP_SIDS, groupSids);
    }
}

