package com.pilog.t8.definition.flow.task.object;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectHandler;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectFieldDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8TaskObjectDefinition extends T8DataObjectDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_TASK";
    public static final String DISPLAY_NAME = "Task Object";
    public static final String DESCRIPTION = "An object referencing a Workflow Task.";
    public enum Datum
    {
        LANGUAGE_IDS,
        INPUT_PARAMETER_DEFINITIONS,
        DATA_ENTITY_ID,
        ENTITY_FIELD_MAPPING,
        CONSTRUCTOR_SCRIPT
    };
    // -------- Definition Meta-Data -------- //

    public T8TaskObjectDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.LANGUAGE_IDS.toString(), "Language Identifiers",  "The languages applicable to this task list."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters", "The input parameters required to refresh the data in this task list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_ID.toString(), "Data Entity", "The id of the data entity containing the task list data."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.ENTITY_FIELD_MAPPING.toString(), "Field Mapping:  List to Entity", "A mapping of task list fields to the corresponding entity fields on which the task list is based."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CONSTRUCTOR_SCRIPT.toString(), "Constructor Script", "The script used to extract and compile data from data records into data objects."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.DATA_ENTITY_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.ENTITY_FIELD_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityId();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataObjectFieldDefinition> objectFieldDefinitions;
                    List<T8DataEntityFieldDefinition> entityFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    objectFieldDefinitions = getFieldDefinitions();
                    entityFieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((objectFieldDefinitions != null) && (entityFieldDefinitions != null))
                    {
                        ArrayList<String> fieldIds;

                        fieldIds = new ArrayList<String>();
                        for (T8DataEntityFieldDefinition fieldDefinition : entityFieldDefinitions)
                        {
                            fieldIds.add(fieldDefinition.getPublicIdentifier());
                        }
                        Collections.sort(fieldIds);

                        identifierMap.put("$ID", fieldIds);
                        for (T8DataObjectFieldDefinition fieldDefinition : objectFieldDefinitions)
                        {
                            identifierMap.put(fieldDefinition.getIdentifier(), fieldIds);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.CONSTRUCTOR_SCRIPT.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TaskObjectConstructorScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        try
        {
            T8DefinitionTestHarness testHarness;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.test.harness.taskobject.T8TaskObjectTestHarness").getConstructor();
            testHarness = (T8DefinitionTestHarness)constructor.newInstance();
            return testHarness;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DataObjectHandler getDataObjectHandler(T8DataTransaction tx)
    {
        return T8Reflections.getInstance("com.pilog.t8.flow.task.object.T8TaskObjectHandler", new Class<?>[]{T8TaskObjectDefinition.class, T8DataTransaction.class}, this, tx);
    }

    public List<String> getLanguageIds()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.LANGUAGE_IDS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setLanguageIds(List<String> ids)
    {
        setDefinitionDatum(Datum.LANGUAGE_IDS.toString(), ids);
    }

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public String getDataEntityId()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_ID.toString());
    }

    public void setDataEntityId(String id)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_ID.toString(), id);
    }

    public Map<String, String> getDataEntityFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.ENTITY_FIELD_MAPPING.toString());
    }

    public void setDataEntityFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.ENTITY_FIELD_MAPPING.toString(), mapping);
    }

    public T8TaskObjectConstructorScriptDefinition getConstructorScript()
    {
        return getDefinitionDatum(Datum.CONSTRUCTOR_SCRIPT);
    }

    public void setConstructorScript(T8TaskObjectConstructorScriptDefinition scriptDefinition)
    {
        setDefinitionDatum(Datum.CONSTRUCTOR_SCRIPT, scriptDefinition);
    }
}
