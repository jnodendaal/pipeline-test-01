package com.pilog.t8.definition.flow;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtFlowStatus;
import com.pilog.t8.datatype.T8DtTaskDetails;
import com.pilog.t8.datatype.T8DtTaskFilter;
import com.pilog.t8.datatype.T8DtTaskListSummary;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtTaskCompletionResult;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLAggregateFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FlowManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_FIND_ACTIVE_INPUT_NODE_IIDS = "@OP_FLW_FIND_ACTIVE_INPUT_NODE_IIDS";
    public static final String OPERATION_RETRY_EXECUTION = "@OP_FLW_FLOW_RETRY_EXECUTION";
    public static final String OPERATION_GET_FLOW_COUNT = "@OP_FLW_GET_FLOW_COUNT";
    public static final String OPERATION_GET_CACHED_FLOW_COUNT = "@OP_FLW_GET_CACHED_FLOW_COUNT";
    public static final String OPERATION_CLEAR_FLOW_CACHE = "@OP_FLW_CLEAR_FLOW_CACHE";
    public static final String OPERATION_GET_FLOW_STATE = "@OP_FLW_GET_FLOW_STATE";
    public static final String OPERATION_GET_TASK_LIST_SUMMARY = "@OP_FLW_GET_TASK_LIST_SUMMARY";
    public static final String OPERATION_GET_TASK_LIST = "@OP_FLW_GET_TASK_LIST";
    public static final String OPERATION_GET_TASK_COUNT = "@OP_FLW_GET_TASK_COUNT";
    public static final String OPERATION_GET_TASK_DETAIL = "@OP_FLW_GET_TASK_DETAIL";
    public static final String OPERATION_GET_TASK_UI_DATA = "@OP_FLW_GET_TASK_UI_DATA";
    public static final String OPERATION_SET_TASK_PRIORITY = "@OP_FLW_SET_TASK_PRIORITY";
    public static final String OPERATION_DECREASE_TASK_PRIORITY = "@OP_FLW_DECREASE_TASK_PRIORITY";
    public static final String OPERATION_INCREASE_TASK_PRIORITY = "@OP_FLW_INCREASE_TASK_PRIORITY";
    public static final String OPERATION_CLAIM_TASK = "@OP_FLW_CLAIM_TASK";
    public static final String OPERATION_UNCLAIM_TASK = "@OP_FLW_UNCLAIM_TASK";
    public static final String OPERATION_COMPLETE_TASK = "@OP_FLW_COMPLETE_TASK";
    public static final String OPERATION_GET_SESSION_FLOW_STATUSES = "@OP_FLW_GET_SESSION_FLOW_STATUSES";
    public static final String OPERATION_GET_FLOW_STATUS = "@OP_FLW_GET_FLOW_STATUS";
    public static final String OPERATION_QUEUE_FLOW = "@OP_FLW_QUEUE_FLOW";
    public static final String OPERATION_START_NEW_FLOW = "@OP_FLW_START_NEW_FLOW";
    public static final String OPERATION_KILL_FLOW = "@OP_FLW_KILL_FLOW";
    public static final String OPERATION_SIGNAL = "@OP_FLW_FLOW_SIGNAL";
    public static final String OPERATION_REASSIGN_TASK = "@OP_FLW_REASSIGN_TASK";
    public static final String OPERATION_REFRESH_TASK_LIST = "@OP_FLW_REFRESH_TASK_LIST";
    public static final String OPERATION_RECACHE_TASK_ESCALATION_TRIGGERS = "@OP_FLW_RECACHE_TASK_ESCALATION_TRIGGERS";

    public static final String PARAMETER_FLOW_COUNT = "$P_FLOW_COUNT";
    public static final String PARAMETER_TASK_COUNT = "$P_TASK_COUNT";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_OUTPUT_PARAMETERS = "$P_OUTPUT_PARAMETERS";
    public static final String PARAMETER_TASK_COMPLETION_RESULT = "$P_TASK_COMPLETION_RESULT";
    public static final String PARAMETER_TASK_LIST_SUMMARY = "$P_TASK_LIST_SUMMARY";
    public static final String PARAMETER_TASK_FILTER = "$P_TASK_FILTER";
    public static final String PARAMETER_TASK_DEFINITION = "$P_TASK_DEFINITION";
    public static final String PARAMETER_TASK_LIST = "$P_TASK_LIST";
    public static final String PARAMETER_TASK_IID = "$P_TASK_IID";
    public static final String PARAMETER_TASK_PRIORITY = "$P_TASK_PRIORITY";
    public static final String PARAMETER_TASK_PRIORITY_AMOUNT = "$P_TASK_PRIORITY_AMOUNT";
    public static final String PARAMETER_TASK_DETAIL = "$P_TASK_DETAIL";
    public static final String PARAMETER_TASK_UI_DATA = "$P_TASK_UI_DATA";
    public static final String PARAMETER_TASK_DETAIL_MODULE_DEFINITION = "$P_TASK_DETAIL_MODULE_DEFINITION";
    public static final String PARAMETER_NODE_IID = "$P_NODE_IID";
    public static final String PARAMETER_NODE_IIDS = "$P_NODE_IIDS";
    public static final String PARAMETER_FLOW_IID = "$P_FLOW_IID";
    public static final String PARAMETER_PARENT_FLOW_IID = "$P_PARENT_FLOW_IID";
    public static final String PARAMETER_FLOW_STATE = "$P_FLOW_STATE";
    public static final String PARAMETER_FLOW_ID = "$P_FLOW_ID";
    public static final String PARAMETER_FLOW_DEFINITION = "$P_FLOW_DEFINITION";
    public static final String PARAMETER_FLOW_STATUS = "$P_FLOW_STATUS";
    public static final String PARAMETER_FLOW_STATUS_LIST = "$P_FLOW_STATUS_LIST";
    public static final String PARAMETER_SIGNAL_ID = "$P_SIGNAL_ID";
    public static final String PARAMETER_SIGNAL_PARAMETERS = "$P_SIGNAL_PARAMETERS";
    public static final String PARAMETER_USER_ID = "$P_USER_ID";
    public static final String PARAMETER_PROFILE_ID = "$P_PROFILE_ID";
    public static final String PARAMETER_INCLUDE_DISPLAY_DATA = "$P_INCLUDE_DISPLAY_DETAILS";
    public static final String PARAMETER_REFRESH_TYPE = "$P_REFRESH_TYPE";
    public static final String PARAMETER_ID_LIST = "$P_ID_LIST";

    public static final String FLOW_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_FLOW";
    public static final String FLOW_HISTORY_DE_IDENTIFIER = "@E_HISTORY_FLOW";
    public static final String FLOW_HISTORY_PAR_DS_IDENTIFIER = "@DS_HISTORY_FLOW_PAR";
    public static final String FLOW_HISTORY_PAR_DE_IDENTIFIER = "@E_HISTORY_FLOW_PAR";
    public static final String FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_FLOW_DATA_OBJECT";
    public static final String FLOW_DATA_OBJECT_HISTORY_DE_IDENTIFIER = "@E_HISTORY_FLOW_DATA_OBJECT";
    public static final String NODE_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_FLOW_NODE";
    public static final String NODE_HISTORY_DE_IDENTIFIER = "@E_HISTORY_FLOW_NODE";
    public static final String NODE_HISTORY_PAR_DS_IDENTIFIER = "@DS_HISTORY_FLOW_NODE_PAR";
    public static final String NODE_HISTORY_PAR_DE_IDENTIFIER = "@E_HISTORY_FLOW_NODE_PAR";
    public static final String TASK_HISTORY_DS_IDENTIFIER = "@DS_HISTORY_TASK";
    public static final String TASK_HISTORY_DE_IDENTIFIER = "@E_HISTORY_TASK";
    public static final String TASK_HISTORY_SUMMARY_DS_IDENTIFIER = "@DS_HISTORY_TASK_SUMMARY";
    public static final String TASK_HISTORY_SUMMARY_DE_IDENTIFIER = "@E_HISTORY_TASK_SUMMARY";
    public static final String TASK_HISTORY_PAR_DS_IDENTIFIER = "@DS_HISTORY_TASK_PAR";
    public static final String TASK_HISTORY_PAR_DE_IDENTIFIER = "@E_HISTORY_TASK_PAR";

    // STATE_FLOW.
    public static final String STATE_FLOW_DS_IDENTIFIER = "@DS_STATE_FLOW";
    public static final String STATE_FLOW_DE_IDENTIFIER = "@E_STATE_FLOW";

    // STATE_FLOW_INPUT_PAR.
    public static final String STATE_FLOW_INPUT_PAR_DS_IDENTIFIER = "@DS_STATE_FLOW_INPUT_PAR";
    public static final String STATE_FLOW_INPUT_PAR_DE_IDENTIFIER = "@E_STATE_FLOW_INPUT_PAR";

    // STATE_FLOW_NODE.
    public static final String STATE_FLOW_NODE_DS_IDENTIFIER = "@DS_STATE_FLOW_NODE";
    public static final String STATE_FLOW_NODE_DE_IDENTIFIER = "@E_STATE_FLOW_NODE";

    // STATE_FLOW_NODE_EXEC_PAR.
    public static final String STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER = "@DS_STATE_FLOW_NODE_EXEC_PAR";
    public static final String STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER = "@E_STATE_FLOW_NODE_EXEC_PAR";

    // STATE_FLOW_NODE_INPUT_PAR.
    public static final String STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER = "@DS_STATE_FLOW_NODE_INPUT_PAR";
    public static final String STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER = "@E_STATE_FLOW_NODE_INPUT_PAR";

    // STATE_FLOW_NODE_OUTPUT_NODE.
    public static final String STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER = "@DS_STATE_FLOW_NODE_OUTPUT_NODE";
    public static final String STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER = "@E_STATE_FLOW_NODE_OUTPUT_NODE";

    // STATE_FLOW_NODE_OUTPUT_PAR.
    public static final String STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER = "@DS_STATE_FLOW_NODE_OUTPUT_PAR";
    public static final String STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER = "@E_STATE_FLOW_NODE_OUTPUT_PAR";

    // STATE_FLOW_TASK_SUMMARY - Summary for state flow task
    public static final String STATE_FLOW_TASK_SUMMARY_DS_IDENTIFIER = "@DS_STATE_FLOW_TASK_SUMMARY";
    public static final String STATE_FLOW_TASK_SUMMARY_DE_IDENTIFIER = "@E_STATE_FLOW_TASK_SUMMARY";

    // STATE_FLOW_TASK.
    public static final String STATE_FLOW_TASK_DS_IDENTIFIER = "@DS_STATE_FLOW_TASK";
    public static final String STATE_FLOW_TASK_DE_IDENTIFIER = "@E_STATE_FLOW_TASK";

    // STATE_FLOW_TASK_INPUT_PAR.
    public static final String STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER = "@DS_STATE_FLOW_TASK_INPUT_PAR";
    public static final String STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER = "@E_STATE_FLOW_TASK_INPUT_PAR";

    // STATE_FLOW_TASK_OUTPUT_PAR.
    public static final String STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER = "@DS_STATE_FLOW_TASK_OUTPUT_PAR";
    public static final String STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER = "@E_STATE_FLOW_TASK_OUTPUT_PAR";

    // STATE_FLOW_TASK_DETAIL_SUMMARY - Summary for joined state flow task details
    public static final String STATE_FLOW_TASK_DETAIL_SUMMARY_DS_IDENTIFIER = "@DS_STATE_FLOW_TASK_DETAIL_SUMMARY";
    public static final String STATE_FLOW_TASK_DETAIL_SUMMARY_DE_IDENTIFIER = "@E_STATE_FLOW_TASK_DETAIL_SUMMARY";

    // STATE_FLOW_DATA_OBJECT.
    public static final String STATE_FLOW_DATA_OBJECT_DS_IDENTIFIER = "@DS_STATE_FLOW_DATA_OBJECT";
    public static final String STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER = "@E_STATE_FLOW_DATA_OBJECT";

    // STATE_FLOW_WAIT_KEY.
    public static final String STATE_FLOW_WAIT_KEY_DS_IDENTIFIER = "@DS_STATE_FLOW_WAIT_KEY";
    public static final String STATE_FLOW_WAIT_KEY_DE_IDENTIFIER = "@E_STATE_FLOW_WAIT_KEY";

    // STATE_FLOW_EVENT_QUEUE.
    public static final String STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER = "@DS_STATE_FLOW_EVENT_QUEUE";
    public static final String STATE_FLOW_EVENT_QUEUE_DE_IDENTIFIER = "@E_STATE_FLOW_EVENT_QUEUE";

    // FLOW_TASK_ESCALATION.
    public static final String FLOW_TASK_ESCALATION_DS_IDENTIFIER = "@DS_FLOW_TASK_ESCALATION";
    public static final String FLOW_TASK_ESCALATION_DE_IDENTIFIER = "@E_FLOW_TASK_ESCALATION";

    // STATE_FLOW_TASK_ESCALATION_GROUP.
    public static final String STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER = "@DS_STATE_FLOW_TASK_ESCALATION_GROUP";
    public static final String STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER = "@E_STATE_FLOW_TASK_ESCALATION_GROUP";

    // Field identifiers.
    public static final String F_ISSUED_TIME = "$ISSUED_TIME";
    public static final String F_ISSUED_USER_ID = "$ISSUED_USER_ID";
    public static final String F_ISSUED_TIME_SPAN = "$ISSUED_TIME_SPAN";
    public static final String F_ISSUED_BUSINESS_TIME_SPAN = "$ISSUED_BUSINESS_TIME_SPAN";
    public static final String F_CLAIMED_TIME = "$CLAIMED_TIME";
    public static final String F_CLAIMED_USER_ID = "$CLAIMED_USER_ID";
    public static final String F_USER_ID = "$USER_ID";
    public static final String F_CLAIMED_TIME_SPAN = "$CLAIMED_TIME_SPAN";
    public static final String F_CLAIMED_BUSINESS_TIME_SPAN = "$CLAIMED_BUSINESS_TIME_SPAN";
    public static final String F_TOTAL_TIME_SPAN = "$TOTAL_TIME_SPAN";
    public static final String F_TOTAL_BUSINESS_TIME_SPAN = "$TOTAL_BUSINESS_TIME_SPAN";
    public static final String F_COMPLETED_TIME = "$COMPLETED_TIME";
    public static final String F_COMPLETED_USER_ID = "$COMPLETED_USER_ID";
    public static final String F_TRIGGER_IID = "$TRIGGER_IID";
    public static final String F_PARENT_PARAMETER_IID = "$PARENT_PARAMETER_IID";
    public static final String F_PARAMETER_IID = "$PARAMETER_IID";
    public static final String F_PARAMETER_ID = "$PARAMETER_ID";
    public static final String F_PARAMETER_KEY = "$PARAMETER_KEY";
    public static final String F_EVENT_IID = "$EVENT_IID";
    public static final String F_TIME = "$TIME";
    public static final String F_FLOW_IID = "$FLOW_IID";
    public static final String F_STATUS = "$STATUS";
    public static final String F_NODE_IID = "$NODE_IID";
    public static final String F_TASK_IID = "$TASK_IID";
    public static final String F_FLOW_PROFILE_ID = "$FLOW_PROFILE_ID";
    public static final String F_FLOW_PARAMETERS = "$FLOW_PARAMETERS";
    public static final String F_USER_PROFILE_ID = "$USER_PROFILE_ID";
    public static final String F_CLAIMED_BY_USER_ID = "$CLAIMED_BY_USER_ID";
    public static final String F_TASK_COUNT = "$CLAIMED_COUNT";
    public static final String F_GROUP_ID = "$GROUP_ID";
    public static final String F_TASK_ID = "$TASK_ID";
    public static final String F_NODE_ID = "$NODE_ID";
    public static final String F_INPUT_NODE_ID = "$INPUT_NODE_ID";
    public static final String F_FLOW_ID = "$FLOW_ID";
    public static final String F_TYPE = "$TYPE";
    public static final String F_PROJECT_ID = "$PROJECT_ID";
    public static final String F_TIME_ELAPSED = "$TIME_ELAPSED";
    public static final String F_RECURRING = "$RECURRING";
    public static final String F_PRIORITY = "$PRIORITY";
    public static final String F_PRIORITY_INCREASE = "$PRIORITY_INCREASE";
    public static final String F_EVENT = "$EVENT";
    public static final String F_TIME_STARTED = "$TIME_STARTED";
    public static final String F_TIME_COMPLETED = "$TIME_COMPLETED";
    public static final String F_TIME_ISSUED = "$TIME_ISSUED";
    public static final String F_TIME_CLAIMED = "$TIME_CLAIMED";
    public static final String F_TIME_ESCALATED = "$TIME_ESCALATED";
    public static final String F_TIME_ACTIVATED = "$TIME_ACTIVATED";
    public static final String F_ESCALATION_LEVEL = "$ESCALATION_LEVEL";
    public static final String F_ADJUSTED_TIME_ISSUED = "$ADJUSTED_TIME_ISSUED";
    public static final String F_ADJUSTED_TIME_ESCALATED = "$ADJUSTED_TIME_ESCALATED";
    public static final String F_GROUP_TIME_SPAN = "$GROUP_TIME_SPAN";
    public static final String F_TIME_SPAN_ISSUED = "$TIME_SPAN_ISSUED";
    public static final String F_TIME_SPAN_CLAIMED = "$TIME_SPAN_CLAIMED";
    public static final String F_INITIATOR_ID = "$INITIATOR_ID";
    public static final String F_INITIATOR_IID = "$INITIATOR_IID";
    public static final String F_INITIATOR_ORG_ID = "$INITIATOR_ORG_ID";
    public static final String F_INITIATOR_ROOT_ORG_ID = "$INITIATOR_ROOT_ORG_ID";
    public static final String F_RESTRICTION_USER_ID = "$RESTRICTION_USER_ID";
    public static final String F_RESTRICTION_PROFILE_ID = "$RESTRICTION_PROFILE_ID";
    public static final String F_TIME_SPAN_TOTAL = "$TIME_SPAN_TOTAL";
    public static final String F_TIME_SPAN_BUSINESS_ISSUED = "$TIME_SPAN_BUSINESS_ISSUED";
    public static final String F_TIME_SPAN_BUSINESS_CLAIMED = "$TIME_SPAN_BUSINESS_CLAIMED";
    public static final String F_TIME_SPAN_BUSINESS_TOTAL = "$TIME_SPAN_BUSINESS_TOTAL";
    public static final String F_COMPLETED_EXECUTION_STEPS = "$COMPLETED_EXECUTION_STEPS";
    public static final String F_GROUP_ITERATION = "$GROUP_ITERATION";
    public static final String F_INFORMATION = "$INFORMATION";
    public static final String F_SEQUENCE = "$SEQUENCE";
    public static final String F_ESCALATION_ID = "$ESCALATION_ID";
    public static final String F_DATA_OBJECT_IID = "$DATA_OBJECT_IID";
    public static final String F_DATA_OBJECT_ID = "$DATA_OBJECT_ID";
    public static final String F_VALUE = "$VALUE";
    public static final String F_ID = "$ID";
    public static final String F_EVENT_PARAMETERS = "$EVENT_PARAMETERS";
    public static final String F_DATA = "$DATA";

    // Table Names.
    public static final String TABLE_FLOW_TASK_ESCALATION = "FLW_TSK_ESC";
    public static final String TABLE_HISTORY_FLOW = "HIS_FLW";
    public static final String TABLE_HISTORY_FLOW_PAR = "HIS_FLW_PAR";
    public static final String TABLE_HISTORY_FLOW_NODE = "HIS_FLW_NDE";
    public static final String TABLE_HISTORY_FLOW_NODE_PAR = "HIS_FLW_NDE_PAR";
    public static final String TABLE_HISTORY_TASK = "HIS_FLW_TSK";
    public static final String TABLE_HISTORY_TASK_SUMMARY = "HIS_FLW_TSK_SUM";
    public static final String TABLE_HISTORY_TASK_PAR = "HIS_FLW_TSK_PAR";
    public static final String TABLE_HISTORY_FLOW_DATA_OBJECT = "HIS_FLW_DAT_OBJ";
    public static final String TABLE_STATE_FLOW = "FLW";
    public static final String TABLE_STATE_FLOW_INPUT_PAR = "FLW_INP_PAR";
    public static final String TABLE_STATE_FLOW_NODE = "FLW_NDE";
    public static final String TABLE_STATE_FLOW_NODE_EXEC_PAR = "FLW_NDE_EXE_PAR";
    public static final String TABLE_STATE_FLOW_NODE_INPUT_PAR = "FLW_NDE_INP_PAR";
    public static final String TABLE_STATE_FLOW_NODE_OUTPUT_NODE = "FLW_NDE_OUT_NDE";
    public static final String TABLE_STATE_FLOW_NODE_OUTPUT_PAR = "FLW_NDE_OUT_PAR";
    public static final String TABLE_STATE_FLOW_TASK = "FLW_TSK";
    public static final String TABLE_STATE_FLOW_TASK_INPUT_PAR = "FLW_TSK_INP_PAR";
    public static final String TABLE_STATE_FLOW_TASK_OUTPUT_PAR = "FLW_TSK_OUT_PAR";
    public static final String TABLE_STATE_FLOW_WAIT_KEY = "FLW_WAT_KEY";
    public static final String TABLE_STATE_FLOW_DATA_OBJECT = "FLW_DAT_OBJ";
    public static final String TABLE_STATE_FLOW_EVENT_QUEUE = "FLW_EVT_QUE";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8SQLBasedGroupByDataSourceDefinition sqlGroupByDataSourceDefinition;
        T8TableDataSourceDefinition tableDataSourceDefinition;
        T8SQLDataSourceDefinition sqlDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // FLOW_TASK_ESCALATION Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(FLOW_TASK_ESCALATION_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_FLOW_TASK_ESCALATION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TRIGGER_IID, "TRIGGER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_ID, "TASK_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ID, "GROUP_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_ELAPSED, "TIME_ELAPSED", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY, "PRIORITY", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY_INCREASE, "PRIORITY_INCREASE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECURRING, "RECURRING", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ESCALATION_ID, "ESCALATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Flow History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(FLOW_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FLOW);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ORG_ID, "INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME, "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_PARAMETERS, "EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INFORMATION, "INFORMATION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Flow History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(FLOW_HISTORY_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FLOW_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_ID, "PARAMETER_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ID, "IDENTIFIER", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Node History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(NODE_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FLOW_NODE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_ID, "NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME, "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INFORMATION, "INFORMATION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Node History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(NODE_HISTORY_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FLOW_NODE_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Task History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(TASK_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_TASK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_ID, "NODE_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_ID, "TASK_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_IID, "TASK_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_PROFILE_ID, "USER_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_PROFILE_ID, "FLOW_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME, "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_PARAMETERS, "EVENT_PARAMETERS", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_PARAMETERS, "FLOW_PARAMETERS", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Task History Symmary Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(TASK_HISTORY_SUMMARY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_TASK_SUMMARY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_ID, "NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_ID, "TASK_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_IID, "TASK_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_PROFILE_ID, "FLOW_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ID, "GROUP_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ITERATION, "GROUP_ITERATION", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ESCALATION_LEVEL, "ESCALATION_LEVEL", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ISSUED_TIME, "ISSUED_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ISSUED_TIME_SPAN, "ISSUED_TIME_SPAN", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ISSUED_BUSINESS_TIME_SPAN, "ISSUED_BUSINESS_TIME_SPAN", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CLAIMED_TIME, "CLAIMED_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CLAIMED_TIME_SPAN, "CLAIMED_TIME_SPAN", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CLAIMED_BUSINESS_TIME_SPAN, "CLAIMED_BUSINESS_TIME_SPAN", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_COMPLETED_TIME, "COMPLETED_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TOTAL_TIME_SPAN, "TOTAL_TIME_SPAN", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TOTAL_BUSINESS_TIME_SPAN, "TOTAL_BUSINESS_TIME_SPAN", false, false, T8DataType.LONG));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Task History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(TASK_HISTORY_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_TASK_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_ID, "PARAMETER_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ID, "IDENTIFIER", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Flow Data Object History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FLOW_DATA_OBJECT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_OBJECT_ID, "DATA_OBJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_OBJECT_IID, "DATA_OBJECT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME, "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STATUS, "STATUS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ORG_ID, "INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ROOT_ORG_ID, "INITIATOR_ROOT_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_STARTED, "TIME_STARTED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_INPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_INPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_INPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_NODE Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_NODE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_NODE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_ID, "NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STATUS, "STATUS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$COMPLETED_EXECUTION_STEPS", "COMPLETED_EXECUTION_STEPS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME_ACTIVATED", "TIME_ACTIVATED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INFORMATION, "INFORMATION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_NODE_EXEC_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_NODE_EXEC_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_NODE_INPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_NODE_INPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INPUT_NODE_ID, "INPUT_NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_NODE_OUTPUT_NODE Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_NODE_OUTPUT_NODE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OUTPUT_NODE_IID", "OUTPUT_NODE_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OUTPUT_NODE_ID", "OUTPUT_NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_NODE_OUTPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_NODE_OUTPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_TASK Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_TASK_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_TASK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_ID, "NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_IID, "TASK_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_ID, "TASK_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROJECT_ID, "PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ID, "GROUP_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RESTRICTION_USER_ID, "RESTRICTION_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RESTRICTION_PROFILE_ID, "RESTRICTION_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ORG_ID, "INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ROOT_ORG_ID, "INITIATOR_ROOT_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CLAIMED_BY_USER_ID, "CLAIMED_BY_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY, "PRIORITY", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_ISSUED, "TIME_ISSUED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_CLAIMED, "TIME_CLAIMED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_COMPLETED, "TIME_COMPLETED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_ESCALATED, "TIME_ESCALATED", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_SPAN_ISSUED, "TIME_SPAN_ISSUED", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_SPAN_CLAIMED, "TIME_SPAN_CLAIMED", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_SPAN_TOTAL, "TIME_SPAN_TOTAL", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_SPAN_BUSINESS_ISSUED, "TIME_SPAN_BUSINESS_ISSUED", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_SPAN_BUSINESS_CLAIMED, "TIME_SPAN_BUSINESS_CLAIMED", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_SPAN_BUSINESS_TOTAL, "TIME_SPAN_BUSINESS_TOTAL", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ITERATION, "GROUP_ITERATION", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ESCALATION_LEVEL, "ESCALATION_LEVEL", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_TASK_SUMMARY SQL Group By Data Source
        sqlGroupByDataSourceDefinition = new T8SQLBasedGroupByDataSourceResourceDefinition(STATE_FLOW_TASK_SUMMARY_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.setOriginatingDataSourceIdentifier(STATE_FLOW_TASK_DS_IDENTIFIER);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_TASK_ID);
        sqlGroupByDataSourceDefinition.addGroupByFieldIdentifier(F_CLAIMED_BY_USER_ID);
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_ID, "TASK_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CLAIMED_BY_USER_ID, "CLAIMED_BY_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlGroupByDataSourceDefinition.addFieldDefinition(new T8SQLAggregateFieldResourceDefinition(F_TASK_COUNT, "TASK_COUNT", false, false, T8DataType.INTEGER, "COUNT(TASK_ID)"));
        definitions.add(sqlGroupByDataSourceDefinition);

        // STATE_FLOW_TASK_INPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_TASK_INPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_IID, "TASK_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_TASK_OUTPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_TASK_OUTPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_IID, "TASK_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_DATA_OBJECT Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_DATA_OBJECT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_DATA_OBJECT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_OBJECT_IID, "DATA_OBJECT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_OBJECT_ID, "DATA_OBJECT_ID", false, true, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_WAIT_KEY Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_WAIT_KEY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_WAIT_KEY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NODE_IID, "NODE_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_ID", "KEY_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_FLOW_IID", "KEY_FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_FLOW_ID", "KEY_FLOW_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_NODE_IID", "KEY_NODE_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_NODE_ID", "KEY_NODE_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_TASK_IID", "KEY_TASK_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_TASK_ID", "KEY_TASK_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_DATA_OBJECT_IID", "KEY_DATA_OBJECT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_DATA_OBJECT_ID", "KEY_DATA_OBJECT_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_SIGNAL_ID", "KEY_SIGNAL_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_SIGNAL_PARAMETER", "KEY_SIGNAL_PARAMETER", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$KEY_PROJECT_ID", "KEY_PROJECT_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_EVENT_QUEUE Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FLOW_EVENT_QUEUE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME, "TIME", false, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA, "DATA", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FLOW_TASK_ESCALATION Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLWithString("WITH GROUP_TIME_SPANS AS\n" +
        "(\n" +
        "	SELECT \n" +
        "	FLOW_IID,\n" +
        "	GROUP_ID,\n" +
        "	GROUP_ITERATION,\n" +
        "	COALESCE(SUM(TIME_SPAN_BUSINESS_ISSUED) + SUM(TIME_SPAN_BUSINESS_CLAIMED), 0) AS GROUP_TIME_SPAN\n" +
        "	FROM FLW_TSK\n" +
        "	WHERE GROUP_ID IS NOT NULL\n" +
        "	GROUP BY FLOW_IID, GROUP_ID, GROUP_ITERATION\n" +
        ")");
        sqlDataSourceDefinition.setSQLSelectString("SELECT \n" +
        "	FLW_TSK.FLOW_IID, \n" +
        "	FLW_TSK.FLOW_ID, \n" +
        "	FLW_TSK.TASK_IID,\n" +
        "	FLW_TSK.TASK_ID,\n" +
        "	FLW_TSK.GROUP_ID,\n" +
        "	FLW_TSK.GROUP_ITERATION,\n" +
        "	FLW_TSK.TIME_ISSUED,\n" +
        "	FLW_TSK.TIME_CLAIMED,\n" +
        "	FLW_TSK.TIME_COMPLETED,\n" +
        "	FLW_TSK.TIME_ESCALATED,\n" +
        "	FLW_TSK.ESCALATION_LEVEL,\n" +
        "	FLW_TSK.PRIORITY,\n" +
        "	GRP_TMS.GROUP_TIME_SPAN,\n" +
        "	<<-ADJUSTED_TIME_ISSUED->> AS ADJUSTED_TIME_ISSUED,\n" +
        "	<<-ADJUSTED_TIME_ESCALATED->> AS ADJUSTED_TIME_ESCALATED\n" +
        "FROM FLW_TSK\n" +
        "INNER JOIN GROUP_TIME_SPANS GRP_TMS\n" +
        "ON FLW_TSK.FLOW_IID = GRP_TMS.FLOW_IID\n" +
        "AND FLW_TSK.GROUP_ID = GRP_TMS.GROUP_ID\n" +
        "AND FLW_TSK.GROUP_ITERATION = GRP_TMS.GROUP_ITERATION\n" +
        "WHERE FLW_TSK.TIME_COMPLETED IS NULL");
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_IID, "TASK_IID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TASK_ID, "TASK_ID", true, true, T8DataType.DEFINITION_IDENTIFIER));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ID, "GROUP_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ITERATION, "GROUP_ITERATION", false, false, T8DataType.INTEGER));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_ISSUED, "TIME_ISSUED", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_CLAIMED, "TIME_CLAIMED", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_COMPLETED, "TIME_COMPLETED", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_ESCALATED, "TIME_ESCALATED", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ESCALATION_LEVEL, "ESCALATION_LEVEL", false, false, T8DataType.INTEGER));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY, "PRIORITY", false, false, T8DataType.INTEGER));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_TIME_SPAN, "GROUP_TIME_SPAN", false, false, T8DataType.LONG));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ADJUSTED_TIME_ISSUED, "ADJUSTED_TIME_ISSUED", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ADJUSTED_TIME_ESCALATED, "ADJUSTED_TIME_ESCALATED", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.setSQLParameterExpressionMap(HashMaps.newHashMap("ADJUSTED_TIME_ISSUED", "dbAdaptor.getSQLColumnTimestampAddMilliseconds(\"TIME_ISSUED\",\"-GRP_TMS.GROUP_TIME_SPAN\")", "ADJUSTED_TIME_ESCALATED", "dbAdaptor.getSQLColumnTimestampAddMilliseconds(\"TIME_ESCALATED\",\"-GRP_TMS.GROUP_TIME_SPAN\")"));
        definitions.add(sqlDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // FLOW_TASK_ESCALATION Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(FLOW_TASK_ESCALATION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(FLOW_TASK_ESCALATION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TRIGGER_IID, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_TRIGGER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ID, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_GROUP_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_ELAPSED, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_TIME_ELAPSED, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_PRIORITY, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY_INCREASE, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_PRIORITY_INCREASE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECURRING, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_RECURRING, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ESCALATION_ID, FLOW_TASK_ESCALATION_DS_IDENTIFIER + F_ESCALATION_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(entityDefinition);

        // Flow History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(FLOW_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(FLOW_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, FLOW_HISTORY_DS_IDENTIFIER + F_EVENT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, FLOW_HISTORY_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, FLOW_HISTORY_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, FLOW_HISTORY_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, FLOW_HISTORY_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, FLOW_HISTORY_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ORG_ID, FLOW_HISTORY_DS_IDENTIFIER + F_INITIATOR_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, FLOW_HISTORY_DS_IDENTIFIER + F_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, FLOW_HISTORY_DS_IDENTIFIER + F_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_PARAMETERS, FLOW_HISTORY_DS_IDENTIFIER + F_EVENT_PARAMETERS, false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INFORMATION, FLOW_HISTORY_DS_IDENTIFIER + F_INFORMATION, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Flow History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(FLOW_HISTORY_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(FLOW_HISTORY_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, FLOW_HISTORY_PAR_DS_IDENTIFIER + F_EVENT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_ID, FLOW_HISTORY_PAR_DS_IDENTIFIER + F_PARAMETER_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ID, FLOW_HISTORY_PAR_DS_IDENTIFIER + F_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, FLOW_HISTORY_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Node History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(NODE_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(NODE_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, NODE_HISTORY_DS_IDENTIFIER + F_EVENT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, NODE_HISTORY_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, NODE_HISTORY_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_ID, NODE_HISTORY_DS_IDENTIFIER + F_NODE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, NODE_HISTORY_DS_IDENTIFIER + F_NODE_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, NODE_HISTORY_DS_IDENTIFIER + F_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, NODE_HISTORY_DS_IDENTIFIER + F_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INFORMATION, NODE_HISTORY_DS_IDENTIFIER + F_INFORMATION, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Node History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(NODE_HISTORY_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(NODE_HISTORY_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, NODE_HISTORY_PAR_DS_IDENTIFIER + F_EVENT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, NODE_HISTORY_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, NODE_HISTORY_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, NODE_HISTORY_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, NODE_HISTORY_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, NODE_HISTORY_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, NODE_HISTORY_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Task History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(TASK_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(TASK_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, TASK_HISTORY_DS_IDENTIFIER + F_EVENT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, TASK_HISTORY_DS_IDENTIFIER + F_FLOW_ID, false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, TASK_HISTORY_DS_IDENTIFIER + F_FLOW_IID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_ID, TASK_HISTORY_DS_IDENTIFIER + F_NODE_ID, false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, TASK_HISTORY_DS_IDENTIFIER + F_NODE_IID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, TASK_HISTORY_DS_IDENTIFIER + F_TASK_ID, false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_IID, TASK_HISTORY_DS_IDENTIFIER + F_TASK_IID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, TASK_HISTORY_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, TASK_HISTORY_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_PROFILE_ID, TASK_HISTORY_DS_IDENTIFIER + F_USER_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_PROFILE_ID, TASK_HISTORY_DS_IDENTIFIER + F_FLOW_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, TASK_HISTORY_DS_IDENTIFIER + F_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, TASK_HISTORY_DS_IDENTIFIER + F_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_PARAMETERS, TASK_HISTORY_DS_IDENTIFIER + F_EVENT_PARAMETERS, false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_PARAMETERS, TASK_HISTORY_DS_IDENTIFIER + F_FLOW_PARAMETERS, false, false, T8DataType.LONG_STRING));
        definitions.add(entityDefinition);

        // Task History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(TASK_HISTORY_SUMMARY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(TASK_HISTORY_SUMMARY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_NODE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_NODE_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_IID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_TASK_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_USER_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_PROFILE_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_FLOW_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ID, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_GROUP_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ITERATION, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_GROUP_ITERATION, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ESCALATION_LEVEL, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_ESCALATION_LEVEL, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ISSUED_TIME, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_ISSUED_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ISSUED_TIME_SPAN, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_ISSUED_TIME_SPAN, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ISSUED_BUSINESS_TIME_SPAN, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_ISSUED_BUSINESS_TIME_SPAN, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CLAIMED_TIME, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_CLAIMED_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CLAIMED_TIME_SPAN, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_CLAIMED_TIME_SPAN, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CLAIMED_BUSINESS_TIME_SPAN, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_CLAIMED_BUSINESS_TIME_SPAN, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_COMPLETED_TIME, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_COMPLETED_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TOTAL_TIME_SPAN, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_TOTAL_TIME_SPAN, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TOTAL_BUSINESS_TIME_SPAN, TASK_HISTORY_SUMMARY_DS_IDENTIFIER + F_TOTAL_BUSINESS_TIME_SPAN, false, false, T8DataType.LONG));
        definitions.add(entityDefinition);

        // Task History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(TASK_HISTORY_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(TASK_HISTORY_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, TASK_HISTORY_PAR_DS_IDENTIFIER + F_EVENT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_ID, TASK_HISTORY_PAR_DS_IDENTIFIER + F_PARAMETER_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ID, TASK_HISTORY_PAR_DS_IDENTIFIER + F_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, TASK_HISTORY_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Flow Data Object History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(FLOW_DATA_OBJECT_HISTORY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_EVENT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_ID, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_DATA_OBJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_IID, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_DATA_OBJECT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, FLOW_DATA_OBJECT_HISTORY_DS_IDENTIFIER + F_TIME, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // STATE_FLOW Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_DS_IDENTIFIER + F_FLOW_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, STATE_FLOW_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, STATE_FLOW_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STATUS, STATE_FLOW_DS_IDENTIFIER + F_STATUS, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, STATE_FLOW_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, STATE_FLOW_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ORG_ID, STATE_FLOW_DS_IDENTIFIER + F_INITIATOR_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ROOT_ORG_ID, STATE_FLOW_DS_IDENTIFIER + F_INITIATOR_ROOT_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_STARTED, STATE_FLOW_DS_IDENTIFIER + F_TIME_STARTED, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // STATE_FLOW_INPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_INPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_INPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FLOW_INPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_NODE Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_NODE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_NODE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_NODE_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_NODE_DS_IDENTIFIER + F_NODE_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_ID, STATE_FLOW_NODE_DS_IDENTIFIER + F_NODE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STATUS, STATE_FLOW_NODE_DS_IDENTIFIER + F_STATUS, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_NODE_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$COMPLETED_EXECUTION_STEPS", STATE_FLOW_NODE_DS_IDENTIFIER + "$COMPLETED_EXECUTION_STEPS", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME_ACTIVATED", STATE_FLOW_NODE_DS_IDENTIFIER + "$TIME_ACTIVATED", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INFORMATION, STATE_FLOW_NODE_DS_IDENTIFIER + F_INFORMATION, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_NODE_EXEC_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_NODE_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FLOW_NODE_EXEC_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_NODE_INPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_NODE_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INPUT_NODE_ID, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_INPUT_NODE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FLOW_NODE_INPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_NODE_OUTPUT_NODE Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER + F_NODE_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OUTPUT_NODE_IID", STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER + "$OUTPUT_NODE_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OUTPUT_NODE_ID", STATE_FLOW_NODE_OUTPUT_NODE_DS_IDENTIFIER + "$OUTPUT_NODE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(entityDefinition);

        // STATE_FLOW_NODE_OUTPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_NODE_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FLOW_NODE_OUTPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_TASK Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_TASK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_TASK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_TASK_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_TASK_DS_IDENTIFIER + F_NODE_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_NODE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_IID, STATE_FLOW_TASK_DS_IDENTIFIER + F_TASK_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROJECT_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_PROJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_GROUP_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RESTRICTION_USER_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_RESTRICTION_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RESTRICTION_PROFILE_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_RESTRICTION_PROFILE_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, STATE_FLOW_TASK_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ORG_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_INITIATOR_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ROOT_ORG_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_INITIATOR_ROOT_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CLAIMED_BY_USER_ID, STATE_FLOW_TASK_DS_IDENTIFIER + F_CLAIMED_BY_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY, STATE_FLOW_TASK_DS_IDENTIFIER + F_PRIORITY, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_ISSUED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_ISSUED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_CLAIMED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_CLAIMED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_COMPLETED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_COMPLETED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_ESCALATED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_ESCALATED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_SPAN_ISSUED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_SPAN_ISSUED, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_SPAN_CLAIMED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_SPAN_CLAIMED, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_SPAN_TOTAL, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_SPAN_TOTAL, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_SPAN_BUSINESS_ISSUED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_SPAN_BUSINESS_ISSUED, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_SPAN_BUSINESS_CLAIMED, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_SPAN_BUSINESS_CLAIMED, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_SPAN_BUSINESS_TOTAL, STATE_FLOW_TASK_DS_IDENTIFIER + F_TIME_SPAN_BUSINESS_TOTAL, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ITERATION, STATE_FLOW_TASK_DS_IDENTIFIER + F_GROUP_ITERATION, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ESCALATION_LEVEL, STATE_FLOW_TASK_DS_IDENTIFIER + F_ESCALATION_LEVEL, false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        // STATE_FLOW_TASK_SUMMARY Data Entity
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_TASK_SUMMARY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_TASK_SUMMARY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, STATE_FLOW_TASK_SUMMARY_DS_IDENTIFIER+F_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CLAIMED_BY_USER_ID, STATE_FLOW_TASK_SUMMARY_DS_IDENTIFIER+F_CLAIMED_BY_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_COUNT, STATE_FLOW_TASK_SUMMARY_DS_IDENTIFIER+F_TASK_COUNT, false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        // STATE_FLOW_TASK_INPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_IID, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_TASK_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FLOW_TASK_INPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_TASK_OUTPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_IID, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_TASK_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FLOW_TASK_OUTPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_TASK_DETAIL_SUMMARY Data Entity
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_TASK_DETAIL_SUMMARY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_TASK_DETAIL_SUMMARY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, STATE_FLOW_TASK_DETAIL_SUMMARY_DS_IDENTIFIER+F_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CLAIMED_BY_USER_ID, STATE_FLOW_TASK_DETAIL_SUMMARY_DS_IDENTIFIER+F_CLAIMED_BY_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_COUNT, STATE_FLOW_TASK_DETAIL_SUMMARY_DS_IDENTIFIER+F_TASK_COUNT, false, false, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        // STATE_FLOW_DATA_OBJECT Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_DATA_OBJECT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_DATA_OBJECT_DS_IDENTIFIER + F_FLOW_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_IID, STATE_FLOW_DATA_OBJECT_DS_IDENTIFIER + F_DATA_OBJECT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_ID, STATE_FLOW_DATA_OBJECT_DS_IDENTIFIER + F_DATA_OBJECT_ID, false, true, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_WAIT_KEY Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_WAIT_KEY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + F_FLOW_IID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NODE_IID, STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + F_NODE_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_ID", true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_FLOW_IID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_FLOW_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_FLOW_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_FLOW_ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_NODE_IID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_NODE_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_NODE_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_NODE_ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_TASK_IID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_TASK_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_TASK_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_TASK_ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_DATA_OBJECT_IID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_DATA_OBJECT_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_DATA_OBJECT_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_DATA_OBJECT_ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_SIGNAL_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_SIGNAL_ID", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_SIGNAL_PARAMETER", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_SIGNAL_PARAMETER", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$KEY_PROJECT_ID", STATE_FLOW_WAIT_KEY_DS_IDENTIFIER + "$KEY_PROJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(entityDefinition);

        // STATE_FLOW_EVENT_QUEUE Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_EVENT_QUEUE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER + F_EVENT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER + F_TYPE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER + F_TIME, false, true, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA, STATE_FLOW_EVENT_QUEUE_DS_IDENTIFIER + F_DATA, false, true, T8DataType.LONG_STRING));
        definitions.add(entityDefinition);

        // STATE_FLOW_TASK_ESCALATION Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_ID, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_IID, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_TASK_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TASK_ID, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ID, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_GROUP_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ITERATION, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_GROUP_ITERATION, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_ISSUED, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_TIME_ISSUED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_CLAIMED, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_TIME_CLAIMED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_COMPLETED, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_TIME_COMPLETED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_ESCALATED, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_TIME_ESCALATED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ESCALATION_LEVEL, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_ESCALATION_LEVEL, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_PRIORITY, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_TIME_SPAN, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_GROUP_TIME_SPAN, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ADJUSTED_TIME_ISSUED, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_ADJUSTED_TIME_ISSUED, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ADJUSTED_TIME_ESCALATED, STATE_FLOW_TASK_ESCALATION_GROUP_DS_IDENTIFIER + F_ADJUSTED_TIME_ESCALATED, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Flow Administration Operations.
        definition = new T8JavaServerOperationDefinition(OPERATION_FIND_ACTIVE_INPUT_NODE_IIDS);
        definition.setMetaDisplayName("Find Node Active Input Identifiers");
        definition.setMetaDescription("Returns the set of active input identifiers of the specified node.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$FindNodeActiveInputIdentifiersOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_IID, "Flow Instance Identifier", "The identifier of the flow instance to check.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NODE_IID, "Node Instance Identifier", "The identifier of the node instance to check.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NODE_IIDS, "Node Instance Identifiers", "A list of instance identifiers of the active input nodes for the specified node.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RETRY_EXECUTION);
        definition.setMetaDisplayName("Retry Flow Execution");
        definition.setMetaDescription("Retries execution of a flow node that is currently in a failure state.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$RetryExecutionOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_IID, "Flow Instance Identifier", "The identifier of the flow instance to check.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NODE_IID, "Node Instance Identifier", "The identifier of the node instance to check.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_FLOW_COUNT);
        definition.setMetaDisplayName("Get Flow Count");
        definition.setMetaDescription("Returns the total number of executing flows on the server.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetFlowCountOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_CACHED_FLOW_COUNT);
        definition.setMetaDisplayName("Get Cached Flow Count");
        definition.setMetaDescription("Returns the total number of flows cached on the server.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetCachedFlowCountOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CLEAR_FLOW_CACHE);
        definition.setMetaDisplayName("Clear Flow Cache");
        definition.setMetaDescription("Clears all non-executing flows from the server cache.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$ClearFlowCacheOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_FLOW_STATE);
        definition.setMetaDescription("Get Flow State");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetFlowStateOperation");
        definitions.add(definition);

        // Flow Operations.
        definition = new T8JavaServerOperationDefinition(OPERATION_GET_TASK_COUNT);
        definition.setMetaDisplayName("Get Task Count");
        definition.setMetaDescription("Returns the number of tasks filtered using the specified criteria.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetTaskCountOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_FILTER, "Task Filter", "The filter to be applied to the retrieval. Null if no filter should be applied.", T8DtTaskFilter.IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_COUNT, "Task Count", "The number of tasks associated with the user and filtered according to the filter supplied, if any.", T8DtTaskListSummary.IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_TASK_LIST_SUMMARY);
        definition.setMetaDisplayName("Get Task List Summary");
        definition.setMetaDescription("Returns the summary details of the list of tasks to be retrieved.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetTaskListSummaryOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_FILTER, "Task Filter", "The filter to be applied to the retrieval. Null if no filter should be applied.", T8DtTaskFilter.IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_LIST_SUMMARY, "Task List Summary", "The task list summary associated with the user and filtered according to the filter supplied, if any.", T8DtTaskListSummary.IDENTIFIER));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_GET_TASK_LIST);
        definition.setMetaDisplayName("Get Task List");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetTaskListOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_FILTER, "Task Filter", "The Task Filter to be used to filter the list of tasks retrieved. Additional to the basic filter.", T8DtTaskFilter.IDENTIFIER));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_LIST, "Task List", "The list of tasks associated with the user and filtered according to the filter supplied, if any.", T8DataType.LIST));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_SET_TASK_PRIORITY);
        definition.setMetaDisplayName("Set Task Priority");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$SetTaskPriorityOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task for which the priority should be set.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_PRIORITY, "Task Priority", "The new priority for the task.", T8DataType.INTEGER));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_DECREASE_TASK_PRIORITY);
        definition.setMetaDisplayName("Decrease Task Priority");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$DecreaseTaskPriorityOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task for which the priority should be set.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_PRIORITY_AMOUNT, "Decrease Amount", "The amount by which this priority will be decreased.", T8DataType.INTEGER));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_INCREASE_TASK_PRIORITY);
        definition.setMetaDisplayName("Increase Task Priority");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$IncreaseTaskPriorityOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task for which the priority should be set.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_PRIORITY_AMOUNT, "Increase Amount", "The amount by which this priority will be increased.", T8DataType.INTEGER));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_GET_TASK_DETAIL);
        definition.setMetaDisplayName("Get Task Details");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetTaskDetailsOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task for which the priority should be set.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_DETAIL, "Task Detail", "The task details of the task.", T8DtTaskDetails.IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_TASK_UI_DATA);
        definition.setMetaDisplayName("Get Task UI Data");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetTaskUiDataOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task for which to retrieve UI data.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_UI_DATA, "Task UI Data", "The data required to display the UI for this task.", T8DataType.MAP));
        definitions.add(definition);

	definition = new T8JavaServerOperationDefinition(OPERATION_REASSIGN_TASK);
        definition.setMetaDisplayName("Reassign Task");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$ReassignTaskOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The task to be reassigned.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_ID, "User Identifier", "The the user to which the task will be reassigned.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROFILE_ID, "Profile Identifier", "The profile to which the task will be reassigned.", T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_REFRESH_TASK_LIST);
        definition.setMetaDisplayName("Refresh Task List");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$RefreshTaskListOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_TYPE, "Refresh Type", "The type of refresh to perform, specifying the scope of the operation.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ID_LIST, "Id List", "The list of identifiers, used in conjunction with the refresh type to determine the scope of the operation.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RECACHE_TASK_ESCALATION_TRIGGERS);
        definition.setMetaDisplayName("Re-cache Task Escalation Triggers");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$RecacheTaskEscalationTriggersOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CLAIM_TASK);
        definition.setMetaDisplayName("Claim Task");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$ClaimTaskOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task to be claimed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_UNCLAIM_TASK);
        definition.setMetaDisplayName("Unclaim Task");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$UnclaimTaskOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_COMPLETE_TASK);
        definition.setMetaDisplayName("Complete Task");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$CompleteTaskOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IID, "Task Instance Identifier", "The instance identifier of the task to be completed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OUTPUT_PARAMETERS, "Task Output Parameters", "The output parameters of the task instance to be completed. Parameters to be passed back to the flow.", T8DataType.MAP));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_COMPLETION_RESULT, "Completion Result", "The result of the request to complete the specified task.", T8DtTaskCompletionResult.IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_SESSION_FLOW_STATUSES);
        definition.setMetaDisplayName("Get Session Flow Statuses");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetSessionFlowStatusesOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_FLOW_STATUS);
        definition.setMetaDisplayName("Get Flow Status");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$GetFlowStatusOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_IID, "Flow Instance Identifier", "The instance identifier of the flow for which to fetch the status.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_FILTER, "Task Filter", "The filter to use if tasks must be included in the returned status.", T8DtTaskFilter.IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_DISPLAY_DATA, "Include Display Data", "A flag to indicate whether or not display details of the requested information should be included.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_STATUS, "Flow Status", "The status of the specified flow requested.", T8DtFlowStatus.IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_QUEUE_FLOW);
        definition.setMetaDisplayName("Queue Flow");
        definition.setMetaDescription("Queues a flow, given the input parameters, to be started in the near future.");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$QueueFlowOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_ID, "Flow Definition Identifier", "The definition identifier of the flow for which a new instance should be queued.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INPUT_PARAMETERS, "Flow Input Parameters", "The input parameters, if any, required to initialize the flow.", new T8DtMap(T8DataType.STRING, T8DataType.STRING)));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PARENT_FLOW_IID, "Parent Flow Instance Identifier", "If the flow to be queued has a parent flow to which there is a dependence", T8DataType.STRING, true));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_STATUS, "Flow Status", "The status of the specified flow requested.", T8DtFlowStatus.IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_START_NEW_FLOW);
        definition.setMetaDisplayName("Start New Flow");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$StartNewFlowOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_ID, "Flow Definition Identifier", "The definition identifier of the flow for which a new instance should be started.", T8DataType.DEFINITION_IDENTIFIER, true));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_DEFINITION, "Flow Definition", "The definition of the flow for which a new instance should be started. Either the identifier, or the definition should be specified.", T8DataType.DEFINITION, true));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INPUT_PARAMETERS, "Flow Input Parameters", "The input parameters, if any, required to initialize the flow.", new T8DtMap(T8DataType.STRING, T8DataType.STRING)));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_STATUS, "Flow Status", "The status of the flow after the start signal has been issued.", T8DtFlowStatus.IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_KILL_FLOW);
        definition.setMetaDisplayName("Kill Flow");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$KillFlowOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_IID, "Flow Instance Identifier", "The instance identifier of the flow which should be killed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definitions.add(definition);

        // BPMN operations.
        definition = new T8JavaServerOperationDefinition(OPERATION_SIGNAL);
        definition.setMetaDisplayName("BPMN Signal");
        definition.setClassName("com.pilog.t8.flow.T8FlowOperations$SignalOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SIGNAL_ID, "Signal Identifier", "The The identifier of the signal to be triggered by the operation.", T8DataType.DEFINITION_IDENTIFIER, true));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SIGNAL_PARAMETERS, "Signal Input Parameters", "The input parameters, if any, required for the signal to be triggered.", T8DataType.MAP));
        definitions.add(definition);

        return definitions;
    }
}
