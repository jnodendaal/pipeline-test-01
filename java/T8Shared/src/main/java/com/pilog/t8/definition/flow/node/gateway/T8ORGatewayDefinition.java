package com.pilog.t8.definition.flow.node.gateway;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ORGatewayDefinition extends T8FlowGatewayDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_GATEWAY_OR";
    public static final String DISPLAY_NAME = "OR Gateway";
    public static final String DESCRIPTION = "A gateway that splits execution into parallel paths that are execution simultaneously.  Each path is only executed if a condition is met.";
    private enum Datum {FORK_CONDITION_DEFINITIONS};
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public T8ORGatewayDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FORK_CONDITION_DEFINITIONS.toString(), "Fork Conditions", "The conditions for each possible path that may be taken from this gateway."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FORK_CONDITION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FlowForkConditionDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.FORK_CONDITION_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.datumeditor.flow.ForkConditionDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.gateway.T8ORGatewayNode").getConstructor(T8Context.class, T8Flow.class, T8ORGatewayDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }

    public ArrayList<T8FlowForkConditionDefinition> getForkConditionDefinitions()
    {
        return (ArrayList<T8FlowForkConditionDefinition>)getDefinitionDatum(Datum.FORK_CONDITION_DEFINITIONS.toString());
    }

    public void setForkConditionDefinitions(ArrayList<T8FlowForkConditionDefinition> conditionDefinitions)
    {
        getForkConditionDefinitions().clear();
        getForkConditionDefinitions().addAll(conditionDefinitions);
    }
}
