package com.pilog.t8.data;

/**
 * Given a specific node in a hierarchical/tree structure, this enumeration defines the types of
 * commonly used sets that can be derived from the node's position in the structure.
 *
 * @author Bouwer du Preez
 */
public enum T8HierarchicalSetType
{
    LINEAGE,    // From a specific node, all ancestors, all descendants including the node itself.
    ANCESTORS,  // From a specific node, all ancestors, excluding the node itself.
    LINE,       // From a specific node, all ancestors, including the node itself.
    SINGLE,     // From a specific node, only the specific node itself.
    HOUSE,      // From a specific node, all nodes with the same parent, including the node itself and its parent.
    SIBLINGS,   // From a specific node, all nodes with the same parent, excluding the node itself.
    BROOD,      // From a specific node, all nodes with the same parent, including the node itself.
    FAMILY,     // From a specific node, all descendants, including the node itself.
    DESCENDANTS // From a specific node, all descendants, excluding the node itself.
}
