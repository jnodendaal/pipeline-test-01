package com.pilog.t8.definition.data.entity;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ENTITY";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_ENTITY";
    public static final String GROUP_NAME = "Data Entities";
    public static final String GROUP_DESCRIPTION = "Definitions describing the data entities that can be fetched from and written to data sources.  Each entity contains a set of fields where the data values of the entity is stored.";
    public static final String STORAGE_PATH = "/data_entities";
    public static final String IDENTIFIER_PREFIX = "E_";
    public static final String DISPLAY_NAME = "Data Entity";
    public static final String DESCRIPTION = "An object containing defined field values.";
    public enum Datum
    {
        DATA_SOURCE_IDENTIFIER,
        FIELD_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    private Map<String, String> fieldToSourceMapping;
    private Map<String, String> sourceToFieldMapping;

    public T8DataEntityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_SOURCE_IDENTIFIER.toString(), "Data Source", "The data source that is used to persist the entity type."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Field Definitions", "The definitions of the fields of this data entity."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_SOURCE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataSourceDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataEntityFieldDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        fieldToSourceMapping = createFieldToSourceIdentifierMapping(false);
        sourceToFieldMapping = T8IdentifierUtilities.createReverseIdentifierMap(fieldToSourceMapping);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.data.entity.T8DataEntityDefinitionActionHandler", new Class<?>[]{T8Context.class, T8DataEntityDefinition.class}, context, this);
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8DataEntityTestHarness", new Class<?>[]{});
    }

    public String getDataSourceIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_SOURCE_IDENTIFIER.toString());
    }

    public void setDataSourceIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_SOURCE_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getFieldToSourceIdentifierMapping()
    {
        return fieldToSourceMapping;
    }

    public Map<String, String> getSourceToFieldIdentifierMapping()
    {
        return sourceToFieldMapping;
    }

    public String mapFieldToSourceIdentifier(String fieldIdentifier)
    {
        return fieldToSourceMapping.get(fieldIdentifier);
    }

    public String mapSourceToFieldIdentifier(String fieldIdentifier)
    {
        return sourceToFieldMapping.get(fieldIdentifier);
    }

    public T8DataEntity getNewDataEntityInstance()
    {
        return new T8DataEntity(this);
    }

    public T8DataEntityFieldDefinition getFieldDefinition(String fieldIdentifier)
    {
        for (T8DataEntityFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if (fieldDefinition.getIdentifier().equals(fieldIdentifier)) return fieldDefinition;
            else if (fieldDefinition.getPublicIdentifier().equals(fieldIdentifier)) return fieldDefinition;
        }

        return null;
    }

    public List<T8DataEntityFieldDefinition> getFieldDefinitions()
    {
        return (List<T8DataEntityFieldDefinition>)getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8DataEntityFieldDefinition> fieldDefinitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), fieldDefinitions);
    }

    public void addFieldDefinition(T8DataEntityFieldDefinition fieldDefinition)
    {
        String fieldIdentifier;

        fieldIdentifier = fieldDefinition.getIdentifier();
        if (!containsField(fieldIdentifier))
        {
            addSubDefinition(Datum.FIELD_DEFINITIONS.toString(), fieldDefinition);
        }
        else throw new RuntimeException("Attempt to add duplicate field: " + fieldIdentifier);
    }

    public T8DataEntityFieldDefinition removeFieldDefinition(String fieldIdentifier)
    {
        T8DataEntityFieldDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldIdentifier);
        if (fieldDefinition != null)
        {
            getFieldDefinitions().remove(fieldDefinition);
            return fieldDefinition;
        }
        else throw new RuntimeException("Field not found: " + fieldIdentifier);
    }

    public boolean containsField(String fieldIdentifier)
    {
        for (T8DataFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if (fieldDefinition.getPublicIdentifier().equals(fieldIdentifier)) return true;
            else if (fieldDefinition.getIdentifier().equals(fieldIdentifier)) return true;
        }

        return false;
    }

    public boolean canOrderByField(String fieldIdentifier)
    {
        T8DataEntityFieldDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldIdentifier);
        if (fieldDefinition != null)
        {
            T8DataType fieldDataType;

            fieldDataType = fieldDefinition.getDataType();
            if (T8DataType.LONG_STRING.isType(fieldDataType) && T8DataType.LONG_STRING.getDataTypeStringRepresentation(true).equals(fieldDataType.getDataTypeStringRepresentation(true))) return false;
            else return true;
        }
        else return false;
    }

    public boolean canGroupByField(String fieldIdentifier)
    {
        T8DataEntityFieldDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldIdentifier);
        if (fieldDefinition != null)
        {
            T8DataType fieldDataType;

            fieldDataType = fieldDefinition.getDataType();
            if (fieldDataType.equals(T8DataType.LONG_STRING)) return false;
            else return true;
        }
        else return false;
    }

    public T8DataType getFieldDataType(String fieldIdentifier)
    {
        T8DataFieldDefinition fieldDefinition;

        fieldDefinition = getFieldDefinition(fieldIdentifier);
        if (fieldDefinition == null) throw new RuntimeException("Field not found: " + fieldIdentifier);
        else return fieldDefinition.getDataType();
    }

    public List<String> getFieldIdentifiers(boolean local)
    {
        ArrayList<String> fieldIdentifier;

        fieldIdentifier = new ArrayList<String>();
        for (T8DataFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            fieldIdentifier.add(local ? fieldDefinition.getIdentifier() : fieldDefinition.getPublicIdentifier());
        }

        return fieldIdentifier;
    }

    public List<String> getKeyFieldIdentifiers(boolean local)
    {
        ArrayList<String> fieldIdentifiers;

        fieldIdentifiers = new ArrayList<String>();
        for (T8DataFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if (fieldDefinition.isKey()) fieldIdentifiers.add(local ? fieldDefinition.getIdentifier() : fieldDefinition.getPublicIdentifier());
        }

        return fieldIdentifiers;
    }

    public List<String> getRequiredFieldIdentifiers(boolean local)
    {
        ArrayList<String> fieldIdentifiers;

        fieldIdentifiers = new ArrayList<String>();
        for (T8DataFieldDefinition fieldDefinition : getFieldDefinitions())
        {
            if (fieldDefinition.isRequired()) fieldIdentifiers.add(local ? fieldDefinition.getIdentifier() : fieldDefinition.getPublicIdentifier());
        }

        return fieldIdentifiers;
    }

    private Map<String, String> createFieldToSourceIdentifierMapping(boolean localFieldIdentifiers)
    {
        List<T8DataEntityFieldDefinition> fieldDefinitions;

        fieldDefinitions = getFieldDefinitions();
        if (fieldDefinitions != null)
        {
            Map<String, String> identifierMap;

            identifierMap = new HashMap<String, String>();
            for (T8DataFieldDefinition fieldDefinition : fieldDefinitions)
            {
                identifierMap.put(localFieldIdentifiers ? fieldDefinition.getIdentifier() : fieldDefinition.getPublicIdentifier(), fieldDefinition.getSourceIdentifier());
            }

            return identifierMap;
        }
        else return null;
    }
}
