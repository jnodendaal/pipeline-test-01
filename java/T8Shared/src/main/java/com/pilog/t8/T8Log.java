package com.pilog.t8;

import com.pilog.t8.log.T8Logger;
import com.pilog.t8.log.T8LoggerJavaDefault;
import com.pilog.t8.utilities.strings.Strings;

/**
 * @author Bouwer du Preez
 */
public class T8Log
{
    // <editor-fold defaultstate="collapsed" desc="Deperecated from old logger">
    private static String getCallerID()
    {
        StackTraceElement caller;
        String className;

        // This is not very ellegant but it is the same method used by Thread.currentThread().getStackTrace(), just slightly quicker.
        caller = new Exception().getStackTrace()[2];
        className = caller.getClassName();
        className = className.substring(className.lastIndexOf(".") + 1);
        return className;
    }

    @Deprecated
    public static final void log(String message)
    {
        System.out.println(getCallerID() + ": " + message);
    }

    @Deprecated
    public static final void log(String message, Throwable throwable)
    {
        System.out.println(getCallerID() + ": " + message);
        throwable.printStackTrace();
    }
    //</editor-fold>

    private static String prefixID;

    private T8Log(){}

    public static boolean isPrefixIdentifierSet()
    {
        return !Strings.isNullOrEmpty(prefixID);
    }

    public static void setPrefixIdentifier(String prefixIdentifier)
    {
        if (!Strings.isNullOrEmpty(prefixID)) throw new IllegalStateException("Cannot modify the logger prefix identifier once set.");
        prefixID = prefixIdentifier;
    }

    //Currently this is hard coded, but it should be changeable from outside the system.
    public static T8Logger getLogger(String className)
    {
        return new T8LoggerJavaDefault(prefixID, className, T8Logger.Level.PERFORMANCE);
    }

    public static T8Logger getLogger(Class<?> clazz)
    {
        return getLogger(clazz.getName());
    }
}
