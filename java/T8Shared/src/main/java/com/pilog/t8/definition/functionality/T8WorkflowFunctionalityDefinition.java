package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_WORKFLOW";
    public static final String DISPLAY_NAME = "Workflow Functionality";
    public static final String DESCRIPTION = "A functionality that starts execution of a workflow.";
    public static final String IDENTIFIER_PREFIX = "FNC_WF_";
    public enum Datum
    {
        FLOW_ID,
        INPUT_PARAMETER_MAPPING,
        INPUT_PARAMETER_EXPRESSION_MAP,
        OUTPUT_PARAMETER_MAPPING,
        COMPONENT_ID,
        COMPONENT_PARAMETER_MAPPING,
        COMPONENT_PARAMETER_EXPRESSION_MAP
    };
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_CREATE_VIEW_COMPONENT = "$P_CREATE_VIEW_COMPONENT";
    public static final String PARAMETER_QUEUE_FLOW = "$P_QUEUE_FLOW";
    public static final String PARAMETER_FLOW_IID = "$P_FLOW_IID";

    public T8WorkflowFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FLOW_ID.toString(), "Workflow", "The workflow to execute."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Functionality Input to Flow Input", "A mapping of Functionality Parameters to the corresponding Flow Input Parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString(), "Input Parameter Expression Map", "The parameters that will be sent to the workflow as inputs."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Flow Output to Functionality Ouput", "A mapping of Flow Parameters to the corresponding Functionality Output Parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.COMPONENT_ID.toString(), "Component", "The id of the component that displays client workflow status."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.COMPONENT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Functionality to Component", "A mapping of Functionality Parameters to the corresponding Component Input Parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString(), "Component Parameter Expression Map", "The parameters that will be used as inputs to the specified component (if any)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.COMPONENT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8NgComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.FLOW_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String flowId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            flowId = getFlowId();
            if (flowId != null)
            {
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)definitionContext.getRawDefinition(getRootProjectId(), flowId);
                if (flowDefinition != null)
                {
                    List<T8DataParameterDefinition> functionalityParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    functionalityParameterDefinitions = getInputParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((functionalityParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition functionalityParameterDefinition : functionalityParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                            {
                                identifierList.add(flowParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(functionalityParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String flowId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            flowId = getFlowId();
            if (flowId != null)
            {
                T8WorkFlowDefinition flowDefinition;

                flowDefinition = (T8WorkFlowDefinition)definitionContext.getRawDefinition(getRootProjectId(), flowId);
                if (flowDefinition != null)
                {
                    List<T8DataParameterDefinition> functionalityParameterDefinitions;
                    List<T8DataParameterDefinition> flowParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    functionalityParameterDefinitions = getOutputParameterDefinitions();
                    flowParameterDefinitions = flowDefinition.getFlowParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((functionalityParameterDefinitions != null) && (flowParameterDefinitions != null))
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<String>();
                        for (T8DataParameterDefinition functionalityParameterDefinition : functionalityParameterDefinitions)
                        {
                            identifierList.add(functionalityParameterDefinition.getIdentifier());
                        }

                        for (T8DataParameterDefinition flowParameterDefinition : flowParameterDefinitions)
                        {
                            identifierMap.put(flowParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId))
        {
            if (Strings.isNullOrEmpty(getFlowId()))
            {
                return new ArrayList<T8DefinitionDatumOption>(0);
            }
            else
            {
                ArrayList<T8DefinitionDatumOption> optionList;
                T8WorkFlowDefinition workflowDefinition;
                Map<String, List<String>> ids;

                ids = new HashMap<String, List<String>>();
                optionList = new ArrayList<T8DefinitionDatumOption>(1);
                workflowDefinition = (T8WorkFlowDefinition)definitionContext.getRawDefinition(getRootProjectId(), getFlowId());
                for (T8DataParameterDefinition flowParameterDefinition : workflowDefinition.getFlowParameterDefinitions())
                {
                    ids.put(flowParameterDefinition.getPublicIdentifier(), null);
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", ids));
                return optionList;
            }
        }
        else if (Datum.COMPONENT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String componentId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            componentId = getComponentId();
            if (componentId != null)
            {
                T8NgComponentDefinition componentDefinition;

                componentDefinition = (T8NgComponentDefinition)definitionContext.getRawDefinition(getRootProjectId(), componentId);
                if (componentDefinition != null)
                {
                    List<T8DataParameterDefinition> functionalityParameterDefinitions;
                    List<T8DataParameterDefinition> componentParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    functionalityParameterDefinitions = getInputParameterDefinitions();
                    componentParameterDefinitions = componentDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((functionalityParameterDefinitions != null) && (componentParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition functionalityParameterDefinition : functionalityParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition componentParameterDefinition : componentParameterDefinitions)
                            {
                                identifierList.add(componentParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(functionalityParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId))
        {
            if (Strings.isNullOrEmpty(getFlowId()))
            {
                return new ArrayList<T8DefinitionDatumOption>(0);
            }
            else
            {
                ArrayList<T8DefinitionDatumOption> optionList;
                T8NgComponentDefinition componentDefinition;
                Map<String, List<String>> ids;

                ids = new HashMap<String, List<String>>();
                optionList = new ArrayList<T8DefinitionDatumOption>(1);
                componentDefinition = (T8NgComponentDefinition)definitionContext.getRawDefinition(getRootProjectId(), getComponentId());
                for (T8DataParameterDefinition componentParameterDefinition : componentDefinition.getInputParameterDefinitions())
                {
                    ids.put(componentParameterDefinition.getPublicIdentifier(), null);
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", ids));
                return optionList;
            }
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if ((Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else if ((Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8WorkflowFunctionalityInstance").getConstructor(T8Context.class, T8WorkflowFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8WorkflowFunctionalityInstance").getConstructor(T8Context.class, T8WorkflowFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataParameterDefinition> parameterDefinitions;

        parameterDefinitions = super.getDefinitionDatum(T8FunctionalityDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString());
        parameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_FLOW_IID, "Flow Instance Id", "The instance id of the flow referenced by this functionality.", T8DataType.GUID));
        parameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_QUEUE_FLOW, "Queue Flow", "If set to true, the flow access by this functionality will not be started synchronously but will rather be queued.", T8DataType.BOOLEAN, true));
        return parameterDefinitions;
    }

    public String getFlowId()
    {
        return getDefinitionDatum(Datum.FLOW_ID);
    }

    public void setFlowId(String id)
    {
        setDefinitionDatum(Datum.FLOW_ID, id);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING);
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING, mapping);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING);
    }

    public void setOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING, mapping);
    }

    public Map<String, String> getInputParameterExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setInputParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_EXPRESSION_MAP, expressionMap);
    }

    public String getComponentId()
    {
        return getDefinitionDatum(Datum.COMPONENT_ID.toString());
    }

    public void setComponentId(String componentId)
    {
        setDefinitionDatum(Datum.COMPONENT_ID.toString(), componentId);
    }

    public Map<String, String> getComponentParameterMapping()
    {
        return getDefinitionDatum(Datum.COMPONENT_PARAMETER_MAPPING);
    }

    public void setComponentParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.COMPONENT_PARAMETER_MAPPING, mapping);
    }

    public Map<String, String> getComponentParameterExpressionMap()
    {
        return getDefinitionDatum(Datum.COMPONENT_PARAMETER_EXPRESSION_MAP);
    }

    public void setComponentParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.COMPONENT_PARAMETER_EXPRESSION_MAP, expressionMap);
    }
}
