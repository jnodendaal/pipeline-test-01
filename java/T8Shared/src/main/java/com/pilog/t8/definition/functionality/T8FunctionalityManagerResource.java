package com.pilog.t8.definition.functionality;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.datatype.T8DtFunctionalityAccessHandle;
import com.pilog.t8.datatype.T8DtFunctionalityGroupHandle;
import com.pilog.t8.datatype.T8DtFunctionalityHandle;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityManagerResource implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_ACCESS_FUNCTIONALITY = "@OS_ACCESS_FUNCTIONALITY";
    public static final String OPERATION_RELEASE_FUNCTIONALITY = "@OS_RELEASE_FUNCTIONALITY";
    public static final String OPERATION_GET_FUNCTIONALITY_HANDLE = "@OS_GET_FUNCTIONALITY_HANDLE";
    public static final String OPERATION_GET_FUNCTIONALITY_GROUP_HANDLE = "@OS_GET_FUNCTIONALITY_GROUP_HANDLE";
    public static final String OPERATION_CLEAR_FUNCTIONALITY_CACHE = "@OS_CLEAR_FUNCTIONALITY_CACHE";
    public static final String OPERATION_RELOAD_FUNCTIONALITY_ACCESS_POLICY = "@OS_RELOAD_FUNCTIONALITY_ACCESS_POLICY";
    public static final String OPERATION_GET_DATA_OBJECT = "@OP_GET_DATA_OBJECT";
    public static final String OPERATION_GET_DATA_OBJECT_STATE = "@OP_GET_DATA_OBJECT_STATE";

    public static final String PARAMETER_FUNCTIONALITY_IID = "$P_FUNCTIONALITY_IID";
    public static final String PARAMETER_FUNCTIONALITY_ID = "$P_FUNCTIONALITY_ID";
    public static final String PARAMETER_FUNCTIONALITY_GROUP_ID = "$P_FUNCTIONALITY_GROUP_ID";
    public static final String PARAMETER_DATA_OBJECT = "$P_DATA_OBJECT";
    public static final String PARAMETER_DATA_OBJECT_STATE = "$P_DATA_OBJECT_STATE";
    public static final String PARAMETER_DATA_OBJECT_ID = "$P_DATA_OBJECT_ID";
    public static final String PARAMETER_DATA_OBJECT_IID = "$P_DATA_OBJECT_IID";
    public static final String PARAMETER_TARGET_OBJECT = "$P_TARGET_OBJECT";
    public static final String PARAMETER_FUNCTIONALITY_PARAMETERS = "$P_FUNCTIONALITY_PARAMETERS";
    public static final String PARAMETER_FUNCTIONALITY_HANDLE = "$P_FUNCTIONALITY_HANDLE";
    public static final String PARAMETER_FUNCTIONALITY_GROUP_HANDLE = "$P_FUNCTIONALITY_GROUP_HANDLE";
    public static final String PARAMETER_FUNCTIONALITY_EXECUTION_HANDLE = "$P_FUNCTIONALITY_EXECUTION_HANDLE";

    public static final String F_EVENT_IID = "$EVENT_IID";
    public static final String F_FUNCTIONALITY_IID = "$FUNCTIONALITY_IID";
    public static final String F_FUNCTIONALITY_ID = "$FUNCTIONALITY_ID";
    public static final String F_DATA_OBJECT_IID = "$DATA_OBJECT_IID";
    public static final String F_DATA_OBJECT_ID = "$DATA_OBJECT_ID";
    public static final String F_INITIATOR_IID = "$INITIATOR_IID";
    public static final String F_INITIATOR_ID = "$INITIATOR_ID";
    public static final String F_INITIATOR_ORG_ID = "$INITIATOR_ORG_ID";
    public static final String F_FLOW_IID = "$FLOW_IID";
    public static final String F_OPERATION_IID = "$OPERATION_IID";
    public static final String F_SESSION_ID = "$SESSION_ID";
    public static final String F_PARAMETER_IID = "$PARAMETER_IID";
    public static final String F_PARENT_PARAMETER_IID = "$PARENT_PARAMETER_IID";
    public static final String F_PARAMETER_ID = "$PARAMETER_ID";
    public static final String F_PARAMETER_KEY = "$PARAMETER_KEY";
    public static final String F_TYPE = "$TYPE";
    public static final String F_SEQUENCE = "$SEQUENCE";
    public static final String F_VALUE = "$VALUE";
    public static final String F_TIME_ACCESSED = "$TIME_ACCESSED";
    public static final String F_TIME = "$TIME";
    public static final String F_EVENT = "$EVENT";

    public static final String HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER = "@DS_HISTORY_DATA_OBJECT_STATE";
    public static final String HISTORY_DATA_OBJECT_STATE_DE_IDENTIFIER = "@E_HISTORY_DATA_OBJECT_STATE";
    public static final String STATE_DATA_OBJECT_DS_IDENTIFIER = "@DS_STATE_DATA_OBJECT";
    public static final String STATE_DATA_OBJECT_DE_IDENTIFIER = "@E_STATE_DATA_OBJECT";

    // HISTORY_FUNC.
    public static final String HISTORY_FUNC_DS_IDENTIFIER = "@DS_HISTORY_FUNC";
    public static final String HISTORY_FUNC_DE_IDENTIFIER = "@E_HISTORY_FUNC";
    public static final String HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER = "@DS_HISTORY_FUNC_INPUT_PAR";
    public static final String HISTORY_FUNC_INPUT_PAR_DE_IDENTIFIER = "@E_HISTORY_FUNC_INPUT_PAR";

    // STATE_FUNC.
    public static final String STATE_FUNC_DS_IDENTIFIER = "@DS_STATE_FUNC";
    public static final String STATE_FUNC_DE_IDENTIFIER = "@E_STATE_FUNC";
    public static final String STATE_FUNC_INPUT_PAR_DS_IDENTIFIER = "@DS_STATE_FUNC_INPUT_PAR";
    public static final String STATE_FUNC_INPUT_PAR_DE_IDENTIFIER = "@E_STATE_FUNC_INPUT_PAR";
    public static final String STATE_FUNC_DATA_OBJECT_DS_IDENTIFIER = "@DS_STATE_FUNC_DATA_OBJECT";
    public static final String STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER = "@E_STATE_FUNC_DATA_OBJECT";

    // STATE_FUNC_BLOCK.
    public static final String STATE_FUNC_BLOCK_DS_IDENTIFIER = "@DS_STATE_FUNC_BLOCK";
    public static final String STATE_FUNC_BLOCK_DE_IDENTIFIER = "@E_STATE_FUNC_BLOCK";

    // Table names.
    public static final String TABLE_HISTORY_DATA_OBJECT_STATE = "HIS_DAT_OBJ_STT";
    public static final String TABLE_STATE_DATA_OBJECT = "DAT_OBJ_STT";
    public static final String TABLE_STATE_FUNC = "FNC";
    public static final String TABLE_STATE_FUNC_DATA_OBJECT = "FNC_DAT_OBJ";
    public static final String TABLE_STATE_FUNC_INPUT_PAR = "FNC_INP_PAR";
    public static final String TABLE_STATE_FUNC_BLOCK = "FNC_BLK";
    public static final String TABLE_HISTORY_FUNC = "HIS_FNC";
    public static final String TABLE_HISTORY_FUNC_INPUT_PAR = "HIS_FNC_INP_PAR";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions(serverContext));
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // State History Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_OBJECT_STATE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_OBJECT_IID", "DATA_OBJECT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$STATE_FROM_ID", "STATE_FROM_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$STATE_TO_ID", "STATE_TO_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.DATE_TIME));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FUNCTIONALITY_ID", "FUNCTIONALITY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$AGENT_ID", "AGENT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$AGENT_IID", "AGENT_IID", false, false, T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // State History Parameter Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_DATA_OBJECT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_DATA_OBJECT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_OBJECT_IID", "DATA_OBJECT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_OBJECT_ID", "DATA_OBJECT_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$STATE_ID", "STATE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FUNC Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FUNC_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FUNC);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FUNCTIONALITY_IID, "FUNCTIONALITY_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FUNCTIONALITY_ID, "FUNCTIONALITY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_ID, "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INITIATOR_IID, "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_OPERATION_IID, "OPERATION_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SESSION_ID, "SESSION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_ACCESSED, "TIME_ACCESSED", false, false, T8DataType.DATE_TIME));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FUNC_INPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FUNC_INPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FUNC_INPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FUNCTIONALITY_IID, "FUNCTIONALITY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FUNC_DATA_OBJECT Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FUNC_DATA_OBJECT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FUNC_DATA_OBJECT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FUNCTIONALITY_IID, "FUNCTIONALITY_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_OBJECT_IID, "DATA_OBJECT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_OBJECT_ID, "DATA_OBJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // STATE_FUNC_BLOCK Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(STATE_FUNC_BLOCK_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_STATE_FUNC_BLOCK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCK_IID", "BLOCK_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKING_FUNCTIONALITY_IID", "BLOCKING_FUNCTIONALITY_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_FUNCTIONALITY_ID", "BLOCKED_FUNCTIONALITY_ID", true, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_ROOT_ORG_ID", "BLOCKED_ROOT_ORG_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_ORG_ID", "BLOCKED_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_DATA_OBJECT_IID", "BLOCKED_DATA_OBJECT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_DATA_OBJECT_ID", "BLOCKED_DATA_OBJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_USER_ID", "BLOCKED_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_PROFILE_ID", "BLOCKED_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$BLOCKED_MESSAGE", "BLOCKED_MESSAGE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // HISTORY_FUNC Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_FUNC_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FUNC);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FUNCTIONALITY_ID", "FUNCTIONALITY_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FUNCTIONALITY_IID", "FUNCTIONALITY_IID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_OBJECT_ID", "DATA_OBJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_OBJECT_IID", "DATA_OBJECT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT", "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.DATE_TIME));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_ID", "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_IID", "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_ORG_ID", "INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // HISTORY_FUNC_INPUT_PAR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_FUNC_INPUT_PAR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EVENT_IID, "EVENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FUNCTIONALITY_IID, "FUNCTIONALITY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_IID, "PARAMETER_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_PARAMETER_IID, "PARENT_PARAMETER_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETER_KEY, "PARAMETER_KEY", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // State History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_DATA_OBJECT_STATE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$EVENT_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_OBJECT_IID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$DATA_OBJECT_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$STATE_FROM_ID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$STATE_FROM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$STATE_TO_ID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$STATE_TO_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$TIME", false, false, T8DataType.DATE_TIME));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FUNCTIONALITY_ID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$FUNCTIONALITY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$AGENT_ID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$AGENT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$AGENT_IID", HISTORY_DATA_OBJECT_STATE_DS_IDENTIFIER + "$AGENT_IID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(entityDefinition);

        // State History Parameter Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_DATA_OBJECT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_DATA_OBJECT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_OBJECT_IID", STATE_DATA_OBJECT_DS_IDENTIFIER + "$DATA_OBJECT_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_OBJECT_ID", STATE_DATA_OBJECT_DS_IDENTIFIER + "$DATA_OBJECT_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$STATE_ID", STATE_DATA_OBJECT_DS_IDENTIFIER + "$STATE_ID", false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // STATE_FUNC Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FUNC_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FUNC_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_IID, STATE_FUNC_DS_IDENTIFIER + F_FUNCTIONALITY_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_ID, STATE_FUNC_DS_IDENTIFIER + F_FUNCTIONALITY_ID, true, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, STATE_FUNC_DS_IDENTIFIER + F_INITIATOR_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, STATE_FUNC_DS_IDENTIFIER + F_INITIATOR_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FLOW_IID, STATE_FUNC_DS_IDENTIFIER + F_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_OPERATION_IID, STATE_FUNC_DS_IDENTIFIER + F_OPERATION_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SESSION_ID, STATE_FUNC_DS_IDENTIFIER + F_SESSION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_ACCESSED, STATE_FUNC_DS_IDENTIFIER + F_TIME_ACCESSED, false, false, T8DataType.DATE_TIME));
        definitions.add(entityDefinition);

        // STATE_FUNC_INPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FUNC_INPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_IID, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_FUNCTIONALITY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, STATE_FUNC_INPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // STATE_FUNC_DATA_OBJECT Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FUNC_DATA_OBJECT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_IID, STATE_FUNC_DATA_OBJECT_DS_IDENTIFIER + F_FUNCTIONALITY_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_IID, STATE_FUNC_DATA_OBJECT_DS_IDENTIFIER + F_DATA_OBJECT_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_ID, STATE_FUNC_DATA_OBJECT_DS_IDENTIFIER + F_DATA_OBJECT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(entityDefinition);

        // STATE_FUNC_BLOCK Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(STATE_FUNC_BLOCK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(STATE_FUNC_BLOCK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCK_IID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCK_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKING_FUNCTIONALITY_IID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKING_FUNCTIONALITY_IID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_FUNCTIONALITY_ID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_FUNCTIONALITY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_ROOT_ORG_ID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_ROOT_ORG_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_ORG_ID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_ORG_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_DATA_OBJECT_IID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_DATA_OBJECT_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_DATA_OBJECT_ID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_DATA_OBJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_USER_ID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_PROFILE_ID", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_PROFILE_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$BLOCKED_MESSAGE", STATE_FUNC_BLOCK_DS_IDENTIFIER + "$BLOCKED_MESSAGE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // HISTORY_FUNC Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_FUNC_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_FUNC_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, HISTORY_FUNC_DS_IDENTIFIER + "$EVENT_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_ID, HISTORY_FUNC_DS_IDENTIFIER + "$FUNCTIONALITY_ID", false, true, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_IID, HISTORY_FUNC_DS_IDENTIFIER + "$FUNCTIONALITY_IID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_ID, HISTORY_FUNC_DS_IDENTIFIER + "$DATA_OBJECT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_OBJECT_IID, HISTORY_FUNC_DS_IDENTIFIER + "$DATA_OBJECT_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT, HISTORY_FUNC_DS_IDENTIFIER + "$EVENT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME, HISTORY_FUNC_DS_IDENTIFIER + "$TIME", false, false, T8DataType.DATE_TIME));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ID, HISTORY_FUNC_DS_IDENTIFIER + "$INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_IID, HISTORY_FUNC_DS_IDENTIFIER + "$INITIATOR_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INITIATOR_ORG_ID, HISTORY_FUNC_DS_IDENTIFIER + "$INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // HISTORY_FUNC_INPUT_PAR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_FUNC_INPUT_PAR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EVENT_IID, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_EVENT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FUNCTIONALITY_IID, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_FUNCTIONALITY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_IID, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_PARAMETER_IID, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_PARENT_PARAMETER_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETER_KEY, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_PARAMETER_KEY, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, HISTORY_FUNC_INPUT_PAR_DS_IDENTIFIER + F_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions(T8ServerContext serverContext)
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_FUNCTIONALITY_GROUP_HANDLE);
        definition.setMetaDisplayName("Get Functionality Group Handle");
        definition.setMetaDescription("Returns the request functionality group handle as applicable to the current user profile and the supplied data object.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$GetFunctionalityGroupHandleOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_GROUP_ID, "Functionality Group Id", "The id of the functionality group to retrieve.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_ID, "Data Object Id", "The id of the data object for which the group handle will be configured.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IID, "Data Object Instance Id", "The instance id of the data object for which the group handle will be configured.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_GROUP_HANDLE, "Functionality Group Handle", "The handle of the requested functionality group configured for the current user profile and the specified data object.", new T8DtFunctionalityGroupHandle()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_FUNCTIONALITY_HANDLE);
        definition.setMetaDisplayName("Get Functionality Handle");
        definition.setMetaDescription("Fetches a handle to the specified functionality.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$GetFunctionalityHandleOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_ID, "Functionality Id", "The id of the functionality to execute.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_ID, "Data Object Id", "The id of the data object for which the handle will be configured.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IID, "Data Object Instance Id", "The instance id of the data object for which the handle will be configured.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_HANDLE, "Functionality Handle", "The handle of the requested functionality.", new T8DtFunctionalityHandle()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_ACCESS_FUNCTIONALITY);
        definition.setMetaDisplayName("Access Functionality");
        definition.setMetaDescription("Accesses the specified functionality on the supplied target object and returns the functionality access handle.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$AccessFunctionalityOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_ID, "Functionality Id", "The id of the functionality to execute.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_PARAMETERS, "Parameters", "The parameters to supply when accessing the functionality.", T8DataType.DEFINITION_IDENTIFIER_KEY_MAP));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_EXECUTION_HANDLE, "Functionality Execution Handle", "The handle of the executing functionality.", new T8DtFunctionalityAccessHandle(serverContext.getDefinitionManager())));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RELEASE_FUNCTIONALITY);
        definition.setMetaDisplayName("Release Functionality");
        definition.setMetaDescription("Releases access to the specified functionality.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$ReleaseFunctionalityOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FUNCTIONALITY_IID, "Functionality Instance Identifier", "The instance identifier of the functionality to release.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_CLEAR_FUNCTIONALITY_CACHE);
        definition.setMetaDisplayName("Clear Functionality Cache");
        definition.setMetaDescription("Clears all of the caches on the functionality manager.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$ClearFunctionalityCacheOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RELOAD_FUNCTIONALITY_ACCESS_POLICY);
        definition.setMetaDisplayName("Reload Functionality Access Policy");
        definition.setMetaDescription("Reloads the functionality access policy used by the functionality manager.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$ReloadFunctionalityAccessPolicyOperation");
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_DATA_OBJECT);
        definition.setMetaDisplayName("Get Data Object");
        definition.setMetaDescription("Returns the complete specified data object.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$GetDataObjectOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_ID, "Data Object Identifier", "The identifier of the data object to return.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IID, "Data Object Instance Identifier", "The instance identifier of the data object to return.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT, "Data Object", "The requested data object.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_GET_DATA_OBJECT_STATE);
        definition.setMetaDisplayName("Get Data Object State");
        definition.setMetaDescription("Returns the state of specified data object.");
        definition.setClassName("com.pilog.t8.functionality.T8FunctionalityOperations$GetDataObjectStateOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IID, "Data Object Instance Identifier", "The instance identifier of the data object for which to return the state.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_STATE, "Data Object State", "The requested data object's state.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        return definitions;
    }
}