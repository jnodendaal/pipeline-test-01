package com.pilog.t8.security.authentication;

import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.http.T8HttpServletRequest;

/**
 * @author Bouwer du Preez
 */
public interface T8AuthenticationHandler
{
    /**
     * Return the id of this handler.
     * @return The id of this handler corresponding to the definition from which it was constructed.
     */
    public String getId();

    /**
     * Registers a user for authentication eligibility by this handler.
     * @param user The definition of the user to be registered.
     */
    public void registerUser(T8UserDefinition user);

    /**
     * Deregisters a user from authentication eligibility by this handler.
     * @param userId The id of the user to be deregistered.
     */
    public void deregisterUser(String userId);

    /**
     * Updates the information of a user registered to this handler.
     * @param user The definition of the user to be updated.
     */
    public void updateUser(T8UserDefinition user);

    /**
     * Checks the supplied authorization header as received as part of an http request and determined whether
     * this handler is applicable to the specified authorization scheme or not.
     * @param authorizationHeader The http authorization header to check.
     * @return Boolean true if this handler is applicable to the supplied authorization header.
     */
    public boolean isApplicable(String authorizationHeader);

    /**
     * Authenticates the supplied request object and returns a response containing the details of the result.
     * @param request The request to authenticate.
     * @return The session context associated with the supplied request.
     */
    public T8AuthenticationResponse authenticate(T8HttpServletRequest request);
}
