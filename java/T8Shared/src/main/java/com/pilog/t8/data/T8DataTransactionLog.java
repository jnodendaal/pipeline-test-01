package com.pilog.t8.data;

import com.pilog.t8.data.T8DataTransactionLogEntry.TransactionOperationType;
import com.pilog.t8.datastructures.stacks.FixedSizeStack;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataTransactionLog implements Serializable
{
    private final String identifier;
    private final List<T8DataTransactionLogEntry> openEntries;
    private final FixedSizeStack<T8DataTransactionLogEntry> entries;

    public T8DataTransactionLog(String identifier, int size)
    {
        this.identifier = identifier;
        this.openEntries = Collections.synchronizedList(new ArrayList<T8DataTransactionLogEntry>());
        this.entries = new FixedSizeStack<T8DataTransactionLogEntry>(size);
    }

    public void logOperationStart(String operationID, String entityIdentifier, String filter, TransactionOperationType type)
    {
        T8DataTransactionLogEntry newEntry;
        Thread currentThread;

        currentThread = Thread.currentThread();
        newEntry = new T8DataTransactionLogEntry(operationID, currentThread.getId(), currentThread.getName(), entityIdentifier, filter, type, System.currentTimeMillis(), -1);
        entries.push(newEntry);
        openEntries.add(newEntry);
    }

    public void logOperationEnd(String operationID)
    {
        T8DataTransactionLogEntry entryToEnd;

        // Find the entry to end.  Synchronize on openEntries because we are iterating over the colleciton.
        entryToEnd = null;
        synchronized (openEntries)
        {
            for (T8DataTransactionLogEntry entry : openEntries)
            {
                if (entry.getOperationID().equals(operationID))
                {
                    entryToEnd = entry;
                    break;
                }
            }
        }

        // End the entry.
        if (entryToEnd != null)
        {
            entryToEnd.setEndTime(System.currentTimeMillis());
            openEntries.remove(entryToEnd);
        }
        else throw new RuntimeException("Cannot find data operation log entry to end (" + identifier + "): " + operationID);
    }

    public int getOpenEntryCount()
    {
        return openEntries.size();
    }

    public List<T8DataTransactionLogEntry> getOpenEntries()
    {
        return new ArrayList<T8DataTransactionLogEntry>(openEntries);
    }

    public List<T8DataTransactionLogEntry> getEntries()
    {
        List<T8DataTransactionLogEntry> entryList;

        entryList = new ArrayList<T8DataTransactionLogEntry>();
        for (int index = 0; index < entries.size(); index++)
        {
            entryList.add(entries.get(index));
        }

        return entryList;
    }
}
