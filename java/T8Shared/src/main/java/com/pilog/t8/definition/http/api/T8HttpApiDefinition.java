package com.pilog.t8.definition.http.api;

import com.pilog.t8.T8Server;
import com.pilog.t8.http.api.T8HttpApi;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8HttpApiDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_HTTP_API";
    public static final String GROUP_NAME = "HTTP API";
    public static final String GROUP_DESCRIPTION = "APIs accessible via HTTP request to the T8 server.";
    public static final String STORAGE_PATH = "/http_apis";
    public static final String IDENTIFIER_PREFIX = "HTTP_API_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8HttpApiDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8HttpApi getHttpApi(T8Server server, T8Context context) throws Exception;
}
