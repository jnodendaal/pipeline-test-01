package com.pilog.t8.definition.flow.task.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8IdentifierValueDefinition;
import com.pilog.t8.definition.script.T8FlowTaskOutputParameterScriptDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleEventDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowUserTaskModuleEventDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_TASK_USER_MODULE_EVENT";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_WORK_TASK_USER_MODULE_EVENT";
    public static final String IDENTIFIER_PREFIX = "FT_USER_MODULE_EVENT_";
    public static final String DISPLAY_NAME = "User Task Module Event";
    public static final String DESCRIPTION = "An event that signals the finalization of a user task performed by a specific module.";
    public enum Datum {ACTION_PARAMETER_IDENTIFIER,
                       ACTION_PARAMETER_VALUE_IDENTIFIER,
                       MODULE_EVENT_IDENTIFIER,
                       MODULE_OUTPUT_PARAMETER_MAPPING,
                       TASK_OUTPUT_PARAMETER_SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowUserTaskModuleEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.ACTION_PARAMETER_IDENTIFIER.toString(), "Action Parameter", "The identifier of the action parameter (task output) to which the action value associated with this event will be mapped."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.ACTION_PARAMETER_VALUE_IDENTIFIER.toString(), "Action Value", "The fixed identifier of the action parameter value that will be associated with this event (if not dynamic)."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.MODULE_EVENT_IDENTIFIER.toString(), "Finalization Event", "The identifier of the event that indicate the finalization of this module task."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.MODULE_OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Module Event to Task Output", "A mapping of Module Event Parameters to the corresponding Task Output Parameters."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.TASK_OUTPUT_PARAMETER_SCRIPT_DEFINITION.toString(), "Additional Task Output Parameter Script", "A script that is executed to compile additional task output parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ACTION_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8DataParameterDefinition> taskParameterDefinitions;
            T8WorkFlowUserTaskDefinition taskDefinition;
            ArrayList<T8DefinitionDatumOption> optionList;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            taskDefinition = (T8WorkFlowUserTaskDefinition)getAncestorDefinition(T8WorkFlowUserTaskDefinition.TYPE_IDENTIFIER);
            taskParameterDefinitions = taskDefinition.getOutputParameterDefinitions();
            for (T8DataParameterDefinition taskParameterDefinition : taskParameterDefinitions)
            {
                optionList.add(new T8DefinitionDatumOption(taskParameterDefinition.getIdentifier(), taskParameterDefinition.getIdentifier()));
            }

            return optionList;
        }
        else if (Datum.ACTION_PARAMETER_VALUE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IdentifierValueDefinition.GROUP_IDENTIFIER), true, "No Fixed Value");
        else if (Datum.MODULE_OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            T8WorkFlowUserTaskModuleDefinition taskModuleDefinition;
            ArrayList<T8DefinitionDatumOption> optionList;
            String moduleId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            taskModuleDefinition = (T8WorkFlowUserTaskModuleDefinition)getParentDefinition();
            if (taskModuleDefinition != null)
            {
                moduleId = taskModuleDefinition.getModuleIdentifier();
                if (moduleId != null)
                {
                    T8ModuleEventDefinition moduleEventDefinition;
                    T8ModuleDefinition moduleDefinition;

                    moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                    moduleEventDefinition = (T8ModuleEventDefinition)moduleDefinition.getSubDefinition(getModuleEventIdentifier());
                    if (moduleEventDefinition != null)
                    {
                        List<T8DataParameterDefinition> eventParameterDefinitions;
                        List<T8DataParameterDefinition> taskParameterDefinitions;
                        HashMap<String, List<String>> identifierMap;
                        T8WorkFlowUserTaskDefinition taskDefinition;

                        eventParameterDefinitions = moduleEventDefinition.getParameterDefinitions();
                        taskDefinition = (T8WorkFlowUserTaskDefinition)getAncestorDefinition(T8WorkFlowUserTaskDefinition.TYPE_IDENTIFIER);
                        taskParameterDefinitions = taskDefinition.getOutputParameterDefinitions();

                        identifierMap = new HashMap<String, List<String>>();
                        if ((eventParameterDefinitions != null) && (taskParameterDefinitions != null))
                        {
                            for (T8DataParameterDefinition eventParameterDefinition : eventParameterDefinitions)
                            {
                                ArrayList<String> identifierList;

                                identifierList = new ArrayList<String>();
                                for (T8DataParameterDefinition taskParameterDefinition : taskParameterDefinitions)
                                {
                                    identifierList.add(taskParameterDefinition.getIdentifier());
                                }

                                identifierMap.put(eventParameterDefinition.getPublicIdentifier(), identifierList);
                            }
                        }

                        optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                        return optionList;
                    }
                    else return optionList;
                }
                else return optionList;
            }
            else return optionList;
        }
        else if (Datum.MODULE_EVENT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8WorkFlowUserTaskModuleDefinition taskModuleDefinition;

            taskModuleDefinition = (T8WorkFlowUserTaskModuleDefinition)getParentDefinition();
            if (taskModuleDefinition != null)
            {
                String moduleId;

                moduleId = taskModuleDefinition.getModuleIdentifier();
                if (moduleId != null)
                {
                    T8ModuleDefinition moduleDefinition;

                    moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                    return createPublicIdentifierOptionsFromDefinitions((List)moduleDefinition.getModuleEventDefinitions());
                }
                else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.TASK_OUTPUT_PARAMETER_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FlowTaskOutputParameterScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getModuleEventIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_EVENT_IDENTIFIER.toString());
    }

    public void setModuleEventIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MODULE_EVENT_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getModuleOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.MODULE_OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setModuleOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.MODULE_OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public String getActionParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ACTION_PARAMETER_IDENTIFIER.toString());
    }

    public void setActionParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ACTION_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getActionParameterValueIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ACTION_PARAMETER_VALUE_IDENTIFIER.toString());
    }

    public void setActionParameterValueIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ACTION_PARAMETER_VALUE_IDENTIFIER.toString(), identifier);
    }

    public T8FlowTaskOutputParameterScriptDefinition getTaskOutputParameterScriptDefinition()
    {
        return (T8FlowTaskOutputParameterScriptDefinition)getDefinitionDatum(Datum.TASK_OUTPUT_PARAMETER_SCRIPT_DEFINITION.toString());
    }

    public void setTaskOutputParameterScriptDefinition(T8FlowTaskOutputParameterScriptDefinition definition)
    {
        setDefinitionDatum(Datum.TASK_OUTPUT_PARAMETER_SCRIPT_DEFINITION.toString(), definition);
    }
}
