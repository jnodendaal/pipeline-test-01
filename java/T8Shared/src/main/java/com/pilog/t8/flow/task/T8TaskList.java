package com.pilog.t8.flow.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TaskList implements Serializable
{
    final private List<T8TaskDetails> taskDetailsList;

    public T8TaskList()
    {
        taskDetailsList = Collections.synchronizedList(new ArrayList<T8TaskDetails>());
    }

    public T8TaskList(T8TaskList taskList)
    {
        this();
        for (T8TaskDetails task : taskList.getTaskDetails())
        {
            addTaskDetails(task);
        }
    }

    T8TaskList(ArrayList<T8TaskDetails> taskDetails)
    {
        this();
        for (T8TaskDetails task : taskDetails)
        {
            addTaskDetails(task);
        }
    }

    public List<T8TaskDetails> getTaskDetails()
    {
        return taskDetailsList;
    }

    public final void addTaskDetails(T8TaskDetails taskDetails)
    {
        taskDetailsList.add(taskDetails);
    }

    public void removeTaskList(T8TaskList taskList)
    {
        for (T8TaskDetails taskDetails : taskList.getTaskDetails())
        {
            removeTaskDetails(taskDetails);
        }
    }

    public void removeTaskDetails(T8TaskDetails taskDetails)
    {
        taskDetailsList.remove(taskDetails);
    }

    public void removeFlowTaskDetails(String flowInstanceIdentifier)
    {
        Iterator<T8TaskDetails> taskIterator;

        synchronized(taskDetailsList)
        {
            taskIterator = taskDetailsList.iterator();
            while (taskIterator.hasNext())
            {
                T8TaskDetails taskDetails;

                taskDetails = taskIterator.next();
                if (flowInstanceIdentifier.equals(taskDetails.getFlowIid()))
                {
                    taskIterator.remove();
                }
            }
        }
    }

    public ArrayList<String> getTaskInstanceIdentifiers()
    {
        ArrayList<String> identifiers;

        identifiers = new ArrayList<>(taskDetailsList.size());
        synchronized(taskDetailsList)
        {
            for (T8TaskDetails taskDetails : taskDetailsList)
            {
                identifiers.add(taskDetails.getTaskIid());
            }
        }

        return identifiers;
    }

    public int getTaskIndex(String taskInstanceIdentifier)
    {
        for (int index = 0; index < taskDetailsList.size(); index++)
        {
            T8TaskDetails taskDetails;

            taskDetails = taskDetailsList.get(index);
            if (taskInstanceIdentifier.equals(taskDetails.getTaskIid()))
            {
                return index;
            }
        }

        return -1;
    }

    public T8TaskDetails getTaskDetails(String taskInstanceIdentifier)
    {
        synchronized(taskDetailsList)
        {
            for (T8TaskDetails taskDetails : taskDetailsList)
            {
                if (taskInstanceIdentifier.equals(taskDetails.getTaskIid()))
                {
                    return taskDetails;
                }
            }
        }

        return null;
    }

    public T8TaskDetails getTaskDetails(int index)
    {
        return taskDetailsList.get(index);
    }

    public int getSize()
    {
        return taskDetailsList.size();
    }
}
