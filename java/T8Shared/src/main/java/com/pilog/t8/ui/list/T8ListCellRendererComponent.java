package com.pilog.t8.ui.list;

import com.pilog.t8.ui.T8Component;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8ListCellRendererComponent extends T8Component
{
    public enum RenderParameter {SELECTED, FOCUSED, EDITABLE, INDEX};
    
    public void setRendererData(T8ListCellRenderableComponent sourceComponent, Map<String, Object> dataRow, String rendererKey, Map<RenderParameter, Object> renderParameters);
}
