package com.pilog.t8.definition.flow;

/**
 * @author Bouwer du Preez
 */
public interface T8WorkFlowTaskDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_WORK_FLOW_TASK";
    public static final String STORAGE_PATH = "/work_flow_tasks";
    public static final String IDENTIFIER_PREFIX = "TASK_";
    // -------- Definition Meta-Data -------- //
}
