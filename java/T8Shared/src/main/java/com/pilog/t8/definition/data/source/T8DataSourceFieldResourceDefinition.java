package com.pilog.t8.definition.data.source;

import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DataSourceFieldResourceDefinition extends T8DataSourceFieldDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RESOURCE_DATA_SOURCE_FIELD";
    public static final String DISPLAY_NAME = "Resource Data Source Field";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8DataSourceFieldResourceDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataSourceFieldResourceDefinition(String id, String sourceId, boolean key, boolean required, T8DataType dataType)
    {
        super(id, sourceId, key, required, dataType);
    }
}
