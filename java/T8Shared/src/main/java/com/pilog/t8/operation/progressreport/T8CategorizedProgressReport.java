package com.pilog.t8.operation.progressreport;

/**
 * @author Hennie Brink
 */
public interface T8CategorizedProgressReport extends T8ProgressReport
{

    String getCategoryMessage();

    Double getCategoryProgress();

    String getCategoryTitle();

    void setCategoryMessage(String categoryMessage);

    void setCategoryProgress(Double categoryProgress);

    void setCategoryTitle(String categoryTitle);

}
