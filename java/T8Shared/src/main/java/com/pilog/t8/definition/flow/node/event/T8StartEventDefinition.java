package com.pilog.t8.definition.flow.node.event;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8StartEventDefinition extends T8FlowEventDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_EVENT_START";
    public static final String DISPLAY_NAME = "Flow Start Event";
    public static final String DESCRIPTION = "An event that signals the start of the flow process.";
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8StartEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String instanceIdentifier) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.event.T8StartEventNode").getConstructor(T8Context.class, T8Flow.class, T8StartEventDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, instanceIdentifier);
    }
}
