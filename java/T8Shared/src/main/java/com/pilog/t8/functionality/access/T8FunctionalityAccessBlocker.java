package com.pilog.t8.functionality.access;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityAccessBlocker
{
    public List<T8FunctionalityBlock> getFunctionalityBlocks(T8Context context, String functionalityId, List<T8DataObject> dataObjects);
}
