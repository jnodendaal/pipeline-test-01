package com.pilog.t8.flow.task;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8TaskTypeSummary implements Serializable
{
    private String taskIdentifier;
    private String displayName;
    private String description;
    private int totalCount;
    private int claimedCount;
    
    public T8TaskTypeSummary(String taskIdentifier)
    {
        this.taskIdentifier = taskIdentifier;
    }

    public String getTaskIdentifier()
    {
        return taskIdentifier;
    }

    public void setTaskIdentifier(String taskIdentifier)
    {
        this.taskIdentifier = taskIdentifier;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public int getClaimedCount()
    {
        return claimedCount;
    }

    public void setClaimedCount(int claimedCount)
    {
        this.claimedCount = claimedCount;
    }
}
