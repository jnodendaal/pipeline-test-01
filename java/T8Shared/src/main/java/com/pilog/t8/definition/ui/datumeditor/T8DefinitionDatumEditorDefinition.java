package com.pilog.t8.definition.ui.datumeditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionDatumEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_DATUM_EDITOR";
    public static final String STORAGE_PATH = "/datum_editors";
    // -------- Definition Meta-Data -------- //

    public String getDatumIdentifier();
    public T8DefinitionDatumEditor getNewDatumEditorInstance(T8DefinitionContext definitionContext, T8Definition editorDefinition, T8DefinitionDatumType datumType) throws Exception;
}
