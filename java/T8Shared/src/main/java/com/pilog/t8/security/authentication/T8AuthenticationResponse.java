package com.pilog.t8.security.authentication;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8SessionContext;

/**
 * @author Bouwer du Preez
 */
public interface T8AuthenticationResponse
{
    public enum ResponseType
    {
        COMPLETED, // Authentication completed.
        CONTINUED, // Authentication in progress and to be continued after subsequent communication with client.
        FAILED // Authentication failure due to incomplete or invalid credentials or operation parameters.
    };

    public ResponseType getResponseType();
    public T8SessionContext getSessionContext();
    public String getUserId();
    public JsonObject serialize();
}
