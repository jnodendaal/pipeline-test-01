package com.pilog.t8;

import java.util.Map;

/**
 * This interface must be implemented by all classes representing operations
 * that can be executed from a T8ClientContext.
 *
 * @author Bouwer du Preez
 */
public interface T8ClientOperation
{
    public Map<String, Object> execute(T8SessionContext sessionContext, Map<String, Object> operationParameters) throws Exception;
    public void stop(T8SessionContext sessionContext); // Stops execution without cancelling already completed progress.
    public void cancel(T8SessionContext sessionContext); // Cancels all completed progress, rolling back changes where possible.
    public void pause(T8SessionContext sessionContext); // Pauses execution of the operation.
    public void resume(T8SessionContext sessionContext); // Resumes a previously paused operation.
    public double getProgress(T8SessionContext sessionContext); // Returns the progress of the operation execution.
    public Object getProgressReport(T8SessionContext sessionContext); // Returns a status report detailing the progress and current status of the operation.
}
