package com.pilog.t8.data.filter;

import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.expression.T8SearchExpressionParseException;
import com.pilog.t8.data.filter.expression.T8FilterExpressionToken;
import com.pilog.t8.data.filter.expression.T8FilterExpressionToken.TokenType;
import com.pilog.t8.data.filter.expression.T8FilterExpressionTokenizer;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author Hennie Brink
 */
public class T8DataFilterExpressionParser
{
    private DataFilterConjunction defaultConjunction;
    private final List<String> fieldIds;
    private final String entityId;

    public T8DataFilterExpressionParser(String entityId, List<String> fieldIds)
    {
        this.entityId = entityId;
        this.defaultConjunction = DataFilterConjunction.AND;
        this.fieldIds = new ArrayList<>();
        setFieldIds(fieldIds);
    }

    public void setFieldIds(List<String> fieldIds)
    {
        this.fieldIds.clear();
        if (fieldIds != null) this.fieldIds.addAll(fieldIds);
    }

    public void setWhitespaceConjunction(DataFilterConjunction conjunction)
    {
        this.defaultConjunction = conjunction;
    }

    public List<String> parseQuickExpressionSearchTerms(String expression)
    {
        if (!Strings.isNullOrEmpty(expression))
        {
            return Strings.split(expression, "%", false, true);
        }
        else return new ArrayList<>();
    }

    public T8DataFilter parseQuickExpression(String expression, boolean useFullText)
    {
        T8DataFilter dataFilter;

        // Create the new data filter.
        dataFilter = new T8DataFilter(entityId);
        if (!Strings.isNullOrEmpty(expression))
        {
            List<String> searchTerms;
            String searchFieldId;

            // Split the input expression on '%' characters.
            searchTerms = Strings.split(expression, "%", false, true);
            searchFieldId = fieldIds.get(0);
            if (useFullText)
            {
                StringBuilder predicateExpression;

                // Create the expression to use with the CONTAINS predicate.
                predicateExpression = new StringBuilder();
                for (int termIndex = 0; termIndex < searchTerms.size(); termIndex++)
                {
                    String searchTerm;

                    searchTerm = searchTerms.get(termIndex);
                    predicateExpression.append("\"");
                    predicateExpression.append(searchTerm);
                    predicateExpression.append("\"");
                    if (termIndex < searchTerms.size() - 1) predicateExpression.append(" AND ");
                }

                // Add the predicate expression to the filter.
                dataFilter.addFilterCriterion(searchFieldId, DataFilterOperator.CONTAINS, predicateExpression);
            }
            else
            {
                // Split the input expression on '%' characters and add each string as a LIKE criterion.
                for (String searchTerm : Strings.split(expression, "%", false, true))
                {
                    dataFilter.addFilterCriterion(searchFieldId, DataFilterOperator.LIKE, searchTerm);
                }
            }
        }

        // Return the final filter.
        return dataFilter;
    }

    public List<String> parseExpressionSearchTerms(String expression) throws T8SearchExpressionParseException
    {
        List<String> searchTerms;

        searchTerms = new ArrayList<>();
        if (!Strings.isNullOrEmpty(expression))
        {
            T8FilterExpressionTokenizer tokenizer;

            tokenizer = new T8FilterExpressionTokenizer(expression);
            while (tokenizer.moveNext())
            {
                T8FilterExpressionToken nextToken;

                nextToken = tokenizer.getToken();
                switch (nextToken.getTokenType())
                {
                    case WORD:
                    case PHRASE:
                        searchTerms.add((String)nextToken.getData());
                }
            }
        }

        return searchTerms;
    }

    public T8DataFilter parseExpression(String expression, boolean useFullText) throws T8SearchExpressionParseException
    {
        T8DataFilter dataFilter;

        dataFilter = new T8DataFilter(entityId);
        if (!Strings.isNullOrEmpty(expression))
        {
            T8DataFilterCriteria rootCriteria;

            rootCriteria = new T8DataFilterCriteria();
            dataFilter.setFilterCriteria(rootCriteria);
            if (fieldIds != null)
            {
                for (String fieldId : fieldIds)
                {
                    if (useFullText)
                    {
                        // Add the predicate expression to the filter.
                        rootCriteria.addFilterClause(DataFilterConjunction.OR, parseContainsFilterCriterion(expression, fieldId));
                    }
                    else
                    {
                        rootCriteria.addFilterClause(DataFilterConjunction.OR, parseFilterCriteria(expression, fieldId));
                    }
                }
            }
            else rootCriteria.addFilterClause(DataFilterConjunction.OR, parseFilterCriteria(expression, null));
        }

        return dataFilter;
    }

    private T8DataFilterCriterion parseContainsFilterCriterion(String expression, String fieldId) throws T8SearchExpressionParseException
    {
        T8FilterExpressionToken previousToken = null;
        T8FilterExpressionTokenizer tokenizer;
        T8FilterExpressionToken nextToken;
        StringBuilder containsExpression;

        containsExpression = new StringBuilder();
        tokenizer = new T8FilterExpressionTokenizer(expression);
        while (tokenizer.moveNext())
        {
            nextToken = tokenizer.getToken();
            switch (nextToken.getTokenType())
            {
                case SYMBOL:
                    // If we have to strings or symbols next to each other, insert a conjunction.
                    if (requiresConjunctionInsertion(previousToken, nextToken))
                    {
                        containsExpression.append(" ");
                        containsExpression.append(defaultConjunction.toString());
                        containsExpression.append(" ");
                    }

                    containsExpression.append(nextToken.getData());
                    break;
                case OPERATOR:
                    containsExpression.append(" ");
                    containsExpression.append(nextToken.getData());
                    containsExpression.append(" ");
                    break;
                case KEYWORD:
                    containsExpression.append(" ");
                    containsExpression.append(nextToken.getData());
                    containsExpression.append(" ");
                    break;
                case WORD:
                case PHRASE:
                    // If we have to strings or symbols next to each other, insert a conjunction.
                    if (requiresConjunctionInsertion(previousToken, nextToken))
                    {
                        System.out.println("Required conjunction: " + previousToken + " - " + nextToken);
                        containsExpression.append(" ");
                        containsExpression.append(defaultConjunction.toString());
                    }

                    // Append the next token data.
                    containsExpression.append(" \"");
                    containsExpression.append(nextToken.getData());
                    containsExpression.append("\" ");
                    break;
            }

            previousToken = nextToken;
        }

        return new T8DataFilterCriterion(fieldId, DataFilterOperator.CONTAINS, containsExpression, false);
    }

    private T8DataFilterCriteria parseFilterCriteria(String expression, String fieldId) throws T8SearchExpressionParseException
    {
        T8FilterExpressionTokenizer tokenizer;
        T8FilterExpressionToken nextToken;
        Queue<T8DataFilterCriteria> filterCriteriaList;
        T8DataFilterCriteria filterCriteria;
        T8FilterExpressionToken previousToken = null;

        tokenizer = new T8FilterExpressionTokenizer(expression);
        filterCriteriaList = Collections.asLifoQueue(new LinkedList<T8DataFilterCriteria>());
        filterCriteria = new T8DataFilterCriteria();
        while (tokenizer.moveNext())
        {
            nextToken = tokenizer.getToken();
            switch(nextToken.getTokenType())
            {
                case SYMBOL:
                    if ((char)nextToken.getData() == '(')
                    {
                        T8DataFilterCriteria newCriteria;

                        newCriteria = new T8DataFilterCriteria();
                        filterCriteria.addFilterClause(determineFilterConjunction(previousToken), newCriteria);
                        // Offer the current criteria so we can go back to it.
                        filterCriteriaList.offer(filterCriteria);
                        filterCriteria = newCriteria;
                    }
                    else if ((char)nextToken.getData() == ')')
                    {
                        filterCriteria = filterCriteriaList.poll();
                    }
                    else throw new RuntimeException("Invalid Symbol Type");
                    break;
                case FIELD_IDENTIFIER:
                    DataFilterOperator operator;
                    Object value;

                    if (tokenizer.moveNext())
                    {
                        T8FilterExpressionToken operatorToken;

                        operatorToken = tokenizer.getToken();
                        if(operatorToken.getTokenType() != T8FilterExpressionToken.TokenType.OPERATOR)
                        {
                            throw new T8SearchExpressionParseException("Operator Expected");
                        }
                        else operator = getFilterOperator(tokenizer, false);
                    }
                    else throw new T8SearchExpressionParseException("Operator Expected");

                    if (tokenizer.moveNext())
                    {
                        T8FilterExpressionToken fieldValueToken;

                        fieldValueToken = tokenizer.getToken();
                        if(fieldValueToken.getTokenType() != T8FilterExpressionToken.TokenType.WORD)
                        {
                            throw new T8SearchExpressionParseException("Field Value Expected");
                        }
                        else value = fieldValueToken.getData();
                    }
                    else throw new T8SearchExpressionParseException("Field Value Expected");

                    filterCriteria.addFilterClause(determineFilterConjunction(previousToken), (String) nextToken.getData(), operator, value);
                    break;
                case WORD:
                case PHRASE:
                    if (fieldId != null)
                    {
                        filterCriteria.addFilterClause(determineFilterConjunction(previousToken), fieldId, DataFilterOperator.LIKE, nextToken.getData(), true);
                    }
                    break;
            }

            previousToken = nextToken;
        }

        if (filterCriteriaList.size() > 0) throw new T8SearchExpressionParseException("Missing closing parens");
        else return filterCriteria;
    }

    private boolean requiresConjunctionInsertion(T8FilterExpressionToken previousToken, T8FilterExpressionToken nextToken)
    {
        if (previousToken != null)
        {
            TokenType previousType;
            TokenType nextType;

            previousType = previousToken.getTokenType();
            nextType = nextToken.getTokenType();
            if ((nextType == TokenType.WORD) || (nextType == TokenType.PHRASE) || ((nextType == TokenType.SYMBOL) && ((char)nextToken.getData() == '(')))
            {
                // We need conjunctions between strings and parenthesis.
                return ((previousType == TokenType.WORD) || (previousType == TokenType.PHRASE) || ((previousType == TokenType.SYMBOL) && ((char)previousToken.getData() == ')')));
            }
            else return false;
        }
        else return false;
    }

    private DataFilterConjunction determineFilterConjunction(T8FilterExpressionToken previousToken)
    {
        if (previousToken == null)
        {
            return DataFilterConjunction.AND;
        }
        else if (previousToken.getTokenType() == T8FilterExpressionToken.TokenType.KEYWORD)
        {
            if ("AND".equals(previousToken.getData()))
            {
                return DataFilterConjunction.AND;
            }
            else if ("OR".equals(previousToken.getData()))
            {
                return DataFilterConjunction.OR;
            }
            else throw new RuntimeException("Unexpected Keyword found: " + previousToken.getData());
        }
        else return defaultConjunction;
    }

    private DataFilterOperator getFilterOperator(T8FilterExpressionTokenizer tokenizer, boolean negative) throws T8SearchExpressionParseException
    {
        switch((char)tokenizer.getToken().getData())
        {
            case '=':
                return negative ? DataFilterOperator.NOT_EQUAL : DataFilterOperator.EQUAL;
            case '<':
                return DataFilterOperator.LESS_THAN_OR_EQUAL;
            case '>':
                return DataFilterOperator.GREATER_THAN_OR_EQUAL;
            case '%':
                return negative ? DataFilterOperator.NOT_LIKE : DataFilterOperator.LIKE;
            case '!':
                tokenizer.moveNext();
                return getFilterOperator(tokenizer, true);
            default:
                throw new RuntimeException("Invalid operator " + tokenizer.getToken().getData());
        }
    }
}
