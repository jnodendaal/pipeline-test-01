package com.pilog.t8;

/**
 * @author Bouwer du Preez
 */
public class T8UserManagerConfiguration
{
    private String userDataFileContextId;

    public T8UserManagerConfiguration()
    {
    }

    public String getUserDataFileContextId()
    {
        return userDataFileContextId;
    }

    public void setUserDataFileContextId(String contextId)
    {
        this.userDataFileContextId = contextId;
    }
}
