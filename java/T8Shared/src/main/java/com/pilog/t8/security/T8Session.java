package com.pilog.t8.security;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8Session implements T8SessionContext, T8PerformanceStatisticsProvider
{
    private String sessionId;
    private transient String systemAgentIdentifier;
    private transient String systemAgentInstanceIdentifier;
    private String userIdentifier;
    private String userProfileIdentifier;
    private String languageDefinitionId;
    private String contentLanguageId;
    private String userName;
    private String userSurname;
    private String organizationId;
    private String rootOrganizationId;
    private String serverURL;
    private List<String> workflowProfileIds;
    private boolean loggedIn; // This flag is never checked by the system, it is set by the security manager for use in client code.
    private transient boolean systemSession = false;
    private final transient T8SessionDataMap sessionDataMap;
    private transient T8PerformanceStatistics stats;

    T8Session(String sessionId)
    {
        this.workflowProfileIds = new ArrayList<>();
        this.sessionId = sessionId;
        this.workflowProfileIds = new ArrayList<>();
        this.languageDefinitionId = null;
        this.organizationId = null;
        this.rootOrganizationId = null;
        this.loggedIn = false;
        this.sessionDataMap = new T8SessionDataMap(this);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName() + ":" + sessionId);
        this.stats.setEnabled(false); // The default behaviour is always false.  Statistics can be enabled on demand.
    }

    T8Session(String sessionIdentifier, String userIdentifier, String userProfileIdentifier)
    {
        this(sessionIdentifier);
        this.userIdentifier = userIdentifier;
        this.userProfileIdentifier = userProfileIdentifier;
    }

    final void updateSession(T8SessionContext sessionContext)
    {
        this.systemAgentIdentifier = sessionContext.getSystemAgentIdentifier();
        this.systemAgentInstanceIdentifier = sessionContext.getSystemAgentInstanceIdentifier();
        this.sessionId = sessionContext.getSessionIdentifier();
        this.userIdentifier = sessionContext.getUserIdentifier();
        this.userProfileIdentifier = sessionContext.getUserProfileIdentifier();
        this.workflowProfileIds = sessionContext.getWorkFlowProfileIdentifiers();
        this.languageDefinitionId = sessionContext.getLanguageIdentifier();
        this.contentLanguageId = sessionContext.getContentLanguageIdentifier();
        this.userName = sessionContext.getUserName();
        this.userSurname = sessionContext.getUserSurname();
        this.organizationId = sessionContext.getOrganizationIdentifier();
        this.rootOrganizationId = sessionContext.getRootOrganizationIdentifier();
        this.serverURL = sessionContext.getServerURL();
    }

    public void anonymize()
    {
        setUserIdentifier("ANONYMOUS_" + getSessionIdentifier());
        setUserProfileIdentifier("ANONYMOUS_" + getSessionIdentifier());
        setWorkFlowProfileIdentifiers(null);
        setUserName("Anonymous");
        setUserSurname("Anonymous");
        setOrganizationIdentifier(null);
        setRootOrganizationIdentifier(null);
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        // Because the performance statistics object is not serialized, this method has to account for possible nulls.
        // This might change in future but the contract of the method must stay the same.
        if (stats == null) stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        // Because the performance statistics object is not serialized, this method has to account for possible nulls.
        // This might change in future but the contract of the method must stay the same i.e. null must never be returned.
        if (stats == null)
        {
            stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
            stats.setEnabled(false);
            return stats;
        }
        else return stats;
    }

    @Override
    public Map<String, Object> getSessionParameterMap()
    {
        Map<String, Object> sessionParameters;

        sessionParameters = new HashMap<>();
        sessionParameters.put("SESSION_IDENTIFIER", sessionId);
        sessionParameters.put("SESSION_USER_IDENTIFIER", userIdentifier);
        sessionParameters.put("SESSION_USER_PROFILE_IDENTIFIER", userProfileIdentifier);
        sessionParameters.put("SESSION_LANGUAGE_IDENTIFIER", languageDefinitionId);
        sessionParameters.put("SESSION_CONTENT_LANGUAGE_IDENTIFIER", contentLanguageId);
        sessionParameters.put("SESSION_ORGANIZATION_IDENTIFIER", organizationId);
        sessionParameters.put("SESSION_ROOT_ORGANIZATION_IDENTIFIER", rootOrganizationId);
        return sessionParameters;
    }

    @Override
    public String getSystemAgentIdentifier()
    {
        return systemAgentIdentifier;
    }

    public void setSystemAgentIdentifier(String identifier)
    {
        this.systemAgentIdentifier = identifier;
    }

    @Override
    public String getSystemAgentInstanceIdentifier()
    {
        return systemAgentInstanceIdentifier;
    }

    public void setSystemAgentInstanceIdentifier(String identifier)
    {
        this.systemAgentInstanceIdentifier = identifier;
    }

    @Override
    public String getSessionIdentifier()
    {
        return sessionId;
    }

    void setSessionIdentifier(String identifier)
    {
        sessionId = identifier;
    }

    @Override
    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    void setUserIdentifier(String identifier)
    {
        userIdentifier = identifier;
    }

    @Override
    public String getUserProfileIdentifier()
    {
        return userProfileIdentifier;
    }

    void setUserProfileIdentifier(String identifier)
    {
        userProfileIdentifier = identifier;
    }

    @Override
    public String getServerURL()
    {
        return serverURL;
    }

    void setServerURL(String url)
    {
        serverURL = url;
    }

    @Override
    public String getLanguageIdentifier()
    {
        return languageDefinitionId;
    }

    void setLanguageIdentifier(String identifier)
    {
        languageDefinitionId = identifier;
    }

    @Override
    public String getOrganizationIdentifier()
    {
        return organizationId;
    }

    void setOrganizationIdentifier(String identifier)
    {
        organizationId = identifier;
    }

    @Override
    public String getRootOrganizationIdentifier()
    {
        return rootOrganizationId;
    }

    void setRootOrganizationIdentifier(String rootOrganizationIdentifier)
    {
        this.rootOrganizationId = rootOrganizationIdentifier;
    }

    @Override
    public String getContentLanguageIdentifier()
    {
        return contentLanguageId;
    }

    void setContentLanguageIdentifier(String isoLanguageIdentifier)
    {
        this.contentLanguageId = isoLanguageIdentifier;
    }

    @Override
    public String getUserName()
    {
        return userName;
    }

    void setUserName(String userName)
    {
        this.userName = userName;
    }

    @Override
    public String getUserSurname()
    {
        return userSurname;
    }

    void setUserSurname(String userSurname)
    {
        this.userSurname = userSurname;
    }

    public void setSystemSession(boolean systemSession)
    {
        this.systemSession = systemSession;
    }

    @Override
    public boolean isSystemSession()
    {
        return systemSession;
    }

    @Override
    public List<String> getWorkFlowProfileIdentifiers()
    {
        return new ArrayList<>(workflowProfileIds);
    }

    void setWorkFlowProfileIdentifiers(List<String> workFlowProfileIdentifiers)
    {
        this.workflowProfileIds.clear();
        if (workFlowProfileIdentifiers != null)
        {
            this.workflowProfileIds.addAll(workFlowProfileIdentifiers);
        }
    }

    @Override
    public boolean isLoggedIn()
    {
        return loggedIn;
    }

    void setLoggedIn(boolean loggedIn)
    {
        this.loggedIn = loggedIn;
    }

    @Override
    public boolean isAnonymousSession()
    {
        return (this.userIdentifier != null && this.userIdentifier.startsWith("ANONYMOUS_"));
    }

    @Override
    public Serializable getSessionData(String dataId)
    {
        return this.sessionDataMap.getData(dataId);
    }

    @Override
    public Serializable setSessionData(String dataId, Serializable data)
    {
        return this.sessionDataMap.setData(dataId, data);
    }

    @Override
    public String toString()
    {
        if (systemSession)
        {
            return "T8Session{" + "sessionIdentifier=" + systemAgentIdentifier + ", userIdentifier=" + userIdentifier + '}';
        }
        else
        {
            return "T8Session{" + "sessionIdentifier=" + sessionId + ", userIdentifier=" + userIdentifier + '}';
        }
    }
}
