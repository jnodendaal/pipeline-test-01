package com.pilog.t8.time;

/**
 * @author Bouwer du Preez
 */
public class T8Hour implements T8TimeUnit
{
    private final long amount;

    public T8Hour(long amount)
    {
        this.amount = amount;
    }

    @Override
    public long getMilliseconds()
    {
        return 3600000 * amount;
    }

    public long getAmount()
    {
        return amount;
    }
}
