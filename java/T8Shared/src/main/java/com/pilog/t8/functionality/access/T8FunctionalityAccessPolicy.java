package com.pilog.t8.functionality.access;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessBlockerDefinition;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessPolicyDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityAccessPolicy
{
    private final List<T8FunctionalityAccessBlocker> blockers;

    public T8FunctionalityAccessPolicy(T8Context context, T8FunctionalityAccessPolicyDefinition definition) throws Exception
    {
        this.blockers = new ArrayList<T8FunctionalityAccessBlocker>();

        if (definition != null)
        {
            for (T8FunctionalityAccessBlockerDefinition blockerDefinition : definition.getAccessBlockerDefinitions())
            {
                blockers.add(blockerDefinition.createNewAccessBlockerInstance(context));
            }
        }
    }

    public List<T8FunctionalityBlock> getFunctionalityBlocks(T8Context context, String functionalityId, List<T8DataObject> dataObjects)
    {
        List<T8FunctionalityBlock> blocks;

        blocks = new ArrayList<T8FunctionalityBlock>();
        for (T8FunctionalityAccessBlocker blocker : blockers)
        {
            blocks.addAll(blocker.getFunctionalityBlocks(context, functionalityId, dataObjects));
        }

        return blocks;
    }
}
