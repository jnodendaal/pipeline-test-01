package com.pilog.t8.definition.data.source;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultRefreshableDataSourceDefinition extends T8DataSourceDefinition implements T8RefreshableDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {REFRESH_PARAMETER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //
    
    public T8DefaultRefreshableDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REFRESH_PARAMETER_DEFINITIONS.toString(), "Refresh Parameters", "The definitions of the parameters that may be used when this data source is refreshed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REFRESH_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getRefreshParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.REFRESH_PARAMETER_DEFINITIONS.toString());
    }

    public void setRefreshParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.REFRESH_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }
}
