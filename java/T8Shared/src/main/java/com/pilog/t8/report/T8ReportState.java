package com.pilog.t8.report;

import com.pilog.t8.time.T8Timestamp;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ReportState implements Serializable
{
    private String projectId;
    private String id;
    private String iid;
    private String name;
    private String description;
    private String userId;
    private String initiatorId;
    private String initiatorIid;
    private Status status;
    private T8Timestamp startTime;
    private T8Timestamp endTime;
    private T8Timestamp expirationTime;
    private String fileContextId;
    private String filePath;
    private final Map<String, Object> parameters;

    public enum Status
    {
        STARTED,
        COMPLETED,
        FAILED,
    }

    public T8ReportState(String projectId, String reportId, String reportIid)
    {
        this.projectId = projectId;
        this.id = reportId;
        this.iid = reportIid;
        this.parameters = new HashMap<String, Object>();
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getIid()
    {
        return iid;
    }

    public void setIid(String iid)
    {
        this.iid = iid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getFileContextId()
    {
        return fileContextId;
    }

    public void setFileContextId(String fileContextId)
    {
        this.fileContextId = fileContextId;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId)
    {
        this.initiatorId = initiatorId;
    }

    public String getInitiatorIid()
    {
        return initiatorIid;
    }

    public void setInitiatorIid(String initiatorIid)
    {
        this.initiatorIid = initiatorIid;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public T8Timestamp getStartTime()
    {
        return startTime;
    }

    public void setStartTime(T8Timestamp startTime)
    {
        this.startTime = startTime;
    }

    public T8Timestamp getEndTime()
    {
        return endTime;
    }

    public void setEndTime(T8Timestamp endTime)
    {
        this.endTime = endTime;
    }

    public T8Timestamp getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(T8Timestamp expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Map<String, Object> getParameters()
    {
        return new HashMap<String, Object>(parameters);
    }

    public void setParameters(Map<String, Object> parameters)
    {
        this.parameters.clear();
        if (parameters != null)
        {
            this.parameters.putAll(parameters);
        }
    }

    public void setParameter(String parameterId, Object parameterValue)
    {
        this.parameters.put(parameterId, parameterValue);
    }

    public Object getParameter(String parameterId)
    {
        return this.parameters.get(parameterId);
    }

    @Override
    public String toString()
    {
        return "T8ReportState{" + "projectId=" + projectId + ", id=" + id + ", iid=" + iid + ", name=" + name + ", description=" + description + ", initiatorId=" + initiatorId + ", initiatorIid=" + initiatorIid + '}';
    }
}
