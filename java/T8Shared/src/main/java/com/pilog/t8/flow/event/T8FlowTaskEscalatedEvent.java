package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskEscalatedEvent extends T8FlowEvent
{
    private final T8TaskEscalationTrigger trigger;
    private final int oldPriority;
    private final int newPriority;

    public static final String TYPE_ID = "TASK_PRIORITY_ESCALATED";

    public T8FlowTaskEscalatedEvent(T8TaskEscalationTrigger trigger, String taskIid, int oldPriority, int newPriority)
    {
        super(taskIid);
        this.trigger = trigger;
        this.oldPriority = oldPriority;
        this.newPriority = newPriority;
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public T8TaskEscalationTrigger getTrigger()
    {
        return trigger;
    }

    public int getOldPriority()
    {
        return oldPriority;
    }

    public int getNewPriority()
    {
        return newPriority;
    }

    public T8TaskDetails getTaskDetails()
    {
        return (T8TaskDetails)source;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
