package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowProfileDefinition;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FlowActivityDefinition extends T8WorkFlowNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {PROFILE_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8FlowActivityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROFILE_IDENTIFIER.toString(), "Work Flow Profile", "The work flow profile to which the task is restricted."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PROFILE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8WorkFlowProfileDefinition.GROUP_IDENTIFIER), true, "SYSTEM");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public FlowNodeType getNodeType()
    {
        return FlowNodeType.ACTIVITY;
    }

    public String getProfileIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROFILE_IDENTIFIER.toString());
    }

    public void setProfileIdentifier(String taskIdentifier)
    {
        setDefinitionDatum(Datum.PROFILE_IDENTIFIER.toString(), taskIdentifier);
    }
}
