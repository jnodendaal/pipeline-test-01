package com.pilog.t8.ui.functionality;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8TargetObjectValidationResult implements Serializable
{
    private final boolean valid;
    private final String message;
    
    public T8TargetObjectValidationResult(boolean valid, String message)
    {
        this.valid = valid;
        this.message = message;
    }

    public boolean isValid()
    {
        return valid;
    }

    public String getMessage()
    {
        return message;
    }
}
