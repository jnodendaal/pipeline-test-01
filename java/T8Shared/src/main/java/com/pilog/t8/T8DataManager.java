package com.pilog.t8;

import com.pilog.t8.cache.T8DataRecordCache;
import com.pilog.t8.cache.T8DataRequirementCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache;
import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataSessionDetails;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DataManager extends T8ServerModule
{
    /**
     * Checks accessibility of the specified entity and returns a boolean
     * result.
     * @param context
     * @param entityId The identifier of the entity to check.
     * @return Boolean true/false indicating accessibility/inaccessibility.
     */
    public boolean isEntityAccessible(T8Context context, String entityId);

    /**
     * This method flushes all cached definitions from the data manager and
     * resets it to a clean state.  This method must be used very carefully
     * because all transactions in progress will be closed and all data sessions
     * ended.
     * @throws Exception
     */
    public void reset() throws Exception;

    /**
     * Returns a map containing the sizes of the caches that are currently active.
     * @return A map containing the names of caches as key set and for each key an
     * integer value indicating the number of entries in the cache.
     */
    public Map<String, Integer> getCacheSizes();

    /**
     * Clears the specified cache, removing all entries it contains.  This method should
     * be used carefully and only when no concurrent access to the cache can be guaranteed.
     * @param cacheId The id of the cache to clear.
     */
    public void clearCache(String cacheId);

    /**
     * Returns the global terminology cache associated with the specified root
     * organization.
     * @param orgId The root organization ID for which to return a
     * terminology cache.
     * @return The global terminology cache associated with the specified root
     * organization.
     */
    public T8TerminologyCache getTerminologyCache(String orgId);

    /**
     * Returns the global data record cache associated with the specified root
     * organization.
     * @param orgId The root organization ID for which to return a
     * data requirement cache.
     * @return The global data record cache associated with the specified root
     * organization.
     */
    public T8DataRecordCache getDataRecordCache(String orgId);

    /**
     * Returns the global data requirement instance cache associated with the specified root
     * organization.
     * @param orgId The root organization ID for which to return a
     * data requirement cache.
     * @return The global data requirement instance cache associated with the specified root
     * organization.
     */
    public T8DataRequirementInstanceCache getDataRequirementInstanceCache(String orgId);

    /**
     * Returns the global data requirement cache associated with the specified root
     * organization.
     * @param orgId The root organization ID for which to return a
     * data requirement cache.
     * @return The global data requirement cache associated with the specified root
     * organization.
     */
    public T8DataRequirementCache getDataRequirementCache(String orgId);

    /**
     * Attempts to find the specified data in one of the server caches using the
     * specified cache parameters.
     * @param dataIdentifier The identifier of the data to find.
     * @param parameters The parameters of the data to find.
     * @return The cached data (if any is found).  If nothing is found, null is
     * returned but it is also possible that the cached value is null.
     * @throws java.lang.Exception
     */
    public Object getCachedData(String dataIdentifier, Map<String, Object> parameters) throws Exception;

    /**
     * Refreshes the data cache responsible for caching of the specified data.
     * @param dataIdentifier The identifier of the data to find.
     * @param parameters The parameters of the data to find.
     * @return The cached data (if any is found).  If nothing is found, null is
     * returned but it is also possible that the cached value is null.
     * @throws java.lang.Exception
     */
    public Object recacheData(String dataIdentifier, Map<String, Object> parameters) throws Exception;

    /**
     * Generates a key value according to the specified key definition.
     * @param keyId The identifier of the key to generate.
     * @return Then key generated from the specified key definition.
     * @throws java.lang.Exception
     */
    public String generateKey(String keyId) throws Exception;

    /**
     * Returns all current data session details from the server.
     * @param context
     * @return
     */
    public List<T8DataSessionDetails> getDataSessionDetails(T8Context context);

    /**
     * Returns the specified connection pool.  If the specified pool has not
     * been initialized, it will be done on request.
     * @param id
     * @return
     */
    public T8DataConnectionPool getDataConnectionPool(String id);

    /**
     * Installs the supplied connection pool in the data manager so that it is
     * available to all requesters.
     * @param connectionPool
     */
    public void installDataConnectionPool(T8DataConnectionPool connectionPool);

    /**
     * Opens a new data session bound to the invoking thread.  Calling this method from a
     * thread to which a data session is already associated is not permitted.
     * @param context The session for which to create the new data session.
     * @return The newly created data session.
     */
    public T8DataSession openDataSession(T8Context context);

    /**
     * Returns the data session bound to the invoking thread.  If no session
     * exists for the calling thread, a new session is created using the supplied session context.
     * @param context The context for which to return the current data session.
     * @return The data session to use within the current thread.
     */
    public T8DataSession getCurrentSession();

    /**
     * Closes the data session bound to the invoking thread if non-active, releasing all
     * allocated resources.  If the session is active i.e. used by a long-running process
     * it is marked for closure and will be released automatically as soon as it becomes inactive.
     */
    public void closeCurrentSession();

    /**
     * Closes and removes the data session associated with the supplies session identifier
     * @param dataSessionId The Identifier of the Data Session to Kill
     */
    public void killDataSession(String dataSessionId);

    /**
     * Validates the supplied data entities and returns a list of validation
     * reports.
     * @param context
     * @param entityList
     * @param scriptDefinition
     * @return
     * @throws Exception
     */
    public List<T8DataEntityValidationReport> validateDataEntities(T8Context context, List<T8DataEntity> entityList, T8ServerEntityValidationScriptDefinition scriptDefinition) throws Exception;
}
