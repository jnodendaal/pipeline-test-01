package com.pilog.t8.definition.process.report;

import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.process.T8Process;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public class T8ReportGenerationProcessDefinition extends T8ProcessDefinition
{
    public static final String TYPE_IDENTIFIER = "@DT_SERVER_PROCESS_REPORT_GENERATION";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;

    public static String PROJECT_ID_INPUT_PARAMETER_ID = "$P_PROJECT_ID";
    public static String REPORT_ID_INPUT_PARAMETER_ID = "$P_REPORT_ID";
    public static String REPORT_PARAMETERS_PARAMETER_ID = "$P_REPORT_PARAMETERS";

    public T8ReportGenerationProcessDefinition(String id)
    {
        super(id);
    }

    @Override
    public T8Process getNewProcessInstance(T8Context context, String processIid)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.process.report.T8ReportGenerationProcess", new Class<?>[]{T8Context.class, T8ReportGenerationProcessDefinition.class, String.class}, context, this, processIid);
    }

    @Override
    public Boolean isFinalizeOnCompletionEnabled()
    {
        // Always return true because the report generation process must always be finalized.
        return true;
    }
}
