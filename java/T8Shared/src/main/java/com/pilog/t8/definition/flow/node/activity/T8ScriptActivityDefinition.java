package com.pilog.t8.definition.flow.node.activity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowNode;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.script.T8FlowActivityScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptActivityDefinition extends T8FlowActivityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_NODE_ACTIVITY_SCRIPT";
    public static final String DISPLAY_NAME = "Script Activity";
    public static final String DESCRIPTION = "An activity that executes a script.";
    private enum Datum {SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8ScriptActivityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SCRIPT_DEFINITION.toString(), "Script", "The script to execute."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FlowActivityScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8FlowNode getNewNodeInstance(T8Context context, T8Flow parentFlow, String nodeIid) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.node.activity.T8ScriptActivityNode").getConstructor(T8Context.class, T8Flow.class, T8ScriptActivityDefinition.class, String.class);
        return (T8FlowNode)constructor.newInstance(context, parentFlow, this, nodeIid);
    }

    @Override
    public String getProfileIdentifier()
    {
        return null;
    }

    public T8FlowActivityScriptDefinition getScriptDefinition()
    {
        return (T8FlowActivityScriptDefinition)getDefinitionDatum(Datum.SCRIPT_DEFINITION.toString());
    }

    public void setScriptDefinition(T8FlowActivityScriptDefinition definition)
    {
        setDefinitionDatum(Datum.SCRIPT_DEFINITION.toString(), definition);
    }
}
