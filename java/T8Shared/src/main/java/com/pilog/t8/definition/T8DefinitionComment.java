package com.pilog.t8.definition;

import com.pilog.t8.time.T8Timestamp;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionComment implements Serializable
{
    private T8Timestamp timestamp;
    private CommentType type;
    private String userId;
    private String text;

    public enum CommentType {CONTRIBUTION, FINALIZATION};

    public T8DefinitionComment()
    {
    }

    public T8DefinitionComment(T8Timestamp timestamp, CommentType type, String userId, String text)
    {
        this.timestamp = timestamp;
        this.type = type;
        this.userId = userId;
        this.text = text;
    }

    public T8DefinitionComment copy()
    {
        T8DefinitionComment copy;

        copy = new T8DefinitionComment();
        copy.setText(text);
        copy.setTimestamp(timestamp);
        copy.setType(type);
        copy.setUserId(userId);
        return copy;
    }

    public T8Timestamp getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(T8Timestamp timestamp)
    {
        this.timestamp = timestamp;
    }

    public CommentType getType()
    {
        return type;
    }

    public void setType(CommentType type)
    {
        this.type = type;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }
}
