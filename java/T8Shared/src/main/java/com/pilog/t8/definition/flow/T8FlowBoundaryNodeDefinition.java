package com.pilog.t8.definition.flow;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowBoundaryNodeDefinition
{
    public String getAttachedToNodeIdentifier();
    public boolean isCancelActivity();
}
