package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.state.data.T8ParameterStateDataSet;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.state.T8StateUpdates;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateParameterMap extends LinkedHashMap<String, Object> implements T8FlowStateParameterCollection
{
    protected final T8FlowStateParameterCollection parentCollection;
    protected final String flowIid;
    protected final String parameterIid;
    protected final String entityId;
    protected final Map<String, String> parameterIids;
    protected final Set<String> insertedKeys;
    protected final Set<String> updatedKeys;

    public T8FlowStateParameterMap(T8FlowStateParameterCollection parentCollection, String flowIid, String parameterIid, String entityId, T8ParameterStateDataSet dataSet)
    {
        this.parentCollection = parentCollection;
        this.flowIid = flowIid;
        this.parameterIid = parameterIid;
        this.entityId = entityId;
        this.parameterIids = new LinkedHashMap<>();
        this.insertedKeys = new HashSet<>();
        this.updatedKeys = new HashSet<>();
        if (dataSet != null) addParameters(dataSet);
    }

    protected synchronized void addParameters(T8ParameterStateDataSet dataSet)
    {
        List<T8DataEntity> entityList;

        entityList = dataSet.getParameterEntitiesByParentIID(parameterIid);
        for (T8DataEntity parameterEntity : entityList)
        {
            String entryParameterIid;
            String parameterKey;
            String parameterValue;
            String parameterType;

            entryParameterIid = (String)parameterEntity.getFieldValue(entityId + "$PARAMETER_IID");
            parameterType = (String)parameterEntity.getFieldValue(entityId + "$TYPE");
            parameterKey = (String)parameterEntity.getFieldValue(entityId + "$PARAMETER_KEY");
            parameterValue = (String)parameterEntity.getFieldValue(entityId + "$VALUE");

            put(parameterKey, createParameterValue(entryParameterIid, parameterType, parameterValue, entityId, dataSet));
            parameterIids.put(parameterKey, entryParameterIid);
        }
    }

    public synchronized T8DataEntity createEntity(int index, String key, T8DataEntityDefinition entityDefinition)
    {
        T8DataEntity newEntity;
        Object value;

        value = get(key);

        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityId + "$PARAMETER_IID", parameterIids.get(key));
        newEntity.setFieldValue(entityId + "$PARENT_PARAMETER_IID", parameterIid); // Each content entity in this map has a PARENT_PARAMETER_IID = the map's PARAMETER_IID.
        newEntity.setFieldValue(entityId + "$TYPE", getParameterType(value));
        newEntity.setFieldValue(entityId + "$SEQUENCE", index);
        newEntity.setFieldValue(entityId + "$PARAMETER_KEY", key);
        newEntity.setFieldValue(entityId + "$VALUE", getParameterStringValue(value));
        newEntity.setFieldValue(entityId + "$FLOW_IID", flowIid);
        return newEntity;
    }

    @Override
    public synchronized T8FlowStateParameterMap createNewParameterMap(T8FlowStateParameterCollection newMapParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        if (parentCollection != null)
        {
            return parentCollection.createNewParameterMap(newMapParentCollection, parameterIID, dataSet);
        }
        else return new T8FlowStateParameterMap(newMapParentCollection, flowIid, parameterIID, entityId, dataSet);
    }

    @Override
    public synchronized T8FlowStateParameterList createNewParameterList(T8FlowStateParameterCollection newListParentCollection, String parameterIID, T8ParameterStateDataSet dataSet)
    {
        if (parentCollection != null)
        {
            return parentCollection.createNewParameterList(newListParentCollection, parameterIID, dataSet);
        }
        return new T8FlowStateParameterList(newListParentCollection, flowIid, parameterIID, entityId, dataSet);
    }

    @Override
    public String getParameterIid()
    {
        return parameterIid;
    }

    public int getTotalParameterCount()
    {
        int count;

        count = 0;
        count += this.size();

        // Check for sub-collections.
        for (Object value : values())
        {
            if (value instanceof T8FlowStateParameterMap)
            {
                count += ((T8FlowStateParameterMap)value).getTotalParameterCount();
            }
            else if (value instanceof T8FlowStateParameterList)
            {
                count += ((T8FlowStateParameterList)value).getTotalParameterCount();
            }
        }

        // Return the total count.
        return count;
    }

    public synchronized void statePersisted()
    {
        // Clear this state's flags.
        insertedKeys.clear();
        updatedKeys.clear();

        // Check for sub-collections.
        for (Object value : values())
        {
            if (value instanceof T8FlowStateParameterMap)
            {
                ((T8FlowStateParameterMap)value).statePersisted();
            }
            else if (value instanceof T8FlowStateParameterList)
            {
                ((T8FlowStateParameterList)value).statePersisted();
            }
        }
    }

    private synchronized int getKeyIndex(String key)
    {
        int index;

        index = 0;
        for (String nextKey : keySet())
        {
            if (nextKey.equals(key)) return index;
            index++;
        }

        return -1;
    }

    public synchronized void addUpdates(T8DataEntityDefinition entityDefinition, T8StateUpdates updates)
    {
        // Add the updates for each inserted key.
        for (String insertedKey : insertedKeys)
        {
            updates.addInsert(createEntity(getKeyIndex(insertedKey), insertedKey, entityDefinition));
        }

        // Add the updates for each updated key.
        for (String updatedKey : updatedKeys)
        {
            updates.addUpdate(createEntity(getKeyIndex(updatedKey), updatedKey, entityDefinition));
        }

        // Check for sub-collections.
        for (Object value : values())
        {
            if (value instanceof T8FlowStateParameterMap)
            {
                ((T8FlowStateParameterMap)value).addUpdates(entityDefinition, updates);
            }
            else if (value instanceof T8FlowStateParameterList)
            {
                ((T8FlowStateParameterList)value).addUpdates(entityDefinition, updates);
            }
        }
    }

    @Override
    public synchronized Object remove(Object key)
    {
        throw new RuntimeException("Cannot remove parameter.");
    }

    @Override
    public synchronized void putAll(Map<? extends String, ? extends Object> map)
    {
        for (Map.Entry<? extends String, ? extends Object> entrySet : map.entrySet())
        {
            put(entrySet.getKey(), entrySet.getValue());
        }
    }

    @Override
    public synchronized Object put(String key, Object value)
    {
        if (!containsKey(key))
        {
            if (value != null)
            {
                String parameterIid;

                parameterIid = T8IdentifierUtilities.createNewGUID();
                parameterIids.put(key, parameterIid);
                insertedKeys.add(key);

                if (value instanceof Map)
                {
                    T8FlowStateParameterMap newMap;

                    newMap = createNewParameterMap(this, parameterIid, null);
                    newMap.putAll((Map)value);
                    return super.put(key, newMap);
                }
                else if (value instanceof List)
                {
                    T8FlowStateParameterList newList;

                    newList = createNewParameterList(this, parameterIid, null);
                    for (Object valueElement : (List)value)
                    {
                        newList.add(valueElement);
                    }

                    return super.put(key, newList);
                }
                else
                {
                    return super.put(key, value);
                }
            }
            else return null;
        }
        else
        {
            Object oldValue;

            oldValue = get(key);
            if ((oldValue instanceof List) || (oldValue instanceof Map))
            {
                if (!Objects.equals(oldValue, value))
                {
                    throw new RuntimeException("Attempt to update an existing collection value in state parameter map " + parameterIid + " to a new value using key: " + key + ". Old Value: " + oldValue + ", New Value: " + value);
                }
                else return oldValue;
            }
            else // Updated value is not a collection.
            {
                if (!Objects.equals(oldValue, value))
                {
                    updatedKeys.add(key);
                    return super.put(key, value);
                }
                else return oldValue;
            }
        }
    }

    public synchronized Object createParameterValue(String parameterIid, String parameterType, String value, String entityIdentifier, T8ParameterStateDataSet dataSet)
    {
        switch (parameterType)
        {
            case "MAP":
            {
                return createNewParameterMap(this, parameterIid, dataSet);
            }
            case "LIST":
            {
                return createNewParameterList(this, parameterIid, dataSet);
            }
            case "INTEGER":
            {
                return Integer.parseInt(value);
            }
            case "LONG":
            {
                return Long.parseLong(value);
            }
            case "DOUBLE":
            {
                return Double.parseDouble(value);
            }
            case "FLOAT":
            {
                return Float.parseFloat(value);
            }
            case "BOOLEAN":
            {
                return Boolean.parseBoolean(value);
            }
            case "TIMESTAMP":
            {
                return T8Timestamp.fromTime(Long.parseLong(value));
            }
            default:
            {
                return value; // Just return the String value.
            }
        }
    }

    public static String getParameterStringValue(Object value)
    {
        if (value instanceof Map)
        {
            return ((T8FlowStateParameterMap)value).getParameterIid();
        }
        else if (value instanceof List)
        {
            return ((T8FlowStateParameterList)value).getParameterIid();
        }
        else if (value instanceof T8Timestamp)
        {
            return String.valueOf(((T8Timestamp)value).getMilliseconds());
        }
        else
        {
            return value.toString();
        }
    }

    public static String getParameterType(Object parameterValue)
    {
        if (parameterValue instanceof Map)
        {
            return "MAP";
        }
        else if (parameterValue instanceof List)
        {
            return "LIST";
        }
        else if (parameterValue instanceof Integer)
        {
            return "INTEGER";
        }
        else if (parameterValue instanceof Long)
        {
            return "LONG";
        }
        else if (parameterValue instanceof Double)
        {
            return "DOUBLE";
        }
        else if (parameterValue instanceof Float)
        {
            return "FLOAT";
        }
        else if (parameterValue instanceof Boolean)
        {
            return "BOOLEAN";
        }
        else if (parameterValue instanceof String)
        {
            return "STRING";
        }
        else if (parameterValue instanceof T8Timestamp)
        {
            return "TIMESTAMP";
        }
        else throw new RuntimeException("Invalid parameter type: " + parameterValue.getClass());
    }
}
