package com.pilog.t8.data.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectState implements Serializable
{
    private final String graphId;
    private final String id;
    private final String conceptId;
    private final List<String> creatorFunctionalityIds;
    private final List<T8DataObjectStateTransition> transitions;

    public T8DataObjectState(String graphId, String id, String conceptId)
    {
        this.graphId = graphId;
        this.id = id;
        this.conceptId = conceptId;
        this.creatorFunctionalityIds = new ArrayList<>();
        this.transitions = new ArrayList<>();
    }

    public String getId()
    {
        return id;
    }

    public String getPublicId()
    {
        return graphId + id;
    }

    public String getGraphId()
    {
        return graphId;
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public boolean isCreatedBy(String functionalityId)
    {
        return creatorFunctionalityIds.contains(functionalityId);
    }

    public List<String> getCreatorFunctionalities()
    {
        return new ArrayList<>(creatorFunctionalityIds);
    }

    public void setCreatorFunctionalityIds(List<String> functionalityIds)
    {
        this.creatorFunctionalityIds.clear();
        if (functionalityIds != null)
        {
            this.creatorFunctionalityIds.addAll(functionalityIds);
        }
    }

    public void addTransition(T8DataObjectStateTransition transition)
    {
        transitions.add(transition);
    }

    public List<T8DataObjectStateTransition> getTransitions()
    {
        return new ArrayList<>(transitions);
    }

    public Set<String> getFunctionalityIdSet()
    {
        HashSet<String> identifiers;

        identifiers = new HashSet<String>();
        for (T8DataObjectStateTransition transition : transitions)
        {
            identifiers.add(transition.getFunctionalityId());
        }

        return identifiers;
    }

    public T8DataObjectState copy()
    {
        T8DataObjectState copy;

        copy = new T8DataObjectState(graphId, id, conceptId);
        copy.setCreatorFunctionalityIds(creatorFunctionalityIds);
        for (T8DataObjectStateTransition transition : transitions)
        {
            copy.addTransition(transition.copy());
        }

        return copy;
    }

    @Override
    public String toString()
    {
        return "T8DataObjectState{" + "graphId=" + graphId + ", id=" + id + ", conceptId=" + conceptId + '}';
    }
}
