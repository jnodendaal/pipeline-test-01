package com.pilog.t8.definition.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8UserResourceDefinitions implements T8DefinitionResource
{
    public static final String USER_CLIENT_ENTITY_IDENTIFIER = "@E_USER_CLIENT";
    public static final String USER_CLIENT_DATA_SOURCE_IDENTIFIER = "@DS_USER_CLIENT";

    // Entity fields defined by the description renderer for entities from which description format definitions are retrieved.
    public static final String EF_USER_IDENTIFIER = "$IDENTIFIER";
    public static final String EF_USER_USERNAME = "$USERNAME";
    public static final String EF_USER_NAME = "$NAME";
    public static final String EF_USER_SURNAME = "$SURNAME";
    public static final String EF_ACTIVE = "$ACTIVE";
    public static final String EF_PASSWORD_EXPIRY_DATE = "$PASSWORD_EXPIRY_DATE";
    public static final String EF_USER_DEFINITION = "$DEFINITION";

    // Table Names.
    private static final String TABLE_USER = "USR";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<T8Definition>();

        // Add data source definitions if needed.
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))
        {
            T8TableDataSourceDefinition tableDataSourceDefinition;

            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(USER_CLIENT_DATA_SOURCE_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
            tableDataSourceDefinition.setTableName(TABLE_USER);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$IDENTIFIER", "IDENTIFIER", true, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$USERNAME", "USERNAME", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME", "NAME", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SURNAME", "SURNAME", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ACTIVE", "ACTIVE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PASSWORD_EXPIRY_DATE", "PASSWORD_EXPIRY_DATE", false, false, T8DataType.DATE_TIME));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DEFINITION", "DEFINITION", false, false, T8DataType.LONG_STRING));
            definitions.add(tableDataSourceDefinition);
        }

        // Added entity definitions if needed.
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = new T8DataEntityResourceDefinition(USER_CLIENT_ENTITY_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(USER_CLIENT_DATA_SOURCE_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_IDENTIFIER, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$IDENTIFIER", true, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_USERNAME, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$USERNAME", false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_NAME, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$NAME", false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_SURNAME, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$SURNAME", false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$ACTIVE", false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PASSWORD_EXPIRY_DATE, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$PASSWORD_EXPIRY_DATE", false, false, T8DataType.DATE_TIME));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_DEFINITION, USER_CLIENT_DATA_SOURCE_IDENTIFIER + "$DEFINITION", false, false, T8DataType.LONG_STRING));
            definitions.add(entityDefinition);
        }

        return definitions;
    }
}
