package com.pilog.t8.definition.script.validation.entity;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.entity.T8DataEntityValidationError;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.script.T8DefaultScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8EntityValidationScriptDefinition extends T8DefaultScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {DATA_ENTITY_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";
    public static final String PARAMETER_VALIDATION_SUCCESS = "$P_VALIDATION_SUCCESS";
    public static final String PARAMETER_VALIDATION_ERROR_LIST = "$P_VALIDATION_ERROR_LIST";

    public T8EntityValidationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity that will be validated by this script."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterDefinitions;
        T8DataParameterDefinition parameterDefinition;

        parameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_DATA_ENTITY, PARAMETER_DATA_ENTITY, "The data entity to validate.", T8DataType.DATA_ENTITY);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        return parameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterDefinitions;
        T8DataParameterDefinition parameterDefinition;

        parameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_VALIDATION_SUCCESS, PARAMETER_VALIDATION_SUCCESS, "A boolean value indicating whether or not the validation succeeded.", T8DataType.BOOLEAN);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_VALIDATION_ERROR_LIST, PARAMETER_VALIDATION_ERROR_LIST, "A list of all the validation errors (if any) that were identified by the script.", new T8DtList(T8DataType.CUSTOM_OBJECT));
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        return parameterDefinitions;
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<T8ScriptClassImport>();
        classImports.add(new T8ScriptClassImport(T8DataEntityValidationError.class.getSimpleName(), T8DataEntityValidationError.class));
        return classImports;
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }
}
