package com.pilog.t8.server.monitor;

import java.io.Serializable;

/**
 * @author Hennie Brink
 */
public class T8ServerMonitorReport implements Serializable
{
    private long reportTime;

    private int totalActiveSessions;
    private int totalActiveDataSessions;
    private int totalActiveDataTransactions;

    private int totalRunningOperations;
    private int totalQueuedCommunications;
    private int totalCachedFlows;

    private long javaMaxMemory;
    private long javaFreeMemory;
    private long javaTotalMemory;

    public long getReportTime()
    {
        return reportTime;
    }

    public void setReportTime(long reportTime)
    {
        this.reportTime = reportTime;
    }

    public int getTotalActiveSessions()
    {
        return totalActiveSessions;
    }

    public void setTotalActiveSessions(int totalActiveSessions)
    {
        this.totalActiveSessions = totalActiveSessions;
    }

    public int getTotalActiveDataSessions()
    {
        return totalActiveDataSessions;
    }

    public void setTotalActiveDataSessions(int totalActiveDataSessions)
    {
        this.totalActiveDataSessions = totalActiveDataSessions;
    }

    public int getTotalActiveDataTransactions()
    {
        return totalActiveDataTransactions;
    }

    public void setTotalActiveDataTransactions(int totalActiveDataTransactions)
    {
        this.totalActiveDataTransactions = totalActiveDataTransactions;
    }

    public int getTotalCachedFlows()
    {
        return totalCachedFlows;
    }

    public void setTotalCachedFlows(int totalCacheFlows)
    {
        this.totalCachedFlows = totalCacheFlows;
    }

    public int getTotalRunningOperations()
    {
        return totalRunningOperations;
    }

    public void setTotalRunningOperations(int totalRunningOperations)
    {
        this.totalRunningOperations = totalRunningOperations;
    }

    public int getTotalQueuedCommunications()
    {
        return totalQueuedCommunications;
    }

    public void setTotalQueuedCommunications(int totalQueuedCommunications)
    {
        this.totalQueuedCommunications = totalQueuedCommunications;
    }

    public long getJavaMaxMemory()
    {
        return javaMaxMemory;
    }

    public void setJavaMaxMemory(long javaMaxMemory)
    {
        this.javaMaxMemory = javaMaxMemory;
    }

    public long getJavaFreeMemory()
    {
        return javaFreeMemory;
    }

    public void setJavaFreeMemory(long javaFreeMemory)
    {
        this.javaFreeMemory = javaFreeMemory;
    }

    public long getJavaTotalMemory()
    {
        return javaTotalMemory;
    }

    public void setJavaTotalMemory(long javaTotalMemory)
    {
        this.javaTotalMemory = javaTotalMemory;
    }
}
