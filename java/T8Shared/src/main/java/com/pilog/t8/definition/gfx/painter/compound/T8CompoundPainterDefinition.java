package com.pilog.t8.definition.gfx.painter.compound;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CompoundPainterDefinition extends T8PainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_GFX_PAINTER_COMPOUND";
    public static final String DISPLAY_NAME = "Compound Painter";
    public static final String DESCRIPTION = "A painter that comprises a collection of sub-painters.  Each of the sub-painters are applied in a sequential fashion to achieve a compound effect.";
    public enum Datum {PAINTER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8CompoundPainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PAINTER_DEFINITIONS.toString(), "Painters", "The painters contained by this compound painter."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PAINTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PainterDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Painter getNewPainterInstance()
    {
        try
        {
            T8Painter painter;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.gfx.painter.compound.T8CompoundPainter").getConstructor(T8CompoundPainterDefinition.class);
            painter = (T8Painter)constructor.newInstance(this);
            return painter;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<T8PainterDefinition> getPainterDefinitions()
    {
        return (ArrayList<T8PainterDefinition>)getDefinitionDatum(Datum.PAINTER_DEFINITIONS.toString());
    }

    public void setPainterDefinitions(ArrayList<T8PainterDefinition> painterDefinitions)
    {
        getPainterDefinitions().clear();
        getPainterDefinitions().addAll(painterDefinitions);
    }
}
