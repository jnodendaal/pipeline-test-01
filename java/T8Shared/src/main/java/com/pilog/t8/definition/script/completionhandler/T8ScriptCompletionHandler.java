package com.pilog.t8.definition.script.completionhandler;

import com.pilog.epic.rsyntax.MethodParameterChoicesProvider;
import com.pilog.epic.rsyntax.RegexCompletionProvider;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8ScriptCompletionHandler
{
    public List<T8ScriptCompletionResolver> getResolvers();
    public MethodParameterChoicesProvider getMethodParameterChoicesProvider();
    public List<RegexCompletionProvider> getRegexCompletionProviders();
}
