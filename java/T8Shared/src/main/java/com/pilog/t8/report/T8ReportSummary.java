package com.pilog.t8.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ReportSummary implements Serializable
{
    private final Map<String, T8ReportTypeSummary> reportTypeSummaries;

    public T8ReportSummary()
    {
        reportTypeSummaries = new LinkedHashMap<String, T8ReportTypeSummary>();
    }

    public List<T8ReportTypeSummary> getTypeSummaries()
    {
        return new ArrayList<T8ReportTypeSummary>(reportTypeSummaries.values());
    }

    public T8ReportTypeSummary getTypeSummary(String reportId)
    {
        return reportTypeSummaries.get(reportId);
    }

    public void putTypeSummary(T8ReportTypeSummary summary)
    {
        reportTypeSummaries.put(summary.getProcessIdentifier(), summary);
    }

    public int getTotalReportCount()
    {
        int count;

        count = 0;
        for (T8ReportTypeSummary typeSummary : reportTypeSummaries.values())
        {
            count += typeSummary.getTotalCount();
        }

        return count;
    }
}
