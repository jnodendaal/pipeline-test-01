package com.pilog.t8.definition.flow;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowProfileDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_PROFILE";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_WORK_PROFILE";
    public static final String GROUP_NAME = "Workflow Profiles";
    public static final String GROUP_DESCRIPTION = "Configuration settings that describe the access rights and settings applicable to a specific type of workflow participant in the system.";
    public static final String STORAGE_PATH = "/work_flow_profiles";
    public static final String DISPLAY_NAME = "Work Flow Profile";
    public static final String DESCRIPTION = "A profile that defines the way that users or groups of users interact with work flow processes.";
    public static final String IDENTIFIER_PREFIX = "FLOW_PROFILE_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowProfileDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
}
