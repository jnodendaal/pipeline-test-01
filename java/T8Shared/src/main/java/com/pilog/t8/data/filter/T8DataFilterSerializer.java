package com.pilog.t8.data.filter;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.util.LinkedHashMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilterSerializer
{
    public T8DataFilterSerializer()
    {
    }

    public JsonValue serializeDataFilter(T8DataFilter filter)
    {
        LinkedHashMap<String, OrderMethod> fieldOrdering;
        T8DataFilterCriteria filterCriteria;
        JsonObject filterObject;
        JsonArray fieldArray;

        // Create a new Json object for the filter.
        filterObject = new JsonObject();
        filterObject.add("entityId", filter.getEntityIdentifier());

        // Add field ordering if required.
        fieldOrdering = filter.getFieldOrdering();
        if (fieldOrdering != null)
        {
            JsonArray orderArray;

            orderArray = new JsonArray();
            for (String fieldIdentifier : fieldOrdering.keySet())
            {
                JsonObject orderObject;

                orderObject = new JsonObject();
                orderObject.add("fieldId", fieldIdentifier);
                orderObject.add("method", fieldOrdering.get(fieldIdentifier).toString());
                orderArray.add(orderObject);
            }

            filterObject.add("order", orderArray);
        }

        // Add the filter criteria if required.
        filterCriteria = filter.getFilterCriteria();
        if (filterCriteria != null)
        {
            JsonObject jsonCriteria;

            jsonCriteria = serializeFilterCriteria(filterCriteria, DataFilterConjunction.AND);
            filterObject.add("criteria", jsonCriteria.getJsonArray("criteria"));
        }

        // Add the list of fields.
        fieldArray = new JsonArray();
        for (String fieldIdentifier : filter.getFieldIdentifiers())
        {
            fieldArray.add(fieldIdentifier);
        }
        filterObject.add("fields", fieldArray);

        // Return the Json representation of the data filter.
        return filterObject;
    }

    protected JsonObject serializeFilterCriteria(T8DataFilterCriteria filterCriteria, DataFilterConjunction conjunction)
    {
        LinkedHashMap<T8DataFilterClause, DataFilterConjunction> filterClauses;
        JsonObject criteriaObject;
        JsonArray clauseArray;

        // Create the criteria object.
        criteriaObject = new JsonObject();
        criteriaObject.add("type", "@FILTER_CRITERIA");
        criteriaObject.add("conjunction", conjunction.toString());

        // Create the clause array.
        clauseArray = new JsonArray();
        filterClauses = filterCriteria.getFilterClauses();
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            clauseArray.add(serializeFilterClause(clause, filterClauses.get(clause)));
        }

        // Add the clauses to the criteria and return the object.
        criteriaObject.add("criteria", clauseArray);
        return criteriaObject;
    }

    protected JsonObject serializeFilterCriterion(T8DataFilterCriterion filterCriterion, DataFilterConjunction conjunction)
    {
        JsonObject criterionObject;

        // Create the criterion object.
        criterionObject = new JsonObject();
        criterionObject.add("type", "@FILTER_CRITERION");
        criterionObject.add("conjunction", conjunction.toString());
        criterionObject.add("fieldId", filterCriterion.getFieldIdentifier());
        criterionObject.add("operator", filterCriterion.getOperator().toString());
        criterionObject.add("value", filterCriterion.getFilterValue());
        return criterionObject;
    }

    protected JsonObject serializeFilterClause(T8DataFilterClause filterClause, DataFilterConjunction conjunction)
    {
        if (filterClause instanceof T8DataFilterCriteria)
        {
            return serializeFilterCriteria((T8DataFilterCriteria)filterClause, conjunction);
        }
        else if (filterClause instanceof T8DataFilterCriterion)
        {
            return serializeFilterCriterion((T8DataFilterCriterion)filterClause, conjunction);
        }
        else throw new IllegalArgumentException("Invalid filter clause type: " + filterClause);
    }

    public T8DataFilter deserializeDataFilter(JsonValue value)
    {
        if (value != null)
        {
            if (value instanceof JsonObject)
            {
                T8DataFilterCriteria filterCriteria;
                JsonArray fieldArray;
                JsonArray orderArray;
                JsonObject jsonFilter;
                T8DataFilter filter;
                String entityIdentifier;

                // Create the new data filter.
                jsonFilter = (JsonObject)value;
                entityIdentifier = jsonFilter.getString("entityId");
                filter = new T8DataFilter(entityIdentifier);

                // Add the specified ordering.
                orderArray = jsonFilter.getJsonArray("order");
                if (orderArray != null)
                {
                    for (JsonValue orderValue : orderArray)
                    {
                        JsonObject orderObject;
                        String fieldIdentifier;

                        orderObject = (JsonObject)orderValue;
                        fieldIdentifier = orderObject.getString("fieldId");
                        fieldIdentifier = T8IdentifierUtilities.setNamespace(fieldIdentifier, entityIdentifier);
                        filter.addFieldOrdering(fieldIdentifier, orderObject.getString("method"));
                    }
                }

                // Add the filter criteria.
                if (jsonFilter.containsName("criteria"))
                {
                    filterCriteria = (T8DataFilterCriteria)deserializeFilterClause(jsonFilter.getJsonObject("criteria"));
                    if (filterCriteria != null) filter.setFilterCriteria(filterCriteria);
                }

                // Add the filter fields.
                fieldArray = jsonFilter.getJsonArray("fields");
                if (fieldArray != null)
                {
                    for (JsonValue field : fieldArray)
                    {
                        filter.addFieldIdentifier(field.asString());
                    }
                }

                // Return the complete filter.
                return filter;
            }
            else throw new IllegalArgumentException("Input value is not a Data Filter object: " + value);
        }
        else return null;
    }

    protected T8DataFilterClause deserializeFilterClause(JsonObject jsonClause)
    {
        String type;

        type = jsonClause.getString("type");
        if (type != null)
        {
            if (type.equals("@FILTER_CRITERIA"))
            {
                return deserializeFilterCriteria(jsonClause);
            }
            else if (type.equals("@FILTER_CRITERION"))
            {
                return deserializeFilterCriterion(jsonClause);
            }
            else throw new RuntimeException("Invalid type '" + type + "' specification found for JSON filter clause: " + jsonClause);
        }
        else throw new RuntimeException("No type specification found for JSON filter clause: " + jsonClause);
    }

    protected T8DataFilterCriteria deserializeFilterCriteria(JsonObject jsonCriteria)
    {
        T8DataFilterCriteria criteria;
        JsonArray clauseArray;

        criteria = new T8DataFilterCriteria();
        clauseArray = jsonCriteria.getJsonArray("criteria");
        if (clauseArray != null)
        {
            for (JsonValue clauseValue : clauseArray)
            {
                T8DataFilterClause clause;
                DataFilterConjunction conjunction;
                JsonObject clauseObject;
                String conjunctionString;

                clauseObject = (JsonObject)clauseValue;
                conjunctionString = clauseObject.getString("conjunction");
                conjunction = Strings.isNullOrEmpty(conjunctionString) ? DataFilterConjunction.AND : DataFilterConjunction.valueOf(conjunctionString);
                clause = deserializeFilterClause(clauseObject);
                if (clause != null) criteria.addFilterClause(conjunction, clause);
            }
        }

        return criteria;
    }

    protected T8DataFilterCriterion deserializeFilterCriterion(JsonObject jsonCriterion)
    {
        String fieldIdentifier;
        DataFilterOperator operator;
        Object filterValue;

        fieldIdentifier = jsonCriterion.getString("fieldId");
        operator = DataFilterOperator.valueOf(jsonCriterion.getString("operator"));
        filterValue = jsonCriterion.getString("value");
        return new T8DataFilterCriterion(fieldIdentifier, operator, filterValue);
    }
}
