package com.pilog.t8.definition.project;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionTagDefinition;
import com.pilog.t8.definition.user.T8AdministratorUserDefinition;
import com.pilog.t8.definition.user.T8ClientUserDefinition;
import com.pilog.t8.definition.user.T8DeveloperUserDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_PROJECT";
    public static final String GROUP_IDENTIFIER = "@DG_PROJECT";
    public static final String DISPLAY_NAME = "Project";
    public static final String DESCRIPTION = "A project that groups all definitions that belong to a specific functional area.";
    public static final String STORAGE_PATH = "/projects";
    public static final String IDENTIFIER_PREFIX = "PROJECT_";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    private enum Datum {VERSION_ID,
                        OWNER_IDENTIFIERS,
                        DEVELOPER_IDENTIFIERS,
                        TAG_DEFINITIONS,
                        REQUIRED_PROJECT_IDS};
    // -------- Definition Meta-Data -------- //

    public T8ProjectDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.OWNER_IDENTIFIERS.toString(), "Project Owners", "The users registered as owners of this project.  Only project owners can modify project definition content."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.DEVELOPER_IDENTIFIERS.toString(), "Developers", "The users registered as developers on this project."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TAG_DEFINITIONS.toString(), "Tags", "The tags associated with this project.  All definitions belonging to this project will be able to use the tags in this list."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.REQUIRED_PROJECT_IDS.toString(), "Required Project", "The ids of projects on which this project is dependent."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        ArrayList<T8DefinitionDatumOption> datumOptions;

        if (Datum.OWNER_IDENTIFIERS.toString().equals(datumIdentifier) || Datum.DEVELOPER_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            datumOptions = createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8AdministratorUserDefinition.TYPE_IDENTIFIER));
            datumOptions.addAll(createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8DeveloperUserDefinition.TYPE_IDENTIFIER)));
            datumOptions.addAll(createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8ClientUserDefinition.TYPE_IDENTIFIER)));
            return datumOptions;
        }
        else if (Datum.TAG_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DefinitionTagDefinition.GROUP_IDENTIFIER));
        else if (Datum.REQUIRED_PROJECT_IDS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ProjectDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public List<String> getOwnerIdentifiers()
    {
        return getDefinitionDatum(Datum.OWNER_IDENTIFIERS);
    }

    public void setOwnerIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.OWNER_IDENTIFIERS, identifiers);
    }

    public List<String> getDeveloperIdentifiers()
    {
        return getDefinitionDatum(Datum.DEVELOPER_IDENTIFIERS);
    }

    public void setDeveloperIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DEVELOPER_IDENTIFIERS, identifiers);
    }

    public List<String> getRequiredProjectIds()
    {
        List<String> requiredProjectIds;

        requiredProjectIds = getDefinitionDatum(Datum.REQUIRED_PROJECT_IDS);
        return requiredProjectIds != null ? requiredProjectIds : new ArrayList<>();
    }

    public void setRequiredProjectIds(List<String> ids)
    {
        setDefinitionDatum(Datum.REQUIRED_PROJECT_IDS, ids);
    }

    public List<T8DefinitionTagDefinition> getTagDefinitions()
    {
        return getDefinitionDatum(Datum.TAG_DEFINITIONS);
    }

    public void setTagDefinitions(List<T8DefinitionTagDefinition> tagDefinitions)
    {
        setDefinitionDatum(Datum.TAG_DEFINITIONS, tagDefinitions);
    }
}
