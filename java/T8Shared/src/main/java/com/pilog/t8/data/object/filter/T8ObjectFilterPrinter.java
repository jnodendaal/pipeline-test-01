package com.pilog.t8.data.object.filter;

import com.pilog.t8.data.object.filter.T8ObjectFilterClause.ObjectFilterConjunction;
import com.pilog.t8.data.object.filter.T8ObjectFilterCriterion.ObjectFilterOperator;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

/**
 * @author Bouwer du Preez
 */
public class T8ObjectFilterPrinter
{
    private final PrintWriter writer;

    public T8ObjectFilterPrinter(OutputStream stream)
    {
        this.writer = new PrintWriter(stream);
    }

    public static void printStructure(OutputStream outputStream, T8ObjectFilter filter)
    {
        T8ObjectFilterPrinter printer;

        printer = new T8ObjectFilterPrinter(outputStream);
        printer.printStructure(filter, 0);
        printer.flush();
    }

    public static void printStructure(OutputStream outputStream, T8ObjectFilterCriteria filterCriteria)
    {
        T8ObjectFilterPrinter printer;

        printer = new T8ObjectFilterPrinter(outputStream);
        printer.printStructure(filterCriteria, 0);
        printer.flush();
    }

    public void flush()
    {
        writer.flush();
    }

    public void printStructure(T8ObjectFilter filter, int indent)
    {
        T8ObjectFilterCriteria filterCriteria;

        writer.println(createString("Object Filter: " + filter.getIdentifier(), indent));
        writer.println(createString("Field Ordering: " + filter.getFieldOrdering(), indent));

        filterCriteria = filter.getFilterCriteria();
        if (filterCriteria != null)
        {
            printStructure(filterCriteria, indent + 1);
        }
    }

    public void printStructure(T8ObjectFilterClause filterClause, int indent)
    {
        if (filterClause instanceof T8ObjectFilterCriteria) printStructure((T8ObjectFilterCriteria)filterClause, indent);
        else printStructure((T8ObjectFilterCriterion)filterClause, indent);
    }

    public void printStructure(T8ObjectFilterCriteria filterCriteria, int indent)
    {
        LinkedHashMap<T8ObjectFilterClause, ObjectFilterConjunction> clauses;

        clauses = filterCriteria.getFilterClauses();

        writer.println(createString("Filter Criteria: " + filterCriteria.getIdentifier(), indent));
        for (T8ObjectFilterClause filterClause : clauses.keySet())
        {
            writer.print(createString(clauses.get(filterClause) + " ", indent));
            printStructure(filterClause, indent + 1);
        }
    }

    public void printStructure(T8ObjectFilterCriterion filterCriterion, int indent)
    {
        ObjectFilterOperator operator;
        String fieldIdentifier;
        Object filterValue;

        fieldIdentifier = filterCriterion.getFieldIdentifier();
        operator = filterCriterion.getOperator();
        filterValue = filterCriterion.getFilterValue();

        if (filterValue instanceof T8ObjectFilter)
        {
            T8ObjectFilter subFilter;

            subFilter = (T8ObjectFilter)filterValue;
            writer.println(createString(fieldIdentifier + " " + operator, indent));
            printStructure(subFilter, indent + 1);
        }
        else
        {
            writer.println(createString(fieldIdentifier + " " + filterCriterion.getOperator() + " " + filterCriterion.getFilterValue(), indent));
        }
    }

    private StringBuffer createString(String inputString, int indent)
    {
        StringBuffer string;

        string = new StringBuffer();
        for (int i = 0; i < indent; i++)
        {
            string.append(" ");
        }

        string.append(inputString);
        return string;
    }
}
