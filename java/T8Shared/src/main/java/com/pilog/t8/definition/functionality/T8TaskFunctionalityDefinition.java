package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_TASK";
    public static final String DISPLAY_NAME = "Task Functionality";
    public static final String DESCRIPTION = "A functionality that provides access to a task to be completed as part of a workflow process.";
    public static final String IDENTIFIER_PREFIX = "FNC_TSK_";
    public enum Datum
    {
        COMPONENT_ID,
        COMPONENT_PARAMETER_MAPPING,
        COMPONENT_PARAMETER_EXPRESSION_MAP
    };
    // -------- Definition Meta-Data -------- //

    public T8TaskFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.COMPONENT_ID.toString(), "Component", "The id of the component that displays the task and handles completion."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.COMPONENT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Functionality to Component", "A mapping of Functionality Parameters to the corresponding Component Input Parameters.")
            .addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER,Datum.COMPONENT_ID.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP, Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString(), "Component Parameter Expression Map", "The parameters that will be used as inputs to the specified component (if any)."));
       return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.COMPONENT_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8NgComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.COMPONENT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String componentId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            componentId = getComponentId();
            if (componentId != null)
            {
                T8NgComponentDefinition componentDefinition;

                componentDefinition = (T8NgComponentDefinition)definitionContext.getRawDefinition(getRootProjectId(), componentId);
                if (componentDefinition != null)
                {
                    List<T8DataParameterDefinition> functionalityParameterDefinitions;
                    List<T8DataParameterDefinition> componentParameterDefinitions;
                    HashMap<String, List<String>> idMap;

                    functionalityParameterDefinitions = getInputParameterDefinitions();
                    componentParameterDefinitions = componentDefinition.getInputParameterDefinitions();

                    idMap = new HashMap<String, List<String>>();
                    if ((functionalityParameterDefinitions != null) && (componentParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition functionalityParameterDefinition : functionalityParameterDefinitions)
                        {
                            ArrayList<String> idList;

                            idList = new ArrayList<String>();
                            for (T8DataParameterDefinition componentParameterDefinition : componentParameterDefinitions)
                            {
                                idList.add(componentParameterDefinition.getPublicIdentifier());
                            }

                            idMap.put(functionalityParameterDefinition.getIdentifier(), idList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", idMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId))
        {
            String componentId;

            componentId = getComponentId();
            if (Strings.isNullOrEmpty(componentId))
            {
                return new ArrayList<T8DefinitionDatumOption>(0);
            }
            else
            {
                ArrayList<T8DefinitionDatumOption> optionList;
                T8NgComponentDefinition componentDefinition;
                Map<String, List<String>> ids;

                ids = new HashMap<String, List<String>>();
                optionList = new ArrayList<T8DefinitionDatumOption>(1);
                componentDefinition = (T8NgComponentDefinition)definitionContext.getRawDefinition(getRootProjectId(), componentId);
                for (T8DataParameterDefinition componentParameterDefinition : componentDefinition.getInputParameterDefinitions())
                {
                    ids.put(componentParameterDefinition.getPublicIdentifier(), null);
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", ids));
                return optionList;
            }
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if ((Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString().equals(datumId)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8TaskFunctionalityInstance").getConstructor(T8Context.class, T8TaskFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8TaskFunctionalityInstance").getConstructor(T8Context.class, T8TaskFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataParameterDefinition> parameterDefinitions;

        parameterDefinitions = super.getDefinitionDatum(T8FunctionalityDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString());
        return parameterDefinitions;
    }

    public String getComponentId()
    {
        return (String)getDefinitionDatum(Datum.COMPONENT_ID.toString());
    }

    public void setComponentId(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.COMPONENT_ID.toString(), moduleIdentifier);
    }

    public Map<String, String> getComponentParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.COMPONENT_PARAMETER_MAPPING.toString());
    }

    public void setComponentParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.COMPONENT_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getComponentParameterExpressionMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setComponentParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.COMPONENT_PARAMETER_EXPRESSION_MAP.toString(), expressionMap);
    }
}
