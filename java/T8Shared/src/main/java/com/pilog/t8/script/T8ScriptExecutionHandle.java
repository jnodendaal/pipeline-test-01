package com.pilog.t8.script;

/**
 * @author Hennie Brink
 */
public class T8ScriptExecutionHandle
{
    private boolean stopped;
    private boolean cancelled;
    private boolean paused;

    public T8ScriptExecutionHandle()
    {
        this.cancelled = false;
        this.paused = false;
    }

    public synchronized void checkPaused() throws InterruptedException
    {
        while(paused)
        {
            wait();
        }
    }

    public boolean isCancelled()
    {
        return cancelled;
    }

    public boolean isPaused()
    {
        return paused;
    }

    public boolean isStopped()
    {
        return stopped;
    }

    public void setCancelled(boolean canceled)
    {
        this.cancelled = canceled;
    }

    public synchronized void setPaused(boolean paused)
    {
        this.paused = paused;
        notifyAll();
    }

    public void setStopped(boolean stopped)
    {
        this.stopped = stopped;
    }
}
