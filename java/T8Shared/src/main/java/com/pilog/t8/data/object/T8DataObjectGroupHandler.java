package com.pilog.t8.data.object;

import com.pilog.t8.data.object.filter.T8ObjectFilter;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DataObjectGroupHandler
{
    public <O extends T8DataObject> List<O> search(T8ObjectFilter objectFilter, String searchExpression, int pageOffset, int pageSize) throws Exception;
}
