package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.time.T8Date;

/**
 * @author Bouwer du Preez
 */
public class T8DtDate extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATE";

    public T8DtDate()
    {
    }

    public T8DtDate(T8DefinitionManager context)
    {
    }

    public T8DtDate(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8Date.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        return JsonValue.valueOf(((T8Date)object).getMilliseconds());
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        return new T8Date(jsonValue.asLong());
    }
}
