package com.pilog.t8;

import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.event.T8FlowEventListener;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowManager extends T8ServerModule
{
    public enum T8TaskListRefreshType
    {
        FLOW_TYPE, // All tasks linked to specified flow types.
        FLOW_INSTANCE, // All tasks linked to specified flow instance.
        TASK_TYPE, // All tasks of specified type.
        TASK_INSTANCE // All specified task instances.
    }

    /**
     * Returns the active input identifiers for any specified node state within
     * an executing flow state.
     * @param context
     * @param flowInstanceIdentifier
     * @param nodeInstanceIdentifier
     * @return
     * @throws Exception
     */
    public List<String> findActiveInputNodeInstanceIdentifiers(T8Context context, String flowInstanceIdentifier, String nodeInstanceIdentifier) throws Exception;

    /**
     * Fires a BPMN Signal event across the entire flow context.
     * @param context
     * @param signalIdentifier
     * @param signalParameters
     */
    public void fireSignalEvent(T8Context context, String signalIdentifier, Map<String, Object> signalParameters);

    /**
     * Adds a flow listener to the controller.
     * @param listener
     */
    public void addFlowEventListener(T8Context context, T8FlowEventListener listener);

    /**
     * Removes a flow listener from the controller.
     * @param listener
     */
    public void removeFlowEventListener(T8Context context, T8FlowEventListener listener);

    /**
     * Admin Only:  Attempts to retry the execution of a flow node that is
     * currently in a failure state.  The result of this method depends on the
     * type of node and the type of issue that resulted in its failure.
     * @param context
     * @param flowInstanceIdentifier The instance identifier of the flow to
     * retry.
     * @param nodeInstanceIdentifier The instance identifier of the node to
     * retry.
     */
    public void retryExecution(T8Context context, String flowInstanceIdentifier, String nodeInstanceIdentifier);

    /**
     * Returns the total number of flows currently active and executing on the server.
     * @param context
     * @return
     */
    public int getFlowCount(T8Context context);

    /**
     * Returns the total number of flows currently cached in the server.
     * @param context
     * @return
     */
    public int getCachedFlowCount(T8Context context);

    /**
     * Clears all non-executing flows from the server cache.
     * @param context
     * @return The number of flows cleared from cache.
     */
    public int clearFlowCache(T8Context context);

    /**
     * Returns the number of the task types that are available within the specified session context, using the specified task filter.
     * @param context
     * @param taskFilter
     * @return
     * @throws Exception
     */
    public int getTaskCount(T8Context context, T8TaskFilter taskFilter) throws Exception;

    /**
     * Returns a summary of the task types and counts that are available within
     * the specified session context, filtered according to the specified task
     * filter.
     *
     * @param context
     * @param taskFilter
     * @return
     * @throws Exception
     */
    public T8TaskListSummary getTaskListSummary(T8Context context, T8TaskFilter taskFilter) throws Exception;

    /**
     * Returns a list of all flow tasks that are available within the specified session context (using the specified task filter).
     * @param context
     * @param taskFilter
     * @return The task list containing task in the specified page.
     * @throws Exception
     */
    public T8TaskList getTaskList(T8Context context, T8TaskFilter taskFilter) throws Exception;

    /**
     * Retrieves the task details for the specified task
     * @param context The session context of the current user
     * @param taskInstanceIdentifier The task instance identifier for which the task details will be retrieved
     * @return The task details for the specified task
     * @throws Exception
     */
    public T8TaskDetails getTaskDetails(T8Context context, String taskInstanceIdentifier) throws Exception;

    /**
     * Retrieves the task details for the specified task
     * @param context The session context of the current user
     * @param taskIid The instance identifier of the task for which UI data will be returned.
     * @return The UI data for the specified task.
     * @throws Exception
     */
    public Map<String, Object> getTaskUiData(T8Context context, String taskIid) throws Exception;

    /**
     * Sets the priority of the task, the task priority will be used to sort tasks according to priority.
     * @param context The session context of the user
     * @param taskInstanceIdentifier The instance identifier for the task for which the priority will be set
     * @param priority The new priority that will be set for the task
     * @throws java.lang.Exception
     */
    public void setTaskPriority(T8Context context, String taskInstanceIdentifier, int priority) throws Exception;

    /**
     * Decreases the task priority is by the specified amount, the minimum priority for a task is 0, and cannot be set to any lower.
     * @param context The session context of the current user
     * @param taskInstanceIdentifier The instance identifier of the task
     * @param amount The amount by which to decrease the task priority
     * @throws Exception
     */
    public void decreaseTaskPriority(T8Context context, String taskInstanceIdentifier, int amount) throws Exception;

    /**
     * Increases the task priority is by the specified amount
     * @param context The session context of the current user
     * @param taskInstanceIdentifier The instance identifier of the task
     * @param amount The amount by which to increase the task priority
     * @throws Exception
     */
    public void increaseTaskPriority(T8Context context, String taskInstanceIdentifier, int amount) throws Exception;

    /**
     * Reassigns the specified task to a new user and workflow profile.
     * @param context
     * @param taskIid
     * @param userId
     * @param profileId
     * @throws Exception
     */
    public void reassignTask(T8Context context, String taskIid, String userId, String profileId) throws Exception;

    /**
     * Claims the specified task from the list of available tasks for the specified session.  Once claimed a task cannot be executed in a different session.
     * @param context
     * @param taskIid
     * @throws Exception
     */
    public void claimTask(T8Context context, String taskIid) throws Exception;

    /**
     * Un-claims a task so that it is returned to the list of available tasks.
     * @param context
     * @param taskIid
     * @throws Exception
     */
    public void unclaimTask(T8Context context, String taskIid) throws Exception;

    /**
     * Completes the specified flow task, allowing to the flow engine to continue processing of subsequent nodes.
     * @param context
     * @param taskIid
     * @param outputParameters
     * @return
     * @throws Exception
     */
    public T8TaskCompletionResult completeTask(T8Context context, String taskIid, Map<String, Object> outputParameters) throws Exception;

    /**
     * Returns the current state of the specified executing flow.
     * @param context
     * @param flowIid
     * @return
     * @throws Exception
     */
    public T8FlowState getFlowState(T8Context context, String flowIid) throws Exception;

    /**
     * Returns the status of a currently executing flow.
     * @param context
     * @param flowIid
     * @param includeDisplayData
     * @return The requested flow status.
     * @throws Exception
     */
    public T8FlowStatus getFlowStatus(T8Context context, String flowIid, boolean includeDisplayData) throws Exception;

    /**
     * Loads the specified flow definition, creates a new instance of the flow from the definition and queues its execution.
     * @param context
     * @param flowIdentifier
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public T8FlowStatus queueFlow(T8Context context, String flowIdentifier, Map<String, Object> inputParameters) throws Exception;

    /**
     * Loads the specified flow definition, creates a new instance of the flow from the definition and starts its execution at the first node.
     * @param context
     * @param flowId
     * @param inputParameters
     * @return
     * @throws Exception
     */
    public T8FlowStatus startFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception;

    /**
     * Kills the specified flow and all descendent and ancestors of it.  All related flows are removed from the list of active flows.  All related flow states are discarded.
     * @param context
     * @param flowIid
     * @throws Exception
     */
    public void killFlow(T8Context context, String flowIid) throws Exception;

    /**
     * Refreshes the task list for all tasks that are currently available and comply with the specified details.
     * @param context
     * @param refreshType
     * @param ids
     * @throws Exception
     */
    public void refreshTaskList(T8Context context, T8TaskListRefreshType refreshType, List<String> ids) throws Exception;

    /**
     * Reloads the task escalation trigger cache.
     * @param context
     * @throws Exception
     */
    public void recacheTaskEscalationTriggers(T8Context context) throws Exception;
}
