package com.pilog.t8.ui;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentUtilities
{
    public static List<T8Component> getDescendentComponents(T8Component component)
    {
        ArrayList<T8Component> descendents;
        List<T8Component> childComponents;
        
        descendents = new ArrayList<T8Component>();
        childComponents = component.getChildComponents();
        if (childComponents != null)
        {
            descendents.addAll(childComponents);
            for (T8Component childComponent : childComponents)
            {
                descendents.addAll(getDescendentComponents(childComponent));
            }
        }
        
        return descendents;
    }
}
