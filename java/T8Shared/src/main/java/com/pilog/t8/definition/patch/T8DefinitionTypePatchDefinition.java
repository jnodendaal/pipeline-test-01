package com.pilog.t8.definition.patch;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTypePatchDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PATCH_DEFINITION_TYPE";
    public static final String TYPE_IDENTIFIER = "@DT_PATCH_DEFINITION_TYPE";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "PATCH_DEF_TYPE_";
    public static final String DISPLAY_NAME = "T8 Definition Type Patch";
    public static final String DESCRIPTION = "A definition containing settings for updating an existing definition type to a new format.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    public enum Datum {DEFINITION_TYPE_IDENTIFIER,
                       NEW_DEFINITION_TYPE_IDENTIFIER,
                       DEFINITION_DATUM_MAP};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionTypePatchDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_TYPE_IDENTIFIER, Datum.DEFINITION_TYPE_IDENTIFIER.toString(), "Type Identifier",  "The existing definition type to which this patch is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_TYPE_IDENTIFIER, Datum.NEW_DEFINITION_TYPE_IDENTIFIER.toString(), "New Type Identifier",  "The new definition type identifier to use in the place of the old one."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.STRING), Datum.DEFINITION_DATUM_MAP.toString(), "Datum Map: Old Datum Identifier to New Datum Identifier",  "A map of old datum identifiers and the new one they will be replaced with."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getDefinitionTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIER.toString());
    }

    public void setDefinitionTypeIdentifier(String typeIdentifier)
    {
        setDefinitionDatum(Datum.DEFINITION_TYPE_IDENTIFIER.toString(), typeIdentifier);
    }

    public String getNewDefinitionTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.NEW_DEFINITION_TYPE_IDENTIFIER.toString());
    }

    public void setNewDefinitionTypeIdentifier(String typeIdentifier)
    {
        setDefinitionDatum(Datum.NEW_DEFINITION_TYPE_IDENTIFIER.toString(), typeIdentifier);
    }

    public Map<String, String> getDefinitionDatumMap()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.DEFINITION_DATUM_MAP.toString());
    }

    public void setDefinitionDatumMap(Map<String, String> datumMap)
    {
        setDefinitionDatum(Datum.DEFINITION_DATUM_MAP.toString(), datumMap);
    }
}

