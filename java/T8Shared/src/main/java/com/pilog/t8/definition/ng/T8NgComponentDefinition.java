package com.pilog.t8.definition.ng;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NgComponentDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_NG_COMPONENT";
    public static final String GROUP_IDENTIFIER = "@DG_NG_COMPONENT";
    public static final String GROUP_NAME = "Angular Components";
    public static final String GROUP_DESCRIPTION = "Configuration of all Angular HTML components.";
    public static final String STORAGE_PATH = "/angular_components";
    public static final String DISPLAY_NAME = "Angular Component";
    public static final String DESCRIPTION = "An Angular HTML component displayed as part of the user interface.";
    public static final String IDENTIFIER_PREFIX = "NG_";
    public enum Datum
    {
        COMPONENT_URI,
        INPUT_PARAMETER_DEFINITIONS,
        INITIALIZATION_SCRIPT_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public T8NgComponentDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.COMPONENT_URI.toString(), "Component URI",  "The URI of the Angular Component."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this Angular Component."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.INITIALIZATION_SCRIPT_DEFINITION.toString(), "Component Initialization Script", "The script that is used to compile the Angular Component parameters (using the input parameters)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.INITIALIZATION_SCRIPT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8NgComponentInitializationScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else if (Datum.INITIALIZATION_SCRIPT_DEFINITION.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getComponentUri()
    {
        return getDefinitionDatum(Datum.COMPONENT_URI);
    }

    public void setComponentUri(String uri)
    {
        setDefinitionDatum(Datum.COMPONENT_URI, uri);
    }

    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public T8NgComponentInitializationScriptDefinition getComponentInitializationScriptDefinition()
    {
        return getDefinitionDatum(Datum.INITIALIZATION_SCRIPT_DEFINITION);
    }

    public void setComponentInitializationScriptDefinition(T8NgComponentInitializationScriptDefinition scriptDefinition)
    {
        setDefinitionDatum(Datum.INITIALIZATION_SCRIPT_DEFINITION, scriptDefinition);
    }

    public Map<String, Object> executeComponentInitializationScript(T8Context context, Map<String, Object> inputParameters) throws Exception
    {
        T8ServerContextScriptDefinition scriptDefinition;

        // Execute the script.
        scriptDefinition = getComponentInitializationScriptDefinition();
        if (scriptDefinition != null)
        {
            T8Script script;
            Object outputObject;

            // Get a new instance of the script and execute it.
            script = scriptDefinition.getNewScriptInstance(context);
            outputObject = script.executeScript(inputParameters);

            // Process the script output.
            if (outputObject == null)
            {
                // We have to return a valid map to incdicate successful execution completion.
                return new HashMap<String, Object>();
            }
            else if (outputObject instanceof Map)
            {
                return T8IdentifierUtilities.stripNamespace((Map<String, Object>)outputObject);
            }
            else throw new RuntimeException("Component initialization script '" + this + "' did not return a valida Map object as output.");
        }
        else return new HashMap<>();
    }
}
