package com.pilog.t8.definition.data.source.table;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.T8RefreshableDataSource;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.source.T8RefreshableDataSourceDefinition;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8RefreshableTableDataSourceDefinition extends T8TableDataSourceDefinition implements T8RefreshableDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATABASE_TABLE_REFRESHABLE";
    public static final String DISPLAY_NAME = "Database Table Data Source (Refreshable)";
    public static final String DESCRIPTION = "A data source that reads data from a database table, and can also be refreshed using an insert into statement.";
    public enum Datum
    {
        INSERT_INTO_STATEMENT,
        SQL_REFRESH_SCRIPT,
        SQL_PARAMETER_EXPRESSION_MAP,
        REFRESH_PARAMETER_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8RefreshableTableDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REFRESH_PARAMETER_DEFINITIONS.toString(), "Refresh Parameters", "The definitions of the parameters that may be used when this data source is refreshed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INSERT_INTO_STATEMENT.toString(), "Insert Into Statement", "This statement will be called when the refresh action on this table is called."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SQL_REFRESH_SCRIPT.toString(), "SQL Refresh Script", "This SQL script that will be executed when this data source is refreshed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION_VALUE_MAP, Datum.SQL_PARAMETER_EXPRESSION_MAP.toString(), "SQL Parameter Expressions", "The parameters used in the SQL Strings of this data source and their corresponding value expressions."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REFRESH_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.REFRESH_PARAMETER_DEFINITIONS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else if (((Datum.INSERT_INTO_STATEMENT.toString().equals(datumId))) || ((Datum.SQL_REFRESH_SCRIPT.toString().equals(datumId))))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.sql.T8SQLDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor) constructor.newInstance(definitionContext, this, getDatumType(datumId));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else
        {
            return super.getDatumEditor(definitionContext, datumId);
        }
    }

    @Override
    public T8RefreshableDataSource getNewDataSourceInstance(T8DataTransaction accessProvider)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.source.table.T8RefreshableTableDataSource").getConstructor(T8RefreshableTableDataSourceDefinition.class, T8DataTransaction.class);
            return (T8RefreshableDataSource) constructor.newInstance(this, accessProvider);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getRefreshParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.REFRESH_PARAMETER_DEFINITIONS.toString());
    }

    public void setRefreshParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.REFRESH_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }

    public String getSqlRefreshScript()
    {
        return (String) getDefinitionDatum(Datum.SQL_REFRESH_SCRIPT.toString());
    }

    public void setSqlRefreshScript(String script)
    {
        setDefinitionDatum(Datum.SQL_REFRESH_SCRIPT.toString(), script);
    }

    public Map<String, String> getSQLParameterExpressionMap()
    {
        return (Map<String, String>) getDefinitionDatum(Datum.SQL_PARAMETER_EXPRESSION_MAP.toString());
    }

    public void setSQLParameterExpressionMap(Map<String, String> expressionMap)
    {
        setDefinitionDatum(Datum.SQL_PARAMETER_EXPRESSION_MAP.toString(), expressionMap);
    }
}
