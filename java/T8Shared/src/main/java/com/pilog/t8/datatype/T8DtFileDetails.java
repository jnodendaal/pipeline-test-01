package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.file.T8FileDetails;

/**
 * @author Bouwer du Preez
 */
public class T8DtFileDetails extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FILE_DETAILS";

    public T8DtFileDetails() {}

    public T8DtFileDetails(T8DefinitionManager context) {}

    public T8DtFileDetails(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FileDetails.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject jsonFileDetails;
            T8FileDetails fileDetails;

            // Create the JSON object.
            fileDetails = (T8FileDetails)object;
            jsonFileDetails = new JsonObject();
            jsonFileDetails.add("contextIid", fileDetails.getContextIid());
            jsonFileDetails.add("contextId", fileDetails.getContextId());
            jsonFileDetails.add("filepath", fileDetails.getFilePath());
            jsonFileDetails.add("size", fileDetails.getFileSize());
            jsonFileDetails.addIfNotNull("mediaType", fileDetails.getMediaType());
            jsonFileDetails.addIfNotNull("md5Checksum", fileDetails.getMD5Checksum());

            // Return the final JSON object.
            return jsonFileDetails;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8FileDetails deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8FileDetails fileDetails;
            JsonObject jsonFileDetails;

            // Create the file details object.
            jsonFileDetails = jsonValue.asObject();
            fileDetails = new T8FileDetails();
            fileDetails.setContextIid(jsonFileDetails.getString("contextIid"));
            fileDetails.setContextIid(jsonFileDetails.getString("contextId"));
            fileDetails.setContextIid(jsonFileDetails.getString("filepath"));
            fileDetails.setFileSize(jsonFileDetails.getLong("size", -1));
            fileDetails.setMediaType(jsonFileDetails.getString("mediaType"));
            fileDetails.setMD5Checksum(jsonFileDetails.getString("md5Checksum"));

            // Return the completed object.
            return fileDetails;
        }
        else return null;
    }
}
