package com.pilog.t8.definition.data;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8DataParameterDefinition extends T8DataValueDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_PARAMETER";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_PARAMETER";
    public static final String IDENTIFIER_PREFIX = "P_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Parameter";
    public static final String DESCRIPTION = "A parameter used as input or output for a particular operation.";
    public enum Datum {OPTIONAL};
    // -------- Definition Meta-Data -------- //

    public T8DataParameterDefinition(String identifier)
    {
        super(identifier);
    }

    public T8DataParameterDefinition(String valueIdentifier, String displayName, String valueDescription, T8DataType dataType)
    {
        this(valueIdentifier, displayName, valueDescription, dataType, false);
    }

    public T8DataParameterDefinition(String valueIdentifier, String displayName, String valueDescription, T8DataType dataType, boolean optional)
    {
        this(valueIdentifier);
        setMetaDescription(valueDescription);
        setMetaDisplayName(displayName);
        setDataType(dataType);
        setOptional(optional);
    }

    public T8DataParameterDefinition(String valueIdentifier, String displayName, String valueDescription, String dataType, boolean optional)
    {
        this(valueIdentifier);
        setMetaDescription(valueDescription);
        setMetaDisplayName(displayName);
        setDataType(dataType);
        setOptional(optional);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPTIONAL.toString(), "Optional", "Whether or not the parameter is optional."));

        return datumTypes;
    }

    public boolean isOptional()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.OPTIONAL));
    }

    public void setOptional(boolean optional)
    {
        setDefinitionDatum(Datum.OPTIONAL, optional);
    }
}
