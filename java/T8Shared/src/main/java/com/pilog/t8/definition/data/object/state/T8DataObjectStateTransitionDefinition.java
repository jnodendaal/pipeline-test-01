package com.pilog.t8.definition.data.object.state;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.object.T8DataObjectStateTransition;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectStateTransitionDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_STATE_TRANSITION";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT_STATE_TRANSITION";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "O_STATE_TRANSITION_";
    public static final String DISPLAY_NAME = "State Transition";
    public static final String DESCRIPTION = "An object containing defined field values.";
    public enum Datum
    {
        FUNCTIONALITY_ID,
        STATE_ID,
        CONDITION_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public T8DataObjectStateTransitionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_ID.toString(), "Functionality", "The functionality that can be executed in order to apply this transition."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.STATE_ID.toString(), "State", "The state that is transitioned to."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "A conditional check of the functionality output parameters to determine applicability of this transition."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.STATE_ID.toString().equals(datumId))
        {
            T8DataObjectStateGraphDefinition graphDefinition;
            T8DataObjectStateDefinition stateDefinition;
            List<String> stateIds;

            graphDefinition = (T8DataObjectStateGraphDefinition)getAncestorDefinition(T8DataObjectStateGraphDefinition.TYPE_IDENTIFIER);
            stateDefinition = (T8DataObjectStateDefinition)getParentDefinition();
            stateIds = graphDefinition.getChildNodeIdentifiers(stateDefinition.getIdentifier());
            stateIds.add(stateDefinition.getIdentifier());
            return createStringOptions(stateIds);
        }
        else if (Datum.FUNCTIONALITY_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8DataObjectStateTransition getNewStateTransition()
    {
        return new T8DataObjectStateTransition(getFunctionalityId(), getStateId(), getConditionExpression());
    }

    public String getStateId()
    {
        return getDefinitionDatum(Datum.STATE_ID.toString());
    }

    public void setStateId(String id)
    {
        setDefinitionDatum(Datum.STATE_ID, id);
    }

    public String getFunctionalityId()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_ID);
    }

    public void setFunctionalityId(String id)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_ID, id);
    }

    public String getConditionExpression()
    {
        return getDefinitionDatum(Datum.CONDITION_EXPRESSION);
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION, expression);
    }
}
