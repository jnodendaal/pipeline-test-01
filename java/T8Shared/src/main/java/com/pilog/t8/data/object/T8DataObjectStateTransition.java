package com.pilog.t8.data.object;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectStateTransition implements Serializable
{
    private final String conditionExpression;
    private final String functionalityId;
    private final String stateId;

    public T8DataObjectStateTransition(String functionalityId, String stateId, String conditionExpression)
    {
        this.functionalityId = functionalityId;
        this.stateId = stateId;
        this.conditionExpression = conditionExpression;
    }

    public T8DataObjectStateTransition copy()
    {
        return new T8DataObjectStateTransition(functionalityId, stateId, conditionExpression);
    }

    public String getConditionExpression()
    {
        return conditionExpression;
    }

    public String getFunctionalityId()
    {
        return functionalityId;
    }

    public String getStateId()
    {
        return stateId;
    }
}
