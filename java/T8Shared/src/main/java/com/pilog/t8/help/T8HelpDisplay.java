/**
 * Created on 04 May 2015, 3:32:33 PM
 */
package com.pilog.t8.help;

import com.pilog.t8.definition.help.T8HelpDisplayDefinition;

/**
 * @author Gavin Boshoff
 */
public abstract class T8HelpDisplay
{
    private final T8HelpDisplayDefinition definition;

    public T8HelpDisplay(T8HelpDisplayDefinition definition)
    {
        this.definition = definition;
    }

    public abstract void display() throws Exception;
}