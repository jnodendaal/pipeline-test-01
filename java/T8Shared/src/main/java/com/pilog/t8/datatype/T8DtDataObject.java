package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataObject extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_OBJECT";

    public T8DtDataObject() {}

    public T8DtDataObject(T8DefinitionManager context) {}

    public T8DtDataObject(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataObject.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            Map<String, Object> fieldValues;
            JsonObject jsonObject;
            T8DataObject dataObject;

            // Create the concept JSON object.
            dataObject = (T8DataObject)object;
            jsonObject = new JsonObject();
            jsonObject.add("id", dataObject.getId());

            // Add the object fields.
            fieldValues = T8IdentifierUtilities.stripNamespace(dataObject.getFieldValues());
            if ((fieldValues != null) && (!fieldValues.isEmpty()))
            {
                T8DtUndefined dtUndefined;
                JsonArray jsonFieldArray;

                dtUndefined = new T8DtUndefined();
                jsonFieldArray = new JsonArray();
                jsonObject.add("fields", jsonFieldArray);
                for (String fieldId : fieldValues.keySet())
                {
                    JsonObject jsonField;

                    jsonField = new JsonObject();
                    jsonField.add("id", fieldId);
                    jsonField.add("name", dataObject.getFieldDisplayName(fieldId));
                    jsonField.add("description", dataObject.getFieldDisplayDescription(fieldId));
                    jsonField.add("sequence", dataObject.getFieldDisplaySequence(fieldId));
                    jsonField.add("priority", dataObject.getFieldDisplayPriority(fieldId));
                    jsonField.add("value", dtUndefined.serialize(fieldValues.get(fieldId)));
                    jsonFieldArray.add(jsonField);
                }
            }

            // Return the final json object.
            return jsonObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataObject deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DataObject dataObject;
            JsonObject jsonObject;

            // Get the JSON values.
            jsonObject = jsonValue.asObject();

            // Return the completed object.
            return null;
        }
        else return null;
    }
}