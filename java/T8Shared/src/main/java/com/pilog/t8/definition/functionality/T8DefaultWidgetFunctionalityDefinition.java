package com.pilog.t8.definition.functionality;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import java.util.ArrayList;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8DefaultWidgetFunctionalityDefinition extends T8ModuleFunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_MODULE_WIDGET";
    public static final String DISPLAY_NAME = "Module Widget Functionality";
    public static final String DESCRIPTION = "A Functionality that creates a widget from a specific module.";
    public static final String IDENTIFIER_PREFIX = "FNC_MW_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8DefaultWidgetFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8ModuleFunctionalityDefinition.Datum.MODULE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), "@DT_UI_COMPONENT_MODULE_WIDGET"));
        else return super.getDatumOptions(definitionContext, datumIdentifier); //To change body of generated methods, choose Tools | Templates.
    }
}
