package com.pilog.t8.data.model;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.T8DataModel;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityRelationshipDefinition;
import com.pilog.t8.definition.data.model.T8DataModelDefinition;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataModel implements T8DataModel
{
    private T8DataModelDefinition definition;
    private T8DataEntityStructure entityStructure;
    
    public T8DefaultDataModel(T8DataModelDefinition definition)
    {
        this.definition = definition;
        this.entityStructure = new T8DataEntityStructure();
        constructEntityStructure();
    }
    
    private void constructEntityStructure()
    {
        // Construct the entity structure.
        for (T8DataEntityRelationshipDefinition nextDefinition : definition.getDataEntityRelationshipDefinitions())
        {
            T8DataEntityStructureNode contextNode;
            T8DataEntityStructureNode relatedNode;
            Map<String, String> fieldRelationships;
            String contextIdentifier;
            String relatedIdentifier;
            
            contextIdentifier = nextDefinition.getContextEntityIdentifier();
            relatedIdentifier = nextDefinition.getRelatedEntityIdentifier();
            fieldRelationships = nextDefinition.getFieldRelationships();
            
            // Try to find an existing context node and also see if the related node is one of the existing base nodes.
            contextNode = entityStructure.getNode(contextIdentifier);
            relatedNode = entityStructure.getBaseEntityNode(relatedIdentifier);
            if (contextNode != null)
            {
                // Add a new related node to the context node that was found.
                contextNode.addChildNode(new T8DataEntityStructureNode(relatedIdentifier), fieldRelationships);
            }
            else if (relatedNode != null)
            {
                T8DataEntityStructureNode newBaseNode;
                
                // Add a new base node, remove the existing base node and add it to the new base node as a child.
                newBaseNode = new T8DataEntityStructureNode(contextIdentifier);
                entityStructure.addBaseEntityNode(newBaseNode);
                entityStructure.removeBaseEntityNode(relatedNode);
                newBaseNode.addChildNode(relatedNode, fieldRelationships);
            }
            else
            {
                T8DataEntityStructureNode newBaseNode;
                T8DataEntityStructureNode newRelatedNode;
                
                newBaseNode = new T8DataEntityStructureNode(contextIdentifier);
                newRelatedNode = new T8DataEntityStructureNode(relatedIdentifier);
                newBaseNode.addChildNode(newRelatedNode, fieldRelationships);
                entityStructure.addBaseEntityNode(newBaseNode);
            }
        }
        
        // Log the results.
        T8Log.log("Entity Structure Constructed: " + entityStructure.getSize());
    }

    @Override
    public int deleteCascade(T8DataTransaction dataAccessProvider, T8DataEntity entity) throws Exception
    {
        T8DataEntityStructureNode entityNode;
        String entityIdentifier;
        
        entityIdentifier = entity.getIdentifier();
        entityNode = entityStructure.getNode(entityIdentifier);
        if (entityNode != null)
        {
            int deletionCount;
            
            // Delete all child entities.
            deletionCount = 0;
            for (T8DataEntityStructureNode childNode : entityNode.getChildNodes())
            {
                Map<String, String> fieldRelationships;
                T8DataIterator<T8DataEntity> iterator;
                String childEntityIdentifier;
                T8DataFilter childFilter;
                
                // Get the child entity identifier and create a filter to use for deletion of all child entities.
                childEntityIdentifier = childNode.getID();
                fieldRelationships = entityNode.getChildFieldRelationships(childEntityIdentifier);
                childFilter = new T8DataFilter(childEntityIdentifier, T8IdentifierUtilities.mapParameters(entity.getFieldValues(), fieldRelationships));

                // Get an iterator over all of the child entities.
                iterator = dataAccessProvider.iterate(childEntityIdentifier, childFilter);
                if (iterator != null)
                {
                    // Delete all entities returned by the iterator recursively using this method.
                    while (iterator.hasNext())
                    {
                        deletionCount += deleteCascade(dataAccessProvider, iterator.next());
                    }
                    
                    // Make sure to close the iterator.
                    iterator.close();
                }
            }
                
            // Now delete this entity.
            dataAccessProvider.delete(entity);
            deletionCount++;
            return deletionCount;
        }
        else
        {
            if (dataAccessProvider.delete(entity)) return 1;
            else return 0;
        }
    }
}
