package com.pilog.t8.definition.notification;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICNotificationParameterDefinition extends T8DataParameterDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_PARAMETER_EPIC_NOTIFICATION";
    public static final String DISPLAY_NAME = "EPIC Notification Data Parameter";
    public static final String DESCRIPTION = "A content data parameter used by an EPIC notification.";
    public enum Datum {DISPLAY_NAME};
    // -------- Definition Meta-Data -------- //
    
    public T8EPICNotificationParameterDefinition(String identifier)
    {
        super(identifier);
    }
    
    public T8EPICNotificationParameterDefinition(String valueIdentifier, String displayName, String valueDescription, T8DataType dataType)
    {
        this(valueIdentifier);
        setMetaDescription(valueDescription);
        setMetaDisplayName(displayName);
        setDataType(dataType);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The name of this parameter to be used when displayed on the UI."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        // Invoke the super implementation.
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }
    
    public String getDisplayName()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_NAME.toString());
    }
    
    public void setDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME.toString(), displayName);
    }
}
