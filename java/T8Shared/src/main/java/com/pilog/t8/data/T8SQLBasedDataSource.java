package com.pilog.t8.data;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;

/**
 * @author Bouwer du Preez
 */
public interface T8SQLBasedDataSource extends T8DataSource
{
    /**
     * Generates an SQL WITH clause to be used when querying this data source.
     * @param entityDefinition
     * @param filter The filter to be used for the query.
     * @param pageOffset The page offset of the required query results.
     * @param pageSize The page size of the required query results (-1 indicates no limit).
     * @return The WITH clause to be used for the specified query.
     * @throws java.lang.Exception
     */
     public ParameterizedString getWithClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize) throws Exception;

     public ParameterizedString getSelectClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize) throws Exception;

     /**
      * Generates an SQL query string to be used when querying this data source.
      * @param entityDefinition The definition of the entity to be retrieved.
      * @param filter The filter to be used for the query.
      * @param includeWithClause
      * @param pageOffset The page offset of the required query results.
      * @param pageSize The page size of the required query results (-1 indicates no limit).
      * @return The SQL query string to be used for the specified query.
      * @throws Exception
      */
     public ParameterizedString getQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause, int pageOffset, int pageSize) throws Exception;
     public ParameterizedString getCountQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause) throws Exception;
}
