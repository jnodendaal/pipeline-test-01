package com.pilog.t8.time;

/**
 * @author Bouwer du Preez
 */
public class T8Day implements T8TimeUnit
{
    private final long amount;

    public T8Day(long amount)
    {
        this.amount = amount;
    }

    @Override
    public long getMilliseconds()
    {
        return 86400000 * amount;
    }

    public long getAmount()
    {
        return amount;
    }
}
