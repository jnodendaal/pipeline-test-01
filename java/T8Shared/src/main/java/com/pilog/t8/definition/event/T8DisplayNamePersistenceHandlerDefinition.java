package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DisplayNamePersistenceHandlerDefinition extends T8DefinitionEventHandlerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_EVENT_HANDLER_DISPLAY_NAME_PERSISTENCE";
    public static final String DISPLAY_NAME = "Display Name Persistence Handler";
    public static final String DESCRIPTION = "A definition that defines a handler that will persist the display names of definitions to a data entity.";
    public enum Datum {DATA_ENTITY_IDENTIFIER,
                       DEFINITION_GROUP_IDENTIFIERS};
    // -------- Definition Meta-Data -------- //

    public T8DisplayNamePersistenceHandlerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity to which the display names will be persisted."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.DEFINITION_GROUP_IDENTIFIERS.toString(), "Definition Groups", "The definition groups for which display names will be persisted."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DEFINITION_GROUP_IDENTIFIERS.toString().equals(datumIdentifier)) return createStringOptions(definitionContext.getDefinitionGroupIdentifiers());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionEventHandler getNewEventHandlerInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.definition.event.T8DisplayNamePersistenceHandler").getConstructor(T8Context.class, T8DisplayNamePersistenceHandlerDefinition.class);
        return (T8DefinitionEventHandler)constructor.newInstance(context, this);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public List<String> getDefinitionGroupIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.DEFINITION_GROUP_IDENTIFIERS.toString());
    }

    public void setDefinitionGroupIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DEFINITION_GROUP_IDENTIFIERS.toString(), identifiers);
    }
}

