/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.constraint;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8DataValidationException extends RuntimeException
{
    private Object value;

    public T8DataValidationException(Object value, String message)
    {
        super(message);
        this.value = value;
    }

    /**
     * @return the value
     */
    public Object getValue()
    {
        return value;
    }

}
