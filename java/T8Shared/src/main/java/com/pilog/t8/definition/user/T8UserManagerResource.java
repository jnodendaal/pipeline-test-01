package com.pilog.t8.definition.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8UserManagerResource implements T8DefinitionResource
{
    public static final String OPERATION_CHANGE_PROFILE_PICTURE = "@OP_USR_CHANGE_PROFILE_PICTURE";

    public static final String PARAMETER_FILE_CONTEXT_IID = "$P_FILE_CONTEXT_IID";
    public static final String PARAMETER_FILE_PATH = "$P_FILE_PATH";
    public static final String PARAMETER_PICTURE_URL = "$P_PICTURE_URL";
    public static final String PARAMETER_THUMBNAIL_URL = "$P_THUMBNAIL_URL";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIds)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIds.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_CHANGE_PROFILE_PICTURE);
        definition.setMetaDisplayName("Change User Profile Picture");
        definition.setMetaDescription("Changes the current user's profile picture to the one at the specified file location.");
        definition.setClassName("com.pilog.t8.user.T8UserManagerOperations$ApiChangeUserProfilePicture");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_CONTEXT_IID, "File Context Iid", "The id of the file context instance where the new profile picture is located.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_PATH, "File Path", "The path (including filename) of the new profile picture in the specified file context instance.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PICTURE_URL, "Picture Url", "The url (relative to base file access url) where the new profile picture can be accessed.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_THUMBNAIL_URL, "Picture Url", "The url (relative to base file access url) where the new profile picture can be accessed.", T8DataType.STRING));
        definitions.add(definition);

        return definitions;
    }
}
