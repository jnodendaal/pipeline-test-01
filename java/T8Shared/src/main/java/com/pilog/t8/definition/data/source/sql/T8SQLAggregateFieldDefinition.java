package com.pilog.t8.definition.data.source.sql;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8SQLAggregateFieldDefinition extends T8DataSourceFieldDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_FIELD_AGGREGATE";
    public static final String DISPLAY_NAME = "Aggregate Function Field";
    public static final String DESCRIPTION = "A Definition to define a sql aggregate function.";
    public static final String IDENTIFIER_PREFIX = "SQL_AGGR_";

    public enum Datum {
        AGGREGATE_FUNCTION
    };
    // -------- Definition Meta-Data -------- //

    public T8SQLAggregateFieldDefinition(String identifier)
    {
        super(identifier);
    }

    public T8SQLAggregateFieldDefinition(String identifier, String sourceIdentifier, boolean key, boolean required, T8DataType dataType)
    {
        super(identifier, sourceIdentifier, key, required, dataType);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AGGREGATE_FUNCTION.toString(), "Aggregate String", "The aggregate that will be performed."));

        return datumTypes;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings); //To change body of generated methods, choose Tools | Templates.

        if (Strings.isNullOrEmpty(getAggregateFunctionString()))
        {
            throw new Exception("Aggregate function not provided in definition " + getPublicIdentifier());
        }
    }

    public String getAggregateFunctionString()
    {
        return getDefinitionDatum(Datum.AGGREGATE_FUNCTION);
    }

    public void setAggregateFunctionString(String aggregateFunctionString)
    {
        setDefinitionDatum(Datum.AGGREGATE_FUNCTION, aggregateFunctionString);
    }
}
