package com.pilog.t8.definition.gfx.image;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Image;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ImageDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_GFX_IMAGE";
    public static final String GROUP_NAME = "Images";
    public static final String GROUP_DESCRIPTION = "Definitions of images used by the vairous user interfaces in the system.";
    public static final String STORAGE_PATH = "/image_definitions";
    public static final String IDENTIFIER_PREFIX = "IMG_";
    // -------- Definition Meta-Data -------- //

    public T8ImageDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return new ArrayList<T8DefinitionDatumType>();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public abstract T8Image getImage();
}
