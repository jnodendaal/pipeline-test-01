package com.pilog.t8.definition.file.context;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityFileContextDefinition extends T8FileContextDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FILE_CONTEXT_DATA_ENTITY";
    public static final String DISPLAY_NAME = "Data Entity File Context";
    public static final String DESCRIPTION = "A file context that stores files in a T8 Data Entity.";
    public enum Datum {ENTITY_ID,
                       FILE_ID_FIELD_ID,
                       FILE_NAME_FIELD_ID,
                       FILE_DATA_FIELD_ID};
    // -------- Definition Meta-Data -------- //

    public T8DataEntityFileContextDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ENTITY_ID.toString(), "Data Entity", "The Data entity that will be used to retrieve the attachments."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_ID_FIELD_ID.toString(), "File Id", "The field on the entity that will be used for the file Id."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_NAME_FIELD_ID.toString(), "File Name", "The field on the entity that will be used for the file name."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_DATA_FIELD_ID.toString(), "File Data", "The field on the entity that stores all the file data."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ENTITY_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if ((Datum.FILE_ID_FIELD_ID.toString().equals(datumIdentifier))
                || (Datum.FILE_NAME_FIELD_ID.toString().equals(datumIdentifier))
                || (Datum.FILE_DATA_FIELD_ID.toString().equals(datumIdentifier)))
        {
            String dataEntityId;

            dataEntityId = this.getEntityId();
            if (dataEntityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), dataEntityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return null;
            }
            else return null;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8FileContext createNewFileContextInstance(T8Context context, T8FileManager fileManager, String contextIid) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.file.context.T8DataEntityFileContext").getConstructor(T8Context.class, T8FileManager.class, this.getClass(), String.class);
        return (T8FileContext)constructor.newInstance(context, fileManager, this, contextIid);
    }

    public String getEntityId()
    {
        return (String)getDefinitionDatum(Datum.ENTITY_ID.toString());
    }

    public void setEntityId(String entityId)
    {
        setDefinitionDatum(Datum.ENTITY_ID.toString(), entityId);
    }

    public String getFileDataFieldId()
    {
        return getDefinitionDatum(Datum.FILE_DATA_FIELD_ID);
    }

    public void setFileDataFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.FILE_DATA_FIELD_ID, fieldId);
    }

    public String getFileNameFieldId()
    {
        return getDefinitionDatum(Datum.FILE_NAME_FIELD_ID);
    }

    public void setFileNameFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.FILE_NAME_FIELD_ID, fieldId);
    }

    public String getFileIdFieldId()
    {
        return getDefinitionDatum(Datum.FILE_ID_FIELD_ID);
    }

    public void setFileIdFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.FILE_ID_FIELD_ID, fieldId);
    }
}
