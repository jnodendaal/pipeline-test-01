package com.pilog.t8.time;

/**
 * @author Bouwer du Preez
 */
public class T8Year implements T8TimeUnit
{
    private final long amount;

    public T8Year(long amount)
    {
        this.amount = amount;
    }

    @Override
    public long getMilliseconds()
    {
        return 31536000000l * amount;
    }

    public long getAmount()
    {
        return amount;
    }
}
