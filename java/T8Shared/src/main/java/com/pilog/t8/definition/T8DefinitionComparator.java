package com.pilog.t8.definition;

import com.pilog.t8.definition.T8Definition;
import java.util.Comparator;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionComparator implements Comparator
{
    private CompareMethod method;
    private CompareType type;
    private String datumIdentifier;
    
    public enum CompareMethod {LEXICOGRAPHIC};
    public enum CompareType {IDENTIFIER, DISPLAY_NAME, DESCRIPTION, DATUM};
    
    public T8DefinitionComparator()
    {
        this(CompareMethod.LEXICOGRAPHIC, CompareType.IDENTIFIER);
    }
    public T8DefinitionComparator(CompareMethod method, CompareType type)
    {
        this.method = method;
        this.type = type;
        this.datumIdentifier = null;
    }
    
    public T8DefinitionComparator(CompareMethod method, String datumIdentifier)
    {
        this.method = method;
        this.type = CompareType.DATUM;
        this.datumIdentifier = datumIdentifier;
    }
    
    @Override
    public int compare(Object o1, Object o2)
    {
        T8Definition definition1;
        T8Definition definition2;
        
        definition1 = (T8Definition)o1;
        definition2 = (T8Definition)o2;
        if (type == CompareType.IDENTIFIER)
        {
            String value1;
            String value2;
            
            value1 = definition1.getIdentifier();
            value2 = definition2.getIdentifier();
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                return value1.compareTo(value2);
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else if (type == CompareType.DISPLAY_NAME)
        {
            String value1;
            String value2;
            
            value1 = definition1.getMetaDisplayName();
            value2 = definition2.getMetaDisplayName();
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                return value1.compareTo(value2);
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else if (type == CompareType.DESCRIPTION)
        {
            String value1;
            String value2;
            
            value1 = definition1.getMetaDescription();
            value2 = definition2.getMetaDescription();
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                return value1.compareTo(value2);
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else if (type == CompareType.DATUM)
        {
            Object value1;
            Object value2;
            
            value1 = definition1.getDefinitionDatum(datumIdentifier);
            value2 = definition2.getDefinitionDatum(datumIdentifier);
            if (method == CompareMethod.LEXICOGRAPHIC)
            {
                if (value1 == null) return -1;
                else if (value2 == null) return 1;
                else return value1.toString().compareTo(value2.toString());
            }
            else throw new RuntimeException("No valid compare method set: " + method);
        }
        else throw new RuntimeException("No valid compare type set: " + type);
    }
}
