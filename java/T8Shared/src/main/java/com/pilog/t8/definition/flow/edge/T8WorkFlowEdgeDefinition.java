package com.pilog.t8.definition.flow.edge;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.graph.T8GraphEdgeDefinition;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowEdgeDefinition extends T8GraphEdgeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_WORK_EDGE";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_WORK_EDGE";
    public static final String DISPLAY_NAME = "Work Flow Edge";
    public static final String DESCRIPTION = "A an edge that connects two work flow nodes.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowEdgeDefinition(String identifier)
    {
        super(identifier);
    }

    public T8WorkFlowEdgeDefinition(String identifier, String parentIdentifier, String childIdentifier)
    {
        this(identifier);
        setParentNodeIdentifier(parentIdentifier);
        setChildNodeIdentifier(childIdentifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return super.getDatumTypes();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
}
