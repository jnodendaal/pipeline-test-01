package com.pilog.t8.time;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bouwer du Preez
 */
public class T8Time implements Serializable
{
    private static DateFormat format = new SimpleDateFormat("hh:mm:ss.SSS");
    private long milliseconds;

    public T8Time(long milliseconds)
    {
        this.milliseconds = milliseconds;
    }

    public long getMilliseconds()
    {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds)
    {
        this.milliseconds = milliseconds;
    }

    @Override
    public String toString()
    {
        return format.format(new Date(milliseconds));
    }
}
