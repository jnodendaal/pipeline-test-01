package com.pilog.t8.definition.data;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataValueDefinition extends T8Definition
{
    public enum Datum
    {
        DATA_TYPE
    };

    private T8DataType dataType;

    public T8DataValueDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_TYPE.toString(), "Data Type", "The data type of this value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String dataTypeString;

        dataTypeString = getDataTypeString();
        if (dataTypeString != null)
        {
            T8DefinitionManager definitionManager;

            definitionManager = context.getServerContext().getDefinitionManager();
            dataType = definitionManager.createDataType(dataTypeString);
        }
        else dataType = null;
    }

    public T8DataType getDataType()
    {
        return dataType;
    }

    public String getDataTypeString()
    {
        return (String)getDefinitionDatum(Datum.DATA_TYPE.toString());
    }

    public void setDataType(String dataType)
    {
        setDefinitionDatum(Datum.DATA_TYPE.toString(), dataType);
    }

    public void setDataType(T8DataType dataType)
    {
        setDefinitionDatum(Datum.DATA_TYPE.toString(), dataType != null ? dataType.getDataTypeStringRepresentation(true) : null);
    }
}
