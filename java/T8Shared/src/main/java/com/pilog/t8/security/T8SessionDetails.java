package com.pilog.t8.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8SessionDetails implements Serializable
{
    private String sessionId;
    private String userId;
    private String userName;
    private String userSurname;
    private long loginTime;
    private long lastActivityTime;
    private long expirationTime;
    private boolean performanceStatisticsEnabled;
    private T8UserProfileDetails userProfileDetails;
    private final List<T8WorkflowProfileDetails> workflowProfileDetails;

    public T8SessionDetails()
    {
        this.performanceStatisticsEnabled = false;
        this.workflowProfileDetails = new ArrayList<>();
    }

    public T8SessionDetails(String sessionId)
    {
        this();
        this.sessionId = sessionId;
    }

    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(long lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public long getLoginTime()
    {
        return loginTime;
    }

    public void setLoginTime(long loginTime)
    {
        this.loginTime = loginTime;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public boolean isPerformanceStatisticsEnabled()
    {
        return performanceStatisticsEnabled;
    }

    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        this.performanceStatisticsEnabled = enabled;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserSurname()
    {
        return userSurname;
    }

    public void setUserSurname(String userSurname)
    {
        this.userSurname = userSurname;
    }

    public String getUserProfileId()
    {
        return userProfileDetails != null ? userProfileDetails.getId() : null;
    }

    public void setUserProfileId(String userProfileId)
    {
        this.userProfileDetails = new T8UserProfileDetails(userProfileId, null, null);
    }

    public T8UserProfileDetails getUserProfileDetails()
    {
        return this.userProfileDetails;
    }

    public void setUserProfileDetails(T8UserProfileDetails profileDetails)
    {
        this.userProfileDetails = profileDetails;
    }

    public List<T8WorkflowProfileDetails> getWorkflowProfileDetails()
    {
        return new ArrayList<>(this.workflowProfileDetails);
    }

    public List<String> getWorkFlowProfileIds()
    {
        List<String> profileIds;

        profileIds = new ArrayList<>();
        for (T8WorkflowProfileDetails profileDetails : this.workflowProfileDetails)
        {
            profileIds.add(profileDetails.getId());
        }

        return profileIds;
    }

    public void setWorkflowProfileIds(List<String> profileIds)
    {
        this.workflowProfileDetails.clear();
        for (String profileId : profileIds)
        {
            addWorkflowProfileDetails(new T8WorkflowProfileDetails(profileId, null, null));
        }
    }

    public void addWorkflowProfileDetails(T8WorkflowProfileDetails details)
    {
        this.workflowProfileDetails.add(details);
    }
}
