package com.pilog.t8.definition.script;

import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleOperationScriptCompletionHandler extends T8DefaultModuleCompletionHandler
{
    public T8ModuleOperationScriptCompletionHandler(T8Context context, T8ModuleOperationScriptDefinition definition)
    {
        super(context, definition);
        System.out.println("GBO - T8ModuleOperationScriptCompletionHandler... ");
    }
}
