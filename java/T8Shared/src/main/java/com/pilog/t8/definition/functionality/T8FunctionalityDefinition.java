package com.pilog.t8.definition.functionality;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8Image;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8FunctionalityDataObjectDefinition;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.definition.script.T8DataObjectValidationScriptDefinition;
import com.pilog.t8.definition.security.T8DataAccessDefinition;
import com.pilog.t8.functionality.T8FunctionalityParameterHandle;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FunctionalityDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FUNCTIONALITY";
    public static final String GROUP_NAME = "Functionalities";
    public static final String GROUP_DESCRIPTION = "Functionalities are the access points to system features, made available to users based on the rights their profiles provide.";
    public static final String DISPLAY_NAME = "System Functionality";
    public static final String DESCRIPTION = "A functionality available in a T8 System.";
    public static final String IDENTIFIER_PREFIX = "FUNC_";
    public static final String STORAGE_PATH = "/functionalities";
    public enum Datum
    {
        DISPLAY_NAME_EXPRESSION,
        DESCRIPTION,
        INPUT_PARAMETER_DEFINITIONS,
        OUTPUT_PARAMETER_DEFINITIONS,
        DATA_OBJECTS,
        TARGET_OBJECT_VALIDATION_SCRIPT_DEFINITION,
        VALIDATION_SCRIPT_DEFINITION,
        TARGET_OBJECT_NOT_FOUND_MESSAGE,
        CONFIRMATION_MESSAGE,
        ICON_IDENTIFIER,
        ICON_URI,
        ACTIVE,
        OPERATION_IDS,
        DATA_ACCESS
    };
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;

    public T8FunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DISPLAY_NAME_EXPRESSION.toString(), "Display Name Expression", "The expression that is evaluated to determine the display name of this functionality."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DESCRIPTION.toString(), "Description", "The text that describes this functionality and which can be used for display on the UI."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The data parameters that must be supplied as input when this functionality is accessed."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters",  "The data parameters that are supplied as output when this functionality completes."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATA_OBJECTS.toString(), "Data Objects", "The objects involved in this functionality."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this functionality when displayed on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ICON_URI.toString(), "Icon URI",  "The URI of the icon denoting this functionality."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "Flags this functionality as active/inactive.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.TARGET_OBJECT_VALIDATION_SCRIPT_DEFINITION.toString(), "Target Object Validation Script", "The script that will be executed in order to validate the supplied target object.  This script is supposed to only perform light weight operations."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.VALIDATION_SCRIPT_DEFINITION.toString(), "Validation Script", "The script that will be executed to perform validation checks that are more heavy weight than the target object checks."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TARGET_OBJECT_NOT_FOUND_MESSAGE.toString(), "Target Object Not Found Message", "An error message that is displayed to the user if the functionality is invoked but no target object is provided."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.CONFIRMATION_MESSAGE.toString(), "Confirmation Message", "If specified, this simple Yes/No confirmation message will be prompted to the user before the functionality is accessed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_LIST, Datum.OPERATION_IDS.toString(), "Operations", "The operations accessible from this functionality on the target data object."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DATA_ACCESS.toString(), "Data Access", "The data access rights granted when accessing this functionality."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.ICON_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else if (Datum.DATA_OBJECTS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FunctionalityDataObjectDefinition.TYPE_IDENTIFIER));
        else if (Datum.TARGET_OBJECT_VALIDATION_SCRIPT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataObjectValidationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.VALIDATION_SCRIPT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataObjectValidationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.OPERATION_IDS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_ACCESS.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataAccessDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumId)) return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        else if (Datum.DATA_OBJECTS.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getIconId();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    public T8FunctionalityHandle getNewFunctionalityHandle(T8Context context) throws Exception
    {
        String displayNameExpression;
        T8FunctionalityHandle handle;

        // Create the new handle.
        handle = new T8FunctionalityHandle(getIdentifier());
        handle.setDisplayName(getIdentifier());
        handle.setDescription(getFunctionalityDescription());
        handle.setIcon(getIcon());
        handle.setIconUri(getIconUri());
        handle.setConfirmationMessage(getConfirmationMessage());

        // Resolve the display name.
        displayNameExpression = getDisplayNameExpression();
        if (!Strings.isNullOrEmpty(displayNameExpression))
        {
            try
            {
                T8ExpressionEvaluator expressionEvaluator;

                expressionEvaluator = context.getExpressionEvaluator();
                handle.setDisplayName((String)expressionEvaluator.evaluateExpression(displayNameExpression, null, null));
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to evaluate functionality '" + getIdentifier() + "' display name expression: " + displayNameExpression, ex);
            }
        }

        // Add the functionality object details.
        for (T8DataParameterDefinition parameterDefinition : getInputParameterDefinitions())
        {
            T8FunctionalityParameterHandle parameterHandle;
            T8FunctionalityDataObjectDefinition objectDefinition;
            String parameterId;

            parameterId = parameterDefinition.getIdentifier();
            parameterHandle = new T8FunctionalityParameterHandle();
            parameterHandle.setParameterId(parameterId);

            objectDefinition = getObjectDefinitionByParameterId(parameterId);
            if (objectDefinition != null)
            {
                parameterHandle.setDataObjectId(objectDefinition.getDataObjectId());
                parameterHandle.setRequired(objectDefinition.isRequired());
            }

            handle.addParameterHandle(parameterHandle);
        }

        // Return the new handle.
        return handle;
    }

    public abstract T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception;
    public abstract T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception;

    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setInputParameterDefinitions(List<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public List<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(List<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }

    public String getDisplayNameExpression()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_NAME_EXPRESSION.toString());
    }

    public void setDisplayNameExpression(String expression)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME_EXPRESSION.toString(), expression);
    }

    public String getFunctionalityDescription()
    {
        return (String)getDefinitionDatum(Datum.DESCRIPTION.toString());
    }

    public void setFunctionalityDescription(String text)
    {
        setDefinitionDatum(Datum.DESCRIPTION.toString(), text);
    }

    public List<T8FunctionalityDataObjectDefinition> getDataObjects()
    {
        return getDefinitionDatum(Datum.DATA_OBJECTS);
    }

    public void setDataObjects(List<T8FunctionalityDataObjectDefinition> objects)
    {
        setDefinitionDatum(Datum.DATA_OBJECTS, objects);
    }

    public List<String> getObjectIds()
    {
        List<String> objectIds;

        objectIds = new ArrayList<>();
        for (T8FunctionalityDataObjectDefinition object : getDataObjects())
        {
            objectIds.add(object.getDataObjectId());
        }

        return objectIds;
    }

    public List<String> getRequiredObjectIds()
    {
        List<String> objectIds;

        objectIds = new ArrayList<>();
        for (T8FunctionalityDataObjectDefinition object : getDataObjects())
        {
            if (object.isRequired())
            {
                objectIds.add(object.getDataObjectId());
            }
        }

        return objectIds;
    }

    public T8FunctionalityDataObjectDefinition getObjectDefinitionByParameterId(String parameterId)
    {
        for (T8FunctionalityDataObjectDefinition object : getDataObjects())
        {
            if (parameterId.equals(object.getFunctionalityInputParameterId()))
            {
                return object;
            }
        }

        return null;
    }

    public String getIconId()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconId(String id)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), id);
    }

    public String getIconUri()
    {
        return getDefinitionDatum(Datum.ICON_URI);
    }

    public void setIconUri(String uri)
    {
        setDefinitionDatum(Datum.ICON_URI, uri);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }

    public ImageIcon getIcon()
    {
        if (iconDefinition != null)
        {
            T8Image iconImage;

            iconImage = iconDefinition.getImage();
            return iconImage != null ? iconImage.getImageIcon() : null;
        }
        else return null;
    }

    public boolean isActive()
    {
        Boolean active;

        active = (Boolean)getDefinitionDatum(Datum.ACTIVE.toString());
        return (active != null) && active;
    }

    public void setActive(boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE.toString(), active);
    }

    public String getTargetObjectNotFoundMessage()
    {
        return (String)getDefinitionDatum(Datum.TARGET_OBJECT_NOT_FOUND_MESSAGE.toString());
    }

    public void setTargetObjectNotFoundMessage(String message)
    {
        setDefinitionDatum(Datum.TARGET_OBJECT_NOT_FOUND_MESSAGE.toString(), message);
    }

    public String getConfirmationMessage()
    {
        return (String)getDefinitionDatum(Datum.CONFIRMATION_MESSAGE.toString());
    }

    public void setConfirmationMessage(String message)
    {
        setDefinitionDatum(Datum.CONFIRMATION_MESSAGE.toString(), message);
    }

    public T8DataObjectValidationScriptDefinition getTargetObjectValidationScriptDefinition()
    {
        return (T8DataObjectValidationScriptDefinition)getDefinitionDatum(Datum.TARGET_OBJECT_VALIDATION_SCRIPT_DEFINITION.toString());
    }

    public void setTargetObjectValidationScriptDefinition(T8DataObjectValidationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.TARGET_OBJECT_VALIDATION_SCRIPT_DEFINITION.toString(), definition);
    }

    public T8DataObjectValidationScriptDefinition getValidationScriptDefinition()
    {
        return (T8DataObjectValidationScriptDefinition)getDefinitionDatum(Datum.VALIDATION_SCRIPT_DEFINITION.toString());
    }

    public void setValidationScriptDefinition(T8DataObjectValidationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.VALIDATION_SCRIPT_DEFINITION.toString(), definition);
    }

    public List<String> getOperationIds()
    {
        List<String> ids;

        ids = getDefinitionDatum(Datum.OPERATION_IDS);
        return ids != null ? ids : new ArrayList<>();
    }

    public void setOperationIds(List<String> operationIds)
    {
        setDefinitionDatum(Datum.OPERATION_IDS, operationIds);
    }

    public T8DataAccessDefinition getDataAccessDefinition()
    {
        return getDefinitionDatum(Datum.DATA_ACCESS);
    }

    public void setDataAccessDefinition(T8DataAccessDefinition definition)
    {
        setDefinitionDatum(Datum.DATA_ACCESS, definition);
    }
}
