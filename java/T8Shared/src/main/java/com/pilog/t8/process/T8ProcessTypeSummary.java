package com.pilog.t8.process;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessTypeSummary implements Serializable
{
    private String processId;
    private String name;
    private String description;
    private int totalCount;
    private int executingCount;
    
    public T8ProcessTypeSummary(String taskIdentifier)
    {
        this.processId = taskIdentifier;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setTaskIdentifier(String taskIdentifier)
    {
        this.processId = taskIdentifier;
    }

    public String getDisplayName()
    {
        return name;
    }

    public void setDisplayName(String displayName)
    {
        this.name = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public int getExecutingCount()
    {
        return executingCount;
    }

    public void setExecutingCount(int executingCount)
    {
        this.executingCount = executingCount;
    }
}
