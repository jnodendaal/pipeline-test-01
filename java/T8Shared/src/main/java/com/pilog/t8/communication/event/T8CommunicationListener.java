package com.pilog.t8.communication.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationListener extends EventListener
{
    public void messageQueued(T8MessageQueuedEvent event);
    public void messageSent(T8MessageSentEvent event);
    public void messageFailed(T8MessageFailedEvent event);
    public void messageCancelled(T8MessageCancelledEvent event);
}
