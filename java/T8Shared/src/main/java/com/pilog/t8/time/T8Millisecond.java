package com.pilog.t8.time;

/**
 * @author Bouwer du Preez
 */
public class T8Millisecond implements T8TimeUnit
{
    private final long amount;

    public T8Millisecond(long amount)
    {
        this.amount = amount;
    }

    @Override
    public long getMilliseconds()
    {
        return amount;
    }

    public long getAmount()
    {
        return amount;
    }
}
