package com.pilog.t8.definition.data.source.table;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8TableDataSourceDefinition extends T8DataSourceDefinition implements T8SQLQueryDataSourceDefinition, T8DataConnectionBasedSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATABASE_TABLE";
    public static final String DISPLAY_NAME = "Database Table Data Source";
    public static final String DESCRIPTION = "A data source that reads data from a database table.";
    public static final String VERSION = "0";
    public enum Datum {CONNECTION_IDENTIFIER,
                       TABLE_NAME,
                       AUTO_GENERATE};
    // -------- Definition Meta-Data -------- //

    public T8TableDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection to which this data source will connect to retrieve its data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TABLE_NAME.toString(), "Table Name", "The table name from which this data source will extract data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_GENERATE.toString(), "Auto Generate", "If this setting is enabled, the table will be automatically generated and checked for consistency every time the framework starts up.", false));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction dataAccessProvider) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.source.table.T8TableDataSource").getConstructor(T8TableDataSourceDefinition.class, T8DataTransaction.class);
        return (T8DataSource)constructor.newInstance(this, dataAccessProvider);
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return getConnectionIdentifier();
    }

    public String getConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString());
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString(), connectionIdentifier);
    }

    public String getTableName()
    {
        return (String)getDefinitionDatum(Datum.TABLE_NAME.toString());
    }

    public void setTableName(String tableName)
    {
        setDefinitionDatum(Datum.TABLE_NAME.toString(), tableName);
    }

    public boolean isAutoGenerate()
    {
        Boolean autoGenerate;

        autoGenerate = (Boolean)getDefinitionDatum(Datum.AUTO_GENERATE.toString());
        return ((autoGenerate != null) && autoGenerate);
    }

    public void setAutoGenerate(boolean autoGenerate)
    {
        setDefinitionDatum(Datum.AUTO_GENERATE.toString(), autoGenerate);
    }

    @Override
    public StringBuilder createQuery(T8DataTransaction dataAccessProvider, T8DataFilter dataFilter, Map<String, String> fieldMapping)
    {
        StringBuilder subQuery;

        subQuery = new StringBuilder();
        subQuery.append("SELECT ");
        if ((fieldMapping != null) && (fieldMapping.size() > 0))
        {
            T8DataEntityDefinition dataEntityDefinition;
            T8DataSourceDefinition dataSourceDefinition;
            Iterator<String> fieldIterator;

            fieldIterator = fieldMapping.keySet().iterator();
            while (fieldIterator.hasNext())
            {
                String fieldIdentifier;
                String fieldSourceIdentifier;

                fieldIdentifier = fieldIterator.next();
                dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier));
                dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
                fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
                subQuery.append(fieldSourceIdentifier);
                subQuery.append(" AS ");

                fieldIdentifier = fieldMapping.get(fieldIdentifier);
                dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier));
                dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
                fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
                subQuery.append(fieldSourceIdentifier);

                if (fieldIterator.hasNext()) subQuery.append(", ");
            }
        }
        else // No Field mapping so just retrieve all fields using original identifiers.
        {
            subQuery.append("*");
        }

        subQuery.append(" FROM ");
        subQuery.append(getTableName());
        if ((dataFilter != null) && (dataFilter.hasFilterCriteria()))
        {
            subQuery.append(" ");
            subQuery.append(dataFilter.getWhereClause(dataAccessProvider, getTableName()));
        }

        return subQuery;
    }
}
