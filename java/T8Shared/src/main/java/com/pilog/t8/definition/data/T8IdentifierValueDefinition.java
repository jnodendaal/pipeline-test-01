package com.pilog.t8.definition.data;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8IdentifierValueDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_IDENTIFIER_VALUE";
    public static final String TYPE_IDENTIFIER = "@DT_IDENTIFIER_VALUE";
    public static final String IDENTIFIER_PREFIX = "V_";
    public static final String STORAGE_PATH = "/identifier_values";
    public static final String DISPLAY_NAME = "Identifier Values";
    public static final String DESCRIPTION = "A definition of an Identifier parameter value that can be globally referenced.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //
    
    public T8IdentifierValueDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
}
