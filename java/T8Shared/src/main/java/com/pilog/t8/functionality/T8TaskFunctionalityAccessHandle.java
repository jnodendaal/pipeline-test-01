package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8TaskFunctionalityAccessHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final Map<String, Object> componentParameters;
    private String flowIid;
    private String taskIid;
    private String componentUri;
    private String iconUri;

    public T8TaskFunctionalityAccessHandle(String functionalityId, String functionalityIid, String displayName, String description, Icon icon)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.componentParameters = new HashMap<>();
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        return null;
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return false;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String flowIid)
    {
        this.flowIid = flowIid;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public void setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
    }

    public String getComponentUri()
    {
        return componentUri;
    }

    public void setComponentUri(String componentUri)
    {
        this.componentUri = componentUri;
    }

    public String getIconUri()
    {
        return iconUri;
    }

    public void setIconUri(String iconUri)
    {
        this.iconUri = iconUri;
    }

    public Map<String, Object> getComponentParameters()
    {
        return new HashMap<>(componentParameters);
    }

    public void setComponentParameters(Map<String, Object> parameters)
    {
        this.componentParameters.clear();
        if (parameters != null)
        {
            this.componentParameters.putAll(parameters);
        }
    }
}
