package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.functionality.T8NgComponentFunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8TaskFunctionalityAccessHandle;
import com.pilog.t8.functionality.T8WorkflowFunctionalityAccessHandle;

/**
 * @author Bouwer du Preez
 */
public class T8DtFunctionalityAccessHandle extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@FUNC_ACCESS_HANDLE";
    private T8DefinitionManager context;
    
    public T8DtFunctionalityAccessHandle(T8DefinitionManager definitionManager)
    {
        this.context = definitionManager;
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8FunctionalityAccessHandle.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof T8NgComponentFunctionalityAccessHandle)
            {
                return new T8DtNgComponentAccessHandle(context).serialize(object);
            }
            else if (object instanceof T8TaskFunctionalityAccessHandle)
            {
                return new T8DtTaskAccessHandle().serialize(object);
            }
            else if (object instanceof T8WorkflowFunctionalityAccessHandle)
            {
                return new T8DtWorkflowAccessHandle().serialize(object);
            }
            else throw new IllegalArgumentException("Type expected: " + getDataTypeClassName() + " Type found: " + object.getClass().getCanonicalName());
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}