package com.pilog.t8.data;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DataEntityResultListener extends EventListener
{
    public void entityRetrieved();
    public void entityResultsClosed();
}
