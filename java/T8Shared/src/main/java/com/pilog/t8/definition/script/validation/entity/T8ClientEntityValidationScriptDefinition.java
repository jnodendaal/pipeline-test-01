package com.pilog.t8.definition.script.validation.entity;

/**
 * @author Bouwer du Preez
 */
public class T8ClientEntityValidationScriptDefinition extends T8EntityValidationScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_CLIENT_ENTITY_VALIDATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_CLIENT_ENTITY_VALIDATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Client Entity Validation Script";
    public static final String DESCRIPTION = "A script that is executed on the client-side to validate a data entity.";
    public static final String VERSION = "0";
    public enum Datum {DATA_ENTITY_IDENTIFIER};
    // -------- Definition Meta-Data -------- //
    
    public T8ClientEntityValidationScriptDefinition(String identifier)
    {
        super(identifier);
    }
}
