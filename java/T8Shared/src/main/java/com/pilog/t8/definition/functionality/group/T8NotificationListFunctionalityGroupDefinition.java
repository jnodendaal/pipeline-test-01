package com.pilog.t8.definition.functionality.group;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.notification.T8NotificationSummary;
import com.pilog.t8.notification.T8NotificationTypeSummary;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectFieldDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.notification.T8NotificationDefinition;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8NotificationListFunctionalityGroupDefinition extends T8DefaultFunctionalityGroupDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_GROUP_SYSTEM_NOTIFICATION_LIST";
    public static final String DISPLAY_NAME = "Notification List Functionality Group";
    public static final String DESCRIPTION = "A grouping used to access notification list functionalities.";
    public static final String IDENTIFIER_PREFIX = "FNC_GRP_";
    public enum Datum
    {
        FUNCTIONALITY_IDENTIFIER,
        TARGET_OBJECT_IDENTIFIER,
        NOTIFICATION_TYPE_FIELD_IDENTIFIER,
        INCLUDE_NEW_NOTIFICATIONS_FIELD_IDENTIFIER,
        INCLUDE_RECEIVED_NOTIFICATIONS_FIELD_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    private T8DataObjectDefinition targetObjectDefinition;

    public T8NotificationListFunctionalityGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_IDENTIFIER.toString(), "Functionality", "The identifier of the module that will be displayed when one of the functionalities in this group is invoked. This module takes responsibility for display of the selected notification type list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_OBJECT_IDENTIFIER.toString(), "Target Object", "The identifier of the target object that will be created for execution of the functionality."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NOTIFICATION_TYPE_FIELD_IDENTIFIER.toString(), "Notification Type Field", "The identifier of the target object field to which the notification type identifier will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INCLUDE_NEW_NOTIFICATIONS_FIELD_IDENTIFIER.toString(), "'Include New Notifications' Field", "The identifier of the target object field to which the boolean flag 'Include New Notifications' (indicating whether or not new notifications should be included) will be sent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INCLUDE_RECEIVED_NOTIFICATIONS_FIELD_IDENTIFIER.toString(), "'Include Received Notifications' Field", "The identifier of the target object field to which the boolean flag 'Include Received Notifications' (indicating whether or not received notifications should be included) will be sent."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TARGET_OBJECT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if ((Datum.NOTIFICATION_TYPE_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
                || (Datum.INCLUDE_NEW_NOTIFICATIONS_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
                || (Datum.INCLUDE_RECEIVED_NOTIFICATIONS_FIELD_IDENTIFIER.toString().equals(datumIdentifier)))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String targetObjectId;

            optionList = new ArrayList<>();
            optionList.add(new T8DefinitionDatumOption("No mapping.", null));

            targetObjectId = getTargetObjectIdentifier();
            if (targetObjectId != null)
            {
                T8DataObjectDefinition dataObjectDefinition;

                dataObjectDefinition = (T8DataObjectDefinition)definitionContext.getRawDefinition(getRootProjectId(), targetObjectId);
                if (dataObjectDefinition != null)
                {
                    List<T8DataObjectFieldDefinition> objectFieldDefinitions;

                    objectFieldDefinitions = dataObjectDefinition.getFieldDefinitions();
                    optionList.addAll(createPublicIdentifierOptionsFromDefinitions(objectFieldDefinitions));
                    return optionList;
                } else return new ArrayList<>();
            } else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        } else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String definitionId;

        super.initializeDefinition(context, inputParameters, configurationSettings);

        // Pre-load the target object definition if one is specified.
        definitionId = getTargetObjectIdentifier();
        if (definitionId != null)
        {
            targetObjectDefinition = (T8DataObjectDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    @Override
    public T8FunctionalityGroupHandle getNewFunctionalityGroupHandle(T8Context context, T8FunctionalityGroupHandle parentGroup) throws Exception
    {
        T8NotificationSummary notificationSummary;
        T8NotificationManager notificationManager;
        T8FunctionalityGroupHandle groupHandle;
        T8ConfigurationManager configManager;
        T8FunctionalityHandle handle;
        String functionalityId;

        try
        {
            // Create the new handle.
            groupHandle = super.getNewFunctionalityGroupHandle(context, parentGroup);
            functionalityId = getFunctionalityIdentifier();

            // Get a configuration and notification managers.
            configManager = context.getServerContext().getConfigurationManager();
            notificationManager = context.getServerContext().getNotificationManager();

            // Get the notification summary.
            notificationSummary = notificationManager.getUserNotificationSummary(context, null);

            // Create the 'All Notifications' Functionality.
            handle = new T8FunctionalityHandle(functionalityId);
            handle.setDisplayName(configManager.getUITranslation(context, "All Notifications") + " (" + notificationSummary.getTotalNotificationCount() + ")");
            handle.setDescription(configManager.getUITranslation(context, "All Notifications currently available."));
            handle.setIcon(getIcon());
            handle.addParameter(functionalityId + "$P_NOTIFICATION_ID", null);
            handle.addParameter(functionalityId + "$P_INCLUDE_OLD_NOTIFICATIONS", true);
            handle.addParameter(functionalityId + "$P_INCLUDE_NEW_NOTIFICATIONS", true);

            // Add the completed handle to the group.
            groupHandle.addFunctionality(handle);

            // Create the 'New Notifications' Functionality
            handle = new T8FunctionalityHandle(functionalityId);
            handle.setDisplayName(configManager.getUITranslation(context, "New Notifications") + " (" + notificationSummary.getNewNotificationCount() + ")");
            handle.setDescription(configManager.getUITranslation(context, "All Notifications which are currently considered to be new."));
            handle.setIcon(getIcon());
            handle.addParameter(functionalityId + "$P_NOTIFICATION_ID", null);
            handle.addParameter(functionalityId + "$P_INCLUDE_OLD_NOTIFICATIONS", false);
            handle.addParameter(functionalityId + "$P_INCLUDE_NEW_NOTIFICATIONS", true);

            // Add the completed handle to the group.
            groupHandle.addFunctionality(handle);

            // Add all the task type functionalities.
            for (T8NotificationTypeSummary notificationTypeSummary : notificationSummary.getNotificationTypeSummaries())
            {
                if (notificationTypeSummary.getTotalCount() > 0)
                {
                    T8NotificationDefinition notificationDefinition;
                    String notificationId;

                    // Get the notification identifier from the type.
                    notificationId = notificationTypeSummary.getNotificationIdentifier();

                    // Get the notification definition.
                    try
                    {
                        notificationDefinition = (T8NotificationDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), notificationId, null);
                        if (notificationDefinition != null)
                        {
                            // Create the Functionality.
                            handle = new T8FunctionalityHandle(functionalityId);
                            handle.setDisplayName(notificationDefinition.getNotificationDisplayName() + " (" + notificationTypeSummary.getTotalCount() + ")");
                            handle.setDescription(notificationTypeSummary.getDescription());
                            handle.setIcon(notificationDefinition.getNotificationIcon());
                            handle.addParameter(functionalityId + "$P_NOTIFICATION_ID", notificationId);
                            handle.addParameter(functionalityId + "$P_INCLUDE_OLD_NOTIFICATIONS", true);
                            handle.addParameter(functionalityId + "$P_INCLUDE_NEW_NOTIFICATIONS", true);

                            // Add the completed handle to the group.
                            groupHandle.addFunctionality(handle);
                        } else throw new RuntimeException("Notification Definition Not Found : {"+notificationId+"}");
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while fetching notification definition: " + notificationId, e);
                    }
                }
            }

            return groupHandle;
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Failed to initialize the functionality group handle for Notifications. Identifier : " + getIdentifier(), ex);
        }
    }

    public String getFunctionalityIdentifier()
    {
        return getDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER);
    }

    public void setFunctionalityIdentifier(String functionalityIdentifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER, functionalityIdentifier);
    }

    public String getTargetObjectIdentifier()
    {
        return getDefinitionDatum(Datum.TARGET_OBJECT_IDENTIFIER);
    }

    public void setTargetObjectIdentifier(String targetObjectIdentifier)
    {
        setDefinitionDatum(Datum.TARGET_OBJECT_IDENTIFIER, targetObjectIdentifier);
    }

    public String getNotificationTypeFieldIdentifier()
    {
        return getDefinitionDatum(Datum.NOTIFICATION_TYPE_FIELD_IDENTIFIER);
    }

    public void setNotificationTypeFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.NOTIFICATION_TYPE_FIELD_IDENTIFIER, identifier);
    }

    public String getIncludeNewNotificationsFieldIdentifier()
    {
        return getDefinitionDatum(Datum.INCLUDE_NEW_NOTIFICATIONS_FIELD_IDENTIFIER);
    }

    public void setIncludeNewNotificationsFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INCLUDE_NEW_NOTIFICATIONS_FIELD_IDENTIFIER, identifier);
    }

    public String getIncludeReceivedNotificationsFieldIdentifier()
    {
        return getDefinitionDatum(Datum.INCLUDE_RECEIVED_NOTIFICATIONS_FIELD_IDENTIFIER);
    }

    public void setIncludeReceivedNotificationsFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INCLUDE_RECEIVED_NOTIFICATIONS_FIELD_IDENTIFIER, identifier);
    }

    public T8DataObjectDefinition getTargetObjectDefinition()
    {
        return this.targetObjectDefinition;
    }
}