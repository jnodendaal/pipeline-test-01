package com.pilog.t8.ui.functionality.event;

import com.pilog.t8.ui.functionality.T8FunctionalityView;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityViewUpdatedEvent extends EventObject
{
    private final T8FunctionalityView functionalityView;
    
    public T8FunctionalityViewUpdatedEvent(T8FunctionalityView functionalityView)
    {
        super(functionalityView);
        this.functionalityView = functionalityView;
    }

    public T8FunctionalityView getFunctionalityView()
    {
        return functionalityView;
    }
}
