package com.pilog.t8.definition.script;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultScriptDefinition extends T8Definition implements T8ScriptDefinition
{
    public enum Datum {SCRIPT};

    public T8DefaultScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.SCRIPT.toString(), "Script", "The script code."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.SCRIPT.toString().equals(datumId))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ScriptDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumId));
        } else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public abstract List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception;
    @Override
    public abstract List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception;

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        return null;
    }

    /**
     * The default method returns no method imports.  Sub-classes may override
     * this method to supply method required by the specific script application.
     *
     * @return The methods to import (in addition to the default methods) before
     * script execution.
     * @throws Exception
     */
    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        return null;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public String getScript()
    {
        return (String)getDefinitionDatum(Datum.SCRIPT.toString());
    }

    public void setScript(String script)
    {
        setDefinitionDatum(Datum.SCRIPT.toString(), script);
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }
}
