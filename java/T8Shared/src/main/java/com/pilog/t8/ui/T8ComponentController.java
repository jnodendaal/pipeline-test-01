package com.pilog.t8.ui;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Window;
import java.util.List;
import java.util.Map;

/**
 * This interface defines a context within which components on the client-side
 * operate.  A component controller receives events reported by the components
 * it controls and fires appropriate events to all listeners.  This model
 * differs from the Swing implementation where listeners are added on individual
 * components themselves but provides a simplified approach the has the
 * following benefits:
 *  -   In Swing, when events must to bubble from a lower descendant component
 *      to one of its ancestors, a hierarchy of listeners must be created if the
 *      lower descendant is encapsulated in such a way that the ancestor does
 *      not have direct access to it.  This often occurs in T8 and the component
 *      controller provides single point of control for all components
 *      operating within a specified context.
 *  -   In T8-client side events are much simpler compared to Swing events and
 *      therefore the fine-grained listener behavior of Swing is not required.
 *
 * It is important to note that component controllers form a hierarchy i.e.
 * there may be many separate and self-contained component contexts operating
 * simultaneously on the client-side (within a single client context).
 *
 * @author Bouwer du Preez
 */
public interface T8ComponentController
{
    public String getId();
    public Window getParentWindow();
    public T8ComponentController getParentController();
    public T8FunctionalityController getFunctionalityController();
    public T8ClientContext getClientContext();
    public T8SessionContext getSessionContext();
    public T8Context getContext();
    public <A extends T8Api> A getApi(String apiId);

    public String translate(String phrase);

    public void setContainer(T8ComponentContainer container);
    public T8ComponentContainer getContainer();

    public void start();
    public void stop();

    public void addComponentEventListener(T8ComponentEventListener listener);
    public void removeComponentEventListener(T8ComponentEventListener listener);

    public void addScript(T8ComponentControllerScriptDefinition scriptDefinition);
    public boolean removeScript(String scriptId);
    public Map<String, Object> executeScript(String scriptId, Map<String, ?  extends Object> inputParameters) throws Exception;

    public List<String> getVariableIds();
    public void setVariable(String name, Object value);
    public Object getVariable(String name);

    public void loadComponent(String parentComponentId, String componentProjectId, String componentId, Map<String, Object> inputParameters);
    public void addComponent(T8Component parentComponent, T8ComponentDefinition componentDefinition, Map<String, Object> inputParameters);
    public void registerComponent(T8Component component);
    public T8Component deregisterComponent(String componentId);
    public T8Component getParentComponent(String componentId);
    public T8Component findComponent(String componentId);

    public void addToButtonGroup(T8Component component, String buttonGroupId);
    public void removeFromButtonGroup(T8Component component, String buttonGroupId);

    public void reportEvent(T8Component sourceComponent, T8ComponentEventDefinition eventDefinition, Map<String, Object> eventParameters);
    public void fireGlobalEvent(String eventId, Map<String, Object> eventParameters);

    public Map<String, Object> showDialog(String dialogId, Map<String, Object> inputParameters, boolean keepAlive);

    public void lockUi(String message);
    public void unlockUi();

    public T8ServerOperationStatusReport getServerOperationStatus(String operationIid) throws Exception;
    public T8ServerOperationStatusReport stopServerOperation(String operationIid) throws Exception;
    public T8ServerOperationStatusReport cancelServerOperation(String operationIid) throws Exception;
    public T8ServerOperationStatusReport pauseServerOperation(String operationIid) throws Exception;
    public T8ServerOperationStatusReport resumeServerOperation(String operationIid) throws Exception;
    public T8ServerOperationStatusReport executeAsynchronousOperation(String operationId, Map<String, Object> operationParameters) throws Exception;
    public Map<String, Object> executeAsynchronousServerOperation(String operationId, Map<String, Object> operationParameters, String message) throws Exception;
    public Map<String, Object> executeSynchronousServerOperation(String operationId, Map<String, Object> operationParameters, String message) throws Exception;
    public Map<String, Object> executeClientOperation(String operationId, Map<String, Object> inputParameters) throws Exception;
}
