package com.pilog.t8.definition.script;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.task.script.T8FlowScriptTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_TASK";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FLOW_TASK";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Task Script";
    public static final String DESCRIPTION = "A script that is packaged as a task that can be executed from a task activity in a flow.";
    // -------- Definition Meta-Data -------- //
    
    public T8FlowTaskScriptDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8FlowScriptTaskDefinition taskDefinition;
        
        taskDefinition = (T8FlowScriptTaskDefinition)getParentDefinition();
        if (taskDefinition != null)
        {
            return taskDefinition.getInputParameterDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8FlowScriptTaskDefinition taskDefinition;
        
        taskDefinition = (T8FlowScriptTaskDefinition)getParentDefinition();
        if (taskDefinition != null)
        {
            return taskDefinition.getOutputParameterDefinitions();
        }
        else return null;
    }
}
