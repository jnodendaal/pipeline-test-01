package com.pilog.t8.definition.graph;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8GraphEdgeDefinition extends T8Definition
{
    private enum Datum {PARENT_NODE_IDENTIFIER, CHILD_NODE_IDENTIFIER};

    public T8GraphEdgeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PARENT_NODE_IDENTIFIER.toString(), "Parent Node", "The node where the edge starts."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CHILD_NODE_IDENTIFIER.toString(), "Child Node", "The node where the edge ends."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARENT_NODE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8GraphDefinition graphDefinition;

            graphDefinition =  (T8GraphDefinition)getAncestorDefinition(T8GraphDefinition.class);
            if (graphDefinition != null)
            {
                return createStringOptions(graphDefinition.getNodeIdentifiers());
            }
            else return null;
        }
        else if (Datum.CHILD_NODE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8GraphDefinition graphDefinition;

            graphDefinition =  (T8GraphDefinition)getAncestorDefinition(T8GraphDefinition.class);
            if (graphDefinition != null)
            {
                return createStringOptions(graphDefinition.getNodeIdentifiers());
            }
            else return null;
        }
        else return null;
    }

    public String getParentNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PARENT_NODE_IDENTIFIER.toString());
    }

    public void setParentNodeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PARENT_NODE_IDENTIFIER.toString(), identifier);
    }

    public String getChildNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CHILD_NODE_IDENTIFIER.toString());
    }

    public void setChildNodeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CHILD_NODE_IDENTIFIER.toString(), identifier);
    }
}
