package com.pilog.t8.definition.data.object.state;

import com.pilog.t8.data.object.T8DataObjectStateGraph;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.ui.T8GraphManager;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.definition.graph.T8GraphEdgeDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectStateGraphDefinition extends T8GraphDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT_STATE_GRAPH";
    public static final String GROUP_NAME = "Data Object State Graphs";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_STATE_GRAPH";
    public static final String STORAGE_PATH = "/state_graphs";
    public static final String DISPLAY_NAME = "Data Object State Graph";
    public static final String DESCRIPTION = "A diagram that defines the various states that a data object may be in.";
    public static final String IDENTIFIER_PREFIX = "O_STATE_GRAPH_";
    public enum Datum {DATA_OBJECT_IDS};
    // -------- Definition Meta-Data -------- //

    public T8DataObjectStateGraphDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_LIST, Datum.DATA_OBJECT_IDS.toString(), "Data Objects", "The list of data objects to which this graph is applicable."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_OBJECT_IDS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if (T8GraphDefinition.Datum.NODE_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataObjectStateDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DataObjectStateDefinition getNodeDefinition(String nodeIdentifier)
    {
        return (T8DataObjectStateDefinition)super.getNodeDefinition(nodeIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8GraphEdgeDefinition createEdgeDefinition(String parentIdentifier, String childIdentifier)
    {
        return new T8DataObjectStateGraphEdgeDefinition(T8Definition.getLocalIdPrefix() + T8IdentifierUtilities.createNewGUID(), parentIdentifier, childIdentifier);
    }

    @Override
    public T8GraphManager getGraphManager(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.developer.definitions.graphview.state.T8DataObjectStateGraphManager").getConstructor(T8Context.class);
        return (T8GraphManager)constructor.newInstance(context);
    }

    public T8DataObjectStateGraph getNewStateGraphInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.object.T8DefaultDataObjectStateGraph").getConstructor(T8Context.class, T8DataObjectStateGraphDefinition.class);
        return (T8DataObjectStateGraph)constructor.newInstance(context, this);
    }

    public List<String> getDataObjectIds()
    {
        List<String> ids;

        ids = getDefinitionDatum(Datum.DATA_OBJECT_IDS);
        return ids != null ? ids : new ArrayList<>();
    }

    public void setDataObjectIds(List<String> objectIds)
    {
        setDefinitionDatum(Datum.DATA_OBJECT_IDS, objectIds);
    }

    public List<T8StateGraphNodeDefinition> getStateNodeDefinitions()
    {
        // This is an unsafe cast, but in our case much safer than usual and better than recreating the list.
        return (List)getNodeDefinitions();
    }

    public List<T8DataObjectStateDefinition> getStateDefinitions()
    {
        List<T8DataObjectStateDefinition> stateDefinitions;

        stateDefinitions = new ArrayList<T8DataObjectStateDefinition>();
        for (T8StateGraphNodeDefinition nodeDefinition : getStateNodeDefinitions())
        {
            if (nodeDefinition instanceof T8DataObjectStateDefinition)
            {
                stateDefinitions.add((T8DataObjectStateDefinition)nodeDefinition);
            }
        }

        return stateDefinitions;
    }
}
