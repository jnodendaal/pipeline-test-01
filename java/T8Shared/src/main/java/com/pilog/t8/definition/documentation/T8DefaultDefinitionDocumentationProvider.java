package com.pilog.t8.definition.documentation;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDefinitionDocumentationProvider implements T8DefinitionDocumentationProvider
{
    protected T8DefinitionContext definitionContext;
    protected LinkedHashMap<String, T8DefinitionDocumentationType> documentationTypes;

    public static final String DEFAULT_DATE_FORMAT = "HH:mm:ss dd/MM/yyyy";
    public static final String DEFINITION_STRUCTURE_DOCUMENTATION_TYPE_IDENTIFIER = "DEFINITION_STRUCTURE";

    public T8DefaultDefinitionDocumentationProvider(T8DefinitionContext definitionContext)
    {
        this.definitionContext = definitionContext;
        this.documentationTypes = new LinkedHashMap<String, T8DefinitionDocumentationType>();
        documentationTypes.put(DEFINITION_STRUCTURE_DOCUMENTATION_TYPE_IDENTIFIER, new T8DefinitionDocumentationType(DEFINITION_STRUCTURE_DOCUMENTATION_TYPE_IDENTIFIER, "Definition Structure", "Documentation describing the internal structure of the definition and the datums it contains."));
    }

    @Override
    public boolean providesDocumentationType(String typeIdentifier)
    {
        return documentationTypes.containsKey(typeIdentifier);
    }

    @Override
    public T8DefinitionDocumentationType getDocumentationType(String typeIdentifier)
    {
        return documentationTypes.get(typeIdentifier);
    }

    @Override
    public List<T8DefinitionDocumentationType> getDocumentationTypes()
    {
        return new ArrayList<T8DefinitionDocumentationType>(documentationTypes.values());
    }

    @Override
    public StringBuffer getHTMLDocumentation(T8Definition definition, String typeIdentifier)
    {
        if (DEFINITION_STRUCTURE_DOCUMENTATION_TYPE_IDENTIFIER.equals(typeIdentifier))
        {
            return createDocumentationString(definitionContext, definition);
        }
        else return null;
    }

    protected StringBuffer createDocumentationString(T8DefinitionContext definitionContext, T8Definition definition)
    {
        T8DefinitionTypeMetaData metaData;
        StringBuffer documentation;

        metaData = definition.getTypeMetaData();

        documentation = new StringBuffer();
        documentation.append("<h1>Definition: " + definition.getIdentifier() + "</h1>");
        documentation.append("</br>");
        documentation.append(createDefinitionDetailsString(definitionContext, definition));
        documentation.append("</br>");
        documentation.append(createDefinitionDatumString(definitionContext, definition));
        return documentation;
    }

    protected StringBuffer createDefinitionDetailsString(T8DefinitionContext definitionContext, T8Definition definition)
    {
        T8DefinitionTypeMetaData metaData;
        StringBuffer documentation;
        String createdUserIdentifier;
        String updatedUserIdentifier;
        DateFormat dateFormat;
        Long createdTime;
        Long updatedTime;

        metaData = definition.getTypeMetaData();
        createdTime = definition.getCreatedTime();
        updatedTime = definition.getUpdatedTime();
        createdUserIdentifier = definition.getCreatedUserIdentifier();
        updatedUserIdentifier = definition.getUpdatedUserIdentifier();
        dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

        documentation = new StringBuffer();
        documentation.append("<h2>Definition Details</h2>");
        documentation.append("<table border=\"0\">");
        documentation.append("<tr><td><b>Identifier: </b></td><td>" + definition.getIdentifier() + "</td></tr>");
        documentation.append("<tr><td><b>Display Name: </b></td><td>" + definition.getMetaDisplayName() + "</td></tr>");
        documentation.append("<tr><td><b>Description: </b></td><td>" + definition.getMetaDescription() + "</td></tr>");
        documentation.append("<tr><td><b>Type: </b></td><td>" + metaData.getTypeId() + "</td></tr>");
        documentation.append("<tr><td><b>Type Display Name: </b></td><td>" + metaData.getDisplayName() + "</td></tr>");
        documentation.append("<tr><td><b>Type Group: </b></td><td>" + metaData.getGroupId() + "</td></tr>");
        documentation.append("<tr><td><b>Type Description: </b></td><td>" + metaData.getDescription() + "</td></tr>");
        documentation.append("<tr><td><b>Version: </b></td><td>" + definition.getVersionId() + "</td></tr>");
        documentation.append("<tr><td><b>Revision: </b></td><td>" + definition.getRevision() + "</td></tr>");
        documentation.append("<tr><td><b>Status: </b></td><td>" + definition.getDefinitionStatus() + "</td></tr>");
        documentation.append("<tr><td><b>Created Date: </b></td><td>" + (createdTime != null ? dateFormat.format(new Date(createdTime)) : "") + "</td></tr>");
        documentation.append("<tr><td><b>Created By: </b></td><td>" + createdUserIdentifier + "</td></tr>");
        documentation.append("<tr><td><b>Last Edited Date: </b></td><td>" + (updatedTime != null ? dateFormat.format(new Date(updatedTime)) : "") + "</td></tr>");
        documentation.append("<tr><td><b>Last Edited By: </b></td><td>" + updatedUserIdentifier + "</td></tr>");
        documentation.append("</table>");

        return documentation;
    }

    protected StringBuffer createDefinitionDatumString(T8DefinitionContext definitionContext, T8Definition definition)
    {
        List<T8DefinitionDatumType> datumTypes;
        StringBuffer documentation;

        datumTypes = definition.getDatumTypes();

        documentation = new StringBuffer();
        documentation.append("<h2>Settings</h2>");
        if ((datumTypes != null) && (datumTypes.size() > 0))
        {
            documentation.append("<table border=\"1\">");
            documentation.append("<tr><th>Setting Identifier</th><th>Setting Name</th><th>Description</th><th>Data Type</th><th>Possible Values</th></tr>");
            for (T8DefinitionDatumType datumType : datumTypes)
            {
                ArrayList<T8DefinitionDatumOption> datumOptions;

                documentation.append("<tr>");
                documentation.append("<td>" + datumType.getIdentifier() + "</td>");
                documentation.append("<td>" + datumType.getDisplayName() + "</td>");
                documentation.append("<td>" + datumType.getDescription() + "</td>");
                documentation.append("<td>" + datumType.getDataType().getDataTypeStringRepresentation(true) + "</td>");
                documentation.append("<td>");
                documentation.append("<ul>");

                try
                {
                    datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
                    if (datumOptions == null)
                    {
                        documentation.append("<li>Any Value</li>");
                    }
                    else
                    {
                        for (T8DefinitionDatumOption datumOption : datumOptions)
                        {
                            documentation.append("<li>" + datumOption.getDisplayName() + ": " + datumOption.getValue() + "</li>");
                        }
                    }
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while creating datum documentation String.", e);
                }

                documentation.append("</ul>");
                documentation.append("</td>");
                documentation.append("</tr>");
            }
            documentation.append("</table>");
        }
        else
        {
            documentation.append("This component does not have any defined events.");
        }

        return documentation;
    }
}
