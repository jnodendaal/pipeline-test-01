package com.pilog.t8.functionality.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.state.T8StateUpdates;
import java.io.Serializable;
import java.util.Objects;

import static com.pilog.t8.definition.functionality.T8FunctionalityManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityDataObjectState implements Serializable
{
    private String functionalityIid;
    private String dataObjectIid;
    private String dataObjectId;
    private boolean updated;
    private boolean inserted;

    public T8FunctionalityDataObjectState(String functionalityIid, String dataObjectId, String dataObjectIid)
    {
        this.functionalityIid = functionalityIid;
        this.dataObjectId = dataObjectId;
        this.dataObjectIid = dataObjectIid;
        this.updated = false;
        this.inserted = true;
    }

    public T8FunctionalityDataObjectState(T8DataEntity dataObjectEntity)
    {
        // Construct the task state.
        this.functionalityIid = (String)dataObjectEntity.getFieldValue(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER + F_FUNCTIONALITY_IID);
        this.dataObjectIid = (String)dataObjectEntity.getFieldValue(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER + F_DATA_OBJECT_IID);
        this.dataObjectId = (String)dataObjectEntity.getFieldValue(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER + F_DATA_OBJECT_ID);
        this.updated = false;
        this.inserted = false;
    }

    public T8DataEntity createEntity(T8DataTransaction tx) throws Exception
    {
        T8DataEntity newEntity;
        String entityId;

        entityId = STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER;
        newEntity = tx.create(entityId, null);
        newEntity.setFieldValue(entityId + F_FUNCTIONALITY_IID, functionalityIid);
        newEntity.setFieldValue(entityId + F_DATA_OBJECT_IID, dataObjectIid);
        newEntity.setFieldValue(entityId + F_DATA_OBJECT_ID, dataObjectId);
        return newEntity;
    }

    public void statePersisted()
    {
        // Reset this state's flags.
        inserted = false;
        updated = false;
    }

    public void addUpdates(T8DataTransaction tx, T8StateUpdates updates) throws Exception
    {
        // Add this object's states updates.
        if (inserted) updates.addInsert(createEntity(tx));
        else if (updated) updates.addUpdate(createEntity(tx));
    }

    public String getFunctionalityIid()
    {
        return functionalityIid;
    }

    public void setFunctionalityIid(String functionalityIid)
    {
        if (!Objects.equals(this.functionalityIid, functionalityIid))
        {
            updated = true;
            this.functionalityIid = functionalityIid;
        }
    }

    public String getDataObjectIid()
    {
        return dataObjectIid;
    }

    public void setDataObjectIid(String dataObjectIid)
    {
        if (!Objects.equals(this.dataObjectIid, dataObjectIid))
        {
            updated = true;
            this.dataObjectIid = dataObjectIid;
        }
    }

    public String getDataObjectId()
    {
        return dataObjectId;
    }

    public void setDataObjectId(String dataObjectId)
    {
        if (!Objects.equals(this.dataObjectId, dataObjectId))
        {
            updated = true;
            this.dataObjectId = dataObjectId;
        }
    }
}