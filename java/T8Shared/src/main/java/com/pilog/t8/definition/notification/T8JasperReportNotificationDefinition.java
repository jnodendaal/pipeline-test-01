package com.pilog.t8.definition.notification;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationGenerator;
import com.pilog.t8.ui.notification.T8NotificationDisplayComponent;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportNotificationDefinition extends T8NotificationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String IDENTIFIER = "@NOTIF_JASPER";
    public static final String TYPE_IDENTIFIER = "@DT_NOTIFICATION_JASPER_RESOURCE";
    public static final String DISPLAY_NAME = "Jasper Report Notification Generator";
    public static final String DESCRIPTION = "A definition of a notification generator for Jasper Report messages.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    // Notification input parameters.
    public static final String PARAMETER_FILE_CONTEXT_IID = "$P_FILE_CONTEXT_IID";
    public static final String PARAMETER_REPORT_FILE_NAME = "$P_REPORT_FILE_NAME";
    public static final String PARAMETER_GENERATION_TIME = "$P_GENERATION_TIME";
    public static final String PARAMETER_GENERATION_SUCCESS = "$P_GENERATION_SUCCESS";
    public static final String PARAMETER_REPORT_NAME = "$P_REPORT_NAME";
    public static final String PARAMETER_REPORT_TYPE = "$P_REPORT_TYPE";

    // Report Type Predefinition
    public enum ReportType
    {
        JASPER(".rpt"),
        PDF(".pdf");

        private final String extension;

        private ReportType(String extension)
        {
            this.extension = extension;
        }

        public String getExtension()
        {
            return this.extension;
        }
    }

    public T8JasperReportNotificationDefinition(String identifier)
    {
        super(IDENTIFIER);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_FILE_CONTEXT_IID, "File Context Instance ID", "The instance identifier for the file context used by the report.", T8DataType.GUID));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_REPORT_FILE_NAME, "Report File Name", "The File Name for the Jasper Report.", T8DataType.STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_GENERATION_TIME, "Generation Time", "The long representation of the date and time at which the report was generated.", T8DataType.LONG));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_GENERATION_SUCCESS, "Generation Success", "Specifies whether or not the report generation was successful.", T8DataType.BOOLEAN));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_REPORT_NAME, "Report Name", "The display name for the report.", T8DataType.DISPLAY_STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_REPORT_TYPE, "Report Type", "The report type, which essentially specifies the format of the report.", T8DataType.STRING));

        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getNotificationParameterDefinitions()
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_FILE_CONTEXT_IID, "File Context Instance ID", "The instance identifier for the file context used by the report.", T8DataType.GUID));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_REPORT_FILE_NAME, "Report File Name", "The File Name for the Jasper Report.", T8DataType.STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_GENERATION_TIME, "Generation Time", "The long representation of the date and time at which the report was generated.", T8DataType.LONG));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_GENERATION_SUCCESS, "Generation Success", "Specifies whether or not the report generation was successful.", T8DataType.BOOLEAN));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_REPORT_NAME, "Report Name", "The display name for the report.", T8DataType.DISPLAY_STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_REPORT_TYPE, "Report Type", "The report type, which essentially specifies the format of the report.", T8DataType.STRING));

        return parameterList;
    }

    @Override
    public String getNotificationDisplayName()
    {
        return "Report";
    }

    @Override
    public Icon getNotificationIcon()
    {
        return new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/reportIcon.png"));
    }

    @Override
    public T8NotificationGenerator getNotificationGeneratorInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.notification.T8JasperNotificationGenerator", new Class<?>[]{T8JasperReportNotificationDefinition.class}, this);
    }

    @Override
    public T8NotificationDisplayComponent getDisplayComponentInstance(T8Context context, T8Notification notification)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.notification.T8JasperReportNotificationDisplayComponent", new Class<?>[]{T8Context.class, T8JasperReportNotificationDefinition.class, T8Notification.class}, context, this, notification);
    }
}