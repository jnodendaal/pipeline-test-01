package com.pilog.t8.data.object.filter;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.data.filter.T8DataFilterCriterionDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ObjectFilterCriterion implements T8ObjectFilterClause, Serializable
{
    private String id;
    private String fieldId;
    private ObjectFilterOperator operator;
    private Object filterValue;
    private boolean caseInsensitive;

    public enum ObjectFilterOperator
    {
        EQUAL("Equal To"),
        NOT_EQUAL("Not Equal To"),
        GREATER_THAN("Greater Than"),
        GREATER_THAN_OR_EQUAL("Greater Than or Equal To"),
        LESS_THAN("Less than"),
        LESS_THAN_OR_EQUAL("Less Than or Equal To"),
        LIKE("Containing"),
        STARTS_WITH("Beginning With"),
        ENDS_WITH("Ending With"),
        MATCHES("Matching"),
        NOT_LIKE("Not Containing"),
        IN("In"),
        NOT_IN("Not In"),
        IS_NULL("Is Empty"),
        IS_NOT_NULL("Is Not Empty"),
        THIS_DAY("This Day");

        private final String displayString;

        ObjectFilterOperator(String displayString)
        {
            this.displayString = displayString;
        }

        public String getDisplayString()
        {
            return this.displayString;
        }

        public DataFilterOperator getDataFilterOperator()
        {
            switch (this)
            {
                case EQUAL: return DataFilterOperator.EQUAL;
                case NOT_EQUAL: return DataFilterOperator.NOT_EQUAL;
                case GREATER_THAN: return DataFilterOperator.GREATER_THAN;
                case GREATER_THAN_OR_EQUAL: return DataFilterOperator.GREATER_THAN_OR_EQUAL;
                case LESS_THAN: return DataFilterOperator.LESS_THAN;
                case LESS_THAN_OR_EQUAL: return DataFilterOperator.LESS_THAN_OR_EQUAL;
                case LIKE: return DataFilterOperator.LIKE;
                case STARTS_WITH: return DataFilterOperator.STARTS_WITH;
                case ENDS_WITH: return DataFilterOperator.ENDS_WITH;
                case MATCHES: return DataFilterOperator.MATCHES;
                case NOT_LIKE: return DataFilterOperator.NOT_LIKE;
                case IN: return DataFilterOperator.IN;
                case NOT_IN: return DataFilterOperator.NOT_IN;
                case IS_NULL: return DataFilterOperator.IS_NULL;
                case IS_NOT_NULL: return DataFilterOperator.IS_NOT_NULL;
                case THIS_DAY: return DataFilterOperator.THIS_DAY;
                default: throw new IllegalArgumentException("Unsupported object type: " + this);
            }
        }
    };

    public T8ObjectFilterCriterion(String fieldIdentifier, ObjectFilterOperator operator, Object filterValue)
    {
        this.fieldId = fieldIdentifier;
        this.operator = operator;
        this.filterValue = filterValue;
    }

    public T8ObjectFilterCriterion(String fieldIdentifier, ObjectFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(fieldIdentifier, operator, filterValue);
        this.caseInsensitive = caseInsensitive;
    }

    public T8ObjectFilterCriterion(String identifier, String fieldIdentifier, ObjectFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(fieldIdentifier, operator, filterValue, caseInsensitive);
        this.id = identifier;
    }

    public T8ObjectFilterCriterion(T8DataFilterCriterionDefinition definition, String fieldIdentifier, ObjectFilterOperator operator, Object filterValue)
    {
        this(fieldIdentifier, operator, filterValue);
        this.id = definition.getIdentifier();
    }

    public T8ObjectFilterCriterion(T8DataFilterCriterionDefinition definition, String fieldIdentifier, ObjectFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        this(fieldIdentifier, operator, filterValue, caseInsensitive);
        this.id = definition.getIdentifier();
    }

    public String getIdentifier()
    {
        return id;
    }

    @Override
    public T8ObjectFilterCriterion copy()
    {
        T8ObjectFilterCriterion copiedCriterion;

        copiedCriterion = new T8ObjectFilterCriterion((String)id, fieldId, operator, filterValue, caseInsensitive);
        return copiedCriterion;
    }

    @Override
    public Set<String> getFilterFieldIds()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<String>();
        fieldSet.add(fieldId);

        return fieldSet;
    }

    @Override
    public List<T8ObjectFilterCriterion> getFilterCriterionList()
    {
        return ArrayLists.typeSafeList(this);
    }

    public String getFieldIdentifier()
    {
        return fieldId;
    }

    public void setFieldIdentifier(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public Object getFilterValue()
    {
        return filterValue;
    }

    public void setFilterValue(Object filterValue)
    {
        this.filterValue = filterValue;
    }

    public ObjectFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(ObjectFilterOperator operator)
    {
        this.operator = operator;
    }

    public boolean isCaseInsensitive()
    {
        return caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        this.caseInsensitive = caseInsensitive;
    }

    @Override
    public boolean hasCriteria()
    {
        if (operator == ObjectFilterOperator.IS_NULL) return true; // This operator does not require a filter value.
        else if (operator == ObjectFilterOperator.IS_NOT_NULL) return true; // This operator does not require a filter value.
        else if (filterValue == null) return false; // All other operators require a filter value.
        else if (filterValue instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return !Strings.isNullOrEmpty((String)filterValue);
        }
        else return true;
    }

    @Override
    public T8ObjectFilterCriterion getFilterCriterion(String id)
    {
        if ((this.id != null) && (this.id.equals(id))) return this;
        else return null;
    }

    @Override
    public T8DataFilterClause getDataFilterClause(String entityId, Map<String, String> objectToEntityFieldMap)
    {
        T8DataFilterClause clause;

        clause = new T8DataFilterCriterion(objectToEntityFieldMap.get(fieldId), operator.getDataFilterOperator(), filterValue);
        return clause;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder();
        toStringBuilder.append("[");

        // Append a descriptor.
        if (id != null)
        {
            toStringBuilder.append(id);
            toStringBuilder.append(":");
        }
        else
        {
            toStringBuilder.append("Filter Criterion:");
        }

        // Append the criterion specifics.
        toStringBuilder.append(fieldId);
        toStringBuilder.append(" ");
        toStringBuilder.append(operator);
        toStringBuilder.append(" ");
        toStringBuilder.append(filterValue);
        toStringBuilder.append("]");
        return toStringBuilder.toString();
    }
}
