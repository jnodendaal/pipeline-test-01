package com.pilog.t8.security;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8PasswordChangeRequest implements Serializable
{
    private final String userIdentifier;
    private final char[] password;

    public T8PasswordChangeRequest(String userIdentifier, char[] password)
    {
        this.userIdentifier = userIdentifier;
        this.password = password;
    }

    public char[] getPassword()
    {
        return password;
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }
}
