package com.pilog.t8;

import com.pilog.t8.security.T8Context;
import com.pilog.t8.user.event.T8UserManagerListener;

/**
 * @author Bouwer du Preez
 */
public interface T8UserManager extends T8ServerModule
{
    /**
     * Adds a listener to the definition manager that will be notified of all
     * events that occur.
     * @param listener The listener to add to the manager.
     */
    public void addUserManagerListener(T8UserManagerListener listener);

    /**
     * Removes the specified listener from the definition manager.
     * @param listener The listener to remove from the user manager.
     */
    public void removeUserManagerListener(T8UserManagerListener listener);

    /**
     * Updates the user profile picture of the currently logged in user.
     * @param context
     * @param fileContextIid
     * @param filePath
     * @throws Exception
     */
    public void updateUserProfilePicture(T8Context context, String fileContextIid, String filePath) throws Exception;
}
