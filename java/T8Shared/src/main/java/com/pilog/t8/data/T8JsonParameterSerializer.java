package com.pilog.t8.data;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8JsonParameterSerializer
{
    public T8JsonParameterSerializer()
    {
    }

    public JsonObject serializeParameters(List<T8DataParameterDefinition> parameterDefinitions, Map<String, Object> parameters) throws Exception
    {
        JsonObject jsonObject;

        jsonObject = new JsonObject();
        if (parameters != null)
        {
            for (T8DataParameterDefinition parameterDefinition : parameterDefinitions)
            {
                T8DataType dataType;

                // Get the data type for the parameter.
                dataType = parameterDefinition.getDataType();
                if (dataType != null)
                {
                    String publicParameterIdentifier;
                    String localParameterIdentifier;
                    Object parameterValue;
                    JsonValue jsonValue;

                    // Get the parameter identifier and value.
                    publicParameterIdentifier = parameterDefinition.getPublicIdentifier();
                    localParameterIdentifier = parameterDefinition.getIdentifier();
                    if (parameters.containsKey(localParameterIdentifier) || parameters.containsKey(publicParameterIdentifier))
                    {
                        parameterValue = parameters.getOrDefault(localParameterIdentifier, parameters.get(publicParameterIdentifier));

                        // Use the parameter data type to serialize the value to JSON.
                        jsonValue = dataType.serialize(parameterValue);
                        jsonObject.add(publicParameterIdentifier, jsonValue);
                    }
                }
                else throw new RuntimeException("Null data type returned from parameter definition: " + parameterDefinition.getPublicIdentifier());
            }
        }

        // Write the JSON object to string output.
        return jsonObject;
    }

    public Map<String, Object> deserializeParameters(List<T8DataParameterDefinition> parameterDefinitions, Reader reader) throws Exception
    {
        JsonObject jsonObject;

        // Read the JSON object from the input reader.
        jsonObject = JsonObject.readFrom(reader);
        return deserializeParameters(parameterDefinitions, jsonObject);
    }

    public Map<String, Object> deserializeParameters(List<T8DataParameterDefinition> parameterDefinitions, JsonObject jsonObject) throws Exception
    {
        Map<String, Object> parameters;

        // Create a map of parameters.
        parameters = new HashMap<>();
        for (T8DataParameterDefinition parameterDefinition : parameterDefinitions)
        {
            String parameterId;
            T8DataType dataType;
            JsonValue jsonValue;

            // Get the parameter identifier and data type.
            parameterId = parameterDefinition.getPublicIdentifier();
            dataType = parameterDefinition.getDataType();
            if (jsonObject.containsName(parameterId))
            {
                if (dataType != null)
                {
                    jsonValue = jsonObject.get(parameterId);

                    // Use the data type to convert the JSON value to a T8 parameter value.
                    parameters.put(parameterId, dataType.deserialize(jsonValue));
                }
                else throw new Exception("No data type found for parameter: " + parameterDefinition);
            }
        }

        // Return the complete parameter map.
        return parameters;
    }

    /**
     * In the event that the serialization is taking place using a map of
     * parameters which have already been mapped to a different key-set of
     * output parameters, the original parameter mapping will be used to map the
     * parameter data types, to serialize the parameters accordingly.
     *
     * @param operationDefinition The {@code T8ServerOperationDefinition} which
     *      will be used to determine the parameter data types to serialize the
     *      parameters accordingly
     * @param parameterMapping The {@code Map} which contains the parameter
     *      identifier mapping to correctly map the data types of the original
     *      parameter identifier to that of the identifiers currently in the
     *      parameter map
     * @param mappedParameters The {@code Map} of parameter values and their
     *      identifiers to be serialized
     *
     * @return The {@code String} containing the serialized parameter values
     */
    public CharSequence generateMappedInputParameterJSON(T8ServerOperationDefinition operationDefinition, Map<String, String> parameterMapping, Map<String, Object> mappedParameters)
    {
        //TODO: GBO - The mapped methods need to be revised.
        String publicParameterIdentifier;
        String localParameterIdentifier;
        String inputParameterIdentifier;
        Object parameterValue;
        T8DataType dataType;
        JsonValue jsonValue;
        JsonObject model;

        model = new JsonObject();
        for (T8DataParameterDefinition parameterDefinition : operationDefinition.getInputParameterDefinitions())
        {
            // Get the data type for the parameter.
            dataType = parameterDefinition.getDataType();
            if (dataType != null)
            {
                // Get the parameter identifier and value.
                publicParameterIdentifier = parameterDefinition.getPublicIdentifier();
                localParameterIdentifier = parameterDefinition.getIdentifier();

                // Get the output parameter public identifier
                inputParameterIdentifier = parameterMapping.getOrDefault(localParameterIdentifier, parameterMapping.get(publicParameterIdentifier));

                // Get the parameter value using the parameter mapping received
                parameterValue = mappedParameters.getOrDefault(parameterMapping.get(localParameterIdentifier), mappedParameters.get(inputParameterIdentifier));

                // Use the parameter data type to serialize the value to JSON.
                jsonValue = dataType.serialize(parameterValue);
                model.add(inputParameterIdentifier, jsonValue);
            }
            else throw new RuntimeException("Null data type returned from parameter definition: " + parameterDefinition.getPublicIdentifier());
        }

        // Write the JSON object to string output.
        return model.toString();
    }

    /**
     * In the event that the de-serialization is taking place using a map of
     * parameters which need to be mapped to a different key-set of parameters,
     * the specified parameter mapping will be used to map the parameter data
     * types, to de-serialize the parameters accordingly.
     *
     * @param operationDefinition The {@code T8ServerOperationDefinition} which
     *      will be used to determine the parameter data types to de-serialize
     *      the parameters accordingly
     * @param parameterMapping The {@code Map} which contains the parameter
     *      identifier mapping to correctly map the data types of the original
     *      parameter identifier to that of the identifiers received from the
     *      {@code JSON}
     * @param jsonReader The {@code String} containing the JSON values to be
     *      read and parsed using the mapping and definition
     *
     * @return The {@code Map} containing the de-serialized parameter values.<br/>
     *      <b>Note:</b> that the map will contain the identifiers as received
     *      from the {@code JSON String} and will still need to be mapped if so
     *      required
     */
    public Map<String, Object> parseMappedOutputParameterJSON(T8ServerOperationDefinition operationDefinition, Map<String, String> parameterMapping, String jsonString) throws IOException
    {
        //TODO: The mapped methods need to be revised
        Map<String, String> reverseMapping;
        String outputParameterIdentifier;
        Map<String, Object> parameters;
        JsonObject jsonObject;

        // Read the JSON object from the input reader.
        jsonObject = JsonObject.readFrom(jsonString);

        reverseMapping = new HashMap<>();
        if (parameterMapping != null)
        {
            for (Map.Entry<String, String> mapping : parameterMapping.entrySet())
            {
                reverseMapping.put(mapping.getValue(), mapping.getKey());
            }
        }

        // Create a map of parameters.
        parameters = new HashMap<>();
        for (T8DataParameterDefinition parameterDefinition : operationDefinition.getOutputParameterDefinitions())
        {
            String localParameterIdentifier;
            String parameterIdentifier;
            T8DataType dataType;
            JsonValue jsonValue;

            // Get the parameter identifier and data type.
            parameterIdentifier = parameterDefinition.getPublicIdentifier();
            localParameterIdentifier = parameterDefinition.getIdentifier();

            dataType = parameterDefinition.getDataType();

            // Get the input parameter public identifier
            outputParameterIdentifier = reverseMapping.getOrDefault(localParameterIdentifier, reverseMapping.get(parameterIdentifier));

            // Get the JSON value associated with the current
            jsonValue = jsonObject.get(outputParameterIdentifier);

            // Use the data type to convert the JSON value to a T8 parameter value.
            parameters.put(outputParameterIdentifier, dataType.deserialize(jsonValue));
        }

        // Return the complete parameter map.
        return parameters;
    }
}
