package com.pilog.t8.flow.task;

import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowTask
{
    public enum TaskPriority
    {
        VERY_LOW,
        LOW,
        NORMAL,
        HIGH,
        VERY_HIGH
    };

    public enum TaskStatus
    {
        NOT_STARTED,
        ACTIVE,
        COMPLETED,
        FAILED,
        STOPPING,
        STOPPED
    };

    public enum TaskEvent
    {
        ISSUED, // The task has been issued and made available on the task list.
        CLAIMED, // The task has been claimed, restricting access to and completion of the task to the user who claimed it.
        UNCLAIMED, // The task has been unclaimed, making it available to other users on the task list.
        COMPLETED, // The task has been completed and therefore removed from the task list.
        CANCELLED, // The task has been cancelled.
        REASSIGNED, // The task has been reassigned to a different user/profile.
        PRIORITY_UPDATED, // The priority of the task has been changed by an authorized user.
        ESCALATED, // The priority of the task has been automatically escalated by the system according to predefined configuration.
        VALIDATION_FAILED, // Validation of the task was requested and failed.
        DATA_UPDATED // Data related to the task and its display has been updated.
    };

    // Returns the definition of the task.
    public T8FlowTaskDefinition getDefinition();
    // Returns the details of the task as issued by the flow manager that spawned this task.
    public T8TaskDetails getTaskDetails();
    // Instructs the task to stop execution as soon as possible.
    public void stopTask();
    // Instructs the task to cancel execution as soon as possible and to roll back any transactional changes if possible.
    public void cancelTask();
    // Returns the status of this task (-1 indicates indeterminate progress).
    public T8FlowTaskStatus getStatus();
    // Starts task execution from within a client-side context and a specific user session context.
    public Map<String, Object> doTask(T8ComponentController componentController, Map<String, Object> inputParameters) throws Exception;
    // Starts task execution from within a server-side context and a specific user session context.
    public Map<String, Object> doTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception;
    // Resumes task execution from within a server-side context and a specific user session context.
    public Map<String, Object> resumeTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception;
}
