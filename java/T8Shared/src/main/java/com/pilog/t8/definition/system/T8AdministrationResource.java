package com.pilog.t8.definition.system;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8AdministrationResource implements T8DefinitionResource
{
    public enum ValidationType{DEFINITION_VALIDATION, UN_USED_DEFINITIONS};

    // These should NOT be changed, only added to.
    public static final String OPERATION_VALIDATE_DEFINITIONS = "@OP_ADM_VALIDATE_DEFINITIONS";
    public static final String OPERATION_VALIDATE_SYSTEM = "@OP_ADM_VALIDATE_SYSTEM";
    public static final String OPERATION_COMPILE_OPERATION = "@OP_ADM_COMPILE_OPERATION";

    public static final String PARAMETER_VALIDATION_TYPE = "$P_VALIDATION_TYPE";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_DEFINITION_ID = "$P_DEFINITION_ID";
    public static final String PARAMETER_DEFINITION_IDS = "$P_DEFINITION_IDS";
    public static final String PARAMETER_DEFINITION_VALIDATION_REPORT = "$P_DEFINITION_VALIDATION_REPORT";
    public static final String PARAMETER_COMPILATION_RESULT = "$P_COMPILATION_RESULT";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_VALIDATE_SYSTEM);
            definition.setMetaDisplayName("Validate System");
            definition.setMetaDescription("Validates all definitions throughout the system");
            definition.setClassName("com.pilog.t8.server.operations.T8AdministrationApiOperations$ApiSystemValidationOperation");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALIDATION_TYPE, "Validation Type", "The type of validation operation that will be performed", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DEFINITION_VALIDATION_REPORT, "Validation Report", "An HTML report that can be displayed to user containing the validation errors found", T8DataType.LONG_STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_VALIDATE_DEFINITIONS);
            definition.setMetaDisplayName("Validate Definitions");
            definition.setMetaDescription("Validates a set of definitions");
            definition.setClassName("com.pilog.t8.server.operations.T8AdministrationApiOperations$ApiDefinitionValidationOperation");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DEFINITION_IDS, "Definition Identifier List", "The list of definition identifiers of the definitions to be validated", T8DataType.DEFINITION_IDENTIFIER_LIST));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DEFINITION_VALIDATION_REPORT, "Validation Report", "An HTML report that can be displayed to user containing the validation errors found", T8DataType.LONG_STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_COMPILE_OPERATION);
            definition.setMetaDisplayName("Compile Operation");
            definition.setMetaDescription("Compiles a specific operaiton so that it may be executed by the system.");
            definition.setClassName("com.pilog.t8.server.operations.T8AdministrationApiOperations$ApiCompileOperation");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROJECT_ID, "Project Identifier", "The id of the project where the dynamic java operation to be compiled is located.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DEFINITION_ID, "Definition Identifier", "The id of the dynamic java operation to compile.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "SUCCESS", "A Boolean flag indicating successful compilation of the operation.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COMPILATION_RESULT, "Compilation Report", "An report detailing the compilation errors (if any) encountered.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
