package com.pilog.t8.flow.state.data;

import com.pilog.t8.data.T8DataEntity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateDataSet
{
    private final T8ParameterStateDataSet inputParameterDataSet;
    private final Map<String, T8NodeStateDataSet> nodeDataSets; // Key: NODE_IID, Value: Node Data Set.
    private final List<T8DataEntity> dataObjectStateEntities;
    private T8DataEntity flowEntity;

    public T8FlowStateDataSet()
    {
        this.inputParameterDataSet = new T8ParameterStateDataSet("$PARENT_PARAMETER_IID");
        this.nodeDataSets = new HashMap<String, T8NodeStateDataSet>();
        this.dataObjectStateEntities = new ArrayList<T8DataEntity>();
    }

    public int getEntityCount()
    {
        int count;

        count = flowEntity != null ? 1 : 0;
        count += inputParameterDataSet.getEntityCount();
        count += dataObjectStateEntities.size();
        for (T8NodeStateDataSet nodeStateDataSet : getNodeStateDataSets())
        {
            count += nodeStateDataSet.getEntityCount();
        }

        return count;
    }

    public T8DataEntity getFlowEntity()
    {
        return flowEntity;
    }

    public void setFlowEntity(T8DataEntity flowEntity)
    {
        this.flowEntity = flowEntity;
    }

    public T8ParameterStateDataSet getInputParameterDataSet()
    {
        return inputParameterDataSet;
    }

    public List<T8NodeStateDataSet> getNodeStateDataSets()
    {
        return new ArrayList<T8NodeStateDataSet>(nodeDataSets.values());
    }

    public T8TaskStateDataSet getTaskStateDataSet(String taskIID)
    {
        for (T8NodeStateDataSet nodeStateDataSet : getNodeStateDataSets())
        {
            T8TaskStateDataSet taskStateDataSet;

            taskStateDataSet = nodeStateDataSet.getTaskStateDataSet();
            if ((taskStateDataSet != null) && (taskStateDataSet.getTaskIID().equals(taskIID)))
            {
                return taskStateDataSet;
            }
        }

        return null;
    }

    public void addDataObjectData(List<T8DataEntity> entities)
    {
        dataObjectStateEntities.addAll(entities);
    }

    public List<T8DataEntity> getDataObjectStateEntities()
    {
        return dataObjectStateEntities;
    }

    public void addInputParameterData(List<T8DataEntity> entityList)
    {
        inputParameterDataSet.addParameterData(entityList);
    }

    public void addNodeData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSets.put(nodeIID, new T8NodeStateDataSet(nodeIID, entity));
        }
    }

    public void addNodeInputParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8NodeStateDataSet nodeDataSet;
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSet = nodeDataSets.get(nodeIID);
            nodeDataSet.addInputParameterData(entity);
        }
    }

    public void addNodeExecutionParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8NodeStateDataSet nodeDataSet;
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSet = nodeDataSets.get(nodeIID);
            nodeDataSet.addExecutionParameterData(entity);
        }
    }

    public void addNodeOutputParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8NodeStateDataSet nodeDataSet;
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSet = nodeDataSets.get(nodeIID);
            nodeDataSet.addOutputParameterData(entity);
        }
    }

    public void addNodeOutputNodeData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8NodeStateDataSet nodeDataSet;
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSet = nodeDataSets.get(nodeIID);
            nodeDataSet.addOutputNodeData(entity);
        }
    }

    public void addTaskData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8NodeStateDataSet nodeDataSet;
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSet = nodeDataSets.get(nodeIID);
            nodeDataSet.setTaskData(entity);
        }
    }

    public void addTaskInputParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8TaskStateDataSet taskDataSet;
            String taskIID;

            taskIID = (String)entity.getFieldValue("$TASK_IID");
            taskDataSet = getTaskStateDataSet(taskIID);
            if (taskDataSet != null) taskDataSet.addInputParameterData(entity);
            else throw new RuntimeException("Task state data not found: " + taskIID);
        }
    }

    public void addTaskOutputParameterData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8TaskStateDataSet taskDataSet;
            String taskIID;

            taskIID = (String)entity.getFieldValue("$TASK_IID");
            taskDataSet = getTaskStateDataSet(taskIID);
            if (taskDataSet != null) taskDataSet.addOutputParameterData(entity);
            else throw new RuntimeException("Task state data not found: " + taskIID);
        }
    }

    public void addTaskPropertyData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8TaskStateDataSet taskDataSet;
            String taskIID;

            taskIID = (String)entity.getFieldValue("$TASK_IID");
            taskDataSet = getTaskStateDataSet(taskIID);
            if (taskDataSet != null) taskDataSet.addPropertyData(entity);
            else throw new RuntimeException("Task state data not found: " + taskIID);
        }
    }

    public void addWaitKeyData(List<T8DataEntity> entityList)
    {
        for (T8DataEntity entity : entityList)
        {
            T8NodeStateDataSet nodeDataSet;
            String nodeIID;

            nodeIID = (String)entity.getFieldValue("$NODE_IID");
            nodeDataSet = nodeDataSets.get(nodeIID);
            nodeDataSet.addWaitKeyData(entity);
        }
    }
}
