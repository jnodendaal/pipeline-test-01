package com.pilog.t8.definition;

import com.pilog.t8.ui.T8DefinitionDatumEditor;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionEditorFactory
{
    public T8DefinitionDatumEditor getDefaultDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType) throws Exception;
}
