package com.pilog.t8.flow.state;

import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.state.T8StateUpdates;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStateUpdates extends T8StateUpdates
{
    private T8DataEntityDefinition flowStateEntityDefinition;
    private T8DataEntityDefinition flowStateInputParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateEntityDefinition;
    private T8DataEntityDefinition nodeStateInputParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateOutputParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateExecutionParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateOutputNodeEntityDefinition;
    private T8DataEntityDefinition nodeStateWaitKeyEntityDefinition;
    private T8DataEntityDefinition taskStateEntityDefinition;
    private T8DataEntityDefinition taskStateInputParameterEntityDefinition;
    private T8DataEntityDefinition taskStateOutputParameterEntityDefinition;
    private T8DataEntityDefinition dataObjectStateEntityDefinition;

    private static final List<String> ENTITY_INSERT_PRIORITIES = ArrayLists.typeSafeList
    (
        T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_INPUT_PAR_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER,
        T8FlowManagerResource.STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER
    );

    public T8FlowStateUpdates()
    {
        super(ENTITY_INSERT_PRIORITIES);
    }

    public T8DataEntityDefinition getFlowStateEntityDefinition()
    {
        return flowStateEntityDefinition;
    }

    public void setFlowStateEntityDefinition(T8DataEntityDefinition flowStateEntityDefinition)
    {
        this.flowStateEntityDefinition = flowStateEntityDefinition;
    }

    public T8DataEntityDefinition getFlowStateInputParameterEntityDefinition()
    {
        return flowStateInputParameterEntityDefinition;
    }

    public void setFlowStateInputParameterEntityDefinition(T8DataEntityDefinition flowStateInputParameterEntityDefinition)
    {
        this.flowStateInputParameterEntityDefinition = flowStateInputParameterEntityDefinition;
    }

    public T8DataEntityDefinition getNodeStateEntityDefinition()
    {
        return nodeStateEntityDefinition;
    }

    public void setNodeStateEntityDefinition(T8DataEntityDefinition nodeStateEntityDefinition)
    {
        this.nodeStateEntityDefinition = nodeStateEntityDefinition;
    }

    public T8DataEntityDefinition getNodeStateInputParameterEntityDefinition()
    {
        return nodeStateInputParameterEntityDefinition;
    }

    public void setNodeStateInputParameterEntityDefinition(T8DataEntityDefinition nodeStateInputParameterEntityDefinition)
    {
        this.nodeStateInputParameterEntityDefinition = nodeStateInputParameterEntityDefinition;
    }

    public T8DataEntityDefinition getNodeStateOutputParameterEntityDefinition()
    {
        return nodeStateOutputParameterEntityDefinition;
    }

    public void setNodeStateOutputParameterEntityDefinition(T8DataEntityDefinition nodeStateOutputParameterEntityDefinition)
    {
        this.nodeStateOutputParameterEntityDefinition = nodeStateOutputParameterEntityDefinition;
    }

    public T8DataEntityDefinition getNodeStateExecutionParameterEntityDefinition()
    {
        return nodeStateExecutionParameterEntityDefinition;
    }

    public void setNodeStateExecutionParameterEntityDefinition(T8DataEntityDefinition nodeStateExecutionParameterEntityDefinition)
    {
        this.nodeStateExecutionParameterEntityDefinition = nodeStateExecutionParameterEntityDefinition;
    }

    public T8DataEntityDefinition getNodeStateOutputNodeEntityDefinition()
    {
        return nodeStateOutputNodeEntityDefinition;
    }

    public void setNodeStateOutputNodeEntityDefinition(T8DataEntityDefinition nodeStateOutputNodeEntityDefinition)
    {
        this.nodeStateOutputNodeEntityDefinition = nodeStateOutputNodeEntityDefinition;
    }

    public T8DataEntityDefinition getNodeStateWaitKeyEntityDefinition()
    {
        return nodeStateWaitKeyEntityDefinition;
    }

    public void setNodeStateWaitKeyEntityDefinition(T8DataEntityDefinition entityDefinition)
    {
        this.nodeStateWaitKeyEntityDefinition = entityDefinition;
    }

    public T8DataEntityDefinition getTaskStateEntityDefinition()
    {
        return taskStateEntityDefinition;
    }

    public void setTaskStateEntityDefinition(T8DataEntityDefinition taskStateEntityDefinition)
    {
        this.taskStateEntityDefinition = taskStateEntityDefinition;
    }

    public T8DataEntityDefinition getTaskStateInputParameterEntityDefinition()
    {
        return taskStateInputParameterEntityDefinition;
    }

    public void setTaskStateInputParameterEntityDefinition(T8DataEntityDefinition taskStateInputParameterEntityDefinition)
    {
        this.taskStateInputParameterEntityDefinition = taskStateInputParameterEntityDefinition;
    }

    public T8DataEntityDefinition getTaskStateOutputParameterEntityDefinition()
    {
        return taskStateOutputParameterEntityDefinition;
    }

    public void setTaskStateOutputParameterEntityDefinition(T8DataEntityDefinition taskStateOutputParameterEntityDefinition)
    {
        this.taskStateOutputParameterEntityDefinition = taskStateOutputParameterEntityDefinition;
    }

    public T8DataEntityDefinition getDataObjectStateEntityDefinition()
    {
        return dataObjectStateEntityDefinition;
    }

    public void setDataObjectStateEntityDefinition(T8DataEntityDefinition dataObjectStateEntityDefinition)
    {
        this.dataObjectStateEntityDefinition = dataObjectStateEntityDefinition;
    }
}