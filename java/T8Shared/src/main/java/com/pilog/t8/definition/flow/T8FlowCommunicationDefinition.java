package com.pilog.t8.definition.flow;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * This definition has been deprecated and replaced by T8TaskActivityCommunicationDefinition.
 * TODO:  Remove this definition completely.
 *
 * @author Bouwer du Preez
 */
public class T8FlowCommunicationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_COMMUNICATION";
    public static final String GROUP_IDENTIFIER = "@DG_FLOW_COMMUNICATION";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "FLOW_COMM_";
    public static final String DISPLAY_NAME = "Workflow Communication";
    public static final String DESCRIPTION = "A communication employed in a workflow process.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8FlowCommunicationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
}
