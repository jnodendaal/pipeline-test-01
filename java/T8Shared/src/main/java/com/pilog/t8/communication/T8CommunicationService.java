package com.pilog.t8.communication;

import com.pilog.t8.communication.event.T8CommunicationServiceListener;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationService
{
    /**
     * @return The identifier of this service
     */
    public String getIdentifier();

    /**
     * Initializes the service for operation.
     * @throws Exception
     */
    public void init() throws Exception;

    /**
     * Shuts down the service, releasing all acquired resources and stopping all
     * maintenance threads.
     */
    public void destroy();

    /**
     * Enables/disables this service.  If disabled queued messages will not be sent until re-enabled.
     * @param enabled Boolean true to set a state of enabled, false for disabled.
     */
    public void setEnabled(boolean enabled);

    /**
     * Sends the specified message immediately.
     * @param message The message to send.
     * @return The result of the communication.
     * @throws java.lang.Exception
     */
    public T8CommunicationResult sendMessage(T8CommunicationMessage message) throws Exception;

    /**
     * Queues the specified message to be sent as soon as possible.
     * @param message The message to queue.
     * @throws java.lang.Exception
     */
    public void queueMessage(T8CommunicationMessage message) throws Exception;

    /**
     * Removes the message from the queue and prevents it from sending.  If no message with the specified
     * communication instance id can be found on the queue then nothing happens.
     * @param messageIid
     * @throws java.lang.Exception
     */
    public void cancelMessage(String messageIid) throws Exception;

    /**
     * Adds a communication listener to the service.
     * @param listener
     */
    public void addServiceListener(T8CommunicationServiceListener listener);

    /**
     * Removes a communication listener from the service.
     * @param listener
     */
    public void removeServiceListener(T8CommunicationServiceListener listener);
}
