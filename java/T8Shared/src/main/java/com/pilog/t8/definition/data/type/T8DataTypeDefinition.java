package com.pilog.t8.definition.data.type;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataTypeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_TYPE";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_TYPE";
    public static final String DISPLAY_NAME = "Data Type";
    public static final String DESCRIPTION = "A data type that specifies a data value's type as used in the T8 context.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {CLASS_NAME};
    // -------- Definition Meta-Data -------- //

    public T8DataTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_NAME.toString(), "Class Name", "The canonical name of the Java class from which this data type can be instantiated."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8DataType getNewDataTypeInstance(T8DefinitionManager dataTypeContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName(getClassName()).getConstructor(T8DefinitionManager.class);
        return (T8DataType)constructor.newInstance(dataTypeContext);
    }

    public T8DataType getNewDataTypeInstance(T8DefinitionManager dataTypeContext, String dataTypeString) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName(getClassName()).getConstructor(T8DefinitionManager.class, String.class);
        return (T8DataType)constructor.newInstance(dataTypeContext, dataTypeString);
    }

    public String getClassName()
    {
        return (String)getDefinitionDatum(Datum.CLASS_NAME.toString());
    }

    public void setClassName(String className)
    {
        setDefinitionDatum(Datum.CLASS_NAME.toString(), className);
    }
}
