package com.pilog.t8.definition.filter;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.filter.T8DefinitionFilterClause;
import com.pilog.t8.definition.filter.T8DefinitionFilterCriterion;
import com.pilog.t8.definition.filter.T8DefinitionFilterCriterion.DefinitionFilterOperator;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFilterCriterionDefinition extends T8DefinitionFilterClauseDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DEFINITION_FILTER_CRITERION";
    public static final String IDENTIFIER_PREFIX = "FILTER_CRITERION_";
    public static final String DISPLAY_NAME = "Definition Filter Criterion";
    public static final String DESCRIPTION = "An object specifying a single filter criterion to be used by a definition filter.";
    public enum Datum {DATUM_IDENTIFIER,
                       OPERATOR,
                       FILTER_VALUE,
                       CASE_INSENSITIVE,
                       FILTER_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionFilterCriterionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATUM_IDENTIFIER.toString(), "Field Identifier", "The identifier of the data field on which this criterion is based."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATOR.toString(), "Operator", "The cirterion operator specifying the filter requirement.", DefinitionFilterOperator.EQUAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.CUSTOM_OBJECT, Datum.FILTER_VALUE.toString(), "Filter Value", "The criterion filter value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CASE_INSENSITIVE.toString(), "Case Insensitive", "If applicable, this setting determines the case sensitivity of the filter criterion.  This setting can only be applied to String type filter values."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FILTER_VALUE_EXPRESSION.toString(), "Filter Value Expression", "An alternative to specifying a filter value.  This expression will be evaluated during runtime to determine the filter value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATUM_IDENTIFIER.toString().equals(datumIdentifier))
        {
            // TODO:  Create a list of datums that can be used.
            return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(DefinitionFilterOperator.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionFilterClause getNewDefinitionFilterClauseInstance(T8SessionContext sessionContext, Map<String, Object> filterParameters)
    {
        String filterValueExpression;

        filterValueExpression = getFilterValueExpression();
        if ((filterValueExpression != null) && (filterValueExpression.trim().length() > 0))
        {
            try
            {
                Map<String, Object> expressionParameters;
                T8DefinitionFilterCriterion filterCriterion;
                ExpressionEvaluator evaluator;
                Object filterValue;

                evaluator = new ExpressionEvaluator();
                expressionParameters = new HashMap<String, Object>();
                if (sessionContext != null) expressionParameters.putAll(sessionContext.getSessionParameterMap());
                if (filterParameters != null) expressionParameters.putAll(filterParameters);

                filterValue = evaluator.evaluateExpression(filterValueExpression, expressionParameters, null);
                filterCriterion = new T8DefinitionFilterCriterion(this, getDatumIdentifier(), getOperator(), filterValue);
                filterCriterion.setCaseInsensitive(isCaseInsensitive());
                return filterCriterion;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while executing filter criterion value expression: " + filterValueExpression, e);
            }
        }
        else
        {
            T8DefinitionFilterCriterion filterCriterion;

            filterCriterion = new T8DefinitionFilterCriterion(this, getDatumIdentifier(), getOperator(), getFilterValue());
            filterCriterion.setCaseInsensitive(isCaseInsensitive());
            return filterCriterion;
        }
    }

    public String getDatumIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATUM_IDENTIFIER.toString());
    }

    public void setDatumIdentifier(String datumIdentifier)
    {
        setDefinitionDatum(Datum.DATUM_IDENTIFIER.toString(), datumIdentifier);
    }

    public DefinitionFilterOperator getOperator()
    {
        return DefinitionFilterOperator.valueOf((String)getDefinitionDatum(Datum.OPERATOR.toString()));
    }

    public void setOperator(DefinitionFilterOperator operator)
    {
        setDefinitionDatum(Datum.OPERATOR.toString(), operator.toString());
    }

    public Object getFilterValue()
    {
        return getDefinitionDatum(Datum.FILTER_VALUE.toString());
    }

    public void setFilterValue(Object filterValue)
    {
        setDefinitionDatum(Datum.FILTER_VALUE.toString(), filterValue);
    }

    public String getFilterValueExpression()
    {
        return (String)getDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString());
    }

    public void setFilterValueExpression(String expression)
    {
        setDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString(), expression);
    }

    public boolean isCaseInsensitive()
    {
        Boolean caseInsensitive;

        caseInsensitive = (Boolean)getDefinitionDatum(Datum.CASE_INSENSITIVE.toString());
        return caseInsensitive != null && caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        setDefinitionDatum(Datum.CASE_INSENSITIVE.toString(), caseInsensitive);
    }
}
