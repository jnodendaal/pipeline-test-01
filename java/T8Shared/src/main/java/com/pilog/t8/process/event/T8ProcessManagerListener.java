package com.pilog.t8.process.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8ProcessManagerListener extends EventListener
{
    public void processCompleted(T8ProcessCompletedEvent event);
    public void processFailed(T8ProcessFailedEvent event);
}
