package com.pilog.t8.data.filter;

import com.pilog.t8.data.T8DataEntity;
import java.util.List;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public interface T8DataFilterClause
{
    public enum DataFilterConjunction {AND, OR};

    /**
     * Returns true if this clause contains active criteria that will affect the
     * filter (i.e. is not empty).
     * @return true if this clause contains active criteria that will affect the
     * filter (i.e. is not empty).
     */
    public boolean hasCriteria();
    public T8DataFilterClause copy();
    public String getEntityIdentifier();
    public void setEntityIdentifier(String identifier);
    public T8DataFilterCriterion getFilterCriterion(String identifier);
    public List<T8DataFilterCriterion> getFilterCriterionList();
    public StringBuffer getWhereClause(T8DataTransaction dataAccessProvider, String sqlIdentifier);
    public List<Object> getWhereClauseParameters(T8DataTransaction dataAccessProvider);
    public boolean includesEntity(T8DataEntity entity);
    public Set<String> getFilterFieldIdentifiers();
}
