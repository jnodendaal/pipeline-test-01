package com.pilog.t8.data.tag;

import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8TagSet implements Serializable
{
    private final List<T8Tag> tags;

    public T8TagSet()
    {
        tags = new ArrayList<>();
    }

    public void clear()
    {
        tags.clear();
    }

    public int countTags(String tagIdentifier)
    {
        int count;

        count = 0;
        for (T8Tag tag : tags)
        {
            if (Objects.equals(tag.getTagIdentifier(), tagIdentifier))
            {
                count++;
            }
        }

        return count;
    }

    public Set<String> getTagIdentifierSet()
    {
        Set<String> tagIdentifierSet;

        tagIdentifierSet = new HashSet<>();
        for (T8Tag tag : tags)
        {
            tagIdentifierSet.add(tag.getTagIdentifier());
        }

        return tagIdentifierSet;
    }

    public List<String> getTagValues(String tagIdentifier)
    {
        List<String> tagValueList;

        tagValueList = new ArrayList<>();
        for (T8Tag tag : tags)
        {
            if (Objects.equals(tag.getTagIdentifier(), tagIdentifier))
            {
                tagValueList.add(tag.getTagValue());
            }
        }

        return tagValueList;
    }

    public String getTagValueString(String tagIdentifier, boolean includeNulls, boolean includeEmptyStrings, String separator)
    {
        StringBuffer tagValueString;

        tagValueString = new StringBuffer();
        for (T8Tag tag : tags)
        {
            if (Objects.equals(tag.getTagIdentifier(), tagIdentifier))
            {
                String tagValue;

                tagValue = tag.getTagValue();
                if ((includeNulls) || (tagValue != null))
                {
                    if ((includeEmptyStrings) || (!Strings.isNullOrEmpty(tagValue)))
                    {
                        if (tagValueString.length() > 0) tagValueString.append(separator);
                        tagValueString.append(tagValue);
                    }
                }
            }
        }

        return tagValueString.toString();
    }

    public List<T8Tag> getTags()
    {
        return new ArrayList<>(tags);
    }

    public T8Tag getTag(String tagIdentifier)
    {
        for (T8Tag tag : tags)
        {
            if (Objects.equals(tag.getTagIdentifier(), tagIdentifier))
            {
                return tag;
            }
        }

        return null;
    }

    public List<T8Tag> getTags(String tagIdentifier)
    {
        List<T8Tag> tagList;

        tagList = new ArrayList<>();
        for (T8Tag tag : tags)
        {
            if (Objects.equals(tag.getTagIdentifier(), tagIdentifier))
            {
                tagList.add(tag);
            }
        }

        return tagList;
    }

    public boolean containsTag(T8Tag tagToCheck)
    {
        for (T8Tag tag : tags)
        {
            if (tag.isEqual(tagToCheck)) return true;
        }

        return false;
    }

    public void addTags(Collection<T8Tag> tagsToAdd)
    {
        addTags(tagsToAdd, false);
    }

    public void addTags(Collection<T8Tag> tagsToAdd, boolean replaceExisting)
    {
        for (T8Tag tagToAdd : tagsToAdd)
        {
            addTag(tagToAdd, replaceExisting);
        }
    }

    public void addTag(T8Tag tag)
    {
        addTag(tag, false);
    }

    public void addTag(T8Tag tag, boolean replaceExisting)
    {
        if (replaceExisting)
        {
            removeTags(tag.getTagIdentifier());
            tags.add(tag);
        }
        else
        {
            if (!containsTag(tag))
            {
                tags.add(tag);
            }
        }
    }

    public boolean removeTag(T8Tag tagToRemove)
    {
        for (T8Tag tag : tags)
        {
            if (tag.isEqual(tagToRemove))
            {
                tags.remove(tag);
                return true;
            }
        }

        return false;
    }

    public List<T8Tag> removeTags(String tagIdentifier)
    {
        List<T8Tag> tagList;
        Iterator<T8Tag> tagIterator;

        tagList = new ArrayList<>();
        tagIterator = tags.iterator();
        while (tagIterator.hasNext())
        {
            T8Tag tag;

            tag = tagIterator.next();
            if (Objects.equals(tag.getTagIdentifier(), tagIdentifier))
            {
                tagList.add(tag);
                tagIterator.remove();
            }
        }

        return tagList;
    }

    /**
     * This method will return the String representation of this tag set as it
     * is used in data persistence.  For this reason, if there are no tags in
     * the set, this method returns null.
     * @return The String representation of this tag set.  Null if the tag set
     * contains no tags.
     */
    public String getTagString()
    {
        if (tags.isEmpty()) return null;
        else
        {
            StringBuilder tagStringBuilder;

            tagStringBuilder = new StringBuilder();
            tags.stream().map(T8Tag::getTagString).forEach(tagStringBuilder::append);

            return tagStringBuilder.toString();
        }
    }

    public T8TagSet copy()
    {
        T8TagSet copy;

        copy = new T8TagSet();
        copy.addTags(tags);
        return copy;
    }

    @Override
    public String toString()
    {
        String tagString;

        tagString = getTagString();
        return tagString != null ? tagString : super.toString();
    }
}
