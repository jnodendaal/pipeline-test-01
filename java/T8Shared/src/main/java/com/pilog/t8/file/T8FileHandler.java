package com.pilog.t8.file;

/**
 * @author Bouwer du Preez
 */
public interface T8FileHandler
{
    public void open() throws Exception;
    public void close() throws Exception;
}
