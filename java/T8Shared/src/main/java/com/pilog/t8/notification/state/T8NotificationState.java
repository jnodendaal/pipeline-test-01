package com.pilog.t8.notification.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import static com.pilog.t8.definition.functionality.T8FunctionalityManagerResource.STATE_FUNC_DE_IDENTIFIER;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8Notification.NotificationStatus;
import com.pilog.t8.state.T8StateDataSet;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.collections.HashMaps;
import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;
import java.util.Objects;
import com.pilog.t8.state.T8StateParameterMap;
import com.pilog.t8.state.T8StateUpdates;

import static com.pilog.t8.definition.notification.T8NotificationManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationState implements Serializable
{
    private final T8StateParameterMap parameterMap;
    private final String notificationIid;
    private final String senderOrganizationId;
    private final String senderIid;
    private final String notificationId;
    private final String senderId;
    private final String userId;
    private final boolean inserted;
    private T8Timestamp timeSent;
    private boolean updated;
    private int priority;
    private NotificationStatus status;

    public T8NotificationState(T8Notification notification)
    {
        this.notificationId = notification.getIdentifier();
        this.notificationIid = notification.getInstanceIdentifier();
        this.senderId = notification.getSenderIdentifier();
        this.senderIid = notification.getSenderInstanceIdentifier();
        this.senderOrganizationId = notification.getSenderOrganizationIdentifier();
        this.userId = notification.getUserIdentifier();
        this.timeSent = new T8Timestamp(notification.getSentTime());
        this.parameterMap = new T8StateParameterMap(null, STATE_NOTIFICATION_PAR_DE_IDENTIFIER, HashMaps.create(STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$NOTIFICATION_IID", notificationIid, STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$PARENT_PARAMETER_IID", null), null);
        this.status = notification.getStatus();
        this.updated = false;
        this.inserted = true;
    }

    public T8NotificationState(T8DataEntity notificationEntity, T8StateDataSet dataSet)
    {
        // Construct the notification state.
        this.notificationId = (String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_ID");
        this.notificationIid = (String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_IID");
        this.userId = (String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$USER_ID");
        this.priority = (Integer)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$PRIORITY");
        this.timeSent = (T8Timestamp)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$TIME_SENT");
        this.status = NotificationStatus.valueOf((String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$STATUS"));
        this.senderId = (String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$SENDER_ID");
        this.senderIid = (String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$SENDER_IID");
        this.senderOrganizationId = (String)notificationEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$SENDER_ORG_ID");
        this.updated = false;
        this.inserted = false;

        // Construct the notification parameters.
        this.parameterMap = new T8StateParameterMap(null, STATE_NOTIFICATION_PAR_DE_IDENTIFIER, HashMaps.create(STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$NOTIFICATION_IID", notificationIid, STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$PARENT_PARAMETER_IID", null), dataSet);
    }

    public T8DataEntity createEntity(T8DataTransaction tx) throws Exception
    {
        T8DataEntity newEntity;
        String entityId;

        entityId = STATE_FUNC_DE_IDENTIFIER;
        newEntity = tx.create(entityId, null);
        newEntity.setFieldValue(entityId + "$NOTIFICATION_IID", this.notificationIid);
        newEntity.setFieldValue(entityId + "$NOTIFICATION_ID", this.notificationId);
        newEntity.setFieldValue(entityId + "$USER_ID", this.userId);
        newEntity.setFieldValue(entityId + "$PRIORITY", this.priority);
        newEntity.setFieldValue(entityId + "$TIME_SENT", this.timeSent);
        newEntity.setFieldValue(entityId + "$STATUS", this.status.toString());
        newEntity.setFieldValue(entityId + "$SENDER_ID", this.senderId);
        newEntity.setFieldValue(entityId + "$SENDER_IID", this.senderIid);
        newEntity.setFieldValue(entityId + "$SENDER_ORG_ID", this.senderOrganizationId);

        return newEntity;
    }

    public boolean isUpdated()
    {
        return updated;
    }

    public void addUpdates(T8DataTransaction tx, T8StateUpdates updates) throws Exception
    {
        // Add this node states updates.
        if (inserted) updates.addInsert(createEntity(tx));
        else if (updated) updates.addUpdate(createEntity(tx));

        // Add updates of the parameter map.
        parameterMap.addUpdates(tx, updates);
    }

    public T8Notification getNotification()
    {
        T8Notification notification;

        // Create the notification.
        notification = new T8Notification(null, this.notificationId, this.notificationIid);
        notification.setUserIdentifier(this.userId);
        notification.setSenderIdentifier(this.senderId);
        notification.setSenderInstanceIdentifier(this.senderIid);
        notification.setSenderOrganizationIdentifier(this.senderOrganizationId);
        notification.setCreationTime(this.timeSent.getMilliseconds());
        notification.setStatus(this.status);

        // Add the notification parameters.
        notification.setParameters(new HashMap<>(parameterMap));
        return notification;
    }

    public String getNotificationInstanceIdentifier()
    {
        return notificationIid;
    }

    public String getNotificationIdentifier()
    {
        return notificationId;
    }

    public T8Timestamp getTimeSent()
    {
        return timeSent;
    }

    public Long getTimeSentMilliseconds()
    {
        return timeSent != null ? timeSent.getMilliseconds() : null;
    }

    public void setTimeSent(T8Timestamp timeSent)
    {
        if (!Objects.equals(this.timeSent, timeSent))
        {
            updated = true;
            this.timeSent = timeSent;
        }
    }

    public boolean isSent()
    {
        return ((timeSent != null) && (timeSent.getMilliseconds() > 0));
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        if (!Objects.equals(this.priority, priority))
        {
            updated = true;
            this.priority = priority;
        }
    }

    public NotificationStatus getStatus()
    {
        return status;
    }

    public void setStatus(NotificationStatus status)
    {
        if (!Objects.equals(this.status, status))
        {
            updated = true;
            this.status = status;
        }
    }

    public T8StateParameterMap getParameters()
    {
        return parameterMap;
    }

    public void setParameters(Map<String, Object> parameters)
    {
        this.parameterMap.clear();
        if (parameters != null)
        {
            this.parameterMap.putAll(parameters);
        }
    }
}
