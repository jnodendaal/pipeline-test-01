package com.pilog.t8.definition.data.source;

import com.pilog.t8.data.filter.T8DataFilter;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public interface T8SQLQueryDataSourceDefinition
{
    public StringBuilder createQuery(T8DataTransaction dataAccessProvider, T8DataFilter dataFilter, Map<String, String> fieldMapping);
}
