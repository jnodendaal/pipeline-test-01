package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.entity.T8DataEntitySerializer;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataEntity extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_ENTITY";

    public T8DtDataEntity()
    {
    }

    public T8DtDataEntity(T8DefinitionManager context)
    {
    }

    public T8DtDataEntity(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataEntity.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof T8DataEntity)
            {
                T8DataEntity dataEntity;

                dataEntity = (T8DataEntity)object;
                return T8DataEntitySerializer.serializeToJSON(dataEntity, true);
            }
            else throw new IllegalArgumentException("Type expected: " + getDataTypeClassName() + " Type found: " + object.getClass().getCanonicalName());
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            throw new UnsupportedOperationException("Deserialization of T8DataEntity not yet implemented.");
        }
        else return null;
    }
}
