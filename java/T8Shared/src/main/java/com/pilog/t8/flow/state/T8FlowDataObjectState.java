package com.pilog.t8.flow.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8FlowDataObjectState implements Serializable
{
    private final T8FlowState parentFlowState;
    private String dataObjectIid;
    private String dataObjectId;
    private boolean updated;
    private boolean inserted;

    public T8FlowDataObjectState(T8FlowState parentFlowState, String dataObjectIdentifier, String dataObjectInstanceIdentifier)
    {
        this.parentFlowState = parentFlowState;
        this.dataObjectId = dataObjectIdentifier;
        this.dataObjectIid = dataObjectInstanceIdentifier;
        this.updated = false;
        this.inserted = true;
    }

    public T8FlowDataObjectState(T8FlowState parentFlowState, T8DataEntity dataObjectEntity)
    {
        // Construct the task state.
        this.parentFlowState = parentFlowState;
        this.dataObjectIid = (String)dataObjectEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER + "$DATA_OBJECT_IID");
        this.dataObjectId = (String)dataObjectEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER + "$DATA_OBJECT_ID");
        this.updated = false;
        this.inserted = false;
    }

    public T8DataEntity createEntity(T8DataEntityDefinition entityDefinition)
    {
        String entityIdentifier;
        T8DataEntity newEntity;

        entityIdentifier = entityDefinition.getIdentifier();
        newEntity = entityDefinition.getNewDataEntityInstance();
        newEntity.setFieldValue(entityIdentifier + "$FLOW_IID", parentFlowState.getFlowIid());
        newEntity.setFieldValue(entityIdentifier + "$DATA_OBJECT_IID", dataObjectIid);
        newEntity.setFieldValue(entityIdentifier + "$DATA_OBJECT_ID", dataObjectId);
        return newEntity;
    }

    public void statePersisted()
    {
        // Reset this state's flags.
        inserted = false;
        updated = false;
    }

    public void addUpdates(T8FlowStateUpdates updates)
    {
        // Add this object's states updates.
        if (inserted) updates.addInsert(createEntity(updates.getDataObjectStateEntityDefinition()));
        else if (updated) updates.addUpdate(createEntity(updates.getDataObjectStateEntityDefinition()));
    }

    public String getFlowIid()
    {
        return parentFlowState.getFlowIid();
    }

    public String getFlowId()
    {
        return parentFlowState.getFlowId();
    }

    public String getDataObjectIid()
    {
        return dataObjectIid;
    }

    public void setDataObjectIid(String dataObjectIid)
    {
        if (!Objects.equals(this.dataObjectIid, dataObjectIid))
        {
            updated = true;
            this.dataObjectIid = dataObjectIid;
        }
    }

    public String getDataObjectId()
    {
        return dataObjectId;
    }

    public void setDataObjectId(String dataObjectId)
    {
        if (!Objects.equals(this.dataObjectId, dataObjectId))
        {
            updated = true;
            this.dataObjectId = dataObjectId;
        }
    }
}
