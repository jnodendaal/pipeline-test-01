package com.pilog.t8.definition.data.source.sequence;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8SequenceDataSourceDefinition extends T8DataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_SEQUENCE";
    public static final String DISPLAY_NAME = "Sequence Data Source";
    public static final String DESCRIPTION = "A data source that reads numbers from a predefined sequence.";
    public static final String IDENTIFIER_PREFIX = "DS_SEQ_";
    public static final String VERSION = "0";
    public enum Datum {CONNECTION_IDENTIFIER,
                       SEQUENCE_NAME};
    // -------- Definition Meta-Data -------- //

    public static final String SEQUENCE_FIELD_IDENTIFIER = "$SEQUENCE";
    
    public T8SequenceDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection to which this data source will connect to retrieve its data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEQUENCE_NAME.toString(), "Sequence Name", "The name of the sequence from which this data source reads."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction dataAccessProvider)
    {
        try
        {
            Constructor constructor;
            
            constructor = Class.forName("com.pilog.t8.data.source.sequence.T8SequenceDataSource").getConstructor(this.getClass(), T8DataTransaction.class);
            return (T8DataSource)constructor.newInstance(this, dataAccessProvider);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }  
    
    public String getConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString());
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString(), connectionIdentifier);
    }    
    
    public String getSequenceName()
    {
        return (String)getDefinitionDatum(Datum.SEQUENCE_NAME.toString());
    }
    
    public void setSequenceName(String sequenceName)
    {
        setDefinitionDatum(Datum.SEQUENCE_NAME.toString(), sequenceName);
    }
}
