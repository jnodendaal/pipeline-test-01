package com.pilog.t8.ui.event;

import com.pilog.t8.ui.T8DeveloperView;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8ViewHeaderChangedEvent extends EventObject
{
    private final String newHeader;

    public T8ViewHeaderChangedEvent(T8DeveloperView source, String newHeader)
    {
        super(source);
        this.newHeader = newHeader;
    }

    public T8DeveloperView getView()
    {
        return (T8DeveloperView)source;
    }

    public String getNewHeader()
    {
        return newHeader;
    }
}
