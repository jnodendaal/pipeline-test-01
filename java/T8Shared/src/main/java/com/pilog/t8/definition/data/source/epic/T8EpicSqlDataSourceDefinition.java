package com.pilog.t8.definition.data.source.epic;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import com.pilog.t8.definition.data.source.cte.T8CTEDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EpicSqlDataSourceDefinition extends T8DataSourceDefinition implements T8SQLQueryDataSourceDefinition, T8DataConnectionBasedSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_EPIC_SQL";
    public static final String DISPLAY_NAME = "EPIC SQL Data Source";
    public static final String DESCRIPTION = "A data source that compiles SQL from dynamic EPIC scripts.";
    public enum Datum
    {
        CONNECTION_IDENTIFIER,
        QUERY_TIMEOUT,
        CTE_DEFINITION_IDENTIFIERS,
        DEFAULT_WHERE_CLAUSE,
        SCRIPT_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public T8EpicSqlDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection to which this data source will connect to retrieve its data."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.CTE_DEFINITION_IDENTIFIERS.toString(), "CTE Definitions", "The CTE's that will be added to this query when it is executed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.QUERY_TIMEOUT.toString(), "Query Timeout", "The number of seconds allowed for queries on this data source to execute before being terminated.", 60));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DEFAULT_WHERE_CLAUSE.toString(), "Default WHERE Clause", "If this flag is set as true, the WHERE clause for any filter supplied to this data source will be generated automatically and not by the EPIC script.", true));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.SCRIPT_DEFINITION.toString(), "Script", "The EPIC script to compile the SQL used by this data source."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else if (Datum.CTE_DEFINITION_IDENTIFIERS.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CTEDefinition.GROUP_IDENTIFIER));
        else if (Datum.SCRIPT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8EpicSqlDataSourceScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        // Check the connection identifier.
        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getConnectionIdentifier()))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.CONNECTION_IDENTIFIER.toString(), "Connection identifier not specified.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction accessProvider)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.source.epic.T8EpicSqlDataSource", new Class<?>[]{T8EpicSqlDataSourceDefinition.class, T8DataTransaction.class}, this, accessProvider);
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return getConnectionIdentifier();
    }

    @Override
    public StringBuilder createQuery(T8DataTransaction dataAccessProvider, T8DataFilter dataFilter, Map<String, String> fieldMapping)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getConnectionIdentifier()
    {
        return getDefinitionDatum(Datum.CONNECTION_IDENTIFIER);
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER, connectionIdentifier);
    }

    public int getQueryTimeout()
    {
        Integer value;

        value = (Integer)getDefinitionDatum(Datum.QUERY_TIMEOUT.toString());
        return value != null ? value : 0;
    }

    public void setQueryTimeout(int timeout)
    {
        setDefinitionDatum(Datum.QUERY_TIMEOUT.toString(), timeout);
    }

    public boolean isDefaultWhereClause()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.DEFAULT_WHERE_CLAUSE);
        return value != null ? value : true;
    }

    public void setDefaultWhereClause(boolean defaultWhereClause)
    {
        setDefinitionDatum(Datum.DEFAULT_WHERE_CLAUSE, defaultWhereClause);
    }

    public T8EpicSqlDataSourceScriptDefinition getScriptDefinition()
    {
        return getDefinitionDatum(Datum.SCRIPT_DEFINITION);
    }

    public void setScriptDefinition(T8EpicSqlDataSourceScriptDefinition scriptDefinition)
    {
        setDefinitionDatum(Datum.SCRIPT_DEFINITION, scriptDefinition);
    }

    public List<String> getCteDefinitionIds()
    {
        return getDefinitionDatum(Datum.CTE_DEFINITION_IDENTIFIERS);
    }

    public void setCteDefinitionIds(List<String> definitions)
    {
        setDefinitionDatum(Datum.CTE_DEFINITION_IDENTIFIERS, definitions);
    }
}
