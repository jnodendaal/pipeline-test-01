package com.pilog.t8.flow.key;

import com.pilog.t8.flow.state.T8FlowNodeState;

/**
 * @author Bouwer du Preez
 */
public class T8NodeCompletionExecutionKey extends T8DefaultFlowExecutionKey
{
    private final String completedNodeIdentifier;
    
    public T8NodeCompletionExecutionKey(T8FlowNodeState nodeState, String completedNodeIdentifier)
    {
        super(nodeState);
        this.completedNodeIdentifier = completedNodeIdentifier;
    }

    public String getCompletedNodeIdentifier()
    {
        return completedNodeIdentifier;
    }
}
