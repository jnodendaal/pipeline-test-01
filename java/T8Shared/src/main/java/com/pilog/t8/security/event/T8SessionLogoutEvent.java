package com.pilog.t8.security.event;

/**
 * @author Bouwer du Preez
 */
public class T8SessionLogoutEvent extends T8SessionEvent
{
    private final String sessionIdentifier;
    
    public T8SessionLogoutEvent(String sessionIdentifier)
    {
        super(sessionIdentifier); // The source of the event is currently the identifier of the session but this may change in future.
        this.sessionIdentifier = sessionIdentifier;
    }

    public String getSessionIdentifier()
    {
        return sessionIdentifier;
    }
}
