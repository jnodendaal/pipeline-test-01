package com.pilog.t8.definition.flow.node.event;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8SignalAccumulationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_FLOW_SIGNAL_ACCUMULATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_FLOW_SIGNAL_ACCUMULATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Flow Signal Accumulation Script";
    public static final String DESCRIPTION = "A script that is executed when a specific flow signal is received in order to accumulate signal data in the flow.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_COMPLETE = "$P_COMPLETE";
    public static final String PARAMETER_SIGNAL_PARAMETERS = "$P_SIGNAL_PARAMETERS";
    public static final String PARAMETER_FLOW_PARAMETERS = "$P_FLOW_PARAMETERS";

    public T8SignalAccumulationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> inputParameters;

        inputParameters = new ArrayList<T8DataParameterDefinition>();
        inputParameters.add(new T8DataParameterDefinition(PARAMETER_FLOW_PARAMETERS, "Flow Parameters", "The parameters of the flow's current state.", T8DataType.MAP));
        inputParameters.add(new T8DataParameterDefinition(PARAMETER_SIGNAL_PARAMETERS, "Signal Parameters", "The map of parameters received from the signal.", T8DataType.MAP));
        return inputParameters;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> outputParameters;

        outputParameters = new ArrayList<T8DataParameterDefinition>();
        outputParameters.add(new T8DataParameterDefinition(PARAMETER_FLOW_PARAMETERS, "Flow Parameters", "The flow parameters to set on the flow state.", T8DataType.MAP));
        outputParameters.add(new T8DataParameterDefinition(PARAMETER_COMPLETE, "Completion Flag", "A flag to indicate if accumulation is complete and the signal can be finalized.", T8DataType.BOOLEAN));
        return outputParameters;
    }
}
