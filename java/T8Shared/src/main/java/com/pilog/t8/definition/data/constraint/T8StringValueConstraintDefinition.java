/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.constraint;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8StringValueConstraintDefinition extends T8DataValueConstraintDefinition
{
// -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_VALUE_CONSTRAINT_STRING";
    public static final String DISPLAY_NAME = "String Value Constraint";
    public static final String DESCRIPTION = "A String Value Constraint";
    public static final String IDENTIFIER_PREFIX = "CONSTR_STR_";

    public enum Datum
    {
        LENGTH
    };
// -------- Definition Meta-Data -------- //
    public T8StringValueConstraintDefinition(String identifier)
    {
        super(identifier);
    }

    public T8StringValueConstraintDefinition(String identifier, int length)
    {
        super(identifier);
        setLength(length);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.LENGTH.toString(), "Length", "The length og the string value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              com.pilog.t8.definition.T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void validate(String fieldIdentifier, Object value)
    {
        if(!enforceValidation())
                return;
        if(value == null)
            return;
        if(!(value instanceof String))
            throw new T8DataValidationException(value, "Value is not of type String. The field " + fieldIdentifier + " requires a String value but found " + value.getClass().getCanonicalName());

        //if the length have not been set or is less than zero then assume it is disabled
        if(getLength() == null || getLength() < 0)
            return;

        String stringValue = (String)value;
        if(stringValue.length() > getLength())
            throw new T8DataValidationException(value, "The Value " + value + " exceeds the maximum allowed length(" + getLength() + ")  for the field " + fieldIdentifier + ".");
    }

    @Override
    public String getSQLRepresentation()
    {
        return "(" + getLength() + ")";
    }

    public Integer getLength()
    {
        return (Integer)getDefinitionDatum(Datum.LENGTH.toString());
    }

    public void setLength(Integer length)
    {
        setDefinitionDatum(Datum.LENGTH.toString(), length);
    }
}
