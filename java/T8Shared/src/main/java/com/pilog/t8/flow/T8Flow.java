package com.pilog.t8.flow;

import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8Flow
{
    public enum FlowStatus {QUEUED, ACTIVE, COMPLETED, FAILED, FINALIZED};
    // Events used when logging flow history.
    public enum FlowHistoryEvent {QUEUED, STARTED, COMPLETED, FAILED};
    // Events used when logging flow data object history.
    public enum FlowDataObjectHistoryEvent{ALLOCATED};

    // As long as this method returns true, the object can be used within a synchronized block for addition of new execution threads and checks on current execution status.
    public boolean isExecuting();
    // Returns the controller of the flow context in which this flow is executing.
    public T8FlowController getFlowController();
    // Returns an identifier for the flow instance.
    public String getInstanceIdentifier();
    // Returns the flow definition.
    public T8WorkFlowDefinition getDefinition();
    // Returns a definition of the current state of the flow and all of its nodes.
    public T8FlowState getState();
    // Used internally to the controller to persist a flow state (in order to capture latest updates).
    public void persistState();
    // Returns the current status of the flow (a summary of all node statuses - no task list is included in the status returned from the flow).
    public T8FlowStatus getStatus();
    // Returns the list of currently executing nodes.
    public List<T8FlowNode> getExecutingNodes();
    // Cancels execution of the specified node and attempts to roll back any transactional changes.
    public boolean cancelNode(String nodeIdentifier) throws Exception;
    // Start execution of the flow from its existing state.
    public void startFlow() throws Exception;
    // Start execution of the flow from a null state using the input parameters.
    public void startFlow(Map<String, Object> inputParameters) throws Exception;
    // Resumes execution of the flow using the specified execution key.  If no key is specified (null) all active flow nodes will be resumed.
    public void executeFlow(T8FlowExecutionKey key) throws Exception;
    // Completes the execution of the flow.  All current execution is stopped.  This method is called internally when an End Event is encountered.
    public void completeFlow(Map<String, Object> outputParameters);
    // Stops all execution in the flow.  This method is called on all active flows when the server flow controller shuts down.
    public void stopFlow();
    // Finalizes the flow.  This method is called right before the flow is terminated and removed from the active flow list by the flow manager.
    public void finalizeFlow();
    // Returns a list of the input node identifiers for the specified node.
    public List<String> getInputNodeIdentifiers(String nodeIdentifier);
    // Returns a list of the output node identifiers for the specified node.
    public List<String> getOutputNodeIdentifiers(String nodeIdentifier);
    // Claims the specified task for the user session context supplied.
    public T8FlowTaskState claimTask(T8Context context, String taskIid) throws Exception;
    // Escalate the priority of the specified task.
    public T8FlowTaskState escalateTask(T8Context context, T8TaskEscalationTrigger trigger, String taskIid) throws Exception;
    // Reassigns the specified task to a new user and/or workflow profile.
    public void reassignTask(String taskIid, String userId, String profileId) throws Exception;
}
