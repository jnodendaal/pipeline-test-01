package com.pilog.t8.definition.file;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtTimeUnit;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FileManagerResource implements T8DefinitionResource
{
    public static final String OPERATION_OPEN_FILE_CONTEXT = "@OP_OPEN_FILE_CONTEXT";
    public static final String OPERATION_CLOSE_FILE_CONTEXT = "@OP_CLOSE_FILE_CONTEXT";
    public static final String OPERATION_CREATE_DIRECTORY = "@OP_CREATE_DIRECTORY";
    public static final String OPERATION_DELETE_FILE = "@OP_DELETE_FILE";
    public static final String OPERATION_RENAME_FILE = "@OP_RENAME_FILE";
    public static final String OPERATION_UPLOAD_FILE_DATA = "@OP_UPLOAD_FILE_DATA";
    public static final String OPERATION_DOWNLOAD_FILE_DATA = "@OP_DOWNLOAD_FILE_DATA";
    public static final String OPERATION_GET_FILE_DETAILS = "@OP_GET_FILE_DETAILS";
    public static final String OPERATION_GET_FILE_DETAILS_LIST = "@OP_GET_FILE_DETAILS_LIST";
    public static final String OPERATION_CHECK_FILE_CONTEXT_EXISTENCE = "@OP_FILE_CONTEXT_EXISTS";
    public static final String OPERATION_CHECK_FILE_EXISTENCE = "@OP_CHECK_FILE_EXISTENCE";
    public static final String OPERATION_GET_FILE_SIZE = "@OP_GET_FILE_SIZE";
    public static final String OPERATION_GET_MD5_CHECKSUM = "@OP_GET_MD5_CHECKSUM";
    public static final String OPERATION_ADD_FILE_LISTENER = "@OP_ADD_FILE_LISTENER";
    public static final String OPERATION_REMOVE_FILE_LISTENER = "@OP_REMOVE_FILE_LISTENER";
    public static final String OPERATION_GET_FILE_LISTENER_DEFINITIONS = "@OP_GET_FILE_LISTENER_DEFINITIONS";

    public static final String PARAMETER_EXPIRATION_TIME = "$P_EXPIRATION_TIME";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_FILE_DETAILS = "$P_FILE_DETAILS";
    public static final String PARAMETER_FILE_DETAILS_LIST = "$P_FILE_DETAILS_LIST";
    public static final String PARAMETER_BYTE_SIZE = "$P_BYTE_SIZE";
    public static final String PARAMETER_MD5_CHECKSUM_HEX = "$P_MD5_CHECKSUM_HEX";
    public static final String PARAMETER_BINARY_DATA = "$P_BINARY_DATA";
    public static final String PARAMETER_EXCEPTION = "$P_EXCEPTION";
    public static final String PARAMETER_CONTEXT_ID = "$P_CONTEXT_ID";
    public static final String PARAMETER_CONTEXT_IID = "$P_CONTEXT_IID";
    public static final String PARAMETER_FILE_PATH = "$P_FILE_PATH";
    public static final String PARAMETER_FILE_NAME = "$P_FILE_NAME";
    public static final String PARAMETER_FILE_OFFSET = "$P_FILE_OFFSET";
    public static final String PARAMETER_CLASS_NAME = "$P_CLASS_NAME";
    public static final String PARAMETER_FILE_LISTENER_DEFINITION = "$P_FILE_LISTENER_DEFINITION";
    public static final String PARAMETER_FILE_LISTENER_DEFINITION_LIST = "$P_FILE_LISTENER_DEFINITION_LIST";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_OPEN_FILE_CONTEXT);
            definition.setMetaDisplayName("Open File Context");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$OpenFileContextOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance Id", "The unique id of the file context instance to create and which will be used to identify the context throughout its lifespan.  The id must be a valid T8 GUID.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_ID, "Context Id", "The id of the predefined type of context to open.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EXPIRATION_TIME, "Expiration Time", "The T8 time unit that defines the amount of time for which the created context will be maintained from last recorded activity before it will be deleted.", new T8DtTimeUnit()));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "A boolean value inicating whether or not the file context was opened successfully.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CLOSE_FILE_CONTEXT);
            definition.setMetaDisplayName("Close File Context");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$CloseFileContextOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance Id", "The id of the file context instance to close.", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "A boolean value inicating whether or not the file context was closed successfully.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CREATE_DIRECTORY);
            definition.setMetaDisplayName("Create Directory");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$CreateDirectoryOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance Id", "The id of the file context instance where the directory will be created.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The remote file path to be created in the specified context.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_FILE);
            definition.setMetaDisplayName("Delete File");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$DeleteFileOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance Id", "The id of the file context instance where the file to be deleted is located.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The remote file path identifying in the specified context to be deleted.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_RENAME_FILE);
            definition.setMetaDisplayName("Rename File");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$DeleteFileOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance ID", "The ID of the file context instance where the file to be renamed is located.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The path of the file to be renamed wihtin the specified context intance.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_NAME, "File Name", "The name to which the specified file will be renamed.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_UPLOAD_FILE_DATA);
            definition.setMetaDisplayName("Upload File Data");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$UploadFileDataOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance ID", "The ID of the file context instance where the file is to be uploaded to.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The remote file path for the file being uploaded.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_BINARY_DATA, "File Byte Data", "The byte array containing the content of the file.", T8DataType.BYTE_ARRAY));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_BYTE_SIZE, "File Byte Size", "The number of data bytes which were uploaded to the server side.", T8DataType.LONG));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_DOWNLOAD_FILE_DATA);
            definition.setMetaDisplayName("Download File Data");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$DownloadFileDataOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_FILE_DETAILS);
            definition.setMetaDisplayName("Get File Details");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$GetFileDetailsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance ID", "The ID of the file context instance where the file is located.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The file path where the target file is located.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_DETAILS, "File Details", "The details of the requested file.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_FILE_DETAILS_LIST);
            definition.setMetaDisplayName("Get File List");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$GetFileListOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONTEXT_IID, "Context Instance ID", "The ID of the file context instance where the files are located.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_PATH, "File Path", "The the path to the directory where the requested files are located.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CHECK_FILE_CONTEXT_EXISTENCE);
            definition.setMetaDisplayName("Check File Context Existence");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$CheckFileContextExistenceOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_CHECK_FILE_EXISTENCE);
            definition.setMetaDisplayName("Check File Existence");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$CheckFileExistenceOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_FILE_SIZE);
            definition.setMetaDisplayName("Get File Size");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$GetFileSizeOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_MD5_CHECKSUM);
            definition.setMetaDisplayName("Get MD5 Checksum");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$GetMD5ChecksumOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_ADD_FILE_LISTENER);
            definition.setMetaDisplayName("Add File Listener");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$AddFileListenerOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_REMOVE_FILE_LISTENER);
            definition.setMetaDisplayName("Remove File Listener");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$RemoveFileListenerOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_GET_FILE_LISTENER_DEFINITIONS);
            definition.setMetaDisplayName("Get File Listener Definitions");
            definition.setClassName("com.pilog.t8.file.T8FileManagerOperations$GetFileListenerDefinitionsOperation");
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
