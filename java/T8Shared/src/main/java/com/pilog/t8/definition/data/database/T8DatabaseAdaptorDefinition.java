package com.pilog.t8.definition.data.database;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DatabaseAdaptorDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATABASE_ADAPTOR";
    public static final String GROUP_IDENTIFIER = "@DG_DATABASE_ADAPTOR";
    public static final String GROUP_NAME = "Database Adaptors";
    public static final String GROUP_DESCRIPTION = "Definitions that describe the implementation specifics of various database vendors and how the system adapts when these databases are used.";
    public static final String STORAGE_PATH = "/database_adaptors";
    public static final String DISPLAY_NAME = "Database Adaptor";
    public static final String DESCRIPTION = "A class used to adapt generic funcionality to various database specfic features.";
    public enum Datum {CLASS_NAME,
                       OPERATION_IMPLEMENTATION_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DatabaseAdaptorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_NAME.toString(), "Class Name", "The Class Name to use when instantiating the adaptor."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8DatabaseAdaptor getNewDatabaseAdaptorInstance()
    {
        return T8Reflections.getInstance(getClassName(), new Class<?>[]{this.getClass()}, this);
    }

    public String getClassName()
    {
        return getDefinitionDatum(Datum.CLASS_NAME);
    }

    public void setClassName(String operator)
    {
        setDefinitionDatum(Datum.CLASS_NAME, operator);
    }
}
