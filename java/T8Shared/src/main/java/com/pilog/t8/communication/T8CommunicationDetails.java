package com.pilog.t8.communication;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationDetails extends Comparable<T8CommunicationDetails>
{
    public int getMessagePriority();
    public String getRecipientRecordID();
    public void setRecipientRecordID(String recipientRecordID);
    public String getRecipientDefinitionIdentifier();
    public void setRecipientDefinitionIdentifier(String recipientDefinitionIdentifier);
    public String getRecipientDisplayName();
    public String getRecipientAddress();
    public Map<String, Object> getAdditionalDetails();
    public void setAdditionalDetails(Map<String, Object> additionalDetails);
    public void verifyRecipientAddress() throws Exception;
}
