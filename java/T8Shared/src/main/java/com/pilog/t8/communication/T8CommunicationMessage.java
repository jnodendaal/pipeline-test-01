package com.pilog.t8.communication;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationMessage
{
    public enum MessageHistoryEvent
    {
        QUEUED,
        SENT,
        FAILED,
        CANCELLED
    }

    public enum MessagePriority
    {
        LOW(0, 29),
        NORMAL(30, 69),
        HIGH(70, 100);

        private final int floorValue;
        private final int ceilingValue;

        MessagePriority(int floorValue, int ceilingValue)
        {
            this.floorValue = floorValue;
            this.ceilingValue = ceilingValue;
        }

        public int getFloorValue()
        {
            return floorValue;
        }

        public int getCeilingValue()
        {
            return ceilingValue;
        }
    }

    /**
     * Returns the instance identifier of this message.
     * @return The instance identifier of this message.
     */
    public String getInstanceIdentifier();

    /**
     * Returns the identifier of the message template from which this message
     * was generated.
     * @return The identifier of the message template from which this message
     * was generated.
     */
    public String getIdentifier();

    /**
     * Returns the identifier of the communication setup from which this message
     * was generated.
     * @return The identifier of the communication setup from which this message
     * was generated.
     */
    public String getCommunicationIdentifier();

    /**
     * Sets the identifier of the communication from which this message was
     * generated.
     * @param identifier The identifier of the communication from which this
     * message was generated.
     */
    public void setCommunicationIdentifier(String identifier);

    /**
     * Returns the instance identifier of the communication setup instance from
     * which this message was generated.
     * @return The instance identifier of the communication setup instance from
     * which this message was generated.
     */
    public String getCommunicationInstanceIdentifier();

    /**
     * Sets the instance identifier of the communication from which this message
     * was generated.
     * @param instanceIdentifier The instance identifier of the communication
     * from which this message was generated.
     */
    public void setCommunicationInstanceIdentifier(String instanceIdentifier);

    /**
     * Returns the identifier of the user that initiated the transmission of
     * this message (if any).
     * @return The user identifier of the initiator of this message.
     */
    public String getSenderIdentifier();

    /**
     * Returns the identifier of the user profile that initiated the
     * transmission of this message (if any).
     * @return The user profile identifier of the initiator of this message.
     */
    public String getSenderInstanceIdentifier();

    /**
     * Returns the definition identifier of the recipient of this message.
     * @return The definition identifier of the recipient of this message if available.
     */
    public String getRecipientIdentifier();

    /**
     * Returns the record id of the recipient of this message.
     * @return The record id of the recipient of this message if available.
     */
    public String getRecipientRecordID();

    /**
     * Returns the address of the recipient of this message.
     * @return The address of the recipient of this message.
     */
    public String getRecipientAddress();

    /**
     * Returns the name of the recipient used for UI display purposes.
     * @return The name of the recipient used for UI display purposes.
     */
    public String getRecipientDisplayName();

    /**
     * Returns the content of this message.
     * @return The content of this message.
     */
    public String getContent();

    /**
     * Returns the subject of this message.
     * @return The subject of this message.
     */
    public String getSubject();

    /**
     * Returns the priority of this message.
     * @return The priority of this message.
     */
    public int getPriority();

    /**
     * Returns the number of times that transmission of this message has failed.
     * @return The number of times that transmission of this message has failed.
     */
    public int getFailureCount();

    /**
     * Sets the failure count for this message.
     * @param count The number of time that transmission of this message has
     * failed.
     */
    public void setFailureCount(int count);

    /**
     * Returns the identifier of the service that is responsible for
     * transmission of this message.
     * @return The identifier of the service that is responsible for
     * transmission of this message.
     */
    public String getServiceIdentifier();

    /**
     * A method to set the service identifier that is responsible for
     * transmission of this message.
     * @param identifier The identifier of the service through which this
     * message will be sent.
     */
    public void setServiceIdentifier(String identifier);

    /**
     * Returns the time at which this message was queued on the service responsible for its transmission.
     * @return The time (in milliseconds since epoch) when this message was queued.
     */
    public long getTimeQueued();

    /**
     * Sets the time at which this message was queued on the service responsible for its transmission.
     * @param timeQueued The time (in milliseconds since epoch) when this message was queued.
     */
    public void setTimeQueued(long timeQueued);

    /**
     * Returns the state of the message.
     * @return The state of the message.
     */
    public T8CommunicationMessageState getState();
}
