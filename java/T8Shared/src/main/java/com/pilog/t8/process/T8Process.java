package com.pilog.t8.process;

import com.pilog.t8.definition.process.T8ProcessDefinition;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8Process
{
    // Status used for tracking life-cycle of the process.
    public enum T8ProcessStatus {CREATED, IN_PROGRESS, COMPLETED, FAILED, CANCELLED, PAUSED, STOPPED};
    // Events used when logging process history.
    public enum T8ProcessHistoryEvent {CREATED, STARTED, STOPPED, PAUSED, RESUMED, CANCELLED, COMPLETED, FAILED, FINALIZED};

    public String getIid();
    public T8ProcessDefinition getDefinition();
    public T8ProcessState getState();
    public T8ProcessStatus getStatus();
    public Map<String, Object> execute(Map<String, Object> processParameters) throws Exception;
    public void stop() throws Exception; // Stops execution without cancelling already completed progress.
    public void cancel() throws Exception; // Cancels all completed progress, rolling back changes where possible.
    public void pause() throws Exception; // Pauses execution of the process.
    public void resume() throws Exception; // Resumes a previously paused process.
    public double getProgress() throws Exception; // Returns the progress of the process execution.
    public T8ProcessDetails getDetails() throws Exception; // Returns the process details.
}
