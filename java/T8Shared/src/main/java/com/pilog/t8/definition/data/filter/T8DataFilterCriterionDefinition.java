package com.pilog.t8.definition.data.filter;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilterCriterionDefinition extends T8DataFilterClauseDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_FILTER_CRITERION";
    public static final String IDENTIFIER_PREFIX = "FILTER_CRITERION_";
    public static final String DISPLAY_NAME = "Data Filter Criterion";
    public static final String DESCRIPTION = "An object specifying a single filter criterion to be used by a data filter.";
    public enum Datum {FIELD_IDENTIFIER,
                       OPERATOR,
                       FILTER_VALUE,
                       CASE_INSENSITIVE,
                       FILTER_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8DataFilterCriterionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FIELD_IDENTIFIER.toString(), "Field Identifier", "The identifier of the data field on which this criterion is based."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATOR.toString(), "Operator", "The cirterion operator specifying the filter requirement.", DataFilterOperator.EQUAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.CUSTOM_OBJECT, Datum.FILTER_VALUE.toString(), "Filter Value", "The criterion filter value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CASE_INSENSITIVE.toString(), "Case Insensitive", "If applicable, this setting determines the case sensitivity of the filter criterion.  This setting can only be applied to String type filter values."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.FILTER_VALUE_EXPRESSION.toString(), "Filter Value Expression", "An alternative to specifying a filter value.  This expression will be evaluated during runtime to determine the filter value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8DataFilterDefinition filterDefinition;
            String entityId;

            filterDefinition = getDataFilterDefinition(definitionContext);
            entityId = filterDefinition.getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;
                List<T8DataEntityFieldDefinition> fieldDefinitions;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
            }
            else return new ArrayList<>();
        }
        else if (Datum.OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(DataFilterOperator.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    private T8DataFilterDefinition getDataFilterDefinition(T8DefinitionContext definitionContext) throws Exception
    {
        List<T8DefinitionTypeMetaData> definitionMetaDatas = definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER);

        for (T8DefinitionTypeMetaData t8DefinitionTypeMetaData : definitionMetaDatas)
        {
            T8Definition ancestorDefinition = getAncestorDefinition(t8DefinitionTypeMetaData.getTypeId());
            if(ancestorDefinition != null) return (T8DataFilterDefinition)ancestorDefinition;
        }

        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(getFieldIdentifier() != null)
        {
            T8DataFilterDefinition filterDefinition;

            filterDefinition = (T8DataFilterDefinition) getAncestorDefinition(T8DataFilterDefinition.class);
            if(!Objects.equals(filterDefinition.getDataEntityIdentifier(), T8IdentifierUtilities.getGlobalIdentifierPart(getFieldIdentifier())))
            {
                validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.FIELD_IDENTIFIER.toString(), "Invalid Field Identifier " + getFieldIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        return validationErrors;
    }

    @Override
    public T8DataFilterClause getNewDataFilterClauseInstance(T8Context context, Map<String, Object> filterParameters)
    {
        String filterValueExpression;

        filterValueExpression = getFilterValueExpression();
        if ((filterValueExpression != null) && (filterValueExpression.trim().length() > 0))
        {
            try
            {
                Map<String, Object> expressionParameters;
                T8DataFilterCriterion filterCriterion;
                T8ExpressionEvaluator evaluator;
                Object filterValue;

                evaluator = context.isServer() ? context.getServerContext().getExpressionEvaluator(context) : context.getClientContext().getExpressionEvaluator();
                expressionParameters = new HashMap<>();
                expressionParameters.putAll(context.getSessionContext().getSessionParameterMap());
                if (filterParameters != null) expressionParameters.putAll(filterParameters);

                filterValue = evaluator.evaluateExpression(filterValueExpression, expressionParameters, null);
                filterCriterion = new T8DataFilterCriterion(this, getFieldIdentifier(), getOperator(), filterValue);
                filterCriterion.setCaseInsensitive(isCaseInsensitive());
                return filterCriterion;
            }
            catch (EPICSyntaxException | EPICRuntimeException e)
            {
                throw new RuntimeException("Exception while executing filter criterion value expression: " + filterValueExpression, e);
            }
        }
        else
        {
            T8DataFilterCriterion filterCriterion;

            filterCriterion = new T8DataFilterCriterion(this, getFieldIdentifier(), getOperator(), getFilterValue());
            filterCriterion.setCaseInsensitive(isCaseInsensitive());
            return filterCriterion;
        }
    }

    public String getFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FIELD_IDENTIFIER.toString());
    }

    public void setDataFilterOperator(String fieldIdentifier)
    {
        setDefinitionDatum(Datum.FIELD_IDENTIFIER.toString(), fieldIdentifier);
    }

    public DataFilterOperator getOperator()
    {
        return DataFilterOperator.valueOf((String)getDefinitionDatum(Datum.OPERATOR.toString()));
    }

    public void setDataFilterOperator(DataFilterOperator operator)
    {
        setDefinitionDatum(Datum.OPERATOR.toString(), operator.toString());
    }

    public Object getFilterValue()
    {
        return getDefinitionDatum(Datum.FILTER_VALUE.toString());
    }

    public void setFilterValue(Object filterValue)
    {
        setDefinitionDatum(Datum.FILTER_VALUE.toString(), filterValue);
    }

    public String getFilterValueExpression()
    {
        return (String)getDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString());
    }

    public void setFilterValueExpression(String expression)
    {
        setDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString(), expression);
    }

    public boolean isCaseInsensitive()
    {
        Boolean caseInsensitive;

        caseInsensitive = (Boolean)getDefinitionDatum(Datum.CASE_INSENSITIVE.toString());
        return caseInsensitive != null && caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        setDefinitionDatum(Datum.CASE_INSENSITIVE.toString(), caseInsensitive);
    }
}
