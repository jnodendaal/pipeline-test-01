package com.pilog.t8;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import java.util.Map;
import com.pilog.t8.functionality.access.T8FunctionalityAccessRights;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityManager extends T8ServerModule
{
    public enum FunctionalityHistoryEvent {ACCESSED, RELEASED};

    /**
     * Returns the access rights associated with the specified functionality instance currently being accessed.
     * If the specified functionality is not currently accessible, null is returned indicating a denied request.
     * @param context The session context of the requesting code.
     * @param functionalityIid The instance id of the requested functionality.
     * @return The current access rights afforded by the specified functionality access.
     */
    public T8FunctionalityAccessRights getFunctionalityAccessRights(T8Context context, String functionalityIid);
    public T8FunctionalityAccessRights getWorkflowAccessRights(T8Context context, String flowIid);

    public T8FunctionalityAccessHandle accessFunctionality(T8Context context, String functionalityId, Map<String, Object> parameters) throws Exception;
    public void accessDataObject(T8DataTransaction tx, String functionalityIid, String dataObjectId, String dataObjectIid) throws Exception;
    public void releaseFunctionality(T8Context context, String functionalityIid) throws Exception;

    public T8FunctionalityGroupHandle getFunctionalityGroupHandle(T8Context context, String functionalityGroupId, String dataObjectId, String dataObjectIid) throws Exception;
    public T8FunctionalityHandle getFunctionalityHandle(T8Context context, String functionalityId, String dataObjectId, String dataObjectIid) throws Exception;

    public String getStateConceptId(T8Context context, String stateId) throws Exception;
    public void reloadAccessPolicy(T8Context context);
    public void clearCache(T8Context context);
}
