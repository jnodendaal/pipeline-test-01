/**
 * Created on 15 Jul 2016, 11:34:40 AM
 */
package com.pilog.t8.definition.org.operational;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Date;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8OrganizationOperationalNonWorkDayDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_ORG_OPERATIONAL_NON_WORK_DAY";
    public static final String GROUP_IDENTIFIER = "@DG_ORG_OPERATIONAL_NON_WORK_DAY";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Non Working Day";
    public static final String DESCRIPTION = "Defines a non working day, such as a public holiday, which is potentially observed during normal operational hours. A Non Working Day during non-operational hours is ignored.";
    public enum Datum { DESCRIPTION,
                        REPEAT_YEARLY,
                        DATE_OBSERVED};
    // -------- Definition Meta-Data -------- //

    public T8OrganizationOperationalNonWorkDayDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8Definition copy()
    {
        T8OrganizationOperationalNonWorkDayDefinition nonWorkDayCopy;

        nonWorkDayCopy = new T8OrganizationOperationalNonWorkDayDefinition(getIdentifier());
        nonWorkDayCopy.setNonWorkingDayDate(getNonWorkingDayDate());
        nonWorkDayCopy.setNonWorkingDayDescription(getNonWorkingDayDescription());
        nonWorkDayCopy.setRepeatYearly(isRepeatYearly());

        return nonWorkDayCopy;
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DESCRIPTION.toString(), "Non Work Day Description", "The description of the non working day or name of the public holiday.", "New Year"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REPEAT_YEARLY.toString(), "Repeat Yearly", "If repeated yearly, the Date specified is the first occurrence.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE, Datum.DATE_OBSERVED.toString(), "Non Work Day Date", "The date on which the non working day falls."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception {}

    public T8Date getNonWorkingDayDate()
    {
        return T8Date.fromTime(getDefinitionDatum(Datum.DATE_OBSERVED));
    }

    public long getNonWorkingDayDateMillis()
    {
        return getDefinitionDatum(Datum.DATE_OBSERVED);
    }

    public void setNonWorkingDayDate(T8Date nonWorkingDayDate)
    {
        setDefinitionDatum(Datum.DATE_OBSERVED, nonWorkingDayDate != null ? nonWorkingDayDate.getMilliseconds() : null);
    }

    public boolean isRepeatYearly()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.REPEAT_YEARLY));
    }

    public void setRepeatYearly(boolean repeatYearly)
    {
        setDefinitionDatum(Datum.REPEAT_YEARLY, repeatYearly);
    }

    public String getNonWorkingDayDescription()
    {
        return getDefinitionDatum(Datum.DESCRIPTION);
    }

    public void setNonWorkingDayDescription(String description)
    {
        setDefinitionDatum(Datum.DESCRIPTION, description);
    }
}