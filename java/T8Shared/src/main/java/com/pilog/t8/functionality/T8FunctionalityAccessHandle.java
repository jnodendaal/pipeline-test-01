package com.pilog.t8.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.ui.functionality.event.T8FunctionalityExecutionListener;
import java.io.Serializable;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public interface T8FunctionalityAccessHandle extends Serializable
{
    /**
     * Enum defining the types of access that can be granted to a client requesting access to a functionality.
     */
    public enum T8FunctionalityAccessType
    {
        ACCESS, // Access granted.
        DENIED_BLOCKED, // Access denied because the functionality requested is blocked by another functionality.
        DENIED_VALIDATION, // Access denied because the validation based on the target object failed.
        DENIED_PERMISSION // Access denied because the user does not have the required permission.
    }

    /**
     * Returns the type of access granted to the requester.
     * @return The type of access granted tot he requester.
     */
    public T8FunctionalityAccessType getAccessType();

    /**
     * Returns the access message (if any) applicable to the access type granted to the requester.
     * @return The access message (if any) applicable to the access type granted to the requester.
     */
    public String getAccessMessage();

    /**
     * Returns the display name of this execution handle.
     * @return The display name of this execution handle.
     */
    public String getDisplayName();

    /**
     * Returns the display description of this execution handle.
     * @return The display description of this execution handle.
     */
    public String getDescription();

    /**
     * Returns the Icon to be associated with this execution handle when
     * displayed on a UI.
     * @return The Icon to be associated with this execution handle when
     * displayed on a UI.
     */
    public Icon getIcon();

    /**
     * Returns the identifier of the functionality to which this execution
     * handle refers.
     * @return The identifier of the functionality to which this execution
     * handle refers.
     */
    public String getFunctionalityId();

    /**
     * Returns the identifier of the functionality instance that this handle
     * references.
     * @return The identifier of the functionality instance that this handle
     * references.
     */
    public String getFunctionalityIid();

    /**
     * Returns a flag indicating whether or not this functionality requires the
     * client to explicitly release it once no longer in use.
     * @return
     */
    public boolean requiresExplicitRelease();

    /**
     * Creates a component that displays the functionality execution state.
     * @param controller The controlled of the newly created component.
     * @return The newly created view component.
     * @throws Exception
     */
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception;

    public void addFunctionalityExecutionListener(T8FunctionalityExecutionListener listener);
    public void removeFunctionalityExecutionListener(T8FunctionalityExecutionListener listener);
}
