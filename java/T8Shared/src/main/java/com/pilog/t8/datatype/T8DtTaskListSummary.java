package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.task.T8TaskTypeSummary;

/**
 * @author Bouwer du Preez
 */
public class T8DtTaskListSummary extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@TASK_LIST_SUMMARY";

    public T8DtTaskListSummary() {}

    public T8DtTaskListSummary(T8DefinitionManager context) {}

    public T8DtTaskListSummary(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8TaskListSummary.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            JsonObject taskSummaryObject;
            T8TaskListSummary taskSummary;
            JsonArray taskTypeArray;

            // Create the concept JSON object.
            taskSummary = (T8TaskListSummary)object;
            taskSummaryObject = new JsonObject();
            taskSummaryObject.add("totalTaskCount", taskSummary.getTotalTaskCount());
            taskSummaryObject.add("claimedTaskCount", taskSummary.getClaimedTaskCount());

            // Add the task type summaries.
            taskTypeArray = new JsonArray();
            taskSummaryObject.add("taskTypes", taskTypeArray);
            for (T8TaskTypeSummary taskTypeSummary : taskSummary.getTaskTypeSummaries())
            {
                JsonObject taskTypeObject;

                taskTypeObject = new JsonObject();
                taskTypeObject.add("taskId", taskTypeSummary.getTaskIdentifier());
                taskTypeObject.add("name", taskTypeSummary.getDisplayName());
                taskTypeObject.add("description", taskTypeSummary.getDescription());
                taskTypeObject.add("totalCount", taskTypeSummary.getTotalCount());
                taskTypeObject.add("claimedCount", taskTypeSummary.getClaimedCount());
                taskTypeArray.add(taskTypeObject);
            }

            // Return the final task list summary object.
            return taskSummaryObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject taskListSummaryObject;
            JsonArray taskTypeArray;
            T8TaskListSummary taskListSummary;

            // Get the JSON values.
            taskListSummaryObject = jsonValue.asObject();
            taskTypeArray = taskListSummaryObject.getJsonArray("taskTypes");

            // Create the task list summary object.
            taskListSummary = new T8TaskListSummary();
            for (JsonValue taskTypeValue : taskTypeArray.values())
            {
                T8TaskTypeSummary taskTypeSummary;
                JsonObject taskTypeObject;

                taskTypeObject = taskTypeValue.asObject();
                taskTypeSummary = new T8TaskTypeSummary(taskTypeObject.getString("taskId"));
                taskTypeSummary.setDisplayName(taskTypeObject.getString("name"));
                taskTypeSummary.setDescription(taskTypeObject.getString("description"));
                taskTypeSummary.setTotalCount(taskTypeObject.getInt("totalCount", 0));
                taskTypeSummary.setClaimedCount(taskTypeObject.getInt("claimedCount", 0));
                taskListSummary.putTaskTypeSummary(taskTypeSummary);
            }

            // Return the completed object.
            return taskListSummary;
        }
        else return null;
    }
}