package com.pilog.t8.ui;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8DefinitionTestHarness
{
    public void configure(T8Context context);
    public void testDefinition(T8Context context, T8Definition definition);
}
