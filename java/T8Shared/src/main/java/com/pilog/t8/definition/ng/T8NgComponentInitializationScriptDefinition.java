package com.pilog.t8.definition.ng;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8NgComponentInitializationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_NG_COMPONENT_INITIALIZATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_NG_COMPONENT_INITIALIZATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Angular Component Initialization Script";
    public static final String DESCRIPTION = "A script that uses preset input parameters to compile a set of component parameter to be used as input in an Angular Component.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMTER_COMPONENT_PARAMETERS = "$P_COMPONENT_PARAMETERS";

    public T8NgComponentInitializationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8NgComponentDefinition componentDefinition;

        componentDefinition = (T8NgComponentDefinition)getParentDefinition();
        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.addAll(componentDefinition.getInputParameterDefinitions());
        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMTER_COMPONENT_PARAMETERS, "Component Parameters", "The map of component parameters that will be sent to the Angular Component during its initialization.", T8DataType.MAP));
        return definitions;
    }
}
