package com.pilog.t8.project;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.json.WriterConfig;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectPackage
{
    private final List<T8ProjectDefinition> projects;
    private final Map<String, List<T8Definition>> content;

    public T8ProjectPackage()
    {
        projects = new ArrayList<>();
        content = new HashMap<>();
    }

    public void addProject(T8ProjectDefinition projectDefinition)
    {
        this.projects.add(projectDefinition);
        this.content.put(projectDefinition.getIdentifier(), new ArrayList<>());
    }

    public void addContentDefinition(T8Definition contentDefinition)
    {
        List<T8Definition> contentDefinitions;

        contentDefinitions = content.get(contentDefinition.getProjectIdentifier());
        if (contentDefinitions != null)
        {
            contentDefinitions.add(contentDefinition);
        }
        else throw new IllegalArgumentException("Project not found in package: " + contentDefinition.getProjectIdentifier());
    }

    public T8ProjectDefinition getProjectDefinition(String projectId)
    {
        for (T8ProjectDefinition projectDefinition : projects)
        {
            if (projectDefinition.getIdentifier().equals(projectId))
            {
                return projectDefinition;
            }
        }

        return null;
    }

    public List<T8Definition> getContentDefinitions(String projectId)
    {
        return content.get(projectId);
    }

    public void clear()
    {
        projects.clear();
        content.clear();
    }

    public void write(T8DefinitionManager definitionManager, Writer writer) throws Exception
    {
        T8DefinitionSerializer serializer;
        JsonArray jsonProjects;
        JsonObject jsonRoot;

        // Get the serializer to use when reading definitions.
        serializer = new T8DefinitionSerializer(definitionManager);

        // Read the root json object and process the content.
        jsonRoot = new JsonObject();
        jsonProjects = new JsonArray();
        jsonRoot.add("projects", jsonProjects);
        for (T8ProjectDefinition projectDefinition : projects)
        {
            List<T8Definition> contentDefinitions;
            JsonObject jsonProjectDefinition;
            JsonArray jsonContentDefinitions;
            JsonObject jsonProject;

            // Add the json objects to the model.
            jsonProject = new JsonObject();
            jsonProjectDefinition = serializer.serialize(projectDefinition);
            jsonProject.add("definition", jsonProjectDefinition);
            jsonContentDefinitions = new JsonArray();
            jsonProject.add("content", jsonContentDefinitions);
            jsonProjects.add(jsonProject);

            // Serialize all of the definitions contained by the project.
            contentDefinitions = content.get(projectDefinition.getIdentifier());
            for (T8Definition contentDefinition : contentDefinitions)
            {
                JsonObject jsonContentDefinition;

                // Deserialize the content definition.
                jsonContentDefinition = serializer.serialize(contentDefinition);
                jsonContentDefinitions.add(jsonContentDefinition);
            }
        }

        // Write the root object to the output stream.
        jsonRoot.writeTo(writer, WriterConfig.PRETTY_PRINT);
    }

    public void read(T8DefinitionManager definitionManager, Reader reader) throws Exception
    {
        T8DefinitionSerializer serializer;
        JsonArray jsonProjects;
        JsonObject jsonRoot;

        // Clear the package.
        clear();

        // Get the serializer to use when reading definitions.
        serializer = new T8DefinitionSerializer(definitionManager);

        // Read the root json object and process the content.
        jsonRoot = JsonObject.readFrom(reader);
        jsonProjects = jsonRoot.getJsonArray("projects");
        for (JsonValue jsonProjectValue : jsonProjects)
        {
            T8ProjectDefinition projectDefinition;
            List<T8Definition> contentDefinitions;
            JsonObject jsonProjectDefinition;
            JsonArray jsonContentDefinitions;
            JsonObject jsonProject;
            String projectId;

            // Get the json objects from the model.
            jsonProject = jsonProjectValue.asObject();
            jsonProjectDefinition = jsonProject.getJsonObject("definition");
            jsonContentDefinitions = jsonProject.getJsonArray("content");

            // Deserialize the project definition.
            projectDefinition = (T8ProjectDefinition)serializer.deserializeDefinition(jsonProjectDefinition);
            projectId = projectDefinition.getIdentifier();

            // Deserialize all of the definitions contained by the project.
            contentDefinitions = new ArrayList<>();
            for (JsonValue jsonDefinitionValue : jsonContentDefinitions)
            {
                JsonObject jsonContentDefinition;
                T8Definition contentDefinition;

                // Deserialize the content definition.
                jsonContentDefinition = jsonDefinitionValue.asObject();
                contentDefinition = serializer.deserializeDefinition(jsonContentDefinition);
                contentDefinitions.add(contentDefinition);
            }

            // Add the project to the result map.
            projects.add(projectDefinition);
            content.put(projectId, contentDefinitions);
        }
    }
}
