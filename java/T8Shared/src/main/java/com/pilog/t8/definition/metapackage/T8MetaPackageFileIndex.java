package com.pilog.t8.definition.metapackage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Bouwer du Preez
 */
public class T8MetaPackageFileIndex implements Serializable
{
    // Standard ID to ensure compatible serialization.
    static final long serialVersionUID = 1L;
    
    private String fileIdentifier;
    private HashMap<String, T8MetaPackageDescriptor> packageDescriptors;

    public T8MetaPackageFileIndex()
    {
        packageDescriptors = new HashMap<String, T8MetaPackageDescriptor>();
    }

    public String getFileIdentifier()
    {
        return fileIdentifier;
    }

    public void setFileIdentifier(String fileIdentifier)
    {
        this.fileIdentifier = fileIdentifier;
    }
    
    public T8MetaPackageDescriptor getPackageDescriptor(String packageIdentifier)
    {
        return packageDescriptors.get(packageIdentifier);
    }

    public int getPackageDefinitionCount(String packageIdentifier)
    {
        if (packageDescriptors.containsKey(packageIdentifier))
        {
            return packageDescriptors.get(packageIdentifier).getDefinitionCount();
        }
        else
        {
            return -1;
        }
    }

    public T8MetaPackageDefinitionDataLocation getPackageDataLocation(String packageIdentifier, int definitionIndex)
    {
        T8MetaPackageDescriptor packageDescriptor;
        
        packageDescriptor = packageDescriptors.get(packageIdentifier);
        return packageDescriptor != null ? packageDescriptor.getDefinitionDataLocation(definitionIndex) : null;
    }
    
    public T8MetaPackageDefinitionDataLocation getPackageDataLocation(String packageIdentifier, String definitionIdentifier)
    {
        T8MetaPackageDescriptor packageDescriptor;
        
        packageDescriptor = packageDescriptors.get(packageIdentifier);
        return packageDescriptor != null ? packageDescriptor.getDefinitionDataLocation(definitionIdentifier) : null;
    }

    public void addDefinitionDataLocation(String packageIdentifier, String definitionIdentifier, long offset, int size)
    {
        packageDescriptors.get(packageIdentifier).addDefinitionDataLocation(definitionIdentifier, offset, size);
    }

    public void clear()
    {
        packageDescriptors.clear();
    }

    public T8MetaPackageDescriptor addPackage(String packageIdentifier)
    {
        if (!packageDescriptors.containsKey(packageIdentifier))
        {
            T8MetaPackageDescriptor descriptor;
            
            descriptor = new T8MetaPackageDescriptor(packageIdentifier);
            packageDescriptors.put(packageIdentifier, descriptor);
            return descriptor;
        }
        else return null;
    }

    public ArrayList<String> getPackageIdentifierList()
    {
        return new ArrayList<String>(packageDescriptors.keySet());
    }

    public ArrayList<String> getPackageDefinitionIdentifierList(String packageIdentifier)
    {
        T8MetaPackageDescriptor packageDescriptor;
        
        packageDescriptor = packageDescriptors.get(packageIdentifier);
        return packageDescriptor != null ? packageDescriptor.getDefinitionIdentifierList() : null;
    }

    public boolean containsPackage(String packageIdentifier)
    {
        return packageDescriptors.containsKey(packageIdentifier);
    }

    public boolean containsDefinition(String definitionIdentifier)
    {
        for (T8MetaPackageDescriptor packageDescriptor : packageDescriptors.values())
        {
            if (packageDescriptor.containsDefinition(definitionIdentifier)) return true;
        }
        
        return false;
    }

    public boolean containsPackageDefinition(String packageIdentifier, String definitionIdentifier)
    {
        T8MetaPackageDescriptor packageDescriptor;
        
        packageDescriptor = packageDescriptors.get(packageIdentifier);
        return packageDescriptor != null ? packageDescriptor.containsDefinition(definitionIdentifier) : false;
    }
}
