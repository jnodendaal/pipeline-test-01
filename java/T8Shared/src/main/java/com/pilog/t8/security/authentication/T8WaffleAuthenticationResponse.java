package com.pilog.t8.security.authentication;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8SessionContext;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8WaffleAuthenticationResponse implements T8AuthenticationResponse, Serializable
{
    private final ResponseType type;
    private final byte[] token;
    private final T8SessionContext sessionContext;
    private final String userId;

    public T8WaffleAuthenticationResponse(ResponseType type, byte[] token, T8SessionContext sessionContext, String userId)
    {
        this.type = type;
        this.token = token;
        this.sessionContext = sessionContext;
        this.userId = userId;
    }

    @Override
    public ResponseType getResponseType()
    {
        return type;
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    @Override
    public JsonObject serialize()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUserId()
    {
        return userId;
    }

    public byte[] getToken()
    {
        return token;
    }
}
