package com.pilog.t8.communication;

import com.pilog.t8.security.T8Context;
import java.util.Map;
import javax.activation.DataSource;

/**
 * @author hennie.brink@pilog.co.za
 */
public interface T8CommunicationMessageAttachment
{
    void generateAttachment(T8Context context, Map<String,Object> templateInputParameters) throws Exception;
    DataSource getAttachment() throws Exception;
    String getName();
    String getDescription();
    String getIdentifier();
    byte[] getBinaryData();
    boolean isEmbedded();
    String getContentID();
}
