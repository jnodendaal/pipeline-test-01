package com.pilog.t8.data;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import java.util.List;
import java.util.Map;

/**
 * This interface is implemented by all data sources.  Many of the methods in
 * this interface that take Maps as parameters are for convenience only, since
 * data sources are often used directly from scripts.
 *
 * @author Bouwer du Preez
 */
public interface T8DataSource extends T8PerformanceStatisticsProvider
{
    public T8DataSourceDefinition getDefinition();

    public void open() throws Exception;
    public void close() throws Exception;
    public void commit() throws Exception;
    public void rollback() throws Exception;
    public void setParameters(Map<String, Object> parameters);
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception;

    public int count(T8DataFilter filter) throws Exception;
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception;
    public void insert(T8DataEntity dataEntity) throws Exception;
    public void insert(List<T8DataEntity> dataEntities) throws Exception;
    public boolean update(T8DataEntity dataEntity) throws Exception;
    public boolean update(List<T8DataEntity> dataEntities) throws Exception;
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception;
    public boolean delete(T8DataEntity dataEntity) throws Exception;
    public int delete(List<T8DataEntity> dataEntities) throws Exception;
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception;
    /**
     * Retrieves a single record from the data source as a {@code T8DataEntity}
     * object. If more than one record is returned, the first in the set is
     * returned. If no records are returned by the data source, the result will
     * be {@code null}.
     *
     * @param entityIdentifier The {@code String} entity identifier to be used
     *      for the data retrieval
     * @param keyMap The {@code Map} containing the key values pairs which
     *      define the filter to be applied for the record to be retrieved
     *
     * @return The first record as a {@code T8DataEntity} from the returned
     *      results. {@code null} if no records are returned
     *
     * @throws Exception If there is any error during the retrieval of the
     *      required data
     */
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyMap) throws Exception;
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap) throws Exception;
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception;
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap, int startOffset, int pageSize) throws Exception;
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int startOffset, int pageSize) throws Exception;
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter filter) throws Exception;
}
