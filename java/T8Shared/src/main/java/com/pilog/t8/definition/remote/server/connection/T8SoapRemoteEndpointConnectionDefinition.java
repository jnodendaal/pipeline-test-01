package com.pilog.t8.definition.remote.server.connection;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Definition which allows the setup of a SOAP connection endpoint to be used.
 * This is simply the endpoint and its associated details used to call a
 * web service operation on a remote server.
 *
 * @author Gavin Boshoff
 */
public class T8SoapRemoteEndpointConnectionDefinition extends T8ConnectionDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REMOTE_SERVER_CONNECTION_SOAP_ENDPOINT";
    public static final String DISPLAY_NAME = "Remote Soap Endpoint Connection";
    public static final String DESCRIPTION = "This definition contains the information neccessary to connect to a remote server endpoint address.";
    public enum Datum {
        OPERATION,
        AUTH_USER,
        AUTH_PASSWORD
    };
    // -------- Definition Meta-Data -------- //

    public T8SoapRemoteEndpointConnectionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATION.toString(), "Operation", "The endpoint operation to be called."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AUTH_USER.toString(), "Auth: Username", "The authentication username for the webservice connection."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AUTH_PASSWORD.toString(), "Auth: Password", "The authentication password for the webservice connection."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Connection getNewConnectionInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.document.integration.T8IntegrationServiceConnector", new Class<?>[]{T8Context.class, T8SoapRemoteEndpointConnectionDefinition.class}, context, this);
    }

    @Override
    public String getURL()
    {
            return new StringBuilder(super.getURL()).append("/").append(getOperation()).toString();
    }

    @Override
    public List<T8RemoteUserDefinition> getRemoteUserDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.remote.T8RemoteServerConnectionTestHarness", new Class<?>[]{});
    }

    public String getOperation()
    {
        return getDefinitionDatum(Datum.OPERATION);
    }

    public void setOperation(String operation)
    {
        setDefinitionDatum(Datum.OPERATION, operation);
    }

    public String getAuthUsername()
    {
        return getDefinitionDatum(Datum.AUTH_USER);
    }

    public void setAuthUsername(String username)
    {
        setDefinitionDatum(Datum.AUTH_USER, username);
    }

    public String getAuthPassword()
    {
        return getDefinitionDatum(Datum.AUTH_PASSWORD);
    }

    public void setAuthPassword(String password)
    {
        setDefinitionDatum(Datum.AUTH_PASSWORD, password);
    }
}
