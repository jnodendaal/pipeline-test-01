package com.pilog.t8.script;

import com.pilog.t8.operation.progressreport.T8ProgressReport;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8Script
{
    // Returns the current execution progress of the script.  -1 Indicates that progress is indeterminate.
    public double getProgress();
    // Returns the current execution progress report of the script.
    public T8ProgressReport getProgressReport();

    // Prepares the script instance for execution.
    public void prepareScript() throws Exception;
    // Adds a new method import to a prepared script.
    public void addMethodImport(String procedureName, Object contextObject, Method method);
    // Executes an already prepared script and does NOT finalize it.  This means the resources used by the script are not released and subsequent executions can be performed.
    public Map<String, Object> executePreparedScript(Map<String, ? extends Object> inputParameters) throws Exception;
    public void finalizeScript() throws Exception;

    // This is a convenience method for executing a script once.  This method will prepare the instance, execute the script, finalize the script and return the output parameters.
    public Map<String, Object> executeScript(Map<String, ? extends Object> inputParameters) throws Exception;
}
