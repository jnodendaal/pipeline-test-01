package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtString extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@STRING";

    public enum T8StringType
    {
        DEFAULT,
        EPIC_EXPRESSION,
        EPIC_SCRIPT,
        JAVA,
        JAVA_SCRIPT,
        JSON,
        CSS,
        XML,
        TRANSLATED_STRING,
        PASSWORD,
        COLOR_HEX,
        GUID,
        DEFINITION_IDENTIFIER,
        DEFINITION_TYPE_IDENTIFIER,
        DEFINITION_GROUP_IDENTIFIER,
        DEFINITION_INSTANCE_IDENTIFIER,
        LONG_STRING
    };

    private final T8StringType type;

    public T8DtString(T8StringType type)
    {
        this.type = type;
    }

    public T8DtString()
    {
        this(T8StringType.DEFAULT);
    }

    public T8DtString(T8DefinitionManager context)
    {
        this(T8StringType.DEFAULT);
    }

    public T8DtString(T8DefinitionManager context, String stringRepresentation)
    {
        Map<String, String> attributes;

        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);

        attributes = parseAttributes(stringRepresentation);
        if (attributes != null)
        {
            String typeString;

            typeString = attributes.get("TYPE");
            if (typeString != null) this.type = T8StringType.valueOf(typeString);
            else this.type = T8StringType.DEFAULT;
        }
        else
        {
            this.type = T8StringType.DEFAULT;
        }
    }

    public T8StringType getType()
    {
        return type;
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        if (includeAttributes)
        {
            StringBuilder buffer;

            buffer = new StringBuilder();
            buffer.append(IDENTIFIER);
            buffer.append("(");
            buffer.append("TYPE=");
            buffer.append(type);
            buffer.append(")");
            return buffer.toString();
        }
        else return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return String.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        return JsonValue.valueOf(object);
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            return jsonValue.asString();
        }
        else return null;
    }
}
