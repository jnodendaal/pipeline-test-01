package com.pilog.t8.definition.ui;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8ComponentOperationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_OPERATION";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_OPERATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Component Operation";
    public static final String DESCRIPTION = "An operation that may be performed on a UI component.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    public enum Datum {INPUT_PARAMETER_DEFINITIONS,
                       OUTPUT_PARAMETER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ComponentOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.derivedDefinitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters",  "The input data parameters required by this operation."));
        datumTypes.add(T8DefinitionDatumType.derivedDefinitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters", "The output data parameters returned by this operation after execution."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public void addInputParameterDefinition(T8DataParameterResourceDefinition parameterDefinition)
    {
        addSubDefinition(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterDefinition>)getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setInputParameterDefinitions(ArrayList<T8DataParameterResourceDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }

    public void addOutputParameterDefinition(T8DataParameterResourceDefinition parameterDefinition)
    {
        addSubDefinition(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinition);
    }

    public ArrayList<T8DataParameterResourceDefinition> getOutputParameterDefinitions()
    {
        return (ArrayList<T8DataParameterResourceDefinition>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString());
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterResourceDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), parameterDefinitions);
    }
}
