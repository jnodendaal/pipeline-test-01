package com.pilog.t8.performance;

import java.io.OutputStream;

/**
 * @author Bouwer du Preez
 */
public interface T8ExecutionPerformanceStatistics
{
    public String getIdentifier();
    public long getIterations();
    public long getFirstStartTime();
    public long getFirstEndTime();
    public long getLastStartTime();
    public long getLastEndTime();
    public long getLongestIterationDuration();
    public long getTotalDuration();
    public void reset();
    public void logStart();
    public void logEnd();
    public void logSubExecutionStart(String executionIdentifier);
    public void logSubExecutionEnd(String executionIdentifier);
    public void printStatistics(OutputStream stream);
    public boolean isExecuting(String executionIdentifier);
    public boolean hasInstances();
    public T8ExecutionPerformanceStatistics getParentExecutionStatistics();
    public T8ExecutionPerformanceStatistics findExecutionStatistics(String identifier);
}
