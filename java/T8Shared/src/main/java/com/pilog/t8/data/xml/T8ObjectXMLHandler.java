package com.pilog.t8.data.xml;

/**
 * @author Bouwer du Preez
 */
public interface T8ObjectXMLHandler
{
    public String getTypeIdentifier();
    public CharSequence getXML(Object object);
    public Object getObject(CharSequence xml);
}
