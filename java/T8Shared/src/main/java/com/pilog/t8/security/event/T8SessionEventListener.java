package com.pilog.t8.security.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8SessionEventListener extends EventListener
{
    public void sessionLoggedOut(T8SessionLogoutEvent event);
}
