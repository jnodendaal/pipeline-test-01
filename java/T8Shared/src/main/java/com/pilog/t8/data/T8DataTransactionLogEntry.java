package com.pilog.t8.data;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataTransactionLogEntry implements Serializable
{
    private final String operationID;
    private final long threadID;
    private final String threadName;
    private final String entityIdentifier;
    private final String filter;
    private final TransactionOperationType type;
    private long startTime;
    private long endTime;

    public enum TransactionOperationType {INSERT, UPDATE, DELETE, SELECT, RETRIEVE, COUNT, SCROLL};

    public T8DataTransactionLogEntry(String operationID, long threadID, String threadName, String entityIdentifier, String filter, TransactionOperationType type, long startTime, long endTime)
    {
        this.operationID = operationID;
        this.threadID = threadID;
        this.threadName = threadName;
        this.entityIdentifier = entityIdentifier;
        this.filter = filter;
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getOperationID()
    {
        return operationID;
    }

    public long getThreadID()
    {
        return threadID;
    }

    public String getThreadName()
    {
        return threadName;
    }

    public String getEntityIdentifier()
    {
        return entityIdentifier;
    }

    public String getFilter()
    {
        return filter;
    }

    public TransactionOperationType getType()
    {
        return type;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public void setStartTime(long startTime)
    {
        this.startTime = startTime;
    }

    public long getEndTime()
    {
        return endTime;
    }

    public void setEndTime(long endTime)
    {
        this.endTime = endTime;
    }
}
