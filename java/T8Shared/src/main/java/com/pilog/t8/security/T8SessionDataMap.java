package com.pilog.t8.security;

import com.pilog.t8.utilities.serialization.SerializationUtilities;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SessionDataMap
{
    private final Map<String, byte[]> dataMap;
    private final T8Session parentSession;
    private long dataSize;

    public T8SessionDataMap(T8Session parentSession)
    {
        this.parentSession = parentSession;
        this.dataMap = new HashMap<>();
        this.dataSize = 0;
    }

    public long getDataSize()
    {
        return dataSize;
    }

    public Serializable setData(String dataId, Serializable newDataObject)
    {
        try
        {
            byte[] replacedData;
            byte[] newData;

            newData = newDataObject != null ? SerializationUtilities.serializeObject(newDataObject) : null;
            replacedData = dataMap.put(dataId, newData);
            if (newData != null) dataSize += newData.length;
            if (replacedData != null)
            {
                dataSize -= replacedData.length;
                return (Serializable)SerializationUtilities.deserializeObject(replacedData);
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Failure to store session data using id " + dataId + " in session: " + parentSession, e);
        }
    }

    public Serializable getData(String dataId)
    {
        try
        {
            byte[] data;

            data = dataMap.get(dataId);
            return data != null ? (Serializable)SerializationUtilities.deserializeObject(data) : null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Failure to retrieve session data using id " + dataId + " in session: " + parentSession, e);
        }
    }
}
