package com.pilog.t8.file;

import com.pilog.t8.commons.io.RandomAccessStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8FileContext
{
    public String getId();
    public String getIid();

    public boolean open() throws Exception;
    public boolean close() throws Exception;

    public InputStream getFileInputStream(String filePath) throws Exception, FileNotFoundException;
    public OutputStream getFileOutputStream(String filePath, boolean append) throws Exception, FileNotFoundException;
    public RandomAccessStream getFileRandomAccessStream(String filePath) throws Exception, FileNotFoundException;

    public File getFile(String filePath) throws Exception, FileNotFoundException;
    public T8FileDetails getFileDetails(String filePath) throws Exception, FileNotFoundException;
    public boolean fileExists(String filePath) throws Exception;
    public boolean createDirectory(String filePath) throws Exception;
    public boolean deleteFile(String path) throws Exception, FileNotFoundException;
    public void renameFile(String filePath, String newFilename) throws Exception, FileNotFoundException;
    public long getFileSize(String path) throws Exception, FileNotFoundException;
    public String getMD5Checksum(String path) throws Exception, FileNotFoundException;
    public List<T8FileDetails> getFileList(String directoryPath) throws Exception, FileNotFoundException;
}
