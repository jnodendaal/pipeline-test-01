package com.pilog.t8.definition.flow.node.event;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FlowEventDefinition extends T8WorkFlowNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8FlowEventDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public FlowNodeType getNodeType()
    {
        return FlowNodeType.EVENT;
    }
}
