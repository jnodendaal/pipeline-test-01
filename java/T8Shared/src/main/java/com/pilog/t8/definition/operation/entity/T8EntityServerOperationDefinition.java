package com.pilog.t8.definition.operation.entity;

import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Pieter Strydom
 */
public class T8EntityServerOperationDefinition extends T8ServerOperationDefinition
{
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_SERVER_OPERATION_ENTITY";
    public static final String DISPLAY_NAME = "Entity Server Operation";
    public static final String DESCRIPTION = "A server-side pre-compiled Entity operation.";
    public enum Datum {DATA_ENTITY_IDENTIFIER};

    public T8EntityServerOperationDefinition(String identifier)
    {
        super(identifier);
        setDefaultParamaters();
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity retrieved with this server operation."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.operation.entity.T8ServerEntityOperation").getConstructor(T8Context.class, this.getClass(), String.class);
            return (T8ServerOperation)constructor.newInstance(context, this, operationIid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityId)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityId);
    }

    private void setDefaultParamaters()
    {
        // Add the default input parmaters of the server operation.
        addInputParameterDefinition(new T8DataParameterDefinition("$P_DATA_FILTER", "Data Filter", "The data filter that needs to be applied when retrieving data.", T8DataType.DATA_FILTER));
        addInputParameterDefinition(new T8DataParameterDefinition("$P_PAGE_SIZE", "Page Size", "The page size that will be used when retrieving the data.", T8DataType.INTEGER));
        addInputParameterDefinition(new T8DataParameterDefinition("$P_PAGE_OFFSET", "Page Offset", "The page offset that will be used when retrieving the data.", T8DataType.INTEGER));

        // Add the default output parmaters of the server operation.
        addOutputParameterDefinition(new T8DataParameterDefinition("$P_ENTITY_LIST", "Data Entity List", "The list of data entities that was retrieved.", new T8DtList(T8DataType.DATA_ENTITY)));
    }
}
