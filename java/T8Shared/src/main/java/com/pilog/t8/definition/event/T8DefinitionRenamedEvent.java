package com.pilog.t8.definition.event;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.security.T8Context;
import java.util.EventObject;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionRenamedEvent extends EventObject
{
    private final T8Context context;
    private final List<T8DefinitionHandle> affectedDefinitions;
    private final T8Definition definition;
    private final String oldIdentifier;

    public T8DefinitionRenamedEvent(T8Context context, T8Definition definition, String oldIdentifier, List<T8DefinitionHandle> affectedDefinitions)
    {
        super(definition);
        this.context = context;
        this.definition = definition;
        this.oldIdentifier = oldIdentifier;
        this.affectedDefinitions = affectedDefinitions;
    }

    public T8Context getContext()
    {
        return context;
    }

    public T8Definition getDefinition()
    {
        return definition;
    }

    public String getOldIdentifier()
    {
        return oldIdentifier;
    }

    public List<T8DefinitionHandle> getAffectedDefinitions()
    {
        return affectedDefinitions;
    }
}
