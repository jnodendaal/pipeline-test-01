package com.pilog.t8.definition.flow.node.gateway;

import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;

/**
 * @author Bouwer du Preez
 */
public abstract class T8FlowGatewayDefinition extends T8WorkFlowNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8FlowGatewayDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public FlowNodeType getNodeType()
    {
        return FlowNodeType.GATEWAY;
    }
}
