package com.pilog.t8.flow;

/**
 * @author Bouwer du Preez
 */
public class T8FlowNodeExecutionResult
{
    private final NodeExecutionResultType resultType;
    private final String information;

    public enum NodeExecutionResultType {COMPLETED, STOPPED, CANCELED, FAILED, WAITING};

    public T8FlowNodeExecutionResult(NodeExecutionResultType resultType, String information)
    {
        this.resultType = resultType;
        this.information = information;
    }

    public NodeExecutionResultType getResultType()
    {
        return resultType;
    }

    public String getInformation()
    {
        return information;
    }
}
