package com.pilog.t8.security;

/**
 * @author Bouwer du Preez
 */
public class T8OperationAccessRights
{
    private String operationId;
    private String dataObjectIid;

    public T8OperationAccessRights(String operationId, String dataObjectIid)
    {
        this.operationId = operationId;
        this.dataObjectIid = dataObjectIid;
    }

    public String getOperationId()
    {
        return operationId;
    }

    public void setOperationId(String operationId)
    {
        this.operationId = operationId;
    }

    public String getDataObjectIid()
    {
        return dataObjectIid;
    }

    public void setDataObjectIid(String dataObjectIid)
    {
        this.dataObjectIid = dataObjectIid;
    }

    @Override
    public String toString()
    {
        return "T8OperationAccessRights{" + "operationId=" + operationId + ", dataObjectIid=" + dataObjectIid + '}';
    }
}
