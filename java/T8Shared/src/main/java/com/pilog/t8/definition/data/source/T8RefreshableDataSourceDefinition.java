package com.pilog.t8.definition.data.source;

import com.pilog.t8.data.T8RefreshableDataSource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public interface T8RefreshableDataSourceDefinition
{
    /**
     * Factory method for data source instances.  Data Sources get all access to
     * the server-environment through the data access provider.
     */
    public T8RefreshableDataSource getNewDataSourceInstance(T8DataTransaction accessProvider);
    public List<T8DataParameterDefinition> getRefreshParameterDefinitions();
}
