package com.pilog.t8.data.filter.expression;

/**
 * @author Bouwer du Preez
 */
public class T8SearchExpressionParseException extends Exception
{
    public T8SearchExpressionParseException(String message)
    {
        super(message);
    }
}
