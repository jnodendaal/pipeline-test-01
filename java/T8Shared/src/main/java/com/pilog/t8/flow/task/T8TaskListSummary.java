package com.pilog.t8.flow.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskListSummary implements Serializable
{
    private final Map<String, T8TaskTypeSummary> taskTypeSummaries;

    public T8TaskListSummary()
    {
        taskTypeSummaries = new LinkedHashMap<String, T8TaskTypeSummary>();
    }

    public List<T8TaskTypeSummary> getTaskTypeSummaries()
    {
        return new ArrayList<T8TaskTypeSummary>(taskTypeSummaries.values());
    }

    public T8TaskTypeSummary getTaskTypeSummary(String taskIdentifier)
    {
        return taskTypeSummaries.get(taskIdentifier);
    }

    public void putTaskTypeSummary(T8TaskTypeSummary summary)
    {
        taskTypeSummaries.put(summary.getTaskIdentifier(), summary);
    }

    public int getTaskCount(T8TaskFilter taskFilter)
    {
        int count;

        count = 0;
        for (T8TaskTypeSummary taskTypeSummary : taskTypeSummaries.values())
        {
            if (taskFilter.isTaskTypeIncluded(taskTypeSummary.getTaskIdentifier()))
            {
                if (taskFilter.isIncludeClaimedTasks())
                {
                    count += taskTypeSummary.getClaimedCount();
                }

                if (taskFilter.isIncludeUnclaimedTasks())
                {
                    count += (taskTypeSummary.getTotalCount() - taskTypeSummary.getClaimedCount());
                }
            }
        }

        return count;
    }

    public int getClaimedTaskCount()
    {
        int count;

        count = 0;
        for (T8TaskTypeSummary taskTypeSummary : taskTypeSummaries.values())
        {
            count += taskTypeSummary.getClaimedCount();
        }

        return count;
    }

    public int getTotalTaskCount()
    {
        int count;

        count = 0;
        for (T8TaskTypeSummary taskTypeSummary : taskTypeSummaries.values())
        {
            count += taskTypeSummary.getTotalCount();
        }

        return count;
    }
}
