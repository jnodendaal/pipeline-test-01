package com.pilog.t8.state;

import com.pilog.t8.data.T8DataEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8StateUpdates implements Serializable
{
    private final List<T8DataEntity> inserts;
    private final List<T8DataEntity> updates;
    private final List<T8DataEntity> deletions;
    private final List<String> entityPriority;

    public T8StateUpdates(List<String> entityPriority)
    {
        this.inserts = new ArrayList<>();
        this.updates = new ArrayList<>();
        this.deletions = new ArrayList<>();
        this.entityPriority = new ArrayList<>();
        if (entityPriority != null) this.entityPriority.addAll(entityPriority);
    }

    public void addInsert(T8DataEntity entity)
    {
        inserts.add(entity);
    }

    public void addInserts(Collection<T8DataEntity> entities)
    {
        inserts.addAll(entities);
    }

    public void addUpdate(T8DataEntity entity)
    {
        updates.add(entity);
    }

    public void addUpdates(Collection<T8DataEntity> entities)
    {
        updates.addAll(entities);
    }

    public void addDeletion(T8DataEntity entity)
    {
        deletions.add(entity);
    }

    public void addDeletions(Collection<T8DataEntity> entities)
    {
        deletions.addAll(entities);
    }

    /**
     * Clears each of the data entity sets in the current object. This allows
     * for the reuse of {@code T8StateObjects}, especially those which have been
     * set up to contain initialized data entity definitions.
     */
    public void clear()
    {
        this.deletions.clear();
        this.updates.clear();
        this.inserts.clear();
    }

    public List<T8DataEntity> getInserts()
    {
        List<T8DataEntity> entities;

        entities = new ArrayList();
        for (String entityId : entityPriority)
        {
            entities.addAll(getEntities(inserts, entityId));
        }

        return entities;
    }

    public List<T8DataEntity> getUpdates()
    {
        List<T8DataEntity> entities;

        entities = new ArrayList();
        for (String entityId : entityPriority)
        {
            entities.addAll(getEntities(updates, entityId));
        }

        return entities;
    }

    public List<T8DataEntity> getDeletions()
    {
        List<T8DataEntity> entities;

        entities = new ArrayList();
        for (String entityId : entityPriority)
        {
            entities.addAll(getEntities(deletions, entityId));
        }

        return entities;
    }

    private List<T8DataEntity> getEntities(List<T8DataEntity> entities, String entityId)
    {
        List<T8DataEntity> filteredList;

        filteredList = new ArrayList<>();
        for (T8DataEntity entity : entities)
        {
            if (entity.getIdentifier().equals(entityId))
            {
                filteredList.add(entity);
            }
        }

        return filteredList;
    }
}