package com.pilog.t8.data.object;

import com.pilog.t8.data.object.T8DataObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectList implements Serializable
{
    final private List<T8DataObject> objects;

    public T8DataObjectList()
    {
        objects = new ArrayList<>();
    }

    public T8DataObjectList(T8DataObjectList objectList)
    {
        this();
        for (T8DataObject object : objectList.getObjects())
        {
            T8DataObjectList.this.addObject(object);
        }
    }

    public List<T8DataObject> getObjects()
    {
        return new ArrayList<>(objects);
    }

    public T8DataObject getObject(int index)
    {
        return objects.get(index);
    }

    public void addObject(T8DataObject object)
    {
        objects.add(object);
    }

    public T8DataObject removeObject(String key)
    {
        for (T8DataObject object : objects)
        {
            if (Objects.equals(object.getKey(), key))
            {
                objects.remove(object);
                return object;
            }
        }

        return null;
    }


    public int getSize()
    {
        return objects.size();
    }
}
