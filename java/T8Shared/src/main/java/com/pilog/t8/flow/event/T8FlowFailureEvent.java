package com.pilog.t8.flow.event;

import com.pilog.json.JsonObject;
import com.pilog.t8.flow.T8FlowController;

/**
 * @author Bouwer du Preez
 */
public class T8FlowFailureEvent extends T8FlowEvent
{
    public static final String TYPE_ID = "FLOW_FAILURE";
    
    public T8FlowFailureEvent(String flowIid)
    {
        super(flowIid);
    }

    public T8FlowFailureEvent(JsonObject object)
    {
        super(object.getString("flowIid"));
    }

    @Override
    public String getEventTypeId()
    {
        return TYPE_ID;
    }

    public String getFlowIid()
    {
        return (String)source;
    }

    @Override
    public JsonObject serialize(T8FlowController controller)
    {
        JsonObject eventObject;

        eventObject = new JsonObject();
        eventObject.add("flowIid", getFlowIid());
        return eventObject;
    }
}
