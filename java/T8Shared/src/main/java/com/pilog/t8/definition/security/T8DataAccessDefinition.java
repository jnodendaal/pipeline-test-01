package com.pilog.t8.definition.security;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8DataAccess;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataAccessDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_ACCESS";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_ACCESS";
    public static final String GROUP_NAME = "Data Access";
    public static final String GROUP_DESCRIPTION = "Configure of data access rights.";
    public static final String STORAGE_PATH = "/data_access";
    public static final String DISPLAY_NAME = "Data Access";
    public static final String DESCRIPTION = "An set of rights that determines accessibility of data within a specific context.";
    public static final String IDENTIFIER_PREFIX = "DAX_";
    public enum Datum
    {
        DATA_RECORD_ACCESS_MAP
    };
    // -------- Definition Meta-Data -------- //

    public T8DataAccessDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_VALUE_MAP, Datum.DATA_RECORD_ACCESS_MAP.toString(), "Data Record Access",  "A mapping of Data Requirement Instance Ids to the Data Access Layer Ids applicable."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_RECORD_ACCESS_MAP.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            Map<String, List<String>> identifierMap;
            List<String> identifierList;

            identifierList = new ArrayList<>();
            for (T8DefinitionMetaData definitionMetaData : definitionContext.getGroupDefinitionMetaData(getRootProjectId(), "@DG_DATA_RECORD_ACCESS"))
            {
                identifierList.add(definitionMetaData.getId());
            }

            identifierMap = new HashMap<String, List<String>>();
            identifierMap.put(null, identifierList);

            optionList = new ArrayList<T8DefinitionDatumOption>(1);
            optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
            return optionList;
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8DataAccess getDataAccess()
    {
        return new T8DataAccess(this);
    }

    public Map<String, String> getDataRecordAccessMap()
    {
        return getDefinitionDatum(Datum.DATA_RECORD_ACCESS_MAP);
    }

    public void setDataRecordAccessMap(Map<String, String> accessMap)
    {
        setDefinitionDatum(Datum.DATA_RECORD_ACCESS_MAP, accessMap);
    }
}
