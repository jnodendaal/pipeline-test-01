package com.pilog.t8.definition.notification;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.notification.T8NotificationGenerator;
import com.pilog.t8.ui.notification.T8NotificationDisplayComponent;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public abstract class T8NotificationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_NOTIFICATION";
    public static final String STORAGE_PATH = "/notifications";
    public static final String IDENTIFIER_PREFIX = "NOTIF_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8NotificationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    /**
     * Gets the list of {@code T8DataParameterDefinition}s defining the
     * definitions of the input parameters for the notification type.
     *
     * @return The list of {@code T8DataParameterDefinition}s for the
     *      notification input parameters
     */
    public abstract List<? extends T8DataParameterDefinition> getInputParameterDefinitions();

    /**
     * Returns the list of {@code T8DataParameterDefinition}s defining the
     * parameters that will be contained by the notifications generated for this
     * type.
     * @return The list of notification parameters contained by notifications of
     * this type.
     */
    public abstract List<? extends T8DataParameterDefinition> getNotificationParameterDefinitions();

    /**
     * Returns the name of this notification type to be used when displayed on
     * the UI.
     * @return The name of this notification type to be used when displayed on
     * the UI.
     */
    public abstract String getNotificationDisplayName();

    /**
     * Returns the icon to use when notifications of this type are displayed.
     * @return The icon to use when notifications of this type is used.
     */
    public abstract Icon getNotificationIcon();

    /**
     * Generates an instance of the {@code T8NotificationGenerator} for the
     * current notification definition.
     *
     * @param context The {@code T8ServerContext} which serves as one of
     *      the instantiating parameters for the {@code T8NotificationGenerator}
     *      instance
     *
     * @return An instance of the {@code T8NotificationGenerator} specific to
     *      the current notification definition
     */
    public abstract T8NotificationGenerator getNotificationGeneratorInstance(T8Context context);

    /**
     * Specifies the default display component class name for the current
     * notification type.
     * @param context
     * @param notification
     * @return The {@code T8NotificationDisplayComponent} capable of displaying
     * the supplied notification on the UI.
     */
    public abstract T8NotificationDisplayComponent getDisplayComponentInstance(T8Context context, T8Notification notification);
}