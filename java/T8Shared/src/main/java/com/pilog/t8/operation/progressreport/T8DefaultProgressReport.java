/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.operation.progressreport;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8DefaultProgressReport implements T8ProgressReport
{
    private String progressMessage;
    private String title;
    private Double progress = -1d;

    public T8DefaultProgressReport()
    {
    }

    public T8DefaultProgressReport(String progressMessage, Double progress)
    {
        this.progressMessage = progressMessage;
        this.progress = progress;
    }

    public T8DefaultProgressReport(String title, String progressMessage, Double progress)
    {
        this(progressMessage, progress);
        this.title = title;
    }


    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public Double getProgress()
    {
        return progress;
    }

    @Override
    public String getProgressMessage()
    {
        return progressMessage;
    }

    @Override
    public void setProgress(Double progress)
    {
        this.progress = progress;
    }

    @Override
    public void setProgressMessage(String progressMessage)
    {
        this.progressMessage = progressMessage;
    }
}