package com.pilog.t8.definition.filter;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.filter.T8DefinitionFilterCriterion.DefinitionFilterOperator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFilterCriteria implements T8DefinitionFilterClause, Serializable
{
    // The ID used to verify serialized object version.
    static final long serialVersionUID = 1L;
    
    private T8DefinitionFilterCriteriaDefinition definition;
    private LinkedHashMap<T8DefinitionFilterClause, DefinitionFilterConjunction> filterClauses;

    public T8DefinitionFilterCriteria()
    {
        filterClauses = new LinkedHashMap<T8DefinitionFilterClause, DefinitionFilterConjunction>();
    }
    
    public T8DefinitionFilterCriteria(T8DefinitionFilterCriteriaDefinition definition)
    {
        this();
        this.definition = definition;
    }
    
    public T8DefinitionFilterCriteria(Map<String, Object> keyMap)
    {
        this();
        addKeyFilter(keyMap, false);
    }

    public T8DefinitionFilterCriteria(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        this();
        addKeyFilter(keyMap, caseInsensitive);
    }
    
    public T8DefinitionFilterCriteria(DefinitionFilterConjunction conjunction, T8DefinitionFilterClause... filterClauses)
    {
        this();
        addFilterClauses(conjunction, filterClauses);
    }
    
    public T8DefinitionFilterCriteriaDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8DefinitionFilterCriteria copy()
    {
        T8DefinitionFilterCriteria copiedCriteria;
        
        copiedCriteria = new T8DefinitionFilterCriteria();
        for (T8DefinitionFilterClause filterClause : filterClauses.keySet())
        {
            copiedCriteria.addFilterClause(filterClauses.get(filterClause), filterClause.copy());
        }
        
        return copiedCriteria;
    }
    
    public void addKeyFilter(Map<String, Object> keyMap, boolean caseInsensitive)
    {
        for (String key : keyMap.keySet())
        {
            Object value;

            value = keyMap.get(key);
            addFilterClause(DefinitionFilterConjunction.AND, key, DefinitionFilterOperator.EQUAL, value, caseInsensitive);
        }
    }
    
    public void addFilterClause(DefinitionFilterConjunction conjunction, T8DefinitionFilterClause filterClause)
    {
        filterClauses.put(filterClause, conjunction);
    }

    public void addFilterClauses(DefinitionFilterConjunction conjunction, T8DefinitionFilterClause... filterClauses)
    {
        for (T8DefinitionFilterClause filterClause : filterClauses)
        {
            addFilterClause(conjunction, filterClause);
        }
    }
    
    public void addFilterClause(DefinitionFilterConjunction conjunction, String columnName, DefinitionFilterOperator operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(conjunction, new T8DefinitionFilterCriterion(columnName, operator, filterValue, caseInsensitive));
    }
    
    public void addFilterCriterion(String conjunction, String columnName, String operator, Object filterValue, boolean caseInsensitive)
    {
        addFilterClause(DefinitionFilterConjunction.valueOf(conjunction), new T8DefinitionFilterCriterion(columnName, DefinitionFilterOperator.valueOf(operator), filterValue, caseInsensitive));
    }
    
    public LinkedHashMap<T8DefinitionFilterClause, DefinitionFilterConjunction> getFilterClauses()
    {
        return filterClauses;
    }

    public void setFilterClauses(LinkedHashMap<T8DefinitionFilterClause, DefinitionFilterConjunction> filterClauses)
    {
        this.filterClauses = filterClauses;
    }

    public boolean hasCriteria()
    {
        return (filterClauses.size() > 0);
    }
    
    @Override
    public boolean includesDefinition(T8Definition definition)
    {
        boolean result;
        
        result = true;
        for (T8DefinitionFilterClause clause : filterClauses.keySet())
        {
            result = includesDefinition(definition, result, clause);
        }
        
        return result;
    }
    
    @Override
    public T8DefinitionFilterCriterion getFilterCriterion(String identifier)
    {
        for (T8DefinitionFilterClause clause : filterClauses.keySet())
        {
            T8DefinitionFilterCriterion filterCriterion;
            
            filterCriterion = clause.getFilterCriterion(identifier);
            if (filterCriterion != null) return filterCriterion;
        }
        
        return null;
    }
    
    private boolean includesDefinition(T8Definition definition, boolean modifier, T8DefinitionFilterClause clause)
    {
        DefinitionFilterConjunction conjunction;

        conjunction = filterClauses.get(clause);
        if (conjunction == DefinitionFilterConjunction.AND) return modifier && clause.includesDefinition(definition);
        else return modifier || clause.includesDefinition(definition);
    }

    @Override
    public List<T8DefinitionFilterCriterion> getFilterCriterionList()
    {
        ArrayList<T8DefinitionFilterCriterion> criterionList;
        
        criterionList = new ArrayList<T8DefinitionFilterCriterion>();
        for (T8DefinitionFilterClause filterClause : filterClauses.keySet())
        {
            criterionList.addAll(filterClause.getFilterCriterionList());
        }
        
        return criterionList;
    }

    @Override
    public Set<String> getFilterDatumIdentifiers()
    {
        Set<String> fieldSet;
        
        fieldSet = new HashSet<String>();
        for (T8DefinitionFilterClause filterClause : filterClauses.keySet())
        {
            fieldSet.addAll(filterClause.getFilterDatumIdentifiers());
        }
        
        return fieldSet;
    }
}
