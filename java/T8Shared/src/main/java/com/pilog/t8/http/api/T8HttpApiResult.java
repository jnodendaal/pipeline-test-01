package com.pilog.t8.http.api;

import com.pilog.json.JsonObject;

/**
 * @author Bouwer du Preez
 */
public class T8HttpApiResult
{
    private JsonObject output;
    private int statusCode;

    public T8HttpApiResult(int statusCode, JsonObject output)
    {
        this.statusCode = statusCode;
        this.output = output;
    }

    public JsonObject getOutput()
    {
        return output;
    }

    public void setOutput(JsonObject output)
    {
        this.output = output;
    }

    public int getStatusCode()
    {
        return statusCode;
    }

    public void setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;
    }
}
