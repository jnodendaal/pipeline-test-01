package com.pilog.t8.definition.patch;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionStringPatchDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_PATCH_DEFINITION_STRING";
    public static final String TYPE_IDENTIFIER = "@DT_PATCH_DEFINITION_STRING";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "PATCH_DEF_STRING_";
    public static final String DISPLAY_NAME = "T8 Definition String Patch";
    public static final String DESCRIPTION = "A definition containing settings for updating an existing definition String to a new format.";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.ROOT;
    public enum Datum {PRE_PATTERN,
                       TARGET_PATTERN,
                       POST_PATTERN,
                       REPLACEMENT_STRING};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionStringPatchDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PRE_PATTERN.toString(), "Pre-Pattern",  "The pattern that must match the prefix to the target string that will be replaced."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TARGET_PATTERN.toString(), "Target Pattern",  "The pattern that must the target string that will be replaced."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.POST_PATTERN.toString(), "Post-Pattern",  "The pattern that must match the suffix to the target string that will be replaced."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REPLACEMENT_STRING.toString(), "Replacement String",  "The new string that will replace the target string."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
  
    public String getPrePattern()
    {
        return (String)getDefinitionDatum(Datum.PRE_PATTERN.toString());
    }
    
    public void setPrePattern(String prePattern)
    {
        setDefinitionDatum(Datum.PRE_PATTERN.toString(), prePattern);
    }
    
    public String getPostPattern()
    {
        return (String)getDefinitionDatum(Datum.POST_PATTERN.toString());
    }
    
    public void setPostPattern(String postPattern)
    {
        setDefinitionDatum(Datum.POST_PATTERN.toString(), postPattern);
    }
    
    public String getTargetPattern()
    {
        return (String)getDefinitionDatum(Datum.TARGET_PATTERN.toString());
    }
    
    public void setTargetPattern(String targetPattern)
    {
        setDefinitionDatum(Datum.TARGET_PATTERN.toString(), targetPattern);
    }
    
    public String getReplacementString()
    {
        return (String)getDefinitionDatum(Datum.REPLACEMENT_STRING.toString());
    }
    
    public void setReplacementString(String replacementString)
    {
        setDefinitionDatum(Datum.REPLACEMENT_STRING.toString(), replacementString);
    }
}

