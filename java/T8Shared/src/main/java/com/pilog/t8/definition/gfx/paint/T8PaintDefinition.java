package com.pilog.t8.definition.gfx.paint;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.awt.Paint;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8PaintDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_GFX_PAINT";
    public static final String IDENTIFIER_PREFIX = "GFX_PAINT_";
    public static final String STORAGE_PATH = "/paints";
    // -------- Definition Meta-Data -------- //
    
    public T8PaintDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return new ArrayList<T8DefinitionDatumType>();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }
    
    public abstract Paint getNewPaintInstance();
}
