package com.pilog.t8.definition.ui.dialog;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DialogDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DIALOG";
    public static final String DISPLAY_NAME = "Dialog";
    public static final String DESCRIPTION = "A UI container to which other UI components can be added all of which will then be displayed in a separate window from the main application.";
    public static final String IDENTIFIER_PREFIX = "C_DIALOG_";
    private enum Datum {CONTENT_COMPONENT_DEFINITION,
                        BACKGROUND_PAINTER_IDENTIFIER,
                        TITLE,
                        MODAL,
                        RESIZABLE,
                        OPEN_MAXIMIZED,
                        SIZE_TYPE,
                        WIDTH,
                        HEIGHT,
                        ANIMATED};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition backgroundPainterDefinition;
    public enum SizeType {ABSOLUTE, PERCENTAGE}

    public T8DialogDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.CONTENT_COMPONENT_DEFINITION.toString(), "Content Component", "The main content component displayed by this dialog."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TITLE.toString(), "Title", "A title of the dialog that will be displayed on the dialog frame."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MODAL.toString(), "Modal", "A boolean value indicating whether or not the module should be displayed as modal.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RESIZABLE.toString(), "Resizable", "A boolean value indicating whether or not the module should be allowed to be resized.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPEN_MAXIMIZED.toString(), "Open Maximised", "A boolean value indicating whether or not the module should be opened maximised.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ANIMATED.toString(), "Animated", "If this dialog should animate upon opening and closing.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SIZE_TYPE.toString(), "Size Type", "What type of size should be used when determining the size of the module (if the maximised option is not used).", SizeType.ABSOLUTE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.WIDTH.toString(), "Width", "The width (in pixels) to which the dialog will be resized when it is opened.", 500));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HEIGHT.toString(), "Height", "The height (in pixels) to which the dialog will be resized when it is opened.", 500));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting this dialog's backgorund."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONTENT_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.SIZE_TYPE.toString().equals(datumIdentifier)) return createStringOptions(SizeType.values());
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String painterId;

        painterId = getBackgroundPainterIdentifier();
        if (painterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        // Validates if the Component Definition has been set.
        if (getContentComponentDefinition() == null)
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.CONTENT_COMPONENT_DEFINITION.toString(), "Content Component Definition not set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        if (!isOpenMaximized() && getSizeType() == SizeType.PERCENTAGE)
        {
            if (getWidth() > 100 || getWidth() < 0)
            {
                validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.WIDTH.toString(), "Width value not within allowed range. Should be between 0 and 100.", T8DefinitionValidationError.ErrorType.WARNING));
            }
            if (getHeight() > 100 || getHeight() < 0)
            {
                validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.HEIGHT.toString(), "Height value not within allowed range. Should be between 0 and 100.", T8DefinitionValidationError.ErrorType.WARNING));
            }
        }

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.dialog.T8Dialog", new Class<?>[]{T8DialogDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DialogAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DialogAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        T8ComponentDefinition contentComponentDefinition;

        contentComponentDefinition = getContentComponentDefinition();
        if (contentComponentDefinition != null) return ArrayLists.typeSafeList(contentComponentDefinition);
        else return new ArrayList<>();
    }

    public T8ComponentDefinition getContentComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.CONTENT_COMPONENT_DEFINITION.toString());
    }

    public void setContentComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        setDefinitionDatum(Datum.CONTENT_COMPONENT_DEFINITION.toString(), componentDefinition);
    }

    public String getTitle()
    {
        return (String)getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String title)
    {
        setDefinitionDatum(Datum.TITLE.toString(), title);
    }

    public boolean isModal()
    {
        return (Boolean)getDefinitionDatum(Datum.MODAL.toString());
    }

    public void setModal(boolean modal)
    {
        setDefinitionDatum(Datum.MODAL.toString(), modal);
    }

    public boolean isResizable()
    {
        Boolean resizable;

        resizable = (Boolean)getDefinitionDatum(Datum.RESIZABLE.toString());
        return resizable == null ? true : resizable;
    }

    public void setResizable(boolean resizable)
    {
        setDefinitionDatum(Datum.RESIZABLE.toString(), resizable);
    }

    public boolean isOpenMaximized()
    {
        Boolean openMaximized;

        openMaximized = (Boolean)getDefinitionDatum(Datum.OPEN_MAXIMIZED.toString());
        return openMaximized == null ? false : openMaximized;
    }

    public void setOpenMaximized(boolean openMaximized)
    {
        setDefinitionDatum(Datum.OPEN_MAXIMIZED.toString(), openMaximized);
    }

    public boolean isAnimated()
    {
        Boolean animated;

        animated = (Boolean)getDefinitionDatum(Datum.ANIMATED.toString());
        return animated == null ? false : animated;
    }

    public void setAnimated(boolean animated)
    {
        setDefinitionDatum(Datum.ANIMATED.toString(), animated);
    }

    public SizeType getSizeType()
    {
        String sizeType;

        sizeType = (String)getDefinitionDatum(Datum.SIZE_TYPE.toString());
        return sizeType == null ? SizeType.ABSOLUTE : SizeType.valueOf(sizeType);
    }

    public void setSizeType(SizeType sizeType)
    {
        setDefinitionDatum(Datum.SIZE_TYPE.toString(), sizeType.toString());
    }

    public int getWidth()
    {
        return (Integer)getDefinitionDatum(Datum.WIDTH.toString());
    }

    public void setWidth(int width)
    {
        setDefinitionDatum(Datum.WIDTH.toString(), width);
    }

    public int getHeight()
    {
        return (Integer)getDefinitionDatum(Datum.HEIGHT.toString());
    }

    public void setHeight(int height)
    {
        setDefinitionDatum(Datum.HEIGHT.toString(), height);
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }
}
