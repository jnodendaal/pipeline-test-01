package com.pilog.t8.ui.event;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import java.util.EventObject;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentEvent extends EventObject
{
    private final T8Component component;
    private final T8ComponentEventDefinition eventDefinition;
    private final Map<String, Object> eventParameters;

    public T8ComponentEvent(T8Component component, T8ComponentEventDefinition eventDefinition, Map<String, Object> eventParameters)
    {
        super(component);
        this.component = component;
        this.eventDefinition = eventDefinition;
        this.eventParameters = eventParameters;
    }

    public T8Component getComponent()
    {
        return component;
    }

    public T8ComponentEventDefinition getEventDefinition()
    {
        return eventDefinition;
    }

    public Map<String, Object> getEventParameters()
    {
        return eventParameters;
    }

    @Override
    public String toString()
    {
        return "T8ComponentEvent{" + "component=" + component.getComponentDefinition() + ", eventDefinition=" + eventDefinition + ", eventParameters=" + eventParameters + '}';
    }
}
