package com.pilog.t8.definition.gfx.painter;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import java.awt.Insets;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AbstractLayoutPainterDefinition extends T8PainterDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {TOP_INSET, BOTTOM_INSET, LEFT_INSET, RIGHT_INSET, FILL_VERTICAL, FILL_HORIZONTAL, HORIZONTAL_ALIGNMENT, VERTICAL_ALIGNMENT};
    // -------- Definition Meta-Data -------- //

    public enum HorizontalAlignment {LEFT, CENTER, RIGHT};
    public enum VerticalAlignment {TOP, CENTER, BOTTOM};
    
    public T8AbstractLayoutPainterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.TOP_INSET.toString(), "Top Inset", "The inset on the top side of the painted area (in pixels).", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.BOTTOM_INSET.toString(), "Bottom Inset", "The inset on the bottom side of the painted area (in pixels).", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.LEFT_INSET.toString(), "Left Inset", "The inset on the left side of the painted area (in pixels).", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.RIGHT_INSET.toString(), "Right Inset", "The inset on the right side of the painted area (in pixels).", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FILL_VERTICAL.toString(), "Fill Vertical", "Indicates if the painter should stretch to fill the available space vertically.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FILL_HORIZONTAL.toString(), "Fill Horizontal", "Indicates if the painter should stretch to fill the available space horizontally.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HORIZONTAL_ALIGNMENT.toString(), "Horizontal Alignment", "If the paint width is smaller than the available space, this setting determines to which side the paint will be horizontally positioned within the available area.", HorizontalAlignment.CENTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.VERTICAL_ALIGNMENT.toString(), "Vertical Alignment", "If the paint height is smaller than the available space, this setting determines to which side the paint will be vertically positioned within the available area.", VerticalAlignment.CENTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.HORIZONTAL_ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(HorizontalAlignment.values());
        else if (Datum.VERTICAL_ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(VerticalAlignment.values());
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    public Insets getInsets()
    {
        Integer topInset;
        Integer bottomInset;
        Integer leftInset;
        Integer rightInset;
        
        topInset = getTopInset();
        bottomInset = getBottomInset();
        leftInset = getLeftInset();
        rightInset = getRightInset();
        
        return new Insets(topInset != null ? topInset : 0, leftInset != null ? leftInset : 0, bottomInset != null ? bottomInset : 0, rightInset != null ? rightInset : 0);
    }
    
    public Integer getTopInset()
    {
        return (Integer)getDefinitionDatum(Datum.TOP_INSET.toString());
    }
    
    public void setTopInset(int inset)
    {
        setDefinitionDatum(Datum.TOP_INSET.toString(), inset);
    }
    
    public Integer getBottomInset()
    {
        return (Integer)getDefinitionDatum(Datum.BOTTOM_INSET.toString());
    }
    
    public void setBottomInset(int inset)
    {
        setDefinitionDatum(Datum.BOTTOM_INSET.toString(), inset);
    }
    
    public Integer getLeftInset()
    {
        return (Integer)getDefinitionDatum(Datum.LEFT_INSET.toString());
    }
    
    public void setLeftInset(int inset)
    {
        setDefinitionDatum(Datum.LEFT_INSET.toString(), inset);
    }
    
    public Integer getRightInset()
    {
        return (Integer)getDefinitionDatum(Datum.RIGHT_INSET.toString());
    }
    
    public void setRightInset(int inset)
    {
        setDefinitionDatum(Datum.RIGHT_INSET.toString(), inset);
    }
    
    public boolean getFillHorizontal()
    {
        Boolean fillHorizontal;
        
        fillHorizontal = (Boolean)getDefinitionDatum(Datum.FILL_HORIZONTAL.toString());
        return fillHorizontal != null && fillHorizontal;
    }
    
    public void setFillHorizontal(boolean fillHorizontal)
    {
        setDefinitionDatum(Datum.FILL_HORIZONTAL.toString(), fillHorizontal);
    }
    
    public boolean getFillVertical()
    {
        Boolean fillVertical;
        
        fillVertical = (Boolean)getDefinitionDatum(Datum.FILL_VERTICAL.toString());
        return fillVertical != null && fillVertical;
    }
    
    public void setFillVertical(boolean fillVertical)
    {
        setDefinitionDatum(Datum.FILL_VERTICAL.toString(), fillVertical);
    }
    
    public HorizontalAlignment getHorizontalAlignment()
    {
        return HorizontalAlignment.valueOf((String)getDefinitionDatum(Datum.HORIZONTAL_ALIGNMENT.toString()));
    }

    public void setHorizontalAlignment(HorizontalAlignment alignment)
    {
         setDefinitionDatum(Datum.HORIZONTAL_ALIGNMENT.toString(), alignment.toString());
    }
    
    public VerticalAlignment getVerticalAlignment()
    {
        return VerticalAlignment.valueOf((String)getDefinitionDatum(Datum.VERTICAL_ALIGNMENT.toString()));
    }

    public void setVerticalAlignment(VerticalAlignment alignment)
    {
         setDefinitionDatum(Datum.VERTICAL_ALIGNMENT.toString(), alignment.toString());
    }    
}
