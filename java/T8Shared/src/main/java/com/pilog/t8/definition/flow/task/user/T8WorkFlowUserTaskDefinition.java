package com.pilog.t8.definition.flow.task.user;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowUserTaskDefinition extends T8FlowTaskDefinition implements T8WorkFlowTaskDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FLOW_SYSTEM_TASK_USER";
    public static final String DISPLAY_NAME = "User Task";
    public static final String DESCRIPTION = "A task that is performed by the user with assistance of a software system.";
    public enum Datum {USER_INTERFACE_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8WorkFlowUserTaskDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.USER_INTERFACE_DEFINITIONS.toString(), "User Interfaces", "Definitions of possible user interfaces that may be used to complete the task."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.USER_INTERFACE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8WorkFlowUserTaskUIDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public TaskExecutionType getExecutionType()
    {
        return TaskExecutionType.EXTERNAL;
    }

    @Override
    public T8FlowTask getNewTaskInstance(T8TaskDetails taskDetails) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.flow.work.task.user.T8WorkFlowUserTask").getConstructor(this.getClass(), T8TaskDetails.class);
        return (T8FlowTask)constructor.newInstance(this, taskDetails);
    }
    
    public List<T8WorkFlowUserTaskUIDefinition> getUserInterfaceDefinitions()
    {
        return (List<T8WorkFlowUserTaskUIDefinition>)getDefinitionDatum(Datum.USER_INTERFACE_DEFINITIONS.toString());
    }
    
    public void setUserInterfaceDefinitions(List<T8WorkFlowUserTaskUIDefinition> definitions)
    {
        setDefinitionDatum(Datum.USER_INTERFACE_DEFINITIONS.toString(), definitions);
    }
}
