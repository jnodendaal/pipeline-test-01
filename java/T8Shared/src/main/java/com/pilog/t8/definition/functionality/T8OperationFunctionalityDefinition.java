package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OperationFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_OPERATION";
    public static final String DISPLAY_NAME = "Operation Functionality";
    public static final String DESCRIPTION = "A functionality that executes a server operation.";
    public static final String IDENTIFIER_PREFIX = "FNC_OP_";
    public enum Datum
    {
        OPERATION_IDENTIFIER,
        INPUT_PARAMETER_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public T8OperationFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.OPERATION_IDENTIFIER.toString(), "Operation", "The operation to execute."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Functionality to Operation Input", "A mapping of Functionality Parameters to the corresponding Operation Input Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.OPERATION_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            operationId = getOperationIdentifier();
            if (operationId != null)
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);
                if (operationDefinition != null)
                {
                    List<T8DataParameterDefinition> functionalityParameterDefinitions;
                    List<T8DataParameterDefinition> operationParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    functionalityParameterDefinitions = getInputParameterDefinitions();
                    operationParameterDefinitions = operationDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((functionalityParameterDefinitions != null) && (operationParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition functionalityParameterDefinition : functionalityParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition operationParameterDefinition : operationParameterDefinitions)
                            {
                                identifierList.add(operationParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(functionalityParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8OperationFunctionalityInstance").getConstructor(T8Context.class, T8OperationFunctionalityDefinition.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this);
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.T8OperationFunctionalityInstance").getConstructor(T8Context.class, T8OperationFunctionalityDefinition.class, T8FunctionalityState.class);
        return (T8FunctionalityInstance)constructor.newInstance(context, this, state);
    }

    public String getOperationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString());
    }

    public void setOperationIdentifierDefinition(String operationIdentifier)
    {
        setDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString(), operationIdentifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
