package com.pilog.t8.functionality.state;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.state.T8StateDataSet;
import com.pilog.t8.state.T8StateParameterMap;
import com.pilog.t8.state.T8StateUpdates;
import com.pilog.t8.utilities.collections.HashMaps;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.pilog.t8.definition.functionality.T8FunctionalityManagerResource.*;
import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityState implements Serializable
{
    private String functionalityIid;
    private String functionalityId;
    private String initiatorId;
    private String initiatorIid;
    private String flowIid;
    private String operationIid;
    private String sessionId;
    private boolean updated;
    private boolean inserted;
    private T8Timestamp timeAccessed;
    private final T8StateParameterMap inputParameters;
    private final List<T8FunctionalityDataObjectState> objectStates;

    public T8FunctionalityState(String functionalityIid)
    {
        this.functionalityIid = functionalityIid;
        this.objectStates = new ArrayList<>();
        this.inputParameters = new T8StateParameterMap(null, STATE_FUNC_INPUT_PAR_DE_IDENTIFIER, HashMaps.create(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid, STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_PARENT_PARAMETER_IID, null), null);
        this.inserted = true;
        this.updated = false;
    }

    public T8FunctionalityState(String functionalityIid, String functionalityId)
    {
        this(functionalityIid);
        this.functionalityId = functionalityId;
    }

    public T8FunctionalityState(T8DataEntity functionalityEntity, T8StateDataSet dataSet)
    {
        // Construct the notification state.
        this.functionalityId = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_FUNCTIONALITY_ID);
        this.functionalityIid = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_FUNCTIONALITY_IID);
        this.initiatorId = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_INITIATOR_ID);
        this.initiatorIid = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_INITIATOR_IID);
        this.flowIid = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_FLOW_IID);
        this.operationIid = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_OPERATION_IID);
        this.sessionId = (String)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_SESSION_ID);
        this.timeAccessed = (T8Timestamp)functionalityEntity.getFieldValue(STATE_FUNC_DE_IDENTIFIER + F_TIME_ACCESSED);
        this.updated = false;
        this.inserted = false;

        // Construct the input parameters.
        this.inputParameters = new T8StateParameterMap(null, STATE_FUNC_INPUT_PAR_DE_IDENTIFIER, HashMaps.create(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid, STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_PARENT_PARAMETER_IID, null), dataSet);

        // Construct the data objects.
        this.objectStates = new ArrayList<>();
        for (T8DataEntity dataObjectEntity : dataSet.getEntities(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER))
        {
            T8FunctionalityState.this.addDataObjectState(new T8FunctionalityDataObjectState(dataObjectEntity));
        }
    }

    public T8DataEntity createEntity(T8DataTransaction tx) throws Exception
    {
        T8DataEntity newEntity;
        String entityId;

        entityId = STATE_FUNC_DE_IDENTIFIER;
        newEntity = tx.create(entityId, null);
        newEntity.setFieldValue(entityId + F_FUNCTIONALITY_IID, functionalityIid);
        newEntity.setFieldValue(entityId + F_FUNCTIONALITY_ID, functionalityId);
        newEntity.setFieldValue(entityId + F_INITIATOR_ID, initiatorId);
        newEntity.setFieldValue(entityId + F_INITIATOR_IID, initiatorIid);
        newEntity.setFieldValue(entityId + F_FLOW_IID, flowIid);
        newEntity.setFieldValue(entityId + F_OPERATION_IID, operationIid);
        newEntity.setFieldValue(entityId + F_SESSION_ID, sessionId);
        newEntity.setFieldValue(entityId + F_TIME_ACCESSED, timeAccessed);
        return newEntity;
    }

    public synchronized void statePersisted()
    {
        // Reset this state's flags.
        inserted = false;
        updated = false;

        // Reset input parameter flags.
        inputParameters.statePersisted();

        // Reset data object state flags.
        for (T8FunctionalityDataObjectState objectState : objectStates)
        {
            objectState.statePersisted();
        }
    }

    public synchronized void addUpdates(T8DataTransaction tx, T8StateUpdates updates) throws Exception
    {
        // Add this flow state's updates.
        if (inserted) updates.addInsert(createEntity(tx));
        else if (updated) updates.addUpdate(createEntity(tx));

        // Add input parameter updates.
        inputParameters.addUpdates(tx, updates);

        // Add the data object updates.
        for (T8FunctionalityDataObjectState dataObjectState : objectStates)
        {
            dataObjectState.addUpdates(tx, updates);
        }
    }

    public String getFunctionalityIid()
    {
        return functionalityIid;
    }

    public void setFunctionalityIid(String functionalityIid)
    {
        this.functionalityIid = functionalityIid;
    }

    public String getFunctionalityId()
    {
        return functionalityId;
    }

    public void setFunctionalityId(String functionalityId)
    {
        this.functionalityId = functionalityId;
    }

    public String getInitiatorId()
    {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId)
    {
        this.initiatorId = initiatorId;
    }

    public String getInitiatorIid()
    {
        return initiatorIid;
    }

    public void setInitiatorIid(String initiatorIid)
    {
        this.initiatorIid = initiatorIid;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String flowIid)
    {
        this.flowIid = flowIid;
    }

    public String getOperationIid()
    {
        return operationIid;
    }

    public void setOperationIid(String operationIid)
    {
        this.operationIid = operationIid;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public T8Timestamp getTimeAccessed()
    {
        return timeAccessed;
    }

    public Long getTimeAccessedMilliseconds()
    {
        return timeAccessed != null ? timeAccessed.getMilliseconds() : null;
    }

    public void setTimeAccessed(T8Timestamp timeStarted)
    {
        if (!Objects.equals(this.timeAccessed, timeStarted))
        {
            updated = true;
            this.timeAccessed = timeStarted;
        }
    }

    public Map<String, Object> getInputParameters()
    {
        return new HashMap<>(inputParameters);
    }

    public void setInputParameters(Map<String, Object> inputParameters)
    {
        this.inputParameters.clear();
        if (inputParameters != null)
        {
            this.inputParameters.putAll(inputParameters);
        }
    }

    public List<T8FunctionalityDataObjectState> getDataObjectStates()
    {
        return new ArrayList<>(objectStates);
    }

    public void setDataObjectState(List<T8FunctionalityDataObjectState> objects)
    {
        this.objectStates.clear();
        if (objects != null)
        {
            this.objectStates.addAll(objects);
        }
    }

    public void addDataObjectState(T8FunctionalityDataObjectState object)
    {
        object.setFunctionalityIid(this.functionalityIid);
        this.objectStates.add(object);
    }

    public T8FunctionalityDataObjectState getDataObjectState(String objectIid)
    {
        for (T8FunctionalityDataObjectState object : objectStates)
        {
            if (object.getDataObjectIid().equals(objectIid)) return object;
        }

        return null;
    }

    public boolean containsDataObjectState(String objectIid)
    {
        return getDataObjectState(objectIid) != null;
    }

    public T8FunctionalityDataObjectState getDataObjectStateById(String objectId)
    {
        for (T8FunctionalityDataObjectState object : objectStates)
        {
            if (object.getDataObjectId().equals(objectId)) return object;
        }

        return null;
    }

    public boolean containsDataObjectStateType(String objectId)
    {
        return getDataObjectStateById(objectId) != null;
    }

    @Override
    public String toString()
    {
        return "T8FunctionalityState{" + "functionalityIid=" + functionalityIid + ", functionalityId=" + functionalityId + '}';
    }
}
