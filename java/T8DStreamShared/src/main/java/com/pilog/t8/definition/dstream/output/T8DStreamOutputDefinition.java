package com.pilog.t8.definition.dstream.output;

import com.pilog.t8.definition.T8Definition;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamOutputDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_OUTPUT";
    public static final String IDENTIFIER_PREFIX = "DSTREAM_OUTPUT_";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //
    
    public T8DStreamOutputDefinition(String identifier)
    {
        super(identifier);
    }
}
