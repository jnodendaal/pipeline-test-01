package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordCreationContext
{
    public DataRecordConstructionResult constructDataRecord(T8DataTransaction tx, String rootRecordID, DataRecord parentRecord, String drInstanceID, Map<String, Object> dataParameters) throws Exception;
    public T8Context getContext();
    public T8ServerContext getServerContext();
    public T8SessionContext getSessionContext();
    public T8FileManager getFileManager();
    public TerminologyProvider getTerminologyProvider();
    public T8OrganizationOntologyFactory getOntologyFactory();
    public T8DataRecordValueStringGenerator getFFTValueStringGenerator();

    public List<String> getConceptIDsByCode(T8DataTransaction tx, String orgID, String conceptODTID, String code) throws Exception;
    public List<String> getConceptIDsByTerm(T8DataTransaction tx, String orgID, String conceptODTID, String term) throws Exception;
}
