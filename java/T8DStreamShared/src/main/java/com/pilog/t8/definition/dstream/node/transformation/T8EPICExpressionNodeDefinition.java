package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICExpressionNodeDefinition extends T8DStreamTransformationNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_EPIC_EXPRESSION";
    public static final String DISPLAY_NAME = "EPIC Expression";
    public static final String DESCRIPTION = "A node that evaluates input data using a boolean expression and conditionally sends a computed value back to the stream.";
    public static final String VERSION = "0";
    public enum Datum {CONDITION_EXPRESSION,
                       VALUE_EXPRESSION,
                       RESULT_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8EPICExpressionNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression",  "The boolean expression that will be used to evaluate input data.  The result of the value expression will only be computed and added to the stream if the condition result is TRUE."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.VALUE_EXPRESSION.toString(), "Value Expression",  "The expression that will be used to compute a result value."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.RESULT_PARAMETER_IDENTIFIER.toString(), "Result Parameter", "The Stream Parameter to which the result of the value expression will be sent."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.RESULT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8EPICExpressionNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public String getValueExpression()
    {
        return (String)getDefinitionDatum(Datum.VALUE_EXPRESSION.toString());
    }

    public void setValueExpression(String expression)
    {
        setDefinitionDatum(Datum.VALUE_EXPRESSION.toString(), expression);
    }

    public String getResultParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RESULT_PARAMETER_IDENTIFIER.toString());
    }

    public void setResultParameterIdentifier(String parameterIdentifier)
    {
        setDefinitionDatum(Datum.RESULT_PARAMETER_IDENTIFIER.toString(), parameterIdentifier);
    }
}
