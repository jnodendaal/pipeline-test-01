package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public final class T8TerminologyLookupTypeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TERMINOLOGY_LOOKUP_TYPE";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_NODE_TERMINOLOGY_LOOKUP_TYPE";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Terminology Lookup Type";
    public static final String DESCRIPTION = "A setup of type of terminology to lookup for a map of concepts.";
    public static final String VERSION = "0";
    public enum Datum {LANGUAGE_ID, 
                       CONCEPT_TERMINOLOGY_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8TerminologyLookupTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.LANGUAGE_ID.toString(), "Language ID", "The language to use when finding terminology."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.CONCEPT_TERMINOLOGY_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Stream (Concept) to Stream (Terminology)", "A concept from each of the input parameters will be taken from the stream, its terminology found and then placed back into the stream in the mapped output parameter."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONCEPT_TERMINOLOGY_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8DStreamDefinition streamDefinition;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                List<T8DataParameterDefinition> streamParameterDefinitions;
                TreeMap<String, List<String>> identifierMap;

                streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                identifierMap = new TreeMap<String, List<String>>();
                if (streamParameterDefinitions != null)
                {
                    for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<String>();
                        for (T8DataParameterDefinition innerStreamParameterDefinition : streamParameterDefinitions)
                        {
                            identifierList.add(innerStreamParameterDefinition.getIdentifier());
                        }

                        Collections.sort(identifierList);
                        identifierMap.put(streamParameterDefinition.getIdentifier(), identifierList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getLanguageID()
    {
        return (String)getDefinitionDatum(Datum.LANGUAGE_ID.toString());
    }
    
    public void setLanguageID(String identifier)
    {
        setDefinitionDatum(Datum.LANGUAGE_ID.toString(), identifier);
    }

    public Map<String, String> getConceptTerminologyParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.CONCEPT_TERMINOLOGY_PARAMETER_MAPPING.toString());
    }
    
    public void setConceptTerminologyParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.CONCEPT_TERMINOLOGY_PARAMETER_MAPPING.toString(), mapping);
    }
}
