package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationContext;
import com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationMapping;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DynamicDataRequirementMappingDefinition extends T8DataRecordCreationMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_REQUIREMENT_DYNAMIC_MAPPING";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Dynamic Data Requirement Mapping";
    public static final String DESCRIPTION = "A mapping of input parameters to a specific part of a Data Requirement structure.";
    public enum Datum {RECORD_FFT_PARAMETER_IDENTIFIER,
                       PROPERTY_FFT_PARAMETER_IDENTIFIER,
                       DR_ID_PARAMETER_IDENTIFIER,
                       PROPERTY_ID_PARAMETER_IDENTIFIER,
                       FIELD_ID_PARAMETER_IDENTIFIER,
                       VALUE_PARAMETER_IDENTIFIER,
                       CONCEPT_ID_PARAMETER_IDENTIFIER,
                       CONCEPT_TERM_PARAMETER_IDENTIFIER,
                       CONCEPT_CODE_PARAMETER_IDENTIFIER,
                       LOWER_VALUE_PARAMETER_IDENTIFIER,
                       UPPER_VALUE_PARAMETER_IDENTIFIER,
                       UOM_ID_PARAMETER_IDENTIFIER,
                       UOM_TERM_PARAMETER_IDENTIFIER,
                       UOM_CODE_PARAMETER_IDENTIFIER,
                       QOM_ID_PARAMETER_IDENTIFIER,
                       QOM_TERM_PARAMETER_IDENTIFIER,
                       QOM_CODE_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DynamicDataRequirementMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_FFT_PARAMETER_IDENTIFIER.toString(), "Record FFT Parameter", "The stream parameter mapped to the FFT of the record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROPERTY_FFT_PARAMETER_IDENTIFIER.toString(), "Property FFT Parameter", "The stream parameter mapped to the FFT of the property."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_ID_PARAMETER_IDENTIFIER.toString(), "DR Parameter", "The stream parameter that specifies the DR of the record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString(), "Property Parameter", "The stream parameter that specifies the record Property."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FIELD_ID_PARAMETER_IDENTIFIER.toString(), "Field Parameter", "The stream parameter that specifies the property Field."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.VALUE_PARAMETER_IDENTIFIER.toString(), "Value Parameter", "The stream parameter that specifies the property/field value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONCEPT_ID_PARAMETER_IDENTIFIER.toString(), "Concept ID Parameter", "The stream parameter that specifies the controlled concept ID."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONCEPT_TERM_PARAMETER_IDENTIFIER.toString(), "Concept Term Parameter", "The stream parameter that specifies the controlled concept term."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONCEPT_CODE_PARAMETER_IDENTIFIER.toString(), "Concept Code Parameter", "The stream parameter that specifies the controlled concept code."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.LOWER_VALUE_PARAMETER_IDENTIFIER.toString(), "Lower Value Parameter", "The stream parameter that contains the lower value of a range."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.UPPER_VALUE_PARAMETER_IDENTIFIER.toString(), "Upper Value Parameter", "The stream parameter that contains the upper value of a range."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.UOM_ID_PARAMETER_IDENTIFIER.toString(), "UOM ID Parameter", "The stream parameter that specifies the UOM ID of a measured number type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.UOM_TERM_PARAMETER_IDENTIFIER.toString(), "UOM Term Parameter", "The stream parameter that specifies the UOM Term of a measured number type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.UOM_CODE_PARAMETER_IDENTIFIER.toString(), "UOM Code Parameter", "The stream parameter that specifies the UOM Code of a measured number type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.QOM_ID_PARAMETER_IDENTIFIER.toString(), "Qualifier ID Parameter", "The stream parameter that specified the Qualifier ID of a measured number type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.QOM_TERM_PARAMETER_IDENTIFIER.toString(), "Qualifier Term Parameter", "The stream parameter that specified the Qualifier Term of a measured number type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.QOM_CODE_PARAMETER_IDENTIFIER.toString(), "Qualifier Code Parameter", "The stream parameter that specified the Qualifier Code of a measured number type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.RECORD_FFT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.PROPERTY_FFT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.DR_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.FIELD_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.VALUE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.CONCEPT_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.CONCEPT_TERM_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.CONCEPT_CODE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.LOWER_VALUE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.UPPER_VALUE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.UOM_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.UOM_CODE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.UOM_TERM_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.QOM_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.QOM_CODE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else if (Datum.QOM_TERM_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DataRecordCreationMapping createNewMappingInstance(T8DataRecordCreationContext context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.output.datarecord.T8DynamicDataRequirementMapping").getConstructor(T8DataRecordCreationContext.class, this.getClass());
        return (T8DataRecordCreationMapping)constructor.newInstance(context, this);
    }

    public String getRecordFFTParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RECORD_FFT_PARAMETER_IDENTIFIER.toString());
    }

    public void setRecordFFTParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.RECORD_FFT_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getPropertyFFTParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_FFT_PARAMETER_IDENTIFIER.toString());
    }

    public void setPropertyFFTParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROPERTY_FFT_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getDRIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_ID_PARAMETER_IDENTIFIER.toString());
    }

    public void setDRIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getPropertyIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString());
    }

    public void setPropertyIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getFieldIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FIELD_ID_PARAMETER_IDENTIFIER.toString());
    }

    public void setFieldIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FIELD_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getValueParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.VALUE_PARAMETER_IDENTIFIER.toString());
    }

    public void setValueParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.VALUE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getConceptIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONCEPT_ID_PARAMETER_IDENTIFIER.toString());
    }

    public void setConceptIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONCEPT_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getConceptTermParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONCEPT_TERM_PARAMETER_IDENTIFIER.toString());
    }

    public void setConceptTermParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONCEPT_TERM_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getConceptCodeParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONCEPT_CODE_PARAMETER_IDENTIFIER.toString());
    }

    public void setConceptCodeParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONCEPT_CODE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getLowerValueParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.LOWER_VALUE_PARAMETER_IDENTIFIER.toString());
    }

    public void setLowerValueParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.LOWER_VALUE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getUpperValueParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.UPPER_VALUE_PARAMETER_IDENTIFIER.toString());
    }

    public void setUpperValueParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.UPPER_VALUE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getUOMIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.UOM_ID_PARAMETER_IDENTIFIER.toString());
    }

    public void setUOMIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.UOM_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getUOMTermParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.UOM_TERM_PARAMETER_IDENTIFIER.toString());
    }

    public void setUOMTermParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.UOM_TERM_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getUOMCodeParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.UOM_CODE_PARAMETER_IDENTIFIER.toString());
    }

    public void setUOMCodeParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.UOM_CODE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getQOMIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.QOM_ID_PARAMETER_IDENTIFIER.toString());
    }

    public void setQOMIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.QOM_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getQOMTermParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.QOM_TERM_PARAMETER_IDENTIFIER.toString());
    }

    public void setQOMTermParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.QOM_TERM_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getQOMCodeParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.QOM_CODE_PARAMETER_IDENTIFIER.toString());
    }

    public void setQOMCodeParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.QOM_CODE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
