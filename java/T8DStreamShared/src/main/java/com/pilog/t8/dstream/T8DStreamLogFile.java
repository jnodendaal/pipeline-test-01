package com.pilog.t8.dstream;

/**
 * @author Bouwer du Preez
 */
public interface T8DStreamLogFile
{
    public void openFile() throws Exception;
    public void log(String logEntry) throws Exception;
    public void closeFile() throws Exception;
}
