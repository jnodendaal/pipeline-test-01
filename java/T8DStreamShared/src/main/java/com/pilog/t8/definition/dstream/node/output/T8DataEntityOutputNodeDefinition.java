package com.pilog.t8.definition.dstream.node.output;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityOutputNodeDefinition extends T8DStreamOutputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_OUTPUT_DATA_ENTITY";
    public static final String DISPLAY_NAME = "Data Entity Output";
    public static final String DESCRIPTION = "A node that writes data to a specific data entity.";
    public static final String VERSION = "0";
    public enum Datum
    {
        DATA_ENTITY_IDENTIFIER,
        COMMIT_INTERVAL,
        INSERT,
        UPDATE,
        DELETE,
        BLIND,
        FIELD_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public T8DataEntityOutputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The identifier of the data entity to which data will be written."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.COMMIT_INTERVAL.toString(), "Commit Interval",  "The interval (in data rows) at which the transaction will be committed.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INSERT.toString(), "Insert",  "A boolean value indicating whether or not rows that do not match any of the records in the table should be inserted as new data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.UPDATE.toString(), "Update",  "A boolean value indicating whether or not rows in the table should be updated using input data if the key matches."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DELETE.toString(), "Delete",  "A boolean value indicating whether or not rows in the table should be deleted if the input key matches."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.BLIND.toString(), "Blind",  "A boolean value indicating whether or not incoming data should be inserted blindly without checking for pre-existing data (higher risk but increased performance)."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.FIELD_MAPPING.toString(), "Field Mapping:  Stream to Entity", "A mapping of stream parameters to the corresponding entity fields."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;
                T8DStreamDefinition streamDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                streamDefinition = (T8DStreamDefinition)this.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((entityDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8DataEntityFieldDefinition> entityFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    entityFieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((streamParameterDefinitions != null) && (entityFieldDefinitions != null))
                    {
                        for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataEntityFieldDefinition entityFieldDefinition : entityFieldDefinitions)
                            {
                                identifierList.add(entityFieldDefinition.getPublicIdentifier());
                            }
                            Collections.sort(identifierList);
                            identifierMap.put(streamParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.node.output.T8DataEntityOutputNode").getConstructor(T8Context.class, this.getClass());
            return (T8DStreamNode)constructor.newInstance(context, this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public int getCommitInterval()
    {
        Integer interval;

        interval = (Integer)getDefinitionDatum(Datum.COMMIT_INTERVAL.toString());
        return interval != null ? interval : 0;
    }

    public void setCommitInterval(int interval)
    {
        setDefinitionDatum(Datum.COMMIT_INTERVAL.toString(), interval);
    }

    public boolean insert()
    {
        Boolean insert;

        insert = (Boolean)getDefinitionDatum(Datum.INSERT.toString());
        return insert != null && insert;
    }

    public void setInsert(boolean insert)
    {
        setDefinitionDatum(Datum.INSERT.toString(), insert);
    }

    public boolean update()
    {
        Boolean update;

        update = (Boolean)getDefinitionDatum(Datum.UPDATE.toString());
        return update != null && update;
    }

    public void setUpdate(boolean update)
    {
        setDefinitionDatum(Datum.UPDATE.toString(), update);
    }

    public boolean delete()
    {
        Boolean delete;

        delete = (Boolean)getDefinitionDatum(Datum.DELETE.toString());
        return delete != null && delete;
    }

    public void setDelete(boolean delete)
    {
        setDefinitionDatum(Datum.DELETE.toString(), delete);
    }

    public boolean blind()
    {
        Boolean blind;

        blind = (Boolean)getDefinitionDatum(Datum.BLIND.toString());
        return blind != null && blind;
    }

    public void setBlind(boolean blind)
    {
        setDefinitionDatum(Datum.BLIND.toString(), blind);
    }

    public Map<String, String> getFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.FIELD_MAPPING.toString());
    }

    public void setFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.FIELD_MAPPING.toString(), mapping);
    }
}
