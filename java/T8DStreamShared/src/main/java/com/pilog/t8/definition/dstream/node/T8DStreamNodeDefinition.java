package com.pilog.t8.definition.dstream.node;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamNodeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {ENABLED,
                       TRANSACTION_TYPE,
                       BATCH_SIZE,
                       INITIALIZATION_NODE_DEFINITIONS,
                       CHILD_NODE_DEFINITIONS,
                       FINALIZATION_NODE_DEFINITIONS,
                       EXCEPTION_NODE_DEFINITIONS,
                       PROGRESS_WEIGHT};
    // -------- Definition Meta-Data -------- //

    public enum NodeTransactionType {INHERITED, ISOLATED, ISOLATED_AUTO_COMMIT};

    public T8DStreamNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ENABLED.toString(), "Enabled",  "A setting to indicate whether this node is enabled or disabled for execution.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TRANSACTION_TYPE.toString(), "Transaction Type",  "The type of transaction to use for this node and its descendants.", NodeTransactionType.INHERITED.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.BATCH_SIZE.toString(), "Batch Size",  "In case of an ISOLATED transaction type, specifies the number of rows to process before a transaction is committed and a new one started.", 0));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INITIALIZATION_NODE_DEFINITIONS.toString(), "Initialization Nodes",  "The initialization nodes that will be executed once when this node is initialized."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHILD_NODE_DEFINITIONS.toString(), "Child Nodes",  "The child nodes that will receive the output data of this node during execution."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FINALIZATION_NODE_DEFINITIONS.toString(), "Finalization Nodes",  "The finalization nodes of that will be executed once when this is finalized."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EXCEPTION_NODE_DEFINITIONS.toString(), "Exception Nodes",  "The exception nodes that will receive Exception output data of this node during execution."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.PROGRESS_WEIGHT.toString(), "Progress Weight",  "The weight of the overall progress that this node represents.", 1.0));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.TRANSACTION_TYPE.toString().equals(datumId)) return createStringOptions(NodeTransactionType.values());
        else if (Datum.CHILD_NODE_DEFINITIONS.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamTransformationNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamOutputNodeDefinition.GROUP_IDENTIFIER)));
            return options;
        }
        else if (Datum.INITIALIZATION_NODE_DEFINITIONS.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamTransformationNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamOutputNodeDefinition.GROUP_IDENTIFIER)));
            return options;
        }
        else if (Datum.FINALIZATION_NODE_DEFINITIONS.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamTransformationNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamOutputNodeDefinition.GROUP_IDENTIFIER)));
            return options;
        }
        else if (Datum.EXCEPTION_NODE_DEFINITIONS.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamTransformationNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamOutputNodeDefinition.GROUP_IDENTIFIER)));
            return options;
        }
        else return null;
    }

    public abstract T8DStreamNode getNewNodeInstance(T8Context context) throws Exception;

    public boolean isEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.ENABLED.toString());
        return enabled != null && enabled;
    }

    public void setEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.ENABLED.toString(), enabled);
    }

    public NodeTransactionType getTransactionType()
    {
        String value;

        value = (String)getDefinitionDatum(Datum.TRANSACTION_TYPE.toString());
        return value != null ? NodeTransactionType.valueOf(value) : NodeTransactionType.INHERITED;
    }

    public void setTransactionType(NodeTransactionType transactionType)
    {
        setDefinitionDatum(Datum.TRANSACTION_TYPE.toString(), transactionType.toString());
    }

    public int getBatchSize()
    {
        Integer value;

        value = (Integer)getDefinitionDatum(Datum.BATCH_SIZE);
        return value != null ? value : 0;
    }

    public void setBatchSize(int size)
    {
        setDefinitionDatum(Datum.BATCH_SIZE, size);
    }

    public Double getProgressWeight()
    {
        return (Double)getDefinitionDatum(Datum.PROGRESS_WEIGHT.toString());
    }

    public void setProgressWeight(double progressWeight)
    {
        setDefinitionDatum(Datum.PROGRESS_WEIGHT.toString(), progressWeight);
    }

    public List<T8DStreamNodeDefinition> getChildNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.CHILD_NODE_DEFINITIONS.toString());
    }

    public void setChildNodeDefinitions(List<T8DStreamNodeDefinition> definitions)
    {
        setDefinitionDatum(Datum.CHILD_NODE_DEFINITIONS.toString(), definitions);
    }

    public void addChildNodeDefinition(T8DStreamNodeDefinition definition)
    {
        List<T8DStreamNodeDefinition> definitions = getChildNodeDefinitions();
        if(definitions == null) definitions = new ArrayList<T8DStreamNodeDefinition>();

        definitions.add(definition);

        setChildNodeDefinitions(definitions);
    }

    public List<T8DStreamNodeDefinition> getInitializationNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.INITIALIZATION_NODE_DEFINITIONS.toString());
    }

    public void setInitializationNodeDefinitions(List<T8DStreamNodeDefinition> nodeDefinitions)
    {
        setDefinitionDatum(Datum.INITIALIZATION_NODE_DEFINITIONS.toString(), nodeDefinitions);
    }

    public List<T8DStreamNodeDefinition> getFinalizationNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.FINALIZATION_NODE_DEFINITIONS.toString());
    }

    public void setFinalizationNodeDefinitions(List<T8DStreamNodeDefinition> nodeDefinitions)
    {
        setDefinitionDatum(Datum.FINALIZATION_NODE_DEFINITIONS.toString(), nodeDefinitions);
    }

    public List<T8DStreamNodeDefinition> getExceptionNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.EXCEPTION_NODE_DEFINITIONS.toString());
    }

    public void setExceptionNodeDefinitions(List<T8DStreamNodeDefinition> definitions)
    {
        setDefinitionDatum(Datum.EXCEPTION_NODE_DEFINITIONS.toString(), definitions);
    }

    protected ArrayList<T8DefinitionDatumOption> createStreamParameterOptions()
    {
        return createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
    }

    public static ArrayList<T8DefinitionDatumOption> createStreamParameterOptions(T8DStreamDefinition streamDefinition)
    {
        ArrayList<T8DefinitionDatumOption> options;

        options = new ArrayList<T8DefinitionDatumOption>();
        options.add(new T8DefinitionDatumOption("No Mapping", null));
        if (streamDefinition != null)
        {
            options.addAll(createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getStreamParameterDefinitions()));
            return options;
        }
        else return options;
    }
}
