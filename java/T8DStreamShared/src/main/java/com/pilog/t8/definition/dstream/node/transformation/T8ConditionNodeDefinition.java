package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ConditionNodeDefinition extends T8DStreamTransformationNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_CONDITION";
    public static final String DISPLAY_NAME = "Condition";
    public static final String DESCRIPTION = "A node that evaluates input data using a boolean expression in order to perform conditional subsequent processing.";
    public static final String VERSION = "0";
    public enum Datum {CONDITION_EXPRESSION,
                       FAILURE_MESSAGE_EXPRESSION,
                       FAILURE_TYPE};
    // -------- Definition Meta-Data -------- //

    public enum ConditionFailureType {STREAM_FAILURE, LOG_AND_PROCESS_CHILD_NODES, LOG_AND_SKIP_CHILD_NODES, SKIP_CHILD_NODES};

    public T8ConditionNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression",  "The boolean expression that will be used to evaluate input data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.FAILURE_MESSAGE_EXPRESSION.toString(), "Failure Message Expression",  "The expression that will be evaluated in order to compile an error message if the condition is not met."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FAILURE_TYPE.toString(), "Condition Failure Type",  "Defines the method of processing when data does not comply with the condition expression (condition expression returns false).", ConditionFailureType.SKIP_CHILD_NODES.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FAILURE_TYPE.toString().equals(datumIdentifier)) return createStringOptions(ConditionFailureType.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8ConditionNode").getConstructor(T8Context.class, this.getClass());
            return (T8DStreamNode)constructor.newInstance(context, this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public String getFailureMessageExpression()
    {
        return (String)getDefinitionDatum(Datum.FAILURE_MESSAGE_EXPRESSION.toString());
    }

    public void setFailureMessageExpression(String expression)
    {
        setDefinitionDatum(Datum.FAILURE_MESSAGE_EXPRESSION.toString(), expression);
    }

    public ConditionFailureType getFailureType()
    {
        String failureType;

        failureType = (String)getDefinitionDatum(Datum.FAILURE_TYPE.toString());
        return failureType == null ? ConditionFailureType.SKIP_CHILD_NODES : ConditionFailureType.valueOf(failureType);
    }

    public void setFailureType(ConditionFailureType failureType)
    {
        setDefinitionDatum(Datum.FAILURE_TYPE.toString(), failureType.toString());
    }
}
