package com.pilog.t8.definition.dstream.input.module;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.input.T8DStreamInputDefinition;
import com.pilog.t8.definition.dstream.input.T8DStreamInputPanel;
import com.pilog.t8.definition.script.T8ModuleOperationScriptDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleInputDefinition extends T8DStreamInputDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_MODULE";
    public static final String DISPLAY_NAME = "Module Stream Input";
    public static final String DESCRIPTION = "A Module that is displayed on the Stream input panel and which allows the user to enter certain data parameters that will be used during Stream execution.";
    public static final String VERSION = "0";
    public enum Datum {MODULE_DEFINITION,
                       OUTPUT_OPERATION_IDENTIFIER,
                       OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ModuleInputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.MODULE_DEFINITION.toString(), "Module", "The Module that will be displayed and used to get certain user data inputs that will be used for stream execution."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.OUTPUT_OPERATION_IDENTIFIER.toString(), "Parameter Extraction Operation Identifier",  "The identifier of the module operation to invoke when stream parameters need to be extracted."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Module to Stream", "A mapping of Module Operation Output Parameters to the corresponding Stream Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MODULE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ModuleDefinition.TYPE_IDENTIFIER));
        else if (Datum.OUTPUT_OPERATION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8ModuleDefinition moduleDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            moduleDefinition = getModuleDefinition();
            if (moduleDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions((List)moduleDefinition.getModuleOperationScriptDefinitions());
            }
            else return options;
        }
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationIdentifier;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            operationIdentifier = getOutputOperationIdentifier();
            if (operationIdentifier != null)
            {
                T8DStreamDefinition streamDefinition;
                T8ModuleOperationScriptDefinition operationDefinition;

                operationDefinition = (T8ModuleOperationScriptDefinition)getLocalDefinition(operationIdentifier);
                streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((operationDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8DataParameterDefinition> operationParameterDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    operationParameterDefinitions = operationDefinition.getOutputParameterDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((streamParameterDefinitions != null) && (operationParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition operationParameter : operationParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                            {
                                identifierList.add(streamParameterDefinition.getIdentifier());
                            }

                            Collections.sort(identifierList);
                            identifierMap.put(operationParameter.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public T8DStreamInputPanel getNewStreamInputPanelInstance(T8ComponentController controller)
    {
        try
        {
            T8DStreamInputPanel inputPanel;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.input.T8ModuleInputPanel").getConstructor(T8ComponentController.class, this.getClass());
            inputPanel = (T8DStreamInputPanel)constructor.newInstance(controller, this);
            return inputPanel;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public T8ModuleDefinition getModuleDefinition()
    {
        return (T8ModuleDefinition)getDefinitionDatum(Datum.MODULE_DEFINITION.toString());
    }

    public void setModuleDefinition(T8ModuleDefinition definition)
    {
        setDefinitionDatum(Datum.MODULE_DEFINITION.toString(), definition);
    }

    public String getOutputOperationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OUTPUT_OPERATION_IDENTIFIER.toString());
    }

    public void setOutputOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OUTPUT_OPERATION_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
