

package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Willem de Bruyn
 */
public class T8RecordAttributeExpressionMappingDefinition extends T8RecordAttributeMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_RECORD_ATTRIBUTE_EXPRESSION_MAPPING";
    public static final String DISPLAY_NAME = "Record Attribute Expression Mapping";
    public static final String DESCRIPTION = "A mapping that will evaluate an expression to determine the value of a specific attribute that will be added to a Data Record.";
    public static final String VERSION = "0";
    public enum Datum {ATTRIBUTE_CONDITION_EXPRESSION,
                       ATTRIBUTE_IDENTIFIER_EXPRESSION,
                       ATTRIBUTE_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //
    
    private transient ExpressionEvaluator conditionExpressionEvaluator;
    private transient ExpressionEvaluator identifierExpressionEvaluator;
    private transient ExpressionEvaluator valueExpressionEvaluator;
    
    public T8RecordAttributeExpressionMappingDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, T8RecordAttributeExpressionMappingDefinition.Datum.ATTRIBUTE_CONDITION_EXPRESSION.toString(), "Attribute Condition Expression", "The boolean expression that will be evaluated to determine if this attribute is applicable or not."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, T8RecordAttributeExpressionMappingDefinition.Datum.ATTRIBUTE_IDENTIFIER_EXPRESSION.toString(), "Attribute Identifier Expression", "The expression that will be evaluated using stream parameters in order to determine the attribute identifier."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, T8RecordAttributeExpressionMappingDefinition.Datum.ATTRIBUTE_VALUE_EXPRESSION.toString(), "Attribute Value Expression", "The expression that will be evaluated using stream parameters in order to determine the attribute value."));
        return datumTypes;
    }
    
    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public boolean isAttributeApplicable(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception 
    {
        // If no expression evaluator has been initialized, try to do it now.
        if (conditionExpressionEvaluator == null)
        {
            String attributeExpression;
            
            attributeExpression = getAttributeConditionExpression();
            if ((attributeExpression != null) && (attributeExpression.trim().length() > 0))
            {
                conditionExpressionEvaluator = new ExpressionEvaluator();
                conditionExpressionEvaluator.compileExpression(attributeExpression);
            }
        }
        
        // Evaluator the expression.
        if (conditionExpressionEvaluator != null) return conditionExpressionEvaluator.evaluateBooleanExpression(dataParameters, null);
        else return true;
    }

    @Override
    public String getAttributeIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception 
    {
        // If no expression evaluator has been initialized, try to do it now.
        if (identifierExpressionEvaluator == null)
        {
            String attributeExpression;
            
            attributeExpression = getAttributeIdentifierExpression();
            if ((attributeExpression != null) && (attributeExpression.trim().length() > 0))
            {
                identifierExpressionEvaluator = new ExpressionEvaluator();
                identifierExpressionEvaluator.compileExpression(attributeExpression);
            }
        }
        
        // Evaluator the expression.
        if (identifierExpressionEvaluator != null) return (String)identifierExpressionEvaluator.evaluateExpression(dataParameters, null);
        else return null;
    }

    @Override
    public Object getAttributeValue(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception 
    {
        // If no expression evaluator has been initialized, try to do it now.
        if (valueExpressionEvaluator == null)
        {
            String attributeExpression;
            
            attributeExpression = getAttributeValueExpression();
            if ((attributeExpression != null) && (attributeExpression.trim().length() > 0))
            {
                valueExpressionEvaluator = new ExpressionEvaluator();
                valueExpressionEvaluator.compileExpression(attributeExpression);
            }
        }
        
        // Evaluator the expression.
        if (valueExpressionEvaluator != null) return valueExpressionEvaluator.evaluateExpression(dataParameters, null);
        else return null;
    }
    
    public String getAttributeConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.ATTRIBUTE_CONDITION_EXPRESSION.toString());
    }
    
    public void setAttributeConditionExpression(String identifier)
    {
        setDefinitionDatum(Datum.ATTRIBUTE_CONDITION_EXPRESSION.toString(), identifier);
    }
    
    public String getAttributeIdentifierExpression()
    {
        return (String)getDefinitionDatum(Datum.ATTRIBUTE_IDENTIFIER_EXPRESSION.toString());
    }
    
    public void setAttributeIdentifierExpression(String identifier)
    {
        setDefinitionDatum(Datum.ATTRIBUTE_IDENTIFIER_EXPRESSION.toString(), identifier);
    }
    
    public String getAttributeValueExpression()
    {
        return (String)getDefinitionDatum(Datum.ATTRIBUTE_VALUE_EXPRESSION.toString());
    }
    
    public void setAttributeValueExpression(String identifier)
    {
        setDefinitionDatum(Datum.ATTRIBUTE_VALUE_EXPRESSION.toString(), identifier);
    }  
}
