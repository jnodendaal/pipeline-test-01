package com.pilog.t8.definition.dstream.node.output.datarecord.datatypes;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8RequirementMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DRInstanceGroupReferenceMappingDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DR_INSTANCE_GROUP_REFERENCE_MAPPING";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_DR_INSTANCE_GROUP_REFERENCE_MAPPING";
    public static final String DISPLAY_NAME = "Data Requirement Instance Group Reference Mapping";
    public static final String DESCRIPTION = "A mapping of input data to a Data Requirement Instance Group Reference requirement type.";
    public enum Datum {DR_INSTANCE_GROUP_ID,
                       DR_INSTANCE_ID,
                       RECORD_ID,
                       DR_INSTANCE_GROUP_PARAMETER_IDENTIFIER,
                       DR_INSTANCE_ID_PARAMETER_IDENTIFIER,
                       DR_INSTANCE_TERM_PARAMETER_IDENTIFIER,
                       DR_INSTANCE_CODE_PARAMETER_IDENTIFIER,
                       RECORD_ID_PARAMETER_IDENTIFIER,
                       CONDITION_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8DRInstanceGroupReferenceMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DR_INSTANCE_GROUP_ID.toString(), "DR Instance Group", "The Data Requirement Instance Group that will be used for the reference."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DR_INSTANCE_ID.toString(), "DR Instance", "The Data Requirement Instance that will be used for the reference."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_ID.toString(), "Data Record", "The Data Record that will be used for the reference."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_INSTANCE_GROUP_PARAMETER_IDENTIFIER.toString(), "DR Instance Group Parameter", "The stream parameter to use for resolution of the DR Instance group."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString(), "DR Instance ID Source Parameter", "The stream parameter to use for resolution of the DR Instance by ID."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_INSTANCE_TERM_PARAMETER_IDENTIFIER.toString(), "DR Instance Term Source Parameter", "The stream parameter to use for resolution of the DR Instance by Term."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_INSTANCE_CODE_PARAMETER_IDENTIFIER.toString(), "DR Instance Code Source Parameter", "The stream parameter to use for resolution of the DR Instance by Code."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_ID_PARAMETER_IDENTIFIER.toString(), "Data Record ID Source Parameter", "The stream parameter to use for resolution of the referenced Record (if any)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated to determine whether or not this mapping must be processed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DR_INSTANCE_GROUP_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.DR_INSTANCE_TERM_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.DR_INSTANCE_CODE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.RECORD_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public List<String> getMatchParameterIdentifiers()
    {
        List<String> parameterIdentifiers;
        String parameterIdentifier;
        
        parameterIdentifiers = new ArrayList<String>();
        
        parameterIdentifier = getDRInstanceIDParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        parameterIdentifier = getDRInstanceTermParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        parameterIdentifier = getDRInstanceCodeParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        parameterIdentifier = getRecordIDParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        return parameterIdentifiers;
    }
    
    public String getDRInstanceGroupID()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_GROUP_ID.toString());
    }
    
    public void setDRInstanceGroupID(String odtID)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_GROUP_ID.toString(), odtID);
    }
    
    public String getDRInstanceID()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_ID.toString());
    }
    
    public void setDRInstanceID(String drInstanceID)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_ID.toString(), drInstanceID);
    }
    
    public String getRecordID()
    {
        return (String)getDefinitionDatum(Datum.RECORD_ID.toString());
    }
    
    public void setRecordID(String recordID)
    {
        setDefinitionDatum(Datum.RECORD_ID.toString(), recordID);
    }
    
    public String getDRInstanceGroupParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_GROUP_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDRInstanceGroupParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_GROUP_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getDRInstanceIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDRInstanceIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getDRInstanceTermParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_TERM_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDRInstanceTermParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_TERM_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getDRInstanceCodeParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_CODE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDRInstanceCodeParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_CODE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getRecordIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RECORD_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }
    
    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }
}
