package com.pilog.t8.dstream;

import java.io.File;

/**
 * @author Bouwer du Preez
 */
public interface T8DStreamFileHandler
{
    public boolean isFileAvailable() throws Exception;
    public File getCurrentFile();
    public boolean isOpen();
    public void openFile() throws Exception;
    public void closeFile() throws Exception;
    public void validateFile() throws Exception;
}
