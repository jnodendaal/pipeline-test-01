package com.pilog.t8.definition.dstream.node.input;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.input.T8DataFileInputDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileInputNodeDefinition extends T8DStreamInputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_INPUT_DATA_FILE";
    public static final String DISPLAY_NAME = "Data Input File";
    public static final String DESCRIPTION = "A node that reads data from a defined input data file.";
    public static final String VERSION = "0";
    public enum Datum {INPUT_IDENTIFIER, TABLE_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DataFileInputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INPUT_IDENTIFIER.toString(), "Input Identifier",  "The Data Input from which this node will read data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TABLE_IDENTIFIER.toString(), "Table Identifier",  "The name of the table within the data file from which this node will read data."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
               // Only get Data Input File Definitions.
                return createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getInputDefinitionsByType(T8DataFileInputDefinition.TYPE_IDENTIFIER));
            }
            else return options;
        }
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.input.T8DataFileInputNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public String getInputIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INPUT_IDENTIFIER.toString());
    }

    public void setInputIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INPUT_IDENTIFIER.toString(), identifier);
    }

    public String getTableIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TABLE_IDENTIFIER.toString());
    }

    public void setTableIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TABLE_IDENTIFIER.toString(), identifier);
    }
}
