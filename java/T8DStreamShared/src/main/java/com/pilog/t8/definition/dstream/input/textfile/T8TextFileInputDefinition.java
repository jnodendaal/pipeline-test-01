package com.pilog.t8.definition.dstream.input.textfile;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.dstream.input.T8DStreamInputFileDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextFileInputDefinition extends T8DStreamInputFileDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_FILE_TEXT";
    public static final String DISPLAY_NAME = "Text Input File";
    public static final String DESCRIPTION = "A text file that will be used as input to the data stream.";
    public enum Datum {DEFAULT_ENCODING,
                       SEPARATOR_STRING,
                       TRIM_STRINGS,
                       CONVERT_EMPTY_STRINGS_TO_NULL,
                       FIELD_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public enum InputTextEncoding
    {
        UTF_8("UTF-8"),
        UTF_16("UTF-16");

        private String code; // Used internally when input/output streams are created.
        InputTextEncoding(String code)
        {
            this.code = code;
        }

        public String getCode()
        {
            return code;
        }
    };

    public T8TextFileInputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DEFAULT_ENCODING.toString(), "Default Encoding",  "The default encoding to use when reading data from the text file.", InputTextEncoding.UTF_8.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEPARATOR_STRING.toString(), "Separator String",  "The String that is used to separate columns."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TRIM_STRINGS.toString(), "Trim Values",  "A boolean value indicating whether or not input String values should be trimmed (whitespace removed from start and end).", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CONVERT_EMPTY_STRINGS_TO_NULL.toString(), "Convert Empty Strings to Null",  "A boolean value indicating whether or not input String values with no content should be set to null.", true));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Fields", "The fields/columns contained by this text file."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DEFAULT_ENCODING.toString().equals(datumIdentifier)) return createStringOptions(InputTextEncoding.values());
        else if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TextFileInputFieldDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.file.T8TextInputFileHandler").getConstructor(T8Context.class, this.getClass(), T8FileManager.class, T8DStreamFileContext.class);
        return (T8DStreamFileHandler)constructor.newInstance(context, this, fileManager, fileContext);
    }

    public InputTextEncoding getDefaultEncoding()
    {
        String encodingString;

        encodingString = (String)getDefinitionDatum(Datum.DEFAULT_ENCODING.toString());
        return encodingString != null ? InputTextEncoding.valueOf(encodingString) : InputTextEncoding.UTF_8;
    }

    public void setDefaultEncoding(InputTextEncoding encoding)
    {
        setDefinitionDatum(Datum.DEFAULT_ENCODING.toString(), encoding.toString());
    }

    public String getSeparatorString()
    {
        return (String)getDefinitionDatum(Datum.SEPARATOR_STRING.toString());
    }

    public void setSeparatorString(String encoding)
    {
        setDefinitionDatum(Datum.SEPARATOR_STRING.toString(), encoding);
    }

    public List<T8TextFileInputFieldDefinition> getFieldDefinitions()
    {
        return (List<T8TextFileInputFieldDefinition>)getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8TextFileInputFieldDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), definitions);
    }

    public void addFieldDefinition(T8TextFileInputFieldDefinition fieldDefinition)
    {
        addSubDefinition(Datum.FIELD_DEFINITIONS.toString(), fieldDefinition);
    }

    public boolean trimStrings()
    {
        Boolean trimStrings;

        trimStrings = (Boolean)getDefinitionDatum(Datum.TRIM_STRINGS.toString());
        return trimStrings != null && trimStrings;
    }

    public void setTrimStrings(boolean trimStrings)
    {
        setDefinitionDatum(Datum.TRIM_STRINGS.toString(), trimStrings);
    }

    public boolean convertEmptyStringsToNull()
    {
        Boolean trimStrings;

        trimStrings = (Boolean)getDefinitionDatum(Datum.CONVERT_EMPTY_STRINGS_TO_NULL.toString());
        return trimStrings != null && trimStrings;
    }

    public void setConvertEmptyStringsToNull(boolean convertEmptyStringsToNull)
    {
        setDefinitionDatum(Datum.CONVERT_EMPTY_STRINGS_TO_NULL.toString(), convertEmptyStringsToNull);
    }
}
