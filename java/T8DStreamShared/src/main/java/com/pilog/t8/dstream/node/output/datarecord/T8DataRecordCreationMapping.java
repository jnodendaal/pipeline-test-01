package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordCreationMapping extends T8PerformanceStatisticsProvider
{
    public boolean isApplicable(DataRequirementInstance dataRequirementInstance, Map<String, Object> dataParameters);
    public DataRecordConstructionResult constructDataRecord(T8DataTransaction tx, String rootRecordID, DataRecord parentRecord, DataRequirementInstance dataRequirementInstance, Map<String, Object> dataParameters) throws Exception;
}
