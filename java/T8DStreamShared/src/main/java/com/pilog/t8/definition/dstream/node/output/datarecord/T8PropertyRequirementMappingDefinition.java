package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8PropertyRequirementMappingDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_PROPERTY_REQUIREMENT_MAPPING";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_PROPERTY_REQUIREMENT_MAPPING";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Property Requirement Mapping";
    public static final String DESCRIPTION = "A mapping of an input parameter to a specific property in a Data Requirement structure.";
    public enum Datum {PROPERTY_IDENTIFIER,
                       FFT_SOURCE_PARAMETER_IDENTIFIER,
                       VALUE_MAPPING_DEFINITION,
                       CONDITION_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8PropertyRequirementMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PROPERTY_IDENTIFIER.toString(), "Property", "The unique identifier of the Data Requirement Property to which this mapping applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString(), "FFT Source Parameter", "The stream parameter mapped to the FFT of this property."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.VALUE_MAPPING_DEFINITION.toString(), "Value Mapping",  "The data mapping to the value contained by the property."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated to determine whether or not this mapping must be processed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.VALUE_MAPPING_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ValueRequirementMappingDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public List<String> getMatchParameterIdentifiers()
    {
        List<String> parameterIdentifiers;
        T8ValueRequirementMappingDefinition valueMappingDefinition;
        
        parameterIdentifiers = new ArrayList<String>();
        valueMappingDefinition = getValueMappingDefinition();
        if (valueMappingDefinition != null)
        {
            parameterIdentifiers.addAll(valueMappingDefinition.getMatchParameterIdentifiers());
        }
        
        return parameterIdentifiers;
    }
    
    public String getPropertyIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_IDENTIFIER.toString());
    }
    
    public void setPropertyIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROPERTY_IDENTIFIER.toString(), identifier);
    }
    
    public String getFFTSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setFFTSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public T8ValueRequirementMappingDefinition getValueMappingDefinition()
    {
        return (T8ValueRequirementMappingDefinition)getDefinitionDatum(Datum.VALUE_MAPPING_DEFINITION.toString());
    }
    
    public void setValueMappingDefinition(T8ValueRequirementMappingDefinition definition)
    {
        setDefinitionDatum(Datum.VALUE_MAPPING_DEFINITION.toString(), definition);
    }
    
    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }
    
    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }
}
