package com.pilog.t8.definition.functionality;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.functionality.T8DefaultFunctionalityAccessHandle;
import com.pilog.t8.functionality.T8WorkflowFunctionalityAccessHandle;
import java.lang.reflect.Constructor;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamFunctionalityExecutionHandle extends T8DefaultFunctionalityAccessHandle implements T8FunctionalityAccessHandle
{
    private final String streamIdentifier;
    private final Map<String, Object> inputParameters;

    public T8DStreamFunctionalityExecutionHandle(String functionalityId, String functionalityIid, String streamIdentifier, Map<String, Object> inputParameters, String displayName, String description, Icon icon)
    {
        super(functionalityId, functionalityIid, displayName, description, icon);
        this.streamIdentifier = streamIdentifier;
        this.inputParameters = inputParameters;
    }

    @Override
    public T8FunctionalityView createViewComponent(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.functionality.view.T8WorkFlowClientFunctionalityView").getConstructor(T8ComponentController.class, T8WorkflowFunctionalityAccessHandle.class, String.class, String.class);
        return (T8FunctionalityView)constructor.newInstance(controller, this, streamIdentifier, inputParameters);
    }

    @Override
    public boolean requiresExplicitRelease()
    {
        return false;
    }
}
