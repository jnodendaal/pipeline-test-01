package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SQLScriptNodeDefinition extends T8DStreamTransformationNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_SQL_SCRIPT";
    public static final String DISPLAY_NAME = "SQL Script";
    public static final String DESCRIPTION = "A node that executes a SQL script on a data connection.";
    public static final String VERSION = "0";
    public enum Datum {CONNECTION_IDENTIFIER, SQL_SCRIPT};
    // -------- Definition Meta-Data -------- //

    public T8SQLScriptNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONNECTION_IDENTIFIER.toString(), "Output Identifier",  "The Excel Output to which this node will write data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.SQL_SCRIPT.toString(), "SQL Script",  "The SQL script the will be executed on the specified connection."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.SQLScriptNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public String getConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString());
    }

    public void setConnectionIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString(), identifier);
    }

    public String getSQLScript()
    {
        return (String)getDefinitionDatum(Datum.SQL_SCRIPT.toString());
    }

    public void setSQLScript(String script)
    {
        setDefinitionDatum(Datum.SQL_SCRIPT.toString(), script);
    }
}
