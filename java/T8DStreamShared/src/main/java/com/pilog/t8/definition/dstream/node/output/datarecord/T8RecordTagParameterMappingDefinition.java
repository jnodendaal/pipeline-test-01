package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8RecordTagParameterMappingDefinition extends T8RecordTagMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_RECORD_TAG_PARAMETER_MAPPING";
    public static final String DISPLAY_NAME = "Record Tag Parameter Mapping";
    public static final String DESCRIPTION = "A mapping of an input parameter to a specific tag that will be added to a Data Record.";
    public static final String VERSION = "0";
    public enum Datum {TAG_IDENTIFIER,
                       SOURCE_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8RecordTagParameterMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TAG_IDENTIFIER.toString(), "Tag Identifier", "The unique identifier of the Data Requirement Class to which this mapping applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), "Source Parameter", "The stream parameter mapped to this tag."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public boolean isTagApplicable(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception
    {
        // This tag type is always applicable.
        return true;
    }
    
    @Override
    public Object getTagValue(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters)
    {
        return dataParameters.get(getSourceParameterIdentifier());
    }
    
    public String getSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    @Override
    public String getTagIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception
    {
        return getTagIdentifier();
    }

    public String getTagIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TAG_IDENTIFIER.toString());
    }
    
    public void setTagIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TAG_IDENTIFIER.toString(), identifier);
    }
}
