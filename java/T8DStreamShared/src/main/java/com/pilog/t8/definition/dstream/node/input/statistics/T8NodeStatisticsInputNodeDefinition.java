package com.pilog.t8.definition.dstream.node.input.statistics;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NodeStatisticsInputNodeDefinition extends T8DStreamInputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_STATISTICS_NODE";
    public static final String DISPLAY_NAME = "Node Statistics";
    public static final String DESCRIPTION = "A node that reads execution statistics of a specific node in the stream and makes the statistics available as stream parameters.";
    public enum Datum {NODE_STATISTICS_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8NodeStatisticsInputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.NODE_STATISTICS_MAPPING_DEFINITIONS.toString(), "Statistics Mappings",  "The mappings from various sources of statistic to the stream parameters where these statistics are to be placed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.NODE_STATISTICS_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8NodeStatisticsMappingDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)  throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.input.T8NodeStatisticsInputNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public List<T8NodeStatisticsMappingDefinition> getNodeStatisticMappingDefinitions()
    {
        return (List<T8NodeStatisticsMappingDefinition>)getDefinitionDatum(Datum.NODE_STATISTICS_MAPPING_DEFINITIONS.toString());
    }

    public void setNodeStatisticsMappingDefinitions(List<T8NodeStatisticsMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.NODE_STATISTICS_MAPPING_DEFINITIONS.toString(), definitions);
    }
}
