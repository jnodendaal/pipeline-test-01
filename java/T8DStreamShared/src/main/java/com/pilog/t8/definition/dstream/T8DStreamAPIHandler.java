package com.pilog.t8.definition.dstream;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaClientOperationDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.definition.process.operation.T8ServerOperationProcessDefinition;
import com.pilog.t8.definition.process.operation.T8ServerOperationProcessResourceDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String PROCESS_DSTREAM_EXECUTION_SERVER = "@PROCESS_DSTREAM_EXECUTION";
    public static final String OPERATION_DSTREAM_EXECUTION_SERVER = "@OS_DSTREAM_EXECUTION";
    public static final String OPERATION_DSTREAM_EXECUTION_CLIENT = "@OC_DSTREAM_EXECUTION";
    public static final String OPERATION_DSTREAM_CREATE_DR_MAPPING_DEFINITIONS = "@OS_DSTREAM_CREATE_DR_MAPPING_DEFINITIONS";

    public static final String PARAMETER_DSTREAM_DEFINITION = "$P_DSTREAM_DEFINITION";
    public static final String PARAMETER_DSTREAM_DEFINITION_IDENTIFIER = "$P_DSTREAM_DEFINITION_IDENTIFIER";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_DSTREAM_FILE_CONTEXT = "$P_DSTREAM_CONTEXT";
    public static final String PARAMETER_FILE_CONTEXT_IDENTIFIER = "$P_FILE_CONTEXT_IDENTIFIER";
    public static final String PARAMETER_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";
    public static final String PARAMETER_DR_ID = "$P_DR_ID";
    public static final String PARAMETER_DR_INSTANCE_ID = "$P_DR_INSTANCE_ID";
    public static final String PARAMETER_DR_MAPPING_DEFINITION = "$P_DR_MAPPING_DEFINITION";
    public static final String PARAMETER_DR_MAPPING_DEFINITION_LIST = "$P_DR_MAPPING_DEFINITION_LIST";
    public static final String PARAMETER_DR_INSTANCE_MAPPING_DEFINITION = "$P_DR_INSTANCE_MAPPING_DEFINITION";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<T8Definition>();
        if (typeIdentifiers.contains(T8JavaClientOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaClientOperationDefinition definition;

            definition = new T8JavaClientOperationDefinition(OPERATION_DSTREAM_EXECUTION_CLIENT);
            definition.setClassName("com.pilog.t8.dstream.T8DStreamClientOperations$DStreamExecutionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER, "Stream Identifier", "The identifier of the stream to execute.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project to which the stream to execute belongs.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETERS, "Stream Input Parameters", "The input parameters that will be used to initialize the stream.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);
        }
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;

            definition = new T8JavaServerOperationDefinition(OPERATION_DSTREAM_EXECUTION_SERVER);
            definition.setMetaDisplayName("Execute Data Stream");
            definition.setClassName("com.pilog.t8.dstream.T8DStreamOperations$DStreamExecutionOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DSTREAM_DEFINITION, "DStream Definition", "The DStream definition that will be executed.", T8DataType.DEFINITION));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER, "DStream Definition Identifier", "The Definition identifier of the stream that should be executed.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project to which the stream to execute belongs.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DSTREAM_FILE_CONTEXT, "DStream File Context", "The File Context that will be used to read/write files.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETERS, "DStream Input Parameters", "The input parameters that will be used when executing the stream.", T8DataType.MAP));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_DSTREAM_CREATE_DR_MAPPING_DEFINITIONS);
            definition.setMetaDisplayName("Create Data Requirement Mapping Definitions");
            definition.setClassName("com.pilog.t8.dstream.T8DStreamOperations$CreateDataRequirementMappingDefinitionsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_ID, "Data Requriement ID", "The ID of the parent Data Requirement from where mapping will start.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_INSTANCE_ID, "Data Requriement Instance ID", "The ID of the parent Data Requirement Instance from where mapping will start.", T8DataType.GUID));
            definitions.add(definition);
        }
        if (typeIdentifiers.contains(T8ServerOperationProcessDefinition.TYPE_IDENTIFIER))
        {
            T8ServerOperationProcessResourceDefinition processDefinition;

            processDefinition = new T8ServerOperationProcessResourceDefinition(PROCESS_DSTREAM_EXECUTION_SERVER);
            processDefinition.setMetaDisplayName("Execute Data Stream");
            processDefinition.setFinalizeOnCompletion(Boolean.TRUE);
            processDefinition.setProcessDisplayName("Executing Data Stream");
            processDefinition.setOperationIdentifier(OPERATION_DSTREAM_EXECUTION_SERVER);
            processDefinition.addSubDefinition(T8ProcessDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString(), new T8DataParameterDefinition(PARAMETER_DSTREAM_DEFINITION, "DStream Definition", "The DStream definition that will be executed.", T8DataType.DEFINITION));
            processDefinition.addSubDefinition(T8ProcessDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString(), new T8DataParameterDefinition(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER, "DStream Definition Identifier", "The Definition identifier of the stream that should be executed.", T8DataType.DEFINITION_IDENTIFIER));
            processDefinition.addSubDefinition(T8ProcessDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString(), new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project to which the stream to execute belongs.", T8DataType.DEFINITION_IDENTIFIER));
            processDefinition.addSubDefinition(T8ProcessDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString(), new T8DataParameterDefinition(PARAMETER_DSTREAM_FILE_CONTEXT, "DStream File Context", "The File Context that will be used to read/write files.", T8DataType.CUSTOM_OBJECT));
            processDefinition.addSubDefinition(T8ProcessDefinition.Datum.INPUT_PARAMETER_DEFINITIONS.toString(), new T8DataParameterDefinition(PARAMETER_INPUT_PARAMETERS, "DStream Input Parameters", "The input parameters that will be used when executing the stream.", T8DataType.MAP));

            //Process Input Parameter mapping
            {
                Map<String, String> inputMapping;

                inputMapping = new HashMap<String, String>();
                inputMapping.put(PARAMETER_DSTREAM_DEFINITION, OPERATION_DSTREAM_EXECUTION_SERVER + PARAMETER_DSTREAM_DEFINITION);
                inputMapping.put(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER, OPERATION_DSTREAM_EXECUTION_SERVER + PARAMETER_DSTREAM_DEFINITION_IDENTIFIER);
                inputMapping.put(PARAMETER_PROJECT_ID, OPERATION_DSTREAM_EXECUTION_SERVER + PARAMETER_PROJECT_ID);
                inputMapping.put(PARAMETER_DSTREAM_FILE_CONTEXT, OPERATION_DSTREAM_EXECUTION_SERVER + PARAMETER_DSTREAM_FILE_CONTEXT);
                inputMapping.put(PARAMETER_INPUT_PARAMETERS, OPERATION_DSTREAM_EXECUTION_SERVER + PARAMETER_INPUT_PARAMETERS);
                processDefinition.setInputParameterMapping(inputMapping);
            }
            definitions.add(processDefinition);
        }

        return definitions.isEmpty() ? null : definitions;
    }
}
