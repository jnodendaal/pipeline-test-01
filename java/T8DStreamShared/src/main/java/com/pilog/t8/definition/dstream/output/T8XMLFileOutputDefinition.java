package com.pilog.t8.definition.dstream.output;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8XMLFileOutputDefinition extends T8DStreamOutputFileDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_XML";
    public static final String DISPLAY_NAME = "XML Output File";
    public static final String DESCRIPTION = "An XML file that will be produced as output of the data stream.";
    public static final String VERSION = "0";
    public enum Datum {INDENT, INDENT_AMOUNT};
    // -------- Definition Meta-Data -------- //

    public T8XMLFileOutputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INDENT.toString(), "Indent",  "A boolean value indicating whether or not XML elements should be indented/pretty-printed when written to the output destination."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.INDENT_AMOUNT.toString(), "Indent Amount",  "The amount of indentation to use when pretty-printing XML output."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.file.T8XMLOutputFileHandler").getConstructor(T8Context.class, this.getClass(), T8FileManager.class, T8DStreamFileContext.class);
        return (T8DStreamFileHandler)constructor.newInstance(context, this, fileManager, fileContext);
    }

    public boolean isIndent()
    {
        Boolean indent;

        indent = (Boolean)getDefinitionDatum(Datum.INDENT.toString());
        return indent != null && indent;
    }

    public void setIndent(boolean indent)
    {
        setDefinitionDatum(Datum.INDENT.toString(), indent);
    }

    public int getIndentAmount()
    {
        return (Integer)getDefinitionDatum(Datum.INDENT_AMOUNT.toString());
    }

    public void setIndentAmount(int indentAmount)
    {
        setDefinitionDatum(Datum.INDENT_AMOUNT.toString(), indentAmount);
    }
}
