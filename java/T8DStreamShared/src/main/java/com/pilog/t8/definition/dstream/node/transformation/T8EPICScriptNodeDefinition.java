package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICScriptNodeDefinition extends T8DStreamTransformationNodeDefinition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_EPIC_SCRIPT";
    public static final String DISPLAY_NAME = "EPIC Script";
    public static final String DESCRIPTION = "A node that executes an EPIC script on a data connection.";
    public static final String VERSION = "0";
    public enum Datum {EPIC_SCRIPT};
    // -------- Definition Meta-Data -------- //

    public T8EPICScriptNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.EPIC_SCRIPT.toString(), "EPIC Script",  "The SQL script the will be executed on the specified connection."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.EPIC_SCRIPT.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ScriptDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8EPICScriptNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8EPICScriptNodeScript").getConstructor(T8Context.class, T8ServerContextScriptDefinition.class);
        return (T8ServerContextScript)constructor.newInstance(context, this);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8DStreamDefinition streamDefinition;

        streamDefinition = (T8DStreamDefinition)this.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
        if (streamDefinition != null)
        {
            return streamDefinition.getStreamParameterDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8DStreamDefinition streamDefinition;

        streamDefinition = (T8DStreamDefinition)this.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
        if (streamDefinition != null)
        {
            return streamDefinition.getStreamParameterDefinitions();
        }
        else return null;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        return null;
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        return null;
    }

    @Override
    public String getScript()
    {
        return getEPICScript();
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }

    public String getEPICScript()
    {
        return (String)getDefinitionDatum(Datum.EPIC_SCRIPT.toString());
    }

    public void setEPICScript(String script)
    {
        setDefinitionDatum(Datum.EPIC_SCRIPT.toString(), script);
    }
}
