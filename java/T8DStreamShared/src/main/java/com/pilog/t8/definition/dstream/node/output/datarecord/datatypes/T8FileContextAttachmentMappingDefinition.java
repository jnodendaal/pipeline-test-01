package com.pilog.t8.definition.dstream.node.output.datarecord.datatypes;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8FileContextAttachmentMappingDefinition extends T8AttachmentMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_ATTACHMENT_MAPPING";
    public static final String DISPLAY_NAME = "File Context Attachment Mapping";
    public static final String DESCRIPTION = "A mapping used to identify and import a specific file from a file context as a Data Record attachment.";
    public enum Datum {SOURCE_FILE_CONTEXT_IDENTIFIER,
                       SOURCE_FILEPATH_PARAMETER_IDENTIFIER,
                       ATTACHMENT_ID_PARAMETER_IDENTIFIER,
                       ATTACHMENT_FILENAME_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8FileContextAttachmentMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SOURCE_FILE_CONTEXT_IDENTIFIER.toString(), "Source File Context", "The unique identifier of the file context from which attachment files will be sourced."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SOURCE_FILEPATH_PARAMETER_IDENTIFIER.toString(), "Source Filepath Parameter", "The stream parameter from which the source filepath will be sourced."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ATTACHMENT_ID_PARAMETER_IDENTIFIER.toString(), "Attachment ID Parameter", "The stream parameter from which the attachment ID will be sourced."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ATTACHMENT_FILENAME_PARAMETER_IDENTIFIER.toString(), "Attachment Filename Parameter", "The stream parameter from which the attachment filename will be sourced."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SOURCE_FILE_CONTEXT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FileContextDefinition.GROUP_IDENTIFIER));
        else if (Datum.SOURCE_FILEPATH_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.ATTACHMENT_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.ATTACHMENT_FILENAME_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<String> getMatchParameterIdentifiers()
    {
        List<String> parameterIdentifiers;
        String parameterIdentifier;
        
        parameterIdentifiers = new ArrayList<String>();
        
        parameterIdentifier = getSourceFilepathParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        parameterIdentifier = getAttachmentIDParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        parameterIdentifier = getAttachmentFilenameParameterIdentifier();
        if (parameterIdentifier != null) parameterIdentifiers.add(parameterIdentifier);
        
        return parameterIdentifiers;
    }

    public String getSourceFileContextIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_FILE_CONTEXT_IDENTIFIER.toString());
    }
    
    public void setSourceFileContextIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SOURCE_FILE_CONTEXT_IDENTIFIER.toString(), identifier);
    }
    
    public String getSourceFilepathParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_FILEPATH_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setSourceFilepathParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SOURCE_FILEPATH_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getAttachmentIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ATTACHMENT_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setAttachmentIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ATTACHMENT_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }   
    
    public String getAttachmentFilenameParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ATTACHMENT_FILENAME_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setAttachmentFilenameParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ATTACHMENT_FILENAME_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
