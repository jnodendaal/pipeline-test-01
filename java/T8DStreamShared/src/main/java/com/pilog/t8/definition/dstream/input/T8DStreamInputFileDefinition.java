package com.pilog.t8.definition.dstream.input;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamInputFileDefinition extends T8DStreamInputDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {FILE_NAME};
    // -------- Definition Meta-Data -------- //

    public T8DStreamInputFileDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FILE_NAME.toString(), "File Name",  "The name of the input file."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public T8DStreamInputPanel getNewStreamInputPanelInstance(T8ComponentController controller)
    {
        try
        {
            T8DStreamInputPanel inputPanel;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.input.T8FileUploadPanel").getConstructor(T8ComponentController.class, T8DStreamInputFileDefinition.class);
            inputPanel = (T8DStreamInputPanel)constructor.newInstance(controller, this);
            return inputPanel;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public abstract T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception;

    public String getFileName()
    {
        return (String)getDefinitionDatum(Datum.FILE_NAME.toString());
    }

    public void setFileName(String text)
    {
        setDefinitionDatum(Datum.FILE_NAME.toString(), text);
    }
}
