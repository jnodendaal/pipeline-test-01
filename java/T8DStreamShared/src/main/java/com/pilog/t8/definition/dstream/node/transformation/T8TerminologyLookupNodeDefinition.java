package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyLookupNodeDefinition extends T8DStreamTransformationNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_TERMINOLOGY_LOOKUP";
    public static final String DISPLAY_NAME = "Terminology Lookup";
    public static final String DESCRIPTION = "A node that uses an input Concept ID to find terminology from the database in a specified language and add it to the stream.";
    public static final String VERSION = "0";
    public enum Datum {TERMINOLOGY_CACHE_SIZE,
                       DEFAULT_LANGUAGE_ID,
                       CACHE_DR_ID_LIST,
                       TERMINOLOGY_DE_IDENTIFIER,
                       DR_DOCUMENT_DE_IDENTIFIER,
                       TERMINOLOGY_LOOKUP_TYPE_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    private static final int DEFAULT_TERMINOLOGY_CACHE_SIZE = 1000;

    public T8TerminologyLookupNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DEFAULT_LANGUAGE_ID.toString(), "Default Language ID", "The ISO Language ID that identifies the fallback language to use when terminology in a requested language can not be found."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.TERMINOLOGY_CACHE_SIZE.toString(), "Terminology Cache Size", "The maximum number of concepts that will be held in the terminology cache at one time.", DEFAULT_TERMINOLOGY_CACHE_SIZE));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.CACHE_DR_ID_LIST.toString(), "Cache Data Requirement ID List", "The list of Data Requirements for which terminology will be cached during the initialization of the stream."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TERMINOLOGY_DE_IDENTIFIER.toString(), "Terminology Entity", "The data entity from which ontological terminology data is retrieved for concepts."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_DOCUMENT_DE_IDENTIFIER.toString(), "Data Requirement Document Entity", "The data entity from which data requirement documents are retrieved."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TERMINOLOGY_LOOKUP_TYPE_DEFINITIONS.toString(), "Terminology Lookup Types",  "The types of terminology lookups to perform."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TERMINOLOGY_LOOKUP_TYPE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TerminologyLookupTypeDefinition.GROUP_IDENTIFIER));
        else if (Datum.TERMINOLOGY_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DR_DOCUMENT_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8TerminologyLookupNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public List<T8TerminologyLookupTypeDefinition> getTerminologyLookupTypeDefinitions()
    {
        return (List<T8TerminologyLookupTypeDefinition>)getDefinitionDatum(Datum.TERMINOLOGY_LOOKUP_TYPE_DEFINITIONS.toString());
    }

    public void setTerminologyLookupTypeDefinitions(List<T8TerminologyLookupTypeDefinition> definitions)
    {
        setDefinitionDatum(Datum.TERMINOLOGY_LOOKUP_TYPE_DEFINITIONS.toString(), definitions);
    }

    public int getTerminologyCacheSize()
    {
        Integer size;

        size = (Integer)getDefinitionDatum(Datum.TERMINOLOGY_CACHE_SIZE.toString());
        return size != null ? size : DEFAULT_TERMINOLOGY_CACHE_SIZE;
    }

    public void setTerminologyCacheSize(int cacheSize)
    {
        setDefinitionDatum(Datum.TERMINOLOGY_CACHE_SIZE.toString(), cacheSize);
    }

    public String getTerminologyDEID()
    {
        return (String)getDefinitionDatum(Datum.TERMINOLOGY_DE_IDENTIFIER.toString());
    }

    public void setTerminologyDEID(String identifier)
    {
        setDefinitionDatum(Datum.TERMINOLOGY_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRequirementDocumentDEID()
    {
        return (String)getDefinitionDatum(Datum.DR_DOCUMENT_DE_IDENTIFIER.toString());
    }

    public void setDataRequirementDocumentDEID(String identifier)
    {
        setDefinitionDatum(Datum.DR_DOCUMENT_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDefaultLanguageID()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_LANGUAGE_ID.toString());
    }

    public void setDefaultLanguageID(String languageID)
    {
        setDefinitionDatum(Datum.DEFAULT_LANGUAGE_ID.toString(), languageID);
    }

    public List<String> getCacheDRIDList()
    {
        return (List<String>)getDefinitionDatum(Datum.CACHE_DR_ID_LIST.toString());
    }

    public void setCacheDRIDList(List<String> drIDList)
    {
        setDefinitionDatum(Datum.CACHE_DR_ID_LIST.toString(), drIDList);
    }
}
