package com.pilog.t8.definition.dstream.node.input;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.input.textfile.T8TextFileInputDefinition;
import com.pilog.t8.definition.dstream.input.textfile.T8TextFileInputFieldDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8TextFileInputNodeDefinition extends T8DStreamInputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_INPUT_TEXT_FILE";
    public static final String DISPLAY_NAME = "Text Input File";
    public static final String DESCRIPTION = "A node that reads Text data from a defined input file.";
    public enum Datum {INPUT_IDENTIFIER,
                       FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER,
                       OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8TextFileInputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INPUT_IDENTIFIER.toString(), "Input Identifier",  "The Text Input from which this node will read data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString(), "File Name Output Parameter",  "The stream parameter to which the name of the input file will be output (if any)."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Text Field to Stream", "A mapping of Text Fields to the corresponding Stream Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                // Only get Text File Input definitions
                return createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getInputDefinitionsByType(T8TextFileInputDefinition.TYPE_IDENTIFIER));
            }
            else return options;
        }
        else if (Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createStreamParameterOptions();
        }
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String inputIdentifier;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            inputIdentifier = getInputIdentifier();
            if (inputIdentifier != null)
            {
                T8TextFileInputDefinition inputDefinition;
                T8DStreamDefinition streamDefinition;

                inputDefinition = (T8TextFileInputDefinition)getLocalDefinition(inputIdentifier);
                streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((inputDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8TextFileInputFieldDefinition> textFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    textFieldDefinitions = inputDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((streamParameterDefinitions != null) && (textFieldDefinitions != null))
                    {
                        for (T8TextFileInputFieldDefinition fieldDefinition : textFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                            {
                                identifierList.add(streamParameterDefinition.getIdentifier());
                            }

                            Collections.sort(identifierList);
                            identifierMap.put(fieldDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.input.T8TextFileInputNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public String getInputIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INPUT_IDENTIFIER.toString());
    }

    public void setInputIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INPUT_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public String getFileNameOutputParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setFileNameOutputParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
