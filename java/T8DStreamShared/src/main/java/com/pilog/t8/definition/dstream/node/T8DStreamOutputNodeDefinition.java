package com.pilog.t8.definition.dstream.node;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamOutputNodeDefinition extends T8DStreamNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_OUTPUT_NODE";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //
    
    public T8DStreamOutputNodeDefinition(String identifier)
    {
        super(identifier);
    }
}
