package com.pilog.t8.definition.dstream.input;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamInputDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_INPUT";
    public static final String IDENTIFIER_PREFIX = "DSTREAM_INPUT_";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8DStreamInputDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract T8DStreamInputPanel getNewStreamInputPanelInstance(T8ComponentController controller);

    protected ArrayList<T8DefinitionDatumOption> createStreamParameterOptions(boolean prependNamespace)
    {
        ArrayList<T8DefinitionDatumOption> options;
        T8DStreamDefinition streamDefinition;

        options = new ArrayList<T8DefinitionDatumOption>();
        streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
        if (streamDefinition != null)
        {
            List<T8DataParameterDefinition> parameterDefinitions;

            parameterDefinitions = streamDefinition.getStreamParameterDefinitions();
            for (T8DataParameterDefinition parameterDefinition : parameterDefinitions)
            {
                options.add(new T8DefinitionDatumOption(prependNamespace ? parameterDefinition.getPublicIdentifier() : parameterDefinition.getIdentifier(), parameterDefinition.getIdentifier()));
            }

            return options;
        }
        else return options;
    }
}
