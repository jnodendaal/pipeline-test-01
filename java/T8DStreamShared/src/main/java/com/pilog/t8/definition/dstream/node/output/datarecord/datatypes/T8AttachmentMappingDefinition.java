package com.pilog.t8.definition.dstream.node.output.datarecord.datatypes;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8ValueRequirementMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AttachmentMappingDefinition extends T8ValueRequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String DISPLAY_NAME = "Attachment Mapping";
    public static final String DESCRIPTION = "A mapping used to identify and import a specific file as a Data Record attachment.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8AttachmentMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.ATTACHMENT;
    }
}
