package com.pilog.t8.definition.dstream.node;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamTransformationNodeDefinition extends T8DStreamNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_TRANSFORMATION_NODE";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //
    
    public T8DStreamTransformationNodeDefinition(String identifier)
    {
        super(identifier);
    }
}
