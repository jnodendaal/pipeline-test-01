package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Bouwer du Preez
 */
public class RecordValueConstructionResult
{
    private RecordValue recordValue;
    private List<DataRecord> subRecords;
    private List<T8DataRecordConformanceError> conformanceErrors;
    private Map<String, T8AttachmentDetails> attachmentDetails;

    public RecordValueConstructionResult()
    {
    }

    public RecordValueConstructionResult(RecordValue recordValue, List<DataRecord> subRecords, Map<String, T8AttachmentDetails> attachmentDetails, List<T8DataRecordConformanceError> conformanceErrors)
    {
        this.recordValue = recordValue;
        this.conformanceErrors = conformanceErrors;
        this.subRecords = subRecords;
        this.attachmentDetails = attachmentDetails;
    }

    public RecordValue getRecordValue()
    {
        return recordValue;
    }

    public void setRecordValue(RecordValue recordValue)
    {
        this.recordValue = recordValue;
    }

    public List<T8DataRecordConformanceError> getConformanceErrors()
    {
        return conformanceErrors;
    }

    public void setConformanceErrors(List<T8DataRecordConformanceError> conformanceErrors)
    {
        this.conformanceErrors = conformanceErrors;
    }

    public List<DataRecord> getSubRecords()
    {
        return subRecords;
    }

    public void setSubRecords(List<DataRecord> subRecords)
    {
        this.subRecords = subRecords;
    }

    public Map<String, T8AttachmentDetails> getAttachmentDetails()
    {
        return attachmentDetails;
    }

    public void setAttachmentDetails(Map<String, T8AttachmentDetails> attachmentDetails)
    {
        this.attachmentDetails = attachmentDetails;
    }
}
