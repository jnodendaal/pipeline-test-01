package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementInstanceDataSourceDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordCreationNodeDefinition extends T8DStreamOutputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_OUTPUT_DATA_RECORD";
    public static final String DISPLAY_NAME = "Data Record Creation";
    public static final String DESCRIPTION = "A node that creates a Data Record from the input data supplied to it.";
    public static final String VERSION = "0";
    public enum Datum
    {
        DATA_RECORD_OUTPUT_PARAMETER_IDENTIFIER,
        DATA_RECORD_ID_OUTPUT_PARAMETER_IDENTIFIER,
        CONFORMANCE_ERROR_OUTPUT_DEFINITION,
        INSERT_DESCRIPTION_INSTANCE_LIST,
        DATA_REQUIREMENT_INSTANCE_DOCUMENT_ENTITY_IDENTIFIER,
        DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER,
        DATA_REQUIREMENT_INSTANCE_MAPPING_DEFINITION,
        DATA_REQUIREMENT_MAPPING_DEFINITIONS,
        TAG_MAPPING_DEFINITIONS,
        ATTIBUTE_MAPPING_DEFINITIONS,
        STATE_MAPPING_DEFINITIONS,
        RECORD_PERSISTENCE,
        RECORD_EXISTENCE_CHECK,
        DESCRIPTION_GENERATION,
        ONTOLOGY_GENERATION,
        DUPLICATE_DETECTION_HASH_GENERATION,
        STATISTICS_PRINTOUT_INTERVAL,
        CONFORMANCE_ERROR_OUTPUT_NODE_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRecordCreationNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_REQUIREMENT_INSTANCE_DOCUMENT_ENTITY_IDENTIFIER.toString(), "Data Requirement Instance Entity", "The Data Entity to use when accessing persisted Data Requirement documents."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER.toString(), "Data Record Entity", "The Data Entity to use when persisting Data Record documents."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_OUTPUT_PARAMETER_IDENTIFIER.toString(), "Data Record Output Parameter", "The Stream parameter to which the newly created data record will be output."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_ID_OUTPUT_PARAMETER_IDENTIFIER.toString(), "Data Record ID Output Parameter", "The Stream parameter to which the newly created data record ID will be output."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CONFORMANCE_ERROR_OUTPUT_DEFINITION.toString(), "Conformance Error Output Setup",  "The setup that determines how conformance errors encountered during record generation will be output to the stream."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.DATA_REQUIREMENT_INSTANCE_MAPPING_DEFINITION.toString(), "DR Instance Mapping",  "The data mapping to the Data Requirement from which records will be created."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATA_REQUIREMENT_MAPPING_DEFINITIONS.toString(), "DR Mappings",  "The data mapping for Data Requirements from which records will be created."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TAG_MAPPING_DEFINITIONS.toString(), "Tag Mappings",  "The data mappings to tags that will be added to the generated Data Record."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ATTIBUTE_MAPPING_DEFINITIONS.toString(), "Attribute Mappings",  "The data mappings for the record attributes that will be added to the generated Data Record."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STATE_MAPPING_DEFINITIONS.toString(), "State Mappings",  "The data mappings of states to which generated records will be asigned."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.INSERT_DESCRIPTION_INSTANCE_LIST.toString(), "Insert Descriptions", "The list of description instances (in addition to auto-generated defaults) that will be generated when a new record is inserted."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RECORD_PERSISTENCE.toString(), "Record Persistence",  "If this boolean flag is checked, the records created by this node will be persisted to the database.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RECORD_EXISTENCE_CHECK.toString(), "Record Existence Check",  "If this boolean flag is checked, the records created will be compare to those in the database in order to perform either an insert or update, depending on the existence of the record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DESCRIPTION_GENERATION.toString(), "Description Generation",  "If this boolean flag is checked, descriptions will be generated for each updated record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ONTOLOGY_GENERATION.toString(), "Ontology Generation",  "If this boolean flag is checked, ontology will be generated for each updated record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DUPLICATE_DETECTION_HASH_GENERATION.toString(), "Duplicate Detection Hash Generation",  "If this boolean flag is checked, duplicate detection data hashes will be generated for each updated record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.STATISTICS_PRINTOUT_INTERVAL.toString(), "Statistics Printout Interval",  "The interval (number of input rows) at which performance statistics will be printed to the standard output (0 = no interval printouts).", 0));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CONFORMANCE_ERROR_OUTPUT_NODE_DEFINITIONS.toString(), "Conformance Error Output Nodes",  "The child nodes that will receive the conformance error output data of this node during execution."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_REQUIREMENT_INSTANCE_DOCUMENT_ENTITY_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_REQUIREMENT_INSTANCE_MAPPING_DEFINITION.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRequirementInstanceMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_REQUIREMENT_MAPPING_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordCreationMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.CONFORMANCE_ERROR_OUTPUT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataConformanceErrorOutputDefinition.TYPE_IDENTIFIER));
        else if (Datum.TAG_MAPPING_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordTagMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.ATTIBUTE_MAPPING_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordAttributeMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.STATE_MAPPING_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordStateMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_RECORD_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            options.add(new T8DefinitionDatumOption("No Mapping", null));
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                options.addAll(createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getStreamParameterDefinitions()));
                return options;
            }
            else return options;
        }
        else if (Datum.DATA_RECORD_ID_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            options.add(new T8DefinitionDatumOption("No Mapping", null));
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                options.addAll(createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getStreamParameterDefinitions()));
                return options;
            }
            else return options;
        }
        else if (Datum.CONFORMANCE_ERROR_OUTPUT_NODE_DEFINITIONS.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamTransformationNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamOutputNodeDefinition.GROUP_IDENTIFIER)));
            return options;
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.dstream.T8DataRecordCreationNodeActionHandler").getConstructor(T8Context.class, this.getClass());
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public List<T8DStreamNodeDefinition> getConformanceErrorOutputNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.CONFORMANCE_ERROR_OUTPUT_NODE_DEFINITIONS.toString());
    }

    public void setConformanceErrorOutputNodeDefinitions(List<T8DStreamNodeDefinition> definitions)
    {
        setDefinitionDatum(Datum.CONFORMANCE_ERROR_OUTPUT_NODE_DEFINITIONS.toString(), definitions);
    }

    public List<String> getInsertDescriptionInstanceList()
    {
        return (List<String>)getDefinitionDatum(Datum.INSERT_DESCRIPTION_INSTANCE_LIST.toString());
    }

    public void setInsertDescriptionInstanceList(List<String> instanceList)
    {
        setDefinitionDatum(Datum.INSERT_DESCRIPTION_INSTANCE_LIST.toString(), instanceList);
    }

    public String getDataRequirementInstanceEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_DOCUMENT_ENTITY_IDENTIFIER.toString());
    }

    public void setDataRequirementEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_DOCUMENT_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordDocumentEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER.toString());
    }

    public void setDataRecordDocumentEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordOutputParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setDataRecordOutputParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_OUTPUT_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordIDOutputParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_ID_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setDataRecordIDOutputParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_ID_OUTPUT_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public T8DataConformanceErrorOutputDefinition getConformanceErrorOutputDefinition()
    {
        return (T8DataConformanceErrorOutputDefinition)getDefinitionDatum(Datum.CONFORMANCE_ERROR_OUTPUT_DEFINITION.toString());
    }

    public void setConformanceErrorOutputDefinition(T8DataConformanceErrorOutputDefinition definition)
    {
        setDefinitionDatum(Datum.CONFORMANCE_ERROR_OUTPUT_DEFINITION.toString(), definition);
    }

    public T8DataRequirementInstanceMappingDefinition getDataRequirementInstanceMappingDefinition()
    {
        return (T8DataRequirementInstanceMappingDefinition)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_MAPPING_DEFINITION.toString());
    }

    public void setDataRequirementInstanceMappingDefinition(T8DataRequirementInstanceMappingDefinition definition)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_MAPPING_DEFINITION.toString(), definition);
    }

    public String getDataRecordEntityKeyFieldIdentifier()
    {
        return getDataRecordDocumentEntityIdentifier() + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER;
    }

    public String getDataRecordEntityDocumentFieldIdentifier()
    {
        return getDataRecordDocumentEntityIdentifier() + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER;
    }

    public String getDRInstanceEntityKeyFieldIdentifier()
    {
        return getDataRequirementInstanceEntityIdentifier() + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER;
    }

    public String getDRInstanceEntityDocumentFieldIdentifier()
    {
        return getDataRequirementInstanceEntityIdentifier() + T8DataRequirementInstanceDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER;
    }

    public List<T8DataRequirementMappingDefinition> getDataRequirementMappingDefinitions()
    {
        return (List<T8DataRequirementMappingDefinition>)getDefinitionDatum(Datum.DATA_REQUIREMENT_MAPPING_DEFINITIONS.toString());
    }

    public T8DataRequirementMappingDefinition getDataRequirementMappingDefinition(String drID)
    {
        for (T8DataRequirementMappingDefinition mappingDefinition : getDataRequirementMappingDefinitions())
        {
            if (Objects.equals(mappingDefinition.getDataRequirementIdentifier(), drID))
            {
                return mappingDefinition;
            }
        }

        // Nothing found.
        return null;
    }

    public T8DataRequirementMappingDefinition removeDataRequirementMappingDefinition(String drID)
    {
        for (T8DataRequirementMappingDefinition mappingDefinition : getDataRequirementMappingDefinitions())
        {
            if (Objects.equals(mappingDefinition.getDataRequirementIdentifier(), drID))
            {
                removeSubDefinition(mappingDefinition);
                return mappingDefinition;
            }
        }

        // Nothing found.
        return null;
    }

    public void addDataRequirementMappingDefinition(T8DataRequirementMappingDefinition definition)
    {
        addSubDefinition(Datum.DATA_REQUIREMENT_MAPPING_DEFINITIONS.toString(), definition);
    }

    public void setDataRequirementMappingDefinitions(List<T8DataRequirementMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public List<T8RecordTagMappingDefinition> getTagMappingDefinitions()
    {
        return (List<T8RecordTagMappingDefinition>)getDefinitionDatum(Datum.TAG_MAPPING_DEFINITIONS.toString());
    }

    public void setTagMappingDefinitions(List<T8RecordTagMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.TAG_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public List<T8RecordAttributeMappingDefinition> getAttributeMappingDefinitions()
    {
        return (List<T8RecordAttributeMappingDefinition>)getDefinitionDatum(Datum.ATTIBUTE_MAPPING_DEFINITIONS.toString());
    }

    public void setAttributeMappingDefinitions(List<T8RecordAttributeMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.ATTIBUTE_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public List<T8RecordStateMappingDefinition> getStateMappingDefinitions()
    {
        return (List<T8RecordStateMappingDefinition>)getDefinitionDatum(Datum.STATE_MAPPING_DEFINITIONS.toString());
    }

    public void setStateMappingDefinitions(List<T8RecordStateMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.STATE_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public boolean isRecordPersistence()
    {
        Boolean recordPersistence;

        recordPersistence = (Boolean)getDefinitionDatum(Datum.RECORD_PERSISTENCE.toString());
        return recordPersistence != null && recordPersistence;
    }

    public void setRecordPersistence(boolean recordPersistence)
    {
        setDefinitionDatum(Datum.RECORD_PERSISTENCE.toString(), recordPersistence);
    }

    public boolean isRecordExistenceCheck()
    {
        Boolean existenceCheck;

        existenceCheck = (Boolean)getDefinitionDatum(Datum.RECORD_EXISTENCE_CHECK.toString());
        return existenceCheck != null && existenceCheck;
    }

    public void setRecordExistenceCheck(boolean existenceCheck)
    {
        setDefinitionDatum(Datum.RECORD_EXISTENCE_CHECK.toString(), existenceCheck);
    }

    public boolean isDescriptionGeneration()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.DESCRIPTION_GENERATION.toString());
        return value != null && value;
    }

    public void setDescriptionGeneration(boolean descriptionGeneration)
    {
        setDefinitionDatum(Datum.DESCRIPTION_GENERATION.toString(), descriptionGeneration);
    }

    public boolean isOntologyGeneration()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ONTOLOGY_GENERATION.toString());
        return value != null && value;
    }

    public void setOntologyGeneration(boolean ontologyGeneration)
    {
        setDefinitionDatum(Datum.ONTOLOGY_GENERATION.toString(), ontologyGeneration);
    }

    public boolean isDuplicateDetectionHashGeneration()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.DUPLICATE_DETECTION_HASH_GENERATION.toString());
        return value != null && value;
    }

    public void setDuplicateDetectionHashGeneration(boolean hashGeneration)
    {
        setDefinitionDatum(Datum.DUPLICATE_DETECTION_HASH_GENERATION.toString(), hashGeneration);
    }

    public int getStatisticsPrintoutInterval()
    {
        Integer value;

        value = (Integer)getDefinitionDatum(Datum.STATISTICS_PRINTOUT_INTERVAL.toString());
        return value != null ? value : 0;
    }

    public void setStatisticsPrintoutInterval(int interval)
    {
        setDefinitionDatum(Datum.STATISTICS_PRINTOUT_INTERVAL.toString(), interval);
    }
}
