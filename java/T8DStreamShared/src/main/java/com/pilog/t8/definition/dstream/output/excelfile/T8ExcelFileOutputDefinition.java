package com.pilog.t8.definition.dstream.output.excelfile;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputFileDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileOutputDefinition extends T8DStreamOutputFileDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_EXCEL";
    public static final String IDENTIFIER_PREFIX = "OUTPUT_EXCEL_";
    public static final String DISPLAY_NAME = "Excel Output File";
    public static final String DESCRIPTION = "An Excel file that will be produced as output of the data stream.";
    public static final String VERSION = "0";
    public enum Datum {SHEET_DEFINITIONS};
    public enum ExcelFileFormat {FILE_TYPE_EXCEL_97};
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileOutputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SHEET_DEFINITIONS.toString(), "Sheets",  "The sheets contained by the Excel file."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SHEET_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ExcelFileOutputSheetDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.file.T8ExcelOutputFileHandler").getConstructor(T8Context.class, this.getClass(), T8FileManager.class, T8DStreamFileContext.class);
        return (T8DStreamFileHandler)constructor.newInstance(context, this, fileManager, fileContext);
    }

    public List<T8ExcelFileOutputSheetDefinition> getSheetDefinitions()
    {
        return (List<T8ExcelFileOutputSheetDefinition>)getDefinitionDatum(Datum.SHEET_DEFINITIONS.toString());
    }

    public T8ExcelFileOutputSheetDefinition getSheetDefinition(String identifier)
    {
        for (T8ExcelFileOutputSheetDefinition sheetDefinition : getSheetDefinitions())
        {
            if (sheetDefinition.getIdentifier().equals(identifier)) return sheetDefinition;
        }

        return null;
    }

    public void setSheetDefinitions(List<T8ExcelFileOutputSheetDefinition> definitions)
    {
        setDefinitionDatum(Datum.SHEET_DEFINITIONS.toString(), definitions);
    }
}
