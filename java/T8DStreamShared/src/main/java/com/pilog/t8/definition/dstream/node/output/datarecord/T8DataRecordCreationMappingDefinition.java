package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationContext;
import com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationMapping;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataRecordCreationMappingDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_DATA_RECORD_CREATION_MAPPING";
    public enum Datum {DATA_RECORD_MATCH_TYPE,
                       KEY_PROPERTY_IDENTIFIERS,
                       KEY_PARAMETER_IDENTIFIERS,
                       MATCH_CODE_TYPE_ID,
                       CODE_MAPPING_DEFINITIONS,
                       TAG_MAPPING_DEFINITIONS,
                       ATTRIBUTE_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public enum DataRecordMatchType 
    {
        VALUE_STRING, 
        KEY_PROPERTIES, 
        CHARACTERISTIC_PROPERTIES, 
        CODE,
        KEY_PARAMETERS
    };

    public T8DataRecordCreationMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_RECORD_MATCH_TYPE.toString(), "Record Match Type", "The method to use when matching newly created records with existing ones in order to determine existence.", DataRecordMatchType.VALUE_STRING.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.KEY_PROPERTY_IDENTIFIERS.toString(), "Key Properties", "The list of Key Property identifiers for this Data Requirement (will be used for record matching e.g. for existence checking)."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.DEFINITION_IDENTIFIER), Datum.KEY_PARAMETER_IDENTIFIERS.toString(), "Key Parameters", "The list of Key Parameter identifiers for this Data Requirement (will be used to match records in memory during construction)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.MATCH_CODE_TYPE_ID.toString(), "Match Code Type", "The Code Type that will be used to match data records (if match type is set to CODE)."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CODE_MAPPING_DEFINITIONS.toString(), "Code Mappings",  "The data mappings for ontology codes that will be added to the generated Data Record based on this Data Requirement Mapping."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TAG_MAPPING_DEFINITIONS.toString(), "Tag Mappings",  "The data mappings to tags that will be added to the generated Data Record based on this Data Requirement Mapping."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ATTRIBUTE_MAPPING_DEFINITIONS.toString(), "Attribute Mappings",  "The data mappings to record att4ributes that will be added to the generated Data Record based on this Data Requirement Mapping."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_RECORD_MATCH_TYPE.toString().equals(datumIdentifier)) return createStringOptions(DataRecordMatchType.values());
        else if (Datum.CODE_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordCodeMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.TAG_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordTagMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.ATTRIBUTE_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordAttributeMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.KEY_PARAMETER_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public abstract T8DataRecordCreationMapping createNewMappingInstance(T8DataRecordCreationContext context) throws Exception;

    public List<String> getKeyPropertyIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.KEY_PROPERTY_IDENTIFIERS.toString());
    }

    public void setKeyPropertyIdentifiers(List<String> identifierList)
    {
        setDefinitionDatum(Datum.KEY_PROPERTY_IDENTIFIERS.toString(), identifierList);
    }
    
    public List<String> getKeyParameterIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.KEY_PARAMETER_IDENTIFIERS.toString());
    }

    public void setKeyParameterIdentifiers(List<String> identifierList)
    {
        setDefinitionDatum(Datum.KEY_PARAMETER_IDENTIFIERS.toString(), identifierList);
    }
    
    public String getMatchCodeTypeID()
    {
        return (String)getDefinitionDatum(Datum.MATCH_CODE_TYPE_ID.toString());
    }
    
    public void setMatchCodeTypeID(String codeTypeID)
    {
        setDefinitionDatum(Datum.MATCH_CODE_TYPE_ID.toString(), codeTypeID);
    }

    public DataRecordMatchType getDataRecordMatchType()
    {
        String value;

        value = (String)getDefinitionDatum(Datum.DATA_RECORD_MATCH_TYPE.toString());
        return value != null ? DataRecordMatchType.valueOf(value) : DataRecordMatchType.VALUE_STRING;
    }

    public void setDataRecordMatchType(DataRecordMatchType matchType)
    {
        setDefinitionDatum(Datum.DATA_RECORD_MATCH_TYPE.toString(), matchType.toString());
    }

    public List<T8RecordCodeMappingDefinition> getCodeMappingDefinitions()
    {
        return (List<T8RecordCodeMappingDefinition>)getDefinitionDatum(Datum.CODE_MAPPING_DEFINITIONS.toString());
    }

    public void setCodeMappingDefinitions(List<T8RecordCodeMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.CODE_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public List<T8RecordTagMappingDefinition> getTagMappingDefinitions()
    {
        return (List<T8RecordTagMappingDefinition>)getDefinitionDatum(Datum.TAG_MAPPING_DEFINITIONS.toString());
    }

    public void setTagMappingDefinitions(List<T8RecordTagMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.TAG_MAPPING_DEFINITIONS.toString(), definitions);
    }    

    public List<T8RecordAttributeMappingDefinition> getAttributeMappingDefinitions()
    {
        return (List<T8RecordAttributeMappingDefinition>)getDefinitionDatum(Datum.ATTRIBUTE_MAPPING_DEFINITIONS.toString());
    }

    public void setAttributeMappingDefinitions(List<T8RecordAttributeMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.ATTRIBUTE_MAPPING_DEFINITIONS.toString(), definitions);
    }    
}
