package com.pilog.t8.definition.functionality;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamFunctionalityDefinition extends T8FunctionalityDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FUNCTIONALITY_DATA_STREAM";
    public static final String DISPLAY_NAME = "Data Stream Functionality";
    public static final String DESCRIPTION = "A T8 functionality that starts execution of a specific data stream.";
    public static final String IDENTIFIER_PREFIX = "FNC_DSTREAM_";
    public enum Datum {DATA_STREAM_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DStreamFunctionalityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_STREAM_IDENTIFIER.toString(), "Data Stream Identifier", "The identifier of the data stream linked to this functionality."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_STREAM_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DStreamDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8FunctionalityInstance getNewFunctionalityInstance(T8Context context, T8FunctionalityState state) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getDataStreamIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_STREAM_IDENTIFIER.toString());
    }

    public void setDataStreamIdentifier(String streamIdentifier)
    {
        setDefinitionDatum(Datum.DATA_STREAM_IDENTIFIER.toString(), streamIdentifier);
    }
}
