package com.pilog.t8.dstream;

import com.pilog.t8.operation.progressreport.T8CategorizedProgressReport;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamExecutionStatus implements T8CategorizedProgressReport
{
    private double overallProgress;
    private String overallProgressText;
    private double phaseProgress;
    private String phaseProgressText;
    private String streamTitle;
    private int phaseIterationCount;
    private int phaseIterationsCompleted;

    public T8DStreamExecutionStatus()
    {
    }

    public T8DStreamExecutionStatus(int overallProgress, String overallProgressText, int phaseProgress, String phaseProgressText)
    {
        this.overallProgress = overallProgress;
        this.overallProgressText = overallProgressText;
        this.phaseProgress = phaseProgress;
        this.phaseProgressText = phaseProgressText;
    }

    public double getOverallProgress()
    {
        return overallProgress;
    }

    public void setOverallProgress(double overallProgress)
    {
        this.overallProgress = overallProgress;
    }

    public String getOverallProgressText()
    {
        return overallProgressText;
    }

    public void setOverallProgressText(String overallProgressText)
    {
        this.overallProgressText = overallProgressText;
    }

    public double getPhaseProgress()
    {
        return phaseProgress;
    }

    public void setPhaseProgress(double phaseProgress)
    {
        this.phaseProgress = phaseProgress;
    }

    public String getPhaseProgressText()
    {
        return phaseProgressText;
    }

    public void setPhaseProgressText(String phaseProgressText)
    {
        this.phaseProgressText = phaseProgressText;
    }

    @Override
    public Double getProgress()
    {
        return (double)getOverallProgress();
    }

    @Override
    public String getProgressMessage()
    {
        return getOverallProgressText();
    }

    @Override
    public void setProgress(Double progress)
    {
        setOverallProgress(progress.intValue());
    }

    @Override
    public void setProgressMessage(String progressMessage)
    {
        setOverallProgressText(progressMessage);
    }

    @Override
    public String getTitle()
    {
        return streamTitle;
    }

    @Override
    public void setTitle(String title)
    {
        this.streamTitle = title;
    }

    public int getPhaseIterationsCompleted()
    {
        return phaseIterationsCompleted;
    }

    public void setPhaseIterationsCompleted(int phaseIterationsCompleted)
    {
        this.phaseIterationsCompleted = phaseIterationsCompleted;
    }

    public int getPhaseIterationCount()
    {
        return phaseIterationCount;
    }

    public void setPhaseIterationCount(int phaseIterationCount)
    {
        this.phaseIterationCount = phaseIterationCount;
    }

    @Override
    public String getCategoryMessage()
    {
        return getPhaseProgressText();
    }

    @Override
    public Double getCategoryProgress()
    {
        return (double)getPhaseProgress();
    }

    @Override
    public String getCategoryTitle()
    {
        return null;
    }

    @Override
    public void setCategoryMessage(String categoryMessage)
    {
        setPhaseProgressText(phaseProgressText);
    }

    @Override
    public void setCategoryProgress(Double categoryProgress)
    {
        setPhaseProgress(categoryProgress.intValue());
    }

    @Override
    public void setCategoryTitle(String categoryTitle)
    {
    }
}
