package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8RecordTagExpressionMappingDefinition extends T8RecordTagMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_RECORD_TAG_EXPRESSION_MAPPING";
    public static final String DISPLAY_NAME = "Record Tag Expression Mapping";
    public static final String DESCRIPTION = "A mapping that will evaluate an expression to determine the value of a specific tag that will be added to a Data Record.";
    public static final String VERSION = "0";
    public enum Datum {TAG_CONDITION_EXPRESSION,
                       TAG_IDENTIFIER_EXPRESSION,
                       TAG_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    private transient ExpressionEvaluator conditionExpressionEvaluator;
    private transient ExpressionEvaluator identifierExpressionEvaluator;
    private transient ExpressionEvaluator valueExpressionEvaluator;

    public T8RecordTagExpressionMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.TAG_CONDITION_EXPRESSION.toString(), "Tag Condition Expression", "The boolean expression that will be evaluated to determine if this tag is applicable or not."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.TAG_IDENTIFIER_EXPRESSION.toString(), "Tag Identifier Expression", "The expression that will be evaluated using stream parameters in order to determine the tag identifier."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.TAG_VALUE_EXPRESSION.toString(), "Tag Value Expression", "The expression that will be evaluated using stream parameters in order to determine the tag value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public boolean isTagApplicable(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception
    {
        // If no expression evaluator has been initialized, try to do it now.
        if (conditionExpressionEvaluator == null)
        {
            String tagExpression;

            tagExpression = getTagConditionExpression();
            if ((tagExpression != null) && (tagExpression.trim().length() > 0))
            {
                conditionExpressionEvaluator = new ExpressionEvaluator();
                conditionExpressionEvaluator.compileExpression(tagExpression);
            }
        }

        // Evaluator the expression.
        if (conditionExpressionEvaluator != null) return conditionExpressionEvaluator.evaluateBooleanExpression(dataParameters, null);
        else return true;
    }

    @Override
    public String getTagIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception
    {
        // If no expression evaluator has been initialized, try to do it now.
        if (identifierExpressionEvaluator == null)
        {
            String tagExpression;

            tagExpression = getTagIdentifierExpression();
            if ((tagExpression != null) && (tagExpression.trim().length() > 0))
            {
                identifierExpressionEvaluator = new ExpressionEvaluator();
                identifierExpressionEvaluator.compileExpression(tagExpression);
            }
        }

        // Evaluator the expression.
        if (identifierExpressionEvaluator != null) return (String)identifierExpressionEvaluator.evaluateExpression(dataParameters, null);
        else return null;
    }

    @Override
    public Object getTagValue(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception
    {
        // If no expression evaluator has been initialized, try to do it now.
        if (valueExpressionEvaluator == null)
        {
            String tagExpression;

            tagExpression = getTagValueExpression();
            if ((tagExpression != null) && (tagExpression.trim().length() > 0))
            {
                valueExpressionEvaluator = new ExpressionEvaluator();
                valueExpressionEvaluator.compileExpression(tagExpression);
            }
        }

        // Evaluator the expression.
        if (valueExpressionEvaluator != null) return valueExpressionEvaluator.evaluateExpression(dataParameters, null);
        else return null;
    }

    public String getTagConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.TAG_CONDITION_EXPRESSION.toString());
    }

    public void setTagConditionExpression(String identifier)
    {
        setDefinitionDatum(Datum.TAG_CONDITION_EXPRESSION.toString(), identifier);
    }

    public String getTagIdentifierExpression()
    {
        return (String)getDefinitionDatum(Datum.TAG_IDENTIFIER_EXPRESSION.toString());
    }

    public void setTagIdentifierExpression(String identifier)
    {
        setDefinitionDatum(Datum.TAG_IDENTIFIER_EXPRESSION.toString(), identifier);
    }

    public String getTagValueExpression()
    {
        return (String)getDefinitionDatum(Datum.TAG_VALUE_EXPRESSION.toString());
    }

    public void setTagValueExpression(String identifier)
    {
        setDefinitionDatum(Datum.TAG_VALUE_EXPRESSION.toString(), identifier);
    }
}
