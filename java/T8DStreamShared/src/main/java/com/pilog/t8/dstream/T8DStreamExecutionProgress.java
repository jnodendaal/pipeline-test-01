package com.pilog.t8.dstream;

/**
 * @author Hennie Brink
 */
public class T8DStreamExecutionProgress
{
    private final double overallExecutionProgress;
    private final String overallExecutionProgressText;
    private final double phaseExecutionProgress;
    private final String phaseExecutionProgressText;
    private int phasIterationCount;
    private int phaseIterationsCompleted;

    public T8DStreamExecutionProgress(double overallExecutionProgress,
                                      String overallExecutionProgressText,
                                      double phaseExecutionProgress,
                                      String phaseExecutionProgressText)
    {
        this.overallExecutionProgress = overallExecutionProgress;
        this.overallExecutionProgressText = overallExecutionProgressText;
        this.phaseExecutionProgress = phaseExecutionProgress;
        this.phaseExecutionProgressText = phaseExecutionProgressText;
    }

    public double getOverallExecutionProgress()
    {
        return overallExecutionProgress;
    }

    public String getOverallExecutionProgressText()
    {
        return overallExecutionProgressText;
    }

    public double getPhaseExecutionProgress()
    {
        return phaseExecutionProgress;
    }

    public String getPhaseExecutionProgressText()
    {
        return phaseExecutionProgressText;
    }

    public int getPhaseIterationsCompleted()
    {
        return phaseIterationsCompleted;
    }

    public void setPhaseIterationsCompleted(int phaseIterationsCompleted)
    {
        this.phaseIterationsCompleted = phaseIterationsCompleted;
    }

    public int getPhasIterationCount()
    {
        return phasIterationCount;
    }

    public void setPhasIterationCount(int phasIterationCount)
    {
        this.phasIterationCount = phasIterationCount;
    }
}
