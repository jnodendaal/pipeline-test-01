package com.pilog.t8.definition.dstream;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamFinalizationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_DSTREAM_FINALIZATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_DSTREAM_FINALIZATION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "DStream Finalization Script";
    public static final String DESCRIPTION = "A script that is executed before DStream execution ends (after all nodes have completed).  The output from this script is added to the stream output parameters.";
    // -------- Definition Meta-Data -------- //
    
    public static final String PARAMETER_IDENTIFIER_STREAM_NODE_MAP = "$P_STREAM_NODES";
    
    public T8DStreamFinalizationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8DStreamDefinition streamDefinition;
        
        streamDefinition = (T8DStreamDefinition)getParentDefinition();
        if (streamDefinition != null)
        {
            List<T8DataParameterDefinition> parameterDefinitions;
            
            parameterDefinitions = streamDefinition.getStreamParameterDefinitions();
            parameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_IDENTIFIER_STREAM_NODE_MAP, "Stream Node Map", "A map of all the nodes contained by the stream.  Node identifiers are used as the keys in the map.", T8DataType.DEFINITION_IDENTIFIER_KEY_MAP));
            return parameterDefinitions;
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8DStreamDefinition streamDefinition;
        
        streamDefinition = (T8DStreamDefinition)getParentDefinition();
        if (streamDefinition != null) return streamDefinition.getStreamParameterDefinitions();
        else return null;
    }
}
