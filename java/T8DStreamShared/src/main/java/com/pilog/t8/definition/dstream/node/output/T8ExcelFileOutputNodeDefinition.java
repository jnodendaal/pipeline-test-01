package com.pilog.t8.definition.dstream.node.output;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputFieldDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputSheetDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileOutputNodeDefinition extends T8DStreamOutputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_OUTPUT_EXCEL_FILE";
    public static final String DISPLAY_NAME = "Excel Output File";
    public static final String DESCRIPTION = "A node that writes data to a defined Excel output file.";
    public static final String VERSION = "0";
    public enum Datum {OUTPUT_IDENTIFIER,
                       SHEET_IDENTIFIER,
                       INPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileOutputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OUTPUT_IDENTIFIER.toString(), "Output Identifier",  "The Excel Output to which this node will write data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SHEET_IDENTIFIER.toString(), "Sheet Identifier",  "The name of the Excel sheet to which this node will write data."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Stream to Excel Field", "A mapping of Stream Parameters to the corresponding Excel output field."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OUTPUT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<>();
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions(streamDefinition.getOutputDefinitions());
            }
            else return options;
        }
        else if (Datum.SHEET_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8ExcelFileOutputDefinition outputDefinition;

            options = new ArrayList<>();
            outputDefinition = (T8ExcelFileOutputDefinition)getLocalDefinition(getOutputIdentifier());
            if (outputDefinition != null)
            {
                List<T8ExcelFileOutputSheetDefinition> sheetDefinitions;

                sheetDefinitions = outputDefinition.getSheetDefinitions();
                for (T8ExcelFileOutputSheetDefinition sheetDefinition : sheetDefinitions)
                {
                    options.add(new T8DefinitionDatumOption(sheetDefinition.getIdentifier(), sheetDefinition.getIdentifier()));
                }

                return options;
            }
            else return options;
        }
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String sheetIdentifier;

            optionList = new ArrayList<>();

            sheetIdentifier = getSheetIdentifier();
            if (sheetIdentifier != null)
            {
                T8ExcelFileOutputSheetDefinition sheetDefinition;
                T8DStreamDefinition streamDefinition;

                sheetDefinition = (T8ExcelFileOutputSheetDefinition)getLocalDefinition(sheetIdentifier);
                streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((sheetDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8ExcelFileOutputFieldDefinition> excelFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    excelFieldDefinitions = sheetDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<>();
                    if ((streamParameterDefinitions != null) && (excelFieldDefinitions != null))
                    {
                        for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8ExcelFileOutputFieldDefinition fieldDefinition : excelFieldDefinitions)
                            {
                                identifierList.add(fieldDefinition.getIdentifier());
                            }

                            Collections.sort(identifierList);
                            identifierMap.put(streamParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.dstream.node.output.T8ExcelFileOutputNode", new Class<?>[]{T8Context.class, getClass()}, context, this);
    }

    public String getOutputIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OUTPUT_IDENTIFIER.toString());
    }

    public void setOutputIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OUTPUT_IDENTIFIER.toString(), identifier);
    }

    public String getSheetIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SHEET_IDENTIFIER.toString());
    }

    public void setSheetIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SHEET_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
