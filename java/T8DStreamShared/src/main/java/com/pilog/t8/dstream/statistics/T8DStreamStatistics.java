package com.pilog.t8.dstream.statistics;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamStatistics implements Serializable
{
    private Map<String, T8DStreamNodeStatistics> nodeStatistics;
    
    public T8DStreamStatistics()
    {
        this.nodeStatistics = new HashMap<String, T8DStreamNodeStatistics>();
    }
    
    public T8DStreamNodeStatistics createNodeStatistics(String nodeIdentifier)
    {
        T8DStreamNodeStatistics nodeStats;
        
        nodeStats = new T8DStreamNodeStatistics(nodeIdentifier);
        nodeStatistics.put(nodeIdentifier, nodeStats);
        return nodeStats;
    }
    
    public T8DStreamNodeStatistics getNodeStatistics(String nodeIdentifier)
    {
        return nodeStatistics.get(nodeIdentifier);
    }
}
