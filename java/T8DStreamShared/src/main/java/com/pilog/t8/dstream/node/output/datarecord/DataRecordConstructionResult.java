package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError;
import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DataRecordConstructionResult
{
    private DataRecord dataRecord;
    private List<T8DataRecordConformanceError> conformanceErrors;

    public DataRecordConstructionResult(DataRecord dataRecord, List<T8DataRecordConformanceError> conformanceErrors)
    {
        this.dataRecord = dataRecord;
        this.conformanceErrors = conformanceErrors;
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public void setDataRecord(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
    }

    public List<T8DataRecordConformanceError> getConformanceErrors()
    {
        return conformanceErrors;
    }

    public void setConformanceErrors(List<T8DataRecordConformanceError> conformanceErrors)
    {
        this.conformanceErrors = conformanceErrors;
    }
}
