package com.pilog.t8.definition.dstream.node.output.datarecord.datatypes;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8ValueRequirementMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DocumentReferenceMappingDefinition extends T8ValueRequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DOCUMENT_REFERENCE_MAPPING";
    public static final String DISPLAY_NAME = "Document Reference Mapping";
    public static final String DESCRIPTION = "A mapping of input data to a Document Reference requirement type.";
    public enum Datum {CONDITION_EXPRESSION,
                       DR_INSTANCE_GROUP_REFERENCE_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DocumentReferenceMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated to determine whether or not this mapping must be processed."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DR_INSTANCE_GROUP_REFERENCE_MAPPING_DEFINITIONS.toString(), "DR Instance Group Reference Mappings",  "The mappings applicable to DR Instance Group References."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DR_INSTANCE_GROUP_REFERENCE_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DRInstanceGroupReferenceMappingDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.DOCUMENT_REFERENCE;
    }

    @Override
    public List<String> getMatchParameterIdentifiers()
    {
        List<String> parameterIdentifiers;

        parameterIdentifiers = new ArrayList<String>();

        for (T8DRInstanceGroupReferenceMappingDefinition referenceMapping : getDRInstanceGroupReferenceMappingDefinitions())
        {
            parameterIdentifiers.addAll(referenceMapping.getMatchParameterIdentifiers());
        }

        return parameterIdentifiers;
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public List<T8DRInstanceGroupReferenceMappingDefinition> getDRInstanceGroupReferenceMappingDefinitions()
    {
        return (List<T8DRInstanceGroupReferenceMappingDefinition>)getDefinitionDatum(Datum.DR_INSTANCE_GROUP_REFERENCE_MAPPING_DEFINITIONS.toString());
    }

    public void setDRInstanceGroupReferenceMappingDefinitions(List<T8DRInstanceGroupReferenceMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_GROUP_REFERENCE_MAPPING_DEFINITIONS.toString(), definitions);
    }
}
