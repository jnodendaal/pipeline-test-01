package com.pilog.t8.dstream;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamNodeStatistics;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DStreamNode extends T8PerformanceStatisticsProvider
{
    public T8DStreamNodeDefinition getDefinition();
    public boolean isEnabled();
    public boolean isInitialized();
    public double getProgress();
    public int getNodeIterationCount();
    public int getNodeIterationsCompleted();
    public String getProgressText();
    public void stopExecution();
    public T8DStreamNodeStatistics getStatistics();

    public void initializeNode(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException;
    public void processNode(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException;
    public void finalizeNode(T8DataTransaction tx) throws Exception;

    public String getTypeName();
    public T8DStream getParentStream();
    public void setParentStream(T8DStream parentStream);
    public T8DStreamNode getParentNode();
    public List<T8DStreamNode> getDescendantNodes();
    public void setParentNode(T8DStreamNode parentNode);
    public ArrayList<String> getRecursableNodeNames();
}
