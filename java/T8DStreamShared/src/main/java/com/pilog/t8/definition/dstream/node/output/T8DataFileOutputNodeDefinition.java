package com.pilog.t8.definition.dstream.node.output;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileOutputNodeDefinition extends T8DStreamOutputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_OUTPUT_DATA_FILE";
    public static final String DISPLAY_NAME = "Data Output File";
    public static final String DESCRIPTION = "A node that writes data to a defined Data output file.";
    public static final String VERSION = "0";
    public enum Datum {OUTPUT_IDENTIFIER, TABLE_IDENTIFIER, COLUMN_VALUE_EXPRESSIONS};
    // -------- Definition Meta-Data -------- //

    public T8DataFileOutputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OUTPUT_IDENTIFIER.toString(), "Output Identifier",  "The Excel Output to which this node will write data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TABLE_IDENTIFIER.toString(), "Table Identifier",  "The name of the table to which this node will write data."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.EPIC_EXPRESSION), Datum.COLUMN_VALUE_EXPRESSIONS.toString(), "Column Value Expressions",  "The expressions that will be evaluated to determine the value written to each of the destination columns."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.node.output.T8DataFileOutputNode").getConstructor(T8Context.class, this.getClass());
            return (T8DStreamNode)constructor.newInstance(context, this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getOutputIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OUTPUT_IDENTIFIER.toString());
    }

    public void setOutputIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OUTPUT_IDENTIFIER.toString(), identifier);
    }

    public String getTableIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TABLE_IDENTIFIER.toString());
    }

    public void setTableIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TABLE_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getColumnValueExpressions()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.COLUMN_VALUE_EXPRESSIONS.toString());
    }

    public void setColumnValueExpressions(Map<String, String> expressions)
    {
        setDefinitionDatum(Datum.COLUMN_VALUE_EXPRESSIONS.toString(), expressions);
    }
}
