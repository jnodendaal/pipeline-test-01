package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8RecordCodeMappingDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_RECORD_CODE_MAPPING";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_DATA_RECORD_CODE_MAPPING";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Record Code Mapping";
    public static final String DESCRIPTION = "A mapping of an input parameter to a specific ontology code type that will be generated for the Data Record.";
    public enum Datum {CODE_TYPE_IDENTIFIER,
                       SOURCE_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8RecordCodeMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CODE_TYPE_IDENTIFIER.toString(), "Code Type", "The unique identifier of the Code Type that this mapping will generate."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), "Source Parameter", "The stream parameter from which the code ID will be fetched."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getCodeTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CODE_TYPE_IDENTIFIER.toString());
    }
    
    public void setCodeTypeIdentifier(String codeTypeIdentifier)
    {
        setDefinitionDatum(Datum.CODE_TYPE_IDENTIFIER.toString(), codeTypeIdentifier);
    }
}
