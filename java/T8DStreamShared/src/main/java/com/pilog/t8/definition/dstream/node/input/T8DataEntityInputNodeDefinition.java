package com.pilog.t8.definition.dstream.node.input;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityInputNodeDefinition extends T8DStreamInputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_INPUT_DATA_ENTITY";
    public static final String DISPLAY_NAME = "Data Entity Input";
    public static final String DESCRIPTION = "A node that reads data from a defined data entity.";
    public enum Datum {LOOKUP_MODE,
                       DATA_ENTITY_IDENTIFIER,
                       DATA_FILTER_DEFINITION,
                       DATA_FILTER_INPUT_PARAMETER_IDENTIFIER,
                       OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8DataEntityInputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier",  "The identifier of the Data Entity from which data will be read."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_FILTER_INPUT_PARAMETER_IDENTIFIER.toString(), "Data Filter Input Parameter",  "The parameter to use for data filter input (optional).  If set, the specified input parameter will be used as data filter for this entity input."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOOKUP_MODE.toString(), "Lookup Mode",  "If enabled, a single entity will be fetched and all child nodes will be processes even if no input data is found.", false));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.DATA_FILTER_DEFINITION.toString(), "Data Filter", "The that will be applied when retrieving data from the data entity."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Data Entity Field to Stream", "A mapping of Data Entity Fields to the corresponding Stream Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_FILTER_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;
                T8DStreamDefinition streamDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((dataEntityDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8DataEntityFieldDefinition> entityFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    entityFieldDefinitions = dataEntityDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<>();
                    if ((streamParameterDefinitions != null) && (entityFieldDefinitions != null))
                    {
                        for (T8DataEntityFieldDefinition fieldDefinition : entityFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                            {
                                identifierList.add(streamParameterDefinition.getIdentifier());
                            }

                            Collections.sort(identifierList);
                            identifierMap.put(fieldDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.DATA_FILTER_INPUT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createStreamParameterOptions();
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.dstream.node.input.T8DataEntityInputNode", new Class<?>[]{T8Context.class, this.getClass()}, context, this);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(getDataFilterDefinition() != null)
        {
            T8DataFilterDefinition dataFilterDefinition;

            dataFilterDefinition = getDataFilterDefinition();
            if (!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getPublicIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Invalid Entity Identifier on Filter, expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getDataFilterInputParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_FILTER_INPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setDataFilterInputParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_FILTER_INPUT_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public T8DataFilterDefinition getDataFilterDefinition()
    {
        return (T8DataFilterDefinition)getDefinitionDatum(Datum.DATA_FILTER_DEFINITION.toString());
    }

    public void setDataFilterDefinition(T8DataFilterDefinition definition)
    {
        setDefinitionDatum(Datum.DATA_FILTER_DEFINITION.toString(), definition);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public boolean isLookupMode()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.LOOKUP_MODE.toString());
        return enabled != null && enabled;
    }

    public void setLookupMode(boolean lookupMode)
    {
        setDefinitionDatum(Datum.LOOKUP_MODE.toString(), lookupMode);
    }
}
