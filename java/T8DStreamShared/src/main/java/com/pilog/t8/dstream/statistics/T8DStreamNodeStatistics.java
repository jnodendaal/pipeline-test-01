package com.pilog.t8.dstream.statistics;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamNodeStatistics implements Serializable
{
    private String nodeIdentifier;
    private int inputRowCount;
    private int outputRowCount;
    private int exceptionRowCount;
    
    public T8DStreamNodeStatistics(String identifier)
    {
        this.nodeIdentifier = identifier;
        this.inputRowCount = 0;
        this.outputRowCount = 0;
        this.exceptionRowCount = 0;
    }
    
    public String getNodeIdentifier()
    {
        return nodeIdentifier;
    }
    
    public int getInputRowCount()
    {
        return inputRowCount;
    }
    
    public void incrementInputRowCount()
    {
        inputRowCount++;
    }
    
    public int getOutputRowCount()
    {
        return outputRowCount;
    }
    
    public void incrementOutputRowCount()
    {
        outputRowCount++;
    }
    
    public int getExceptionRowCount()
    {
        return exceptionRowCount;
    }
    
    public void incrementExceptionRowCount()
    {
        exceptionRowCount++;
    }
}
