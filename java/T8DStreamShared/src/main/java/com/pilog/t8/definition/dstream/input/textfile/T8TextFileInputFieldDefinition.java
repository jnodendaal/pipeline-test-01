package com.pilog.t8.definition.dstream.input.textfile;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8TextFileInputFieldDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_FILE_TEXT_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_INPUT_FILE_TEXT_FIELD";
    public static final String IDENTIFIER_PREFIX = "TEXT_FIELD_IN_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Input Text File Field";
    public static final String DESCRIPTION = "A field contained by an input text file.";
    public enum Datum {TEXT_COLUMN_IDENTIFIER,
                       DATA_TYPE};
    // -------- Definition Meta-Data -------- //

    public T8TextFileInputFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        // Get the super class datum types.
        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TEXT_COLUMN_IDENTIFIER.toString(), "Text Column", "The name of the Text column to which this field corresponds."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_TYPE.toString(), "Data Type", "The data type of this field."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getTextColumnIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TEXT_COLUMN_IDENTIFIER.toString());
    }
    
    public void setTextColumnIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TEXT_COLUMN_IDENTIFIER.toString(), identifier);
    }    
    
    public String getDataTypeString()
    {
        return (String)getDefinitionDatum(Datum.DATA_TYPE.toString());
    }

    public void setDataTypeString(String dataTypeString)
    {
        setDefinitionDatum(Datum.DATA_TYPE.toString(), dataTypeString);
    }        
}
