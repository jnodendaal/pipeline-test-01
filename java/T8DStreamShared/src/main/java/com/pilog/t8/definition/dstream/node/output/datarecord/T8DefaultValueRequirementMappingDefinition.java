package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DefaultValueRequirementMappingDefinition extends T8ValueRequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_VALUE_REQUIREMENT_MAPPING";
    public static final String DISPLAY_NAME = "Value Requirement Mapping";
    public static final String DESCRIPTION = "A mapping of an input parameter to a specific value in a Data Requirement structure.";
    public enum Datum {REQUIREMENT_DATA_TYPE_IDENTIFIER,
                       REQUIREMENT_CONCEPT_IDENTIFIER,
                       ORGANIZATION_PARAMETER_IDENTIFIER,
                       CONCEPT_GROUP_PARAMETER_IDENTIFIER,
                       SOURCE_PARAMETER_IDENTIFIER,
                       CONDITION_EXPRESSION,
                       VALUE_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DefaultValueRequirementMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REQUIREMENT_DATA_TYPE_IDENTIFIER.toString(), "Requirement Data Type", "The requirement data type of the Data Requirement Value to which this mapping applies.", RequirementType.STRING_TYPE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REQUIREMENT_CONCEPT_IDENTIFIER.toString(), "Requirement Concept", "The requirement Concept to which this mapping is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ORGANIZATION_PARAMETER_IDENTIFIER.toString(), "Organization Parameter", "The parameter to use when an organization needs to be determined for this mapping."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONCEPT_GROUP_PARAMETER_IDENTIFIER.toString(), "Concept Group Parameter", "The parameter to use when a concept group needs to be determined for this mapping."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), "Source Parameter", "The stream parameter mapped to this value requirement."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated to determine whether or not this mapping must be processed."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.VALUE_MAPPING_DEFINITIONS.toString(), "Value Mappings",  "The data mappings to sub-values contained by the value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.ORGANIZATION_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.CONCEPT_GROUP_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.REQUIREMENT_DATA_TYPE_IDENTIFIER.toString().equals(datumIdentifier)) return createStringOptions(RequirementType.getAllDataTypeIDs());
        else if (Datum.VALUE_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DefaultValueRequirementMappingDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.getRequirementType(getRequirementDataTypeIdentifier());
    }

    @Override
    public List<String> getMatchParameterIdentifiers()
    {
        List<String> parameterIdentifiers;
        String sourceParameterIdentifier;

        sourceParameterIdentifier = getSourceParameterIdentifier();

        parameterIdentifiers = new ArrayList<String>();
        if (sourceParameterIdentifier != null)
        {
            RequirementType requirementType;

            // Make sure not to include sub-record references in the matching
            requirementType = getRequirementType();
            if (requirementType != RequirementType.DOCUMENT_REFERENCE)
            {
                parameterIdentifiers.add(sourceParameterIdentifier);
            }
        }

        for (T8DefaultValueRequirementMappingDefinition valueMappingDefinition : getValueMappingDefinitions())
        {
            List<String> valueParameterIdentifier;

            valueParameterIdentifier = valueMappingDefinition.getMatchParameterIdentifiers();
            for (String parameterIdentifier : valueParameterIdentifier)
            {
                if (!parameterIdentifiers.contains(parameterIdentifier))
                {
                    parameterIdentifiers.add(parameterIdentifier);
                }
            }
        }

        return parameterIdentifiers;
    }

    public String getRequirementDataTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_DATA_TYPE_IDENTIFIER.toString());
    }

    public void setRequirementDataTypeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_DATA_TYPE_IDENTIFIER.toString(), identifier);
    }

    public String getRequirementConceptIdentifier()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_CONCEPT_IDENTIFIER.toString());
    }

    public void setRequirementConceptIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_CONCEPT_IDENTIFIER.toString(), identifier);
    }

    public String getConceptGroupParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONCEPT_GROUP_PARAMETER_IDENTIFIER.toString());
    }

    public void setConceptGroupParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONCEPT_GROUP_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getOrganizationParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ORGANIZATION_PARAMETER_IDENTIFIER.toString());
    }

    public void setOrganizationParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ORGANIZATION_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SOURCE_PARAMETER_IDENTIFIER.toString());
    }

    public void setSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public List<T8DefaultValueRequirementMappingDefinition> getValueMappingDefinitions()
    {
        return (List<T8DefaultValueRequirementMappingDefinition>)getDefinitionDatum(Datum.VALUE_MAPPING_DEFINITIONS.toString());
    }

    public List<T8DefaultValueRequirementMappingDefinition> getValueMappingDefinitions(String dataTypeIdentifier)
    {
        List<T8DefaultValueRequirementMappingDefinition> mappingDefinitions;

        mappingDefinitions = new ArrayList<T8DefaultValueRequirementMappingDefinition>();
        for (T8DefaultValueRequirementMappingDefinition mappingDefinition : getValueMappingDefinitions())
        {
            if (dataTypeIdentifier.equals(mappingDefinition.getRequirementDataTypeIdentifier()))
            {
                mappingDefinitions.add(mappingDefinition);
            }
        }

        return mappingDefinitions;
    }

    public void setValueMappingDefinitions(List<T8DefaultValueRequirementMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.VALUE_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public void addValueMappingDefinition(T8DefaultValueRequirementMappingDefinition definition)
    {
        addSubDefinition(Datum.VALUE_MAPPING_DEFINITIONS.toString(), definition);
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }
}
