package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8RecordStatePredefinedMappingDefinition extends T8RecordStateMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_RECORD_STATE_PREDEFINED_MAPPING";
    public static final String DISPLAY_NAME = "Predefined Record State Mapping";
    public static final String DESCRIPTION = "A predefined mapping that will determine the state to assign to a generated Data Record.";
    public enum Datum {DATA_OBJECT_IDENTIFIER,
                       STATE_GRAPH_IDENTIFIER,
                       STATE_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8RecordStatePredefinedMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_OBJECT_IDENTIFIER.toString(), "Data Object Identifier", "The identifier of the data object to which a state will be assigned."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.STATE_GRAPH_IDENTIFIER.toString(), "State Identifier", "The identifier of the state to assign to the data object."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.STATE_IDENTIFIER.toString(), "State Graph Identifier", "The identifier of the state graph to which the data object will be assigned."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_OBJECT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if (Datum.STATE_GRAPH_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectStateGraphDefinition.GROUP_IDENTIFIER));
        else if (Datum.STATE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String stateGraphId;

            stateGraphId = getStateGraphIdentifier();
            if (stateGraphId != null)
            {
                T8DataObjectStateGraphDefinition graphDefinition;

                graphDefinition = (T8DataObjectStateGraphDefinition)definitionContext.getRawDefinition(getRootProjectId(), stateGraphId);
                if (graphDefinition != null)
                {
                    return createPublicIdentifierOptionsFromDefinitions(graphDefinition.getStateDefinitions());
                }
                else return null;
            }
            else return null;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public boolean isMappingApplicable(T8ServerContext serverContext, T8SessionContext sessionContext, DataRecord dataRecord)
    {
        // This mapping is always applicable.
        return true;
    }

    @Override
    public String getDataObjectIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, DataRecord dataRecord)
    {
        return getDataObjectIdentifier();
    }

    @Override
    public String getStateIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, DataRecord dataRecord)
    {
        return getStateIdentifier();
    }

    public String getDataObjectIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_OBJECT_IDENTIFIER.toString());
    }

    public void setDataObjectIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_OBJECT_IDENTIFIER.toString(), identifier);
    }

    public String getStateGraphIdentifier()
    {
        return (String)getDefinitionDatum(Datum.STATE_GRAPH_IDENTIFIER.toString());
    }

    public void setStateGraphIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.STATE_GRAPH_IDENTIFIER.toString(), identifier);
    }

    public String getStateIdentifier()
    {
        return (String)getDefinitionDatum(Datum.STATE_IDENTIFIER.toString());
    }

    public void setStateIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.STATE_IDENTIFIER.toString(), identifier);
    }
}
