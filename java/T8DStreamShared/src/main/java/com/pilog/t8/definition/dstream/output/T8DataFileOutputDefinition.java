package com.pilog.t8.definition.dstream.output;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileOutputDefinition extends T8DStreamOutputFileDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_DATA";
    public static final String DISPLAY_NAME = "Data Output File";
    public static final String DESCRIPTION = "A Data file that will be produced as output of the data stream.";
    public static final String VERSION = "0";
    public enum Datum {PASSWORD, COLUMN_IDENTIFIERS};
    // -------- Definition Meta-Data -------- //

    public T8DataFileOutputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PASSWORD.toString(), "Password",  "The password used to encrypt/decrypt data stored in the data file."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.COLUMN_IDENTIFIERS.toString(), "Columns",  "The Identifiers/Names of the columns in the file."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.file.T8DataFileOutputFileHandler").getConstructor(T8Context.class, this.getClass(), T8FileManager.class, T8DStreamFileContext.class);
        return (T8DStreamFileHandler)constructor.newInstance(context, this, fileManager, fileContext);
    }

    public String getPassword()
    {
        return (String)getDefinitionDatum(Datum.PASSWORD.toString());
    }

    public void setPassword(String password)
    {
        setDefinitionDatum(Datum.PASSWORD.toString(), password);
    }

    public List<String> getColumnIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.COLUMN_IDENTIFIERS.toString());
    }

    public void setColumnIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.COLUMN_IDENTIFIERS.toString(), identifiers);
    }

    public void addColumnIdentifier(String columnIdentifier)
    {
        List<String> columnIdentifiers = getColumnIdentifiers();

        if(columnIdentifiers == null) columnIdentifiers = new ArrayList<String>();

        columnIdentifiers.add(columnIdentifier);

        setColumnIdentifiers(columnIdentifiers);
    }
}
