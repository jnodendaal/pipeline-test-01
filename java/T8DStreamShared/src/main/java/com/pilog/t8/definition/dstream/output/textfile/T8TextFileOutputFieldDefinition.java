package com.pilog.t8.definition.dstream.output.textfile;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8TextFileOutputFieldDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_TEXT_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_OUTPUT_FILE_TEXT_FIELD";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "TEXT_FIELD_OUT_";
    public static final String DISPLAY_NAME = "Output Text File Field";
    public static final String DESCRIPTION = "A field contained by an output Text file.";
    public enum Datum {TEXT_COLUMN_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8TextFileOutputFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TEXT_COLUMN_IDENTIFIER.toString(), "Text Column", "The name of the text column to which this field corresponds."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getTextColumnIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TEXT_COLUMN_IDENTIFIER.toString());
    }
    
    public void setTextColumnIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TEXT_COLUMN_IDENTIFIER.toString(), identifier);
    }
}
