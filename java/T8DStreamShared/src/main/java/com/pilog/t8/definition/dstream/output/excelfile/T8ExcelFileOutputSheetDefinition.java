package com.pilog.t8.definition.dstream.output.excelfile;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8RequirementMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8ExcelFileOutputSheetDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_EXCEL_SHEET";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_OUTPUT_FILE_EXCEL_SHEET";
    public static final String IDENTIFIER_PREFIX = "OUTPUT_EXCEL_SHEET_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Output Excel File Sheet";
    public static final String DESCRIPTION = "A sheet contained by an output excel file.";
    public static final String VERSION = "0";
    public enum Datum {SHEET_NAME,
                       FIELD_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileOutputSheetDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SHEET_NAME.toString(), "Sheet Name", "The unique name of the Excel sheet as it appears in the Excel document."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Fields", "The fields/columns contained by this Excel sheet."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ExcelFileOutputFieldDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getSheetName()
    {
        return (String)getDefinitionDatum(Datum.SHEET_NAME.toString());
    }

    public void setSheetName(String identifier)
    {
        setDefinitionDatum(Datum.SHEET_NAME.toString(), identifier);
    }

    public List<T8ExcelFileOutputFieldDefinition> getFieldDefinitions()
    {
        return (List<T8ExcelFileOutputFieldDefinition>)getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8ExcelFileOutputFieldDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), definitions);
    }

    public void addFieldDefinition(T8ExcelFileOutputFieldDefinition definition)
    {
        addSubDefinition(Datum.FIELD_DEFINITIONS.toString(), definition);
    }
}
