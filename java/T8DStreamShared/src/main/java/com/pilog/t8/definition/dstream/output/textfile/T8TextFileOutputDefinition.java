package com.pilog.t8.definition.dstream.output.textfile;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputFileDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextFileOutputDefinition extends T8DStreamOutputFileDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_TEXT";
    public static final String DISPLAY_NAME = "Text Output File";
    public static final String DESCRIPTION = "A text file that will be produced as output of the data stream.";
    public static final String VERSION = "0";
    public enum Datum {ENCODING,
                       SEPARATOR_STRING,
                       FIELD_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public enum OutputTextEncoding
    {
        UTF_8("UTF-8"),
        UTF_16("UTF-16");

        private String code; // Used internally when input/output streams are created.
        OutputTextEncoding(String code)
        {
            this.code = code;
        }

        public String getCode()
        {
            return code;
        }
    };

    public T8TextFileOutputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ENCODING.toString(), "Default Encoding",  "The default encoding to use when writing data to the text file.", OutputTextEncoding.UTF_8.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEPARATOR_STRING.toString(), "Separator String",  "The String that is used to separate columns."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Fields", "The fields/columns contained be this Text file."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ENCODING.toString().equals(datumIdentifier)) return createStringOptions(OutputTextEncoding.values());
        else if (Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TextFileOutputFieldDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);

    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.file.T8TextOutputFileHandler").getConstructor(T8Context.class, this.getClass(), T8FileManager.class, T8DStreamFileContext.class);
        return (T8DStreamFileHandler)constructor.newInstance(context, this, fileManager, fileContext);
    }

    public OutputTextEncoding getEncoding()
    {
        String encodingString;

        encodingString = (String)getDefinitionDatum(Datum.ENCODING.toString());
        return encodingString != null ? OutputTextEncoding.valueOf(encodingString) : OutputTextEncoding.UTF_8;
    }

    public void setEncoding(OutputTextEncoding encoding)
    {
        setDefinitionDatum(Datum.ENCODING.toString(), encoding.toString());
    }

    public String getSeparatorString()
    {
        return (String)getDefinitionDatum(Datum.SEPARATOR_STRING.toString());
    }

    public void setSeparatorString(String encoding)
    {
        setDefinitionDatum(Datum.SEPARATOR_STRING.toString(), encoding);
    }

    public List<T8TextFileOutputFieldDefinition> getFieldDefinitions()
    {
        return (List<T8TextFileOutputFieldDefinition>)getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8TextFileOutputFieldDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), definitions);
    }

    public void addFieldDefinition(T8TextFileOutputFieldDefinition definition)
    {
        addSubDefinition(Datum.FIELD_DEFINITIONS.toString(), definition);
    }
}
