package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationContext;
import com.pilog.t8.dstream.node.output.datarecord.T8DataRecordCreationMapping;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DataRequirementMappingDefinition extends T8DataRecordCreationMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_REQUIREMENT_MAPPING";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Requirement Mapping";
    public static final String DESCRIPTION = "A mapping of input parameters to a specific part of a Data Requirement structure.";
    public enum Datum
    {
        ALLOW_EMPTY_RECORD,
        DATA_REQUIREMENT_IDENTIFIER,
        RECORD_ID_SOURCE_PARAMETER_IDENTIFIER,
        FFT_SOURCE_PARAMETER_IDENTIFIER,
        PROPERTY_MAPPING_DEFINITIONS,
        CONDITION_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRequirementMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_EMPTY_RECORD.toString(), "Allow Empty Record", "If this flag is set, records will be created even when no property content has been added."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_REQUIREMENT_IDENTIFIER.toString(), "Data Requirement", "The unique identifier of the Data Requirement to which this mapping applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_ID_SOURCE_PARAMETER_IDENTIFIER.toString(), "Record ID Source Parameter", "The stream parameter mapped to the Record ID of the record to be generated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString(), "FFT Source Parameter", "The stream parameter mapped to the FFT of the record."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_MAPPING_DEFINITIONS.toString(), "Property Mappings",  "The data mappings to properties contained by the Data Requirement."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated to determine whether or not this mapping must be processed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.RECORD_ID_SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumId))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumId))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.PROPERTY_MAPPING_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PropertyRequirementMappingDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.dstream.T8DataRequirementMappingActionHandler").getConstructor(T8Context.class, this.getClass());
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DataRecordCreationMapping createNewMappingInstance(T8DataRecordCreationContext context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.output.datarecord.T8DataRequirementMapping").getConstructor(T8DataRecordCreationContext.class, this.getClass());
        return (T8DataRecordCreationMapping)constructor.newInstance(context, this);
    }

    public List<String> getMatchParameterIdentifiers()
    {
        DataRecordMatchType matchType;

        matchType = getDataRecordMatchType();
        if (matchType == DataRecordMatchType.KEY_PARAMETERS)
        {
            return getKeyParameterIdentifiers();
        }
        else if (matchType == DataRecordMatchType.CODE)
        {
            String codeType;

            codeType = this.getMatchCodeTypeID();
            for (T8RecordCodeMappingDefinition codeMappingDefinition : getCodeMappingDefinitions())
            {
                if (codeType.equals(codeMappingDefinition.getCodeTypeIdentifier()))
                {
                    return ArrayLists.newArrayList(codeMappingDefinition.getSourceParameterIdentifier());
                }
            }

            return new ArrayList<String>();
        }
        else
        {
            List<String> parameterIdentifiers;

            parameterIdentifiers = new ArrayList<String>();
            for (T8PropertyRequirementMappingDefinition propertyMappingDefinition : getPropertyMappingDefinitions())
            {
                List<String> propertyParameterIdentifiers;

                propertyParameterIdentifiers = propertyMappingDefinition.getMatchParameterIdentifiers();
                for (String parameterIdentifier : propertyParameterIdentifiers)
                {
                    if (!parameterIdentifiers.contains(parameterIdentifier))
                    {
                        parameterIdentifiers.add(parameterIdentifier);
                    }
                }
            }

            return parameterIdentifiers;
        }
    }

    public void setAllowEmptyRecord(boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_EMPTY_RECORD.toString(), allow);
    }

    public boolean isAllowEmptyRecord()
    {
        Boolean allow;

        allow = (Boolean)getDefinitionDatum(Datum.ALLOW_EMPTY_RECORD.toString());
        return allow != null && allow;
    }

    public String getDataRequirementIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_REQUIREMENT_IDENTIFIER.toString());
    }

    public void setDataRequirementIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_IDENTIFIER.toString(), identifier);
    }

    public String getRecordIDSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RECORD_ID_SOURCE_PARAMETER_IDENTIFIER.toString());
    }

    public void setRecordIDSourceParameterIdentfier(String identifier)
    {
        setDefinitionDatum(Datum.RECORD_ID_SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getFFTSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString());
    }

    public void setFFTSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public List<T8PropertyRequirementMappingDefinition> getPropertyMappingDefinitions()
    {
        return (List<T8PropertyRequirementMappingDefinition>)getDefinitionDatum(Datum.PROPERTY_MAPPING_DEFINITIONS.toString());
    }

    public T8PropertyRequirementMappingDefinition getPropertyMappingDefinition(String propertyIdentifier)
    {
        for (T8PropertyRequirementMappingDefinition mappingDefinition : getPropertyMappingDefinitions())
        {
            if (propertyIdentifier.equals(mappingDefinition.getPropertyIdentifier()))
            {
                return mappingDefinition;
            }
        }

        return null;
    }

    public void setPropertyMappingDefinitions(List<T8PropertyRequirementMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_MAPPING_DEFINITIONS.toString(), definitions);
    }

    public void addPropertyMappingDefinition(T8PropertyRequirementMappingDefinition definition)
    {
        addSubDefinition(Datum.PROPERTY_MAPPING_DEFINITIONS.toString(), definition);
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }
}
