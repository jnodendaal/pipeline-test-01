package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8SectionRequirementMappingDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_CLASS_REQUIREMENT_MAPPING";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_CLASS_REQUIREMENT_MAPPING";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Section Requirement Mapping";
    public static final String DESCRIPTION = "A mapping of an input parameter to a specific class in a Data Requirement structure.";
    public enum Datum {CLASS_IDENTIFIER, PROPERTY_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8SectionRequirementMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CLASS_IDENTIFIER.toString(), "Class", "The unique identifier of the Data Requirement Class to which this mapping applies."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_MAPPING_DEFINITIONS.toString(), "Property Mappings",  "The data mappings to properties contained by the class."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PROPERTY_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PropertyRequirementMappingDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getSectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CLASS_IDENTIFIER.toString());
    }
    
    public void setSectionIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CLASS_IDENTIFIER.toString(), identifier);
    }
    
    public List<T8PropertyRequirementMappingDefinition> getPropertyMappingDefinitions()
    {
        return (List<T8PropertyRequirementMappingDefinition>)getDefinitionDatum(Datum.PROPERTY_MAPPING_DEFINITIONS.toString());
    }
    
    public T8PropertyRequirementMappingDefinition getPropertyMappingDefinition(String propertyIdentifier)
    {
        for (T8PropertyRequirementMappingDefinition mappingDefinition : getPropertyMappingDefinitions())
        {
            if (propertyIdentifier.equals(mappingDefinition.getPropertyIdentifier()))
            {
                return mappingDefinition;
            }
        }
        
        return null;
    }
    
    public void setPropertyMappingDefinitions(List<T8PropertyRequirementMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_MAPPING_DEFINITIONS.toString(), definitions);
    }
    
    public void addPropertyMappingDefinition(T8PropertyRequirementMappingDefinition definition)
    {
        addSubDefinition(Datum.PROPERTY_MAPPING_DEFINITIONS.toString(), definition);
    }
}
