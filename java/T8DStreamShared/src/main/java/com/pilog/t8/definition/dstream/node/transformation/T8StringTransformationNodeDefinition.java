package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Hennie Brink
 */
public class T8StringTransformationNodeDefinition extends T8DStreamTransformationNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_STRING";
    public static final String DISPLAY_NAME = "String Transformation";
    public static final String DESCRIPTION = "A node that can perform string transformations to string parameters.";

    public enum Datum {TRANSFORMATION_TYPE,
                       TRANFORMATION_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public enum TransformationType
    {
        UPPER_CASE,
        LOWER_CASE,
        SENTENCE_CASE,
        TITLE_CASE
    }

    public T8StringTransformationNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TRANSFORMATION_TYPE.toString(), "Transformation Type", "The type of transformation that will be applied to the parameters", TransformationType.SENTENCE_CASE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.TRANFORMATION_PARAMETER_MAPPING.toString(), "Transformation Mapping (Stream input - > Stream output)", "The mapping of input parameters that will be transformed and the placed in the output parameters"));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TRANSFORMATION_TYPE.toString().equals(datumIdentifier)) return createStringOptions(TransformationType.values());
        else if (Datum.TRANFORMATION_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;

            optionList = new ArrayList<>();

            T8DStreamDefinition streamDefinition;

            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

            if (streamDefinition != null)
            {
                List<T8DataParameterDefinition> streamParameterDefinitions;
                TreeMap<String, List<String>> identifierMap;

                streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();

                identifierMap = new TreeMap<>();
                if (streamParameterDefinitions != null)
                {
                    for (T8DataParameterDefinition fieldDefinition : streamParameterDefinitions)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<>();
                        for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                        {
                            identifierList.add(streamParameterDefinition.getIdentifier());
                        }

                        Collections.sort(identifierList);
                        identifierMap.put(fieldDefinition.getIdentifier(), identifierList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            else return new ArrayList<>();
        }
        else
            return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8StringTransformationNode").getConstructor(T8Context.class, T8StringTransformationNodeDefinition.class);
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public TransformationType getTransformationType()
    {
        return TransformationType.valueOf((String)getDefinitionDatum(Datum.TRANSFORMATION_TYPE.toString()));
    }

    public void setTransformationType(TransformationType type)
    {
        setDefinitionDatum(Datum.TRANSFORMATION_TYPE.toString(), type.toString());
    }

    public Map<String, String> getTransformationParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.TRANFORMATION_PARAMETER_MAPPING.toString());
    }

    public void setTransformationParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.TRANFORMATION_PARAMETER_MAPPING.toString(), mapping);
    }
}
