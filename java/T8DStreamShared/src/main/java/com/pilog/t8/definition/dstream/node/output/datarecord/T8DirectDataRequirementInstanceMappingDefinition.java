package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DirectDataRequirementInstanceMappingDefinition extends T8DataRequirementInstanceMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_REQUIREMENT_INSTANCE_MAPPING";
    public static final String DISPLAY_NAME = "Data Requirement Instance Mapping";
    public static final String DESCRIPTION = "A mapping of input parameters to a specific part of a Data Requirement structure.";
    public enum Datum
    {
        DATA_REQUIREMENT_INSTANCE_IDENTIFIER,
        FFT_SOURCE_PARAMETER_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public T8DirectDataRequirementInstanceMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString(), "Data Requirement Instance", "The unique identifier of the Data Requirement Instance to which this mapping applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString(), "FFT Source Parameter", "The stream parameter mapped to the FFT of the record."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.dstream.T8DataRequirementInstanceMappingActionHandler").getConstructor(T8Context.class, this.getClass());
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getDataRequirementInstanceIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString());
    }

    public void setDataRequirementInstanceIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString(), identifier);
    }

    public String getFFTSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString());
    }

    public void setFFTSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FFT_SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
