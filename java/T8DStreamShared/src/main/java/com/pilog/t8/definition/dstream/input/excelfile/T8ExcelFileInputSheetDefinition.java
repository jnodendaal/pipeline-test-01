package com.pilog.t8.definition.dstream.input.excelfile;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8RequirementMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8ExcelFileInputSheetDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_FILE_EXCEL_SHEET";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_INPUT_FILE_EXCEL_SHEET";
    public static final String IDENTIFIER_PREFIX = "INPUT_EXCEL_SHEET_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Input Excel File Sheet";
    public static final String DESCRIPTION = "A sheet contained by an input excel file.";
    public static final String VERSION = "0";
    public enum Datum
    {
        SHEET_NAME,
        FIELD_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileInputSheetDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SHEET_NAME.toString(), "Sheet", "The unique identifier/name of the Excel sheet as it appears in the Excel document."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Fields", "The fields/columns contained by this Excel sheet."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ExcelFileInputFieldDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.FIELD_DEFINITIONS.toString().equals(datumId))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumId));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.dstream.T8ExcelFileInputSheetActionHandler").getConstructor(T8Context.class, this.getClass());
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getSheetName()
    {
        return (String)getDefinitionDatum(Datum.SHEET_NAME.toString());
    }

    public void setSheetName(String identifier)
    {
        setDefinitionDatum(Datum.SHEET_NAME.toString(), identifier);
    }

    public List<T8ExcelFileInputFieldDefinition> getFieldDefinitions()
    {
        return (List<T8ExcelFileInputFieldDefinition>)getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8ExcelFileInputFieldDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), definitions);
    }

    public void addFieldDefinition(T8ExcelFileInputFieldDefinition fieldDefinition)
    {
        addSubDefinition(Datum.FIELD_DEFINITIONS.toString(), fieldDefinition);
    }
}
