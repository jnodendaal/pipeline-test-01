package com.pilog.t8.dstream.exception;

import com.pilog.t8.exception.T8UserException;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamException extends Exception implements T8UserException
{
    private String userMessage;
    private String detailMessage;
    private String nodeName;
    private Map<String, Object> dataRow;

    public T8DStreamException(String detailMessage, String userMessage, String nodeName, Map<String, Object> dataRow)
    {
        this(null, detailMessage, userMessage, nodeName, dataRow);
    }

    public T8DStreamException(Throwable cause, String detailMessage, String userMessage, String nodeName, Map<String, Object> dataRow)
    {
        super(detailMessage, cause);
        this.detailMessage = detailMessage;
        this.userMessage = userMessage;
        this.nodeName = nodeName;
        this.dataRow = dataRow;
    }

    public Map<String, Object> getDataRow()
    {
        return dataRow;
    }

    public String getNodeName()
    {
        return nodeName;
    }

    @Override
    public String getUserMessage()
    {
        return userMessage;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }
}
