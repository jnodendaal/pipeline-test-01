package com.pilog.t8.definition.dstream.input;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DStreamInputPanelListener extends EventListener
{
    public void inputStateChanged(T8DStreamInputPanel source);
}
