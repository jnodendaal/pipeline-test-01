package com.pilog.t8.dstream;

import com.pilog.t8.definition.dstream.input.T8DStreamInputFileDefinition;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputFileDefinition;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamFileContext implements Serializable
{
    private String fileContextInstanceIdentifier;
    private Map<String, String> inputFileNames;
    private Map<String, String> outputFileNames;

    public static final String LOG_FILE_NAME = "Log.txt";
    public static final String REMOTE_INPUT_DIRECTORY_PATH = "input/";
    public static final String REMOTE_OUTPUT_DIRECTORY_PATH = "output/";

    public T8DStreamFileContext(String fileContextInstanceIdentifier)
    {
        this.fileContextInstanceIdentifier = fileContextInstanceIdentifier;
        this.inputFileNames = new HashMap<String, String>();
        this.outputFileNames = new HashMap<String, String>();
    }

    public String getFileContextInstanceIdentifier()
    {
        return fileContextInstanceIdentifier;
    }

    public String getInputDirectoryPath()
    {
        return REMOTE_INPUT_DIRECTORY_PATH;
    }

    public String getOutputDirectoryPath()
    {
        return REMOTE_OUTPUT_DIRECTORY_PATH;
    }

    public String getLogFilePath()
    {
        return REMOTE_OUTPUT_DIRECTORY_PATH + "/" + LOG_FILE_NAME;
    }

    /**
     * Returns the path of the directory where all output data is temporarily
     * stored during stream execution.  All output files are stored in the same
     * folder.
     * @param outputDefinition The definition for which to return the output
     * directory.
     * @return The path of the directory where output data is stored.
     */
    public String getOutputDirectoryPath(T8DStreamOutputFileDefinition outputDefinition)
    {
        return REMOTE_OUTPUT_DIRECTORY_PATH;
    }

    /**
     * Returns the path of the directory where input data is temporarily stored
     * during stream execution.  Each input definition's input data is stored
     * in a separate folder.
     * @param inputDefinition The definition for which to resolve the input
     * directory path.
     * @return The path of the directory where input data for the supplied
     * definition is stored.
     */
    public String getInputDirectoryPath(T8DStreamInputFileDefinition inputDefinition)
    {
        return REMOTE_INPUT_DIRECTORY_PATH + "/" + inputDefinition.getIdentifier();
    }

    public String getOutputFilePath(T8DStreamOutputFileDefinition outputDefinition)
    {
        String outputFileName;

        outputFileName = getOutputFileName(outputDefinition);
        return (outputFileName != null) ? (getOutputDirectoryPath(outputDefinition) + "/" + outputFileName) : null;
    }

    public String getInputFilePath(T8DStreamInputFileDefinition inputDefinition)
    {
        String inputFileName;

        inputFileName = getInputFileName(inputDefinition);
        return (inputFileName != null) ? (getInputDirectoryPath(inputDefinition) + "/" + inputFileName) : null;
    }

    public String getOutputFileName(T8DStreamOutputFileDefinition outputDefinition)
    {
        return outputFileNames.get(outputDefinition.getIdentifier());
    }

    public void addOuputFileName(T8DStreamOutputFileDefinition outputDefinition, String fileName)
    {
        outputFileNames.put(outputDefinition.getIdentifier(), fileName);
    }

    public String getInputFileName(T8DStreamInputFileDefinition inputDefinition)
    {
        return inputFileNames.get(inputDefinition.getIdentifier());
    }

    public void addInputFileName(T8DStreamInputFileDefinition inputDefinition, String fileName)
    {
        inputFileNames.put(inputDefinition.getIdentifier(), fileName);
    }
}
