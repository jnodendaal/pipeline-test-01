package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DataConformanceErrorOutputDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_CONFORMANCE_ERROR_OUTPUT";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_DATA_CONFORMANCE_ERROR_OUTPUT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Conformance Error Output";
    public static final String DESCRIPTION = "A setup of how data conformance errors are output to the stream.";
    public static final String VERSION = "0";
    public enum Datum {DATA_RECORD_ID_PARAMETER_IDENTIFIER, 
                       DR_INSTANCE_ID_PARAMETER_IDENTIFIER,
                       DR_ID_PARAMETER_IDENTIFIER,
                       CLASS_ID_PARAMETER_IDENTIFIER, 
                       SECTION_ID_PARAMETER_IDENTIFIER,
                       PROPERTY_ID_PARAMETER_IDENTIFIER,
                       VALUE_PARAMETER_IDENTIFIER,
                       ERROR_TYPE_PARAMETER_IDENTIFIER,
                       ERROR_MESSAGE_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DataConformanceErrorOutputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_ID_PARAMETER_IDENTIFIER.toString(), "Data Record ID Parameter", "The Stream parameter to which the Data Record ID will be output (where a conformance failure occurred)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString(), "Data Requirement Instance ID Parameter", "The Stream parameter to which the Data Requirement Instance ID will be output (where a conformance failure occurred)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DR_ID_PARAMETER_IDENTIFIER.toString(), "Data Requirement ID Parameter", "The Stream parameter to which the Data Requirement ID will be output (where a conformance failure occurred)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CLASS_ID_PARAMETER_IDENTIFIER.toString(), "Class ID Parameter", "The Stream parameter to which the Record Class ID will be output (where a conformance failure occurred)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SECTION_ID_PARAMETER_IDENTIFIER.toString(), "Section ID Parameter", "The Stream parameter to which the Record Section ID will be output (where a conformance failure occurred)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString(), "Property ID Parameter", "The Stream parameter to which the Record Property ID will be output (where a conformance failure occurred)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.VALUE_PARAMETER_IDENTIFIER.toString(), "Value Parameter", "The Stream parameter to which the value that caused the error will be output (when a conformance failure occurs)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ERROR_TYPE_PARAMETER_IDENTIFIER.toString(), "Error Type Parameter", "The Stream parameter to which the error type will be output (when a conformance failure occurs)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ERROR_MESSAGE_PARAMETER_IDENTIFIER.toString(), "Error Message Parameter", "The Stream parameter to which the error message will be output (when a conformance failure occurs)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_RECORD_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.DR_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.CLASS_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.SECTION_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.VALUE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.ERROR_TYPE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else if (Datum.ERROR_MESSAGE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getDataRecordIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDataRecordIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getDRInstanceIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDRInstanceIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getDRIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setDRIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getClassIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CLASS_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setClassIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CLASS_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getSectionIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SECTION_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setSectionIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SECTION_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getPropertyIDParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setPropertyIDParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROPERTY_ID_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getValueParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.VALUE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setValueParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.VALUE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getErrorTypeParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ERROR_TYPE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setErrorTypeParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ERROR_TYPE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getErrorMessageParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ERROR_MESSAGE_PARAMETER_IDENTIFIER.toString());
    }
    
    public void setErrorMessageParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ERROR_MESSAGE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    private ArrayList<T8DefinitionDatumOption> createStreamParameterOptions()
    {
        ArrayList<T8DefinitionDatumOption> options;
        T8DStreamDefinition streamDefinition;

        options = new ArrayList<T8DefinitionDatumOption>();
        options.add(new T8DefinitionDatumOption("No Mapping", null));
        streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
        if (streamDefinition != null)
        {
            options.addAll(createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getStreamParameterDefinitions()));
            return options;
        }
        else return options;
    }
}
