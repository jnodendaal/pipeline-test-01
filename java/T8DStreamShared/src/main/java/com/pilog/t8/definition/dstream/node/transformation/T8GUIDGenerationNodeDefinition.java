package com.pilog.t8.definition.dstream.node.transformation;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8GUIDGenerationNodeDefinition extends T8DStreamTransformationNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_TRANSFORMATION_GUID_GENERATION";
    public static final String DISPLAY_NAME = "GUID Generation";
    public static final String DESCRIPTION = "A node that generates a new GUID and sends it into the stream.";
    public static final String VERSION = "0";
    public enum Datum {RESULT_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8GUIDGenerationNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.RESULT_PARAMETER_IDENTIFIER.toString(), "Result Parameter", "The Stream Parameter to which the generated GUID will be sent."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.RESULT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier)) return createStreamParameterOptions();
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.transformation.T8GUIDGenerationNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public String getResultParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RESULT_PARAMETER_IDENTIFIER.toString());
    }

    public void setResultParameterIdentifier(String parameterIdentifier)
    {
        setDefinitionDatum(Datum.RESULT_PARAMETER_IDENTIFIER.toString(), parameterIdentifier);
    }
}
