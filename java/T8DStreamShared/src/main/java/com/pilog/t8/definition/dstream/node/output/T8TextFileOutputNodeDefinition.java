package com.pilog.t8.definition.dstream.node.output;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.definition.dstream.output.textfile.T8TextFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.textfile.T8TextFileOutputFieldDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8TextFileOutputNodeDefinition extends T8DStreamOutputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_OUTPUT_TEXT_FILE";
    public static final String DISPLAY_NAME = "Text Output File";
    public static final String DESCRIPTION = "A node that writes data to a defined Text output file.";
    public static final String VERSION = "0";
    public enum Datum {OUTPUT_IDENTIFIER,
                       INPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8TextFileOutputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OUTPUT_IDENTIFIER.toString(), "Output Identifier",  "The Text Output to which this node will write data."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Stream to Text Field", "A mapping of Stream Parameters to the corresponding Text output field."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OUTPUT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getOutputDefinitions());
            }
            else return options;
        }
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String outputIdentifier;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            outputIdentifier = getOutputIdentifier();
            if (outputIdentifier != null)
            {
                T8TextFileOutputDefinition outputDefinition;
                T8DStreamDefinition streamDefinition;

                outputDefinition = (T8TextFileOutputDefinition)getLocalDefinition(outputIdentifier);
                streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((outputDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8TextFileOutputFieldDefinition> textFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    textFieldDefinitions = outputDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((streamParameterDefinitions != null) && (textFieldDefinitions != null))
                    {
                        for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8TextFileOutputFieldDefinition fieldDefinition : textFieldDefinitions)
                            {
                                identifierList.add(fieldDefinition.getIdentifier());
                            }

                            Collections.sort(identifierList);
                            identifierMap.put(streamParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.node.output.T8TextFileOutputNode").getConstructor(T8Context.class, this.getClass());
            return (T8DStreamNode)constructor.newInstance(context, this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public String getOutputIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OUTPUT_IDENTIFIER.toString());
    }

    public void setOutputIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OUTPUT_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
