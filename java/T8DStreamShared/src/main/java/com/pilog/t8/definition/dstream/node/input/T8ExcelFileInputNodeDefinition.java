package com.pilog.t8.definition.dstream.node.input;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputFieldDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputSheetDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileInputNodeDefinition extends T8DStreamInputNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_INPUT_EXCEL_FILE";
    public static final String DISPLAY_NAME = "Excel Input File";
    public static final String DESCRIPTION = "A node that reads Excel data from a defined input file.";
    public enum Datum
    {
        INPUT_IDENTIFIER,
        SHEET_IDENTIFIER,
        FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER,
        ROW_NUMBER_OUTPUT_PARAMETER_IDENTIFIER,
        OUTPUT_PARAMETER_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileInputNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INPUT_IDENTIFIER.toString(), "Input Identifier",  "The Excel Input from which this node will read data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SHEET_IDENTIFIER.toString(), "Sheet Identifier",  "The name of the Excel sheet from which this node will read data.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.INPUT_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString(), "File Name Output Parameter",  "The stream parameter to which the name of the input file will be output (if any)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ROW_NUMBER_OUTPUT_PARAMETER_IDENTIFIER.toString(), "Row Number Output Parameter",  "The stream parameter to which the row number read from the excel file will be output."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Excel Field to Stream", "A mapping of Excel Fields to the corresponding Stream Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8DStreamDefinition streamDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            if (streamDefinition != null)
            {
                // Only get Excel Input File Definitions.
                return createLocalIdentifierOptionsFromDefinitions((List)streamDefinition.getInputDefinitionsByType(T8ExcelFileInputDefinition.TYPE_IDENTIFIER));
            }
            else return options;
        }
        else if (Datum.SHEET_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            T8ExcelFileInputDefinition inputDefinition;

            options = new ArrayList<T8DefinitionDatumOption>();
            inputDefinition = (T8ExcelFileInputDefinition)getLocalDefinition(getInputIdentifier());
            if (inputDefinition != null)
            {
                List<T8ExcelFileInputSheetDefinition> sheetDefinitions;

                sheetDefinitions = inputDefinition.getSheetDefinitions();
                for (T8ExcelFileInputSheetDefinition sheetDefinition : sheetDefinitions)
                {
                    options.add(new T8DefinitionDatumOption(sheetDefinition.getIdentifier(), sheetDefinition.getIdentifier()));
                }

                return options;
            }
            else return options;
        }
        else if (Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createStreamParameterOptions();
        }
        else if (Datum.ROW_NUMBER_OUTPUT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createStreamParameterOptions();
        }
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String sheetIdentifier;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            sheetIdentifier = getSheetIdentifier();
            if (sheetIdentifier != null)
            {
                T8ExcelFileInputSheetDefinition sheetDefinition;
                T8DStreamDefinition streamDefinition;

                sheetDefinition = (T8ExcelFileInputSheetDefinition)getLocalDefinition(sheetIdentifier);
                streamDefinition = (T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);

                if ((sheetDefinition != null) && (streamDefinition != null))
                {
                    List<T8DataParameterDefinition> streamParameterDefinitions;
                    List<T8ExcelFileInputFieldDefinition> excelFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    streamParameterDefinitions = streamDefinition.getStreamParameterDefinitions();
                    excelFieldDefinitions = sheetDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((streamParameterDefinitions != null) && (excelFieldDefinitions != null))
                    {
                        for (T8ExcelFileInputFieldDefinition fieldDefinition : excelFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition streamParameterDefinition : streamParameterDefinitions)
                            {
                                identifierList.add(streamParameterDefinition.getIdentifier());
                            }

                            Collections.sort(identifierList);
                            identifierMap.put(fieldDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.dstream.T8ExcelFileInputNodeActionHandler").getConstructor(T8Context.class, this.getClass());
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DStreamNode getNewNodeInstance(T8Context context)  throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.node.input.T8ExcelFileInputNode").getConstructor(T8Context.class, this.getClass());
        return (T8DStreamNode)constructor.newInstance(context, this);
    }

    public String getInputIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INPUT_IDENTIFIER.toString());
    }

    public void setInputIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INPUT_IDENTIFIER.toString(), identifier);
    }

    public String getSheetIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SHEET_IDENTIFIER.toString());
    }

    public void setSheetIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SHEET_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public String getFileNameOutputParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setFileNameOutputParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FILE_NAME_OUTPUT_PARAMETER_IDENTIFIER.toString(), identifier);
    }

    public String getRowNumberOutputParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ROW_NUMBER_OUTPUT_PARAMETER_IDENTIFIER.toString());
    }

    public void setRowNumberOutputParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ROW_NUMBER_OUTPUT_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
