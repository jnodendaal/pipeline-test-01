package com.pilog.t8.definition.dstream;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.input.T8DStreamInputDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputDefinition;
import com.pilog.t8.dstream.T8DStream;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM";
    public static final String STORAGE_PATH = "/data_streams";
    public static final String DISPLAY_NAME = "Data Stream";
    public static final String DESCRIPTION = "An operation that extracts, transforms and loads data from a data source into a data destination.";
    public static final String IDENTIFIER_PREFIX = "DSTREAM_";
    public static final String VERSION = "0";
    public enum Datum
    {
        EXECUTION_DIALOG_HEADER_TEXT,
        INPUT_DIALOG_HEADER_TEXT,
        OUTPUT_DIALOG_HEADER_TEXT,
        DEFAULT_PROGRESS_TEXT,
        LOGGING_ENABLED,
        EXECUTE_AS_PROCESS,
        STREAM_PARAMETER_DEFINITIONS,
        INITIALIZATION_NODE_DEFINITIONS,
        ROOT_NODE_DEFINITIONS,
        FINALIZATION_NODE_DEFINITIONS,
        EXCEPTION_ROOT_NODE_DEFINITIONS,
        INPUT_DEFINITIONS,
        OUTPUT_DEFINITIONS,
        RECORD_PERFORMANCE_STATISTICS,
        INITIALIZATION_SCRIPT_DEFINITION,
        FINALIZATION_SCRIPT_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public T8DStreamDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.EXECUTION_DIALOG_HEADER_TEXT.toString(), "Execution Dialog Header",  "The header text that will be displayed on the progress dialog while the Data Strea is executed.", "Processing Data..."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.INPUT_DIALOG_HEADER_TEXT.toString(), "Input Dialog Header",  "The header text that will be displayed on the stream input setup dialog.", "Operation Input Setup"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.OUTPUT_DIALOG_HEADER_TEXT.toString(), "Output Dialog Header",  "The header text that will be displayed on the stream output dialog.", "Operation Output Data"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DEFAULT_PROGRESS_TEXT.toString(), "Default Progress Text",  "The default text that will be displayed on the progress dialog while the Data Strea is executed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOGGING_ENABLED.toString(), "Enable Log Output",  "If this flag is set, a log file will be written and supplied as part of the stream output after execution."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EXECUTE_AS_PROCESS.toString(), "Execute As Process",  "If this flag is set, then the stream will also be executed as a process on the server. Note that processes do not have the capability to provided outputs.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RECORD_PERFORMANCE_STATISTICS.toString(), "Record Performance Statistics",  "If this flag is set, then the stream will record execution statistics and print a report on completion to the default output stream.", false));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STREAM_PARAMETER_DEFINITIONS.toString(), "Stream Data Parameters", "The input data parameters that flow through the stream."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INITIALIZATION_NODE_DEFINITIONS.toString(), "Stream Initialization Nodes",  "The initialization nodes of the Data Stream that will be executed once when the stream is initialized."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ROOT_NODE_DEFINITIONS.toString(), "Stream Root Nodes",  "The root nodes of the Data Stream."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FINALIZATION_NODE_DEFINITIONS.toString(), "Stream Finalization Nodes",  "The finalization nodes of the Data Stream that will be executed once when the stream is finalized."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EXCEPTION_ROOT_NODE_DEFINITIONS.toString(), "Exception Root Nodes",  "The root nodes of the Data Stream exception flow."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_DEFINITIONS.toString(), "Stream Inputs",  "The external inputs that will be used by the Data Stream."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_DEFINITIONS.toString(), "Stream Outputs",  "The outputs that will be created by the Data Stream."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.INITIALIZATION_SCRIPT_DEFINITION.toString(), "Initialization Script", "The script that will be executed when the stream is initialized (before execution).  The stream input parameters are available to this script, and the output parameters of this script will be added to the input parameter collection to be used for stream execution."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FINALIZATION_SCRIPT_DEFINITION.toString(), "Finalization Script", "A script that is executed before DStream execution ends (after all nodes have completed).  The output from this script is added to the stream output parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ROOT_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
        else if (Datum.INITIALIZATION_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
        else if (Datum.FINALIZATION_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
        else if (Datum.EXCEPTION_ROOT_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER));
        else if (Datum.INPUT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamInputDefinition.GROUP_IDENTIFIER));
        else if (Datum.OUTPUT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DStreamOutputDefinition.GROUP_IDENTIFIER));
        else if (Datum.STREAM_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.INITIALIZATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DStreamInitializationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.FINALIZATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DStreamFinalizationScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.STREAM_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        try
        {
            T8DefinitionTestHarness testHarness;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.T8DStreamDefinitionTestHarness").getConstructor();
            testHarness = (T8DefinitionTestHarness)constructor.newInstance();
            return testHarness;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public T8DStream getNewDataStreamInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.T8DefaultDStream").getConstructor(T8Context.class, this.getClass());
        return (T8DStream)constructor.newInstance(context, this);
    }

    public String getExecutionDialogHeaderText()
    {
        return (String)getDefinitionDatum(Datum.EXECUTION_DIALOG_HEADER_TEXT.toString());
    }

    public void setExecutionDialogHeaderText(String text)
    {
        setDefinitionDatum(Datum.EXECUTION_DIALOG_HEADER_TEXT.toString(), text);
    }

    public String getInputDialogHeaderText()
    {
        return (String)getDefinitionDatum(Datum.INPUT_DIALOG_HEADER_TEXT.toString());
    }

    public void setInputDialogHeaderText(String text)
    {
        setDefinitionDatum(Datum.INPUT_DIALOG_HEADER_TEXT.toString(), text);
    }

    public String getOutputDialogHeaderText()
    {
        return (String)getDefinitionDatum(Datum.OUTPUT_DIALOG_HEADER_TEXT.toString());
    }

    public void setOutputDialogHeaderText(String text)
    {
        setDefinitionDatum(Datum.OUTPUT_DIALOG_HEADER_TEXT.toString(), text);
    }

    public String getDefaultProgressText()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_PROGRESS_TEXT.toString());
    }

    public void setDefaultProgressText(String text)
    {
        setDefinitionDatum(Datum.DEFAULT_PROGRESS_TEXT.toString(), text);
    }

    public List<T8DStreamNodeDefinition> getRootNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.ROOT_NODE_DEFINITIONS.toString());
    }

    public void setRootNodeDefinitions(List<T8DStreamNodeDefinition> rootNodeDefinitions)
    {
        setDefinitionDatum(Datum.ROOT_NODE_DEFINITIONS.toString(), rootNodeDefinitions);
    }

    public List<T8DStreamNodeDefinition> getInitializationNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.INITIALIZATION_NODE_DEFINITIONS.toString());
    }

    public void setInitializationNodeDefinitions(List<T8DStreamNodeDefinition> nodeDefinitions)
    {
        setDefinitionDatum(Datum.INITIALIZATION_NODE_DEFINITIONS.toString(), nodeDefinitions);
    }

    public List<T8DStreamNodeDefinition> getFinalizationNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.FINALIZATION_NODE_DEFINITIONS.toString());
    }

    public void setFinalizationNodeDefinitions(List<T8DStreamNodeDefinition> nodeDefinitions)
    {
        setDefinitionDatum(Datum.FINALIZATION_NODE_DEFINITIONS.toString(), nodeDefinitions);
    }

    public List<T8DStreamNodeDefinition> getExceptionRootNodeDefinitions()
    {
        return (List<T8DStreamNodeDefinition>)getDefinitionDatum(Datum.EXCEPTION_ROOT_NODE_DEFINITIONS.toString());
    }

    public void setExceptionRootNodeDefinitions(List<T8DStreamNodeDefinition> rootNodeDefinitions)
    {
        setDefinitionDatum(Datum.EXCEPTION_ROOT_NODE_DEFINITIONS.toString(), rootNodeDefinitions);
    }

    public List<T8DStreamInputDefinition> getInputDefinitions()
    {
        return (List<T8DStreamInputDefinition>)getDefinitionDatum(Datum.INPUT_DEFINITIONS.toString());
    }

    /**
     * Only returns the {@code T8DStreamInputDefinition}s for the type identifier supplied.
     *
     * @param inputDefinitionTypeIdentifier The {@code String} input definition Identifier.
     * @return a {@code List} of {@code T8DStreamInputDefinition}s.
     */
    public List<T8DStreamInputDefinition> getInputDefinitionsByType(String inputDefinitionTypeIdentifier)
    {
        List<T8DStreamInputDefinition> typeDefinitionList;

        typeDefinitionList = new ArrayList<T8DStreamInputDefinition>();

        // Loops throught all the input definitions and checks the types and then only add the matching ones.
        for(T8DStreamInputDefinition inputDefinition : getInputDefinitions())
        {
            T8DefinitionTypeMetaData definitionMetaData;

            definitionMetaData = inputDefinition.getTypeMetaData();
            if (definitionMetaData.getTypeId() != null && definitionMetaData.getTypeId().equals(inputDefinitionTypeIdentifier))
            {
                typeDefinitionList.add(inputDefinition);
            }
        }

        return typeDefinitionList;
    }

    public void setInputDefinitions(List<T8DStreamInputDefinition> inputDefinitions)
    {
        setDefinitionDatum(Datum.INPUT_DEFINITIONS.toString(), inputDefinitions);
    }

    public List<T8DStreamOutputDefinition> getOutputDefinitions()
    {
        return (List<T8DStreamOutputDefinition>)getDefinitionDatum(Datum.OUTPUT_DEFINITIONS.toString());
    }

    public void setOutputDefinitions(List<T8DStreamOutputDefinition> outputDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_DEFINITIONS.toString(), outputDefinitions);
    }

    public List<T8DataParameterDefinition> getStreamParameterDefinitions()
    {
        return (List<T8DataParameterDefinition>)getDefinitionDatum(Datum.STREAM_PARAMETER_DEFINITIONS.toString());
    }

    public void setStreamParameterDefinitions(List<T8DataParameterDefinition> definitions)
    {
        setDefinitionDatum(Datum.STREAM_PARAMETER_DEFINITIONS.toString(), definitions);
    }

    public void addStreamParameterDefinition(T8DataParameterDefinition definition)
    {
        addSubDefinition(Datum.STREAM_PARAMETER_DEFINITIONS.toString(), definition);
    }

    public Map<String, Object> createStreamParameterMap()
    {
        Map<String, Object> parameterMap;

        parameterMap = new HashMap<String, Object>();
        for (T8DataParameterDefinition parameterDefinition : getStreamParameterDefinitions())
        {
            parameterMap.put(parameterDefinition.getIdentifier(), null);
        }

        return parameterMap;
    }

    public boolean isRecordPerformanceStatistics()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.RECORD_PERFORMANCE_STATISTICS.toString());
        return enabled != null && enabled;
    }

    public void setRecordPerformanceStatistics(boolean loggingEnabled)
    {
        setDefinitionDatum(Datum.RECORD_PERFORMANCE_STATISTICS.toString(), loggingEnabled);
    }

    public boolean isLoggingEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.LOGGING_ENABLED.toString());
        return enabled != null && enabled;
    }

    public void setLoggingEnabled(boolean loggingEnabled)
    {
        setDefinitionDatum(Datum.LOGGING_ENABLED.toString(), loggingEnabled);
    }

    public boolean isExecuteAsProcess()
    {
        return (Boolean)getDefinitionDatum(Datum.EXECUTE_AS_PROCESS.toString());
    }

    public void setExecuteAsProcess(boolean loggingEnabled)
    {
        setDefinitionDatum(Datum.EXECUTE_AS_PROCESS.toString(), loggingEnabled);
    }

    public T8DStreamInitializationScriptDefinition getInitializationScriptDefinition()
    {
        return (T8DStreamInitializationScriptDefinition)getDefinitionDatum(Datum.INITIALIZATION_SCRIPT_DEFINITION.toString());
    }

    public void setInitializationScriptDefinition(T8DStreamInitializationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.INITIALIZATION_SCRIPT_DEFINITION.toString(), definition);
    }

    public T8DStreamFinalizationScriptDefinition getFinalizationScriptDefinition()
    {
        return (T8DStreamFinalizationScriptDefinition)getDefinitionDatum(Datum.FINALIZATION_SCRIPT_DEFINITION.toString());
    }

    public void setFinalizationScriptDefinition(T8DStreamFinalizationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.FINALIZATION_SCRIPT_DEFINITION.toString(), definition);
    }
}
