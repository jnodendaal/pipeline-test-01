package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8RecordTagMappingDefinition extends T8RequirementMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_DATA_RECORD_TAG_MAPPING";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8RecordTagMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public abstract boolean isTagApplicable(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception;
    public abstract String getTagIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception;
    public abstract Object getTagValue(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception;
}
