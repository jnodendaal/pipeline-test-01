package com.pilog.t8.definition.dstream.output.excelfile;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8ExcelFileOutputFieldDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_OUTPUT_FILE_EXCEL_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_OUTPUT_FILE_EXCEL_FIELD";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "EXCEL_FIELD_OUT_";
    public static final String DISPLAY_NAME = "Output Excel File Field";
    public static final String DESCRIPTION = "A field contained by an output Excel file.";
    public static final String VERSION = "0";
    public enum Datum { EXCEL_COLUMN_IDENTIFIER,
                        EXCEL_COLUMN_STYLE};
    // -------- Definition Meta-Data -------- //

    // TODO: GBO - We can handle more complex types as well. View link below
    // https://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/BuiltinFormats.html
    public enum ExcelColumnStyle {
        TEXT("@"),
        NUMERIC_NO_DECIMAL("0"),
        NUMERIC_DECIMAL("0.00"),
        GENERAL("General");

        private final String stylePattern;

        private ExcelColumnStyle(String stylePattern)
        {
            this.stylePattern = stylePattern;
        }

        public String getStylePattern()
        {
            return this.stylePattern;
        }
    };

    public T8ExcelFileOutputFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.EXCEL_COLUMN_IDENTIFIER.toString(), "Excel Column", "The name of the excel column to which this field corresponds."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.EXCEL_COLUMN_STYLE.toString(), "Excel Column Style", "The Excel specific style of the excel column to which this field corresponds.", ExcelColumnStyle.GENERAL.toString(), T8DefinitionDatumType.T8DefinitionDatumOptionType.ENUMERATION));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.EXCEL_COLUMN_STYLE.toString().equals(datumIdentifier)) return createStringOptions(ExcelColumnStyle.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getExcelColumnIdentifier()
    {
        return getDefinitionDatum(Datum.EXCEL_COLUMN_IDENTIFIER);
    }

    public void setExcelColumnIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.EXCEL_COLUMN_IDENTIFIER, identifier);
    }

    public ExcelColumnStyle getExcelColumnStyle()
    {
        String style;

        style = getDefinitionDatum(Datum.EXCEL_COLUMN_STYLE);

        return style != null ? ExcelColumnStyle.valueOf(style) : ExcelColumnStyle.GENERAL;
    }

    public void getExcelColumnStyle(ExcelColumnStyle style)
    {
        setDefinitionDatum(Datum.EXCEL_COLUMN_STYLE, style.toString());
    }
}
