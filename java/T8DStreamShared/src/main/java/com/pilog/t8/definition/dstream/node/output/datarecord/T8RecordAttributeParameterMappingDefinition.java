package com.pilog.t8.definition.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Willem de Bruyn
 */
public class T8RecordAttributeParameterMappingDefinition extends T8RecordAttributeMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_DATA_RECORD_ATTRIBUTE_PARAMETER_MAPPING";
    public static final String DISPLAY_NAME = "Record Attribute Parameter Mapping";
    public static final String DESCRIPTION = "A mapping of an input parameter to a specific attribute that will be added to a Data Record.";
    public static final String VERSION = "0";
    public enum Datum {ATTRIBUTE_IDENTIFIER,
                       SOURCE_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //
    
    public T8RecordAttributeParameterMappingDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, T8RecordAttributeParameterMappingDefinition.Datum.ATTRIBUTE_IDENTIFIER.toString(), "Attribute Identifier", "The unique identifier of the Data Requirement Class to which this mapping applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, T8RecordAttributeParameterMappingDefinition.Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), "Source Parameter", "The stream parameter mapped to this attribute."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8RecordAttributeParameterMappingDefinition.Datum.SOURCE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public boolean isAttributeApplicable(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception 
    {
        // This attribute type is always applicable.
        return true;
    }
    
     @Override
    public Object getAttributeValue(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters)
    {
        return dataParameters.get(getSourceParameterIdentifier());
    }
    
    public String getSourceParameterIdentifier()
    {
        return (String)getDefinitionDatum(T8RecordAttributeParameterMappingDefinition.Datum.SOURCE_PARAMETER_IDENTIFIER.toString());
    }

    public void setSourceParameterIdentifier(String identifier)
    {
        setDefinitionDatum(T8RecordAttributeParameterMappingDefinition.Datum.SOURCE_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    @Override
    public String getAttributeIdentifier(T8ServerContext serverContext, T8SessionContext sessionContext, Map<String, Object> dataParameters) throws Exception
    {
        return getAttributeIdentifier();
    }

    public String getAttributeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ATTRIBUTE_IDENTIFIER.toString());
    }
    
    public void setAttributeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ATTRIBUTE_IDENTIFIER.toString(), identifier);
    }
    
}
