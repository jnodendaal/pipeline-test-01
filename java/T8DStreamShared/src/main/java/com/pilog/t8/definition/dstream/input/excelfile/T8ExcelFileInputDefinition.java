package com.pilog.t8.definition.dstream.input.excelfile;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.input.T8DStreamInputFileDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileInputDefinition extends T8DStreamInputFileDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_FILE_EXCEL";
    public static final String IDENTIFIER_PREFIX = "INPUT_EXCEL_";
    public static final String DISPLAY_NAME = "Excel Input File";
    public static final String DESCRIPTION = "An Excel file that will be used as input to the data stream.";
    public enum Datum
    {
        TRIM_STRINGS,
        CONVERT_EMPTY_STRINGS_TO_NULL,
        SHEET_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileInputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TRIM_STRINGS.toString(), "Trim Values",  "A boolean value indicating whether or not input String values should be trimmed (whitespace removed from start and end).", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CONVERT_EMPTY_STRINGS_TO_NULL.toString(), "Convert Empty Strings to Null",  "A boolean value indicating whether or not input String values with no content should be set to null.", true));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SHEET_DEFINITIONS.toString(), "Sheets",  "The sheets contained by the Excel file."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SHEET_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ExcelFileInputSheetDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        try
        {
            T8DefinitionActionHandler actionHandler;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.actionhandlers.dstream.T8ExcelFileInputActionHandler").getConstructor(T8Context.class, this.getClass());
            actionHandler = (T8DefinitionActionHandler)constructor.newInstance(context, this);
            return actionHandler;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DStreamFileHandler getFileHandler(T8Context context, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.dstream.file.T8ExcelInputFileHandler").getConstructor(T8Context.class, this.getClass(), T8FileManager.class, T8DStreamFileContext.class);
        return (T8DStreamFileHandler)constructor.newInstance(context, this, fileManager, fileContext);
    }

    public List<T8ExcelFileInputSheetDefinition> getSheetDefinitions()
    {
        return (List<T8ExcelFileInputSheetDefinition>)getDefinitionDatum(Datum.SHEET_DEFINITIONS.toString());
    }

    public T8ExcelFileInputSheetDefinition getSheetDefinition(String identifier)
    {
        for (T8ExcelFileInputSheetDefinition sheetDefinition : getSheetDefinitions())
        {
            if (sheetDefinition.getIdentifier().equals(identifier)) return sheetDefinition;
        }

        return null;
    }

    public void setSheetDefinitions(List<T8ExcelFileInputSheetDefinition> definitions)
    {
        setDefinitionDatum(Datum.SHEET_DEFINITIONS.toString(), definitions);
    }

    public void addSheetDefinition(T8ExcelFileInputSheetDefinition sheetDefinition)
    {
        addSubDefinition(Datum.SHEET_DEFINITIONS.toString(), sheetDefinition);
    }

    public boolean trimStrings()
    {
        Boolean trimStrings;

        trimStrings = (Boolean)getDefinitionDatum(Datum.TRIM_STRINGS.toString());
        return trimStrings != null && trimStrings;
    }

    public void setTrimStrings(boolean trimStrings)
    {
        setDefinitionDatum(Datum.TRIM_STRINGS.toString(), trimStrings);
    }

    public boolean convertEmptyStringsToNull()
    {
        Boolean trimStrings;

        trimStrings = (Boolean)getDefinitionDatum(Datum.CONVERT_EMPTY_STRINGS_TO_NULL.toString());
        return trimStrings != null && trimStrings;
    }

    public void setConvertEmptyStringsToNull(boolean convertEmptyStringsToNull)
    {
        setDefinitionDatum(Datum.CONVERT_EMPTY_STRINGS_TO_NULL.toString(), convertEmptyStringsToNull);
    }
}
