package com.pilog.t8.definition.dstream.input;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextFieldInputDefinition extends T8DStreamInputDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_TEXT_FIELD";
    public static final String DISPLAY_NAME = "Text Field";
    public static final String DESCRIPTION = "A text field allowing the user to enter a value to be used as input to the data stream.";
    public static final String VERSION = "0";
    public enum Datum {LABEL_TEXT, STREAM_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8TextFieldInputDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.LABEL_TEXT.toString(), "Label Text", "The label that will be displayed next to the text field on the Stream input dialog."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.STREAM_PARAMETER_IDENTIFIER.toString(), "Source Parameter", "The stream parameter to which the value entered into the text field will be mapped."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.STREAM_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createStreamParameterOptions(false);
        }
        else return null;
    }

    @Override
    public T8DStreamInputPanel getNewStreamInputPanelInstance(T8ComponentController controller)
    {
        try
        {
            T8DStreamInputPanel inputPanel;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.dstream.input.T8TextFieldInputPanel").getConstructor(this.getClass());
            inputPanel = (T8DStreamInputPanel)constructor.newInstance(this);
            return inputPanel;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public String getLabelText()
    {
        return (String)getDefinitionDatum(Datum.LABEL_TEXT.toString());
    }

    public void setLabelText(String text)
    {
        setDefinitionDatum(Datum.LABEL_TEXT.toString(), text);
    }

    public String getStreamParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.STREAM_PARAMETER_IDENTIFIER.toString());
    }

    public void setStreamParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.STREAM_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
