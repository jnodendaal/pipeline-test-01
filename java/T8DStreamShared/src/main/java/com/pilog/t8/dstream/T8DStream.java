package com.pilog.t8.dstream;

import com.pilog.t8.dstream.exception.T8DStreamException;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public interface T8DStream
{
    // Parameters used for exception flow.
    public static final String STREAM_PARAMETER_EXCEPTION_NODE_NAME = "$P_EXCEPTION_NODE_NAME";
    public static final String STREAM_PARAMETER_EXCEPTION_CAUSE = "$P_EXCEPTION_CAUSE";
    public static final String STREAM_PARAMETER_EXCEPTION_ROOT_CAUSE = "$P_EXCEPTION_ROOT_CAUSE";
    public static final String STREAM_PARAMETER_EXCEPTION_ROOT_CAUSE_MESSAGE = "$P_EXCEPTION_ROOT_CAUSE_MESSAGE";
    public static final String STREAM_PARAMETER_EXCEPTION_DETAIL_MESSAGE = "$P_EXCEPTION_DETAIL_MESSAGE";
    public static final String STREAM_PARAMETER_EXCEPTION_USER_MESSAGE = "$P_EXCEPTION_USER_MESSAGE";

    public void execute(T8DataTransaction tx, HashMap<String, Object> inputParameters, T8DStreamFileContext context) throws T8DStreamException, Exception;
    public boolean hasExceptionFlow();
    public void setVariable(String identifier, Object value);
    public Object getVariable(String identifier);
    public void processExceptionFlow(Throwable cause, String detailMessage, String userMessage, String nodeName, Map<String, Object> dataRow) throws T8DStreamException;
    public T8DStreamExecutionProgress getExecutionProgress();
    public double getOverallExecutionProgress();
    public void stopExecution();
    public T8DStreamLogFile getLogFile();
    public T8DStreamFileHandler getInputFileHandler(String inputIdentifier) throws Exception;
    public T8DStreamFileHandler getOutputFileHandler(String ouputIdentifier) throws Exception;
}
