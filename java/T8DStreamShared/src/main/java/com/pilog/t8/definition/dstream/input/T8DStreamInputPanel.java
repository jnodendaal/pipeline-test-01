package com.pilog.t8.definition.dstream.input;

import com.pilog.t8.dstream.T8DStreamFileContext;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DStreamInputPanel
{
    public void initialize(T8DStreamFileContext fileContext, Map<String, Object> inputParameters);
    public void destroy();
    public boolean validateInput();
    public Map<String, Object> getInputData();
    public void addInputListener(T8DStreamInputPanelListener listener);
    public void removeInputListener(T8DStreamInputPanelListener listener);
}
