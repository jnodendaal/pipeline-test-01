package com.pilog.t8.definition.dstream.node;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DStreamInputNodeDefinition extends T8DStreamNodeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_INPUT_NODE";
    public static final String STORAGE_PATH = null;
    public enum Datum {PROGRESS_TEXT,
                       COUNT_ROWS};
    // -------- Definition Meta-Data -------- //
    
    public T8DStreamInputNodeDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COUNT_ROWS.toString(), "Count Rows",  "A boolean value indicating whether or not the available input rows should be counted before processing in order to determine accurate progress."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PROGRESS_TEXT.toString(), "Progress Text",  "The default text that will be displayed on the progress dialog while the node is executed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    public String getProgressText()
    {
        return (String)getDefinitionDatum(Datum.PROGRESS_TEXT.toString());
    }
    
    public void setProgressText(String text)
    {
        setDefinitionDatum(Datum.PROGRESS_TEXT.toString(), text);
    }
    
    public boolean countRows()
    {
        Boolean countRows;
        
        countRows = (Boolean)getDefinitionDatum(Datum.COUNT_ROWS.toString());
        return countRows != null && countRows;
    }
    
    public void setCountRows(boolean countRows)
    {
        setDefinitionDatum(Datum.COUNT_ROWS.toString(), countRows);
    }    
}
