package com.pilog.t8.definition.dstream.input.excelfile;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8ExcelFileInputFieldDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_INPUT_FILE_EXCEL_FIELD";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_STREAM_INPUT_FILE_EXCEL_FIELD";
    public static final String IDENTIFIER_PREFIX = "EXCEL_FIELD_IN_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Input Excel File Field";
    public static final String DESCRIPTION = "A field contained by an input Excel file.";
    public enum Datum {EXCEL_COLUMN_IDENTIFIER,
                       DATA_TYPE,
                       REQUIRED};
    // -------- Definition Meta-Data -------- //

    public T8ExcelFileInputFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        // Get the super class datum types.
        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.EXCEL_COLUMN_IDENTIFIER.toString(), "Excel Column", "The name of the Excel column to which this field corresponds."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_TYPE.toString(), "Data Type", "The data type of this field."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REQUIRED.toString(), "Required",  "A setting to indicate whether this field is required for successful execution or optional.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public boolean isRequired()
    {
        Boolean required;

        required = (Boolean)getDefinitionDatum(Datum.REQUIRED.toString());
        return required != null && required;
    }

    public void setRequired(boolean required)
    {
        setDefinitionDatum(Datum.REQUIRED.toString(), required);
    }
    
    public String getExcelColumnIdentifier()
    {
        return (String)getDefinitionDatum(Datum.EXCEL_COLUMN_IDENTIFIER.toString());
    }
    
    public void setExcelColumnIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.EXCEL_COLUMN_IDENTIFIER.toString(), identifier);
    }    
    
    public String getDataTypeString()
    {
        return (String)getDefinitionDatum(Datum.DATA_TYPE.toString());
    }

    public void setDataTypeString(String dataTypeString)
    {
        setDefinitionDatum(Datum.DATA_TYPE.toString(), dataTypeString);
    }    
}
