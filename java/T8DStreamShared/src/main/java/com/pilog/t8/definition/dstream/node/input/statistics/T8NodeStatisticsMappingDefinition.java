package com.pilog.t8.definition.dstream.node.input.statistics;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamTransformationNodeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NodeStatisticsMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DT_DATA_STREAM_NODE_STATISTICS_MAPPING";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_STREAM_NODE_STATISTICS_MAPPING";
    public static final String DISPLAY_NAME = "Node Statistics Mapping";
    public static final String DESCRIPTION = "A mapping of node statistics to stream parameters.";
    public enum Datum {NODE_IDENTIFIER,
                       INPUT_ROW_COUNT_PARAMETER_IDENTIFIER,
                       OUTPUT_ROW_COUNT_PARAMETER_IDENTIFIER,
                       EXCEPTION_ROW_COUNT_PARAMETER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8NodeStatisticsMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NODE_IDENTIFIER.toString(), "Node",  "The node from which the statistics will be read."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString(), "Input Row Count Parameter",  "The stream parameter to which the input row count statistic is mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.OUTPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString(), "Output Row Count Parameter",  "The stream parameter to which the output row count statistic is mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.EXCEPTION_ROW_COUNT_PARAMETER_IDENTIFIER.toString(), "Exception Row Count Parameter",  "The stream parameter to which the exception row count statistic is mapped."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.NODE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = new ArrayList<T8DefinitionDatumOption>();
            options.addAll(createLocalIdentifierOptionsFromDefinitions(getLocalDefinitionsOfGroup(T8DStreamInputNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createLocalIdentifierOptionsFromDefinitions(getLocalDefinitionsOfGroup(T8DStreamOutputNodeDefinition.GROUP_IDENTIFIER)));
            options.addAll(createLocalIdentifierOptionsFromDefinitions(getLocalDefinitionsOfGroup(T8DStreamTransformationNodeDefinition.GROUP_IDENTIFIER)));
            return options;
        }
        else if (Datum.INPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.OUTPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.EXCEPTION_ROW_COUNT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return T8DStreamNodeDefinition.createStreamParameterOptions((T8DStreamDefinition)getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public String getNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.NODE_IDENTIFIER.toString());
    }

    public void setNodeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.NODE_IDENTIFIER.toString(), identifier);
    }
    
    public String getInputRowCountParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString());
    }

    public void setInputRowCountParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getOutputRowCountParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.OUTPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString());
    }

    public void setOutputRowCountParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OUTPUT_ROW_COUNT_PARAMETER_IDENTIFIER.toString(), identifier);
    }
    
    public String getExceptionRowCountParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.EXCEPTION_ROW_COUNT_PARAMETER_IDENTIFIER.toString());
    }

    public void setExceptionRowCountParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.EXCEPTION_ROW_COUNT_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
