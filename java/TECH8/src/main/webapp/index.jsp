<!--This file will be set as the welcome page when redirecting to the Home Page which is a central WAR.-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <script>
        window.open('../<%=application.getInitParameter("homepage")%>', '_self');
    </script>
</html>
