// Extended .find(selector) so that ID's containing '$' characters are automatically escaped.
//(function ($)
//{
//    var originalFind = $.fn.find;
//    $.fn.find = function (selector)
//    {
//        return originalFind.call(this, selector.replace(/[$]/g, '\\$&'));
//    };
//})(jQuery);

function element(elementIdentifier)
{
    return $('#' + elementIdentifier.substring(1));
}

/**
 * Removes all child nodes from the supplied element.
 * @param element The element to clear.
 */
function clearElement(element)
{
    while (element.firstChild)
    {
        element.removeChild(element.firstChild);
    }
}

/**
 * Initializes the supplied data record and all of its descendants.  
 * Initialization sets various references on the constituent parts of the 
 * document so that traversal can be performed more efficiently.
 * @param parentRecord The parent data record of this record.
 * @param inputRecord The data record to initialize.
 */
function DataRecord(parentRecord, inputRecord)
{
    var inputSubRecords;
    var inputSections;
    
    // Initialize this instance.
    this.parentRecord = parentRecord;
    this.recordID = inputRecord.recordID;
    this.drInstance = new DataRequirementInstance(inputRecord.drInstance);
    this.sections = [];
    this.subRecords = [];
    
    // Initialize sections.
    inputSections = inputRecord.sections;
    if (inputSections)
    {
        for (var sectionIndex = 0; sectionIndex < inputSections.length; sectionIndex++)
        {
            var inputSection;
            var section;

            // Get the section and initialize it.
            inputSection = inputSections[sectionIndex];
            section = new RecordSection(this, inputSection.sectionID, inputSection);
            this.sections.push(section);
        }
    }

    // Initialize sub-records.
    inputSubRecords = inputRecord.subRecords;
    if (inputSubRecords)
    {
        for (var subRecordIndex = 0; subRecordIndex < inputSubRecords.length; subRecordIndex++)
        {
            var inputSubRecord;
            var subRecord;

            inputSubRecord = inputSubRecords[subRecordIndex];
            subRecord = new DataRecord(this, inputSubRecord);
            this.subRecords.push(subRecord);
        }
    }
}

/**
 * Serializes the supplied data record object using JSON.stringify().  This
 * function safely avoids cyclic references created by the initializeDataRecord
 * function.
 * @returns The string represenation of the supplied data record.
 */
DataRecord.prototype.serialize = function()
{
    return JSON.stringify(this, function (key, val)
    {
        switch (key)
        {
            case "parentInstance":
                return;
            case "parentRecord":
                return;
            case "parentSection":
                return;
            case "parentProperty":
                return;
            default:
                return val;
        }
    });
};

/**
 * Returns the value string of this record's content.
 * @returns The value string of this record's content.
 */
DataRecord.prototype.getValueString = function()
{
    var valueString;
    var sections;
    
    // Create the new value string for this record.
    valueString = this.drInstance.term;
    valueString += ": ";
    
    // Append all non-empty section value strings.
    sections = this.sections;
    if (sections)
    {
        for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++)
        {
            var sectionValueString;
            var section;

            // Get the section value string and append it if not empty.
            section = sections[sectionIndex];
            sectionValueString = section.getValueString();
            if (sectionValueString)
            {
                valueString += sectionValueString;
            }
        }
    }
    
    // Return the completed value string.
    return valueString;
};

/**
 * Creates an array containing the records from the root record (index 0) to
 * this record (last index in array).
 * @returns An array containing the records from the root record (index 0) to
 * this record (last index in array).
 */
DataRecord.prototype.getPathRecords = function()
{
    var recordList;
    var record;
    
    recordList = [];
    record = this;
    while (record)
    {
        recordList.unshift(record);
        record = record.parentRecord;
    }
    
    return recordList;
};

/**
 * Searches the supplied object model for the specified data record object.
 * @param recordID The ID of the record to find.
 * @returns The data record object with the specified ID or null if it was not
 *  found.
 */
DataRecord.prototype.getRecord = function(recordID)
{
    if (this.recordID === recordID)
    {
        return this;
    }
    else
    {
        var subRecords;

        subRecords = this.subRecords;
        if (subRecords)
        {
            for (var subRecordIndex = 0; subRecordIndex < subRecords.length; subRecordIndex++)
            {
                var subRecord;
                var foundRecord;

                subRecord = subRecords[subRecordIndex];
                foundRecord = subRecord.getRecord(recordID);
                if (foundRecord) return foundRecord;
            }

            return null;
        }
        else return null;
    }
};

/**
 * Returns the specified section from the data record object or null if the
 * section does not exist.
 * @param sectionID The ID of the section to find.
 */
DataRecord.prototype.getSection = function(sectionID)
{
    var sections;
    
    sections = this.sections;
    if (sections)
    {
        for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++)
        {
            var section;

            section = sections[sectionIndex];
            if (section.sectionID === sectionID)
            {
                return section;
            }
        }

        return null;
    }
    else return null;
};

/**
 * Returns the specified property from the data record object or null if the
 * property does not exist.
 * @param propertyID The ID of the property to find.
 */
DataRecord.prototype.getProperty = function(propertyID)
{
    var sections;
    
    sections = this.sections;
    if (sections)
    {
        for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++)
        {
            var section;
            var properties;

            section = sections[sectionIndex];
            properties = section.properties;
            if (properties)
            {
                for (var propertyIndex = 0; propertyIndex < properties.length; propertyIndex++)
                {
                    var property;
                    
                    property = properties[propertyIndex];
                    if (property.propertyID === propertyID)
                    {
                        return property;
                    }
                }
            }
            else return null;
        }

        return null;
    }
    else return null;
};

DataRecord.prototype.getField = function(propertyID, fieldID)
{
    var property;
    
    // Get the property from the record.
    property = this.getProperty(propertyID);
    if (property)
    {
        var composite;
        
        // Get the composite value.
        composite = property.value;
        if (composite)
        {
            var fields;

            // Get all fields from the composite.
            fields = composite.fields;
            for (var fieldIndex = 0; fieldIndex < fields.length; fieldIndex++)
            {
                var field;
                
                field = fields[fieldIndex];
                if (field.fieldID === fieldID)
                {
                    return field;
                }
            }
        }

        return null;
    }
    else return null;
};

DataRecord.prototype.getValue = function(propertyID, fieldID)
{
    var property;
    
    property = this.getProperty(propertyID);
    if (property)
    {
        if (fieldID)
        {
            var field;
            
            field = getField(property, fieldID);
            if (field)
            {
                return field.value;
            }
            else return null;
        }
        else return property.value;
    }
    else return null;
};

/**
 * Creates the specified section in this data record object.  If the specified
 * section already exists, it is replaced with the new section.
 * @param sectionID The ID of the new section to create.
 * @returns The newly created section object.
 */
DataRecord.prototype.createSection = function(sectionID)
{
    var sectionRequirement;
    
    sectionRequirement = this.drInstance.getSectionRequirement(sectionID);
    if (sectionRequirement)
    {
        // First make sure we find the section requirement.
        var sectionIndex;
        var newSection;
        var sections;

        // Check existing sections for the specified ID.
        sections = this.sections;
        for (sectionIndex = 0; sectionIndex < sections.length; sectionIndex++)
        {
            if (sections[sectionIndex].sectionID === sectionID)
            {
                break;
            }
        }

        // Create the new section.
        newSection = new RecordSection(this, sectionID, null);

        // Replace the existing section if it was found, or add a new section.
        if (sectionIndex < sections.length)
        {
            sections[sectionIndex] = newSection;
        }
        else
        {
            sections.push(newSection);
        }

        // Return the new section.
        return newSection;
    }
    else throw "Section Requirement not found: " + sectionID;
};

/**
 * Returns the specified section from this data record object or creates the
 * section if it does not exist and then returns the new object.
 * @param sectionID The ID of the section to get/create.
 * @returns The specified section in this data record.
 */
DataRecord.prototype.ensureSection = function(sectionID)
{
    var section;
    
    section = this.getSection(sectionID);
    if (section)
    {
        return section;
    }
    else
    {
        return this.createSection(sectionID);
    }
};

/**
 * Creates the specified property in this data record object.  If the specified
 * property already exists, it is replaced with the new property.
 * @param propertyID The ID of the new property to create.
 * @returns The newly created property object.
 */
DataRecord.prototype.createProperty = function(propertyID)
{
    var propertyRequirement;
    
    // First make sure that we find the property requirement before adding it.
    propertyRequirement = this.drInstance.getPropertyRequirement(propertyID);
    if (propertyRequirement)
    {
        var section;
        var propertyIndex;
        var newProperty;
        var properties;
        var section;
        
        // Get the section requirement to which the property belongs.
        section = this.ensureSection(propertyRequirement.parentSection.sectionID);

        // Check existing properties for the specified ID.
        properties = section.properties;
        for (propertyIndex = 0; propertyIndex < properties.length; propertyIndex++)
        {
            if (properties[propertyIndex].propertyID === propertyID)
            {
                break;
            }
        }

        // Create the new property.
        newProperty = new RecordProperty(this, propertyID, null);

        // Replace the existing property if it was found, or add a new property.
        if (propertyIndex < properties.length)
        {
            properties[propertyIndex] = newProperty;
        }
        else
        {
            properties.push(newProperty);
        }

        // Return the new property.
        return newProperty;
    }
    else throw "Property Requirement not found: " + propertyID;
};

/**
 * Returns the specified property from this data record object or creates the
 * property if it does not exist and then returns the new object.
 * @param propertyID The ID of the property to get/create.
 * @returns The specified property in this data record.
 */
DataRecord.prototype.ensureProperty = function(propertyID)
{
    var property;
    
    property = this.getProperty(propertyID);
    if (property)
    {
        return property;
    }
    else
    {
        return this.createProperty(propertyID);
    }
};

DataRecord.prototype.setStringValue = function(propertyID, fieldID, stringValue)
{
    var property;
    
    // Set the new value of the property.
    property = this.ensureProperty(propertyID);
    property.value =
    {
        "typeID":"STRING",
        "value":stringValue
    };
};

function RecordSection(parentRecord, sectionID, inputSection)
{
    var inputProperties;
    
    // Initialize this instance.
    this.parentRecord = parentRecord;
    this.sectionID = sectionID;
    this.properties = [];
    
    // Initialize the properties.
    if (inputSection)
    {
        inputProperties = inputSection.properties;
        if (inputProperties)
        {
            for (var propertyIndex = 0; propertyIndex < inputProperties.length; propertyIndex++)
            {
                var inputProperty;
                var property;

                // Get the property and initialize it.
                inputProperty = inputProperties[propertyIndex];
                property = new RecordProperty(this, inputProperty.propertyID, inputProperty);
                this.properties.push(property);
            }
        }
    }
}

/**
 * Returns the value string of this section's content.
 * @returns The value string of this section's content.
 */
RecordSection.prototype.getValueString = function()
{
    var valueString;
    var properties;
    var appendSeparator;
    
    // Create the new value string for this section.
    appendSeparator = false;
    valueString = "";
    
    // Append all non-empty property value strings.
    properties = this.properties;
    if (properties)
    {
        for (var propertyIndex = 0; propertyIndex < properties.length; propertyIndex++)
        {
            var propertyValueString;
            var property;

            // Get the property value string and append it if not empty.
            property = properties[propertyIndex];
            propertyValueString = property.getValueString();
            if (propertyValueString)
            {
                if (appendSeparator) valueString += ",";
                valueString += propertyValueString;
                appendSeparator = true; // Since we've added a value to the string, subsequent values must be separated.
            }
        }
    }
    
    // Return the completed value string.
    return valueString;
};

function RecordProperty(parentSection, propertyID, inputProperty)
{
    // Initialize this instance.
    this.parentSection = parentSection;
    this.propertyID = propertyID;
    
    // Set the input value according to the input property.
    if (inputProperty)
    {
        this.value = inputProperty.value;
    }
}

/**
 * Returns the value string of this property's content.
 * @returns The value string of this property's content.
 */
RecordProperty.prototype.getValueString = function()
{
    var value;
    
    // Append all non-empty property value strings.
    value = this.value;
    if (value)
    {
        // TODO:  Implement proper value string generation here.  This method
        // will currently only work for simple data types where value.value
        // contains a string.
        return value.value;
    }
    else return null;
};

function DataRequirementInstance(inputInstance)
{
    var inputSections;
    
    // Initialize this instance.
    this.drInstanceID = inputInstance.drInstanceID;
    this.drID = inputInstance.drID;
    this.term = inputInstance.term;
    this.independent = inputInstance.independent;
    this.sections = [];
    
    // Initialize sections.
    inputSections = inputInstance.sections;
    if (inputSections)
    {
        for (var sectionIndex = 0; sectionIndex < inputSections.length; sectionIndex++)
        {
            var inputSection;
            var section;

            // Get the section and initialize it.
            inputSection = inputSections[sectionIndex];
            section = new SectionRequirement(this, inputSection);
            this.sections.push(section);
        }
    }
}

/**
 * Returns the specified section requirement from the instance object or null if
 * the section requirement does not exist.
 * @param sectionID The ID of the section requirement to find.
 */
DataRequirementInstance.prototype.getSectionRequirement = function(sectionID)
{
    var sections;
    
    sections = this.sections;
    if (sections)
    {
        for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++)
        {
            var section;

            section = sections[sectionIndex];
            if (section.sectionID === sectionID)
            {
                return section;
            }
        }

        return null;
    }
    else return null;
};

/**
 * Returns the specified property requirement from the instance object or null 
 * if the property does not exist.
 * @param propertyID The ID of the property requirement to find.
 */
DataRequirementInstance.prototype.getPropertyRequirement = function(propertyID)
{
    var sections;
    
    sections = this.sections;
    if (sections)
    {
        for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++)
        {
            var section;
            var properties;

            section = sections[sectionIndex];
            properties = section.properties;
            if (properties)
            {
                for (var propertyIndex = 0; propertyIndex < properties.length; propertyIndex++)
                {
                    var property;
                    
                    property = properties[propertyIndex];
                    if (property.propertyID === propertyID)
                    {
                        return property;
                    }
                }
            }
            else return null;
        }

        return null;
    }
    else return null;
};

DataRequirementInstance.prototype.getFieldRequirement = function(propertyID, fieldID)
{
    var property;
    
    // Get the property from the record.
    property = this.getPropertyRequirement(propertyID);
    if (property)
    {
        var composite;
        
        // Get the composite value.
        composite = property.value;
        if (composite)
        {
            var fields;

            // Get all fields from the composite.
            fields = composite.fields;
            for (var fieldIndex = 0; fieldIndex < fields.length; fieldIndex++)
            {
                var field;
                
                field = fields[fieldIndex];
                if (field.fieldID === fieldID)
                {
                    return field;
                }
            }
        }

        return null;
    }
    else return null;
};

DataRequirementInstance.prototype.getValueRequirement = function(propertyID, fieldID)
{
    var property;
    
    property = this.getPropertyRequirement(propertyID);
    if (property)
    {
        if (fieldID)
        {
            var field;
            
            field = this.getFieldRequirement(property, fieldID);
            if (field)
            {
                return field.value;
            }
            else return null;
        }
        else return property.value;
    }
    else return null;
};

function SectionRequirement(parentInstance, inputSection)
{
    var inputProperties;
    
    // Initialize this instance.
    this.parentInstance = parentInstance;
    this.sectionID = inputSection.sectionID;
    this.term = inputSection.term;
    this.properties = [];
    
    // Initialize the properties.
    inputProperties = inputSection.properties;
    if (inputProperties)
    {
        for (var propertyIndex = 0; propertyIndex < inputProperties.length; propertyIndex++)
        {
            var inputProperty;
            var property;

            // Get the property and initialize it.
            inputProperty = inputProperties[propertyIndex];
            property = new PropertyRequirement(this, inputProperty);
            this.properties.push(property);
        }
    }
}

function PropertyRequirement(parentSection, inputProperty)
{
    var inputValue;
    
    // Initialize this instance.
    this.parentSection = parentSection;
    this.propertyID = inputProperty.propertyID;
    this.term = inputProperty.term;
    this.value = inputProperty.value;
}