<?xml version="1.0" encoding="UTF-8"?>
<!--
$Id: terminology-service-dictionary.sch 441 2009-08-24 18:15:36Z radack $
ISO TC 184/SC 4/WG 12 N6767 - ISO/TS 22745-10 Terminology service dictionary restriction for ISO 22745 - Schematron schema
-->
<!--
The following permission notice and disclaimer shall be included in all copies of this Schematron schema ("the Schema"), and derivations of the Schema:

Permission is hereby granted, free of charge in perpetuity, to any person obtaining a copy of the Schema, to use, copy, modify, merge and distribute free of charge, copies of the Schema for the purposes of developing, implementing, installing and using software based on the  Schema, and to permit persons to whom the Schema is furnished to do so, subject to the following conditions:

THE SCHEMA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN THE SCHEMA.

In addition, any modified copy of the Schema shall include the following notice:

THIS SCHEMA HAS BEEN MODIFIED FROM THE SCHEMA DEFINED IN ISO 22745-10, AND SHOULD NOT BE INTERPRETED AS COMPLYING WITH THAT STANDARD.

-->
<svrl:schema xmlns:svrl="http://purl.oclc.org/dsdl/schematron" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ascc.net/xml/schematron schematron1-5.xsd">
	<svrl:ns uri="urn:iso:std:iso:ts:29002:-6:ed-1:tech:xml-schema:terminology-service-dictionary" prefix="tsd"/>
	<svrl:phase>
		<svrl:pattern name="ID pattern">
			<!-- tsd:language must have the id attribute -->
			<svrl:rule context="tsd:language">
				<svrl:assert test="@id">the id attribute should be present</svrl:assert>
			</svrl:rule>
			<!-- tsd:terminological_item must have the id attribute -->
			<svrl:rule context="tsd:definition">
				<svrl:assert test="@id">the id attribute should be present</svrl:assert>
			</svrl:rule>
			<svrl:rule context="tsd:term">
				<svrl:assert test="@id">the id attribute should be present</svrl:assert>
			</svrl:rule>
			<svrl:rule context="tsd:abbreviation">
				<svrl:assert test="@id">the id attribute should be present</svrl:assert>
			</svrl:rule>
			<svrl:rule context="tsd:symbol">
				<svrl:assert test="@id">the id attribute should be present</svrl:assert>
			</svrl:rule>
			<svrl:rule context="tsd:image">
				<svrl:assert test="@id">the id attribute should be present</svrl:assert>
			</svrl:rule>
		</svrl:pattern>
	</svrl:phase>
</svrl:schema>
