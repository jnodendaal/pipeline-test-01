<?xml version="1.0" encoding="UTF-8"?>
<!--
$Id: terminology-service-dictionary.xsd 411 2009-07-21 01:57:47Z radack $
ISO TC 184/SC 4/WG 12 N6651 - ISO/TS 29002-6 Terminology service dictionary - XML schema
-->
<!--
The following permission notice and disclaimer shall be included in all copies of this XML schema ("the Schema"), and derivations of the Schema:

Permission is hereby granted, free of charge in perpetuity, to any person obtaining a copy of the Schema, to use, copy, modify, merge and distribute free of charge, copies of the Schema for the purposes of developing, implementing, installing and using software based on the  Schema, and to permit persons to whom the Schema is furnished to do so, subject to the following conditions:

THE SCHEMA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN THE SCHEMA.

In addition, any modified copy of the Schema shall include the following notice:

THIS SCHEMA HAS BEEN MODIFIED FROM THE SCHEMA DEFINED IN ISO 29002-6, AND SHOULD NOT BE INTERPRETED AS COMPLYING WITH THAT STANDARD.

-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tsd="urn:iso:std:iso:ts:29002:-6:ed-1:tech:xml-schema:terminology-service-dictionary" xmlns:basic="urn:iso:std:iso:ts:29002:-4:ed-1:tech:xml-schema:basic" xmlns:id="urn:iso:std:iso:ts:29002:-5:ed-1:tech:xml-schema:identifier" targetNamespace="urn:iso:std:iso:ts:29002:-6:ed-1:tech:xml-schema:terminology-service-dictionary" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:import namespace="urn:iso:std:iso:ts:29002:-5:ed-1:tech:xml-schema:identifier" schemaLocation="identifier.xsd"/>
	<xs:import namespace="urn:iso:std:iso:ts:29002:-4:ed-1:tech:xml-schema:basic" schemaLocation="basic.xsd"/>
	<!-- Elements -->
	<xs:element name="abbreviation" type="tsd:abbreviation_Type"/>
	<xs:element name="concept" type="tsd:concept_Type"/>
	<xs:element name="concept_equivalence_relationship" type="tsd:concept_equivalence_relationship_Type"/>
	<xs:element name="concept_type" type="tsd:concept_type_Type"/>
	<xs:element name="definition" type="tsd:definition_Type"/>
	<xs:element name="document" type="tsd:document_Type"/>
	<xs:element name="file_representation" type="tsd:file_representation_Type"/>
	<xs:element name="image" type="tsd:image_Type"/>
	<xs:element name="language" type="tsd:language_Type"/>
	<xs:element name="organization" type="tsd:organization_Type"/>
	<xs:element name="source_location" type="tsd:source_location_Type"/>
	<xs:element name="symbol" type="tsd:symbol_Type"/>
	<xs:element name="term" type="tsd:term_Type"/>
	<!-- Types -->
	<!-- Abbreviation-->
	<xs:complexType name="abbreviation_Type">
		<xs:complexContent>
			<xs:extension base="tsd:terminological_item_Type">
				<xs:sequence>
					<!-- attributes -->
					<xs:element name="content" type="xs:string"/>
					<!-- back pointer -->
					<xs:element name="abbreviates" type="id:IRDI_type"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<!-- Abbreviation Sequence -->
	<xs:complexType name="abbreviation_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:abbreviation" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Concept -->
	<xs:complexType name="concept_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="is_deprecated" type="xs:boolean"/>
			<!-- compositions -->
			<xs:element name="definition" type="tsd:definition_Type" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="image" type="tsd:image_Type" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="symbol" type="tsd:symbol_Type" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="term" type="tsd:term_Type" minOccurs="0" maxOccurs="unbounded"/>
			<!-- associations -->
			<xs:element name="type" type="id:IRDI_type"/>
			<!-- reifications -->
			<xs:element name="has_equivalence_with_another_concept_asserted_by" type="id:IRDI_type" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="required"/>
	</xs:complexType>
	<!-- Concept Sequence -->
	<xs:complexType name="concept_sequence">
		<xs:sequence>
			<xs:element ref="tsd:concept" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Concept Equivalence Relationship -->
	<xs:complexType name="concept_equivalence_relationship_Type">
		<xs:sequence>
			<!-- associations -->
			<xs:element name="equivalent_concept" type="id:IRDI_type" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="required"/>
	</xs:complexType>
	<!-- Concept Equivalence Relationship Sequence -->
	<xs:complexType name="concept_equivalence_relationship_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:concept_equivalence_relationship" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Concept Type -->
	<xs:complexType name="concept_type_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="name" type="basic:international_text_Type"/>
			<xs:element name="definition" type="basic:international_text_Type" minOccurs="0"/>
			<xs:element name="code" type="xs:string"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="required"/>
	</xs:complexType>
	<!-- Concept Type Sequence -->
	<xs:complexType name="concept_type_sequence">
		<xs:sequence>
			<xs:element ref="tsd:concept_type" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Definition -->
	<xs:complexType name="definition_Type">
		<xs:complexContent>
			<xs:extension base="tsd:terminological_item_Type">
				<xs:sequence>
					<!-- attributes -->
					<xs:element name="content" type="xs:string"/>
					<!-- back pointer -->
					<xs:element name="defines" type="id:IRDI_type"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<!-- Definition Sequence -->
	<xs:complexType name="definition_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:definition" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Document -->
	<xs:complexType name="document_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="title" type="basic:international_text_Type"/>
			<xs:element name="URI" type="xs:anyURI" minOccurs="0"/>
			<xs:element name="version" type="xs:string" minOccurs="0"/>
			<xs:element name="edition" type="xs:int" minOccurs="0"/>
			<xs:element name="publication_date" type="basic:day_interval_Type"/>
			<xs:element name="designator" type="xs:string" minOccurs="0"/>
			<!-- associations -->
			<xs:element name="publisher" type="id:IRDI_type"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="required"/>
	</xs:complexType>
	<!-- Document Sequence -->
	<xs:complexType name="document_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:document" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- File Representation -->
	<xs:complexType name="file_representation_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="mime_type" type="xs:string"/>
			<xs:element name="width_pixels" type="xs:int" minOccurs="0"/>
			<xs:element name="height_pixels" type="xs:int" minOccurs="0"/>
			<xs:element name="content" type="id:IRDI_type"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="required"/>
	</xs:complexType>
	<!-- File Representation Sequence -->
	<xs:complexType name="file_representation_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:file_representation" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Image -->
	<xs:complexType name="image_Type">
		<xs:complexContent>
			<xs:extension base="tsd:terminological_item_Type">
				<xs:sequence>
					<!-- attributes -->
					<xs:element name="name" type="xs:string" minOccurs="0"/>
					<!-- associations -->
					<xs:element name="representation" type="id:IRDI_type" maxOccurs="unbounded"/>
					<!-- back pointer -->
					<xs:element name="illustrates" type="id:IRDI_type"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<!-- Image Sequence -->
	<xs:complexType name="image_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:image" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Language -->
	<xs:complexType name="language_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="language_code" type="basic:ISO_language_code_Type"/>
			<xs:element name="country_code" type="basic:ISO_country_code_Type" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="optional"/>
	</xs:complexType>
	<!-- Language Sequence -->
	<xs:complexType name="language_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:language" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Organization -->
	<xs:complexType name="organization_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="name" type="basic:international_text_Type"/>
			<xs:element name="mail_address" type="xs:string"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="required"/>
	</xs:complexType>
	<!-- Organization Sequence -->
	<xs:complexType name="organization_sequence">
		<xs:sequence>
			<xs:element ref="tsd:organization" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Source Location -->
	<xs:complexType name="source_location_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="URL" type="xs:anyURI"/>
			<xs:element name="description" type="basic:international_text_Type" minOccurs="0"/>
			<!-- associations -->
			<xs:element name="provider" type="id:IRDI_type"/>
			<!-- back pointer -->
			<xs:element name="is_the_source_location_for" type="id:IRDI_type"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Symbol -->
	<xs:complexType name="symbol_Type">
		<xs:complexContent>
			<xs:extension base="tsd:terminological_item_Type">
				<xs:sequence>
					<!-- attributes -->
					<xs:element name="text_representation" type="xs:string" minOccurs="0"/>
					<!-- associations -->
					<xs:element name="file_representation" type="id:IRDI_type" minOccurs="0" maxOccurs="unbounded"/>
					<!-- back pointer -->
					<xs:element name="designates" type="id:IRDI_type"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<!-- Symbol Sequence -->
	<xs:complexType name="symbol_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:symbol" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Term -->
	<xs:complexType name="term_Type">
		<xs:complexContent>
			<xs:extension base="tsd:terminological_item_Type">
				<xs:sequence>
					<!-- attributes -->
					<xs:element name="content" type="xs:string"/>
					<!-- compositions -->
					<xs:element name="abbreviation" type="tsd:abbreviation_Type" minOccurs="0" maxOccurs="unbounded"/>
					<!-- back pointer -->
					<xs:element name="names" type="id:IRDI_type"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<!-- Term Sequence -->
	<xs:complexType name="term_sequence_Type">
		<xs:sequence>
			<xs:element ref="tsd:term" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Terminological Item -->
	<xs:complexType name="terminological_item_Type" abstract="true">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="originator_reference" type="xs:string" minOccurs="0"/>
			<!-- compositions -->
			<xs:element name="source_location" type="tsd:source_location_Type" minOccurs="0" maxOccurs="unbounded"/>
			<!-- associations -->
			<xs:element name="conformed_document" type="id:IRDI_type" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="containing_document" type="id:IRDI_type"/>
			<xs:element name="language" type="id:IRDI_type" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="id" type="id:IRDI_type" use="optional"/>
	</xs:complexType>
</xs:schema>
