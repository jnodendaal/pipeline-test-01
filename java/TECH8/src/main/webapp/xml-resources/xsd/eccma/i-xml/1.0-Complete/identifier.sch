<?xml version="1.0" encoding="UTF-8"?>
<!--
$Id: $
ISO TC 184/SC 4/WG 12 Nxxxx - ISO 22745-13 Identifier restriction for ISO 22745 - Schematron schema
-->
<!--
The following permission notice and disclaimer shall be included in all copies of this Schematron schema ("the Schema"), and derivations of the Schema:

Permission is hereby granted, free of charge in perpetuity, to any person obtaining a copy of the Schema, to use, copy, modify, merge and distribute free of charge, copies of the Schema for the purposes of developing, implementing, installing and using software based on the  Schema, and to permit persons to whom the Schema is furnished to do so, subject to the following conditions:

THE SCHEMA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN THE SCHEMA.

In addition, any modified copy of the Schema shall include the following notice:

THIS SCHEMA HAS BEEN MODIFIED FROM THE SCHEMA DEFINED IN ISO 22745-13, AND SHOULD NOT BE INTERPRETED AS COMPLYING WITH THAT STANDARD.

-->
<svrl:schema xmlns:svrl="http://purl.oclc.org/dsdl/schematron" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ascc.net/xml/schematron schematron1-5.xsd">
	<svrl:ns uri="http://www.w3.org/2005/xpath-functions" prefix="fn"/>
	<svrl:phase>
		<svrl:pattern name="IRDI">
			<title>An IRDI must have a version number of "1"</title>
			<!-- By convention, any attribute whose name is "id" or end with "_ref" but not "_local_ref" is an IRDI. -->
			<svrl:rule context="attribute()[(fn:local-name() eq 'id') or (fn:ends-with(fn:local-name(),'_ref') and not(fn:ends-with(fn:local-name(),'local_ref')))]">
<!-- fn:ends-with(.,'#1') -->
	<svrl:assert test="fn:matches(.,'^[^#]+$') or fn:matches(.,'[^#]+[#][^#]+#1')">the version number must be "1"</svrl:assert>
			</svrl:rule>
		</svrl:pattern>
	</svrl:phase>
</svrl:schema>
