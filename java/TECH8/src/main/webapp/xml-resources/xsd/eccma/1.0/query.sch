<?xml version="1.0" encoding="UTF-8"?>
<!--
$Id: $
ISO TC 184/SC 4/WG 12 N6779 - ISO/TS 22745-35 Query restriction for ISO 22745 - Schematron schema
-->
<!--
The following permission notice and disclaimer shall be included in all copies of this Schematron schema ("the Schema"), and derivations of the Schema:

Permission is hereby granted, free of charge in perpetuity, to any person obtaining a copy of the Schema, to use, copy, modify, merge and distribute free of charge, copies of the Schema for the purposes of developing, implementing, installing and using software based on the  Schema, and to permit persons to whom the Schema is furnished to do so, subject to the following conditions:

THE SCHEMA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN THE SCHEMA.

In addition, any modified copy of the Schema shall include the following notice:

THIS SCHEMA HAS BEEN MODIFIED FROM THE SCHEMA DEFINED IN ISO/TS 22745-35, AND SHOULD NOT BE INTERPRETED AS COMPLYING WITH THAT STANDARD.

-->
<svrl:schema xmlns:svrl="http://purl.oclc.org/dsdl/schematron" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ascc.net/xml/schematron schematron1-5.xsd">
	<svrl:ns uri="urn:iso:std:iso:ts:29002:-31:ed-1:tech:xml-schema:query" prefix="qy"/>
	<svrl:phase>
		<svrl:pattern name="ref attributes">
			<!-- qy:string_pattern may not have language_code attribute and may not have the country_code attribute -->
			<svrl:rule context="qy:string_pattern">
				<svrl:assert test="not(@language_code)">the language_code attribute should not be present</svrl:assert>
				<svrl:assert test="not(@country_code)">the country_code attribute should not be present</svrl:assert>
			</svrl:rule>
			<!-- qy:string_size may not have language_code attribute and may not have the country_code -->
			<svrl:rule context="qy:string_size">
				<svrl:assert test="not(@language_code)">the language_code attribute should not be present</svrl:assert>
				<svrl:assert test="not(@country_code)">the country_code attribute should not be present</svrl:assert>
			</svrl:rule>
			<!-- qy:range may not have UOM_code attribute and may not have the currency_code -->
			<svrl:rule context="qy:range">
				<svrl:assert test="not(@UOM_code)">the UOM_code attribute should not be present</svrl:assert>
				<svrl:assert test="not(@currency_code)">the currency_code attribute should not be present</svrl:assert>
			</svrl:rule>
		</svrl:pattern>
	</svrl:phase>
</svrl:schema>
