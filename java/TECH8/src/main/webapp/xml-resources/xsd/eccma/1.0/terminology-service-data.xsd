<?xml version="1.0" encoding="UTF-8"?>
<!--
$Id: terminology-service-data.xsd 411 2009-07-21 01:57:47Z radack $
ISO TC 184/SC 4/WG 12 N6661 - ISO/TS Terminology service data - XML schema
-->
<!--
The following permission notice and disclaimer shall be included in all copies of this XML schema ("the Schema"), and derivations of the Schema:

Permission is hereby granted, free of charge in perpetuity, to any person obtaining a copy of the Schema, to use, copy, modify, merge and distribute free of charge, copies of the Schema for the purposes of developing, implementing, installing and using software based on the  Schema, and to permit persons to whom the Schema is furnished to do so, subject to the following conditions:

THE SCHEMA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN THE SCHEMA.

In addition, any modified copy of the Schema shall include the following notice:

THIS SCHEMA HAS BEEN MODIFIED FROM THE SCHEMA DEFINED IN ISO/TS 29002-20, AND SHOULD NOT BE INTERPRETED AS COMPLYING WITH THAT STANDARD.

-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ts_d="urn:iso:std:iso:ts:29002:-20:ed-1:tech:xml-schema:terminology-service-data" xmlns:bas="urn:iso:std:iso:ts:29002:-4:ed-1:tech:xml-schema:basic" xmlns:id="urn:iso:std:iso:ts:29002:-5:ed-1:tech:xml-schema:identifier" targetNamespace="urn:iso:std:iso:ts:29002:-20:ed-1:tech:xml-schema:terminology-service-data" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:import namespace="urn:iso:std:iso:ts:29002:-5:ed-1:tech:xml-schema:identifier" schemaLocation="identifier.xsd"/>
	<xs:import namespace="urn:iso:std:iso:ts:29002:-4:ed-1:tech:xml-schema:basic" schemaLocation="basic.xsd"/>
	<!-- Elements -->
	<xs:element name="concept_search_specification" type="ts_d:concept_search_specification_Type"/>
	<xs:element name="concept_search_result" type="ts_d:concept_search_result_Type"/>
	<xs:element name="document_search_specification" type="ts_d:document_search_specification_Type"/>
	<xs:element name="document_search_result" type="ts_d:document_search_result_Type"/>
	<xs:element name="language_search_specification" type="ts_d:language_search_specification_Type"/>
	<xs:element name="language_search_result" type="ts_d:language_search_result_Type"/>
	<!-- Types -->
	<!-- Concept Search Specification -->
	<xs:complexType name="concept_search_specification_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="language_ref" type="id:IRDI_type" minOccurs="0"/>
			<xs:element name="language_code" type="bas:ISO_language_code_Type" minOccurs="0"/>
			<xs:element name="country_code" type="bas:ISO_country_code_Type" minOccurs="0"/>
			<xs:element name="concept_type_ref" type="id:IRDI_type" minOccurs="0"/>
			<xs:element name="term_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="definition_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="abbreviation_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="textual_symbol_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="originator_reference_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="not_deprecated_only" type="xs:boolean"/>
			<xs:element name="conformed_document_ref" type="id:IRDI_type" minOccurs="0"/>
			<xs:element name="containing_document_ref" type="id:IRDI_type" minOccurs="0"/>
			<xs:element name="referencing_document_ref" type="id:IRDI_type" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Concept Search Result -->
	<xs:complexType name="concept_search_result_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="concept_ref" type="id:IRDI_type"/>
			<xs:element name="score" type="xs:double"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Concept Search Result Sequence -->
	<xs:complexType name="concept_search_result_sequence_Type">
		<xs:sequence>
			<xs:element ref="ts_d:concept_search_result" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Document Search Specification -->
	<xs:complexType name="document_search_specification_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="title_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="URI_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="version_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="edition_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="min_publication_date" type="xs:date" minOccurs="0"/>
			<xs:element name="max_publication_date" type="xs:date" minOccurs="0"/>
			<xs:element name="publisher_ref" type="id:IRDI_type" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Document Search Result -->
	<xs:complexType name="document_search_result_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="document_ref" type="id:IRDI_type"/>
			<xs:element name="score" type="xs:double"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Document Search Result Sequence -->
	<xs:complexType name="document_search_result_sequence_Type">
		<xs:sequence>
			<xs:element ref="ts_d:document_search_result" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Language Search Specification -->
	<xs:complexType name="language_search_specification_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="language_code_pattern" type="xs:string" minOccurs="0"/>
			<xs:element name="country_code_pattern" type="xs:string" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Language Search Result -->
	<xs:complexType name="language_search_result_Type">
		<xs:sequence>
			<!-- attributes -->
			<xs:element name="language_ref" type="id:IRDI_type"/>
			<xs:element name="score" type="xs:double"/>
		</xs:sequence>
	</xs:complexType>
	<!-- Language Search Result Sequence -->
	<xs:complexType name="language_search_result_sequence_Type">
		<xs:sequence>
			<xs:element ref="ts_d:language_search_result" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
