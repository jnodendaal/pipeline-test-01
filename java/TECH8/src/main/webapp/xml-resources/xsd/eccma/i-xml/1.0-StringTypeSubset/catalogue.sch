<?xml version="1.0" encoding="UTF-8"?>
<!--
$Id: catalogue.sch 442 2009-08-24 18:28:47Z radack $
ISO TC 184/SC 4/WG 12 N6781 - ISO/TS 22745-40 Catalogue restriction for ISO 22745 - Schematron schema
-->
<!--
The following permission notice and disclaimer shall be included in all copies of this Schematron schema ("the Schema"), and derivations of the Schema:

Permission is hereby granted, free of charge in perpetuity, to any person obtaining a copy of the Schema, to use, copy, modify, merge and distribute free of charge, copies of the Schema for the purposes of developing, implementing, installing and using software based on the  Schema, and to permit persons to whom the Schema is furnished to do so, subject to the following conditions:

THE SCHEMA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN THE SCHEMA.

In addition, any modified copy of the Schema shall include the following notice:

THIS SCHEMA HAS BEEN MODIFIED FROM THE SCHEMA DEFINED IN ISO/TS 22745-40, AND SHOULD NOT BE INTERPRETED AS COMPLYING WITH THAT STANDARD.

-->
<svrl:schema xmlns:svrl="http://purl.oclc.org/dsdl/schematron" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ascc.net/xml/schematron schematron1-5.xsd">
  <svrl:ns uri="urn:iso:std:iso:ts:29002:-10:ed-1:tech:xml-schema:catalogue" prefix="cat"/>  
  <svrl:ns uri="urn:iso:std:iso:ts:29002:-10:ed-1:tech:xml-schema:value" prefix="val"/>
  <svrl:phase>
    <svrl:pattern name="ref attributes">
      <!-- cat:item must have data_specification_ref attribute -->
      <svrl:rule context="cat:item">
	<svrl:assert test="@data_specification_ref">the data_specification_ref attribute should be present</svrl:assert>
      </svrl:rule>
      <!-- cat:reference must have organization_ref attribute and may not have organization_code attribute -->
      <svrl:rule context="cat:reference">
	<svrl:assert test="@organization_ref">the organization_ref attribute should be present</svrl:assert>
	<svrl:assert test="not(@organization_code)">the organization_code attribute should not be present</svrl:assert>
      </svrl:rule>
      <!-- val:controlled_value must have value_ref attribute and may not have value_code attribute -->
      <svrl:rule context="val:controlled_value">
	<svrl:assert test="@value_ref">the value_ref attribute should be present</svrl:assert>
	<svrl:assert test="not(@value_code)">the value_code attribute should not be present</svrl:assert>
      </svrl:rule>
      <!-- val:currency_value must have value_ref attribute and may not have value_code attribute -->
      <svrl:rule context="val:currency_value">
	<svrl:assert test="@currency_ref">the currency_ref should be present</svrl:assert>
	<svrl:assert test="not(@currency_code)">the currency_code attribute should not be present</svrl:assert>
      </svrl:rule>
      <!-- val:measure_number_value must have UOM_ref attribute and may not have UOM_code attribute -->
      <svrl:rule context="val:measure_range_value">
	<svrl:assert test="@UOM_ref">the UOM_ref attribute should be present</svrl:assert>
	<svrl:assert test="not(@UOM_code)">the UOM_code attribute should not be present</svrl:assert>
      </svrl:rule>
      <!-- val:measure_range_value must have UOM_ref attribute and may not have UOM_code attribute -->
      <svrl:rule context="val:measure_range_value">
	<svrl:assert test="@UOM_ref">the UOM_ref attribute should be present</svrl:assert>
	<svrl:assert test="not(@UOM_code)">the UOM_code attribute should not be present</svrl:assert>
      </svrl:rule>
      <!-- val:qualified_value must have UOM_ref attribute and may not have UOM_code attribute -->
      <svrl:rule context="val:qualified_value">
	<svrl:assert test="@qualifier_ref">the qualifier_ref attribute should be present</svrl:assert>
	<svrl:assert test="not(@qualifier_code)">attribute qualifier_code is present</svrl:assert>
      </svrl:rule>
      <!-- bas:language_string must have language_ref element and may not have language_code or country_code element -->
      <svrl:rule context="val:local_string">
	<svrl:assert test="val:language_ref">the language_ref attribute should be present</svrl:assert>
	<svrl:assert test="not(val:language_code)">the language_code attribute should not be present</svrl:assert>
	<svrl:assert test="not(val:country_code)">the country_code attribute should not present</svrl:assert>
      </svrl:rule>
    </svrl:pattern>
  </svrl:phase>
</svrl:schema>
