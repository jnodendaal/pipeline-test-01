/*
 * T8 Record Editor v1.0.0 (2015-02-09)
 * Copyright (c) 2011-2014 PiLog.
 * http://www.pilog.in/
 *
 * author: Bouwer du Preez
 */
$.widget("t8.t8RecordEditor",
{
    options:
    {
        navClass: "t8-recordeditor-nav",
        navListClass: "t8-recordeditor-nav-list",
        navListItemClass: "t8-recordeditor-nav-list-item",
        navHeaderClass: "t8-recordeditor-nav-header",
        navContentClass: "t8-recordeditor-nav-content",
        docClass: "t8-recordeditor-doc",
        docHeaderClass: "t8-recordeditor-doc-header",
        docContentClass: "t8-recordeditor-doc-content",
        width: "500px",
        dataRecord: null
    },

    // This is where you setup everything related to your widget, such as
    // creating elements, binding events, etc. This gets run once, immediately
    // after instantiation.
    _create: function ()
    {
        var docElement;
        var docHeaderElement;
        var docContentElement;
        var navElement;
        var navHeaderElement;
        var navContentElement;

        this.element.addClass("t8RecordEditor");

        // Create the document header element.
        docHeaderElement = document.createElement("div");
        docHeaderElement.setAttribute("class", this.options.docHeaderClass);
        docHeaderElement.textContent = "Document";

        // Create the document content element.
        docContentElement = document.createElement("div");
        docContentElement.setAttribute("class", this.options.docContentClass);

        // Create the document container element.
        docElement = document.createElement("div");
        docElement.setAttribute("class", this.options.docClass);
        docElement.appendChild(docHeaderElement);
        docElement.appendChild(docContentElement);
        docElement.style.float = "left";

        // Create the navigation header element.
        navHeaderElement = document.createElement("div");
        navHeaderElement.textContent = "Navigation";
        navHeaderElement.setAttribute("class", this.options.navHeaderClass);

        // Create the navigation content element.
        navContentElement = document.createElement("div");
        navContentElement.setAttribute("class", this.options.navContentClass);

        // Create the navigation container element.
        navElement = document.createElement("div");
        navElement.setAttribute("class", this.options.navClass);
        navElement.appendChild(navHeaderElement);
        navElement.appendChild(navContentElement);
        navElement.style.margin.right = "20px";
        navElement.style.float = "left";

        // Create the UI references.
        this.ui =
        {
            "doc":docElement,
            "docHeader":docHeaderElement,
            "docContent":docContentElement,
            "nav":navElement,
            "navHeader":navHeaderElement,
            "navContent":navContentElement,
            "form":null,
            "sections":{},
            "properties":{}
        };

        // Append the create form to the container element.
        this.element.hide();
        this.element.css("overflow", "auto");
        this.element.css("zoom", "1");
        this.element.empty();
        this.element.append(navElement);
        this.element.append(docElement);

        this._update();
    },

    // This method is invoked anytime your widget is invoked without either 0
    // arguments or with a single options argument. This could be the first time
    // it is invoked, in which case _init will get called after _create. It
    // could also be called at any time after the widget create, in which case
    // _init allows you to handle any re-initialization without forcing the user
    // to perform a destroy->create cycle.
    _init: function()
    {
    },

    _setOption: function (key, value)
    {
        this.options[ key ] = value;
        this._update();
    },

    _update: function ()
    {
    },

    /**
     * Saves all data in the currently displayed UI to the underlying
     * Data Record object.
     */
    _commitChanges: function()
    {
        var properties;

        // Iterate over all propertyUI objects and commit() changes on each one.
        properties = this.ui.properties;
        if (properties)
        {
            for (var propertyID in properties)
            {
                if (properties.hasOwnProperty(propertyID))
                {
                    var propertyUI;

                    // Make sure the commit function exists.
                    propertyUI = properties[propertyID];
                    if (propertyUI.commit)
                    {
                        // Call the commit function.
                        propertyUI.commit();
                    }
                }
            }
        }
    },

    // Returns the root Data Record displayed in the editor.

    /**
     * Commits all changes on the editor and returns the updated root data
     * record.
     * @returns The root data record being edited.
     */
    getDataRecord: function()
    {
        // Commit outstanding changes.
        this._commitChanges();

        // Return the root data record.
        return this.options.dataRecord;
    },

    setDataRecord: function(newDataRecord)
    {
        // Set the local variable.
        this.options.dataRecord = new DataRecord(null, newDataRecord);

        // Initialize the data record.
        //initializeDataRecord(this.options.dataRecord);

        // Set the root record as focus.
        this.showDataRecord(newDataRecord.recordID);
    },

    // Sets the root Data Record in the editor.
    showDataRecord: function(targetRecordID)
    {
        var targetRecord;

        // First commit changes on any existing record.
        this._commitChanges();

        // Debugging prinout.
        console.log("Showing record: " + targetRecordID);

        // Make sure this element is visible (it is hidden when first created).
        jQuery(this.element).show();

        // Hide the document content while it is being constructed.
        jQuery(this.ui.doc).hide();

        // Find the target record.
        targetRecord = this.options.dataRecord.getRecord(targetRecordID);

        // Construct the navigation section.
        this._constructNavigation(targetRecord);

        // Construct the document editor.
        this._constructDocument(targetRecord);

        // Fade the document content back in.
        jQuery(this.ui.doc).fadeIn('slow');
    },

    /**
     * Construct the navigation part of the record editor.  This element is
     * stored in the ui.nav variable but only the ui.navContent part is altered
     * by this function.
     *
     * @param targetRecord The currently displayed record in the editor.  The
     * navigation view will be constructed to show the record path to this
     * record.
     */
    _constructNavigation: function(targetRecord)
    {
        var recordPath;
        var navListElement;

        // Create the navigation content element.
        navListElement = document.createElement("ul");
        navListElement.setAttribute("class", this.options.navListClass);

        // Get the record path to the target record.
        recordPath = targetRecord.getPathRecords();

        // Construct some test items.
        for (var itemIndex = 0; itemIndex < recordPath.length; itemIndex++)
        {
            var navItemElement;
            var navLinkElement;
            var linkRecordID;
            var linkRecord;

            // Get the link record ID.
            linkRecord = recordPath[itemIndex];
            linkRecordID = linkRecord.recordID;

            // Construct the navigation hyperlink.
            navLinkElement = document.createElement("a");
            navLinkElement.setAttribute("href", "#");
            navLinkElement.setAttribute("onClick", "element('$" + this.element.attr("id") + "').t8RecordEditor('showDataRecord','" + linkRecordID + "');return false;");
            navLinkElement.textContent = linkRecord.drInstance.term;

            // Create the navigation list element.
            navItemElement = document.createElement("li");
            navItemElement.setAttribute("class", this.options.navListItemClass);
            navItemElement.setAttribute("onClick", "element('$" + this.element.attr("id") + "').t8RecordEditor('showDataRecord','" + linkRecordID + "');");
            navItemElement.appendChild(navLinkElement);

            // Add the navigation list element to the list.
            navListElement.appendChild(navItemElement);
        }

        // Clear the navigation content and then add the new navigation list.
        clearElement(this.ui.navContent);
        this.ui.navContent.appendChild(navListElement);
    },

    _constructDocument: function(targetRecord)
    {
        var drInstance;
        var sections;
        var newElement;

        // Create the new header html.
        drInstance = targetRecord.drInstance;
        this.ui.docHeader.textContent = drInstance.term;

        // Create the form.
        newElement = document.createElement("form");
        newElement.style.padding = "5px";
        this.ui.form = newElement;

        // Append all sections.
        sections = drInstance.sections;
        for (var sectionIndex = 0; sectionIndex < sections.length; ++sectionIndex)
        {
            this._constructSection(targetRecord, sections[sectionIndex]);
        }

        // Add the new form to the content element.
        clearElement(this.ui.docContent);
        this.ui.docContent.appendChild(this.ui.form);
    },

    /**
     * Constructs input elements for the supplied section.
     * @param targetRecord The data record object o which data entered in the
     * section will be saved.
     * @param section The section requirement object for which to construct the
     * UI.
     */
    _constructSection: function(targetRecord, section)
    {
        var sectionID;
        var properties;
        var sectionElement;

        // Get the section object from the data record.
        sectionID = section.sectionID;

        // Create a new section element.
        sectionElement = document.createElement("section");
        this.ui.form.appendChild(sectionElement);
        this.ui.sections.sectionID = sectionElement;

        // Append all properties in the section.
        properties = section.properties;
        for(var propertyIndex = 0; propertyIndex < properties.length; ++propertyIndex)
        {
            this._constructProperty(targetRecord, sectionElement, properties[propertyIndex]);
        }
    },

    /**
     * Construct an input element for the supplied property and adds the element
     * the the specified section element.
     * @param targetRecord The data record object to which data entered in the
     * property will be saved.
     * @param sectionElement The section UI element to which the property
     * element will be added.
     * @param property The property requirement object.
     */
    _constructProperty: function(targetRecord, sectionElement, property)
    {
        var propertyID;
        var property;
        var propertyUI;

        // Get the property object from the data record.
        propertyID = property.propertyID;

        // Create the property UI.
        propertyUI =
        {
            "property":property,
            "element":null,
            "composite":false,
            "fields":{},
            "commit": null // Commit function is added here.
        };
        this.ui.properties[propertyID] = propertyUI;

        // Check for composite/regular values.
        if (property.value !== null)
        {
            if (property.value.typeID === "COMPOSITE")
            {
                var fieldSetElement;
                var legendElement;
                var composite;
                var fields;

                // Set the composite flag on the property UI.
                propertyUI.composite = true;

                // Get the composite value from the data record.
                composite = property.value;

                // Create the fieldset element.
                fieldSetElement = document.createElement("fieldset");
                fieldSetElement.style.margin = "10px";
                fieldSetElement.style.background = "#ffffee";
                sectionElement.appendChild(fieldSetElement);

                // Create the legend element.
                legendElement = document.createElement("legend");
                legendElement.textContent = property.term;
                fieldSetElement.appendChild(legendElement);

                // Create all the field elements.
                fields = composite.fields;
                for (var fieldIndex = 0; fieldIndex < fields.length; ++fieldIndex)
                {
                    var fieldUI;
                    var field;
                    var fieldID;

                    // Get the field from the data record.
                    field = fields[fieldIndex];
                    fieldID = field.fieldID;

                    // Create the field UI.
                    fieldUI =
                    {
                        "field":field,
                        "element":null
                    };
                    propertyUI.fields[fieldID] = fieldUI;

                    // Create the value element.
                    if (field.value !== null)
                    {
                        var inputElements;
                        var inputElement;
                        var valueElement;

                        // Create the value element.
                        valueElement = this._constructValueElement(targetRecord, propertyID, fieldID, field.term, field.value);

                        // Add the value to the fieldset.
                        fieldSetElement.appendChild(valueElement);

                        // Store the input element in a collection where it can be easily accessed.
                        inputElements = valueElement.getElementsByTagName("input");
                        inputElement = inputElements[0];
                        fieldUI.element = inputElement;
                    }
                }
            }
            else
            {
                var inputElements;
                var inputElement;
                var valueElement;

                // Create the value element.
                valueElement = this._constructValueElement(targetRecord, propertyID, null, property.term, property.value);

                // Add the value to the section.
                sectionElement.appendChild(valueElement);

                // Store the input element in a collection where it can be easily accessed.
                propertyUI.element = valueElement;
            }
        }
    },

    _constructValueElement: function(targetRecord, propertyID, fieldID, label, valueRequirement)
    {
        var paragraphElement;
        var labelElement;
        var spanElement;
        var typeID;
        var inputID;
        var valueObject;
        var propertyUI;

        // Get the UI object for the property.
        propertyUI = this.ui.properties[propertyID];

        // Determine the input ID.
        if (fieldID) inputID = propertyID + "." + fieldID;
        else inputID = propertyID;

        // Find the value object.
        valueObject = targetRecord.getValue(propertyID, fieldID);

        // Get the type ID.
        typeID = valueRequirement.typeID;

        // Create the paragraph element.
        paragraphElement = document.createElement("p");
        paragraphElement.style.margin = "10px";

        // Create the label element.
        labelElement = document.createElement("label");
        labelElement.setAttribute("for", inputID);
        paragraphElement.appendChild(labelElement);

        // Create the span element.
        spanElement = document.createElement("span");
        spanElement.textContent = label;
        labelElement.appendChild(spanElement);

        // Create the input element.
        switch (typeID)
        {
            case "BOOLEAN":
                var checkBoxElement;

                // Create the input element.
                checkBoxElement = document.createElement("div");
                checkBoxElement.setAttribute("id", inputID);
                checkBoxElement.style.marginTop = "5px";
                labelElement.appendChild(checkBoxElement);

                // Apply the input widget.
                jQuery(checkBoxElement).jqxCheckBox({"theme":"t8", "checked":(valueObject === true)});
                break;
            case "CONTROLLED_CONCEPT":
                var conceptListElement;
                var concepts;
                var jqListElement;

                // Create an element to act as the concept list.
                conceptListElement = document.createElement("div");

                // Get the concepts from the value object.
                concepts = valueRequirement.concepts;

                // Add the concept list element to the label.
                labelElement.appendChild(conceptListElement);

                // Create a JQuery object of the list element.
                jqListElement = jQuery(conceptListElement);

                // Apply the input widget.
                jqListElement.jqxComboBox({"theme":"t8", "width":"400px", "source":concepts, "valueMember":"conceptID", "displayMember":"term"});

                // Set the selected concept.
                if (valueObject)
                {
                    console.log("Setting selected concept: " + valueObject.value);
                    jqListElement.val(valueObject.value);
                }

                break;
            case "DOCUMENT_REFERENCE":
                var referenceListElement;

                // Create an element to act as the reference list container.
                referenceListElement = document.createElement("div");

                // Check that we actually have a value to add.
                if (valueObject)
                {
                    var references;

                    // Get the references from the value object.
                    references = valueObject.references;
                    if ((references) && (references.length > 0))
                    {
                        for (var referenceIndex = 0; referenceIndex < references.length; referenceIndex++)
                        {
                            var referenceItemElement;
                            var referenceElement;
                            var reference;
                            var referenceRecordID;
                            var referencedRecord;

                            // Get the reference object.
                            reference = references[referenceIndex];
                            referenceRecordID = reference.recordID;
                            referencedRecord = targetRecord.getRecord(referenceRecordID);
                            //TODO: GBO - Remove this. It was added as a temporary check for data issues
                            if (!referencedRecord)
                            {
                                console.log("Referenced record is null. Reference Record ID : " + referenceRecordID);
                                alert("Referenced record for reference record ID [" + referenceRecordID + "] is null.");
                            }

                            // Construct the reference hyperlink.
                            referenceElement = document.createElement("a");
                            referenceElement.setAttribute("href", "#");
                            referenceElement.setAttribute("onClick", "element('$" + this.element.attr("id") + "').t8RecordEditor('showDataRecord','" + referenceRecordID + "');return false;");
                            referenceElement.setAttribute("recordID", referenceRecordID);
                            referenceElement.textContent = referencedRecord.getValueString();

                            // Add the reference link to the reference list element.
                            referenceItemElement = document.createElement("p");
                            referenceItemElement.appendChild(referenceElement);
                            referenceListElement.appendChild(referenceItemElement);
                        }
                    }
                    else
                    {
                        var noValueElement;

                        noValueElement = document.createElement("span");
                        noValueElement.textContent = "No Value";
                        noValueElement.style.color = "lightgray";
                        referenceListElement.appendChild(noValueElement);
                    }
                }
                else
                {
                    var noValueElement;

                    noValueElement = document.createElement("p");
                    noValueElement.textContent = "No Value";
                    noValueElement.style.color = "lightgray";
                    referenceListElement.appendChild(noValueElement);
                }

                // Add the reference list element to the label.
                labelElement.appendChild(referenceListElement);
                break;
            case "STRING":
                var textFieldElement;

                // Create the text field input element.
                textFieldElement = document.createElement("input");
                textFieldElement.setAttribute("id", inputID);
                textFieldElement.setAttribute("type", "text");
                textFieldElement.style.width = "100%";
                textFieldElement.style.marginTop = "5px";
                labelElement.appendChild(textFieldElement);

                // Set the value.
                if (valueObject) textFieldElement.value = valueObject.value;

                // Apply the input widget.
                jQuery(textFieldElement).jqxInput({"theme":"t8", "height":"25px", "width":"100%"});

                // Add the commit function to the UI.
                propertyUI.commit = function()
                {
                    var inputValue;

                    inputValue = jQuery(textFieldElement).val();
                    targetRecord.setStringValue(propertyID, null, inputValue);
                };

                // End input construction.
                break;
            default:
                var noValueElement;

                noValueElement = document.createElement("p");
                noValueElement.textContent = "No Value";
                noValueElement.style.color = "lightgray";
                labelElement.appendChild(noValueElement);
        }

        // Return the created element.
        return paragraphElement;
    },

    // This destroys an instantiated plugin and does any necessary cleanup. All
    // modifications your plugin performs must be removed on destroy. This
    // includes removing classes, unbinding events, destroying created elements,
    // etc. The widget factory provides a starting point, but should be extended
    // to meet the needs of the individual plugin.
    destroy: function ()
    {
        this.element
                .removeClass("t8RecordEditor")
                .text("");

        // Call the base destroy function.
        $.Widget.prototype.destroy.call(this);
    }
});
