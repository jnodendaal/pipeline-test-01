package com.pilog.t8.document.reverse.integration;

import com.pilog.reverse.ObjectFactory;
import com.pilog.reverse.Record;
import com.pilog.reverse.ReverseOperation;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.document.integration.T8IntegrationServicePersistenceHandler;
import com.pilog.t8.data.document.integration.T8RevereseIntegrationServiceOperationResult;
import com.pilog.t8.definition.data.document.reverse.integration.T8ReverseIntegrationDefinition;
import com.pilog.t8.definition.data.document.reverse.integration.T8ReverseIntegrationDomainsDefinition;
import com.pilog.t8.definition.data.document.reverse.integration.T8ReverseIntegrationOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.webservices.utils.WebServiceUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
 *
 * @author Andre Scheepers
 */
@WebService(serviceName = "MDQMReverseInterface",
            portName = "MDQMReverseInterfacePort",
            endpointInterface = "com.pilog.reverse.MDQMReverseInterface",
            targetNamespace = "http://reverse.pilog.com/",
            wsdlLocation = "WEB-INF/wsdl/MDQMReverseInterfaceService.wsdl")

public class MDQMReverseInterfaceService
{
    @Resource
    private WebServiceContext wsContext;

    public void reverseOperation(ReverseOperation reverseOperation)
    {
        ServletContext servletContext;
        T8ServerContext serverContext;
        T8SessionContext sessionContext;
        T8Context context;
        T8SecurityManager securityManager = null;
        T8DataManager dataManager = null;
        T8IntegrationServicePersistenceHandler persistenceHandler;
        T8ReverseIntegrationDefinition definition = null;

        //Determine the correct server operation to call, depending on the request information.
        T8ReverseIntegrationOperationDefinition serverOperationDefinition;
        Map<String, Object> inputParameters;

        try
        {
            //Get the T8 server context
            servletContext = (ServletContext) wsContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            serverContext = WebServiceUtils.getT8ServerContext(servletContext);

            for (String identifier : serverContext.getDefinitionManager().getDefinitionIdentifiers(null, T8ReverseIntegrationDefinition.TYPE_IDENTIFIER))
            {
                if(definition == null)
                {
                    definition = (T8ReverseIntegrationDefinition) serverContext.getDefinitionManager().getInitializedDefinition(null, null, identifier, null);
                } else throw new Exception("Only one T8 Reverse Integration Definition allowed per server instance");
            }

            if(definition == null)
            {
                throw new Exception("No T8 Reverse Integration Definition was defined for this server instance");
            }

            securityManager = serverContext.getSecurityManager();
            context = securityManager.createOrganizationContext(null, definition.getOrganizationID());
            dataManager = serverContext.getDataManager();
            dataManager.openDataSession(context);

            //Now execute the required operation
            List<T8RevereseIntegrationServiceOperationResult> results;

            results = new ArrayList<>();

            for (Record record : reverseOperation.getRecord())
            {
                T8RevereseIntegrationServiceOperationResult result;
                Map<String, String> propertyMappings;

                propertyMappings = new HashMap<>();

                record.getContent().getProperty().stream().forEach((propertyContent) ->
                {
                    propertyMappings.put(propertyContent.getPropertyTerm(), propertyContent.getValue());
                });

                result = new T8RevereseIntegrationServiceOperationResult(reverseOperation.getOperationID(), definition.getOrganizationID(), record.getHeader().getDomainType(), record.getHeader().getOperationType(), record.getHeader().getExternalReference(), record.getHeader().getRequestNumber(), propertyMappings);
                results.add(result);
            }
            serverOperationDefinition = getServerOperationDefintion(definition, reverseOperation);
            inputParameters = new HashMap<>();
            inputParameters.put(serverOperationDefinition.getServerOperationResultParameterIdentifier(), results);

            persistenceHandler = new T8IntegrationServicePersistenceHandler(context);
            persistenceHandler.setOperationIID(reverseOperation.getOperationID());
            persistenceHandler.logReceivedEvent(new ObjectFactory().createReverseOperation(reverseOperation));
            serverContext.queueOperation(context, serverOperationDefinition.getServerOperationIdentifier(), inputParameters);
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to service T8 Integration response request", ex);
            throw new RuntimeException("Failed to service T8 Integration Response request");
        }
        finally
        {
            // Unbind the request thread from the data session.
            if (dataManager != null) dataManager.closeCurrentSession();
            if (securityManager != null) securityManager.destroyContext();
        }
    }

    private T8ReverseIntegrationOperationDefinition getServerOperationDefintion(T8ReverseIntegrationDefinition definition, ReverseOperation reverseOperation)
    {
        String domainIdentifier;
        String operationIdentifier;

        domainIdentifier = reverseOperation.getRecord().get(0).getHeader().getDomainType();
        operationIdentifier = reverseOperation.getRecord().get(0).getHeader().getOperationType();

        for (T8ReverseIntegrationDomainsDefinition domainDefinition : definition.getDomainsDefinitions())
        {
            if (domainDefinition.getDomainIdentifier().equals(domainIdentifier))
            {
                for (T8ReverseIntegrationOperationDefinition operationDefinition : domainDefinition.getOperationDefinitions())
                {
                    if (operationDefinition.getOperationIdentifier().equals(operationIdentifier))
                    {
                        return operationDefinition;
                    }
                }
            }
        }

        return null;
    }
}
