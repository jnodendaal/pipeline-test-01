package com.pilog.t8.document.integration;

import com.pilog.DataRows;
import com.pilog.ObjectFactory;
import com.pilog.OperationResponse;
import com.pilog.RecordHeader;
import com.pilog.SapTables;
import com.pilog.TableSections;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.integration.T8IntegrationServiceOperationResult;
import com.pilog.t8.data.document.integration.T8IntegrationServicePersistenceHandler;
import com.pilog.t8.data.document.integration.structure.sap.SAPDataRow;
import com.pilog.t8.data.document.integration.structure.sap.SAPField;
import com.pilog.t8.data.document.integration.structure.sap.SAPTable;
import com.pilog.t8.data.document.integration.structure.sap.SAPTableSection;
import com.pilog.t8.definition.data.document.integration.T8IntegrationResponseDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.webservices.utils.WebServiceUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
* @author Hennie Brink
*/
@WebService(serviceName = "MDQMInterfaceResponse",
            portName = "MDQMInterfaceResponsePort",
            endpointInterface = "com.pilog.MDQMInterfaceResponse",
            targetNamespace = "http://pilog.com/",
            wsdlLocation = "wsdl/MDQMInterfaceResponseService.wsdl")
public class MDQMInterfaceResponseService
{
    private static final T8Logger LOGGER = T8Log.getLogger(MDQMInterfaceResponseService.class);

    @Resource
    private WebServiceContext wsContext;

    public void respond(OperationResponse operationResponse)
    {
        ServletContext servletContext;
        T8ServerContext serverContext;
        T8IntegrationServicePersistenceHandler persistenceHandler;

        //Get the T8 server context
        servletContext = (ServletContext) wsContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        serverContext = WebServiceUtils.getT8ServerContext(servletContext);

        try
        {
            List<T8IntegrationServiceOperationResult> results;
            T8IntegrationResponseDefinition definition = null;
            Map<String, Object> inputParameters;
            T8Context context;

            try
            {
                for (String identifier : serverContext.getDefinitionManager().getDefinitionIdentifiers(null, T8IntegrationResponseDefinition.TYPE_IDENTIFIER))
                {
                    if (definition == null)
                    {
                        definition = (T8IntegrationResponseDefinition) serverContext.getDefinitionManager().getInitializedDefinition(null, null, identifier, null);
                    } else throw new Exception("Only one T8 Integration Response Definition allowed per server instance");
                }

                if (definition == null)
                {
                    throw new Exception("No T8 Integration Response Definition was defined for this server instance");
                }
            }
            catch(Exception ex)
            {
                LOGGER.log("Failed to retrieve a T8 Definition to handle the Integration Service Response", ex);
                throw new RuntimeException("The T8 server could not handle the response request at this time.");
            }

            // Create the data session for this thread
            // We can only do this once we have both the server and session contexts
            context = serverContext.getSecurityManager().createOrganizationContext(null, definition.getOrganizationIdentifier());
            serverContext.getDataManager().openDataSession(context);

            //Now execute the required operation
            results = new ArrayList<>();
            for (OperationResponse.OperationResult operationResult : operationResponse.getOperationResult())
            {
                T8IntegrationServiceOperationResult result;
                Map<String, String> keyMap;

                keyMap = new HashMap<>();
                for (RecordHeader.Organization organization : operationResult.getRecord().getOrganization())
                {
                    keyMap.put(organization.getOrganizationTerm(), organization.getOrganizationID());
                }

                result = new T8IntegrationServiceOperationResult(operationResponse.getOperationID(), operationResult.getRecord().getRecordID(), operationResult.getRecord().getExternalReference(),
                        keyMap, operationResult.isSuccess(), operationResult.getErrorCode(), operationResult.getErrorMessage(), constructSAPTableStructure(operationResult));

                results.add(result);
            }

            inputParameters = new HashMap<>();
            inputParameters.put(definition.getResponseParameterIdentifier(), results);
            try
            {
                persistenceHandler = new T8IntegrationServicePersistenceHandler(context);
                persistenceHandler.setOperationIID(operationResponse.getOperationID());
                persistenceHandler.logReceivedEvent(new ObjectFactory().createOperationResponse(operationResponse));
                serverContext.executeSynchronousOperation(context, definition.getOperationIdentifier(), inputParameters);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to service T8 Integration response request", ex);
                throw new RuntimeException("Failed to service T8 Integration Response request");
            }
        }
        finally
        {
            // Close the data session and access context for this thread
            serverContext.getDataManager().closeCurrentSession();
            serverContext.getSecurityManager().destroyContext();
        }
    }

    private List<SAPTable> constructSAPTableStructure(OperationResponse.OperationResult operationResult)
    {
        List<SAPTable> t8SAPTables;

        // Check if response Operation consists of a SAP table structure, if not return a null value.
        if (operationResult.getSapTables() == null || operationResult.getSapTables().getSapTable().isEmpty()) return null;

        // SAP Tables Construction
        t8SAPTables = new ArrayList<>();
        for (SapTables.SapTable sapTable : operationResult.getSapTables().getSapTable())
        {
            SAPTable t8SAPTable;
            List<SAPTableSection> t8SAPTableSections;

            // SAP Table Construction
            t8SAPTable = new SAPTable();
            t8SAPTable.setTableName(sapTable.getTableName());

            // SAP Table Sections Construction
            t8SAPTableSections = new ArrayList<>();
            for (TableSections.TableSection tableSection : sapTable.getTableSections().getTableSection())
            {
                SAPTableSection t8SAPTableSection;
                List<SAPDataRow> t8SAPDataRows;

                // SAP Table Section Construction
                t8SAPTableSection = new SAPTableSection();
                t8SAPTableSection.setSectionTerm(tableSection.getSectionTerm());

                // SAP Data Rows Construction
                t8SAPDataRows = new ArrayList<>();
                for (DataRows.DataRow dataRow : tableSection.getDataRows().getDataRow())
                {
                    SAPDataRow t8SAPDataRow;
                    List<SAPField> t8SAPFields;

                    // SAP Data Row Construction
                    t8SAPDataRow = new SAPDataRow();

                    // SAP Fields Construction
                    t8SAPFields = new ArrayList<>();
                    for (DataRows.DataRow.Field field : dataRow.getField())
                    {
                        SAPField t8SAPField;

                        // SAP Field Construction
                        t8SAPField = new SAPField();
                        t8SAPField.setFieldName(field.getFieldName());
                        t8SAPField.setValue(field.getValue());

                        t8SAPFields.add(t8SAPField);
                    }
                    t8SAPDataRow.setFields(t8SAPFields);
                    t8SAPDataRows.add(t8SAPDataRow);
                }
                t8SAPTableSection.setDataRows(t8SAPDataRows);
                t8SAPTableSections.add(t8SAPTableSection);

            }

            t8SAPTable.setTableSections(t8SAPTableSections);
            t8SAPTables.add(t8SAPTable);
        }

        return t8SAPTables;
    }
}


