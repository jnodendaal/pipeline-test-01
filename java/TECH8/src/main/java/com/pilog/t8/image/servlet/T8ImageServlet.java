package com.pilog.t8.image.servlet;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.gfx.image.T8ImageDefinition;
import com.pilog.t8.mainserver.T8MainServer;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andre Scheepers
 */
public class T8ImageServlet extends HttpServlet
{
    private ServletConfig config;
    private T8ServerContext serverContext;
    private T8DefinitionManager definitionManager;

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        this.config = config;
        serverContext = (T8ServerContext) config.getServletContext().getAttribute(T8MainServer.T8_SERVER_CONTEXT_IDENTIFIER);
        definitionManager = (T8DefinitionManager) serverContext.getDefinitionManager();
    }

    @Override
    public ServletConfig getServletConfig()
    {
        return config;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        T8ImageDefinition imageDefinition;
        BufferedImage bufferedImage;
        String imageId;

        imageId = request.getPathInfo().substring(1);

        if (imageId == null) throw new IllegalArgumentException("The Image Identifier must be provided.");
        if(!T8IdentifierUtilities.isPublicId(imageId)) throw new IllegalArgumentException("The Image Identifier specified is not a valid T8 Definitin Identifier.");

        try
        {
            imageDefinition = (T8ImageDefinition) definitionManager.getRawDefinition(null, null, imageId);
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to retrieve image " + imageId, ex);
            throw new ServletException("Fail to retrieve Image Definition " + imageId, ex);
        }

        if (imageDefinition == null) throw new IllegalArgumentException("The Image Definition not found " + imageId + ".");
        bufferedImage = imageDefinition.getImage().getBufferedImage();

        response.setContentType("image/png");

        try
        {
            ImageIO.write(bufferedImage, "PNG", response.getOutputStream());
            //Release resources
            response.getOutputStream().close();
        }
        catch(IOException ex)
        {
            T8Log.log("Failed to write image " + imageId + " to output stream.", ex);
            throw new ServletException("Failed to write image " + imageId + " to output stream.");
        }
    }

    @Override
    public String getServletInfo()
    {
        return "ImageServlet";
    }
}