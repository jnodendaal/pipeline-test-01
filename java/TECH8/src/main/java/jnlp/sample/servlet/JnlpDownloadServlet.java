/*
 * Copyright (c) 2006, 2010, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * -Redistribution of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 * -Redistribution in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * Neither the name of Oracle nor the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN MICROSYSTEMS, INC. ("SUN")
 * AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 * EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You acknowledge that this software is not designed, licensed or intended
 * for use in the design, construction, operation or maintenance of any
 * nuclear facility.
 */
package jnlp.sample.servlet;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * This Servlet class is an implementation of JNLP Specification's
 * Download Protocols.
 *
 * All requests to this Servlet is in the form of HTTP GET commands.
 * The parameters that are needed are:
 * <ul>
 * <li><code>arch</code>,
 * <li><code>os</code>,
 * <li><code>locale</code>,
 * <li><code>version-id</code> or <code>platform-version-id</code>,
 * <li><code>current-version-id</code>,
 * <li>code>known-platforms</code>
 * </ul>
 * <p>
 *
 * @version 1.8 01/23/03
 */
public class JnlpDownloadServlet extends HttpServlet
{

    // Localization
    private static ResourceBundle _resourceBundle = null;
    // Servlet configuration
    private static final String PARAM_JNLP_EXTENSION = "jnlp-extension";
    private static final String PARAM_JAR_EXTENSION = "jar-extension";
    // Servlet configuration
    private JnlpFileHandler _jnlpFileHandler = null;
    private ResourceCatalog _resourceCatalog = null;

    /**
     * Initializes the {@code JnlpDownloadServlet}.
     *
     * @param config The {@code ServletConfig}.
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        System.out.println("============= JnlpDownloadServlet Initialized =============");

        // Get extension from Servlet configuration, or use default
        JnlpResource.setDefaultExtensions(config.getInitParameter(PARAM_JNLP_EXTENSION), config.getInitParameter(PARAM_JAR_EXTENSION));

        _jnlpFileHandler = new JnlpFileHandler(config.getServletContext());
        _resourceCatalog = new ResourceCatalog(config.getServletContext());
    }

    public static synchronized ResourceBundle getResourceBundle()
    {
        if (_resourceBundle == null)
            _resourceBundle = ResourceBundle.getBundle("jnlp/sample/servlet/resources/strings");

        return _resourceBundle;
    }

    @Override
    public void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handleRequest(request, response, true);
    }

    /**
     * We handle get requests too - even though the spec. only requires POST requests
     *
     * @param request The {@code HttpServletRequest} for the JNLP.
     * @param response The {@code HttpServletResponse} for the JNLP.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handleRequest(request, response, false);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response, boolean isHead) throws IOException
    {
        DownloadRequest downloadRequest;
        long ifModifiedSince;

        downloadRequest = new DownloadRequest(getServletContext(), request);
        ifModifiedSince = request.getDateHeader("If-Modified-Since");

        try
        {
            JnlpResource jnlpResource;
            DownloadResponse downloadResource;

            validateRequest(downloadRequest);
            jnlpResource = locateResource(downloadRequest);

            // isHead only checks the content Length and no
            if (isHead)
            {
                int contentLength;

                contentLength = jnlpResource.getResource().openConnection().getContentLength();
                downloadResource = DownloadResponse.getHeadRequestResponse(jnlpResource.getMimeType(), jnlpResource.getVersionId(), jnlpResource.getLastModified(), contentLength);
            }
            else if (ifModifiedSince != -1 && (ifModifiedSince / 1000) >= (jnlpResource.getLastModified() / 1000))
            {
                // We divide the value returned by getLastModified here by 1000
                // because if protocol is HTTP, last 3 digits will always be
                // zero. However, if protocol is JNDI, that's not the case.
                // so we divide the value by 1000 to remove the last 3 digits
                // before comparison

                // return 304 not modified if possible
                downloadResource = DownloadResponse.getNotModifiedResponse();
            }
            else
            {
                downloadResource = constructResponse(jnlpResource, downloadRequest);
            }

            downloadResource.sendRespond(response);
        }
        catch (ErrorResponseException ere)
        {
            ere.getDownloadResponse().sendRespond(response);
        }
        catch (Throwable e)
        {
            if (e.getCause() != null && !(e.getCause() instanceof SocketException))
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Make sure that it is a valid request. This is also the place to implement the
     * reverse IP lookup
     *
     * @param downloadRequest The {@code DownloadRequest} fro the JNLP.
     * @throws ErrorResponseException
     */
    private void validateRequest(DownloadRequest downloadRequest) throws ErrorResponseException
    {
        String path = downloadRequest.getPath();
        if (path.endsWith(ResourceCatalog.VERSION_XML_FILENAME) || path.contains("__"))
        {
            throw new ErrorResponseException(DownloadResponse.getNoContentResponse());
        }
    }

    /**
     * Interprets the download request and convert it into a resource that is
     * part of the Web Archive.
     */
    private JnlpResource locateResource(DownloadRequest dreq) throws IOException, ErrorResponseException
    {
        if (dreq.getVersion() == null)
        {
            return handleBasicDownload(dreq);
        }
        else
        {
            return handleVersionRequest(dreq);
        }
    }

    private JnlpResource handleBasicDownload(DownloadRequest dreq) throws ErrorResponseException, IOException
    {
        // Do not return directory names for basic protocol
        if (dreq.getPath() == null || dreq.getPath().endsWith("/"))
        {
            throw new ErrorResponseException(DownloadResponse.getNoContentResponse());
        }
        // Lookup resource
        JnlpResource jnlpres = new JnlpResource(getServletContext(), dreq.getPath());
        if (!jnlpres.exists())
        {
            throw new ErrorResponseException(DownloadResponse.getNoContentResponse());
        }
        return jnlpres;
    }

    private JnlpResource handleVersionRequest(DownloadRequest dreq) throws IOException, ErrorResponseException
    {
        return _resourceCatalog.lookupResource(dreq);
    }

    /**
     * Given a DownloadPath and a DownloadRequest, it constructs the data stream to return
     * to the requester.
     */
    private DownloadResponse constructResponse(JnlpResource jnlpResource, DownloadRequest downloadRequest) throws IOException
    {
        JnlpResource newJnlpResource;

        if (jnlpResource.isJnlpFile())
        {
            return _jnlpFileHandler.getJnlpFileEx(jnlpResource, downloadRequest);
        }

        // Check and see if we can use pack resource
        newJnlpResource = new JnlpResource(getServletContext(), jnlpResource.getName(), jnlpResource.getVersionId(), jnlpResource.getOSList(),
                jnlpResource.getArchList(), jnlpResource.getLocaleList(), jnlpResource.getPath(), jnlpResource.getReturnVersionId(), downloadRequest.getEncoding());

        // Return WAR file resource
        if (newJnlpResource.exists())
            return DownloadResponse.getFileDownloadResponse(newJnlpResource.getResource(), newJnlpResource.getMimeType(), newJnlpResource.getLastModified(), newJnlpResource.getReturnVersionId());
        else
            return DownloadResponse.getFileDownloadResponse(jnlpResource.getResource(), jnlpResource.getMimeType(), jnlpResource.getLastModified(), jnlpResource.getReturnVersionId());
    }
}
