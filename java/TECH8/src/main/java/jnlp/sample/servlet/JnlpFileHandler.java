/*
 * Copyright (c) 2006, 2010, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * -Redistribution of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 * -Redistribution in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * Neither the name of Oracle nor the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN MICROSYSTEMS, INC. ("SUN")
 * AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 * EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You acknowledge that this software is not designed, licensed or intended
 * for use in the design, construction, operation or maintenance of any
 * nuclear facility.
 */
package jnlp.sample.servlet;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.system.T8SystemDetails;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.webservices.utils.WebServiceUtils;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TimeZone;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The JNLP file handler implements a class that keeps
 * track of JNLP files and their specializations
 */
public class JnlpFileHandler
{

    private static final String JNLP_MIME_TYPE = "application/x-java-jnlp-file";
    private final ServletContext _servletContext;
    private HashMap _jnlpFiles = null;

    /**
     * Initializes the {@code JnlpFileHandler} for this specific instance of the Servlet.
     *
     * @param servletContext The {@code ServletContext} passed from the initialized Servlet.
     */
    public JnlpFileHandler(ServletContext servletContext)
    {
        _servletContext = servletContext;
        _jnlpFiles = new HashMap();
    }

    private static class JnlpFileEntry
    {

        DownloadResponse _response;
        private final long _lastModified;

        JnlpFileEntry(DownloadResponse response, long lastmodfied)
        {
            _response = response;
            _lastModified = lastmodfied;
        }

        public DownloadResponse getResponse()
        {
            return _response;
        }

        long getLastModified()
        {
            return _lastModified;
        }
    }

    public synchronized DownloadResponse getJnlpFileEx(JnlpResource jnlpResource, DownloadRequest downloadRequest) throws IOException
    {
        URL resource;
        long lastModified;
        long timeStamp;
        String reqUrl;
        String mimeType;
        String path;
        JnlpFileEntry jnlpFile;
        String jnlpFileContent;

        path = jnlpResource.getPath();
        resource = jnlpResource.getResource();
        lastModified = jnlpResource.getLastModified();
        reqUrl = downloadRequest.getHttpRequest().getRequestURL().toString();

        // SQE: To support query string, we changed the hash key from Request URL to (Request URL + query string)
        if (downloadRequest.getQuery() != null)
            reqUrl += downloadRequest.getQuery();

        // Check if entry already exist in HashMap
        {
            jnlpFile = (JnlpFileEntry)_jnlpFiles.get(reqUrl);

            if (jnlpFile != null && jnlpFile.getLastModified() == lastModified)
            {
                // Entry found in cache, so return it
                return jnlpFile.getResponse();
            }
        }

        timeStamp = lastModified;
        mimeType = _servletContext.getMimeType(path);

        if (mimeType == null)
            mimeType = JNLP_MIME_TYPE;

        // Read information from WAR file
        {
            StringBuilder jnlpFileTemplate;
            URLConnection conn;
            BufferedReader bufferedReader;
            String line;

            jnlpFileTemplate = new StringBuilder();
            conn = resource.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            line = bufferedReader.readLine();

            if (line != null && line.startsWith("TS:"))
            {
                timeStamp = parseTimeStamp(line.substring(3));
                if (timeStamp == 0)
                {
                    timeStamp = lastModified;
                }
                line = bufferedReader.readLine();
            }

            while (line != null)
            {
                jnlpFileTemplate.append(line);
                line = bufferedReader.readLine();
            }

            jnlpFileContent = specializeJnlpTemplate(downloadRequest.getHttpRequest(), path, jnlpFileTemplate.toString());
        }

        /* SQE: We need to add query string back to href in jnlp file. We also need to handle JRE requirement for
         * the test. We reconstruct the xml DOM object, modify the value, then regenerate the jnlpFileContent.
         */
        {
            String query = downloadRequest.getQuery();
            String testJRE = downloadRequest.getTestJRE();
            // For backward compatibility: Always check if the href value exists.
            // Bug 4939273: We will retain the jnlp template structure and will NOT add href value. Above old
            // approach to always check href value caused some test case not run.
            if (query != null)
            {
                byte[] jnlpByteArray = jnlpFileContent.getBytes("UTF-8");
                ByteArrayInputStream byteArrayInput = new ByteArrayInputStream(jnlpByteArray);
                try
                {
                    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    Document document = builder.parse(byteArrayInput);
                    if (document != null && document.getNodeType() == Node.DOCUMENT_NODE)
                    {
                        boolean modified = false;
                        Element root = document.getDocumentElement();

                        if (root.hasAttribute("href"))
                        {
                            String href = root.getAttribute("href");
                            root.setAttribute("href", href + "?" + query);
                            modified = true;
                        }

                        // Update version value for j2se tag
                        if (testJRE != null)
                        {
                            NodeList j2seNL = root.getElementsByTagName("j2se");
                            if (j2seNL != null)
                            {
                                Element j2se = (Element)j2seNL.item(0);
                                String ver = j2se.getAttribute("version");
                                if (ver.length() > 0)
                                {
                                    j2se.setAttribute("version", testJRE);
                                    modified = true;
                                }
                            }
                        }

                        TransformerFactory tFactory = TransformerFactory.newInstance();
                        Transformer transformer = tFactory.newTransformer();
                        DOMSource source = new DOMSource(document);
                        StringWriter sw = new StringWriter();
                        StreamResult result = new StreamResult(sw);
                        transformer.transform(source, result);
                        jnlpFileContent = sw.toString();

                        // Since we modified the file on the fly, we always update the timestamp value with current time
                        if (modified)
                            timeStamp = new java.util.Date().getTime();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        // Convert to bytes as a UTF-8 encoding
        byte[] byteContent = jnlpFileContent.getBytes("UTF-8");

        // Create entry
        DownloadResponse newDownloadResponse = DownloadResponse.getFileDownloadResponse(byteContent, mimeType, timeStamp, jnlpResource.getReturnVersionId());
        jnlpFile = new JnlpFileEntry(newDownloadResponse, lastModified);
        _jnlpFiles.put(reqUrl, jnlpFile);

        return newDownloadResponse;
    }

    /**
     * This Method replaces certain place holders in the JNLP file.
     *
     * @param request The {@code HttpServletRequest} that the JNLP is requested from.
     * @param requestPath
     * @param jnlpTemplate The {@code String} variable containing the XML fro the APPLICATION_TEMPLATE.JNLP contained in the Main JAR.
     * @return
     */
    @SuppressWarnings("AssignmentToMethodParameter")
    private String specializeJnlpTemplate(HttpServletRequest request, String requestPath, String jnlpTemplate)
    {
        String urlprefix;
        int idx;
        String name;
        String codebase;
        Enumeration<String> parameterNames;
        T8ServerContext t8ServerInternalContext;

        urlprefix = getUrlPrefix(request);

        idx = requestPath.lastIndexOf('/');
        name = requestPath.substring(idx + 1);
        codebase = requestPath.substring(0, idx + 1);

        // We need to process the request parameters first, so that malicous attempts cannot be made at chaning the hostname or codebase etc.
        parameterNames = request.getParameterNames();
        t8ServerInternalContext = WebServiceUtils.getT8ServerContext(request.getServletContext());
        while (parameterNames.hasMoreElements())
        {
            String parameterName;
            String parameterValue;

            parameterName = parameterNames.nextElement();
            //Parameter values provided already decoded
            parameterValue = request.getParameter(parameterName);

            if (Strings.isNullOrEmpty(parameterValue))
                parameterValue = "";

            // The place holder parameter with the given value from the request Parameter.
            jnlpTemplate = substitute(jnlpTemplate, "$$" + parameterName, parameterName + "=" + parameterValue);
        }

        jnlpTemplate = substitute(jnlpTemplate, "$$name", name);
        // fix for 5039951: Add $$hostname macro
        jnlpTemplate = substitute(jnlpTemplate, "$$hostname", request.getServerName());
        jnlpTemplate = substitute(jnlpTemplate, "$$codebase", urlprefix + request.getContextPath() + codebase);
        // fix for 6256326: add $$site macro to sample jnlp servlet
        jnlpTemplate = substitute(jnlpTemplate, "$$site", urlprefix);

        // Gets the T8 System Details and replaces the place holder Property.
        try
        {
            T8SystemDetails systemDetails;

            // Get the system details.
            systemDetails = t8ServerInternalContext.getDefinitionManager().getSystemDetails();

            /**
             * The context will be either an internal or external binding. Internal
             * binding requires for an additional request parameter to be present.
             * The expectation is clients accessing the application through a portal
             * which indicates 'external' access. Internal assumes that the request
             * URL is the correct to be used when accessing the application.
             */
            if (request.getParameter("internal") != null) jnlpTemplate = substitute(jnlpTemplate, "$$context", urlprefix + request.getContextPath());
            else jnlpTemplate = substitute(jnlpTemplate, "$$context", systemDetails.getExternalBinding() + request.getContextPath());

            // Substitute the rest of the system configuration variables.
            jnlpTemplate = substitute(jnlpTemplate, "$$SystemName", systemDetails.getDisplayName());
            jnlpTemplate = substitute(jnlpTemplate, "$$ProductIcon_Configuration", systemDetails.getProductConfigurationIconIdentifier());
            jnlpTemplate = substitute(jnlpTemplate, "$$ProductIcon_Client", systemDetails.getProductClientIconIdentifier());
            jnlpTemplate = substitute(jnlpTemplate, "$$ProductIcon_Developer", systemDetails.getProductDeveloperIconIdentifier());
            jnlpTemplate = substitute(jnlpTemplate, "$$ProductSplash", systemDetails.getProductSplashIconIdentifier());
        }
        catch (Exception ex)
        {
            System.out.println("Failed to set all the system details. Attempting to set defaults.");
            jnlpTemplate = substituteDefaults(jnlpTemplate);
            ex.printStackTrace();
        }
        return jnlpTemplate;
    }

    /**
     * This Method derives the URL Prefix from the {@code HttpServletRequest}.
     *
     * @param request The {@code HttpServletRequest} to get the URL Prefix from.
     * @return The {@code String} URL Prefix.
     */
    private String getUrlPrefix(HttpServletRequest request)
    {
        StringBuilder urlPrefix;
        String scheme;
        int port;

        urlPrefix = new StringBuilder();
        scheme = request.getScheme();
        port = request.getServerPort();

        // A Prefix Example : http://localhost
        urlPrefix.append(scheme).append("://").append(request.getServerName());

        // If the this is a custom port and not the default ports for HTTP or HTTPS than add is to the url.
        if ((scheme.equals("http") && port != 80) || (scheme.equals("https") && port != 443))
        {
            urlPrefix.append(':');
            urlPrefix.append(request.getServerPort());
        }
        return urlPrefix.toString();
    }

    private String substitute(String target, String key, String replacementString)
    {
        int start = 0;
        do
        {
            int replacementStartIndex;

            replacementStartIndex = target.indexOf(key, start);
            if (replacementStartIndex == -1)
            {
                return target;
            }
            else
            {
                int replacementLength;

                replacementLength = replacementString != null ? replacementString.length() : 4; // Length 4 for 'null' value.
                target = target.substring(0, replacementStartIndex) + replacementString + target.substring(replacementStartIndex + key.length());
                start = replacementStartIndex + replacementLength;
            }
        }
        while (true);
    }

    private String substituteDefaults(String jnlpTemplate)
    {
        String returnString;

        returnString = jnlpTemplate;
        returnString = substitute(returnString, "$$SystemName", "Tech 8");
        returnString = substitute(returnString, "$$ProductIcon_Configuration", "@ICN_DEFAULT_INSTALL");
        returnString = substitute(returnString, "$$ProductIcon_Client", "@ICN_DEFAULT_INSTALL");
        returnString = substitute(returnString, "$$ProductIcon_Developer", "@ICN_DEFAULT_INSTALL");

        return returnString;
    }

    /** Parses a ISO 8601 Timestamp. The format of the timestamp is:
     *
     * YYYY-MM-DD hh:mm:ss or YYYYMMDDhhmmss
     *
     * Hours (hh) is in 24h format. ss are optional. Time are by default relative
     * to the current timezone. Timezone information can be specified
     * by:
     *
     * - Appending a 'Z', e.g., 2001-12-19 12:00Z
     * - Appending +hh:mm, +hhmm, +hh, -hh:mm -hhmm, -hh to
     * indicate that the locale timezone used is either the specified
     * amound before or after GMT. For example,
     *
     * 12:00Z = 13:00+1:00 = 0700-0500
     *
     * The method returns 0 if it cannot pass the string. Otherwise, it is
     * the number of milliseconds size sometime in 1969.
     */
    private long parseTimeStamp(String timestamp)
    {
        int YYYY = 0;
        int MM = 0;
        int DD = 0;
        int hh = 0;
        int mm = 0;
        int ss = 0;

        timestamp = timestamp.trim();
        try
        {
            // Check what format is used
            if (matchPattern("####-##-## ##:##", timestamp))
            {
                YYYY = getIntValue(timestamp, 0, 4);
                MM = getIntValue(timestamp, 5, 7);
                DD = getIntValue(timestamp, 8, 10);
                hh = getIntValue(timestamp, 11, 13);
                mm = getIntValue(timestamp, 14, 16);
                timestamp = timestamp.substring(16);
                if (matchPattern(":##", timestamp))
                {
                    ss = getIntValue(timestamp, 1, 3);
                    timestamp = timestamp.substring(3);
                }
            }
            else if (matchPattern("############", timestamp))
            {
                YYYY = getIntValue(timestamp, 0, 4);
                MM = getIntValue(timestamp, 4, 6);
                DD = getIntValue(timestamp, 6, 8);
                hh = getIntValue(timestamp, 8, 10);
                mm = getIntValue(timestamp, 10, 12);
                timestamp = timestamp.substring(12);
                if (matchPattern("##", timestamp))
                {
                    ss = getIntValue(timestamp, 0, 2);
                    timestamp = timestamp.substring(2);
                }
            }
            else
            {
                // Unknown format
                return 0;
            }
        }
        catch (NumberFormatException e)
        {
            // Bad number
            return 0;
        }

        String timezone = null;
        // Remove timezone information
        timestamp = timestamp.trim();
        if (timestamp.equalsIgnoreCase("Z"))
        {
            timezone = "GMT";
        }
        else if (timestamp.startsWith("+") || timestamp.startsWith("-"))
        {
            timezone = "GMT" + timestamp;
        }

        if (timezone == null)
        {
            // Date is relative to current locale
            Calendar cal = Calendar.getInstance();
            cal.set(YYYY, MM - 1, DD, hh, mm, ss);
            return cal.getTime().getTime();
        }
        else
        {
            // Date is relative to a timezone
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
            cal.set(YYYY, MM - 1, DD, hh, mm, ss);
            return cal.getTime().getTime();
        }
    }

    private int getIntValue(String key, int start, int end)
    {
        return Integer.parseInt(key.substring(start, end));
    }

    private boolean matchPattern(String pattern, String key)
    {
        // Key must be longer than pattern
        if (key.length() < pattern.length())
            return false;
        for (int i = 0; i < pattern.length(); i++)
        {
            char format = pattern.charAt(i);
            char ch = key.charAt(i);
            if (!((format == '#' && Character.isDigit(ch)) || (format == ch)))
            {
                return false;
            }
        }
        return true;
    }
}
