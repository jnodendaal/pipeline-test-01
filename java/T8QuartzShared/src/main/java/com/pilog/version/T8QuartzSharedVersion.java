package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8QuartzSharedVersion
{
    public final static String VERSION = "30";
}
