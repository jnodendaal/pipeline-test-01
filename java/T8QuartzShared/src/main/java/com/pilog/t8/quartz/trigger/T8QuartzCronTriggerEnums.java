package com.pilog.t8.quartz.trigger;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzCronTriggerEnums
{
    /**
     * This class representation of the quartz compatible missfire instructions.
     */
    public static enum MissfireInstructions
    {
        MISFIRE_INSTRUCTION_SMART_POLICY("Smart Policy"),
        MISFIRE_INSTRUCTION_DO_NOTHING("Do Nothing"),
        MISFIRE_INSTRUCTION_FIRE_ONCE_NOW("Fire Once Now"),
        MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY("Ignore Misfire Policy");

        private final String displayName;

        private MissfireInstructions(String displayName)
        {
            this.displayName = displayName;
        }

        @Override
        public String toString()
        {
            return this.displayName;
        }
    }
}
