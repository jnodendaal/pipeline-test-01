package com.pilog.t8.definition.quartz.trigger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.quartz.trigger.IT8QuartzTrigger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public abstract class T8QuartzTriggerDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_QUARTZ_TRIGGERS";
    public static final String IDENTIFIER_PREFIX = "QUARTZ_TRIG_";
    public enum Datum {
        ACTIVE,
        /** The start date that the trigger will be made active */
        START_DATE,
        /** The end date after which the trigger will be made inactive */
        END_DATE,
        /** The priority of this trigger */
        PRIORITY,
        /** The missfire instruction of this trigger */
        MISSFIRE_INSTRUCTION};
    // -------- Definition Meta-Data -------- //

    public T8QuartzTriggerDefinition(String identifier)
    {
        super(identifier);
    }

    /**
     * All subclasses must implement this method to create a new instance of their representative class.
     * @param serverContext The server's internal context.
     * @param serviceDefinition
     *
     * @return The newly created instance.
     */
    public abstract IT8QuartzTrigger instantiateTrigger(T8ServerContext serverContext, T8QuartzServiceDefinition serviceDefinition);

    /**
     * All subclasses must implement this method to get the allowed missfire instructions for that trigger.
     * @return A Hashmap containing the missfire value and user friendly name.
     */
    public abstract HashMap<Integer, String> getMissfireInstructions();

    /**
     * Builds a descriptive string for the trigger to be displayed for a client
     * user.
     *
     * @return The user-friendly descriptive {@code String}
     */
    public abstract String getTriggerDescriptiveString();

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "If this trigger should be used.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.START_DATE.toString(), "Trigger Start Date", "The start date on which the trigger will first be made active and start following its schedule."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DATE_TIME, Datum.END_DATE.toString(), "Trigger End Date", "The end date after which the trigger will be made inactive and will no longer be executed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PRIORITY.toString(), "Trigger Priority", "The trigger priority will be used to determine the order in which triggers are fired, and also how the default misfire instruction will behave."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MISSFIRE_INSTRUCTION.toString(), "Trigger Missfire instruction", "The misfire instruction tells the trigger what to do in the event that it did not fire correctly", 1, T8DefinitionDatumType.T8DefinitionDatumOptionType.ENUMERATION));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MISSFIRE_INSTRUCTION.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> datumOptions = new ArrayList<>();
            HashMap<Integer,String> missfireInstructions = getMissfireInstructions();
            for (Integer key : missfireInstructions.keySet())
            {
                datumOptions.add(new T8DefinitionDatumOption(missfireInstructions.get(key), key));
            }
            return datumOptions;
        }
        else return null;
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (getPriority() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.PRIORITY.toString(), "Priority for trigger cannot be empty. Enter valid numeric value.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if (getMissFireInscruction() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.MISSFIRE_INSTRUCTION.toString(), "Priority for trigger cannot be empty. Select an appropriate value.", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    public Long getStartDateMillis()
    {
        return getDefinitionDatum(Datum.START_DATE);
    }

    public Date getStartDate()
    {
        Long millis;

        millis = getDefinitionDatum(Datum.START_DATE);
        return millis != null ? new Date(millis) : null;
    }

    public void setStartDateMillis(Long millis)
    {
        setDefinitionDatum(Datum.START_DATE, millis);
    }

    public void setStartDate(Date date)
    {
        setDefinitionDatum(Datum.START_DATE, date != null ? date.getTime() : null);
    }

    public Long getEndDateMillis()
    {
        return getDefinitionDatum(Datum.END_DATE);
    }

    public Date getEndDate()
    {
        Long millis;

        millis = getDefinitionDatum(Datum.END_DATE);
        return millis != null ? new Date(millis) : null;
    }

    public void setEndDateMillis(Long millis)
    {
        setDefinitionDatum(Datum.END_DATE, millis);
    }

    public void setEndDate(Date date)
    {
        setDefinitionDatum(Datum.END_DATE, date != null ? date.getTime() : null);
    }

    public Integer getPriority()
    {
        return getDefinitionDatum(Datum.PRIORITY);
    }

    public void setPriority(Integer priority)
    {
        setDefinitionDatum(Datum.PRIORITY, priority);
    }

    public Integer getMissFireInscruction()
    {
        return getDefinitionDatum(Datum.MISSFIRE_INSTRUCTION);
    }

    public void setMissFireInscruction(Integer instruction)
    {
        setDefinitionDatum(Datum.MISSFIRE_INSTRUCTION, instruction);
    }

    public Boolean isActive()
    {
        return getDefinitionDatum(Datum.ACTIVE);
    }

    public void setActive(Boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE, active);
    }
}
