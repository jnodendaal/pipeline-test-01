package com.pilog.t8.definition.quartz.service;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.service.T8Service;
import com.pilog.t8.definition.quartz.task.T8QuartzProcessTaskDefinition;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.definition.service.T8ServiceOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzServiceDefinition extends T8ServiceDefinition
{
     // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SYSTEM_SERVICE_QUARTZ_SCHEDULER";
    public static final String DISPLAY_NAME = "Quartz Scheduler Service";
    public static final String DESCRIPTION = "A Scheduler service used to schedule jobs that should do certain tasks at specified times.";
    public enum Datum
    {
                    TASKS
    };
    // -------- Definition Meta-Data -------- //

    public T8QuartzServiceDefinition(String identifier)
    {
        super(identifier);
    }

    /**
     * Returns a new instance of the T8SchedulerService class
     *
     * @param serverContext The server's internal context
     *
     * @return A new instance of T8SchedulerService
     */
    @Override
    public T8Service getNewServiceInstance(T8ServerContext serverContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.quartz.T8QuartzService", new Class<?>[]{T8ServerContext.class, this.getClass()}, serverContext, this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TASKS.toString(), "Attached Tasks", "The tasks attached to this scheduler instance."));

        return datumTypes;
    }


    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.TASKS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8QuartzProcessTaskDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }


    @Override
    public ArrayList<T8ServiceOperationDefinition> getOperationDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.service.quartz.T8QuartzServiceDefinitionActionHandler", new Class<?>[]{T8Context.class, this.getClass()}, context, this);
    }

    public List<T8QuartzProcessTaskDefinition> getTasks()
    {
        return getDefinitionDatum(Datum.TASKS);
    }

    public T8QuartzProcessTaskDefinition getTask(String taskIdentifier)
    {
        return (T8QuartzProcessTaskDefinition) getLocalDefinition(taskIdentifier);
    }
}
