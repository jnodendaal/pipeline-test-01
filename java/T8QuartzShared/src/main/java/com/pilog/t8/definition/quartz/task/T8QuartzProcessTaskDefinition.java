package com.pilog.t8.definition.quartz.task;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.definition.quartz.trigger.T8QuartzTriggerDefinition;
import com.pilog.t8.definition.script.T8QuartProcessParameterCreationScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzProcessTaskDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_QUARTZ_PROCESS_TASKS";
    public static final String IDENTIFIER_PREFIX = "QUARTZ_TASK_";
    public static final String TYPE_IDENTIFIER = "@DT_QUARTZ_PROCESS_TASK";
    public static final String DISPLAY_NAME = "Process Task";
    public static final String DESCRIPTION = "A Quartz Scheduler task to trigger a T8Process.";
    public static final String VERSION = "0";
    public enum Datum
    {
        ACTIVE,
        TASK_NAME,
        TASK_DESCRIPTION,
        ORGANIZATION_IDENTIFIER,
        PROCESS_IDENTIFIER,
        PROCESS_PARAMETER_SCRIPT_DEFINITION,
        TRIGGERS
    };
    // -------- Definition Meta-Data -------- //

    public T8QuartzProcessTaskDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "If this task should be allowed to execute.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TASK_NAME.toString(), "Task Name", "The simple name for this task to be used in the system."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TASK_DESCRIPTION.toString(), "Task Description", "The description for this task to be used in the system."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.ORGANIZATION_IDENTIFIER.toString(), "Organization Identifier", "The organization identifier that will be used when generating terms etc."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROCESS_IDENTIFIER.toString(), "Process", "The process to execute."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.PROCESS_PARAMETER_SCRIPT_DEFINITION.toString(), "Parameter Script", "The script to execute in order to compile a map of input parametesr for the process."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TRIGGERS.toString(), "Task Triggers", "The triggers attached to this task instance."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.PROCESS_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ProcessDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROCESS_PARAMETER_SCRIPT_DEFINITION.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8QuartProcessParameterCreationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.TRIGGERS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8QuartzTriggerDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.service.quartz.T8QuartzTaskDefinitionActionHandler", new Class<?>[]{T8Context.class, this.getClass()}, context, this);
    }

    public String getOrganizationIdentifier()
    {
        return getDefinitionDatum(Datum.ORGANIZATION_IDENTIFIER);
    }

    public void setOrganizationIdentifier(String organizationIdentifier)
    {
        setDefinitionDatum(Datum.ORGANIZATION_IDENTIFIER, organizationIdentifier);
    }

    public String getProcessIdentifier()
    {
        return getDefinitionDatum(Datum.PROCESS_IDENTIFIER);
    }

    public void setProcessIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROCESS_IDENTIFIER, identifier);
    }

    public T8QuartProcessParameterCreationScriptDefinition getProcessParameterScript()
    {
        return getDefinitionDatum(Datum.PROCESS_PARAMETER_SCRIPT_DEFINITION);
    }

    public void setProcessParameterScript(T8QuartProcessParameterCreationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.PROCESS_PARAMETER_SCRIPT_DEFINITION, definition);
    }

    public T8QuartzTriggerDefinition getTrigger(String identifier)
    {
        return (T8QuartzTriggerDefinition) getLocalDefinition(identifier);
    }

    public ArrayList<T8QuartzTriggerDefinition> getTriggers()
    {
        return getDefinitionDatum(Datum.TRIGGERS);
    }

    public void setTriggers(ArrayList<T8QuartzTriggerDefinition> definitions)
    {
        setDefinitionDatum(Datum.TRIGGERS, definitions);
    }

    public Boolean isActive()
    {
        return getDefinitionDatum(Datum.ACTIVE);
    }

    public void setActive(Boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE, active);
    }

    public String getTaskName()
    {
        return getDefinitionDatum(Datum.TASK_NAME);
    }

    public void setTaskName(String taskName)
    {
        setDefinitionDatum(Datum.TASK_NAME, taskName);
    }

    public String getTaskDescription()
    {
        return getDefinitionDatum(Datum.TASK_DESCRIPTION);
    }

    public void setTaskDescription(String taskDescription)
    {
        setDefinitionDatum(Datum.TASK_DESCRIPTION, taskDescription);
    }
}
