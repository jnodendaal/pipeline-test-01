package com.pilog.t8.quartz.trigger;

import org.quartz.Trigger;

/**
 * @author Gavin Boshoff
 */
public interface IT8QuartzTrigger
{
    /**
     * All sub classes must implement this method to build a quartz compatible
     * trigger.
     *
     * @return Quartz compatible trigger
     */
    Trigger buildQuartzTrigger();

    int getMissFireInstruction();

    String getName();

    void setMissFireInstruction(int missFireInstruction);

    public String getGroup();
}