package com.pilog.t8.definition.quartz.service;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.service.T8ServiceOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8QuartzServiceAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    // Server operations.
    public static final String OPERATION_TRIGGER_TASK = "@OP_QUARTZ_SCHEDULER_TRIGGER_TASK";
    public static final String OPERATION_RESTART = "@OP_QUARTZ_SCHEDULER_RESTART";

    //Input Parameters
    public static final String PARAMETER_TASK_IDENTIFIER = "$P_TASK_IDENTIFIER";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8ServiceOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8ServiceOperationDefinition(OPERATION_TRIGGER_TASK);
            definition.setMetaDisplayName("Trigger Task");
            definition.setMetaDescription("This operation will trigger a task for immediate execution.");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TASK_IDENTIFIER, "Task Identifier", "The identifier of the task to be executed.", T8DataType.DEFINITION_IDENTIFIER));
            definitions.add(definition);

            definition = new T8ServiceOperationDefinition(OPERATION_RESTART);
            definition.setMetaDisplayName("Restart");
            definition.setMetaDescription("Restarts the scheduler service, loading all new definitions.");
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}