package com.pilog.t8.definition.quartz.trigger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.quartz.trigger.IT8QuartzTrigger;
import com.pilog.t8.quartz.trigger.T8QuartzSimpleTriggerEnums;
import com.pilog.t8.quartz.trigger.T8QuartzSimpleTriggerEnums.IntervalFields;
import com.pilog.t8.quartz.trigger.T8QuartzSimpleTriggerEnums.MissfireInstructions;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzSimpleTriggerDefinition extends T8QuartzTriggerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_QUARTZ_TRIGGER_SIMPLE";
    public static final String DISPLAY_NAME = "Simple Trigger";
    public static final String DESCRIPTION = "A trigger for firing scheduler jobs based on a simple trigger schedule.";
    public static final String VERSION = "0";
    public enum Datum {
        /**
         * The amount of times this trigger will repeat
         */
        REPEAT_AMOUNT,
        /**
         * The interval at which this trigger will repeat
         */
        INTERVAL,
        /**
         * The field that will be used to determine repeat interval
         */
        INTERVAL_FIELD
    };
    // -------- Definition Meta-Data -------- //

    public T8QuartzSimpleTriggerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public IT8QuartzTrigger instantiateTrigger(T8ServerContext serverContext, T8QuartzServiceDefinition serviceDefinition)
    {
        return T8Reflections.getInstance("com.pilog.t8.quartz.trigger.T8QuartzSimpleTrigger", new Class<?>[]{T8ServerContext.class, T8QuartzSimpleTriggerDefinition.class, T8QuartzServiceDefinition.class}, serverContext, this, serviceDefinition);
    }

    @Override
    public HashMap<Integer, String> getMissfireInstructions()
    {
        HashMap<Integer,String> missfireInstruction = new HashMap<>();

        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_FIRE_NOW.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_FIRE_NOW.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_SMART_POLICY.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_SMART_POLICY.toString());

        return missfireInstruction;
    }

    @Override
    public String getTriggerDescriptiveString()
    {
        StringBuilder descriptionBuilder;
        SimpleDateFormat dateFormat;

        dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        descriptionBuilder = new StringBuilder("Execute from ");
        descriptionBuilder.append(dateFormat.format(getStartDate()));
        // Check the end date value to add appropriate value
        if (getEndDate() == null)
        {
            descriptionBuilder.append(" indefinitely");
        } else descriptionBuilder.append(" to ").append(dateFormat.format(getEndDate()));
        // Add the interval section
        descriptionBuilder.append(" every ");
        descriptionBuilder.append(getInterval());
        descriptionBuilder.append(" ").append(getIntervalField());
        // Add the max repititions, if specified
        if (getRepeatAmount() != null && getRepeatAmount() != 0)
        {
            descriptionBuilder.append(", a maximum of ");
            descriptionBuilder.append(getRepeatAmount());
            descriptionBuilder.append(" times");
        }

        descriptionBuilder.append(".");

        return descriptionBuilder.toString();
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.REPEAT_AMOUNT.toString(), "Repeat Amount", "The amount of times this trigger must repeat, 0 is infinite.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.INTERVAL.toString(), "Repeat Interval", "The interval at wich repeats are triggered."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INTERVAL_FIELD.toString(), "Repeat Interval Field", "The field that will be used to determine interval.", IntervalFields.Hours.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INTERVAL_FIELD.toString().equals(datumIdentifier)) return createStringOptions(IntervalFields.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public Integer getRepeatAmount()
    {
        return getDefinitionDatum(Datum.REPEAT_AMOUNT);
    }

    public void setRepeatAmount(String repeatAmount)
    {
        setDefinitionDatum(Datum.REPEAT_AMOUNT, repeatAmount);
    }

    public Double getInterval()
    {
        return getDefinitionDatum(Datum.INTERVAL);
    }

    public void setInterval(Double interval)
    {
        setDefinitionDatum(Datum.INTERVAL, interval);
    }

    public T8QuartzSimpleTriggerEnums.IntervalFields getIntervalField()
    {
        return T8QuartzSimpleTriggerEnums.IntervalFields.valueOf(getDefinitionDatum(Datum.INTERVAL_FIELD));
    }

    public void setIntervalField(T8QuartzSimpleTriggerEnums.IntervalFields intervalField)
    {
        setDefinitionDatum(Datum.INTERVAL_FIELD, intervalField.toString());
    }
}
