package com.pilog.t8.definition.script;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.definition.quartz.task.T8QuartzProcessTaskDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public class T8QuartProcessParameterCreationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    private static final T8Logger logger = T8Log.getLogger(T8QuartProcessParameterCreationScriptDefinition.class);

    public static final String GROUP_IDENTIFIER = "@DG_QUARTZ_PROCESS_TASK_PARAMETER_SCRIPTS";
    public static final String IDENTIFIER_PREFIX = "QUARTZ_PROCESS_TASK_PARAMETER_SCRIPT_";
    public static final String STORAGE_PATH = null;
    public static final String TYPE_IDENTIFIER = "@DT_QUARTZ_PROCESS_TASK_PARAMETER_SCRIPT";
    public static final String DISPLAY_NAME = "Process Task Parameter Script";
    public static final String DESCRIPTION = "A script to create the parameter map that will be sent to a process.";
    public static final String VERSION = "0";

    public T8QuartProcessParameterCreationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        return new ArrayList<>();
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        T8QuartzProcessTaskDefinition taskDefinition;

        taskDefinition = (T8QuartzProcessTaskDefinition) getParentDefinition();
        if (Strings.isNullOrEmpty(taskDefinition.getProcessIdentifier()))
        {
            try
            {
                T8ProcessDefinition processDefinition = (T8ProcessDefinition)context.getServerContext().getDefinitionManager().getRawDefinition(context, context.getProjectId(), taskDefinition.getProcessIdentifier());
                return processDefinition.getInputParameterDefinitions();
            }
            catch (Exception ex)
            {
                logger.log("Failed to get the output parameter definitions for definition : " + getPublicIdentifier(), ex);
            }
        }

        return new ArrayList<>(0);
    }
}
