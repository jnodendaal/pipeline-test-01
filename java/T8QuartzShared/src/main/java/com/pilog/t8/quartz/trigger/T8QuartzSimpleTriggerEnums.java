package com.pilog.t8.quartz.trigger;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzSimpleTriggerEnums
{
    /**
     * The fields that are available to be selected by the user as valid intervals
     */
    public static enum IntervalFields
    {
        Hours,
        Minutes,
        Seconds,
        MilliSeconds
    }

    /**
     * This classes own representation of the quartz missfire instructions.
     */
    public static enum MissfireInstructions
    {
        MISFIRE_INSTRUCTION_SMART_POLICY("Smart Policy"),
        MISFIRE_INSTRUCTION_FIRE_NOW("Fire Now"),
        MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY("Ignore Missfire Policy"),
        MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT("Reschedule Next with Existing Count (Preferred)"),
        MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT("Reschedule Next with Remaining Count"),
        MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT("Reschedule Now with Existing Repeat Count");

        private final String displayName;

        private MissfireInstructions(String displayName)
        {
            this.displayName = displayName;
        }

        @Override
        public String toString()
        {
            return this.displayName;
        }
    }
}
