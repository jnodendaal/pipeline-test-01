package com.pilog.t8.definition.quartz.trigger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.quartz.trigger.IT8QuartzTrigger;
import com.pilog.t8.quartz.trigger.T8QuartzCronTriggerEnums.MissfireInstructions;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.date.Dates;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.quartz.CronExpression;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzCronTriggerDefinition extends T8QuartzTriggerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_QUARTZ_TRIGGER_CRON";
    public static final String DISPLAY_NAME = "Cron Trigger";
    public static final String DESCRIPTION = "A trigger for firing scheduler jobs based on a cron exression schedule.";
    public static final String VERSION = "0";
    public enum Datum {
        SECONDS,
        MINUTES,
        HOURS,
        DAY_OF_MONTH,
        MONTH,
        DAY_OF_WEEK,
        YEAR
    };
    // -------- Definition Meta-Data -------- //

    public T8QuartzCronTriggerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public IT8QuartzTrigger instantiateTrigger(T8ServerContext serverContext, T8QuartzServiceDefinition serviceDefinition)
    {
        return T8Reflections.getInstance("com.pilog.t8.quartz.trigger.T8QuartzCronTrigger", new Class<?>[]{T8ServerContext.class, T8QuartzCronTriggerDefinition.class, T8QuartzServiceDefinition.class}, serverContext, this, serviceDefinition);
    }

    @Override
    public HashMap<Integer, String> getMissfireInstructions()
    {
        HashMap<Integer,String> missfireInstruction = new HashMap<>();

        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_DO_NOTHING.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_DO_NOTHING.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY.toString());
        missfireInstruction.put(MissfireInstructions.MISFIRE_INSTRUCTION_SMART_POLICY.ordinal(), MissfireInstructions.MISFIRE_INSTRUCTION_SMART_POLICY.toString());

        return missfireInstruction;
    }

    @Override
    public String getTriggerDescriptiveString()
    {
        // Investigate using the cron-utils library to create human readable cron expression

        CronExpression expression;
        Date nextExecution;

        try
        {
            expression = new CronExpression(getCronExpression());
            nextExecution = expression.getNextValidTimeAfter(new Date());

            return Dates.STD_SYS_TS_FORMATTER.format(nextExecution);  // Simply return the next execution date/time
        }
        catch (ParseException pe)
        {
            throw new RuntimeException("Invalid Cron Expression detected: "+getCronExpression(), pe);
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SECONDS.toString(), "Seconds", "Indicates the seconds on which the trigger fires. Valid values are 0-59,*", "*"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MINUTES.toString(), "Minutes", "Indicates the minutes on which the trigger fires. Valid values are 0-59,*", "*"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HOURS.toString(), "Hours", "Indicates the hours on which the trigger fires. Valid values are 0-23,*", "*"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DAY_OF_MONTH.toString(), "Day Of Month", "The day of month on which the trigger fires. Valid values are 0-31,*,L(Last day of month),W(Nearest weekday, 15W),?", "*"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MONTH.toString(), "Month", "The month on which the trigger fires. Valid value are 0-11,3 letter month name ex JAN,FEB,MAR", "*"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DAY_OF_WEEK.toString(), "Day Of Week", "The day of the week on which the trigger fires. Valid values are 0-7 or 3 letter day name such as MON,TUE,WED etc. Keep in mind that sunday=0. To use last day of week "
                + "you can specify L, or use {weekday}+L example 7L to indicate last saturday of the month. You can use hash to indicate the week in the month,"
                + "for instance FRI#3 will indicate the 3rd friday of the month.", "?"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.YEAR.toString(), "Year", "(Optional) The year on which the trigger fires.", "*"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public String getSeconds()
    {
        return getDefinitionDatum(Datum.SECONDS);
    }

    public void setSeconds(String seconds)
    {
        setDefinitionDatum(Datum.SECONDS, seconds);
    }

    public String getMinutes()
    {
        return getDefinitionDatum(Datum.MINUTES);
    }

    public void setMinutes(String minutes)
    {
        setDefinitionDatum(Datum.MINUTES, minutes);
    }

    public String getHours()
    {
        return getDefinitionDatum(Datum.HOURS);
    }

    public void setHours(String hours)
    {
        setDefinitionDatum(Datum.HOURS, hours);
    }

    public String getDayOfMonth()
    {
        return getDefinitionDatum(Datum.DAY_OF_MONTH);
    }

    public void setDayOfMonth(String dayOfMonth)
    {
        setDefinitionDatum(Datum.DAY_OF_MONTH, dayOfMonth);
    }

    public String getMonth()
    {
        return getDefinitionDatum(Datum.MONTH);
    }

    public void setMonth(String month)
    {
        setDefinitionDatum(Datum.MONTH, month);
    }

    public String getDayOfWeek()
    {
        return getDefinitionDatum(Datum.DAY_OF_WEEK);
    }

    public void setDayOfWeek(String dayOfWeek)
    {
        setDefinitionDatum(Datum.DAY_OF_WEEK, dayOfWeek);
    }

    public String getYear()
    {
        return getDefinitionDatum(Datum.YEAR);
    }

    public void setYear(String year)
    {
        setDefinitionDatum(Datum.YEAR, year);
    }

    public String getCronExpression()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getSeconds()).append(" ");
        builder.append(getMinutes()).append(" ");
        builder.append(getHours()).append(" ");
        builder.append(getDayOfMonth()).append(" ");
        builder.append(getMonth()).append(" ");
        builder.append(getDayOfWeek()).append(" ");
        builder.append(getYear());

        return builder.toString();
    }
}
