package com.pilog.t8.developer.definitions.test.harness;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.developer.definitions.test.T8JasperReportTestFrame;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8JasperReportTestHarness implements T8DefinitionTestHarness
{
    public T8JasperReportTestHarness()
    {
    }

    @Override
    public void configure(T8Context context)
    {
    }

    @Override
    public void testDefinition(T8Context context, T8Definition definition)
    {
        T8JasperReportTestFrame.testReportDefinition(context, (T8ReportDefinition)definition, this);
    }
}
