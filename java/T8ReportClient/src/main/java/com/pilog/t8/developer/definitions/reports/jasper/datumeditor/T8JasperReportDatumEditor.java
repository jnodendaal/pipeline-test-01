package com.pilog.t8.developer.definitions.reports.jasper.datumeditor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.definition.report.jasper.T8JasperReportDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperDesignViewer;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8JasperReportDatumEditor extends JPanel implements T8DefinitionDatumEditor
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperReportDatumEditor.class);

    private static File _lastSelectedFile;

    private final JButton btnSaveReport;
    private final JButton btnDownloadReport;
    private final JButton btnViewButtonReport;
    private final T8JasperReportDefinition targetDefinition;
    private final T8DefinitionContext definitionContext;
    private final T8DefinitionDatumType datumType;

    public T8JasperReportDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        this.definitionContext = definitionContext;
        this.datumType = datumType;
        this.targetDefinition = (T8JasperReportDefinition)definition;
        setLayout(new java.awt.FlowLayout());
        btnSaveReport = new JButton("Upload Report");
        btnSaveReport.addActionListener(new SaveReportActionListener(this));
        btnDownloadReport = new JButton("Download Report");
        btnDownloadReport.addActionListener(new DownloadReportActionListener(this));
        btnViewButtonReport = new JButton("View Report");
        btnViewButtonReport.addActionListener(new ViewReportActionListener(this));
        add(btnSaveReport, java.awt.FlowLayout.LEFT);
        add(btnDownloadReport, java.awt.FlowLayout.LEFT);
        add(btnViewButtonReport, java.awt.FlowLayout.LEFT);
    }

    @Override
    public void refreshEditor()
    {
        invalidate();
    }

    @Override
    public void setEditable(boolean editable)
    {
        btnSaveReport.setEnabled(editable);
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
    }

    protected void setDefinitionDatum(Object value)
    {
        targetDefinition.setDefinitionDatum(datumType.getIdentifier(), value, this);
    }

    protected Object getDefinitionDatum()
    {
        return targetDefinition.getDefinitionDatum(datumType.getIdentifier());
    }

    @Override
    public T8Context getContext()
    {
        return definitionContext.getContext();
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return definitionContext.getClientContext();
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return definitionContext.getSessionContext();
    }

    @Override
    public T8DefinitionContext getDefinitionContext()
    {
        return definitionContext;
    }

    @Override
    public String getDatumIdentifier()
    {
        return datumType.getIdentifier();
    }

    @Override
    public T8DefinitionDatumType getDatumType()
    {
        return datumType;
    }

    @Override
    public T8JasperReportDefinition getDefinition()
    {
        return targetDefinition;
    }

    @Override
    public void addDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    @Override
    public void removeDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    private class SaveReportActionListener implements ActionListener
    {
        private final T8JasperReportDatumEditor parent;

        private SaveReportActionListener(T8JasperReportDatumEditor parent)
        {
            this.parent = parent;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                String filePath = BasicFileChooser.getLoadFilePath(parent, ".jrxml", "Jasper Report", _lastSelectedFile);
                try
                {
                    File report = new File(filePath);
                    _lastSelectedFile = report;
                    FileInputStream reportStream = new FileInputStream(report);
                    byte[] reportData = new byte[reportStream.available()];
                    reportStream.read(reportData);
                    reportStream.close();

                    parent.setDefinitionDatum(reportData);
                    Toast.makeText(getClientContext().getParentWindow(), "Report data successfully loaded.").display();
                }
                catch (IOException ex)
                {
                    Toast.makeText(getClientContext().getParentWindow(), "Failed to load report data.").display();
                    LOGGER.log(T8Logger.Level.ERROR, "Failed to save report file from location " + filePath, ex);
                }
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to upload report file", ex);
            }
        }
    }

    private class DownloadReportActionListener implements ActionListener
    {
        private final T8JasperReportDatumEditor parent;

        private DownloadReportActionListener(T8JasperReportDatumEditor parent)
        {
            this.parent = parent;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            byte[] reportData;
            String filePath;

            try
            {
                reportData = (byte[]) parent.getDefinitionDatum();
                if (reportData == null || reportData.length == 0)
                {
                    Toast.makeText(getClientContext().getParentWindow(), "No file available.", Toast.Style.ERROR);
                    return;
                }

                filePath = BasicFileChooser.getSaveFilePath(parent, ".jrxml", "Jasper Report", new File(targetDefinition.getIdentifier().substring(1)), BasicFileChooser.FileSelectionMode.FILES);
                if (filePath != null && !"".equals(filePath))
                {
                    try
                    {
                        File report = new File(filePath);
                        report.createNewFile();
                        FileOutputStream outStream = new FileOutputStream(report);
                        outStream.write(reportData);
                        outStream.close();
                        Toast.makeText(getClientContext().getParentWindow(), "Report file saved to specified location.").display();
                    }
                    catch (IOException ex)
                    {
                        LOGGER.log(T8Logger.Level.ERROR, "Failed to save report file to " + filePath, ex);
                        Toast.makeText(getClientContext().getParentWindow(), "Failed to download report file to selected location: " + filePath).display();
                    }
                }
                else
                {
                    LOGGER.log(T8Logger.Level.WARNING, "No file path selected " + filePath);
                }
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to download the report file.", ex);
            }
        }
    }

    private class ViewReportActionListener implements ActionListener
    {
        private final T8JasperReportDatumEditor parent;

        private ViewReportActionListener(T8JasperReportDatumEditor parent)
        {
            this.parent = parent;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                byte[] reportArray = (byte[]) parent.getDefinitionDatum();
                InputStream reportStreamIn;
                if (reportArray == null || reportArray.length < 2)
                {
                    ByteArrayOutputStream reportStreamOut = new ByteArrayOutputStream();
                    JasperDesign newReport = new JasperDesign();
                    newReport.setName(targetDefinition.getIdentifier());

                    JasperCompileManager.compileReportToStream(newReport, reportStreamOut);
                    reportStreamIn = new ByteArrayInputStream(reportStreamOut.toByteArray());
                    reportStreamOut.close();
                }
                else
                {
                    ByteArrayOutputStream reportStreamOut = new ByteArrayOutputStream();
                    JasperCompileManager.compileReportToStream((reportStreamIn = new ByteArrayInputStream(reportArray)), reportStreamOut);
                    reportStreamIn.close();

                    reportStreamIn = new ByteArrayInputStream(reportStreamOut.toByteArray());
                    reportStreamOut.close();
                }

                JasperDesignViewer designer = new JasperDesignViewer(reportStreamIn, false);
                designer.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                designer.setVisible(true);
            }
            catch (IOException | JRException ex)
            {
                LOGGER.log("Failed to open the report file for viewing.", ex);
            }
        }
    }
}
