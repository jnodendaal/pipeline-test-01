package com.pilog.t8.ui.report.jasper;

import com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerDefinition;
import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReportsContext;

import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.swing.JRViewerToolbar;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReport extends JRViewer
{

    public T8JasperReport(String fileName, boolean isXML) throws JRException
    {
        super(fileName, isXML);
    }

    public T8JasperReport(InputStream is, boolean isXML) throws JRException
    {
        super(is, isXML);
    }

    public T8JasperReport(JasperPrint jrPrint)
    {
        super(jrPrint);
    }

    public T8JasperReport(String fileName, boolean isXML, Locale locale) throws JRException
    {
        super(fileName, isXML, locale);
    }

    public T8JasperReport(InputStream is, boolean isXML, Locale locale) throws JRException
    {
        super(is, isXML, locale);
    }

    public T8JasperReport(JasperPrint jrPrint, Locale locale)
    {
        super(jrPrint, locale);
    }

    public T8JasperReport(String fileName, boolean isXML, Locale locale, ResourceBundle resBundle) throws JRException
    {
        super(fileName, isXML, locale, resBundle);
    }

    public T8JasperReport(InputStream is, boolean isXML, Locale locale, ResourceBundle resBundle) throws JRException
    {
        super(is, isXML, locale, resBundle);
    }

    public T8JasperReport(JasperPrint jrPrint, Locale locale, ResourceBundle resBundle)
    {
        super(jrPrint, locale, resBundle);
    }

    public T8JasperReport(JasperReportsContext jasperReportsContext, String fileName, boolean isXML, Locale locale, ResourceBundle resBundle) throws JRException
    {
        super(jasperReportsContext, fileName, isXML, locale, resBundle);
    }

    public T8JasperReport(JasperReportsContext jasperReportsContext, InputStream is, boolean isXML, Locale locale, ResourceBundle resBundle) throws JRException
    {
        super(jasperReportsContext, is, isXML, locale, resBundle);
    }

    public T8JasperReport(JasperReportsContext jasperReportsContext, JasperPrint jrPrint, Locale locale, ResourceBundle resBundle)
    {
        super(jasperReportsContext, jrPrint, locale, resBundle);
    }

    /**
     * Set the buttons according to the Definition
     *
     * @param definition The @{code T8JasperReportViewerDefinition} that contains all the configuration for the report.
     */
    public void setDisplayProperties(T8JasperReportViewerDefinition definition)
    {
        JRViewerToolbar toolbar = createToolbar();

        toolbar.getComponent(0).setEnabled(definition.isSaveEnabled());
        toolbar.getComponent(1).setEnabled(definition.isPrintEnabled());
    }
}
