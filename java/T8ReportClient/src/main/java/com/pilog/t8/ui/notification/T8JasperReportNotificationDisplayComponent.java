package com.pilog.t8.ui.notification;

import static com.pilog.t8.definition.notification.T8JasperReportNotificationDefinition.*;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification;
import com.pilog.t8.definition.notification.T8JasperReportNotificationDefinition;
import com.pilog.t8.definition.notification.T8JasperReportNotificationDefinition.ReportType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.desktop.DesktopUtil;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.progressbar.T8ProgressBarUI;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.JXTitledPanel;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportNotificationDisplayComponent extends JPanel implements T8NotificationDisplayComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperReportNotificationDisplayComponent.class);

    private final Map<String, Object> notificationParameters;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8Notification notification;
    private boolean downloaded;
    private File localFile;

    public T8JasperReportNotificationDisplayComponent(T8Context context, T8JasperReportNotificationDefinition definition, T8Notification notification)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.notification = notification;
        this.notificationParameters = new HashMap<>();
        this.downloaded = false;
        initComponents();
    }

    @Override
    public T8Notification getNotification()
    {
        return notification;
    }

    @Override
    public void initialize()
    {
        JXTitledPanel titledPanel;
        Long reportGenerationTime;

        //Build the ui
        titledPanel = new JXTitledPanel(translate("Report Generated"));
        titledPanel.setContentContainer(jPanelContent);
        titledPanel.setOpaque(false);

        // Add the content for display
        this.add(titledPanel);

        jProgressBar.setUI(new T8ProgressBarUI());
        jProgressBar.setVisible(false); //Hide the progress bar until the user clicks view so it does not appear so confusing

        // Hide the open report button until the report is downloaded
        jButtonOpenDownloadedReport.setEnabled(false);
        jButtonOpenDownloadedReport.setVisible(false);

        this.notificationParameters.putAll(notification.getParameters());
        jLabelReportName.setText(translate((String) this.notificationParameters.get(PARAMETER_REPORT_NAME)));
        jLabelFileType.setText((String) this.notificationParameters.get(PARAMETER_REPORT_TYPE));

        reportGenerationTime = (Long) this.notificationParameters.get(PARAMETER_GENERATION_TIME);
        if (reportGenerationTime != null && reportGenerationTime > 0)
        {
            jLabelGenerationTime.setText(new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss").format(new Date(reportGenerationTime)));
        }

        if (Boolean.TRUE.equals(this.notificationParameters.getOrDefault(PARAMETER_GENERATION_SUCCESS, true)))
        {
            try
            {
                long fileSize;

                fileSize = clientContext.getFileManager().getFileSize(context, (String) this.notificationParameters.get(PARAMETER_FILE_CONTEXT_IID), (String) this.notificationParameters.get(PARAMETER_REPORT_FILE_NAME));
                jLabelReportSize.setText(StringUtilities.getFileSizeString(fileSize));
            }
            catch (Exception ex)
            {
                // Consider to change something here to allow the user to request the report be re-generated
                this.jLabelReportSize.setText(translate("Not Available."));
                this.jButtonDownloadReport.setToolTipText(translate("Report no longer available."));
                this.jButtonDownloadReport.setEnabled(false);
                LOGGER.log("Failed to get report file size", ex);
            }
        }
        else
        {
            this.jLabelReportSize.setText(translate("Generation Failed."));
            this.jButtonDownloadReport.setEnabled(false);
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        try
        {
            clientContext.getFileManager().closeFileContext(context, (String) this.notificationParameters.get(PARAMETER_FILE_CONTEXT_IID));
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to close file context", e);
        }
    }

    private void downloadReport()
    {
        boolean downloadRequested;

        // We want to make sure the user knows they have already downloaded this report
        if (this.downloaded)
        {
            downloadRequested = false;
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, translate("The report has already been downloaded. Would you like to download it again?"), translate("Already Downloaded"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
            {
                downloadRequested = true;
            }
        } else downloadRequested = true;

        if (downloadRequested)
        {
            jProgressBar.setVisible(true);

            final T8FileManager fileManager;
            String localFileDirectory;
            String extension;

            try
            {
                extension = ReportType.valueOf((String) this.notificationParameters.get(PARAMETER_REPORT_TYPE)).getExtension();

                // Get the target file path that identifies the directory where the report will be saved
                localFileDirectory = BasicFileChooser.getLoadFilePath(SwingUtilities.getWindowAncestor(this), null, "PDF Files", null, BasicFileChooser.FileSelectionMode.DIRECTORIES);
                if (localFileDirectory != null)
                {
                    this.localFile = new File(localFileDirectory, this.notificationParameters.get(PARAMETER_REPORT_NAME)+extension);

                    if (this.localFile.exists() || this.localFile.createNewFile())
                    {
                        fileManager = clientContext.getFileManager();
                        new Thread(() ->
                        {
                            DownloadOperationChecker downloadOperationChecker;

                            downloadOperationChecker = new DownloadOperationChecker(fileManager);
                            try
                            {
                                new Thread(downloadOperationChecker).start();
                                fileManager.downloadFile(context, (String) notificationParameters.get(PARAMETER_FILE_CONTEXT_IID), localFile.getAbsolutePath(), (String) notificationParameters.get(PARAMETER_REPORT_FILE_NAME));

                                downloaded = true;
                                jButtonDownloadReport.setEnabled(false);
                                jButtonDownloadReport.setVisible(false);
                                jButtonOpenDownloadedReport.setEnabled(true);
                                jButtonOpenDownloadedReport.setVisible(true);
                            }
                            catch (Exception ex)
                            {
                                LOGGER.log("Failed to download report file", ex);
                                JOptionPane.showMessageDialog(T8JasperReportNotificationDisplayComponent.this, translate("Could not download report. If this report was generated more than 6 hours ago, please generate it again."), translate("Failed to Download"), JOptionPane.ERROR_MESSAGE);
                            }

                            downloadOperationChecker.stop();
                        }).start();
                    } else JOptionPane.showMessageDialog(T8JasperReportNotificationDisplayComponent.this, translate("Could not download report. The file could not be created on this computer."), translate("Failed to Download"), JOptionPane.ERROR_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to obtain user file path", ex);
                JOptionPane.showMessageDialog(T8JasperReportNotificationDisplayComponent.this, translate("Could not download report, if this report was generated more than 6 hours ago, please generate it again."), translate("Failed to Download"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void openReport()
    {
        SwingUtilities.invokeLater(() ->
        {
            if (ReportType.valueOf((String) notificationParameters.get(PARAMETER_REPORT_TYPE)) == ReportType.PDF)
            {
                try
                {
                    if (!DesktopUtil.openFile(localFile))
                    {
                        JOptionPane.showMessageDialog(T8JasperReportNotificationDisplayComponent.this, translate("Could not open the report automatically.\nPlease open the report from the directory where it was saved."), translate("Failed to Open Report"), JOptionPane.ERROR_MESSAGE);
                    }
                }
                catch (IOException ioe)
                {
                    LOGGER.log("Failed to open report", ioe);
                    JOptionPane.showMessageDialog(T8JasperReportNotificationDisplayComponent.this, translate("Could not open the report automatically.\nPlease open the report from the directory where it was saved."), translate("Failed to Open Report"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private String translate(String toTranslate)
    {
        return clientContext.getConfigurationManager().getUITranslation(context, toTranslate);
    }

    private class DownloadOperationChecker implements Runnable
    {
        private final T8FileManager fileManager;
        private boolean stopped;

        private DownloadOperationChecker(T8FileManager fileManager)
        {
            this.fileManager = fileManager;
            this.stopped = false;
        }

        @Override
        public void run()
        {
            while (!stopped)
            {
                try
                {
                    final long bytesDownloaded;
                    final int progress;

                    //Sleep now to give the download operation some time to start
                    Thread.sleep(100);
                    bytesDownloaded = fileManager.getDownloadOperationBytesDownloaded(context, localFile.getAbsolutePath());
                    progress = fileManager.getDownloadOperationProgress(context, localFile.getAbsolutePath());

                    SwingUtilities.invokeLater(() ->
                    {
                        if (progress != -1)
                        {
                            jProgressBar.setString(StringUtilities.getFileSizeString(bytesDownloaded));
                            jProgressBar.setValue(progress);
                        }
                    });
                } catch (Exception e)
                {
                    LOGGER.log("Failed to update progress for file download operation", e);
                }
            }

            //Make sure that the progress shows 100
            SwingUtilities.invokeLater(() ->
            {
                jProgressBar.setString(null);
                jProgressBar.setValue(100);
            });
        }

        private void stop()
        {
            stopped = true;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jLabelReportName = new javax.swing.JLabel();
        jButtonDownloadReport = new javax.swing.JButton();
        jButtonOpenDownloadedReport = new javax.swing.JButton();
        jProgressBar = new javax.swing.JProgressBar();
        jLabelReportSizeTitle = new javax.swing.JLabel();
        jLabelReportSize = new javax.swing.JLabel();
        jLabelReportNameTitle = new javax.swing.JLabel();
        jLabelFileTypeTitle = new javax.swing.JLabel();
        jLabelFileType = new javax.swing.JLabel();
        jLabelGenerationTimeTitle = new javax.swing.JLabel();
        jLabelGenerationTime = new javax.swing.JLabel();

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jLabelReportName.setText("jLabel1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelReportName, gridBagConstraints);

        jButtonDownloadReport.setText(translate("Download")
        );
        jButtonDownloadReport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDownloadReportActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jButtonDownloadReport, gridBagConstraints);

        jButtonOpenDownloadedReport.setText(translate("Open Report")
        );
        jButtonOpenDownloadedReport.setEnabled(false);
        jButtonOpenDownloadedReport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonOpenDownloadedReportActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jButtonOpenDownloadedReport, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jProgressBar, gridBagConstraints);

        jLabelReportSizeTitle.setText(translate("Report Size:")
        );
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelReportSizeTitle, gridBagConstraints);

        jLabelReportSize.setText(translate("Unknown"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelReportSize, gridBagConstraints);

        jLabelReportNameTitle.setText(translate("Report Name:")
        );
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelReportNameTitle, gridBagConstraints);

        jLabelFileTypeTitle.setText(translate("File Type:")
        );
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelFileTypeTitle, gridBagConstraints);

        jLabelFileType.setText("jLabel2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelFileType, gridBagConstraints);

        jLabelGenerationTimeTitle.setText(translate("Generated At:")
        );
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelGenerationTimeTitle, gridBagConstraints);

        jLabelGenerationTime.setText(translate("Unknown"));
        jLabelGenerationTime.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jLabelGenerationTime, gridBagConstraints);

        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(400, 150));
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonDownloadReportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDownloadReportActionPerformed
    {//GEN-HEADEREND:event_jButtonDownloadReportActionPerformed
        downloadReport();
    }//GEN-LAST:event_jButtonDownloadReportActionPerformed

    private void jButtonOpenDownloadedReportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonOpenDownloadedReportActionPerformed
    {//GEN-HEADEREND:event_jButtonOpenDownloadedReportActionPerformed
        openReport();
    }//GEN-LAST:event_jButtonOpenDownloadedReportActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDownloadReport;
    private javax.swing.JButton jButtonOpenDownloadedReport;
    private javax.swing.JLabel jLabelFileType;
    private javax.swing.JLabel jLabelFileTypeTitle;
    private javax.swing.JLabel jLabelGenerationTime;
    private javax.swing.JLabel jLabelGenerationTimeTitle;
    private javax.swing.JLabel jLabelReportName;
    private javax.swing.JLabel jLabelReportNameTitle;
    private javax.swing.JLabel jLabelReportSize;
    private javax.swing.JLabel jLabelReportSizeTitle;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JProgressBar jProgressBar;
    // End of variables declaration//GEN-END:variables
}
