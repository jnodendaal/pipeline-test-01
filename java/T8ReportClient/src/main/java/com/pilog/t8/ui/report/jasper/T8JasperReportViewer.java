package com.pilog.t8.ui.report.jasper;

import static com.pilog.t8.definition.report.jasper.T8JasperReportServerAPIHandler.*;
import static com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerAPIHandler.*;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.groupby.T8GroupByDataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.report.jasper.T8JasperReportDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.task.T8ComponentTask;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import net.sf.jasperreports.engine.JasperPrint;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportViewer extends JXPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperReportViewer.class);

    private final T8Context context;
    private final T8ComponentController controller;
    private final T8JasperReportViewerDefinition viewerDefinition;
    private final T8DefaultComponentContainer componentContainer;
    private final T8ConfigurationManager configurationManager;
    private final Map<String, T8DataFilter> preFilters;

    private T8JasperReportDefinition reportDefinition;
    private Map<String, Object> inputParameters;
    private T8JasperReport reportViewer;
    private Map<String, Object> reportParameters;
    private String reportDefinitionIdentifier;
    private ReportLoader lastLoader;
    private String reportDescription;
    private String reportTitle;

    public T8JasperReportViewer(T8ComponentController controller, T8JasperReportViewerDefinition definition)
    {
        this.controller = controller;
        this.context = controller.getContext();
        this.viewerDefinition = definition;
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.reportDefinitionIdentifier = definition.getReportDefinitionIdentifier();
        this.componentContainer = new T8DefaultComponentContainer();

        //Add an empty JPanel to prevent a null pointer when the container is initially locked
        this.componentContainer.setComponent(new JPanel());
        this.preFilters = new HashMap<>();
        this.setLayout(new GridBagLayout());
        add(componentContainer, new GridBagConstraints(0, 0, 0, 0, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    private void loadReport() throws Exception
    {
        if(lastLoader != null) lastLoader.cancel(false);
        lastLoader = new ReportLoader();
        lastLoader.execute();
    }

    @Override
    public T8ComponentController getController()
    {
        return this.controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return this.inputParameters;
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        try
        {
            if (OPERATION_REFRESH_REPORT.equals(operationId))
            {
                loadReport();
            }
            else if (OPERATION_SET_REPORT_TITLE.equals(operationId))
            {
                setReportTitle((String)operationParameters.get(PARAMETER_REPORT_TITLE));
            }
            else if (OPERATION_SET_REPORT_DESCRIPTION.equals(operationId))
            {
                setReportDescription((String)operationParameters.get(PARAMETER_REPORT_DESCRIPTION));
            }
            else if (OPERATION_SET_REPORT_IDENTIFIER.equals(operationId))
            {
                reportDefinitionIdentifier = (String) operationParameters.get(PARAMETER_REPORT_IDENTIFIER);
            }
            else if (OPERATION_SET_REPORT_FILTER.equals(operationId))
            {
                T8DataFilter preFilter = (T8DataFilter) operationParameters.get(PARAMETER_REPORT_FILTER);
                String identifier = (String) operationParameters.get(PARAMETER_FILTER_IDENTIFIER);
                preFilters.put(identifier, preFilter);
                if (operationParameters.containsKey(PARAMETER_REFRESH))
                {
                    if ((Boolean) operationParameters.get(PARAMETER_REFRESH))
                    {
                        loadReport();
                    }
                }
            }
            else if (OPERATION_RELOAD_REPORT_PARAMETERS.equals(operationId))
            {
                if (operationParameters.containsKey(PARAMETER_REPORT_PARAMETERS))
                {
                    reportParameters = (Map<String, Object>) operationParameters.get(PARAMETER_REPORT_PARAMETERS);
                }

                if (operationParameters.containsKey(PARAMETER_REFRESH))
                {
                    if ((Boolean) operationParameters.get(PARAMETER_REFRESH))
                    {
                        loadReport();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LOGGER.log(T8Logger.Level.ERROR, ()->"Failed to execute operation " + operationId + "." + getExceptionDetails("\nOperation Parameters: " + operationParameters), ex);
        }
        return new HashMap<>(0);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return this.viewerDefinition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        inputParameters = map;
    }

    @Override
    public void startComponent()
    {
        if (viewerDefinition.isLoadOnStart())
        {
            try
            {
                loadReport();
            }
            catch (Exception ex)
            {
                LOGGER.log(T8Logger.Level.ERROR, "Failed to load report.", ex);
            }
        }
    }

    @Override
    public void stopComponent()
    {
        if(lastLoader != null) lastLoader.cancel(false);
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(0);
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    /**
     * Setting the title overrides the report display name set on the definition
     * for the specific report.
     *
     * @param title The {@code String} title to be set for the report
     */
    private void setReportTitle(String title)
    {
        this.reportTitle = title;
    }

    /**
     * Setting the description overrides the report display name on the
     * definition for the specific report.
     *
     * @param description The {@code String} description to be set for the
     *      report
     */
    private void setReportDescription(String description)
    {
        this.reportDescription = description;
    }

    private String getTranslatedString(String text)
    {
        return this.configurationManager.getUITranslation(context, text);
    }

    private String getExceptionDetails(String otherDetails)
    {
        StringBuilder stringBuilder = new StringBuilder(otherDetails);
        stringBuilder.append("\n T8 Jasper Report Viewer Definition: ").append(viewerDefinition);
        stringBuilder.append("\n T8 Jasper Report Definition: ").append(reportDefinition);
        stringBuilder.append("\n Report Parameters: ").append(reportParameters);
        return stringBuilder.toString();
    }

    private T8DataFilter createCombinedFilter()
    {
        T8DataFilter filter = getNewDataFilterInstance();
        for (T8DataFilter t8DataFilter : preFilters.values())
        {
            if(t8DataFilter != null) filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, t8DataFilter.copy());
        }
        return filter;
    }

    private T8DataFilter getNewDataFilterInstance()
    {
        for (T8DataFilter t8DataFilter : preFilters.values())
        {
            if(t8DataFilter instanceof T8GroupByDataFilter) return new T8GroupByDataFilter(reportDefinition.getReportDataEntityIdentifier());
        }

        return new T8DataFilter(reportDefinition.getReportDataEntityIdentifier());
    }

    private class ReportLoader extends SwingWorker<JasperPrint, Void>
    {
        private String description;
        private String title;

        @Override
        protected JasperPrint doInBackground() throws Exception
        {
            //Check if we should load the report definition, and if the one we currently have loaded is the same as the one that should be loaded
            if (reportDefinition == null || !reportDefinition.getIdentifier().equals(reportDefinitionIdentifier))
            {
                try
                {
                    reportDefinition = (T8JasperReportDefinition)controller.getClientContext().getDefinitionManager().getRawDefinition(context, viewerDefinition.getRootProjectId(), reportDefinitionIdentifier);
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to load report definition " + reportDefinitionIdentifier, ex);
                    throw new DefinitionLoadException(ex);
                }
            }

            // Set the report title and description
            this.description = "Temp Name";
            this.title = "Temp Description";

            try
            {
                HashMap<String, Object> operationParameters = new HashMap<>(2);
                operationParameters.put(PARAMETER_REPORT_DEFINITION_IDENTIFIER, reportDefinitionIdentifier);

                //do not allow null parameters to be sent through
                if (reportParameters == null)
                {
                    LOGGER.log(T8Logger.Level.WARNING, "No report parameters provided.");
                    reportParameters = new HashMap<>(0);
                }
                operationParameters.put(PARAMETER_REPORT_PARAMETERS, reportParameters);

                T8DataFilter reportFilter = createCombinedFilter();
                if (reportFilter != null)
                {
                    operationParameters.put(PARAMETER_REPORT_ALTERNATE_FILTER, reportFilter);
                    //TODO: GBO - Remove this line
                    reportFilter.printStructure();
                }

                // Add the report title and description to the server operation parameters
                operationParameters.put(PARAMETER_REPORT_TITLE, this.title);
                operationParameters.put(PARAMETER_REPORT_DESCRIPTION, this.description);

                LOGGER.log(()->"Calling server to create report " + reportDefinitionIdentifier + " with parameters " + operationParameters);
                T8ServerOperationStatusReport operationStatusReport = T8MainServerClient.executeAsynchronousOperation(controller.getContext(), OPERATION_GENERATE_REPORT, operationParameters);

                // Set the progress layer message and then lock the layer during execution of the operation.
                GetReportFromServer task = new GetReportFromServer(operationStatusReport.getOperationInstanceIdentifier(), "Generating Report");
                componentContainer.setProgressUpdateInterval(500);
                componentContainer.addTask(task);

                // Wait for execution to complete.
                synchronized (task)
                {
                    while (task.isActive())
                    {
                        try
                        {
                            /*
                             Wait for this task to complete, the component container will call notify on the task object once it is complete,
                             if an exception occurs or the component container failed to call the notify some reason then this thread will wait
                             for the amount specified after which it will recheck the validation in any case and continue.
                             */
                            task.wait(10 * 1000);
                        }
                        catch (IllegalMonitorStateException | InterruptedException e)
                        {
                            LOGGER.log("Exception while waiting for asynchronous task to complete.", e);
                        }
                    }
                }

                if (task.getException() != null)
                {
                    throw task.getException();
                }
                LOGGER.log(T8Logger.Level.INFO, "Server operation complete. Response: " + task.getResult());
                JasperPrint generatedReport = (JasperPrint) task.getResult().get(PARAMETER_REPORT);
                LOGGER.log(T8Logger.Level.INFO, "Recieved report object: " + generatedReport);

                return generatedReport;
            }
            catch (Exception e)
            {
                LOGGER.log(T8Logger.Level.ERROR, ()->"Failed to generate report:" + getExceptionDetails("\n Report Identifier: " + reportDefinitionIdentifier), e);
                throw e;
            }
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    JasperPrint jasperPrint = get();

                    setBorder(new T8ContentHeaderBorder(getTranslatedString(this.title)));
                    setToolTipText(getTranslatedString(this.description));

                    //load the report into the viewer
                    reportViewer = new T8JasperReport(jasperPrint);
                    componentContainer.setComponent(reportViewer);
                    reportViewer.setDisplayProperties(viewerDefinition);

                    componentContainer.repaint();
                    componentContainer.revalidate();

                    // Report the report loaded event
                    controller.reportEvent(T8JasperReportViewer.this, viewerDefinition.getComponentEventDefinition(EVENT_REPORT_LOADED), new HashMap<>());
                }
                catch (InterruptedException ex)
                {
                    LOGGER.log(T8Logger.Level.FATAL, "Failed to build the report for display, due to an unexpected error: " + ex.getMessage(), ex);
                }
                catch (ExecutionException ex)
                {
                    if (ex.getCause() instanceof DefinitionLoadException)
                    {
                        T8OptionPane.showMessageDialog(T8JasperReportViewer.this, getTranslatedString("Could not load the report defined in the meta schema."), getTranslatedString("Report Error"), T8OptionPane.ERROR_MESSAGE);
                    }
                    else
                    {
                        T8OptionPane.showMessageDialog(T8JasperReportViewer.this, getTranslatedString("Could not load the report due to a server side error."), getTranslatedString("Report Error"), T8OptionPane.ERROR_MESSAGE);
                    }
                    componentContainer.unlock();
                }
            }
        }
    }

    private class GetReportFromServer implements T8ComponentTask
    {
        private final String operationInstanceIdentifier;
        private final String message;
        private Map<String, Object> operationResult;
        private Exception exception;
        private double progress;
        private boolean active;
        private Object progressReport;

        private GetReportFromServer(String operationInstanceIdentifier, String message)
        {
            this.operationInstanceIdentifier = operationInstanceIdentifier;
            this.message = message;
            this.progress = 0;
            this.active = true;
        }

        public Map<String, Object> getResult()
        {
            return operationResult;
        }

        public Exception getException()
        {
            return exception;
        }

        @Override
        public String getMessage()
        {
            return message != null ? message : getTranslatedString("Processing...");
        }

        @Override
        public boolean isIndeterminate()
        {
            return false;
        }

        @Override
        public Object getProgressReportObject()
        {
            return progressReport;
        }

        @Override
        public double getProgress()
        {
            try
            {
                T8ServerOperationStatusReport statusReport;
                T8ServerOperation.T8ServerOperationStatus status;

                // Get the status of the operation from the server.
                statusReport = T8MainServerClient.getOperationStatus(controller.getContext(), operationInstanceIdentifier);
                status = statusReport.getOperationStatus();

                // Check the status.
                if (status == T8ServerOperation.T8ServerOperationStatus.FAILED)
                {
                    exception = (Exception) statusReport.getOperationResult();
                    active = false;
                    return progress;
                }
                else if (status == T8ServerOperation.T8ServerOperationStatus.IN_PROGRESS)
                {
                    progress = statusReport.getOperationProgress();
                    progressReport = statusReport.getProgressReportObject();
                    return progress;
                }
                else
                {
                    operationResult = statusReport.getOperationResult();
                    active = false;
                    return 100.00;
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while checking progress of asynchronous operation: " + operationInstanceIdentifier, e);
                active = false;
                return progress;
            }
        }

        @Override
        public boolean isActive()
        {
            return active;
        }
    }

    private class DefinitionLoadException extends Exception
    {
        private static final long serialVersionUID = 2128218651810857578L;

        private DefinitionLoadException(Throwable cause)
        {
            super(cause);
        }
    }
}
