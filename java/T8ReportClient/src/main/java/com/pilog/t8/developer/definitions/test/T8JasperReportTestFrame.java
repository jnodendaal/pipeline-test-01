package com.pilog.t8.developer.definitions.test;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerDefinition;
import com.pilog.t8.developer.definitions.test.harness.T8JasperReportTestHarness;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ClientController;
import com.pilog.t8.ui.report.jasper.T8JasperReportViewer;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8JasperReportTestFrame extends javax.swing.JFrame
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperReportTestFrame.class);

    private T8JasperReportViewer viewer;
    private T8JasperReportViewerDefinition viewerDefinition;
    private T8ClientContext clientContext;
    private T8Context testContext;
    private T8Context internalContext;
    private T8ClientController testClient;
    private T8ModuleDefinition moduleDefinition;

    public T8JasperReportTestFrame(T8Context context, T8ReportDefinition reportDefinition, T8JasperReportTestHarness testHarness)
    {
        initComponents();
        this.internalContext = context;
        this.clientContext = internalContext.getClientContext();

        // Initialize the Module Definition and then create a new T8 instance.
        try
        {
            T8PanelDefinition panelDefinition;
            T8PanelSlotDefinition panelSlotDefinition;

            testContext = internalContext;

            viewerDefinition = new T8JasperReportViewerDefinition("JASPER_REPORT_TEST");
            viewerDefinition.setLoadOnStart(true);
            viewerDefinition.setPrintEnabled(true);
            viewerDefinition.setSaveEnabled(true);
            viewerDefinition.setReportDefinitionIdentifier(reportDefinition.getIdentifier());


            moduleDefinition = new T8ModuleDefinition("JASPER_REPORT_TEST_MODULE");
            panelDefinition = new T8PanelDefinition("JASPER_REPORT_TEST_PANEL");
            panelSlotDefinition = new T8PanelSlotDefinition("JASPER_REPORT_TEST_PANEL_SLOT");

            panelSlotDefinition.setSlotComponentDefinition(viewerDefinition);
            panelSlotDefinition.setAnchor(T8PanelSlotDefinition.Anchor.CENTER);
            panelSlotDefinition.setFill(T8PanelSlotDefinition.Fill.BOTH);
            panelSlotDefinition.setGridHeight(1);
            panelSlotDefinition.setGridWidth(1);
            panelSlotDefinition.setGridX(0);
            panelSlotDefinition.setGridY(0);
            panelSlotDefinition.setWeightX(0.1);
            panelSlotDefinition.setWeightY(0.1);

            panelDefinition.setPanelSlotDefinitions(new ArrayList<>(Arrays.asList(panelSlotDefinition)));
            moduleDefinition.setRootComponentDefinition(panelDefinition);

            testClient = new T8ClientController(testContext);

            testClient.initializeComponent(null);
            // Instantiate a new client.
            add(testClient, java.awt.BorderLayout.CENTER);
            validate();
        }
        catch (Exception e)
        {
            LOGGER.log(e);
        }

        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());
    }

    public void start()
    {
        try
        {
            testClient.startComponent();
            testClient.stopTaskExecution();
            testClient.loadModule(moduleDefinition, null);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while starting main module.", e);
        }
    }

    public void stop()
    {
        try
        {
            testClient.stopComponent();
            if(testContext != internalContext)
            {
                testClient.getSecurityManager().logout(testContext);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while trying to log out.", e);
        }
    }

    public static final void testReportDefinition(T8Context context, T8ReportDefinition reportDefinition, T8JasperReportTestHarness testHarness)
    {
        T8JasperReportTestFrame testFrame;

        testFrame = new T8JasperReportTestFrame(context, reportDefinition, testHarness);
        testFrame.setVisible(true);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Report Test");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        start();
    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        stop();
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
