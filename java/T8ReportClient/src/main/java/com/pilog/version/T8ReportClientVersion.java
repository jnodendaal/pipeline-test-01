package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8ReportClientVersion
{
    public final static String VERSION = "53";
}
