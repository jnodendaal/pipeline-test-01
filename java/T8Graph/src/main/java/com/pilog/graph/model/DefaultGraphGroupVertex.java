package com.pilog.graph.model;

import com.pilog.graph.view.Graph;
import com.pilog.graph.view.GraphVertexRenderer;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.LinkedList;

/**
 * @author Bouwer du Preez
 */
public class DefaultGraphGroupVertex extends DefaultGraphVertex
{
    private Insets insets;

    public DefaultGraphGroupVertex(String id, int x, int y, Object data)
    {
        super(id, x, y, data);
    }

    public Insets getInsets()
    {
        return insets;
    }

    public void setInsets(Insets insets)
    {
        this.insets = insets;
    }

    @Override
    public void doLayout(Graph graph, GraphVertexRenderer renderer, boolean isSelected)
    {
        super.doLayout(graph, renderer, isSelected);
        coordinates = getVertexBounds().getLocation();
    }

    @Override
    protected Rectangle calculateBounds(Dimension size)
    {
        if (children.size() > 0)
        {
            LinkedList<GraphVertex> groupVertices;
            Rectangle groupBounds = null;

            groupVertices = new LinkedList<GraphVertex>();
            groupVertices.addAll(children);
            while (groupVertices.size() > 0)
            {
                GraphVertex nextVertex;

                nextVertex = groupVertices.pop();
                if (groupBounds == null) groupBounds = new Rectangle(nextVertex.getVertexBounds());
                else groupBounds.add(nextVertex.getVertexBounds());

                groupVertices.addAll(nextVertex.getChildren());
            }

            // Add insets if set.
            if (insets != null)
            {
                groupBounds.height += (insets.top + insets.bottom);
                groupBounds.width += (insets.left + insets.right);
                groupBounds.translate(-insets.left, -insets.top);
            }

            return groupBounds;
        }
        else return new Rectangle(coordinates);
    }
}
