package com.pilog.graph.view.event;

/**
 * @author Bouwer du Preez
 */
public interface GraphViewListener
{
    public void verticesMoved(VerticesMovedEvent event);
}
