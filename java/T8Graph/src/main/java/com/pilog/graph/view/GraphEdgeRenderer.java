package com.pilog.graph.view;

import com.pilog.graph.model.GraphEdge;
import java.awt.Component;

/**
 * @author Bouwer du Preez
 */
public interface GraphEdgeRenderer
{
    public Component getRendererComponent(Graph graph, GraphEdge edge, boolean isSelected);
}
