package com.pilog.graph.view;

import com.pilog.graph.model.GraphVertex;
import java.awt.Component;
import java.awt.Dimension;

/**
 * @author Bouwer du Preez
 */
public interface GraphVertexRenderer
{
    public Component getRendererComponent(Graph graph, GraphVertex vertex, boolean isSelected);
    public Dimension getVertexSize(Graph graph, GraphVertex vertex, boolean isSelected);
}
