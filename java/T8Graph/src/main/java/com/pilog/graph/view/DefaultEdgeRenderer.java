package com.pilog.graph.view;

import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphEdge.EdgeArrowType;
import com.pilog.graph.model.GraphEdge.EdgeType;
import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.model.GraphVertexPort;
import com.pilog.graph.model.GraphVertexPort.PortType;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DefaultEdgeRenderer extends Component implements GraphEdgeRenderer
{
    private ArrayList<Point> coordinates;
    private Polygon arrow;
    private EdgeType edgeType;
    private double startArrowAngle;
    private double endArrowAngle;
    private EdgeArrowType startArrowType;
    private EdgeArrowType endArrowType;
    private int clearanceDistance;

    protected Color edgeColor = Color.GRAY;
    protected BasicStroke stroke;

    public DefaultEdgeRenderer()
    {
        arrow = new Polygon();
        arrow.addPoint(20, 20);
        arrow.addPoint(26, 30);
        arrow.addPoint(14, 30);

        startArrowAngle = 0;
        endArrowAngle = 0;
        startArrowType = EdgeArrowType.NONE;
        endArrowType = EdgeArrowType.NONE;
        clearanceDistance = 20;

        stroke = new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    }

    @Override
    public Component getRendererComponent(Graph graph, GraphEdge edge, boolean isSelected)
    {
        edgeType = edge.getEdgeType();
        edgeColor = (isSelected || isEitherEdgeVertexSelected(graph, edge) ? getEdgeSelectedColor() : getEdgeDeselectedColor());
        if (edge.getEdgeType() == EdgeType.STRAIGHT) doStraightEdgeLayout(edge);
        else if (edge.getEdgeType() == EdgeType.ANGLED) doAngledEdgeLayout(edge);
        else if (edge.getEdgeType() == EdgeType.BEZIER) doAngledEdgeLayout(edge);
        else doStraightEdgeLayout(edge);

        return this;
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D arrowG;
        Graphics2D g2;

        g2 = (Graphics2D)g;
        g2.setStroke(stroke);
        g2.setColor(edgeColor);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Draw the edge lines.
        if (edgeType == EdgeType.ANGLED)
        {
            for (int i = 0; i < coordinates.size() -1; i++)
            {
                Point startCoordinates;
                Point endCoordinates;

                startCoordinates = coordinates.get(i);
                endCoordinates = coordinates.get(i + 1);

                g.drawLine(startCoordinates.x, startCoordinates.y, endCoordinates.x, endCoordinates.y);
            }
        }
        else if (edgeType == EdgeType.BEZIER)
        {
            GeneralPath edgePath;

            edgePath = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
            if (coordinates.size() == 4)
            {
                Point start;
                Point end;
                Point control1;
                Point control2;

                start = coordinates.get(0);
                control1 = coordinates.get(1);
                control2 = coordinates.get(2);
                end = coordinates.get(3);

                edgePath.moveTo((float)start.x, (float)start.y);
                edgePath.curveTo((float)control1.x, (float)control1.y, (float)control2.x, (float)control2.y, (float)end.x, (float)end.y);
                g2.draw(edgePath);
            }
            else if (coordinates.size() == 3)
            {
                Point start;
                Point end;
                Point control1;

                start = coordinates.get(0);
                control1 = coordinates.get(1);
                end = coordinates.get(2);

                edgePath.moveTo((float)start.x, (float)start.y);
                edgePath.quadTo((float)control1.x, (float)control1.y, (float)end.x, (float)end.y);
                g2.draw(edgePath);
            }
        }
        else
        {
            for (int i = 0; i < coordinates.size() -1; i++)
            {
                Point startCoordinates;
                Point endCoordinates;

                startCoordinates = coordinates.get(i);
                endCoordinates = coordinates.get(i + 1);

                g.drawLine(startCoordinates.x, startCoordinates.y, endCoordinates.x, endCoordinates.y);
            }
        }

        // Draw the arrows at the ends of the edge.
        {
            Point startCoordinates;
            Point endCoordinates;

            startCoordinates = coordinates.get(0);
            endCoordinates = coordinates.get(coordinates.size()-1);

            if (startArrowType != EdgeArrowType.NONE)
            {
                arrowG = (Graphics2D)g2.create(startCoordinates.x-20, startCoordinates.y-20, 40, 40);
                arrowG.rotate(startArrowAngle, 20, 20);

                if (startArrowType == EdgeArrowType.FILLED)
                {
                    arrowG.fillPolygon(arrow);
                }
                else
                {
                    arrowG.drawPolygon(arrow);
                }
            }

            if (endArrowType != EdgeArrowType.NONE)
            {
                arrowG = (Graphics2D)g.create(endCoordinates.x-20, endCoordinates.y-20, 40, 40);
                arrowG.rotate(endArrowAngle, 20, 20);

                if (endArrowType == EdgeArrowType.FILLED)
                {
                    arrowG.fillPolygon(arrow);
                }
                else
                {
                    arrowG.drawPolygon(arrow);
                }
            }
        }
    }

    private void doStraightEdgeLayout(GraphEdge edge)
    {
        coordinates = new ArrayList<Point>();
        coordinates.add(edge.getStartPort().getPortCoordinates());
        coordinates.add(edge.getEndPort().getPortCoordinates());
    }

    private void doAngledEdgeLayout(GraphEdge edge)
    {
        PortType startPortType;
        PortType endPortType;
        int halfXDistance;
        int halfYDistance;
        Point startCoordinates;
        Point endCoordinates;
        GraphVertexPort startPort;
        GraphVertexPort endPort;

        startPort = edge.getStartPort();
        endPort = edge.getEndPort();
        startPortType = startPort.getPortType();
        endPortType = endPort.getPortType();
        startCoordinates = startPort.getPortCoordinates();
        endCoordinates = endPort.getPortCoordinates();
        halfXDistance = (endCoordinates.x - startCoordinates.x) / 2;
        halfYDistance = (endCoordinates.y - startCoordinates.y) / 2;

        startArrowType = edge.getEdgeStartArrowType();
        endArrowType = edge.getEdgeEndArrowType();

        coordinates = new ArrayList<Point>();
        coordinates.add(startCoordinates);
        if (startPortType == PortType.NORTH_CENTER)
        {
            startArrowAngle = Math.PI;

            if (endPortType == PortType.NORTH_CENTER)
            {
                endArrowAngle = Math.PI;

                if (startCoordinates.y > endCoordinates.y)
                {
                    coordinates.add(new Point(startCoordinates.x, endCoordinates.y - clearanceDistance));
                    coordinates.add(new Point(endCoordinates.x, endCoordinates.y - clearanceDistance));
                }
                else
                {
                    coordinates.add(new Point(startCoordinates.x, startCoordinates.y - clearanceDistance));
                    coordinates.add(new Point(endCoordinates.x, startCoordinates.y - clearanceDistance));
                }
            }
            else if (endPortType == PortType.EAST_CENTER)
            {
                endArrowAngle = -1.57;

                if (startCoordinates.x > (endCoordinates.x - clearanceDistance))
                {
                    coordinates.add(new Point(startCoordinates.x, startCoordinates.y - halfYDistance));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, startCoordinates.y - clearanceDistance));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, startCoordinates.y));
                }
                else
                {
                    coordinates.add(new Point(startCoordinates.x, endCoordinates.y));
                }
            }
            else if (endPortType == PortType.SOUTH_CENTER)
            {
                endArrowAngle = 0.0;

                coordinates.add(new Point(startCoordinates.x, startCoordinates.y + halfYDistance));
                coordinates.add(new Point(endCoordinates.x, startCoordinates.y + halfYDistance));
            }
            else if (endPortType == PortType.WEST_CENTER)
            {
                endArrowAngle = 1.57;

                if (startCoordinates.x > (endCoordinates.x - clearanceDistance))
                {
                    coordinates.add(new Point(startCoordinates.x, startCoordinates.y + halfYDistance));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, startCoordinates.y + halfYDistance));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, endCoordinates.y));
                }
                else
                {
                    coordinates.add(new Point(startCoordinates.x, endCoordinates.y));
                }
            }
        }
        else if (startPortType == PortType.EAST_CENTER)
        {
            startArrowAngle = -1.57;

            if (endPortType == PortType.NORTH_CENTER)
            {
                endArrowAngle = Math.PI;

                if (startCoordinates.x > (endCoordinates.x - clearanceDistance))
                {
                    coordinates.add(new Point(startCoordinates.x, startCoordinates.y - halfYDistance));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, startCoordinates.y - clearanceDistance));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, startCoordinates.y));
                }
                else
                {
                    coordinates.add(new Point(endCoordinates.x, startCoordinates.y));
                }
            }
            else if (endPortType == PortType.EAST_CENTER)
            {
                endArrowAngle = -1.57;

                if (startCoordinates.x > endCoordinates.x)
                {
                    coordinates.add(new Point(startCoordinates.x + clearanceDistance, startCoordinates.y));
                    coordinates.add(new Point(startCoordinates.x + clearanceDistance, endCoordinates.y));
                }
                else
                {
                    coordinates.add(new Point(endCoordinates.x + clearanceDistance, startCoordinates.y));
                    coordinates.add(new Point(endCoordinates.x + clearanceDistance, endCoordinates.y));
                }
            }
            else if (endPortType == PortType.SOUTH_CENTER)
            {
                endArrowAngle = 0.0;

                coordinates.add(new Point(endCoordinates.x, startCoordinates.y));
            }
            else if (endPortType == PortType.WEST_CENTER)
            {
                endArrowAngle = 1.57;

                coordinates.add(new Point(startCoordinates.x + halfXDistance, startCoordinates.y));
                coordinates.add(new Point(startCoordinates.x + halfXDistance, endCoordinates.y));
            }
        }
        else if (startPortType == PortType.SOUTH_CENTER)
        {
            startArrowAngle = 0.0;

            if (endPortType == PortType.NORTH_CENTER)
            {
                endArrowAngle = Math.PI;

                coordinates.add(new Point(startCoordinates.x, startCoordinates.y + halfYDistance));
                coordinates.add(new Point(endCoordinates.x, startCoordinates.y + halfYDistance));
            }
            else if (endPortType == PortType.EAST_CENTER)
            {
                endArrowAngle = -1.57;

                coordinates.add(new Point(startCoordinates.x, endCoordinates.y));
            }
            else if (endPortType == PortType.SOUTH_CENTER)
            {
                endArrowAngle = 0.0;

                if (startCoordinates.y > endCoordinates.y)
                {
                    coordinates.add(new Point(startCoordinates.x, startCoordinates.y + clearanceDistance));
                    coordinates.add(new Point(endCoordinates.x, startCoordinates.y + clearanceDistance));
                }
                else
                {
                    coordinates.add(new Point(startCoordinates.x, endCoordinates.y + clearanceDistance));
                    coordinates.add(new Point(endCoordinates.x, endCoordinates.y + clearanceDistance));
                }
            }
            else if (endPortType == PortType.WEST_CENTER)
            {
                endArrowAngle = 1.57;

                coordinates.add(new Point(startCoordinates.x, endCoordinates.y));
            }
        }
        else if (startPortType == PortType.WEST_CENTER)
        {
            startArrowAngle = 1.57;

            if (endPortType == PortType.NORTH_CENTER)
            {
                endArrowAngle = Math.PI;

                coordinates.add(new Point(endCoordinates.x, startCoordinates.y));
            }
            else if (endPortType == PortType.EAST_CENTER)
            {
                endArrowAngle = -1.57;

                coordinates.add(new Point(startCoordinates.x + halfXDistance, startCoordinates.y));
                coordinates.add(new Point(startCoordinates.x + halfXDistance, endCoordinates.y));
            }
            else if (endPortType == PortType.SOUTH_CENTER)
            {
                endArrowAngle = 0.0;

                coordinates.add(new Point(endCoordinates.x, startCoordinates.y));
            }
            else if (endPortType == PortType.WEST_CENTER)
            {
                endArrowAngle = 1.57;

                if (startCoordinates.x > endCoordinates.x)
                {
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, startCoordinates.y));
                    coordinates.add(new Point(endCoordinates.x - clearanceDistance, endCoordinates.y));
                }
                else
                {
                    coordinates.add(new Point(startCoordinates.x - clearanceDistance, startCoordinates.y));
                    coordinates.add(new Point(startCoordinates.x - clearanceDistance, endCoordinates.y));
                }
            }
        }
        coordinates.add(endCoordinates);
    }

    protected Color getEdgeSelectedColor()
    {
        return Color.LIGHT_GRAY;
    }

    protected Color getEdgeDeselectedColor()
    {
        return Color.GRAY;
    }

    protected boolean isEitherEdgeVertexSelected(Graph graph, GraphEdge edge)
    {
        List<GraphVertex> selectedVertices;

        selectedVertices = graph.getSelectedVertices();
        if (selectedVertices.contains(edge.getStartPort().getParentVertex())) return true;
        else if (selectedVertices.contains(edge.getEndPort().getParentVertex())) return true;
        else return false;
    }
}
