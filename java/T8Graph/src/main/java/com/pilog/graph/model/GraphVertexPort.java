package com.pilog.graph.model;

import java.awt.Point;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface GraphVertexPort
{
    public enum PortType {DYNAMIC_CENTER,
                          CENTER,
                          NORTH_CENTER,
                          EAST_CENTER,
                          SOUTH_CENTER,
                          WEST_CENTER,
                          NORTH_FIXED,
                          EAST_FIXED,
                          SOUTH_FIXED,
                          WEST_FIXED,
                          NORTH_EAST,
                          SOUTH_EAST,
                          SOUTH_WEST,
                          NORTH_WEST};

    public GraphVertex getParentVertex();
    public void setParentVertex(GraphVertex parent);
    public void addEdge(GraphEdge edge);
    public boolean removeEdge(GraphEdge edge);
    public Set<GraphEdge> getEdges();

    public Point getPortCoordinates();
    public Point getPortRelativeCoordinates();
    public void setPortRelativeCoordinates(Point coordinates);
    public void setPortMovable(boolean movable);
    public boolean isPortMovable();
    public PortType getPortType();
    public void setPortType(PortType portType);
}
