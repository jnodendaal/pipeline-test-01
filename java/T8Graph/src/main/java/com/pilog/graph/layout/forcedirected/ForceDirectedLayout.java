package com.pilog.graph.layout.forcedirected;

import com.pilog.graph.layout.GraphLayout;
import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.model.GraphVertex;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ForceDirectedLayout implements GraphLayout
{
    private final double defaultVertexMass;
    private final double defaultEdgeLength;
    private final double stiffness; // spring stiffness constant
    private final double repulsion; // repulsion constant
    private final double damping; // velocity damping factor
    private final double minEnergyThreshold; //threshold used to determine render stop
    private final double maxSpeed; // nodes aren't allowed to exceed this speed
    private final List<VertexState> vertexStates; // keep track of points associated with nodes
    private final List<Spring> edgeSprings; // keep track of springs associated with edges
    private GraphModel model;

    public ForceDirectedLayout(GraphModel model, double stiffness, double repulsion, double damping, Double minEnergyThreshold, Double maxSpeed, Double vertexMass, Double edgeLength)
    {
        this.model = model;
        this.stiffness = stiffness; // Spring stiffness constant.
        this.repulsion = repulsion; // Repulsion constant.
        this.damping = damping; // Velocity damping factor.
        this.defaultVertexMass = (vertexMass != null ? vertexMass : 10.0);
        this.defaultEdgeLength = (edgeLength != null ? edgeLength : 50.0);
        this.minEnergyThreshold = (minEnergyThreshold != null ? minEnergyThreshold : 0.01); //threshold used to determine render stop
        this.maxSpeed = (maxSpeed != null ? maxSpeed : Double.POSITIVE_INFINITY); // nodes aren't allowed to exceed this speed
        this.vertexStates = new ArrayList<>(); // keep track of points associated with nodes
        this.edgeSprings = new ArrayList<>(); // keep track of springs associated with edges
        initialize();
    }

    private void initialize()
    {
        if (model != null)
        {
            for (GraphVertex vertex : model.getVertices())
            {
                vertexStates.add(new VertexState(vertex, defaultVertexMass));
            }

            for (GraphEdge edge : model.getEdges())
            {
                GraphVertex startVertex;
                GraphVertex endVertex;

                startVertex = edge.getStartVertex();
                endVertex = edge.getEndVertex();
                edgeSprings.add(new Spring(getVertexState(startVertex), getVertexState(endVertex), defaultEdgeLength, this.stiffness));
            }
        }
    }

    @Override
    public void update()
    {
        this.vertexStates.clear();
        this.edgeSprings.clear();
        initialize();
    }

    @Override
    public void setModel(GraphModel model)
    {
        this.model = model;
        update();
    }

    private VertexState getVertexState(GraphVertex vertex)
    {
        for (VertexState state : vertexStates)
        {
            if (state.getVertex() == vertex)
            {
                return state;
            }
        }

        return null;
    }

    /**
     * The magnitude of the electrostatic force of attraction or repulsion between two point charges is directly
     * proportional to the product of the magnitudes of charges and inversely proportional to the square of the
     * distance between them. The force is along the straight line joining them.
     */
    public void applyCoulombsLaw()
    {
        for (VertexState state1 : vertexStates)
        {
            for (VertexState state2 : vertexStates)
            {
                if (state1 != state2)
                {
                    Vector d;
                    double distance;
                    Vector direction;

                    // Calculate the force.
                    d = state1.getPosition().subtract(state2.getPosition());
                    distance = d.magnitude() + 0.1; // Add 0.1 to avoid massive forces at small distances (and division by zero).
                    direction = d.normalise();

                    // Apply force to each vertex.
                    state1.applyForce(direction.multiply(this.repulsion).divide(distance * distance * 0.5));
                    state2.applyForce(direction.multiply(this.repulsion).divide(distance * distance * -0.5));
                }
            }
        }
    }

    /**
     * The force (F) needed to extend or compress a spring by some distance X scales linearly with respect to that distance.
     */
    public void applyHookesLaw()
    {
        for (Spring spring : edgeSprings)
        {
            Vector springDirection;
            double displacement;
            Vector direction;

            // Calculate the force of the spring.
            springDirection = spring.getState2().getPosition().subtract(spring.getState1().getPosition());
            displacement = spring.getLength() - springDirection.magnitude();
            direction = springDirection.normalise();

            // Apply force to each end point.
            spring.getState1().applyForce(direction.multiply(spring.getK() * displacement * -0.5));
            spring.getState2().applyForce(direction.multiply(spring.getK() * displacement * 0.5));
        }
    }

    public void attractToCentre()
    {
        for (VertexState state : vertexStates)
        {
            Vector springDirection;
            double displacement;
            Vector direction;

            // Calculate the force of an imaginary spring to the center of space.
            springDirection = state.getPosition();
            displacement = springDirection.magnitude();
            direction = springDirection.normalise();

            // Apply force to the point.
            state.applyForce(direction.multiply(stiffness * displacement * -0.5 * 0.1));
        }
    }

    public void updateVelocity(double timestep)
    {
        for (VertexState state : vertexStates)
        {
            state.setVelocity(state.getVelocity().add(state.getAcceleration().multiply(timestep)).multiply(this.damping));
            if (state.getVelocity().magnitude() > this.maxSpeed)
            {
                state.setVelocity(state.getVelocity().normalise().multiply(this.maxSpeed));
            }

            state.setAcceleration(new Vector(0,0));
        }
    }

    public void updatePosition(double timestep)
    {
        for (VertexState point : vertexStates)
        {
            point.setPosition(point.getPosition().add(point.getVelocity().multiply(timestep)));
        }
    }

    // Calculate the total kinetic energy of the system.
    public double totalEnergy()
    {
        double energy;

        energy = 0.0;
        for (VertexState point : vertexStates)
        {
            double speed = point.getVelocity().magnitude();
            energy += 0.5 * point.getMass() * speed * speed;
        }

        return energy;
    }

    @Override
    public boolean renderNextStep(double timestep)
    {
        // Calculate the next step.
        calculateNextStep(timestep);

        // Apply the results to all vertices.
        for (VertexState vertexState : vertexStates)
        {
            vertexState.applyPositionToVertex();
        }

        // Return a value indicating whether or not a next step remains to be calculated.
        return (totalEnergy() > minEnergyThreshold);
    }

    public void calculateNextStep(double timestep)
    {
        applyCoulombsLaw();
        applyHookesLaw();
        attractToCentre();
        updateVelocity(timestep);
        updatePosition(timestep);
    }
}
