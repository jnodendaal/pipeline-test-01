package com.pilog.graph.model;

import com.pilog.graph.model.GraphVertexPort.PortType;
import com.pilog.graph.view.Graph;
import com.pilog.graph.view.GraphVertexRenderer;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface GraphVertex extends GraphElement
{
    public Point getVertexCoordinates();
    public void setVertexCoordinates(Point coordinates);
    public Rectangle getVertexBounds();
    public int getChildCount();
    public GraphVertex getChildAt(int index);
    public void addChild(GraphVertex child);
    public void removeChild(GraphVertex child);
    public List<GraphVertex> getChildren();
    public Set<GraphEdge> getEdges();
    public Set<GraphVertex> getEdgeVertices();

    public int getPortCount();
    public GraphVertexPort getPort(int index);
    public GraphVertexPort getPort(PortType portType);
    public GraphVertexPort createPort(PortType portType, boolean movable);
    public void removePort(GraphVertexPort port);

    public boolean isVertexSelectable();
    public void setVertexSelectable(boolean selectable);

    public boolean isVertexMovable();
    public void setVertexMovable(boolean movable);

    public void setVertexDataObject(Object data);
    public Object getVertexDataObject();

    // Rendering operations.
    public void doLayout(Graph graph, GraphVertexRenderer renderer, boolean isSelected);

    // Vertex tree operations.
    public void translate(int dx, int dy);
}
