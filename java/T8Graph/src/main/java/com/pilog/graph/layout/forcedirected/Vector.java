package com.pilog.graph.layout.forcedirected;

/**
 * @author Bouwer du Preez
 */
public class Vector
{
    private double x;
    private double y;

    public Vector(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public static Vector random()
    {
        return new Vector(10.0 * (Math.random() - 0.5), 10.0 * (Math.random() - 0.5));
    }

    public Vector add(Vector v2)
    {
        return new Vector(this.x + v2.x, this.y + v2.y);
    }

    public Vector subtract(Vector v2)
    {
        return new Vector(this.x - v2.x, this.y - v2.y);
    }

    public Vector multiply(double factor)
    {
        return new Vector(this.x * factor, this.y * factor);
    }

    public Vector divide(double denominator)
    {
        if (denominator == 0)
        {
            return new Vector(0, 0); // Avoid divide by zero errors.
        }
        else
        {
            return new Vector(this.x / denominator, this.y / denominator);
        }
    }

    public double magnitude()
    {
        return Math.sqrt(this.x*this.x + this.y*this.y);
    }

    public Vector normal()
    {
        return new Vector(-this.y, this.x);
    }

    public Vector normalise()
    {
        return this.divide(this.magnitude());
    }
}
