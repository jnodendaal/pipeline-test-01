package com.pilog.graph.model;

import com.pilog.graph.model.event.GraphModelListener;
import com.pilog.graph.exceptions.InvalidEdgeException;
import com.pilog.graph.utils.SortedList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DefaultGraphModel implements GraphModel
{
    private final List<GraphVertex> vertices;
    private final List<GraphEdge> edges;
    private final List<GraphElement> depthSortedElements; // Holds all model elements sorted according to Z-depth.
    private final List<GraphModelListener> listeners;

    public DefaultGraphModel()
    {
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
        listeners = new ArrayList<>();
        depthSortedElements = new SortedList<>(new DepthComparator());
    }

    @Override
    public void addGraphModelListener(GraphModelListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void removeGraphModelListener(GraphModelListener listener)
    {
        listeners.remove(listener);
    }

    @Override
    public Collection<GraphElement> getGraphElements()
    {
        return depthSortedElements;
    }

    @Override
    public List<GraphVertex> getVertices()
    {
        return new ArrayList<>(vertices);
    }

    @Override
    public List<GraphEdge> getEdges()
    {
        return new ArrayList<>(edges);
    }

    @Override
    public int getVertexCount()
    {
        return vertices.size();
    }

    @Override
    public GraphVertex getVertex(String id)
    {
        for (GraphVertex vertex : vertices)
        {
            if (id.equals(vertex.getId()))
            {
                return vertex;
            }
        }

        return null;
    }

    @Override
    public GraphVertex getVertex(int index)
    {
        return vertices.get(index);
    }

    @Override
    public int getEdgeCount()
    {
        return edges.size();
    }

    @Override
    public GraphEdge getEdge(String id)
    {
        for (GraphEdge edge : edges)
        {
            if (id.equals(edge.getId()))
            {
                return edge;
            }
        }

        return null;
    }

    @Override
    public GraphEdge getEdge(int index)
    {
        return edges.get(index);
    }

    @Override
    public Object getChildVertex(GraphVertex parent, int index)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getChildCount(GraphVertex parent)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getIndexOfChild(GraphVertex parent, Object child)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addVertex(GraphVertex vertex)
    {
        vertices.add(vertex);
        depthSortedElements.add(vertex);
    }

    public void removeVertex(GraphVertex vertex)
    {
        // Remove all edges connected to the vertex.
        for (int i = edges.size() - 1; i > -1; i--)
        {
            GraphEdge edge;

            edge = edges.get(i);
            if ((edge.getStartPort().getParentVertex() == vertex) || (edge.getEndPort().getParentVertex() == vertex))
            {
                removeEdge(edge);
            }
        }

        // Remove the vertex.
        vertices.remove(vertex);
        depthSortedElements.remove(vertex);
    }

    public void addEdge(GraphEdge edge) throws InvalidEdgeException
    {
        GraphVertex parentVertex;
        GraphVertex childVertex;

        // Get the parent and child vertices of the edge.
        parentVertex = edge.getStartPort().getParentVertex();
        childVertex = edge.getEndPort().getParentVertex();

        if (!vertices.contains(parentVertex))
        {
            throw new InvalidEdgeException("Parent vertex not found in Graph Model.");
        }
        else if (!vertices.contains(childVertex))
        {
            throw new InvalidEdgeException("Child vertex not found in Graph Model.");
        }
        else if (parentVertex == childVertex)
        {
            throw new InvalidEdgeException("Parent and Child vertex are the same.");
        }
        else
        {
            edges.add(edge);
            depthSortedElements.add(edge);
        }
    }

    public void removeEdge(GraphEdge edge)
    {
        if (edges.contains(edge))
        {
            GraphVertex startVertex;
            GraphVertex endVertex;
            GraphVertexPort startPort;
            GraphVertexPort endPort;

            // Get the parent and child vertices of the edge.
            startPort = edge.getStartPort();
            endPort = edge.getEndPort();
            startVertex = startPort.getParentVertex();
            endVertex = endPort.getParentVertex();

            // Detach the edges from its ports.
            startPort.removeEdge(edge);
            endPort.removeEdge(edge);

            // Detach the parent and child vertices from each other.
            startVertex.removeChild(endVertex);
            endVertex.removeChild(startVertex);

            // Remove the edge from the model.
            edges.remove(edge);
            depthSortedElements.remove(edge);
        }
    }

    private class DepthComparator implements Comparator<GraphElement>
    {
        @Override
        public int compare(GraphElement o1, GraphElement o2)
        {
            Integer depth1;
            Integer depth2;

            depth1 = o1.getZDepth();
            depth2 = o2.getZDepth();

            return depth1.compareTo(depth2);
        }
    }
}
