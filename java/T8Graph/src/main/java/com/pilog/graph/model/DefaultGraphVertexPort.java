package com.pilog.graph.model;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class DefaultGraphVertexPort implements GraphVertexPort
{
    private final Set<GraphEdge> edges;
    private GraphVertex parentVertex;
    private Point relativeCoordinates;
    private PortType portType;
    private boolean movable;

    public DefaultGraphVertexPort(GraphVertex parentVertex, PortType portType, boolean movable)
    {
        this.parentVertex = parentVertex;
        this.portType = portType;
        this.movable = movable;
        this.edges = new HashSet<>();
        this.relativeCoordinates = new Point(0, 0);
    }

    @Override
    public GraphVertex getParentVertex()
    {
        return parentVertex;
    }

    @Override
    public void setParentVertex(GraphVertex parent)
    {
        parentVertex = parent;
    }

    @Override
    public Point getPortCoordinates()
    {
        Point portCoordinates;

        portCoordinates = new Point(parentVertex.getVertexCoordinates());
        portCoordinates.translate(relativeCoordinates.x, relativeCoordinates.y);
        return portCoordinates;
    }

    @Override
    public Point getPortRelativeCoordinates()
    {
        return relativeCoordinates;
    }

    @Override
    public void setPortRelativeCoordinates(Point coordinates)
    {
        relativeCoordinates = coordinates;
    }

    @Override
    public void setPortMovable(boolean movable)
    {
        this.movable = movable;
    }

    @Override
    public boolean isPortMovable()
    {
        return movable;
    }

    @Override
    public PortType getPortType()
    {
        return portType;
    }

    @Override
    public void setPortType(PortType portType)
    {
        // Only do the change if required.
        if (this.portType != portType)
        {
            this.portType = portType;
            repositionPort();
        }
    }

    public void addEdge(GraphEdge edge)
    {
        this.edges.add(edge);
    }

    public boolean removeEdge(GraphEdge edge)
    {
        return edges.remove(edge);
    }

    public Set<GraphEdge> getEdges()
    {
        return new HashSet<>(edges);
    }

    protected void repositionPort()
    {
        Point portCoordinates;
        Rectangle vertexBounds;
        int halfVertexWidth;
        int halfVertexHeight;

        vertexBounds = parentVertex.getVertexBounds();
        halfVertexWidth = (int)vertexBounds.getWidth()/2;
        halfVertexHeight = (int)vertexBounds.getHeight()/2;

        portCoordinates = getPortRelativeCoordinates();
        if (portType == PortType.NORTH_CENTER)
        {
            portCoordinates.setLocation(0, -halfVertexHeight);
        }
        else if (portType == PortType.EAST_CENTER)
        {
            portCoordinates.setLocation(halfVertexWidth, 0);
        }
        else if (portType == PortType.SOUTH_CENTER)
        {
            portCoordinates.setLocation(0, halfVertexHeight);
        }
        else if (portType == PortType.WEST_CENTER)
        {
            portCoordinates.setLocation(-halfVertexWidth, 0);
        }
        else if (portType == PortType.NORTH_EAST)
        {
            portCoordinates.setLocation(halfVertexWidth, -halfVertexHeight);
        }
        else if (portType == PortType.SOUTH_EAST)
        {
            portCoordinates.setLocation(halfVertexWidth, halfVertexHeight);
        }
        else if (portType == PortType.NORTH_WEST)
        {
            portCoordinates.setLocation(-halfVertexWidth, -halfVertexHeight);
        }
        else if (portType == PortType.SOUTH_WEST)
        {
            portCoordinates.setLocation(-halfVertexWidth, halfVertexHeight);
        }
    }
}
