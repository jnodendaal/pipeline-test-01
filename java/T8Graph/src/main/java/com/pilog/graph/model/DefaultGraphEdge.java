package com.pilog.graph.model;

import com.pilog.graph.model.GraphVertexPort.PortType;
import com.pilog.graph.view.Graph;
import com.pilog.graph.view.GraphEdgeRenderer;

/**
 * @author Bouwer du Preez
 */
public class DefaultGraphEdge implements GraphEdge
{
    private String id;
    private GraphVertex startVertex;
    private GraphVertex endVertex;
    private GraphVertexPort startPort;
    private GraphVertexPort endPort;
    private EdgeType edgeType;
    private EdgeLineType edgeLineType;
    private EdgeArrowType edgeStartArrowType;
    private EdgeArrowType edgeEndArrowType;
    private PortType startPortType;
    private PortType endPortType;
    private int zDepth;

    public DefaultGraphEdge(String id, GraphVertex startVertex, PortType startPortType, GraphVertex endVertex, PortType endPortType)
    {
        this.id = id;
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        this.startPortType = startPortType;
        this.endPortType = endPortType;
        this.edgeType = EdgeType.STRAIGHT;
        this.edgeLineType = EdgeLineType.SOLID;
        this.edgeStartArrowType = EdgeArrowType.NONE;
        this.edgeEndArrowType = EdgeArrowType.NONE;
        this.zDepth = -1;
        layoutPorts();
    }

    public DefaultGraphEdge(String id, GraphVertex startVertex, GraphVertex endVertex)
    {
        this(id, startVertex, PortType.CENTER, endVertex, PortType.CENTER);
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public int getZDepth()
    {
        return zDepth;
    }

    public void setZDepth(int zDepth)
    {
        this.zDepth = zDepth;
    }

    @Override
    public GraphVertex getStartVertex()
    {
        return startVertex;
    }

    @Override
    public GraphVertexPort getStartPort()
    {
        return startPort;
    }

    @Override
    public GraphVertex getEndVertex()
    {
        return endVertex;
    }

    @Override
    public GraphVertexPort getEndPort()
    {
        return endPort;
    }

    @Override
    public EdgeType getEdgeType()
    {
        return edgeType;
    }

    public void setEdgeType(EdgeType type)
    {
        edgeType = type;
    }

    @Override
    public EdgeLineType getEdgeLineType()
    {
        return edgeLineType;
    }

    public void setEdgeLineType(EdgeLineType type)
    {
        edgeLineType = type;
    }

    @Override
    public EdgeArrowType getEdgeStartArrowType()
    {
        return edgeStartArrowType;
    }

    public void setEdgeStartArrowType(EdgeArrowType type)
    {
        edgeStartArrowType = type;
    }

    @Override
    public EdgeArrowType getEdgeEndArrowType()
    {
        return edgeEndArrowType;
    }

    public void setEdgeEndArrowType(EdgeArrowType type)
    {
        edgeEndArrowType = type;
    }

    private void layoutPorts()
    {
        // Make sure the start port has been set.
        if (startPort == null)
        {
            startPort = startVertex.createPort(startPortType, false);
            startPort.addEdge(this);
        }

        // Make sure the end port has been set.
        if (endPort == null)
        {
            endPort = endVertex.createPort(endPortType, false);
            startPort.addEdge(this);
        }

        // Set normal (non-dynamic) ports.
        if ((startPortType == PortType.DYNAMIC_CENTER) || ((endPortType == PortType.DYNAMIC_CENTER)))
        {
            int startX;
            int startY;
            int endX;
            int endY;
            float xDistance;
            float yDistance;
            float distanceRatio;

            // Create a new port, based on the dynamic distance between the start and end vertices.
            startX = startVertex.getVertexCoordinates().x;
            startY = startVertex.getVertexCoordinates().y;
            endX = endVertex.getVertexCoordinates().x;
            endY = endVertex.getVertexCoordinates().y;
            xDistance = Math.abs(endX - startX);
            yDistance = Math.abs(endY - startY);
            distanceRatio = xDistance > yDistance ? yDistance/xDistance : xDistance/yDistance;

            // Set the start port if it is dynamic.
            if ((startPortType == PortType.DYNAMIC_CENTER))
            {
                if (xDistance > yDistance)
                {
                    if (startX < endX)
                    {
                        startPort.setPortType(PortType.EAST_CENTER);
                    }
                    else
                    {
                        startPort.setPortType(PortType.WEST_CENTER);
                    }
                }
                else
                {
                    if (startY < endY)
                    {
                        startPort.setPortType(PortType.SOUTH_CENTER);
                    }
                    else
                    {
                        startPort.setPortType(PortType.NORTH_CENTER);
                    }
                }
            }

            // Set the end port if it is dynamic.
            if ((endPortType == PortType.DYNAMIC_CENTER))
            {
                if (xDistance > yDistance)
                {
                    if (startX < endX)
                    {
                        endPort.setPortType(PortType.WEST_CENTER);
                    }
                    else
                    {
                        endPort.setPortType(PortType.EAST_CENTER);
                    }
                }
                else
                {
                    if (startY < endY)
                    {
                        endPort.setPortType(PortType.NORTH_CENTER);
                    }
                    else
                    {
                        endPort.setPortType(PortType.SOUTH_CENTER);
                    }
                }
            }
        }
    }

    @Override
    public void doLayout(Graph graph, GraphEdgeRenderer renderer, boolean isSelected)
    {
        layoutPorts();
    }
}
