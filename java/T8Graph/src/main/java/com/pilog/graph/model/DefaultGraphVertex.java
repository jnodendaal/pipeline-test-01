package com.pilog.graph.model;

import com.pilog.graph.model.GraphVertexPort.PortType;
import com.pilog.graph.view.Graph;
import com.pilog.graph.view.GraphVertexRenderer;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class DefaultGraphVertex implements GraphVertex
{
    protected String id;
    protected Rectangle bounds;
    protected Point coordinates;
    protected ArrayList<GraphVertex> children;
    protected ArrayList<GraphVertexPort> ports;
    protected int zDepth;
    protected boolean movable;
    protected boolean selectable;
    protected Object dataObject;

    public DefaultGraphVertex(String id, int x, int y, Object data)
    {
        this.id = id;
        this.coordinates = new Point(x, y);
        this.children = new ArrayList<GraphVertex>();
        this.ports = new ArrayList<GraphVertexPort>();
        this.dataObject = data;
        this.zDepth = 0;
        this.bounds = new Rectangle(x, y, 0, 0);
        this.selectable = true;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public Rectangle getVertexBounds()
    {
        return bounds;
    }

    @Override
    public int getZDepth()
    {
        return zDepth;
    }

    public void setZDepth(int zDepth)
    {
        this.zDepth = zDepth;
    }

    @Override
    public Point getVertexCoordinates()
    {
        return coordinates;
    }

    @Override
    public void setVertexCoordinates(Point coordinates)
    {
        this.coordinates = coordinates;
    }

    @Override
    public void translate(int dx, int dy)
    {
        // Translate this vertex's coordinates.
        coordinates.translate(dx, dy);

        // Send the translation down the vertex tree.
        for (GraphVertex child : children)
        {
            child.translate(dx, dy);
        }
    }

    @Override
    public int getChildCount()
    {
        return children.size();
    }

    @Override
    public Set<GraphEdge> getEdges()
    {
        Set<GraphEdge> edges;

        edges = new HashSet<>();
        for (GraphVertexPort port : ports)
        {
            edges.addAll(port.getEdges());
        }

        return edges;
    }

    @Override
    public Set<GraphVertex> getEdgeVertices()
    {
        Set<GraphVertex> vertices;

        vertices = new HashSet<>();
        for (GraphVertexPort port : ports)
        {
            for (GraphEdge edge : port.getEdges())
            {
                GraphVertex startVertex;

                startVertex = edge.getStartVertex();
                if (startVertex != this) vertices.add(startVertex);
                else vertices.add(edge.getEndVertex());
            }
        }

        return vertices;
    }

    @Override
    public List<GraphVertex> getChildren()
    {
        return children;
    }

    @Override
    public GraphVertex getChildAt(int index)
    {
        return children.get(index);
    }

    @Override
    public void addChild(GraphVertex vertex)
    {
        children.add(vertex);
    }

    @Override
    public void removeChild(GraphVertex vertex)
    {
        children.remove(vertex);
    }

    @Override
    public int getPortCount()
    {
        return ports.size();
    }

    @Override
    public GraphVertexPort getPort(int index)
    {
        return ports.get(index);
    }

    @Override
    public GraphVertexPort getPort(PortType portType)
    {
        for (GraphVertexPort port : ports)
        {
            if (port.getPortType() ==  portType)
            {
                return port;
            }
        }

        return null;
    }

    @Override
    public GraphVertexPort createPort(GraphVertexPort.PortType portType, boolean movable)
    {
        GraphVertexPort newPort;

        for (GraphVertexPort port : ports)
        {
            if (port.getPortType() == portType)
            {
                return port;
            }
        }

        newPort = new DefaultGraphVertexPort(this, portType, movable);
        ports.add(newPort);

        return newPort;
    }

    @Override
    public void removePort(GraphVertexPort port)
    {
        if (ports.contains(port))
        {
            port.setParentVertex(null);
            ports.remove(port);
        }
    }

    @Override
    public boolean isVertexSelectable()
    {
        return selectable;
    }

    @Override
    public void setVertexSelectable(boolean selectable)
    {
        this.selectable = selectable;
    }

    @Override
    public boolean isVertexMovable()
    {
        return movable;
    }

    @Override
    public void setVertexMovable(boolean movable)
    {
        this.movable = movable;
    }

    @Override
    public void setVertexDataObject(Object data)
    {
        dataObject = data;
    }

    @Override
    public Object getVertexDataObject()
    {
        return dataObject;
    }

    @Override
    public void doLayout(Graph graph, GraphVertexRenderer renderer, boolean isSelected)
    {
        // Do the layout of child nodes of this node.
        doChildLayout(graph, renderer, isSelected);

        // Recalculate the vertex bounds.
        bounds = calculateBounds(renderer.getVertexSize(graph, this, isSelected));

        // Reposition the ports of the vertex according to its display size.
        repositionPorts(bounds);
    }

    protected void doChildLayout(Graph graph, GraphVertexRenderer renderer, boolean isSelected)
    {
        for (GraphVertex child : children)
        {
            child.doLayout(graph, renderer, isSelected);
        }
    }

    protected Rectangle calculateBounds(Dimension size)
    {
        Rectangle vertexBounds;

        // Calculate the bounds of the vertex, as it its displayed on the grid.
        vertexBounds = new Rectangle(0, 0, (int)size.getWidth(), (int)size.getHeight());
        vertexBounds.translate(coordinates.x - (vertexBounds.width / 2), coordinates.y - (vertexBounds.height / 2));
        return vertexBounds;
    }

    protected void repositionPorts(Rectangle bounds)
    {
        int halfVertexWidth;
        int halfVertexHeight;

        halfVertexWidth = (int)bounds.getWidth()/2;
        halfVertexHeight = (int)bounds.getHeight()/2;
        for (int i = 0; i < getPortCount(); i++)
        {
            GraphVertexPort port;
            Point portCoordinates;
            PortType portType;

            port = getPort(i);
            portType = port.getPortType();
            portCoordinates = port.getPortRelativeCoordinates();

            if (portType == PortType.NORTH_CENTER)
            {
                portCoordinates.setLocation(0, -halfVertexHeight);
            }
            else if (portType == PortType.EAST_CENTER)
            {
                portCoordinates.setLocation(halfVertexWidth, 0);
            }
            else if (portType == PortType.SOUTH_CENTER)
            {
                portCoordinates.setLocation(0, halfVertexHeight);
            }
            else if (portType == PortType.WEST_CENTER)
            {
                portCoordinates.setLocation(-halfVertexWidth, 0);
            }
            else if (portType == PortType.NORTH_EAST)
            {
                portCoordinates.setLocation(halfVertexWidth, -halfVertexHeight);
            }
            else if (portType == PortType.SOUTH_EAST)
            {
                portCoordinates.setLocation(halfVertexWidth, halfVertexHeight);
            }
            else if (portType == PortType.NORTH_WEST)
            {
                portCoordinates.setLocation(-halfVertexWidth, -halfVertexHeight);
            }
            else if (portType == PortType.SOUTH_WEST)
            {
                portCoordinates.setLocation(-halfVertexWidth, halfVertexHeight);
            }
        }
    }
}
