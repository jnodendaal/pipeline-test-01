package com.pilog.graph.exceptions;

/**
 * @author Bouwer du Preez
 */
public class InvalidEdgeException extends RuntimeException
{
    public InvalidEdgeException(String message)
    {
        super(message);
    }
}
