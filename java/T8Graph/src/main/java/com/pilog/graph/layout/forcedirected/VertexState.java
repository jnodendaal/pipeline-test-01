package com.pilog.graph.layout.forcedirected;

import com.pilog.graph.model.GraphVertex;
import java.awt.Point;

/**
 * @author Bouwer du Preez
 */
public class VertexState
{
    private final GraphVertex vertex;
    private Vector position;
    private double mass;
    private Vector velocity;
    private Vector acceleration;

    public VertexState(GraphVertex vertex, double mass)
    {
        this.vertex = vertex;
        this.position = new Vector(vertex.getVertexCoordinates().getX(), vertex.getVertexCoordinates().getY());
        this.mass = mass; // mass
        this.velocity = new Vector(0, 0);
        this.acceleration = new Vector(0, 0);
    }

    public GraphVertex getVertex()
    {
        return vertex;
    }

    public Vector getPosition()
    {
        return position;
    }

    public void setPosition(Vector position)
    {
        this.position = position;
    }

    public double getMass()
    {
        return mass;
    }

    public void setMass(double mass)
    {
        this.mass = mass;
    }

    public Vector getVelocity()
    {
        return velocity;
    }

    public void setVelocity(Vector velocity)
    {
        this.velocity = velocity;
    }

    public Vector getAcceleration()
    {
        return acceleration;
    }

    public void setAcceleration(Vector acceleration)
    {
        this.acceleration = acceleration;
    }

    public void applyForce(Vector force)
    {
        this.acceleration = this.acceleration.add(force.divide(this.mass));
    }

    public void applyPositionToVertex()
    {
        vertex.setVertexCoordinates(new Point((int)position.getX(), (int)position.getY()));
    }
}
