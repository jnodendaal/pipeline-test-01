package com.pilog.graph.view.event;

import com.pilog.graph.model.GraphVertex;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class VerticesMovedEvent
{
    private final List<GraphVertex> vertices;

    public VerticesMovedEvent(List<GraphVertex> vertices)
    {
        this.vertices = vertices;
    }

    public List<GraphVertex> getVertices()
    {
        return vertices;
    }
}
