package com.pilog.graph.view;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface GraphSelectionListener extends EventListener
{
    public void selectionChanged(GraphSelectionEvent e);
}
