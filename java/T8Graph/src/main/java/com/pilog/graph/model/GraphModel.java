package com.pilog.graph.model;

import com.pilog.graph.model.event.GraphModelListener;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface GraphModel
{
    public void addGraphModelListener(GraphModelListener listener);
    public void removeGraphModelListener(GraphModelListener listener);

    public Collection<GraphElement> getGraphElements();
    public List<GraphVertex> getVertices();
    public List<GraphEdge> getEdges();

    public int getVertexCount();
    public GraphVertex getVertex(String id);
    public GraphVertex getVertex(int index);
    public int getEdgeCount();
    public GraphEdge getEdge(String id);
    public GraphEdge getEdge(int index);
    public Object getChildVertex(GraphVertex parent, int index);
    public int getChildCount(GraphVertex parent);
    public int getIndexOfChild(GraphVertex parent, Object child);
}
