package com.pilog.graph.view;

import com.pilog.graph.layout.GraphLayout;
import com.pilog.graph.model.DefaultGraphModel;
import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphElement;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.event.GraphViewListener;
import com.pilog.graph.view.event.VerticesMovedEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public class Graph extends JComponent
{
    private GraphModel graphModel;
    private GraphVertexRenderer vertexRenderer;
    private GraphEdgeRenderer edgeRenderer;
    private List<MouseListener> mouseListeners;
    private final List<GraphViewListener> viewListeners;
    private GraphSelectionModel selectionModel;
    private GraphBackgroundPainter backgroundPainter;
    private CellRendererPane rendererPane;
    private int previousMouseX;
    private int previousMouseY;
    private int startMouseX;
    private int startMouseY;
    private boolean motionSelection;
    private double zoomLevel;
    private Point.Double translation;
    private boolean snapToGridEnabled;
    private boolean gridEnabled;
    private boolean axisEnabled;
    private boolean editable;
    private int gridXSize = 10;
    private int gridYSize = 10;
    private AnimationThread animationThread;
    private GraphLayout layout;

    public Graph(GraphModel model)
    {
        graphModel = model;
        vertexRenderer = new DefaultVertexRenderer();
        edgeRenderer = new VertexSelectionEdgeRenderer();
        mouseListeners = new ArrayList<>();
        viewListeners = new ArrayList<>();
        selectionModel = new DefaultGraphSelectionModel();
        rendererPane = new CellRendererPane();
        motionSelection = false;
        zoomLevel = 1.0;
        translation = new Point.Double(0.0, 0.0);
        gridEnabled = false;
        axisEnabled = false;
        snapToGridEnabled = false;
        editable = true;

        this.add(rendererPane, java.awt.BorderLayout.CENTER);
        this.setDoubleBuffered(true);
        this.addMouseListener(new GraphMouseListener());
        this.addMouseMotionListener(new GraphMouseMotionListener());
        this.addMouseWheelListener(new GraphMouseWheelListener());
    }

    public Graph()
    {
        this(new DefaultGraphModel());
    }

    public GraphLayout getGraphLayout()
    {
        return layout;
    }

    public void setGraphLayout(GraphLayout layout)
    {
        this.layout = layout;
    }

    public int getGridXSize()
    {
        return gridXSize;
    }

    public void setGridXSize(int gridXSize)
    {
        this.gridXSize = gridXSize;
    }

    public int getGridYSize()
    {
        return gridYSize;
    }

    public void setGridYSize(int gridYSize)
    {
        this.gridYSize = gridYSize;
    }

    public void setSnapToGridEnabled(boolean enabled)
    {
        this.snapToGridEnabled = enabled;
    }

    public void setGridEnabled(boolean enabled)
    {
        this.gridEnabled = enabled;
    }

    public void setAxisEnabled(boolean enabled)
    {
        this.axisEnabled = enabled;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
    }

    public void addViewListener(GraphViewListener listener)
    {
        viewListeners.add(listener);
    }

    public boolean removeViewListener(GraphViewListener listener)
    {
        return viewListeners.remove(listener);
    }

    public void addGraphSelectionListener(GraphSelectionListener listener)
    {
        selectionModel.addGraphSelectionListener(listener);
    }

    public void removeGraphSelectionListener(GraphSelectionListener listener)
    {
        selectionModel.removeGraphSelectionListener(listener);
    }

    public void setVertexRenderer(GraphVertexRenderer renderer)
    {
        vertexRenderer = renderer;
    }

    public void setEdgeRenderer(GraphEdgeRenderer renderer)
    {
        edgeRenderer = renderer;
    }

    public GraphBackgroundPainter getBackgroundPainter()
    {
        return backgroundPainter;
    }

    public void setBackgroundPainter(GraphBackgroundPainter backgroundPainter)
    {
        this.backgroundPainter = backgroundPainter;
        repaint();
    }

    public void setModel(GraphModel model)
    {
        selectionModel.clearSelection();
        graphModel = model;
        if (layout != null) layout.setModel(model);
        repaint();
    }

    public GraphModel getModel()
    {
        return graphModel;
    }

    public GraphSelectionModel getSelectionModel()
    {
        return selectionModel;
    }

    public List<GraphVertex> getSelectedVertices()
    {
        return selectionModel.getSelectedVertices();
    }

    public void setSelectedVertices(Collection<GraphVertex> vertices)
    {
        selectionModel.setSelectedVertices(vertices);
        repaint();
    }

    public List<GraphEdge> getSelectedEdges()
    {
        return selectionModel.getSelectedEdges();
    }

    public void setSelectedEdges(Collection<GraphEdge> edges)
    {
        selectionModel.setSelectedEdges(edges);
        repaint();
    }

    public void clearSelection()
    {
        selectionModel.clearSelection();
        repaint();
    }

    public void setMotionSelection(boolean value)
    {
        motionSelection = value;
    }

    public void centerGraph()
    {
        // Reset the position to the center point
        this.translation = new Point.Double(0.0, 0.0);
        this.previousMouseX = 0;
        this.previousMouseY = 0;

        repaint();
    }

    public void doGraphLayout()
    {
        // Do the vertex layout.
        for (int vertexIndex = 0; vertexIndex < graphModel.getVertexCount(); vertexIndex++)
        {
            GraphVertex vertex;

            vertex = graphModel.getVertex(vertexIndex);
            vertex.doLayout(this, vertexRenderer, false);
        }

        // Do the edge layout.
        for (int edgeIndex = 0; edgeIndex < graphModel.getEdgeCount(); edgeIndex++)
        {
            GraphEdge edge;

            edge = graphModel.getEdge(edgeIndex);
            edge.doLayout(this, edgeRenderer, false);
        }
    }

    @Override
    public void paint(Graphics g)
    {
        Rectangle clipBounds;
        Rectangle2D.Double viewBounds;
        Graphics2D g2;

        // Apply the zoom level
        g2 = (Graphics2D)g;
        g2.scale(zoomLevel, zoomLevel);
        g2.translate(translation.x, translation.y);

        // Get the clipping bounds of the drawing operation.
        clipBounds = g2.getClipBounds();

        // Get the view bounds on the graph.
        viewBounds = getGraphViewBounds();

        // Clear the area that will be updated.
        if (backgroundPainter == null)
        {
            g2.setColor(Color.WHITE);
            g2.fillRect(clipBounds.x, clipBounds.y, clipBounds.width, clipBounds.height);
            g2.setColor(getForeground());
        }
        else
        {
            backgroundPainter.paintBackground(g2);
        }

        // Paint the grid if enabled.
        if (gridEnabled)
        {
            // Set the grid color.
            g2.setColor(Color.LIGHT_GRAY);

            // Draw lines intersecting the positive x axis.
            for (int x = 0; x < viewBounds.x + viewBounds.width; x += 100)
            {
                g2.drawLine(x, (int)viewBounds.y, x, (int)(viewBounds.y + viewBounds.height));
            }

            // Draw lines intersecting the negative x axis.
            for (int x = 0; x > viewBounds.x; x -= 100)
            {
                g2.drawLine(x, (int)viewBounds.y, x, (int)(viewBounds.y + viewBounds.height));
            }

            // Draw lines intersecting the positive y axis.
            for (int y = 0; y < viewBounds.y + viewBounds.height; y += 100)
            {
                g2.drawLine((int)viewBounds.x, y, (int)(viewBounds.x + viewBounds.width), y);
            }

            // Draw lines intersecting the negative y axis.
            for (int y = 0; y > viewBounds.y; y -= 100)
            {
                g2.drawLine((int)viewBounds.x, y, (int)(viewBounds.x + viewBounds.width), y);
            }
        }

        // If axis are enabled, paint them now.
        if (axisEnabled)
        {
            g2.setColor(Color.BLUE);
            g2.drawLine(0, 0, 100, 0);
            g2.setColor(Color.RED);
            g2.drawLine(0, 0, 0, 100);
            g2.setColor(Color.GREEN);
            g2.draw(getGraphBounds());
        }

        // Paint the graph if the model contains data.
        if (graphModel != null)
        {
            // Paint all the model vertices.
            if (graphModel.getVertexCount() > 0)
            {
                Collection<GraphElement> modelElements;

                // This block adds the renderer component once to the rendererPane (wihtout this code, the first cell does not paint correctly - check for origin of problem).
                {
                    GraphVertex vertex;
                    Component rendererComponent;

                    vertex = graphModel.getVertex(0);
                    rendererComponent = vertexRenderer.getRendererComponent(this, vertex, selectionModel.isVertexSelected(vertex));
                    rendererPane.add(rendererComponent);
                }

                // Get all model elements and paint them.
                modelElements = graphModel.getGraphElements();
                for (GraphElement element : modelElements)
                {
                    paintElement(g2, element);
                }
            }
        }

        // Clear the cell renderer pane so that renderer components can be garbage collected.
        rendererPane.removeAll();
    }

    private void paintElement(Graphics2D g2, GraphElement element)
    {
        if (element instanceof GraphEdge) paintEdge(g2, (GraphEdge)element);
        else paintVertex(g2, (GraphVertex)element);
    }

    /**
     * This method will paint the supplied edge IF it intersects the clip bounds
     * of the supplied graphics context.
     *
     * @param g2 The graphics context onto which the edge will be painted.
     * @param edge The edge to paint.
     */
    private void paintEdge(Graphics2D g2, GraphEdge edge)
    {
        Rectangle clipBounds;
        Rectangle edgeBounds;
        Point startPoint;
        Point endPoint;

        // Get the clip bounds of the graphics context and compute the edge bounds.
        clipBounds = g2.getClipBounds();
        startPoint = edge.getStartPort().getPortCoordinates();
        endPoint = edge.getEndPort().getPortCoordinates();
        edgeBounds = new Rectangle();
        edgeBounds.setLocation(startPoint.x < endPoint.x ? startPoint.x : endPoint.x, startPoint.y < endPoint.y ? startPoint.y : endPoint.y);
        edgeBounds.setSize(Math.abs(startPoint.x - endPoint.x), Math.abs(startPoint.y - endPoint.y));
        if (edgeBounds.width == 0) edgeBounds.width = 1;
        if (edgeBounds.height == 0) edgeBounds.height = 1;

        // Only paint the edge if it intersects the clip bounds.
        if (clipBounds.intersects(edgeBounds))
        {
            Component rendererComponent;

            rendererComponent = edgeRenderer.getRendererComponent(this, edge, selectionModel.isEdgeSelected(edge));
            rendererComponent.paint(g2);
        }
    }

    /**
     * This method will paint the supplied vertex IF it intersects the clip
     * bounds of the supplied graphics context.
     *
     * @param g2 The graphics context onto which the vertex will be painted.
     * @param vertex The vertex to paint.
     */
    private void paintVertex(Graphics2D g2, GraphVertex vertex)
    {
        Component rendererComponent;
        Rectangle clipBounds;
        Rectangle componentBounds;

        rendererComponent = vertexRenderer.getRendererComponent(this, vertex, selectionModel.isVertexSelected(vertex));
        if (!rendererComponent.isValid()) rendererComponent.validate();

        clipBounds = g2.getClipBounds();
        componentBounds = vertex.getVertexBounds();

        // Only paint the vertex if it intersects the clip bounds.
        if (clipBounds.intersects(componentBounds))
        {
            rendererPane.add(rendererComponent);
            rendererPane.paintComponent(g2, rendererComponent, this, componentBounds.x, componentBounds.y, componentBounds.width, componentBounds.height);
        }
    }

    /**
     * Returns the bounds of the graph.
     *
     * @return The current graph bounds.
     */
    public Rectangle2D.Double getGraphBounds()
    {
        Rectangle2D.Double bounds;
        Point.Double upperLeft;
        Point.Double lowerRight;

        upperLeft = new Point.Double(0, 0);
        lowerRight = new Point.Double(0, 0);
        for (GraphVertex vertex : graphModel.getVertices())
        {
            Point vertexCoordinates;

            vertexCoordinates = vertex.getVertexCoordinates();
            if (vertexCoordinates.x < upperLeft.x) upperLeft.x = vertexCoordinates.x;
            if (vertexCoordinates.y < upperLeft.y) upperLeft.y = vertexCoordinates.y;
            if (vertexCoordinates.x > lowerRight.x) lowerRight.x = vertexCoordinates.x;
            if (vertexCoordinates.y > lowerRight.y) lowerRight.y = vertexCoordinates.y;
        }

        bounds = new Rectangle2D.Double();
        bounds.setRect(upperLeft.x, upperLeft.y, lowerRight.x - upperLeft.x, lowerRight.y - upperLeft.y);
        return bounds;
    }

    /**
     * Returns the bounds of the currently visible area on the graph coordinate
     * system.
     *
     * @return The current graph view bounds (graph coordinates).
     */
    public Rectangle2D.Double getGraphViewBounds()
    {
        Rectangle2D.Double bounds;
        Point.Double upperLeft;
        Point.Double lowerRight;

        upperLeft = new Point.Double(0, 0);
        lowerRight = new Point.Double(getWidth(), getHeight());

        convertComponentLocationToGraphLocation(upperLeft);
        convertComponentLocationToGraphLocation(lowerRight);
        bounds = new Rectangle2D.Double();
        bounds.setRect(upperLeft.x, upperLeft.y, lowerRight.x - upperLeft.x, lowerRight.y - upperLeft.y);
        return bounds;
    }

    /**
     * This method converts the supplied point location on the component
     * coordinate system to the corresponding point location on the graph
     * coordinate system.
     *
     * @param location The point location on the component coordinate system
     * that will be converted.
     */
    public void convertComponentLocationToGraphLocation(Point.Double location)
    {
        // Apply the global zoom level (scaling).
        location.x = location.x * (1.0/zoomLevel);
        location.y = location.y * (1.0/zoomLevel);

        // Apply the global translation.
        location.x -= translation.x;
        location.y -= translation.y;
    }

    /**
     * This method converts the supplied point location on the graph coordinate
     * system to the corresponding point location on the component coordinate
     * system.
     *
     * @param location The point location on the component coordinate system
     * that will be converted.
     */
    public void convertGraphLocationToComponentLocation(Point.Double location)
    {
        // Apply the global zoom level (scaling).
        location.x = location.x * (zoomLevel);
        location.y = location.y * (zoomLevel);

        // Apply the global translation.
        location.x += translation.x;
        location.y += translation.y;
    }

    /**
     * Converts the supplied distance on the coordinate system of the component
     * to the corresponding distance on the coordinate system of the graph.
     *
     * @param distance The distance to convert.
     * @return The converted distance.
     */
    public double convertComponentDistanceToGraphDistance(double distance)
    {
        return distance * (1.0/zoomLevel);
    }

    /**
     * Converts the supplied distance on the coordinate system of the graph
     * to the corresponding distance on the coordinate system of the component.
     *
     * @param distance The distance to convert.
     * @return The converted distance.
     */
    public double convertGraphDistanceToComponentDistance(double distance)
    {
        return distance * zoomLevel;
    }

    public void selectVerticesAtGraphLocation(double x, double y, boolean clearPreviousSelection)
    {
        if (clearPreviousSelection)
        {
            selectionModel.clearSelection();
        }

        selectionModel.addSelectedVertices(getVerticesAtGraphLocation(x, y));
        repaint();
    }

    public void selectNextVertexAtGraphLocation(double x, double y, boolean clearPreviousSelection)
    {
        List<GraphVertex> vertices;
        GraphVertex nextVertex = null;

        vertices = getVerticesAtGraphLocation(x, y);
        for (GraphVertex vertex : vertices)
        {
            if (vertex.isVertexSelectable())
            {
                if (nextVertex == null) nextVertex = vertex;
                else
                {
                    if (nextVertex.getZDepth() < vertex.getZDepth()) nextVertex = vertex;
                }
            }
        }

        // We have to make sure to perform only one action on the selection model to prevent redundant events from firing.
        // This means, we either want to set the new list of selected vertices, add a vertex to the existing selection or clear the existing selection.
        if (clearPreviousSelection)
        {
            if (nextVertex != null)
            {
                ArrayList<GraphVertex> selectedVertexList;

                selectedVertexList = new ArrayList<>();
                selectedVertexList.add(nextVertex);
                selectionModel.setSelectedVertices(selectedVertexList);
                repaint();
            }
            else
            {
                selectionModel.clearSelection();
                repaint();
            }
        }
        else if (nextVertex != null)
        {
            selectionModel.addSelectedVertex(nextVertex);
            repaint();
        }
    }

    public void selectVertex(GraphVertex vertex, boolean clearPreviousSelection)
    {
        if (clearPreviousSelection)
        {
            selectionModel.clearSelection();
        }

        selectionModel.addSelectedVertex(vertex);
        repaint();
    }

    public ArrayList<GraphVertex> getVerticesAtComponentLocation(int x, int y)
    {
        Point.Double location;

        location = new Point.Double(x, y);
        convertComponentLocationToGraphLocation(location);
        return getVerticesAtGraphLocation(location.x, location.y);
    }

    public ArrayList<GraphVertex> getVerticesAtGraphLocation(double x, double y)
    {
        ArrayList<GraphVertex> vertices;

        vertices = new ArrayList<>();
        for (int i = 0; i < graphModel.getVertexCount(); i++)
        {
            GraphVertex vertex;
            Rectangle vertexBounds;

            vertex = graphModel.getVertex(i);
            vertexBounds = vertex.getVertexBounds();
            if (vertexBounds.contains(x, y))
            {
                vertices.add(vertex);
            }
        }

        return vertices;
    }

    /**
     * Animates the graph according to the currently set layout.
     */
    public void animate()
    {
        if ((animationThread == null) || (!animationThread.isRunning()))
        {
            animationThread = new AnimationThread();
            animationThread.start();
        }
    }

    public void updateLayout()
    {
        if (layout != null)
        {
            synchronized(layout)
            {
                layout.update();
                animate();
            }
        }
    }

    private void fireVerticesMovedEvent(List<GraphVertex> vertices)
    {
        VerticesMovedEvent event;

        event = new VerticesMovedEvent(vertices);
        for (GraphViewListener listener : viewListeners)
        {
            listener.verticesMoved(event);
        }
    }

    private class GraphMouseListener extends MouseAdapter
    {
        @Override
        public void mousePressed(MouseEvent e)
        {
            // Only react to left-clicks when selecting nodes.
            if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK)
            {
                Point.Double scaledLocation;

                // Request the focus.
                requestFocusInWindow();

                previousMouseX = e.getX();
                previousMouseY = e.getY();
                scaledLocation = new Point.Double(e.getPoint().x, e.getPoint().y);
                convertComponentLocationToGraphLocation(scaledLocation);
                selectNextVertexAtGraphLocation(scaledLocation.x, scaledLocation.y, !e.isShiftDown());
                repaint();
            }
            else // We have to set these variables to allow the mouse-dragged action to have up-to-date values.
            {
                previousMouseX = e.getX();
                previousMouseY = e.getY();
                startMouseX = e.getX();
                startMouseY = e.getY();
            }
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
            if (motionSelection)
            {
                clearSelection();
            }
        }
    }

    private class GraphMouseMotionListener implements MouseMotionListener
    {
        @Override
        public void mouseDragged(MouseEvent event)
        {
            int mouseX;
            int mouseY;

            // Get the X and Y coordinates of the mouse pointer.
            mouseX = event.getX();
            mouseY = event.getY();

            // Handle the event depending on the mouse button pressed.
            if ((event.getModifiersEx() & MouseEvent.BUTTON3_DOWN_MASK) == MouseEvent.BUTTON3_DOWN_MASK)
            {
                // Set the global translation.
                translation.x += convertComponentDistanceToGraphDistance(mouseX - previousMouseX);
                translation.y += convertComponentDistanceToGraphDistance(mouseY - previousMouseY);

                // Keep track of where the mouse was when the event was processed last.
                previousMouseX = mouseX;
                previousMouseY = mouseY;
                repaint();
            }
            else if (isEditable()) // Left button (move vertices).
            {
                if (selectionModel.getSelectedVertexCount() > 0)
                {
                    List<GraphVertex> selectedVertices;

                    selectedVertices = selectionModel.getSelectedVertices();
                    if (!selectedVertices.isEmpty())
                    {
                        for (GraphVertex selectedVertex : selectedVertices)
                        {
                            if (selectedVertex.isVertexMovable())
                            {
                                if (snapToGridEnabled)
                                {
                                    Point.Double pointerLocation;
                                    int xLocation;
                                    int yLocation;

                                    // Calculate the translation based on the grid size.
                                    pointerLocation = new Point.Double(mouseX, mouseY);
                                    convertComponentLocationToGraphLocation(pointerLocation);
                                    xLocation = gridXSize * (int)Math.round(pointerLocation.getX()/gridXSize);
                                    yLocation = gridYSize * (int)Math.round(pointerLocation.getY()/gridYSize);

                                    // Set the vertex's coordinates to the pointer location.
                                    selectedVertex.setVertexCoordinates(new Point(xLocation, yLocation));
                                }
                                else
                                {
                                    Point.Double pointerLocation;
                                    int xLocation;
                                    int yLocation;

                                    // Calculate the translation.
                                    pointerLocation = new Point.Double(mouseX, mouseY);
                                    convertComponentLocationToGraphLocation(pointerLocation);
                                    xLocation = (int)Math.round(pointerLocation.getX());
                                    yLocation = (int)Math.round(pointerLocation.getY());

                                    // Set the vertex's coordinates to the pointer location.
                                    selectedVertex.setVertexCoordinates(new Point(xLocation, yLocation));
                                }
                            }
                        }

                        // Update the layout if one is set.
                        updateLayout();

                        // Fire the event to indicate that vertices were moved by the user.
                        fireVerticesMovedEvent(selectedVertices);
                    }

                    // Keep track of where the mouse was when the event was processed last.
                    previousMouseX = mouseX;
                    previousMouseY = mouseY;

                    // Do the graph layout.
                    doGraphLayout();

                    // Repaint the graph with the changes.
                    repaint();
                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent e)
        {
            if (motionSelection)
            {
                selectVerticesAtGraphLocation(e.getX(), e.getY(), !e.isShiftDown());

                if (!hasFocus())
                {
                    requestFocusInWindow();
                }
            }
        }
    }

    private class GraphMouseWheelListener implements MouseWheelListener
    {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e)
        {
            Point.Double graphPointBeforeZoom;
            Point.Double graphPointAfterZoom;
            double xDistance;
            double yDistance;
            int notches;

            // Get the amount of nothes that was rotated on the mouse wheel.
            notches = e.getWheelRotation();

            // Get the point on the graph pointed at before the zoom is adjusted.
            graphPointBeforeZoom = new Point.Double(e.getPoint().x, e.getPoint().y);
            convertComponentLocationToGraphLocation(graphPointBeforeZoom);

            // Adjust the global zoom level.
            zoomLevel -= (notches * 0.1);
            if (zoomLevel < 0.1) zoomLevel = 0.1; // Make sure the zoom level is never less than the zoom adjustment factor since this would cause the space-time continuum to collapse.

            // Get the point on the graph pointed at after the zoom has been adjusted.
            graphPointAfterZoom = new Point.Double(e.getPoint().x, e.getPoint().y);
            convertComponentLocationToGraphLocation(graphPointAfterZoom);

            // Compute the coordinate delta between the location pointed at before and after the zoom.
            xDistance = graphPointAfterZoom.x - graphPointBeforeZoom.x;
            yDistance = graphPointAfterZoom.y - graphPointBeforeZoom.y;

            //translation = graphEventLocation;
            translation.x += xDistance;
            translation.y += yDistance;

            // Repaint to reflect the changes.
            repaint();
        }
    }

    private class AnimationThread extends Thread
    {
        private volatile boolean running;

        public AnimationThread()
        {
            this.running = false;
        }

        public boolean isRunning()
        {
            return this.running;
        }

        @Override
        public void run()
        {
            running = true;
            while (running)
            {
                synchronized(layout)
                {
                    running = layout.renderNextStep(1);
                }

                doGraphLayout();
                repaint();

                try
                {
                    Thread.sleep(10);
                }
                catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
            }

            running = false;
            doGraphLayout();
            repaint();
        }
    }
}
