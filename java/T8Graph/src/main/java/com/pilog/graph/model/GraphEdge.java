package com.pilog.graph.model;

import com.pilog.graph.view.Graph;
import com.pilog.graph.view.GraphEdgeRenderer;

/**
 * @author Bouwer du Preez
 */
public interface GraphEdge extends GraphElement
{
    public enum EdgeType {STRAIGHT, ANGLED, BEZIER};
    public enum EdgeLineType {SOLID, DASHED};
    public enum EdgeArrowType {NONE, LINE, FILLED};

    public EdgeType getEdgeType();
    public EdgeLineType getEdgeLineType();
    public EdgeArrowType getEdgeStartArrowType();
    public EdgeArrowType getEdgeEndArrowType();
    public GraphVertex getStartVertex();
    public GraphVertex getEndVertex();
    public GraphVertexPort getStartPort();
    public GraphVertexPort getEndPort();

    // Rendering operations.
    public void doLayout(Graph graph, GraphEdgeRenderer renderer, boolean isSelected);
}
