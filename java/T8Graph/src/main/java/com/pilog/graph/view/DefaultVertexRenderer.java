package com.pilog.graph.view;

import com.pilog.graph.model.GraphVertex;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public class DefaultVertexRenderer extends JComponent implements GraphVertexRenderer
{
    protected boolean isSelected;
    protected Graph graph;
    protected GraphVertex vertex;

    @Override
    public Component getRendererComponent(Graph graph, GraphVertex vertex, boolean isSelected)
    {
        Dimension size;

        this.graph = graph;
        this.vertex = vertex;
        this.isSelected = isSelected;

        size = vertex.getVertexBounds().getSize();
        this.setSize(size);
        this.setPreferredSize(size);
        return this;
    }

    @Override
    public Dimension getVertexSize(Graph graph, GraphVertex vertex, boolean isSelected)
    {
        return new Dimension(50, 50);
    }

    @Override
    public void paint(Graphics g)
    {
        g.setColor(isSelected ? Color.GRAY : Color.BLACK);
        g.fillOval(0, 0, 50, 50);
    }
}
