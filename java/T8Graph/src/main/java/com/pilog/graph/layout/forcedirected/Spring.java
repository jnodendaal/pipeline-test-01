package com.pilog.graph.layout.forcedirected;

/**
 * @author Bouwer du Preez
 */
public class Spring
{
    private VertexState state1;
    private VertexState state2;
    private double length;
    private double k;

    public Spring(VertexState state1, VertexState state2, double length, double k)
    {
        this.state1 = state1;
        this.state2 = state2;
        this.length = length; // spring length at rest
        this.k = k; // spring constant or stiffness (See Hooke's law).
    }

    public VertexState getState1()
    {
        return state1;
    }

    public void setState1(VertexState state1)
    {
        this.state1 = state1;
    }

    public VertexState getState2()
    {
        return state2;
    }

    public void setState2(VertexState state2)
    {
        this.state2 = state2;
    }

    public double getLength()
    {
        return length;
    }

    public void setLength(double length)
    {
        this.length = length;
    }

    public double getK()
    {
        return k;
    }

    public void setK(double k)
    {
        this.k = k;
    }
}
