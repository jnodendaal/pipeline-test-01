package com.pilog.graph.view;

import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphVertex;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public class DefaultGraphSelectionModel implements GraphSelectionModel
{
    private final ArrayList<PropertyChangeListener> propertyChangeListeners;
    private final ArrayList<GraphSelectionListener> graphSelectionListeners;
    private ArrayList<GraphVertex> selectedVertices;
    private ArrayList<GraphEdge> selectedEdges;
    private int selectionMode;

    public DefaultGraphSelectionModel()
    {
        propertyChangeListeners = new ArrayList<PropertyChangeListener>();
        graphSelectionListeners = new ArrayList<GraphSelectionListener>();
        selectedVertices = new ArrayList<GraphVertex>();
        selectedEdges = new ArrayList<GraphEdge>();
        selectionMode = GraphSelectionModel.MULTIPLE_SELECTION;
    }

    @Override
    public void clearSelection()
    {
        if (selectedVertices.size() > 0)
        {
            GraphSelectionEvent e;

            selectedVertices.clear();
            e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, null, selectedVertices, selectedEdges);

            // Fire the event to indicate the selection change.
            fireValueChanged(e);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeListeners.add(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeListeners.remove(listener);
    }

    @Override
    public void addGraphSelectionListener(GraphSelectionListener listener)
    {
        graphSelectionListeners.add(listener);
    }

    @Override
    public void removeGraphSelectionListener(GraphSelectionListener listener)
    {
        graphSelectionListeners.remove(listener);
    }

    @Override
    public void addSelectedVertex(GraphVertex vertex)
    {
        if (!selectedVertices.contains(vertex))
        {
            GraphSelectionEvent e;

            selectedVertices.add(vertex);
            e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, vertex, null, null, null);

            // Fire the event to indicate the selection change.
            fireValueChanged(e);
        }
    }

    @Override
    public void removeSelectedVertex(GraphVertex vertex)
    {
        if (!selectedVertices.contains(vertex))
        {
            GraphSelectionEvent e;

            selectedVertices.remove(vertex);
            e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, null, vertex, null);

            // Fire the event to indicate the selection change.
            fireValueChanged(e);
        }
    }

    @Override
    public void addSelectedVertices(Collection<GraphVertex> vertices)
    {
        ArrayList<GraphVertex> newVertices;

        // Create a new collection of all the vertices that are not yet selected.
        newVertices = new ArrayList<GraphVertex>();
        for (GraphVertex vertex : vertices)
        {
            if (!selectedVertices.contains(vertex)) newVertices.add(vertex);
        }

        // Add all of the new vertices to the selected collection.
        if (newVertices.size() > 0)
        {
            GraphSelectionEvent e;

            selectedVertices.addAll(newVertices);
            e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, newVertices, null, null, null);

            // Fire the event to indicate the selection change.
            fireValueChanged(e);
        }
    }

    @Override
    public void removeSelectedVertices(Collection<GraphVertex> vertices)
    {
        ArrayList<GraphVertex> existingVertices;

        // Create a new collection of all the vertices that exist in the selected collection.
        existingVertices = new ArrayList<GraphVertex>();
        for (GraphVertex vertex : vertices)
        {
            if (selectedVertices.contains(vertex)) existingVertices.add(vertex);
        }

        // Remove all of the specified vertices from the selected collection.
        if (existingVertices.size() > 0)
        {
            GraphSelectionEvent e;

            selectedVertices.removeAll(existingVertices);
            e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, null, existingVertices, null);

            // Fire the event to indicate the selection change.
            fireValueChanged(e);
        }
    }

    @Override
    public ArrayList<GraphVertex> getSelectedVertices()
    {
        return selectedVertices;
    }

    @Override
    public void setSelectedVertices(Collection<GraphVertex> vertices)
    {
        GraphSelectionEvent e;

        selectedVertices = new ArrayList<>(vertices);
        e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, new ArrayList<>(vertices), null, selectedVertices, null);

        // Fire the event to indicate the selection change.
        fireValueChanged(e);
    }

    @Override
    public void addSelectedEdge(GraphEdge edge)
    {
        GraphSelectionEvent e;

        selectedEdges.add(edge);
        e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, edge, null, null);

        // Fire the event to indicate the selection change.
        fireValueChanged(e);
    }

    @Override
    public void removeSelectedEdge(GraphEdge edge)
    {
        GraphSelectionEvent e;

        selectedEdges.remove(edge);
        e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, null, null, edge);

        // Fire the event to indicate the selection change.
        fireValueChanged(e);
    }

    @Override
    public void addSelectedEdges(Collection<GraphEdge> edges)
    {
        GraphSelectionEvent e;

        selectedEdges.addAll(edges);
        e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, new ArrayList<>(edges), null, null);

        // Fire the event to indicate the selection change.
        fireValueChanged(e);
    }

    @Override
    public void removeSelectedEdges(Collection<GraphEdge> edges)
    {
        GraphSelectionEvent e;

        selectedEdges.removeAll(edges);
        e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, null, null, new ArrayList<>(edges));

        // Fire the event to indicate the selection change.
        fireValueChanged(e);
    }

    @Override
    public ArrayList<GraphEdge> getSelectedEdges()
    {
        return selectedEdges;
    }

    @Override
    public void setSelectedEdges(Collection<GraphEdge> edges)
    {
        GraphSelectionEvent e;

        e = new GraphSelectionEvent(this, selectedVertices, selectedEdges, null, new ArrayList<>(edges), null, selectedEdges);
        selectedEdges = new ArrayList<>(edges);

        // Fire the event to indicate the selection change.
        fireValueChanged(e);
    }

    @Override
    public void setSelectionMode(int selectionMode)
    {
        this.selectionMode = selectionMode;
    }

    @Override
    public int getSelectionMode()
    {
        return selectionMode;
    }

    @Override
    public boolean isVertexSelected(GraphVertex vertex)
    {
        return selectedVertices.contains(vertex);
    }

    @Override
    public boolean isEdgeSelected(GraphEdge edge)
    {
        return selectedEdges.contains(edge);
    }

    @Override
    public int getSelectedVertexCount()
    {
        return selectedVertices.size();
    }

    @Override
    public int getSelectedEdgeCount()
    {
        return selectedEdges.size();
    }

    public void fireValueChanged(GraphSelectionEvent e)
    {
        for (GraphSelectionListener listener : graphSelectionListeners)
        {
            listener.selectionChanged(e);
        }
    }
}
