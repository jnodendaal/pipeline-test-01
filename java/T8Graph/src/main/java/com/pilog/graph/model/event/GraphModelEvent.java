package com.pilog.graph.model.event;

import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public abstract class GraphModelEvent extends EventObject
{
    public GraphModelEvent(Object source)
    {
        super(source);
    }
}
