package com.pilog.graph.model.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface GraphModelListener extends EventListener
{
    public void graphNodesChanged(GraphModelEvent e);
    public void graphNodesInserted(GraphModelEvent e);
    public void graphNodesRemoved(GraphModelEvent e);
}
