package com.pilog.graph.view;

import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphVertex;
import java.awt.Color;
import java.awt.Component;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class VertexSelectionEdgeRenderer extends DefaultEdgeRenderer
{
    private final Color edgeStartPortSelected = Color.BLUE;
    private final Color edgeEndPortSelected = Color.GREEN;
    private Color edgeRenderColor;

    @Override
    public Component getRendererComponent(Graph graph, GraphEdge edge, boolean isSelected)
    {
        List<GraphVertex> selectedVertices;
        boolean startPortSelected;
        boolean endPortSelected;

        selectedVertices = graph.getSelectedVertices();

        startPortSelected = selectedVertices.contains(edge.getStartPort().getParentVertex());
        endPortSelected = selectedVertices.contains(edge.getEndPort().getParentVertex());

        if(startPortSelected && !endPortSelected) edgeRenderColor = edgeStartPortSelected;
        else if(!startPortSelected && endPortSelected) edgeRenderColor = edgeEndPortSelected;
        else
        {
            edgeRenderColor = (isSelected || isEitherEdgeVertexSelected(graph, edge) ? super.getEdgeSelectedColor() : super.getEdgeDeselectedColor());
        }

        return super.getRendererComponent(graph, edge, isSelected);
    }

    @Override
    protected Color getEdgeSelectedColor()
    {
        return edgeRenderColor.darker();
    }

    @Override
    protected Color getEdgeDeselectedColor()
    {
        return edgeRenderColor;
    }
}
