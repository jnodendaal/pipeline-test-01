package com.pilog.graph.view;

import java.awt.Graphics;

/**
 * @author Bouwer du Preez
 */
public interface GraphBackgroundPainter
{
    public void paintBackground(Graphics g);
}
