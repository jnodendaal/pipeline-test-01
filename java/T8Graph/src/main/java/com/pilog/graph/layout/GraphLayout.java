package com.pilog.graph.layout;

import com.pilog.graph.model.GraphModel;

/**
 * @author Bouwer du Preez
 */
public interface GraphLayout
{
    /**
     * Sets the model on which the layout will act.
     * @param model The graph model that will be altered by this layout algorithm.
     */
    public void setModel(GraphModel model);

    /**
     * Updates the layout's internal state to reflect external changes to the underlying graph model.
     */
    public void update();

    /**
     * Calculates the next step in an animation sequence and applies the layout step to all elements
     * of the underlying graph model.
     * @param timestep A number indicating the size of the step to calculate.  Larger numbers will mean more movement per step.
     * @return A boolean flag to indicate whether subsequent step are available.  While a layout animation is in progress, true is returned
     * and when the algorithm has finished, false is returned.
     */
    public boolean renderNextStep(double timestep);
}
