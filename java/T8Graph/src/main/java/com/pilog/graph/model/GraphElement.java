package com.pilog.graph.model;

/**
 * @author Bouwer du Preez
 */
public interface GraphElement
{
    public String getId();
    public int getZDepth();
}
