package com.pilog.graph.view;

import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphVertex;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface GraphSelectionModel
{
    public static final int SINGLE_EDGE_ONLY_SELECTION = 0;
    public static final int SINGLE_VERTEX_ONLY_SELECTION = 1;
    public static final int MULTIPLE_EDGE_ONLY_SELECTION = 2;
    public static final int MULTIPLE_VERTEX_ONLY_SELECTION = 3;
    public static final int SINGLE_SELECTION = 4;
    public static final int MULTIPLE_SELECTION = 5;

    public void clearSelection();
    public void addPropertyChangeListener(PropertyChangeListener listener);
    public void removePropertyChangeListener(PropertyChangeListener listener);
    public void addGraphSelectionListener(GraphSelectionListener listener);
    public void removeGraphSelectionListener(GraphSelectionListener listener);
    public void addSelectedVertex(GraphVertex vertex);
    public void removeSelectedVertex(GraphVertex vertex);
    public void addSelectedVertices(Collection<GraphVertex> vertices);
    public void removeSelectedVertices(Collection<GraphVertex> vertices);
    public List<GraphVertex> getSelectedVertices();
    public void setSelectedVertices(Collection<GraphVertex> vertices);
    public void addSelectedEdge(GraphEdge edge);
    public void removeSelectedEdge(GraphEdge edge);
    public void addSelectedEdges(Collection<GraphEdge> edges);
    public void removeSelectedEdges(Collection<GraphEdge> edges);
    public List<GraphEdge> getSelectedEdges();
    public void setSelectedEdges(Collection<GraphEdge> edges);
    public void setSelectionMode(int selectionMode);
    public int getSelectionMode();
    public boolean isVertexSelected(GraphVertex vertex);
    public boolean isEdgeSelected(GraphEdge edge);
    public int getSelectedVertexCount();
    public int getSelectedEdgeCount();
}
