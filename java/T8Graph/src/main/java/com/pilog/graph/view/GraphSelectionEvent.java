package com.pilog.graph.view;

import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphVertex;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class GraphSelectionEvent extends EventObject
{
    private final List<GraphVertex> selectedVertices;
    private final List<GraphVertex> addedVertices;
    private final List<GraphVertex> removedVertices;
    private final List<GraphEdge> selectedEdges;
    private final List<GraphEdge> addedEdges;
    private final List<GraphEdge> removedEdges;

    public GraphSelectionEvent(Object source, List<GraphVertex> selectedVertices, List<GraphEdge> selectedEdges, GraphVertex addedVertex, GraphEdge addedEdge, GraphVertex removedVertex, GraphEdge removedEdge)
    {
        super(source);
        this.source = source;
        this.selectedVertices = new ArrayList<>();
        this.addedVertices = new ArrayList<>();
        this.removedVertices = new ArrayList<>();
        this.selectedEdges = new ArrayList<>();
        this.addedEdges = new ArrayList<>();
        this.removedEdges = new ArrayList<>();

        if (selectedVertices != null) selectedVertices.addAll(selectedVertices);
        if (addedVertex != null) addedVertices.add(addedVertex);
        if (removedVertex != null) removedVertices.add(removedVertex);
        if (selectedEdges != null) selectedEdges.addAll(selectedEdges);
        if (addedEdge != null) addedEdges.add(addedEdge);
        if (removedEdge != null) removedEdges.add(removedEdge);
    }

    public GraphSelectionEvent(Object source, List<GraphVertex> selectedVertices, List<GraphEdge> selectedEdges, ArrayList<GraphVertex> addedVertices, ArrayList<GraphEdge> addedEdges, ArrayList<GraphVertex> removedVertices, ArrayList<GraphEdge> removedEdges)
    {
        super(source);
        this.source = source;
        this.selectedVertices = selectedVertices == null ? new ArrayList<>() : new ArrayList<>(selectedVertices);
        this.addedVertices = addedVertices == null ? new ArrayList<>() : addedVertices;
        this.removedVertices = removedVertices == null ? new ArrayList<>() : removedVertices;
        this.selectedEdges = selectedEdges == null ? new ArrayList<>() : new ArrayList<>(selectedEdges);
        this.addedEdges = addedEdges == null ? new ArrayList<>() : addedEdges;
        this.removedEdges = removedEdges == null ? new ArrayList<>() : removedEdges;
    }

    public GraphVertex getAddedVertex()
    {
        if (addedVertices.size() > 0)
        {
            return addedVertices.get(0);
        }
        else
        {
            return null;
        }
    }

    public GraphVertex getRemovedVertex()
    {
        if (removedVertices.size() > 0)
        {
            return removedVertices.get(0);
        }
        else
        {
            return null;
        }
    }

    public GraphEdge getAddedEdge()
    {
        if (addedEdges.size() > 0)
        {
            return addedEdges.get(0);
        }
        else
        {
            return null;
        }
    }

    public GraphEdge getRemovedEdge()
    {
        if (removedEdges.size() > 0)
        {
            return removedEdges.get(0);
        }
        else
        {
            return null;
        }
    }

    public List<GraphVertex> getSelectedVertices()
    {
        return selectedVertices;
    }

    public List<GraphVertex> getAddedVertices()
    {
        return addedVertices;
    }

    public List<GraphVertex> getRemovedVertices()
    {
        return removedVertices;
    }

    public List<GraphEdge> getSelectedEdges()
    {
        return selectedEdges;
    }

    public List<GraphEdge> getAddedEdges()
    {
        return addedEdges;
    }

    public List<GraphEdge> getRemovedEdges()
    {
        return removedEdges;
    }
}
