/**
 * Created on 06 Apr 2016, 10:59:56 AM
 */
package com.pilog.version;

/**
 * @author Gavin Boshoff
 */
public class T8ServerDataUtilityOperationsSharedVersion
{
    public static final String VERSION = "3";
}