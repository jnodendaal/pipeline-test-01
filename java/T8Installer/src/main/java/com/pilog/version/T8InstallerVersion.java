package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8InstallerVersion
{
    public final static String VERSION = "T.8.0.0.16";
}
