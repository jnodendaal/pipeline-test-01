package com.pilog.t8.installer.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * @author Hennie Brink
 */
public class T8MetaDirUtilities
{
    public enum CopyType
    {
        REPLACE,
        MERGE
    }

    public static void copyMetaToPath(File targetDir, CopyType copyType) throws IOException
    {
        if (copyType == CopyType.REPLACE)
        {
            if(targetDir.exists()) deleteFile(targetDir);
        }

        FileUtilities.copyToPath(new File(Paths.get("").toAbsolutePath() + "/META/"), targetDir, copyType == CopyType.MERGE);
    }

    private static void deleteFile(File f) throws IOException
    {
        if (f.isDirectory())
        {
            for (File c : f.listFiles())
            {
                deleteFile(c);
            }
        }
        if (!f.delete())
        {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }
}
