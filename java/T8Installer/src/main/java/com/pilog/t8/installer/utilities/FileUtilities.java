package com.pilog.t8.installer.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * @author Hennie Brink
 */
public class FileUtilities
{
    public static void copyToPath(File source, File target, boolean replace) throws IOException
    {
        Files.walkFileTree(source.toPath(), EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE, new CopyFileVisitor(source.toPath(), target.toPath(), replace));
    }

    public static void replaceFileContents(File inputFile, String regexMatch,
                                           String replacement) throws FileNotFoundException, IOException
    {
        InputStream inputFileStream = null;
        OutputStream outputFileStream = null;
        String fileContents = "";

        try
        {
            inputFileStream = new FileInputStream(inputFile);

            int input;
            while ((input = inputFileStream.read()) != -1)
            {
                fileContents += (char) input;
            }

            fileContents = fileContents.replaceAll(regexMatch, replacement);

            outputFileStream = new FileOutputStream(inputFile);

            outputFileStream.write(fileContents.getBytes());
        }
        finally
        {
            if(inputFileStream != null)inputFileStream.close();
            if(outputFileStream != null)outputFileStream.close();
        }
    }

    public static String getFileContents(File inputFile) throws FileNotFoundException, IOException
    {
        InputStream inputFileStream = null;
        String fileContents = "";

        try
        {
            inputFileStream = new FileInputStream(inputFile);

            int input;
            while ((input = inputFileStream.read()) != -1)
            {
                fileContents += (char) input;
            }

            return fileContents;
        }
        finally
        {
            if(inputFileStream != null)inputFileStream.close();
        }
    }

    private static class CopyFileVisitor extends SimpleFileVisitor<Path>
    {
        private final Path target;
        private final Path source;
        private final boolean replaceExisting;

        public CopyFileVisitor(Path source, Path target, boolean replaceExisting)
        {
            this.target = target;
            this.source = source;
            this.replaceExisting = replaceExisting;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
        {
            Path targetdir = target.resolve(source.relativize(dir));
            try
            {
                if (replaceExisting && !dir.toFile().isDirectory())
                {
                    Files.copy(dir, targetdir, StandardCopyOption.REPLACE_EXISTING);
                }
                else
                {
                    Files.copy(dir, targetdir);
                }
            }
            catch (FileAlreadyExistsException e)
            {
                //Just ignore this if it occurs
            }
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                throws IOException
        {
            if (replaceExisting)
            {
                Files.copy(file, target.resolve(source.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
            }
            else
            {
                Files.copy(file, target.resolve(source.relativize(file)));
            }
            return CONTINUE;
        }

    }

    public static void unzipBasic(InputStream archive, File destDir) throws IOException
    {
        if (!destDir.exists())
        {
            destDir.mkdir();
        }
        try (ZipInputStream zipIn = new ZipInputStream(archive))
        {
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null)
            {
                String filePath = destDir.toString() + File.separator + entry.getName();
                if (!entry.isDirectory())
                {
                    try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath)))
                    {
                        byte[] bytesIn = new byte[4096];
                        int read;
                        while ((read = zipIn.read(bytesIn)) != -1)
                        {
                            bos.write(bytesIn, 0, read);
                        }
                    }
                }
                else
                {
                    File dir = new File(filePath);
                    dir.mkdir();
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        }
        catch(IOException ioex)
        {
            destDir.delete();
            throw ioex;
        }
    }
}
