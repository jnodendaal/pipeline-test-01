/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.installer;

import com.pilog.t8.installer.commons.T8InstallerWizard;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Andre Scheepers
 */
public class T8Installer
{

    public static void main(String args[])
    {
        Logger.getGlobal().setLevel(Level.ALL);
        SwingUtilities.invokeLater(new Runnable()
        {

            @Override
            public void run()
            {
                T8InstallerWizard installerWizard;
                installerWizard = new T8InstallerWizard();

                JFrame installer;
                installer = new JFrame();
                installer.setLayout(new BorderLayout());
                installer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                installer.setLayout(new BorderLayout());
                installer.setTitle("PiLog T8 Installation");
                installer.setBackground(Color.BLUE);
                installer.add(installerWizard);
                installer.setSize(new Dimension(1024, 700));
                installer.setVisible(true);
                installer.setLocationRelativeTo(null);
            }
        });
    }
}
