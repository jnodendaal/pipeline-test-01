/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.installer.commons;

import java.util.Map;

/**
 *
 * @author Andre Scheepers
 */
public interface T8InstallerInterface
{
    public String getIdentifier();
    public String getDefinition();
    //if the implementing class sets the value to "-1" then its the starting Installer.
    public String getPreviousIdentifier();
    //if the implementing class sets the value to "-1" then its the ending Installer.
    public String getNextIdentifier();

    public boolean validateInput(Map<String,Object> wizardParameters);
    public void initializeStep(Map<String,Object> wizardParameters);
}
