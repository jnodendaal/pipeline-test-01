/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.installer.ui;

import com.pilog.t8.installer.commons.InstallerPanel;
import com.pilog.t8.installer.utilities.JBossUtilities;
import com.pilog.t8.installer.utilities.T8MetaDirUtilities;
import com.pilog.t8.installer.utilities.WarDeploymentUtilities;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Andre Scheepers
 */
public class T8InstallerT8Installation extends InstallerPanel
{
    private static final String PREF_META_DIR = "PREF_META_DIR";
    private static final String PREF_WAR_NAME = "PREF_WAR_NAME";

    public static final String P_META_INSTALL_DIR = "P_META_INSTALL_DIR";

    private final String T8_IDENTIEFIER = "T8InstallerT8Installation";
    private final String T8_DEFINITION = "T8 Installation";
    private final String T8_PREVIOUS_IDENTIEFIER = "T8InstallerJBossConfiguration";
    private final String T8_NEXT_IDENTIEFIER = "T8InstallerFinalization";
    private final Preferences preferences;

    private File jBossInstallPath;
    private String serviceIdentifier;

    /**
     * Creates new form T8InstallerMetaInstallation
     */
    public T8InstallerT8Installation()
    {
        this.preferences = Preferences.userNodeForPackage(T8InstallerT8Installation.class);
        initT8InstallerMetaInstallationComponents();
    }

    private void initT8InstallerMetaInstallationComponents()
    {
        initComponents();
        setMetaInstallLocation(preferences.get(PREF_META_DIR, ""));
        setWarName(preferences.get(PREF_WAR_NAME, "TECH8"));
    }

    private void setMetaInstallLocation(String location)
    {
        jTextFieldMetaDir.setText(location);
        preferences.put(PREF_META_DIR, location);
    }

    private String getMetaInstallLocation()
    {
        return jTextFieldMetaDir.getText();
    }

    private void setWarName(String name)
    {
        jTextFieldSiteName.setText(name);
        preferences.put(PREF_WAR_NAME, name);
    }

    private String getWarName()
    {
        return jTextFieldSiteName.getText();
    }

    private void browseDir()
    {
        JFileChooser fileChooser;

        fileChooser = new JFileChooser(new File(getMetaInstallLocation()));
        fileChooser.setDialogTitle("Meta Install Location");
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.showOpenDialog(this);
        if(fileChooser.getSelectedFile() != null)
        {
            setMetaInstallLocation(fileChooser.getSelectedFile().getAbsolutePath());
        }
    }

    private void replaceMeta()
    {
        if(getMetaInstallLocation() != null && !getMetaInstallLocation().isEmpty())
        {
            if(JOptionPane.showConfirmDialog(this, "This option will remove all the files and folders in the selected directory, continue?", "Replace Files", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
            {
                try
                {
                    T8MetaDirUtilities.copyMetaToPath(new File(getMetaInstallLocation()), T8MetaDirUtilities.CopyType.REPLACE);
                    JOptionPane.showMessageDialog(this, "Meta succesfully replaced");
                }
                catch (IOException ex)
                {
                    JOptionPane.showMessageDialog(this, "Failed to replace the meta in the directory " + getMetaInstallLocation());
                    Logger.getLogger(T8InstallerT8Installation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else JOptionPane.showMessageDialog(this, "Meta Install Directory is invalid");
    }

    private void deployWar()
    {
        if(getWarName() != null && !getWarName().isEmpty())
        {
            try
            {
                WarDeploymentUtilities.copyWarToPath(new File(jBossInstallPath.getAbsolutePath() + File.separator + "standalone" + File.separator + "deployments"), getWarName(), getMetaInstallLocation());
                JOptionPane.showMessageDialog(this, "Site successfully deployed");
            }
            catch (IOException ex)
            {
                JOptionPane.showMessageDialog(this, "Failed to deploy site to specified location\n" + ex.getMessage());
                Logger.getLogger(T8InstallerT8Installation.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else JOptionPane.showMessageDialog(this, "Site name cannot be empty");
    }

    private void stopJBossService()
    {
        if(serviceIdentifier != null)
        {
            try
            {
                jTextArea1.append(JBossUtilities.startStopJBossService(serviceIdentifier, false));
            }
            catch (IOException ex)
            {
                jTextArea1.append("Failed to stop the JBoss service");
                jTextArea1.append(ex.getMessage());
                Logger.getLogger(T8InstallerT8Installation.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else JOptionPane.showMessageDialog(this, "Service identifier not set");
    }

    private void startJBossService()
    {
        if(serviceIdentifier != null)
        {
            try
            {
                jTextArea1.append(JBossUtilities.startStopJBossService(serviceIdentifier, true));
            }
            catch (IOException ex)
            {
                jTextArea1.append("Failed to stop the JBoss service");
                jTextArea1.append(ex.getMessage());
                Logger.getLogger(T8InstallerT8Installation.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else JOptionPane.showMessageDialog(this, "Service identifier not set");
    }

    @Override
    public final String getIdentifier()
    {
        return T8_IDENTIEFIER;
    }

    @Override
    public String getPreviousIdentifier()
    {
        return T8_PREVIOUS_IDENTIEFIER;
    }

    @Override
    public String getNextIdentifier()
    {
        return T8_NEXT_IDENTIEFIER;
    }

    @Override
    public String getDefinition()
    {
        return T8_DEFINITION;
    }

    @Override
    public void initializeStep(Map<String, Object> wizardParameters)
    {
        jBossInstallPath = (File) wizardParameters.get(T8InstallerJBossInstallation.P_JBOSS_INSTALLATION_DIR);
        serviceIdentifier = (String) wizardParameters.get(T8InstallerJBossInstallation.P_JBOSS_SERVICE_IDENTIFIER);
    }

    @Override
    public boolean validateInput(Map<String, Object> wizardParameters)
    {
        File metaInstallDir;

        metaInstallDir = new File(getMetaInstallLocation());
        if(metaInstallDir.exists())
        {
            wizardParameters.put(P_META_INSTALL_DIR, metaInstallDir);
            return true;
        }
        else
        {
            wizardParameters.remove(P_META_INSTALL_DIR);
            return false;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldMetaDir = new javax.swing.JTextField();
        jButtonBrowseDir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jButtonReplaceMeta = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldSiteName = new javax.swing.JTextField();
        jButtonDeployWar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jButtonStopService = new javax.swing.JButton();
        jButtonStartService = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        jLabel1.setText("T8 Installation");
        jLabel1.setToolTipText("");
        add(jLabel1, java.awt.BorderLayout.NORTH);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Meta"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Select T8 Meta Directory");
        jLabel2.setToolTipText("The folder in which the existing meta resides, or into which the meta should be placed.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabel2, gridBagConstraints);

        jTextFieldMetaDir.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jTextFieldMetaDir, gridBagConstraints);

        jButtonBrowseDir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/t8installer/magnifier-zoom.png"))); // NOI18N
        jButtonBrowseDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBrowseDirActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonBrowseDir, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jButtonReplaceMeta.setText("Insert / Replace Meta");
        jButtonReplaceMeta.setToolTipText("<html><body><p>This option will insert the new Meta, overriding any <br/> existing files and/or folders in the specified directory.</p></body></html>");
        jButtonReplaceMeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReplaceMetaActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weighty = 0.1;
        jPanel2.add(jButtonReplaceMeta, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("War"));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Enter Site Name");
        jLabel3.setToolTipText("The name of the Tech8 Client/Developer Application.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jLabel3, gridBagConstraints);

        jTextFieldSiteName.setToolTipText("");
        jTextFieldSiteName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldSiteNameFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jTextFieldSiteName, gridBagConstraints);

        jButtonDeployWar.setText("Deploy!");
        jButtonDeployWar.setToolTipText("Update the Application file with the Meta Directory information and put it in the JBoss deployments folder.");
        jButtonDeployWar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeployWarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonDeployWar, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanel1.add(jPanel4, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("JBoss Service"));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jButtonStopService.setText("Stop JBoss Service");
        jButtonStopService.setToolTipText("Attempt to stop the service specified in the JBoss Installation step.");
        jButtonStopService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStopServiceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonStopService, gridBagConstraints);

        jButtonStartService.setText("Start JBoss Service");
        jButtonStartService.setToolTipText("Attempt to start the service specified in the JBoss Installation step.");
        jButtonStartService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartServiceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonStartService, gridBagConstraints);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanel1.add(jPanel5, gridBagConstraints);

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonBrowseDirActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonBrowseDirActionPerformed
    {//GEN-HEADEREND:event_jButtonBrowseDirActionPerformed
        browseDir();
    }//GEN-LAST:event_jButtonBrowseDirActionPerformed

    private void jButtonReplaceMetaActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonReplaceMetaActionPerformed
    {//GEN-HEADEREND:event_jButtonReplaceMetaActionPerformed
        replaceMeta();
    }//GEN-LAST:event_jButtonReplaceMetaActionPerformed

    private void jButtonDeployWarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeployWarActionPerformed
    {//GEN-HEADEREND:event_jButtonDeployWarActionPerformed
        deployWar();
    }//GEN-LAST:event_jButtonDeployWarActionPerformed

    private void jTextFieldSiteNameFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jTextFieldSiteNameFocusLost
    {//GEN-HEADEREND:event_jTextFieldSiteNameFocusLost
        setWarName(jTextFieldSiteName.getText());
    }//GEN-LAST:event_jTextFieldSiteNameFocusLost

    private void jButtonStopServiceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStopServiceActionPerformed
    {//GEN-HEADEREND:event_jButtonStopServiceActionPerformed
        stopJBossService();
    }//GEN-LAST:event_jButtonStopServiceActionPerformed

    private void jButtonStartServiceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStartServiceActionPerformed
    {//GEN-HEADEREND:event_jButtonStartServiceActionPerformed
        startJBossService();
    }//GEN-LAST:event_jButtonStartServiceActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBrowseDir;
    private javax.swing.JButton jButtonDeployWar;
    private javax.swing.JButton jButtonReplaceMeta;
    private javax.swing.JButton jButtonStartService;
    private javax.swing.JButton jButtonStopService;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldMetaDir;
    private javax.swing.JTextField jTextFieldSiteName;
    // End of variables declaration//GEN-END:variables

}
