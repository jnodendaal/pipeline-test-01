package com.pilog.t8.installer.commons;

import com.pilog.t8.installer.ui.T8InstallerFinalization;
import com.pilog.t8.installer.ui.T8InstallerJBossConfiguration;
import com.pilog.t8.installer.ui.T8InstallerJBossInstallation;
import com.pilog.t8.installer.ui.T8InstallerT8Installation;
import com.pilog.t8.installer.ui.T8InstallerWelcome;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class InstallerStepsFactory
{
    private final List<T8InstallerInterface> installerSteps;

    public InstallerStepsFactory()
    {
        this.installerSteps = new ArrayList<T8InstallerInterface>();
        installerSteps.add(new T8InstallerWelcome());
        installerSteps.add(new T8InstallerJBossInstallation());
        installerSteps.add(new T8InstallerJBossConfiguration());
        installerSteps.add(new T8InstallerT8Installation());
        installerSteps.add(new T8InstallerFinalization());
    }

    public List<T8InstallerInterface> getInstallerSteps()
    {
        return Collections.unmodifiableList(installerSteps);
    }

    public T8InstallerInterface getInstallerStep(String stepIdentifier)
    {
        for (T8InstallerInterface t8InstallerInterface : installerSteps)
        {
            if(t8InstallerInterface.getIdentifier().equals(stepIdentifier)) return t8InstallerInterface;
        }
        throw new RuntimeException("Step not found " + stepIdentifier);
    }
}
