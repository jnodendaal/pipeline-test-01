package com.pilog.t8.installer.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Hennie Brink, Jaco Odendaal
 */
public class JBossUtilities
{
//    private static final String jbossArchive = File.pathSeparator + "com" + File.pathSeparator + "pilog" + File.pathSeparator + "t8" + File.pathSeparator + "t8installer" + File.separator + "jboss-as.zip";
    private static final String jbossArchive = "/com/pilog/t8/t8installer/jboss-as.zip";
//    private static final String configBat = File.pathSeparator + "bin" + File.separator + "standalone.conf.bat";
    private static final String configBat = "/bin/standalone.conf.bat";
//    private static final String configXML = File.pathSeparator + "standalone" + File.pathSeparator + "configuration" + File.separator + "standalone.xml";
    private static final String configXML = "/standalone/configuration/standalone.xml";
//    private static final String serviceDir = File.pathSeparator + "bin" + File.pathSeparator + "service";
    private static final String serviceDir = "/bin/service";
//    private static final String serviceBat = File.pathSeparator + "bin" + File.pathSeparator + "service" + File.separator + "service.bat";
    private static final String serviceBat = "/bin/service/service.bat";
    
    public static enum MemoryUnit
    {
        KILOBYTE("KB", "K"),
        MEGABYTE("MB", "M"),
        GIGABYTE("GB", "G");

        private final String abbreviation;
        private final String javaAbbreviation;

        private MemoryUnit(String abbreviation, String javaAbbreviation)
        {
            this.abbreviation = abbreviation;
            this.javaAbbreviation = javaAbbreviation;
        }

        public String getAbbreviation()
        {
            return abbreviation;
        }

        public String getJavaAbbreviation()
        {
            return javaAbbreviation;
        }
    }
    
    /**
     * Returns the integer value in kilobytes from the string provided
     * @param value Any integer value with the postfix K/KB/M/MB/G/GB
     * @return The integer representation in kilobytes, zero if the provided value is invalid
     */
    public static int parseMemoryValue(String value)
    {
        if (value != null)
        {
            if (value.matches("\\b[0-9]+[K|k][B|b]?\\b"))
            {
                //KILOBYTES
                return Integer.parseInt(value.toUpperCase().replace("K", "").replace("B", ""));
            }
            else if (value.matches("\\b[0-9]+[M|m][B|b]?\\b"))
            {
                //MEGABYTES
                return Integer.parseInt(value.toUpperCase().replace("M", "").replace("B", "")) * 1000;
            }
            else if (value.matches("\\b[0-9]+[G|g][B|b]?\\b"))
            {
                //GIGABYTES
                return Integer.parseInt(value.toUpperCase().replace("G", "").replace("B", "")) * 1000000;
            }
        }
        return 0;
    }

    public static boolean checkJBossExists(File directory)
    {
        if(directory.exists() && directory.isDirectory())
        {
            List<String> files = Arrays.asList(directory.list());

            return files.contains("bin") && files.contains("modules") && files.contains("standalone");
        }
        return false;
    }

    public static void copyJBossToPath(File targetDir) throws IOException
    {
        BufferedInputStream archive = new BufferedInputStream(JBossUtilities.class.getResourceAsStream(jbossArchive));
        FileUtilities.unzipBasic(archive, targetDir);
    }

    public static String getJBossMaxMemory(Path pathToJBoss) throws IOException
    {
        String fileContents = FileUtilities.getFileContents(new File(pathToJBoss.toString() + configBat));
        Matcher matcher = Pattern.compile("-Xmx[0-9A-z]*").matcher(fileContents);

        if(matcher.find() && !matcher.hitEnd())
        {
            return matcher.group().replaceAll("-Xmx", "");
        }
        return "";
    }

    public static String getJBossMinMemory(Path pathToJBoss) throws IOException
    {
        String fileContents = FileUtilities.getFileContents(new File(pathToJBoss.toString() + configBat));
        Matcher matcher = Pattern.compile("-Xms[0-9A-z]*").matcher(fileContents);

        if(matcher.find() && !matcher.hitEnd())
        {
            return matcher.group().replaceAll("-Xms", "");
        }
        return "";
    }

//    public static String getJBossPermGenMMemory(Path pathToJBoss) throws IOException
//    {
//        String fileContents;
//        Matcher matcher;
//
//        fileContents = FileUtilities.getFileContents(new File(pathToJBoss.toString() + "/bin/standalone.conf.bat"));
//        matcher = Pattern.compile("-XX:MaxPermSize=[0-9A-z]*").matcher(fileContents);
//
//        if(matcher.find() && !matcher.hitEnd())
//        {
//            return matcher.group().replaceAll("-XX:MaxPermSize=", "");
//        }
//        return "";
//    }

    public static String getJBossHttpPort(Path pathToJBoss) throws IOException
    {
        String fileContents = FileUtilities.getFileContents(new File(pathToJBoss.toString() + configXML));
        //old pattern: "<socket-binding name=\"http\" port=\"[0-9]*\"/>"
        //new pattern: "jboss.http.port:[0-9]*"
        Matcher matcher = Pattern.compile("jboss.http.port:[0-9]*").matcher(fileContents);

        if(matcher.find() && !matcher.hitEnd())
        {
            return matcher.group().replaceAll("jboss.http.port:", "");
        }
        return "";
    }

    public static String getJBossHttpsPort(Path pathToJBoss) throws IOException
    {
        String fileContents = FileUtilities.getFileContents(new File(pathToJBoss.toString() + configXML));
        //old pattern: "<socket-binding name=\"https\" port=\"[0-9]*\"/>"
        //new pattern: "jboss.https.port:[0-9]*"
        Matcher matcher = Pattern.compile("jboss.https.port:[0-9]*").matcher(fileContents);

        if(matcher.find() && !matcher.hitEnd())
        {
            return matcher.group().replaceAll("jboss.https.port:", "");
        }
        return "";
    }

    public static void setJBossMinMemory(Path pathToJBoss, String memory) throws IOException
    {
        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + configBat), "-Xms[0-9A-z]*", "-Xms" + memory);
    }
    
    public static void setJBossMaxMemory(Path pathToJBoss, String memory) throws IOException
    {
        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + configBat), "-Xmx[0-9A-z]*", "-Xmx" + memory);
    }
    
//    public static void setJBossPermGenMemory(Path pathToJBoss, String memory) throws IOException
//    {
//        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + "/bin/standalone.conf.bat"), "-XX:MaxPermSize=[0-9A-z]*", "-XX:MaxPermSize=" + memory);
//    }

    public static void setJBossHttpPort(Path pathToJBoss, String port) throws IOException
    {
//        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + configXML),
//                "<socket-binding name=\"http\" port=\"[0-9]*\"/>", "<socket-binding name=\"http\" port=\"" + port + "\"/>");
        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + configXML), "jboss.http.port:[0-9]*", "jboss.http.port:" + port);
    }

    public static void setJBossHttpsPort(Path pathToJBoss, String port) throws IOException
    {
//        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + configXML),
//                "<socket-binding name=\"https\" port=\"[0-9]*\"/>", "<socket-binding name=\"https\" port=\"" + port + "\"/>");
        FileUtilities.replaceFileContents(new File(pathToJBoss.toString() + configXML), "jboss.https.port:[0-9]*", "jboss.https.port:" + port);
    }

    public static String installJBossService(Path pathToJBoss, String serviceName, String displayName, String description) throws IOException
    {
        File serviceBatFile = new File(pathToJBoss.toString() + serviceBat);
        
        FileUtilities.replaceFileContents(serviceBatFile, "SHORTNAME=.*", "SHORTNAME=" + serviceName);
        FileUtilities.replaceFileContents(serviceBatFile, "DISPLAYNAME=.*", "DISPLAYNAME=\"" + displayName + "\"");
        FileUtilities.replaceFileContents(serviceBatFile, "DESCRIPTION=.*", "DESCRIPTION=\"" + description + "\"");

        ProcessBuilder pb = new ProcessBuilder(serviceBatFile.getAbsolutePath(), "install").directory(
                new File(pathToJBoss.toString() + serviceDir));

        pb.redirectErrorStream(true);
        Process process = pb.start();
        BufferedReader inStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String newLine;
        String result = "";
        while ((newLine = inStreamReader.readLine()) != null)
        {
            result += "\n" + newLine;
        }

        return result;
    }

//    public static String uninstallJBossService(Path pathToJBoss, String serviceName) throws IOException
//    {
//        File serviceBatFile = new File(pathToJBoss.toString() + serviceBat);
//
//        ProcessBuilder pb = new ProcessBuilder(serviceBatFile.getAbsolutePath(),"uninstall").directory(
//                new File(pathToJBoss.toString() + serviceDir));
//
//        pb.redirectErrorStream(true);
//        Process process = pb.start();
//        BufferedReader inStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//
//        String newLine;
//        String result = "";
//        while ((newLine = inStreamReader.readLine()) != null)
//        {
//            result += "\n" + newLine;
//        }
//
//        return result;
//    }

    public static String startStopJBossService(String serviceName, boolean start) throws IOException
    {
        ProcessBuilder pb = new ProcessBuilder("net",(start ? "start" : "stop"), serviceName);
        pb.redirectErrorStream(true);
        Process process = pb.start();
        BufferedReader inStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String newLine;
        String result = "";
        while ((newLine = inStreamReader.readLine()) != null)
        {
            result += "\n" + newLine;
        }

        return result;
    }
}
