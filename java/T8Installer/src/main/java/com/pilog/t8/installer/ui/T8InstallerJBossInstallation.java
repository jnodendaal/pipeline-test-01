/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.installer.ui;

import com.pilog.t8.installer.commons.InstallerPanel;
import com.pilog.t8.installer.utilities.JBossUtilities;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Andre Scheepers
 */
public class T8InstallerJBossInstallation extends InstallerPanel
{
    private static final String PREF_INSTALL_DIR = "PREF_INSTALL_DIR";
    private static final String PREF_SERVICE_IDENTIFIER = "PREF_SERVICE_IDENTIFIER";
    private static final String PREF_SERVICE_NAME = "PREF_SERVICE_NAME";
    private static final String PREF_SERVIC_DESC = "PREF_SERVIC_DESC";

    public static final String P_JBOSS_INSTALLATION_DIR = "P_JBOSS_INSTALLATION_DIR";
    public static final String P_JBOSS_SERVICE_IDENTIFIER = "P_JBOSS_SERVICE_IDENTIFIER";

    private final String T8_IDENTIEFIER = "T8InstallerJBossInstallation";
    private final String T8_DEFINITION = "JBoss Installation";
    private final String T8_PREVIOUS_IDENTIEFIER = "T8InstallerWelcome";
    private final String T8_NEXT_IDENTIEFIER = "T8InstallerJBossConfiguration";
    private final Preferences preferences;

    /**
     * Creates new form T8InstallerJBossInstallation
     */
    public T8InstallerJBossInstallation()
    {
        this.preferences = Preferences.userNodeForPackage(T8InstallerJBossInstallation.class);

        initT8InstallerJBossInstallationComponents();
    }

    private void initT8InstallerJBossInstallationComponents()
    {
        initComponents();
        
        System.out.println(preferences.get(PREF_SERVICE_IDENTIFIER, "WILDFLY"));
        
        setInstallLocation(preferences.get(PREF_INSTALL_DIR, ""));
        setServiceIdentifier(preferences.get(PREF_SERVICE_IDENTIFIER, "WILDFLY"));
        setServiceName(preferences.get(PREF_SERVICE_NAME, "Wildfly"));
        setServiceDescription(preferences.get(PREF_SERVIC_DESC, "The Wildfly Application Server"));
    }

    private String getInstallLocation()
    {
        return jTextFieldInstallLocation.getText();
    }

    private void setInstallLocation(String location)
    {
        jTextFieldInstallLocation.setText(location);
        preferences.put(PREF_INSTALL_DIR, location);

        jButtonInstallJBoss.setEnabled(!JBossUtilities.checkJBossExists(new File(location)));
        jButtonInstallService.setEnabled(JBossUtilities.checkJBossExists(new File(location)));
    }

    private void setServiceIdentifier(String identifier)
    {
        jTextFieldServiceIdentifier.setText(identifier);
        preferences.put(PREF_SERVICE_IDENTIFIER, identifier);
    }

    private void setServiceName(String newLocation)
    {
        jTextFieldServiceName.setText(newLocation);
        preferences.put(PREF_SERVICE_NAME, newLocation);
    }

    private void setServiceDescription(String newLocation)
    {
        jTextFieldServiceDescription.setText(newLocation);
        preferences.put(PREF_SERVIC_DESC, newLocation);
    }

    private void browseDir()
    {
        JFileChooser fileChooser;

        fileChooser = new JFileChooser(new File(getInstallLocation()));
        fileChooser.setDialogTitle("JBoss Install Location");
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);
        int fileChooserAction = fileChooser.showOpenDialog(this);
        if(fileChooser.getSelectedFile() != null && fileChooserAction == JFileChooser.APPROVE_OPTION)
        {
            setInstallLocation(fileChooser.getSelectedFile().getAbsolutePath());
        }
    }

    private void installJBoss()
    {
        String installLocation = getInstallLocation();
        File installLocationFile;

        if(installLocation != null && !installLocation.isEmpty())
        {
            installLocationFile = new File(installLocation);

            if(installLocationFile.canRead() && installLocationFile.canWrite() && installLocationFile.canExecute())
            {
                try
                {
                    JBossUtilities.copyJBossToPath(installLocationFile);

                    //Revalidate the install location
                    for (String fileName : installLocationFile.list())
                    {
                        if(fileName.toUpperCase().contains("JBOSS"))
                        {
                            jTextAreaLog.append("JBoss installed to " + installLocation + File.separator + fileName);
                            setInstallLocation(installLocation + File.separator + fileName);
                        }
                    }
                }
                catch (IOException ex)
                {
                    JOptionPane.showMessageDialog(this, "Failed to install JBoss to the specified location\n" + ex.getMessage());
                    Logger.getLogger(T8InstallerJBossInstallation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else JOptionPane.showMessageDialog(this, "Invalid permission for selected path, ensure the current user has the required permission to modify the folder.");
        }
        else
        {
            JOptionPane.showMessageDialog(this, "No install path provided.");
        }
    }

    private void installService()
    {
        String serviceIdentifier = jTextFieldServiceIdentifier.getText();
        String serviceName = jTextFieldServiceName.getText();
        String serviceDescription = jTextFieldServiceDescription.getText();

        if(serviceIdentifier == null || serviceIdentifier.isEmpty())
        {
            JOptionPane.showMessageDialog(this, "Invalid service identifier");
        }
        else
        {
            if(serviceName == null || serviceName.isEmpty())
            {
                JOptionPane.showMessageDialog(this, "Invalid service name");
            }
            else
            {
                try
                {
                    String installJBossService = JBossUtilities.installJBossService(new File(getInstallLocation()).toPath(), serviceIdentifier, serviceName, serviceDescription);
                    jTextAreaLog.append(installJBossService);
                }
                catch (IOException ex)
                {
                    JOptionPane.showMessageDialog(this, "Failed to install the JBoss service\n" + ex.getMessage());
                    Logger.getLogger(T8InstallerJBossInstallation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public boolean validateInput(Map<String, Object> wizardParameters)
    {
        File installLocation;

        installLocation = new File(getInstallLocation());
        if(JBossUtilities.checkJBossExists(installLocation))
        {
            wizardParameters.put(P_JBOSS_INSTALLATION_DIR, installLocation);
            wizardParameters.put(P_JBOSS_SERVICE_IDENTIFIER, jTextFieldServiceIdentifier.getText());
            return true;
        }
        else
        {
            wizardParameters.remove(P_JBOSS_INSTALLATION_DIR);
            wizardParameters.remove(P_JBOSS_SERVICE_IDENTIFIER);
            JOptionPane.showMessageDialog(this, "Please provide a valid JBoss installation path to continue.");
            return false;
        }
    }

    @Override
    public final String getIdentifier()
    {
        return T8_IDENTIEFIER;
    }

    @Override
    public String getPreviousIdentifier()
    {
        return T8_PREVIOUS_IDENTIEFIER;
    }

    @Override
    public String getNextIdentifier()
    {
        return T8_NEXT_IDENTIEFIER;
    }

    @Override
    public String getDefinition()
    {
        return T8_DEFINITION;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldInstallLocation = new javax.swing.JTextField();
        jButtonBrowseDir = new javax.swing.JButton();
        jPanelControls = new javax.swing.JPanel();
        jButtonInstallJBoss = new javax.swing.JButton();
        jButtonInstallService = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaLog = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldServiceIdentifier = new javax.swing.JTextField();
        jTextFieldServiceName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldServiceDescription = new javax.swing.JTextField();

        jLabel4.setText("jLabel4");

        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        jLabel1.setText("JBoss Installation");
        add(jLabel1, java.awt.BorderLayout.NORTH);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Select the JBoss installation directory ( <Drive:/Folders/>jboss-as/ )");
        jLabel2.setToolTipText("The folder in which JBoss is installed or should be installed into.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel2, gridBagConstraints);

        jTextFieldInstallLocation.setEditable(false);
        jTextFieldInstallLocation.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jTextFieldInstallLocation, gridBagConstraints);

        jButtonBrowseDir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/t8installer/magnifier-zoom.png"))); // NOI18N
        jButtonBrowseDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBrowseDirActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jButtonBrowseDir, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.GridBagLayout());

        jButtonInstallJBoss.setText("Install JBoss");
        jButtonInstallJBoss.setToolTipText("Install JBoss to the specified installation directory.");
        jButtonInstallJBoss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInstallJBossActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelControls.add(jButtonInstallJBoss, gridBagConstraints);

        jButtonInstallService.setText("Install Windows Service");
        jButtonInstallService.setToolTipText("Create or Register a service with the information provided.");
        jButtonInstallService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInstallServiceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelControls.add(jButtonInstallService, gridBagConstraints);

        jTextAreaLog.setEditable(false);
        jTextAreaLog.setColumns(20);
        jTextAreaLog.setRows(5);
        jScrollPane1.setViewportView(jTextAreaLog);

        jScrollPane2.setViewportView(jScrollPane1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelControls.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jPanelControls, gridBagConstraints);

        jLabel3.setText("Service Name:");
        jLabel3.setToolTipText("<html><body><p>A handle or name that Windows uses to uniquely<br/>identify this service. Rules: ALL CAPS and no spaces.</p></body></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel3, gridBagConstraints);

        jTextFieldServiceIdentifier.setText("WILDFLY");
        jTextFieldServiceIdentifier.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldServiceIdentifierFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jTextFieldServiceIdentifier, gridBagConstraints);

        jTextFieldServiceName.setText("WildFly");
        jTextFieldServiceName.setToolTipText("");
        jTextFieldServiceName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldServiceNameFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jTextFieldServiceName, gridBagConstraints);

        jLabel5.setText("Service Display Name:");
        jLabel5.setToolTipText("<html><body><p>A descriptive name that will be displayed in the<br/>Services Console. It can be the same as the 'Service Name'</p></body></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel5, gridBagConstraints);

        jLabel6.setText("Service Description:");
        jLabel6.setToolTipText("<html><body><p>A brief description of what this service entails.<br/>This is useful when there are more than one JBoss service.</p></body></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel6, gridBagConstraints);

        jTextFieldServiceDescription.setText("The Wildfly Application Server");
        jTextFieldServiceDescription.setToolTipText("");
        jTextFieldServiceDescription.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldServiceDescriptionFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jTextFieldServiceDescription, gridBagConstraints);

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonBrowseDirActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonBrowseDirActionPerformed
    {//GEN-HEADEREND:event_jButtonBrowseDirActionPerformed
        browseDir();
    }//GEN-LAST:event_jButtonBrowseDirActionPerformed

    private void jButtonInstallJBossActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInstallJBossActionPerformed
    {//GEN-HEADEREND:event_jButtonInstallJBossActionPerformed
        installJBoss();
    }//GEN-LAST:event_jButtonInstallJBossActionPerformed

    private void jButtonInstallServiceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInstallServiceActionPerformed
    {//GEN-HEADEREND:event_jButtonInstallServiceActionPerformed
        installService();
    }//GEN-LAST:event_jButtonInstallServiceActionPerformed

    private void jTextFieldServiceIdentifierFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jTextFieldServiceIdentifierFocusLost
    {//GEN-HEADEREND:event_jTextFieldServiceIdentifierFocusLost
        preferences.put(PREF_SERVICE_IDENTIFIER, jTextFieldServiceIdentifier.getText());
    }//GEN-LAST:event_jTextFieldServiceIdentifierFocusLost

    private void jTextFieldServiceNameFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jTextFieldServiceNameFocusLost
    {//GEN-HEADEREND:event_jTextFieldServiceNameFocusLost
         preferences.put(PREF_SERVICE_NAME, jTextFieldServiceName.getText());
    }//GEN-LAST:event_jTextFieldServiceNameFocusLost

    private void jTextFieldServiceDescriptionFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jTextFieldServiceDescriptionFocusLost
    {//GEN-HEADEREND:event_jTextFieldServiceDescriptionFocusLost
         preferences.put(PREF_SERVIC_DESC, jTextFieldServiceDescription.getText());
    }//GEN-LAST:event_jTextFieldServiceDescriptionFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBrowseDir;
    private javax.swing.JButton jButtonInstallJBoss;
    private javax.swing.JButton jButtonInstallService;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextAreaLog;
    private javax.swing.JTextField jTextFieldInstallLocation;
    private javax.swing.JTextField jTextFieldServiceDescription;
    private javax.swing.JTextField jTextFieldServiceIdentifier;
    private javax.swing.JTextField jTextFieldServiceName;
    // End of variables declaration//GEN-END:variables

}
