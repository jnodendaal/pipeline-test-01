package com.pilog.t8.installer.commons;

import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Hennie Brink
 */
public abstract class InstallerPanel extends JPanel implements T8InstallerInterface
{
    @Override
    public boolean validateInput(Map<String,Object> wizardParameters)
    {
        return true;
    }

    @Override
    public void initializeStep(Map<String, Object> wizardParameters)
    {
    }
}
