package com.pilog.t8.installer.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author Hennie Brink
 */
public class WarDeploymentUtilities
{
    public static void copyWarToPath(File jbossDeploymentPath, String warName, String newMetaDir) throws IOException
    {
        InputStream originalWarFile = WarDeploymentUtilities.class.getResourceAsStream("/com/pilog/t8/t8installer/TECH8.war");
        createNewWar(originalWarFile, new File(jbossDeploymentPath + "/" + warName + ".war"), newMetaDir);
    }

    private static void createNewWar(InputStream installerWarData, File warFile,
                                     String newMetaDir) throws FileNotFoundException, IOException
    {
        //Remove the existing war file
        warFile.delete();

        warFile.createNewFile();

        ZipInputStream oldWarInputStream;
        ZipOutputStream out;
        String originalWebXml = "";

        oldWarInputStream = new ZipInputStream(installerWarData);
        out = new ZipOutputStream(new FileOutputStream(warFile));

        ZipEntry entry;

        byte[] buf = new byte[4096 * 1024];
        try
        {
            entry = oldWarInputStream.getNextEntry();
            while (entry != null)
            {
                String name = entry.getName();
                if (!name.equals("WEB-INF/web.xml"))
                {
                    // Add ZIP entry to output stream.
                    out.putNextEntry(new ZipEntry(name));
                    // Transfer bytes from the ZIP file to the output file
                    int len;
                    while ((len = oldWarInputStream.read(buf)) > 0)
                    {
                        out.write(buf, 0, len);
                    }
                }
                else
                {
                    int len;
                    while ((len = oldWarInputStream.read()) > 0)
                    {
                        originalWebXml += (char) len;
                    }
                }
                entry = oldWarInputStream.getNextEntry();
            }

            //Now put the new web xml entry into the zip, (matching both back- and forward slashes)
            originalWebXml = originalWebXml.replaceFirst(">[A-z]{1}:[\\w\\s\\\\/]+<", ">" + Matcher.quoteReplacement(newMetaDir) + "<");

            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry("WEB-INF/web.xml"));

            // Write the file contents to the file
            out.write(originalWebXml.getBytes());
        }
        catch (IOException ex)
        {
            Logger.getLogger(WarDeploymentUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            oldWarInputStream.close();
            out.close();
        }
    }
}
