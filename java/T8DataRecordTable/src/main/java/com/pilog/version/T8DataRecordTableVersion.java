package com.pilog.version;

/**
 * @author  Bouwer du Preez
 */
public class T8DataRecordTableVersion
{
    public final static String VERSION = "144";
}
