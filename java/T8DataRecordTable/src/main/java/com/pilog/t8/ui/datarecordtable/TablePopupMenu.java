package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

/**
 * @author Bouwer du Preez
 */
public class TablePopupMenu extends JPopupMenu
{
    public TablePopupMenu(final DataRecordTable recordTable, final Component target)
    {
        JMenuItem menuItem;
        
        // Create the ActionListener for the menu items.
        ActionListener menuListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                JMenuItem menuItem;
                String code;

                menuItem = (JMenuItem)event.getSource();
                code = menuItem.getName();
                if (code.equals("FILL_DOWN"))
                {
                    int rowIndex;
                    int columnIndex;
                    ViewHandler viewHandler;
                    ModelHandler modelHandler;
                            
                    viewHandler = recordTable.getTableViewHandler();
                    modelHandler = recordTable.getTableModelHandler();
                    
                    rowIndex = viewHandler.getFocusedRowIndex();
                    columnIndex = viewHandler.getFocusedColumnIndex();
                    if ((rowIndex > -1) && (columnIndex > -1))
                    {
                        columnIndex = viewHandler.convertViewToModelIndex(columnIndex);
                        modelHandler.fillDownProperty(rowIndex, modelHandler.getPropertyRequirement(columnIndex).getConceptID());
                    }
                    else JOptionPane.showMessageDialog(target, recordTable.translate("No Cell Selected."), recordTable.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                }
                else if (code.equals("PASTE_COPY"))
                {
                    int rowIndex;
                    int columnIndex;
                    ViewHandler viewHandler;
                    ModelHandler modelHandler;
                            
                    viewHandler = recordTable.getTableViewHandler();
                    modelHandler = recordTable.getTableModelHandler();
                    
                    rowIndex = viewHandler.getFocusedRowIndex();
                    columnIndex = viewHandler.getFocusedColumnIndex();
                    if ((rowIndex > -1) && (columnIndex > -1))
                    {
                        RecordProperty property;
                        PropertyRequirement propertyRequirement;
                        
                        property = CopyAndPasteHandler.getCopiedProperty();
                        columnIndex = viewHandler.convertViewToModelIndex(columnIndex);
                        propertyRequirement = modelHandler.getPropertyRequirement(columnIndex);
                        modelHandler.setRecordProperty(rowIndex, property.copy(propertyRequirement));
                    }
                    else JOptionPane.showMessageDialog(target, recordTable.translate("No Cell Selected."), recordTable.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                }
                else if (code.equals("PASTE_CUT"))
                {
                    int rowIndex;
                    int columnIndex;
                    ViewHandler viewHandler;
                    ModelHandler modelHandler;
                            
                    viewHandler = recordTable.getTableViewHandler();
                    modelHandler = recordTable.getTableModelHandler();
                    
                    rowIndex = viewHandler.getFocusedRowIndex();
                    columnIndex = viewHandler.getFocusedColumnIndex();
                    if ((rowIndex > -1) && (columnIndex > -1))
                    {
                        RecordProperty property;
                        PropertyRequirement propertyRequirement;
                        
                        property = CopyAndPasteHandler.getCutProperty();
                        columnIndex = viewHandler.convertViewToModelIndex(columnIndex);
                        propertyRequirement = modelHandler.getPropertyRequirement(columnIndex);
                        modelHandler.setRecordProperty(rowIndex, property.copy(propertyRequirement));
                    }
                    else JOptionPane.showMessageDialog(target, recordTable.translate("No Cell Selected."), recordTable.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                }
                else if (code.equals("COPY"))
                {
                    int rowIndex;
                    int columnIndex;
                    ViewHandler viewHandler;
                    ModelHandler modelHandler;
                            
                    viewHandler = recordTable.getTableViewHandler();
                    modelHandler = recordTable.getTableModelHandler();
                    
                    rowIndex = viewHandler.getFocusedRowIndex();
                    columnIndex = viewHandler.getFocusedColumnIndex();
                    if ((rowIndex > -1) && (columnIndex > -1))
                    {
                        columnIndex = viewHandler.convertViewToModelIndex(columnIndex);
                        CopyAndPasteHandler.copyProperty(modelHandler.getRecordProperty(rowIndex, columnIndex));
                    }
                    else JOptionPane.showMessageDialog(target, recordTable.translate("No Cell Selected."), recordTable.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                }
                else if (code.equals("CUT"))
                {
                    int rowIndex;
                    int columnIndex;
                    ViewHandler viewHandler;
                    ModelHandler modelHandler;
                            
                    viewHandler = recordTable.getTableViewHandler();
                    modelHandler = recordTable.getTableModelHandler();
                    
                    rowIndex = viewHandler.getFocusedRowIndex();
                    columnIndex = viewHandler.getFocusedColumnIndex();
                    if ((rowIndex > -1) && (columnIndex > -1))
                    {
                        columnIndex = viewHandler.convertViewToModelIndex(columnIndex);
                        CopyAndPasteHandler.cutProperty(modelHandler.getRecordProperty(rowIndex, columnIndex));
                    }
                    else JOptionPane.showMessageDialog(target, recordTable.translate("No Cell Selected."), recordTable.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
        };
        
        menuItem = new JMenuItem(recordTable.translate("Fill Down"));
        menuItem.setName("FILL_DOWN");
        menuItem.addActionListener(menuListener);
        add(menuItem);
        
        menuItem = new JMenuItem(recordTable.translate("Copy"));
        menuItem.setName("COPY");
        menuItem.addActionListener(menuListener);
        add(menuItem);
        
        menuItem = new JMenuItem(recordTable.translate("Cut"));
        menuItem.setName("CUT");
        menuItem.addActionListener(menuListener);
        add(menuItem);
        
        if (CopyAndPasteHandler.hasCutProperty())
        {
            menuItem = new JMenuItem(recordTable.translate("Paste"));
            menuItem.setName("PASTE_CUT");
            menuItem.addActionListener(menuListener);
            add(menuItem);
        }
        
        if (CopyAndPasteHandler.hasCopiedProperty())
        {
            menuItem = new JMenuItem(recordTable.translate("Paste"));
            menuItem.setName("PASTE_COPY");
            menuItem.addActionListener(menuListener);
            add(menuItem);
        }
    }
}
