package com.pilog.t8.ui.datarecordtable.rowheader;

import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.document.validation.T8RecordValueValidationError;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableChange;
import com.pilog.t8.ui.datarecord.RecordPropertyTableColumnDefinition;
import com.pilog.t8.ui.datarecordtable.DataChangeHandler;
import com.pilog.t8.ui.datarecordtable.ModelHandler;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.Objects;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author Bouwer du Preez
 */
public class RowHeaderTable extends JTable
{
    private final JTable table;
    private final ModelHandler modelHandler;
    private DataChangeHandler changeHandler;
    private int rowConfigured; // Holds a row index for which tooltip text is currently set up.
    private boolean selectionUpdateFromTable; // A boolean value that is set when the selection of row headers is being updated from the target table.
    private boolean selectionUpdateFromHeader; // A boolean value that is set when the selection of table rows is being updated from the row header table.

    public RowHeaderTable(ModelHandler modelHandler, DataChangeHandler changeHandler, JTable table)
    {
        this.table = table;
        this.modelHandler = modelHandler;
        this.changeHandler = changeHandler;
        this.setAutoCreateColumnsFromModel(false);
        this.setGridColor(Color.WHITE);
        this.addMouseMotionListener(new RowHeaderMouseListener());
        this.getSelectionModel().addListSelectionListener(new RowSelectionListener());
        this.table.getSelectionModel().addListSelectionListener(new TableSelectionListener());
        this.rowConfigured = -1;
        this.selectionUpdateFromTable = false;
        this.selectionUpdateFromHeader = false;
    }

    public void setDataChangeHandler(DataChangeHandler changeHandler)
    {
        this.changeHandler = changeHandler;
    }

    private void updateTooltip(Point mouseLocation)
    {
        int rowIndex;

        rowIndex = this.rowAtPoint(mouseLocation);
        if ((rowIndex > -1) && (rowIndex != rowConfigured))
        {
            TableRowHeader rowHeader;

            rowHeader = modelHandler.getRowHeader(rowIndex);
            setToolTipText(createTooltipText(rowHeader));
            rowConfigured = rowIndex;
        }
    }

    private String createTooltipText(TableRowHeader rowHeader)
    {
        T8DataRecordValueStringGenerator valueStringGenerator;
        List<T8DataValidationError> validationErrors;
        String rowGUID;
        boolean rowUpdated;

        // Create a value String generator.
        valueStringGenerator = new T8DataRecordValueStringGenerator();

        // Get the row GUID.
        rowGUID = rowHeader.getDataRecord().getID();
        rowUpdated = changeHandler.isRowUpdated(rowGUID);
        validationErrors = rowHeader.getValidationErrors();

        // Only generate a tooltip if there are row updates or validation errors for the row.
        if ((validationErrors != null) && (validationErrors.size() > 0) || rowUpdated)
        {
            StringBuffer text;

            text = new StringBuffer();
            text.append("<html>\n");

            // Append the updated data information if applicable.
            if (rowUpdated)
            {
                T8DataRecordTableChange change;
                DataRecord dataRecord;

                change = changeHandler.getUpdateChange(rowGUID);
                text.append("<h4>Row Updates</h4>\n");
                text.append("<table>");
                text.append("<tr><td><b>Field</b></td><td width=100><b>Old Value</b></td><td width=200><b>New Value</b></td></tr>");

                dataRecord = change.getDataRecord();
                for (RecordPropertyTableColumnDefinition columnDefinition : modelHandler.getColumnDefinitions())
                {
                    String propertyID;

                    propertyID = columnDefinition.getPropertyRequirement().getConceptID();
                    if (columnDefinition.isVisible())
                    {
                        RecordValue newValue;
                        String oldValueString;
                        StringBuilder newValueString;

                        newValue = dataRecord.getRecordValue(propertyID);
                        oldValueString = "Not Available";
                        newValueString = newValue != null ? valueStringGenerator.generateValueString(newValue) : null;

                        // Only show values that changed.
                        if (!Objects.equals(oldValueString, newValueString))
                        {
                            text.append("<tr><td>");
                            text.append(columnDefinition.getColumnName());
                            text.append("</td><td>");
                            text.append(oldValueString);
                            text.append("</td><td>");
                            text.append(newValueString);
                            text.append("</td></tr>");
                        }
                    }
                }

                text.append("</table>");
            }

            // Append the validation error information if applicable.
            if (validationErrors != null)
            {
                text.append("<h4>Validation Errors</h4>\n");
                text.append("<table>");
                text.append("<tr><td><b>Field</b></td><td><b>Validation Error Message</b></td></tr>");

                for (T8DataValidationError error : validationErrors)
                {
                    if (error instanceof T8RecordValueValidationError)
                    {
                        T8RecordValueValidationError validationError;
                        String propertyID;
                        String columnName;

                        validationError = (T8RecordValueValidationError)error;
                        propertyID = validationError.getPropertyId();
                        columnName = validationError.getPropertyId();
                        for (RecordPropertyTableColumnDefinition columnDefinition : modelHandler.getColumnDefinitions())
                        {
                            if (columnDefinition.getPropertyRequirement().getConceptID().equals(propertyID))
                            {
                                columnName = columnDefinition.getColumnName();
                            }
                        }

                        text.append("<tr><td>");
                        text.append(columnName);
                        text.append("</td><td>");
                        text.append(error.getErrorMessage());
                        text.append("</td></tr>");
                    }
                }

                text.append("</table>");
            }

            text.append("</html>");
            return text.toString();
        }
        else return null;
    }

    private class RowHeaderMouseListener implements MouseMotionListener
    {
        @Override
        public void mouseDragged(MouseEvent e)
        {
        }

        @Override
        public void mouseMoved(MouseEvent e)
        {
            updateTooltip(e.getPoint());
        }
    }

    private class RowSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!selectionUpdateFromTable)
            {
                int[] selectedIndices;

                selectionUpdateFromHeader = true;

                table.clearSelection();
                selectedIndices = getSelectedRows();
                for (int selectedIndex : selectedIndices)
                {
                    table.getSelectionModel().addSelectionInterval(selectedIndex, selectedIndex);
                }

                selectionUpdateFromHeader = false;
            }
        }
    }

    private class TableSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!selectionUpdateFromHeader)
            {
                int[] selectedIndices;

                selectionUpdateFromTable = true;

                clearSelection();
                selectedIndices = table.getSelectedRows();
                for (int selectedIndex : selectedIndices)
                {
                    getSelectionModel().addSelectionInterval(selectedIndex, selectedIndex);
                }

                selectionUpdateFromTable = false;
            }
        }
    }
}
