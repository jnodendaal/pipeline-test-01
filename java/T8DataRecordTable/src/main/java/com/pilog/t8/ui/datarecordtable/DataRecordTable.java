package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import com.pilog.t8.ui.busypanel.T8BusyPanel;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import com.pilog.t8.ui.T8ComponentController;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class DataRecordTable extends javax.swing.JPanel
{
    private final DataRecordTableContainer tableContainer;
    private final T8DataRecordTableDefinition definition;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private FilterPanel filterPanel;
    private DataHandler dataHandler;
    private DataChangeHandler changeHandler;
    private ModelHandler modelHandler;
    private ViewHandler viewHandler;
    private Map<String, T8DataFilter> prefilters;
    private CachedTerminologyProvider terminologyProvider;
    private T8DataFilter userDefinedFilter;
    private final T8Context context;
    private T8BusyPanel busyPanel;
    private LinkedHashMap<String, OrderMethod> fieldOrdering;
    private String drInstanceID;
    private Map<String, List<T8DataValidationError>> recordValidationErrors;
    private DataRecord parentRecord;
    private DisplayModeRadioListener displayModeRadioListener;
    private TableMouseListener tableMouseListener;
    private CellMouseListener cellMouseListener;
    private DisplayMode displayMode;

    public enum DisplayMode {PROPERTY_VALUE, PROPERTY_FTT};

    public DataRecordTable(DataRecordTableContainer tableContainer, T8DataRecordTableDefinition definition)
    {
        this.tableContainer = tableContainer;
        this.controller = tableContainer.getController();
        this.definition = definition;
        this.context = tableContainer.getAccessContext();
        this.configurationManager = context.getClientContext().getConfigurationManager();
        this.recordValidationErrors = new HashMap<String, List<T8DataValidationError>>();

        // Components have to be initialized after the configuration manager is set otherwise NPE will be caused by all translation attemps.
        initComponents();

        this.setBorder(new T8ContentHeaderBorder(tableContainer.translate("Data View")));
        this.jPanelDataView.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    }

    public void initializeComponent()
    {
        // Get the data cache.
        terminologyProvider = tableContainer.getTerminologyProvider();
        cellMouseListener = new CellMouseListener();
        tableMouseListener = new TableMouseListener();

        // Create the table handlers (the sequence is very important and should not be changed).
        dataHandler = new DataHandler(controller.getContext(), definition);
        modelHandler = new ModelHandler(definition, this, terminologyProvider);
        viewHandler = new ViewHandler(definition, this, jScrollPaneTable, jTableData);
        changeHandler = new DataChangeHandler(this, modelHandler, viewHandler, jTableData);
        viewHandler.setDataChangeHandler(changeHandler);

        // Initialize the rest of the supporting components.
        filterPanel = new FilterPanel(this);
        busyPanel = new T8BusyPanel("Loading Data...");
        jPanelProcessingView.add(busyPanel, java.awt.BorderLayout.CENTER);
        prefilters = new HashMap<String, T8DataFilter>();
        fieldOrdering = new LinkedHashMap<String, OrderMethod>();
        jLabelRecordCount.setVisible(false);
        jTableData.addMouseListener(tableMouseListener);
        displayModeRadioListener = new DisplayModeRadioListener();
        jRadioButtonPropertyValue.addItemListener(displayModeRadioListener);

        // Initialize all the available prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getPrefilterDefinitions())
        {
            prefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, null));
        }

        // Add the filter panel.
        add(filterPanel, "FILTER_VIEW");

        // Enabled the required functionality.
        displayMode = DisplayMode.PROPERTY_VALUE;
        jTextAreaParentDataRecordValueString.setMargin(new Insets(5, 5, 5, 5));
        jTextAreaParentDataRecordValueString.setText(translate("No Parent Selected"));
        jButtonInsertNewRecord.setVisible(definition.isInsertEnabled());
        jButtonDeleteSelectedRecords.setVisible(definition.isDeleteEnabled());
        jButtonCommitChanges.setVisible(definition.isCommitEnabled());
        jButtonFilter.setVisible(definition.isUserFilterEnabled());
        jButtonRefresh.setVisible(definition.isUserRefreshEnabled());

        // Set the visibility of the tool bar.
        jToolBarMain.setVisible(definition.isToolBarVisible());
        jToolBarPageControls.setVisible(definition.isToolBarVisible());
    }

    public void startComponent()
    {
        bindKeys();
    }

    public void stopComponent()
    {
        busyPanel.setBusy(false);
    }

    public void stopTableEditing()
    {
        TableCellEditor cellEditor;

        // Ensure that all editing has stopped.
        cellEditor = jTableData.getCellEditor();
        if (cellEditor != null) jTableData.getCellEditor().stopCellEditing();
    }

    private void bindKeys()
    {
        InputMap inputMap;
        ActionMap actionMap;

        // Get the correct InputMap and ActionMap.
        inputMap = this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        actionMap = this.getActionMap();

        // Create a key binding for the Enter key pressed.
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.ALT_DOWN_MASK), "modeSwitchPressed");
        actionMap.put("modeSwitchPressed", new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                toggleDisplayMode();
            }
        });
    }

    public DataRecord getParentDataRecord()
    {
        return parentRecord;
    }

    public List<String> getDataRecordIDList()
    {
        return modelHandler.getDataRecordIDList();
    }

    public List<String> getParentDataRecordIDList()
    {
        DataRecordTable parentTable;

        parentTable = tableContainer.getParentTable(drInstanceID);
        if (parentTable != null)
        {
            return parentTable.getDataRecordIDList();
        }
        else return null;
    }

    public void setParentDataRecord(DataRecord parentDataRecord)
    {
        parentRecord = parentDataRecord;
        if (parentRecord != null)
        {
            T8DataRecordDefinitionGenerator valueStringGenerator;
            String valueString;

            valueStringGenerator = new T8DataRecordDefinitionGenerator(terminologyProvider);
            valueString = valueStringGenerator.generateValueString(parentRecord).toString();

            jButtonInsertNewRecord.setEnabled(true);
            jButtonDeleteSelectedRecords.setEnabled(true);
            jTextAreaParentDataRecordValueString.setText(valueString);
        }
        else
        {
            jButtonInsertNewRecord.setEnabled(false);
            jButtonDeleteSelectedRecords.setEnabled(false);
            jTextAreaParentDataRecordValueString.setText(translate("No Parent Selected"));
        }
    }

    public String getDataRequirementInstanceIdentifier()
    {
        return drInstanceID;
    }

    public void setDataRequirementInstanceIdentifier(String dataRequirementInstanceIdentifier)
    {
        this.drInstanceID = dataRequirementInstanceIdentifier;
    }

    public DataRecordTableContainer getTableContainer()
    {
        return tableContainer;
    }

    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public void showFilterView()
    {
        ((CardLayout)this.getLayout()).show(this, "FILTER_VIEW");
    }

    public void showDataView()
    {
        ((CardLayout)this.getLayout()).show(this, "DATA_VIEW");
    }

    public void showProcessingView(String message)
    {
        busyPanel.setMessage(message);
        ((CardLayout)this.getLayout()).show(this, "PROCESSING_VIEW");
    }

    public void setDisplayMode(DisplayMode mode)
    {
        // Stop table editing.
        stopTableEditing();

        // Remove the listener from the radio button so that we don't get cyclical events when we set the UI state.
        jRadioButtonPropertyValue.removeItemListener(displayModeRadioListener);
        if (mode == DisplayMode.PROPERTY_VALUE) jRadioButtonPropertyValue.setSelected(true);
        else jRadioButtonPropertyFFT.setSelected(true);
        displayMode = mode;
        viewHandler.setDisplayMode(mode);

        // Add the removed listener back onto the radio button.
        jRadioButtonPropertyValue.addItemListener(displayModeRadioListener);
    }

    public DisplayMode getDisplayMode()
    {
        return displayMode;
    }

    public void toggleDisplayMode()
    {
        DisplayMode mode;

        mode = getDisplayMode();
        setDisplayMode(mode == DisplayMode.PROPERTY_VALUE ? DisplayMode.PROPERTY_FTT : DisplayMode.PROPERTY_VALUE);
    }

    public T8DataFilter getUserDefinedFilter()
    {
        return userDefinedFilter;
    }

    public void setUserDefinedFilter(T8DataFilter filter)
    {
        this.userDefinedFilter = filter;
        this.dataHandler.resetPageOffset();
        this.tableContainer.refreshNavigator();
    }

    public void setPrefilters(Map<String, T8DataFilter> filters)
    {
        for (String filterIdentifier : filters.keySet())
        {
            this.prefilters.put(filterIdentifier, filters.get(filterIdentifier));
        }
    }

    public void setPrefilter(String identifier, T8DataFilter filter)
    {
        prefilters.put(identifier, filter);
        this.dataHandler.resetPageOffset();
    }

    public T8DataFilter getPrefilter(String identifier)
    {
        return prefilters.get(identifier);
    }

    public void setFieldOrdering(LinkedHashMap<String, OrderMethod> fieldOrdering)
    {
        this.fieldOrdering.clear();
        if (fieldOrdering != null)
        {
            this.fieldOrdering.putAll(fieldOrdering);
        }
    }

    public Map<String, List<T8DataValidationError>> getValidationErrors()
    {
        return recordValidationErrors;
    }

    public void setValidationErrors(Map<String, List<T8DataValidationError>> recordValidationErrors)
    {
        this.recordValidationErrors = recordValidationErrors;

        // Set the validation errors on the model handler so that they can be distributed to the table row headers.
        modelHandler.setValidationErrors(recordValidationErrors);
    }

    public void quickOrderOnField(String fieldIdentifier)
    {
        if (fieldIdentifier != null)
        {
            OrderMethod orderMethod;

            orderMethod = fieldOrdering.get(fieldIdentifier); // Get existing ordering for the field (if set).
            fieldOrdering.clear(); // Clear pre-existing ordering.
            if (orderMethod == null) // Ordering was not set on the field, so set it to ascending.
            {
                fieldOrdering.put(fieldIdentifier, OrderMethod.ASCENDING);
            }
            else if (orderMethod == OrderMethod.ASCENDING) // Ordering was set to descending, so change it to ascending.
            {
                fieldOrdering.put(fieldIdentifier, OrderMethod.DESCENDING);
            }
            else // Ordering was set to descending, so change it to ascending.
            {
                fieldOrdering.put(fieldIdentifier, OrderMethod.ASCENDING);
            }

            // Refresh the data.
            refreshData();
        }
    }

    public void refreshModel()
    {
        ModelLoader loader;

        // Check that a Data Requirement Instance is specified in the definition.
        if (definition.getDataRequirementInstanceIdentifier() == null) throw new RuntimeException("No Data Requirement Instance set in definition: " + definition);

        // Create a new loader.
        loader = new ModelLoader();

        // Run the loader in a new Thread.
        new Thread(loader).start();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!loader.hasCompleted())
            {
                showProcessingView(translate("Loading Data Requirement..."));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void refreshModelUnthreaded()
    {
        ModelLoader loader;

        // Check that a Data Requirement Instance is specified in the definition.
        if (definition.getDataRequirementInstanceIdentifier() == null) throw new RuntimeException("No Data Requirement Instance set in definition: " + definition);

        // Create a new loader.
        loader = new ModelLoader();
        loader.run();
    }

    public void refreshData()
    {
        DataLoader loader;

        // Create a new loader.
        loader = new DataLoader();

        // Run the loader in a new Thread.
        new Thread(loader).start();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!loader.hasCompleted())
            {
                showProcessingView(translate("Loading Data..."));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Also do the record count if required.
        refreshRecordCount();
    }

    public void refreshDataUnthreaded()
    {
        DataLoader loader;

        // Create a new loader.
        loader = new DataLoader();
        loader.run();
    }

    public void refreshRecordCount()
    {
        DataCounter counter;

        // Hide the existing record count until it has been updated.
        jLabelRecordCount.setVisible(true);

        // Create a new loader.
        counter = new DataCounter();

        // Run the loader in a new Thread.
        new Thread(counter).start();
    }

    public boolean hasUnsavedChanges()
    {
        return changeHandler.getTableChangeCount() > 0;
    }

    private void commitTableChanges()
    {
        tableContainer.commitChanges(false);
    }

    private void retrievePreviousPage()
    {
        dataHandler.decrementPage();
        refreshData();
    }

    private void retrieveNextPage()
    {
        dataHandler.incrementPage();
        refreshData();
    }

    public List<DataRecord> getSelectedDataRecords()
    {
        return modelHandler.getDataRecords(jTableData.getSelectedRows());
    }

    public ListSelectionModel getTableSelectionModel()
    {
        return jTableData.getSelectionModel();
    }

    public void addTableSelectionListener(ListSelectionListener listener)
    {
        jTableData.getSelectionModel().addListSelectionListener(listener);
    }

    public void removeTableSelectionListener(ListSelectionListener listener)
    {
        jTableData.getSelectionModel().removeListSelectionListener(listener);
    }

    public DataChangeHandler getTableDataChangeHandler()
    {
        return changeHandler;
    }

    public ViewHandler getTableViewHandler()
    {
        return viewHandler;
    }

    public ModelHandler getTableModelHandler()
    {
        return modelHandler;
    }

    public void addTableModelListener(TableModelListener listener)
    {
        modelHandler.addTableModelListener(listener);
    }

    public void removeTableModelListener(TableModelListener listener)
    {
        modelHandler.removeTableModelListener(listener);
    }

    MouseListener getTableMouseListener()
    {
        return tableMouseListener;
    }

    MouseListener getCellMouseListener()
    {
        return cellMouseListener;
    }

    /**
     * Creates and returns a data filter that includes all of the prefilters set
     * on the table along with the user-set filter criteria and ordering.
     *
     * @return The combined filter to use when retrieving data to display in the
     * table.
     */
    public T8DataFilter getCombinedFilter()
    {
        T8DataFilterCriteria combinedFilterCriteria;
        T8DataFilter combinedFilter;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilterCriteria = new T8DataFilterCriteria();

        // Add all available and valid prefilters.
        for (T8DataFilter prefilter : prefilters.values())
        {
            // Only include filters that have valid filter criteria.
            if ((prefilter != null) && (prefilter.hasFilterCriteria()))
            {
                combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
            }
        }

        // Add a filter clause for retrieving only the specified DR Instance.
        combinedFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(definition.getDataRecordDocumentDEID() + EF_DR_INSTANCE_ID, DataFilterOperator.EQUAL, drInstanceID));

        // Add the regular user-defined filter if applicable.
        if ((userDefinedFilter != null) && (userDefinedFilter.hasFilterCriteria())) combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, userDefinedFilter.getFilterCriteria());

        // Create the combined filter from the filter criteria.
        combinedFilter = new T8DataFilter(definition.getDataRecordDocumentDEID(), combinedFilterCriteria);

        // Add the user-defined ordering if applicable.
        combinedFilter.setFieldOrdering(fieldOrdering);

        // Return a new filter created from the combined criteria.
        return combinedFilter;
    }

    public DataRecord insertNewRecord()
    {
        stopTableEditing();
        jTableData.getSelectionModel().clearSelection();
        if (parentRecord != null)
        {
            DataRecord newRecord;

            jTableData.getSelectionModel().setValueIsAdjusting(true);
            newRecord = changeHandler.insertNewRecord(parentRecord);
            jTableData.getSelectionModel().setValueIsAdjusting(false);
            return newRecord;
        }
        else
        {
            JOptionPane.showMessageDialog(this, translate("A new record cannot be added without a selected parent."), translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
    }

    public void deleteSelectedRecords()
    {
        stopTableEditing();
        if (parentRecord != null)
        {
            // Remove the selected records from this table.
            changeHandler.deleteSelectedRows(parentRecord, getSelectedDataRecords());
        }
        else
        {
            JOptionPane.showMessageDialog(this, translate("Only records belonging to the selected parent can be deleted."), translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void displayCellPopupMenu(Object source, Point location)
    {
        JPopupMenu popupMenu;

        // Display the newly constructed popup menu.
        popupMenu = new TablePopupMenu(this, (Component)source);
        popupMenu.show((Component)source, location.x, location.y);
    }

    private void displayTablePopupMenu(Point location)
    {
        JPopupMenu popupMenu;

        // Display the newly constructed popup menu.
        popupMenu = new TablePopupMenu(this, jTableData);
        popupMenu.show(jTableData, location.x, location.y);
    }

    private class ModelLoader implements Runnable
    {
        private boolean completed = false;

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            try
            {
                DataRequirementInstance dataRequirementInstance;

                // Retrieve the Data Requirement Instance specified by the definition.
                dataRequirementInstance = dataHandler.loadDataRequirementInstance(drInstanceID);
                if (dataRequirementInstance != null)
                {
                    // Cache the concepts used by the data requirement.
                    terminologyProvider.cacheTerminology(ArrayLists.newArrayList(context.getSessionContext().getContentLanguageIdentifier()), DocumentHandler.getAllConceptIds(dataRequirementInstance));

                    // Use the retrieved Data Requirement Instance to construct a table model.
                    modelHandler.buildTableModel(dataRequirementInstance);
                    viewHandler.setModel(modelHandler.getTableModel());
                    changeHandler.setModel(modelHandler.getTableModel());
                }
                else
                {
                    modelHandler.clearTableModel();
                    viewHandler.setModel(modelHandler.getTableModel());
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                modelHandler.clearTableModel();
                viewHandler.setModel(modelHandler.getTableModel());
            }

            // Clear all previous table changes.
            changeHandler.clearChanges();

            // Rebuild the Filter panel using the new model.
            filterPanel.rebuildFilterTable();

            // Set the completed flag.
            completed = true;

            // Show the loaded data.
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    // Show the loaded data.
                    showDataView();
                }
            });
        }
    }

    private class DataLoader implements Runnable
    {
        private boolean completed = false;

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            try
            {
                List<DataRecord> dataRecords;

                // Use the combined filter to retrieve the filtered data and to construct a table model.
                dataRecords = dataHandler.loadDataRecords(getParentDataRecordIDList(), getCombinedFilter(), terminologyProvider);
                modelHandler.setModelDataRecords(dataRecords, dataHandler.getPageOffset());
                viewHandler.setModel(modelHandler.getTableModel());
                changeHandler.clearChanges();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            // Set the page number label.
            jLabelPageStartOffset.setText("" + dataHandler.getPageOffset());
            jLabelPageEndOffset.setText("" + (dataHandler.getPageOffset() + dataHandler.getPageSize() -1));

            // Clear all previous table changes.
            changeHandler.clearChanges();

            // Set the completed flag.
            completed = true;

            // Show the loaded data.
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    // Show the loaded data.
                    showDataView();
                }
            });
        }
    }

    private class DataCounter implements Runnable
    {
        private boolean completed = false;

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            try
            {
                final int recordCount;

                // Use the combined filter to retrieve the filtered data and to construct a table model.
                recordCount = dataHandler.countDataRecords(getCombinedFilter());

                // Update the UI to reflect the record count.
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        jLabelRecordCount.setText("(" + recordCount + ")");
                        jLabelRecordCount.setVisible(true);
                    }
                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                jLabelRecordCount.setVisible(false);
            }

            // Set the completed flag.
            completed = true;
        }
    }

    private class DisplayModeRadioListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            setDisplayMode((e.getStateChange() == ItemEvent.SELECTED) ? DisplayMode.PROPERTY_VALUE : DisplayMode.PROPERTY_FTT);
        }
    }

    private class TableMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger()) displayTablePopupMenu(e.getPoint());
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger()) displayTablePopupMenu(e.getPoint());
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger()) displayTablePopupMenu(e.getPoint());
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class CellMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger()) displayCellPopupMenu(e.getSource(), e.getPoint());
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger()) displayCellPopupMenu(e.getSource(), e.getPoint());
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger()) displayCellPopupMenu(e.getSource(), e.getPoint());
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupDisplayMode = new javax.swing.ButtonGroup();
        jPanelDataView = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonCommitChanges = new javax.swing.JButton();
        jButtonRefresh = new javax.swing.JButton();
        jButtonFilter = new javax.swing.JButton();
        jButtonInsertNewRecord = new javax.swing.JButton();
        jButtonDeleteSelectedRecords = new javax.swing.JButton();
        jLabelDisplayMode = new javax.swing.JLabel();
        jRadioButtonPropertyValue = new javax.swing.JRadioButton();
        jRadioButtonPropertyFFT = new javax.swing.JRadioButton();
        jToolBarPageControls = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelPageRow = new javax.swing.JLabel();
        jLabelPageStartOffset = new javax.swing.JLabel();
        jLabelPageRangeDash = new javax.swing.JLabel();
        jLabelPageEndOffset = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jLabelRecordCount = new javax.swing.JLabel();
        jPanelParentView = new javax.swing.JPanel();
        jLabelSelectedParent = new javax.swing.JLabel();
        jTextAreaParentDataRecordValueString = new javax.swing.JTextArea();
        jScrollPaneTable = new javax.swing.JScrollPane();
        jTableData = new javax.swing.JTable();
        jPanelProcessingView = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.CardLayout());

        jPanelDataView.setOpaque(false);
        jPanelDataView.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonCommitChanges.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/commitIcon.png"))); // NOI18N
        jButtonCommitChanges.setText(translate("Commit"));
        jButtonCommitChanges.setToolTipText("Commit Changes");
        jButtonCommitChanges.setFocusable(false);
        jButtonCommitChanges.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonCommitChanges.setOpaque(false);
        jButtonCommitChanges.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCommitChanges.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCommitChangesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCommitChanges);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(translate("Refresh"));
        jButtonRefresh.setToolTipText("Refresh Data");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonFilter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/filterIcon.png"))); // NOI18N
        jButtonFilter.setText(translate("Filter"));
        jButtonFilter.setFocusable(false);
        jButtonFilter.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonFilter.setOpaque(false);
        jButtonFilter.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonFilter.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonFilterActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonFilter);

        jButtonInsertNewRecord.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableRowInsertIcon.png"))); // NOI18N
        jButtonInsertNewRecord.setText(translate("Insert"));
        jButtonInsertNewRecord.setToolTipText(translate("Insert New Record"));
        jButtonInsertNewRecord.setFocusable(false);
        jButtonInsertNewRecord.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonInsertNewRecord.setOpaque(false);
        jButtonInsertNewRecord.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonInsertNewRecord.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonInsertNewRecordActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonInsertNewRecord);

        jButtonDeleteSelectedRecords.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableRowDeleteIcon.png"))); // NOI18N
        jButtonDeleteSelectedRecords.setText(translate("Delete"));
        jButtonDeleteSelectedRecords.setToolTipText(translate("Delete Selected Records"));
        jButtonDeleteSelectedRecords.setFocusable(false);
        jButtonDeleteSelectedRecords.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDeleteSelectedRecords.setOpaque(false);
        jButtonDeleteSelectedRecords.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteSelectedRecords.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteSelectedRecordsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteSelectedRecords);

        jLabelDisplayMode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordtable/icons/displayModeIcon.png"))); // NOI18N
        jLabelDisplayMode.setText(translate("Display Mode:"));
        jLabelDisplayMode.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 2, 0, 3));
        jToolBarMain.add(jLabelDisplayMode);

        buttonGroupDisplayMode.add(jRadioButtonPropertyValue);
        jRadioButtonPropertyValue.setSelected(true);
        jRadioButtonPropertyValue.setText(translate("Property Value"));
        jRadioButtonPropertyValue.setToolTipText(translate("Show Property Value"));
        jRadioButtonPropertyValue.setFocusable(false);
        jRadioButtonPropertyValue.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jRadioButtonPropertyValue.setOpaque(false);
        jToolBarMain.add(jRadioButtonPropertyValue);

        buttonGroupDisplayMode.add(jRadioButtonPropertyFFT);
        jRadioButtonPropertyFFT.setText(translate("Property FFT"));
        jRadioButtonPropertyFFT.setToolTipText(translate("Show Property Free-Format Text"));
        jRadioButtonPropertyFFT.setFocusable(false);
        jRadioButtonPropertyFFT.setOpaque(false);
        jToolBarMain.add(jRadioButtonPropertyFFT);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelDataView.add(jToolBarMain, gridBagConstraints);

        jToolBarPageControls.setFloatable(false);
        jToolBarPageControls.setRollover(true);
        jToolBarPageControls.setOpaque(false);

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(translate("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonPreviousPage);

        jLabelPageRow.setText(translate("Row: "));
        jToolBarPageControls.add(jLabelPageRow);
        jToolBarPageControls.add(jLabelPageStartOffset);

        jLabelPageRangeDash.setText("-");
        jToolBarPageControls.add(jLabelPageRangeDash);
        jToolBarPageControls.add(jLabelPageEndOffset);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(translate("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonNextPage);

        jLabelRecordCount.setText("(0)");
        jToolBarPageControls.add(jLabelRecordCount);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelDataView.add(jToolBarPageControls, gridBagConstraints);

        jPanelParentView.setOpaque(false);
        jPanelParentView.setLayout(new java.awt.GridBagLayout());

        jLabelSelectedParent.setText(translate("Selected Parent:"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanelParentView.add(jLabelSelectedParent, gridBagConstraints);

        jTextAreaParentDataRecordValueString.setEditable(false);
        jTextAreaParentDataRecordValueString.setColumns(20);
        jTextAreaParentDataRecordValueString.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jTextAreaParentDataRecordValueString.setForeground(new java.awt.Color(102, 102, 102));
        jTextAreaParentDataRecordValueString.setLineWrap(true);
        jTextAreaParentDataRecordValueString.setTabSize(0);
        jTextAreaParentDataRecordValueString.setWrapStyleWord(true);
        jTextAreaParentDataRecordValueString.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        jPanelParentView.add(jTextAreaParentDataRecordValueString, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelDataView.add(jPanelParentView, gridBagConstraints);

        jTableData.setAutoCreateColumnsFromModel(false);
        jTableData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        jTableData.setGridColor(new java.awt.Color(204, 204, 204));
        jTableData.setRowHeight(20);
        jScrollPaneTable.setViewportView(jTableData);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelDataView.add(jScrollPaneTable, gridBagConstraints);

        add(jPanelDataView, "DATA_VIEW");

        jPanelProcessingView.setLayout(new java.awt.BorderLayout());
        add(jPanelProcessingView, "PROCESSING_VIEW");
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        tableContainer.revertChanges();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        retrievePreviousPage();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        retrieveNextPage();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    private void jButtonInsertNewRecordActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInsertNewRecordActionPerformed
    {//GEN-HEADEREND:event_jButtonInsertNewRecordActionPerformed
        insertNewRecord();
    }//GEN-LAST:event_jButtonInsertNewRecordActionPerformed

    private void jButtonDeleteSelectedRecordsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteSelectedRecordsActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteSelectedRecordsActionPerformed
        deleteSelectedRecords();
    }//GEN-LAST:event_jButtonDeleteSelectedRecordsActionPerformed

    private void jButtonCommitChangesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCommitChangesActionPerformed
    {//GEN-HEADEREND:event_jButtonCommitChangesActionPerformed
        commitTableChanges();
    }//GEN-LAST:event_jButtonCommitChangesActionPerformed

    private void jButtonFilterActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonFilterActionPerformed
    {//GEN-HEADEREND:event_jButtonFilterActionPerformed
        showFilterView();
    }//GEN-LAST:event_jButtonFilterActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupDisplayMode;
    private javax.swing.JButton jButtonCommitChanges;
    private javax.swing.JButton jButtonDeleteSelectedRecords;
    private javax.swing.JButton jButtonFilter;
    private javax.swing.JButton jButtonInsertNewRecord;
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelDisplayMode;
    private javax.swing.JLabel jLabelPageEndOffset;
    private javax.swing.JLabel jLabelPageRangeDash;
    private javax.swing.JLabel jLabelPageRow;
    private javax.swing.JLabel jLabelPageStartOffset;
    private javax.swing.JLabel jLabelRecordCount;
    private javax.swing.JLabel jLabelSelectedParent;
    private javax.swing.JPanel jPanelDataView;
    private javax.swing.JPanel jPanelParentView;
    private javax.swing.JPanel jPanelProcessingView;
    private javax.swing.JRadioButton jRadioButtonPropertyFFT;
    private javax.swing.JRadioButton jRadioButtonPropertyValue;
    private javax.swing.JScrollPane jScrollPaneTable;
    private javax.swing.JTable jTableData;
    private javax.swing.JTextArea jTextAreaParentDataRecordValueString;
    private javax.swing.JToolBar jToolBarMain;
    private javax.swing.JToolBar jToolBarPageControls;
    // End of variables declaration//GEN-END:variables
}
