package com.pilog.t8.ui.datarecordtable;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataRecordValidationException;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableChange;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class ContainerDataHandler
{
    private final T8DataRecordTableDefinition tableDefinition;
    private final T8Context context;
    private final T8DataRecordClientApi recApi;

    public ContainerDataHandler(T8Context context, T8DataRecordClientApi recApi, T8DataRecordTableDefinition tableDefinition)
    {
        this.context = context;
        this.tableDefinition = tableDefinition;
        this.recApi = recApi;
    }

    public void commitTableChanges(List<T8DataRecordTableChange> dataChanges) throws Exception, T8DataRecordValidationException
    {
        List<DataRecord> insertedRecords;
        List<DataRecord> updatedRecords;
        List<String> deletedRecords;

        // Create lists of changes according to type.
        insertedRecords = new ArrayList<DataRecord>();
        updatedRecords = new ArrayList<DataRecord>();
        deletedRecords = new ArrayList<String>();
        for (T8DataRecordTableChange tableChange : dataChanges)
        {
            int changeType;

            changeType = tableChange.getChangeType();
            if (changeType == T8DataRecordTableChange.CHANGE_TYPE_INSERT)
            {
                insertedRecords.add(tableChange.getDataRecord());
            }
            else if (changeType == T8DataRecordTableChange.CHANGE_TYPE_UPDATE)
            {
                updatedRecords.add(tableChange.getDataRecord());
            }
            else if (changeType == T8DataRecordTableChange.CHANGE_TYPE_UPDATE)
            {
                deletedRecords.add(tableChange.getDataRecord().getID());
            }
            else throw new RuntimeException("Invalid Data Record change type encountered: " + changeType);
        }

        // Commit the changes using the organization API.
        //recApi.saveDataRecordChanges(insertedRecords, updatedRecords, deletedRecords, tableDefinition.getDataRecordAccessIdentifier(), EnumSet.allOf(RecordMetaDataType.class), false);
    }

    public void validateFilteredRecords(T8DataFilter dataFilter, List<String> validationScriptIdentifiers) throws Exception, T8DataRecordValidationException
    {
        Map<String, Object> inputParameters;
        Map<String, Object> outputParameters;
        T8DataFileValidationReport validationReport;

        // Create the server operation parameters collection.
        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER, tableDefinition.getDataRecordDocumentDEID());
        inputParameters.put(PARAMETER_SERVER_VALIDATION_SCRIPT_IDENTIFIER_LIST, validationScriptIdentifiers);
        inputParameters.put(PARAMETER_DATA_FILTER, dataFilter);

        // Execute the server operation.
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, SERVER_OPERATION_VALIDATE_FILTERED_DATA_RECORDS, inputParameters);
        if (outputParameters != null)
        {
            validationReport = (T8DataFileValidationReport)outputParameters.get(PARAMETER_DATA_RECORD_VALIDATION_REPORT_LIST);
            if (validationReport != null) throw new T8DataRecordValidationException("Record Validation Failed", validationReport);
        }
    }
}
