package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.CachedDataRecordProvider;
import com.pilog.t8.data.document.CachedDataRequirementInstanceProvider;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8LRUCachedDataRecordProvider;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.document.validation.T8DataRecordValidationException;
import com.pilog.t8.data.org.T8ClientDataRecordProvider;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableAPIHandler;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableChange;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import com.pilog.t8.ui.busypanel.T8BusyPanel;
import com.pilog.t8.ui.datarecord.DataRecordEditorFactory;
import com.pilog.t8.ui.datarecordtable.component.T8DataRecordTableComponentFactory;
import com.pilog.t8.ui.datarecordtable.navigationtree.NavigationModel;
import com.pilog.t8.ui.datarecordtable.navigationtree.NavigationView;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTable extends javax.swing.JPanel implements T8Component, DataRecordTableContainer
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordTable.class);
    private static final String SPLIT_LAYOUT = "(ROW (LEAF name=left weight=0.3) (LEAF name=right weight=0.7))";

    public static final String ROW_HEADER_COLUMN_NAME = "T8_TABLE_ROW_HEADER";

    private final T8DataRecordTableDefinition definition;
    private T8DataRecordClientApi recApi;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private T8DataRecordTableOperationHandler operationHandler;
    private final DataRecordEditorFactory componentFactory;
    private final T8DataRecordAccessHandler accessHandler;
    private final T8Context context;
    private final T8Context accessContext;
    private CachedDataRecordProvider dataRecordProvider;
    private CachedTerminologyProvider terminologyProvider;
    private CachedDataRequirementInstanceProvider drInstanceProvider;
    private T8BusyPanel busyPanel;
    private ContainerDataHandler dataHandler;
    private final NavigationView navigationView;
    private final NavigationModel navigationModel;
    private final Map<String, T8DataFilter> prefilters;
    private final JXMultiSplitPane jXMultiSplitPaneContent;
    private final JPanel jPanelContent;
    private final TableSelectionListener tableSelectionListener;
    private final TableModelListener tableChangeListener;
    private DataRecordTable currentTable; // Holds a reference to the currently displayed table.
    private final Map<String, DataRecordTable> tables; // Holds the collection of all currently loaded tables.
    private final Map<String, Map<String, List<T8DataValidationError>>> validationErrors; // Holds all current validation errors Map<DR_ISNTANCE_ID, Map<RECORD_ID, List<T8DataRecordValidationError>>>.

    public T8DataRecordTable(T8DataRecordTableDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.accessContext = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.accessHandler = null;
        this.componentFactory = new T8DataRecordTableComponentFactory(this);
        this.navigationModel = new NavigationModel();
        this.navigationView = new NavigationView(this);
        this.navigationView.setNavigationModel(navigationModel);
        this.tables = new HashMap<>();
        this.prefilters = new HashMap<>();
        this.validationErrors = new HashMap<>();

        // Instantiate table listeners.
        tableChangeListener = new TableChangeListener();
        tableSelectionListener = new TableSelectionListener();

        // Components have to be initialized after the configuration manager is set otherwise NPE will be caused by all translation attemps.
        initComponents();

        this.jPanelContent = new JPanel();
        this.jPanelContent.setOpaque(false);
        this.jPanelContent.setLayout(new BorderLayout());
        this.jXMultiSplitPaneContent = new JXMultiSplitPane();
        this.jXMultiSplitPaneContent.setOpaque(false);
        this.add(jXMultiSplitPaneContent, "DATA_VIEW");
        this.jXMultiSplitPaneContent.setModel(MultiSplitLayout.parseModel(SPLIT_LAYOUT));
        this.jXMultiSplitPaneContent.getMultiSplitLayout().setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        this.jXMultiSplitPaneContent.getMultiSplitLayout().setLayoutByWeight(true);
        this.jXMultiSplitPaneContent.add(navigationView, "left");
        this.jXMultiSplitPaneContent.add(jPanelContent, "right");

        // Show the data view.
        showDataView();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public T8Context getAccessContext()
    {
        return accessContext;
    }

    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8DataRecordTableDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.recApi = (T8DataRecordClientApi)controller.getClientContext().getConfigurationManager().getAPI(context, T8DataRecordClientApi.API_IDENTIFIER);
        this.dataHandler = new ContainerDataHandler(controller.getContext(), recApi, definition);
        this.dataRecordProvider = new T8LRUCachedDataRecordProvider(new T8ClientDataRecordProvider(recApi), 50);
        //this.drInstanceProvider = new T8LRUCachedDataRequirementInstanceProvider(new T8ClientOrganizationDataRequirementInstanceProvider(recApi), 50);
        //this.terminologyProvider = new T8LRUCachedTerminologyProvider(new T8ClientOrganizationTerminologyProvider(recApi), 200);
        //this.terminologyProvider.setLanguage(sessionContext.getContentLanguageIdentifier());
        this.operationHandler = new T8DataRecordTableOperationHandler(this);
        this.busyPanel = new T8BusyPanel("Initializing Component...");
        this.jPanelProcessingView.add(busyPanel, java.awt.BorderLayout.CENTER);

        // Initialize all the available prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getPrefilterDefinitions())
        {
            prefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, null));
        }
    }

    @Override
    public void startComponent()
    {
        DataRecordTable newTable;
        String drInstanceID;

        // Add the first (root) table and show it.
        drInstanceID = definition.getDataRequirementInstanceIdentifier();
        newTable = addTable(drInstanceID);
        showTable(drInstanceID);

        // Load the table's model.
        newTable.refreshModelUnthreaded();

        // Auto-retrieve data if necessary.
        if (definition.isAutoRetrieve())
        {
            newTable.refreshDataUnthreaded();
        }
    }

    @Override
    public void stopComponent()
    {
        // Stop the busy panel.
        busyPanel.setBusy(false);

        // Stop all open tables.
        for (DataRecordTable table : tables.values())
        {
            table.stopComponent();
        }
    }

    public void refreshData(String drInstanceID)
    {
        if (drInstanceID != null)
        {
            DataRecordTable table;

            table = tables.get(drInstanceID);
            if (table != null) table.refreshData();
        }
        else
        {
            String rootDRInstanceID;

            rootDRInstanceID = navigationModel.getRootDRInstanceID();
            if (rootDRInstanceID != null)
            {
                DataRecordTable rootTable;

                rootTable = tables.get(rootDRInstanceID);
                if (rootTable != null) rootTable.refreshData();
            }
        }
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object layoutConstraints)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    @Override
    public DataRecordTable addTable(String drInstanceID)
    {
        DataRecordTable newTable;

        LOGGER.log("Adding Table: " + drInstanceID);
        newTable = new DataRecordTable(this, definition);
        newTable.setDataRequirementInstanceIdentifier(drInstanceID);
        newTable.initializeComponent();
        newTable.startComponent();
        newTable.setValidationErrors(validationErrors.get(drInstanceID));
        tables.put(drInstanceID, newTable);

        // If this is the first document to be added, create a new navigation model for it.
        if (navigationModel.getRootDRInstanceID() == null)
        {
            navigationModel.setRootDRInstanceID(drInstanceID);
            newTable.setPrefilters(prefilters); // We only add the prefilters to the root table.  All descendants are then filtered from this root.
        }
        else
        {
            navigationModel.addNode(currentTable.getDataRequirementInstanceIdentifier(), drInstanceID);
        }

        // Add listeners.
        newTable.addTableModelListener(tableChangeListener);
        newTable.addTableSelectionListener(tableSelectionListener);

        return newTable;
    }

    public void removeTable(String drInstanceID)
    {
        DataRecordTable tableToRemove;

        // Get the table to remove and stop it's functionality.
        tableToRemove = getTable(drInstanceID);
        if (tableToRemove != null) tableToRemove.stopComponent();

        // Remove listeners.
        tableToRemove.removeTableModelListener(tableChangeListener);
        tableToRemove.removeTableSelectionListener(tableSelectionListener);

        // If the table to remove is the currently displayed table, switch the view to its parent.
        if (currentTable.getDataRequirementInstanceIdentifier().equals(drInstanceID))
        {
            showDocument(navigationModel.findParentDRInstanceID(drInstanceID));
        }

        // Remove the table from the navigation model and table collection.
        navigationModel.removeNode(drInstanceID);
        tables.remove(drInstanceID);
    }

    public void removeAllTables()
    {
        List<String> drInstanceIDList;

        drInstanceIDList = new ArrayList<>(tables.keySet());
        for (String drInstanceID : drInstanceIDList)
        {
            removeTable(drInstanceID);
        }

        showDocument(null);
    }

    @Override
    public void showTable(String drInstanceID)
    {
        if (drInstanceID == null)
        {
            LOGGER.log("Showing Table: None");
            currentTable = null;
            navigationView.setSelectedNode(drInstanceID);
            jPanelContent.removeAll();
            jPanelContent.revalidate();
            repaint();
        }
        else
        {
            if ((currentTable == null) || (!drInstanceID.equals(currentTable.getDataRequirementInstanceIdentifier())))
            {
                LOGGER.log("Showing Table: " + drInstanceID);
                jPanelContent.removeAll();
                currentTable = tables.get(drInstanceID);
                if (currentTable != null)
                {
                    jPanelContent.add(currentTable);
                }

                navigationView.setSelectedNode(drInstanceID);
                jPanelContent.revalidate();
                repaint();
            }
        }
    }

    @Override
    public DataRecordTable getTable(String drInstanceID)
    {
        return tables.get(drInstanceID);
    }

    @Override
    public DataRecordTable getParentTable(String drInstanceID)
    {
        String parentDRInstanceID;

        parentDRInstanceID = navigationModel.findParentDRInstanceID(drInstanceID);
        return tables.get(parentDRInstanceID);
    }

    @Override
    public List<DataRecordTable> getChildTables(String drInstanceID)
    {
        List<String> drInstanceIDList;
        List<DataRecordTable> childTables;

        childTables = new ArrayList<>();
        drInstanceIDList = navigationModel.findChildDRInstanceIDs(drInstanceID);
        for (String childDRInstanceID : drInstanceIDList)
        {
            childTables.add(tables.get(childDRInstanceID));
        }

        return childTables;
    }

    @Override
    public boolean containsTable(String drInstanceID)
    {
        return tables.containsKey(drInstanceID);
    }

    private void showDataView()
    {
        ((CardLayout)this.getLayout()).show(this, "DATA_VIEW");
    }

    public void showProcessingView(String message)
    {
        busyPanel.setMessage(message);
        ((CardLayout)this.getLayout()).show(this, "PROCESSING_VIEW");
    }

    public T8DataFilter getUserDefinedFilter()
    {
        return null;
    }

    public void setUserDefinedFilter(T8DataFilter filter)
    {
    }

    public void setPrefilter(String identifier, T8DataFilter filter)
    {
        prefilters.put(identifier, filter);
        for (DataRecordTable table : tables.values())
        {
            table.setPrefilter(identifier, filter);
        }
    }

    public T8DataFilter getPrefilter(String identifier)
    {
        return prefilters.get(identifier);
    }

    public T8DataFilter getCombinedFilter()
    {
        return null;
    }

    public List<DataRecord> getSelectedDataRecords()
    {
        return new ArrayList<>();
    }

    @Override
    public CachedDataRecordProvider getDataRecordProvider()
    {
        return dataRecordProvider;
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return null;
    }

    @Override
    public CachedTerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    @Override
    public T8DataRecordAccessHandler getAccessHandler()
    {
        return accessHandler;
    }

    @Override
    public DataRecordEditorFactory getEditorComponentFactory()
    {
        return componentFactory;
    }

    @Override
    public DataRecord createNewDocument(String parentRecordID, String parentPropertyID, String parentFieldID, String recordID, String drInstanceID, boolean refresh) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean containsDocument(String string)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DataRecord getDocument(String string)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void showDocument(String string)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DataRecord loadDocument(String string, boolean refresh)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeDocument(String string)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getDescriptionClassificationIdentifier()
    {
        return definition.getDescriptionClassificationIdentifier();
    }

    @Override
    public List<String> getServerValidationScriptIdentifiers()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getNewRecordTagInheritanceList()
    {
        return definition.getNewRecordTagInheritanceList();
    }

    public void setValidationErrors(Map<String, List<T8DataValidationError>> validationErrors)
    {
        // Compile a map of all validation errors from the list of reports.
        this.validationErrors.clear();

        // Set the validation errors on all of the tables.
        for (DataRecordTable table : tables.values())
        {
        }

        // Refresh the navigation view.
        refreshNavigator();
    }

    @Override
    public void refreshNavigator()
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                navigationModel.nodesChanged();
            }
        });
    }

    public boolean hasUnsavedChanges()
    {
        for (DataRecordTable table : tables.values())
        {
            if (table.hasUnsavedChanges()) return true;
        }

        return false;
    }

    @Override
    public void refreshData(DataRecordTable targetTable, boolean refreshDescendants)
    {
        DataLoader loader;

        // Create a new loader.
        loader = new DataLoader(targetTable, refreshDescendants);

        // Run the loader in a new Thread.
        new Thread(loader).start();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!loader.hasCompleted())
            {
                showProcessingView(translate("Loading Data..."));
            }
        }
        catch (Exception e)
        {
            LOGGER.log(e);
        }
    }

    @Override
    public void revertChanges()
    {
        ChangeReverter reverter;

        // Create a new reverter operation handler.
        reverter = new ChangeReverter(new ArrayList<>(tables.values()));

        // Run the loader in a new Thread.
        new Thread(reverter).start();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!reverter.hasCompleted())
            {
                showProcessingView(translate("Reverting Changes..."));
            }

            // If we have to wait for completion of the process, do it now.
            while (!reverter.hasCompleted())
            {
                Thread.sleep(100);
            }
        }
        catch (InterruptedException e)
        {
            LOGGER.log("Exception while reverting changes.", e);
        }
    }

    @Override
    public boolean commitChanges(boolean waitForCompletion)
    {
        List<T8DataRecordTableChange> tableChanges;
        DataSaver saver;

        tableChanges = new ArrayList<>();
        for (DataRecordTable table : tables.values())
        {
            table.stopTableEditing();
            tableChanges.addAll(table.getTableDataChangeHandler().getTableChangeList());
        }

        // Create a new save operation handler.
        saver = new DataSaver(tableChanges);

        // Run the loader in a new Thread.
        new Thread(saver).start();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!saver.hasCompleted())
            {
                showProcessingView(translate("Saving Changes..."));
            }

            // If we have to wait for completion of the process, do it now.
            if (waitForCompletion)
            {
                while (!saver.hasCompleted())
                {
                    Thread.sleep(100);
                }
            }

            // If all went well, return true.
            return ((saver.hasCompleted()) && (saver.isSuccess()));
        }
        catch (InterruptedException e)
        {
            LOGGER.log("Exception while committing changes.", e);
            return false;
        }
    }

    public boolean validatePrefilteredRecords(boolean waitForCompletion)
    {
        return true;
    }

    @Override
    public void stopTableEditing()
    {
        synchronized (tables)
        {
            for (DataRecordTable table : tables.values())
            {
                table.stopTableEditing();
            }
        }
    }

    /**
     * Creates and returns a data filter that includes all of the prefilters set
     * on the table.
     *
     * @return The combined prefilter set on the table.
     */
    public T8DataFilter getCombinedPrefilter(String drInstanceID)
    {
        T8DataFilterCriteria combinedFilterCriteria;
        T8DataFilter combinedFilter;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilterCriteria = new T8DataFilterCriteria();

        // Add all available and valid prefilters.
        for (T8DataFilter prefilter : prefilters.values())
        {
            // Only include filters that have valid filter criteria.
            if ((prefilter != null) && (prefilter.hasFilterCriteria()))
            {
                combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
            }
        }

        // Add a filter clause for retrieving only the specified DR Instance.
        combinedFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(definition.getDataRecordDocumentDEID() + EF_DR_INSTANCE_ID, DataFilterOperator.EQUAL, drInstanceID));

        // Create the combined filter from the filter criteria.
        combinedFilter = new T8DataFilter(definition.getDataRecordDocumentDEID(), combinedFilterCriteria);

        // Return a new filter created from the combined criteria.
        return combinedFilter;
    }

    @Override
    public DataRecord changeDocumentDRInstance(String recordID, String drInstanceID)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addDocument(DataRecord dataRecord)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void unlockUI()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void lockUI(String message)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void commitChanges()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Object> getSelectedClassification(String osID, List<String> ocIDList)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Object> getSelectedValue(DataRecord record, ValueRequirement valueRequirement, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getProjectId()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class TableChangeListener implements TableModelListener
    {
        @Override
        public void tableChanged(TableModelEvent e)
        {
            for (final DataRecordTable table : tables.values())
            {
                if (table.getTableModelHandler().getTableModel() == e.getSource())
                {
                    SwingUtilities.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            navigationModel.nodeChanged(table.getDataRequirementInstanceIdentifier());
                        }
                    });
                }
            }
        }
    }

    private class TableSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!e.getValueIsAdjusting())
            {
                for (final DataRecordTable table : tables.values())
                {
                    if (table.getTableSelectionModel() == e.getSource())
                    {
                        HashMap<String, Object> eventParameters;
                        List<DataRecord> selectedRecords;

                        selectedRecords = table.getSelectedDataRecords();
                        if (selectedRecords.size() == 1)
                        {
                            DataRecord parentRecord;

                            // Find all child tables of the selected table, and set the parent record on the child tables.
                            parentRecord = selectedRecords.get(0);

                            // Set the selected parent on all the child tables.
                            for (DataRecordTable childTable : getChildTables(table.getDataRequirementInstanceIdentifier()))
                            {
                                childTable.setParentDataRecord(parentRecord);
                            }
                        }
                        else
                        {
                            // Set the selected parent on all the child tables.
                            for (DataRecordTable childTable : getChildTables(table.getDataRequirementInstanceIdentifier()))
                            {
                                childTable.setParentDataRecord(null);
                            }
                        }

                        // Report the component event.
                        eventParameters = new HashMap<>();
                        eventParameters.put(T8DataRecordTableAPIHandler.PARAMETER_DATA_RECORD_LIST, selectedRecords);
                        controller.reportEvent(T8DataRecordTable.this, definition.getComponentEventDefinition(T8DataRecordTableAPIHandler.EVENT_ROW_SELECTION_CHANGED), eventParameters);
                    }
                }
            }
        }
    }

    private class DataLoader implements Runnable
    {
        private final boolean completed = false;
        private final boolean refreshDescendants;
        private final DataRecordTable targetTable;

        private DataLoader(DataRecordTable table, boolean refreshDescendants)
        {
            this.targetTable = table;
            this.refreshDescendants = refreshDescendants;
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            LinkedList<DataRecordTable> tableQueue;

            // Refresh the target table and all of its descendants if required.
            tableQueue = new LinkedList<>();
            tableQueue.add(targetTable);
            while (tableQueue.size() > 0)
            {
                DataRecordTable nextTable;

                nextTable = tableQueue.pop();
                nextTable.refreshDataUnthreaded();
                if (refreshDescendants) tableQueue.addAll(getChildTables(nextTable.getDataRequirementInstanceIdentifier()));
            }

            // Show the loaded data.
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    // Show the loaded data.
                    showDataView();
                    navigationModel.nodesChanged();
                }
            });
        }
    }

    private class ChangeReverter extends Thread
    {
        private boolean completed = false;
        private final List<DataRecordTable> tables;

        private ChangeReverter(List<DataRecordTable> tables)
        {
            this.tables = tables;
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            completed = false;

            // Refresh data in all of the supplied tables.
            for (DataRecordTable table : tables)
            {
                table.refreshDataUnthreaded();
            }

            // Set the completed flag and switch back to the main card.
            completed = true;

            // Switch back to data view.
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    // Show the loaded data.
                    showDataView();
                    navigationModel.nodesChanged();
                }
            });
        }
    }

    private class DataSaver extends Thread
    {
        private final List<T8DataRecordTableChange> dataRecordChanges;
        private boolean completed = false;
        private boolean valid;
        private boolean success;

        private DataSaver(List<T8DataRecordTableChange> dataRecordChanges)
        {
            this.dataRecordChanges = dataRecordChanges;
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        public boolean isValid()
        {
            return valid;
        }

        public boolean isSuccess()
        {
            return success;
        }

        @Override
        public void run()
        {
            completed = false;
            valid = true;
            success = true;

            try
            {
                // Save all open documents.
                dataHandler.commitTableChanges(dataRecordChanges);

                // Clear changes.
                for (DataRecordTable table : tables.values())
                {
                    table.getTableDataChangeHandler().clearChanges();
                }

                // Clear validation errors.
                setValidationErrors(null);
            }
            catch (T8DataRecordValidationException e)
            {
                // Handle the record validation exception.
                success = false;
                valid = false;
                setValidationErrors(e.getValidationReport().getValidationErrors());
            }
            catch (Throwable e)
            {
                success = false;
                LOGGER.log(e);
                T8OptionPane.showMessageDialog(T8DataRecordTable.this, translate("The documents could not be saved successfully."), translate("Status"), T8OptionPane.ERROR_MESSAGE, getController());
            }

            // Set the completed flag and switch back to the main card.
            completed = true;

            // Switch back to data view.
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    // Show the loaded data.
                    showDataView();
                    navigationModel.nodesChanged();
                }
            });

            // Check for validation errors.
            if (!valid)
            {
                JOptionPane.showMessageDialog(T8DataRecordTable.this, translate("Please correct invalid data.") + "\n" + translate("You can view validation errors by hovering the mouse over the indicated row headers."), translate("Invalid Content"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class DataValidator extends Thread
    {
        private final List<String> validationScriptIdentifiers;
        private final T8DataFilter dataFilter;
        private boolean completed = false;
        private boolean valid;
        private boolean success;

        private DataValidator(T8DataFilter dataFilter, List<String> validationScriptIdentifiers)
        {
            this.dataFilter = dataFilter;
            this.validationScriptIdentifiers = validationScriptIdentifiers;
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        public boolean isValid()
        {
            return valid;
        }

        public boolean isSuccess()
        {
            return success;
        }

        @Override
        public void run()
        {
            completed = false;
            valid = true;
            success = true;

            try
            {
                // Save all open documents.
                dataHandler.validateFilteredRecords(dataFilter, validationScriptIdentifiers);

                // Clear validation errors.
                setValidationErrors(null);
            }
            catch (T8DataRecordValidationException e)
            {
                // Handle the record validation exception.
                success = false;
                valid = false;
                setValidationErrors(e.getValidationReport().getValidationErrors());
            }
            catch (Throwable e)
            {
                success = false;
                LOGGER.log(e);
                T8OptionPane.showMessageDialog(T8DataRecordTable.this, translate("The documents could not be saved successfully."), translate("Status"), T8OptionPane.ERROR_MESSAGE, getController());
            }

            // Set the completed flag and switch back to the main card.
            completed = true;

            // Switch back to data view.
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    // Show the loaded data.
                    showDataView();
                    navigationModel.nodesChanged();
                }
            });

            // Check for validation errors.
            if (!valid)
            {
                JOptionPane.showMessageDialog(T8DataRecordTable.this, translate("Please correct invalid data.") + "\n" + translate("You can view validation errors by hovering the mouse over the indicated row headers."), translate("Invalid Content"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanelProcessingView = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.CardLayout());

        jPanelProcessingView.setLayout(new java.awt.BorderLayout());
        add(jPanelProcessingView, "PROCESSING_VIEW");
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelProcessingView;
    // End of variables declaration//GEN-END:variables
}
