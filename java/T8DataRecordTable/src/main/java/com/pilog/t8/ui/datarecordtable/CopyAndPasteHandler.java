package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.utilities.serialization.SerializationUtilities;

/**
 * @author Bouwer du Preez
 */
public class CopyAndPasteHandler
{
    private static RecordProperty copiedProperty;
    private static RecordProperty cutProperty;

    public static boolean copyProperty(RecordProperty propertyValue)
    {
        try
        {
            copiedProperty = (RecordProperty)SerializationUtilities.copyObjectBySerialization(propertyValue);
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            copiedProperty = null;
            return false;
        }
    }
    
    public static boolean cutProperty(RecordProperty requirement)
    {
        try
        {
            cutProperty = requirement;
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            cutProperty = null;
            return false;
        }
    }

    public static RecordProperty getCopiedProperty()
    {
        RecordProperty returnProperty;

        returnProperty = copiedProperty;
        copiedProperty = null;
        return returnProperty;
    }
    
    public static RecordProperty getCutProperty()
    {
        RecordProperty returnProperty;

        returnProperty = cutProperty;
        cutProperty = null;
        return returnProperty;
    }

    public static boolean hasCopiedProperty()
    {
        return copiedProperty != null;
    }
    
    public static boolean hasCutProperty()
    {
        return cutProperty != null;
    }
}
