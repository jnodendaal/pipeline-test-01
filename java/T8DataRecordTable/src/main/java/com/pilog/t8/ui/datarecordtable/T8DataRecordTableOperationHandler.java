package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableOperationHandler extends T8ComponentOperationHandler
{
    private T8DataRecordTable table;

    public T8DataRecordTableOperationHandler(T8DataRecordTable table)
    {
        super(table);
        this.table = table;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_GET_COMBINED_FILTER))
        {
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_DATA_FILTER, table.getCombinedFilter());
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_GET_USER_DEFINED_FILTER))
        {
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_DATA_FILTER, table.getUserDefinedFilter());
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_GET_PREFILTER))
        {
            String filterIdentifier;

            filterIdentifier = (String)operationParameters.get(T8DataRecordTableAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_DATA_FILTER, table.getPrefilter(filterIdentifier));
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_SET_PREFILTER))
        {
            T8DataFilter dataFilter;
            String filterIdentifier;
            Boolean refresh;

            dataFilter = (T8DataFilter)operationParameters.get(T8DataRecordTableAPIHandler.PARAMETER_DATA_FILTER);
            refresh = (Boolean)operationParameters.get(T8DataRecordTableAPIHandler.PARAMETER_REFRESH_DATA);
            filterIdentifier = (String)operationParameters.get(T8DataRecordTableAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            if (refresh == null) refresh = true;
            
            table.setPrefilter(filterIdentifier, dataFilter);
            if (refresh) table.refreshData(null);
            return null;
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_GET_SELECTED_DATA_RECORDS))
        {
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_DATA_RECORD_LIST, table.getSelectedDataRecords());
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_GET_FIRST_SELECTED_DATA_RECORD))
        {
            List<DataRecord> selectedRecords;
            
            selectedRecords = table.getSelectedDataRecords();
            if ((selectedRecords != null) && (selectedRecords.size() > 0))
            {
                return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_DATA_RECORD, selectedRecords.get(0));
            }
            else return null;
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_REFRESH_DATA))
        {
            table.refreshData(null);
            return null;
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_HAS_UNSAVED_CHANGES))
        {
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_HAS_UNSAVED_CHANGES, table.hasUnsavedChanges());
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_SAVE_RECORD_CHANGES))
        {
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_SUCCESS, table.commitChanges(true));
        }
        else if (operationIdentifier.equals(T8DataRecordTableAPIHandler.OPERATION_VALIDATE_PREFILTERED_RECORDS))
        {
            return HashMaps.newHashMap(T8DataRecordTableAPIHandler.PARAMETER_SUCCESS, table.validatePrefilteredRecords(true));
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
