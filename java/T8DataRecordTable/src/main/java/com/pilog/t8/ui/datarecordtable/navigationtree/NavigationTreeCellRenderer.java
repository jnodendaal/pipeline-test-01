package com.pilog.t8.ui.datarecordtable.navigationtree;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecordtable.DataChangeHandler;
import com.pilog.t8.ui.datarecordtable.DataRecordTable;
import com.pilog.t8.ui.datarecordtable.DataRecordTableContainer;
import com.pilog.t8.ui.datarecordtable.rowheader.TableRowHeaderCellRenderer;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Color;
import java.awt.Component;
import java.util.List;
import java.util.Map;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class NavigationTreeCellRenderer extends javax.swing.JPanel implements TreeCellRenderer
{
    private DataRecordTableContainer tableContainer;

    public NavigationTreeCellRenderer(DataRecordTableContainer tableContainer)
    {
        this.tableContainer = tableContainer;
        initComponents();
        this.jLabelIcon.setVisible(false);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        Map<String, List<T8DataValidationError>> validationErrors;
        DefaultMutableTreeNode treeNode;
        TerminologyProvider terminologyProvider;
        DataChangeHandler tableChangeHandler;
        T8DataFilter userFilter;
        DataRecordTable table;
        Color backgroundColor;
        String drInstanceTerm;
        String drInstanceID;
        int tableChangeCount;
        int updateCount;
        int insertionCount;
        int deletionCount;
        int validationErrorCount;

        treeNode = (DefaultMutableTreeNode)value;
        drInstanceID = (String)treeNode.getUserObject();
        table = tableContainer.getTable(drInstanceID);
        userFilter = table.getUserDefinedFilter();
        terminologyProvider = tableContainer.getTerminologyProvider();
        drInstanceTerm = terminologyProvider.getTerm(null, drInstanceID);
        tableChangeHandler = table.getTableDataChangeHandler();
        tableChangeCount = tableChangeHandler.getTableChangeCount();
        updateCount = tableChangeHandler.getTableUpdateCount();
        insertionCount = tableChangeHandler.getTableInsertCount();
        deletionCount = tableChangeHandler.getTableDeleteCount();
        validationErrors = table.getValidationErrors();
        validationErrorCount = validationErrors != null ? validationErrors.size() : 0;
        
        // Determine the background color of the document.
        if (validationErrorCount > 0)
        {
            backgroundColor = LAFConstants.INVALID_PINK;
        }
        else
        {
            if (selected)
            {
                backgroundColor = LAFConstants.SELECTION_ORANGE;
            }
            else if (tableChangeCount > 0)
            {
                backgroundColor = TableRowHeaderCellRenderer.ROW_UPDATED_COLOR;
            }
            else
            {
                backgroundColor = LAFConstants.PROPERTY_BLUE;
            }
        }
        
        jLabelNodeName.setText(drInstanceTerm);
        jLabelFiltered.setVisible(userFilter != null && userFilter.hasFilterCriteria());
        jLabelUpdates.setText(getUpdateString(updateCount, insertionCount, deletionCount));
        jLabelUpdates.setVisible(tableChangeCount > 0);
        jLabelValidationErrorCount.setText(getValidationErrorString(validationErrorCount));
        jLabelValidationErrorCount.setVisible(validationErrorCount > 0);
        jPanelMain.setBackground(backgroundColor);
        jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);

        doLayout();
        return this;
    }
    
    private String getUpdateString(int updateCount, int insertCount, int deleteCount)
    {
        StringBuffer updateString;
        
        updateString = new StringBuffer();
        
        if (updateCount > 0)
        {
            updateString.append(updateCount);
            updateString.append(" ");
            if (updateCount > 1) updateString.append(tableContainer.translate("Updates"));
            else updateString.append(tableContainer.translate("Update"));
        }
        
        if (insertCount > 0)
        {
            if (updateString.length() > 0) updateString.append(", ");
            updateString.append(insertCount);
            updateString.append(" ");
            if (insertCount > 1) updateString.append(tableContainer.translate("Insertions"));
            else updateString.append(tableContainer.translate("Insertion"));
        }
        
        if (deleteCount > 0)
        {
            if (updateString.length() > 0) updateString.append(", ");
            updateString.append(deleteCount);
            updateString.append(" ");
            if (deleteCount > 1) updateString.append(tableContainer.translate("Deletions"));
            else updateString.append(tableContainer.translate("Deletion"));
        }
        
        return updateString.toString();
    }
    
    private String getValidationErrorString(int errorCount)
    {
        if (errorCount > 1) return errorCount + " " + tableContainer.translate("Validation Errors");
        else return errorCount + " " + tableContainer.translate("Validation Error");
    }
    
    private String translate(String inputString)
    {
        return tableContainer.translate(inputString);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jLabelIcon = new javax.swing.JLabel();
        jLabelNodeName = new javax.swing.JLabel();
        jLabelFiltered = new javax.swing.JLabel();
        jLabelUpdates = new javax.swing.JLabel();
        jLabelValidationErrorCount = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelMain.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanelMain.setLayout(new java.awt.GridBagLayout());
        jPanelMain.add(jLabelIcon, new java.awt.GridBagConstraints());

        jLabelNodeName.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNodeName.setText("Node Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 5);
        jPanelMain.add(jLabelNodeName, gridBagConstraints);

        jLabelFiltered.setText(translate("Filtered"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelMain.add(jLabelFiltered, gridBagConstraints);

        jLabelUpdates.setText("0 Updates");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelMain.add(jLabelUpdates, gridBagConstraints);

        jLabelValidationErrorCount.setText("0 Validation Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 2, 5);
        jPanelMain.add(jLabelValidationErrorCount, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
        add(jPanelMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelFiltered;
    private javax.swing.JLabel jLabelIcon;
    private javax.swing.JLabel jLabelNodeName;
    private javax.swing.JLabel jLabelUpdates;
    private javax.swing.JLabel jLabelValidationErrorCount;
    private javax.swing.JPanel jPanelMain;
    // End of variables declaration//GEN-END:variables
}
