package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableChange;
import com.pilog.t8.ui.datarecordtable.rowheader.TableRowHeader;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;


/**
 * The T8DataRecordTable model holds the data record for each row in the first
 * column of the model.  This column is a system column and is never altered by
 * the user. When one of the table cells is updated, the change is then first
 * logged and then applied to the data record.  At all times the record in the
 * first column must contain the same property values as the table model row
 * that displays the values to the user.
 *
 * @author Bouwer du Preez
 */
public class DataChangeHandler
{
    private LinkedHashMap<String, T8DataRecordTableChange> tableInserts;
    private LinkedHashMap<String, T8DataRecordTableChange> tableUpdates;
    private LinkedHashMap<String, T8DataRecordTableChange> tableDeletes;
    private T8DataRecordValueStringGenerator valueStringGenerator;
    private DataRecordTableContainer tableContainer;
    private DataRecordTable recordTable;
    private Map<String, String> savedStateValueStrings;
    private TableChangeListener tableChangeListener;
    private ModelHandler modelHandler;
    private ViewHandler viewHandler;
    private DefaultTableModel tableModel;
    private JTable table;

    public DataChangeHandler(DataRecordTable recordTable, ModelHandler modelHandler, ViewHandler viewHandler, JTable table)
    {
        this.recordTable = recordTable;
        this.tableContainer = recordTable.getTableContainer();
        this.table = table;
        this.modelHandler = modelHandler;
        this.viewHandler = viewHandler;
        this.tableModel = (DefaultTableModel)table.getModel();
        this.tableInserts = new LinkedHashMap<String, T8DataRecordTableChange>();
        this.tableUpdates = new LinkedHashMap<String, T8DataRecordTableChange>();
        this.tableDeletes = new LinkedHashMap<String, T8DataRecordTableChange>();
        this.tableChangeListener = new TableChangeListener();
        this.savedStateValueStrings = new HashMap<String, String>();

        // Create the Value String Generator that will be used to generate the "save-state" Value Strings for records.
        this.valueStringGenerator = new T8DataRecordValueStringGenerator();
        this.valueStringGenerator.setIncludeSubDocumentReferences(true);
        this.valueStringGenerator.setIncludePropertyFFT(true);
        this.valueStringGenerator.setIncludeRecordFFT(true);
    }

    public void setModel(DefaultTableModel model)
    {
        // Remove listeners from old model reference if set.
        if (tableModel != null)
        {
            tableModel.removeTableModelListener(tableChangeListener);
        }

        // Update the local model reference.
        this.tableModel = model;

        // Add all listeners.
        this.tableModel.addTableModelListener(tableChangeListener);
    }

    public void clearChanges()
    {
        tableInserts.clear();
        tableUpdates.clear();
        tableDeletes.clear();

        refreshSavedStateValueStrings();
    }

    private void refreshSavedStateValueStrings()
    {
        savedStateValueStrings.clear();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            DataRecord rowRecord;

            rowRecord = modelHandler.getDataRecord(rowIndex);
            savedStateValueStrings.put(rowRecord.getID(), generateSavedStateValueString(rowRecord));
        }
    }

    public LinkedHashMap<String, T8DataRecordTableChange> getTableChanges()
    {
        LinkedHashMap<String, T8DataRecordTableChange> tableChanges;

        tableChanges = new LinkedHashMap<String, T8DataRecordTableChange>();
        tableChanges.putAll(tableInserts);
        tableChanges.putAll(tableUpdates);
        tableChanges.putAll(tableDeletes);

        return tableChanges;
    }

    public List<T8DataRecordTableChange> getTableChangeList()
    {
        List<T8DataRecordTableChange> tableChanges;

        // Combine all outstanding table changes in one list.
        tableChanges = new ArrayList<T8DataRecordTableChange>();
        tableChanges.addAll(tableUpdates.values());
        tableChanges.addAll(tableInserts.values());
        tableChanges.addAll(tableDeletes.values());

        return tableChanges;
    }

    public int getTableChangeCount()
    {
        return tableUpdates.size() + tableInserts.size() + tableDeletes.size();
    }

    public int getTableUpdateCount()
    {
        return tableUpdates.size();
    }

    public int getTableInsertCount()
    {
        return tableInserts.size();
    }

    public int getTableDeleteCount()
    {
        return tableDeletes.size();
    }

    private boolean deleteDataRecordReferencesFromParentRecord(DataRecord parentRecord, List<DataRecord> dataRecordList)
    {
        if (parentRecord != null)
        {
            DataRecordTable parentTable;

            // Get the parent table.
            parentTable = tableContainer.getParentTable(recordTable.getDataRequirementInstanceIdentifier());

            // We've gotta check all records for validity before removing any references.
            for (DataRecord dataRecord : dataRecordList)
            {
                List<RecordProperty> referenceProperties;

                // Fetch the property from the parent record that references the selected record.
                referenceProperties = parentRecord.getDataRecordReferenceProperties(dataRecord.getID());
                if (referenceProperties.isEmpty())
                {
                    // The selected record is not referenced from the selected parent.
                    JOptionPane.showMessageDialog(recordTable, tableContainer.translate("Only records belonging to the selected parent can be deleted."), tableContainer.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }

            // Now that we've checked all records and know that they all belong to the specified parent record, we can remove the references one by one.
            for (DataRecord dataRecord : dataRecordList)
            {
                List<RecordProperty> referenceProperties;

                // Fetch the property from the parent record that references the selected record.
                referenceProperties = parentRecord.getDataRecordReferenceProperties(dataRecord.getID());
                if (referenceProperties.size() > 0)
                {
                    RecordProperty referenceProperty;
                    RecordValue referenceValue;
                    RecordValue valueToRemove;

                    referenceProperty = referenceProperties.get(0);
                    referenceValue = referenceProperty.getRecordValue();
                    valueToRemove = referenceValue.getSubRecordValue(dataRecord.getID());
                    if (valueToRemove != null)
                    {
                        // Remove the value.
                        referenceValue.removeSubValue(valueToRemove);

                        // If no more references remain from this property, remove the entire property.
                        if (referenceValue.getSubValues().isEmpty())
                        {
                            parentRecord.removeRecordProperty(referenceProperty);
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(recordTable, tableContainer.translate("Only records belonging to the selected parent can be deleted."), tableContainer.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                        return false;
                    }
                }
                else // The selected record is not referenced from the selected parent.
                {
                    JOptionPane.showMessageDialog(recordTable, tableContainer.translate("Only records belonging to the selected parent can be deleted."), tableContainer.translate("Invalid Operation"), JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }

            // Update the parent record model to reflect the changes (if the parent table is available).
            if (parentTable != null) parentTable.getTableModelHandler().refreshRecordRow(parentRecord.getID());
            return true;
        }
        else return false;
    }

    private boolean addDataRecordReferencesToParentRecord(DataRecord parentRecord, List<DataRecord> dataRecordList)
    {
//        DataRecordTable parentTable;
//        String drInstanceID;
//
//        drInstanceID = recordTable.getDataRequirementInstanceIdentifier();
//        parentTable = tableContainer.getParentTable(drInstanceID);
//        if (parentRecord != null)
//        {
//            for (DataRecord dataRecord : dataRecordList)
//            {
//                List<RecordProperty> referenceProperties;
//                List<PropertyRequirement> referenceRequirements;
//                RecordProperty parentProperty;
//
//                referenceProperties = parentRecord.getDataRequirementReferenceProperties(drInstanceID);
//                referenceRequirements = parentRecord.getDataRequirement().getDataRequirementReferencePropertyRequirements(drInstanceID);
//
//                // If a property already exists that references the specified DR Instance, use it, otherwise add a new reference property to the parent record.
//                if (referenceProperties.size() > 0)
//                {
//                    RecordValue documentReferenceValue;
//                    ValueRequirement drInstanceRequirement;
//
//                    parentProperty = referenceProperties.get(0);
//                    drInstanceRequirement = parentProperty.getPropertyRequirement().getValueRequirement().getDescendentRequirement(RequirementType.DATA_REQUIREMENT_INSTANCE_REFERENCE_TYPE, drInstanceID, null);
//                    documentReferenceValue = parentProperty.getRecordValue();
//                    documentReferenceValue.setSubValue(new DocumentReference(drInstanceRequirement, dataRecord.getID()));
//                }
//                else
//                {
//                    if (referenceRequirements.size() == 1)
//                    {
//                        PropertyRequirement referenceRequirement;
//                        RecordValue documentReferenceValue;
//                        ValueRequirement drInstanceRequirement;
//
//                        // Get the reference property requirement and attempt to find the property in the parent record (create one if it doesn't exist).
//                        referenceRequirement = referenceRequirements.get(0);
//                        parentProperty = parentRecord.getRecordProperty(referenceRequirement.getConceptID());
//                        if (parentProperty == null)
//                        {
//                            // Create the new property since it does not exist.
//                            parentProperty = new RecordProperty(referenceRequirement);
//                            parentRecord.setRecordProperty(parentProperty);
//                            parentProperty.setRecordValue(new DocumentReferenceList(referenceRequirement.getValueRequirement()));
//                            documentReferenceValue = parentProperty.getRecordValue();
//                        }
//                        else
//                        {
//                            // Get the existing property value or add a new value if none exists.
//                            documentReferenceValue = parentProperty.getRecordValue();
//                            if (documentReferenceValue == null)
//                            {
//                                documentReferenceValue = new DocumentReferenceList(referenceRequirement.getValueRequirement());
//                                parentProperty.setRecordValue(documentReferenceValue);
//                            }
//                        }
//
//                        // Get the correct DR Instance reference requirement and add the sub-value to the Document Reference value.
//                        drInstanceRequirement = referenceRequirement.getValueRequirement().getDescendentRequirement(RequirementType.DATA_REQUIREMENT_INSTANCE_REFERENCE_TYPE, drInstanceID, null);
//                        documentReferenceValue.setSubValue(new DocumentReference(drInstanceRequirement, dataRecord.getID()));
//                    }
//                    else if (referenceRequirements.isEmpty()) throw new RuntimeException("No property requirements reference the sub-DR Instance: " + drInstanceID);
//                    else throw new RuntimeException("Multiple property requirements reference the same sub-DR Instance: " + drInstanceID);
//                }
//            }
//
//            // Update the parent record model to reflect the changes (if the parent table is available).
//            if (parentTable != null) parentTable.getTableModelHandler().refreshRecordRow(parentRecord.getID());
//            return true;
//        }
//        else return false;
        return false;
    }

    public void deleteSelectedRows(DataRecord parentRecord, List<DataRecord> recordsToDelete)
    {
        // Remove all references from the parent record to the selected data records.
        if (deleteDataRecordReferencesFromParentRecord(parentRecord, recordsToDelete))
        {
            for (DataRecord recordToDelete : recordsToDelete)
            {
                int rowIndex;

                rowIndex = modelHandler.getRowIndex(recordToDelete.getID());
                modelHandler.deleteRow(rowIndex);
                logRowDelete(getRowDataRecord(rowIndex));
            }
        }
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public DataRecord insertNewRecord(DataRecord parentRecord)
    {
        DataRequirementInstance drInstance;
        DataRecord newRowRecord;
        List<String> inheritedTagIdentifiers;
        int rowIndex;

        // Ensure that all editing has stopped.
        stopTableEditing();

        // Get the selected row index where the new row will be inserted.
        rowIndex = table.getSelectedRow();
        if (rowIndex == -1) rowIndex = 0;

        // Create the new record and add at least the class to it.
        drInstance = modelHandler.getDataRequirementInstance();
        newRowRecord = new DataRecord(drInstance);
        newRowRecord.setID(T8IdentifierUtilities.createNewGUID());

        // Add a reference to the parent record and if successful, add the new record to the table model.
        if (addDataRecordReferencesToParentRecord(parentRecord, ArrayLists.newArrayList(newRowRecord)))
        {
            modelHandler.insertRow(rowIndex, newRowRecord);
            logRowInsert(newRowRecord);

            return newRowRecord;
        }
        else return null;
    }

    public T8DataRecordTableChange getUpdateChange(String rowGUID)
    {
        return tableUpdates.get(rowGUID);
    }

    public boolean isRowUpdated(String rowGUID)
    {
        return tableUpdates.containsKey(rowGUID);
    }

    public boolean isRowInserted(String rowGUID)
    {
        return tableInserts.containsKey(rowGUID);
    }

    private void stopTableEditing()
    {
        // Ensure that all editing has stopped.
        if (table.getCellEditor() != null) table.getCellEditor().stopCellEditing();
    }

    private void printTableChanges()
    {
        System.out.println("\nTable Changes");
        System.out.println("==============");
        System.out.println("\nInserted Rows");
        for (T8DataRecordTableChange change : tableInserts.values())
        {
            System.out.println("" + change.getDataRecord().getID());
        }
        System.out.println("\nUpdated Rows");
        for (T8DataRecordTableChange change : tableUpdates.values())
        {
            System.out.println("" + change.getDataRecord().getID());
        }
        System.out.println("\nDeleted Rows");
        for (T8DataRecordTableChange change : tableDeletes.values())
        {
            System.out.println("" + change.getDataRecord().getID());
        }
    }

    private void logRowInsert(DataRecord rowRecord)
    {
        String recordID;

        recordID = rowRecord.getID();
        if (!tableInserts.containsKey(recordID))
        {
            tableInserts.put(recordID, new T8DataRecordTableChange(T8DataRecordTableChange.CHANGE_TYPE_INSERT, rowRecord));
        }
        else throw new RuntimeException("Duplicate system row GUID found during row insertion.");

        printTableChanges();
    }

    private void logRowUpdate(DataRecord rowRecord, int rowIndex, String propertyID)
    {
        String recordID;

        recordID = rowRecord.getID();
        if (tableInserts.containsKey(recordID)) // The row being updated was previously inserted.
        {
            // Nothing to do here yet.
        }
        else if (tableUpdates.containsKey(recordID)) // The row being updated was previously updated.
        {
            String updatedRecordValueString;
            String savedStateValueString;

            // Check for table changes that are reverted (changed back to original value).
            updatedRecordValueString = generateSavedStateValueString(rowRecord);
            savedStateValueString = getSavedStateValueString(recordID);
            if (Objects.equals(updatedRecordValueString, savedStateValueString))
            {
                // The record has been updated in such a way that it matches the original "saved state" of the record.
                tableUpdates.remove(recordID);
            }
        }
        else // The row being updated is now newly altered.
        {
            String updatedRecordValueString;
            String savedStateValueString;

            // Check for table changes that are reverted (changed back to original value).
            updatedRecordValueString = generateSavedStateValueString(rowRecord);
            savedStateValueString = getSavedStateValueString(recordID);
            if (!Objects.equals(updatedRecordValueString, savedStateValueString))
            {
                tableUpdates.put(recordID, new T8DataRecordTableChange(T8DataRecordTableChange.CHANGE_TYPE_UPDATE, rowRecord));
            }
            else
            {
                // No real value change, just a cell update.  End the method.
                return;
            }
        }

        // Print the table changes.
        printTableChanges();

        // Validate the row in a separate thread.
        new Thread(new RowValidation(getRowHeader(rowIndex))).start();
    }

    private void logRowDelete(DataRecord dataRecord)
    {
        String recordID;

        recordID = dataRecord.getID();
        if (tableInserts.containsKey(recordID)) // The row being deleted was previously inserted.
        {
            tableInserts.remove(recordID);
        }
        else if (tableUpdates.containsKey(recordID)) // The row being deleted was previously updated.
        {
            tableUpdates.remove(recordID);
            tableDeletes.put(recordID, new T8DataRecordTableChange(T8DataRecordTableChange.CHANGE_TYPE_DELETE, dataRecord));
        }
        else // The row being deleted has not been previously inserted or updated.
        {
            tableDeletes.put(recordID, new T8DataRecordTableChange(T8DataRecordTableChange.CHANGE_TYPE_DELETE, dataRecord));
        }

        printTableChanges();
    }

    private TableRowHeader getRowHeader(int rowIndex)
    {
        return (TableRowHeader)tableModel.getValueAt(rowIndex, 0);
    }

    private DataRecord getRowDataRecord(int rowIndex)
    {
        return ((TableRowHeader)tableModel.getValueAt(rowIndex, 0)).getDataRecord();
    }

    public String getSavedStateValueString(String recordID)
    {
        return savedStateValueStrings.get(recordID);
    }

    public String generateSavedStateValueString(DataRecord record)
    {
        return valueStringGenerator.generateValueString(record).toString();
    }

    private class TableChangeListener implements TableModelListener
    {
        @Override
        public void tableChanged(TableModelEvent e)
        {
            int changeType;

            changeType = e.getType();
            if (changeType == TableModelEvent.UPDATE)
            {
                for (int rowIndex = e.getFirstRow(); rowIndex <= e.getLastRow(); rowIndex++)
                {
                    DataRecord rowRecord;
                    RecordProperty newProperty;
                    String propertyID;

                    // Get the edited row record.
                    rowRecord = getRowDataRecord(rowIndex);

                    // Get the changed section and property ID.
                    propertyID = modelHandler.getPropertyRequirement(e.getColumn()).getConceptID();

                    // Now apply the change on the model to the row record.
                    newProperty = (RecordProperty)tableModel.getValueAt(rowIndex, e.getColumn());
                    if (newProperty == null)
                    {
                        rowRecord.removeRecordProperty(propertyID);
                    }
                    else
                    {
                        rowRecord.setRecordProperty(newProperty);
                    }

                    // Now log the update as a change.
                    logRowUpdate(rowRecord, rowIndex, propertyID);
                }
            }

            // Repaint row headers to allow conditional rendering to take effect.
            viewHandler.repaintRowHeaders();
        }
    }

    private class RowValidation implements Runnable
    {
        private List<TableRowHeader> rowHeaderList;
        private List<DataRecord> recordList;

        public RowValidation(List<TableRowHeader> rowHeaders)
        {
            this.rowHeaderList = rowHeaders;
            this.recordList = new ArrayList<DataRecord>();

            // Create a list of records with indices corresponding to the list of row headers.
            for (TableRowHeader rowHeader : rowHeaderList)
            {
                recordList.add(rowHeader.getDataRecord());
            }
        }

        public RowValidation(TableRowHeader rowHeader)
        {
            this(Arrays.asList(rowHeader));
        }

        @Override
        public void run()
        {
//            T8ClientEntityValidationScriptDefinition clientValidationScriptDefinition;
//            T8ServerEntityValidationScriptDefinition serverValidationScriptDefinition;
//
//            // Do the client-side validation of the rows.
//            clientValidationScriptDefinition = tableDefinition.getClientValidationScriptDefinition();
//            if (clientValidationScriptDefinition != null)
//            {
//                T8ClientScriptRunner scriptRunner;
//
//                scriptRunner = new T8ClientScriptRunner(recordTable.getController());
//
//                try
//                {
//                    scriptRunner.prepareScript(clientValidationScriptDefinition);
//                    for (TableRowHeader rowHeader : rowHeaderList)
//                    {
//                        Map<String, Object> scriptOutputParameters;
//                        T8DataEntity rowEntity;
//                        Boolean validationSuccess;
//
//                        rowEntity = rowHeader.getDataRecord();
//                        scriptOutputParameters = scriptRunner.runScript(rowEntity.getFieldValues());
//                        validationSuccess = (Boolean)scriptOutputParameters.get(clientValidationScriptDefinition.getNamespace() + T8EntityValidationScriptDefinition.PARAMETER_VALIDATION_SUCCESS);
//                        if (validationSuccess != null)
//                        {
//                            if (!validationSuccess)
//                            {
//                                List<T8DataEntityValidationError> validationErrors;
//
//                                validationErrors = (List<T8DataEntityValidationError>)scriptOutputParameters.get(clientValidationScriptDefinition.getNamespace() + T8EntityValidationScriptDefinition.PARAMETER_VALIDATION_ERROR_LIST);
//                                if ((validationErrors != null) || (validationErrors.size() == 0))
//                                {
//                                    rowHeader.setValidationErrors(validationErrors);
//                                }
//                                else throw new Exception("Validation script '" + clientValidationScriptDefinition + "' reported validation failure but did not return validation errors: " + scriptOutputParameters);
//                            }
//                            else
//                            {
//                                // Clear any existing validation errors.
//                                rowHeader.setValidationErrors(null);
//                            }
//                        }
//                        else throw new Exception("Validation script '" + clientValidationScriptDefinition + "' did not return valid success parameter: " + scriptOutputParameters);
//                    }
//                }
//                catch (Exception e)
//                {
//                    T8Log.log("Exeption while executing client-side entity validation.", e);
//                }
//                finally
//                {
//                    scriptRunner.finalizeScript();
//                }
//            }
//
//            // Do the server-side validation of the rows.
//            serverValidationScriptDefinition = tableDefinition.getServerValidationScriptDefinition();
//            if (serverValidationScriptDefinition != null)
//            {
//                try
//                {
//                    List<T8DataEntityValidationReport> validationReportList;
//
//                    validationReportList = T8DataManagerOperationHandler.validateDataEntities(recordTable.getSessionContext(), recordList, serverValidationScriptDefinition);
//                    for (int validationIndex = 0; validationIndex < rowHeaderList.size(); validationIndex++)
//                    {
//                        TableRowHeader rowHeader;
//                        T8DataEntityValidationReport validationReport;
//
//                        rowHeader = rowHeaderList.get(validationIndex);
//                        validationReport = validationReportList.get(validationIndex);
//                        if (!validationReport.isValidationSuccess())
//                        {
//                            List<T8DataEntityValidationError> validationErrors;
//
//                            validationErrors = validationReport.getValidationErrors();
//                            if ((validationErrors != null) || (validationErrors.size() == 0))
//                            {
//                                rowHeader.addValidationErrors(validationErrors);
//                            }
//                            else throw new Exception("Validation script '" + serverValidationScriptDefinition + "' reported validation failure but did not return validation errors: " + validationReport);
//                        }
//                    }
//                }
//                catch (Exception e)
//                {
//                    T8Log.log("Exeption while executing server-side entity validation.", e);
//                }
//            }
//
//            // Repaint the table row headers to allow any conditional rendering to take place after validation.
//            recordTable.getTableViewHandler().repaintRowHeaders();
        }
    }
}