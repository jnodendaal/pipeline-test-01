package com.pilog.t8.ui.datarecordtable.component;

import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DefaultRecordValueEditor;
import com.pilog.t8.ui.datarecordtable.DataRecordTable;
import com.pilog.t8.ui.datarecordtable.DataRecordTableContainer;
import java.awt.Component;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceEditorComponent extends DefaultRecordValueEditor
{
    private final ValueRequirement ontologyClassRequirement;
    private final DataRecordTableContainer tableContainer;
    private final CachedTerminologyProvider terminologyProvider;
    private boolean editable;

    public DocumentReferenceEditorComponent(DataRecordTableContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.tableContainer = container;
        this.terminologyProvider = tableContainer.getTerminologyProvider();
        ontologyClassRequirement = valueRequirement.getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        initComponents();
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshEditor()
    {
        List<RecordValue> subValues;

        // Set the UI value.
        subValues = recordValue.getSubValues();
        jLabelReferencedDocuments.setText(subValues.size() + " " + translate("Referenced Documents"));
        jButtonOpenSubTable.setVisible(subValues.size() > 0);
    }

    @Override
    public boolean commitChanges()
    {
        // We don't have to do anything because the changed made by this editor
        // are committed as they are made.
        return true;
    }

    @Override
    public List<T8DataValidationError> getValidationErrors()
    {
        return null;
    }

    private void openSubTable(Component source, int x, int y)
    {
//        JPopupMenu popupMenu;
//        JMenuItem menuItem;
//        ArrayList<String> drInstanceIDs;
//        HashMap<String, String> drInstanceTerms;
//
//        // Create a list of all Data Requirements that my be chosen from.
//        drInstanceIDs = new ArrayList<String>(drInstanceReferenceRequirements.keySet());
//
//        // Get the titles of all the possible Data Requirements from the cache.
//        terminologyProvider.cacheTerminology(ArrayLists.newArrayList(context.getContentLanguageIdentifier()), drInstanceIDs);
//        drInstanceTerms = new HashMap<String, String>();
//        for (String drInstanceID : drInstanceIDs)
//        {
//            T8ConceptTerminology terminology;
//            String term;
//
//            terminology = terminologyProvider.getTerminology(null, drInstanceID);
//            term = terminology != null ? terminology.getTerm() : null;
//            drInstanceTerms.put(drInstanceID, term != null ? term : drInstanceID);
//        }
//
//        // Create the ActionListener for the menu items.
//        ActionListener menuListener = new ActionListener()
//        {
//            @Override
//            public void actionPerformed(ActionEvent event)
//            {
//                String optionText;
//
//                optionText = ((JMenuItem)event.getSource()).getName();
//                for (ValueRequirement drInstanceRequirement : drInstanceReferenceRequirements.values())
//                {
//                    if (drInstanceRequirement.getValue().equals(optionText))
//                    {
//                        DataRecord parentRecord;
//                        String drInstanceID;
//
//                        // Determine the parent record (the record to which the value of this editor belongs) that will be set on the child table.
//                        parentRecord = recordValue != null ? recordValue.getParentDataRecord() : null;
//
//                        drInstanceID = drInstanceRequirement.getValue();
//                        if (!tableContainer.containsTable(drInstanceID))
//                        {
//                            DataRecordTable newTable;
//
//                            newTable = tableContainer.addTable(drInstanceID);
//                            tableContainer.showTable(drInstanceID);
//                            newTable.refreshModel();
//                            newTable.refreshData();
//                            newTable.setParentDataRecord(parentRecord);
//                        }
//                        else
//                        {
//                            DataRecordTable existingTable;
//
//                            existingTable = tableContainer.getTable(drInstanceID);
//                            tableContainer.showTable(drInstanceID);
//                            existingTable.refreshData();
//                            existingTable.setParentDataRecord(parentRecord);
//                        }
//                    }
//                }
//            }
//        };
//
//        // Create a new popup menu.
//        popupMenu = new JPopupMenu();
//
//        for (String drID : drInstanceReferenceRequirements.keySet())
//        {
//            menuItem = new JMenuItem(drInstanceTerms.get(drID));
//            menuItem.setName(drID);
//            menuItem.addActionListener(menuListener);
//            popupMenu.add(menuItem);
//        }
//
//        // Display the newly constructed popup menu.
//        popupMenu.show(source, x, y);
    }

    private void addNewSubRecord(Component source, int x, int y)
    {
//        JPopupMenu popupMenu;
//        JMenuItem menuItem;
//        ArrayList<String> drInstanceIDs;
//        HashMap<String, String> drInstanceTerms;
//
//        // Create a list of all Data Requirements that my be chosen from.
//        drInstanceIDs = new ArrayList<String>(drInstanceReferenceRequirements.keySet());
//
//        // Get the titles of all the possible Data Requirements from the cache.
//        terminologyProvider.cacheTerminology(ArrayLists.newArrayList(context.getContentLanguageIdentifier()), drInstanceIDs);
//        drInstanceTerms = new HashMap<String, String>();
//        for (String drInstanceID : drInstanceIDs)
//        {
//            T8ConceptTerminology terminology;
//            String term;
//
//            terminology = terminologyProvider.getTerminology(null, drInstanceID);
//            term = terminology != null ? terminology.getTerm() : null;
//            drInstanceTerms.put(drInstanceID, term != null ? term : drInstanceID);
//        }
//
//        // Create the ActionListener for the menu items.
//        ActionListener menuListener = new ActionListener()
//        {
//            @Override
//            public void actionPerformed(ActionEvent event)
//            {
//                String optionText;
//
//                optionText = ((JMenuItem)event.getSource()).getName();
//                for (ValueRequirement drRequirement : drInstanceReferenceRequirements.values())
//                {
//                    if (drRequirement.getValue().equals(optionText))
//                    {
//                        addSubRecord(drRequirement);
//                    }
//                }
//            }
//        };
//
//        // Create a new popup menu.
//        popupMenu = new JPopupMenu();
//
//        for (String drID : drInstanceReferenceRequirements.keySet())
//        {
//            menuItem = new JMenuItem(drInstanceTerms.get(drID));
//            menuItem.setName(drID);
//            menuItem.addActionListener(menuListener);
//            popupMenu.add(menuItem);
//        }
//
//        // Display the newly constructed popup menu.
//        popupMenu.show(source, x, y);
    }

    private void addSubRecord(ValueRequirement drInstanceReferenceRequirement)
    {
        try
        {
            DataRecord parentRecord;
            DataRecord newRecord;
            String drInstanceID;

            parentRecord = getParentDataRecord();
            drInstanceID = drInstanceReferenceRequirement.getValue();
            if (!tableContainer.containsTable(drInstanceID))
            {
                DataRecordTable newTable;

                newTable = tableContainer.addTable(drInstanceID);
                tableContainer.showTable(drInstanceID);
                newTable.refreshModelUnthreaded();
                newTable.refreshDataUnthreaded();
                newTable.setParentDataRecord(parentRecord);
                newRecord = newTable.insertNewRecord();
            }
            else
            {
                DataRecordTable existingTable;

                existingTable = tableContainer.getTable(drInstanceID);
                tableContainer.showTable(drInstanceID);
                existingTable.setParentDataRecord(parentRecord);
                newRecord = existingTable.insertNewRecord();
            }

            // If the new record was successfully inserted in the child table, add the reference to this editor's value.
            if (newRecord != null)
            {
                // If no value existed for this editor (a null value), get the newly created record value from the parent record.
                if (recordValue == null)
                {
                    String propertyID;

                    propertyID = valueRequirement.getParentPropertyRequirement().getConceptID();
                    recordValue = parentRecord.getRecordValue(propertyID);
                }

                // Refresh the editor display.
                jLabelReferencedDocuments.setText(recordValue.getSubValues().size() + translate(" Referenced Documents"));
                jButtonOpenSubTable.setVisible(recordValue.getSubValues().size() > 0);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private DataRecord getParentDataRecord()
    {
        DataRecordTable table;
        List<DataRecord> selectedRecords;

        table = tableContainer.getTable(valueRequirement.getParentDataRequirementInstance().getConceptID());
        selectedRecords = table.getSelectedDataRecords();
        if (selectedRecords.size() == 1)
        {
            return selectedRecords.get(0);
        }
        else
        {
            // We have a problem.  According to the way the table selection is set, only the row being edited (which is the one this editor belongs to) should be selected.
            throw new RuntimeException("Invalid row selection encountered.");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelReferencedDocuments = new javax.swing.JLabel();
        jToolBarReferenceControls = new javax.swing.JToolBar();
        jButtonOpenSubTable = new javax.swing.JButton();
        jButtonAddSubRecord = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jLabelReferencedDocuments.setText("0 Referenced Documents");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(jLabelReferencedDocuments, gridBagConstraints);

        jToolBarReferenceControls.setFloatable(false);
        jToolBarReferenceControls.setRollover(true);

        jButtonOpenSubTable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordtable/icons/gotoTableIcon.png"))); // NOI18N
        jButtonOpenSubTable.setText(translate("Open..."));
        jButtonOpenSubTable.setToolTipText(translate("Navigate to one of the documents referenced."));
        jButtonOpenSubTable.setFocusable(false);
        jButtonOpenSubTable.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonOpenSubTable.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonOpenSubTableActionPerformed(evt);
            }
        });
        jToolBarReferenceControls.add(jButtonOpenSubTable);

        jButtonAddSubRecord.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordtable/icons/addTableIcon.png"))); // NOI18N
        jButtonAddSubRecord.setText(translate("Add..."));
        jButtonAddSubRecord.setToolTipText(translate("Add a new sub-record."));
        jButtonAddSubRecord.setFocusable(false);
        jButtonAddSubRecord.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddSubRecord.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddSubRecordActionPerformed(evt);
            }
        });
        jToolBarReferenceControls.add(jButtonAddSubRecord);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarReferenceControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOpenSubTableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonOpenSubTableActionPerformed
    {//GEN-HEADEREND:event_jButtonOpenSubTableActionPerformed
        openSubTable(jButtonOpenSubTable, 0, jButtonOpenSubTable.getHeight()-1);
    }//GEN-LAST:event_jButtonOpenSubTableActionPerformed

    private void jButtonAddSubRecordActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddSubRecordActionPerformed
    {//GEN-HEADEREND:event_jButtonAddSubRecordActionPerformed
        addNewSubRecord(jButtonAddSubRecord, 0, jButtonAddSubRecord.getHeight()-1);
    }//GEN-LAST:event_jButtonAddSubRecordActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddSubRecord;
    private javax.swing.JButton jButtonOpenSubTable;
    private javax.swing.JLabel jLabelReferencedDocuments;
    private javax.swing.JToolBar jToolBarReferenceControls;
    // End of variables declaration//GEN-END:variables
}
