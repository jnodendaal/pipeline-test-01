package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableModel extends DefaultTableModel
{
    private T8DataRecordTableDefinition tableDefinition;
    
    public T8DataRecordTableModel(T8DataRecordTableDefinition tableDefinition)
    {
        this.tableDefinition = tableDefinition;
    }
    
    @Override
    public boolean isCellEditable(int row, int column)
    {
        if (column == 0)
        {
            // The row header is at column index 0 and is not editable.
            return false;
        }
        else
        {
            // TODO:  Check the data record access definition to determine editability.
            return true;
        }
    }
}