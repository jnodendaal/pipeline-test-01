package com.pilog.t8.ui.datarecordtable.rowheader;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class TableRowHeader
{
    private int rowIndex;
    private DataRecord dataRecord;
    private List<T8DataValidationError> validationErrors;
    
    public TableRowHeader(DataRecord dataRecord, int rowIndex)
    {
        this.dataRecord = dataRecord;
        this.rowIndex = rowIndex;
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public void setDataRecord(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
    }

    public int getRowIndex()
    {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex)
    {
        this.rowIndex = rowIndex;
    }

    public List<T8DataValidationError> getValidationErrors()
    {
        return validationErrors;
    }

    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        this.validationErrors = validationErrors;
    }
    
    public void addValidationErrors(List<T8DataValidationError> validationErrors)
    {
        if (this.validationErrors == null)
        {
            this.validationErrors = validationErrors;
        }
        else this.validationErrors.addAll(validationErrors);
    }
}
