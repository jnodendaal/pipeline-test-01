package com.pilog.t8.ui.datarecordtable;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementInstanceDataSourceDefinition;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class DataHandler
{
    private T8DataRecordTableDefinition tableDefinition;
    private T8Context context;
    private int pageOffset;
    private int pageSize;

    public DataHandler(T8Context context, T8DataRecordTableDefinition tableDefinition)
    {
        this.context = context;
        this.tableDefinition = tableDefinition;
        this.pageOffset = 0;
        this.pageSize = tableDefinition.getPageSize();
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void resetPageOffset()
    {
        pageOffset = 0;
    }

    public int getPageOffset()
    {
        return pageOffset;
    }

    public void setPageOffset(int offset)
    {
        pageOffset = offset;
    }

    public int getPageNumber()
    {
        return (pageOffset/pageSize) + 1;
    }

    public void incrementPage()
    {
        pageOffset += pageSize;
    }

    public void decrementPage()
    {
        if (pageOffset > pageSize)
        {
            pageOffset -= pageSize;
        }
        else pageOffset = 0;
    }

    public DataRequirementInstance loadDataRequirementInstance(String drInstanceID) throws Exception
    {
        Map<String, Object> keyMap;
        T8DataEntity requirementEntity;
        String drInstanceDocumentDEID;
        String keyFieldIdentifier;
        String documentFieldIdentifier;

        drInstanceDocumentDEID = tableDefinition.getDataRequirementInstanceDocumentDEID();
        keyFieldIdentifier = drInstanceDocumentDEID + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER;
        documentFieldIdentifier = drInstanceDocumentDEID + T8DataRequirementInstanceDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER;

        keyMap = new HashMap<String, Object>();
        keyMap.put(keyFieldIdentifier, drInstanceID);

        requirementEntity = T8DataManagerOperationHandler.retrieveDataEntity(context, drInstanceDocumentDEID, keyMap);
        if (requirementEntity != null)
        {
            return (DataRequirementInstance)requirementEntity.getFieldValue(documentFieldIdentifier);
        }
        else return null;
    }

    public int countDataRecords(T8DataFilter dataFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER, tableDefinition.getDataRecordDocumentDEID());
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);

        return (Integer)T8MainServerClient.executeSynchronousOperation(context, SERVER_OPERATION_COUNT_DATA_RECORDS, operationParameters).get(PARAMETER_DATA_RECORD_COUNT);
    }

    public List<DataRecord> loadDataRecords(List<String> parentRecordIDList, T8DataFilter dataFilter, CachedTerminologyProvider terminologyProvider) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> outputParameters;
        List<T8ConceptTerminology> terminology;
        List<DataRecord> retrievedDataRecords;

        // Create the operation input parameters.
        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER, tableDefinition.getDataRecordDocumentDEID());
        operationParameters.put(PARAMETER_DATA_RECORD_STRUCTURE_ENTITY_IDENTIFIER, tableDefinition.getDataRecordStructureDEID());
        operationParameters.put(PARAMETER_TERMINOLOGY_ENTITY_IDENTIFIER, tableDefinition.getTerminologyDEID());
        operationParameters.put(PARAMETER_LANGUAGE_ID, context.getSessionContext().getContentLanguageIdentifier());
        operationParameters.put(PARAMETER_PARENT_DATA_RECORD_ID_LIST, parentRecordIDList);
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);
        operationParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        operationParameters.put(PARAMETER_PAGE_SIZE, pageSize);

        // Execute the operation and extract the output parameters.
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, SERVER_OPERATION_RETRIEVE_DATA_RECORDS, operationParameters);
        retrievedDataRecords = (List<DataRecord>)outputParameters.get(PARAMETER_DATA_RECORD_LIST);
        terminology = (List<T8ConceptTerminology>)outputParameters.get(PARAMETER_CONCEPT_TERMINOLOGY_LIST);

        // Cache the additional terminology received from the server.
        if (terminology != null)
        {
            terminologyProvider.addTerminology(terminology);
        }

        // Return the retrieved records.
        return retrievedDataRecords;
    }
}
