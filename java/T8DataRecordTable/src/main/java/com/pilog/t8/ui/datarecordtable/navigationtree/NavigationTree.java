package com.pilog.t8.ui.datarecordtable.navigationtree;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class NavigationTree extends JTree
{
    public NavigationTree(NavigationModel model)
    {
        this.setOpaque(false);
        this.setModel(model);
    }

    public void refreshStructure()
    {
        if (treeModel != null) getModel().structureChanged();
    }

    public final void setModel(NavigationModel model)
    {
        super.setModel(model);
    }
    
    @Override
    public NavigationModel getModel()
    {
        return (NavigationModel)treeModel;
    }

    public void setSelectedNode(String recordID)
    {
        if (recordID != null)
        {
            if (treeModel != null)
            {
                DefaultMutableTreeNode node;

                node = getModel().findNode(recordID);
                if (node != null)
                {
                    this.setSelectionPath(new TreePath(node.getPath()));
                }
            }
        }
        else this.clearSelection();
    }

    public void expandAllNodes()
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            expandPath(getPathForRow(rowIndex));
        }
    }

    @Override
    protected void setExpandedState(TreePath path, boolean state)
    {
        // Ignore all collapse requests; collapse events will not be fired.
        if (state)
        {
            super.setExpandedState(path, state);
        }
    }
}
