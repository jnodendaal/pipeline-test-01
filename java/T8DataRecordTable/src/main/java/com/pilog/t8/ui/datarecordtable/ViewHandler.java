package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.ui.datarecord.RecordPropertyTableColumnDefinition;
import com.pilog.t8.client.T8ClientUtilities;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition.AutoResizeMode;
import com.pilog.t8.ui.datarecord.DataRecordEditorFactory;
import com.pilog.t8.ui.datarecordtable.DataRecordTable.DisplayMode;
import com.pilog.t8.ui.datarecord.celleditor.T8RecordTableCellEditor;
import com.pilog.t8.ui.datarecord.celleditor.T8RecordTableCellRenderer;
import com.pilog.t8.ui.datarecord.celleditor.TableCellPropertyResolver;
import com.pilog.t8.ui.datarecordtable.rowheader.RowHeaderTable;
import com.pilog.t8.ui.datarecordtable.rowheader.TableRowHeaderCellRenderer;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class ViewHandler
{
    private final T8DataRecordTableDefinition definition;
    private final DataRecordTable recordTable;
    private final JTable table;
    private final JScrollPane scrollPane;
    private final RowHeaderTable rowHeaderTable;
    private final T8RecordTableCellRenderer cellRenderer;
    private final T8RecordTableCellEditor cellEditor;
    private final T8TableHeaderMouseListener headerMouseListener;
    private final T8TableCopyActionListener copyActionListener;
    private final TableCellPropertyResolver propertyResolver;

    private final KeyStroke copyKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
    private final KeyStroke pasteKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK, false);

    public ViewHandler(T8DataRecordTableDefinition definition, DataRecordTable recordTable, JScrollPane scrollPane, JTable table)
    {
        this.definition = definition;
        this.recordTable = recordTable;
        this.table = table;
        this.scrollPane = scrollPane;
        this.propertyResolver = new ViewHandlerCellPropertyResolver();
        this.cellRenderer = new T8RecordTableCellRenderer(propertyResolver);
        this.cellEditor = new T8RecordTableCellEditor(recordTable.getCellMouseListener(), propertyResolver);
        this.rowHeaderTable = new RowHeaderTable(recordTable.getTableModelHandler(), null, table);
        this.headerMouseListener = new T8TableHeaderMouseListener();
        this.copyActionListener = new T8TableCopyActionListener();
    }

    public void setDisplayMode(DisplayMode mode)
    {
        if (mode == DisplayMode.PROPERTY_VALUE)
        {
            cellRenderer.setDisplayMode(T8RecordTableCellRenderer.DisplayMode.PROPERTY_VALUE);
            cellEditor.setEditMode(T8RecordTableCellEditor.EditMode.PROPERTY_VALUE);
        }
        else
        {
            cellRenderer.setDisplayMode(T8RecordTableCellRenderer.DisplayMode.PROPERTY_FFT);
            cellEditor.setEditMode(T8RecordTableCellEditor.EditMode.PROPERTY_FFT);
        }

        // Repaint the table to allow the change to take effect.
        table.repaint();
    }

    public void setDataChangeHandler(DataChangeHandler changeHandler)
    {
        this.rowHeaderTable.setDataChangeHandler(changeHandler);
    }

    private void configureTable()
    {
        AutoResizeMode autoResizeMode;

        // Set the row height of the table.
        table.setRowHeight(definition.getRowHeight());

        // Set the table's auto-resize mode.
        autoResizeMode = definition.getAutoResizeMode();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        if (autoResizeMode == AutoResizeMode.SUBSEQUENT_COLUMNS) table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        else if(autoResizeMode == AutoResizeMode.ALL_COLUMNS) table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else if(autoResizeMode == AutoResizeMode.NEXT_COLUMN) table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        else if(autoResizeMode == AutoResizeMode.LAST_COLUMN) table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        // Configure all of the columns according to their definitions.
        configureColumnModel();

        // Configure the row header column.
        configureRowHeaderColumnModel();

        // Make sure to only have one header mouse listener added.
        table.getTableHeader().removeMouseListener(headerMouseListener);
        table.getTableHeader().addMouseListener(headerMouseListener);

        // Override copy & paste actions.
        table.registerKeyboardAction(copyActionListener, "Copy", copyKeyStroke, JComponent.WHEN_FOCUSED);
    }

    private void configureColumnModel()
    {
        List<RecordPropertyTableColumnDefinition> columnDefinitions;
        TableColumnModel columnModel;
        int modelIndex;

        columnDefinitions = recordTable.getTableModelHandler().getColumnDefinitions();

        // Remove any current columns.
        columnModel = table.getColumnModel();
        while (columnModel.getColumnCount() > 0)
        {
            columnModel.removeColumn(columnModel.getColumn(0));
        }

        // Create new columns from the table definition.
        modelIndex = 1; // The model index starts at 1, so that the first system column (row guid) is not displayed.
        for (RecordPropertyTableColumnDefinition columnDefinition : columnDefinitions)
        {
            // Only add the column to the model if it is visible.
            if (columnDefinition.isVisible())
            {
                TableColumn newColumn;

                newColumn = new TableColumn(modelIndex++, columnDefinition.getWidth());
                newColumn.setHeaderValue(columnDefinition.getColumnName());
                newColumn.setCellRenderer(cellRenderer);
                newColumn.setCellEditor(cellEditor);
                table.addColumn(newColumn);
            }
            else modelIndex++;
        }
    }

    private void configureRowHeaderColumnModel()
    {
        TableColumnModel columnModel;
        TableColumn newColumn;

        // Remove any current columns.
        columnModel = rowHeaderTable.getColumnModel();
        while (columnModel.getColumnCount() > 0)
        {
            columnModel.removeColumn(columnModel.getColumn(0));
        }

        newColumn = new TableColumn(0, 25);
        newColumn.setHeaderValue(" ");
        rowHeaderTable.addColumn(newColumn);

        newColumn.setCellRenderer(new TableRowHeaderCellRenderer(recordTable, recordTable.getTableDataChangeHandler()));
    }

    public void setColumnRenderer(int columnIndex, TableCellRenderer renderer)
    {
        table.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    public void setColumnEditor(int columnIndex, TableCellEditor editor)
    {
        table.getColumnModel().getColumn(columnIndex).setCellEditor(editor);
    }

    public int convertModelToViewIndex(int modelIndex)
    {
        for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++)
        {
            TableColumn column;

            column = table.getColumnModel().getColumn(columnIndex);
            if (column.getModelIndex() == modelIndex)
            {
                return columnIndex;
            }
        }

        return -1;
    }

    public int convertViewToModelIndex(int viewIndex)
    {
        if (viewIndex >= table.getColumnCount())
        {
            return -1;
        }
        else return table.getColumnModel().getColumn(viewIndex).getModelIndex();
    }

    public void setModel(TableModel tableModel)
    {
        JTableHeader corner;
        Dimension dimension;

        // Set the new model on both the content table as well as the row header table.
        table.setModel(tableModel);
        rowHeaderTable.setModel(tableModel);

        // Configure the table according to the new model.
        configureTable();

        // Auto-size the row header table so that all row numbers fit.
        T8ClientUtilities.autoSizeTableColumn(rowHeaderTable, 0, 10);

        dimension = rowHeaderTable.getPreferredScrollableViewportSize();
        dimension.width = rowHeaderTable.getPreferredSize().width;
        rowHeaderTable.setPreferredScrollableViewportSize(dimension);
        rowHeaderTable.setRowHeight(table.getRowHeight());

        scrollPane.setRowHeaderView(rowHeaderTable);

        corner = rowHeaderTable.getTableHeader();
        corner.setReorderingAllowed(false);
        corner.setResizingAllowed(false);

        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, corner);
    }

    public void repaintRowHeaders()
    {
        rowHeaderTable.repaint();
    }

    public int getFocusedRowIndex()
    {
        return table.getSelectionModel().getLeadSelectionIndex();
    }

    public void setFocusedRowIndex(int rowIndex)
    {
        table.getSelectionModel().setLeadSelectionIndex(rowIndex);
    }

    public int getFocusedColumnIndex()
    {
        return table.getColumnModel().getSelectionModel().getLeadSelectionIndex();
    }

    public void setFocusedColumnIndex(int columnIndex)
    {
        table.getColumnModel().getSelectionModel().setLeadSelectionIndex(columnIndex);
    }

    public int getRowIndexForLocation(Point location)
    {
        return table.rowAtPoint(location);
    }

    public int getColumnIndexForLocation(Point location)
    {
        return table.columnAtPoint(location);
    }

    private void doFocusedCellCopy()
    {
        int column;
        int row;

        // Get the focused column and row index.
        row = getFocusedRowIndex();
        column = getFocusedColumnIndex();
        if ((column != -1) && (row != -1))
        {
            Object value;
            String data;
            StringSelection selection;
            Clipboard clipboard;

            value = table.getValueAt(row, column);
            data = value == null ? "" : value.toString();

            selection = new StringSelection(data);
            clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);
        }
    }

    private class T8TableHeaderMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            int columnIndex;

            columnIndex = table.convertColumnIndexToModel(table.columnAtPoint(e.getPoint()));
            if (columnIndex > -1)
            {
                PropertyRequirement propertyRequirement;
                DataChangeHandler dataChangeHandler;

                // If there are any uncomitted changes on the table, ask the user for confirmation before continuing.
                dataChangeHandler = recordTable.getTableDataChangeHandler();
                if (dataChangeHandler.getTableChangeCount() > 0)
                {
                    int option;

                    option = JOptionPane.showConfirmDialog(recordTable, recordTable.translate("Uncomitted changes will be lost.  Continue?"), recordTable.translate("Confirmation"), JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.NO_OPTION) return;
                }

                // Ok so continue with the ordering.
                propertyRequirement = recordTable.getTableModelHandler().getPropertyRequirement(columnIndex);
                recordTable.quickOrderOnField(definition.getDataRecordDocumentDEID() + "${" + propertyRequirement.getRequirementType() + "}");
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class T8TableCopyActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            doFocusedCellCopy();
        }
    };

    private class ViewHandlerCellPropertyResolver implements TableCellPropertyResolver
    {
        @Override
        public PropertyRequirement getViewCellPropertyRequirement(int rowIndex, int columnIndex)
        {
            int modelColumnIndex;

            modelColumnIndex = convertViewToModelIndex(columnIndex);
            return recordTable.getTableModelHandler().getPropertyRequirement(modelColumnIndex);
        }

        @Override
        public boolean isColumnEditable(int columnIndex)
        {
            int modelColumnIndex;

            modelColumnIndex = convertViewToModelIndex(columnIndex);
            return recordTable.getTableModelHandler().isColumnEditable(modelColumnIndex);
        }

        @Override
        public String translate(String inputString)
        {
            return recordTable.translate(inputString);
        }

        @Override
        public DataRecordEditorFactory getCellEditorComponentFactory()
        {
            return recordTable.getTableContainer().getEditorComponentFactory();
        }

        @Override
        public boolean isCellEditable(EventObject e)
        {
            // Make sure the mouse is clicked twice before allowing the edit.  This
            // is default behaviour but is not properly implemented by AbstractCellEditor.
            if (e instanceof MouseEvent)
            {
                MouseEvent event;

                event = (MouseEvent)e;
                if (event.getClickCount() >= 2)
                {
                    ViewHandler viewHandler;
                    Point location;
                    int columnIndex;

                    location = event.getPoint();
                    viewHandler = recordTable.getTableViewHandler();
                    columnIndex = viewHandler.getColumnIndexForLocation(location);
                    return isColumnEditable(columnIndex);
                }
                else return false;
            }
            else if (e instanceof KeyEvent)
            {
                ViewHandler viewHandler;
                int columnIndex;

                viewHandler = recordTable.getTableViewHandler();
                columnIndex = viewHandler.getFocusedColumnIndex();
                return isColumnEditable(columnIndex);
            }
            else return false;
        }

        @Override
        public TerminologyProvider getTerminologyProvider()
        {
            return recordTable.getTableContainer().getTerminologyProvider();
        }

        @Override
        public DataRecord getDataRecord(int rowIndex)
        {
            ModelHandler modelHandler;

            modelHandler = recordTable.getTableModelHandler();
            return modelHandler.getDataRecord(rowIndex);
        }
    }
}
