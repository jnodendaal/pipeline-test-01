package com.pilog.t8.ui.datarecordtable.navigationtree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

/**
 * @author Bouwer du Preez
 */
public class NavigationModel extends DefaultTreeModel
{
    public NavigationModel()
    {
        super(null);
    }

    public void setRootDRInstanceID(String drInstanceID)
    {
        this.setRoot(drInstanceID != null ? new DefaultMutableTreeNode(drInstanceID) : null);
    }

    public String getRootDRInstanceID()
    {
        return getRootNode() != null ? (String)getRootNode().getUserObject() : null;
    }

    public DefaultMutableTreeNode getRootNode()
    {
        return (DefaultMutableTreeNode)getRoot();
    }

    public void clear()
    {
        setRoot(null);
    }

    public void structureChanged()
    {
        nodeStructureChanged(root);
    }
    
    public void nodeChanged(String drInstanceID)
    {
        this.nodeChanged(findNode(drInstanceID));
    }
    
    public void nodesChanged()
    {
        for (TreeNode node : getAllNodes())
        {
            this.nodeChanged(node);
        }
    }
    
    public List<DefaultMutableTreeNode> getAllNodes()
    {
        List<DefaultMutableTreeNode> nodes;
        
        nodes = new ArrayList<DefaultMutableTreeNode>();
        if (root != null)
        {
            LinkedList<DefaultMutableTreeNode> nodeQueue;
            
            nodeQueue = new LinkedList<DefaultMutableTreeNode>();
            nodeQueue.add((DefaultMutableTreeNode)root);
            while (nodeQueue.size() > 0)
            {
                DefaultMutableTreeNode nextNode;
                
                nextNode = nodeQueue.pop();
                nodes.add(nextNode);
                for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
                {
                    nodeQueue.add((DefaultMutableTreeNode)nextNode.getChildAt(childIndex));
                }
            }
        }
        
        return nodes;
    }

    public DefaultMutableTreeNode findNode(String drInstanceID)
    {
        LinkedList<DefaultMutableTreeNode> nodeList;

        nodeList = new LinkedList<DefaultMutableTreeNode>();
        nodeList.add(this.getRootNode());
        while (nodeList.size() > 0)
        {
            DefaultMutableTreeNode nextNode;

            nextNode = nodeList.pollFirst();
            if (drInstanceID.equals(nextNode.getUserObject()))
            {
                return nextNode;
            }
            else
            {
                for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
                {
                    nodeList.add((DefaultMutableTreeNode)nextNode.getChildAt(childIndex));
                }
            }
        }

        return null;
    }
    
    public List<String> findChildDRInstanceIDs(String drInstanceID)
    {
        DefaultMutableTreeNode parentNode;
        List<String> childDRInstanceIDs;
        
        childDRInstanceIDs = new ArrayList<String>();
        parentNode = findNode(drInstanceID);
        if (parentNode != null)
        {
            for (int childIndex = 0; childIndex < parentNode.getChildCount(); childIndex++)
            {
                DefaultMutableTreeNode childNode;
                
                childNode = (DefaultMutableTreeNode)parentNode.getChildAt(childIndex);
                childDRInstanceIDs.add((String)childNode.getUserObject());
            }
        }
        
        return childDRInstanceIDs;
    }

    public void addNode(String parentDRInstanceID, String newDRInstanceID)
    {
        DefaultMutableTreeNode parentNode;

        parentNode = findNode(parentDRInstanceID);
        if (parentNode != null)
        {
            DefaultMutableTreeNode newNode;

            newNode = new DefaultMutableTreeNode(newDRInstanceID);
            insertNodeInto(newNode, parentNode, parentNode.getChildCount());
        }
    }

    public void removeNode(String drInstanceID)
    {
        DefaultMutableTreeNode node;

        node = findNode(drInstanceID);
        if (node != null)
        {
            removeNodeFromParent(node);
        }
    }

    public String findParentDRInstanceID(String drInstanceID)
    {
        DefaultMutableTreeNode node;

        node = findNode(drInstanceID);
        if (node != null)
        {
            DefaultMutableTreeNode parentNode;

            parentNode = (DefaultMutableTreeNode)node.getParent();
            if (parentNode != null)
            {
                return (String) parentNode.getUserObject();
            }
            else return null;
        }
        else return null;
    }
}
