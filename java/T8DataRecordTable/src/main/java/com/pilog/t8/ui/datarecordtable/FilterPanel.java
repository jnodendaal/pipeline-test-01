package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.ui.datarecord.DataRecordEditorFactory;
import com.pilog.t8.ui.datarecord.FilterTable;
import com.pilog.t8.ui.datarecord.RecordPropertyTableColumnDefinition;
import com.pilog.t8.ui.datarecord.celleditor.TableCellPropertyResolver;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class FilterPanel extends javax.swing.JPanel
{
    private final DataRecordTable recordTable;
    private final FilterTable filterTable;
    private List<RecordPropertyTableColumnDefinition> filterColumnDefinitions;
    private DataRecord filterDataRecord; // Used internally to the filter panel for editing of filter values.

    public FilterPanel(DataRecordTable recordTable)
    {
        initComponents();
        this.recordTable = recordTable;
        this.filterColumnDefinitions = getFilterColumnDefinitions();
        this.filterTable = new FilterTable(filterColumnDefinitions, new FilterPanelCellPropertyResolver());
        this.jScrollPaneFilterValues.setViewportView(filterTable);
        this.filterTable.rebuildFilterTable(getFilterColumnDefinitions());
    }
    
    private List<RecordPropertyTableColumnDefinition> getFilterColumnDefinitions()
    {
        List<RecordPropertyTableColumnDefinition> columnDefinitions;

        columnDefinitions = new ArrayList<RecordPropertyTableColumnDefinition>();
        for (RecordPropertyTableColumnDefinition columnDefinition : recordTable.getTableModelHandler().getColumnDefinitions())
        {
            // Only add visible columns to the filter table.
            if (columnDefinition.isVisible())
            {
                columnDefinitions.add(columnDefinition);
            }
        }
        
        return columnDefinitions;
    }
    
    public void rebuildFilterTable()
    {
        DataRequirementInstance drInstance;
        
        drInstance = recordTable.getTableModelHandler().getDataRequirementInstance();
        this.filterDataRecord = new DataRecord(drInstance);
        
        for (SectionRequirement sectionRequirement : drInstance.getDataRequirement().getSectionRequirements())
        {
            this.filterDataRecord.setRecordSection(new RecordSection(sectionRequirement));
        }
        
        this.filterColumnDefinitions = getFilterColumnDefinitions();
        filterTable.rebuildFilterTable(getFilterColumnDefinitions());
    }

    private class FilterPanelCellPropertyResolver implements TableCellPropertyResolver
    {
        @Override
        public PropertyRequirement getViewCellPropertyRequirement(int rowIndex, int columnIndex)
        {
            if (columnIndex == 2)
            {
                return filterColumnDefinitions.get(rowIndex).getPropertyRequirement();
            }
            else throw new RuntimeException("Invalid column index encountered while resolving property: " + columnIndex);
        }

        @Override
        public boolean isColumnEditable(int columnIndex)
        {
            // All filter values are editable.
            return true;
        }

        @Override
        public String translate(String inputString)
        {
            return recordTable.translate(inputString);
        }

        @Override
        public DataRecordEditorFactory getCellEditorComponentFactory()
        {
            return recordTable.getTableContainer().getEditorComponentFactory();
        }

        @Override
        public boolean isCellEditable(EventObject e)
        {
            // Make sure the mouse is clicked twice before allowing the edit.  This
            // is default behaviour but is not properly implemented by AbstractCellEditor.
            if (e instanceof MouseEvent)
            { 
                MouseEvent event;

                event = (MouseEvent)e;
                if (event.getClickCount() >= 2)
                {
                    Point location;
                    int columnIndex;

                    location = event.getPoint();
                    columnIndex = filterTable.getColumnIndexForLocation(location);
                    return isColumnEditable(columnIndex);
                }
                else return false;
            }
            else if (e instanceof KeyEvent)
            { 
                int columnIndex;

                columnIndex = filterTable.getFocusedColumnIndex();
                return isColumnEditable(columnIndex);
            }
            else return false;
        }

        @Override
        public TerminologyProvider getTerminologyProvider()
        {
            return recordTable.getTableContainer().getTerminologyProvider();
        }
        
        @Override
        public DataRecord getDataRecord(int rowIndex)
        {
            return filterDataRecord;
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPaneFilterValues = new javax.swing.JScrollPane();
        jLabelSpacer = new javax.swing.JLabel();
        jButtonClear = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jButtonApply = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        add(jScrollPaneFilterValues, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jLabelSpacer, gridBagConstraints);

        jButtonClear.setText("Clear");
        jButtonClear.setMaximumSize(new java.awt.Dimension(65, 23));
        jButtonClear.setMinimumSize(new java.awt.Dimension(65, 23));
        jButtonClear.setPreferredSize(new java.awt.Dimension(65, 23));
        jButtonClear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jButtonClear, gridBagConstraints);

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jButtonCancel, gridBagConstraints);

        jButtonApply.setText("Apply");
        jButtonApply.setMaximumSize(new java.awt.Dimension(65, 23));
        jButtonApply.setMinimumSize(new java.awt.Dimension(65, 23));
        jButtonApply.setPreferredSize(new java.awt.Dimension(65, 23));
        jButtonApply.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonApplyActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 6, 9);
        add(jButtonApply, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonClearActionPerformed
    {//GEN-HEADEREND:event_jButtonClearActionPerformed
        filterTable.clearFilterValues();
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        recordTable.showDataView();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonApplyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonApplyActionPerformed
    {//GEN-HEADEREND:event_jButtonApplyActionPerformed
        recordTable.setUserDefinedFilter(new T8DataFilter(null, filterTable.getFilterCriteria()));
        recordTable.getTableContainer().refreshData(recordTable, true);
    }//GEN-LAST:event_jButtonApplyActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonApply;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonClear;
    private javax.swing.JLabel jLabelSpacer;
    private javax.swing.JScrollPane jScrollPaneFilterValues;
    // End of variables declaration//GEN-END:variables
}
