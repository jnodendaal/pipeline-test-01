package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableDefinition;
import com.pilog.t8.ui.datarecord.RecordPropertyTableColumnDefinition;
import com.pilog.t8.ui.datarecordtable.rowheader.TableRowHeader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector; //TODO: GBO - Get rid of obsolete class usage
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * The table model columns each have the following three important attributes:
 *  - Column Identifier:   The identifier of the column definition that will
 *                         also be the name of the column in the table model.
 *  - Column Name:         The display name of the column.
 *  - Source Name:         The identifier of the data source field from which
 *                         the column's data is obtained.
 *
 * @author Bouwer du Preez
 */
public class ModelHandler
{
    private static final T8Logger logger = T8Log.getLogger(ModelHandler.class);

    private final T8DataRecordTableDefinition tableDefinition;
    private final TerminologyProvider terminologyProvider;
    private ArrayList<RecordPropertyTableColumnDefinition> columnDefinitions;
    private DataRequirementInstance dataRequirementInstance;
    private DefaultTableModel tableModel;
    private final DataRecordTable recordTable;
    private final List<TableModelListener> modelListeners;
    private final T8DataRecordAccessHandler accessHandler;
    private Map<String, List<T8DataValidationError>> recordValidationErrors;

    public ModelHandler(T8DataRecordTableDefinition tableDefinition, DataRecordTable recordTable, TerminologyProvider terminologyProvider)
    {
        this.tableDefinition = tableDefinition;
        this.recordTable = recordTable;
        this.accessHandler = recordTable.getTableContainer().getAccessHandler();
        this.terminologyProvider = terminologyProvider;
        this.columnDefinitions = new ArrayList<>();
        this.tableModel = new T8DataRecordTableModel(tableDefinition);
        this.modelListeners = new ArrayList<>();
        this.recordValidationErrors = new HashMap<>();
    }

    public DefaultTableModel getTableModel()
    {
        return tableModel;
    }

    public DataRequirementInstance getDataRequirementInstance()
    {
        return dataRequirementInstance;
    }

    public List<RecordPropertyTableColumnDefinition> getColumnDefinitions()
    {
        return columnDefinitions;
    }

    public void addTableModelListener(TableModelListener listener)
    {
        modelListeners.add(listener);
        if (tableModel != null) tableModel.addTableModelListener(listener);
    }

    public void removeTableModelListener(TableModelListener listener)
    {
        modelListeners.remove(listener);
        if (tableModel != null) tableModel.removeTableModelListener(listener);
    }

    private void addAllTableModelListeners()
    {
        for (TableModelListener listener : modelListeners)
        {
            tableModel.addTableModelListener(listener);
        }
    }

    private void removeAllTableModelListeners()
    {
        if (tableModel != null)
        {
            for (TableModelListener listener : modelListeners)
            {
                tableModel.removeTableModelListener(listener);
            }
        }
    }

    public void clearTableModel()
    {
        if (tableModel != null) tableModel.setRowCount(0);
        columnDefinitions.clear();
    }

    public DefaultTableModel buildTableModel(DataRequirementInstance dataRequirementInstance)
    {
        List<PropertyRequirement> propertyRequirementList;

        // Set the Data Requirement instance variable.
        this.dataRequirementInstance = dataRequirementInstance;

        // Create the new table model.
        tableModel = new T8DataRecordTableModel(tableDefinition);

        // Add all of the columns the table model.
        propertyRequirementList = new ArrayList<>(DocumentHandler.getAllPropertyRequirements(dataRequirementInstance.getDataRequirement()));
        columnDefinitions = new ArrayList<>();
        tableModel.addColumn(T8DataRecordTable.ROW_HEADER_COLUMN_NAME);

        // Add all of the properties from the Data Requirement as columns.
        for (PropertyRequirement propertyRequirement : propertyRequirementList)
        {
            RecordPropertyTableColumnDefinition columnDefinition;

            columnDefinition = new RecordPropertyTableColumnDefinition();
            columnDefinition.setPropertyRequirement(propertyRequirement);
            columnDefinition.setColumnName(terminologyProvider.getTerm(null, propertyRequirement.getConceptID()));
            columnDefinition.setVisible(true);
            columnDefinition.setWidth(260);

            columnDefinitions.add(columnDefinition);
            tableModel.addColumn(propertyRequirement);
        }

        // Add all table model listeners.
        addAllTableModelListeners();

        return tableModel;
    }

    public void setModelDataRecords(List<DataRecord> dataRecords, int pageOffset)
    {
        // Remove all table model listeners.
        removeAllTableModelListeners();

        // Clear existing data.
        while (tableModel.getRowCount() > 0) tableModel.setRowCount(0);

        // Add all of the rows to the table model.
        if (dataRecords != null)
        {
            for (int rowIndex = 0; rowIndex < dataRecords.size(); rowIndex++)
            {
                TableRowHeader rowHeader;
                DataRecord rowRecord;
                Vector rowValues;

                // Get the row record and create a header for it.
                rowRecord = dataRecords.get(rowIndex);
                rowHeader = new TableRowHeader(rowRecord, pageOffset + rowIndex);
                rowHeader.setValidationErrors(recordValidationErrors.get(rowRecord.getID()));

                // Create the new row, by filling it with data from the retrieved collection.
                rowValues = new Vector();
                rowValues.add(rowHeader); // Add the row record to the first (system) column.

                // Add the properties.
                for (RecordPropertyTableColumnDefinition columnDefinition : columnDefinitions)
                {
                    rowValues.add(rowRecord.getRecordProperty(columnDefinition.getPropertyRequirement().getConceptID()));
                }

                // Add the new row to the table model.
                tableModel.addRow(rowValues);
            }
        }

        // Add all table model listeners.
        addAllTableModelListeners();
    }

    /**
     * Returns the index in the model of the specified column.  This method
     * takes into account that the first column of the model (index 0) contains
     * a system value i.e. this method will return the column definition index
     * + 1 (for the system column).
     *
     * @param propertyID The identifier of the column for which an index
     * will be returned.
     * @return The index of the specified column or -1 if the column identifier
     * is not found.
     */
    public int getColumnIndex(String propertyID)
    {
        for (int columnIndex = 0; columnIndex < columnDefinitions.size(); columnIndex++)
        {
            if (columnDefinitions.get(columnIndex).getPropertyRequirement().getConceptID().equals(propertyID)) return columnIndex + 1;
        }

        return -1;
    }

    /**
     * Returns the PropertyRequirement that defines the column at the specified
     * index.  The index is the absolute model index, including the first
     * system column i.e. invoking this method for index 0 will result in an
     * exception.
     *
     * @param columnIndex The absolute model column index for which to return
     * the defining property requirement.
     * @return The PropertyRequirement that defines the model column and the
     * specified index (excluding index 0 that contains no property).
     */
    public PropertyRequirement getPropertyRequirement(int columnIndex)
    {
        if ((columnIndex < 1) || (columnIndex > columnDefinitions.size()))
        {
            throw new IllegalArgumentException("Invalid column index: " + columnIndex + ", column count: " + (columnDefinitions.size() + 1));
        }
        else
        {
            RecordPropertyTableColumnDefinition columnDefinition;

            columnDefinition = columnDefinitions.get(columnIndex -1); // Subtract one from the index to account for the first model column that is a system column and does not contain a property.
            return columnDefinition.getPropertyRequirement();
        }
    }

    public boolean isColumnEditable(int columnIndex)
    {
        PropertyRequirement propertyRequirement;

        propertyRequirement = getPropertyRequirement(columnIndex);
        if (propertyRequirement != null)
        {
            ValueRequirement valueRequirement;

            // There is one exception to editability - a document reference must
            // be editable as a table cell in order to allow navigation to sub-
            // records.  The table cell editor itself must then handle
            // editability of the references it contains.
            valueRequirement = propertyRequirement.getValueRequirement();
            if (valueRequirement != null)
            {
                if (RequirementType.DOCUMENT_REFERENCE.equals(valueRequirement.getRequirementType())) return true;
            }

            // Return the editability of the cell as specified by the access handler.
            //return accessHandler.isEditable(propertyRequirement);
            return true;
        }
        else return false;
    }

    public int getRowIndex(String recordID)
    {
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            TableRowHeader rowHeader;

            rowHeader = getRowHeader(rowIndex);
            if (rowHeader.getDataRecord().getID().equals(recordID)) return rowIndex;
        }

        return -1;
    }

    public void refreshRecordRow(String recordID)
    {
        int rowIndex;

        rowIndex = getRowIndex(recordID);
        refreshRecordRow(rowIndex);
    }

    public void refreshRecordRow(int rowIndex)
    {
        if (rowIndex > -1)
        {
            DataRecord rowRecord;

            rowRecord = getDataRecord(rowIndex);
            for (int columnDefinitionIndex = 0; columnDefinitionIndex < columnDefinitions.size(); columnDefinitionIndex++)
            {
                RecordPropertyTableColumnDefinition columnDefinition;

                columnDefinition = columnDefinitions.get(columnDefinitionIndex);
                tableModel.setValueAt(rowRecord.getRecordProperty(columnDefinition.getPropertyRequirement().getConceptID()), rowIndex, columnDefinitionIndex + 1);
            }
        }
    }

    public boolean deleteDataRecordRow(DataRecord dataRecord)
    {
        int rowIndex;

        rowIndex = getRowIndex(dataRecord.getID());
        if (rowIndex > -1)
        {
            deleteRow(rowIndex);
            return true;
        }
        else return false;
    }

    public void deleteRow(int rowIndex)
    {
        tableModel.removeRow(rowIndex);
    }

    public void insertRow(int rowIndex, DataRecord newRowRecord)
    {
        Object[] newRow;

        newRow = new Object[columnDefinitions.size()+1];

        Arrays.fill(newRow, null);
        newRow[0] = new TableRowHeader(newRowRecord, 0);

        tableModel.insertRow(rowIndex, newRow);
    }

    public TableRowHeader getRowHeader(int rowIndex)
    {
        return (TableRowHeader)tableModel.getValueAt(rowIndex, 0);
    }

    public DataRecord getDataRecord(int rowIndex)
    {
        return ((TableRowHeader)tableModel.getValueAt(rowIndex, 0)).getDataRecord();
    }

    public List<DataRecord> getDataRecords(int[] rowIndices)
    {
        List<DataRecord> dataRecords;

        dataRecords = new ArrayList<>();
        for (int rowIndex : rowIndices)
        {
            dataRecords.add(getDataRecord(rowIndex));
        }

        return dataRecords;
    }

    public List<String> getDataRecordIDList()
    {
        List<String> recordIDList;

        recordIDList = new ArrayList<>();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            recordIDList.add(getDataRecord(rowIndex).getID());
        }

        return recordIDList;
    }

    public void setRecordProperty(int rowIndex, RecordProperty recordProperty)
    {
        DataRecord record;

        record = getDataRecord(rowIndex);
        record.setRecordProperty(recordProperty);
        refreshRecordRow(rowIndex);
    }

    public RecordProperty getRecordProperty(int rowIndex, int columnIndex)
    {
        PropertyRequirement propertyRequirement;
        DataRecord record;

        record = getDataRecord(rowIndex);
        propertyRequirement = getPropertyRequirement(columnIndex);
        return record.getRecordProperty(propertyRequirement.getConceptID());
    }

    public void fillDownProperty(int rowIndex, String propertyID)
    {
        try
        {
            DataRecord sourceRecord;
            RecordValue sourceValue;

            recordTable.stopTableEditing();

            sourceRecord = getDataRecord(rowIndex);
            sourceValue = sourceRecord.getRecordValue(propertyID);
            for (int index = rowIndex+1; index < tableModel.getRowCount(); index++)
            {
                PropertyRequirement targetPropertyRequirement;
                ValueRequirement targetValueRequirement;
                DataRecord targetRecord;
                RecordValue targetValue;

                targetRecord = getDataRecord(index);
                if (sourceValue != null)
                {
                    targetPropertyRequirement = targetRecord.getDataRequirement().getPropertyRequirement(propertyID);
                    targetValueRequirement = targetPropertyRequirement.getValueRequirement();
                    if (!targetValueRequirement.isEquivalentRequirement(sourceValue.getValueRequirement()))
                    {
                        ValueRequirement sourceValueRequirement;

                        sourceValueRequirement = sourceValue.getValueRequirement();
                        targetValueRequirement = targetValueRequirement.getDescendentRequirement(sourceValueRequirement.getRequirementType(), sourceValueRequirement.getValue(), sourceValueRequirement.getIndex());
                    }

                    targetValue = sourceValue.copy(targetValueRequirement);
                }
                else targetValue = null;

                targetRecord.setRecordValue(propertyID, targetValue);
                refreshRecordRow(index);
            }
        }
        catch (Exception e)
        {
            logger.log("Exception while filling down record value.", e);
        }
    }

    public void setValidationErrors(Map<String, List<T8DataValidationError>> validtionErrors)
    {
        this.recordValidationErrors = validtionErrors;
        if (recordValidationErrors == null) recordValidationErrors = new HashMap<>();

        // Set the validation errors for each row header.
        if (tableModel != null)
        {
            for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
            {
                TableRowHeader rowHeader;

                rowHeader = getRowHeader(rowIndex);
                rowHeader.setValidationErrors(recordValidationErrors.get(rowHeader.getDataRecord().getID()));
            }
        }
    }
}
