package com.pilog.t8.ui.datarecordtable.component;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecordeditor.view.content.RecordEditorFactory;
import com.pilog.t8.ui.datarecordeditor.component.datatype.SetEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.ChoiceEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.composite.CompositeEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.composite.FieldEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.StringEditor;
import com.pilog.t8.ui.datarecordtable.DataRecordTableContainer;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableComponentFactory extends RecordEditorFactory
{
    private DataRecordTableContainer container;

    public T8DataRecordTableComponentFactory(DataRecordTableContainer container)
    {
        super(container);
        this.container = container;
    }

    @Override
    public DataRecordEditor createDataRecordEditorComponent(RecordValue recordValue)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;

        valueRequirement = recordValue.getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType.equals(RequirementType.BAG_TYPE))
        {
            return new SetEditor(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.CHOICE_TYPE))
        {
            return new ChoiceEditor(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.COMPOSITE_TYPE))
        {
            return new CompositeEditor(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.DOCUMENT_REFERENCE))
        {
            return new DocumentReferenceEditorComponent(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.FIELD_TYPE))
        {
            return new FieldEditor(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.SEQUENCE_TYPE))
        {
            return new SetEditor(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.SET_TYPE))
        {
            return new SetEditor(container, recordValue);
        }
        else if (requirementType.equals(RequirementType.ONTOLOGY_CLASS))
        {
            return new StringEditor(container, recordValue, null);
        }
        else return super.createDataRecordEditorComponent(recordValue);
    }
}
