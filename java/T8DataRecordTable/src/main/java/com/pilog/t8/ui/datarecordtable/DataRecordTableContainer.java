package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface DataRecordTableContainer extends DataRecordEditorContainer
{
    public boolean commitChanges(boolean waitForCompletion);
    public void revertChanges();
    
    public void stopTableEditing();
    public void refreshNavigator();
    public T8ComponentController getController();
    public String translate(String inputString);
    public boolean containsTable(String drInstanceID);
    public void refreshData(DataRecordTable targetTable, boolean refreshDescendants);
    public DataRecordTable addTable(String drInstanceID);
    public void showTable(String drInstanceID);
    public DataRecordTable getTable(String drInstanceID);
    public DataRecordTable getParentTable(String drInstanceID);
    public List<DataRecordTable> getChildTables(String drInstanceID);
    public List<String> getServerValidationScriptIdentifiers();
    public List<String> getNewRecordTagInheritanceList();
}
