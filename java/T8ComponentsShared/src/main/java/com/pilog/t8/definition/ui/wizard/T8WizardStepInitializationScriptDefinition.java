package com.pilog.t8.definition.ui.wizard;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WizardStepInitializationScriptDefinition extends T8ComponentControllerScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_UI_COMPONENT_WIZARD_STEP_INITIALIZATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_UI_COMPONENT_WIZARD_STEP_INITIALIZATION";
    public static final String IDENTIFIER_PREFIX = "SCRIPT_INIT_";
    public static final String DISPLAY_NAME = "Step Initialization Script";
    public static final String DESCRIPTION = "A script that is executed before a step is shown on the wizard.";
    // -------- Definition Meta-Data -------- //

    public T8WizardStepInitializationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8WizardDefinition wizardDefinition;

        wizardDefinition = (T8WizardDefinition)getAncestorDefinition(T8WizardDefinition.TYPE_IDENTIFIER);
        if (wizardDefinition != null)
        {
            return wizardDefinition.getWizardParameterDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }
}
