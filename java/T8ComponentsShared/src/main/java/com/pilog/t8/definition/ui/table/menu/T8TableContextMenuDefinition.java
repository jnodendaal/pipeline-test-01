/**
 * Created on 19 Oct 2015, 9:33:53 AM
 */
package com.pilog.t8.definition.ui.table.menu;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.menu.T8MenuDefinition;
import com.pilog.t8.definition.ui.menuitem.T8MenuItemDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8TableContextMenuDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TABLE_CONTEXT_MENU";
    public static final String DISPLAY_NAME = "Table Context Menu";
    public static final String DESCRIPTION = "A UI component that manages the display of the context menu for a table.";
    public static final String IDENTIFIER_PREFIX = "C_TABLE_MENU_";
    public enum Datum { HIDE_FILL_DOWN,
                        HIDE_EXPORT,
                        HIDE_COPY_ROWS,
                        SHOW_ON_EDITABLE_FIELDS_ONLY,
                        ADDITIONAL_MENU_DEFINITIONS,
                        MENU_DISPLAY_EVALUATION_SCRIPT};
    // -------- Definition Meta-Data -------- //

    public static final String P_CURRENT_ROW_ENTITY = "$P_CURRENT_ROW_ENTITY";
    public static final String P_COLUMN_IDENTIFIER = "$P_COLUMN_IDENTIFIER";

    public T8TableContextMenuDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_FILL_DOWN.toString(), "Hide Fill Down", "A boolean setting that hides the Fill Down context menu item.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_EXPORT.toString(), "Hide Export", "A boolean setting that hides the Export context menu item.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_COPY_ROWS.toString(), "Hide Copy Rows", "A boolean setting that hides the Copy Rows context menu item.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SHOW_ON_EDITABLE_FIELDS_ONLY.toString(), "Show On Editable Fields Only", "A boolean setting that hides the menu whenever the field it was triggered on is non-editable.", false));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ADDITIONAL_MENU_DEFINITIONS.toString(), "Additional Menus", "Additional menu's and their items to be added to the predefined context menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.MENU_DISPLAY_EVALUATION_SCRIPT.toString(), "Display Evaluation Script", "The script will be executed each time before the menu is displayed."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ADDITIONAL_MENU_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8MenuDefinition.GROUP_IDENTIFIER));
        else if (Datum.MENU_DISPLAY_EVALUATION_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableMenuDisplayEvalScriptDefinition.TYPE_IDENTIFIER));
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.table.menu.T8TableContextMenu", new Class<?>[]{T8TableContextMenuDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TableContextMenuAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TableContextMenuAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8MenuItemDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<>(getDefinitionDatum(Datum.ADDITIONAL_MENU_DEFINITIONS));
    }

    List<T8DataParameterDefinition> getContextMenuParameterDefinitions()
    {
        List<T8DataParameterDefinition> inputParameterDefinitions;

        // Add the default parameters.
        inputParameterDefinitions = new ArrayList<>();
        inputParameterDefinitions.add(new T8DataParameterDefinition(P_COLUMN_IDENTIFIER, "Column Identifier", "The identifier of the column at which the menu will be displayed.", T8DataType.DEFINITION_IDENTIFIER));
        inputParameterDefinitions.add(new T8DataParameterDefinition(P_CURRENT_ROW_ENTITY, "Current Row Data Entity", "The data entity of the row at which the menu will be displayed.", T8DataType.DATA_ENTITY));

        // Return the complete list of valid parameter definitions.
        return inputParameterDefinitions;
    }

    public boolean isHideCopyRows()
    {
        return getDefinitionDatum(Datum.HIDE_COPY_ROWS);
    }

    public void setHideCopyRows(boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_COPY_ROWS, hidden);
    }

    public boolean isHideExport()
    {
        return getDefinitionDatum(Datum.HIDE_EXPORT);
    }

    public void setHideExport(boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_EXPORT, hidden);
    }

    public boolean isHideFillDown()
    {
        return getDefinitionDatum(Datum.HIDE_FILL_DOWN);
    }

    public void setHideFillDown(boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_FILL_DOWN, hidden);
    }

    public boolean isShowOnEditableFieldsOnly()
    {
        return getDefinitionDatum(Datum.SHOW_ON_EDITABLE_FIELDS_ONLY);
    }

    public void setShowOnEditableFieldsOnly(boolean show)
    {
        setDefinitionDatum(Datum.SHOW_ON_EDITABLE_FIELDS_ONLY, show);
    }

    public T8TableMenuDisplayEvalScriptDefinition getDisplayEvaluationScriptDefinition()
    {
        return getDefinitionDatum(Datum.MENU_DISPLAY_EVALUATION_SCRIPT);
    }

    public void setInitializationScriptDefinition(T8TableMenuDisplayEvalScriptDefinition displayEvaluationScriptDefinition)
    {
        setDefinitionDatum(Datum.MENU_DISPLAY_EVALUATION_SCRIPT, displayEvaluationScriptDefinition);
    }
}