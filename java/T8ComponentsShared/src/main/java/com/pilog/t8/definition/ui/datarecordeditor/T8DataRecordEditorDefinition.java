package com.pilog.t8.definition.ui.datarecordeditor;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_RECORD_EDITOR";
    public static final String IDENTIFIER_PREFIX = "C_DATA_RECORD_EDITOR_";
    public static final String DISPLAY_NAME = "Data Record Editor";
    public static final String DESCRIPTION = "A component that displays a data record and allows the user to edit the content of the document.";
    public enum Datum {
        COMMIT_ENABLED,
        EXPORT_ENABLED,
        LANGUAGE_SWITCH_ENABLED,
        DESCRIPTION_CLASSIFICATION_IDENTIFIER,
        COMMIT_DATA_RECORD_ACCESS_IDENTIFIER,
        DATA_RECORD_ACCESS_IDENTIFIER,
        VALUE_LOOKUP_DIALOG_IDENTIFIER,
        CLASSIFICATION_LOOKUP_DIALOG_IDENTIFIER,
        DATA_RECORD_HISTORY_MODULE_IDENTIFIER,
        HIDE_TREE_NAVIGATION,
        HIDE_TOOLBAR,
        HIDE_TOOLBAR_REFRESH,
        HIDE_TOOLBAR_PROPERTY_FFT,
        HIDE_TOOLBAR_TREE_NAVIGATION
    };
    // -------- Definition Meta-Data -------- //

    // Caches the client-side validation scripts.
    private T8DataRecordAccessDefinition dataRecordAccessDefinition;

    public enum AccessValidationLevel {SAVE, FINAL};

    public T8DataRecordEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COMMIT_ENABLED.toString(), "Save Changes Enabled", "A boolean setting that enables (if true) the option to save changes to the document displayed by the editor.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EXPORT_ENABLED.toString(), "Export Enabled", "A boolean setting that enables (if true) the option to export the document displayed by the editor.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LANGUAGE_SWITCH_ENABLED.toString(), "Language Switch Enabled", "A boolean setting that enables (if true) the option to switch the dictionary language used to display the document in the editor.", false));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.COMMIT_DATA_RECORD_ACCESS_IDENTIFIER.toString(), "Commit Data Record Access", "The access rights used when a record is saved using the commit button on the UI."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString(), "Data Record Access", "The access rights to data displayed/edited in the editor."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.VALUE_LOOKUP_DIALOG_IDENTIFIER.toString(), "Value Lookup Dialog", "The lookup dialog to use when the user has to select a value for a specific property/field/UOM/QOM.", "@C_DATA_LOOKUP_DIALOG_RECORD_EDITOR_VALUE"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CLASSIFICATION_LOOKUP_DIALOG_IDENTIFIER.toString(), "Classification Lookup Dialog", "The lookup dialog to use when the user has to select a classification for the specific record.", "@C_DATA_LOOKUP_DIALOG_CLASSIFICATION"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_HISTORY_MODULE_IDENTIFIER.toString(), "Data Record History Module", "The module that will be show to show the history information for a data record.", "@M_DATA_RECORD_EDITOR_HISTORY_VIEWER"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_TREE_NAVIGATION.toString(), "Hide Tree Navigation", "A boolean setting that hides the tree navigation section of the editor if enabled.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_TOOLBAR.toString(), "Hide Toolbar", "A boolean setting that hides the entire toolbar. This overrides individual toolbar hide settings.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_TOOLBAR_REFRESH.toString(), "Hide Toolbar Refresh", "A boolean setting that hides the Refresh button on the toolbar.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_TOOLBAR_PROPERTY_FFT.toString(), "Hide Toolbar Property FFT", "A boolean setting that hides the Property FFT button on the toolbar.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HIDE_TOOLBAR_TREE_NAVIGATION.toString(), "Hide Toolbar Tree Navigation", "A boolean setting that hides the Tree Navigation button on the toolbar.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataRecordAccessDefinition.GROUP_IDENTIFIER), true, "No Restriction");
        else if (Datum.COMMIT_DATA_RECORD_ACCESS_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataRecordAccessDefinition.GROUP_IDENTIFIER), true, "No Restriction");
        else if (Datum.VALUE_LOOKUP_DIALOG_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.CLASSIFICATION_LOOKUP_DIALOG_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_RECORD_HISTORY_MODULE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8ModuleDefinition.TYPE_IDENTIFIER), true, "Not Viewable");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String recordAccessId;

        // Retrieve the data record access definition.
        recordAccessId = getDataRecordAccessIdentifier();
        if (recordAccessId != null)
        {
            dataRecordAccessDefinition = (T8DataRecordAccessDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), recordAccessId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datarecordeditor.T8DataRecordEditor", new Class<?>[]{T8DataRecordEditorDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return T8DataRecordEditorAPIHandler.getInputParameterDefinitions();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataRecordEditorAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataRecordEditorAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public boolean isCommitEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.COMMIT_ENABLED));
    }

    public void setCommitEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.COMMIT_ENABLED, enabled);
    }

    public boolean isExportEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.EXPORT_ENABLED));
    }

    public void setExportEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.EXPORT_ENABLED, enabled);
    }

    public boolean isLanguageSwitchEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.LANGUAGE_SWITCH_ENABLED));
    }

    public void setLanguageSwitchEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.LANGUAGE_SWITCH_ENABLED, enabled);
    }

    public String getDescriptionClassificationIdentifier()
    {
        return getDefinitionDatum(Datum.DESCRIPTION_CLASSIFICATION_IDENTIFIER);
    }

    public void setDescriptionClassificationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DESCRIPTION_CLASSIFICATION_IDENTIFIER, identifier);
    }

    public T8DataRecordAccessDefinition getDataRecordAccessDefinition()
    {
        return dataRecordAccessDefinition;
    }

    public String getDataRecordAccessIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_RECORD_ACCESS_IDENTIFIER);
    }

    public void setDataRecordAccessIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_ACCESS_IDENTIFIER, identifier);
    }

    public String getCommitDataRecordAccessIdentifier()
    {
        return getDefinitionDatum(Datum.COMMIT_DATA_RECORD_ACCESS_IDENTIFIER);
    }

    public void setCommitDataRecordAccessIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.COMMIT_DATA_RECORD_ACCESS_IDENTIFIER, identifier);
    }

    public String getValueLookupDialogIdentifier()
    {
        return getDefinitionDatum(Datum.VALUE_LOOKUP_DIALOG_IDENTIFIER);
    }

    public void setValueLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.VALUE_LOOKUP_DIALOG_IDENTIFIER, identifier);
    }

    public String getClassificationLookupDialogIdentifier()
    {
        return getDefinitionDatum(Datum.CLASSIFICATION_LOOKUP_DIALOG_IDENTIFIER);
    }

    public void setClassificationLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CLASSIFICATION_LOOKUP_DIALOG_IDENTIFIER, identifier);
    }

    public String getDataRecordHistoryModuleIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_RECORD_HISTORY_MODULE_IDENTIFIER);
    }

    public void setDataRecordHistoryModuleIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_HISTORY_MODULE_IDENTIFIER, identifier);
    }

    public boolean isTreeNavigationHidden()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.HIDE_TREE_NAVIGATION));
    }

    public void setTreeNavigationHidden(Boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_TREE_NAVIGATION, hidden);
    }

    public boolean isToolbarHidden()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.HIDE_TOOLBAR));
    }

    public void setToolbarHidden(Boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_TOOLBAR, hidden);
    }

    public boolean isToolbarRefreshHidden()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.HIDE_TOOLBAR_REFRESH));
    }

    public void setToolbarRefreshHidden(Boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_TOOLBAR_REFRESH, hidden);
    }

    public boolean isToolbarPropertyFFTHidden()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.HIDE_TOOLBAR_PROPERTY_FFT));
    }

    public void setToolbarPropertyFFTHidden(Boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_TOOLBAR_PROPERTY_FFT, hidden);
    }

    public boolean isToolbarTreeNavigationHidden()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.HIDE_TOOLBAR_TREE_NAVIGATION));
    }

    public void setToolbarTreeNavigationHidden(Boolean hidden)
    {
        setDefinitionDatum(Datum.HIDE_TOOLBAR_TREE_NAVIGATION, hidden);
    }
}
