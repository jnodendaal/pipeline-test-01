package com.pilog.t8.definition.ui.label;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LabelDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LABEL";
    public static final String DISPLAY_NAME = "Label";
    public static final String DESCRIPTION = "A component that displays a text String.";
    public static final String IDENTIFIER_PREFIX = "C_LABEL_";
    private enum Datum {TEXT,
                        HORIZONTAL_TEXT_ALIGNMENT,
                        VERTICAL_TEXT_ALIGNMENT,
                        FONT_DEFINITION,
                        FOREGROUND_COLOR};
    // -------- Definition Meta-Data -------- //

    public enum HorizontalTextAlignment {LEFT, CENTER, RIGHT};
    public enum VerticalTextAlignment {TOP, CENTER, BOTTOM};
    
    public T8LabelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text displayed on the label."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HORIZONTAL_TEXT_ALIGNMENT.toString(), "Horizontal Text Alignment", "Sets the alignment of the label's contents along the X axis.", HorizontalTextAlignment.LEFT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.VERTICAL_TEXT_ALIGNMENT.toString(), "Vertical Text Alignment", "Sets the alignment of the label's contents along the Y axis.", VerticalTextAlignment.CENTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FONT_DEFINITION.toString(), "Font", "The font to use for the label text."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.COLOR_HEX, Datum.FOREGROUND_COLOR.toString(), "Foreground Color", "The color of the label foreground.", null));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FONT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FontDefinition.TYPE_IDENTIFIER));
        else if (Datum.HORIZONTAL_TEXT_ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(HorizontalTextAlignment.values());
        else if (Datum.VERTICAL_TEXT_ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(VerticalTextAlignment.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        
        constructor = Class.forName("com.pilog.t8.ui.label.T8Label").getConstructor(T8LabelDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8LabelAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8LabelAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }   

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT.toString(), text);
    }  
    
    public HorizontalTextAlignment getHorizontalTextAlignment()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.HORIZONTAL_TEXT_ALIGNMENT.toString());
        return value != null ? HorizontalTextAlignment.valueOf(value) : HorizontalTextAlignment.LEFT;
    }

    public void setHorizontalTextAlignment(HorizontalTextAlignment alignment)
    {
        setDefinitionDatum(Datum.HORIZONTAL_TEXT_ALIGNMENT.toString(), alignment.toString());
    }
    
    public VerticalTextAlignment getVerticalTextAlignment()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.VERTICAL_TEXT_ALIGNMENT.toString());
        return value != null ? VerticalTextAlignment.valueOf(value) : VerticalTextAlignment.CENTER;
    }

    public void setVerticalTextAlignment(VerticalTextAlignment alignment)
    {
        setDefinitionDatum(Datum.VERTICAL_TEXT_ALIGNMENT.toString(), alignment.toString());
    }
    
    public T8FontDefinition getFontDefinition()
    {
        return (T8FontDefinition)getDefinitionDatum(Datum.FONT_DEFINITION.toString());
    }

    public void setFontDefinition(T8FontDefinition fontDefinition)
    {
         setDefinitionDatum(Datum.FONT_DEFINITION.toString(), fontDefinition);
    }
    
    public Color getForegroundColor()
    {
        return T8DefinitionDatumUtilities.getColor((String)getDefinitionDatum(Datum.FOREGROUND_COLOR.toString()));
    }
    
    public void setForegroundColor(Color foregroundColor)
    {
        setDefinitionDatum(Datum.FOREGROUND_COLOR.toString(), T8DefinitionDatumUtilities.getColorHex(foregroundColor));
    }
}
