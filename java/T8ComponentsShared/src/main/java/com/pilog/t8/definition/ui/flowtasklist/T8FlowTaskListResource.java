package com.pilog.t8.definition.ui.flowtasklist;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8FlowTaskList and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8FlowTaskListResource
{
    public static ArrayList<T8ComponentEventDefinition> events;
    public static ArrayList<T8ComponentOperationDefinition> operations;

    // These should NOT be changed, only added to.
    public static final String EVENT_TASK_CLAIMED = "$CE_TASK_CLAIMED";
    public static final String EVENT_TASK_CONTINUED = "$CE_TASK_CONTINUED";
    public static final String EVENT_TASK_FUNCTIONALITY_EXECUTION_UPDATED = "$CE_TASK_FUNCTIONALITY_EXECUTION_UPDATED";
    public static final String EVENT_TASK_FUNCTIONALITY_EXECUTION_ENDED = "$CE_TASK_FUNCTIONALITY_EXECUTION_ENDED";
    public static final String OPERATION_FILTER_TASK_LIST = "$CO_FILTER_TASK_LIST";
    public static final String PARAMETER_TASK_IDENTIFIER = "$P_TASK_IDENTIFIER";
    public static final String PARAMETER_INCLUDE_CLAIMED_TASKS = "$P_INCLUDE_CLAIMED_TASKS";
    public static final String PARAMETER_INCLUDE_UNCLAIMED_TASKS = "$P_INCLUDE_UNCLAIMED_TASKS";
    public static final String PARAMETER_FLOW_INSTANCE_IDENTIFIER = "$P_FLOW_INSTANCE_IDENTIFIER";
    public static final String PARAMETER_TASK_INSTANCE_IDENTIFIER = "$P_TASK_INSTANCE_IDENTIFIER";
    public static final String PARAMETER_TASK_OBJECT = "$P_TASK_OBJECT";

    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TASK_CLAIMED);
        newEventDefinition.setMetaDisplayName("Task Claimed");
        newEventDefinition.setMetaDescription("This event occurs after a task is selected by the user and successfully claimed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_INSTANCE_IDENTIFIER, "Task Instance Identifier", "The Instance Identifier of the task that was claimed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance Identifier", "The Instance Identifier of the flow to which the task that was claimed, belongs.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TASK_CONTINUED);
        newEventDefinition.setMetaDisplayName("Task Continued");
        newEventDefinition.setMetaDescription("This event occurs after a task is selected by the user to continue with.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_INSTANCE_IDENTIFIER, "Task Instance Identifier", "The Instance Identifier of the task that was selected.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance Identifier", "The Instance Identifier of the flow to which the task that was selected, belongs.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TASK_FUNCTIONALITY_EXECUTION_UPDATED);
        newEventDefinition.setMetaDisplayName("Task Functionality Execution Updated");
        newEventDefinition.setMetaDescription("This event occurs whenever the task functionality reports an update.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_INSTANCE_IDENTIFIER, "Task Instance Identifier", "The Instance Identifier of the task being executed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance Identifier", "The Instance Identifier of the flow to which the task being executed, belongs.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TASK_FUNCTIONALITY_EXECUTION_ENDED);
        newEventDefinition.setMetaDisplayName("Task Functionality Execution Ended");
        newEventDefinition.setMetaDescription("This event occurs after a task functionality ends.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_INSTANCE_IDENTIFIER, "Task Instance Identifier", "The Instance Identifier of the task that was executed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance Identifier", "The Instance Identifier of the flow to which the task that was executed, belongs.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_FILTER_TASK_LIST);
        newOperationDefinition.setMetaDisplayName("Filter Task List");
        newOperationDefinition.setMetaDescription("This operations fiters the task list according to the specified criteria.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_IDENTIFIER, "Task Identifier", "The identifier of the task type to include.  A null value indicates that all tasks types should be included.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_CLAIMED_TASKS, "Include Claimed Tasks", "A Boolean flag indicating whether or not claimed tasks should be included.", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_UNCLAIMED_TASKS, "Include Idle Processes", "A Boolean flag indicating whether or not unclaimed tasks should be included.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}
