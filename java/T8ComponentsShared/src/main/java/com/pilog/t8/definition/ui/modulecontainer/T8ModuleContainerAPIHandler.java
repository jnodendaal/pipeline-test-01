package com.pilog.t8.definition.ui.modulecontainer;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8SubModuleContainer and responds to specific requests 
 * regarding these definitions.  It is extremely important that only new 
 * definitions are added to this class and none of the old definitions removed 
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ModuleContainerAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_UNLOAD_MODULE = "$CO_UNLOAD_MODULE";
    public static final String OPERATION_RELOAD_MODULE = "$CO_RELOAD_MODULE";
    public static final String OPERATION_LOAD_MODULE = "$CO_LOAD_MODULE";
    
    public static final String PARAMETER_MAPPING_IDENTIFIER = "$P_MAPPING_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions(T8Definition parentDefinition)
    {
        return null;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions(T8Definition parentDefinition)
    {
        T8ComponentOperationDefinition newOperationDefinition;
        ArrayList<T8ComponentOperationDefinition> operations;
        
        operations = new ArrayList<T8ComponentOperationDefinition>();
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_MODULE);
        newOperationDefinition.setMetaDisplayName("Load Module");
        newOperationDefinition.setMetaDescription("This operations loads a sub-module into the container.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MAPPING_IDENTIFIER, "Mapping", "The identifier of the sub-module mapping definition to use for the loading of the sub-module.", T8DataType.DEFINITION_IDENTIFIER));
        T8DefinitionUtilities.assignDefinitionParent(parentDefinition, newOperationDefinition); // Each operation is assigned specifically to its parent, they cannot exist statically like other component definitions.
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_RELOAD_MODULE);
        newOperationDefinition.setMetaDisplayName("Reload Module");
        newOperationDefinition.setMetaDescription("This operations reloads the sub-module.");
        T8DefinitionUtilities.assignDefinitionParent(parentDefinition, newOperationDefinition); // Each operation is assigned specifically to its parent, they cannot exist statically like other component definitions.        
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_UNLOAD_MODULE);
        newOperationDefinition.setMetaDisplayName("Unload Module");
        newOperationDefinition.setMetaDescription("This operations unloads the currently loaded sub-module (if any is loaded).");
        T8DefinitionUtilities.assignDefinitionParent(parentDefinition, newOperationDefinition); // Each operation is assigned specifically to its parent, they cannot exist statically like other component definitions.        
        operations.add(newOperationDefinition);
        
        return operations;
    }
}
