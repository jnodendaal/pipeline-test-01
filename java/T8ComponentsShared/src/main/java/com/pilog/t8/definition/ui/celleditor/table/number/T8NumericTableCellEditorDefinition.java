/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.ui.celleditor.table.number;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.celleditor.T8TableCellEditorDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8NumericTableCellEditorDefinition extends T8ComponentDefinition implements T8TableCellEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_EDITOR_NUMERIC";
    public static final String DISPLAY_NAME = "Numeric Cell Editor";
    public static final String DESCRIPTION = "A cell editor that allows for the entering of data in valid numeric format.";
    private enum Datum {TARGET_ENTITY_IDENTIFIER,
                        TARGET_FIELD_IDENTIFIER,
                        TYPE,
                        FORMAT_PATTERN,
                        USE_STRICT_FORMATTING};
    // -------- Definition Meta-Data -------- //

    public enum FormattingTypes
    {
        GENERAL,
        CURRENCY,
        INTEGER,
        PERCENTAGE
    }

    public T8NumericTableCellEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_ENTITY_IDENTIFIER.toString(), "Target Entity", "The data entity that will be modified/acted on by this cell editor.").addDatumDependency(new T8DefinitionDatumDependency(T8TableDefinition.TYPE_IDENTIFIER, T8TableDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_FIELD_IDENTIFIER.toString(), "Target Field", "The data entity field that will be modified/acted on by this cell editor.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.TARGET_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TYPE.toString(), "Type", "The type of formatting that will be applied", FormattingTypes.GENERAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FORMAT_PATTERN.toString(), "Format Pattern", "The pattern that will be applied to validate formatting.", "#.0#"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USE_STRICT_FORMATTING.toString(), "Use Strict Formatting", "If this option is set then better number checking will be performed.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.TYPE.toString().equals(datumIdentifier)) return createStringOptions(FormattingTypes.values());
        else if (Datum.TARGET_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TARGET_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String targetEntityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            targetEntityId = getTargetEntityIdentifier();
            if (targetEntityId != null)
            {
                T8DataEntityDefinition targetEntityDefinition;

                targetEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), targetEntityId);

                if (targetEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> targetFieldDefinitions;

                    targetFieldDefinitions = targetEntityDefinition.getFieldDefinitions();
                    for (T8DataEntityFieldDefinition targetFieldDefinition : targetFieldDefinitions)
                    {
                        optionList.add(new T8DefinitionDatumOption(targetFieldDefinition.getPublicIdentifier(), targetFieldDefinition.getPublicIdentifier()));
                    }

                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }


    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            return (T8Component)Class.forName("com.pilog.t8.ui.celleditor.table.numeric.T8NumericTableCellEditor").getConstructor(T8NumericTableCellEditorDefinition.class, T8ComponentController.class).newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<T8ComponentEventDefinition>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<T8ComponentOperationDefinition>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getTargetEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER.toString());
    }

    public void setTargetEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getTargetFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_FIELD_IDENTIFIER.toString());
    }

    public void setTargetFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getFormatPattern()
    {
        return (String)getDefinitionDatum(Datum.FORMAT_PATTERN.toString());
    }

    public void setFormatPattern(String formatPattern)
    {
        setDefinitionDatum(Datum.FORMAT_PATTERN.toString(), formatPattern);
    }
    public FormattingTypes getNumberFormatType()
    {
        return FormattingTypes.valueOf((String)(getDefinitionDatum(Datum.TYPE.toString())));
    }

    public void setNumberFormatType(FormattingTypes numberFormatType)
    {
        setDefinitionDatum(Datum.TYPE.toString(), numberFormatType.toString());
    }

    public Boolean useStrictFormatting()
    {
        return (Boolean)(getDefinitionDatum(Datum.USE_STRICT_FORMATTING.toString()));
    }

    public void setUseStrictFormatting(Boolean useStrictFormatting)
    {
        setDefinitionDatum(Datum.USE_STRICT_FORMATTING.toString(), useStrictFormatting);
    }
}
