package com.pilog.t8.definition.ui.filter.panel.datasource;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelListItemDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FILTER_PANEL_LIST_ITEMS";
    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_LIST_ITEM";
    public static final String DISPLAY_NAME = "List Item";
    public static final String DESCRIPTION = "List Item";
    public static final String IDENTIFIER_PREFIX = "LIST_ITEM_";
    public enum Datum
    {
        ITEM_VALUE,
        ITEM_DISPLAY,
        ICON_IDENTIFIER,
        CUSTOM_CRITERIA
    };
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;

    public T8FilterPanelListItemDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {

        ArrayList<T8DefinitionDatumType> datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ITEM_VALUE.toString(), "Item Value", "The value that will be used in the filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ITEM_DISPLAY.toString(), "Item Display", "Teh value that will be displayed on the list component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this button's text."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.CUSTOM_CRITERIA.toString(), "Custom Filter Criteria", "A Custom filter criteria that will be used. This will override the default functionality of creating a filter criterion with the value field."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        }
        else if (Datum.CUSTOM_CRITERIA.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        }
        else
        {
            return null;
        }
    }

    public String getItemValue()
    {
        return (String) getDefinitionDatum(Datum.ITEM_VALUE.toString());
    }

    public void setItemValue(String value)
    {
        setDefinitionDatum(Datum.ITEM_VALUE.toString(), value);
    }

    public String getItemDisplay()
    {
        return (String) getDefinitionDatum(Datum.ITEM_DISPLAY.toString());
    }

    public void setItemDisplay(String display)
    {
        setDefinitionDatum(Datum.ITEM_DISPLAY.toString(), display);
    }

    public String getIconIdentifier()
    {
        return (String) getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    public T8DataFilterDefinition getCustomFilterDefinition()
    {
        return (T8DataFilterDefinition) getDefinitionDatum(Datum.CUSTOM_CRITERIA.toString());
    }

    public void setCustomFilterDefinition(T8DataFilterDefinition criteriaDefinition)
    {
        setDefinitionDatum(Datum.CUSTOM_CRITERIA.toString(), criteriaDefinition);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }
}
