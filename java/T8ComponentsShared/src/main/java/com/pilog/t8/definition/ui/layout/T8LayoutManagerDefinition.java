package com.pilog.t8.definition.ui.layout;

import java.awt.LayoutManager;

/**
 * @author Hennie Brink
 */
public interface T8LayoutManagerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_LAYOUT_DEFINITION";
    public static final String IDENTIFIER_PREFIX = "UI_LAYOUT_";
    // -------- Definition Meta-Data -------- //

    public  LayoutManager constructLayoutManager() throws Exception;
}
