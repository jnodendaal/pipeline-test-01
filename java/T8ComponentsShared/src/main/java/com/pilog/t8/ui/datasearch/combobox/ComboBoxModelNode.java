package com.pilog.t8.ui.datasearch.combobox;

import java.io.Serializable;

/**
 * @author Hennie Brink
 */
public interface ComboBoxModelNode extends Serializable
{
    public String getDisplayString();
    public String getDescription();

    public boolean isSelected();
    public void setSelected(boolean selected);
}
