/**
 * Created on 10 Sep 2015, 11:33:29 AM
 */
package com.pilog.t8.definition.ui.checkbox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author Gavin Boshoff
 */
public class T8MultiSelectionCheckBoxGroupAPIHandler
{
    // These should NOT be changed, only added to.
    // Component event identifiers
    public static final String EVENT_SELECTION_CHANGED = "$CE_SELECTION_CHANGED";
    // Component peration identifiers
    public static final String OPERATION_ENABLE_GROUP = "$CO_ENABLE_GROUP";
    public static final String OPERATION_DISABLE_GROUP = "$CO_DISABLE_GROUP";
    public static final String OPERATION_SET_SELECTED = "$CO_SET_SELECTED";
    public static final String OPERATION_GET_SELECTED = "$CO_GET_SELECTED";
    public static final String OPERATION_CLEAR_SELECTION = "$CO_CLEAR_SELECTION";
    // Component event/operation parameter identifiers
    public static final String PARAMETER_SELECTED_IDENTIFIER_LIST = "$P_SELECTED_IDENTIFIER_LIST";

    public static ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when one of the checkboxes are selected and the selection is different from the previously selected checkboxes.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED_IDENTIFIER_LIST, "Child Identifier", "The list of identifiers for the currently selected checkboxes.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        events.add(newEventDefinition);

        return events;
    }

    public static ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ENABLE_GROUP);
        newOperationDefinition.setMetaDisplayName("Enable Group");
        newOperationDefinition.setMetaDescription("This operations enables the checkbox group.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_GROUP);
        newOperationDefinition.setMetaDisplayName("Disable Group");
        newOperationDefinition.setMetaDescription("This operations disables the checkbox group.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED);
        newOperationDefinition.setMetaDisplayName("Set Selected");
        newOperationDefinition.setMetaDescription("Specifies the list of checkbox identifiers to be selected.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED_IDENTIFIER_LIST, "Child Identifiers", "The identifiers for the selected checkboxes to be set selected.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED);
        newOperationDefinition.setMetaDisplayName("Get Selected");
        newOperationDefinition.setMetaDescription("Specifies the list of checkbox identifiers that are currently selected.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED_IDENTIFIER_LIST, "Child Identifiers", "The identifiers of the currently selected checkboxes.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_SELECTION);
        newOperationDefinition.setMetaDisplayName("Clear Selection");
        newOperationDefinition.setMetaDescription("Resets the selection so that no item is selected in the group.");
        operations.add(newOperationDefinition);

        return operations;
    }
}