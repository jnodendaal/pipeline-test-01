package com.pilog.t8.definition.ui.entityeditor;

/**
 * @author Bouwer du Preez
 */
public interface T8DataEntityFieldEditorDefinition extends T8DataEntityEditorDefinition
{
    public String getEditorDataEntityFieldIdentifier();
    public void setEditorDataEntityFieldIdentifier(String identifier);
}
