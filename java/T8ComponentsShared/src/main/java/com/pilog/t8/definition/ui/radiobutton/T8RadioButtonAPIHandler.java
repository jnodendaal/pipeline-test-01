package com.pilog.t8.definition.ui.radiobutton;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8RadioButton and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8RadioButtonAPIHandler
{
    // These should NOT be changed, only added to.
    // Component event identifiers
    public static final String EVENT_BUTTON_SELECTED = "$CE_BUTTON_SELECTED";
    // Component operation identifiers
    public static final String OPERATION_ENABLE_BUTTON = "$CO_ENABLE_BUTTON";
    public static final String OPERATION_DISABLE_BUTTON = "$CO_DISABLE_BUTTON";
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_SET_SELECTED = "$CO_SET_SELECTED";
    // Component event/operation parameter identifiers
    public static final String PARAMETER_TEXT = "$P_TEXT";
    public static final String PARAMETER_SELECTED = "$P_SELECTED";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_BUTTON_SELECTED);
        newEventDefinition.setMetaDisplayName("Button Selected");
        newEventDefinition.setMetaDescription("This event occurs when the button is selected.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ENABLE_BUTTON);
        newOperationDefinition.setMetaDisplayName("Enable Button");
        newOperationDefinition.setMetaDescription("This operations enables the button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_BUTTON);
        newOperationDefinition.setMetaDisplayName("Disable Button");
        newOperationDefinition.setMetaDescription("This operations disables the button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Button Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed on the button.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the button.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED);
        newOperationDefinition.setMetaDisplayName("Set Button Selection State");
        newOperationDefinition.setMetaDescription("Sets whether or not the specified button is selected.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED, "Selected", "The boolen for the selection state of the radio button.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
