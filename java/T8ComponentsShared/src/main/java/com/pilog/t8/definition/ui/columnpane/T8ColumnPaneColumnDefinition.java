package com.pilog.t8.definition.ui.columnpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ColumnPaneColumnDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_COLUMN_PANE_COLUMN";
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_COLUMN_PANE_COLUMN";
    public static final String DISPLAY_NAME = "Column";
    public static final String DESCRIPTION = "A column in a column pane container to which other UI components can be added.";
    public static final String IDENTIFIER_PREFIX = "PANE_COLUMN_";
    public enum Datum {PREFERRED_WIDTH,
                       WEIGHT,
                       COMPONENT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ColumnPaneColumnDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PREFERRED_WIDTH.toString(), "Preferred Width", "The fixed width (preferred) of this column.  If 0, the column will be freely resized according to its weight.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.WEIGHT.toString(), "Weight", "The weight of the column.  All available space in the container will be divided between the columns according to their weights (after preferred widths have been taken into account).  Column weights must add up to 1.0.", 0.0));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COMPONENT_DEFINITIONS.toString(), "Components", "The components stacked into this column in sequence from top to bottom."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public List<T8ComponentDefinition> getComponentDefinitions()
    {
        return (List<T8ComponentDefinition>)getDefinitionDatum(Datum.COMPONENT_DEFINITIONS.toString());
    }

    public void setComponentDefinitions(List<T8ComponentDefinition> componentDefinitions)
    {
        setDefinitionDatum(Datum.COMPONENT_DEFINITIONS.toString(), componentDefinitions);
    }
    
    public int getPreferredWidth()
    {
        Integer width;
        
        width = (Integer)getDefinitionDatum(Datum.PREFERRED_WIDTH.toString());
        return width != null ? width : 0;
    }
    
    public void setPreferredWidth(int width)
    {
        setDefinitionDatum(Datum.PREFERRED_WIDTH.toString(), width);
    }
    
    public double getWeight()
    {
        Double weight;
        
        weight = (Double)getDefinitionDatum(Datum.WEIGHT.toString());
        return weight != null ? weight : 0.0;
    }
    
    public void setWeight(double weight)
    {
        setDefinitionDatum(Datum.WEIGHT.toString(), weight);
    }
}
