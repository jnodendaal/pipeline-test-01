package com.pilog.t8.definition.ui.datasearch.combobox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SEARCH = "$CE_SEARCH";

    public static final String COMPONENT_OPERATION_CLEAR_DATA_SOURCE_FILTER = "$CO_CLEAR_DATA_SOURCE_FILTER";
    public static final String COMPONENT_OPERATION_SET_DATA_SOURCE_FILTER = "$CO_SET_DATA_SOURCE_FILTER";
    public static final String COMPONENT_OPERATION_SET_SELECTED_VALUES = "$CO_SET_SELECTED_VALUES";
    public static final String COMPONENT_OPERATION_GET_SELECTED_VALUES = "$CO_GET_SELECTED_VALUES";
    public static final String COMPONENT_OPERATION_GET_DATA_FILTER = "$OC_GET_DATA_FILTER";
    public static final String COMPONENT_OPERATION_REFRESH_DATA_SOURCE = "$OC_REFRESH_DATA_SOURCE";
    public static final String COMPONENT_OPERATION_CLEAR_DATA_FILTER = "$OC_CLEAR_DATA_FILTER";

    public static final String SERVER_OPERATION_RETRIEVE_FILTER_DATA = "@OS_DATA_SEARCH_COMBO_BOX_RETRIEVE_MODEL_DATA";

    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_APPLY_FILTER = "$CP_APPLY_FILTER";
    public static final String PARAMETER_DATA_SOURCE_IDENTIFIER = "$P_DATA_SOURCE_IDENTIFIER";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_SEARCH_QUERY = "$P_SEARCH_QUERY";
    public static final String PARAMETER_RETRIEVAL_SIZE = "$P_RETRIEVAL_SIZE";
    public static final String PARAMETER_DATA_MODEL_ROOT_NODE = "$P_DATA_MODEL_ROOT_NODE";
    public static final String PARAMETER_REFRESH_DATA_SOURCE = "$P_REFRESH_DATA_SOURCE";
    public static final String PARAMETER_VALUE_LIST = "$P_VALUE_LIST";
    public static final String PARAMETER_CLEAR_SELECTION = "$P_CLEAR_SELECTION";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SEARCH);
        newEventDefinition.setMetaDisplayName("Search");
        newEventDefinition.setMetaDescription("This event occurs when the user clicks the search button.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Search Data Filter", "The data filter object that will be produced by this search as output.", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_CLEAR_DATA_SOURCE_FILTER);
        newOperationDefinition.setMetaDisplayName("Clear Data Source Filter");
        newOperationDefinition.setMetaDescription("Clears any data source filter which may have been set. If none has been set, this has no effect.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA_SOURCE, "Refresh Data Source", "Specifies whether or not the combo box data should be refreshed", T8DataType.BOOLEAN, true));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_SET_DATA_SOURCE_FILTER);
        newOperationDefinition.setMetaDisplayName("Set Data Source Filter");
        newOperationDefinition.setMetaDescription("Sets an additional filter on the data source used for the selection of filter values.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The T8 Data Filter that will be used to filter the data within the combo box", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA_SOURCE, "Refresh Data Source", "Specifies whether or not the combo box data should be refreshed with the filter", T8DataType.BOOLEAN, true));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_SET_SELECTED_VALUES);
        newOperationDefinition.setMetaDisplayName("Set Selected Values");
        newOperationDefinition.setMetaDescription("Sets the values to be selected, with an option to clear the current selection. The new data filter is returned.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE_LIST, "Value List", "Specifies the list of values to be set as selected on the combo box", T8DataType.LIST));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CLEAR_SELECTION, "Clear Selection", "Specifies whether or not the current selection will be cleared before updating the selection", T8DataType.BOOLEAN));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The T8 Data Filter that will be created based on the user selected values", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_GET_SELECTED_VALUES);
        newOperationDefinition.setMetaDisplayName("Get Selected Values");
        newOperationDefinition.setMetaDescription("Get the selected Values from comboBox");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE_LIST, "Value List", "Specifies the list of values to be returned from the combo box.", T8DataType.LIST));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_GET_DATA_FILTER);
        newOperationDefinition.setMetaDisplayName("Get Data Filter");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The T8 Data Filter that will be created based on the user selected values", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_REFRESH_DATA_SOURCE);
        newOperationDefinition.setMetaDisplayName("Refresh Data Source");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_CLEAR_DATA_FILTER);
        newOperationDefinition.setMetaDisplayName("Clear Data Filter");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_APPLY_FILTER, "Apply Cleared Filter", "Specifies whether or not to refresh the data source used by this filter. Defaults to true if not specified.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_RETRIEVE_FILTER_DATA);
            definition.setMetaDescription("Data Search Combo Box Data Retrieval");
            definition.setClassName("com.pilog.t8.ui.datasearch.combobox.server.T8DataSearchComboBoxServerOperations$RetrieveFilterData");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project to which the combo box belongs.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_SOURCE_IDENTIFIER, "Data Source Identifier", "The identifier of the data source used for the data in the combo box", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH_QUERY, "Search Query", "A string value used as a quick filter for the data displayed in the combo box", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RETRIEVAL_SIZE, "Retrieval Size", "The number of values to be returned", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Source Filter", "A data filter which is used to pre-filter the displayed filter values", T8DataType.CUSTOM_OBJECT, true));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_MODEL_ROOT_NODE, "Data Model Root Node", "The root node containing all the filter values to be displayed in the combo box", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
