package com.pilog.t8.definition.ui.wizard;

import com.pilog.t8.ui.T8ComponentController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8WizardStep implements Serializable
{
    private final String identifier;
    private String name;
    private String description;
    private Icon icon;
    private boolean selected;
    private boolean completed;
    private boolean active;
    private boolean backAction;
    private boolean finishAction;
    private boolean cancelAction;
    private String initializationScriptIdentifier;
    private String finalizationScriptIdentifier;
    private final List<T8WizardStep> subSteps;
    private T8WizardStep parentStep;

    public T8WizardStep(String identifier)
    {
        this.identifier = identifier;
        this.subSteps = new ArrayList<T8WizardStep>();
    }

    public T8WizardStep(T8WizardStepDefinition definition, T8ComponentController controller)
    {
        this(definition.getIdentifier());
        this.name = definition.getStepName();
        this.description = definition.getStepDescription();
        this.icon = definition.getStepIcon();
        this.active = true;
        this.selected = false;
        this.completed = false;
        this.backAction = definition.isBackAction();
        this.finishAction = definition.isFinishAction();
        this.cancelAction = definition.isCancelAction();
        this.initializationScriptIdentifier = definition.getInitializationScriptIdentifier();
        this.finalizationScriptIdentifier = definition.getFinalizationScriptIdentifier();

        // Add sub-steps.
        for (T8WizardStepDefinition stepDefinition : definition.getStepDefinitions())
        {
            addSubStep(stepDefinition.createWizardStep(controller));
        }
    }

    public String getIdentifier()
    {
        return identifier;
    }

    void setParentStep(T8WizardStep parentStep)
    {
        this.parentStep = parentStep;
    }

    public T8WizardStep getParentStep()
    {
        return parentStep;
    }

    public final void addSubStep(T8WizardStep step)
    {
        step.setParentStep(this);
        this.subSteps.add(step);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Icon getIcon()
    {
        return icon;
    }

    public void setIcon(Icon icon)
    {
        this.icon = icon;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public boolean isCompleted()
    {
        return completed;
    }

    public void setCompleted(boolean completed)
    {
        this.completed = completed;
    }

    public boolean isCategory()
    {
        return !subSteps.isEmpty();
    }

    public boolean isBackAction()
    {
        return backAction;
    }

    public void setBackAction(boolean backAction)
    {
        this.backAction = backAction;
    }

    public boolean isFinishAction()
    {
        return finishAction;
    }

    public void setFinishAction(boolean finishAction)
    {
        this.finishAction = finishAction;
    }

    public boolean isCancelAction()
    {
        return cancelAction;
    }

    public void setCancelAction(boolean cancelAction)
    {
        this.cancelAction = cancelAction;
    }

    public String getInitializationScriptIdentifier()
    {
        return initializationScriptIdentifier;
    }

    public void setInitializationScriptIdentifier(String initializationScriptIdentifier)
    {
        this.initializationScriptIdentifier = initializationScriptIdentifier;
    }

    public String getFinalizationScriptIdentifier()
    {
        return finalizationScriptIdentifier;
    }

    public void setFinalizationScriptIdentifier(String validationScriptIdentifier)
    {
        this.finalizationScriptIdentifier = validationScriptIdentifier;
    }

    public List<T8WizardStep> getSubSteps()
    {
        return new ArrayList<>(subSteps);
    }

    public T8WizardStep getStep(String identifier)
    {
        if (this.identifier.equals(identifier)) return this;
        else
        {
            for (T8WizardStep subStep : subSteps)
            {
                T8WizardStep foundStep;

                foundStep = subStep.getStep(identifier);
                if (foundStep != null) return foundStep;
            }
        }

        return null;
    }

    public boolean hasSelectableNextStep()
    {
        return getNextSelectableStep() != null;
    }

    public boolean hasSelectablePreviousStep()
    {
        return getPreviousCompletedStep() != null;
    }

    public T8WizardStep getNextSelectableStep()
    {
        T8WizardStep nextStep;

        // First get the next step.
        nextStep = getNextStep();
        if (nextStep != null)
        {
            // If the next step is active and selectable, return it.
            if ((nextStep.isActive()) && (!nextStep.isCategory()))
            {
                return nextStep;
            }
            else return nextStep.getNextSelectableStep();
        }
        else return null;
    }

    public T8WizardStep getPreviousCompletedStep()
    {
        T8WizardStep previousStep;

        // First get the previous step.
        previousStep = getPreviousStep();
        if (previousStep != null)
        {
            // If the previous step is active and selectable, return it.
            if ((previousStep.isActive()) && (!previousStep.isCategory()) && (previousStep.isCompleted()))
            {
                return previousStep;
            }
            else return previousStep.getPreviousCompletedStep();
        }
        else return null;
    }

    public T8WizardStep getNextSiblingStep()
    {
        if (parentStep != null)
        {
            // Get the next sibling of this step.
            return parentStep.getNextSubStep(this);
        }
        else return null;
    }

    public T8WizardStep getNextStep()
    {
        // If this step has substeps, the first one is the next step.
        if (subSteps.size() > 0)
        {
            return subSteps.get(0);
        }
        else if (parentStep != null) // No substeps, so we need to find the next sibling of this step.
        {
            T8WizardStep nextStep;

            // Get the next sibling of this step.
            nextStep = parentStep.getNextSubStep(this);
            if (nextStep != null)
            {
                return nextStep;
            }
            else // No sibling found for this step, so get the next sibling of the parent step.
            {
                return parentStep.getNextSiblingStep();
            }
        }
        else return null;
    }

    public T8WizardStep getLastDescendantStep()
    {
        if (subSteps.size() > 0)
        {
            T8WizardStep lastSubStep;
            T8WizardStep lastDescendant;

            lastSubStep = subSteps.get(subSteps.size()-1);
            lastDescendant = lastSubStep.getLastDescendantStep();
            return lastDescendant != null ? lastDescendant : lastSubStep;
        }
        else return null;
    }

    public T8WizardStep getPreviousStep()
    {
        if (parentStep != null)
        {
            T8WizardStep previousSibling;

            // Get the previous sibling of this step from the parent.
            previousSibling = parentStep.getPreviousSubStep(this);
            if (previousSibling != null)
            {
                T8WizardStep previousStep;

                // We need to find the lowest descendant.
                previousStep = previousSibling.getLastDescendantStep();
                return previousStep != null ? previousStep : previousSibling;
            }
            else return parentStep; // No previous sibling found, so the parent is the previous step.
        }
        else return null; // No parent, so we cannot go up the hierarchy, nor to the previous sibling.
    }

    public T8WizardStep getNextSubStep(T8WizardStep subStep)
    {
        int index;

        index = subSteps.indexOf(subStep);
        if (index > -1)
        {
            if (index < subSteps.size() - 1)
            {
                return subSteps.get(index + 1);
            }
            else return null;
        }
        else return null;
    }

    public T8WizardStep getPreviousSubStep(T8WizardStep subStep)
    {
        int index;

        index = subSteps.indexOf(subStep);
        if (index > 0)
        {
            return subSteps.get(index - 1);
        }
        else return null;
    }

    public List<T8WizardStep> getSubsequentSteps()
    {
        List<T8WizardStep> stepList;
        T8WizardStep nextStep;

        stepList = new ArrayList<T8WizardStep>();
        nextStep = getNextStep();
        while (nextStep != null)
        {
            stepList.add(nextStep);
            nextStep = nextStep.getNextStep();
        }

        return stepList;
    }
}
