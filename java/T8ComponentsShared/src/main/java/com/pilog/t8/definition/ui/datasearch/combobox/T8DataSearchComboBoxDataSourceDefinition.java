package com.pilog.t8.definition.ui.datasearch.combobox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBoxModelProvider;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public abstract class T8DataSearchComboBoxDataSourceDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_SEARCH_COMBO_BOX_DATA_SOURCE";
    public static final String GROUP_NAME = "UI Filter Combobox Data Source";
    public static final String GROUP_DESCRIPTION = "A data source employed by a UI combobox as part of a filter bar.";
    public static final String STORAGE_PATH = "/data_search_data_sources";
    public static final String IDENTIFIER_PREFIX = "SEARCH_DS_";
    // -------- Definition Meta-Data -------- //

    public enum Datum
    {
        SEARCH_VISIBLE,
        OPTIONS_VISIBLE,
        SELECTION_MODE
    };
    // -------- Definition Meta-Data -------- //

    public T8DataSearchComboBoxDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SEARCH_VISIBLE.toString(), "Search Visible", "If the search box should be displayed", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPTIONS_VISIBLE.toString(), "Options Visible", "If the options buttons should be displayed", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SELECTION_MODE.toString(), "Selection Mode", "The selection mode allowed for this combo box", SelectionMode.MULTI_SELECTION.toString(), T8DefinitionDatumOptionType.ENUMERATION));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SELECTION_MODE.toString().equals(datumIdentifier)) return createStringOptions(SelectionMode.values());
        else return null;
    }

    public abstract T8DataSearchComboBoxModelProvider createModelProvider(T8Context context);

    public Boolean isSearchVisible()
    {
        return (Boolean) getDefinitionDatum(Datum.SEARCH_VISIBLE.toString());
    }

    public void setSearchVisible(boolean visible)
    {
        setDefinitionDatum(Datum.SEARCH_VISIBLE.toString(), visible);
    }

    public Boolean isOptionsVisible()
    {
        return (Boolean) getDefinitionDatum(Datum.OPTIONS_VISIBLE.toString());
    }

    public void setOptionsVisible(boolean visible)
    {
        setDefinitionDatum(Datum.OPTIONS_VISIBLE.toString(), visible);
    }

    public SelectionMode getSelectionMode()
    {
        return SelectionMode.valueOf((String)getDefinitionDatum(Datum.SELECTION_MODE.toString()));
    }

    public void setSelectionMode(SelectionMode selectionMode)
    {
        setDefinitionDatum(Datum.SELECTION_MODE.toString(), selectionMode.toString());
    }

    public enum SelectionMode
    {
        SINGLE_SELECTION,
        MULTI_SELECTION,
        CATEGORY_SELECTION
    }
}
