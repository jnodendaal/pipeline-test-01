package com.pilog.t8.definition.ui.label;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Label and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8LabelAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_GET_TEXT = "$CO_GET_TEXT";
    public static final String OPERATION_SET_ICON = "$CO_SET_ICON";
    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_ICON_IDENTIFIER = "$CP_ICON_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Label Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed on the label.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the label.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_TEXT);
        newOperationDefinition.setMetaDisplayName("Get Label Text");
        newOperationDefinition.setMetaDescription("This operations returns the text displayed on the label.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value displayed on the label.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_ICON);
        newOperationDefinition.setMetaDisplayName("Set Icon");
        newOperationDefinition.setMetaDescription("Sets the icon on the label.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ICON_IDENTIFIER, "Icon Identifier", "The definition identifier of the icon to set on the label.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
