package com.pilog.t8.definition.ui.cellrenderer.table.label.formats;

import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererFormatDefinition;
import java.text.Format;
import java.text.MessageFormat;

/**
 * @author Hennie Brink
 */
public class MessageFormatDefinition extends T8LabelTableCellRendererFormatDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_LABEL_FORMATER_MESSAGE";
    public static final String DISPLAY_NAME = "Message Formatter";
    public static final String DESCRIPTION = "Formatter that will use format the value(s) with a message pattern";

    public enum Datum
    {
    };
    // -------- Definition Meta-Data -------- //
    public MessageFormatDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public Format getFormat()
    {
        MessageFormat format;

        format = new MessageFormat(getFormatPattern());
        return format;
    }
}
