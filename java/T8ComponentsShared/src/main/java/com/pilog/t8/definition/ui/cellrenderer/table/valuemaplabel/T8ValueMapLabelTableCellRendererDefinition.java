package com.pilog.t8.definition.ui.cellrenderer.table.valuemaplabel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.cellrenderer.T8LabelCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ValueMapLabelTableCellRendererDefinition extends T8LabelCellRendererDefinition implements T8TableCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_VALUE_MAP_LABEL";
    public static final String DISPLAY_NAME = "Value Map Label Cell Renderer";
    public static final String DESCRIPTION = "A cell renderer that uses a map of pre-calculated values to determine the appropriate display string for cell data.";
    public enum Datum {VALUE_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ValueMapLabelTableCellRendererDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION_DISPLAY_STRING_MAP, Datum.VALUE_MAPPING.toString(), "Mapping: Value Expression to Display String", "A map containing value expressions and the corresponding Display String for each."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.cellrenderer.table.valuemaplabel.T8ValueMapLabelTableCellRenderer").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }
    
    public Map<String, String> getValueMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.VALUE_MAPPING.toString());
    }
    
    public void setValueMapping(Map<String, String> valueMapping)
    {
        setDefinitionDatum(Datum.VALUE_MAPPING.toString(), valueMapping);
    }
}
