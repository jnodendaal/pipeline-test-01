package com.pilog.t8.definition.ui.textarea;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8TextArea and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8TextAreaAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_TEXT_CHANGED = "$CE_TEXT_CHANGED";
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_SET_EDITABLE = "$CO_SET_EDITABLE";
    public static final String OPERATION_GET_TEXT = "$CO_GET_TEXT";
    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_EDITABLE = "$CP_EDITABLE";
    public static final String OPERATION_APPEND_TEXT = "$CO_APPEND_TEXT";
    public static final String OPERATION_APPEND_LINE = "$CO_APPEND_LINE";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TEXT_CHANGED);
        newEventDefinition.setMetaDisplayName("Text Changed");
        newEventDefinition.setMetaDescription("This event occurs every time the text displayed in the text area is changed.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed in the text area.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text value to display in the text area.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_TEXT);
        newOperationDefinition.setMetaDisplayName("Get Text");
        newOperationDefinition.setMetaDescription("This operations returns the text displayed in the text area.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text value displayed in the text area.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_EDITABLE);
        newOperationDefinition.setMetaDisplayName("Sets component Editable");
        newOperationDefinition.setMetaDescription("This operation sets the components editable status.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_EDITABLE, "Editable", "If this text component should be editable.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_APPEND_TEXT);
        newOperationDefinition.setMetaDisplayName("Append Text");
        newOperationDefinition.setMetaDescription("Appends the specified text to the end of the existing text.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text to append to the text area.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_APPEND_LINE);
        newOperationDefinition.setMetaDisplayName("Append Line of Text");
        newOperationDefinition.setMetaDescription("Appends the specified text as a new line in the text area.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text to append as a new line in the text area.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        return operations;
    }
}
