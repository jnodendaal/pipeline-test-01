/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.ui.filter.panel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelDefinition extends T8PanelDefinition
{
    // -------- Definition Meta-Data -------- //

    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FILTER_PANEL";
    public static final String DISPLAY_NAME = "Filter Panel (Deprecated, Do not use!)";
    public static final String DESCRIPTION = "A Filter Panel that can be used to add filter components, allowing the user to filter results on a table or other compatible components";
    public static final String IDENTIFIER_PREFIX = "C_FILTER_PANEL_";
    public static final String VERSION = "0";
    public enum Datum
    {
        DATA_ENTITY_DEFINITION_IDENTIFIER,
        DEFAULT_DATA_FILTER,
        LAYOUT_MANAGER
    };
    // -------- Definition Meta-Data -------- //

    public enum FilterPanelLayouts
    {
        GRIDBAG,
        WRAPPED_FLOW
    }

    public T8FilterPanelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_DEFINITION_IDENTIFIER.toString(), "Data Entity Definition", "The data entity that will be used when constructing this filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DEFAULT_DATA_FILTER.toString(), "Pre-Filter", "The data filter that will be used as the initial Data Filter, all other filter options will be appended to this filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.LAYOUT_MANAGER.toString(), "Layout Manager", "The layout manager that will be used when constructing this filter panel.", FilterPanelLayouts.GRIDBAG.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            T8DefinitionContext definitionContext, String datumIdentifier)
            throws Exception
    {
        if (Datum.DATA_ENTITY_DEFINITION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        }
        else if (T8PanelDefinition.Datum.PANEL_SLOT_DEFINITIONS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> slots = super.getDatumOptions(definitionContext, datumIdentifier);
            slots.addAll(createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FilterPanelSlotDefinition.TYPE_IDENTIFIER)));
            return slots;
        } else if (Datum.DEFAULT_DATA_FILTER.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        } else if (Datum.LAYOUT_MANAGER.toString().equals(datumIdentifier))
        {
            return createStringOptions(FilterPanelLayouts.values());
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.filter.panel.T8FilterPanel").getConstructor(T8FilterPanelDefinition.class, T8ComponentController.class);
            return (T8Component) constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return super.getComponentInputParameterDefinitions();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FilterPanelAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FilterPanelAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return super.getChildComponentDefinitions();
    }

    public void setDataEntityDefinitionIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_DEFINITION_IDENTIFIER.toString(), identifier);
    }

    public String getDataEntityDefinitionIdentifier()
    {
        return (String) getDefinitionDatum(Datum.DATA_ENTITY_DEFINITION_IDENTIFIER.toString());
    }

    public T8DataFilterDefinition getPreFilter()
    {
        return (T8DataFilterDefinition) getDefinitionDatum(Datum.DEFAULT_DATA_FILTER.toString());
    }

    public void setPreFilter(T8DataFilterDefinition dataFilterDefinition)
    {
        setDefinitionDatum(Datum.DEFAULT_DATA_FILTER.toString(), dataFilterDefinition);
    }

    public FilterPanelLayouts getLayoutManager()
    {
        return FilterPanelLayouts.valueOf((String) getDefinitionDatum(Datum.LAYOUT_MANAGER.toString()));
    }

    public void setLayoutManager(FilterPanelLayouts manager)
    {
        setDefinitionDatum(Datum.LAYOUT_MANAGER.toString(), manager.toString());
    }
}
