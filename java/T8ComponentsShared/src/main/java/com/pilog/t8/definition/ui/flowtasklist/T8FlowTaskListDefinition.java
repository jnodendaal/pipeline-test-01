package com.pilog.t8.definition.ui.flowtasklist;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectGroupDefinition;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.list.T8ComponentListDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowTaskListDefinition extends T8ComponentListDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FLOW_TASK_LIST";
    public static final String DISPLAY_NAME = "Flow Task List";
    public static final String DESCRIPTION = "A component that displays the task list available to the currently logged in user and allows for new tasks to be claimed or claimed tasks to be continued.";
    public enum Datum
    {
        TASK_OBJECT_GROUP_ID,
        TASK_FUNCTIONALITY_ID,
        TASK_MENU_BACKGROUND_PAINTER_IDENTIFIER,
        TASK_LIST_BACKGROUND_PAINTER_IDENTIFIER,
        TASK_BACKGROUND_PAINTER_IDENTIFIER,
        TASK_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER,
        TASK_VERY_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition taskListBackgroundPainterDefinition;
    private T8PainterDefinition taskMenuBackgroundPainterDefinition;
    private T8PainterDefinition taskBackgroundPainterDefinition;
    private T8PainterDefinition taskHighPriorityBackgroundPainterDefinition;
    private T8PainterDefinition taskVeryHighPriorityBackgroundPainterDefinition;

    public T8FlowTaskListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_OBJECT_GROUP_ID.toString(), "Task Object Group", "The group of task objects that will be displayed by this task list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_FUNCTIONALITY_ID.toString(), "Task Functionality", "The functionality to execute when a task is started from the task list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Task Menu Background Painter", "The painter to use for painting the background of the task menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Task List Background Painter", "The painter to use for painting the background of the task list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Task Background Painter", "The painter to use for painting the background of the tasks in the task list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Task High Priority Background Painter", "The painter to use for painting the background of the tasks in the task list that are marked as high priority."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TASK_VERY_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Task Very High Priority Background Painter", "The painter to use for painting the background of the tasks in the task list that are marked as very high priority."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TASK_OBJECT_GROUP_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8TaskObjectGroupDefinition.TYPE_IDENTIFIER), true, "No Group");
        else if (Datum.TASK_FUNCTIONALITY_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER), true, "No Functionality");
        else if (Datum.TASK_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TASK_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TASK_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TASK_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TASK_VERY_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DefinitionManager definitionManager;
        String definitionId;

        super.initializeDefinition(context, inputParameters, configurationSettings);

        definitionManager = context.getServerContext().getDefinitionManager();

        definitionId = getTaskListBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            taskListBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getTaskMenuBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            taskMenuBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getTaskBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            taskBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getTaskHighPriorityBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            taskHighPriorityBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getTaskVeryHighPriorityBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            taskVeryHighPriorityBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.flowtasklist.T8FlowTaskList", new Class<?>[]{T8FlowTaskListDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FlowTaskListResource.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FlowTaskListResource.getOperationDefinitions();
    }

    public T8PainterDefinition getTaskListBackgroundPainterDefinition()
    {
        return taskListBackgroundPainterDefinition;
    }

    public T8PainterDefinition getTaskMenuBackgroundPainterDefinition()
    {
        return taskMenuBackgroundPainterDefinition;
    }

    public T8PainterDefinition getTaskBackgroundPainterDefinition()
    {
        return taskBackgroundPainterDefinition;
    }

    public T8PainterDefinition getTaskHighPriorityBackgroundPainterDefinition()
    {
        return taskHighPriorityBackgroundPainterDefinition;
    }

    public T8PainterDefinition getTaskVeryHighPriorityBackgroundPainterDefinition()
    {
        return taskVeryHighPriorityBackgroundPainterDefinition;
    }

    public String getTaskObjectGroupId()
    {
        return getDefinitionDatum(Datum.TASK_OBJECT_GROUP_ID);
    }

    public void setTaskObjectGroupId(String groupId)
    {
        setDefinitionDatum(Datum.TASK_OBJECT_GROUP_ID, groupId);
    }

    public String getTaskFunctionalityId()
    {
        return getDefinitionDatum(Datum.TASK_FUNCTIONALITY_ID);
    }

    public void setTaskFunctionalityId(String id)
    {
        setDefinitionDatum(Datum.TASK_FUNCTIONALITY_ID, id);
    }

    public String getTaskListBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setTaskListBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TASK_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getTaskMenuBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setTaskMenuBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TASK_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getTaskBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setTaskBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TASK_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getTaskHighPriorityBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setTaskHighPriorityBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TASK_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getTaskVeryHighPriorityBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TASK_VERY_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setTaskVeryHighPriorityBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TASK_VERY_HIGH_PRIORITY_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }
}
