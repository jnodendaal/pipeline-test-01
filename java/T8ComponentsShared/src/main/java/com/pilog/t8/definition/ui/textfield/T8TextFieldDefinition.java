package com.pilog.t8.definition.ui.textfield;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererFormatDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author Bouwer du Preez
 */
public class T8TextFieldDefinition extends T8ComponentDefinition
{
    private static final T8Logger logger = T8Log.getLogger(T8TextFieldDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TEXT_FIELD";
    public static final String DISPLAY_NAME = "Text Field";
    public static final String DESCRIPTION = "A component that displays field for entering text values.";
    public static final String IDENTIFIER_PREFIX = "C_TEXT_FIELD_";
    public enum Datum { EDITABLE,
                        BORDER_TYPE,
                        UNDECORATED,
                        FORMATTER,
                        REGEX_VALIDATION_PATTERN};
    public enum TFBorderType {NONE, DEFAULT, PULSATING};
    // -------- Definition Meta-Data -------- //

    public T8TextFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EDITABLE.toString(), "Editable", "If this field should be editable or not.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BORDER_TYPE.toString(), "Border Type", "The border type to use for this panel.", TFBorderType.PULSATING.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.UNDECORATED.toString(), "Undecorated (Deprecated)", "If set to true, the text area will have no border or background painted and will display only its content text on a transparent background.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FORMATTER.toString(), "Formatter", "The formatter that will be used to format the value of this field."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REGEX_VALIDATION_PATTERN.toString(), "Regex Validation Pattern", "A regular expression to be used for validation on the text field. Invalid input is highlighted."));

        return datumTypes;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(!isValidRegexSyntax()) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.REGEX_VALIDATION_PATTERN.toString(), "Invalid Regex Pattern Syntax", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BORDER_TYPE.toString().equals(datumIdentifier)) return createStringOptions(TFBorderType.values());
        else if (Datum.FORMATTER.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8LabelTableCellRendererFormatDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.textfield.T8TextField", new Class<?>[]{T8TextFieldDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TextFieldAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TextFieldAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public boolean isEditable()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.EDITABLE));
    }

    public void setEditable(Boolean editable)
    {
        setDefinitionDatum(Datum.EDITABLE, editable);
    }

    public TFBorderType getBorderType()
    {
        return TFBorderType.valueOf(getDefinitionDatum(Datum.BORDER_TYPE));
    }

    public void setBorderType(TFBorderType border)
    {
        setDefinitionDatum(Datum.BORDER_TYPE, border.toString());
    }

    public boolean isUndecorated()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.UNDECORATED));
    }

    public void setUndecorated(boolean undecorated)
    {
        setDefinitionDatum(Datum.UNDECORATED, undecorated);
    }

    //TODO: GBO - Why do we have a table cell renderer format for a text field???
    public T8LabelTableCellRendererFormatDefinition getFormatter()
    {
        return getDefinitionDatum(Datum.FORMATTER);
    }

    public void setFormatter(T8LabelTableCellRendererFormatDefinition formatter)
    {
        setDefinitionDatum(Datum.FORMATTER, formatter);
    }

    public String getRegexValidationPattern()
    {
        return getDefinitionDatum(Datum.REGEX_VALIDATION_PATTERN);
    }

    public void setRegexValidationPattern(String validationPattern)
    {
        setDefinitionDatum(Datum.REGEX_VALIDATION_PATTERN, validationPattern);
    }

    private boolean isValidRegexSyntax()
    {
        try
        {
            Pattern.compile(getRegexValidationPattern());
            return true;
        }
        catch (PatternSyntaxException pse)
        {
            logger.log("Invalid Pattern Found : " + getRegexValidationPattern(), pse);
            return false;
        }
    }
}
