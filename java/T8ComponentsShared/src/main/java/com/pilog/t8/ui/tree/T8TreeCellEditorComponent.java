package com.pilog.t8.ui.tree;

import com.pilog.t8.ui.T8Component;

/**
 * @author Bouwer du Preez
 */
public interface T8TreeCellEditorComponent extends T8Component
{
    public boolean singleClickEdit();
    
    public void addNodeEditorListener(T8TreeNodeEditorListener editorListener);
    public void removeNodeEditorListener(T8TreeNodeEditorListener editorListener);
    
    public void setEditorNode(T8TreeCellEditableComponent sourceComponent, T8TreeNode node, boolean selected, boolean focused, boolean editable, boolean expanded, boolean leaf, int row, int currentPage, int pageCount);
    public T8TreeNode getEditorNode();
}
