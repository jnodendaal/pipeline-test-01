package com.pilog.t8.definition.ui.menubar;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.layout.T8LayoutManagerDefinition;
import com.pilog.t8.definition.ui.menu.T8MenuDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MenuBarDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MENU_BAR";
    public static final String DISPLAY_NAME = "Menu Bar";
    public static final String DESCRIPTION = "A horizontal bar to which menus can be added.  The header of each menu is display on the menu bar and can be used to access the menu itself.";
    public static final String IDENTIFIER_PREFIX = "C_MENU_BAR_";
    public enum Datum {MENU_DEFINITIONS,
                       HEIGHT,
                       BORDER_TYPE,
                       BACKGROUND_PAINTER_IDENTIFIER,
                       LAYOUT_MANAGER};
    // -------- Definition Meta-Data -------- //

    public enum BorderType {NONE, BEVEL, ETCHED, LINE, TITLED, SHADOW, CONTENT};

    private T8PainterDefinition backgroundPainterDefinition;

    public T8MenuBarDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MENU_DEFINITIONS.toString(), "Menus", "The menus contained by this menu bar."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HEIGHT.toString(), "Height", "The height of the menu bar.  A value of 0 will reset the height to the default value.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BORDER_TYPE.toString(), "Border Type", "The border type to use for this panel.", BorderType.NONE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting the background of the entire menu bar component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.LAYOUT_MANAGER.toString(), "Layout Manager", "The layout manager to be used when laying out the components."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BORDER_TYPE.toString().equals(datumIdentifier)) return createStringOptions(BorderType.values());
        else if (Datum.MENU_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8MenuDefinition.GROUP_IDENTIFIER));
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.LAYOUT_MANAGER.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8LayoutManagerDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String painterId;

        painterId = getBackgroundPainterIdentifier();
        if (painterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.menubar.T8MenuBar").getConstructor(T8MenuBarDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8MenuBarAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8MenuBarAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList)getMenuDefinitions();
    }

    public BorderType getBorderType()
    {
        return BorderType.valueOf((String)getDefinitionDatum(Datum.BORDER_TYPE.toString()));
    }

    public void setBorderType(BorderType borderType)
    {
        setDefinitionDatum(Datum.BORDER_TYPE.toString(), borderType.toString());
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public int getHeight()
    {
        Integer height;

        height = (Integer)getDefinitionDatum(Datum.HEIGHT.toString());
        return height != null ? height : 0;
    }

    public void setHeight(int height)
    {
        setDefinitionDatum(Datum.HEIGHT.toString(), height);
    }

    public ArrayList<T8MenuDefinition> getMenuDefinitions()
    {
        return (ArrayList<T8MenuDefinition>)getDefinitionDatum(Datum.MENU_DEFINITIONS.toString());
    }

    public void setMenuDefinitions(ArrayList<T8MenuDefinition> menuDefinitions)
    {
        setDefinitionDatum(Datum.MENU_DEFINITIONS.toString(), menuDefinitions);
    }

    public T8MenuDefinition getMenuDefinition(String identifier)
    {
        for (T8MenuDefinition menuDefinition : getMenuDefinitions())
        {
            if (menuDefinition.getIdentifier().equals(identifier))
            {
                return menuDefinition;
            }
        }

        return null;
    }

    public T8LayoutManagerDefinition getLayoutManagerDefinition()
    {
        return (T8LayoutManagerDefinition)getDefinitionDatum(Datum.LAYOUT_MANAGER.toString());
    }

    public void setLayoutManagerDefinition(T8LayoutManagerDefinition definition)
    {
        setDefinitionDatum(Datum.LAYOUT_MANAGER.toString(), definition);
    }
}
