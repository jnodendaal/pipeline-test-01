package com.pilog.t8.definition.ui.functionality.navigator;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.functionality.group.T8FunctionalityGroupDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8FunctionalityNavigatorDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FUNCTIONALITY_NAVIGATOR";
    public static final String DISPLAY_NAME = "Functionality Navigator";
    public static final String DESCRIPTION = "A component that displays a menu list the functionalities from a specified group and allows the user to navigate down the hierarchy of nested functionalities/groups.";
    public static final String IDENTIFIER_PREFIX = "C_FUNC_NAV_";
    public enum Datum {FUNCTIONALITY_GROUP_IDENTIFIER,
                       BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BACKGROUND_PAINTER_IDENTIFIER,
                       CONTENT_BACKGROUND_PAINTER_IDENTIFIER,
                       CONTENT_ITEM_BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition backgroundPainterDefinition;
    private T8PainterDefinition contentBackgroundPainterDefinition;
    private T8PainterDefinition menuBackgroundPainterDefinition;
    private T8PainterDefinition contentItemBackgroundPainterDefinition;
    private T8PainterDefinition menuButtonBackgroundPainterDefinition;
    private T8PainterDefinition menuButtonFocusBackgroundPainterDefinition;
    private T8PainterDefinition menuButtonSelectedBackgroundPainterDefinition;

    public T8FunctionalityNavigatorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString(), "Functionality Group", "The group of functionalities that will be displayed by this menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting the background of the entire navigator component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Background Painter", "The painter to use for painting the background of the navigator menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONTENT_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Content Background Painter", "The painter to use for painting the background of the content panel."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONTENT_ITEM_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Content Item Background Painter", "The painter to use for painting the background of the content items."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Button Background Painter", "The painter to use for painting the background of the menu item buttons in the navigator menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Button Focus Background Painter", "The painter to use for painting the background of the focused menu item buttons in the navigator menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Button Selected Background Painter", "The painter to use for painting the background of the selected menu item buttons in the navigator menu."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityGroupDefinition.GROUP_IDENTIFIER));
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.CONTENT_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.CONTENT_ITEM_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DefinitionManager definitionManager;
        String painterId;

        definitionManager = context.getServerContext().getDefinitionManager();

        painterId = getBackgroundPainterIdentifier();
        if (painterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getContentBackgroundPainterIdentifier();
        if (painterId != null)
        {
            contentBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getContentItemBackgroundPainterIdentifier();
        if (painterId != null)
        {
            contentItemBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuButtonBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuButtonBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuButtonFocusBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuButtonFocusBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuButtonSelectedBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuButtonSelectedBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.functionality.navigator.T8FunctionalityNavigator").getConstructor(T8FunctionalityNavigatorDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    public T8PainterDefinition getContentBackgroundPainterDefinition()
    {
        return contentBackgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuBackgroundPainterDefinition()
    {
        return menuBackgroundPainterDefinition;
    }

    public T8PainterDefinition getContentItemBackgroundPainterDefinition()
    {
        return contentItemBackgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuButtonBackgroundPainterDefinition()
    {
        return menuButtonBackgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuButtonFocusBackgroundPainterDefinition()
    {
        return menuButtonFocusBackgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuButtonSelectedBackgroundPainterDefinition()
    {
        return menuButtonSelectedBackgroundPainterDefinition;
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getContentBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTENT_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setContentBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONTENT_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getMenuBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getContentItemBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTENT_ITEM_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setContentItemBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONTENT_ITEM_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getMenuButtonBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuButtonBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getMenuButtonFocusBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuButtonFocusBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getMenuButtonSelectedBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuButtonSelectedBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getFunctionalityGroupIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString());
    }

    public void setFunctionalityGroupIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString(), identifier);
    }
}
