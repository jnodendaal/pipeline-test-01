package com.pilog.t8.definition.ui.multisplitpane;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8MultiSplitPaneDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MULTI_SPLIT_PANE";
    public static final String DISPLAY_NAME = "Multi-Split Pane";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  The layout of the split dividers is defined using a simple layout definition syntax.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_MULTI_SPLIT_PANE_";
    public enum Datum {CHILD_COMPONENT_DEFINITIONS,
                       BACKGROUND_PAINTER_IDENTIFIER,
                       LAYOUT};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition backgroundPainterDefinition;

    public T8MultiSplitPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting this multi-split pane's background."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.IDENTIFIER_STRING, Datum.LAYOUT.toString(), "Layout", "The layout definition of this multi-split pane."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHILD_COMPONENT_DEFINITIONS.toString(), "Split Pane Components", "The components contained by this multi-split pane."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CHILD_COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public String getLayout()
    {
        return (String)getDefinitionDatum(Datum.LAYOUT.toString());
    }

    public void setLayout(String layout)
    {
        setDefinitionDatum(Datum.LAYOUT.toString(), layout);
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        backgroundPainterId = getBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.multisplitpane.T8MultiSplitPane").getConstructor(T8MultiSplitPaneDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.LAYOUT.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.TextAreaDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<T8ComponentEventDefinition>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<T8ComponentOperationDefinition>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList<T8ComponentDefinition>)getDefinitionDatum(Datum.CHILD_COMPONENT_DEFINITIONS.toString());
    }

    public void addChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        getChildComponentDefinitions().add(componentDefinition);
    }

    public boolean removeChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        return getChildComponentDefinitions().remove(componentDefinition);
    }
}
