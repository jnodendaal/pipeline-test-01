package com.pilog.t8.definition.ui.chart.bar;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T83DBarChartAPIHandler and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T83DBarChartAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_UPDATE_DATA_SET = "$CO_UPDATE_DATA_SET";
    public static final String PARAMETER_BAR_DATA_SET = "$P_BAR_DATA_SET";
    public static final String PARAMETER_BAR_DATA_LIST = "$P_BAR_DATA_LIST";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_UPDATE_DATA_SET);
        newOperationDefinition.setMetaDisplayName("Update Data Set");
        newOperationDefinition.setMetaDescription("This operations updates the data set displayed by the bar chart.  Either a Bar Data Set or a List containing the data to update may be supplied as parameters.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BAR_DATA_SET, "Bar Data Set", "The Bar Data Set to update the chart with.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BAR_DATA_LIST, "Bar Data List", "The Bar Data List to update the chart with.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        return operations;
    }
}
