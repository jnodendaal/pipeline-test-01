package com.pilog.t8.definition.ui.document.datarecord.search;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.document.datarecord.filter.T8RecordFilterClauseDefinition;
import com.pilog.t8.definition.data.document.datarecord.filter.T8RecordFilterCriteriaDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordSearchDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_RECORD_SEARCH";
    public static final String DISPLAY_NAME = "Data Record Search";
    public static final String DESCRIPTION = "A component that allows users to enter advanced search criteria for finding Data Records.";
    public enum Datum {DATA_FILE_STRUCTURE_ID_LIST,
                       DATA_ENTITY_IDENTIFIER,
                       FILTER_FIELD_IDENTIFIER,
                       PRESET_FILTER_DEFINITION,
                       PRESET_FILTER_ID,
                       USER_LOOKUP_DIALOG_IDENTIFIER,
                       DR_INSTANCE_LOOKUP_DIALOG_IDENTIFIER,
                       CODE_TYPE_LOOKUP_DIALOG_IDENTIFIER,
                       PROPERTY_LOOKUP_DIALOG_IDENTIFIER,
                       CONCEPT_LOOKUP_DIALOG,
                       ORGANIZATION_LOOKUP_DIALOG,
                       ADVANCED_OPTIONS_VISIBLE};

    public T8DataRecordSearchDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.DATA_FILE_STRUCTURE_ID_LIST.toString(), "Data File Structures",  "The Data File Structures to which this search is applicable."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity on which the search will be performed."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.FILTER_FIELD_IDENTIFIER.toString(), "Filter Field", "The entity field that will be filtered using the record search criteria (normall RECORD_ID column).").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.PRESET_FILTER_DEFINITION.toString(), "Preset Filter", "The preset filter to set when the search criteria is opened for the first time."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.PRESET_FILTER_ID.toString(), "Preset Filter Identifier", "The preset filter id to set when the search criteria is opened for the first time."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.USER_LOOKUP_DIALOG_IDENTIFIER.toString(), "User Lookup Dialog", "The lookup dialog to use when a User must be selected."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DR_INSTANCE_LOOKUP_DIALOG_IDENTIFIER.toString(), "DR Instance Lookup Dialog", "The lookup dialog to use when a DR Instance must be selected."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.CODE_TYPE_LOOKUP_DIALOG_IDENTIFIER.toString(), "Code Type Lookup Dialog", "The lookup dialog to use when a code type must be selected."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.PROPERTY_LOOKUP_DIALOG_IDENTIFIER.toString(), "Property Lookup Dialog", "The lookup dialog to use when a DR Property must be selected."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.CONCEPT_LOOKUP_DIALOG.toString(), "Concept Lookup Dialog", "The lookup dialog to use when a controlled concept must be selected."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.ORGANIZATION_LOOKUP_DIALOG.toString(), "Organization Lookup Dialog", "The lookup dialog to use when a organization must be selected."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ADVANCED_OPTIONS_VISIBLE.toString(), "Advanced Options", "Sets the initial visibility of the advanced options.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.PRESET_FILTER_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8RecordFilterCriteriaDefinition.TYPE_IDENTIFIER));
        else if (Datum.PRESET_FILTER_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8RecordFilterCriteriaDefinition.GROUP_IDENTIFIER));
        else if (Datum.DR_INSTANCE_LOOKUP_DIALOG_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.USER_LOOKUP_DIALOG_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.CODE_TYPE_LOOKUP_DIALOG_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROPERTY_LOOKUP_DIALOG_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.CONCEPT_LOOKUP_DIALOG.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.ORGANIZATION_LOOKUP_DIALOG.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8GlobalDialogDefinition.GROUP_IDENTIFIER));
        else if (Datum.FILTER_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();
                    if (fieldDefinitions != null)
                    {
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            optionList.add(new T8DefinitionDatumOption(fieldDefinition.getPublicIdentifier(), fieldDefinition.getPublicIdentifier()));
                        }
                    }

                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch", new Class<?>[]{T8DataRecordSearchDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataRecordSearchAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataRecordSearchAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getFilterFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FILTER_FIELD_IDENTIFIER.toString());
    }

    public void setFilterFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FILTER_FIELD_IDENTIFIER.toString(), identifier);
    }

    public T8RecordFilterCriteriaDefinition getPresetFilterDefinition()
    {
        return (T8RecordFilterCriteriaDefinition)getDefinitionDatum(Datum.PRESET_FILTER_DEFINITION.toString());
    }

    public void setPresetFilterDefinition(T8RecordFilterCriteriaDefinition definition)
    {
        setDefinitionDatum(Datum.PRESET_FILTER_DEFINITION.toString(), definition);
    }
    
    public String getPresetFilterId()
    {
        return (String)getDefinitionDatum(Datum.PRESET_FILTER_ID.toString());
    }

    public void setPresetFilterId(String definitionId)
    {
        setDefinitionDatum(Datum.PRESET_FILTER_ID.toString(), definitionId);
    }

    public String getUserLookupDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.USER_LOOKUP_DIALOG_IDENTIFIER.toString());
    }

    public void setUserLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.USER_LOOKUP_DIALOG_IDENTIFIER.toString(), identifier);
    }

    public String getDRInstanceLookupDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_LOOKUP_DIALOG_IDENTIFIER.toString());
    }

    public void setDRInstanceLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_LOOKUP_DIALOG_IDENTIFIER.toString(), identifier);
    }

    public String getCodeTypeLookupDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CODE_TYPE_LOOKUP_DIALOG_IDENTIFIER.toString());
    }

    public void setCodeTypeLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CODE_TYPE_LOOKUP_DIALOG_IDENTIFIER.toString(), identifier);
    }

    public String getPropertyLookupDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_LOOKUP_DIALOG_IDENTIFIER.toString());
    }

    public void setPropertyLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROPERTY_LOOKUP_DIALOG_IDENTIFIER.toString(), identifier);
    }

    public String getConceptLookupDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONCEPT_LOOKUP_DIALOG.toString());
    }

    public void setConceptLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CONCEPT_LOOKUP_DIALOG.toString(), identifier);
    }

    public String getOrganizationLookupDialogIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ORGANIZATION_LOOKUP_DIALOG.toString());
    }

    public void setOrganizationLookupDialogIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ORGANIZATION_LOOKUP_DIALOG.toString(), identifier);
    }

    public List<String> getDataFileStructureIDList()
    {
        return (List<String>)getDefinitionDatum(Datum.DATA_FILE_STRUCTURE_ID_LIST.toString());
    }

    public void setRootDRInstanceIDList(List<String> drInstanceIDList)
    {
        setDefinitionDatum(Datum.DATA_FILE_STRUCTURE_ID_LIST.toString(), drInstanceIDList);
    }

    public boolean isAdvancedOptionsEnabled()
    {
        return (Boolean)getDefinitionDatum(Datum.ADVANCED_OPTIONS_VISIBLE.toString());
    }

    public void setAdvancedOptionsEnabled(boolean optionsEnabled)
    {
        setDefinitionDatum(Datum.ADVANCED_OPTIONS_VISIBLE.toString(), optionsEnabled);
    }
}