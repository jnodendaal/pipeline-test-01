package com.pilog.t8.definition.ui.spinner;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8NumericSpinnerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_NUMERIC_SPINNER";
    public static final String DISPLAY_NAME = "Numeric Spinner";
    public static final String DESCRIPTION = "A component that allows the selection of numeric values.";
    public static final String IDENTIFIER_PREFIX = "C_NUMERIC_SPINNER_";
    public enum Datum {MINIMUM_VALUE,
                       MAXIMUM_VALUE,
                       STEP_VALUE};
    // -------- Definition Meta-Data -------- //

    public T8NumericSpinnerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MINIMUM_VALUE.toString(), "Minimum", "The mimimum amout allowed.", "0"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MAXIMUM_VALUE.toString(), "Maximum", "The maximum amount allowed.", "100"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.STEP_VALUE.toString(), "Step Size", "The increment size that will be used.", "1"));

        return datumTypes;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.spinner.T8NumericSpinner").getConstructor(T8ComponentController.class, T8NumericSpinnerDefinition.class);
        return (T8Component)constructor.newInstance(controller, this);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8NumericSpinnerAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8NumericSpinnerAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getMinimumValue()
    {
        return (String)getDefinitionDatum(Datum.MINIMUM_VALUE.toString());
    }

    public void setMinimumValue(String amount)
    {
        setDefinitionDatum(Datum.MINIMUM_VALUE.toString(), amount);
    }

    public String getMaximumValue()
    {
        return (String)getDefinitionDatum(Datum.MAXIMUM_VALUE.toString());
    }

    public void setMaximumValue(String amount)
    {
        setDefinitionDatum(Datum.MAXIMUM_VALUE.toString(), amount);
    }

    public String getStepValue()
    {
        return (String)getDefinitionDatum(Datum.STEP_VALUE.toString());
    }

    public void setStepValue(String amount)
    {
        setDefinitionDatum(Datum.STEP_VALUE.toString(), amount);
    }
}
