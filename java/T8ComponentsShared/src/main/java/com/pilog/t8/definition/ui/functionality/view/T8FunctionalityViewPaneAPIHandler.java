package com.pilog.t8.definition.ui.functionality.view;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;


/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8FunctionalityViewAPIHandler and responds to specific
 * requests regarding these definitions.  It is extremely important that only
 * new definitions are added to this class and none of the old definitions
 * removed or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8FunctionalityViewPaneAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_INVOKE_FUNCTIONALITY = "$CO_INVOKE_FUNCTIONALITY";
    public static final String PARAMETER_FUNCTIONALITY_IDENTIFIER = "$P_FUNCTIONALITY_IDENTIFIER";
    public static final String PARAMETER_DATA_OBJECT_IID = "$P_DATA_OBJECT_IID";
    public static final String PARAMETER_FUNCTIONALITY_PARAMETERS = "$P_FUNCTIONALITY_PARAMETERS";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        T8ComponentOperationDefinition newOperationDefinition;

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_INVOKE_FUNCTIONALITY);
        newOperationDefinition.setMetaDisplayName("Invoke Functionality");
        newOperationDefinition.setMetaDescription("This operations invokes the specified functionality within the context of this functionality view.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FUNCTIONALITY_IDENTIFIER, "Functionality Identifier", "The identifier of the functionality to invoke.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_OBJECT_IID, "Target Object Iid", "The instance id of the data object on which the functionality will be onvoked.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FUNCTIONALITY_PARAMETERS, "Parameters", "The parameter supplied for access to the functionality.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
