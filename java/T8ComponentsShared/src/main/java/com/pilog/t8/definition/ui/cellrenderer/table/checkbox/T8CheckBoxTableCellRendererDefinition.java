package com.pilog.t8.definition.ui.cellrenderer.table.checkbox;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.celleditor.table.checkbox.T8CheckBoxTableCellEditorDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxTableCellRendererDefinition extends T8CheckBoxTableCellEditorDefinition implements T8TableCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_CHECK_BOX";
    public static final String DISPLAY_NAME = "Check Box Cell Renderer";
    public static final String DESCRIPTION = "A cell renderer that uses a checkbox to display a selected or deselected value.";
    public static final String VERSION = "0";
    public enum Datum {TARGET_ENTITY_IDENTIFIER,
                       TARGET_FIELD_IDENTIFIER,
                       SELECTED_VALUE_EXPRESSION,
                       DESELECTED_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8CheckBoxTableCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.cellrenderer.table.checkbox.T8CheckBoxTableCellRenderer", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }
}
