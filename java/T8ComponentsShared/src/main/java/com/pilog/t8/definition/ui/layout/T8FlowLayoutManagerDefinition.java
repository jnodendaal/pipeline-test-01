package com.pilog.t8.definition.ui.layout;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import java.awt.LayoutManager;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8FlowLayoutManagerDefinition extends T8Definition implements T8LayoutManagerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_LAYOUT_FLOW";
    public static final String DISPLAY_NAME = "Flow Layout";
    public static final String DESCRIPTION = "Flow layout manager";

    public enum Datum
    {
        ALIGNMENT,
        HORIZONTAL_GAP,
        VERTICAL_GAP
    };
    // -------- Definition Meta-Data -------- //

    public enum FlowAlign
    {
        LEFT,
        CENTRE,
        RIGHT,
        LEADING,
        TRAILING
    }

    public T8FlowLayoutManagerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ALIGNMENT.toString(), "Alignment", "The alignment to be used by the flow layout", FlowAlign.CENTRE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HORIZONTAL_GAP.toString(), "Horizontal Gap", "The horizontal gap to be used between components.", 5));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.VERTICAL_GAP.toString(), "Vertical Gap", "The vertical gap to be used between components.", 5));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(FlowAlign.values());
        else return null;
    }

    @Override
    public LayoutManager constructLayoutManager() throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("java.awt.FlowLayout").getConstructor(int.class, int.class, int.class);
        return (LayoutManager)constructor.newInstance(getAlignment().ordinal(), getHorizontalGap(), getVerticalGap());
    }

    public void setAlignment(FlowAlign align)
    {
        setDefinitionDatum(Datum.ALIGNMENT.toString(), align.toString());
    }

    public FlowAlign getAlignment()
    {
        return FlowAlign.valueOf((String)getDefinitionDatum(Datum.ALIGNMENT.toString()));
    }

    public void setHorizontalGap(Integer gap)
    {
        setDefinitionDatum(Datum.HORIZONTAL_GAP.toString(), gap);
    }

    public Integer getHorizontalGap()
    {
        return (Integer)getDefinitionDatum(Datum.HORIZONTAL_GAP.toString());
    }

    public void setVerticalGap(Integer gap)
    {
        setDefinitionDatum(Datum.VERTICAL_GAP.toString(), gap);
    }

    public Integer getVerticalGap()
    {
        return (Integer)getDefinitionDatum(Datum.VERTICAL_GAP.toString());
    }
}
