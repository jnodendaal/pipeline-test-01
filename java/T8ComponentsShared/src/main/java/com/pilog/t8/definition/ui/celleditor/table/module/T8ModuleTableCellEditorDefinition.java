package com.pilog.t8.definition.ui.celleditor.table.module;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.celleditor.T8TableCellEditorDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import java.lang.reflect.Constructor;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleTableCellEditorDefinition extends T8ModuleDefinition implements T8TableCellEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_EDITOR_MODULE";
    public static final String DISPLAY_NAME = "Module Cell Editor";
    public static final String DESCRIPTION = "A cell editor that uses a sub-module for its layout and functionality.";
    public static final String VERSION = "0";
    // -------- Definition Meta-Data -------- //
    
    public T8ModuleTableCellEditorDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;
            
            constructor = Class.forName("com.pilog.t8.ui.celleditor.table.module.T8ModuleTableCellEditor").getConstructor(T8ModuleTableCellEditorDefinition.class, T8ComponentController.class);
            return (T8Component)constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
