package com.pilog.t8.definition.ui.textarea;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextAreaDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TEXT_AREA";
    public static final String DISPLAY_NAME = "Text Area";
    public static final String DESCRIPTION = "A component that displays a multi-line text field.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_TEXT_AREA_";
    public enum Datum {EDITABLE,
                       TEXT,
                       FONT_DEFINITION,
                       ROWS,
                       COLUMNS,
                       UNDECORATED};
    // -------- Definition Meta-Data -------- //

    public T8TextAreaDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EDITABLE.toString(), "Editable", "A boolean value to set whether or not the text inside of the text area is editable by the user.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TEXT.toString(), "Text", "The initial content String of this text area."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROWS.toString(), "Rows", "The preferred number of rows to be made visible when the text area layout is performed.", 5));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.COLUMNS.toString(), "Columns", "The preferred number of column to be made visible when the text area layout is performed.", 8));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FONT_DEFINITION.toString(), "Font", "The font to use for the text in the textarea."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.UNDECORATED.toString(), "Undecorated", "If set to true, the text area will have no border or background painted and will display only its content text on a transparent background.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FONT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FontDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.TEXT.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.TextAreaDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else if (Datum.FONT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.textarea.T8TextArea", new Class<?>[]{T8TextAreaDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TextAreaAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TextAreaAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT.toString(), text);
    }

    public T8FontDefinition getFontDefinition()
    {
        return (T8FontDefinition)getDefinitionDatum(Datum.FONT_DEFINITION.toString());
    }

    public void setFontDefinition(T8FontDefinition fontDefinition)
    {
         setDefinitionDatum(Datum.FONT_DEFINITION.toString(), fontDefinition);
    }

    public boolean isEditable()
    {
        return (Boolean)getDefinitionDatum(Datum.EDITABLE.toString());
    }

    public void setEditable(boolean editable)
    {
        setDefinitionDatum(Datum.EDITABLE.toString(), editable);
    }

    public boolean isUndecorated()
    {
        Boolean undecorated;

        undecorated = (Boolean)getDefinitionDatum(Datum.UNDECORATED.toString());
        return undecorated != null ? undecorated : false;
    }

    public void setUndecorated(boolean undecorated)
    {
        setDefinitionDatum(Datum.UNDECORATED.toString(), undecorated);
    }

    public int getRows()
    {
        Integer rows;

        rows = (Integer)getDefinitionDatum(Datum.ROWS.toString());
        return rows != null ? rows : 0;
    }

    public void setRows(int rows)
    {
        setDefinitionDatum(Datum.ROWS.toString(), rows);
    }

    public int getColumns()
    {
        Integer columns;

        columns = (Integer)getDefinitionDatum(Datum.COLUMNS.toString());
        return columns != null ? columns : 0;
    }

    public void setColumns(int columns)
    {
        setDefinitionDatum(Datum.COLUMNS.toString(), columns);
    }
}
