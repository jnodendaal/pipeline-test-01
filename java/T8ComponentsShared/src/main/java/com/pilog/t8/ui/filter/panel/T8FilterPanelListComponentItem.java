package com.pilog.t8.ui.filter.panel;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.filter.panel.datasource.T8FilterPanelListItemDefinition;
import com.pilog.t8.security.T8Context;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelListComponentItem
{
    public T8FilterPanelListComponentItem(String displayName, Object value)
    {
        this._displayName = displayName;
        this._value = value;
    }

    public T8FilterPanelListComponentItem(String displayName, Object value,  T8IconDefinition icon)
    {
        this._displayName = displayName;
        this._value = value;
        this.icon = icon;
    }

    public T8FilterPanelListComponentItem(T8FilterPanelListItemDefinition itemDefinition)
    {
        this._displayName = itemDefinition.getItemDisplay();
        this._value = itemDefinition.getItemValue();
        this.icon = itemDefinition.getIconDefinition();
        this.customClause = itemDefinition.getCustomFilterDefinition();
    }

    private String _displayName;
    private Object _value;
    private Boolean visible = true;
    private T8IconDefinition icon;
    private T8DataFilterDefinition customClause;

    public T8DataFilterClause getFilterClause(T8Context context, String fieldId)
    {
        if (customClause != null)
        {
            return customClause.getNewDataFilterInstance(context, null).getFilterCriteria();
        }

        return new T8DataFilterCriterion(fieldId, T8DataFilterCriterion.DataFilterOperator.EQUAL, _value);
    }

    public boolean hasCustomClause()
    {
        return customClause != null;
    }

    /**
     * @return the _displayName
     */
    public String getDisplayName()
    {
        return _displayName;
    }

    /**
     * @param displayName the _displayName to set
     */
    public void setDisplayName(String displayName)
    {
        this._displayName = displayName;
    }

    /**
     * @return the value
     */
    public Object getValue()
    {
        return _value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value)
    {
        this._value = value;
    }

    @Override
    public String toString()
    {
        return _displayName;
    }

    /**
     * @return the visible
     */
    public Boolean getVisible()
    {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible)
    {
        this.visible = visible;
    }

    /**
     * @return the icon
     */
    public T8IconDefinition getIcon()
    {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(T8IconDefinition icon)
    {
        this.icon = icon;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 73 * hash + (this._value != null ? this._value.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final T8FilterPanelListComponentItem other = (T8FilterPanelListComponentItem) obj;
        if (this._value != other._value && (this._value == null || !this._value.equals(other._value)))
        {
            return false;
        }
        return true;
    }
}
