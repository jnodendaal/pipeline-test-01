package com.pilog.t8.definition.ui.layout;

import java.awt.LayoutManager;
import java.lang.reflect.Constructor;

/**
 * @author Hennie Brink
 */
public class T8WrapLayoutManagerDefinition extends T8FlowLayoutManagerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_LAYOUT_WRAP";
    public static final String DISPLAY_NAME = "Wrap Layout";
    public static final String DESCRIPTION = "This layout manager will overflow to the next line if the layout manager runs out of space";
    // -------- Definition Meta-Data -------- //


    public T8WrapLayoutManagerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public LayoutManager constructLayoutManager() throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.utilities.components.layout.WrapLayout").getConstructor(int.class, int.class, int.class);
        return (LayoutManager)constructor.newInstance(getAlignment().ordinal(), getHorizontalGap(), getVerticalGap());
    }
}
