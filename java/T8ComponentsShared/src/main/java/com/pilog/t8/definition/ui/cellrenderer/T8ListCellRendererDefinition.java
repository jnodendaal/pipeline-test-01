package com.pilog.t8.definition.ui.cellrenderer;

/**
 * @author Bouwer du Preez
 */
public interface T8ListCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_CELL_RENDERER_LIST";
    public static final String STORAGE_PATH = "/list_cell_renderers"; 
    // -------- Definition Meta-Data -------- //
}
