/**
 * Created on 21 May 2015, 2:12:07 PM
 */
package com.pilog.t8.definition.ui.list;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public abstract class T8ComponentListDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER,
                       MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition menuButtonSelectedBackgroundPainterDefinition;
    private T8PainterDefinition menuButtonFocusBackgroundPainterDefinition;
    private T8PainterDefinition menuButtonBackgroundPainterDefinition;
    private T8PainterDefinition backgroundPainterDefinition;

    public T8ComponentListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting the background of the entire process list component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Button Background Painter", "The painter to use for painting the background of the process type buttons in the process menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Button Focus Background Painter", "The painter to use for painting the background of the focused process type buttons in the process menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Button Selected Background Painter", "The painter to use for painting the background of the selected process type buttons in the process menu."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DefinitionManager definitionManager;
        String painterId;

        definitionManager = context.getServerContext().getDefinitionManager();

        painterId = getBackgroundPainterIdentifier();
        if (painterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuButtonBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuButtonBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuButtonFocusBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuButtonFocusBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getMenuButtonSelectedBackgroundPainterIdentifier();
        if (painterId != null)
        {
            menuButtonSelectedBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuButtonBackgroundPainterDefinition()
    {
        return menuButtonBackgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuButtonFocusBackgroundPainterDefinition()
    {
        return menuButtonFocusBackgroundPainterDefinition;
    }

    public T8PainterDefinition getMenuButtonSelectedBackgroundPainterDefinition()
    {
        return menuButtonSelectedBackgroundPainterDefinition;
    }

    public String getBackgroundPainterIdentifier()
    {
        return getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER);
    }

    public void setBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER, identifier);
    }

    public String getMenuButtonBackgroundPainterIdentifier()
    {
        return getDefinitionDatum(Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER);
    }

    public void setMenuButtonBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BUTTON_BACKGROUND_PAINTER_IDENTIFIER, identifier);
    }

    public String getMenuButtonFocusBackgroundPainterIdentifier()
    {
        return getDefinitionDatum(Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER);
    }

    public void setMenuButtonFocusBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BUTTON_FOCUS_BACKGROUND_PAINTER_IDENTIFIER, identifier);
    }

    public String getMenuButtonSelectedBackgroundPainterIdentifier()
    {
        return getDefinitionDatum(Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER);
    }

    public void setMenuButtonSelectedBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MENU_BUTTON_SELECTED_BACKGROUND_PAINTER_IDENTIFIER, identifier);
    }
}