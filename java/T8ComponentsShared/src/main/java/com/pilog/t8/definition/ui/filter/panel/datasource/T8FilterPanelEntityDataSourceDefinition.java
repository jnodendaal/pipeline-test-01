package com.pilog.t8.definition.ui.filter.panel.datasource;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelEntityDataSourceDefinition extends T8FilterPanelListComponentItemDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //

    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_DATASOURCE_T8_DATA_ENTITY";
    public static final String DISPLAY_NAME = "T8Data Entity";
    public static final String DESCRIPTION = "A T8 Data Entity Data Source";
    public enum Datum
    {
        DATA_ENTITY_IDENTIFIER,
        DATA_FILTER,
        DISPLAY_FIELD,
        DISPLAY_FIELD_EXPRESSION,
        VALUE_FIELD,
        ICONS
    };
    // -------- Definition Meta-Data -------- //

    public T8FilterPanelEntityDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8FilterPanelListComponentItemDataSource getInstance()
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.filter.panel.datasource.T8FilterPanelEntityDataSource").getConstructor(T8FilterPanelEntityDataSourceDefinition.class);
            return (T8FilterPanelListComponentItemDataSource) constructor.newInstance(this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier", "The data entity to use to populate the data for the list item."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DISPLAY_FIELD.toString(), "Display Field", "The field that will be used for display to the user.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DISPLAY_FIELD_EXPRESSION.toString(), "Display Field Expression", "The Expression that will be used to build up the display shown to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.VALUE_FIELD.toString(), "Value Field", "The field that will be used as the filter value.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.addAll(super.getDatumTypes());
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DATA_FILTER.toString(), "Filter", "The filter that will be used to filter values displayed to the user."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ICONS.toString(), "Icons", "The icons that this data source is capable of providing."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.DISPLAY_FIELD.toString().equals(datumIdentifier))
        {
            return getDataEntityFields(definitionContext);
        }
        else if (Datum.VALUE_FIELD.toString().equals(datumIdentifier))
        {
            return getDataEntityFields(definitionContext);
        }
        else if (Datum.DATA_FILTER.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.ICONS.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FilterPanelDataEntityIconDefinition.GROUP_IDENTIFIER));
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    private ArrayList<T8DefinitionDatumOption> getDataEntityFields(T8DefinitionContext definitionContext) throws Exception
    {
        if (getDataEntityIdentifier() == null)
        {
            return new ArrayList<T8DefinitionDatumOption>();
        }
        T8DataEntityDefinition entityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), getDataEntityIdentifier());
        return createStringOptions(entityDefinition.getFieldIdentifiers(true));
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getDataEntityIdentifier()
    {
        return (String) getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDisplayField(String identifier)
    {
        setDefinitionDatum(Datum.DISPLAY_FIELD.toString(), identifier);
    }

    public String getDisplayField()
    {
        return (String) getDefinitionDatum(Datum.DISPLAY_FIELD.toString());
    }

    public void setValueField(String identifier)
    {
        setDefinitionDatum(Datum.VALUE_FIELD.toString(), identifier);
    }

    public String getValueField()
    {
        return (String) getDefinitionDatum(Datum.VALUE_FIELD.toString());
    }

    public void setDataFilter(T8DataFilterDefinition filter)
    {
        setDefinitionDatum(Datum.DATA_FILTER.toString(), filter);
    }

    public T8DataFilterDefinition getDataFilter()
    {
        return (T8DataFilterDefinition) getDefinitionDatum(Datum.DATA_FILTER.toString());
    }

    public void setIcons(List<T8FilterPanelDataEntityIconDefinition> filter)
    {
        setDefinitionDatum(Datum.ICONS.toString(), filter);
    }

    public List<T8FilterPanelDataEntityIconDefinition> getIcons()
    {
        return (List<T8FilterPanelDataEntityIconDefinition>) getDefinitionDatum(Datum.ICONS.toString());
    }

    public String getDisplayFieldExpression()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_FIELD_EXPRESSION.toString());
    }

    public void setDisplayFieldExpression(String expression)
    {
        setDefinitionDatum(Datum.DISPLAY_FIELD_EXPRESSION.toString(), expression);
    }
}
