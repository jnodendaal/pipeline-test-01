package com.pilog.t8.definition.ui.splitpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8SplitPaneDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_SPLIT_PANE";
    public static final String DISPLAY_NAME = "Split Pane";
    public static final String DESCRIPTION = "A split pane container to which two UI components can be added.  The two components are split vertically or horicontally by a movable deivider.";
    public static final String IDENTIFIER_PREFIX = "C_SPLIT_PANE_";
    public enum Datum {ORIENTATION, DIVIDER_LOCATION, CHILD_COMPONENT_DEFINITIONS};
    public enum Orientation {VERTICAL_DIVIDER, HORIZONTAL_DIVIDER};
    // -------- Definition Meta-Data -------- //

    public T8SplitPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.DIVIDER_LOCATION.toString(), "Dividier Location", "The location of the divider given as a decimal that specifies the fraction of available space assigned to the first (top or right) split component.", 0.5));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ORIENTATION.toString(), "Orientation", "The orientation of the divider of the split pane.", Orientation.VERTICAL_DIVIDER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHILD_COMPONENT_DEFINITIONS.toString(), "Split Pane Components", "The two components contained by the split pane."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ORIENTATION.toString().equals(datumIdentifier)) return createStringOptions(Orientation.values());
        else if (Datum.CHILD_COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(getChildComponentDefinitions() == null || (getChildComponentDefinitions() != null && getChildComponentDefinitions().isEmpty()))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.CHILD_COMPONENT_DEFINITIONS.toString(), "No Child components defined.", T8DefinitionValidationError.ErrorType.WARNING));
        }

        return validationErrors;
    }

    public Orientation getOrientation()
    {
        return Orientation.valueOf((String)getDefinitionDatum(Datum.ORIENTATION.toString()));
    }

    public void setOrientation(Orientation orientation)
    {
        setDefinitionDatum(Datum.ORIENTATION.toString(), orientation.toString());
    }

    public double getDividerLocation()
    {
        return (Double)getDefinitionDatum(Datum.DIVIDER_LOCATION.toString());
    }

    public void setDividerLocation(double dividerLocation)
    {
        setDefinitionDatum(Datum.DIVIDER_LOCATION.toString(), dividerLocation);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.splitpane.T8SplitPane", new Class<?>[]{T8SplitPaneDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList<T8ComponentDefinition>)getDefinitionDatum(Datum.CHILD_COMPONENT_DEFINITIONS.toString());
    }

    public void addChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        getChildComponentDefinitions().add(componentDefinition);
    }

    public boolean removeChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        return getChildComponentDefinitions().remove(componentDefinition);
    }
}
