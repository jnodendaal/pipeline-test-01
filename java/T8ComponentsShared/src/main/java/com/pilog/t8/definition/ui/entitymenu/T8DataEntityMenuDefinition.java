package com.pilog.t8.definition.ui.entitymenu;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.menuitem.T8MenuItemDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityMenuDefinition extends T8MenuItemDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_ENTITY_MENU";
    public static final String DISPLAY_NAME = "Data Entity Menu";
    public static final String DESCRIPTION = "A menu component to which menu items can be added.  All menu items are then displayed as a vertical list.";
    public static final String IDENTIFIER_PREFIX = "C_ENTITY_MENU_";
    private enum Datum {MENU_ITEM_DEFINITIONS,
                        MENU_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition menuBackgroundPainterDefinition;

    public enum MenuBorderType {NONE, LINE, SHADOW};

    public T8DataEntityMenuDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        // Change the inherited description to fit properly with a menu instead of a menu item.
        datumTypes = super.getDatumTypes();
        for (T8DefinitionDatumType datumType : datumTypes)
        {
            if (datumType.getIdentifier().equals(T8MenuItemDefinition.Datum.TEXT.toString()))
            {
                datumType.setDescription("The text to display on the menu.");
            }
        }

        // Add the additional datum types that are specific to menus.
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Background Painter", "The painter to use for painting this menu's backgorund."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MENU_ITEM_DEFINITIONS.toString(), "Menu Items", "The menu items contained by this menu."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MENU_ITEM_DEFINITIONS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataEntityMenuItemDefinition.GROUP_IDENTIFIER));
            return options;
        }
        else if (Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        super.initializeDefinition(context, inputParameters, configurationSettings);

        backgroundPainterId = getMenuBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            menuBackgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.entitymenu.T8DataEntityMenu", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<? extends T8ComponentDefinition> getChildComponentDefinitions()
    {
        return getMenuItemDefinitions();
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    public ArrayList<T8MenuItemDefinition> getMenuItemDefinitions()
    {
        return getDefinitionDatum(Datum.MENU_ITEM_DEFINITIONS);
    }

    public void setMenuItemDefinitions(ArrayList<T8MenuItemDefinition> menuItemDefinitions)
    {
        getMenuItemDefinitions().clear();
        getMenuItemDefinitions().addAll(menuItemDefinitions);
    }

    public T8MenuItemDefinition getMenuItemDefinition(String identifier)
    {
        for (T8MenuItemDefinition menuItemDefinition : getMenuItemDefinitions())
        {
            if (menuItemDefinition.getIdentifier().equals(identifier))
            {
                return menuItemDefinition;
            }
        }

        return null;
    }

    public String getMenuBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getMenuBackgroundPainterDefinition()
    {
        return menuBackgroundPainterDefinition;
    }
}
