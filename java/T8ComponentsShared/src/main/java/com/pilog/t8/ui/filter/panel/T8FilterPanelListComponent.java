package com.pilog.t8.ui.filter.panel;

import com.pilog.t8.data.filter.T8DataFilter;

/**
 * @author Hennie Brink
 */
public interface T8FilterPanelListComponent extends T8FilterPanelComponent
{
    public void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception;
}
