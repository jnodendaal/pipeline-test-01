package com.pilog.t8.definition.ui.chart.pie;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.chart.T8ChartDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T83DPieChartDefinition extends T8ChartDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CHART_3DPIE";
    public static final String DISPLAY_NAME = "3D Pie Chart";
    public static final String DESCRIPTION = "A component that displays a three dimensional pie chart.";
    public static final String IDENTIFIER_PREFIX = "C_CHART_3DPIE_";
    public enum Datum {DEPTH_FACTOR};
    // -------- Definition Meta-Data -------- //

    public T83DPieChartDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.DEPTH_FACTOR.toString(), "Depth Factor", "The depth of the pie chart.", 0.5));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.chart.pie.T83DPieChart").getConstructor(T83DPieChartDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T83DPieChartAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T83DPieChartAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    } 
    
    public Double getDepthFactor()
    {
        Double depthFactor;
        
        depthFactor = (Double)getDefinitionDatum(Datum.DEPTH_FACTOR.toString());
        return depthFactor != null ? depthFactor : 0.5;
    }
    
    public void setDepthFactor(double depthFactor)
    {
        setDefinitionDatum(Datum.DEPTH_FACTOR.toString(), depthFactor);
    }
}
