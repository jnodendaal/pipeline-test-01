package com.pilog.t8.definition.ui.sessionlabel;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.label.T8LabelAPIHandler;
import com.pilog.t8.definition.ui.label.T8LabelDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SessionLabelDefinition extends T8LabelDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LABEL_SESSION";
    public static final String DISPLAY_NAME = "Session Label";
    public static final String DESCRIPTION = "A component that displays a text String containing the specified session details.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_LABEL_SESSION_";
    private enum Datum {SESSION_LABEL_TYPE};
    // -------- Definition Meta-Data -------- //

    private String text;

    public enum SessionLabelType {USER_NAME,
                                  USER_SURNAME,
                                  USER_NAME_AND_SURNAME,
                                  USER_PROFILE_IDENTIFIER,
                                  USER_PROFILE_DISPLAY_NAME};

    public T8SessionLabelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SESSION_LABEL_TYPE.toString(), "Label Type", "The type of label text to display.", SessionLabelType.USER_NAME.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SESSION_LABEL_TYPE.toString().equals(datumIdentifier)) return createStringOptions(SessionLabelType.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DefinitionManager definitionManager;
        T8SessionContext sessionContext;
        SessionLabelType labelType;

        sessionContext = context.getSessionContext();
        definitionManager = context.getServerContext().getDefinitionManager();
        labelType = getSessionLabelType();
        if (labelType == SessionLabelType.USER_NAME)
        {
            text = sessionContext.getUserName();
        }
        else if (labelType == SessionLabelType.USER_SURNAME)
        {
            text = sessionContext.getUserSurname();
        }
        else if (labelType == SessionLabelType.USER_NAME_AND_SURNAME)
        {
            text = sessionContext.getUserName() + " " + sessionContext.getUserSurname();
        }
        else if (labelType == SessionLabelType.USER_PROFILE_IDENTIFIER)
        {
            text = sessionContext.getUserProfileIdentifier();
        }
        else if (labelType == SessionLabelType.USER_PROFILE_DISPLAY_NAME)
        {
            T8DefinitionMetaData definitionMetaData;

            definitionMetaData = definitionManager.getDefinitionMetaData(null, sessionContext.getUserProfileIdentifier());
            text = definitionMetaData != null ? definitionMetaData.getDisplayName() : null;
        }
        else text = null;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            return (T8Component)Class.forName("com.pilog.t8.ui.sessionlabel.T8SessionLabel").getConstructor(T8SessionLabelDefinition.class, T8ComponentController.class).newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8LabelAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8LabelAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public SessionLabelType getSessionLabelType()
    {
        return SessionLabelType.valueOf((String)getDefinitionDatum(Datum.SESSION_LABEL_TYPE.toString()));
    }

    public void setSessionLabelType(SessionLabelType labelType)
    {
        setDefinitionDatum(Datum.SESSION_LABEL_TYPE.toString(), labelType.toString());
    }

    @Override
    public String getText()
    {
        return text;
    }
}
