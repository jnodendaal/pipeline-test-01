package com.pilog.t8.definition.ui.labelmatrix;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LabelMatrixCellDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LABEL_MATRIX_CELL";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_LABEL_MATRIX_CELL";
    public static final String IDENTIFIER_PREFIX = "C_LABEL_MATRIX_CELL_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Label Matrix Cell";
    public static final String DESCRIPTION = "A definition of the content and/or formatting of a specific cell in the matrix.";
    private enum Datum {ROW_INDEX, 
                        COLUMN_INDEX, 
                        TEXT};
    // -------- Definition Meta-Data -------- //

    public T8LabelMatrixCellDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text to be displayed in the matrix cell."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROW_INDEX.toString(), "Row Index", "The row index of the cell.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.COLUMN_INDEX.toString(), "Column Index", "The column index of the cell.", 0));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT.toString(), text);
    }    
    
    public int getRowIndex()
    {
        Integer rowIndex;
        
        rowIndex = (Integer)getDefinitionDatum(Datum.ROW_INDEX.toString());
        return rowIndex != null ? rowIndex : 0;
    }
    
    public void setRowIndex(int index)
    {
        setDefinitionDatum(Datum.ROW_INDEX.toString(), index);
    }
    
    public int getColumnIndex()
    {
        Integer columnIndex;
        
        columnIndex = (Integer)getDefinitionDatum(Datum.COLUMN_INDEX.toString());
        return columnIndex != null ? columnIndex : 0;
    }
    
    public void setColumnIndex(int index)
    {
        setDefinitionDatum(Datum.COLUMN_INDEX.toString(), index);
    }
}
