package com.pilog.t8.definition.ui.processlist;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8ProcessList and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ProcessListAPIHandler
{
    public static ArrayList<T8ComponentEventDefinition> events;
    public static ArrayList<T8ComponentOperationDefinition> operations;

    // These should NOT be changed, only added to.
    public static final String EVENT_PROCESS_STOPPED = "$CE_PROCESS_STOPPED";
    public static final String EVENT_PROCESS_FINALIZED = "$CE_PROCESS_FINALIZED";
    public static final String OPERATION_FILTER_PROCESS_LIST = "$CO_FILTER_PROCESS_LIST";
    public static final String PARAMETER_PROCESS_IDENTIFIER = "$P_PROCESS_IDENTIFIER";
    public static final String PARAMETER_PROCESS_INSTANCE_IDENTIFIER = "$P_PROCESS_INSTANCE_IDENTIFIER";
    public static final String PARAMETER_INCLUDE_EXECUTING_PROCESSES = "$P_INCLUDE_EXECUTING_PROCESSES";
    public static final String PARAMETER_INCLUDE_IDLE_PROCESSES = "$P_INCLUDE_IDLE_PROCESSES";

    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_PROCESS_STOPPED);
        newEventDefinition.setMetaDisplayName("Process Stopped");
        newEventDefinition.setMetaDescription("This event occurs after a process has been stopped.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROCESS_IDENTIFIER, "Process Identifier", "The Identifier of the process that was stopped.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROCESS_INSTANCE_IDENTIFIER, "Process Instance Identifier", "The Instance Identifier of the process that was stopped.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_PROCESS_FINALIZED);
        newEventDefinition.setMetaDisplayName("Process Finalized");
        newEventDefinition.setMetaDescription("This event occurs after a process has been finalized.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROCESS_IDENTIFIER, "Process Identifier", "The Identifier of the process that was closed.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROCESS_INSTANCE_IDENTIFIER, "Process Instance Identifier", "The Instance Identifier of the process that was closed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_FILTER_PROCESS_LIST);
        newOperationDefinition.setMetaDisplayName("Filter Process List");
        newOperationDefinition.setMetaDescription("This operations fiters the process list according to the specified criteria.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PROCESS_IDENTIFIER, "Process Identifier", "The identifier of the process type to include.  A null value indicates that all processes should be included.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_EXECUTING_PROCESSES, "Include Executing Processes", "A Boolean flag indicating whether or not currently executing processes should be included.", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_IDLE_PROCESSES, "Include Idle Processes", "A Boolean flag indicating whether or not currently idle processes should be included.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}
