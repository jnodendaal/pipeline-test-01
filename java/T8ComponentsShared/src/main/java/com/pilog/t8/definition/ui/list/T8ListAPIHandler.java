package com.pilog.t8.definition.ui.list;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8CheckBox and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ListAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGED = "$CE_SELECTION_CHANGED";
    public static final String EVENT_DOUBLE_CLICKED = "$CE_DOUBLE_CLICKED";

    public static final String OPERATION_ADD_ELEMENT = "$CO_ADD_ELEMENT";
    public static final String OPERATION_ADD_ELEMENTS = "$CO_ADD_ELEMENTS";
    public static final String OPERATION_REMOVE_ELEMENT = "$CO_REMOVE_ELEMENT";
    public static final String OPERATION_REMOVE_ELEMENTS = "$CO_REMOVE_ELEMENTS";
    public static final String OPERATION_REMOVE_ALL_ELEMENTS = "$CO_REMOVE_ALL_ELEMENTS";
    public static final String OPERATION_GET_ALL_ELEMENTS = "$CO_GET_ALL_ELEMENTS";
    public static final String OPERATION_GET_SELECTED_ELEMENTS = "$CO_GET_SELECTED_ELEMENTS";
    public static final String OPERATION_SET_SELECTED_ELEMENTS = "$CO_SET_SELECTED_ELEMENTS";
    public static final String OPERATION_CLEAR_SELECTION = "$CO_CLEAR_SELECTION";
    public static final String OPERATION_INCREMENT_SELECTED_ELEMENT_INDEX = "$CO_INCREMENT_SELECTED_ELEMENT_INDEX";
    public static final String OPERATION_DECREMENT_SELECTED_ELEMENT_INDEX = "$CO_DECREMENT_SELECTED_ELEMENT_INDEX";

    public static final String PARAMETER_ELEMENT = "$CP_ELEMENT";
    public static final String PARAMETER_ELEMENT_LIST = "$CP_ELEMENT_LIST";
    public static final String PARAMETER_SELECTED_ELEMENT_LIST = "$CP_SELECTED_ELEMENT_LIST";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the element selection on the list has changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED_ELEMENT_LIST, "Selected Elements", "The list of elements selected in the list", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DOUBLE_CLICKED);
        newEventDefinition.setMetaDisplayName("Double Clicked");
        newEventDefinition.setMetaDescription("This event occurs when the list is double clicked.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED_ELEMENT_LIST, "Selected Elements", "The list of elements selected in the list", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_ELEMENT);
        newOperationDefinition.setMetaDisplayName("Add Element");
        newOperationDefinition.setMetaDescription("This operation adds an element to the list.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT, "Element", "The element to add to the list.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_ELEMENTS);
        newOperationDefinition.setMetaDisplayName("Add Elements");
        newOperationDefinition.setMetaDescription("This operation adds elements to the list.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_LIST, "Element List", "The list of elements to add to the list.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REMOVE_ELEMENT);
        newOperationDefinition.setMetaDisplayName("Remove Element");
        newOperationDefinition.setMetaDescription("This operation removes an element from the list.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT, "Element", "The element to remove from the list.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REMOVE_ELEMENTS);
        newOperationDefinition.setMetaDisplayName("Remove Elements");
        newOperationDefinition.setMetaDescription("This operation removes elements from the list.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_LIST, "Element List", "The list of elements to remove from the list.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REMOVE_ALL_ELEMENTS);
        newOperationDefinition.setMetaDisplayName("Remove All Elements");
        newOperationDefinition.setMetaDescription("This operation removes all elements from the list.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_ALL_ELEMENTS);
        newOperationDefinition.setMetaDisplayName("Get All Elements");
        newOperationDefinition.setMetaDescription("This operation returns the complete element list.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_LIST, "Element List", "The list of elements.", T8DataType.LIST));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_ELEMENTS);
        newOperationDefinition.setMetaDisplayName("Get Selected Elements");
        newOperationDefinition.setMetaDescription("This operation returns the list of elements currently selected.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_LIST, "Element List", "The list of elements currently selected.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED_ELEMENTS);
        newOperationDefinition.setMetaDisplayName("Set Selected Elements");
        newOperationDefinition.setMetaDescription("This operation sets the selection of elements in the list.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_LIST, "Element List", "The list of elements to set as selected.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_SELECTION);
        newOperationDefinition.setMetaDisplayName("Clear Selection");
        newOperationDefinition.setMetaDescription("This operation sets all elements in the list as unselected.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_INCREMENT_SELECTED_ELEMENT_INDEX);
        newOperationDefinition.setMetaDisplayName("Increment Selected Element Index");
        newOperationDefinition.setMetaDescription("Increments the index of the selected elements in the list, effectively shifting all of them down the order.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DECREMENT_SELECTED_ELEMENT_INDEX);
        newOperationDefinition.setMetaDisplayName("Decrement Selected Element Index");
        newOperationDefinition.setMetaDescription("Decrements the index of the selected elements in the list, effectively shifting all of them up the order.");
        operations.add(newOperationDefinition);

        return operations;
    }
}
