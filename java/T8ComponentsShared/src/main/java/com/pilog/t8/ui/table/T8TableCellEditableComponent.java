package com.pilog.t8.ui.table;

import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8TableCellEditableComponent extends T8TableCellRenderableComponent
{
    public void cellDataEdited(Map<String, Object> dataRow, int rowIndex);
}
