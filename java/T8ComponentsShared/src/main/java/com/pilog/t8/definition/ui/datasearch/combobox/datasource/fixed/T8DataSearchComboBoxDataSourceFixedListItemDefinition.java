package com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public abstract class T8DataSearchComboBoxDataSourceFixedListItemDefinition extends T8Definition
{

    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_FIXED_LIST_NODE_ITEMS";
    public static final String DISPLAY_NAME = "Item";
    public static final String DESCRIPTION = "A Group node item";
    public static final String IDENTIFIER_PREFIX = "ITEM_";

    public enum Datum
    {
        ITEM_DISPLAY,
        ITEM_DESCRIPTION
    };
    // -------- Definition Meta-Data -------- //

    public T8DataSearchComboBoxDataSourceFixedListItemDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.ITEM_DISPLAY.toString(), "Item Name", "The name of this item"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.ITEM_DESCRIPTION.toString(), "Item Description", "The description for this item"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public String getItemDisplay()
    {
        return getDefinitionDatum(Datum.ITEM_DISPLAY);
    }

    public void setItemDisplay(String identifier)
    {
        setDefinitionDatum(Datum.ITEM_DISPLAY, identifier);
    }

    public String getItemDescription()
    {
        return getDefinitionDatum(Datum.ITEM_DESCRIPTION);
    }

    public void setItemDescription(String identifier)
    {
        setDefinitionDatum(Datum.ITEM_DESCRIPTION, identifier);
    }

    public abstract <V extends Object> V getItemValue();
}
