package com.pilog.t8.definition.ui.cellrenderer.table.label;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.text.Format;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public abstract class T8LabelTableCellRendererFormatDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_CELL_RENDERER_LABEL_FORMATER";
    public static final String DISPLAY_NAME = "Lable Cell Renderer Formatter";
    public static final String DESCRIPTION = "A Formatter that can be used to format the data of this label cell renderer.";
    public static final String IDENTIFIER_PREFIX = "C_LCRF_";
    public enum Datum {FORMAT_PATTERN};
    // -------- Definition Meta-Data -------- //

    public T8LabelTableCellRendererFormatDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract Format getFormat();

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FORMAT_PATTERN.toString(), "Format Pattern", "The format pattern that will be used for this formatter."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public String getFormatPattern()
    {
        return getDefinitionDatum(Datum.FORMAT_PATTERN);
    }

    public void setFormatPattern(String formatPattern)
    {
        setDefinitionDatum(Datum.FORMAT_PATTERN, formatPattern);
    }
}
