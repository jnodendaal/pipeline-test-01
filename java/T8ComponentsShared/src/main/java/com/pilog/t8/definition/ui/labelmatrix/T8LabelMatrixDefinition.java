package com.pilog.t8.definition.ui.labelmatrix;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LabelMatrixDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LABEL_MATRIX";
    public static final String DISPLAY_NAME = "Label Matrix";
    public static final String DESCRIPTION = "A component that displays a matrix of text Strings.";
    public static final String IDENTIFIER_PREFIX = "C_LABEL_MATRIX_";
    private enum Datum {HORIZONTAL_SPACING,
                        VERTICAL_SPACING,
                        CELL_DEFINITIONS,
                        COLUMN_DEFINITIONS,
                        ROW_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8LabelMatrixDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HORIZONTAL_SPACING.toString(), "Horizontal Spacing", "The horizontal spacing between Strings in the matrix.", 10));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.VERTICAL_SPACING.toString(), "Vertical Spacing", "The certical spacing between Strings in the matrix.", 10));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CELL_DEFINITIONS.toString(), "Cells", "Predefined cells in the matrix."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COLUMN_DEFINITIONS.toString(), "Columns", "Predefined columns in the matrix."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ROW_DEFINITIONS.toString(), "Rows", "Predefined rows in the matrix."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CELL_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8LabelMatrixCellDefinition.TYPE_IDENTIFIER));
        else if (Datum.COLUMN_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8LabelMatrixColumnDefinition.TYPE_IDENTIFIER));
        else if (Datum.ROW_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8LabelMatrixRowDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            return (T8Component)Class.forName("com.pilog.t8.ui.labelmatrix.T8LabelMatrix").getConstructor(T8LabelMatrixDefinition.class, T8ComponentController.class).newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8LabelMatrixAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8LabelMatrixAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    } 
    
    public int getHorizontalSpacing()
    {
        Integer spacing;
        
        spacing = (Integer)getDefinitionDatum(Datum.HORIZONTAL_SPACING.toString());
        return spacing != null ? spacing : 0;
    }
    
    public void setHorizontalSpacing(int spacing)
    {
        setDefinitionDatum(Datum.HORIZONTAL_SPACING.toString(), spacing);
    }
    
    public int getVerticalSpacing()
    {
        Integer spacing;
        
        spacing = (Integer)getDefinitionDatum(Datum.VERTICAL_SPACING.toString());
        return spacing != null ? spacing : 0;
    }
    
    public void setVerticalSpacing(int spacing)
    {
        setDefinitionDatum(Datum.VERTICAL_SPACING.toString(), spacing);
    }
    
    public List<T8LabelMatrixCellDefinition> getCellDefinitions()
    {
        return (List<T8LabelMatrixCellDefinition>)getDefinitionDatum(Datum.CELL_DEFINITIONS.toString());
    }
    
    public void setCellDefinitions(List<T8LabelMatrixCellDefinition> cellDefinitions)
    {
        setDefinitionDatum(Datum.CELL_DEFINITIONS.toString(), cellDefinitions);
    }
    
    public List<T8LabelMatrixColumnDefinition> getColumnDefinitions()
    {
        return (List<T8LabelMatrixColumnDefinition>)getDefinitionDatum(Datum.COLUMN_DEFINITIONS.toString());
    }
    
    public void setColumnDefinitions(List<T8LabelMatrixColumnDefinition> columnDefinitions)
    {
        setDefinitionDatum(Datum.COLUMN_DEFINITIONS.toString(), columnDefinitions);
    }
    
    public List<T8LabelMatrixRowDefinition> getRowDefinitions()
    {
        return (List<T8LabelMatrixRowDefinition>)getDefinitionDatum(Datum.ROW_DEFINITIONS.toString());
    }
    
    public void setRowDefinitions(List<T8LabelMatrixRowDefinition> rowDefinitions)
    {
        setDefinitionDatum(Datum.ROW_DEFINITIONS.toString(), rowDefinitions);
    }
}
