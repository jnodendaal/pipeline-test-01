package com.pilog.t8.definition.ui.datasearch.combobox.datasource.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_ENTITY_NODE_GROUP";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_ENTITY_NODE_GROUP";
    public static final String DISPLAY_NAME = "Group Node";
    public static final String DESCRIPTION = "A Group node containing a grouping of items";
    public static final String IDENTIFIER_PREFIX = "NODE_GROUP_";
    public enum Datum
    {
        GROUP_NAME,
        GROUP_NAME_FIELD_IDENTIFIER,
        GROUP_KEY_FIELD_IDENTIFIER,
        ITEM_DISPLAY_FIELD_IDENTIFIER,
        ITEM_DESCRIPTION_FIELD_IDENTIFIER,
        ITEM_VALUE_FIELD_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.GROUP_NAME.toString(), "Group Name", "The name of this group that will be displayed"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.GROUP_NAME_FIELD_IDENTIFIER.toString(), "Group Name Field Identifier", "The field identifier that will be used for the group display name"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.GROUP_KEY_FIELD_IDENTIFIER.toString(), "Group Key Field Identifier", "The field identifier that will be used to group the item nodes from the data source"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ITEM_DISPLAY_FIELD_IDENTIFIER.toString(), "Display Field Identifier", "The field identifier that will be used for the display value of the item"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ITEM_DESCRIPTION_FIELD_IDENTIFIER.toString(), "Description Field Identifier", "The field identifier that will be used for the description of the item"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ITEM_VALUE_FIELD_IDENTIFIER.toString(), "Value Field Identifier", "The field identifier that will be used for the value of the item"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.GROUP_NAME_FIELD_IDENTIFIER.toString().equals(datumId))
        {
            T8DataSearchComboBoxDataSourceEntityDefinition sourceEntityDefinition;
            T8DataEntityDefinition entityDefinition;
            ArrayList<T8DefinitionDatumOption> datumOptions;

            sourceEntityDefinition = (T8DataSearchComboBoxDataSourceEntityDefinition) getAncestorDefinition(T8DataSearchComboBoxDataSourceEntityDefinition.TYPE_IDENTIFIER);
            entityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), sourceEntityDefinition.getDataEntityIdentifier());

            datumOptions = createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions());

            datumOptions.add(0, new T8DefinitionDatumOption("Use Provided", null));
            return datumOptions;
        }
        else if(Datum.GROUP_KEY_FIELD_IDENTIFIER.toString().equals(datumId))
        {
            T8DataSearchComboBoxDataSourceEntityDefinition sourceEntityDefinition;
            T8DataEntityDefinition entityDefinition;
            ArrayList<T8DefinitionDatumOption> datumOptions;

            sourceEntityDefinition = (T8DataSearchComboBoxDataSourceEntityDefinition) getAncestorDefinition(T8DataSearchComboBoxDataSourceEntityDefinition.TYPE_IDENTIFIER);
            entityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), sourceEntityDefinition.getDataEntityIdentifier());

            datumOptions = createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions());

            datumOptions.add(0, new T8DefinitionDatumOption("None", null));
            return datumOptions;
        }
        else if(Datum.ITEM_DISPLAY_FIELD_IDENTIFIER.toString().equals(datumId))
        {
            T8DataSearchComboBoxDataSourceEntityDefinition sourceEntityDefinition;
            T8DataEntityDefinition entityDefinition;

            sourceEntityDefinition = (T8DataSearchComboBoxDataSourceEntityDefinition) getAncestorDefinition(T8DataSearchComboBoxDataSourceEntityDefinition.TYPE_IDENTIFIER);
            entityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), sourceEntityDefinition.getDataEntityIdentifier());

            return createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions());
        }
        else if(Datum.ITEM_DESCRIPTION_FIELD_IDENTIFIER.toString().equals(datumId))
        {
            T8DataSearchComboBoxDataSourceEntityDefinition sourceEntityDefinition;
            T8DataEntityDefinition entityDefinition;
            ArrayList<T8DefinitionDatumOption> datumOptions;

            sourceEntityDefinition = (T8DataSearchComboBoxDataSourceEntityDefinition) getAncestorDefinition(T8DataSearchComboBoxDataSourceEntityDefinition.TYPE_IDENTIFIER);
            entityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), sourceEntityDefinition.getDataEntityIdentifier());

            datumOptions = createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions());

            datumOptions.add(0, new T8DefinitionDatumOption("None", null));
            return datumOptions;
        }
        else if(Datum.ITEM_VALUE_FIELD_IDENTIFIER.toString().equals(datumId))
        {
            T8DataSearchComboBoxDataSourceEntityDefinition sourceEntityDefinition;
            T8DataEntityDefinition entityDefinition;

            sourceEntityDefinition = (T8DataSearchComboBoxDataSourceEntityDefinition) getAncestorDefinition(T8DataSearchComboBoxDataSourceEntityDefinition.TYPE_IDENTIFIER);
            entityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), sourceEntityDefinition.getDataEntityIdentifier());

            return createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions());
        }
        else return null;
    }

    public String getGroupName()
    {
        return (String)getDefinitionDatum(Datum.GROUP_NAME.toString());
    }

    public void setGroupName(String name)
    {
        setDefinitionDatum(Datum.GROUP_NAME.toString(), name);
    }

    public String getGroupNameFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.GROUP_NAME_FIELD_IDENTIFIER.toString());
    }

    public void setGroupNameFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.GROUP_NAME_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getGroupKeyFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.GROUP_KEY_FIELD_IDENTIFIER.toString());
    }

    public void setGroupKeyFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.GROUP_KEY_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getItemDisplayFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ITEM_DISPLAY_FIELD_IDENTIFIER.toString());
    }

    public void setItemDisplayFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ITEM_DISPLAY_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getItemDescriptionFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ITEM_DESCRIPTION_FIELD_IDENTIFIER.toString());
    }

    public void setItemDescriptionFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ITEM_DESCRIPTION_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getItemValueFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ITEM_VALUE_FIELD_IDENTIFIER.toString());
    }

    public void setItemValueFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ITEM_VALUE_FIELD_IDENTIFIER.toString(), identifier);
    }
}
