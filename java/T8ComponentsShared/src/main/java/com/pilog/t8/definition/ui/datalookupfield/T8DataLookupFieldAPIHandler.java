package com.pilog.t8.definition.ui.datalookupfield;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataLookupField and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataLookupFieldAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGED = "$CE_SELECTION_CHANGED";

    // Operations.
    public static final String OPERATION_SET_SELECTED_DATA_ENTITY_KEY = "$CO_SET_SELECTED_DATA_ENTITY_KEY";
    public static final String OPERATION_GET_SELECTED_DATA_ENTITY = "$CO_GET_SELECTED_DATA_ENTITY";
    public static final String OPERATION_GET_SELECTED_DATA_VALUE = "$CO_GET_SELECTED_DATA_VALUE";
    public static final String OPERATION_SET_PREFILTER = "$CO_SET_PREFILTER";
    public static final String OPERATION_CLEAR = "$CO_CLEAR";
    public static final String OPERATION_SET_EDITABLE = "$CO_SET_EDITABLE";

    // Parameters.
    public static final String PARAMETER_DATA_ENTITY = "$CP_DATA_ENTITY";
    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_DATA_VALUE = "$CP_DATA_VALUE";
    public static final String PARAMETER_DATA_VALUE_MAP = "$CP_DATA_VALUE_MAP";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$CP_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_REFRESH_DATA = "$CP_REFRESH_DATA";
    public static final String PARAMETER_EDITABLE = "$CP_EDITABLE";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selected entity has changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The selected data entity.", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED_DATA_ENTITY_KEY);
        newOperationDefinition.setMetaDisplayName("Set Selected Entity Key");
        newOperationDefinition.setMetaDescription("This operations uses the supplied value map as an entity key and sets the entity identified by the key as the selected entity in the lookup field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE_MAP, "Data Value Map", "A Map containing the key values to use for entity retrieval.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Selected Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently selected and displayed by the component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The selected data entity.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_VALUE);
        newOperationDefinition.setMetaDisplayName("Get Selected Value");
        newOperationDefinition.setMetaDescription("This operations returns the data value currently selected and displayed by the component (if a value field is specified in the definition).");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE, "Data Value", "The selected data value.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the pre-filter by which data available in the lookup dialog will be filtered.  This method removes all previously added prefilters.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the data lookup dialog.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the table that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR);
        newOperationDefinition.setMetaDisplayName("Clear");
        newOperationDefinition.setMetaDescription("Clears the selected data entity from the lookup field.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_EDITABLE);
        newOperationDefinition.setMetaDisplayName("Set Editable");
        newOperationDefinition.setMetaDescription("Sets the editability of this component.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_EDITABLE, "Editable", "If this component should be editable.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
