/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.ui.filter.panel.component.list;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.T8DefaultFilterPanelComponentDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.cellrenderer.T8CellRendererDefinition;
import com.pilog.t8.definition.ui.filter.panel.datasource.T8FilterPanelListComponentItemDataSourceDefinition;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8FilterPanelListComponentDefinition extends T8DefaultFilterPanelComponentDefinition
{
    // -------- Definition Meta-Data -------- //

    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_LIST_COMPONENT";
    public static final String DISPLAY_NAME = "Filter Panel List Component";
    public static final String DESCRIPTION = "Filter Panel List Componenet";
    public static final String IDENTIFIER_PREFIX = "C_FP_";

    public static final String VERSION = "0";

    public enum Datum
    {
        FILTER_PANEL_ITEM_DATASOURCE_DEFINITION,
        RENDERER_COMPONENT_DEFINITION,
        LOAD_ON_STARTUP
    };
// -------- Definition Meta-Data -------- //
    public T8FilterPanelListComponentDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOAD_ON_STARTUP.toString(), "Load on Start", "If this option is active then the component will load its data on start up.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FILTER_PANEL_ITEM_DATASOURCE_DEFINITION.toString(), "Data Source", "The data source that will be used to populate this components options."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.RENDERER_COMPONENT_DEFINITION.toString(), "Renderer", "The renderer to use for displaying the contents of this column's cells."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            com.pilog.t8.definition.T8DefinitionContext definitionContext, String datumIdentifier)
            throws Exception
    {
        if (Datum.FILTER_PANEL_ITEM_DATASOURCE_DEFINITION.toString().equals(datumIdentifier))return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FilterPanelListComponentItemDataSourceDefinition.GROUP_IDENTIFIER));
        else if (Datum.RENDERER_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8CellRendererDefinition.GROUP_IDENTIFIER));
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FilterPanelListComponentAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FilterPanelListComponentAPIHandler.getOperationDefinitions();
    }

    public void setDatasourceDefinition(T8FilterPanelListComponentItemDataSourceDefinition definition)
    {
        setDefinitionDatum(Datum.FILTER_PANEL_ITEM_DATASOURCE_DEFINITION.toString(), definition);
    }

    public T8FilterPanelListComponentItemDataSourceDefinition getDatasourceDefinition()
    {
        return (T8FilterPanelListComponentItemDataSourceDefinition) getDefinitionDatum(Datum.FILTER_PANEL_ITEM_DATASOURCE_DEFINITION.toString());
    }

    public T8ComponentDefinition getRendererComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.RENDERER_COMPONENT_DEFINITION.toString());
    }

    public void setRendererComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.RENDERER_COMPONENT_DEFINITION.toString(), definition);
    }

    public Boolean isLoadOnStartup()
    {
        return (Boolean)getDefinitionDatum(Datum.LOAD_ON_STARTUP.toString());
    }

    public void setLoadOnStartup(Boolean loadOnStartup)
    {
        setDefinitionDatum(Datum.LOAD_ON_STARTUP.toString(), loadOnStartup);
    }
}
