package com.pilog.t8.definition.ui.processlist;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.list.T8ComponentListDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessListDefinition extends T8ComponentListDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_PROCESS_LIST";
    public static final String DISPLAY_NAME = "Process List";
    public static final String DESCRIPTION = "A component that displays the process list available to the currently logged in user.";
    public enum Datum {PROCESS_MENU_BACKGROUND_PAINTER_IDENTIFIER,
                       PROCESS_LIST_BACKGROUND_PAINTER_IDENTIFIER,
                       PROCESS_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //


    private T8PainterDefinition processListBackgroundPainterDefinition;
    private T8PainterDefinition processMenuBackgroundPainterDefinition;
    private T8PainterDefinition processBackgroundPainterDefinition;

    public T8ProcessListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROCESS_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Process Menu Background Painter", "The painter to use for painting the background of the process menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROCESS_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Process List Background Painter", "The painter to use for painting the background of the process list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PROCESS_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Process Background Painter", "The painter to use for painting the background of the processes in the process list."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PROCESS_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.PROCESS_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.PROCESS_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DefinitionManager definitionManager;
        String painterId;

        super.initializeDefinition(context, inputParameters, configurationSettings);

        definitionManager = context.getServerContext().getDefinitionManager();
        painterId = getProcessListBackgroundPainterIdentifier();
        if (painterId != null)
        {
            processListBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getProcessMenuBackgroundPainterIdentifier();
        if (painterId != null)
        {
            processMenuBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }

        painterId = getProcessBackgroundPainterIdentifier();
        if (painterId != null)
        {
            processBackgroundPainterDefinition = (T8PainterDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), painterId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.processlist.T8ProcessListView", new Class<?>[]{T8ProcessListDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8ProcessListAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8ProcessListAPIHandler.getOperationDefinitions();
    }

    public T8PainterDefinition getProcessListBackgroundPainterDefinition()
    {
        return processListBackgroundPainterDefinition;
    }

    public T8PainterDefinition getProcessMenuBackgroundPainterDefinition()
    {
        return processMenuBackgroundPainterDefinition;
    }

    public T8PainterDefinition getProcessBackgroundPainterDefinition()
    {
        return processBackgroundPainterDefinition;
    }

    public String getProcessListBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROCESS_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setProcessListBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROCESS_LIST_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getProcessMenuBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROCESS_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setProcessMenuBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROCESS_MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getProcessBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PROCESS_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setProcessBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PROCESS_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }
}
