package com.pilog.t8.definition.ui;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8FlowClient and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ClientControllerApiHandler
{
    public static ArrayList<T8ComponentEventDefinition> events;
    public static ArrayList<T8ComponentOperationDefinition> operations;

    // These should NOT be changed, only added to.
    public static final String EVENT_MODULE_LOADED = "$CE_MODULE_LOADED";
    public static final String EVENT_TASK_COMPLETED = "$CE_TASK_COMPLETED";
    public static final String EVENT_TASKS_COMPLETED = "$CE_TASKS_COMPLETED";
    public static final String OPERATION_LOAD_MODULE = "$CO_LOAD_MODULE";
    public static final String OPERATION_SWITCH_FLOW = "$CO_SWITCH_FLOW";
    public static final String OPERATION_START_NEW_FLOW = "$CO_START_NEW_FLOW";
    public static final String PARAMETER_MODULE_IDENTIFIER = "$CP_MODULE_IDENTIFIER";
    public static final String PARAMETER_FLOW_IDENTIFIER = "$CP_FLOW_IDENTIFIER";
    public static final String PARAMETER_FLOW_INSTANCE_IDENTIFIER = "$CP_FLOW_INSTANCE_IDENTIFIER";
    public static final String PARAMETER_TASK_INSTANCE_IDENTIFIER = "$CP_TASK_INSTANCE_IDENTIFIER";
    public static final String PARAMETER_INPUT_PARAMETERS = "$CP_INPUT_PARAMETERS";

    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_MODULE_LOADED);
        newEventDefinition.setMetaDisplayName("Module Loaded");
        newEventDefinition.setMetaDescription("This event occurs after a new Module has been loaded into the flow client and initialized.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MODULE_IDENTIFIER, "Module Identifier", "The Identifier of the Module that has been loaded.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TASKS_COMPLETED);
        newEventDefinition.setMetaDisplayName("Tasks Completed");
        newEventDefinition.setMetaDescription("This event occurs after all available tasks for the current user have been completed and there are no remaining tasks to claim.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance", "The Identifier of the Flow instance of which all tasks have been completed, belongs.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);

         newEventDefinition = new T8ComponentEventDefinition(EVENT_TASK_COMPLETED);
        newEventDefinition.setMetaDisplayName("Task Completed");
        newEventDefinition.setMetaDescription("This event occurs after a specific task has been completed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance", "The Identifier of the Flow instance to which the task that has been completed, belongs.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TASK_INSTANCE_IDENTIFIER, "Task Instance", "The Identifier of the Task instance that has been completed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_MODULE);
        newOperationDefinition.setMetaDisplayName("Load Module");
        newOperationDefinition.setMetaDescription("This operations loads a new module into the flow client and initializes it.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MODULE_IDENTIFIER, "Module Identifier", "The Identifier of the Module to load.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SWITCH_FLOW);
        newOperationDefinition.setMetaDisplayName("Switch Flow");
        newOperationDefinition.setMetaDescription("This operations switches the flow to which the client is set.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_INSTANCE_IDENTIFIER, "Flow Instance Identifier", "The Instance Identifier of the flow to switch to.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_START_NEW_FLOW);
        newOperationDefinition.setMetaDisplayName("Start New Flow");
        newOperationDefinition.setMetaDescription("This operations start the specified flow and then switches the client to the newly created Flow instance.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FLOW_IDENTIFIER, "Flow Identifier", "The Identifier of the flow to start.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INPUT_PARAMETERS, "Input Parameters", "The input parameters to supplied to the new flow instance.", T8DataType.MAP));
        operations.add(newOperationDefinition);
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}
