package com.pilog.t8.definition.ui.notificationlist;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author Gavin Boshoff
 */
public class T8NotificationListAPIHandler
{
    private static final ArrayList<T8ComponentOperationDefinition> operations;
    private static final ArrayList<T8ComponentEventDefinition> events;

    // These should NOT be changed, only added to.
    public static final String EVENT_NOTIFICATION_DISMISSED = "$CE_NOTIFICATION_DISMISSED";
    public static final String EVENT_NOTIFICATION_CLOSED = "$CE_NOTIFICATION_CLOSED";
    public static final String OPERATION_FILTER_NOTIFICATION_LIST = "$CO_FILTER_NOTIFICATION_LIST";
    public static final String PARAMETER_NOTIFICATION_IDENTIFIER = "$P_NOTIFICATION_IDENTIFIER";
    public static final String PARAMETER_NOTIFICATION_INSTANCE_IDENTIFIER = "$P_NOTIFICATION_INSTANCE_IDENTIFIER";
    public static final String PARAMETER_INCLUDE_NEW_NOTIFICATIONS = "$P_INCLUDE_NEW_NOTIFICATIONS";
    public static final String PARAMETER_INCLUDE_OLD_NOTIFICATIONS = "$P_INCLUDE_OLD_NOTIFICATIONS";
    public static final String PARAMETER_INCLUDE_RECEIVED_NOTIFICATIONS = "$P_INCLUDE_RECEIVED_NOTIFICATIONS";

    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_NOTIFICATION_DISMISSED);
        newEventDefinition.setMetaDisplayName("Notification Dismissed");
        newEventDefinition.setMetaDescription("This event occurs when a notification is dismissed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NOTIFICATION_IDENTIFIER, "Notification Identifier", "The Identifier of the notification that was dismissed.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NOTIFICATION_INSTANCE_IDENTIFIER, "Notification Instance Identifier", "The Instance Identifier of the notification that was dismissed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);
        
        newEventDefinition = new T8ComponentEventDefinition(EVENT_NOTIFICATION_CLOSED);
        newEventDefinition.setMetaDisplayName("Notification Closed");
        newEventDefinition.setMetaDescription("This event occurs when a notification is closed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NOTIFICATION_IDENTIFIER, "Notification Identifier", "The Identifier of the notification that was closed.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NOTIFICATION_INSTANCE_IDENTIFIER, "Notification Instance Identifier", "The Instance Identifier of the notification that was closed.", T8DataType.DEFINITION_INSTANCE_IDENTIFIER));
        events.add(newEventDefinition);
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_FILTER_NOTIFICATION_LIST);
        newOperationDefinition.setMetaDisplayName("Filter Notification List");
        newOperationDefinition.setMetaDescription("This operations fiters the notification list according to the specified criteria.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NOTIFICATION_IDENTIFIER, "Process Identifier", "The identifier of the process type to include.  A null value indicates that all processes should be included.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_NEW_NOTIFICATIONS, "Include New Notifications", "A Boolean flag indicating whether or not new (unreceived) notifications should be included.", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_RECEIVED_NOTIFICATIONS, "Include Received Notifications", "A Boolean flag indicating whether or not received notifications should be included.", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_OLD_NOTIFICATIONS, "Include Old Notifications", "A Boolean flag indicating whether or not old (historic) notifications should be included.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);
    }

    static ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    static ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}