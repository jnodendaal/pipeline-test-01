/**
 * Created on 10 Sep 2015, 11:50:16 AM
 */
package com.pilog.t8.definition.ui.checkbox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8MultiSelectionCheckBoxGroupDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MULTI_SELECT_CHECK_BOX_GROUP";
    public static final String DISPLAY_NAME = "Multi Selection Check Box Group";
    public static final String DESCRIPTION = "A component that allows the user to select a set of the checkbox options available in a group.";
    public static final String IDENTIFIER_PREFIX = "C_MULTI_SEL_CHECK_BOX_GRP_";
    public enum Datum {CHECKBOXES};
    // -------- Definition Meta-Data -------- //

    public T8MultiSelectionCheckBoxGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHECKBOXES.toString(), "Checkboxes", "The checkboxes belonging to this group."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.CHECKBOXES.toString().equals(datumIdentifier))
        {
           return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8CheckBoxDefinition.GROUP_IDENTIFIER));
        } else return super.getDatumOptions(definitionContext,datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.checkbox.T8MultiSelectionCheckBoxGroup", new Class<?>[]{T8MultiSelectionCheckBoxGroupDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8MultiSelectionCheckBoxGroupAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8MultiSelectionCheckBoxGroupAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<? extends T8ComponentDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<>(getDefinitionDatum(Datum.CHECKBOXES));
    }

}