package com.pilog.t8.definition.ui.datarecordtable;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableChange implements Serializable
{
    public static final int CHANGE_TYPE_INSERT = 0;
    public static final int CHANGE_TYPE_UPDATE = 1;
    public static final int CHANGE_TYPE_DELETE = 2;

    private int changeType;
    private DataRecord dataRecord;

    public T8DataRecordTableChange(int changeType, DataRecord dataRecord)
    {
        this.changeType = changeType;
        this.dataRecord = dataRecord;
    }

    public int getChangeType()
    {
        return changeType;
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public void setDataRecord(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
    }
}
