package com.pilog.t8.ui.table;

import com.pilog.t8.ui.T8Component;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8TableCellRendererComponent extends T8Component
{
    public enum RenderParameter {SELECTED, FOCUSED, EDITABLE, ROW, COLUMN, LEAF, EXPANDED};
    
    public Object getAdditionalRenderData(Map<String, Object> dataRow, String fieldIdentifier);
    public void setRendererData(T8TableCellRenderableComponent sourceComponent, Map<String, Object> dataRow, String fieldIdentifier, Map<RenderParameter, Object> renderParameters);
}
