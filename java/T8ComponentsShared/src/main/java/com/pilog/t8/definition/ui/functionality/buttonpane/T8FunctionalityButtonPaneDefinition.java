package com.pilog.t8.definition.ui.functionality.buttonpane;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.functionality.group.T8FunctionalityGroupDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.T8DataObjectProviderComponentDefinition;
import com.pilog.t8.definition.ui.flowpane.T8DynamicButtonFlowPaneDefinition;
import com.pilog.t8.definition.ui.functionality.view.T8FunctionalityViewPaneDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityButtonPaneDefinition extends T8DynamicButtonFlowPaneDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FUNCTIONALITY_BUTTON_PANE";
    public static final String DISPLAY_NAME = "Functionality Button Pane";
    public static final String DESCRIPTION = "A component that displays all available system functionality within a session context in a flow pane and allows the user to select one of the functionalities for execution.";
    public static final String IDENTIFIER_PREFIX = "C_FUNC_MENU_BAR_";
    public enum Datum {MENU_BACKGROUND_PAINTER_IDENTIFIER,
                       FUNCTIONALITY_GROUP_IDENTIFIER,
                       FUNCTIONALITY_VIEW_IDENTIFIER,
                       TARGET_OBJECT_PROVIDER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8FunctionalityGroupDefinition functionalityGroupDefinition;
    private T8PainterDefinition menuBackgroundPainterDefinition;

    public T8FunctionalityButtonPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Background Painter", "The painter to use for painting popup menu backgrounds."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString(), "Functionality Group", "The group of functionality that will be displayed by this menu."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_VIEW_IDENTIFIER.toString(), "Functionality View", "The view where selected functionality will be opened."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_OBJECT_PROVIDER_IDENTIFIER.toString(), "Target Object Provider", "The component that will provide the data object on which the functionalities in this menu will be executed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityGroupDefinition.GROUP_IDENTIFIER));
        else if (Datum.FUNCTIONALITY_VIEW_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            List<T8Definition> localDefinitions;

            localDefinitions = this.getLocalDefinitions();
            options = new ArrayList<T8DefinitionDatumOption>();
            for (T8Definition localDefinition : localDefinitions)
            {
                if (localDefinition.getMetaData().getTypeId().equals(T8FunctionalityViewPaneDefinition.TYPE_IDENTIFIER))
                {
                    options.add(new T8DefinitionDatumOption(localDefinition.getIdentifier(), localDefinition.getIdentifier()));
                }
            }

            return options;
        }
        else if (Datum.TARGET_OBJECT_PROVIDER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            List<T8Definition> localDefinitions;

            localDefinitions = this.getLocalDefinitions();
            options = new ArrayList<T8DefinitionDatumOption>();
            for (T8Definition localDefinition : localDefinitions)
            {
                if (localDefinition instanceof T8DataObjectProviderComponentDefinition)
                {
                    options.add(new T8DefinitionDatumOption(localDefinition.getIdentifier(), localDefinition.getIdentifier()));
                }
            }

            return options;
        }
        else if (Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String functionalityGroupId;
        String backgroundPainterId;

        // Make sure the super definition is initialized.
        super.initializeDefinition(context, inputParameters, configurationSettings);

        // Load the background painter to use for popup menus.
        backgroundPainterId = getMenuBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            menuBackgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }

        // Load the functionality group.
        functionalityGroupId = getFunctionalityGroupIdentifier();
        if (functionalityGroupId != null)
        {
            T8DefinitionManager definitionManager;

            definitionManager = context.getServerContext().getDefinitionManager();
            functionalityGroupDefinition = (T8FunctionalityGroupDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), functionalityGroupId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.functionality.menubar.T8FunctionalityMenuBar").getConstructor(T8FunctionalityButtonPaneDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FunctionalityButtonPaneAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FunctionalityButtonPaneAPIHandler.getOperationDefinitions();
    }

    public T8PainterDefinition getMenuBackgroundPainterDefinition()
    {
        return menuBackgroundPainterDefinition;
    }

    public T8FunctionalityGroupDefinition getFunctionalityGroupDefinition()
    {
        return functionalityGroupDefinition;
    }

    public String getMenuBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public String getFunctionalityGroupIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString());
    }

    public void setFunctionalityGroupIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString(), identifier);
    }

    public String getFunctionalityViewIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_VIEW_IDENTIFIER.toString());
    }

    public void setFunctionalityViewIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_VIEW_IDENTIFIER.toString(), identifier);
    }

    public String getTargetObjectProviderIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_OBJECT_PROVIDER_IDENTIFIER.toString());
    }

    public void setTargetObjectProviderIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_OBJECT_PROVIDER_IDENTIFIER.toString(), identifier);
    }
}
