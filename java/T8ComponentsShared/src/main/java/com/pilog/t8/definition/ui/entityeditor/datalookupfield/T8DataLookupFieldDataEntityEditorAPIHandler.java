package com.pilog.t8.definition.ui.entityeditor.datalookupfield;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datalookupfield.T8DataLookupFieldAPIHandler;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataLookupFieldDataEntityEditorAPIHandler and responds to
 * specific requests regarding these definitions.  It is extremely important
 * that only new definitions are added to this class and none of the old
 * definitions removed or altered, since this will affect backwards
 * compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataLookupFieldDataEntityEditorAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_SET_DATA_ENTITY = "$CO_SET_DATA_ENTITY";
    public static final String OPERATION_GET_DATA_ENTITY = "$CO_GET_DATA_ENTITY";
    public static final String OPERATION_COMMIT_CHANGES = "$CO_COMMIT_CHANGES";
    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";


    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8DataLookupFieldAPIHandler.getEventDefinitions());

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8DataLookupFieldAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Set Data Entity");
        newOperationDefinition.setMetaDescription("This operations sets the data entity displayed by and editable via the editor.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity to set on the editor.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Data Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently displayed by the editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity currently displayed by the editor.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_COMMIT_CHANGES);
        newOperationDefinition.setMetaDisplayName("Commit Changes");
        newOperationDefinition.setMetaDescription("This operations commits all changes on the entity editor and all of it's descendent editors to the current editor entity.");
        operations.add(newOperationDefinition);

        return operations;
    }
}
