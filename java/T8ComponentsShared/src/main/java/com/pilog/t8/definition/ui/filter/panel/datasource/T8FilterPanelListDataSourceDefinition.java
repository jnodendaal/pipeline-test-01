package com.pilog.t8.definition.ui.filter.panel.datasource;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.filter.panel.*;
import java.lang.reflect.Constructor;
import java.util.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelListDataSourceDefinition extends T8FilterPanelListComponentItemDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_DATASOURCE_LIST";
    public static final String DISPLAY_NAME = "List";
    public static final String DESCRIPTION = "A Custom list data source";
    public enum Datum
    {
        ITEMS
    };
    // -------- Definition Meta-Data -------- //

    public T8FilterPanelListDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8FilterPanelListComponentItemDataSource getInstance()
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.filter.panel.datasource.T8FilterPanelListDataSource").getConstructor(T8FilterPanelListDataSourceDefinition.class);
            return (T8FilterPanelListComponentItemDataSource) constructor.newInstance(this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ITEMS.toString(), "Item List", "The list of items that will be used to as a selection for the user to choose from."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            T8DefinitionContext definitionContext, String datumIdentifier)
            throws Exception
    {
        if (Datum.ITEMS.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FilterPanelListItemDefinition.GROUP_IDENTIFIER));
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.ITEMS.toString().equals(datumIdentifier)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor) constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    public void setItems(List<T8FilterPanelListItemDefinition> definitions)
    {
        setDefinitionDatum(Datum.ITEMS.toString(), definitions);
    }

    public List<T8FilterPanelListItemDefinition> getItems()
    {
        return (List<T8FilterPanelListItemDefinition>) getDefinitionDatum(Datum.ITEMS.toString());
    }
}
