package com.pilog.t8.definition.ui.tabbedpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TabbedPaneTabDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TABBED_PANE_TAB";
    public static final String DISPLAY_NAME = "Tabbed Pane Tab";
    public static final String DESCRIPTION = "A container to which one other UI component can be added.";
    public static final String IDENTIFIER_PREFIX = "C_TABBED_PANE_TAB_";
    public enum Datum {TAB_COMPONENT_DEFINITION,
                       TITLE,
                       ENABLED};
    // -------- Definition Meta-Data -------- //

    public T8TabbedPaneTabDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> dataTypes;

        dataTypes = super.getDatumTypes();
        dataTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.TAB_COMPONENT_DEFINITION.toString(), "Tab Component", "The component contained by this tab."));
        dataTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TITLE.toString(), "Title", "The title of the tab."));
        dataTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ENABLED.toString(), "Enabled", "A boolean value indicating whether or not this tab is enabled (accessible)."));

        return dataTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TAB_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            return (T8Component)Class.forName("com.pilog.t8.ui.tabbedpane.T8TabbedPaneTab").getConstructor(T8TabbedPaneTabDefinition.class, T8ComponentController.class).newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<T8ComponentEventDefinition>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<T8ComponentOperationDefinition>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;
        T8ComponentDefinition tabComponentDefinition;
        
        tabComponentDefinition = getTabComponentDefinition();
        childComponentDefinitions = new ArrayList<T8ComponentDefinition>();
        if (tabComponentDefinition != null) childComponentDefinitions.add(tabComponentDefinition);
        return childComponentDefinitions;
    }     
    
    public T8ComponentDefinition getTabComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.TAB_COMPONENT_DEFINITION.toString());
    }

    public void setTabComponentDefinition(T8ComponentDefinition componentDefinition)
    {
         setDefinitionDatum(Datum.TAB_COMPONENT_DEFINITION.toString(), componentDefinition);
    }
    
    public String getTitle()
    {
        return (String)getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String componentIdentifier)
    {
        setDefinitionDatum(Datum.TITLE.toString(), componentIdentifier);
    }

    public boolean isEnabled()
    {
        return (Boolean)getDefinitionDatum(Datum.ENABLED.toString());
    }

    public void setEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.ENABLED.toString(), enabled);
    }
}
