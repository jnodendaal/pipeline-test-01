package com.pilog.t8.definition.ui.wizard;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8WizardAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_WIZARD_FINISHED = "$CE_WIZARD_FINISHED";
    public static final String EVENT_WIZARD_CANCELED = "$CE_WIZARD_CANCELED";
    public static final String EVENT_WIZARD_NEXT = "$CE_WIZARD_NEXT";
    public static final String EVENT_WIZARD_PREVIOUS = "$CE_WIZARD_PREVIOUS";

    public static final String OPERATION_RESET = "$OC_RESET";
    public static final String OPERATION_SET_PARAMETERS = "$OC_SET_PARAMETERS";
    public static final String OPERATION_MOVE_NEXT = "$OC_MOVE_NEXT";
    public static final String OPERATION_MOVE_PREVIOUS = "$OC_MOVE_PREVIOUS";
    public static final String OPERATION_SKIP = "$OC_SKIP";
    public static final String OPERATION_VALIDATE_CURRENT_STEP = "$OC_VALIDATE_CURRENT_STEP";
    public static final String OPERATION_SET_STEP_NAME = "$OC_SET_STEP_NAME";
    public static final String OPERATION_SET_PREVIOUS_VISIBILITY = "$OC_SET_PREVIOUS_VISIBILITY";
    public static final String OPERATION_SET_SKIP_VISIBILITY = "$OC_SET_SKIP_VISIBILITY";
    public static final String OPERATION_SET_CANCEL_VISIBILITY = "$OC_SET_CANCEL_VISIBILITY";
    public static final String OPERATION_SET_FINISH_VISIBILITY = "$OC_SET_FINISH_VISIBILITY";
    public static final String OPERATION_SET_NEXT_VISIBILITY = "$OC_SET_NEXT_VISIBILITY";
    public static final String OPERATION_SET_NAVIGATION_PANEL_VISIBILITY = "$OC_SET_NAVIGATION_PANEL_VISIBILITY";

    public static final String PARAMETER_PREVIOUS_STEP_IDENTIFIER = "$CP_PREVIOUS_STEP_IDENTIFIER";
    public static final String PARAMETER_WIZARD_PARAMETERS = "$P_WIZARD_PARAMETERS";
    public static final String PARAMETER_STEP_IDENTIFIER = "$CP_STEP_IDENTIFIER";
    public static final String PARAMETER_STEP_NAME= "$CP_STEP_NAME";
    public static final String PARAMETER_VISIBLE= "$CP_VISIBLE";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_WIZARD_FINISHED);
        newEventDefinition.setMetaDisplayName("Wizard Finished");
        newEventDefinition.setMetaDescription("This event occurs when the user presses the finish button on the wizard");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WIZARD_PARAMETERS, "Wizard Parameters", "The complete set of wizard parameters compiled throughout its steps.", T8DataType.MAP));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_WIZARD_CANCELED);
        newEventDefinition.setMetaDisplayName("Wizard Canceled");
        newEventDefinition.setMetaDescription("This event occurs when the user presses the cancel button on the wizard.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_WIZARD_NEXT);
        newEventDefinition.setMetaDisplayName("Wizard Next");
        newEventDefinition.setMetaDescription("This event occurs when the user presses the next button on the wizard.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_STEP_IDENTIFIER, "Step Identifier", "The identifier of the step that is currently showing after the step change.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PREVIOUS_STEP_IDENTIFIER, "Previous Step Identifier", "The identifier of the step that was shown before the step change.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_WIZARD_PREVIOUS);
        newEventDefinition.setMetaDisplayName("Wizard Previous");
        newEventDefinition.setMetaDescription("This event occurs when the user presses the previous button on the wizard.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_STEP_IDENTIFIER, "Step Identifier", "The identifier of the step that is currently showing after the step change.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PREVIOUS_STEP_IDENTIFIER, "Previous Step Identifier", "The identifier of the step that was shown before the step change.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        T8ComponentOperationDefinition newOperationDefinition;

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_RESET);
        newOperationDefinition.setMetaDisplayName("Reset Wizard");
        newOperationDefinition.setMetaDescription("Resets the state of the wizard, clearing selected parameters and switching to the first step.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PARAMETERS);
        newOperationDefinition.setMetaDisplayName("Set Parameters");
        newOperationDefinition.setMetaDescription("Sets the specified wizard parameters.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WIZARD_PARAMETERS, "Wizard Parameters", "The map of parameters to set on the wizard.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_STEP_NAME);
        newOperationDefinition.setMetaDisplayName("Set Step Name");
        newOperationDefinition.setMetaDescription("Sets the new name of a step.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_STEP_IDENTIFIER, "Step Identifier", "The identifier used to identify this step.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_STEP_NAME, "Step Name", "The new name for this step.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_MOVE_NEXT);
        newOperationDefinition.setMetaDisplayName("Move Next");
        newOperationDefinition.setMetaDescription("Moves the wizard to the next step, as if the user pressed the next button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_MOVE_PREVIOUS);
        newOperationDefinition.setMetaDisplayName("Move Previous");
        newOperationDefinition.setMetaDescription("Moves the wizard to the previous step, as if the user pressed the previous button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SKIP);
        newOperationDefinition.setMetaDisplayName("Skip Step");
        newOperationDefinition.setMetaDescription("Moves the wizard to the next step, as if the user pressed the skip button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_VALIDATE_CURRENT_STEP);
        newOperationDefinition.setMetaDisplayName("Validate Current Step");
        newOperationDefinition.setMetaDescription("Preforms the validation script of the current step, as if the user pressed the next button without moving to the next step.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_CANCEL_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Cancel Button Visibility");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If this button should be visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_FINISH_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Finish Button Visibility");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If this button should be visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREVIOUS_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Previous Button Visibility");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If this button should be visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SKIP_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Skip Button Visibility");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If this button should be visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_NEXT_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Next Button Visibility");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If this button should be visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_NAVIGATION_PANEL_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Navigation Panel Visibility");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If the navigation panel should be visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
