/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.ui.filter.panel.component;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public interface T8FilterPanelComponentDefinition
{
    String DESCRIPTION = "Filter Panel Component";
    String DISPLAY_NAME = "Filter Panel Componenet";
    // -------- Definition Meta-Data -------- //
    String GROUP_IDENTIFIER = "@DG_COMPONENT_FILTER_PANEL_COMPONENT";
    String IDENTIFIER_PREFIX = "C_FP_";
    String TYPE_IDENTIFIER = "@DT_COMPONENT_FILTER_PANEL_COMPONENT";
    String VERSION = "0";

    public static enum Datum
    {
        DATA_ENTITY_KEY_FIELD_IDENTIFIER,
        DISPLAY_LABEL,
        IS_TAG_FIELD
    };
    
    String getDataEntityKeyFieldIdentifier();

    String getDisplayLabel();

    Boolean isTagField();

    void setDataEntityKeyFieldIdentifier(String definitions);

    void setDisplayLabel(String name);

    void setTagField(Boolean isTagField);

}
