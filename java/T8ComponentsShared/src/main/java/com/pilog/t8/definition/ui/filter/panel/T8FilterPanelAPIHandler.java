package com.pilog.t8.definition.ui.filter.panel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelAPIHandler;
import java.util.ArrayList;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_FILTER_CREATED = "$CE_FILTER_CREATED";

    public static final String OPERATION_CREATE_FILTER = "$OC_CREATE_FILTER";
    public static final String OPERATION_SET_PRE_FILTER = "$OC_SET_PRE_FILTER";
    public static final String OPERATION_ADD_PRE_FILTER_CRITERIA = "$OC_ADD_PRE_FILTER_CRITERIA";
    public static final String OPERATION_REFRESH = "$OC_REFRESH";


    public static final String PARAMETER_FILTER = "$CP_FILTER";
    public static final String PARAMETER_FILTER_CRITERIA = "$CP_FILTER_CRITERIA";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8PanelAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FILTER_CREATED);
        newEventDefinition.setMetaDisplayName("Filter Created");
        newEventDefinition.setMetaDescription("This event occurs when the user presses the filter button and the filter object is created.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILTER, "Filter", "The T8DataFilter that was created by this filter panel", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        operations = new ArrayList<T8ComponentOperationDefinition>(T8PanelAPIHandler.getOperationDefinitions());

        T8ComponentOperationDefinition newOperationDefinition;

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CREATE_FILTER);
        newOperationDefinition.setMetaDisplayName("Create Filter");
        newOperationDefinition.setMetaDescription("Ask this filter panel to create a new T8Filter with the values of all the selected child components.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILTER, "Filter", "The Filter that was created.", T8DataType.CUSTOM_OBJECT));

        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH);
        newOperationDefinition.setMetaDisplayName("Refresh");
        newOperationDefinition.setMetaDescription("Refeshes all of the components ons the panel, this only affects data source based components.");

        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PRE_FILTER);
        newOperationDefinition.setMetaDisplayName("Set Pre-Filter");
        newOperationDefinition.setMetaDescription("The Pre-Filter that will be used by this filter panel.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILTER, "Filter", "The Data Filter.", T8DataType.CUSTOM_OBJECT));

        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_PRE_FILTER_CRITERIA);
        newOperationDefinition.setMetaDisplayName("Add to Pre-Filter Criteria");
        newOperationDefinition.setMetaDescription("Filter Criteria that will be added to the pre-filter.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILTER_CRITERIA, "Criteria", "The T8 Data Filter Criteria to add.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILTER, "FIlter", "The T8 Data Filter to add to criteria.", T8DataType.CUSTOM_OBJECT));

        operations.add(newOperationDefinition);

        return operations;
    }
}
