package com.pilog.t8.ui.datarecord;

import com.pilog.t8.data.document.datarecord.RecordValue;

/**
 * @author Bouwer du Preez
 */
public interface DataRecordEditorFactory
{
    public DataRecordEditor createDataRecordEditorComponent(RecordValue recordValue);
}
