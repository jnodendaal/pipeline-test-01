package com.pilog.t8.definition.ui.cellrenderer.table.label.formats;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererFormatDefinition;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class NumberFormatDefinition extends T8LabelTableCellRendererFormatDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_LABEL_FORMATER_NUMBER";
    public static final String DISPLAY_NAME = "Number Formatter";
    public static final String DESCRIPTION = "Formatter that will use a number pattern to format a number";
    public enum Datum
    {
        FORMAT_INSTANCE

    };
    // -------- Definition Meta-Data -------- //

    public enum FormatInstance
    {
        DECIMAL_FORMAT_INSTANCE,
        INTEGER_FORMAT_INSTANCE,
        CURRENCY_FORMAT_INSTANCE,
        PERCENTAGE_INSTANCE
    }

    public NumberFormatDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public Format getFormat()
    {
        NumberFormat numberFormat;
        FormatInstance formatInstance;

        if(getFormatInstance() == null) throw new RuntimeException("Invalid format instance specified for " + getPublicIdentifier());

        formatInstance = FormatInstance.valueOf(getFormatInstance());
        switch(formatInstance)
        {
            case CURRENCY_FORMAT_INSTANCE:
            {
                numberFormat = NumberFormat.getCurrencyInstance();
                break;
            }
            case DECIMAL_FORMAT_INSTANCE:
            {
                numberFormat = new DecimalFormat(getFormatPattern());
                break;
            }
            case INTEGER_FORMAT_INSTANCE:
            {
                numberFormat = NumberFormat.getIntegerInstance();
                break;
            }
            case PERCENTAGE_INSTANCE:
            {
                numberFormat = NumberFormat.getPercentInstance();
                break;
            }
            default:
            {
                numberFormat = NumberFormat.getInstance();
                break;
            }
        }
        return numberFormat;
    }

     @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FORMAT_INSTANCE.toString(), "Format Instance", "The format instance that will be used for this number formatter.", FormatInstance.DECIMAL_FORMAT_INSTANCE.toString(), T8DefinitionDatumOptionType.ENUMERATION));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FORMAT_INSTANCE.toString().equals(datumIdentifier)) return createStringOptions(FormatInstance.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public String getFormatInstance()
    {
        return (String)getDefinitionDatum(Datum.FORMAT_INSTANCE.toString());
    }

    public void setFormatInstance(String formatPattern)
    {
        setDefinitionDatum(Datum.FORMAT_INSTANCE.toString(), formatPattern);
    }
}
