package com.pilog.t8.definition.ui.panel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8PanelAPIHandler
{
    // These should NOT be changed, only added to.

    public static final String OPERATION_SET_HEADER_TEXT = "$CO_SET_HEADER_TEXT";
    public static final String OPERATION_SET_PANEL_SLOT_HEADER_TEXT = "$CO_SET_PANEL_SLOT_HEADER_TEXT";
    public static final String OPERATION_SET_BACKGROUND_PAINTER = "$CO_SET_BACKGROUND_PAINTER";

    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_PANEL_SLOT_IDENTIFIER = "$CP_PANEL_SLOT_IDENTIFIER";
    public static final String PARAMETER_BACKGROUND_PAINTER_IDENTIFIER = "$CP_BACKGROUND_PAINTER_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_HEADER_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Header Text");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The new text to set for the header.", T8DataType.DISPLAY_STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PANEL_SLOT_HEADER_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Panel Slot Header Text");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PANEL_SLOT_IDENTIFIER, "Panel Slot Identifier", "The identifier of the panel slot for which to update the header text.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The new text to set for the header.", T8DataType.DISPLAY_STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_BACKGROUND_PAINTER);
        newOperationDefinition.setMetaDisplayName("Set Background Painter");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BACKGROUND_PAINTER_IDENTIFIER, "Painter Identifier", "The identifier of the painter to set the background to.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
