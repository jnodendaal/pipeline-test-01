package com.pilog.t8.definition.ui.modulecontainer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ModuleOperationScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceOperationDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleInterfaceOperationMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_INTERFACE_OPERATION_MAPPING";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MODULE_INTERFACE_OPERATION_MAPPING";
    public static final String DISPLAY_NAME = "Module Operation Mapping";
    public static final String DESCRIPTION = "A mapping of a module event to the container interface.";
    public static final String IDENTIFIER_PREFIX = "CO_MAPPING_";
    public enum Datum {INTERFACE_OPERATION_IDENTIFIER,
                       MODULE_OPERATION_IDENTIFIER,
                       INPUT_PARAMETER_MAPPING,
                       OUTPUT_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ModuleInterfaceOperationMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INTERFACE_OPERATION_IDENTIFIER.toString(), "Interface Operation Identifier", "The identifier of the interface operation to which the module operation will be mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MODULE_OPERATION_IDENTIFIER.toString(), "Module Operation Identifier", "The identifier of the module operation to which the interface operation will be mapped."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Interface Operation Input to Module Operation Input", "A mapping of Interface Operation Input Parameters to the corresponding Module Operation Input parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.OUTPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Module Operation Output to Interface Operation Output", "A mapping of Module Operation Output parameters to the corresponding Interface Operation Output Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INTERFACE_OPERATION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8ModuleContainerDefinition containerDefinition;
            T8ComponentInterfaceDefinition interfaceDefinition;

            containerDefinition = (T8ModuleContainerDefinition)getAncestorDefinition(T8ModuleContainerDefinition.TYPE_IDENTIFIER);
            interfaceDefinition = containerDefinition.getInterfaceDefinition();
            if (interfaceDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions((List)interfaceDefinition.getOperationDefinitions());
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.MODULE_OPERATION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8ModuleInterfaceMappingDefinition moduleMappingDefinition;
            String moduleId;

            moduleMappingDefinition = (T8ModuleInterfaceMappingDefinition)getParentDefinition();
            moduleId = moduleMappingDefinition.getModuleIdentifier();
            if (moduleId != null)
            {
                T8ModuleDefinition moduleDefinition;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                if (moduleDefinition != null)
                {
                    return createPublicIdentifierOptionsFromDefinitions((List)moduleDefinition.getModuleOperationScriptDefinitions());
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8ModuleContainerDefinition containerDefinition;
            T8ComponentInterfaceDefinition interfaceDefinition;
            T8ModuleInterfaceMappingDefinition moduleMappingDefinition;
            String moduleOperationIdentifier;
            String interfaceOperationIdentifier;
            String moduleId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            containerDefinition = (T8ModuleContainerDefinition)getAncestorDefinition(T8ModuleContainerDefinition.TYPE_IDENTIFIER);
            moduleMappingDefinition = (T8ModuleInterfaceMappingDefinition)getParentDefinition();
            moduleId = moduleMappingDefinition.getModuleIdentifier();
            interfaceOperationIdentifier = getInterfaceOperationIdentifier();
            moduleOperationIdentifier = getModuleOperationIdentifier();
            interfaceDefinition = containerDefinition.getInterfaceDefinition();

            if ((moduleId != null) && (moduleOperationIdentifier != null))
            {
                T8ModuleDefinition moduleDefinition;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                if ((interfaceDefinition != null) && (moduleDefinition != null))
                {
                    List<T8DataParameterDefinition> moduleInputParameterDefinitions;
                    List<T8DataParameterDefinition> interfaceInputParameterDefinitions;
                    T8ComponentInterfaceOperationDefinition interfaceOperationDefinition;
                    T8ModuleOperationScriptDefinition moduleOperationDefinition;
                    HashMap<String, List<String>> identifierMap;

                    moduleOperationDefinition = moduleDefinition.getModuleOperationScriptDefinition(moduleOperationIdentifier);
                    interfaceOperationDefinition = interfaceDefinition.getOperationDefinition(interfaceOperationIdentifier);
                    moduleInputParameterDefinitions = moduleOperationDefinition.getInputParameterDefinitions();
                    interfaceInputParameterDefinitions = interfaceOperationDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((moduleInputParameterDefinitions != null) && (interfaceInputParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition interfaceInputParameterDefinition : interfaceInputParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition moduleInputParameterDefinition : moduleInputParameterDefinitions)
                            {
                                identifierList.add(moduleInputParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(interfaceInputParameterDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.OUTPUT_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8ModuleContainerDefinition containerDefinition;
            T8ComponentInterfaceDefinition interfaceDefinition;
            T8ModuleInterfaceMappingDefinition moduleMappingDefinition;
            String moduleOperationIdentifier;
            String interfaceOperationIdentifier;
            String moduleId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            containerDefinition = (T8ModuleContainerDefinition)getAncestorDefinition(T8ModuleContainerDefinition.TYPE_IDENTIFIER);
            moduleMappingDefinition = (T8ModuleInterfaceMappingDefinition)getParentDefinition();
            moduleId = moduleMappingDefinition.getModuleIdentifier();
            interfaceOperationIdentifier = getInterfaceOperationIdentifier();
            moduleOperationIdentifier = getModuleOperationIdentifier();
            interfaceDefinition = containerDefinition.getInterfaceDefinition();

            if ((moduleId != null) && (moduleOperationIdentifier != null))
            {
                T8ModuleDefinition moduleDefinition;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                if ((interfaceDefinition != null) && (moduleDefinition != null))
                {
                    List<T8DataParameterDefinition> moduleOutputParameterDefinitions;
                    List<T8DataParameterDefinition> interfaceOutputParameterDefinitions;
                    T8ComponentInterfaceOperationDefinition interfaceOperationDefinition;
                    T8ModuleOperationScriptDefinition moduleOperationDefinition;
                    HashMap<String, List<String>> identifierMap;

                    moduleOperationDefinition = moduleDefinition.getModuleOperationScriptDefinition(moduleOperationIdentifier);
                    interfaceOperationDefinition = interfaceDefinition.getOperationDefinition(interfaceOperationIdentifier);
                    moduleOutputParameterDefinitions = moduleOperationDefinition.getOutputParameterDefinitions();
                    interfaceOutputParameterDefinitions = interfaceOperationDefinition.getOutputParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((moduleOutputParameterDefinitions != null) && (interfaceOutputParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition moduleInputParameterDefinition : moduleOutputParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition interfaceInputParameterDefinition : interfaceOutputParameterDefinitions)
                            {
                                identifierList.add(interfaceInputParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(moduleInputParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getInterfaceOperationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INTERFACE_OPERATION_IDENTIFIER.toString());
    }

    public void setInterfaceEventIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INTERFACE_OPERATION_IDENTIFIER.toString(), identifier);
    }

    public String getModuleOperationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_OPERATION_IDENTIFIER.toString());
    }

    public void setModuleOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MODULE_OPERATION_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString());
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING.toString(), mapping);
    }

    public Map<String, String> getOutputParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString());
    }

    public void setOutputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_MAPPING.toString(), mapping);
    }
}
