package com.pilog.t8.definition.ui.labelmatrix;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8LabelMatrixFormattingDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    private enum Datum {HORIZONTAL_TEXT_ALIGNMENT};
    // -------- Definition Meta-Data -------- //

    public enum HorizontalTextAlignment {LEADING, CENTER, TRAILING};
    
    public T8LabelMatrixFormattingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HORIZONTAL_TEXT_ALIGNMENT.toString(), "Horizontal Text Alignment", "The horizontal alignment of all text in the column.", HorizontalTextAlignment.CENTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.HORIZONTAL_TEXT_ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(HorizontalTextAlignment.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public HorizontalTextAlignment getHorizontalTextAlignment()
    {
        return HorizontalTextAlignment.valueOf((String)getDefinitionDatum(Datum.HORIZONTAL_TEXT_ALIGNMENT.toString()));
    }
    
    public void setHorizontalTextAlignment(HorizontalTextAlignment alignment)
    {
        setDefinitionDatum(Datum.HORIZONTAL_TEXT_ALIGNMENT.toString(), alignment.toString());
    }    
}
