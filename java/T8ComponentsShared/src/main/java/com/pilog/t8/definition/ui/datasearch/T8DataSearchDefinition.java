package com.pilog.t8.definition.ui.datasearch;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8DataSearchDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_SEARCH";
    public static final String DISPLAY_NAME = "Data Search";
    public static final String DESCRIPTION = "A UI component that allows for the setup of a Data Filter object that can be used to filter the data entities used by other components.";
    public static final String IDENTIFIER_PREFIX = "C_SEARCH_";
    private enum Datum {SEARCH_TYPE,
                        SEARCH_PROMPT,
                        SEARCH_INFORMATION,
                        SEARCH_TYPE_SWITCH_ENABLED,
                        FULL_TEXT_ENABLED,
                        WHITESPACE_CONJUNCTION,
                        DATA_ENTITY_IDENTIFIER,
                        SEARCH_FIELD_IDENTIFIERS};
    // -------- Definition Meta-Data -------- //

    public enum T8SearchType
    {
        QUICK("Quick"),
        EXPRESSION("Expression");

        private final String displayName;

        T8SearchType(String displayName)
        {
            this.displayName = displayName;
        }

        public String getDisplayName()
        {
            return this.displayName;
        }
    };

    public T8DataSearchDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEARCH_TYPE.toString(), "Search Type", "The type of quick search strategy to employ.", T8SearchType.EXPRESSION.name(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.WHITESPACE_CONJUNCTION.toString(), "Default Conjunction", "The default filter conjunction to use when creating a filter from search terms.", DataFilterConjunction.OR.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SEARCH_TYPE_SWITCH_ENABLED.toString(), "Enable Search Type Switch", "If set to true, the user will be able to switch between different search types that are available.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FULL_TEXT_ENABLED.toString(), "Enable Full-Text Support", "If set to true, the full-text search features will be used to improve search performance.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.SEARCH_PROMPT.toString(), "Quick Search Prompt", "The prompt to display in the quick search text field."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.SEARCH_INFORMATION.toString(), "Quick Search Information", "The information to display om the quick search text field tooltip."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity on which the search will be performed.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.SEARCH_FIELD_IDENTIFIERS.toString())));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.SEARCH_FIELD_IDENTIFIERS.toString(), "Search Fields", "The entity fields on which the search will be performed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SEARCH_TYPE.toString().equals(datumIdentifier)) return createStringOptions(T8SearchType.values());
        else if (Datum.WHITESPACE_CONJUNCTION.toString().equals(datumIdentifier)) return createStringOptions(DataFilterConjunction.values());
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.SEARCH_FIELD_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();
                    if (fieldDefinitions != null)
                    {
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            optionList.add(new T8DefinitionDatumOption(fieldDefinition.getPublicIdentifier(), fieldDefinition.getPublicIdentifier()));
                        }
                    }

                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datasearch.T8DataSearch", new Class<?>[]{T8DataSearchDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataSearchAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataSearchAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public boolean isFullTextEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.FULL_TEXT_ENABLED);
        return value != null && value;
    }

    public void setFullTextEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.FULL_TEXT_ENABLED, enabled);
    }

    public boolean isSearchTypeSwitchEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.SEARCH_TYPE_SWITCH_ENABLED);
        return value != null && value;
    }

    public void setSearchTypeSwitchEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.SEARCH_TYPE_SWITCH_ENABLED, enabled);
    }

    public DataFilterConjunction getWhitespaceConjunction()
    {
        String value;

        value = getDefinitionDatum(Datum.WHITESPACE_CONJUNCTION);
        return value != null ? DataFilterConjunction.valueOf(value) : DataFilterConjunction.OR;
    }

    public void setWhitespaceConjunction(DataFilterConjunction conjunction)
    {
        setDefinitionDatum(Datum.WHITESPACE_CONJUNCTION, conjunction.toString());
    }

    public T8SearchType getSearchType()
    {
        String value;

        value = getDefinitionDatum(Datum.SEARCH_TYPE);
        return value != null ? T8SearchType.valueOf(value) : T8SearchType.EXPRESSION;
    }

    public void setSearchType(T8SearchType type)
    {
        setDefinitionDatum(Datum.SEARCH_TYPE, type.toString());
    }

    public String getQuickSearchPrompt()
    {
        return (String)getDefinitionDatum(Datum.SEARCH_PROMPT.toString());
    }

    public void setQuickSearchPrompt(String label)
    {
        setDefinitionDatum(Datum.SEARCH_PROMPT.toString(), label);
    }

    public String getQuickSearchInformation()
    {
        return (String)getDefinitionDatum(Datum.SEARCH_INFORMATION.toString());
    }

    public void setQuickSearchInformation(String information)
    {
        setDefinitionDatum(Datum.SEARCH_INFORMATION.toString(), information);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public List<String> getSearchFieldIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.SEARCH_FIELD_IDENTIFIERS.toString());
    }

    public void setSearchFieldIdentifiers(List<String> fieldIdentifiers)
    {
        setDefinitionDatum(Datum.SEARCH_FIELD_IDENTIFIERS.toString(), fieldIdentifiers);
    }
}
