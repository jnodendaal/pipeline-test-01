package com.pilog.t8.definition.ui.tabbedpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TabbedPaneDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TABBED_PANE";
    public static final String DISPLAY_NAME = "Tabbed Pane";
    public static final String DESCRIPTION = "A container to which tab components can be added.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_TABBED_PANE_";
    public enum Datum {TAB_DEFINITIONS,
                       ANIMATED};
    // -------- Definition Meta-Data -------- //

    public T8TabbedPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TAB_DEFINITIONS.toString(), "Tabs", "The tabs displayed by the tabbed pane."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ANIMATED.toString(), "Animated", "If this option is set to true then tabs will be animated when the user changes between them.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TAB_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TabbedPaneTabDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public ArrayList<T8TabbedPaneTabDefinition> getTabDefinitions()
    {
        return (ArrayList<T8TabbedPaneTabDefinition>)getDefinitionDatum(Datum.TAB_DEFINITIONS.toString());
    }

    public void setTabDefinitions(ArrayList<T8TabbedPaneTabDefinition> tabDefinitions)
    {
        getTabDefinitions().clear();
        getTabDefinitions().addAll(tabDefinitions);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            return (T8Component)Class.forName("com.pilog.t8.ui.tabbedpane.T8TabbedPane").getConstructor(T8TabbedPaneDefinition.class, T8ComponentController.class).newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TabbedPaneAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TabbedPaneAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;

        childComponentDefinitions = new ArrayList<T8ComponentDefinition>();
        childComponentDefinitions.addAll(getTabDefinitions());
        return childComponentDefinitions;
    }

    public Boolean isAnimated()
    {
        return (Boolean)getDefinitionDatum(Datum.ANIMATED.toString());
    }

    public void setAnimated(Boolean animated)
    {
        setDefinitionDatum(Datum.ANIMATED.toString(), animated);
    }
}
