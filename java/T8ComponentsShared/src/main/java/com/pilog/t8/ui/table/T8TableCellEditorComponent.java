package com.pilog.t8.ui.table;

import com.pilog.t8.ui.T8Component;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8TableCellEditorComponent extends T8Component
{
    public enum EditParameter {SELECTED, FOCUSED, ROW, COLUMN, LEAF, EXPANDED};
    
    public boolean singleClickEdit();
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorKey, Map<EditParameter, Object> editParameters);
    public Map<String, Object> getEditorData();
}
