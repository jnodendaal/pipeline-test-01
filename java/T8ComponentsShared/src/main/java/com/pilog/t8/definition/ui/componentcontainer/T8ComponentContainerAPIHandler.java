package com.pilog.t8.definition.ui.componentcontainer;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8ComponentContainerAPIHandler and responds to specific
 * requests regarding these definitions.  It is extremely important that only
 * new definitions are added to this class and none of the old definitions
 * removed or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ComponentContainerAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_UNLOAD_COMPONENT = "$CO_UNLOAD_COMPONENT";
    public static final String OPERATION_RELOAD_COMPONENT = "$CO_RELOAD_COMPONENT";
    public static final String OPERATION_LOAD_COMPONENT = "$CO_LOAD_COMPONENT";
    public static final String PARAMETER_COMPONENT_IDENTIFIER = "$P_COMPONENT_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_RELOAD_COMPONENT);
        newOperationDefinition.setMetaDisplayName("Reload Component");
        newOperationDefinition.setMetaDescription("This operations reloads the currently loaded component.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_UNLOAD_COMPONENT);
        newOperationDefinition.setMetaDisplayName("Unload Component");
        newOperationDefinition.setMetaDescription("This operations unloads the currently loaded component (if any is loaded) and clears are used by the module.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_COMPONENT);
        newOperationDefinition.setMetaDisplayName("Load Component");
        newOperationDefinition.setMetaDescription("This operations loads the specified component into the container.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COMPONENT_IDENTIFIER, "Component Identifier", "The identifier of the component to load.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
