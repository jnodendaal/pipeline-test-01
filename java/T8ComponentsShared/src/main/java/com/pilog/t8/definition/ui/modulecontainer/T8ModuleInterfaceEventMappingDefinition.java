package com.pilog.t8.definition.ui.modulecontainer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceEventDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleEventDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleInterfaceEventMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_INTERFACE_EVENT_MAPPING";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MODULE_INTERFACE_EVENT_MAPPING";
    public static final String DISPLAY_NAME = "Module Event Mapping";
    public static final String DESCRIPTION = "A mapping of a module event to the container interface.";
    public static final String IDENTIFIER_PREFIX = "CE_MAPPING_";
    public enum Datum {INTERFACE_EVENT_IDENTIFIER,
                       MODULE_EVENT_IDENTIFIER,
                       PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ModuleInterfaceEventMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INTERFACE_EVENT_IDENTIFIER.toString(), "Interface Event Identifier", "The identifier of the interface event to which the module event will be mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MODULE_EVENT_IDENTIFIER.toString(), "Module Event Identifier", "The identifier of the module event to which the interface event will be mapped."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.PARAMETER_MAPPING.toString(), "Parameter Mapping:  Module Event to Interface Event", "A mapping of Module Event Parameters to the corresponding Interface Event Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INTERFACE_EVENT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8ModuleContainerDefinition containerDefinition;
            T8ComponentInterfaceDefinition interfaceDefinition;

            containerDefinition = (T8ModuleContainerDefinition)getAncestorDefinition(T8ModuleContainerDefinition.TYPE_IDENTIFIER);
            interfaceDefinition = containerDefinition.getInterfaceDefinition();
            if (interfaceDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions((List)interfaceDefinition.getEventDefinitions());
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.MODULE_EVENT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8ModuleInterfaceMappingDefinition moduleMappingDefinition;
            String moduleId;

            moduleMappingDefinition = (T8ModuleInterfaceMappingDefinition)getParentDefinition();
            moduleId = moduleMappingDefinition.getModuleIdentifier();
            if (moduleId != null)
            {
                T8ModuleDefinition moduleDefinition;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                if (moduleDefinition != null)
                {
                    return createPublicIdentifierOptionsFromDefinitions((List)moduleDefinition.getModuleEventDefinitions());
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8ModuleContainerDefinition containerDefinition;
            T8ComponentInterfaceDefinition interfaceDefinition;
            T8ModuleInterfaceMappingDefinition moduleMappingDefinition;
            String moduleEventIdentifier;
            String interfaceEventIdentifier;
            String moduleId;

            optionList = new ArrayList<T8DefinitionDatumOption>();
            containerDefinition = (T8ModuleContainerDefinition)getAncestorDefinition(T8ModuleContainerDefinition.TYPE_IDENTIFIER);
            moduleMappingDefinition = (T8ModuleInterfaceMappingDefinition)getParentDefinition();
            moduleId = moduleMappingDefinition.getModuleIdentifier();
            interfaceEventIdentifier = getInterfaceEventIdentifier();
            moduleEventIdentifier = getModuleEventIdentifier();
            interfaceDefinition = containerDefinition.getInterfaceDefinition();

            if ((moduleId != null) && (moduleEventIdentifier != null))
            {
                T8ModuleDefinition moduleDefinition;

                moduleDefinition = (T8ModuleDefinition)definitionContext.getRawDefinition(getRootProjectId(), moduleId);
                if ((interfaceDefinition != null) && (moduleDefinition != null))
                {
                    List<T8DataParameterDefinition> moduleEventParameterDefinitions;
                    List<T8DataParameterDefinition> interfaceEventParameterDefinitions;
                    T8ComponentInterfaceEventDefinition interfaceEventDefinition;
                    T8ModuleEventDefinition moduleEventDefinition;
                    HashMap<String, List<String>> identifierMap;

                    moduleEventDefinition = moduleDefinition.getModuleEventDefinition(moduleEventIdentifier);
                    interfaceEventDefinition = interfaceDefinition.getEventDefinition(interfaceEventIdentifier);
                    moduleEventParameterDefinitions = moduleEventDefinition.getParameterDefinitions();
                    interfaceEventParameterDefinitions = interfaceEventDefinition.getParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((moduleEventParameterDefinitions != null) && (interfaceEventParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition moduleEventParameterDefinition : moduleEventParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition interfaceEventParameterDefinition : interfaceEventParameterDefinitions)
                            {
                                identifierList.add(interfaceEventParameterDefinition.getIdentifier());
                            }

                            identifierMap.put(moduleEventParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getInterfaceEventIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INTERFACE_EVENT_IDENTIFIER.toString());
    }

    public void setInterfaceEventIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INTERFACE_EVENT_IDENTIFIER.toString(), identifier);
    }

    public String getModuleEventIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_EVENT_IDENTIFIER.toString());
    }

    public void setModuleEventIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.MODULE_EVENT_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getParameterMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.PARAMETER_MAPPING.toString());
    }

    public void setParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.PARAMETER_MAPPING.toString(), mapping);
    }
}
