package com.pilog.t8.definition.ui.entityeditor;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;

/**
 * @author Bouwer du Preez
 */
public interface T8DataEntityEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String STORAGE_PATH = "/data_entity_editors"; 
    // -------- Definition Meta-Data -------- //
    
    public String getEditorDataEntityIdentifier();
    public void setEditorDataEntityIdentifier(String identifier);
    public String getParentEditorIdentifier();
    public void setParentEditorIdentifier(String identifier);
    public T8DataEntityEditorComponent getNewComponentInstance(T8ComponentController controller) throws Exception;
}
