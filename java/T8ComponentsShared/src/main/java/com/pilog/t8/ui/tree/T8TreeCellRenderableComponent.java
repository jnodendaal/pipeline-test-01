package com.pilog.t8.ui.tree;

import com.pilog.t8.ui.T8CellRenderableComponent;

/**
 * @author Bouwer du Preez
 */
public interface T8TreeCellRenderableComponent extends T8CellRenderableComponent
{
}
