package com.pilog.t8.definition.ui.labelmatrix;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LabelMatrixRowDefinition extends T8LabelMatrixFormattingDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LABEL_MATRIX_ROW";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_LABEL_MATRIX_ROW";
    public static final String IDENTIFIER_PREFIX = "C_LABEL_MATRIX_ROW_";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Label Matrix Row";
    public static final String DESCRIPTION = "A definition of the formatting of a specific row in the matrix.";
    private enum Datum {ROW_INDEX,
                        WEIGHT};
    // -------- Definition Meta-Data -------- //

    public T8LabelMatrixRowDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROW_INDEX.toString(), "Row Index", "The row index of the cell.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.WEIGHT.toString(), "Weight", "The weight of the row relative to the others in the matrix.", 0.0));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public double getWeight()
    {
        Double weight;
        
        weight = (Double)getDefinitionDatum(Datum.WEIGHT.toString());
        return weight != null ? weight : 0.0;
    }

    public void setWeight(double weight)
    {
        setDefinitionDatum(Datum.WEIGHT.toString(), weight);
    }   
   
    public int getRowIndex()
    {
        Integer rowIndex;
        
        rowIndex = (Integer)getDefinitionDatum(Datum.ROW_INDEX.toString());
        return rowIndex != null ? rowIndex : 0;
    }
    
    public void setRowIndex(int index)
    {
        setDefinitionDatum(Datum.ROW_INDEX.toString(), index);
    } 
}
