package com.pilog.t8.definition.ui.functionality.buttonpane;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8FunctionalityMenu and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8FunctionalityButtonPaneAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_MENU_ITEM_SELECTED = "$CE_MENU_ITEM_SELECTED";
    public static final String OPERATION_DISABLE_MENU_ITEM = "$CO_DISABLE_MENU_ITEM";
    public static final String OPERATION_SET_DATA_OBJECT = "$CO_SET_DATA_OBJECT";
    public static final String PARAMETER_MENU_ITEM_IDENTIFIER = "$P_MENU_ITEM_IDENTIFIER";
    public static final String PARAMETER_FUNCTIONALITY_IDENTIFIER = "$P_FUNCTIONALITY_IDENTIFIER";
    public static final String PARAMETER_DATA_OBJECT = "$P_DATA_OBJECT";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_MENU_ITEM_SELECTED);
        newEventDefinition.setMetaDisplayName("Menu Item Selected");
        newEventDefinition.setMetaDescription("This event occurs when a menu item is selected.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MENU_ITEM_IDENTIFIER, "Menu Item Identifier", "The identifier of the menu item that was selected.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FUNCTIONALITY_IDENTIFIER, "Functionality Identifier", "The identifier of the functionality that was selected (if any).", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_MENU_ITEM);
        newOperationDefinition.setMetaDisplayName("Disable Menu Item");
        newOperationDefinition.setMetaDescription("This operation disables a specified menu item.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FUNCTIONALITY_IDENTIFIER, "Menu Item Identifier", "The identifier of the menu item to disable.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_OBJECT);
        newOperationDefinition.setMetaDisplayName("Set Data Object");
        newOperationDefinition.setMetaDescription("This operation sets the target data object of the menu bar, which will cause a refresh.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_OBJECT, "Data Object", "The data object to set.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        return operations;
    }
}
