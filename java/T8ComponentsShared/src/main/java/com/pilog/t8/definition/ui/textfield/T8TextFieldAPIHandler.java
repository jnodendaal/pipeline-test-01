package com.pilog.t8.definition.ui.textfield;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8TextField and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8TextFieldAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_TEXT_CHANGED = "$CE_TEXT_CHANGED";
    public static final String EVENT_FOCUS_GAINED = "$CE_FOCUS_GAINED";
    public static final String EVENT_FOCUS_LOST = "$CE_FOCUS_LOST";

    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_SET_VALUE = "$CO_SET_VALUE";
    public static final String OPERATION_SET_REGEX_PATTERN = "$CO_SET_REGEX_PATTERN";
    public static final String OPERATION_SET_EDITABLE = "$CO_SET_EDITABLE";
    public static final String OPERATION_GET_TEXT = "$CO_GET_TEXT";
    public static final String OPERATION_GET_VALUE = "$CO_GET_VALUE";
    public static final String OPERATION_IS_VALID_CONTENT = "$CO_IS_VALID_CONTENT";

    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_VALUE = "$CP_VALUE";
    public static final String PARAMETER_EDITABLE = "$CP_EDITABLE";
    public static final String PARAMETER_VALID = "$CP_VALID";
    public static final String PARAMETER_REGEX_PATTERN = "$CP_REGEX_PATTERN";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TEXT_CHANGED);
        newEventDefinition.setMetaDisplayName("Text Changed");
        newEventDefinition.setMetaDescription("This event occurs every time the text displayed in the text field is changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The new text", T8DataType.STRING));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FOCUS_GAINED);
        newEventDefinition.setMetaDisplayName("Focus Gained");
        newEventDefinition.setMetaDescription("This event occurs every time the text field gains focus.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text as it is when focus was gained", T8DataType.STRING));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The Value as it is when focus was gained", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FOCUS_LOST);
        newEventDefinition.setMetaDisplayName("Focus Lost");
        newEventDefinition.setMetaDescription("This event occurs every time the text field loses focus.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text as it is when focus was lost", T8DataType.STRING));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The Value as it is when focus was gained", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed in the text field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text value to display in the text field.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_VALUE);
        newOperationDefinition.setMetaDisplayName("Set Value");
        newOperationDefinition.setMetaDescription("This operations sets the value in the text field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The value to display in the text field.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_TEXT);
        newOperationDefinition.setMetaDisplayName("Get Text");
        newOperationDefinition.setMetaDescription("This operations returns the text displayed in the text field.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text value displayed in the text field.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_VALUE);
        newOperationDefinition.setMetaDisplayName("Get Value");
        newOperationDefinition.setMetaDescription("This operations returns the value of the format.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The value of the format object.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_EDITABLE);
        newOperationDefinition.setMetaDisplayName("Sets component Editable");
        newOperationDefinition.setMetaDescription("This operation sets the components editable status.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_EDITABLE, "Editable", "If this text component should be editable.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_REGEX_PATTERN);
        newOperationDefinition.setMetaDisplayName("Sets a Regular Expression");
        newOperationDefinition.setMetaDescription("This operation sets a regular expression to validate the content of the field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REGEX_PATTERN, "Regular Expression", "The regular expression to set.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_IS_VALID_CONTENT);
        newOperationDefinition.setMetaDisplayName("Checks for Valid Value");
        newOperationDefinition.setMetaDescription("This operation checks if the value is valid based on the regular expression applied. Always true if none applied.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALID, "Is Valid", "Whether or not the value is valid.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
