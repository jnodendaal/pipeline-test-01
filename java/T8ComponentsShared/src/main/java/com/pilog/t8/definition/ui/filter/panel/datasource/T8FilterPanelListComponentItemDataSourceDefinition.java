package com.pilog.t8.definition.ui.filter.panel.datasource;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import java.util.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8FilterPanelListComponentItemDataSourceDefinition extends T8Definition
{
// -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FILTER_PANEL_COMPONENT_DATASOURCE";
    public static final String DISPLAY_NAME = "Filter Panel Component Data Source";
    public static final String IDENTIFIER_PREFIX = "FP_DS_";
    public static final String VERSION = "0";

    public enum Datum
    {
        ADD_NULL_VALUE,
        NULL_VALUE_DISPLAY_NAME,
        NULL_VALUE_ICON_IDENTIFIER
    };
// -------- Definition Meta-Data -------- //
    private T8IconDefinition iconDefinition;

    public T8FilterPanelListComponentItemDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract T8FilterPanelListComponentItemDataSource getInstance();

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getNullValueIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = new ArrayList<T8DefinitionDatumType>(2);
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ADD_NULL_VALUE.toString(), "Add Null Value", "If a Null Value Item should be added to the list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.NULL_VALUE_DISPLAY_NAME.toString(), "Null Value Display Name", "The Display Name of the Null Value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NULL_VALUE_ICON_IDENTIFIER.toString(), "Null Value Icon", "The icon to render next to this button's text."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            T8DefinitionContext definitionContext, String datumIdentifier)
            throws Exception
    {
        if (Datum.NULL_VALUE_ICON_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        }
        else
        {
            return null;
        }
    }

    public void setAddNullValue(Boolean addNullValue)
    {
        setDefinitionDatum(Datum.ADD_NULL_VALUE.toString(), addNullValue);
    }

    public Boolean isAddNullValueOn()
    {
        return (Boolean) getDefinitionDatum(Datum.ADD_NULL_VALUE.toString());
    }

    public void setNullValueDisplayName(String nullValueDisplayName)
    {
        setDefinitionDatum(Datum.NULL_VALUE_DISPLAY_NAME.toString(), nullValueDisplayName);
    }

    public String getNullValueDisplayName()
    {
        return (String) getDefinitionDatum(Datum.NULL_VALUE_DISPLAY_NAME.toString());
    }

    public String getNullValueIconIdentifier()
    {
        return (String) getDefinitionDatum(Datum.NULL_VALUE_ICON_IDENTIFIER.toString());
    }

    public void setNullValueIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.NULL_VALUE_ICON_IDENTIFIER.toString(), identifier);
    }

    public T8IconDefinition getNullValueIconDefinition()
    {
        return iconDefinition;
    }
}
