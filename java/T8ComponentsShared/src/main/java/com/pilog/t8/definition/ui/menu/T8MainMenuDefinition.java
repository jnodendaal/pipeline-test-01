package com.pilog.t8.definition.ui.menu;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public class T8MainMenuDefinition extends T8MenuDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MENU_MAIN";
    public static final String DISPLAY_NAME = "Main Menu";
    public static final String DESCRIPTION = "A menu component to which menu items can be added.  All menu items are then displayed as a vertical list.";
    public static final String IDENTIFIER_PREFIX = "C_MENU_";
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8MainMenuDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.menu.T8MainMenu", new Class<?>[]{T8MainMenuDefinition.class, T8ComponentController.class}, this, controller);
    }
}
