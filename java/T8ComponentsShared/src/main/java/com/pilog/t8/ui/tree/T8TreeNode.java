package com.pilog.t8.ui.tree;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.tree.T8TreeNodeDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * @author Bouwer du Preez
 */
public class T8TreeNode extends DefaultMutableTreeNode
{
    private String identifier;
    private String groupDisplayName;
    private Map<String, T8DataFilter> userDefinedChildNodeFilters;
    private boolean childrenLoaded;
    private NodeType nodeType;
    private int childNodePageSize;
    private int childNodePageOffset;
    private int childNodeCount;

    public enum NodeType {GROUP, DATA};

    public T8TreeNode(T8TreeNodeDefinition definition, T8DataEntity entity)
    {
        this.identifier = definition.getIdentifier();
        this.childrenLoaded = false;
        this.childNodePageOffset = 0;
        this.childNodePageSize = definition.getPageSize();
        this.childNodeCount = 0;
        setUserObject(entity);
        nodeType = NodeType.DATA;
    }

    public T8TreeNode(String groupDisplayName, int pageSize)
    {
        this.groupDisplayName = groupDisplayName;
        this.childrenLoaded = false;
        this.childNodePageOffset = 0;
        this.childNodePageSize = pageSize;
        this.childNodeCount = 0;
        this.nodeType = NodeType.GROUP;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public String getGroupDisplayName()
    {
        return groupDisplayName;
    }
    
    public T8DataEntity getDataEntity()
    {
        return (T8DataEntity)getUserObject();
    }

    public void setDataEntity(T8DataEntity entity)
    {
        setUserObject(entity);
    }
    
    public List<T8TreeNode> getChildNodes()
    {
        List<T8TreeNode> childNodes;
        
        childNodes = new ArrayList<T8TreeNode>();
        for (int childIndex = 0; childIndex < this.getChildCount(); childIndex++)
        {
            childNodes.add((T8TreeNode)getChildAt(childIndex));
        }
        
        return childNodes;
    }
    
    public List<T8TreeNode> getDescendantNodes()
    {
        List<T8TreeNode> descendants;
        
        descendants = new ArrayList<T8TreeNode>();
        for (int childIndex = 0; childIndex < this.getChildCount(); childIndex++)
        {
            T8TreeNode childNode;
            
            childNode = (T8TreeNode)getChildAt(childIndex);
            descendants.add(childNode);
            descendants.addAll(childNode.getDescendantNodes());
        }
        
        return descendants;
    }

    public boolean areChildrenLoaded()
    {
        return childrenLoaded;
    }

    public void setChildrenLoaded(boolean childrenLoaded)
    {
        this.childrenLoaded = childrenLoaded;
    }

    public NodeType getNodeType()
    {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType)
    {
        this.nodeType = nodeType;
    }

    public int getChildNodePageSize()
    {
        return childNodePageSize;
    }

    public void setChildNodePageSize(int childNodePageSize)
    {
        this.childNodePageSize = childNodePageSize;
    }

    public int getChildNodePageOffset()
    {
        return childNodePageOffset;
    }

    public void setChildNodePageOffset(int childNodePageOffset)
    {
        this.childNodePageOffset = childNodePageOffset;
    }

    public int getChildNodeCount()
    {
        return childNodeCount;
    }

    public void setChildNodeCount(int childNodeCount)
    {
        this.childNodeCount = childNodeCount;
    }

    public void goToChildNodePage(int pageNumber)
    {
        if(pageNumber < 0)
            childNodePageOffset = 0;
        else if(pageNumber < getChildNodePageCount())
            childNodePageOffset = (pageNumber * childNodePageSize);
        else
            childNodePageOffset = childNodeCount;
    }

    public void incrementChildNodePage()
    {
        if ((getChildNodePageNumber() + 1) < getChildNodePageCount())
        {
            childNodePageOffset += childNodePageSize;
        }
        else
        {
            childNodePageOffset = 0;
        }
    }

    public void decrementChildNodePage()
    {
        if (getChildNodePageNumber() > 0)
        {
            childNodePageOffset -= childNodePageSize;
        }
        else
        {
            childNodePageOffset = ((getChildNodePageCount() - 1) * childNodePageSize) + 1;
        }
    }

    /**
     * Returns the node's current child page number.
     *
     * @return The current child page number.
     */
    public int getChildNodePageNumber()
    {
        if (childNodePageSize == 0)
        {
            return 0;
        }
        else
        {
            return java.lang.Math.round(childNodePageOffset/childNodePageSize);
        }
    }

    /**
     * Returns the total amount of child pages that are available to this node
     * in the database.
     *
     * @return The number of child pages available.
     */
    public int getChildNodePageCount()
    {
        if (childNodePageSize == 0)
        {
            return 0;
        }
        else
        {
            return (int)java.lang.Math.ceil(childNodeCount/(double)childNodePageSize);
        }
    }

    public void clearUserDefinedChildNodeFilters()
    {
        userDefinedChildNodeFilters.clear();
    }

    public void setUserDefinedChildNodeFilter(String childNodeIdentifier, T8DataFilter filter)
    {
        if (userDefinedChildNodeFilters == null) userDefinedChildNodeFilters = new HashMap<String, T8DataFilter>();
        userDefinedChildNodeFilters.put(childNodeIdentifier, filter);
    }

    public T8DataFilter getUserDefinedChildNodeFilter(String childNodeIdentifier)
    {
        return userDefinedChildNodeFilters != null ? userDefinedChildNodeFilters.get(childNodeIdentifier) : null;
    }

    public Map<String, T8DataFilter> getUserDefinedChildNodeFilters()
    {
        return userDefinedChildNodeFilters;
    }
}
