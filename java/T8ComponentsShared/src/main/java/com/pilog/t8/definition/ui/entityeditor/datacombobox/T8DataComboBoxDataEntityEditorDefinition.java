 package com.pilog.t8.definition.ui.entityeditor.datacombobox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityFieldEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxDataEntityEditorDefinition extends T8DataComboBoxDefinition implements T8DataEntityFieldEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_ENTITY_EDITOR_DATA_COMBO_BOX";
    public static final String DISPLAY_NAME = "Data Combo Box Data Entity Editor";
    public static final String DESCRIPTION = "An entity editor component that displays a single value selected by the user from a drop-down list of available options.";
    public enum Datum {PARENT_EDITOR_IDENTIFIER,
                       EDITOR_DATA_ENTITY_IDENTIFIER,
                       EDITOR_FIELD_MAPPING,
                       EDITOR_FILTER_FIELD_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8DataComboBoxDataEntityEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PARENT_EDITOR_IDENTIFIER.toString(), "Parent Data Entity Editor", "The data entity editor on which this editor is dependent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString(), "Editor Data Entity", "The data entity to which this editor is applicable."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.EDITOR_FIELD_MAPPING.toString(), "Field Mapping:  Editor Entity to Combo Box Entity", "A mapping of entity editor fields to the corresponding entity fields in the combo box entity.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.EDITOR_FILTER_FIELD_MAPPING.toString(), "Field Mapping:  Editor Entity to Combo Box Filter", "A mapping of entity editor fields to the filter used by the combo box for retrieval of content entities.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString())));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARENT_EDITOR_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8Definition> ancestors;
            ArrayList<T8DefinitionDatumOption> options;

            ancestors = getAncestorDefinitions();
            options = new ArrayList<>();
            for (T8Definition ancestor : ancestors)
            {
                if (ancestor instanceof T8DataEntityEditorDefinition)
                {
                    options.add(new T8DefinitionDatumOption(ancestor.getIdentifier(), ancestor.getIdentifier()));
                }
            }

            return options;
        }
        else if (Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if ((Datum.EDITOR_FIELD_MAPPING.toString().equals(datumIdentifier)) || (Datum.EDITOR_FILTER_FIELD_MAPPING.toString().equals(datumIdentifier)))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String editorEntityId;

            optionList = new ArrayList<>();

            editorEntityId = getEditorDataEntityIdentifier();
            if (editorEntityId != null)
            {
                T8DataEntityDefinition editorEntityDefinition;
                T8DataEntityDefinition comboBoxEntityDefinition;
                String projectId;

                projectId = getRootProjectId();
                editorEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, editorEntityId);
                comboBoxEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, getDataEntityIdentifier());

                if ((editorEntityDefinition != null) && (comboBoxEntityDefinition != null))
                {
                    List<T8DataEntityFieldDefinition> comboBoxEntityFieldDefinitions;
                    List<T8DataEntityFieldDefinition> editorEntityFieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    comboBoxEntityFieldDefinitions = comboBoxEntityDefinition.getFieldDefinitions();
                    editorEntityFieldDefinitions = editorEntityDefinition.getFieldDefinitions();

                    identifierMap = new HashMap<>();
                    if ((comboBoxEntityFieldDefinitions != null) && (editorEntityFieldDefinitions != null))
                    {
                        for (T8DataEntityFieldDefinition editorFieldDefinition : editorEntityFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataEntityFieldDefinition comboBoxFieldDefinition : comboBoxEntityFieldDefinitions)
                            {
                                identifierList.add(comboBoxFieldDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(editorFieldDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(Strings.isNullOrEmpty(getEditorDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(getEditorFieldMapping() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.EDITOR_FIELD_MAPPING.toString(), "Field Mappings not provided.", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    @Override
    public T8DataEntityEditorComponent getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.entityeditor.datacombobox.T8DataComboBoxDataEntityEditor", new Class<?>[]{T8DataComboBoxDataEntityEditorDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataComboBoxDataEntityEditorAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataComboBoxDataEntityEditorAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString());
    }

    @Override
    public void setParentEditorIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString(), identifier);
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString());
    }

    @Override
    public void setEditorDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.EDITOR_DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    @Override
    public String getEditorDataEntityFieldIdentifier()
    {
        return null;
    }

    @Override
    public void setEditorDataEntityFieldIdentifier(String identifier)
    {
    }

    public Map<String, String> getEditorFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.EDITOR_FIELD_MAPPING.toString());
    }

    public void setEditorFieldMapping(Map<String, String> fieldMapping)
    {
        setDefinitionDatum(Datum.EDITOR_FIELD_MAPPING.toString(), fieldMapping);
    }

    public Map<String, String> getEditorFilterFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.EDITOR_FILTER_FIELD_MAPPING.toString());
    }

    public void setEditorFilterFieldMapping(Map<String, String> fieldMapping)
    {
        setDefinitionDatum(Datum.EDITOR_FILTER_FIELD_MAPPING.toString(), fieldMapping);
    }
}
