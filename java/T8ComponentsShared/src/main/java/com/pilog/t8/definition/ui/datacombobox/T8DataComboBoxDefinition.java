package com.pilog.t8.definition.ui.datacombobox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_COMBO_BOX";
    public static final String DISPLAY_NAME = "Data Combobox";
    public static final String DESCRIPTION = "A component that dispays a single value selected by the user from a drop-down list of available options.";
    public static final String IDENTIFIER_PREFIX = "C_DATA_COMBO_BOX_";
    public enum Datum {DISPLAY_FIELD_IDENTIFIER,
                       DATA_ENTITY_IDENTIFIER,
                       PREFILTER_DEFINITIONS,
                       LOAD_ON_STARTUP,
                       SELECT_FRIST_VALUE_ON_LOAD,
                       ADD_NULL_VALUE,
                       NULL_SELECTION_DISPAY_rrNAME};
    // -------- Definition Meta-Data -------- //

    public T8DataComboBoxDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity displayed by this component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DISPLAY_FIELD_IDENTIFIER.toString(), "Display Field", "The field from which the value to display will be retrieved.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOAD_ON_STARTUP.toString(), "Load on Startup", "Load the data values on startup.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SELECT_FRIST_VALUE_ON_LOAD.toString(), "Select First Value on Load", "When this option is set to true, then the first value, if available, will be selected when the data is loaded.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ADD_NULL_VALUE.toString(), "Add Null Value", "When this option is set to true, then a null value with the specified display name will be added to the combo box options.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.NULL_SELECTION_DISPAY_rrNAME.toString(), "Null Selection Display Name", "The display name that will be used when a null selection is made on the combo box. Not that this will also appear when even if a null value was not added."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PREFILTER_DEFINITIONS.toString(), "Prefilters", "The prefilters that will be applied when the content data of the combobox is retrieved."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DISPLAY_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>();
        }
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(Strings.isNullOrEmpty(getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier not set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(Strings.isNullOrEmpty(getDisplayFieldIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DISPLAY_FIELD_IDENTIFIER.toString(), "Display Field not set.", T8DefinitionValidationError.ErrorType.CRITICAL));

        if(getPrefilterDefinitions() != null)
        {
            for (T8Definition t8Definition : getPrefilterDefinitions())
            {
                T8DataFilterDefinition dataFilterDefinition;

                dataFilterDefinition = (T8DataFilterDefinition) t8Definition;

                if(!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getPublicIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Invalid Entity Identifier on Filter, expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datacombobox.T8DataComboBox", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataComboBoxAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataComboBoxAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getDisplayFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_FIELD_IDENTIFIER.toString());
    }

    public void setDisplayFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DISPLAY_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public List<T8DataFilterDefinition> getPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString());
    }

    public void setPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public Boolean isLoadOnStartup()
    {
        return (Boolean)getDefinitionDatum(Datum.LOAD_ON_STARTUP.toString());
    }

    public void setLoadOnStartup(Boolean selectValue)
    {
        setDefinitionDatum(Datum.LOAD_ON_STARTUP.toString(), selectValue);
    }

    public Boolean isSelectFirstValueOnLoad()
    {
        return (Boolean)getDefinitionDatum(Datum.SELECT_FRIST_VALUE_ON_LOAD.toString());
    }

    public void setSelectFirstValueOnLoad(Boolean selectValue)
    {
        setDefinitionDatum(Datum.SELECT_FRIST_VALUE_ON_LOAD.toString(), selectValue);
    }

    public Boolean isAddNullValue()
    {
        return (Boolean)getDefinitionDatum(Datum.ADD_NULL_VALUE.toString());
    }

    public void setAddNullValue(Boolean addNullValue)
    {
        setDefinitionDatum(Datum.ADD_NULL_VALUE.toString(), addNullValue);
    }

    public String getNullSelectionDisplayName()
    {
        return (String)getDefinitionDatum(Datum.NULL_SELECTION_DISPAY_rrNAME.toString());
    }

    public void setNullSelectionDisplayName(String nullValueDisplayName)
    {
        setDefinitionDatum(Datum.NULL_SELECTION_DISPAY_rrNAME.toString(), nullValueDisplayName);
    }
}
