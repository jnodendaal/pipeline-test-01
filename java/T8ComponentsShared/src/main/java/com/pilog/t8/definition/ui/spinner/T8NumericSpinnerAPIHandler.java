package com.pilog.t8.definition.ui.spinner;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8TextField and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8NumericSpinnerAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_VALUE_CHANGED = "$CE_VALUE_CHANGED";
    public static final String EVENT_FOCUS_GAINED = "$CE_FOCUS_GAINED";
    public static final String EVENT_FOCUS_LOST = "$CE_FOCUS_LOST";

    public static final String OPERATION_SET_VALUE = "$CO_SET_VALUE";
    public static final String OPERATION_GET_VALUE = "$CO_GET_VALUE";
    public static final String OPERATION_SET_MIN_VALUE = "$CO_SET_MIN_VALUE";
    public static final String OPERATION_SET_MAX_VALUE = "$CO_SET_MAX_VALUE";

    public static final String PARAMETER_VALUE = "$CP_VALUE";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_VALUE_CHANGED);
        newEventDefinition.setMetaDisplayName("Value Changed");
        newEventDefinition.setMetaDescription("This event occurs every time the value changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The new value", T8DataType.DOUBLE));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FOCUS_GAINED);
        newEventDefinition.setMetaDisplayName("Focus Gained");
        newEventDefinition.setMetaDescription("This event occurs every time the text field gains focus.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The Value as it is when focus was gained", T8DataType.DOUBLE));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FOCUS_LOST);
        newEventDefinition.setMetaDisplayName("Focus Lost");
        newEventDefinition.setMetaDescription("This event occurs every time the text field loses focus.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The Value as it is when focus was gained", T8DataType.DOUBLE));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_VALUE);
        newOperationDefinition.setMetaDisplayName("Set Value");
        newOperationDefinition.setMetaDescription("This operations sets the value of the spinner.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The value to set the spinner to.", T8DataType.DOUBLE));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_MIN_VALUE);
        newOperationDefinition.setMetaDisplayName("Set Min Value");
        newOperationDefinition.setMetaDescription("This operations sets the minimum value of the spinner.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The min value to set on the spinner.", T8DataType.DOUBLE));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_MAX_VALUE);
        newOperationDefinition.setMetaDisplayName("Set Max Value");
        newOperationDefinition.setMetaDescription("This operations sets the maximum value of the spinner.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The max value to set on the spinner.", T8DataType.DOUBLE));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_VALUE);
        newOperationDefinition.setMetaDisplayName("Get Value");
        newOperationDefinition.setMetaDescription("This operations returns the value of the spinner.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The value of the spinner.", T8DataType.DOUBLE));
        operations.add(newOperationDefinition);

        return operations;
    }
}
