package com.pilog.t8.definition.ui.datasearch.combobox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_SEARCH_COMBO_BOX";
    public static final String DISPLAY_NAME = "Data Search Combobox";
    public static final String DESCRIPTION = "A component that dispays a single value selected by the user from a drop-down list of available options.";
    public static final String IDENTIFIER_PREFIX = "C_SEARCH_";
    public enum Datum {SEARCH_DISPLAY_NAME,
                       SEARCH_PROMPT,
                       DATA_SOURCE_IDENTIFIER,
                       DATA_ENTITY_IDENTIFIER,
                       SEARCH_FIELD_IDENTIFIERS,
                       TARGET_TABLE_IDENTIFIER,
                       ADD_OR_CONJUCTION};
    // -------- Definition Meta-Data -------- //

    public T8DataSearchComboBoxDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.SEARCH_DISPLAY_NAME.toString(), "Search Display Name", "The dispay name of this search that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.SEARCH_PROMPT.toString(), "Search Prompt", "The search filter prompt that will display search information to the user"));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_SOURCE_IDENTIFIER.toString(), "Data Source", "The data source that will be used to retrieve the display values."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity on which the search will be performed."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.SEARCH_FIELD_IDENTIFIERS.toString(), "Search Fields", "The entity fields on which the search will be performed.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.TARGET_TABLE_IDENTIFIER.toString(), "Target Table Identifier", "The table that will receive the user filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ADD_OR_CONJUCTION.toString(), "Add OR Conjunction", "Adds the OR conjunction on the filter criteria.", false));
        datumTypes.addAll(super.getDatumTypes());

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_SOURCE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataSearchComboBoxDataSourceDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.SEARCH_FIELD_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();
                    if (fieldDefinitions != null)
                    {
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            optionList.add(new T8DefinitionDatumOption(fieldDefinition.getPublicIdentifier(), fieldDefinition.getPublicIdentifier()));
                        }
                    }

                    return optionList;
                } else return new ArrayList<>();
            } else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.TARGET_TABLE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> definitionDatumOptions;
            List<T8Definition> tableDefinitions;

            tableDefinitions = getLocalDefinitionsOfType(T8TableDefinition.TYPE_IDENTIFIER);
            definitionDatumOptions = createLocalIdentifierOptionsFromDefinitions(tableDefinitions);

            definitionDatumOptions.add(0, new T8DefinitionDatumOption("Use Event Handler", null));

            return definitionDatumOptions;
        } else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataSearchComboBoxAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataSearchComboBoxAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBox", new Class<?>[]{T8DataSearchComboBoxDefinition.class, T8ComponentController.class}, this, controller);
    }

    public String getSearchDisplayName()
    {
        return getDefinitionDatum(Datum.SEARCH_DISPLAY_NAME);
    }

    public void setSearchDisplayName(String name)
    {
        setDefinitionDatum(Datum.SEARCH_DISPLAY_NAME, name);
    }

    public String getSearchPrompt()
    {
        return getDefinitionDatum(Datum.SEARCH_PROMPT);
    }

    public void setSearchPrompt(String prompt)
    {
        setDefinitionDatum(Datum.SEARCH_PROMPT, prompt);
    }

    public String getDataSourceIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_SOURCE_IDENTIFIER);
    }

    public void setDataSourceIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_SOURCE_IDENTIFIER, identifier);
    }

    public String getDataEntityIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER);
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER, identifier);
    }

    public List<String> getSearchFieldIdentifiers()
    {
        return getDefinitionDatum(Datum.SEARCH_FIELD_IDENTIFIERS);
    }

    public void setSearchFieldIdentifiers(List<String> fieldIdentifiers)
    {
        setDefinitionDatum(Datum.SEARCH_FIELD_IDENTIFIERS, fieldIdentifiers);
    }

    public String getTargetTableIdentifier()
    {
        return getDefinitionDatum(Datum.TARGET_TABLE_IDENTIFIER);
    }

    public void setTargetTableIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_TABLE_IDENTIFIER, identifier);
    }

    public void setOrConjunction(boolean orConjunction)
    {
        setDefinitionDatum(Datum.ADD_OR_CONJUCTION.toString(), orConjunction);
    }

    public boolean isOrConjunction()
    {
        return (Boolean)getDefinitionDatum(Datum.ADD_OR_CONJUCTION.toString());
    }
}
