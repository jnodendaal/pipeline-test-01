package com.pilog.t8.definition.ui.list;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.list.T8CustomListCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ListDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LIST";
    public static final String DISPLAY_NAME = "List";
    public static final String DESCRIPTION = "A component displays a list of values.  Values can be added or removed form the list.";
    public static final String IDENTIFIER_PREFIX = "C_LIST_";
    public enum Datum {SELECTION_MODE,
                       SCROLLABLE,
                       CELL_RENDERER_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public enum ListSelectionMode {SINGLE_SELECTION, SINGLE_INTERVAL_SELECTION, MULTIPLE_SELECTION};

    public T8ListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SELECTION_MODE.toString(), "Selection Mode", "Sets the selection mode of the list.  SINGLE_SELECTION allows only one item to be selected.  SINGLE_INTERVAL_SELECTION allows only one contiguous interval.  MULTIPLE_SELECTION allows any number of elements to be selected.", ListSelectionMode.SINGLE_SELECTION.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SCROLLABLE.toString(), "Scrollable", "If enabled, the list will be wrapped in a scroll pane", true));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CELL_RENDERER_DEFINITION.toString(), "Cell Renderer", "The cell renderer that will be used to draw the cells."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SELECTION_MODE.toString().equals(datumIdentifier)) return createStringOptions(ListSelectionMode.values());
        if (Datum.CELL_RENDERER_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8CustomListCellRendererDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.list.T8List", new Class<?>[]{T8ListDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8ListAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8ListAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        T8ComponentDefinition cellRendererDefinition;

        cellRendererDefinition = getCellRendererDefinition();
        if (cellRendererDefinition != null)
        {
            ArrayList<T8ComponentDefinition> childComponentDefinitions;

            childComponentDefinitions = new ArrayList<>();
            childComponentDefinitions.add(cellRendererDefinition);
            return childComponentDefinitions;
        }
        else return null;
    }

    public ListSelectionMode getSelectionMode()
    {
        return ListSelectionMode.valueOf(getDefinitionDatum(Datum.SELECTION_MODE));
    }

    public void setSelectionMode(ListSelectionMode selectionMode)
    {
        setDefinitionDatum(Datum.SELECTION_MODE, selectionMode.toString());
    }

    public boolean isScrollable()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.SCROLLABLE));
    }

    public void setScrollable(boolean scrollable)
    {
        setDefinitionDatum(Datum.SCROLLABLE, scrollable);
    }

    public T8CustomListCellRendererDefinition getCellRendererDefinition()
    {
        return getDefinitionDatum(Datum.CELL_RENDERER_DEFINITION);
    }

    public void setCellRendererDefinition(T8CustomListCellRendererDefinition definition)
    {
        setDefinitionDatum(Datum.CELL_RENDERER_DEFINITION, definition);
    }
}
