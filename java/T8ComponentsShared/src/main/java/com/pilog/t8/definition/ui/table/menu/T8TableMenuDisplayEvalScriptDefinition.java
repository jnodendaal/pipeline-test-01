/**
 * Created on Nov 9, 2015, 9:23:30 PM
 */
package com.pilog.t8.definition.ui.table.menu;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public class T8TableMenuDisplayEvalScriptDefinition extends T8ComponentControllerScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_UI_COMPONENT_TABLE_MENU_DISPLAY_EVAL";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_UI_COMPONENT_TABLE_MENU_DISPLAY_EVAL";
    public static final String IDENTIFIER_PREFIX = "TBL_DISP_EVAL_";
    public static final String DISPLAY_NAME = "Context Menu Display Evaluation Script";
    public static final String DESCRIPTION = "A script that will be executed each time before the menu is displayed.";
    // -------- Definition Meta-Data -------- //

    public T8TableMenuDisplayEvalScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8TableContextMenuDefinition contextMenuDefinition;

        contextMenuDefinition = (T8TableContextMenuDefinition)getAncestorDefinition(T8TableContextMenuDefinition.TYPE_IDENTIFIER);

        if (contextMenuDefinition != null) return contextMenuDefinition.getContextMenuParameterDefinitions();
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }
}