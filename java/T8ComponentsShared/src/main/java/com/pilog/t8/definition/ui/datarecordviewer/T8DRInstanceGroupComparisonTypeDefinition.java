package com.pilog.t8.definition.ui.datarecordviewer;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DRInstanceGroupComparisonTypeDefinition extends T8ComparisonTypeDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_RECORD_VIEWER_COMPARISON_TYPE_DR_INSTANCE_GROUP";
    public static final String DISPLAY_NAME = "DR Instance Group Comparison Type";
    public static final String DESCRIPTION = "A comparison type that groups records based on DR Instances from the same concept group tofether.";
    private enum Datum {DR_INSTANCE_GROUP_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DRInstanceGroupComparisonTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.DR_INSTANCE_GROUP_IDENTIFIER.toString(), "DR Instance Group ID", "The Concept ID of the DR Instance group to include in this comparison type."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8ComparisonType createComparisonType(T8ComponentController controller, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider) throws Exception
    {
         Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.datarecordviewer.model.T8DRInstanceGroupComparisonType").getConstructor(T8ComponentController.class, TerminologyProvider.class, OntologyProvider.class, T8DRInstanceGroupComparisonTypeDefinition.class);
        return (T8ComparisonType)constructor.newInstance(controller, terminologyProvider, ontologyProvider, this);
    }

    public String getDRInstanceGroupIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DR_INSTANCE_GROUP_IDENTIFIER.toString());
    }

    public void setDRInstanceGroupIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_GROUP_IDENTIFIER.toString(), identifier);
    }
}
