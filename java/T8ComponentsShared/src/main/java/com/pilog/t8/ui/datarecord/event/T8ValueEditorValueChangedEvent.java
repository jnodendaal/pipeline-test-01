package com.pilog.t8.ui.datarecord.event;

import com.pilog.t8.ui.datarecord.DataRecordEditor;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8ValueEditorValueChangedEvent extends EventObject
{
    private Object oldValue;
    private Object newValue;
    
    public T8ValueEditorValueChangedEvent(DataRecordEditor source, Object oldValue, Object newValue)
    {
        super(source);
        this.oldValue = oldValue;
        this.newValue = newValue;
    }
    
    public DataRecordEditor getDataRecordEditor()
    {
        return (DataRecordEditor)getSource();
    }

    public Object getOldValue()
    {
        return oldValue;
    }

    public Object getNewValue()
    {
        return newValue;
    }
}
