package com.pilog.t8.definition.ui.flowpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPaneDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FLOW_PANE";
    public static final String DISPLAY_NAME = "Flow Pane";
    public static final String DESCRIPTION = "A UI container to which other UI components can be added.  The contained components are organized using a flow layout.";
    public enum Datum {ALIGNMENT,
                       VERTICAL_GAP,
                       HORIZONTAL_GAP,
                       COMPONENT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //
    
    public enum FlowAlignment {LEFT, CENTER, RIGHT};

    public T8FlowPaneDefinition(String identifier)
    {
        super(identifier);
        setAlignment(FlowAlignment.CENTER);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ALIGNMENT.toString(), "Alignment", "The alignment of the component within the flow layout.", FlowAlignment.LEFT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.VERTICAL_GAP.toString(), "Vertical Gap", "The gap (in pixels) to add to the top and bottom of content components within the flow layout."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HORIZONTAL_GAP.toString(), "Horizontal Inset", "The gap (in pixels) to add to the left and right of content components within the flow layout."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COMPONENT_DEFINITIONS.toString(), "Components", "The components stacked into this column in sequence from top to bottom."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(FlowAlignment.values());
        else if (Datum.COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        
        constructor = Class.forName("com.pilog.t8.ui.flowpane.T8FlowPane").getConstructor(T8FlowPaneDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<T8ComponentDefinition>(getComponentDefinitions());
    }     
    
    public List<T8ComponentDefinition> getComponentDefinitions()
    {
        return (List<T8ComponentDefinition>)getDefinitionDatum(Datum.COMPONENT_DEFINITIONS.toString());
    }

    public void setComponentDefinitions(List<T8ComponentDefinition> componentDefinitions)
    {
        setDefinitionDatum(Datum.COMPONENT_DEFINITIONS.toString(), componentDefinitions);
    }
    
    public FlowAlignment getAlignment()
    {
        return FlowAlignment.valueOf((String)getDefinitionDatum(Datum.ALIGNMENT.toString()));
    }

    public void setAlignment(FlowAlignment alignment)
    {
        setDefinitionDatum(Datum.ALIGNMENT.toString(), alignment.toString());
    }  
    
    public int getVerticalGap()
    {
        Integer gap;
        
        gap = (Integer)getDefinitionDatum(Datum.VERTICAL_GAP.toString());
        return gap != null ? gap : 0;
    }

    public void setVerticalGap(int verticalGap)
    {
        setDefinitionDatum(Datum.VERTICAL_GAP.toString(), verticalGap);
    }  
    
    public int getHorizontalGap()
    {
        Integer gap;
        
        gap = (Integer)getDefinitionDatum(Datum.HORIZONTAL_GAP.toString());
        return gap != null ? gap : 0;
    }

    public void setHorizontalGap(int horizontalGap)
    {
        setDefinitionDatum(Datum.HORIZONTAL_GAP.toString(), horizontalGap);
    }  
}
