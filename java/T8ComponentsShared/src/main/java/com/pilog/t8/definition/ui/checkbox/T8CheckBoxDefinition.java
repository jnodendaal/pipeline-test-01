package com.pilog.t8.definition.ui.checkbox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CHECK_BOX";
    public static final String DISPLAY_NAME = "Check Box";
    public static final String DESCRIPTION = "A component that allows the user to thick a box in order to set a boolean true/false value.";
    public static final String IDENTIFIER_PREFIX = "C_CHECK_BOX_";
    public enum Datum {TEXT, CHECKED};
    // -------- Definition Meta-Data -------- //

    public T8CheckBoxDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text displayed next to the check box."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CHECKED.toString(), "Checked", "The initial state of the check box."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.checkbox.T8CheckBox").getConstructor(T8CheckBoxDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8CheckBoxAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8CheckBoxAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT);
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT, text);
    }

    public boolean isChecked()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.CHECKED);
        return value != null && value;
    }

    public void setChecked(Boolean checked)
    {
        setDefinitionDatum(Datum.CHECKED, checked);
    }
}
