package com.pilog.t8.definition.ui.cellrenderer.table.attachment;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Hennie Brink
 */
public class T8AttachmentTableCellRendererAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String O_REC_RETRIEVE_ATTACHMENT_TUMBNAIL = "@O_REC_RETRIEVE_ATTACHMENT_TUMBNAIL";

    public static final String PARAMETER_ATTACHMENT_ID = "$P_ATTACHMENT_ID";
    public static final String PARAMETER_ATTACHMENT_TUMBNAIL = "$P_ATTACHMENT_TUMBNAIL";
    public static final String PARAMETER_WIDTH = "$P_WIDTH";
    public static final String PARAMETER_HEIGHT = "$P_HEIGHT";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(O_REC_RETRIEVE_ATTACHMENT_TUMBNAIL);
            definition.setMetaDisplayName("Retrieve Attachment Thumbnail");
            definition.setMetaDescription("Retrieves a thumbnail for the specified attachment.");
            definition.setClassName("com.pilog.t8.ui.cellrenderer.table.attachment.server.T8AttachmentTableCellRendererOperations$RetrieveAttachmentThumbnailOperation");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ATTACHMENT_ID, "Attachment ID", "The attachment ID of the attachment", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_WIDTH, "Width", "The width of the thumbnail.", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_HEIGHT, "Height", "The height of the thumbnail.", T8DataType.INTEGER));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ATTACHMENT_TUMBNAIL, "Ättachment Tumbnail", "The thumbnail for the requested attachment.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        }
        else
        {
            return null;
        }
    }
}
