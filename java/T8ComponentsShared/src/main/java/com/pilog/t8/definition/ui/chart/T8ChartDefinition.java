package com.pilog.t8.definition.ui.chart;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ChartDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public enum Datum {TITLE,
                       LEGEND_VISIBLE,
                       OUTLINE_VISIBLE,
                       CHART_SERIES_PAINT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ChartDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TITLE.toString(), "Title", "The title of the chart."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LEGEND_VISIBLE.toString(), "Show Legend", "A boolean setting that makes the chart legend visible (if set to true)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OUTLINE_VISIBLE.toString(), "Draw Outline", "A setting to enabled/disable the drawing of an outline around the chart.", true));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHART_SERIES_PAINT_DEFINITIONS.toString(), "Series Paint Definitions", "The paint definitions that will be used to set the paint colours for the diferent series, note that this list is index based."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CHART_SERIES_PAINT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PaintDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getTitle()
    {
        return (String)getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String title)
    {
        setDefinitionDatum(Datum.TITLE.toString(), title);
    }

    public boolean isLegendVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.LEGEND_VISIBLE.toString());
        return (visible != null) && visible;
    }

    public void setLegendVisible(boolean visible)
    {
        setDefinitionDatum(Datum.LEGEND_VISIBLE.toString(), visible);
    }

    public boolean isOutlineVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.OUTLINE_VISIBLE.toString());
        return (visible != null) && visible;
    }

    public void setOutlineVisible(boolean visible)
    {
        setDefinitionDatum(Datum.OUTLINE_VISIBLE.toString(), visible);
    }

    public List<T8PaintDefinition> getChartSeriesPaintDefinitions()
    {
        return (List<T8PaintDefinition>)getDefinitionDatum(Datum.CHART_SERIES_PAINT_DEFINITIONS.toString());
    }

    public void setChartSeriesPaintDefinitions(List<T8PaintDefinition> paintDefinitions)
    {
        setDefinitionDatum(Datum.CHART_SERIES_PAINT_DEFINITIONS.toString(), paintDefinitions);
    }
}
