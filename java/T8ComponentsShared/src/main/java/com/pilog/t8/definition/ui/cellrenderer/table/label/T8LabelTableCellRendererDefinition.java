package com.pilog.t8.definition.ui.cellrenderer.table.label;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.cellrenderer.T8LabelCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8LabelTableCellRendererDefinition extends T8LabelCellRendererDefinition implements T8TableCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_LABEL";
    public static final String DISPLAY_NAME = "Label Cell Renderer";
    public static final String DESCRIPTION = "A cell editor that displays the content of a cell as a simple text string.";
    public enum Datum {DISPLAY_VALUE_EXPRESSION,
                       FORMATTER};
    // -------- Definition Meta-Data -------- //

    public T8LabelTableCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DISPLAY_VALUE_EXPRESSION.toString(), "Display Value Expression", "An optional expression that is evaluated using the renderer data in order to determine the value to display."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FORMATTER.toString(), "Formatter", "The formatter that will be used to format the value of this field."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FORMATTER.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8LabelTableCellRendererFormatDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.cellrenderer.table.label.T8LabelTableCellRenderer", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    public String getDisplayValueExpression()
    {
        return getDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION);
    }

    public void setDisplayValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION, expression);
    }

    public T8LabelTableCellRendererFormatDefinition getFormatter()
    {
        return getDefinitionDatum(Datum.FORMATTER);
    }

    public void setFormatter(T8LabelTableCellRendererFormatDefinition formatter)
    {
        setDefinitionDatum(Datum.FORMATTER, formatter);
    }
}
