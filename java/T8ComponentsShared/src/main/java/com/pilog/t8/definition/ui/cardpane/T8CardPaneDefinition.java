package com.pilog.t8.definition.ui.cardpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CardPaneDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CARD_PANE";
    public static final String DISPLAY_NAME = "Card Pane";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  Each of the components added to a card pane is referred to as a card.  Any card may be made visible, but only one card is visible at a time.";
    public static final String IDENTIFIER_PREFIX = "C_CARD_PANE_";
    public enum Datum {CHILD_COMPONENT_DEFINITIONS,
                       INITIAL_COMPONENT_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8CardPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.INITIAL_COMPONENT_IDENTIFIER.toString(), "Initial Component", "The component to show when this card pane is first initialized."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHILD_COMPONENT_DEFINITIONS.toString(), "Card Components", "The components contained by this card pane."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CHILD_COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.INITIAL_COMPONENT_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8ComponentDefinition> childComponentDefinitions;
            ArrayList<T8DefinitionDatumOption> options;
            
            options = new ArrayList<T8DefinitionDatumOption>();
            childComponentDefinitions = getChildComponentDefinitions();
            for (T8ComponentDefinition childComponentDefinition : childComponentDefinitions)
            {
                options.add(new T8DefinitionDatumOption(childComponentDefinition.getIdentifier(), childComponentDefinition.getIdentifier()));
            }
            
            return options;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            return (T8Component)Class.forName("com.pilog.t8.ui.cardpane.T8CardPane").getConstructor(T8CardPaneDefinition.class, T8ComponentController.class).newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8CardPaneAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8CardPaneAPIHandler.getOperationDefinitions();
    }
    
    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList<T8ComponentDefinition>)getDefinitionDatum(Datum.CHILD_COMPONENT_DEFINITIONS.toString());
    }

    public void addChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        getChildComponentDefinitions().add(componentDefinition);
    }

    public boolean removeChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        return getChildComponentDefinitions().remove(componentDefinition);
    }  
    
    public String getInitialComponentIdentifier()
    {
        return (String)getDefinitionDatum(Datum.INITIAL_COMPONENT_IDENTIFIER.toString());
    }

    public void setInitialComponentIdentifier(String buttonText)
    {
        setDefinitionDatum(Datum.INITIAL_COMPONENT_IDENTIFIER.toString(), buttonText);
    }    
}
