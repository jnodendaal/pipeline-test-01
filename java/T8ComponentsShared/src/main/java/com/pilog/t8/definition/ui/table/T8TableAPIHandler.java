package com.pilog.t8.definition.ui.table;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataManagerResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Table and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8TableAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String EVENT_ROW_SELECTION_CHANGED = "$CE_ROW_SELECTION_CHANGED";
    public static final String EVENT_TICK_SELECTION_CHANGED = "$CE_TICK_SELECTION_CHANGED";
    public static final String EVENT_DOUBLE_CLICKED = "$CE_DOUBLE_CLICKED";
    public static final String EVENT_FILTER_UPDATED = "$CE_FILTER_UPDATED";
    public static final String EVENT_DATA_CHANGES_SAVED = "$CE_DATA_CHANGES_SAVED";
    public static final String EVENT_DATA_CHANGES_SAVE_FAILED = "$CE_DATA_CHANGES_SAVE_FAILED";
    public static final String EVENT_DATA_LOADED = "$CE_DATA_REFRESHED";
    public static final String EVENT_DATA_LOADING = "$CE_DATA_LOADING";
    public static final String EVENT_DATA_ENTITY_COUNT_REFRESHED = "$CE_DATA_ENTITY_COUNT_REFRESHED";

    public static final String SERVER_OPERATION_TABLE_SAVE_DATA_CHANGES = "@OS_API_TABLE_SAVE_DATA_CHANGES";

    public static final String OPERATION_GET_DATA_OBJECT = "$CO_GET_DATA_OBJECT";
    public static final String OPERATION_GET_FIRST_SELECTED_DATA_ROW = "$CO_GET_FIRST_SELECTED_DATA_ROW";
    public static final String OPERATION_GET_SELECTED_DATA_ROWS = "$CO_GET_SELECTED_DATA_ROWS";
    public static final String OPERATION_GET_DATA_ENTITIES = "$CO_GET_DATA_ENTITIES";
    public static final String OPERATION_GET_SELECTED_DATA_ENTITIES = "$CO_GET_SELECTED_DATA_ENTITIES";
    public static final String OPERATION_GET_TICKED_DATA_ENTITIES = "$CO_GET_TICKED_DATA_ENTITIES";
    public static final String OPERATION_PREFILTER_BY_KEY = "$CO_PREFILTER_BY_KEY";
    public static final String OPERATION_CLEAR_PREFILTERS = "$CO_CLEAR_PREFILTERS";
    public static final String OPERATION_SET_PREFILTER = "$CO_SET_PREFILTER";
    public static final String OPERATION_GET_PREFILTER = "$CO_GET_PREFILTER";
    public static final String OPERATION_SET_KEY = "$CO_SET_KEY";
    public static final String OPERATION_GET_KEY = "$CO_GET_KEY";
    public static final String OPERATION_CLEAR_USER_DEFINED_FILTER = "$CO_CLEAR_USER_DEFINED_FILTER";
    public static final String OPERATION_GET_USER_DEFINED_FILTER = "$CO_GET_USER_DEFINED_FILTER";
    public static final String OPERATION_GET_COMBINED_FILTER = "$CO_GET_COMBINED_FILTER";
    public static final String OPERATION_REFRESH = "$CO_REFRESH";
    public static final String OPERATION_HAS_DATA_CHANGES = "$CO_HAS_DATA_CHANGES";
    public static final String OPERATION_SAVE_DATA_CHANGES = "$CO_SAVE_DATA_CHANGES";
    public static final String OPERATION_GET_DATA_CHANGES = "$CO_GET_DATA_CHANGES";
    public static final String OPERATION_GET_DATA_ENTITY_COUNT = "$CO_GET_DATA_ENTITY_COUNT";
    public static final String OPERATION_SET_COLUMN_VISIBILITY = "$CO_SET_COLUMN_VISIBILITY";
    public static final String OPERATION_CLEAR_SELECTION = "$CO_CLEAR_SELECTION";
    public static final String OPERATION_CLEAR_DATA = "$CO_CLEAR_DATA";
    public static final String OPERATION_SET_OUTDATED = "$CO_SET_OUTDATED";
    public static final String OPERATION_IS_OUTDATED = "$CO_IS_OUTDATED";

    public static final String PARAMETER_DATA_OBJECT_IDENTIFIER = "$P_DATA_OBJECT_IDENTIFIER";
    public static final String PARAMETER_DATA_OBJECT = "$P_DATA_OBJECT";
    public static final String PARAMETER_DATA_ENTITY_COUNT = "$P_DATA_ENTITY_COUNT";
    public static final String PARAMETER_KEY_MAP = "$CP_KEY_MAP";
    public static final String PARAMETER_DATA_ROW = "$CP_DATA_ROW";
    public static final String PARAMETER_DATA_ROWS = "$CP_DATA_ROWS";
    public static final String PARAMETER_DATA_ENTITY_LIST = "$CP_DATA_ENTITY_LIST";
    public static final String PARAMETER_VALIDATION_ERROR_LIST = "$P_VALIDATION_ERROR_LIST";
    public static final String PARAMETER_VALIDATION_REPORT_LIST = "$P_VALIDATION_REPORT_LIST";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_INSERTED_ENTITY_LIST = "$CP_INSERTED_ENTITY_LIST";
    public static final String PARAMETER_DELETED_ENTITY_LIST = "$CP_DELETED_ENTITY_LIST";
    public static final String PARAMETER_UPDATED_OLD_ENTITY_LIST = "$CP_UPDATED_OLD_ENTITY_LIST";
    public static final String PARAMETER_UPDATED_NEW_ENTITY_LIST = "$CP_UPDATED_NEW_ENTITY_LIST";
    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$CP_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_REFRESH_DATA = "$CP_REFRESH_DATA";
    public static final String PARAMETER_COLUMN_IDENTIFIER_LIST = "$P_COLUMN_IDENTIFIER_LIST";
    public static final String PARAMETER_VISIBLE = "$P_VISIBLE";
    public static final String PARAMETER_CHECK_OUTDATED = "$P_CHECK_OUTDATED";
    public static final String PARAMETER_OUTDATED = "$P_OUTDATED";
    public static final String PARAMETER_HAS_DATA_CHANGES = "$P_HAS_DATA_CHANGES";
    public static final String PARAMETER_APPLY_INVERSE = "$P_APPLY_INVERSE";
    public static final String PARAMETER_TABLE_ROW_CHANGE_LIST = "$P_TABLE_ROW_CHANGE_LIST";
    public static final String PARAMETER_TABLE_ID = "$P_TABLE_ID";
    public static final String PARAMETER_OUTPUT_DATA_ENTITY_LIST = "$P_OUTPUT_DATA_ENTITY_LIST";
    public static final String PARAMETER_DATA_ENTITY_VALIDATION_REPORT_LIST = T8DataManagerResource.PARAMETER_DATA_ENTITY_VALIDATION_REPORT_LIST;
    public static final String PARAMETER_DATA_ENTITY_VALIDATION_SCRIPT_DEFINITION = T8DataManagerResource.PARAMETER_DATA_ENTITY_VALIDATION_SCRIPT_DEFINITION;

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FILTER_UPDATED);
        newEventDefinition.setMetaDisplayName("Event Filter Updated");
        newEventDefinition.setMetaDescription("This event occurs when the table filter has been updated.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_LOADING);
        newEventDefinition.setMetaDisplayName("Data Loading");
        newEventDefinition.setMetaDescription("This event occurs when the table is starts loading new data.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_LOADED);
        newEventDefinition.setMetaDisplayName("Data Refreshed");
        newEventDefinition.setMetaDescription("This event occurs when the data of the table has been updated and the user can interact with the table again.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_ENTITY_COUNT_REFRESHED);
        newEventDefinition.setMetaDisplayName("Data Entity Count Refreshed");
        newEventDefinition.setMetaDescription("This event occurs when the data entity count displayed by the table has been refreshed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_COUNT, "Data Entity Count", "The new updated data entity count.", T8DataType.INTEGER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_ROW_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Table Row Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selection of rows in the table changes.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "A List of Data Entities selected.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TICK_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Table Row Tick Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the rows in the table are ticked or unticked.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DOUBLE_CLICKED);
        newEventDefinition.setMetaDisplayName("Double Clicked");
        newEventDefinition.setMetaDescription("This event occurs when the double clicks on a selection of data in the table (only when clicking on non-editable cells).");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "A List of Data Entities selected when the double clicked occurred.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_CHANGES_SAVED);
        newEventDefinition.setMetaDisplayName("Data Changes Saved");
        newEventDefinition.setMetaDescription("This event occurs when all outstanding changes on the table session has been saved.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INSERTED_ENTITY_LIST, "Inserted Entities", "A List of Data Entities inserted when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DELETED_ENTITY_LIST, "Deleted Entities", "A List of Data Entities deleted when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_UPDATED_OLD_ENTITY_LIST, "Updated Old Entities", "A List of Data Entities that were updated (old values) when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_UPDATED_NEW_ENTITY_LIST, "Updated New Entities", "A List of Data Entities that were updated (new values) when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_CHANGES_SAVE_FAILED);
        newEventDefinition.setMetaDisplayName("Data Changes Save Failed");
        newEventDefinition.setMetaDescription("This event occurs when an attempt was made to save outstanding changes on the table, but the save action failed due to validation errors.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INSERTED_ENTITY_LIST, "Inserted Entities", "A List of Data Entities inserted when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DELETED_ENTITY_LIST, "Deleted Entities", "A List of Data Entities deleted when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_UPDATED_OLD_ENTITY_LIST, "Updated Old Entities", "A List of Data Entities that were updated (old values) when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_UPDATED_NEW_ENTITY_LIST, "Updated New Entities", "A List of Data Entities that were updated (new values) when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALIDATION_REPORT_LIST, "Validation Error List", "A List of validation errors which occurred during the save process.", T8DataType.LIST));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_OBJECT);
        newOperationDefinition.setMetaDisplayName("Get Data Object");
        newOperationDefinition.setMetaDescription("This operation returns the data object of the specified type provided by the table.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_OBJECT_IDENTIFIER, "Data Object Identifier", "The Identifier of the Data Object to fetch.  If null, the default data object will be provided.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_OBJECT, "Data Object", "The specified Data Object fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_KEY);
        newOperationDefinition.setMetaDisplayName("Set Key");
        newOperationDefinition.setMetaDescription("This operation sets the table's key values.  If not null, the table will be filtered according to these key values and whenever any new entities are created by the table the key values will automatically be added to the entity.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values to add to the table pre-filter.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_KEY);
        newOperationDefinition.setMetaDisplayName("Get Key");
        newOperationDefinition.setMetaDescription("Returns the table's current key values.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values currently set on the table.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_PREFILTER_BY_KEY);
        newOperationDefinition.setMetaDisplayName("Prefilter by Key");
        newOperationDefinition.setMetaDescription("This operation sets the table's pre-filter according to the supplied key values.  The pre-filter is the base data filter used when retrieving table data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values to add to the table pre-filter.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_PREFILTERS);
        newOperationDefinition.setMetaDisplayName("Clear Prefilters");
        newOperationDefinition.setMetaDescription("This operation will clear any prefilters that have been set on the table component. This does not clear user defined filters.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter's have been removed (default=false).", T8DataType.BOOLEAN, true));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the table's pre-filter.  The pre-filter is the base data filter used when retrieving table data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the table.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the table that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Get Prefilter");
        newOperationDefinition.setMetaDescription("This operation returns one of the prefilters currently set on the table.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Filter Identifier", "The Identifier of the prefilter to fetch.", T8DataType.MAP));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Prefilter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_USER_DEFINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Clear User Defined Filter");
        newOperationDefinition.setMetaDescription("This operation clears the data filter that is currently set on the table by the user.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the user defined filter has been set (default=false).", T8DataType.BOOLEAN, true));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_USER_DEFINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Get User Defined Filter");
        newOperationDefinition.setMetaDescription("This operation returns the data filter that is currently set on the table by the user.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Filter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_COMBINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Get Combined Filter");
        newOperationDefinition.setMetaDescription("This operation returns the combined data filter (user-defined filter + prefilters) that is currently set on the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Filter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_FIRST_SELECTED_DATA_ROW);
        newOperationDefinition.setMetaDisplayName("Get First Selected Data Rows");
        newOperationDefinition.setMetaDescription("This operation returns the first selected data row in the table.  If no rows are selected a null value is returned.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ROW, "Data Row", "A Map containing the values of the first selected row in the table.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ROWS);
        newOperationDefinition.setMetaDisplayName("Get Selected Data Rows");
        newOperationDefinition.setMetaDescription("This operation returns a list of all the selected data rows in the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ROWS, "Data Rows", "A List of Maps, each Map representing one row of selected data.", new T8DtList(new T8DtMap(T8DataType.STRING, T8DataType.CUSTOM_OBJECT))));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Get Selected Data Entities");
        newOperationDefinition.setMetaDescription("This operation returns a list of all the selected data entities in the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "A List of Data Entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_TICKED_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Get Ticked Data Entities");
        newOperationDefinition.setMetaDescription("This operation returns a list of all the data entities in the table that are ticked.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "A List of Data Entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Get Data Entities");
        newOperationDefinition.setMetaDescription("This operation returns a list of all the data entities currently retrieved and displayed in the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "A List of Data Entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_OUTDATED);
        newOperationDefinition.setMetaDisplayName("Set Outdated Flag");
        newOperationDefinition.setMetaDescription("Sets a flag on the table indicating whether or not the data it is currently displaying can be considered outdated.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OUTDATED, "Outdated", "If this parameter is true, it indicates that the data in the table is outdated and should be refreshed at the soonest opportunity.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_IS_OUTDATED);
        newOperationDefinition.setMetaDisplayName("Is Outdated");
        newOperationDefinition.setMetaDescription("Returns the flag on the table indicating whether or not the data it is currently displaying can be considered outdated.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OUTDATED, "Outdated", "If this parameter is true, it indicates that the data in the table is outdated and should be refreshed at the soonest opportunity.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH);
        newOperationDefinition.setMetaDisplayName("Refresh");
        newOperationDefinition.setMetaDescription("This operation re-retrieves the data displayed by the table.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CHECK_OUTDATED, "Check Outdated Flag", "If this parameter is true, the outdated flag of the table will be checked and data will only be refreshed if the flag is set to true.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SAVE_DATA_CHANGES);
        newOperationDefinition.setMetaDisplayName("Save Data Changes");
        newOperationDefinition.setMetaDescription("This operation saves all data changes (inserts, updates and deletions) on the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "A boolean flag to indicate whether or not the save operation was successful.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_SELECTION);
        newOperationDefinition.setMetaDisplayName("Clear Selection");
        newOperationDefinition.setMetaDescription("Deselects all rows in the table.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_DATA);
        newOperationDefinition.setMetaDisplayName("Clear Data");
        newOperationDefinition.setMetaDescription("Clears all retrieved data from the table.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_HAS_DATA_CHANGES);
        newOperationDefinition.setMetaDisplayName("Has Data Changes");
        newOperationDefinition.setMetaDescription("This operation returns a Boolean 'true' value if the table contains any unsaved changes.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_HAS_DATA_CHANGES, "Has Data Changes", "A boolean value indicating whether or not the table contains unsaved data changes.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_CHANGES);
        newOperationDefinition.setMetaDisplayName("Get Data Changes");
        newOperationDefinition.setMetaDescription("This operation gets all data changes (inserts, updates and deletions) on the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INSERTED_ENTITY_LIST, "Inserted Entities", "A List of Data Entities inserted when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DELETED_ENTITY_LIST, "Deleted Entities", "A List of Data Entities deleted when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_UPDATED_OLD_ENTITY_LIST, "Updated Old Entities", "A List of Data Entities that were updated (old values) when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_UPDATED_NEW_ENTITY_LIST, "Updated New Entities", "A List of Data Entities that were updated (new values) when when table changes were saved.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_ENTITY_COUNT);
        newOperationDefinition.setMetaDisplayName("Get Data Entity Count");
        newOperationDefinition.setMetaDescription("This operation returns a total count of all entities available in the result set of the table using all currently set filters.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_COUNT, "Data Entity Count", "An integer value denoting the number of entities available in theh table's result set.", T8DataType.INTEGER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_COLUMN_VISIBILITY);
        newOperationDefinition.setMetaDisplayName("Set Column Visibility");
        newOperationDefinition.setMetaDescription("This sets the visibility of a specified list of tables columns.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COLUMN_IDENTIFIER_LIST, "Column Identifiers", "The list of column identifiers for which visibility will be set.", T8DataType.LIST));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "If true, all columns referred to in the supplied list will be set as visible.  If false, the columns will be hidden.", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_APPLY_INVERSE, "Apply Inverse", "If true, the inverse of this operation will also be applied i.e. all columns not in the list will be set to the opposite visibility state as specified.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_TABLE_SAVE_DATA_CHANGES);
            definition.setMetaDescription("Save Table Data Changes");
            definition.setClassName("com.pilog.t8.ui.table.server.T8TableAPIOperations$SaveDataChangesOperation");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TABLE_ID, "Table Id", "The Id of the table requesting the data changes.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TABLE_ROW_CHANGE_LIST, "Table Changes", "The list of table changes to be committed.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OUTPUT_DATA_ENTITY_LIST, "Output Entities", "The list of post-commit entity changes to be reflected on the T8Table.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALIDATION_REPORT_LIST, "Validation Report List", "The list of validation errors (if any) identified during the commit operation.", T8DataType.LIST));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
