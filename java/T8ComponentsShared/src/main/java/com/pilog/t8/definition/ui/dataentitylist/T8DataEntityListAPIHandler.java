package com.pilog.t8.definition.ui.dataentitylist;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataEntityList and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataEntityListAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGE = "$CE_SELECTION_CHANGE";
    public static final String EVENT_SEQUENCE_CHANGE = "$CE_SEQUENCE_CHANGE";
    public static final String EVENT_DATA_ENTITIES_ADDED = "$CE_DATA_ENTITIES_ADDED";
    public static final String EVENT_DATA_ENTITIES_REMOVED = "$CE_DATA_ENTITIES_REMOVED";

    // Operations
    public static final String OPERATION_GET_SELECTED_DATA_ENTITIES = "$CO_GET_SELECTED_DATA_ENTITIES";
    public static final String OPERATION_GET_SELECTED_DATA_ENTITY = "$CO_GET_SELECTED_DATA_ENTITY";
    public static final String OPERATION_GET_DATA_ENTITY_LIST = "$CO_GET_DATA_ENTITY_LIST";
    public static final String OPERATION_SET_LIST_PREFILTER = "$CO_SET_LIST_PREFILTER";
    public static final String OPERATION_SET_LOOKUP_PREFILTER = "$CO_SET_LOOKUP_PREFILTER";
    public static final String OPERATION_ADD_DATA_ENTITIES = "$CO_ADD_DATA_ENTITIES";
    public static final String OPERATION_CLEAR = "$CO_CLEAR";
    public static final String OPERATION_REFRESH = "$CO_REFRESH";
    public static final String OPERATION_REMOVE_SELECTED_DATA_ENTITIES = "$CO_REMOVE_SELECTED_DATA_ENTITIES";

    // Parameters.
    public static final String PARAMETER_DATA_ENTITY_LIST = "$CP_DATA_ENTITY_LIST";
    public static final String PARAMETER_DATA_ENTITY = "$CP_DATA_ENTITY";
    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$CP_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_REFRESH_DATA = "$CP_REFRESH_DATA";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGE);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selection of entities has changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The first selected entity in the list.", T8DataType.DATA_ENTITY));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "The list of selected data entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SEQUENCE_CHANGE);
        newEventDefinition.setMetaDisplayName("Sequence Changed");
        newEventDefinition.setMetaDescription("This event occurs when the sequence of the entities in the list has changed.");
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_ENTITIES_ADDED);
        newEventDefinition.setMetaDisplayName("Data Entities Added");
        newEventDefinition.setMetaDescription("This event occurs when new data entities have been added to the list.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "The list of data entities added.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_ENTITIES_REMOVED);
        newEventDefinition.setMetaDisplayName("Data Entities Removed");
        newEventDefinition.setMetaDescription("This event occurs when new data entities have been removed from the list.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "The list of data entities removed.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Selected Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently selected and displayed by the component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The selected data entity.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Get Selected Entities");
        newOperationDefinition.setMetaDescription("This operations returns the data entities currently selected and displayed by the component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "The selected data entity list.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_ENTITY_LIST);
        newOperationDefinition.setMetaDisplayName("Get Entities");
        newOperationDefinition.setMetaDescription("This operations returns the list of data entities currently contained by the list.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "The data entities contained by the list.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_LOOKUP_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Lookup Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the pre-filter by which data available in the lookup menu/dialog will be filtered.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the data lookup menu/dialog.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the list that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the list should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_LIST_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set List Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the list's pre-filter.  The pre-filter is the base data filter used when retrieving list data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the list.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the list that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the list should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR);
        newOperationDefinition.setMetaDisplayName("Clear");
        newOperationDefinition.setMetaDescription("Clears all entities from the list.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Add Entities");
        newOperationDefinition.setMetaDescription("This operations adds all of the supplied data entities to the list.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "The data entities to add to the list.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH);
        newOperationDefinition.setMetaDisplayName("Refresh");
        newOperationDefinition.setMetaDescription("This operation re-retrieves the data displayed by the list using the list prefilters.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REMOVE_SELECTED_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Remove Selected Data Entities");
        newOperationDefinition.setMetaDescription("This operation removes the selected data entities from the list.");
        operations.add(newOperationDefinition);

        return operations;
    }
}
