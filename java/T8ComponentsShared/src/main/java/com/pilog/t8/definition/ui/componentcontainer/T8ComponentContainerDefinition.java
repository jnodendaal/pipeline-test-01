package com.pilog.t8.definition.ui.componentcontainer;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentContainerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CONTAINER";
    public static final String IDENTIFIER_PREFIX = "C_CONTAINER_";
    public static final String DISPLAY_NAME = "Component Container";
    public static final String DESCRIPTION = "A container that holds one or more components.";
    public enum Datum
    {
        COMPONENT_LOADING_TEXT,
        CONTENT_COMPONENT_IDENTIFIER,
        CONTENT_COMPONENT_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    private T8ComponentDefinition contentComponentDefinition;

    public T8ComponentContainerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.COMPONENT_LOADING_TEXT.toString(), "Component Loading Text", "The text displayed on the progress indicator when a new component is loaded into the container."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONTENT_COMPONENT_IDENTIFIER.toString(), "Component Identifier", "The identifier of the component that will be loaded into the container during initialization."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CONTENT_COMPONENT_DEFINITIONS.toString(), "Components", "The predefined components contained by the container."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONTENT_COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String viewId;

        // Get the definition of the first component to load if set.
        viewId = getContentComponentIdentifier();
        configurationSettings = new HashMap<String, Object>();
        if ((viewId != null) && (viewId.length() > 0))
        {
            if (T8IdentifierUtilities.isPublicId(viewId))
            {
                T8DefinitionManager definitionManager;

                definitionManager = context.getServerContext().getDefinitionManager();
                contentComponentDefinition = (T8ComponentDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), viewId, inputParameters);
            }
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.componentcontainer.T8UIComponentContainer").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList)getContentComponentDefinitions();
    }

    public void addChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        getChildComponentDefinitions().add(componentDefinition);
    }

    public boolean removeChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        return getChildComponentDefinitions().remove(componentDefinition);
    }

    public void setContentComponentDefinition(T8ComponentDefinition definition)
    {
        contentComponentDefinition = definition;
    }

    public T8ComponentDefinition getContentComponentDefinition()
    {
        return contentComponentDefinition;
    }

    public String getContentComponentIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTENT_COMPONENT_IDENTIFIER.toString());
    }

    public void setContentComponentIdentifier(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.CONTENT_COMPONENT_IDENTIFIER.toString(), moduleIdentifier);
    }

    public String getComponentLoadingText()
    {
        return (String)getDefinitionDatum(Datum.COMPONENT_LOADING_TEXT.toString());
    }

    public void setComponentLoadingText(String title)
    {
        setDefinitionDatum(Datum.COMPONENT_LOADING_TEXT.toString(), title);
    }

    public List<T8ComponentDefinition> getContentComponentDefinitions()
    {
        return (List<T8ComponentDefinition>)getDefinitionDatum(Datum.CONTENT_COMPONENT_DEFINITIONS.toString());
    }

    public void setContentComponentDefinitions(List<T8ComponentDefinition> definitions)
    {
        setDefinitionDatum(Datum.CONTENT_COMPONENT_DEFINITIONS.toString(), definitions);
    }
}
