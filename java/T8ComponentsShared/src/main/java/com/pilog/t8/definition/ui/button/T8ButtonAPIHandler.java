package com.pilog.t8.definition.ui.button;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Button and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ButtonAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_BUTTON_PRESSED = "$CE_BUTTON_PRESSED";
    public static final String OPERATION_ENABLE_BUTTON = "$CO_ENABLE_BUTTON";
    public static final String OPERATION_DISABLE_BUTTON = "$CO_DISABLE_BUTTON";
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_SET_ICON = "$CO_SET_ICON";
    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_ICON_IDENTIFIER = "$P_ICON_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_BUTTON_PRESSED);
        newEventDefinition.setMetaDisplayName("Button Pressed");
        newEventDefinition.setMetaDescription("This event occurs when the button is pressed.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ENABLE_BUTTON);
        newOperationDefinition.setMetaDisplayName("Enable Button");
        newOperationDefinition.setMetaDescription("This operations enables the button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_BUTTON);
        newOperationDefinition.setMetaDisplayName("Disable Button");
        newOperationDefinition.setMetaDescription("This operations disables the button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Button Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed on the button.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the button.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_ICON);
        newOperationDefinition.setMetaDisplayName("Set Button Icon");
        newOperationDefinition.setMetaDescription("This operations sets the icon displayed on the button.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ICON_IDENTIFIER, "Icon Identifier", "The identifier of the icon to display on the button.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);
        
        return operations;
    }
}
