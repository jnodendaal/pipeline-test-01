package com.pilog.t8.definition.ui.filter.panel.component.cellrenderer;

/**
 * @author Bouwer du Preez
 */
public interface T8CellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_CELL_RENDERER_FILTER_PANEL";
    // -------- Definition Meta-Data -------- //
}
