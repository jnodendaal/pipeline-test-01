package com.pilog.t8.definition.ui.modulecontainer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentInterfaceOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleContainerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_CONTAINER";
    public static final String DISPLAY_NAME = "Module Container";
    public static final String DESCRIPTION = "A container that holds one or more module components.  The container acts as a relay between the parent- and sub-modules.";
    public enum Datum {INTERFACE_DEFINITION,
                       MODULE_INTERFACE_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //
    
    private List<T8ComponentEventDefinition> predefinedEventDefinitions;
    private List<T8ComponentOperationDefinition> predefinedOperationDefinitions;

    public T8ModuleContainerDefinition(String identifier)
    {
        super(identifier);
        predefinedEventDefinitions = T8ModuleContainerAPIHandler.getEventDefinitions(this);
        predefinedOperationDefinitions = T8ModuleContainerAPIHandler.getOperationDefinitions(this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.INTERFACE_DEFINITION.toString(), "Interface", "The interface to which all modules loaded into the container must comply."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MODULE_INTERFACE_MAPPING_DEFINITIONS.toString(), "Module Interface Mappings", "The mapping definitions of all module types that can be loaded into this container."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INTERFACE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ComponentInterfaceDefinition.TYPE_IDENTIFIER));
        else if (Datum.MODULE_INTERFACE_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ModuleInterfaceMappingDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.modulecontainer.T8ModuleContainer").getConstructor(T8ModuleContainerDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> definitions;

        // Get the pre-defined module event definitions.
        definitions = new ArrayList<T8ComponentEventDefinition>();
        if (predefinedEventDefinitions != null) definitions.addAll(predefinedEventDefinitions);
        
        // Add all user-defined module events.
        definitions.addAll(convertInterfaceEventDefinitions());
        return definitions;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> definitions;

        // Get the pre-defined module operation definitions.
        definitions = new ArrayList<T8ComponentOperationDefinition>();
        if (predefinedOperationDefinitions != null) definitions.addAll(predefinedOperationDefinitions);
        
        // Add the user-defined module operations.
        definitions.addAll(convertInterfaceOperationDefinitions());
        return definitions;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }
    
    public T8ComponentInterfaceDefinition getInterfaceDefinition()
    {
        return (T8ComponentInterfaceDefinition)getDefinitionDatum(Datum.INTERFACE_DEFINITION.toString());
    }
    
    public void setInterfaceDefinition(T8ComponentInterfaceDefinition definition)
    {
        setDefinitionDatum(Datum.INTERFACE_DEFINITION.toString(), definition);
    }
    
    public T8ModuleInterfaceMappingDefinition getModuleInterfaceMappingDefinitions(String identifier)
    {
        return (T8ModuleInterfaceMappingDefinition)getSubDefinition(identifier);
    }
    
    public List<T8ModuleInterfaceMappingDefinition> getModuleInterfaceMappingDefinitions()
    {
        return (List<T8ModuleInterfaceMappingDefinition>)getDefinitionDatum(Datum.MODULE_INTERFACE_MAPPING_DEFINITIONS.toString());
    }
    
    public void setModuleInterfaceMappingDefinitions(List<T8ModuleInterfaceMappingDefinition> interfaceDefinitions)
    {
        setDefinitionDatum(Datum.MODULE_INTERFACE_MAPPING_DEFINITIONS.toString(), interfaceDefinitions);
    }
    
    /**
     * Converts all interface event definitions to the more generic component event
     * type.
     *
     * @return A list of all interface events converted to component events.
     */
    private ArrayList<T8ComponentEventDefinition> convertInterfaceEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> eventDefinitions;
        List<T8ComponentInterfaceEventDefinition> interfaceEventDefinitions;
        T8ComponentInterfaceDefinition interfaceDefinition;

        eventDefinitions = new ArrayList<T8ComponentEventDefinition>();
        interfaceDefinition = getInterfaceDefinition();
        interfaceEventDefinitions = interfaceDefinition != null ? interfaceDefinition.getEventDefinitions() : null;

        if (interfaceEventDefinitions != null)
        {
            for (T8ComponentInterfaceEventDefinition interfaceEventDefinition : interfaceEventDefinitions)
            {
                T8ComponentEventDefinition eventCopy;

                // Copy the event definition.
                eventCopy = new T8ComponentEventDefinition(interfaceEventDefinition.getIdentifier());
                eventCopy.setMetaDisplayName(interfaceEventDefinition.getMetaDisplayName());
                eventCopy.setMetaDescription(interfaceEventDefinition.getMetaDescription());
                T8DefinitionUtilities.assignDefinitionParent(this, eventCopy);

                // Copy all event parameters to the new event.
                for (T8DataParameterDefinition eventParameterDefinition : interfaceEventDefinition.getParameterDefinitions())
                {
                    T8DataParameterResourceDefinition parameterCopy;

                    parameterCopy = new T8DataParameterResourceDefinition(eventParameterDefinition.getIdentifier());
                    parameterCopy.setMetaDisplayName(eventParameterDefinition.getMetaDisplayName());
                    parameterCopy.setMetaDescription(eventParameterDefinition.getMetaDescription());
                    parameterCopy.setDataType(eventParameterDefinition.getDataType());
                    eventCopy.addParameterDefinition(parameterCopy);
                }

                // Add the copied event to the list that will be returned.
                eventDefinitions.add(eventCopy);
            }
        }

        return eventDefinitions;
    }

    /**
     * Converts all interface operation definitions to the more generic
     * component operation type.
     *
     * @return A list of all interface operations converted to component
     * operations.
     */
    public ArrayList<T8ComponentOperationDefinition> convertInterfaceOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operationDefinitions;
        List<T8ComponentInterfaceOperationDefinition> interfaceOperationDefinitions;
        T8ComponentInterfaceDefinition interfaceDefinition;

        operationDefinitions = new ArrayList<T8ComponentOperationDefinition>();
        interfaceDefinition = getInterfaceDefinition();
        interfaceOperationDefinitions = interfaceDefinition != null ? interfaceDefinition.getOperationDefinitions() : null;

        if (interfaceOperationDefinitions != null)
        {
            for (T8ComponentInterfaceOperationDefinition interfaceOperationDefinition : interfaceOperationDefinitions)
            {
                T8ComponentOperationDefinition operationCopy;

                // Copy the operation definition.
                operationCopy = new T8ComponentOperationDefinition(interfaceOperationDefinition.getIdentifier());
                operationCopy.setMetaDisplayName(interfaceOperationDefinition.getMetaDisplayName());
                operationCopy.setMetaDescription(interfaceOperationDefinition.getMetaDescription());
                T8DefinitionUtilities.assignDefinitionParent(this, operationCopy);

                // Copy all operation input parameters to the new operation.
                for (T8DataParameterDefinition parameterDefinition : interfaceOperationDefinition.getInputParameterDefinitions())
                {
                    T8DataParameterResourceDefinition parameterCopy;

                    parameterCopy = new T8DataParameterResourceDefinition(parameterDefinition.getIdentifier());
                    parameterCopy.setMetaDisplayName(parameterDefinition.getMetaDisplayName());
                    parameterCopy.setMetaDescription(parameterDefinition.getMetaDescription());
                    parameterCopy.setDataType(parameterDefinition.getDataType());
                    operationCopy.addInputParameterDefinition(parameterCopy);
                }

                // Copy all operation output parameters to the new operation.
                for (T8DataParameterDefinition parameterDefinition : interfaceOperationDefinition.getOutputParameterDefinitions())
                {
                    T8DataParameterResourceDefinition parameterCopy;

                    parameterCopy = new T8DataParameterResourceDefinition(parameterDefinition.getIdentifier());
                    parameterCopy.setMetaDisplayName(parameterDefinition.getMetaDisplayName());
                    parameterCopy.setMetaDescription(parameterDefinition.getMetaDescription());
                    parameterCopy.setDataType(parameterDefinition.getDataType());
                    operationCopy.addOutputParameterDefinition(parameterCopy);
                }

                // Add the copied operation definition to the list that will be returned.
                operationDefinitions.add(operationCopy);
            }
        }

        return operationDefinitions;
    }
}
