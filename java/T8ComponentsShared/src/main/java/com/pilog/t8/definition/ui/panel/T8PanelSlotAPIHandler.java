package com.pilog.t8.definition.ui.panel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8PanelSlotAPIHandler
{
    // These should NOT be changed, only added to.

    public static final String OPERATION_SET_HEADER_TEXT = "$CO_SET_HEADER_TEXT";
    public static final String OPERATION_SET_SLOT_COMPONENT = "$CO_SET_SLOT_COMPONENT";
    public static final String PARAMETER_TEXT = "$P_TEXT";
    public static final String PARAMETER_COMPONENT_IDENTIFIER = "$P_COMPONENT_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_HEADER_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Header Text");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The new text to set for the header of the slot.", T8DataType.DISPLAY_STRING));
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SLOT_COMPONENT);
        newOperationDefinition.setMetaDisplayName("Set Slot Component");
        newOperationDefinition.setMetaDescription("Sets one of the component available in the current context as the new content of this slot.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COMPONENT_IDENTIFIER, "Component Identifier", "The identifier of the component to set as this slot's content.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
