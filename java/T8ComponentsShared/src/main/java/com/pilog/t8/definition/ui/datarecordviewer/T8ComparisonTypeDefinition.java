package com.pilog.t8.definition.ui.datarecordviewer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ComparisonTypeDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_DATA_RECORD_VIEWER_COMPARISON_TYPE";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Comparison Type";
    public static final String DESCRIPTION = "A type of grouping into which comparable records will be divided when loaded into the record viewer.";
    private enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8ComparisonTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8ComparisonType createComparisonType(T8ComponentController controller, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider) throws Exception;
}
