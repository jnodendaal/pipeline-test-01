package com.pilog.t8.definition.ui.modulecontainer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleInterfaceMappingDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_INTERFACE";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MODULE_INTERFACE";
    public static final String DISPLAY_NAME = "Module Interface Mapping";
    public static final String DESCRIPTION = "A mapping of the Module Container API to a specific module.";
    private enum Datum {MODULE_IDENTIFIER,
                        EVENT_MAPPING_DEFINITIONS,
                        OPERATION_MAPPING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ModuleInterfaceMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MODULE_IDENTIFIER.toString(), "Field Identifier", "The identifier of the data source field from which this column's data will be extracted."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.EVENT_MAPPING_DEFINITIONS.toString(), "Event Mappings", "The mappings of module events to the interface."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OPERATION_MAPPING_DEFINITIONS.toString(), "Operation Mappings", "The mappings of module the interface operations to the module."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MODULE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ModuleDefinition.GROUP_IDENTIFIER));
        else if (Datum.EVENT_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ModuleInterfaceEventMappingDefinition.TYPE_IDENTIFIER));
        else if (Datum.OPERATION_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ModuleInterfaceOperationMappingDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public String getModuleIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MODULE_IDENTIFIER.toString());
    }

    public void setModuleIdentifier(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.MODULE_IDENTIFIER.toString(), moduleIdentifier);
    }
    
    public List<T8ModuleInterfaceEventMappingDefinition> getEventMappingDefinitions()
    {
        return (List<T8ModuleInterfaceEventMappingDefinition>)getDefinitionDatum(Datum.EVENT_MAPPING_DEFINITIONS.toString());
    }
    
    public void setEventMappingDefinitions(List<T8ModuleInterfaceEventMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.EVENT_MAPPING_DEFINITIONS.toString(), definitions);
    }
    
    public List<T8ModuleInterfaceOperationMappingDefinition> getOperationMappingDefinitions()
    {
        return (List<T8ModuleInterfaceOperationMappingDefinition>)getDefinitionDatum(Datum.OPERATION_MAPPING_DEFINITIONS.toString());
    }
    
    public void setOperationMappingDefinitions(List<T8ModuleInterfaceOperationMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.OPERATION_MAPPING_DEFINITIONS.toString(), definitions);
    }
}
