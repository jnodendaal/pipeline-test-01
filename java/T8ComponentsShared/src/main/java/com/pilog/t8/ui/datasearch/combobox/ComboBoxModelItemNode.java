package com.pilog.t8.ui.datasearch.combobox;

/**
 * @author Hennie Brink
 */
public class ComboBoxModelItemNode implements ComboBoxModelNode
{
    private String itemName;
    private String description;
    private Object itemValue;
    private boolean selected;

    public ComboBoxModelItemNode()
    {
    }

    public ComboBoxModelItemNode(String itemName, Object itemValue, String description)
    {
        this.itemName = itemName;
        this.itemValue = itemValue;
        this.description = description;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public Object getItemValue()
    {
        return itemValue;
    }

    public void setItemValue(Object itemValue)
    {
        this.itemValue = itemValue;
    }

    @Override
    public String getDisplayString()
    {
        return this.getItemName();
    }

    @Override
    public boolean isSelected()
    {
       return this.selected;
    }

    @Override
    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
