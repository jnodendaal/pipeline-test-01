package com.pilog.t8.definition.ui.file.downloadlink;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8FileDownloadLink and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8FileDownloadLinkApiHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_SET_REMOTE_FILE_DETAILS = "$CO_SET_REMOTE_FILE_DETAILS";
    public static final String EVENT_FILE_DOWNLOADED = "$CE_FILE_DOWNLOADED";
    public static final String PARAMETER_FILE_CONTEXT_IID = "$P_FILE_CONTEXT_IID";
    public static final String PARAMETER_FILE_PATH = "$P_FILE_PATH";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        T8ComponentEventDefinition newEventDefinition;
        ArrayList<T8ComponentEventDefinition> events;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_FILE_DOWNLOADED);
        newEventDefinition.setMetaDisplayName("File Downloaded");
        newEventDefinition.setMetaDescription("This event occurs after the file pointed to by this link has been completely downloaded to the client.");
        events.add(newEventDefinition);
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_REMOTE_FILE_DETAILS);
        newOperationDefinition.setMetaDisplayName("Set File Details");
        newOperationDefinition.setMetaDescription("This operation sets the details of the file to which this download link is applicable.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_CONTEXT_IID, "Context Instance ID", "The instance id of the file context where the remote file is located.", T8DataType.GUID));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_PATH, "File Path", "The file path (if any) where the file is located in the remote context.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        return operations;
    }
}
