package com.pilog.t8.definition.ui.datarequirementeditor;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementEditorDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_REQUIREMENT_EDITOR";
    public static final String DISPLAY_NAME = "Data Requirement Editor";
    public static final String DESCRIPTION = "A component that allows editing and maintenance of data requirement documents.";
    public static final String VERSION = "0";
    public enum Datum {DATA_REQUIREMENT_TREE_DEFINITION,
                       DATA_REQUIREMENT_INSTANCE_TREE_DEFINITION,
                       RELATIONSHIP_TREE_DEFINITION,
                       DATA_REQUIREMENT_INSTANCE_LIST_DEFINITION,
                       ONTOLOGY_TREE_DEFINITION,
                       PRESCRIBED_DR_INSTANCE_TABLE_DEFINITION,
                       PREVIEW_RECORD_EDITOR_DEFINITION,
                       REQUIREMENT_DOCUMENT_DE_IDENTIFIER,
                       REQUIREMENT_DE_IDENTIFIER,
                       REQUIREMENT_INSTANCE_DE_IDENTIFIER,
                       TERMINOLOGY_DE_IDENTIFIER,
                       LANGUAGE_DE_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8DataRequirementEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REQUIREMENT_DOCUMENT_DE_IDENTIFIER.toString(), "Data Requirement Document Entity", "The data entity from which data requirement documents are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REQUIREMENT_DE_IDENTIFIER.toString(), "Data Requirement Entity", "The data entity from which the list of available data requirement documents and their titles are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString(), "Data Requirement Instance Entity", "The data entity from which the list of available data requirement instance documents and their titles are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TERMINOLOGY_DE_IDENTIFIER.toString(), "Terminology Entity", "The data entity from which ontological and terminological data is retrieved for concepts."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.LANGUAGE_DE_IDENTIFIER.toString(), "Language Entity", "The data entity from which ISO language data is retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DATA_REQUIREMENT_TREE_DEFINITION.toString(), "Data Requirement Tree", "The tree that is used to navigate the Data Requirements for the organization."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DATA_REQUIREMENT_INSTANCE_TREE_DEFINITION.toString(), "Data Requirement Instance Tree", "The tree that is used to navigate the Data Requirements Instances for the organization."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.RELATIONSHIP_TREE_DEFINITION.toString(), "Relationship Tree", "The tree that is used to navigate the ontological relationships available to the organization."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.ONTOLOGY_TREE_DEFINITION.toString(), "Ontology Tree", "The tree that is used to navigate the source ontology from which Data Requirements are composed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.PRESCRIBED_DR_INSTANCE_TABLE_DEFINITION.toString(), "Prescribed DR Instance Table", "The table that is used to display prescribed data requirement instances linked to a Document Reference type in a Data Requirement."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.PREVIEW_RECORD_EDITOR_DEFINITION.toString(), "Preview Record Editor", "The record editor to use when previewing data entry."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REQUIREMENT_DOCUMENT_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REQUIREMENT_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TERMINOLOGY_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.LANGUAGE_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_REQUIREMENT_TREE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TreeDefinition.TYPE_IDENTIFIER));
        else if (Datum.DATA_REQUIREMENT_INSTANCE_TREE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TreeDefinition.TYPE_IDENTIFIER));
        else if (Datum.RELATIONSHIP_TREE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TreeDefinition.TYPE_IDENTIFIER));
        else if (Datum.ONTOLOGY_TREE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TreeDefinition.TYPE_IDENTIFIER));
        else if (Datum.PRESCRIBED_DR_INSTANCE_TABLE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableDefinition.TYPE_IDENTIFIER));
        else if (Datum.PREVIEW_RECORD_EDITOR_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataRecordEditorDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditor").getConstructor(T8DataRequirementEditorDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataRequirementEditorAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataRequirementEditorAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public T8TreeDefinition getRelationshipTreeDefinition()
    {
        return (T8TreeDefinition)getDefinitionDatum(Datum.RELATIONSHIP_TREE_DEFINITION.toString());
    }

    public void setRelationshipTreeDefinition(T8TreeDefinition treeDefinition)
    {
        setDefinitionDatum(Datum.RELATIONSHIP_TREE_DEFINITION.toString(), treeDefinition);
    }

    public T8TreeDefinition getDataRequirementTreeDefinition()
    {
        return (T8TreeDefinition)getDefinitionDatum(Datum.DATA_REQUIREMENT_TREE_DEFINITION.toString());
    }

    public void setDataRequirementTreeDefinition(T8TreeDefinition treeDefinition)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_TREE_DEFINITION.toString(), treeDefinition);
    }

    public T8TreeDefinition getDataRequirementInstanceTreeDefinition()
    {
        return (T8TreeDefinition)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_TREE_DEFINITION.toString());
    }

    public void setDataRequirementInstanceTreeDefinition(T8TreeDefinition treeDefinition)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_TREE_DEFINITION.toString(), treeDefinition);
    }

    public T8TreeDefinition getOntologyTreeDefinition()
    {
        return (T8TreeDefinition)getDefinitionDatum(Datum.ONTOLOGY_TREE_DEFINITION.toString());
    }

    public void setOntologyTreeDefinition(T8TreeDefinition treeDefinition)
    {
        setDefinitionDatum(Datum.ONTOLOGY_TREE_DEFINITION.toString(), treeDefinition);
    }

    public T8TableDefinition getPrescribedDrInstanceTableDefinition()
    {
        return (T8TableDefinition)getDefinitionDatum(Datum.PRESCRIBED_DR_INSTANCE_TABLE_DEFINITION.toString());
    }

    public void setPrescribedDrInstanceTableDefinition(T8TableDefinition definition)
    {
        setDefinitionDatum(Datum.PRESCRIBED_DR_INSTANCE_TABLE_DEFINITION.toString(), definition);
    }

    public T8DataRecordEditorDefinition getPreviewRecordEditorDefinition()
    {
        return (T8DataRecordEditorDefinition)getDefinitionDatum(Datum.PREVIEW_RECORD_EDITOR_DEFINITION.toString());
    }

    public void setPreviewRecordEditorDefinition(T8DataRecordEditorDefinition definition)
    {
        setDefinitionDatum(Datum.PREVIEW_RECORD_EDITOR_DEFINITION.toString(), definition);
    }

    public String getDataRequirementDocumentDEID()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_DOCUMENT_DE_IDENTIFIER.toString());
    }

    public void setDataRequirementDocumentDEID(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_DOCUMENT_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRequirementDEID()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_DE_IDENTIFIER.toString());
    }

    public void setDataRequirementDEID(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRequirementInstanceDEID()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString());
    }

    public void setDataRequirementInstanceDEID(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString(), identifier);
    }

    public String getTerminologyDEID()
    {
        return (String)getDefinitionDatum(Datum.TERMINOLOGY_DE_IDENTIFIER.toString());
    }

    public void setTerminologyDEID(String identifier)
    {
        setDefinitionDatum(Datum.TERMINOLOGY_DE_IDENTIFIER.toString(), identifier);
    }

    public String getLanguageDEID()
    {
        return (String)getDefinitionDatum(Datum.LANGUAGE_DE_IDENTIFIER.toString());
    }

    public void setLanguageDEID(String identifier)
    {
        setDefinitionDatum(Datum.LANGUAGE_DE_IDENTIFIER.toString(), identifier);
    }
}
