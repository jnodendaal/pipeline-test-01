/**
 * Created on 10 Sep 2015, 8:08:52 AM
 *
 * Copyright(c) 2015 ShadowOfLies. All Rights Reserved.
 * The code from this class and all associated code, with the exception of third
 * party library code, is the proprietary information of ShadowOfLies.
 */
package com.pilog.t8.definition.ui.button.group;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @version 1
 * @author Gavin Boshoff
 */
public class T8ButtonGroupAPIHandler
{
    // These should NOT be changed, only added to.
    // Component event identifiers
    public static final String EVENT_SELECTION_CHANGED = "$CE_SELECTION_CHANGED";
    // Component peration identifiers
    public static final String OPERATION_ENABLE_GROUP = "$CO_ENABLE_GROUP";
    public static final String OPERATION_DISABLE_GROUP = "$CO_DISABLE_GROUP";
    public static final String OPERATION_SET_SELECTED = "$CO_SET_SELECTED";
    public static final String OPERATION_GET_SELECTED = "$CO_GET_SELECTED";
    // Component event/operation parameter identifiers
    public static final String PARAMETER_CHILD_IDENTIFIER = "$P_CHILD_IDENTIFIER";

    public static ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when one of the buttons are selected and the selection is different from the button previously selected.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CHILD_IDENTIFIER, "Child Identifier", "The identifier for the child button which was selected.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ENABLE_GROUP);
        newOperationDefinition.setMetaDisplayName("Enable Group");
        newOperationDefinition.setMetaDescription("This operations enables the button group.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_GROUP);
        newOperationDefinition.setMetaDisplayName("Disable Group");
        newOperationDefinition.setMetaDescription("This operations disables the button group.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED);
        newOperationDefinition.setMetaDisplayName("Set Selected");
        newOperationDefinition.setMetaDescription("Specifies the child button to be selected.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CHILD_IDENTIFIER, "Child Identifier", "The identifier for the button to be set as the selected item.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED);
        newOperationDefinition.setMetaDisplayName("Get Selected");
        newOperationDefinition.setMetaDescription("Specifies the child button that is currently selected.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CHILD_IDENTIFIER, "Child Identifier", "The identifier for the button that is currently the selected item.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}