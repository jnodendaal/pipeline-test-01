package com.pilog.t8.definition.ui.columnpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ColumnPaneDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_COLUMN_PANE";
    public static final String DISPLAY_NAME = "Column Pane";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  The container uses a column-based layout scheme where each of its content components is stacked into one of the available columns from top to bottom.";
    public static final String IDENTIFIER_PREFIX = "C_COLUMN_PANE_";
    public enum Datum {HORIZONTAL_SPACING,
                       VERTICAL_SPACING,
                       COLUMN_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8ColumnPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HORIZONTAL_SPACING.toString(), "Horizontal Spacing", "The horizontal spacing between columns.", 10));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.VERTICAL_SPACING.toString(), "Vertical Spacing", "The certical spacing between components in a column.", 10));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COLUMN_DEFINITIONS.toString(), "Columns", "The layout columns contained by this pane."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.COLUMN_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ColumnPaneColumnDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.columnpane.T8ColumnPane").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<T8ComponentEventDefinition>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<T8ComponentOperationDefinition>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;

        childComponentDefinitions = new ArrayList<T8ComponentDefinition>();
        for (T8ColumnPaneColumnDefinition columnDefinition : getColumnDefinitions())
        {
            if(columnDefinition.getComponentDefinitions() != null)childComponentDefinitions.addAll(columnDefinition.getComponentDefinitions());
        }

        return childComponentDefinitions;
    }

    public List<T8ColumnPaneColumnDefinition> getColumnDefinitions()
    {
        return (List<T8ColumnPaneColumnDefinition>)getDefinitionDatum(Datum.COLUMN_DEFINITIONS.toString());
    }

    public void setColumnDefinitions(List<T8ColumnPaneColumnDefinition> columnDefinitions)
    {
        setDefinitionDatum(Datum.COLUMN_DEFINITIONS.toString(), columnDefinitions);
    }

    public int getHorizontalSpacing()
    {
        Integer spacing;

        spacing = (Integer)getDefinitionDatum(Datum.HORIZONTAL_SPACING.toString());
        return spacing != null ? spacing : 0;
    }

    public void setHorizontalSpacing(int spacing)
    {
        setDefinitionDatum(Datum.HORIZONTAL_SPACING.toString(), spacing);
    }

    public int getVerticalSpacing()
    {
        Integer spacing;

        spacing = (Integer)getDefinitionDatum(Datum.VERTICAL_SPACING.toString());
        return spacing != null ? spacing : 0;
    }

    public void setVerticalSpacing(int spacing)
    {
        setDefinitionDatum(Datum.VERTICAL_SPACING.toString(), spacing);
    }
}
