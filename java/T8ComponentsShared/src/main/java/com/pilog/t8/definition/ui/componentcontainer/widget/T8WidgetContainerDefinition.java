package com.pilog.t8.definition.ui.componentcontainer.widget;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.functionality.group.T8FunctionalityGroupDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.columnpane.T8ColumnPaneDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8WidgetContainerDefinition extends T8ColumnPaneDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CONTAINER_WIDGET";
    public static final String DISPLAY_NAME = "Widget Container";
    public static final String DESCRIPTION = "A component container that can be used to display widgets that are linked to a specified functionality group.";
    public enum Datum
    {
        FUNCTIONALITY_GROUP_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //
    
    public T8WidgetContainerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString(), "Functionality Group Identifier", "The functionality group identifier that will be used to determine what functionalities will be displayed for the current role."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityGroupDefinition.GROUP_IDENTIFIER));
        else if (T8ColumnPaneDefinition.Datum.COLUMN_DEFINITIONS.toString().equals(datumIdentifier))  return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8WidgetContainerColumnDefinition.TYPE_IDENTIFIER));
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        
        constructor = Class.forName("com.pilog.t8.ui.componentcontainer.widget.T8WidgetContainer").getConstructor(T8WidgetContainerDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8WidgetContainerAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8WidgetContainerAPIHandler.getOperationDefinitions();
    }

    public String getFuntionalityGroupIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString());
    }

    public void setFuntionalityGroupIdentifier(String funtionalityGroupIdentifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_GROUP_IDENTIFIER.toString(), funtionalityGroupIdentifier);
    }
}
