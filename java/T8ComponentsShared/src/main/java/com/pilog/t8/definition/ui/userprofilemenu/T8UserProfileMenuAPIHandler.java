package com.pilog.t8.definition.ui.userprofilemenu;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8UserProfileSwitchMenu and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8UserProfileMenuAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_USER_PROFILE_SELECTED = "$CE_USER_PROFILE_SELECTED";
    public static final String PARAMETER_USER_PROFILE_IDENTIFIER = "$CP_USER_PROFILE_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_USER_PROFILE_SELECTED);
        newEventDefinition.setMetaDisplayName("User Profile Selected");
        newEventDefinition.setMetaDescription("This event occurs when a user profile is selected from the menu.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_USER_PROFILE_IDENTIFIER, "User Profile Identifier", "The identifier of the selected user profile.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());
        return operations;
    }
}
