package com.pilog.t8.definition.ui.table;

import com.pilog.t8.data.T8DataEntity;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8TableRowChange implements Serializable
{
    public static final int CHANGE_TYPE_INSERT = 0;
    public static final int CHANGE_TYPE_UPDATE = 1;
    public static final int CHANGE_TYPE_DELETE = 2;

    private final int changeType;
    private T8DataEntity oldData;
    private T8DataEntity newData;

    public T8TableRowChange(int changeType, T8DataEntity oldData, T8DataEntity newData)
    {
        this.changeType = changeType;
        this.oldData = oldData;
        this.newData = newData;
    }

    public int getChangeType()
    {
        return changeType;
    }

    public T8DataEntity getNewDataEntity()
    {
        return newData;
    }

    public T8DataEntity getOldDataEntity()
    {
        return oldData;
    }

    public void setNewDataEntity(T8DataEntity newData)
    {
        this.newData = newData;
    }

    public void setOldDataEntity(T8DataEntity oldData)
    {
        this.oldData = oldData;
    }
}
