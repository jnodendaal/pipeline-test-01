package com.pilog.t8.definition.ui.entityeditor.panel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8PanelDataEntityEditor and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_DATA_ENTITY_SAVED = "$CE_DATA_ENTITY_SAVED";

    public static final String OPERATION_SET_DATA_ENTITY_FIELD_VALUES = "$CO_SET_DATA_ENTITY_FIELD_VALUES";
    public static final String OPERATION_SET_DATA_ENTITY = "$CO_SET_DATA_ENTITY";
    public static final String OPERATION_GET_DATA_ENTITY = "$CO_GET_DATA_ENTITY";
    public static final String OPERATION_SAVE_DATA_ENTITY = "$CO_SAVE_DATA_ENTITY";
    public static final String OPERATION_PREVIOUS_DATA_ENTITY = "$CO_PREVIOUS_DATA_ENTITY";
    public static final String OPERATION_NEXT_DATA_ENTITY = "$CO_NEXT_DATA_ENTITY";
    public static final String OPERATION_COMMIT_CHANGES = "$CO_COMMIT_CHANGES";
    public static final String OPERATION_PREFILTER_BY_KEY = "$CO_PREFILTER_BY_KEY";
    public static final String OPERATION_SET_PREFILTER = "$CO_SET_PREFILTER";
    public static final String OPERATION_GET_PREFILTER = "$CO_GET_PREFILTER";
    public static final String OPERATION_SET_KEY = "$CO_SET_KEY";
    public static final String OPERATION_GET_KEY = "$CO_GET_KEY";
    public static final String OPERATION_GET_USER_DEFINED_FILTER = "$CO_GET_USER_DEFINED_FILTER";
    public static final String OPERATION_GET_COMBINED_FILTER = "$CO_GET_COMBINED_FILTER";
    public static final String OPERATION_REFRESH_DATA = "$CO_REFRESH_DATA";

    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";
    public static final String PARAMETER_KEY_MAP = "$P_KEY_MAP";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$P_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_REFRESH_DATA = "$P_REFRESH_DATA";
    public static final String PARAMETER_DATA_VALUE_MAP = "$P_DATA_VALUE_MAP";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DATA_ENTITY_SAVED);
        newEventDefinition.setMetaDisplayName("Data Entity Saved");
        newEventDefinition.setMetaDescription("This event occurs when an operation to save the current editor entity has completed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The entity that was saved.", T8DataType.DATA_ENTITY));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SAVE_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Save Data Entity");
        newOperationDefinition.setMetaDescription("This operations saves all changes to the current data entity.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_ENTITY_FIELD_VALUES);
        newOperationDefinition.setMetaDisplayName("Set Data Entity Field Values");
        newOperationDefinition.setMetaDescription("This operation sets field values on the Data Entity that is currently editable on the panel.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE_MAP, "Data Value Map", "A Map containing the values to set on the data entity.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the editor should refresh its data after the new entity values have been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Set Data Entity");
        newOperationDefinition.setMetaDescription("This operations sets the data entity displayed by and editable via the editor.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity to set on the editor.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Data Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently displayed by the editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity currently displayed by the editor.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_COMMIT_CHANGES);
        newOperationDefinition.setMetaDisplayName("Commit Changes");
        newOperationDefinition.setMetaDescription("This operations commits all changes on the entity editor and all of it's descendent editors to the current editor entity.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_PREVIOUS_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Previous Entity");
        newOperationDefinition.setMetaDescription("This operations loads the previous entity in the set, defined by the combined filter of the entity editor.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_NEXT_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Next Entity");
        newOperationDefinition.setMetaDescription("This operations loads the next entity in the set, defined by the combined filter of the entity editor.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_PREFILTER_BY_KEY);
        newOperationDefinition.setMetaDisplayName("Prefilter by Key");
        newOperationDefinition.setMetaDescription("This operation sets the editor's pre-filter according to the supplied key values.  The pre-filter is the base data filter used when retrieving entity data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values to add to the editor pre-filter.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the editor should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the editor's pre-filter.  The pre-filter is the base data filter used when retrieving entity data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the editor.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the editor that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the editor should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Get Prefilter");
        newOperationDefinition.setMetaDescription("This operation returns one of the prefilters currently set on the editor.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Filter Identifier", "The Identifier of the prefilter to fetch.", T8DataType.MAP));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Prefilter", "The specified data filter fetched from the editor.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_KEY);
        newOperationDefinition.setMetaDisplayName("Set Key");
        newOperationDefinition.setMetaDescription("This operation sets the editors's key values.  If not null, the editor will be filtered according to these key values and whenever any new entities are created by the editor the key values will automatically be added to the entity.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values to add to the editor pre-filter.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the editor should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_KEY);
        newOperationDefinition.setMetaDisplayName("Get Key");
        newOperationDefinition.setMetaDescription("Returns the editor's current key values.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values currently set on the editor.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_USER_DEFINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Get User Defined Filter");
        newOperationDefinition.setMetaDescription("This operation returns the data filter that is currently set on the editor by the user.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Filter", "The specified data filter fetched from the editor.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_COMBINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Get Combined Filter");
        newOperationDefinition.setMetaDescription("This operation returns the combined data filter (user-defined filter + prefilters) that is currently set on the editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Filter", "The specified data filter fetched from the editor.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH_DATA);
        newOperationDefinition.setMetaDisplayName("Refresh data");
        newOperationDefinition.setMetaDescription("Refreshes the data on the panel using the current filter.");
        operations.add(newOperationDefinition);

        return operations;
    }
}
