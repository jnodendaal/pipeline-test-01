package com.pilog.t8.definition.ui.notificationlist;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.list.T8ComponentListDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8NotificationListDefinition extends T8ComponentListDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_NOTIFICATION_LIST";
    public static final String DISPLAY_NAME = "Notification List";
    public static final String DESCRIPTION = "A component that displays a list of user notifications.";
    public enum Datum {NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition notificationBackgroundPainterDefinition;

    public T8NotificationListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Notification Background Painter", "The painter to use for painting the background of the notification in the notification list."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);

        String definitionId;

        definitionId = getNotificationBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            this.notificationBackgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.notificationlist.T8NotificationList", new Class<?>[]{T8NotificationListDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8NotificationListAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8NotificationListAPIHandler.getOperationDefinitions();
    }

    public T8PainterDefinition getTaskBackgroundPainterDefinition()
    {
        return this.notificationBackgroundPainterDefinition;
    }

    public String getNotificationBackgroundPainterIdentifier()
    {
        return getDefinitionDatum(Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER);
    }

    public void setNotificationBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.NOTIFICATION_BACKGROUND_PAINTER_IDENTIFIER, identifier);
    }
}
