package com.pilog.t8.ui.datasearch.combobox;

import com.pilog.t8.data.filter.T8DataFilter;

/**
 * @author Hennie Brink
 */
public interface T8DataSearchComboBoxModelProvider
{
    public ComboBoxModelRootNode createRootNode(String searchQuery, int retrievalSize, T8DataFilter dataSourceFilter) throws Exception;
}
