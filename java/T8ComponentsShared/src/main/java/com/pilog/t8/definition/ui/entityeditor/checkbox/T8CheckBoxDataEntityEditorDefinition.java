package com.pilog.t8.definition.ui.entityeditor.checkbox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.checkbox.T8CheckBoxDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityFieldEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8CheckBoxDataEntityEditorDefinition extends T8CheckBoxDefinition implements T8DataEntityFieldEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_ENTITY_EDITOR_CHECK_BOX";
    public static final String DISPLAY_NAME = "Check Box Data Entity Editor";
    public static final String DESCRIPTION = "An entity editor component that displays a check box that can be used for true and false values.";
    public enum Datum {PARENT_EDITOR_IDENTIFIER,
                       DATA_ENTITY_IDENTIFIER,
                       FIELD_IDENTIFIER,
                       SELECTED_VALUE_EXPRESSION,
                       DESELECTED_VALUE_EXPRESSION,
                       DEFAULT_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8CheckBoxDataEntityEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PARENT_EDITOR_IDENTIFIER.toString(), "Parent Data Entity Editor", "The data entity editor on which this editor is dependent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity to which this editor is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FIELD_IDENTIFIER.toString(), "Data Entity Field", "The data entity field that will be editable by this text field.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.SELECTED_VALUE_EXPRESSION.toString(), "Selected Value Expression", "An expression that evaluates to the value to use for this checkbox's selected state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DESELECTED_VALUE_EXPRESSION.toString(), "Deselected Value Expression", "An expression that evaluates to the value to use for this checkbox's deselected state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DEFAULT_VALUE_EXPRESSION.toString(), "Default Value Expression", "An expression that evaluates to the value to use for this checkbox's default/null state."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARENT_EDITOR_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8Definition> ancestors;
            ArrayList<T8DefinitionDatumOption> options;

            ancestors = getAncestorDefinitions();
            options = new ArrayList<>();
            for (T8Definition ancestor : ancestors)
            {
                if (ancestor instanceof T8DataEntityEditorDefinition)
                {
                    options.add(new T8DefinitionDatumOption(ancestor.getIdentifier(), ancestor.getIdentifier()));
                }
            }

            return options;
        }
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getEditorDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>();
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(Strings.isNullOrEmpty(getEditorDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(Strings.isNullOrEmpty(getEditorDataEntityFieldIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.FIELD_IDENTIFIER.toString(), "Data Entity Field Identifier Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    @Override
    public T8DataEntityEditorComponent getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.entityeditor.checkbox.T8CheckBoxDataEntityEditor", new Class<?>[]{T8CheckBoxDataEntityEditorDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8CheckBoxDataEntityEditorAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8CheckBoxDataEntityEditorAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString());
    }

    @Override
    public void setParentEditorIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString(), identifier);
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    @Override
    public void setEditorDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    @Override
    public String getEditorDataEntityFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FIELD_IDENTIFIER.toString());
    }

    @Override
    public void setEditorDataEntityFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getSelectedValueExpression()
    {
        return (String)getDefinitionDatum(Datum.SELECTED_VALUE_EXPRESSION.toString());
    }

    public void setSelectedValueExpression(String expression)
    {
        setDefinitionDatum(Datum.SELECTED_VALUE_EXPRESSION.toString(), expression);
    }

    public String getDeselectedValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DESELECTED_VALUE_EXPRESSION.toString());
    }

    public void setDeselectedValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DESELECTED_VALUE_EXPRESSION.name(), expression);
    }

    public String getDefaultValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString());
    }

    public void setDefaultValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.name(), expression);
    }

}
