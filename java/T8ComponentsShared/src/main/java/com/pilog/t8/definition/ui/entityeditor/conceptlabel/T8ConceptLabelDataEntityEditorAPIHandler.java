/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.ui.entityeditor.conceptlabel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.label.T8LabelAPIHandler;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8ConceptLabelDataEntityEditorAPIHandler 
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_SET_DATA_ENTITY = "$CO_SET_DATA_ENTITY";
    public static final String OPERATION_GET_DATA_ENTITY = "$CO_GET_DATA_ENTITY";
    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;

        events = new ArrayList<T8ComponentEventDefinition>(T8LabelAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8LabelAPIHandler.getOperationDefinitions());
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Set Data Entity");
        newOperationDefinition.setMetaDescription("This operations sets the data entity displayed by and editable via the editor.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity to set on the editor.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Data Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently displayed by the editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity currently displayed by the editor.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        return operations;
    }
}
