/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.ui.cellrenderer.table.value.string;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8ValueStringTableCellRendererDefinition extends T8ComponentDefinition implements T8TableCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_VALUE_STRING";
    public static final String DISPLAY_NAME = "Value String Cell Renderer";
    public static final String DESCRIPTION = "A cell editor that displays the content of a Value String cell as a simple text string.";

    public enum Datum
    {
        IS_SINGLE_CONCEPT_FIELD
    }
    // -------- Definition Meta-Data -------- //

    public T8ValueStringTableCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.IS_SINGLE_CONCEPT_FIELD.toString(), "Is Single Concept Field", "Select this option if the field value only contains a single concept value", false));
        return datumTypes;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.cellrenderer.table.value.string.T8ValueStringTableCellRenderer").getConstructor(this.getClass(), T8ComponentController.class);
            return (T8Component)constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public Boolean isSingleConceptField()
    {
        return (Boolean) getDefinitionDatum(Datum.IS_SINGLE_CONCEPT_FIELD.toString());
    }

    public void setIsSingleConceptField(boolean value)
    {
        setDefinitionDatum(Datum.IS_SINGLE_CONCEPT_FIELD.toString(), value);
    }
}
