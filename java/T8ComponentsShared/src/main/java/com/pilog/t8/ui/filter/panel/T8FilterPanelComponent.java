package com.pilog.t8.ui.filter.panel;

import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.security.T8Context;
import java.awt.Component;
import javax.swing.event.ChangeListener;

/**
 * @author hennie.brink@pilog.co.za
 */
public interface T8FilterPanelComponent
{
    T8DataFilterCriteria getFilterCriteria();
    void initialize(T8Context context) throws Exception;
    void setDataEntityIdentifier(String dataEntityIdentifier);
    Component getComponent();
    void addChangeListener(ChangeListener changeListener);
    void removeChangeListener(ChangeListener changeListener);
    void fireStateChanged();
    void refresh() throws Exception;
}
