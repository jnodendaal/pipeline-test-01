package com.pilog.t8.definition.ui.datarecordtable;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataRecordEditor and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataRecordTableAPIHandler implements T8DefinitionResource
{
    // Component Events.
    public static final String EVENT_RECORD_CHANGES_SAVED = "$CE_RECORD_CHANGES_SAVED";
    public static final String EVENT_ROW_SELECTION_CHANGED = "$CE_ROW_SELECTION_CHANGED";
    public static final String EVENT_DOUBLE_CLICKED = "$CE_DOUBLE_CLICKED";

    // Component Operations.
    public static final String OPERATION_GET_RECORD_ID = "$CO_GET_RECORD_ID";
    public static final String OPERATION_SAVE_RECORD_CHANGES = "$CO_SAVE_RECORD_CHANGES";
    public static final String OPERATION_LOAD_RECORD = "$CO_LOAD_RECORD";
    public static final String OPERATION_CREATE_NEW_RECORD = "$CO_CREATE_NEW_RECORD";
    public static final String OPERATION_HAS_UNSAVED_CHANGES = "$CO_HAS_UNSAVED_CHANGES";
    public static final String OPERATION_VALIDATE_PREFILTERED_RECORDS = "$CO_VALIDATE_PREFILTERED_RECORDS";
    public static final String OPERATION_GET_FIRST_SELECTED_DATA_RECORD = "$CO_GET_FIRST_SELECTED_DATA_RECORD";
    public static final String OPERATION_GET_SELECTED_DATA_RECORDS = "$CO_GET_SELECTED_DATA_RECORDS";
    public static final String OPERATION_PREFILTER_BY_KEY = "$CO_PREFILTER_BY_KEY";
    public static final String OPERATION_SET_PREFILTER = "$CO_SET_PREFILTER";
    public static final String OPERATION_GET_PREFILTER = "$CO_GET_PREFILTER";
    public static final String OPERATION_GET_USER_DEFINED_FILTER = "$CO_GET_USER_DEFINED_FILTER";
    public static final String OPERATION_GET_COMBINED_FILTER = "$CO_GET_COMBINED_FILTER";
    public static final String OPERATION_REFRESH_DATA = "$CO_REFRESH_DATA";

    // Server operations.
    public static final String SERVER_OPERATION_COUNT_DATA_RECORDS = "@OS_DATA_RECORD_TABLE_COUNT_DATA_RECORDS";
    public static final String SERVER_OPERATION_RETRIEVE_DATA_RECORDS = "@OS_DATA_RECORD_TABLE_RETRIEVE_DATA_RECORDS";
    public static final String SERVER_OPERATION_COMMIT_DATA_RECORD_CHANGES = "@OS_DATA_RECORD_TABLE_COMMIT_DATA_RECORD_CHANGES";
    public static final String SERVER_OPERATION_VALIDATE_FILTERED_DATA_RECORDS = "@OS_DATA_RECORD_TABLE_VALIDATE_FILTERED_DATA_RECORDS";

    // Parameters.
    public static final String PARAMETER_RECORD_ID = "$P_RECORD_ID";
    public static final String PARAMETER_DR_INSTANCE_ID = "$P_DR_INSTANCE_ID";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_HAS_UNSAVED_CHANGES = "$P_HAS_UNSAVED_CHANGES";
    public static final String PARAMETER_KEY_MAP = "$P_KEY_MAP";
    public static final String PARAMETER_REFRESH_DATA = "$P_REFRESH_DATA";
    public static final String PARAMETER_ODT_ID = "$P_ODT_ID";
    public static final String PARAMETER_DATA_RECORD = "$P_DATA_RECORD";
    public static final String PARAMETER_DATA_RECORD_COUNT = "$P_DATA_RECORD_COUNT";
    public static final String PARAMETER_DATA_RECORD_HANDLE_LIST = "$P_DATA_RECORD_HANDLE_LIST";
    public static final String PARAMETER_DATA_RECORD_LIST = "$P_DATA_RECORD_LIST";
    public static final String PARAMETER_DATA_RECORD_CHANGE_LIST = "$P_DATA_RECORD_CHANGE_LIST";
    public static final String PARAMETER_DATA_RECORD_DETAILS_ENTITY_IDENTIFIER = "$P_DATA_RECORD_DETAILS_ENTITY_IDENTIFIER";
    public static final String PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER = "$P_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER";
    public static final String PARAMETER_DATA_RECORD_STRUCTURE_ENTITY_IDENTIFIER = "$P_DATA_RECORD_STRUCTURE_ENTITY_IDENTIFIER";
    public static final String PARAMETER_PARENT_DATA_RECORD_ID_LIST = "$P_PARENT_DATA_RECORD_ID_LIST";
    public static final String PARAMETER_TERMINOLOGY_ENTITY_IDENTIFIER = "$P_TERMINOLOGY_IDENTIFIER";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_DESCRIPTION_CLASSIFICATION_ID = "$P_DESCRIPTION_CLASSIFICATION_ID";
    public static final String PARAMETER_CLIENT_VALIDATION_SCRIPT_IDENTIFIER_LIST = "$P_CLIENT_VALIDATION_SCRIPT_IDENTIFIER_LIST";
    public static final String PARAMETER_SERVER_VALIDATION_SCRIPT_IDENTIFIER_LIST = "$P_SERVER_VALIDATION_SCRIPT_IDENTIFIER_LIST";
    public static final String PARAMETER_DESCRIPTION_ENTITY_IDENTIFIER = "$P_DESCRIPTION_ENTITY_IDENTIFIER";
    public static final String PARAMETER_DESCRIPTION_RENDERER_IDENTIFIER = "$P_DESCRIPTION_RENDERER_ID";
    public static final String PARAMETER_DESCRIPTION_RENDERER_SERVICE_IDENTIFIER = "$P_DESCRIPTION_RENDERER_SERVICE_ID";
    public static final String PARAMETER_DESCRIPTION_HANDLE_LIST = "$P_DESCRIPTION_HANDLE_LIST";
    public static final String PARAMETER_SUB_RECORD_DESCRIPTION_HANDLE_LIST = "$P_SUB_RECORD_DESCRIPTION_HANDLE_LIST";
    public static final String PARAMETER_CONCEPT_TERMINOLOGY_LIST = "$P_CONCEPT_TERMINOLOGY_LIST";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$P_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";
    public static final String PARAMETER_DATA_RECORD_VALIDATION_REPORT_LIST = "$P_DATA_RECORD_VALIDATION_REPORT_LIST";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();

            // Data Record Operations.
            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_COUNT_DATA_RECORDS);
            definition.setMetaDescription("Count Data Record");
            definition.setClassName("com.pilog.t8.ui.datarecordtable.T8DataRecordTableOperations$CountDataRecordsOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_RETRIEVE_DATA_RECORDS);
            definition.setMetaDescription("Retrieve Data Record");
            definition.setClassName("com.pilog.t8.ui.datarecordtable.T8DataRecordTableOperations$RetrieveDataRecordsOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_COMMIT_DATA_RECORD_CHANGES);
            definition.setMetaDescription("Commit Data Record Changes");
            definition.setClassName("com.pilog.t8.ui.datarecordtable.T8DataRecordTableOperations$CommitDataRecordChangesOperation");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "A boolean value indicating whether or not the changes were successfully committed.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_RECORD_VALIDATION_REPORT_LIST, "Data Record Validation Report List", "A List of validation errors (if any) that prevent the successful completion of the operation.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_VALIDATE_FILTERED_DATA_RECORDS);
            definition.setMetaDescription("Validate Filtered Data Records");
            definition.setClassName("com.pilog.t8.ui.datarecordtable.T8DataRecordTableOperations$ValidateFilteredDataRecordsOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER, "Data Record Document Entity Identifier", "The identifier of the entity from which Data Record documents are retrieved.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SERVER_VALIDATION_SCRIPT_IDENTIFIER_LIST, "Validation Script Identifiers", "A list of the identifiers of all validation scripts that will be used for the validation of the data records.", new T8DtList(T8DataType.DEFINITION_IDENTIFIER)));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The Data Filter that will be used to filter records to be validated.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "A boolean value indicating whether or not the changes were successfully committed.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_RECORD_VALIDATION_REPORT_LIST, "Data Record Validation Report List", "A List of validation errors (if any) that prevent the successful completion of the operation.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_RECORD_CHANGES_SAVED);
        newEventDefinition.setMetaDisplayName("Record Changes Saved");
        newEventDefinition.setMetaDescription("This event occurs when the user successfully saves changes to the current record.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data Record ID", "The unique identifier of the data record to which changes were saved.", T8DataType.STRING));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_ROW_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Table Row Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selection of rows in the table changes.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD_LIST, "Data Records", "A List of Data Records selected.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DOUBLE_CLICKED);
        newEventDefinition.setMetaDisplayName("Double Clicked");
        newEventDefinition.setMetaDescription("This event occurs when the double clicks on a selection of data in the table (only when clicking on non-editable cells).");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD_LIST, "Data Records", "A List of Data Records selected when the double clicked occurred.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_RECORD_ID);
        newOperationDefinition.setMetaDisplayName("Get Record ID");
        newOperationDefinition.setMetaDescription("This operation returns the ID of the record currently loaded in the record editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data Record ID", "The unique identifier of the data record currently loaded in the record editor.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SAVE_RECORD_CHANGES);
        newOperationDefinition.setMetaDisplayName("Save Record Changes");
        newOperationDefinition.setMetaDescription("This operation saves all oustanding changes to the record currently loaded in the record editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "A boolean value indicating if record changes were successfully saved.  A false value may be the result of invalid data or an operation failure.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_RECORD);
        newOperationDefinition.setMetaDisplayName("Load Record");
        newOperationDefinition.setMetaDescription("This operation loads the specified data record.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data Record ID", "The unique identifier of the data record to load.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CREATE_NEW_RECORD);
        newOperationDefinition.setMetaDisplayName("Create New Record");
        newOperationDefinition.setMetaDescription("This operation creates a new data record using the specified data requirement.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_INSTANCE_ID, "Data Requirement Instance ID", "The unique identifier of the data requirement instance from which the new record will be created.", T8DataType.GUID));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data Record ID", "The unique identifier to assign to the newly created record.  If this parameter is not supplied a new system generated ID will be assigned to the created record.", T8DataType.GUID));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_PREFILTER_BY_KEY);
        newOperationDefinition.setMetaDisplayName("Prefilter by Key");
        newOperationDefinition.setMetaDescription("This operation sets the table's pre-filter according to the supplied key values.  The pre-filter is the base data filter used when retrieving table data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_KEY_MAP, "Key Map", "A Map containing all of the key values to add to the table pre-filter.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the table's pre-filter.  The pre-filter is the base data filter used when retrieving table data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the table.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the table that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Get Prefilter");
        newOperationDefinition.setMetaDescription("This operation returns one of the prefilters currently set on the table.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Filter Identifier", "The Identifier of the prefilter to fetch.", T8DataType.MAP));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Prefilter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_USER_DEFINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Get User Defined Filter");
        newOperationDefinition.setMetaDescription("This operation returns the data filter that is currently set on the table by the user.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Filter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_COMBINED_FILTER);
        newOperationDefinition.setMetaDisplayName("Get Combined Filter");
        newOperationDefinition.setMetaDescription("This operation returns the combined data filter (user-defined filter + prefilters) that is currently set on the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Filter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_FIRST_SELECTED_DATA_RECORD);
        newOperationDefinition.setMetaDisplayName("Get First Selected Data Record");
        newOperationDefinition.setMetaDescription("This operation returns the first selected Data Record in the table.  If no Records are selected a null value is returned.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD, "Data Record", "The first selected Data Record in the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_RECORDS);
        newOperationDefinition.setMetaDisplayName("Get Selected Data Records");
        newOperationDefinition.setMetaDescription("This operation returns a list of all the selected Data Records in the table.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD_LIST, "Data Records", "A List of the selected Data Record documents.", T8DataType.LIST));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH_DATA);
        newOperationDefinition.setMetaDisplayName("Refresh");
        newOperationDefinition.setMetaDescription("This operation re-retrieves the data displayed by the table.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_VALIDATE_PREFILTERED_RECORDS);
        newOperationDefinition.setMetaDisplayName("Validate Prefiltered Records");
        newOperationDefinition.setMetaDescription("This operation validates all of the records that are filtered using the combined collection of prefilters on the table (user-defined filter excluded).");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Filter Identifier", "The Identifier of the prefilter to fetch.", T8DataType.MAP));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "A boolean value indicating successful validation (and validity) of all filtered records.  A false value may be the result of invalid data or an operation failure.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataValueDefinition> inputParameters;

        inputParameters = new ArrayList<T8DataValueDefinition>();

        return inputParameters;
    }
}
