package com.pilog.t8.definition.ui.datacombobox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataComboBox and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataComboBoxAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGE = "$CE_SELECTION_CHANGE";

    public static final String OPERATION_GET_SELECTED_DATA_ENTITY = "$CO_GET_SELECTED_DATA_ENTITY";
    public static final String OPERATION_SET_SELECTED_DATA_ENTITY = "$CO_SET_SELECTED_DATA_ENTITY";
    public static final String OPERATION_SET_PREFILTER = "$CO_SET_PREFILTER";

    public static final String PARAMETER_DATA_ENTITY = "$CP_DATA_ENTITY";
    public static final String PARAMETER_DATA_VALUE_MAP = "$P_DATA_VALUE_MAP";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$P_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_REFRESH_DATA = "$P_REFRESH_DATA";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGE);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selected entity has changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The currently selected data entity of the combo box", T8DataType.DATA_ENTITY));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Selected Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently selected and displayed by the component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The selected data entity.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Set Selected Entity");
        newOperationDefinition.setMetaDescription("This operations sets the selected data entity in the combo box.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE_MAP, "Data Value Map", "A Map containing the key values to use for entity identification.", T8DataType.MAP));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets a pre-filter on the combobox.  The pre-filter is the base data filter used when retrieving list data.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set on the combobox.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the pre-filter being set.  The existing pre-filter on the combobox using this identifier will be replaced (if any).", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the combobox should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
