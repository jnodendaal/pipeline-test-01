package com.pilog.t8.definition.ui.document.datarecord.search;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataDataRecordSearch and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataRecordSearchAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SEARCH_FILTER_CRITERIA_UPDATED = "$CE_SEARCH_FILTER_CRITERIA_UPDATED";
    public static final String OPERATION_SET_DATA_FILE_STRUCTURE = "$CO_SET_DATA_FILE_STRUCTURE";
    public static final String OPERATION_LOAD_DATA_FILE_STRUCTURE = "$CO_LOAD_DATA_FILE_STRUCTURE";
    public static final String OPERATION_SET_SEARCH_FILTER_CRITERIA = "$CO_SET_SEARCH_FILTER_CRITERIA";
    public static final String OPERATION_GET_SEARCH_FILTER_CRITERIA = "$CO_GET_SEARCH_FILTER_CRITERIA";
    public static final String OPERATION_CLEAR_FILTER_VALUES = "$CO_CLEAR_FILTER_VALUES";
    public static final String OPERATION_SET_ADVANCED_OPTIONS_VISIBLE = "$CO_SET_ADVANCED_OPTIONS_VISIBLE";
    public static final String PARAMETER_SEARCH_FILTER_CRITERIA = "$P_SEARCH_FILTER_CRITERIA";
    public static final String PARAMETER_DATA_FILE_STRUCTURE = "$P_DATA_FILE_STRUCTURE";
    public static final String PARAMETER_DATA_FILE_STRUCTURE_ID = "$P_DATA_FILE_STRUCTURE_ID";
    public static final String PARAMETER_SET_ADVANCED_OPTIONS_VISIBLE = "$P_SET_ADVANCED_OPTIONS_VISIBLE";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SEARCH_FILTER_CRITERIA_UPDATED);
        newEventDefinition.setMetaDisplayName("Search Filter Updated");
        newEventDefinition.setMetaDescription("This event occurs when the user alters/removes/adds a search criterion.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH_FILTER_CRITERIA, "Search Filter", "The new search filter.", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SEARCH_FILTER_CRITERIA);
        newOperationDefinition.setMetaDisplayName("Set Search Filter");
        newOperationDefinition.setMetaDescription("This operation sets a set of filter criteria on the search panel.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH_FILTER_CRITERIA, "Search Filter", "The search filter to set on the search panel.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SEARCH_FILTER_CRITERIA);
        newOperationDefinition.setMetaDisplayName("Get Search Filter Criteria");
        newOperationDefinition.setMetaDescription("This operation returns the set of filter criteria currently entered on the search panel.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH_FILTER_CRITERIA, "Search Filter Criteria", "The search filter entered into the search panel.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_FILTER_VALUES);
        newOperationDefinition.setMetaDisplayName("Clear Filter Values");
        newOperationDefinition.setMetaDescription("This operation clears the filter values entered into the search criteria panel.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_FILE_STRUCTURE);
        newOperationDefinition.setMetaDisplayName("Set Data File Structure");
        newOperationDefinition.setMetaDescription("This operation sets the data file structure to be searched on.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILE_STRUCTURE, "Data File Structure", "The data file structure to be searched.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_DATA_FILE_STRUCTURE);
        newOperationDefinition.setMetaDisplayName("Load Data File Structure");
        newOperationDefinition.setMetaDescription("This operation loads the data file structure to be searched on.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILE_STRUCTURE_ID, "Data File Structure ID", "The ID of the data file structure to be searched.", T8DataType.GUID));
        operations.add(newOperationDefinition);
        
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_ADVANCED_OPTIONS_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Visibility Of The Advanced Options");
        newOperationDefinition.setMetaDescription("This operation will set the advanced options visible or not visible, depending on the input paramater.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SET_ADVANCED_OPTIONS_VISIBLE, "Set Visibility Of The Advanced Options", "A boolean flag to display whether to set the advanced options visible, or not visible.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);
        
        return operations;
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataValueDefinition> inputParameters;

        inputParameters = new ArrayList<T8DataValueDefinition>();
        return inputParameters;
    }
}
