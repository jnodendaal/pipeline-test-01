/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.ui.table.export;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public interface T8TableExporter
{
    public void export();
}
