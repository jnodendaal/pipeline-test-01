package com.pilog.t8.definition.ui.entitymenu;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataEntityMenuItem and responds to specific requests 
 * regarding these definitions.  It is extremely important that only new 
 * definitions are added to this class and none of the old definitions removed 
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataEntityMenuItemAPIHandler
{
    public static ArrayList<T8ComponentEventDefinition> events;
    public static ArrayList<T8ComponentOperationDefinition> operations;

    // These should NOT be changed, only added to.
    public static final String EVENT_MENU_ITEM_SELECTED = "$CE_MENU_ITEM_SELECTED";
    public static final String OPERATION_DISABLE_MENU_ITEM = "$CO_DISABLE_MENU_ITEM";
    public static final String PARAMETER_MENU_ITEM_IDENTIFIER = "$P_MENU_ITEM_IDENTIFIER";
    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";

    // Setup of all event definitions.
    static
    {
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_MENU_ITEM_SELECTED);
        newEventDefinition.setMetaDisplayName("Menu Item Selected");
        newEventDefinition.setMetaDescription("This event occurs when the menu item is selected.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MENU_ITEM_IDENTIFIER, "Menu Item Identifier", "The identifier of the selected menu item.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The data entity to which the menu item is applicable.", T8DataType.DATA_ENTITY));
        events.add(newEventDefinition);
    }

    // Setup of all operation definitions.
    static
    {
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_MENU_ITEM);
        newOperationDefinition.setMetaDisplayName("Disable Menu Item");
        newOperationDefinition.setMetaDescription("This operation disables the menu item.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MENU_ITEM_IDENTIFIER, "Menu Item Identifier", "The identifier of the menu item to disable.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return operations;
    }
}
