package com.pilog.t8.definition.ui.datarecordviewer;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataRecordViewer and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataRecordViewerAPIHandler
{
    // Component events.
    public static final String EVENT_RECORD_CHANGES_SAVED = "$CE_RECORD_CHANGES_SAVED";

    // Component operations.
    public static final String OPERATION_LOAD_RECORDS = "$CO_LOAD_RECORDS";
    public static final String OPERATION_LOAD_ROOT_RECORD_HISTORY_SNAPSHOTS = "$CO_LOAD_ROOT_RECORD_HISTORY_SNAPSHOTS";

    // Parameters.
    public static final String PARAMETER_CLEAR_CURRENT_RECORDS = "$P_CLEAR_CURRENT_RECORDS";
    public static final String PARAMETER_RECORD_ID_LIST = "$P_RECORD_ID_LIST";
    public static final String PARAMETER_DATA_FILE_HEADER_MAP = "$P_DATA_FILE_HEADER_MAP";
    public static final String PARAMETER_DR_INSTANCE_ID = "$P_DR_INSTANCE_ID";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_ROOT_RECORD_ID = "$P_ROOT_RECORD_ID";
    public static final String PARAMETER_TRANSACTION_ID_LIST = "$P_TRANSACTION_ID_LIST";
    public static final String PARAMETER_INCLUDE_CURRENT_RECORD = "$P_INCLUDE_CURRENT_RECORD";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return new ArrayList<>();
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_RECORDS);
        newOperationDefinition.setMetaDisplayName("Load Record");
        newOperationDefinition.setMetaDescription("This operation loads the specified data record.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID_LIST, "Data Record ID List", "The list of identifiers of the data records to load.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILE_HEADER_MAP, "Data File Header Map", "A map containing the ID's of each Data File added to the viewer and the column headers mapped to each.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CLEAR_CURRENT_RECORDS, "Clear Current Records", "A boolean flag to indicate whether or not current records in the viewer should be cleared before adding the newly loaded records (default = true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_ROOT_RECORD_HISTORY_SNAPSHOTS);
        newOperationDefinition.setMetaDisplayName("Load Root Record History Snapshots");
        newOperationDefinition.setMetaDescription("This operation loads the history snapshots for the specified root record.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CLEAR_CURRENT_RECORDS, "Clear Current Records", "A boolean flag to indicate whether or not current records in the viewer should be cleared before adding the newly loaded records (default = true).", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ROOT_RECORD_ID, "Root Record ID", "The record ID of the root record for which the history will be retrieved.", T8DataType.GUID));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TRANSACTION_ID_LIST, "Transaction ID List", "The list of transaction ID's for which history will be retrieved, if no values are given the full history across all transactions will be retrieved", new T8DtList(T8DataType.GUID)));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_INCLUDE_CURRENT_RECORD, "Include Current Transaction in Results", "If the current record as it is should be included in the Results for the viewer", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        return new ArrayList<>();
    }
}
