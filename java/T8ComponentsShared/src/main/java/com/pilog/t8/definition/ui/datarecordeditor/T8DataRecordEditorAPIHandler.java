package com.pilog.t8.definition.ui.datarecordeditor;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtDataRecord;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataRecordEditor and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorAPIHandler
{
    // Component events.
    public static final String EVENT_RECORD_CHANGES_SAVED = "$CE_RECORD_CHANGES_SAVED";

    // Component operations.
    public static final String OPERATION_GET_ROOT_DATA_RECORD = "$CO_GET_ROOT_DATA_RECORD";
    public static final String OPERATION_GET_RECORD_ID = "$CO_GET_RECORD_ID";
    public static final String OPERATION_SAVE_RECORD_CHANGES = "$CO_SAVE_RECORD_CHANGES";
    public static final String OPERATION_LOAD_RECORD = "$CO_LOAD_RECORD";
    public static final String OPERATION_CREATE_NEW_RECORD = "$CO_CREATE_NEW_RECORD";
    public static final String OPERATION_ADD_RECORD = "$CO_ADD_RECORD";
    public static final String OPERATION_CLEAR = "$CO_CLEAR";
    public static final String OPERATION_HAS_UNSAVED_CHANGES = "$CO_HAS_UNSAVED_CHANGES";

    // Parameters.
    public static final String PARAMETER_DATA_RECORD = "$P_DATA_RECORD";
    public static final String PARAMETER_RECORD_ID = "$CP_RECORD_ID";
    public static final String PARAMETER_OPEN_RECORD_ID = "$P_OPEN_RECORD_ID";
    public static final String PARAMETER_ACCESS_IDENTIFIER = "$CP_ACCESS_IDENTIFIER";
    public static final String PARAMETER_DR_INSTANCE_ID = "$CP_DR_INSTANCE_ID";
    public static final String PARAMETER_SUCCESS = "$CP_SUCCESS";
    public static final String PARAMETER_ACCESS_VALIDATION_LEVEL = "$CP_ACCESS_VALIDATION_LEVEL";
    public static final String PARAMETER_HAS_UNSAVED_CHANGES = "$CP_HAS_UNSAVED_CHANGES";
    public static final String PARAMETER_SUPPRESS_TOAST = "$CP_SUPPRESS_TOAST";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_RECORD_CHANGES_SAVED);
        newEventDefinition.setMetaDisplayName("Record Changes Saved");
        newEventDefinition.setMetaDescription("This event occurs when the user successfully saves changes to the current record.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_ROOT_DATA_RECORD);
        newOperationDefinition.setMetaDisplayName("Get Root Data Record");
        newOperationDefinition.setMetaDescription("This operation returns the current root data record from the record editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD, "Data Record", "The root data record currently being edited in the record editor.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_RECORD_ID);
        newOperationDefinition.setMetaDisplayName("Get Record ID");
        newOperationDefinition.setMetaDescription("This operation returns the ID of the record currently loaded in the record editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data Record ID", "The unique identifier of the data record currently loaded in the record editor.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SAVE_RECORD_CHANGES);
        newOperationDefinition.setMetaDisplayName("Save Record Changes");
        newOperationDefinition.setMetaDescription("This operation saves all oustanding changes to the record currently loaded in the record editor.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ACCESS_VALIDATION_LEVEL, "Access Validation Level", "The level of access validation that must be performed before the record changes are saved (Options=SAVE/FINAL).", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUPPRESS_TOAST, "Suppress Toast", "If true, the toast will not be displayed when the record is successfully saved.", T8DataType.BOOLEAN, true));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SUCCESS, "Success", "A boolean value indicating if record changes were successfully saved.  A false value may be the result of invalid data or an operation failure.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_HAS_UNSAVED_CHANGES);
        newOperationDefinition.setMetaDisplayName("Has Unsaved Changes");
        newOperationDefinition.setMetaDescription("This operation checks all loaded documents for unsaved changes and returns a Boolea value indicating the result.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_HAS_UNSAVED_CHANGES, "Has Unsaved Changes", "A boolean value indicating whether or not the record editor session contains unsaved changes.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_LOAD_RECORD);
        newOperationDefinition.setMetaDisplayName("Load Record");
        newOperationDefinition.setMetaDescription("This operation loads the specified data record.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data File ID", "The unique identifier of the data record to load.", T8DataType.GUID));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_OPEN_RECORD_ID, "Open Record ID", "The unique identifier of the data record to open, once the data file has been loaded.", T8DataType.GUID));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ACCESS_IDENTIFIER, "Access Identifier", "The access Identifier to use when retrieving the specified record (if null, the default for the editor will be used).", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD, "Data Record", "The data record retrieved and loaded into the record editor.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CREATE_NEW_RECORD);
        newOperationDefinition.setMetaDisplayName("Create New Record");
        newOperationDefinition.setMetaDescription("This operation creates a new data record using the specified data requirement.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_INSTANCE_ID, "Data Requirement Instance ID", "The unique identifier of the data requirement instance from which the new record will be created.", T8DataType.GUID));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID, "Data Record ID", "The unique identifier to assign to the newly created record.  If this parameter is not supplied a new system generated ID will be assigned to the created record.", T8DataType.GUID));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD, "Data Record", "The newly created data record.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_RECORD);
        newOperationDefinition.setMetaDisplayName("Add Record");
        newOperationDefinition.setMetaDescription("This operation adds the specified record to the editor.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD, "Data Record", "The data record to add to the editor.", new T8DtDataRecord()));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR);
        newOperationDefinition.setMetaDisplayName("Clear");
        newOperationDefinition.setMetaDescription("Removes all records from the editor.");
        operations.add(newOperationDefinition);

        return operations;
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        return new ArrayList<>();
    }
}
