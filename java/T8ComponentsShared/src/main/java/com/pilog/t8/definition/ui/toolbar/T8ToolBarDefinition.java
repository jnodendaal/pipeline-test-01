package com.pilog.t8.definition.ui.toolbar;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ToolBarDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TOOL_BAR";
    public static final String IDENTIFIER_PREFIX = "C_TOOL_BAR_";
    public static final String DISPLAY_NAME = "Tool Bar";
    public static final String DESCRIPTION = "A UI container to which other UI components can be added.  The contained components are organized using a flow layout.";
    public enum Datum {ALIGNMENT,
                       VERTICAL_INSET,
                       HORIZONTAL_INSET,
                       BACKGROUND_PAINTER_IDENTIFIER,
                       TOOL_BAR_COMPONENT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public enum FlowAlignment {LEFT, CENTER, RIGHT};

    private T8PainterDefinition backgroundPainterDefinition;

    public T8ToolBarDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ALIGNMENT.toString(), "Alignment", "The alignment of the component within the flow layout.", FlowAlignment.CENTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.VERTICAL_INSET.toString(), "Vertical Inset", "The inset (in pixels) to add to the top and bottom of content components within the flow layout.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.HORIZONTAL_INSET.toString(), "Horizontal Inset", "The inset (in pixels) to add to the left and right of content components within the flow layout.", 1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting this tool bar's backgorund."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.TOOL_BAR_COMPONENT_DEFINITIONS.toString(), "Tool Bar Components", "The components contained by this tool bar."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TOOL_BAR_COMPONENT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.ALIGNMENT.toString().equals(datumIdentifier)) return createStringOptions(FlowAlignment.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        backgroundPainterId = getBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.ui.toolbar.T8ToolBar", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<>(getToolBarComponentDefinitions());
    }

    public FlowAlignment getAlignment()
    {
        return FlowAlignment.valueOf(getDefinitionDatum(Datum.ALIGNMENT));
    }

    public void setAlignment(FlowAlignment alignment)
    {
        setDefinitionDatum(Datum.ALIGNMENT, alignment.toString());
    }

    public int getVerticalInset()
    {
        return getDefinitionDatum(Datum.VERTICAL_INSET);
    }

    public void setVerticalInset(int verticalInset)
    {
        setDefinitionDatum(Datum.VERTICAL_INSET, verticalInset);
    }

    public int getHorizontalInset()
    {
        return getDefinitionDatum(Datum.HORIZONTAL_INSET);
    }

    public void setHorizontalInset(int horizontalInset)
    {
        setDefinitionDatum(Datum.HORIZONTAL_INSET, horizontalInset);
    }

    public List<T8ComponentDefinition> getToolBarComponentDefinitions()
    {
        return getDefinitionDatum(Datum.TOOL_BAR_COMPONENT_DEFINITIONS);
    }

    public void setToolBarComponentDefinitions(List<T8ComponentDefinition> componentDefinitions)
    {
        setDefinitionDatum(Datum.TOOL_BAR_COMPONENT_DEFINITIONS, componentDefinitions);
    }

    public String getBackgroundPainterIdentifier()
    {
        return getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER);
    }

    public void setBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER, title);
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }
}
