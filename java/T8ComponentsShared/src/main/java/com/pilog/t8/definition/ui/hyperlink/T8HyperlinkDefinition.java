package com.pilog.t8.definition.ui.hyperlink;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8HyperlinkDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_HYPERLINK";
    public static final String DISPLAY_NAME = "Hyperlink";
    public static final String DESCRIPTION = "A component that displays a web-page.";
    public static final String IDENTIFIER_PREFIX = "C_HYPERLINK_";
    public enum Datum {TEXT,
                       URI};
    // -------- Definition Meta-Data -------- //

    public T8HyperlinkDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text displayed on the button."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.URI.toString(), "URI", "The URI to which the hyperlink points (optional)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.hyperlink.T8Hyperlink").getConstructor(T8HyperlinkDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8HyperlinkAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8HyperlinkAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String buttonText)
    {
        setDefinitionDatum(Datum.TEXT.toString(), buttonText);
    }

    public String getURI()
    {
        return (String)getDefinitionDatum(Datum.URI.toString());
    }

    public void setURL(String url)
    {
        setDefinitionDatum(Datum.URI.toString(), url);
    }
}
