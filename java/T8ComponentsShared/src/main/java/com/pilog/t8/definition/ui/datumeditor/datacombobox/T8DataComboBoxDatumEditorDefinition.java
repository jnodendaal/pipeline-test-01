package com.pilog.t8.definition.ui.datumeditor.datacombobox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxDefinition;
import com.pilog.t8.definition.ui.datumeditor.T8DefinitionDatumEditorDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxDatumEditorDefinition extends T8DataComboBoxDefinition implements T8DefinitionDatumEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATUM_EDITOR_DATA_COMBO_BOX";
    public static final String DISPLAY_NAME = "Data Combo Box Datum Editor";
    public static final String DESCRIPTION = "A datum editor that uses a combobox to allow the user to select one value from a prefiltered collection of data.";
    public enum Datum {DATUM_IDENTIFIER,
                       TARGET_ENTITY_IDENTIFIER,
                       EDITOR_INPUT_FIELD_MAPPING,
                       EDITOR_OUTPUT_FIELD_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8DataComboBoxDatumEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATUM_IDENTIFIER.toString(), "Datum", "The datum to which this editor applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_ENTITY_IDENTIFIER.toString(), "Target Entity", "The data entity that will be modified/acted on by this cell editor."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.EDITOR_INPUT_FIELD_MAPPING.toString(), "Field Mapping:  Target to Editor Input", "A mapping of Flow Parameters to the corresponding Task Input Parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.EDITOR_OUTPUT_FIELD_MAPPING.toString(), "Field Mapping:  Editor Output to Target", "A mapping of Task Output Parameters to the corresponding Flow Parameters."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TARGET_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.EDITOR_OUTPUT_FIELD_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getTargetEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition targetEntityDefinition;
                T8DataEntityDefinition editorEntityDefinition;
                String projectId;

                projectId = getRootProjectId();
                targetEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, entityId);
                editorEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, getDataEntityIdentifier());

                if ((targetEntityDefinition != null) && (editorEntityDefinition != null))
                {
                    List<T8DataEntityFieldDefinition> editorFieldDefinitions;
                    List<T8DataEntityFieldDefinition> targetFieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    editorFieldDefinitions = editorEntityDefinition.getFieldDefinitions();
                    targetFieldDefinitions = targetEntityDefinition.getFieldDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((editorFieldDefinitions != null) && (targetFieldDefinitions != null))
                    {
                        for (T8DataEntityFieldDefinition editorFieldDefinition : editorFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataEntityFieldDefinition targetFieldDefinition : targetFieldDefinitions)
                            {
                                identifierList.add(targetFieldDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(editorFieldDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.EDITOR_INPUT_FIELD_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getTargetEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition targetEntityDefinition;
                T8DataEntityDefinition editorEntityDefinition;
                String projectId;

                projectId = getRootProjectId();
                targetEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, entityId);
                editorEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, getDataEntityIdentifier());

                if ((targetEntityDefinition != null) && (editorEntityDefinition != null))
                {
                    List<T8DataEntityFieldDefinition> editorFieldDefinitions;
                    List<T8DataEntityFieldDefinition> targetFieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    editorFieldDefinitions = editorEntityDefinition.getFieldDefinitions();
                    targetFieldDefinitions = targetEntityDefinition.getFieldDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((editorFieldDefinitions != null) && (targetFieldDefinitions != null))
                    {
                        for (T8DataEntityFieldDefinition targetFieldDefinition : targetFieldDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataEntityFieldDefinition editorFieldDefinition : editorFieldDefinitions)
                            {
                                identifierList.add(editorFieldDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(targetFieldDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.datacombobox.T8DataComboBox").getConstructor(T8DataComboBoxDefinition.class, T8ComponentController.class);
            return (T8Component)constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DefinitionDatumEditor getNewDatumEditorInstance(T8DefinitionContext definitionContext, T8Definition targetDefinition, T8DefinitionDatumType datumType)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.datumeditor.datacombobox.T8DataComboBoxDatumEditor").getConstructor(T8DefinitionContext.class, T8DataComboBoxDatumEditorDefinition.class, T8Definition.class, T8DefinitionDatumType.class);
            return (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, targetDefinition, datumType);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getDatumIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATUM_IDENTIFIER.toString());
    }

    public void setDatumIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATUM_IDENTIFIER.toString(), identifier);
    }

    public String getTargetEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER.toString());
    }

    public void setTargetEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public Map<String, String> getEditorInputFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.EDITOR_INPUT_FIELD_MAPPING.toString());
    }

    public void setEditorInputFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.EDITOR_INPUT_FIELD_MAPPING.toString(), mapping);
    }

    public Map<String, String> getEditorOutputFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.EDITOR_OUTPUT_FIELD_MAPPING.toString());
    }

    public void setEditorOutputFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.EDITOR_OUTPUT_FIELD_MAPPING.toString(), mapping);
    }
}
