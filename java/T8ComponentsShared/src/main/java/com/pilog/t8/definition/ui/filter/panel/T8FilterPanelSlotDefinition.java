/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.ui.filter.panel;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.ui.filter.panel.component.T8FilterPanelComponentDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelSlotDefinition extends T8PanelSlotDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_FILTER_PANEL_SLOT";
    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_SLOT";
    public static final String DISPLAY_NAME = "Filter Panel Slot";
    public static final String DESCRIPTION = "Filter Panel Slot";
    public static final String IDENTIFIER_PREFIX = "C_FP_SLOT_";
    public static final String VERSION = "0";
// -------- Definition Meta-Data -------- //
    public T8FilterPanelSlotDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8PanelSlotDefinition.Datum.SLOT_COMPONENT_DEFINITION.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8FilterPanelComponentDefinition.GROUP_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }
}
