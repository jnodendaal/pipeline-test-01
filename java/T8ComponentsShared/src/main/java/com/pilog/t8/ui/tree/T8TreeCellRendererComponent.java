package com.pilog.t8.ui.tree;

import com.pilog.t8.ui.T8Component;

/**
 * @author Bouwer du Preez
 */
public interface T8TreeCellRendererComponent extends T8Component
{
    public void setRendererNode(T8TreeCellRenderableComponent sourceComponent, T8TreeNode node, boolean selected, boolean focused, boolean editable, boolean expanded, boolean leaf, int row, int currentPage, int pageCount);
}
