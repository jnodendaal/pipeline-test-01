package com.pilog.t8.definition.ui.datepicker;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author Gavin Boshoff
 */
public class T8DatePickerApiHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_VALUE_CHANGED = "$CE_VALUE_CHANGED";
    public static final String EVENT_FOCUS_GAINED = "$CE_FOCUS_GAINED";
    public static final String EVENT_FOCUS_LOST = "$CE_FOCUS_LOST";

    public static final String OPERATION_SET_VALUE = "$CO_SET_VALUE";
    public static final String OPERATION_GET_VALUE = "$CO_GET_VALUE";
    public static final String OPERATION_SET_EDITABLE = "$CO_SET_EDITABLE";

    public static final String PARAMETER_VALUE = "$P_VALUE";
    public static final String PARAMETER_EDITABLE = "$P_EDITABLE";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_VALUE_CHANGED);
        newEventDefinition.setMetaDisplayName("Value Changed");
        newEventDefinition.setMetaDescription("This event occurs every time the selected value is changed changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The selected value (milliseconds since 1970-01-01).", T8DataType.LONG));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FOCUS_GAINED);
        newEventDefinition.setMetaDisplayName("Focus Gained");
        newEventDefinition.setMetaDescription("This event occurs every time the date picker gains focus.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The selected value (milliseconds since 1970-01-01).", T8DataType.LONG));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_FOCUS_LOST);
        newEventDefinition.setMetaDisplayName("Focus Lost");
        newEventDefinition.setMetaDescription("This event occurs every time the date picker loses focus.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The selected value (milliseconds since 1970-01-01).", T8DataType.LONG));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_VALUE);
        newOperationDefinition.setMetaDisplayName("Set Value");
        newOperationDefinition.setMetaDescription("This operations sets the value on the date picker.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The value to set (milliseconds since 1970-01-01).", T8DataType.LONG));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_VALUE);
        newOperationDefinition.setMetaDisplayName("Get Value");
        newOperationDefinition.setMetaDescription("This operations returns the cuurently selected value on the date picker.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The selected value (milliseconds since 1970-01-01).", T8DataType.LONG));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_EDITABLE);
        newOperationDefinition.setMetaDisplayName("Sets component Editable");
        newOperationDefinition.setMetaDescription("This operation sets the components editable status.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_EDITABLE, "Editable", "If this text component should be editable.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
