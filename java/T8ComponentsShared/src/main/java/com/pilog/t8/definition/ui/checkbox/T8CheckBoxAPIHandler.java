package com.pilog.t8.definition.ui.checkbox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8CheckBox and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8CheckBoxAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_STATE_CHANGED = "$CE_STATE_CHANGED";
    public static final String OPERATION_SELECT = "$CO_SELECT";
    public static final String OPERATION_DESELECT = "$CO_DESELECT";
    public static final String OPERATION_SET_SELECTED = "$CO_SET_SELECTED";
    public static final String OPERATION_IS_SELECTED = "$CO_IS_SELECTED";
    public static final String PARAMETER_SELECTED = "$CP_SELECTED";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_STATE_CHANGED);
        newEventDefinition.setMetaDisplayName("State Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selected state of the checkbox changes.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED, "Selected", "The boolean value specifying the selection state to set on the check box.", T8DataType.BOOLEAN));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SELECT);
        newOperationDefinition.setMetaDisplayName("Select");
        newOperationDefinition.setMetaDescription("This operation sets the selection state of the checkbox to true (checked).");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DESELECT);
        newOperationDefinition.setMetaDisplayName("Deselect");
        newOperationDefinition.setMetaDescription("This operation sets the selection state of the checkbox to false (unchecked).");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED);
        newOperationDefinition.setMetaDisplayName("Set Selection");
        newOperationDefinition.setMetaDescription("This operation sets the selection state of the checkbox according to the supplied boolean parameter.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED, "Selected", "The boolean value specifying the selection state to set on the check box.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_IS_SELECTED);
        newOperationDefinition.setMetaDisplayName("Is Selected");
        newOperationDefinition.setMetaDescription("This operation returns the current selection state of the checkbox as a boolean value.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SELECTED, "Selected", "The boolean value indicating the current selection state of the checkbox.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
