package com.pilog.t8.definition.ui.celleditor.table.datepicker;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.celleditor.T8TableCellEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DatePickerTableCellEditorDefinition extends T8ComponentDefinition implements T8TableCellEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_EDITOR_DATE_PICKER";
    public static final String DISPLAY_NAME = "Date Picker Cell Editor";
    public static final String DESCRIPTION = "A cell editor that allows for the selection of a data from a calendar-like view.";
    public static final String VERSION = "0";
    private enum Datum {DATE_FORMAT,
                        UNDERLYING_TYPE};
    // -------- Definition Meta-Data -------- //

    public enum UnderlyingType {TIMESTAMP, EPOCH_STRING;};

    public T8DatePickerTableCellEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATE_FORMAT.toString(), "Date Format", "The format of the date that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.UNDERLYING_TYPE.toString(), "Underlying Data Type", "The underlying data type for the cell value.", UnderlyingType.TIMESTAMP.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.UNDERLYING_TYPE.toString().equals(datumIdentifier)) return createStringOptions(UnderlyingType.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.celleditor.table.datepicker.T8DatePickerTableCellEditor", new Class<?>[]{T8DatePickerTableCellEditorDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getDateFormat()
    {
        return getDefinitionDatum(Datum.DATE_FORMAT);
    }

    public void setDateFormat(String format)
    {
        setDefinitionDatum(Datum.DATE_FORMAT, format);
    }

    public UnderlyingType getUnderlyingDataType()
    {
        return UnderlyingType.valueOf(getDefinitionDatum(Datum.UNDERLYING_TYPE));
    }

    public void setUnderlyingDataType(UnderlyingType type)
    {
        setDefinitionDatum(Datum.UNDERLYING_TYPE, type);
    }
}
