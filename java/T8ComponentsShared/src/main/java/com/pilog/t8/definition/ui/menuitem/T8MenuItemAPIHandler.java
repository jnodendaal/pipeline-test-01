package com.pilog.t8.definition.ui.menuitem;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8MenuItem and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8MenuItemAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_MENU_ITEM_SELECTED = "$CE_MENU_ITEM_SELECTED";
    public static final String OPERATION_DISABLE_MENU_ITEM = "$CO_DISABLE_MENU_ITEM";
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";

    public static final String PARAMETER_MENU_ITEM_IDENTIFIER = "$CP_MENU_ITEM_IDENTIFIER";
    public static final String PARAMETER_TEXT = "$CP_TEXT";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_MENU_ITEM_SELECTED);
        newEventDefinition.setMetaDisplayName("Menu Item Selected");
        newEventDefinition.setMetaDescription("This event occurs when the menu item is selected.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MENU_ITEM_IDENTIFIER, "Menu Item Identifier", "The identifier of the selected menu item.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_MENU_ITEM);
        newOperationDefinition.setMetaDisplayName("Disable Menu Item");
        newOperationDefinition.setMetaDescription("This operation disables the menu item.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Menu Item Text");
        newOperationDefinition.setMetaDescription("This operation sets the text for the menu item.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text for the menu item.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        return operations;
    }
}
