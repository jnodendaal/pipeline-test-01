package com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBoxModelProvider;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxDataSourceFixedListDefinition extends T8DataSearchComboBoxDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_FIXED_LIST";
    public static final String DISPLAY_NAME = "Fixed List Data Source";
    public static final String DESCRIPTION = "An fixed list data source that will retrieve the data source items from a T8 data entity";
    public enum Datum
    {
        GROUP_NODE_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8DataSearchComboBoxDataSourceFixedListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.GROUP_NODE_DEFINITIONS.toString(), "Group Node Definitions", "The group node definitions"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.GROUP_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DataSearchComboBoxModelProvider createModelProvider(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datasearch.combobox.server.T8DataRecordSearchComboBoxModelProviderFixedList", new Class<?>[]{T8Context.class, T8DataSearchComboBoxDataSourceFixedListDefinition.class}, context, this);
    }

    public List<T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition> getGroupNodeDefinitions()
    {
        return (List<T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition>)getDefinitionDatum(Datum.GROUP_NODE_DEFINITIONS.toString());
    }

    public void setGroupNodeDefinitions(List<T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition> definitions)
    {
        setDefinitionDatum(Datum.GROUP_NODE_DEFINITIONS.toString(), definitions);
    }
}
