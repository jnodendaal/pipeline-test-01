package com.pilog.t8.definition.ui.popupmenu;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.menu.T8MenuDefinition;
import com.pilog.t8.definition.ui.menuitem.T8MenuItemDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PopupMenuDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_POPUP_MENU";
    public static final String DISPLAY_NAME = "Popup Menu";
    public static final String DESCRIPTION = "A popup menu to which menu items or sub-menus can be added.";
    public static final String IDENTIFIER_PREFIX = "C_POPUP_MENU_";
    public enum Datum {MENU_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8PopupMenuDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MENU_DEFINITIONS.toString(), "Menus", "The menus contained by this popup menu."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MENU_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8MenuItemDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(getMenuDefinitions() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.MENU_DEFINITIONS.toString(), "No Menu Definitions defined.", T8DefinitionValidationError.ErrorType.WARNING));

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.popupmenu.T8PopupMenu", new Class<?>[]{T8PopupMenuDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8PopupMenuAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8PopupMenuAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public ArrayList<T8MenuDefinition> getMenuDefinitions()
    {
        return (ArrayList<T8MenuDefinition>)getDefinitionDatum(Datum.MENU_DEFINITIONS.toString());
    }

    public void setMenuDefinitions(ArrayList<T8MenuDefinition> menuDefinitions)
    {
        getMenuDefinitions().clear();
        getMenuDefinitions().addAll(menuDefinitions);
    }

    public T8MenuDefinition getMenuDefinition(String identifier)
    {
        for (T8MenuDefinition menuDefinition : getMenuDefinitions())
        {
            if (menuDefinition.getIdentifier().equals(identifier))
            {
                return menuDefinition;
            }
        }

        return null;
    }
}
