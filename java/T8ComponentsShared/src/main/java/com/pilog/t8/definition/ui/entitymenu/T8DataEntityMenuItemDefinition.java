package com.pilog.t8.definition.ui.entitymenu;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityMenuItemDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_ENTITY_MENU_ITEM";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_ENTITY_MENU_ITEM";
    public static final String DISPLAY_NAME = "Data Entity Menu Item";
    public static final String DESCRIPTION = "A selectable menu item component that can be added to a menu.";
    public static final String IDENTIFIER_PREFIX = "C_ENTITY_MENU_ITEM_";
    public enum Datum {TEXT,
                       VISIBLE_EXPRESSION,
                       ENABLED_EXPRESSION,
                       TEXT_EXPRESSION,
                       ICON_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;

    public T8DataEntityMenuItemDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text to display on the menu item."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.TEXT_EXPRESSION.toString(), "Text Expression", "The expression that will be evaluated on the context data entity in order to determine the text of this menu item.  If this datum is null, the default text for the menu item will be displayed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.VISIBLE_EXPRESSION.toString(), "Visibility Expression", "The expression that will be evaluated on the context data entity in order to determine if this menu item is visible or not."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_EXPRESSION.toString(), "Enabled Expression", "The expression that will be evaluated on the context data entity in order to determine if this menu item is enabled or not."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this menu item."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.entitymenu.T8DataEntityMenuItem").getConstructor(T8DataEntityMenuItemDefinition.class, T8ComponentController.class);
            return (T8Component)constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataEntityMenuItemAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataEntityMenuItemAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT.toString(), text);
    }

    public String getTextExpression()
    {
        return (String)getDefinitionDatum(Datum.TEXT_EXPRESSION.toString());
    }

    public void setTextExpression(String expression)
    {
        setDefinitionDatum(Datum.TEXT_EXPRESSION.toString(), expression);
    }

    public String getVisibleExpression()
    {
        return (String)getDefinitionDatum(Datum.VISIBLE_EXPRESSION.toString());
    }

    public void setVisibleExpression(String expression)
    {
        setDefinitionDatum(Datum.VISIBLE_EXPRESSION.toString(), expression);
    }

    public String getEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.ENABLED_EXPRESSION.toString());
    }

    public void setEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_EXPRESSION.toString(), expression);
    }

    public String getIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }
}
