package com.pilog.t8.definition.ui.componentcontainer.widget;

import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.ui.columnpane.T8ColumnPaneColumnDefinition;
import java.util.ArrayList;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8WidgetContainerColumnDefinition extends T8ColumnPaneColumnDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CONTAINER_WIDGET_COLUMN";
    // -------- Definition Meta-Data -------- //

    public T8WidgetContainerColumnDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;
        T8DefinitionDatumType componentDefinitions= null;

        datumTypes = super.getDatumTypes();

        for (T8DefinitionDatumType t8DefinitionDatumType : datumTypes)
        {
            if(t8DefinitionDatumType.getIdentifier().equals(T8ColumnPaneColumnDefinition.Datum.COMPONENT_DEFINITIONS.toString()))
            {
                componentDefinitions = t8DefinitionDatumType;
                break;
            }
        }

        datumTypes.remove(componentDefinitions);

        return datumTypes;
    }
}
