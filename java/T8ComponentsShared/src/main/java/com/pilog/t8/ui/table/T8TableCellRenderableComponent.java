package com.pilog.t8.ui.table;

import com.pilog.t8.ui.T8CellRenderableComponent;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8TableCellRenderableComponent extends T8CellRenderableComponent
{
    public Map<String, Object> getRowEntityFieldValues(int row);
    public Object getAdditionalRenderData(int row, String fieldIdentifier);
}
