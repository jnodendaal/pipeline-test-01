package com.pilog.t8.definition.ui.panel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PanelSlotDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_PANEL_SLOT";
    public static final String DISPLAY_NAME = "Panel Slot";
    public static final String DESCRIPTION = "A layout slot that is added to a panel container and occupies a specific area of the available are in the container.  Any UI component can then be added to the slot.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_PANEL_SLOT_";
    public enum Datum {TITLE,
                       BORDER_TYPE,
                       COLLAPSIBLE,
                       SLOT_COMPONENT_DEFINITION,
                       ANCHOR,
                       FILL,
                       GRID_X,
                       GRID_Y,
                       GRID_WIDTH,
                       GRID_HEIGHT,
                       WEIGHT_X,
                       WEIGHT_Y,
                       PADDING_X,
                       PADDING_Y,
                       INSET_TOP,
                       INSET_BOTTOM,
                       INSET_LEFT,
                       INSET_RIGHT,
                       MINIMUM_WIDTH,
                       MINIMUM_HEIGHT,
                       PREFERRED_WIDTH,
                       PREFERRED_HEIGHT};
    // -------- Definition Meta-Data -------- //

    public enum BorderType {NONE, BEVEL, ETCHED, LINE, TITLED, SHADOW, CONTENT};

    public enum Fill
    {
        NONE(GridBagConstraints.NONE),
        BOTH(GridBagConstraints.BOTH),
        HORIZONTAL(GridBagConstraints.HORIZONTAL),
        VERTICAL(GridBagConstraints.VERTICAL);

        private int code;

        Fill(int code)
        {
            this.code = code;
        }

        public int getCode()
        {
            return code;
        }

        public static Fill valueOf(int code)
        {
            for (Fill fill : Fill.values())
            {
                if (fill.getCode() == code) return fill;
            }

            return null;
        }
    };

    public enum Anchor
    {
        NORTH(GridBagConstraints.NORTH),
        NORTHEAST(GridBagConstraints.NORTHEAST),
        EAST(GridBagConstraints.EAST),
        SOUTHEAST(GridBagConstraints.SOUTHEAST),
        SOUTH(GridBagConstraints.SOUTH),
        SOUTHWEST(GridBagConstraints.SOUTHWEST),
        WEST(GridBagConstraints.WEST),
        NORTHWEST(GridBagConstraints.NORTHWEST),
        CENTER(GridBagConstraints.CENTER);

        private int code;

        Anchor(int code)
        {
            this.code = code;
        }

        public int getCode()
        {
            return code;
        }

        public static Anchor valueOf(int code)
        {
            for (Anchor anchor : Anchor.values())
            {
                if (anchor.getCode() == code) return anchor;
            }

            return null;
        }
    };

    public T8PanelSlotDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TITLE.toString(), "Header Text", "The display title of the slot (only used if a border is applied)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BORDER_TYPE.toString(), "Border Type", "The border type to use for this panel slot.", BorderType.NONE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COLLAPSIBLE.toString(), "Collapsible", "If this boolean flag is set and the content header border is used, the border can be clicked to collapse this panel slot.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SLOT_COMPONENT_DEFINITION.toString(), "Slot Component", "The component contained by this slot."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ANCHOR.toString(), "Anchor", "The direction of the anchor point for this constraint.  If more space is available than required, the content will be anchored to this point of the available area.", Anchor.CENTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FILL.toString(), "Fill", "Indicates whether the content should be stretched to fill the available area.", Fill.BOTH.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.GRID_X.toString(), "Grid X", "The x-coordinate of the constraint within the layout grid.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.GRID_Y.toString(), "Grid Y", "The y-coordinate of the constraint within the layout grid.", GridBagConstraints.RELATIVE));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.GRID_WIDTH.toString(), "Grid Width", "The number of layout columns taken up in width by the content.", 1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.GRID_HEIGHT.toString(), "Grid Height", "The number of layout rows taken up in height by the content.", 1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.WEIGHT_X.toString(), "Weight X", "The x-weight specifies the relative amount of available horizontal space that should be assigned to content.", 0.1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DOUBLE, Datum.WEIGHT_Y.toString(), "Weight Y", "The y-weight specifies the relative amount of available vertical space that should be assigned to content.", 0.1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PADDING_X.toString(), "Padding X", "The number of pixels to pad to the left and right side of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PADDING_Y.toString(), "Padding Y", "The number of pixels to pad to the top and bottom side of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.INSET_TOP.toString(), "Top Inset", "The number of pixels to pad to the op of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.INSET_BOTTOM.toString(), "Bottom Inset", "The number of pixels to pad to the bottom of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.INSET_LEFT.toString(), "Left Inset", "The number of pixels to pad to the left of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.INSET_RIGHT.toString(), "Right Inset", "The number of pixels to pad to the right of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MINIMUM_WIDTH.toString(), "Minimum Width", "The minimum pixel width of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MINIMUM_HEIGHT.toString(), "Minimum Height", "The minimum pixel height of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PREFERRED_WIDTH.toString(), "Preferred Width", "The preferred pixel width of content.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PREFERRED_HEIGHT.toString(), "Preferred Height", "The preferred pixel height of content.", 0));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BORDER_TYPE.toString().equals(datumIdentifier)) return createStringOptions(BorderType.values());
        else if (Datum.SLOT_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.ANCHOR.toString().equals(datumIdentifier)) return createStringOptions(Anchor.values());
        else if (Datum.FILL.toString().equals(datumIdentifier)) return createStringOptions(Fill.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
        
        constructor = Class.forName("com.pilog.t8.ui.panel.T8PanelSlot").getConstructor(T8PanelSlotDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8PanelSlotAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8PanelSlotAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;
        T8ComponentDefinition slotComponentDefinition;

        slotComponentDefinition = getSlotComponentDefinition();
        childComponentDefinitions = new ArrayList<T8ComponentDefinition>();
        if (slotComponentDefinition != null) childComponentDefinitions.add(slotComponentDefinition);
        return childComponentDefinitions;
    }

    public String getTitle()
    {
        return (String)getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String title)
    {
        setDefinitionDatum(Datum.TITLE.toString(), title);
    }

    public BorderType getBorderType()
    {
        return BorderType.valueOf((String)getDefinitionDatum(Datum.BORDER_TYPE.toString()));
    }

    public void setBorderType(BorderType borderType)
    {
        setDefinitionDatum(Datum.BORDER_TYPE.toString(), borderType.toString());
    }
    
    public boolean isCollapsible()
    {
        Boolean value;
        
        value = (Boolean)getDefinitionDatum(Datum.COLLAPSIBLE.toString());
        return value != null && value;
    }
    
    public void setCollapsible(Boolean collapsible)
    {
        setDefinitionDatum(Datum.COLLAPSIBLE.toString(), collapsible);
    }

    public T8ComponentDefinition getSlotComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.SLOT_COMPONENT_DEFINITION.toString());
    }

    public void setSlotComponentDefinition(T8ComponentDefinition componentDefinition)
    {
         setDefinitionDatum(Datum.SLOT_COMPONENT_DEFINITION.toString(), componentDefinition);
    }

    public Anchor getAnchor()
    {
        return Anchor.valueOf((String)getDefinitionDatum(Datum.ANCHOR.toString()));
    }

    public void setAnchor(Anchor anchor)
    {
        setDefinitionDatum(Datum.ANCHOR.toString(), anchor.toString());
    }

    public Fill getFill()
    {
        return Fill.valueOf((String)getDefinitionDatum(Datum.FILL.toString()));
    }

    public void setFill(Fill fill)
    {
        setDefinitionDatum(Datum.FILL.toString(), fill.toString());
    }

    public int getGridHeight()
    {
        return (Integer)getDefinitionDatum(Datum.GRID_HEIGHT.toString());
    }

    public void setGridHeight(int gridHeight)
    {
        setDefinitionDatum(Datum.GRID_HEIGHT.toString(), gridHeight);
    }

    public int getGridWidth()
    {
        return (Integer)getDefinitionDatum(Datum.GRID_WIDTH.toString());
    }

    public void setGridWidth(int gridWidth)
    {
        setDefinitionDatum(Datum.GRID_WIDTH.toString(), gridWidth);
    }

    public int getGridX()
    {
        return (Integer)getDefinitionDatum(Datum.GRID_X.toString());
    }

    public void setGridX(int gridX)
    {
        setDefinitionDatum(Datum.GRID_X.toString(), gridX);
    }

    public int getGridY()
    {
        return (Integer)getDefinitionDatum(Datum.GRID_Y.toString());
    }

    public void setGridY(int gridY)
    {
        setDefinitionDatum(Datum.GRID_Y.toString(), gridY);
    }

    public int getInsetBottom()
    {
        return (Integer)getDefinitionDatum(Datum.INSET_BOTTOM.toString());
    }

    public void setInsetBottom(int insetBottom)
    {
        setDefinitionDatum(Datum.INSET_BOTTOM.toString(), insetBottom);
    }

    public int getInsetLeft()
    {
        return (Integer)getDefinitionDatum(Datum.INSET_LEFT.toString());
    }

    public void setInsetLeft(int insetLeft)
    {
        setDefinitionDatum(Datum.INSET_LEFT.toString(), insetLeft);
    }

    public int getInsetRight()
    {
        return (Integer)getDefinitionDatum(Datum.INSET_RIGHT.toString());
    }

    public void setInsetRight(int insetRight)
    {
        setDefinitionDatum(Datum.INSET_RIGHT.toString(), insetRight);
    }

    public int getInsetTop()
    {
        return (Integer)getDefinitionDatum(Datum.INSET_TOP.toString());
    }

    public void setInsetTop(int insetTop)
    {
        setDefinitionDatum(Datum.INSET_TOP.toString(), insetTop);
    }

    public int getMinimumHeight()
    {
        return (Integer)getDefinitionDatum(Datum.MINIMUM_HEIGHT.toString());
    }

    public void setMinimumHeight(int minimumHeight)
    {
        setDefinitionDatum(Datum.MINIMUM_HEIGHT.toString(), minimumHeight);
    }

    public int getMinimumWidth()
    {
        return (Integer)getDefinitionDatum(Datum.MINIMUM_WIDTH.toString());
    }

    public void setMinimumWidth(int minimumWidth)
    {
        setDefinitionDatum(Datum.MINIMUM_WIDTH.toString(), minimumWidth);
    }

    public int getPaddingX()
    {
        return (Integer)getDefinitionDatum(Datum.PADDING_X.toString());
    }

    public void setPaddingX(int paddingX)
    {
        setDefinitionDatum(Datum.PADDING_X.toString(), paddingX);
    }

    public int getPaddingY()
    {
        return (Integer)getDefinitionDatum(Datum.PADDING_Y.toString());
    }

    public void setPaddingY(int paddingY)
    {
        setDefinitionDatum(Datum.PADDING_Y.toString(), paddingY);
    }

    public int getPreferredHeight()
    {
        return (Integer)getDefinitionDatum(Datum.PREFERRED_HEIGHT.toString());
    }

    public void setPreferredHeight(int preferredHeight)
    {
        setDefinitionDatum(Datum.PREFERRED_HEIGHT.toString(), preferredHeight);
    }

    public int getPreferredWidth()
    {
        return (Integer)getDefinitionDatum(Datum.PREFERRED_WIDTH.toString());
    }

    public void setPreferredWidth(int preferredWidth)
    {
        setDefinitionDatum(Datum.PREFERRED_WIDTH.toString(), preferredWidth);
    }

    public double getWeightX()
    {
        return (Double)getDefinitionDatum(Datum.WEIGHT_X.toString());
    }

    public void setWeightX(double weightX)
    {
        setDefinitionDatum(Datum.WEIGHT_X.toString(), weightX);
    }

    public double getWeightY()
    {
        return (Double)getDefinitionDatum(Datum.WEIGHT_Y.toString());
    }

    public void setWeightY(double weightY)
    {
        setDefinitionDatum(Datum.WEIGHT_Y.toString(), weightY);
    }

    public Dimension getMinimumDimensions()
    {
        return new Dimension(getMinimumWidth(), getMinimumHeight());
    }

    public Dimension getPreferredDimensions()
    {
        return new Dimension(getPreferredWidth(), getPreferredHeight());
    }

    public GridBagConstraints getGridBagConstraints()
    {
        GridBagConstraints constraints;

        constraints = new GridBagConstraints();
        constraints.gridx = getGridX();
        constraints.gridy = getGridY();
        constraints.gridwidth = getGridWidth();
        constraints.gridheight = getGridHeight();
        constraints.fill = getFill().getCode();
        constraints.weightx = getWeightX();
        constraints.weighty = getWeightY();
        constraints.anchor = getAnchor().getCode();
        constraints.ipadx = getPaddingX();
        constraints.ipady = getPaddingY();
        constraints.insets = new Insets(getInsetTop(), getInsetLeft(), getInsetBottom(), getInsetRight());

        return constraints;
    }

    public void setGridBagConstraints(GridBagConstraints constraints)
    {
        setGridX(constraints.gridx);
        setGridY(constraints.gridy);
        setGridWidth(constraints.gridwidth);
        setGridHeight(constraints.gridheight);
        setFill(Fill.valueOf(constraints.fill));
        setWeightX(constraints.weightx);
        setWeightY(constraints.weighty);
        setAnchor(Anchor.valueOf(constraints.anchor));
        setPaddingX(constraints.ipadx);
        setPaddingY(constraints.ipady);
        setInsetTop(constraints.insets.top);
        setInsetBottom(constraints.insets.bottom);
        setInsetLeft(constraints.insets.left);
        setInsetRight(constraints.insets.right);
    }
}
