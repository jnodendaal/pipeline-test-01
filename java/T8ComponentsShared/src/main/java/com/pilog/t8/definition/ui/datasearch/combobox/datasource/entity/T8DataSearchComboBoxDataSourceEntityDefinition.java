package com.pilog.t8.definition.ui.datasearch.combobox.datasource.entity;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBoxModelProvider;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxDataSourceEntityDefinition extends T8DataSearchComboBoxDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_ENTITY";
    public static final String DISPLAY_NAME = "Entity Data Source";
    public static final String DESCRIPTION = "An entity data source that will retrieve the data source items from a T8 data entity";

    public enum Datum
    {
        ENTITY_IDENTIFIER,
        PRE_FILTERS,
        GROUP_NODE_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8DataSearchComboBoxDataSourceEntityDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ENTITY_IDENTIFIER.toString(), "Data Entity Identifier", "The data entity identifier of the data source that will be used to retrieve the data"));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PRE_FILTERS.toString(), "Pre Filters", "The pre filters that will be applied to the entity"));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.GROUP_NODE_DEFINITIONS.toString(), "Group Node Definitions", "The group node definitions"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        if(Datum.PRE_FILTERS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if(Datum.GROUP_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DataSearchComboBoxModelProvider createModelProvider(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datasearch.combobox.server.T8DataRecordSearchComboBoxModelProviderEntity", new Class<?>[]{T8Context.class, T8DataSearchComboBoxDataSourceEntityDefinition.class}, context, this);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ENTITY_IDENTIFIER.toString(), identifier);
    }

    public List<T8DataFilterDefinition> getPreFilters()
    {
        return (List<T8DataFilterDefinition> )getDefinitionDatum(Datum.PRE_FILTERS.toString());
    }

    public void setPreFilters(List<T8DataFilterDefinition> definitions)
    {
        setDefinitionDatum(Datum.PRE_FILTERS.toString(), definitions);
    }

    public List<T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition> getGroupNodeDefinitions()
    {
        return (List<T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition>)getDefinitionDatum(Datum.GROUP_NODE_DEFINITIONS.toString());
    }

    public void setGroupNodeDefinitions(List<T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition> definitions)
    {
        setDefinitionDatum(Datum.GROUP_NODE_DEFINITIONS.toString(), definitions);
    }

}
