package com.pilog.t8.ui.tree;

/**
 * @author Bouwer du Preez
 */
public interface T8TreeCellEditableComponent extends T8TreeCellRenderableComponent
{
    //public void treeNodeEdited(T8TreeNode node);
}
