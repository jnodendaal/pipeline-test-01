package com.pilog.t8.definition.ui.panel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_PANEL";
    public static final String DISPLAY_NAME = "Panel";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  The container uses a default gridbag layout manager.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_PANEL_";
    public enum Datum {TITLE,
                       HEADER_VISIBLE,
                       BORDER_TYPE,
                       COLLAPSIBLE,
                       PAINT_BORDER_INSETS,
                       BACKGROUND_PAINTER_IDENTIFIER,
                       PANEL_SLOT_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public enum BorderType {NONE, BEVEL, ETCHED, LINE, TITLED, SHADOW, CONTENT};
    private T8PainterDefinition backgroundPainterDefinition;

    public T8PanelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TITLE.toString(), "Header Text", "The text displayed at the top of the panel in a header bar (if it is set to be visible)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.HEADER_VISIBLE.toString(), "Header Visible", "A boolean value indicating whether or not the panel's header bar should be displayed.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BORDER_TYPE.toString(), "Border Type", "The border type to use for this panel.", BorderType.NONE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COLLAPSIBLE.toString(), "Collapsible", "If this boolean flag is set and the content header border is used, the border can be clicked to collapse this panel slot.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.PAINT_BORDER_INSETS.toString(), "Paint Border Insets", "A boolean value indicating whether or not the panel's bakcground header should paint the area used by the panel border.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting this panel's backgorund."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PANEL_SLOT_DEFINITIONS.toString(), "Panel Slots", "The layout slots contained by this panel."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BORDER_TYPE.toString().equals(datumIdentifier)) return createStringOptions(BorderType.values());
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.PANEL_SLOT_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8PanelSlotDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.PANEL_SLOT_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.PanelSlotDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        backgroundPainterId = getBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (getPanelSlotDefinitions()== null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.PANEL_SLOT_DEFINITIONS.toString(), "No Panel Slot Definitions defined.", T8DefinitionValidationError.ErrorType.WARNING));

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.panel.T8Panel", new Class<?>[]{T8PanelDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8PanelAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8PanelAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList)getPanelSlotDefinitions();
    }

    public void addChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        getChildComponentDefinitions().add(componentDefinition);
    }

    public boolean removeChildComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        return getChildComponentDefinitions().remove(componentDefinition);
    }

    public T8PanelSlotDefinition getPanelSlotDefinition(String identifier)
    {
        for (T8PanelSlotDefinition layoutConstraints : getPanelSlotDefinitions())
        {
            if (identifier.equalsIgnoreCase(layoutConstraints.getIdentifier()))
            {
                return layoutConstraints;
            }
        }

        return null;
    }

    public List<T8PanelSlotDefinition> getPanelSlotDefinitions()
    {
        return (List<T8PanelSlotDefinition>)getDefinitionDatum(Datum.PANEL_SLOT_DEFINITIONS.toString());
    }

    public void setPanelSlotDefinitions(List<T8PanelSlotDefinition> slotDefinitions)
    {
        setDefinitionDatum(Datum.PANEL_SLOT_DEFINITIONS.toString(), slotDefinitions);
    }

    public String getTitle()
    {
        return (String)getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String title)
    {
        setDefinitionDatum(Datum.TITLE.toString(), title);
    }

    public boolean isPaintBorderInsets()
    {
        Boolean paintBorderInsets;

        paintBorderInsets = (Boolean)getDefinitionDatum(Datum.PAINT_BORDER_INSETS.toString());
        return paintBorderInsets != null ? paintBorderInsets : false;
    }

    public void setPaintBorderInsets(boolean paintBorderInsets)
    {
        setDefinitionDatum(Datum.PAINT_BORDER_INSETS.toString(), paintBorderInsets);
    }

    public boolean isHeaderVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.HEADER_VISIBLE.toString());
        return visible != null ? visible : false;
    }

    public void setHeaderVisible(boolean visible)
    {
        setDefinitionDatum(Datum.HEADER_VISIBLE.toString(), visible);
    }

    public BorderType getBorderType()
    {
        return BorderType.valueOf((String)getDefinitionDatum(Datum.BORDER_TYPE.toString()));
    }

    public void setBorderType(BorderType borderType)
    {
        setDefinitionDatum(Datum.BORDER_TYPE.toString(), borderType.toString());
    }

    public boolean isCollapsible()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COLLAPSIBLE.toString());
        return value != null && value;
    }

    public void setCollapsible(Boolean collapsible)
    {
        setDefinitionDatum(Datum.COLLAPSIBLE.toString(), collapsible);
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }
}
