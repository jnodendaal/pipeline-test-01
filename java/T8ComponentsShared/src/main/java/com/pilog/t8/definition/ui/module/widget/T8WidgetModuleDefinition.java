package com.pilog.t8.definition.ui.module.widget;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8WidgetModuleDefinition extends T8ModuleDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_WIDGET";
    public static final String DISPLAY_NAME = "Widget";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  The widget acts the same as the module with additional functionality for it to display as a widget.";

    // -------- Definition Meta-Data -------- //
    public enum Datum {PREFERRED_WIDTH,
                       PREFERRED_HEIGHT,
                       WIDGET_TITLE_BACKGROUND_PAINTER_IDENTIFIER,
                       WIDGET_TITLE_FOREGROUND_COLOR};

    private T8PainterDefinition backgroundPainterDefinition;

    public T8WidgetModuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PREFERRED_WIDTH.toString(), "Preferred Width", "The preferred width that this module must try to open with, if the width exceeds the container size it will be made smaller.", 100));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PREFERRED_HEIGHT.toString(), "Preferred Height", "The preferred height that this old must try to open with. If the height exceeds the container height a scroll bar will be made visible.", 100));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.WIDGET_TITLE_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Title Background Painter", "The painter that will be used to draw the background of the title bar."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.COLOR_HEX, Datum.WIDGET_TITLE_FOREGROUND_COLOR.toString(), "Title Foreground Color", "The color that will be used for the foreground text displayed on the title bar.", null));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.WIDGET_TITLE_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }



    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
        String backgroundPainterId;

        backgroundPainterId = getTitleBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    public String getTitleBackgroundPainterIdentifier()
    {
        return (String) getDefinitionDatum(Datum.WIDGET_TITLE_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setTitleBackgroundPainterIdentifier(String titleBackgroundPainterIdentifier)
    {
        setDefinitionDatum(Datum.WIDGET_TITLE_BACKGROUND_PAINTER_IDENTIFIER.toString(), titleBackgroundPainterIdentifier);
    }

    public T8PainterDefinition getTitleBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    public Integer getPreferredWidth()
    {
        return (Integer)getDefinitionDatum(Datum.PREFERRED_WIDTH.toString());
    }

    public void setPreferredWidth(Integer preferredWidth)
    {
        setDefinitionDatum(Datum.PREFERRED_WIDTH.toString(), preferredWidth);
    }

    public Integer getPreferredHeight()
    {
        return (Integer)getDefinitionDatum(Datum.PREFERRED_HEIGHT.toString());
    }

    public void setPreferredHeight(Integer preferredHeight)
    {
        setDefinitionDatum(Datum.PREFERRED_HEIGHT.toString(), preferredHeight);
    }

    public Color getTitleForegroundColor()
    {
        return T8DefinitionDatumUtilities.getColor((String)getDefinitionDatum(Datum.WIDGET_TITLE_FOREGROUND_COLOR.toString()));
    }

    public void setTitleForegroundColor(Color foregroundColor)
    {
        setDefinitionDatum(Datum.WIDGET_TITLE_FOREGROUND_COLOR.toString(), T8DefinitionDatumUtilities.getColorHex(foregroundColor));
    }
}
