package com.pilog.t8.definition.ui.table.object;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.data.object.T8ComponentDataObjectProvider;
import com.pilog.t8.ui.T8ComponentController;

/**
 * @author Bouwer du Preez
 */
public abstract class T8TableDataObjectProviderDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DT_UI_TABLE_DATA_OBJECT_PROVIDER";
    public static final String STORAGE_PATH = null;
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8TableDataObjectProviderDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract T8ComponentDataObjectProvider getNewDataObjectProviderInstance(T8ComponentController controller) throws Exception;
}
