/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.ui.filter.panel.component.list;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelCheckedListDefinition extends T8FilterPanelListComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_COMPONENT_CHECK_LIST";
    public static final String DISPLAY_NAME = "Check List";
    public static final String DESCRIPTION = "A Filter Panel Check List Component.";
    public enum Datum
    {
        BACKGROUND_PAINTER_IDENTIFIER,
        ICON_IDENTIFIER,
        TRANSPARENT
    };
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition backgroundPainterDefinition;
    private T8IconDefinition iconDefinition;

    public T8FilterPanelCheckedListDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        backgroundPainterId = getBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }

        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Background Painter", "The painter to use for painting this panel's backgorund."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this button's text."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TRANSPARENT.toString(), "Transparent", "Whether this components background must be set be transparent."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            T8DefinitionContext definitionContext, String datumIdentifier)
            throws Exception
    {
        if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        }
        else if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.filter.panel.component.list.T8FilterPanelCheckedList").getConstructor(T8FilterPanelCheckedListDefinition.class, T8ComponentController.class);
            return (T8Component) constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return super.getComponentEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return super.getComponentOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String) getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public Boolean isTransparentSet()
    {
        return (Boolean) getDefinitionDatum(Datum.TRANSPARENT.toString());
    }

    public void setTransparent(Boolean transparency)
    {
        setDefinitionDatum(Datum.TRANSPARENT.toString(), transparency);
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    public String getIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }
}
