package com.pilog.t8.definition.ui.tree;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.celleditor.T8TreeCellEditorDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TreeCellRendererDefinition;
import com.pilog.t8.definition.ui.entitymenu.T8DataEntityMenuItemDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8TreeNodeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TREE_NODE";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_TREE_NODE";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Tree Node";
    public static final String DESCRIPTION = "A node that forms part of a tree structure.";
    public static final String IDENTIFIER_PREFIX = "C_TREE_NODE_";
    private enum Datum {PAGE_SIZE,
                        GROUP_DISPLAY_NAME,
                        GROUP_NODE_ENABLED,
                        GROUP_RENDERER_COMPONENT_DEFINITION,
                        RECURSION_NODE_IDENTIFIER,
                        RECURSION_FIELD_MAPPING,
                        RECURSION_PREFILTER_DEFINITIONS,
                        DATA_ENTITY_IDENTIFIER,
                        PREFILTER_DEFINITIONS,
                        RELATIONSHIP_FIELD_MAPPING,
                        CHILD_NODE_DEFINITIONS,
                        RENDERER_COMPONENT_DEFINITION,
                        EDITOR_COMPONENT_DEFINITION,
                        QUICK_SEARCH_FILTER_DEFINITION,
                        MENU_ITEM_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8TreeNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PAGE_SIZE.toString(), "Page Size", "The number of records that will be retrieved from the data source per page of this node type.", 100));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.GROUP_DISPLAY_NAME.toString(), "Group Display Name", "The display name that labels nodes of this type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.GROUP_NODE_ENABLED.toString(), "Group Node Enabled", "If this value is set to true, a group node will be added to the tree and all nodes of this type will be added to it as children.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.GROUP_RENDERER_COMPONENT_DEFINITION.toString(), "Group Renderer", "The renderer to use for displaying the group node."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity displayed by this component."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PREFILTER_DEFINITIONS.toString(), "Prefilters", "The prefilters that will be applied when data for this node type is retrieved."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.RELATIONSHIP_FIELD_MAPPING.toString(), "Field Mapping:  Parent to Node", "A mapping of this parent node's fields to the related node fields."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.RECURSION_NODE_IDENTIFIER.toString(), "Recursion Target", "A parent node that may appear as a child of this node (forming a recursive loop)."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.RECURSION_PREFILTER_DEFINITIONS.toString(), "Recursion Prefilters", "The prefilters that will be applied when recursive data is retrieved (data for the recursion target node)."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.RECURSION_FIELD_MAPPING.toString(), "Field Mapping:  Node to Recursion Target", "A mapping of this node's fields to the related fields of the recursion target node."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.RENDERER_COMPONENT_DEFINITION.toString(), "Renderer", "The renderer to use for displaying the contents of this node type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.EDITOR_COMPONENT_DEFINITION.toString(), "Editor", "The editor to use for editing the contents of this node type."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CHILD_NODE_DEFINITIONS.toString(), "Child Nodes", "The possible child nodes that stem from this node."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.QUICK_SEARCH_FILTER_DEFINITION.toString(), "Quick Search Filter", "The filter used when a quick search is performed on this node type."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.MENU_ITEM_DEFINITIONS.toString(), "Menu Items", "The menu items applicable to this node."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.RECURSION_PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.CHILD_NODE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TreeNodeDefinition.TYPE_IDENTIFIER));
        else if (Datum.QUICK_SEARCH_FILTER_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.GROUP_RENDERER_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TreeCellRendererDefinition.GROUP_IDENTIFIER));
        else if (Datum.RENDERER_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TreeCellRendererDefinition.GROUP_IDENTIFIER));
        else if (Datum.EDITOR_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TreeCellEditorDefinition.GROUP_IDENTIFIER));
        else if (Datum.RECURSION_NODE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8TreeNodeDefinition> ancestorDefinitions;
            ArrayList<T8DefinitionDatumOption> datumOptions;

            datumOptions = new ArrayList<>();
            ancestorDefinitions = (List)getAncestorDefinitions(T8TreeNodeDefinition.TYPE_IDENTIFIER);
            for (T8TreeNodeDefinition ancestorDefinition : ancestorDefinitions)
            {
                datumOptions.add(new T8DefinitionDatumOption(ancestorDefinition.getIdentifier(), ancestorDefinition.getIdentifier()));
            }

            // Add this node to the recursion options (recurs-to-self).
            datumOptions.add(new T8DefinitionDatumOption(getIdentifier(), getIdentifier()));

            // Add an option to be able to remove the recursion.
            datumOptions.add(new T8DefinitionDatumOption("No Recursion", null));

            // Return the options.
            return datumOptions;
        }
        else if (Datum.RECURSION_FIELD_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8TreeNodeDefinition recursionNodeDefinition;

            optionList = new ArrayList<>();

            recursionNodeDefinition = getRecursionNodeDefinition();
            if (recursionNodeDefinition != null)
            {

                String ancestorEntityId;

                ancestorEntityId = recursionNodeDefinition.getDataEntityIdentifier();
                if (ancestorEntityId != null)
                {
                    T8DataEntityDefinition ancestorEntityDefinition;
                    T8DataEntityDefinition localEntityDefinition;

                    ancestorEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), ancestorEntityId);
                    localEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), getDataEntityIdentifier());

                    if ((ancestorEntityDefinition != null) && (localEntityDefinition != null))
                    {
                        List<T8DataEntityFieldDefinition> localFieldDefinitions;
                        List<T8DataEntityFieldDefinition> parentFieldDefinitions;
                        HashMap<String, List<String>> identifierMap;

                        localFieldDefinitions = localEntityDefinition.getFieldDefinitions();
                        parentFieldDefinitions = ancestorEntityDefinition.getFieldDefinitions();

                        identifierMap = new HashMap<>();
                        if ((localFieldDefinitions != null) && (parentFieldDefinitions != null))
                        {
                            for (T8DataEntityFieldDefinition localFieldDefinition : localFieldDefinitions)
                            {
                                ArrayList<String> identifierList;

                                identifierList = new ArrayList<>();
                                for (T8DataEntityFieldDefinition parentFieldDefinition : parentFieldDefinitions)
                                {
                                    identifierList.add(parentFieldDefinition.getPublicIdentifier());
                                }

                                identifierMap.put(localFieldDefinition.getPublicIdentifier(), identifierList);
                            }
                        }

                        optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                        return optionList;
                    }
                    else return optionList;
                }
                else return optionList;
            }
            else return optionList;
        }
        else if (Datum.RELATIONSHIP_FIELD_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            T8Definition parentDefinition;

            optionList = new ArrayList<>();
            parentDefinition = getParentDefinition();
            if (parentDefinition instanceof T8TreeNodeDefinition)
            {
                String parentEntityId;

                parentEntityId = ((T8TreeNodeDefinition)parentDefinition).getDataEntityIdentifier();
                if (parentEntityId != null)
                {
                    T8DataEntityDefinition parentEntityDefinition;
                    T8DataEntityDefinition localEntityDefinition;

                    parentEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), parentEntityId);
                    localEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), getDataEntityIdentifier());

                    if ((parentEntityDefinition != null) && (localEntityDefinition != null))
                    {
                        List<T8DataEntityFieldDefinition> localFieldDefinitions;
                        List<T8DataEntityFieldDefinition> parentFieldDefinitions;
                        HashMap<String, List<String>> identifierMap;

                        localFieldDefinitions = localEntityDefinition.getFieldDefinitions();
                        parentFieldDefinitions = parentEntityDefinition.getFieldDefinitions();

                        identifierMap = new HashMap<>();
                        if ((localFieldDefinitions != null) && (parentFieldDefinitions != null))
                        {
                            for (T8DataEntityFieldDefinition parentFieldDefinition : parentFieldDefinitions)
                            {
                                ArrayList<String> identifierList;

                                identifierList = new ArrayList<>();
                                for (T8DataEntityFieldDefinition localFieldDefinition : localFieldDefinitions)
                                {
                                    identifierList.add(localFieldDefinition.getPublicIdentifier());
                                }

                                identifierMap.put(parentFieldDefinition.getPublicIdentifier(), identifierList);
                            }
                        }

                        optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                        return optionList;
                    }
                    else return optionList;
                }
                else return optionList; // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
            }
            else return optionList;
        }
        else if (Datum.MENU_ITEM_DEFINITIONS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;

            options = createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataEntityMenuItemDefinition.GROUP_IDENTIFIER));
            return options;
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8TreeNodeDefinition getRecursionNodeDefinition()
    {
        String recursionNodeIdentifier;

        recursionNodeIdentifier = getRecursionNodeIdentifier();
        if (recursionNodeIdentifier != null)
        {
            if (recursionNodeIdentifier.equals(getIdentifier()))
            {
                return this; // Recurse-to-self.
            }
            else
            {
                List<T8TreeNodeDefinition> ancestorDefinitions;

                ancestorDefinitions = (List)getAncestorDefinitions(T8TreeNodeDefinition.TYPE_IDENTIFIER);
                for (T8TreeNodeDefinition ancestorDefinition : ancestorDefinitions)
                {
                    if (ancestorDefinition.getIdentifier().equals(recursionNodeIdentifier))
                    {
                        return ancestorDefinition;
                    }
                }

                return null;
            }
        }
        else return null;
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        if(getPrefilterDefinitions() != null)
        {
            for (T8Definition t8Definition : getPrefilterDefinitions())
            {
                T8DataFilterDefinition dataFilterDefinition;

                dataFilterDefinition = (T8DataFilterDefinition) t8Definition;

                if(!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getPublicIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Invalid Entity Identifier on Filter, expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        if(getQuickSearchFilterDefinition() != null)
        {
            T8DataFilterDefinition dataFilterDefinition;

            dataFilterDefinition = getQuickSearchFilterDefinition();

            if(!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getPublicIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Invalid Entity Identifier on Filter, expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    public int getPageSize()
    {
        return (Integer)getDefinitionDatum(Datum.PAGE_SIZE.toString());
    }

    public void setPageSize(int pageSize)
    {
        setDefinitionDatum(Datum.PAGE_SIZE.toString(), pageSize);
    }

    public String getGroupDisplayName()
    {
        return (String)getDefinitionDatum(Datum.GROUP_DISPLAY_NAME.toString());
    }

    public void setGroupDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.GROUP_DISPLAY_NAME.toString(), displayName);
    }

    public boolean isGroupNodeEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.GROUP_NODE_ENABLED.toString());
        return enabled != null ? enabled : false;
    }

    public void setGroupNodeEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.GROUP_NODE_ENABLED.toString(), enabled);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public String getRecursionNodeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RECURSION_NODE_IDENTIFIER.toString());
    }

    public void setRecursionNodeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.RECURSION_NODE_IDENTIFIER.toString(), identifier);
    }

    public List<T8DataFilterDefinition> getRecursionPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.RECURSION_PREFILTER_DEFINITIONS.toString());
    }

    public void setRecursionPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.RECURSION_PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public List<T8DataFilterDefinition> getPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString());
    }

    public void setPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public Map<String, String> getRelationshipFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.RELATIONSHIP_FIELD_MAPPING.toString());
    }

    public void setRelationshipFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.RELATIONSHIP_FIELD_MAPPING.toString(), mapping);
    }

    public Map<String, String> getRecursionFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.RECURSION_FIELD_MAPPING.toString());
    }

    public void setRecursionFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.RECURSION_FIELD_MAPPING.toString(), mapping);
    }

    public T8TreeNodeDefinition getChildNodeDefinition(String childNodeIdentifier)
    {
        for (T8TreeNodeDefinition childNodeDefinition : getChildNodeDefinitions())
        {
            if (childNodeDefinition.getIdentifier().equals(childNodeIdentifier)) return childNodeDefinition;
        }

        return null;
    }

    public List<T8TreeNodeDefinition> getChildNodeDefinitions()
    {
        return (List<T8TreeNodeDefinition>)getDefinitionDatum(Datum.CHILD_NODE_DEFINITIONS.toString());
    }

    public void setChildNodeDefinitions(List<T8TreeNodeDefinition> nodeDefinitions)
    {
        setDefinitionDatum(Datum.CHILD_NODE_DEFINITIONS.toString(), nodeDefinitions);
    }

    public T8ComponentDefinition getGroupRendererComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.GROUP_RENDERER_COMPONENT_DEFINITION.toString());
    }

    public void setGroupRendererComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.GROUP_RENDERER_COMPONENT_DEFINITION.toString(), definition);
    }

    public T8ComponentDefinition getRendererComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.RENDERER_COMPONENT_DEFINITION.toString());
    }

    public void setRendererComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.RENDERER_COMPONENT_DEFINITION.toString(), definition);
    }

    public T8ComponentDefinition getEditorComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.EDITOR_COMPONENT_DEFINITION.toString());
    }

    public void setEditorComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.EDITOR_COMPONENT_DEFINITION.toString(), definition);
    }

    public T8DataFilterDefinition getQuickSearchFilterDefinition()
    {
        return (T8DataFilterDefinition)getDefinitionDatum(Datum.QUICK_SEARCH_FILTER_DEFINITION.toString());
    }

    public void setQuickSearchFilterDefinition(T8DataFilterDefinition definition)
    {
        setDefinitionDatum(Datum.QUICK_SEARCH_FILTER_DEFINITION.toString(), definition);
    }
    public ArrayList<T8DataEntityMenuItemDefinition> getMenuItemDefinitions()
    {
        return (ArrayList<T8DataEntityMenuItemDefinition>)getDefinitionDatum(Datum.MENU_ITEM_DEFINITIONS.toString());
    }

    public void setMenuItemDefinitions(ArrayList<T8DataEntityMenuItemDefinition> menuItemDefinitions)
    {
        getMenuItemDefinitions().clear();
        getMenuItemDefinitions().addAll(menuItemDefinitions);
    }

    public T8DataEntityMenuItemDefinition getMenuItemDefinition(String identifier)
    {
        for (T8DataEntityMenuItemDefinition menuItemDefinition : getMenuItemDefinitions())
        {
            if (menuItemDefinition.getIdentifier().equals(identifier))
            {
                return menuItemDefinition;
            }
        }

        return null;
    }
}
