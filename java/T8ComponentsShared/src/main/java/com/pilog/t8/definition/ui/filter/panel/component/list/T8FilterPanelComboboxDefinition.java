/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.ui.filter.panel.component.list;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelComboboxDefinition extends T8FilterPanelListComponentDefinition
{
    // -------- Definition Meta-Data -------- //

    public static final String TYPE_IDENTIFIER = "@DT_FILTER_PANEL_COMPONENT_COMBOBOX";
    public static final String DISPLAY_NAME = "Combobox";
    public static final String DESCRIPTION = "A Filter Panel Combobox component.";
    public static final String VERSION = "0";

    public enum Datum
    {
    };
// -------- Definition Meta-Data -------- //
    public T8FilterPanelComboboxDefinition(String identifier)
    {
        super(identifier);
    }

//    @Override
//    public T8FilterPanelComponent getInstance()
//    {
//        try
//        {
//            Constructor constructor;
//
//            constructor = Class.forName("com.pilog.t8.ui.filter.panel.component.T8FilterPanelCombobox").getConstructor(T8FilterPanelComboboxDefinition.class);
//            return (T8FilterPanelComponent) constructor.newInstance(this);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            return null;
//        }
//    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
    }



    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.filter.panel.component.list.T8FilterPanelCombobox").getConstructor(T8FilterPanelComboboxDefinition.class,T8ComponentController.class);
            return (T8Component) constructor.newInstance(this,controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return super.getComponentEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return super.getComponentOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }
}
