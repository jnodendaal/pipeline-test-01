package com.pilog.t8.definition.ui.wizard;

/**
 * This class is kept solely for backwards compatibility. The entire definition
 * has been replaced by T8WizardStepDefinition.
 * @author Bouwer du Preez
 */
public class T8WizardStepCategoryDefinition extends T8WizardStepDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_WIZARD_STEP_CATEGORY";
    public static final String DISPLAY_NAME = "Category";
    public static final String DESCRIPTION = "This will categories steps underneath this category";
    // -------- Definition Meta-Data -------- //

    public T8WizardStepCategoryDefinition(String identifier)
    {
        super(identifier);
    }
}
