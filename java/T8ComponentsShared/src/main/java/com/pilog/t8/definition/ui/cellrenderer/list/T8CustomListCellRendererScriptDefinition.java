package com.pilog.t8.definition.ui.cellrenderer.list;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.script.T8ClientContextScriptDefinition;
import com.pilog.t8.definition.script.T8DefaultModuleCompletionHandler;
import com.pilog.t8.definition.script.T8DefaultScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8CustomListCellRendererScriptDefinition extends T8DefaultScriptDefinition implements T8ClientContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_UI_COMPONENT_LIST_CELL_RENDERER_CUSTOM";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_UI_COMPONENT_LIST_CELL_RENDERER_CUSTOM";
    public static final String DISPLAY_NAME = "Cell Renderer Script";
    public static final String DESCRIPTION = "A script that is executed for each cell render.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_SELECTED = "$P_SELECTED";
    public static final String PARAMETER_FOCUSED = "$P_FOCUSED";
    public static final String PARAMETER_INDEX = "$P_INDEX";
    public static final String PARAMETER_VALUE = "$P_VALUE";


    public T8CustomListCellRendererScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> inputParameters;

        inputParameters = new ArrayList<>();

        inputParameters.add(new T8DataParameterResourceDefinition(PARAMETER_SELECTED, "Selected", "If the component is selecteed", T8DataType.BOOLEAN));
        inputParameters.add(new T8DataParameterResourceDefinition(PARAMETER_FOCUSED, "Focused", "If the component is focused", T8DataType.BOOLEAN));
        inputParameters.add(new T8DataParameterResourceDefinition(PARAMETER_INDEX, "Index", "The current index in the list", T8DataType.INTEGER));
        inputParameters.add(new T8DataParameterResourceDefinition(PARAMETER_VALUE, "Value", "The current list value", T8DataType.CUSTOM_OBJECT));

        return inputParameters;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return new ArrayList<>();
    }

    @Override
    public T8ClientContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultClientContextScript").getConstructor(T8Context.class, T8ClientContextScriptDefinition.class);
        return (T8ClientContextScript)constructor.newInstance(context, this);
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultModuleCompletionHandler(context, this);
    }


}
