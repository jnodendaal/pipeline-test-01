package com.pilog.t8.definition.ui.cellrenderer.table.attachment;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8AttachmentTableCellRendererDefinition extends T8ComponentDefinition implements T8TableCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_TABLE_ATTACHMENT";
    public static final String DISPLAY_NAME = "Attachment Cell Renderer";
    public static final String DESCRIPTION = "A cell renderer that renders attachments";
    public enum Datum {THUMBNAIL_WIDTH,
                       THUMBNAIL_HEIGHT};
    // -------- Definition Meta-Data -------- //

    public T8AttachmentTableCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.THUMBNAIL_WIDTH.toString(), "Thumbnail Width", "The width of the thumbnail image to generate for attachments.", 64));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.THUMBNAIL_HEIGHT.toString(), "Thumbnail Height", "The height of the thumbnail image to generate for attachments.", 64));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.cellrenderer.table.attachment.T8AttachmentTableCellRenderer", new Class<?>[]{T8ComponentController.class, T8AttachmentTableCellRendererDefinition.class}, controller, this);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public int getThumbnailWidth()
    {
        Integer value;

        value = getDefinitionDatum(Datum.THUMBNAIL_WIDTH);
        return value != null ? value : 64;
    }

    public void setThumbnailWidth(int width)
    {
        setDefinitionDatum(Datum.THUMBNAIL_WIDTH, width);
    }

    public int getThumbnailHeight()
    {
        Integer value;

        value = getDefinitionDatum(Datum.THUMBNAIL_HEIGHT);
        return value != null ? value : 64;
    }

    public void setThumbnailHeight(int height)
    {
        setDefinitionDatum(Datum.THUMBNAIL_HEIGHT, height);
    }
}
