package com.pilog.t8.definition.ui.collapsiblepanel;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8CollapsiblePanelAPIHandler
{
    // These should NOT be changed, only added to.

    public static final String OPERATION_SET_HEADER_TEXT = "$CO_SET_HEADER_TEXT";

    public static final String PARAMETER_TEXT = "$CP_TEXT";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_HEADER_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Header Text");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The new text to set for the header.", T8DataType.DISPLAY_STRING));
        operations.add(newOperationDefinition);

        return operations;
    }
}
