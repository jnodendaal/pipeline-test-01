package com.pilog.t8.definition.ui.datalookuplink;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupLinkDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_LOOKUP_LINK";
    public static final String DISPLAY_NAME = "Data Lookup Link";
    public static final String DESCRIPTION = "A component that displays a hyperlink which allows the user to click through to a dialog where a new value can be searched for.";
    public static final String IDENTIFIER_PREFIX = "C_DATA_LOOKUP_LINK_";
    public enum Datum {DATA_ENTITY_IDENTIFIER,
                       DISPLAY_FIELD_IDENTIFIER,
                       VALUE_FIELD_IDENTIFIER,
                       LOOKUP_DIALOG_DEFINITION,
                       NULL_DISPLAY_STRING};
    // -------- Definition Meta-Data -------- //

    private T8DataEntityDefinition dataEntityDefinition;

    public T8DataLookupLinkDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity on which this component acts."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DISPLAY_FIELD_IDENTIFIER.toString(), "Display Field", "The field from which the value to display will be retrieved.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.VALUE_FIELD_IDENTIFIER.toString(), "Value Field", "The field from which the lookup data value is retrieved (the purpose of the value field depends on the application of the lookup field).").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.NULL_DISPLAY_STRING.toString(), "Null Display String", "The string to display on the link when no value is selected."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.LOOKUP_DIALOG_DEFINITION.toString(), "Dialog", "The lookup dialog that will be popped up to allow the user to select a data value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DISPLAY_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.VALUE_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.LOOKUP_DIALOG_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataLookupDialogDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String entityId;

        entityId = getDataEntityIdentifier();
        if (entityId != null)
        {
            dataEntityDefinition = (T8DataEntityDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), entityId, inputParameters);
        }
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(Strings.isNullOrEmpty(getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier not set on definition " + getPublicIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        if(Strings.isNullOrEmpty(getDisplayFieldIdentifier()))validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DISPLAY_FIELD_IDENTIFIER.toString(), "Display Field Identifier not set on definition " + getPublicIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        if(Strings.isNullOrEmpty(getValueFieldIdentifier()))validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.VALUE_FIELD_IDENTIFIER.toString(), "Value Field Identifier not set on definition " + getPublicIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        if(getLookupDialogDefinition() == null)validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.LOOKUP_DIALOG_DEFINITION.toString(), "No Lookup Dialog Definition set for definition " + getPublicIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datalookuplink.T8DataLookupLink", new Class<?>[]{T8DataLookupLinkDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataLookupLinkAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataLookupLinkAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getDisplayFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_FIELD_IDENTIFIER.toString());
    }

    public void setDisplayFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DISPLAY_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getValueFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.VALUE_FIELD_IDENTIFIER.toString());
    }

    public void setValueFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.VALUE_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getNullDisplayString()
    {
        return (String)getDefinitionDatum(Datum.NULL_DISPLAY_STRING.toString());
    }

    public void setNullDisplayString(String displayString)
    {
        setDefinitionDatum(Datum.NULL_DISPLAY_STRING.toString(), displayString);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public T8DataLookupDialogDefinition getLookupDialogDefinition()
    {
        return (T8DataLookupDialogDefinition)getDefinitionDatum(Datum.LOOKUP_DIALOG_DEFINITION.toString());
    }

    public void setLookupDialogDefinition(T8DataLookupDialogDefinition definition)
    {
        setDefinitionDatum(Datum.LOOKUP_DIALOG_DEFINITION.toString(), definition);
    }

    public T8DataEntityDefinition getDataEntityDefinition()
    {
        return dataEntityDefinition;
    }
}
