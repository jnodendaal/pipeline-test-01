package com.pilog.t8.definition.ui.chart.container;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ChartContainerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_CHART_CONTAINER";
    public static final String IDENTIFIER_PREFIX = "C_CHART_CONTAINER_";
    public static final String DISPLAY_NAME = "Chart Container";
    public static final String DESCRIPTION = "A container that holds a chart.";
    public enum Datum {CHART_REFRESH_TEXT,
                       CHART_AUTO_RETRIEVE,
                       CHART_COMPONENT_DEFINITION,
                       SOURCE_SERVER_OPERATION_IDENTIFIER,
                       CHART_REFRESH_OPERATION_IDENTIFIER,
                       SOURCE_PARAMETER_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8ChartContainerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.CHART_REFRESH_TEXT.toString(), "Chart Refresh Text", "The text displayed on the progress indicator when the chart is refreshed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CHART_AUTO_RETRIEVE.toString(), "Chart Auto Retrieve", "Whether or not the chart should start retrieving its data as soon as it's loaded.", Boolean.TRUE));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CHART_COMPONENT_DEFINITION.toString(), "Chart Component", "The chart component contained by this container."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.SOURCE_SERVER_OPERATION_IDENTIFIER.toString(), "Source Operation", "The operation that handles the retrieval and compilation of the chart data set."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.CHART_REFRESH_OPERATION_IDENTIFIER.toString(), "Chart Refresh Operation", "The chart operation that will be used to refresh the data set."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.SOURCE_PARAMETER_MAPPING.toString(), "Parameter Mapping", "Script Output to Chart Operation."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CHART_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.SOURCE_SERVER_OPERATION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER));
        else if (Datum.CHART_REFRESH_OPERATION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8ComponentDefinition chartComponentDefinition;

            chartComponentDefinition = getChartComponentDefinition();
            if (chartComponentDefinition != null)
            {
                return createLocalIdentifierOptionsFromDefinitions(new ArrayList<>(chartComponentDefinition.getComponentOperationDefinitions()));
            }
            else return new ArrayList<>();
        }
        else if (Datum.SOURCE_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String scriptId;

            optionList = new ArrayList<>();

            scriptId = getSourceServerOperationIdentifier();
            if (scriptId != null)
            {
                T8ServerOperationDefinition serverOperationDefinition;
                T8ComponentOperationDefinition operationDefinition;

                serverOperationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), scriptId);
                operationDefinition = getChartRefreshOperationDefinition();

                if ((serverOperationDefinition != null) && (operationDefinition != null))
                {
                    List<T8DataParameterDefinition> scriptOutputParameters;
                    List<T8DataParameterDefinition> operationInputParameters;
                    HashMap<String, List<String>> identifierMap;

                    scriptOutputParameters = serverOperationDefinition.getOutputParameterDefinitions();
                    operationInputParameters = operationDefinition.getInputParameterDefinitions();

                    identifierMap = new HashMap<>();
                    if ((scriptOutputParameters != null) && (operationInputParameters != null))
                    {
                        for (T8DataParameterDefinition scriptOutputParameter : scriptOutputParameters)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataParameterDefinition operationInputParameter : operationInputParameters)
                            {
                                identifierList.add(operationInputParameter.getIdentifier());
                            }

                            identifierMap.put(scriptOutputParameter.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.chart.container.T8ChartContainer", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8ChartContainerAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8ChartContainerAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return ArrayLists.typeSafeList(getChartComponentDefinition());
    }

    private T8ComponentOperationDefinition getChartRefreshOperationDefinition()
    {
        T8ComponentDefinition chartComponentDefinition;

        chartComponentDefinition = getChartComponentDefinition();
        if (chartComponentDefinition != null)
        {
            List<T8ComponentOperationDefinition> operationDefinitions;

            operationDefinitions = chartComponentDefinition.getComponentOperationDefinitions();
            if (operationDefinitions != null)
            {
                for (T8ComponentOperationDefinition operationDefinition : operationDefinitions)
                {
                    if (operationDefinition.getIdentifier().equals(getChartRefreshOperationIdentifier()))
                    {
                        return operationDefinition;
                    }
                }

                return null;
            }
            else return null;
        }
        else return null;
    }

    public T8ComponentDefinition getChartComponentDefinition()
    {
        return getDefinitionDatum(Datum.CHART_COMPONENT_DEFINITION);
    }

    public void setChartComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.CHART_COMPONENT_DEFINITION, definition);
    }

    public String getChartRefreshText()
    {
        return getDefinitionDatum(Datum.CHART_REFRESH_TEXT);
    }

    public void setChartRefreshText(String title)
    {
        setDefinitionDatum(Datum.CHART_REFRESH_TEXT, title);
    }

    public boolean isChartAutoRetrieve()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.CHART_AUTO_RETRIEVE));
    }

    public void setChartAutoRetrieve(Boolean autoRetrieve)
    {
        setDefinitionDatum(Datum.CHART_AUTO_RETRIEVE, autoRetrieve);
    }

    public String getSourceServerOperationIdentifier()
    {
        return getDefinitionDatum(Datum.SOURCE_SERVER_OPERATION_IDENTIFIER);
    }

    public void setSourceServerOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SOURCE_SERVER_OPERATION_IDENTIFIER, identifier);
    }

    public String getChartRefreshOperationIdentifier()
    {
        return getDefinitionDatum(Datum.CHART_REFRESH_OPERATION_IDENTIFIER);
    }

    public void setChartRefreshOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.CHART_REFRESH_OPERATION_IDENTIFIER, identifier);
    }

    public Map<String, String> getSourceParameterMapping()
    {
        return getDefinitionDatum(Datum.SOURCE_PARAMETER_MAPPING);
    }

    public void setSourceParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.SOURCE_PARAMETER_MAPPING, mapping);
    }
}
