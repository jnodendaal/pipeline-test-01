package com.pilog.t8.definition.ui.cellrenderer.table.identifier;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionIdentifierTableCellRendererDefinition extends T8ComponentDefinition implements T8TableCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_DEFINITION_IDENTIFIER";
    public static final String DISPLAY_NAME = "Definition Identifier Cell Renderer";
    public static final String DESCRIPTION = "A cell renderer that renders a display value based on the datums of the definition referenced by the value contained in a cell.";
    public static final String VERSION = "0";
    public enum Datum {CACHE_SIZE,
                       NAMESPACE_EXPRESSION,
                       CACHE_DEFINITION_IDENTIFIERS,
                       CACHE_DEFINITION_GROUP_IDENTIFIERS,
                       DISPLAY_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8DefinitionIdentifierTableCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.CACHE_SIZE.toString(), "Cache Size", "The number of definitions to keep in the client-side cache.", 100));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.NAMESPACE_EXPRESSION.toString(), "Namespace Expression", "The expression that will be evaluated to determine the namespace of the data value (identifier) to render.  Only applicable if the identifier is local and does not already include the namespace.", null));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.CACHE_DEFINITION_IDENTIFIERS.toString(), "Cache Definitions", "The definitions to cache (if any)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_GROUP_IDENTIFIER_LIST, Datum.CACHE_DEFINITION_GROUP_IDENTIFIERS.toString(), "Cache Definition Groups", "The definition groups to cache (if any)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DISPLAY_VALUE_EXPRESSION.toString(), "Display Value Expression", "An expression that is evaluated using the definition datums in order to determine the value to display."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CACHE_DEFINITION_IDENTIFIERS.toString().equals(datumIdentifier)) return createStringOptions(definitionContext.getDefinitionIdentifiers(null, null));
        else if (Datum.CACHE_DEFINITION_GROUP_IDENTIFIERS.toString().equals(datumIdentifier)) return createStringOptions(definitionContext.getDefinitionGroupIdentifiers());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.cellrenderer.table.identifier.T8DefinitionIdentifierTableCellRenderer").getConstructor(this.getClass(), T8ComponentController.class);
            return (T8Component)constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public Integer getDefinitionCacheSize()
    {
        Integer size;

        size = (Integer)getDefinitionDatum(Datum.CACHE_SIZE.toString());
        return size != null ? size : 100;
    }

    public void setDefinitionCacheSize(int size)
    {
        setDefinitionDatum(Datum.CACHE_SIZE.toString(), size);
    }

    public List<String> getCacheDefinitionIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.CACHE_DEFINITION_IDENTIFIERS.toString());
    }

    public void setCacheDefinitionIdentifiers(List<String> definitionIdentifiers)
    {
        setDefinitionDatum(Datum.CACHE_DEFINITION_IDENTIFIERS.toString(), definitionIdentifiers);
    }

    public List<String> getCacheDefinitionGroupIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.CACHE_DEFINITION_GROUP_IDENTIFIERS.toString());
    }

    public void setCacheDefinitionGroupIdentifiers(List<String> groupIdentifiers)
    {
        setDefinitionDatum(Datum.CACHE_DEFINITION_GROUP_IDENTIFIERS.toString(), groupIdentifiers);
    }

    public String getDisplayValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION.toString());
    }

    public void setDisplayValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION.toString(), expression);
    }

    public String getNamespaceExpression()
    {
        return (String)getDefinitionDatum(Datum.NAMESPACE_EXPRESSION.toString());
    }

    public void setNamespaceExpression(String expression)
    {
        setDefinitionDatum(Datum.NAMESPACE_EXPRESSION.toString(), expression);
    }
}
