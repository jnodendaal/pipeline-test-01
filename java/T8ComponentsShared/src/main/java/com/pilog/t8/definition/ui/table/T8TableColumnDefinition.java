package com.pilog.t8.definition.ui.table;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.celleditor.T8TableCellEditorDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TableColumnDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TABLE_COLUMN";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_TABLE_COLUMN";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Table Column";
    public static final String DESCRIPTION = "A column that may be added to a table component.";
    private enum Datum {FIELD_IDENTIFIER,
                        COLUMN_NAME,
                        WIDTH,
                        MAX_WIDTH,
                        MIN_WIDTH,
                        EDITABLE,
                        VISIBLE,
                        REQUIRED,
                        DEFAULT_VALUE_EXPRESSION,
                        CELL_EDITABLE_EXPRESSION,
                        RENDERER_COMPONENT_DEFINITION,
                        EDITOR_COMPONENT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8TableColumnDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FIELD_IDENTIFIER.toString(), "Field Identifier", "The identifier of the data source field from which this column's data will be extracted."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.COLUMN_NAME.toString(), "Column Name", "The display name of this column."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.WIDTH.toString(), "Width", "The preferred width of this column."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MAX_WIDTH.toString(), "Max Width", "The maximum width of this column."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MIN_WIDTH.toString(), "Min Width", "The minimum width of this column."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EDITABLE.toString(), "Editable", "A boolean value indicating whether or not the user may edit the data in this column."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.VISIBLE.toString(), "Visible", "A boolean value indicating whether or not this column will be visible to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REQUIRED.toString(), "Required", "A boolean value indicating whether or not the user will be forced to enter a value for this column before changes are committed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.RENDERER_COMPONENT_DEFINITION.toString(), "Renderer", "The renderer to use for displaying the contents of this column's cells."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.EDITOR_COMPONENT_DEFINITION.toString(), "Editor", "The editor to use for editing the contents of this column's cells."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DEFAULT_VALUE_EXPRESSION.toString(), "Default Value Expression", "The expression that will be evaluated to determine the default value for this column when a new row is inserted."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CELL_EDITABLE_EXPRESSION.toString(), "Cell Editable Expression", "The expression that will be evaluated to determine if the cell should be editable or not."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8TableDefinition tableDefinition;
            String entityId;

            tableDefinition = (T8TableDefinition)getParentDefinition();
            entityId = tableDefinition.getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return null;
            }
            else return null;
        }
        else if (Datum.RENDERER_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TableCellRendererDefinition.GROUP_IDENTIFIER));
        else if (Datum.EDITOR_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TableCellEditorDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8ComponentDefinition getEditorComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.EDITOR_COMPONENT_DEFINITION.toString());
    }

    public void setEditorComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.EDITOR_COMPONENT_DEFINITION.toString(), definition);
    }

    public T8ComponentDefinition getRendererComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.RENDERER_COMPONENT_DEFINITION.toString());
    }

    public void setRendererComponentDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.RENDERER_COMPONENT_DEFINITION.toString(), definition);
    }

    public String getColumnName()
    {
        return (String)getDefinitionDatum(Datum.COLUMN_NAME.toString());
    }

    public void setColumnName(String columnName)
    {
        setDefinitionDatum(Datum.COLUMN_NAME.toString(), columnName);
    }

    public boolean isEditable()
    {
        return (Boolean)getDefinitionDatum(Datum.EDITABLE.toString());
    }

    public void setEditable(boolean editable)
    {
        setDefinitionDatum(Datum.EDITABLE.toString(), editable);
    }

    public String getFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FIELD_IDENTIFIER.toString());
    }

    public void setFieldIdentifier(String sourceName)
    {
        setDefinitionDatum(Datum.FIELD_IDENTIFIER.toString(), sourceName);
    }

    public boolean isVisible()
    {
        return (Boolean)getDefinitionDatum(Datum.VISIBLE.toString());
    }

    public void setVisible(boolean visible)
    {
        setDefinitionDatum(Datum.VISIBLE.toString(), visible);
    }

    public int getWidth()
    {
        return (Integer)getDefinitionDatum(Datum.WIDTH.toString());
    }

    public void setWidth(int width)
    {
        setDefinitionDatum(Datum.WIDTH.toString(), width);
    }

    public Integer getMaxWidth()
    {
        return (Integer)getDefinitionDatum(Datum.MAX_WIDTH.toString());
    }

    public void setMaxWidth(Integer maxWidth)
    {
        setDefinitionDatum(Datum.MAX_WIDTH.toString(), maxWidth);
    }

    public Integer getMinWidth()
    {
        return (Integer)getDefinitionDatum(Datum.MIN_WIDTH.toString());
    }

    public void setMinWidth(Integer minWidth)
    {
        setDefinitionDatum(Datum.MIN_WIDTH.toString(), minWidth);
    }

    public boolean isRequired()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.REQUIRED));
    }

    public void setRequired(boolean required)
    {
        setDefinitionDatum(Datum.REQUIRED.toString(), required);
    }

    public String getDefaultValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString());
    }

    public void setDefaultValueExpression(String defaultValueExpression)
    {
        setDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString(), defaultValueExpression);
    }

    public String getCellEditableExpression()
    {
        return (String)getDefinitionDatum(Datum.CELL_EDITABLE_EXPRESSION.toString());
    }

    public void setCellEditableExpression(String cellEditableExpression)
    {
        setDefinitionDatum(Datum.CELL_EDITABLE_EXPRESSION.toString(), cellEditableExpression);
    }
}
