package com.pilog.t8.ui.datasearch.combobox;

import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDataSourceDefinition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class ComboBoxModelRootNode implements Serializable
{
    private final List<ComboBoxModelGroupNode> groups;
    private boolean completeDataSet;
    private boolean searchVisible;
    private boolean optionsVisible;
    private T8DataSearchComboBoxDataSourceDefinition.SelectionMode selectionMode;

    public ComboBoxModelRootNode()
    {
        this.groups = new ArrayList<>();
    }

    public void addGroupNode(ComboBoxModelGroupNode node)
    {
        groups.add(node);
    }

    public void removeGroupNode(ComboBoxModelGroupNode node)
    {
        groups.remove(node);
    }

    public List<ComboBoxModelGroupNode> getGroupNodes()
    {
        return new ArrayList<>(groups);
    }

    public boolean isCompleteDataSet()
    {
        return completeDataSet;
    }

    public void setCompleteDataSet(boolean completeDataSet)
    {
        this.completeDataSet = completeDataSet;
    }

    public boolean isSearchVisible()
    {
        return searchVisible;
    }

    public void setSearchVisible(boolean searchVisible)
    {
        this.searchVisible = searchVisible;
    }

    public boolean isOptionsVisible()
    {
        return optionsVisible;
    }

    public void setOptionsVisible(boolean optionsVisible)
    {
        this.optionsVisible = optionsVisible;
    }

    public T8DataSearchComboBoxDataSourceDefinition.SelectionMode getSelectionMode()
    {
        return selectionMode;
    }

    public void setSelectionMode(
            T8DataSearchComboBoxDataSourceDefinition.SelectionMode selectionMode)
    {
        this.selectionMode = selectionMode;
    }
}
