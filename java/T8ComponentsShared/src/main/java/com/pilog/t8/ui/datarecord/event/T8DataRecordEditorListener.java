package com.pilog.t8.ui.datarecord.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordEditorListener extends EventListener
{
    public void recordValueChanged(T8ValueEditorValueChangedEvent event);
}
