package com.pilog.t8.definition.ui.table.object;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.data.object.T8ComponentDataObjectProvider;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;

/**
 * @author Bouwer du Preez
 */
public class T8TableScriptObjectProviderDefinition extends T8TableDataObjectProviderDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_TABLE_DATA_OBJECT_PROVIDER_SCRIPT";
    public static final String DISPLAY_NAME = "Table Script Data Object Provider";
    public static final String DESCRIPTION = "A definition of an object provider that creates objects using an EPIC script.";
    public enum Datum {SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8TableScriptObjectProviderDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SCRIPT_DEFINITION.toString(), "Script", "The script to execute."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableDataObjectProviderScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ComponentDataObjectProvider getNewDataObjectProviderInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.table.object.T8TableScriptDataObjectProvider").getConstructor(T8ComponentController.class, T8TableScriptObjectProviderDefinition.class);
        return (T8ComponentDataObjectProvider)constructor.newInstance(controller, this);
    }

    public T8TableDataObjectProviderScriptDefinition getScriptDefinition()
    {
        return (T8TableDataObjectProviderScriptDefinition)getDefinitionDatum(Datum.SCRIPT_DEFINITION.toString());
    }

    public void setScriptDefinition(T8TableDataObjectProviderScriptDefinition definition)
    {
        setDefinitionDatum(Datum.SCRIPT_DEFINITION.toString(), definition);
    }
}
