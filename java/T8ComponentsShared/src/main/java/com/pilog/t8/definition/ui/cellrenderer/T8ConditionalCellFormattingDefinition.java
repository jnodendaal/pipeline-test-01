package com.pilog.t8.definition.ui.cellrenderer;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ConditionalCellFormattingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_CONDITIONAL_CELL_FORMATTING";
    public static final String GROUP_IDENTIFIER = "@DG_UI_CONDITIONAL_CELL_FORMATTING";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Conditional Cell Formatting";
    public static final String DESCRIPTION = "A set of cell formatting rules that are applied conditionally based on the content of a cell.";
    private enum Datum
    {
        CONDITION_EXPRESSION,
        BACKGROUND_COLOR,
        SELECTED_BACKGROUND_COLOR,
        FONT_DEFINITION,
        ICON_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;
    public T8ConditionalCellFormattingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The boolean condition expression that is evaluated using the cell contents as input."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.COLOR_HEX, Datum.BACKGROUND_COLOR.toString(), "Background Color", "The color to set on the cell background."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.COLOR_HEX, Datum.SELECTED_BACKGROUND_COLOR.toString(), "Selected Background Color", "The color to set on the cell background when the cell is selected."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FONT_DEFINITION.toString(), "Font", "The font to use when painting the text."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this button's text."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FONT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8FontDefinition.TYPE_IDENTIFIER));
        else if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumId)
    {
        if (Datum.FONT_DEFINITION.toString().equals(datumId))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class, String.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumId), T8FontDefinition.TYPE_IDENTIFIER);
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return super.getDatumEditor(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconIdentifier;

        // Pre-load the icon definition if one is specified.
        iconIdentifier = getIconIdentifier();
        if (iconIdentifier != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconIdentifier, inputParameters);
        }
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpresion(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public Color getBackgroundColor()
    {
        String colorHex;

        colorHex = (String)getDefinitionDatum(Datum.BACKGROUND_COLOR.toString());
        return colorHex != null ? T8DefinitionDatumUtilities.getColor(colorHex) : null;
    }

    public void setBackgroundColor(Color color)
    {
        setDefinitionDatum(Datum.BACKGROUND_COLOR.toString(), T8DefinitionDatumUtilities.getColorHex(color));
    }

    public Color getSelectedBackgroundColor()
    {
        String colorHex;

        colorHex = (String)getDefinitionDatum(Datum.SELECTED_BACKGROUND_COLOR.toString());
        return colorHex != null ? T8DefinitionDatumUtilities.getColor(colorHex) : null;
    }

    public void setSelectedBackgroundColor(Color color)
    {
        setDefinitionDatum(Datum.SELECTED_BACKGROUND_COLOR.toString(), T8DefinitionDatumUtilities.getColorHex(color));
    }

    public T8FontDefinition getFontDefinition()
    {
        return (T8FontDefinition)getDefinitionDatum(Datum.FONT_DEFINITION.toString());
    }

    public void setFontDefinition(T8FontDefinition fontDefinition)
    {
         setDefinitionDatum(Datum.FONT_DEFINITION.toString(), fontDefinition);
    }

    public String getIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }
}
