package com.pilog.t8.definition.ui.flowpane;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DynamicButtonFlowPane and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DynamicButtonFlowPaneAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_BUTTON_CLICKED = "$CE_BUTTON_CLICKED";
    public static final String OPERATION_ADD_BUTTON = "$CO_ADD_BUTTON";
    public static final String OPERATION_REMOVE_BUTTON = "$CO_REMOVE_BUTTON";
    public static final String OPERATION_CLEAR_BUTTONS = "$CO_CLEAR_BUTTONS";
    public static final String PARAMETER_BUTTON_IDENTIFIER = "$P_BUTTON_IDENTIFIER";
    public static final String PARAMETER_BUTTON_TEXT = "$P_BUTTON_TEXT";
    public static final String PARAMETER_BUTTON_ICON_IDENTIFIER = "$P_BUTTON_ICON_IDENTIFIER";
    public static final String PARAMETER_BUTTON_TOOLTIP_TEXT = "$P_BUTTON_TOOLTIP_TEXT";
    public static final String PARAMETER_CLEAR_BUTTONS = "$P_CLEAR_BUTTONS";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_BUTTON_CLICKED);
        newEventDefinition.setMetaDisplayName("Button Clicked");
        newEventDefinition.setMetaDescription("This event occurs when one of the dynamic buttons on the flow pane is clicked.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BUTTON_IDENTIFIER, "Button Identifier", "The Identifier of the button that was clicked.", T8DataType.STRING));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_BUTTON);
        newOperationDefinition.setMetaDisplayName("Add Button");
        newOperationDefinition.setMetaDescription("Adds a button to the flow pane.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BUTTON_IDENTIFIER, "Button Identifier", "The Identifier of the button to add.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BUTTON_TEXT, "Button Text", "The text to display on the new button.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BUTTON_ICON_IDENTIFIER, "Button Icon", "The Identifier of the icon to display on the new button.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BUTTON_TOOLTIP_TEXT, "Button Tooltip Text", "The Tooltip Text to display when the user mouses over the new button.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_CLEAR_BUTTONS, "Clear Buttons", "A Boolean flag indicating whether or not existing buttons on the flow pane should be cleared before adding the new button (default=false).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REMOVE_BUTTON);
        newOperationDefinition.setMetaDisplayName("Remove Button");
        newOperationDefinition.setMetaDescription("Removes a button from the flow pane.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_BUTTON_IDENTIFIER, "Button Identifier", "The Instance of the button to remove.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_BUTTONS);
        newOperationDefinition.setMetaDisplayName("Clear Buttons");
        newOperationDefinition.setMetaDescription("Removes all buttons from the flow pane.");
        operations.add(newOperationDefinition);

        return operations;
    }
}
