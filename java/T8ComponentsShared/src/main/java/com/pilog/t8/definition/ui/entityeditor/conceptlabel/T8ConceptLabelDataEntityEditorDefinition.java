/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.definition.ui.entityeditor.conceptlabel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityFieldEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.textarea.T8TextAreaDataEntityEditorAPIHandler;
import com.pilog.t8.definition.ui.label.T8LabelDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8ConceptLabelDataEntityEditorDefinition extends T8LabelDefinition implements T8DataEntityFieldEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_ENTITY_EDITOR_CONCEPT_LABEL";
    public static final String DISPLAY_NAME = "Data Entity Concept Label";
    public static final String DESCRIPTION = "An entity editor component that displays the transalated concept of the data entity.";
    public enum Datum {PARENT_EDITOR_IDENTIFIER,
                       DATA_ENTITY_IDENTIFIER,
                       FIELD_IDENTIFIER,
                       DISPLAY_TYPE,
                       TOOLTIP_DISPLAY_TYPE};
    // -------- Definition Meta-Data -------- //

    public enum DisplayType {
        TERM,
        ABBREVIATION,
        CODE,
        DEFINITION,
        DEFAULT
    }

    public T8ConceptLabelDataEntityEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PARENT_EDITOR_IDENTIFIER.toString(), "Parent Data Entity Editor", "The data entity editor on which this editor is dependent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity to which this editor is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FIELD_IDENTIFIER.toString(), "Data Entity Field", "The data entity field that will be editable by this text field.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DISPLAY_TYPE.toString(), "Display Type", "The translation that will be displayed to the used.", DisplayType.DEFAULT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TOOLTIP_DISPLAY_TYPE.toString(), "Tooltip Display Type", "The translation that will be displayed to the used (overrides fixed tooltip if set).", DisplayType.DEFAULT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARENT_EDITOR_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8Definition> ancestors;
            ArrayList<T8DefinitionDatumOption> options;

            ancestors = getAncestorDefinitions();
            options = new ArrayList<>();
            for (T8Definition ancestor : ancestors)
            {
                if (ancestor instanceof T8DataEntityEditorDefinition)
                {
                    options.add(new T8DefinitionDatumOption(ancestor.getIdentifier(), ancestor.getIdentifier()));
                }
            }

            return options;
        }
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getEditorDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>();
        } else if (Datum.DISPLAY_TYPE.toString().equals(datumIdentifier) || Datum.TOOLTIP_DISPLAY_TYPE.toString().equals(datumIdentifier))
        {
            return createStringOptions(DisplayType.values());
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if(Strings.isNullOrEmpty(getEditorDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(Strings.isNullOrEmpty(getEditorDataEntityFieldIdentifier())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.FIELD_IDENTIFIER.toString(), "Data Entity Field Identifier Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(getDisplayType() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.DISPLAY_TYPE.toString(), "Display Type Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if(getTooltipDisplayType()== null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), Datum.TOOLTIP_DISPLAY_TYPE.toString(), "Tooltip Display Type Not Set.", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    protected void performValidations() throws Exception
    {
        if(Strings.isNullOrEmpty(getEditorDataEntityIdentifier())) throw new Exception("Data Entity Identifier not set for definition " + getPublicIdentifier());
        if(Strings.isNullOrEmpty(getEditorDataEntityFieldIdentifier())) throw new Exception("Data Entity Field Identifier not set for definition " + getPublicIdentifier());
        if(getDisplayType() == null) throw new Exception("Display Type not set for definition " + getPublicIdentifier());
        if(getTooltipDisplayType()== null) throw new Exception("Tooltip Display Type not set for definition " + getPublicIdentifier());
    }

    @Override
    public T8DataEntityEditorComponent getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.entityeditor.conceptlabel.T8ConceptLabelDataEntityEditor", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TextAreaDataEntityEditorAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TextAreaDataEntityEditorAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString());
    }

    @Override
    public void setParentEditorIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString(), identifier);
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    @Override
    public void setEditorDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    @Override
    public String getEditorDataEntityFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FIELD_IDENTIFIER.toString());
    }

    @Override
    public void setEditorDataEntityFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FIELD_IDENTIFIER.toString(), identifier);
    }

    public DisplayType getDisplayType()
    {
        String displayType = (String)getDefinitionDatum(Datum.DISPLAY_TYPE.toString());
        return displayType == null ? null : DisplayType.valueOf(displayType);
    }

    public void setDisplayType(DisplayType displayType)
    {
        setDefinitionDatum(Datum.DISPLAY_TYPE.toString(), displayType);
    }

    public DisplayType getTooltipDisplayType()
    {
        String displayType = (String)getDefinitionDatum(Datum.TOOLTIP_DISPLAY_TYPE.toString());
        return displayType != null ? DisplayType.valueOf(displayType) : null;
    }

    public void setTooltipDisplayType(DisplayType displayType)
    {
        setDefinitionDatum(Datum.TOOLTIP_DISPLAY_TYPE.toString(), displayType);
    }
}
