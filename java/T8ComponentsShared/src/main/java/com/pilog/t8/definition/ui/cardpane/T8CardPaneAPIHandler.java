package com.pilog.t8.definition.ui.cardpane;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8CardPaneAPIHandler and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8CardPaneAPIHandler
{
    // These should NOT be changed, only added to.
    // Component events
    public static final String EVENT_COMPONENT_SWITCHED = "$CE_CARD_SWITCHED";
    // Component operations
    public static final String OPERATION_CURRENT_COMPONENT = "$CO_CURRENT_COMPONENT";
    public static final String OPERATION_SHOW_COMPONENT = "$CO_SHOW_COMPONENT";
    // Component event/operation parameters
    public static final String PARAMETER_COMPONENT_IDENTIFIER = "$CP_COMPONENT_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_COMPONENT_SWITCHED);
        newEventDefinition.setMetaDisplayName("Component Switched");
        newEventDefinition.setMetaDescription("This event occurs when the visible component in the card pane is switched.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COMPONENT_IDENTIFIER, "Component Identifier", "The Identifier of the Component shown by the card pane.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SHOW_COMPONENT);
        newOperationDefinition.setMetaDisplayName("Show Component");
        newOperationDefinition.setMetaDescription("This operations shows the specified component.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COMPONENT_IDENTIFIER, "Component Identifier", "The Identifier of the Component to show.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CURRENT_COMPONENT);
        newOperationDefinition.setMetaDisplayName("Current Component");
        newOperationDefinition.setMetaDescription("This operation gets the identifier for the currently displayed component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COMPONENT_IDENTIFIER, "Component Identifier", "The Identifier of the Component being shown.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
