package com.pilog.t8.definition.ui.celleditor.table.predefined;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8PredefinedTableCellEditorDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_EDITOR_PREDEFINED";
    public static final String DISPLAY_NAME = "Predefined Cell Editor";
    public static final String DESCRIPTION = "A reference to a predefined cell editor component.";
    private enum Datum {CELL_EDITOR_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8ComponentDefinition editorDefinition;

    public T8PredefinedTableCellEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CELL_EDITOR_IDENTIFIER.toString(), "Table Cell Editor", "The identifier of a predefined cell editor."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CELL_EDITOR_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ComponentDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public String getCellEditorIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CELL_EDITOR_IDENTIFIER.toString());
    }

    public void setCellEditorIdentifier(String moduleIdentifier)
    {
        setDefinitionDatum(Datum.CELL_EDITOR_IDENTIFIER.toString(), moduleIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String cellEditorId;

        cellEditorId = getCellEditorIdentifier();
        configurationSettings = new HashMap<String, Object>();
        if ((cellEditorId != null) && (cellEditorId.length() > 0))
        {
            T8DefinitionManager definitionManager;

            definitionManager = context.getServerContext().getDefinitionManager();
            editorDefinition = (T8ComponentDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), cellEditorId, inputParameters);

            // Copy the module definition, renaming it to the sub-module identifier.
            editorDefinition = (T8ComponentDefinition)definitionManager.copyDefinition(context, editorDefinition, getIdentifier());
        }
        else throw new Exception("No Cell Editor Identifier set in " + this.getClass().getName() + ".");
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        editorDefinition.setEventHandlerScriptDefinitions(getEventHandlerScriptDefinitions());
        return editorDefinition.getNewComponentInstance(controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        if (editorDefinition != null)
        {
            return editorDefinition.getComponentInputParameterDefinitions();
        }
        else return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        if (editorDefinition != null)
        {
            return editorDefinition.getComponentEventDefinitions();
        }
        else return new ArrayList<T8ComponentEventDefinition>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        if (editorDefinition != null)
        {
            return editorDefinition.getComponentOperationDefinitions();
        }
        return new ArrayList<T8ComponentOperationDefinition>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }
}
