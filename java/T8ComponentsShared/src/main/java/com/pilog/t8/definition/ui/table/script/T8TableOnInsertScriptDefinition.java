package com.pilog.t8.definition.ui.table.script;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TableOnInsertScriptDefinition extends T8DefaultScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_TABLE_ON_INSERT";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_TABLE_ON_INSERT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "T8Table: On-Insert Script";
    public static final String DESCRIPTION = "A script that is executed on the client-side when the 'Add New Row' toolbar button is clicked on the T8Table.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_INPUT_ENTITY_LIST = "$P_INPUT_ENTITY_LIST";
    public static final String PARAMETER_OUTPUT_ENTITY_LIST = "$P_OUTPUT_ENTITY_LIST";

    public T8TableOnInsertScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterDefinitions;
        T8DataParameterDefinition parameterDefinition;

        parameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_INPUT_ENTITY_LIST, PARAMETER_INPUT_ENTITY_LIST, "The input list of data entities to form part of the deletion change.", T8DataType.LIST);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        return parameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterDefinitions;
        T8DataParameterDefinition parameterDefinition;

        parameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_OUTPUT_ENTITY_LIST, PARAMETER_OUTPUT_ENTITY_LIST, "The output list of data entities to form part of the deletion change.", T8DataType.LIST);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        return parameterDefinitions;
    }
}
