package com.pilog.t8.definition.ui.chart.container;

import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8ChartContainerAPIHandler and responds to specific
 * requests regarding these definitions.  It is extremely important that only
 * new definitions are added to this class and none of the old definitions
 * removed or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ChartContainerAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_REFRESH_CHART = "$CO_REFRESH_CHART";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH_CHART);
        newOperationDefinition.setMetaDisplayName("Refresh Chart");
        newOperationDefinition.setMetaDescription("This operations refreshes the chart data set.");
        operations.add(newOperationDefinition);

        return operations;
    }
}
