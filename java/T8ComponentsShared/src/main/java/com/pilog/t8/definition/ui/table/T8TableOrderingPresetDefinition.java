package com.pilog.t8.definition.ui.table;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8TableOrderingPresetDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_TABLE_PRESET_ORDERING";
    public static final String TYPE_IDENTIFIER = "@DT_UI_TABLE_PRESET_ORDERING";
    public static final String DISPLAY_NAME = "Ordering Preset";
    public static final String DESCRIPTION = "Table ordering preset. All the presets on the table will be made available to the user as selectable options, the user can then select an option to order the data by.";
    public static final String IDENTIFIER_PREFIX = "PO_";
    public enum Datum
    {
        PRESET_NAME,
        FIELD_ODERING_MAP
    };
    // -------- Definition Meta-Data -------- //

    public T8TableOrderingPresetDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.PRESET_NAME.toString(), "Name", "The preset name for this preset ordering that will be displayed to the user."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER_STRING_MAP, Datum.FIELD_ODERING_MAP.toString(), "Data Ordering", "The ordering that will be applied to data retrieved by the table if the user selects this preset."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FIELD_ODERING_MAP.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new LinkedHashMap<String, List<String>>();
                    if (fieldDefinitions != null)
                    {
                        ArrayList<String> options;

                        options = new ArrayList<String>();
                        options.add(OrderMethod.ASCENDING.toString());
                        options.add(OrderMethod.DESCENDING.toString());
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            identifierMap.put(fieldDefinition.getPublicIdentifier(), options);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
    }

    private String getDataEntityIdentifier()
    {
        T8TableDefinition ancestorDefinition;

        ancestorDefinition = (T8TableDefinition) getAncestorDefinition(T8TableDefinition.TYPE_IDENTIFIER);
        return ancestorDefinition.getDataEntityIdentifier();
    }

    public void setPresetName(String displayName)
    {
        setDefinitionDatum(Datum.PRESET_NAME.toString(), displayName);
    }

    public String getPresetName()
    {
        return (String)getDefinitionDatum(Datum.PRESET_NAME.toString());
    }

    public void setFieldOrderingMap(Map<String, OrderMethod> orderingMap)
    {
        setDefinitionDatum(Datum.FIELD_ODERING_MAP.toString(), orderingMap);
    }

    public LinkedHashMap<String, OrderMethod> getFieldOrderingMap()
    {
        Map<String, String> orderMapDatum;

        orderMapDatum = (Map<String, String>)getDefinitionDatum(Datum.FIELD_ODERING_MAP.toString());
        if (orderMapDatum != null)
        {
            LinkedHashMap<String, OrderMethod> orderMap;

            orderMap = new LinkedHashMap<String, OrderMethod>();
            for (String fieldIdentifier : orderMapDatum.keySet())
            {
                String orderMethod;

                orderMethod = orderMapDatum.get(fieldIdentifier);
                if (orderMethod != null)
                {
                    orderMap.put(fieldIdentifier, OrderMethod.valueOf(orderMethod));
                }
            }

            return orderMap;
        }
        else return null;
    }
}
