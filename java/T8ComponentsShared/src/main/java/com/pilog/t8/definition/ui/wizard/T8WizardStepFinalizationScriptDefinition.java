package com.pilog.t8.definition.ui.wizard;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WizardStepFinalizationScriptDefinition extends T8ComponentControllerScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_UI_COMPONENT_WIZARD_STEP_FINALIZATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_UI_COMPONENT_WIZARD_STEP_FINALIZATION";
    public static final String IDENTIFIER_PREFIX = "SCRIPT_FIN_";
    public static final String DISPLAY_NAME = "Step Finalization Script";
    public static final String DESCRIPTION = "A script that is executed when the current step in the wizard is completed.";
    // -------- Definition Meta-Data -------- //

    public static final String P_CONTINUE = "$P_CONTINUE";
    public static final String P_NEXT_STEP_IDENTIFIER = "$P_NEXT_STEP_IDENTIFIER";

    public T8WizardStepFinalizationScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8WizardDefinition wizardDefinition;

        wizardDefinition = (T8WizardDefinition)getAncestorDefinition(T8WizardDefinition.TYPE_IDENTIFIER);
        if (wizardDefinition != null)
        {
            return wizardDefinition.getWizardParameterDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> outputParameterDefinitions;
        T8WizardDefinition wizardDefinition;

        // Add the default parameters.
        outputParameterDefinitions = new ArrayList<>();
        outputParameterDefinitions.add(new T8DataParameterDefinition(P_CONTINUE, "Continue", "If the user is allowed to continue to the next step.", T8DataType.BOOLEAN));
        outputParameterDefinitions.add(new T8DataParameterDefinition(P_NEXT_STEP_IDENTIFIER, "Next Step", "The identifier of the next step to which the wizard will navigate.", T8DataType.DEFINITION_IDENTIFIER));

        // Add all of the wizard parameters.
        wizardDefinition = (T8WizardDefinition)getAncestorDefinition(T8WizardDefinition.TYPE_IDENTIFIER);
        if (wizardDefinition != null)
        {
            outputParameterDefinitions.addAll(wizardDefinition.getWizardParameterDefinitions());
        }

        // Return the complete list of valid output parameter definitions.
        return outputParameterDefinitions;
    }
}
