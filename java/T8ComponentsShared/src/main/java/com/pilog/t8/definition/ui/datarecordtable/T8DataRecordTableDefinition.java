package com.pilog.t8.definition.ui.datarecordtable;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_RECORD_TABLE";
    public static final String IDENTIFIER_PREFIX = "C_DATA_RECORD_TABLE_";
    public static final String DISPLAY_NAME = "Data Record Table";
    public static final String DESCRIPTION = "A component that displays a filtered collection of data records in a tabular view and allows the user to edit the content of the documents.";
    public static final String VERSION = "0";
    public enum Datum {PAGE_SIZE,
                       ROW_HEIGHT,
                       AUTO_RESIZE_MODE,
                       AUTO_RETRIEVE,
                       INSERT_ENABLED,
                       DELETE_ENABLED,
                       COMMIT_ENABLED,
                       USER_FILTER_ENABLED,
                       USER_REFRESH_ENABLED,
                       OPTIONS_ENABLED,
                       TOOL_BAR_VISIBLE,
                       EXPORT_ENABLED,
                       LANGUAGE_SWITCH_ENABLED,
                       DATA_REQUIREMENT_INSTANCE_IDENTIFIER,
                       RECORD_DOCUMENT_DE_IDENTIFIER,
                       RECORD_STRUCTURE_DE_IDENTIFIER,
                       RECORD_DE_IDENTIFIER,
                       REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER,
                       REQUIREMENT_INSTANCE_DE_IDENTIFIER,
                       TERMINOLOGY_DE_IDENTIFIER,
                       ISO_LANGUAGE_DE_IDENTIFIER,
                       DATA_RECORD_DESCRIPTION_DE_IDENTIFIER,
                       DESCRIPTION_CLASSIFICATION_IDENTIFIER,
                       DATA_RECORD_ACCESS_IDENTIFIER,
                       CONTROLLED_VALUE_LOOKUP_DIALOG_DEFINITION,
                       DATA_REQUIREMENT_INSTANCE_LOOKUP_DIALOG_DEFINITION,
                       PREFILTER_DEFINITIONS,
                       NEW_RECORD_TAG_INHERITANCE_LIST};
    // -------- Definition Meta-Data -------- //

    public enum AutoResizeMode {OFF, SUBSEQUENT_COLUMNS, LAST_COLUMN, ALL_COLUMNS, NEXT_COLUMN}

    public T8DataRecordTableDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PAGE_SIZE.toString(), "Page Size", "The number of records that will be retrieved from the data source per page.", 20));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROW_HEIGHT.toString(), "Row Height", "The display height (in pixels) of each row in the table.", 25));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AUTO_RESIZE_MODE.toString(), "Auto Resize Mode", "The resize mode of the table.  This setting indicates how columns will be resized relative to each other when the available display area changes.", AutoResizeMode.OFF.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_RETRIEVE.toString(), "Auto Retrieve", "A boolean setting that enables automatic retrieval of table data when the table is initialized.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INSERT_ENABLED.toString(), "Insert Enabled", "A boolean setting that enables (if true) the insertion of rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DELETE_ENABLED.toString(), "Delete Enabled", "A boolean setting that enables (if true) the deletion of rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COMMIT_ENABLED.toString(), "Commit Enabled", "A boolean setting that enables (if true) the comittal of changes to rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_FILTER_ENABLED.toString(), "Filter Enabled", "A boolean setting that enables (if true) the filtering of rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_REFRESH_ENABLED.toString(), "Refresh Enabled", "A boolean setting that enables (if true) the refresh option on the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPTIONS_ENABLED.toString(), "Options Enabled", "A boolean setting that enables (if true) the options menu available from the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TOOL_BAR_VISIBLE.toString(), "Tool Bar Visible", "A boolean setting that sets the visibility of the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EXPORT_ENABLED.toString(), "Export Enabled", "A boolean setting that enables (if true) the option to export the document displayed by the editor.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LANGUAGE_SWITCH_ENABLED.toString(), "Language Switch Enabled", "A boolean setting that enables (if true) the option to switch the dictionary language used to display the document in the editor.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString(), "Data Requirement Instance", "The data requirement instance on which the records displayed by the table will be based on."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_DOCUMENT_DE_IDENTIFIER.toString(), "Data Record Document Entity", "The data entity from which data record documents are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_STRUCTURE_DE_IDENTIFIER.toString(), "Data Record Structure Entity", "The data entity from which data record document relationships are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_DE_IDENTIFIER.toString(), "Data Record Entity", "The data entity from which the list of available data records and their value Strings will be retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER.toString(), "Data Requirement Instance Document Entity", "The data entity from which data requirement instance documents are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString(), "Data Requirement Instance Entity", "The data entity from which the list of available data requirement instance documents and their titles are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TERMINOLOGY_DE_IDENTIFIER.toString(), "Terminology Entity", "The data entity from which ontological and terminological data is retrieved for concepts."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ISO_LANGUAGE_DE_IDENTIFIER.toString(), "ISO Language Entity", "The data entity from which ISO language data is retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_DESCRIPTION_DE_IDENTIFIER.toString(), "Data Record Description Entity", "The data entity from which record descriptions are retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DESCRIPTION_CLASSIFICATION_IDENTIFIER.toString(), "Description Classification Identifier", "The description classification type that will be used for display purposes by the record editor."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString(), "Data Record Access", "The access rights to data displayed/edited in the editor."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CONTROLLED_VALUE_LOOKUP_DIALOG_DEFINITION.toString(), "Controlled Value Lookup Dialog", "The lookup dialog to use when the user has to select a controlled value from the organization structure."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PREFILTER_DEFINITIONS.toString(), "Prefilters", "The prefilters that will be applied when the content data of the table is retrieved."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.NEW_RECORD_TAG_INHERITANCE_LIST.toString(), "New Record Inherited Tags", "The identifiers of the tags that will be inherited from parent records when new records are inserted."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.DATA_REQUIREMENT_INSTANCE_LOOKUP_DIALOG_DEFINITION.toString(), "Data Requirement Instance Lookup Dialog", "The lookup dialog to use when the user has to select a data requirement instance from the organization structure."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.AUTO_RESIZE_MODE.toString().equals(datumIdentifier)) return createStringOptions(AutoResizeMode.values());
        else if (Datum.RECORD_DOCUMENT_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.RECORD_STRUCTURE_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.RECORD_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TERMINOLOGY_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.ISO_LANGUAGE_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_RECORD_DESCRIPTION_DE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataRecordAccessDefinition.GROUP_IDENTIFIER));
        else if (Datum.CONTROLLED_VALUE_LOOKUP_DIALOG_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataLookupDialogDefinition.TYPE_IDENTIFIER));
        else if (Datum.PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_REQUIREMENT_INSTANCE_LOOKUP_DIALOG_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataLookupDialogDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.datarecordtable.T8DataRecordTable").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return T8DataRecordTableAPIHandler.getInputParameterDefinitions();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataRecordTableAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataRecordTableAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public AutoResizeMode getAutoResizeMode()
    {
        return AutoResizeMode.valueOf((String)getDefinitionDatum(Datum.AUTO_RESIZE_MODE.toString()));
    }

    public void setAutoResizeMode(AutoResizeMode autoResizeMode)
    {
        setDefinitionDatum(Datum.AUTO_RESIZE_MODE.toString(), autoResizeMode.toString());
    }

    public int getPageSize()
    {
        return (Integer)getDefinitionDatum(Datum.PAGE_SIZE.toString());
    }

    public void setPageSize(int pageSize)
    {
        setDefinitionDatum(Datum.PAGE_SIZE.toString(), pageSize);
    }

    public int getRowHeight()
    {
        return (Integer)getDefinitionDatum(Datum.ROW_HEIGHT.toString());
    }

    public void setRowHeight(int rowHeight)
    {
        setDefinitionDatum(Datum.ROW_HEIGHT.toString(), rowHeight);
    }

    public boolean isAutoRetrieve()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.AUTO_RETRIEVE.toString());
        return ((enabled != null) && enabled);
    }

    public void setAutoRetrieve(boolean enabled)
    {
        setDefinitionDatum(Datum.AUTO_RETRIEVE.toString(), enabled);
    }


    public boolean isInsertEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.INSERT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setInsertEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.INSERT_ENABLED.toString(), enabled);
    }

    public boolean isDeleteEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.DELETE_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setDeleteEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.DELETE_ENABLED.toString(), enabled);
    }

    public boolean isCommitEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.COMMIT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setCommitEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.COMMIT_ENABLED.toString(), enabled);
    }

    public boolean isUserFilterEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_FILTER_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserFilterEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_FILTER_ENABLED.toString(), enabled);
    }

    public boolean isUserRefreshEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_REFRESH_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserRefreshEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_REFRESH_ENABLED.toString(), enabled);
    }

    public boolean isOptionsEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.OPTIONS_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setOptionsEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.OPTIONS_ENABLED.toString(), enabled);
    }

    public boolean isToolBarVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString());
        return ((visible != null) && visible);
    }

    public void setToolBarVisible(boolean visible)
    {
        setDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString(), visible);
    }

    public boolean isExportEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.EXPORT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setExportEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.EXPORT_ENABLED.toString(), enabled);
    }

    public boolean isLanguageSwitchEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.LANGUAGE_SWITCH_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setLanguageSwitchEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.LANGUAGE_SWITCH_ENABLED.toString(), enabled);
    }

    public String getDataRequirementInstanceIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString());
    }

    public void setDataRequirementInstanceIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRequirementInstanceDocumentDEID()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER.toString());
    }

    public void setDataRequirementInstanceDocumentDEID(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRequirementInstanceDEID()
    {
        return (String)getDefinitionDatum(Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString());
    }

    public void setDataRequirementInstanceDEID(String identifier)
    {
        setDefinitionDatum(Datum.REQUIREMENT_INSTANCE_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordDocumentDEID()
    {
        return (String)getDefinitionDatum(Datum.RECORD_DOCUMENT_DE_IDENTIFIER.toString());
    }

    public void setDataRecordDocumentDEID(String identifier)
    {
        setDefinitionDatum(Datum.RECORD_DOCUMENT_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordStructureDEID()
    {
        return (String)getDefinitionDatum(Datum.RECORD_STRUCTURE_DE_IDENTIFIER.toString());
    }

    public void setDataRecordStructureDEID(String identifier)
    {
        setDefinitionDatum(Datum.RECORD_DOCUMENT_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordDEID()
    {
        return (String)getDefinitionDatum(Datum.RECORD_DE_IDENTIFIER.toString());
    }

    public void setDataRecordDEID(String identifier)
    {
        setDefinitionDatum(Datum.RECORD_DE_IDENTIFIER.toString(), identifier);
    }

    public String getTerminologyDEID()
    {
        return (String)getDefinitionDatum(Datum.TERMINOLOGY_DE_IDENTIFIER.toString());
    }

    public void setTerminologyDEID(String identifier)
    {
        setDefinitionDatum(Datum.TERMINOLOGY_DE_IDENTIFIER.toString(), identifier);
    }

    public String getISOLanguageDEID()
    {
        return (String)getDefinitionDatum(Datum.ISO_LANGUAGE_DE_IDENTIFIER.toString());
    }

    public void setISOLanguageDEID(String identifier)
    {
        setDefinitionDatum(Datum.ISO_LANGUAGE_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordDescriptionDEID()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_DESCRIPTION_DE_IDENTIFIER.toString());
    }

    public void setDataRecordDescriptionDEID(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_DESCRIPTION_DE_IDENTIFIER.toString(), identifier);
    }

    public String getDescriptionClassificationIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DESCRIPTION_CLASSIFICATION_IDENTIFIER.toString());
    }

    public void setDescriptionClassificationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DESCRIPTION_CLASSIFICATION_IDENTIFIER.toString(), identifier);
    }

    public String getDataRecordAccessIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString());
    }

    public void setDataRecordAccessIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString(), identifier);
    }

    public T8DataLookupDialogDefinition getControlledValueLookupDialogDefinition()
    {
        return (T8DataLookupDialogDefinition)getDefinitionDatum(Datum.CONTROLLED_VALUE_LOOKUP_DIALOG_DEFINITION.toString());
    }

    public void setControlledValueLookupDialogDefinition(T8DataLookupDialogDefinition definition)
    {
        setDefinitionDatum(Datum.CONTROLLED_VALUE_LOOKUP_DIALOG_DEFINITION.toString(), definition);
    }

    public List<T8DataFilterDefinition> getPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString());
    }

    public void setPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public List<String> getNewRecordTagInheritanceList()
    {
        return (List<String>)getDefinitionDatum(Datum.NEW_RECORD_TAG_INHERITANCE_LIST.toString());
    }

    public void setNewRecordTagInheritanceList(List<String> tagList)
    {
        setDefinitionDatum(Datum.NEW_RECORD_TAG_INHERITANCE_LIST.toString(), tagList);
    }

    public T8DataLookupDialogDefinition getDataRequirementInstanceLookupDialogDefinition()
    {
        return (T8DataLookupDialogDefinition)getDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_LOOKUP_DIALOG_DEFINITION.toString());
    }

    public void getDataRequirementInstanceLookupDialogDefinition(T8DataLookupDialogDefinition definition)
    {
        setDefinitionDatum(Datum.DATA_REQUIREMENT_INSTANCE_LOOKUP_DIALOG_DEFINITION.toString(), definition);
    }
}
