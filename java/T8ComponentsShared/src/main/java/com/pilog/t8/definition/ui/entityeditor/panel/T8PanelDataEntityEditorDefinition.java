package com.pilog.t8.definition.ui.entityeditor.panel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityEditorDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorDefinition extends T8PanelDefinition implements T8DataEntityEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_ENTITY_EDITOR_PANEL";
    public static final String DISPLAY_NAME = "Panel Data Entity Editor";
    public static final String DESCRIPTION = "A container to which other UI components can be added and which provides functionality for editing a single data entity.";
    public enum Datum {
        PARENT_EDITOR_IDENTIFIER,
        DATA_ENTITY_IDENTIFIER,
        PREFILTER_DEFINITIONS,
        INSERT_ENABLED,
        DELETE_ENABLED,
        COMMIT_ENABLED,
        USER_FILTER_ENABLED,
        USER_REFRESH_ENABLED,
        OPTIONS_ENABLED,
        TOOL_BAR_VISIBLE
    };
    // -------- Definition Meta-Data -------- //

    public T8PanelDataEntityEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.PARENT_EDITOR_IDENTIFIER.toString(), "Parent Data Entity Editor", "The data entity editor on which this editor is dependent."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity to which this editor is applicable."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PREFILTER_DEFINITIONS.toString(), "Prefilters", "The prefilters that will be applied when the content data of entity editor is retrieved."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INSERT_ENABLED.toString(), "Insert Enabled", "A boolean setting that enables (if true) the insertion of rows/entities in the editor.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DELETE_ENABLED.toString(), "Delete Enabled", "A boolean setting that enables (if true) the deletion of rows/entities in the editor.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COMMIT_ENABLED.toString(), "Commit Enabled", "A boolean setting that enables (if true) the comittal of changes to rows/entities in the editor.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_FILTER_ENABLED.toString(), "Filter Enabled", "A boolean setting that enables (if true) the filtering of rows/entities in the editor.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_REFRESH_ENABLED.toString(), "Refresh Enabled", "A boolean setting that enables (if true) the refresh option on the editor tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPTIONS_ENABLED.toString(), "Options Enabled", "A boolean setting that enables (if true) the options menu available from the editor tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TOOL_BAR_VISIBLE.toString(), "Tool Bar Visible", "A boolean setting that sets the visibility of the editor tool bar.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PARENT_EDITOR_IDENTIFIER.toString().equals(datumIdentifier))
        {
            List<T8Definition> ancestors;
            ArrayList<T8DefinitionDatumOption> options;

            ancestors = getAncestorDefinitions();
            options = new ArrayList<>();
            for (T8Definition ancestor : ancestors)
            {
                if (ancestor instanceof T8DataEntityEditorDefinition)
                {
                    options.add(new T8DefinitionDatumOption(ancestor.getIdentifier(), ancestor.getIdentifier()));
                }
            }

            return options;
        }
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.ui.entityeditor.panel.T8PanelDataEntityEditorActionHandler", new Class<?>[]{T8Context.class, this.getClass()}, context, this);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        // Validates if the Data Entity is set for the Data Lookup Field.
        validationErrors = new ArrayList<>();

        if(getPrefilterDefinitions()!= null)
        {
            for (T8DataFilterDefinition dataFilterDefinition : getPrefilterDefinitions())
            {
                if(!Objects.equals(getEditorDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Filter Entity identifier incorrect. Expected: " + getEditorDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        return validationErrors;
    }

    @Override
    public T8DataEntityEditorComponent getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.entityeditor.panel.T8PanelDataEntityEditor", new Class<?>[]{T8PanelDataEntityEditorDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8PanelDataEntityEditorAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8PanelDataEntityEditorAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return (ArrayList)getPanelSlotDefinitions();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return (String)getDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString());
    }

    @Override
    public void setParentEditorIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.PARENT_EDITOR_IDENTIFIER.toString(), identifier);
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    @Override
    public void setEditorDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public List<T8DataFilterDefinition> getPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString());
    }

    public void setPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public boolean isInsertEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.INSERT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setInsertEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.INSERT_ENABLED.toString(), enabled);
    }

    public boolean isDeleteEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.DELETE_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setDeleteEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.DELETE_ENABLED.toString(), enabled);
    }

    public boolean isCommitEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.COMMIT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setCommitEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.COMMIT_ENABLED.toString(), enabled);
    }

    public boolean isUserFilterEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_FILTER_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserFilterEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_FILTER_ENABLED.toString(), enabled);
    }

    public boolean isUserRefreshEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_REFRESH_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserRefreshEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_REFRESH_ENABLED.toString(), enabled);
    }

    public boolean isOptionsEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.OPTIONS_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setOptionsEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.OPTIONS_ENABLED.toString(), enabled);
    }

    public boolean isToolBarVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString());
        return ((visible != null) && visible);
    }

    public void setToolBarVisible(boolean visible)
    {
        setDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString(), visible);
    }
}
