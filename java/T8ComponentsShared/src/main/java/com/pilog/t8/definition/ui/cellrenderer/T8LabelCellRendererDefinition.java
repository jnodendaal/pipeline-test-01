package com.pilog.t8.definition.ui.cellrenderer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8LabelCellRendererDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_LABEL";
    public static final String DISPLAY_NAME = "Label List Cell Renderer";
    public static final String DESCRIPTION = "A cell editor that displays the content of a list cell as a simple text string.";
    public static final String IDENTIFIER_PREFIX = "C_RENDERER_";
    public enum Datum {CONDITIONAL_FORMATTING_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8LabelCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CONDITIONAL_FORMATTING_DEFINITIONS.toString(), "Conditional Formatting", "Formatting settings to be applied to cells based on the value of the cell."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONDITIONAL_FORMATTING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ConditionalCellFormattingDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public List<T8ConditionalCellFormattingDefinition> getConditionalFormattingDefinitions()
    {
        return getDefinitionDatum(Datum.CONDITIONAL_FORMATTING_DEFINITIONS);
    }

    public void setConditionalFormattingDefinitions(List<T8ConditionalCellFormattingDefinition> definitions)
    {
        setDefinitionDatum(Datum.CONDITIONAL_FORMATTING_DEFINITIONS, definitions);
    }
}
