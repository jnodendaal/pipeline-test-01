package com.pilog.t8.definition.ui.wizard;

/**
 * This class remains only for backwards compatibility.  It has been replaced by
 * T8WizardStepFinalizationScriptDefinition and will be removed in future.
 * 
 * @author hennie.brink@pilog.co.za
 */
public class T8WizardStepValidationScriptDefinition extends T8WizardStepFinalizationScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_UI_COMPONENT_WIZARD_STEP_VALIDATION";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_UI_COMPONENT_WIZARD_STEP_VALIDATION";
    public static final String DISPLAY_NAME = "Step Validation Script";
    public static final String DESCRIPTION = "A script that is executed before the next step is shown on the wizard.";
    public static final String IDENTIFIER_PREFIX = "INIT_";
    // -------- Definition Meta-Data -------- //

    public T8WizardStepValidationScriptDefinition(String identifier)
    {
        super(identifier);
    }
}
