package com.pilog.t8.definition.ui.combobox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataComboBox and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8ComboBoxAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGE = "$CE_SELECTION_CHANGE";
    public static final String OPERATION_GET_SELECTED_DATA_VALUE = "$CO_GET_SELECTED_DATA_VALUE";
    public static final String OPERATION_SET_SELECTED_DATA_VALUE = "$CO_SET_SELECTED_DATA_VALUE";
    public static final String OPERATION_ADD_ELEMENT = "$CO_ADD_ELEMENT";
    public static final String OPERATION_REMOVE_ELEMENT = "$CO_REMOVE_ELEMENT";
    public static final String PARAMETER_DATA_VALUE = "$CP_DATA_VALUE";
    public static final String PARAMETER_ELEMENT_NAME = "$CP_ELEMENT_NAME";
    public static final String PARAMETER_ELEMENT_IDENTIFIER = "$CP_ELEMENT_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGE);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selected item has changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE, "Data Value", "The currently selected value", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_VALUE);
        newOperationDefinition.setMetaDisplayName("Get Selected Data Value");
        newOperationDefinition.setMetaDescription("This operations returns the data value currently selected and displayed by the component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE, "Data Value", "The selected data value.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED_DATA_VALUE);
        newOperationDefinition.setMetaDisplayName("Set Selected Data Value");
        newOperationDefinition.setMetaDescription("This operations sets the data value currently selected and displayed by the component.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE, "Data Value", "The selected data value.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_ELEMENT);
        newOperationDefinition.setMetaDisplayName("Add Element");
        newOperationDefinition.setMetaDescription("This operation adds an element to the combo box.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_IDENTIFIER, "Element Identifier", "The identifier of the element to add.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_NAME, "Item Name", "The display name of the element to add.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_VALUE, "Data Value", "The data value of the element to add.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REMOVE_ELEMENT);
        newOperationDefinition.setMetaDisplayName("Remove Element");
        newOperationDefinition.setMetaDescription("This operation removes an element from the combo box.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ELEMENT_IDENTIFIER, "Element Identifier", "The identifier of the element to remove.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
