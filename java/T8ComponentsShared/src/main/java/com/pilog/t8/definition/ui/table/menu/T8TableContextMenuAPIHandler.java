/**
 * Created on 19 Oct 2015, 10:03:02 AM
 */
package com.pilog.t8.definition.ui.table.menu;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Gavin Boshoff
 */
public class T8TableContextMenuAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.

    // Component Operations
    public static final String OPERATION_SET_COPY_ROWS_VISIBLE = "$CO_SET_COPY_ROWS_VISIBLE";
    public static final String OPERATION_SET_EXPORT_VISIBLE = "$CO_SET_EXPORT_VISIBLE";
    public static final String OPERATION_SET_FILL_DOWN_VISIBLE = "$CO_SET_FILL_DOWN_VISIBLE";
    public static final String OPERATION_SET_MENU_ITEM_VISIBLE = "$CO_SET_MENU_ITEM_VISIBLE";

    // Component Operation/Event Parameters
    public static final String PARAMETER_MENU_ITEM_IDENTIFIER = "$P_MENU_ITEM_IDENTIFIER";
    public static final String PARAMETER_VISIBLE = "$P_VISIBLE";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        return new ArrayList<>();
    }

    public static ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return new ArrayList<>();
    }

    public static ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        T8ComponentOperationDefinition newOperationDefinition;
        ArrayList<T8ComponentOperationDefinition> operations;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_COPY_ROWS_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Copy Rows Visibility");
        newOperationDefinition.setMetaDescription("Specifies whether or not the copy rows menu item should be visible.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "The visible state for the menu item to be set.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_EXPORT_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Export Visibility");
        newOperationDefinition.setMetaDescription("Specifies whether or not the export menu item should be visible.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "The visible state for the menu item to be set.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_FILL_DOWN_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Fill Down Visibility");
        newOperationDefinition.setMetaDescription("Specifies whether or not the fill down menu item should be visible.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "The visible state for the menu item to be set.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_MENU_ITEM_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Menu Item Visibility");
        newOperationDefinition.setMetaDescription("Specifies whether or not a specific menu item should be visible.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_MENU_ITEM_IDENTIFIER, "Menu Item Identifier", "The menu item identfier for the menu item of which the visibility will be set.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "The visible state for the menu item to be set.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}