package com.pilog.t8.definition.ui.tabbedpane;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;


/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8TabbedPane and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8TabbedPaneAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_TAB_CHANGED = "$CE_TAB_CHANGED";

    public static final String OPERATION_SET_TAB_TITLE = "$CO_SET_TAB_TITLE";
    public static final String OPERATION_SET_TAB_VISIBLE = "$CO_SET_TAB_VISIBLE";
    public static final String OPERATION_SET_TAB_ENABLED = "$CO_SET_TAB_ENABLED";
    public static final String OPERATION_SET_SELECTED_TAB = "$CO_SET_SELECTED_TAB";
    public static final String OPERATION_GET_SELECTED_TAB = "$CO_GET_SELECTED_TAB";

    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_VISIBLE = "$P_VISIBLE";
    public static final String PARAMETER_ENABLED = "$P_ENABLED";
    public static final String PARAMETER_TAB_IDENTIFIER = "$CP_TAB_IDENTIFIER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_TAB_CHANGED);
        newEventDefinition.setMetaDisplayName("Tab Changed");
        newEventDefinition.setMetaDescription("This event occurs when the current tab is changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TAB_IDENTIFIER, "Tab Identifier", "The newly active tab.", T8DataType.DEFINITION_IDENTIFIER));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TAB_TITLE);
        newOperationDefinition.setMetaDisplayName("Set Tab Title");
        newOperationDefinition.setMetaDescription("Sets the title displayed by the defined tab.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the tab.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TAB_IDENTIFIER, "Tab Identifier", "The Identifier of the tab that should be altered.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TAB_VISIBLE);
        newOperationDefinition.setMetaDisplayName("Set Tab Visible");
        newOperationDefinition.setMetaDescription("Sets the visibility of the specified tab.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TAB_IDENTIFIER, "Tab Identifier", "The Identifier of the tab for which to set visibility.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_VISIBLE, "Visible", "A Boolean value indicating visible(true) or hidden(false).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TAB_ENABLED);
        newOperationDefinition.setMetaDisplayName("Set Tab Enabled");
        newOperationDefinition.setMetaDescription("Sets the enabled state of the specified tab.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TAB_IDENTIFIER, "Tab Identifier", "The Identifier of the tab for which to set the enabled state.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ENABLED, "Enabled", "A Boolean value indicating enabled(true) or disabled(false).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SELECTED_TAB);
        newOperationDefinition.setMetaDisplayName("Set Selected Tab");
        newOperationDefinition.setMetaDescription("Sets the active tab on the tabbed pane.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TAB_IDENTIFIER, "Tab Identifier", "The Identifier of the tab to select", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_TAB);
        newOperationDefinition.setMetaDisplayName("Get Selected Tab");
        newOperationDefinition.setMetaDescription("Gets the identifier of the currently active tab.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TAB_IDENTIFIER, "Tab Identifier", "The Identifier of the selected tab.", T8DataType.DEFINITION_IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
