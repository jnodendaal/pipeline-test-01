package com.pilog.t8.definition.ui.hyperlink;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Hyperlink and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8HyperlinkAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_LINK_CLICKED = "$CE_LINK_CLICKED";
    public static final String OPERATION_ENABLE_LINK = "$CO_ENABLE_LINK";
    public static final String OPERATION_DISABLE_LINK = "$CO_DISABLE_LINK";
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_GET_TEXT = "$CO_GET_TEXT";
    public static final String OPERATION_SET_URI = "$CO_SET_URI";
    public static final String OPERATION_SET_LINK = "$CO_SET_LINK";
    public static final String PARAMETER_TEXT = "$P_TEXT";
    public static final String PARAMETER_URI = "$P_URI";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        newEventDefinition = new T8ComponentEventDefinition(EVENT_LINK_CLICKED);
        newEventDefinition.setMetaDisplayName("Link Clicked");
        newEventDefinition.setMetaDescription("This event occurs when the hyperlink is clicked.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ENABLE_LINK);
        newOperationDefinition.setMetaDisplayName("Enable Link");
        newOperationDefinition.setMetaDescription("This operation enables the hyperlink.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_DISABLE_LINK);
        newOperationDefinition.setMetaDisplayName("Disable Link");
        newOperationDefinition.setMetaDescription("This operation disables the hyperlink.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Link Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed on the hyperlink.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the hyperlink.", T8DataType.STRING, false));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_TEXT);
        newOperationDefinition.setMetaDisplayName("Get Link Text");
        newOperationDefinition.setMetaDescription("This operation gets the text displayed on the hyperlink.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value displayed on the hyperlink.", T8DataType.STRING, false));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_URI);
        newOperationDefinition.setMetaDisplayName("Set Link URI");
        newOperationDefinition.setMetaDescription("This operations sets the underlying URI to which hyperlink points.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_URI, "URI String", "The URI String value to which the hyperlink points.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_LINK);
        newOperationDefinition.setMetaDisplayName("Set Link");
        newOperationDefinition.setMetaDescription("This operations sets the underlying URI to which hyperlink points as well as the text display by the hyperlink.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the hyperlink.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_URI, "URI String", "The URI String value to which the hyperlink points.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        return operations;
    }
}
