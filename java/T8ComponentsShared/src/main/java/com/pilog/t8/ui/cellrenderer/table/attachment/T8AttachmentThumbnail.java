package com.pilog.t8.ui.cellrenderer.table.attachment;

import com.pilog.t8.file.T8FileDetails;
import java.io.Serializable;
import javax.swing.ImageIcon;

/**
 * @author Hennie Brink
 */
public class T8AttachmentThumbnail implements Serializable
{
    private final String attachmentID;
    private final T8FileDetails fileDetails;
    private ImageIcon thumbnail;

    public T8AttachmentThumbnail(String attachmentID, T8FileDetails fileDetails)
    {
        this.attachmentID = attachmentID;
        this.fileDetails = fileDetails;
    }

    public String getAttachmentID()
    {
        return attachmentID;
    }

    public T8FileDetails getFileDetails()
    {
        return fileDetails;
    }

    public ImageIcon getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(ImageIcon thumbnail)
    {
        this.thumbnail = thumbnail;
    }
}
