package com.pilog.t8.ui.datasearch.combobox;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class ComboBoxModelGroupNode implements ComboBoxModelNode
{
    private final List<ComboBoxModelItemNode> nodes;

    private String groupName;
    private String groupKey;
    private boolean selected;

    public ComboBoxModelGroupNode()
    {
        this.nodes = new ArrayList<>();
    }

    public ComboBoxModelGroupNode(String groupName, String groupKey)
    {
        this();
        this.groupName = groupName;
        this.groupKey = groupKey;
    }

    public String getGroupKey()
    {
        return groupKey;
    }

    public void setGroupKey(String groupKey)
    {
        this.groupKey = groupKey;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    @Override
    public boolean isSelected()
    {
        return selected;
    }

    @Override
    public void setSelected(boolean selected)
    {
        this.selected = selected;

        for (ComboBoxModelItemNode node : nodes)
        {
            node.setSelected(selected);
        }
    }

    public void addNode(ComboBoxModelItemNode node)
    {
        nodes.add(node);
    }

    public void removeNode(ComboBoxModelItemNode node)
    {
        nodes.remove(node);
    }

    public List<ComboBoxModelItemNode> getNodes()
    {
        return new ArrayList<>(nodes);
    }

    @Override
    public String getDisplayString()
    {
        return this.getGroupName();
    }

    @Override
    public String getDescription()
    {
        return null;
    }
}
