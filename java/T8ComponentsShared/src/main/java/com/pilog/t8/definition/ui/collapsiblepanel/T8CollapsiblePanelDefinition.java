package com.pilog.t8.definition.ui.collapsiblepanel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CollapsiblePanelDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_COLLAPSIBLE_PANEL";
    public static final String DISPLAY_NAME = "Collapsible Panel";
    public static final String DESCRIPTION = "Define a container which can collapse or expand its content area with animation and fade in/fade out effects.  It also acts as a standard container for other T8 components.";
    public static final String IDENTIFIER_PREFIX = "C_COLLAPSIBLE_PANEL_";
    public enum Datum {TITLE,
                       CONTENT_COMPONENT_DEFINITION,
                       COLLAPSED,
                       ANIMATED};
    // -------- Definition Meta-Data -------- //

    public T8CollapsiblePanelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TITLE.toString(), "Title", "The display title of the pane."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COLLAPSED.toString(), "Collapsed", "If this collapsible pane should be initially collapsed.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ANIMATED.toString(), "Animated", "A flag indicating whether or not the change between collapsed and expanded states should be animated.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.CONTENT_COMPONENT_DEFINITION.toString(), "Content Component", "The component contained by this panel."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONTENT_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.collapsiblepanel.T8CollapsiblePanel").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8CollapsiblePanelAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8CollapsiblePanelAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;
        T8ComponentDefinition contentComponentDefinition;

        contentComponentDefinition = getContentComponentDefinition();
        childComponentDefinitions = new ArrayList<T8ComponentDefinition>();
        if (contentComponentDefinition != null) childComponentDefinitions.add(contentComponentDefinition);
        return childComponentDefinitions;
    }

    public String getTitle()
    {
        return (String)getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String title)
    {
        setDefinitionDatum(Datum.TITLE.toString(), title);
    }

    public Boolean isCollapsed()
    {
        return (Boolean)getDefinitionDatum(Datum.COLLAPSED.toString());
    }

    public void setCollapsed(Boolean collapsed)
    {
        setDefinitionDatum(Datum.COLLAPSED.toString(), collapsed);
    }

    public Boolean isAnimated()
    {
        Boolean animated;

        animated = (Boolean)getDefinitionDatum(Datum.ANIMATED.toString());
        return animated != null && animated;
    }

    public void setAnimated(Boolean animated)
    {
        setDefinitionDatum(Datum.ANIMATED.toString(), animated);
    }

    public T8ComponentDefinition getContentComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.CONTENT_COMPONENT_DEFINITION.toString());
    }

    public void setContentComponentDefinition(T8ComponentDefinition componentDefinition)
    {
         setDefinitionDatum(Datum.CONTENT_COMPONENT_DEFINITION.toString(), componentDefinition);
    }
}
