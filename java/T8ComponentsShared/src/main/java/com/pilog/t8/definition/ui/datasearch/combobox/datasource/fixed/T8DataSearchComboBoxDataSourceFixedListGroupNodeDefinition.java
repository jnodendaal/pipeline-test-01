package com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_FIXED_LIST_NODE_GROUPS";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_FIXED_LIST_NODE_GROUP";
    public static final String DISPLAY_NAME = "Group Node";
    public static final String DESCRIPTION = "A Group node containing a grouping of items";
    public static final String IDENTIFIER_PREFIX = "NODE_GROUP_";

    public enum Datum
    {
        GROUP_NAME,
        ITEMS
    };
// -------- Definition Meta-Data -------- //
    public T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.GROUP_NAME.toString(), "Group Name", "The name of this group that will be displayed"));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ITEMS.toString(), "Group Items", "The items available for this group"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.ITEMS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataSearchComboBoxDataSourceFixedListItemDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    public String getGroupName()
    {
        return getDefinitionDatum(Datum.GROUP_NAME);
    }

    public void setGroupName(String name)
    {
        setDefinitionDatum(Datum.GROUP_NAME, name);
    }

    public List<T8DataSearchComboBoxDataSourceFixedListItemDefinition> getGroupItems()
    {
        return getDefinitionDatum(Datum.ITEMS);
    }

    public void setGroupItems(List<T8DataSearchComboBoxDataSourceFixedListItemDefinition> items)
    {
        setDefinitionDatum(Datum.ITEMS, items);
    }
}
