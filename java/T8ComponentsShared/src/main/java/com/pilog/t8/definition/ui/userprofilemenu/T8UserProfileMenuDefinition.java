package com.pilog.t8.definition.ui.userprofilemenu;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8UserProfileMenuDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MENU_USER_PROFILE";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MENU_ITEM";
    public static final String DISPLAY_NAME = "User Profile Menu";
    public static final String DESCRIPTION = "A menu that shows the available user profiles for the current session and allows the user to select one options.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_MENU_USER_PROFILES_";
    public enum Datum {TEXT,
                       MENU_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition menuBackgroundPainterDefinition;

    public T8UserProfileMenuDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        backgroundPainterId = getMenuBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            menuBackgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Menu Background Painter", "The painter to use for painting this menu's backgorund."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text to display on the menu item."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.userprofilemenu.T8UserProfileMenu").getConstructor(T8UserProfileMenuDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8UserProfileMenuAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8UserProfileMenuAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT.toString(), text);
    }

    public String getMenuBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setMenuBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.MENU_BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getMenuBackgroundPainterDefinition()
    {
        return menuBackgroundPainterDefinition;
    }
}
