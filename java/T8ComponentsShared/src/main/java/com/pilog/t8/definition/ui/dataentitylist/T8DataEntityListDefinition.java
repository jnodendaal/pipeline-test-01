package com.pilog.t8.definition.ui.dataentitylist;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8ListCellRendererDefinition;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityListDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_ENTITY_LIST";
    public static final String DISPLAY_NAME = "Data Entity List";
    public static final String DESCRIPTION = "A component that contains a list of data entities and displays the entities using a cell renderer.";
    public static final String IDENTIFIER_PREFIX = "C_DATA_ENTITY_LIST_";
    public enum Datum {DISPLAY_FIELD_IDENTIFIER,
                       DATA_ENTITY_IDENTIFIER,
                       AUTO_RETRIEVE,
                       RENDERER_DEFINITION,
                       LIST_PREFILTER_DEFINITIONS,
                       PREFILTER_DEFINITIONS,
                       ENTITY_RETRIEVAL_TYPE,
                       TOOL_BAR_VISIBLE,
                       LOOKUP_DIALOG_DEFINITION,
                       VISIBLE_ROW_COUNT,
                       ALLOW_DUPLICATE_ENTRIES,
                       ADD_ENABLED,
                       REMOVE_ENABLED,
                       INDEX_INCREMENT_ENABLED,
                       INDEX_DECREMENT_ENABLED};
    // -------- Definition Meta-Data -------- //

    public enum EntityRetrievalType {PRELOADED, LOAD_ON_ADD, LOOKUP_DIALOG};

    public T8DataEntityListDefinition(String identifier)
    {
        super(identifier);
        setDefinitionDatum(Datum.ENTITY_RETRIEVAL_TYPE.toString(), EntityRetrievalType.PRELOADED.toString(), false, this);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity displayed by this component."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DISPLAY_FIELD_IDENTIFIER.toString(), "Display Field", "The field from which the value to display will be retrieved.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ENTITY_RETRIEVAL_TYPE.toString(), "Entity Retrieval Type", "The method to use for entity retrieval.", EntityRetrievalType.LOOKUP_DIALOG.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.LOOKUP_DIALOG_DEFINITION.toString(), "Lookup Dialog", "The dialog that will be used to allow a user to search for entities to add to the list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_RETRIEVE.toString(), "Auto Retrieve", "A boolean setting that enables automatic retrieval of list data when the list is initialized.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_DUPLICATE_ENTRIES.toString(), "Allow Duplicate Entries", "A boolean setting that sets if duplicate entries will be allowed in the list.", false));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.LIST_PREFILTER_DEFINITIONS.toString(), "List Prefilters", "If set, will be used to retrieve data to populate the list on startup/refresh."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PREFILTER_DEFINITIONS.toString(), "Lookup Prefilters", "The prefilters that will be applied in order to identify lookup data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.RENDERER_DEFINITION.toString(), "Renderer", "The renderer to use for displaying the contents of the list."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TOOL_BAR_VISIBLE.toString(), "Tool Bar Visible", "A boolean setting that sets the visibility of the list tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.VISIBLE_ROW_COUNT.toString(), "Visible Row Count", "This tells the list how many rows to render before it start displaying a scroll bar."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ADD_ENABLED.toString(), "Add enabled", "A boolean setting that sets the visibility of the add button.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.REMOVE_ENABLED.toString(), "Remove enabled", "A boolean setting that sets the visibility of the remove button.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INDEX_INCREMENT_ENABLED.toString(), "Index increment enabled", "A boolean setting that sets the visibility of the up button that increments the index.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INDEX_DECREMENT_ENABLED.toString(), "Index decrement enabled", "A boolean setting that sets the visibility of the down button that decrements the index.", true));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DISPLAY_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.ENTITY_RETRIEVAL_TYPE.toString().equals(datumIdentifier)) return createStringOptions(EntityRetrievalType.values());
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.LIST_PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.LOOKUP_DIALOG_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataLookupDialogDefinition.TYPE_IDENTIFIER));
        else if (Datum.RENDERER_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ListCellRendererDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        // Validates if the Data Entity is set for the Data Lookup Field.
        if (getLookupDialogDefinition() != null)
        {
            T8DataLookupDialogDefinition lookupDialogDefinition;

            lookupDialogDefinition = getLookupDialogDefinition();
            if(!Objects.equals(getDataEntityIdentifier(), lookupDialogDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(lookupDialogDefinition.getProjectIdentifier(), lookupDialogDefinition.getIdentifier(), T8DataLookupDialogDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Lookup Dialog Entity identifier incorrect. Expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        if(getListPrefilterDefinitions() != null)
        {
            for (T8DataFilterDefinition dataFilterDefinition : getListPrefilterDefinitions())
            {
                if(!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Filter Entity identifier incorrect. Expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        if(getPrefilterDefinitions()!= null)
        {
            for (T8DataFilterDefinition dataFilterDefinition : getPrefilterDefinitions())
            {
                if(!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Filter Entity identifier incorrect. Expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.dataentitylist.T8DataEntityList", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataEntityListAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataEntityListAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;

        childComponentDefinitions = new ArrayList<>();

        if(getRendererDefinition() != null) childComponentDefinitions.add(getRendererDefinition());

        return childComponentDefinitions;
    }

    public String getDisplayFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_FIELD_IDENTIFIER.toString());
    }

    public void setDisplayFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DISPLAY_FIELD_IDENTIFIER.toString(), identifier);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public EntityRetrievalType getEntityRetrievalType()
    {
        return EntityRetrievalType.valueOf((String)getDefinitionDatum(Datum.ENTITY_RETRIEVAL_TYPE.toString()));
    }

    public void setEntityRetrievalType(EntityRetrievalType type)
    {
        setDefinitionDatum(Datum.ENTITY_RETRIEVAL_TYPE.toString(), type.toString());
    }

    public boolean isAutoRetrieve()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.AUTO_RETRIEVE.toString());
        return ((enabled != null) && enabled);
    }

    public void setAutoRetrieve(boolean enabled)
    {
        setDefinitionDatum(Datum.AUTO_RETRIEVE.toString(), enabled);
    }

    public boolean isAllowDuplicateEntries()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.ALLOW_DUPLICATE_ENTRIES.toString());
        return ((enabled != null) && enabled);
    }

    public void setAllowDuplicateEntries(boolean enabled)
    {
        setDefinitionDatum(Datum.ALLOW_DUPLICATE_ENTRIES.toString(), enabled);
    }

    public List<T8DataFilterDefinition> getListPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.LIST_PREFILTER_DEFINITIONS.toString());
    }

    public void setListPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.LIST_PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public List<T8DataFilterDefinition> getPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString());
    }

    public void setPrefilterDefinitions(ArrayList<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public T8DataLookupDialogDefinition getLookupDialogDefinition()
    {
        return (T8DataLookupDialogDefinition)getDefinitionDatum(Datum.LOOKUP_DIALOG_DEFINITION.toString());
    }

    public void setLookupDialogDefinition(T8DataLookupDialogDefinition definition)
    {
        setDefinitionDatum(Datum.LOOKUP_DIALOG_DEFINITION.toString(), definition);
    }

    public boolean isToolBarVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString());
        return ((visible != null) && visible);
    }

    public void setToolBarVisible(boolean visible)
    {
        setDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString(), visible);
    }

    public boolean isAddEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.ADD_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setAddEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.ADD_ENABLED.toString(), enabled);
    }

    public boolean isRemoveEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.REMOVE_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setRemoveEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.REMOVE_ENABLED.toString(), enabled);
    }

    public boolean isIndexIncrementEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.INDEX_INCREMENT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setIndexIncremenEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.INDEX_INCREMENT_ENABLED.toString(), enabled);
    }

    public boolean isIndexDecrementEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.INDEX_DECREMENT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setIndexDecremenEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.INDEX_DECREMENT_ENABLED.toString(), enabled);
    }

    public T8ComponentDefinition getRendererDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.RENDERER_DEFINITION.toString());
    }

    public void setRendererDefinition(T8ComponentDefinition definition)
    {
        setDefinitionDatum(Datum.RENDERER_DEFINITION.toString(), definition);
    }

    public Integer getVisibleRowCount()
    {
        return (Integer) getDefinitionDatum(Datum.VISIBLE_ROW_COUNT.toString());
    }

    public void setVisibleRowCount(Integer visibleRowCount)
    {
        setDefinitionDatum(Datum.VISIBLE_ROW_COUNT.toString(), visibleRowCount);
    }
}
