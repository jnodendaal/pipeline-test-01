package com.pilog.t8.definition.ui.table;

import com.pilog.t8.definition.ui.table.script.T8TableOnDeleteScriptDefinition;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ClientEntityValidationScriptDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.T8DataObjectProviderComponentDefinition;
import com.pilog.t8.definition.ui.table.menu.T8TableContextMenuDefinition;
import com.pilog.t8.definition.ui.table.object.T8TableDataObjectProviderDefinition;
import com.pilog.t8.definition.ui.table.script.T8TableCommitScriptDefinition;
import com.pilog.t8.definition.ui.table.script.T8TableOnInsertScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public final class T8TableDefinition extends T8ComponentDefinition implements T8DataObjectProviderComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TABLE";
    public static final String DISPLAY_NAME = "Table";
    public static final String DESCRIPTION = "A UI component that displays relation data in a tabular format.";
    public static final String IDENTIFIER_PREFIX = "C_TABLE_";
    public enum Datum {
        AUTO_RESIZE_MODE,
        RESIZE_TO_COLUMN_HEADER,
        RESIZE_TO_COLUMN_DATA,
        AUTO_ROW_COUNT,
        AUTO_RETRIEVE,
        AUTO_SELECT_ON_RETRIEVE,
        PAGE_SIZE,
        ROW_HEIGHT,
        INSERT_ENABLED,
        DELETE_ENABLED,
        COMMIT_ENABLED,
        USER_FILTER_ENABLED,
        USER_REFRESH_ENABLED,
        USER_EXPORT_ENABLED,
        OPTIONS_ENABLED,
        TICK_SELECTION_ENABLED,
        TICK_ALL_ENABLED,
        TICK_SELECTION_MODE,
        POP_OUT_ENABLED,
        TOOL_BAR_VISIBLE,
        DATA_ENTITY_IDENTIFIER,
        ON_DELETE_SCRIPT_DEFINITION,
        ON_INSERT_SCRIPT_DEFINITION,
        COMMIT_SCRIPT_DEFINITION,
        CLIENT_VALIDATION_SCRIPT_DEFINITION,
        SERVER_VALIDATION_SCRIPT_DEFINITION,
        COLUMN_DEFINITIONS,
        PREFILTER_DEFINITIONS,
        SELECTION_MODE,
        ORDERING_PRESETS,
        DATA_OBJECT_PROVIDER_DEFINITIONS,
        CONTEXT_MENU_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public enum AutoResizeMode {OFF, SUBSEQUENT_COLUMNS, LAST_COLUMN, ALL_COLUMNS, NEXT_COLUMN}
    public enum SelectionMode{SINGLE_SELECTION, SINGLE_INTERVAL_SELECTION, MULTIPLE_INTERVAL_SELECTION}
    public enum TickSelectionMode{SINGLE_SELECTION, MULTIPLE_INTERVAL_SELECTION}
    private T8DataEntityDefinition entityDefinition;

    public T8TableDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity displayed and manipulated by this table."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.AUTO_RESIZE_MODE.toString(), "Auto Resize Mode", "The resize mode of the table.  This setting indicates how columns will be resized relative to each other when the available display area changes.", AutoResizeMode.OFF.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RESIZE_TO_COLUMN_HEADER.toString(), "Resize Column to Fit Header Text", "This option only works if the resize mode is set to OFF. Resizes the columns so that the column will be large enough to fit the header text.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RESIZE_TO_COLUMN_DATA.toString(), "Resize Column to Fit Data", "This option only works if the resize mode is set to OFF. Resizes the columns so that the column will be large enough to fit the largest value of the data in the column.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.PAGE_SIZE.toString(), "Page Size", "The number of records that will be retrieved from the data source per page.", 100));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.ROW_HEIGHT.toString(), "Row Height", "The display height (in pixels) of each row in the table.", 25));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COLUMN_DEFINITIONS.toString(), "Columns", "The columns contained by the table.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PREFILTER_DEFINITIONS.toString(), "Prefilters", "The prefilters that will be applied when the content data of the table is retrieved."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATA_OBJECT_PROVIDER_DEFINITIONS.toString(), "Data Object Providers", "The types of data object providers used to provider objects from this table."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ORDERING_PRESETS.toString(), "Preset Ordering", "The preset orderings that will be available to the user to choose from."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.ON_DELETE_SCRIPT_DEFINITION.toString(), "On Deletion Script", "The client-side script that will be executed when the 'Delete Selected Rows' toolbar button is clicked."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.ON_INSERT_SCRIPT_DEFINITION.toString(), "On Insert Script", "The client-side script that will be executed when the 'Add New Rows' toolbar button is clicked."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.COMMIT_SCRIPT_DEFINITION.toString(), "Commit Script", "A script that is executed on the server-side when table changes must be persisted.  If this script is defined, the default commit operation of the table is effectively overridden by the custom behaviour implemented in the script."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.CLIENT_VALIDATION_SCRIPT_DEFINITION.toString(), "Client Validation Script", "The client-side validation script that will be used to validate data entered into the table."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.SERVER_VALIDATION_SCRIPT_DEFINITION.toString(), "Server Validation Script", "The server validation script that will be used to validate data entered into the table."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_ROW_COUNT.toString(), "Auto Row Count", "A boolean setting that enables automatic counting of total row count for the currently filtered set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_RETRIEVE.toString(), "Auto Retrieve", "A boolean setting that enables automatic retrieval of table data when the table is initialized.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_SELECT_ON_RETRIEVE.toString(), "Auto Select on Retrieve", "A boolean setting that enables automatic selection of the first data row after the table data has been retrieve.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INSERT_ENABLED.toString(), "Insert Enabled", "A boolean setting that enables (if true) the insertion of rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DELETE_ENABLED.toString(), "Delete Enabled", "A boolean setting that enables (if true) the deletion of rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COMMIT_ENABLED.toString(), "Commit Enabled", "A boolean setting that enables (if true) the comittal of changes to rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_FILTER_ENABLED.toString(), "Filter Enabled", "A boolean setting that enables (if true) the filtering of rows/entities in the table.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_REFRESH_ENABLED.toString(), "Refresh Enabled", "A boolean setting that enables (if true) the refresh option on the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USER_EXPORT_ENABLED.toString(), "Export Enabled", "A boolean setting that enables (if true) the export option on the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.OPTIONS_ENABLED.toString(), "Options Enabled", "A boolean setting that enables (if true) the options menu available from the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.POP_OUT_ENABLED.toString(), "Pop-Out Enabled", "A boolean setting that enables (if true) the Pop-Out option available from the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TOOL_BAR_VISIBLE.toString(), "Tool Bar Visible", "A boolean setting that sets the visibility of the table tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SELECTION_MODE.toString(), "Selection Mode", "Defines the selection mode that this table will use to determine how selections are handled.", SelectionMode.MULTIPLE_INTERVAL_SELECTION.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TICK_SELECTION_ENABLED.toString(), "Tick Selection Enabled", "A boolean setting that enables (if true) a checkbox displayed on each row header which allows the user to select the row by ticking the box.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TICK_ALL_ENABLED.toString(), "Tick All Enabled", "A boolean setting that enables (if true) a checkbox displayed on the column names row header which allows the user to select all rows by ticking the box. Ignored if Tick Selection is not enabled.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TICK_SELECTION_MODE.toString(), "Tick Selection Mode", "Defines the selection mode that this table will use to determine how tick selections are handled. Ignored if Tick Selection is not enabled.", TickSelectionMode.MULTIPLE_INTERVAL_SELECTION.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.CONTEXT_MENU_DEFINITION.toString(), "Context Menu", "The context menu definition to be used for the current table."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.AUTO_RESIZE_MODE.toString().equals(datumIdentifier)) return createStringOptions(AutoResizeMode.values());
        else if (Datum.ON_DELETE_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableOnDeleteScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.ON_INSERT_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableOnInsertScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.COMMIT_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableCommitScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.CLIENT_VALIDATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ClientEntityValidationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.SERVER_VALIDATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ServerEntityValidationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.COLUMN_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableColumnDefinition.TYPE_IDENTIFIER));
        else if (Datum.PREFILTER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.ORDERING_PRESETS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TableOrderingPresetDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_OBJECT_PROVIDER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8TableDataObjectProviderDefinition.GROUP_IDENTIFIER));
        else if (Datum.SELECTION_MODE.toString().equals(datumIdentifier)) return createStringOptions(SelectionMode.values());
        else if (Datum.TICK_SELECTION_MODE.toString().equals(datumIdentifier)) return createStringOptions(TickSelectionMode.values());
        else if (Datum.CONTEXT_MENU_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableContextMenuDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.COLUMN_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String dataEntityId;

        dataEntityId = getDataEntityIdentifier();
        if (dataEntityId != null)
        {
            T8DefinitionManager definitionManager;

            definitionManager = context.getServerContext().getDefinitionManager();
            entityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(context, context.getProjectId(), dataEntityId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.table.T8Table", new Class<?>[]{T8TableDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.ui.table.T8TableDefinitionActionHandler", new Class<?>[]{T8Context.class, T8TableDefinition.class}, context, this);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TableAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TableAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> editorDefinitions;

        editorDefinitions = new ArrayList<>();
        for (T8TableColumnDefinition columnDefinition : getColumnDefinitions())
        {
            T8ComponentDefinition componentDefinition;

            componentDefinition = columnDefinition.getEditorComponentDefinition();
            if (componentDefinition != null) editorDefinitions.add(componentDefinition);

            componentDefinition = columnDefinition.getRendererComponentDefinition();
            if (componentDefinition != null) editorDefinitions.add(componentDefinition);
        }

        return editorDefinitions;
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        // Create a list to hold all validation error we identify.
        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);

        // Validate prefilters.
        if (getPrefilterDefinitions() != null)
        {
            for (T8Definition prefilterDefinition : getPrefilterDefinitions())
            {
                T8DataFilterDefinition dataFilterDefinition;

                dataFilterDefinition = (T8DataFilterDefinition)prefilterDefinition;
                if(!Objects.equals(getDataEntityIdentifier(), dataFilterDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(dataFilterDefinition.getProjectIdentifier(), dataFilterDefinition.getPublicIdentifier(), T8DataFilterDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Invalid Entity Identifier on Filter, expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
            }
        }

        // Check auto-retrieve is set to false.  This is a temporary validation that will be removed as soon as auto-retrieve has been eliminated from all definitions.
        Boolean autoRetrieve;

        autoRetrieve = (Boolean)getDefinitionDatum(Datum.AUTO_RETRIEVE.toString());
        if ((autoRetrieve != null) && (autoRetrieve))
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.AUTO_RETRIEVE.toString(), "Table Auto-Retrieve setting enabled.  This feature has been removed.", T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public T8TableOnDeleteScriptDefinition getOnDeleteScriptDefinition()
    {
        return (T8TableOnDeleteScriptDefinition)getDefinitionDatum(Datum.ON_DELETE_SCRIPT_DEFINITION.toString());
    }

    public void setOnDeleteScriptDefinition(T8TableOnDeleteScriptDefinition definition)
    {
        setDefinitionDatum(Datum.ON_DELETE_SCRIPT_DEFINITION.toString(), definition);
    }

    public T8TableOnInsertScriptDefinition getOnInsertScriptDefinition()
    {
        return (T8TableOnInsertScriptDefinition)getDefinitionDatum(Datum.ON_INSERT_SCRIPT_DEFINITION.toString());
    }

    public void setOnInsertScriptDefnition(T8TableOnInsertScriptDefinition definition)
    {
        setDefinitionDatum(Datum.ON_INSERT_SCRIPT_DEFINITION.toString(), definition);
    }

    public T8TableCommitScriptDefinition getCommitScriptDefinition()
    {
        return (T8TableCommitScriptDefinition)getDefinitionDatum(Datum.COMMIT_SCRIPT_DEFINITION.toString());
    }

    public void setCommitScriptDefinition(T8TableCommitScriptDefinition definition)
    {
        setDefinitionDatum(Datum.COMMIT_SCRIPT_DEFINITION.toString(), definition);
    }

    public T8ClientEntityValidationScriptDefinition getClientValidationScriptDefinition()
    {
        return (T8ClientEntityValidationScriptDefinition)getDefinitionDatum(Datum.CLIENT_VALIDATION_SCRIPT_DEFINITION.toString());
    }

    public void setClientValidationScriptDefinition(T8ClientEntityValidationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.CLIENT_VALIDATION_SCRIPT_DEFINITION.toString(), definition);
    }

    public T8ServerEntityValidationScriptDefinition getServerValidationScriptDefinition()
    {
        return (T8ServerEntityValidationScriptDefinition)getDefinitionDatum(Datum.SERVER_VALIDATION_SCRIPT_DEFINITION.toString());
    }

    public void setServerValidationScriptDefinition(T8ServerEntityValidationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.SERVER_VALIDATION_SCRIPT_DEFINITION.toString(), definition);
    }

    public void clearColumnDefinitions()
    {
        getColumnDefinitions().clear();
    }

    public T8TableColumnDefinition getColumnDefinition(String identifier)
    {
        for (T8TableColumnDefinition columnDefinition : getColumnDefinitions())
        {
            if (columnDefinition.getIdentifier().equals(identifier)) return columnDefinition;
        }

        return null;
    }

    public ArrayList<T8TableColumnDefinition> getColumnDefinitions()
    {
        return (ArrayList<T8TableColumnDefinition>)getDefinitionDatum(Datum.COLUMN_DEFINITIONS.toString());
    }

    public void addColumnDefinition(T8TableColumnDefinition columnDefinition)
    {
        addSubDefinition(Datum.COLUMN_DEFINITIONS.toString(), columnDefinition);
    }

    public void setColumnDefinitions(ArrayList<T8TableColumnDefinition> columnDefinitions)
    {
        setDefinitionDatum(Datum.COLUMN_DEFINITIONS.toString(), columnDefinitions);
    }

    public List<T8DataFilterDefinition> getPrefilterDefinitions()
    {
        return (List<T8DataFilterDefinition>)getDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString());
    }

    public void setPrefilterDefinitions(List<T8DataFilterDefinition> filterDefinitions)
    {
        setDefinitionDatum(Datum.PREFILTER_DEFINITIONS.toString(), filterDefinitions);
    }

    public List<T8TableOrderingPresetDefinition> getOrderingPresetDefinitions()
    {
        return (List<T8TableOrderingPresetDefinition>)getDefinitionDatum(Datum.ORDERING_PRESETS.toString());
    }

    public void setOrderingPresetDefinitions(List<T8TableOrderingPresetDefinition> orderingPresetDefinitions)
    {
        setDefinitionDatum(Datum.ORDERING_PRESETS.toString(), orderingPresetDefinitions);
    }

    public List<T8TableDataObjectProviderDefinition> getDataObjectProviderDefinitions()
    {
        return (List<T8TableDataObjectProviderDefinition>)getDefinitionDatum(Datum.DATA_OBJECT_PROVIDER_DEFINITIONS.toString());
    }

    public void setDataObjectProviderDefinition(List<T8TableDataObjectProviderDefinition> definitions)
    {
        setDefinitionDatum(Datum.DATA_OBJECT_PROVIDER_DEFINITIONS.toString(), definitions);
    }

    public AutoResizeMode getAutoResizeMode()
    {
        return AutoResizeMode.valueOf((String)getDefinitionDatum(Datum.AUTO_RESIZE_MODE.toString()));
    }

    public void setAutoResizeMode(AutoResizeMode autoResizeMode)
    {
        setDefinitionDatum(Datum.AUTO_RESIZE_MODE.toString(), autoResizeMode.toString());
    }

    public Boolean isResizeToColumnHeader()
    {
        return (Boolean)getDefinitionDatum(Datum.RESIZE_TO_COLUMN_HEADER.toString());
    }

    public void setResizeToColumnHeader(Boolean resizeToColumnHeader)
    {
        setDefinitionDatum(Datum.RESIZE_TO_COLUMN_HEADER.toString(), resizeToColumnHeader);
    }

    public Boolean isResizeToColumnData()
    {
        return (Boolean)getDefinitionDatum(Datum.RESIZE_TO_COLUMN_DATA.toString());
    }

    public void setResizeToColumnData(Boolean resizeToColumnData)
    {
        setDefinitionDatum(Datum.RESIZE_TO_COLUMN_DATA.toString(), resizeToColumnData);
    }

    public int getPageSize()
    {
        return (Integer)getDefinitionDatum(Datum.PAGE_SIZE.toString());
    }

    public void setPageSize(int pageSize)
    {
        setDefinitionDatum(Datum.PAGE_SIZE.toString(), pageSize);
    }

    public int getRowHeight()
    {
        return (Integer)getDefinitionDatum(Datum.ROW_HEIGHT.toString());
    }

    public void setRowHeight(int rowHeight)
    {
        setDefinitionDatum(Datum.ROW_HEIGHT.toString(), rowHeight);
    }

    public T8DataEntityDefinition getEntityDefinition()
    {
        return entityDefinition;
    }

    public void setEntityDefinition(T8DataEntityDefinition entityDefinition)
    {
        this.entityDefinition = entityDefinition;
    }

    public boolean isAutoRetrieve()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.AUTO_RETRIEVE.toString());
        return ((enabled != null) && enabled);
    }

    public void setAutoRetrieve(boolean enabled)
    {
        setDefinitionDatum(Datum.AUTO_RETRIEVE.toString(), enabled);
    }

    public boolean isAutoRowCount()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.AUTO_ROW_COUNT.toString());
        return ((enabled != null) && enabled);
    }

    public void setAutoRowCount(boolean enabled)
    {
        setDefinitionDatum(Datum.AUTO_ROW_COUNT.toString(), enabled);
    }

    public boolean isAutoSelectOnRetrieve()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.AUTO_SELECT_ON_RETRIEVE.toString());
        return ((enabled != null) && enabled);
    }

    public void setAutoSelectOnRetrieve(boolean autoSelect)
    {
        setDefinitionDatum(Datum.AUTO_SELECT_ON_RETRIEVE.toString(), autoSelect);
    }

    public boolean isInsertEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.INSERT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setInsertEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.INSERT_ENABLED.toString(), enabled);
    }

    public boolean isDeleteEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.DELETE_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setDeleteEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.DELETE_ENABLED.toString(), enabled);
    }

    public boolean isCommitEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.COMMIT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setCommitEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.COMMIT_ENABLED.toString(), enabled);
    }

    public boolean isUserFilterEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_FILTER_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserFilterEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_FILTER_ENABLED.toString(), enabled);
    }

    public boolean isUserRefreshEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_REFRESH_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserRefreshEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_REFRESH_ENABLED.toString(), enabled);
    }

    public boolean isUserExportEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.USER_EXPORT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setUserExportEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.USER_EXPORT_ENABLED.toString(), enabled);
    }

    public boolean isOptionsEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.OPTIONS_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setOptionsEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.OPTIONS_ENABLED.toString(), enabled);
    }

    public boolean isPopOutEnabled()
    {
        Boolean enabled;

        enabled = getDefinitionDatum(Datum.POP_OUT_ENABLED);
        return (enabled == null || enabled);
    }

    public void setPopOutEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.POP_OUT_ENABLED, enabled);
    }

    public boolean isToolBarVisible()
    {
        Boolean visible;

        visible = (Boolean)getDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString());
        return ((visible != null) && visible);
    }

    public void setToolBarVisible(boolean visible)
    {
        setDefinitionDatum(Datum.TOOL_BAR_VISIBLE.toString(), visible);
    }

    public SelectionMode getSelectionMode()
    {
        String selectionMode;

        selectionMode = (String)getDefinitionDatum(Datum.SELECTION_MODE.toString());
        return Strings.isNullOrEmpty(selectionMode) ? SelectionMode.MULTIPLE_INTERVAL_SELECTION : SelectionMode.valueOf(selectionMode);
    }

    public void setSelectionMode(SelectionMode selectionMode)
    {
        setDefinitionDatum(Datum.SELECTION_MODE.toString(), selectionMode.toString());
    }

    public T8TableContextMenuDefinition getContextMenuDefinition()
    {
        return getDefinitionDatum(Datum.CONTEXT_MENU_DEFINITION);
    }

    public void setContextMenuDefinition(T8TableContextMenuDefinition definition)
    {
        setDefinitionDatum(Datum.CONTEXT_MENU_DEFINITION, definition);
    }

    public boolean isTickSelectionEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.TICK_SELECTION_ENABLED));
    }

    public void setTickSelectionEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.TICK_SELECTION_ENABLED, enabled);
    }

    public boolean isTickAllEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.TICK_ALL_ENABLED));
    }

    public void setTickAllEnabled(boolean tickAllEnabled)
    {
        setDefinitionDatum(Datum.TICK_ALL_ENABLED, tickAllEnabled);
    }

    public TickSelectionMode getTickSelectionMode()
    {
        String selectionMode;

        selectionMode = getDefinitionDatum(Datum.TICK_SELECTION_MODE);
        return Strings.isNullOrEmpty(selectionMode) ? TickSelectionMode.MULTIPLE_INTERVAL_SELECTION : TickSelectionMode.valueOf(selectionMode);
    }

    public void setTickSelectionMode(TickSelectionMode selectionMode)
    {
        setDefinitionDatum(Datum.TICK_SELECTION_MODE, selectionMode.toString());
    }
}
