package com.pilog.t8.definition.ui.cellrenderer.list;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8ListCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8CustomListCellRendererDefinition extends T8ComponentDefinition implements T8ListCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LIST_CELL_RENDERER_CUSTOM";
    public static final String DISPLAY_NAME = "Custom Cell Renderer";
    public static final String DESCRIPTION = "A Custom List Cell Renderer";
    public static final String IDENTIFIER_PREFIX = "C_RENDERER_";
    public enum Datum
    {
        CUSTOM_COMPONENT_DEFINITION,
        PRE_RENDER_SCRIPT
    };
    // -------- Definition Meta-Data -------- //

    public T8CustomListCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CUSTOM_COMPONENT_DEFINITION.toString(), "Custom Component Definition", "A Custom component that will be used to render the list items."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.PRE_RENDER_SCRIPT.toString(), "Pre Render Script", "The script that will be executed before the component is rendered."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CUSTOM_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.PRE_RENDER_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8CustomListCellRendererScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.cellrenderer.list.T8CustomListCellRenderer").getConstructor(T8ComponentController.class, T8CustomListCellRendererDefinition.class);
            return (T8Component)constructor.newInstance(controller, this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponents;

        childComponents = new ArrayList<>();
        childComponents.add(getCustomComponentDefinition());
        return childComponents;
    }

    public T8ComponentDefinition getCustomComponentDefinition()
    {
        return (T8ComponentDefinition) getDefinitionDatum(Datum.CUSTOM_COMPONENT_DEFINITION.toString());
    }

    public void setCustomComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        setDefinitionDatum(Datum.CUSTOM_COMPONENT_DEFINITION.toString(), componentDefinition);
    }

    public T8CustomListCellRendererScriptDefinition getPreRendererScriptDefinition()
    {
        return (T8CustomListCellRendererScriptDefinition) getDefinitionDatum(Datum.PRE_RENDER_SCRIPT.toString());
    }

    public void setPreRenderScript(T8CustomListCellRendererScriptDefinition script)
    {
        setDefinitionDatum(Datum.PRE_RENDER_SCRIPT.toString(), script);
    }
}
