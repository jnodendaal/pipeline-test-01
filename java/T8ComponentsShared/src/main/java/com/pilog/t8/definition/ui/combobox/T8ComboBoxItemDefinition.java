package com.pilog.t8.definition.ui.combobox;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComboBoxItemDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_COMBO_BOX_ITEM";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_COMBO_BOX_ITEM";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Combobox Item";
    public static final String DESCRIPTION = "An item selectable from a combobox.";
    private enum Datum {ITEM_NAME,
                        VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8ComboBoxItemDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.ITEM_NAME.toString(), "Item Name", "The display name of this item."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.VALUE_EXPRESSION.toString(), "Value Expression", "The expression used to evaluate the data value for this item."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getItemName()
    {
        return (String)getDefinitionDatum(Datum.ITEM_NAME.toString());
    }

    public void setItemName(String itemName)
    {
        setDefinitionDatum(Datum.ITEM_NAME.toString(), itemName);
    }

    public String getValueExpression()
    {
        return (String)getDefinitionDatum(Datum.VALUE_EXPRESSION.toString());
    }

    public void setValueExpression(String defaultValueExpression)
    {
        setDefinitionDatum(Datum.VALUE_EXPRESSION.toString(), defaultValueExpression);
    }
}
