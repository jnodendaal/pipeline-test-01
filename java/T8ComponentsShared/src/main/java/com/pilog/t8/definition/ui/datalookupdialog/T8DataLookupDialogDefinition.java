package com.pilog.t8.definition.ui.datalookupdialog;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchDefinition;
import com.pilog.t8.definition.ui.dialog.T8DialogDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupDialogDefinition extends T8DialogDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_LOOKUP_DIALOG";
    public static final String DISPLAY_NAME = "Data Lookup Dialog";
    public static final String DESCRIPTION = "A dialog for searching for required data from a base collection.";
    public static final String IDENTIFIER_PREFIX = "C_DATA_LOOKUP_DIALOG_";
    public enum Datum
    {
        DATA_ENTITY_IDENTIFIER,
        MAXIMUM_SELECTION_QUANTITY,
        DATA_SEARCH_DEFINITION,
        TABLE_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public T8DataLookupDialogDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity on which this component acts."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MAXIMUM_SELECTION_QUANTITY.toString(), "Maximum Selection Quantity", "The maximum number of entities that may be selected from the dialog.", 1));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DATA_SEARCH_DEFINITION.toString(), "Quick Search", "The search to display on the data lookup dialog."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.TABLE_DEFINITION.toString(), "Table", "The search results table to display on the data lookup dialog."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_SEARCH_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataSearchDefinition.TYPE_IDENTIFIER));
        else if (Datum.TABLE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TableDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        // Validates if the Data Entity is set for the Data Lookup Field.
        validationErrors = new ArrayList<>();
        if (getDataEntityIdentifier() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), T8DataLookupDialogDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier not set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        if (getTableDefinition() == null) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), T8DataLookupDialogDefinition.Datum.TABLE_DEFINITION.toString(), "Table Definition not set.", T8DefinitionValidationError.ErrorType.CRITICAL));
        else
        {
            T8TableDefinition tableDefinition;

            tableDefinition = getTableDefinition();
            if(!Objects.equals(getDataEntityIdentifier(), tableDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(tableDefinition.getProjectIdentifier(), tableDefinition.getIdentifier(), T8TableDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Table Data Entity identifier incorrect. Expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        }
        if(getDataSearchDefinition() != null)
        {
            T8DataSearchDefinition searchDefinition;

            searchDefinition = getDataSearchDefinition();
            if(!Objects.equals(getDataEntityIdentifier(), searchDefinition.getDataEntityIdentifier())) validationErrors.add(new T8DefinitionValidationError(searchDefinition.getProjectIdentifier(), searchDefinition.getIdentifier(), T8TableDefinition.Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Search Data Entity identifier incorrect. Expected: " + getDataEntityIdentifier(), T8DefinitionValidationError.ErrorType.CRITICAL));
        }

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datalookupdialog.T8DataLookupDialog", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataLookupDialogAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataLookupDialogAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> componentDefinitions;

        componentDefinitions = new ArrayList<>();

        if(getTableDefinition() != null) componentDefinitions.add(getTableDefinition());
        if(getDataSearchDefinition() != null) componentDefinitions.add(getDataSearchDefinition());

        return componentDefinitions;
    }

    public int getMaximumSelectionQuantity()
    {
        Integer quantity;

        quantity = (Integer)getDefinitionDatum(Datum.MAXIMUM_SELECTION_QUANTITY.toString());
        return quantity != null ? quantity : 1;
    }

    public void setMaximumSelectionQuantity(int quantity)
    {
        setDefinitionDatum(Datum.MAXIMUM_SELECTION_QUANTITY.toString(), quantity);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public T8TableDefinition getTableDefinition()
    {
        return (T8TableDefinition)getDefinitionDatum(Datum.TABLE_DEFINITION.toString());
    }

    public void setTableDefinition(T8TableDefinition definition)
    {
        setDefinitionDatum(Datum.TABLE_DEFINITION.toString(), definition);
    }

    public T8DataSearchDefinition getDataSearchDefinition()
    {
        return getDefinitionDatum(Datum.DATA_SEARCH_DEFINITION);
    }

    public void setDataSearchDefinition(T8DataSearchDefinition definition)
    {
        setDefinitionDatum(Datum.DATA_SEARCH_DEFINITION, definition);
    }
}
