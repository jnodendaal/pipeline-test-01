package com.pilog.t8.definition.ui.table.object;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.data.object.T8ComponentDataObjectProvider;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TableEntityMappedObjectProviderDefinition extends T8TableDataObjectProviderDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_TABLE_DATA_OBJECT_PROVIDER_ENTITY_MAPPED";
    public static final String DISPLAY_NAME = "Table Entity Mapped Data Object Provider";
    public static final String DESCRIPTION = "A definition of an object provider that creates objects from the entities by mapping the entity fields to corresponding object fields.";
    public enum Datum
    {
        DATA_OBJECT_ID,
        DATA_ENTITY_ID,
        KEY_FIELD_ID
    };
    // -------- Definition Meta-Data -------- //

    public T8TableEntityMappedObjectProviderDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_OBJECT_ID.toString(), "Data Object", "The objects that will be created from the specified entity."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_ID.toString(), "Data Entity", "The data entity from which the data objects will be created."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.KEY_FIELD_ID.toString(), "Key Field", "The entity field from which the key value to identify the data object will be fetched.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_ID.toString())).addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_OBJECT_ID.toString())));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_OBJECT_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataObjectDefinition.GROUP_IDENTIFIER));
        else if (Datum.KEY_FIELD_ID.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getDataEntityId();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;
                String projectId;

                projectId = getRootProjectId();
                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(projectId, entityId);
                if (entityDefinition != null)
                {
                    return createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions());
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8ComponentDataObjectProvider getNewDataObjectProviderInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.table.object.T8TableEntityMappedObjectProvider").getConstructor(T8TableEntityMappedObjectProviderDefinition.class, T8ComponentController.class);
        return (T8ComponentDataObjectProvider)constructor.newInstance(this, controller);
    }

    public String getDataEntityId()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_ID.toString());
    }

    public void setDataEntityId(String id)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_ID.toString(), id);
    }

    public String getDataObjectId()
    {
        return (String)getDefinitionDatum(Datum.DATA_OBJECT_ID.toString());
    }

    public void setDataObjectId(String id)
    {
        setDefinitionDatum(Datum.DATA_OBJECT_ID.toString(), id);
    }

    public String getKeyFieldId()
    {
        return getDefinitionDatum(Datum.KEY_FIELD_ID);
    }

    public void setKeyFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.KEY_FIELD_ID, fieldId);
    }
}
