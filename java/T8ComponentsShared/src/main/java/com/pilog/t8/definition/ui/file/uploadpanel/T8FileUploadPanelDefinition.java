package com.pilog.t8.definition.ui.file.uploadpanel;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FileUploadPanelDefinition extends T8ComponentDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FILE_UPLOAD_PANEL";
    public static final String DISPLAY_NAME = "File Upload Panel";
    public static final String DESCRIPTION = "A panel that allows the user to upload files to a preset remote location.";
    public static final String IDENTIFIER_PREFIX = "C_FILE_UPLOAD_PANEL_";
    private enum Datum {FILE_CONTEXT_ID,
                        FILE_PATH,
                        CLOSE_FILE_CONTEXT};
    // -------- Definition Meta-Data -------- //

    public T8FileUploadPanelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_CONTEXT_ID.toString(), "File Context", "The preset file context to which files will be uploaded."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FILE_PATH.toString(), "File Path", "The preset file path within the destination context to which files will be uploaded."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CLOSE_FILE_CONTEXT.toString(), "Close File Context", "If set to true, the file context to which files are uploaded will be closed when all uploads have been completed."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILE_CONTEXT_ID.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FileContextDefinition.GROUP_IDENTIFIER));
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.file.uploadpanel.T8FileUploadPanel").getConstructor(T8FileUploadPanelDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FileUploadPanelApiHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FileUploadPanelApiHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getFileContextId()
    {
        return (String)getDefinitionDatum(Datum.FILE_CONTEXT_ID.toString());
    }

    public void setFileContextId(String fileContextId)
    {
        setDefinitionDatum(Datum.FILE_CONTEXT_ID.toString(), fileContextId);
    }

    public String getFilePath()
    {
        return (String)getDefinitionDatum(Datum.FILE_PATH.toString());
    }

    public void setFilePath(String filePath)
    {
        setDefinitionDatum(Datum.FILE_PATH.toString(), filePath);
    }
}
