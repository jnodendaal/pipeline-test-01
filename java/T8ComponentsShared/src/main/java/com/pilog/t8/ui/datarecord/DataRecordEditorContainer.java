package com.pilog.t8.ui.datarecord;

import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface DataRecordEditorContainer
{
    // Identifiers of parameter used by Global Dialog that allows value selection.
    public static final String PARAMETER_VALUE_LOOKUP_IN_TITLE = "$P_IN_TITLE";
    public static final String PARAMETER_VALUE_LOOKUP_IN_DATA_RECORD = "$P_IN_DATA_RECORD";
    public static final String PARAMETER_VALUE_LOOKUP_IN_VALUE_REQUIREMENT = "$P_IN_VALUE_REQUIREMENT";
    public static final String PARAMETER_VALUE_LOOKUP_IN_FILTER_CONCEPT_ID_LIST = "$P_IN_FILTER_CONCEPT_ID_LIST";
    public static final String PARAMETER_VALUE_LOOKUP_IN_FILTER_ODT_ID_LIST = "$P_IN_FILTER_ODT_ID_LIST";
    public static final String PARAMETER_VALUE_LOOKUP_IN_FILTER_ORG_ID_LIST = "$P_IN_FILTER_ORG_ID_LIST";
    public static final String PARAMETER_VALUE_LOOKUP_IN_FILTER_CONCEPT_DEPENDENCY_LIST = "$P_IN_FILTER_CONCEPT_DEPENDENCY_LIST";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_VALUE = "$P_OUT_VALUE";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_LOWER_BOUND_VALUE = "$P_OUT_LOWER_BOUND_VALUE";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UPPER_BOUND_VALUE = "$P_OUT_UPPER_BOUND_VALUE";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_RECORD_ID = "$P_OUT_RECORD_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_ID = "$P_OUT_CONCEPT_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_TERM = "$P_OUT_CONCEPT_TERM";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_TERM_ID = "$P_OUT_CONCEPT_TERM_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_DEFINITION = "$P_OUT_CONCEPT_DEFINITION";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_DEFINITION_ID = "$P_OUT_CONCEPT_DEFINITION_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_CODE = "$P_OUT_CONCEPT_CODE";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_CODE_ID = "$P_OUT_CONCEPT_CODE_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_ID = "$P_OUT_UOM_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_TERM = "$P_OUT_UOM_TERM";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_TERM_ID = "$P_OUT_UOM_TERM_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_DEFINITION = "$P_OUT_UOM_DEFINITION";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_DEFINITION_ID = "$P_OUT_UOM_DEFINITION_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_CODE = "$P_OUT_UOM_CODE";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_UOM_CODE_ID = "$P_OUT_UOM_CODE_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_ID = "$P_OUT_QOM_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_TERM = "$P_OUT_QOM_TERM";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_TERM_ID = "$P_OUT_QOM_TERM_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_DEFINITION = "$P_OUT_QOM_DEFINITION";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_DEFINITION_ID = "$P_OUT_QOM_DEFINITION_ID";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_CODE = "$P_OUT_QOM_CODE";
    public static final String PARAMETER_VALUE_LOOKUP_OUT_QOM_CODE_ID = "$P_OUT_QOM_CODE_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_IN_TITLE = "$P_IN_TITLE";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_IN_FILTER_ORG_ID = "$P_IN_FILTER_ORG_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_IN_FILTER_OS_ID = "$P_IN_FILTER_OS_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_IN_FILTER_OC_ID_LIST = "$P_IN_FILTER_OC_ID_LIST";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_ID = "$P_OUT_OC_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_TERM_ID = "$P_OUT_OC_TERM_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_TERM = "$P_OUT_OC_TERM";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_DEFINITION_ID = "$P_OUT_OC_DEFINITION_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_DEFINITION = "$P_OUT_OC_DEFINITION";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_CODE_ID = "$P_OUT_OC_CODE_ID";
    public static final String PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_CODE = "$P_OUT_OC_CODE";

    public String getProjectId();
    public T8Context getAccessContext();
    public DataRecordProvider getDataRecordProvider();
    public CachedTerminologyProvider getTerminologyProvider();
    public OntologyProvider getOntologyProvider();
    public T8DataRecordAccessHandler getAccessHandler();
    public DataRecordEditorFactory getEditorComponentFactory();
    public Map<String, Object> getSelectedValue(DataRecord record, ValueRequirement valueRequirement, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList);
    public Map<String, Object> getSelectedClassification(String osId, List<String> ocIdList);

    public void unlockUI();
    public void lockUI(final String message);

    public void commitChanges();
    public DataRecord getDocument(String recordID);
    public DataRecord createNewDocument(String parentRecordId, String propertyId, String fieldId, String recordId, String drInstanceId, boolean refresh) throws Exception;
    public void addDocument(DataRecord dataRecord);
    public boolean containsDocument(String recordID);
    public void showDocument(String recordID);
    public DataRecord loadDocument(String recordID, boolean refresh);
    public DataRecord changeDocumentDRInstance(String recordID, String drInstanceID);
    public void removeDocument(String recordID);

    public String getDescriptionClassificationIdentifier(); // Identifier of the description classification type to use when displaying records.
}

