package com.pilog.t8.definition.ui.menuitem;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MenuItemDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MENU_ITEM";
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_MENU_ITEM";
    public static final String STORAGE_PATH = "/menus";
    public static final String DISPLAY_NAME = "Menu Item";
    public static final String DESCRIPTION = "A selectable menu item component that can be added to a menu.";
    public static final String IDENTIFIER_PREFIX = "C_MENU_ITEM_";
    public enum Datum {TEXT,
                       ICON_IDENTIFIER,
                       ADD_SEPERATOR};
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;

    public T8MenuItemDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text to display on the menu item."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this menu item."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ADD_SEPERATOR.toString(), "Add Seperator", "If enabled, a seperator will be added after this component.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.menuitem.T8MenuItem", new Class<?>[]{T8MenuItemDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8MenuItemAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8MenuItemAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<? extends T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setText(String text)
    {
        setDefinitionDatum(Datum.TEXT.toString(), text);
    }

    public Boolean addSeperator()
    {
        return (Boolean)getDefinitionDatum(Datum.ADD_SEPERATOR.toString());
    }

    public void setAddSeperator(Boolean addSeperator)
    {
        setDefinitionDatum(Datum.ADD_SEPERATOR.toString(), addSeperator);
    }

    public String getIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }
}
