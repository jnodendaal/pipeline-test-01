package com.pilog.t8.definition.ui.tree;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public final class T8TreeDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TREE";
    public static final String DISPLAY_NAME = "Tree";
    public static final String DESCRIPTION = "A component that displays hierarchical relational data in a navigable tree format.";
    public static final String IDENTIFIER_PREFIX = "C_TREE_";
    public enum Datum {ROOT_NODE_DEFINITION,
                       TOOL_BAR_VISIBLE,
                       ALLOW_EXPAND_ALL,
                       AUTO_SELECT_ON_RETRIEVE};
    // -------- Definition Meta-Data -------- //

    public T8TreeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.ROOT_NODE_DEFINITION.toString(), "Root Node", "The root node type of the tree."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.TOOL_BAR_VISIBLE.toString(), "Tool Bar Visible", "A boolean setting that sets the visibility of the tree tool bar.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_EXPAND_ALL.toString(), "Allow Expand All", "A boolean setting that sets if the user is allowed to user the expand all feature on this tree.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.AUTO_SELECT_ON_RETRIEVE.toString(), "Auto Select on Retrieve", "A boolean setting that enables automatic selection of the first node after the tree data has been retrieved.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ROOT_NODE_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8TreeNodeDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.tree.T8Tree", new Class<?>[]{T8TreeDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8TreeAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8TreeAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public T8TreeNodeDefinition getNodeDefinition(String nodeIdentifier)
    {
        return (T8TreeNodeDefinition)getLocalDefinition(nodeIdentifier);
    }

    public T8TreeNodeDefinition getRootNodeDefinition()
    {
        return getDefinitionDatum(Datum.ROOT_NODE_DEFINITION);
    }

    public void setRootNodeDefinition(T8TreeNodeDefinition nodeDefinition)
    {
        setDefinitionDatum(Datum.ROOT_NODE_DEFINITION, nodeDefinition);
    }

    public boolean isToolBarVisible()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.TOOL_BAR_VISIBLE));
    }

    public void setToolBarVisible(boolean visible)
    {
        setDefinitionDatum(Datum.TOOL_BAR_VISIBLE, visible);
    }

    public boolean allowExpandAll()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.ALLOW_EXPAND_ALL));
    }

    public void setAllowExpandAll(boolean allowExpandAll)
    {
        setDefinitionDatum(Datum.ALLOW_EXPAND_ALL.toString(), allowExpandAll);
    }

    public boolean isAutoSelectOnRetrieve()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.AUTO_SELECT_ON_RETRIEVE));
    }

    public void setAutoSelectOnRetrieve(boolean autoSelect)
    {
        setDefinitionDatum(Datum.AUTO_SELECT_ON_RETRIEVE, autoSelect);
    }
}
