package com.pilog.t8.definition.ui.radiobutton;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.button.group.T8ButtonGroupDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8RadioButtonGroupDefinition extends T8ButtonGroupDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_RADIO_BUTTON_GROUP";
    public static final String DISPLAY_NAME = "Radio Button Group";
    public static final String DESCRIPTION = "A component that allows the user to select one of the radio options available in a group.";
    public static final String IDENTIFIER_PREFIX = "C_RADIO_GRP_";
    public enum Datum {RADIO_BUTTONS};
    // -------- Definition Meta-Data -------- //

    public T8RadioButtonGroupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.RADIO_BUTTONS.toString(), "Radio Buttons", "The radio buttons belonging to this group."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.RADIO_BUTTONS.toString().equals(datumIdentifier))
        {
           return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RadioButtonDefinition.GROUP_IDENTIFIER));
        } else return super.getDatumOptions(definitionContext,datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.radiobutton.T8RadioButtonGroup", new Class<?>[]{T8RadioButtonGroupDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8RadioButtonDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<>(getDefinitionDatum(Datum.RADIO_BUTTONS));
    }
}
