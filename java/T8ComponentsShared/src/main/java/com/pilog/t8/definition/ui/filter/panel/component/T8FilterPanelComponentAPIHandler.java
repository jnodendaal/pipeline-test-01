package com.pilog.t8.definition.ui.filter.panel.component;

import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;


/**
 * @author Hennie Brink
 */
public class T8FilterPanelComponentAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_REFRESH = "$OC_REFRESH";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());
        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH);
        newOperationDefinition.setMetaDisplayName("Refresh");
        newOperationDefinition.setMetaDescription("Causes this component to refresh its data source and display the new data.");
        operations.add(newOperationDefinition);
        return operations;
    }
}
