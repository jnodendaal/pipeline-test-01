package com.pilog.t8.definition.ui.functionality.button;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.button.T8ButtonAPIHandler;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8FunctionalityMenu and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8FunctionalityButtonAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_SET_DATA_OBJECT = "$CO_SET_DATA_OBJECT";
    public static final String PARAMETER_FUNCTIONALITY_IDENTIFIER = "$P_FUNCTIONALITY_IDENTIFIER";
    public static final String PARAMETER_DATA_OBJECT = "$P_DATA_OBJECT";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ButtonAPIHandler.getEventDefinitions());

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ButtonAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_OBJECT);
        newOperationDefinition.setMetaDisplayName("Set Data Object");
        newOperationDefinition.setMetaDescription("This operation sets the target data object of the button, which will cause a refresh.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_OBJECT, "Data Object", "The data object to set.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        return operations;
    }
}
