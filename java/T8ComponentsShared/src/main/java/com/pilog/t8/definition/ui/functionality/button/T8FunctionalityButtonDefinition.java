package com.pilog.t8.definition.ui.functionality.button;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.button.T8ButtonDefinition;
import com.pilog.t8.definition.ui.functionality.view.T8FunctionalityViewPaneDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityButtonDefinition extends T8ButtonDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FUNCTIONALITY_BUTTON";
    public static final String DISPLAY_NAME = "Functionality Button";
    public static final String DESCRIPTION = "A component that displays all a button to access a system functionality.";
    public static final String IDENTIFIER_PREFIX = "C_FUNC_BUTTON_";
    public enum Datum {FUNCTIONALITY_IDENTIFIER,
                       FUNCTIONALITY_CONTROLLER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8FunctionalityButtonDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_IDENTIFIER.toString(), "Functionality", "The functionality that will be accessed when this button is pressed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FUNCTIONALITY_CONTROLLER_IDENTIFIER.toString(), "Functionality Controller", "The view where selected functionality will be opened."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FUNCTIONALITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FunctionalityDefinition.GROUP_IDENTIFIER));
        else if (Datum.FUNCTIONALITY_CONTROLLER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> options;
            List<T8Definition> localDefinitions;

            localDefinitions = this.getLocalDefinitions();
            options = new ArrayList<T8DefinitionDatumOption>();
            for (T8Definition localDefinition : localDefinitions)
            {
                if (localDefinition.getMetaData().getTypeId().equals(T8FunctionalityViewPaneDefinition.TYPE_IDENTIFIER))
                {
                    options.add(new T8DefinitionDatumOption(localDefinition.getIdentifier(), localDefinition.getIdentifier()));
                }
            }

            return options;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        // Make sure the super definition is initialized.
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.functionality.button.T8FunctionalityButton").getConstructor(T8FunctionalityButtonDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FunctionalityButtonAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FunctionalityButtonAPIHandler.getOperationDefinitions();
    }

    public String getFunctionalityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER.toString());
    }

    public void setFunctionalityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_IDENTIFIER.toString(), identifier);
    }

    public String getFunctionalityControllerIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FUNCTIONALITY_CONTROLLER_IDENTIFIER.toString());
    }

    public void setFunctionalityControllerIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FUNCTIONALITY_CONTROLLER_IDENTIFIER.toString(), identifier);
    }
}
