package com.pilog.t8.ui.tree;

import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8TreeNodeEditorEvent extends EventObject
{
    public T8TreeNodeEditorEvent(T8TreeNode sourceNode)
    {
        super(sourceNode);
    }
    
    public T8TreeNode getTreeNode()
    {
        return (T8TreeNode)getSource();
    }
}
