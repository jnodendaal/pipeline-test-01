package com.pilog.t8.definition.ui.cellrenderer.table.label.formats;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.time.T8TimestampFormatter;
import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererFormatDefinition;
import com.pilog.t8.utilities.date.Dates;
import java.text.Format;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class DateFormatDefinition extends T8LabelTableCellRendererFormatDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_RENDERER_LABEL_FORMATER_DATE";
    public static final String DISPLAY_NAME = "Date Formatter";
    public static final String DESCRIPTION = "Formatter that will use a date pattern to format a date";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public DateFormatDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public Format getFormat()
    {
        return new T8TimestampFormatter(getFormatPattern());
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, T8LabelTableCellRendererFormatDefinition.Datum.FORMAT_PATTERN.toString(), "Format Pattern", "The format pattern that will be used for this formatter.", Dates.STD_SYS_TS_FORMAT));

        return datumTypes;
    }
}
