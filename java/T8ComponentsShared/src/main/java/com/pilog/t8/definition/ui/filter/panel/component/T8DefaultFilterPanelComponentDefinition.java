package com.pilog.t8.definition.ui.filter.panel.component;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.filter.panel.T8FilterPanelDefinition;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8DefaultFilterPanelComponentDefinition extends T8ComponentDefinition implements T8FilterPanelComponentDefinition
{
    public T8DefaultFilterPanelComponentDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, T8FilterPanelComponentDefinition.Datum.DISPLAY_LABEL.toString(), "Display Label", "The Display name that will indicate to the user the field that will be filtered."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, T8FilterPanelComponentDefinition.Datum.DATA_ENTITY_KEY_FIELD_IDENTIFIER.toString(), "Data Entity Key Field", "The data entity field that will be used when constructing the filter for this component."));
        if (addTagFieldDatumTypes())
        {
            datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, T8FilterPanelComponentDefinition.Datum.IS_TAG_FIELD.toString(), "Is Tag Field?", "Set wether the the selected field identifier is a tag field and tag filtering should be used.", false));
        }
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
            T8DefinitionContext definitionContext, String datumIdentifier)
            throws Exception
    {
        if (T8FilterPanelComponentDefinition.Datum.DATA_ENTITY_KEY_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8FilterPanelDefinition parentDefinition = (T8FilterPanelDefinition) getAncestorDefinition(T8FilterPanelDefinition.TYPE_IDENTIFIER);
            T8Definition dataSourceDefinition = definitionContext.getRawDefinition(getRootProjectId(), parentDefinition.getDataEntityDefinitionIdentifier());
            T8DataEntityDefinition dataEntityDefinition = ((T8DataEntityDefinition) dataSourceDefinition);
            if (dataEntityDefinition == null || dataEntityDefinition.getFieldDefinitions() == null)
            {
                return new ArrayList<T8DefinitionDatumOption>();
            }
            ArrayList<T8DefinitionDatumOption> options = new ArrayList<T8DefinitionDatumOption>(dataEntityDefinition.getFieldDefinitions().size());
            for (T8DataEntityFieldDefinition data : dataEntityDefinition.getFieldDefinitions())
            {
                options.add(new T8DefinitionDatumOption(data.getPublicIdentifier(), data.getPublicIdentifier()));
            }
            return options;
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8FilterPanelComponentAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8FilterPanelComponentAPIHandler.getOperationDefinitions();
    }

    protected Boolean addTagFieldDatumTypes()
    {
        return true;
    }

    @Override
    public void setDataEntityKeyFieldIdentifier(String definitions)
    {
        setDefinitionDatum(T8FilterPanelComponentDefinition.Datum.DATA_ENTITY_KEY_FIELD_IDENTIFIER.toString(), definitions);
    }

    @Override
    public String getDataEntityKeyFieldIdentifier()
    {
        return (String) getDefinitionDatum(T8FilterPanelComponentDefinition.Datum.DATA_ENTITY_KEY_FIELD_IDENTIFIER.toString());
    }

    @Override
    public void setDisplayLabel(String name)
    {
        setDefinitionDatum(T8FilterPanelComponentDefinition.Datum.DISPLAY_LABEL.toString(), name);
    }

    @Override
    public String getDisplayLabel()
    {
        return (String) getDefinitionDatum(T8FilterPanelComponentDefinition.Datum.DISPLAY_LABEL.toString());
    }

    @Override
    public void setTagField(Boolean isTagField)
    {
        setDefinitionDatum(T8FilterPanelComponentDefinition.Datum.IS_TAG_FIELD.toString(), isTagField);
    }

    @Override
    public Boolean isTagField()
    {
        Boolean isTagField = (Boolean) getDefinitionDatum(T8FilterPanelComponentDefinition.Datum.IS_TAG_FIELD.toString());
        if (isTagField == null)
        {
            return false;
        }
        return isTagField;
    }
}
