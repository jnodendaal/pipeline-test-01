package com.pilog.t8.definition.ui.datarecordviewer;

import com.pilog.t8.data.document.datarecord.RecordContent;

/**
 * @author Bouwer du Preez
 */
public interface T8ComparisonType
{
    public void clear();
    public boolean matchesRecord(RecordContent record);
    public void addDataRecord(RecordContent record);
}
