package com.pilog.t8.definition.ui.wizard;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WizardDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_WIZARD";
    public static final String DISPLAY_NAME = "Wizard";
    public static final String DESCRIPTION = "A Wizard component that guides a user through a sequence of steps in order to achieve a specific goal.";
    public static final String IDENTIFIER_PREFIX = "C_WIZ_";
    public enum Datum
    {
        TITLE,
        TITEL_ICON_IDENTIFIER,
        WIZARD_DESCRIPTION,
        INITIALIZATION_SCRIPT,
        WIZARD_PARAMETER_DEFINITIONS,
        STEPS,
        HEADER_BACKGROUND_PAINTER_IDENTIFIER,
        NAVIGATOR_BACKGROUND_PAINTER_IDENTIFIER,
        CONTROL_BACKGROUND_PAINTER_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;
    private T8PainterDefinition headerBackgroundPainterDefinition;
    private T8PainterDefinition navigatorBackgroundPainterDefinition;
    private T8PainterDefinition controlBackgroundPainterDefinition;

    public T8WizardDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STEPS.toString(), "Steps", "The steps belonging to this wizard."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TITLE.toString(), "Title", "The title for this wizard."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TITEL_ICON_IDENTIFIER.toString(), "Title Icon", "The Icon that will be displayed for the title."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.WIZARD_DESCRIPTION.toString(), "Description", "The description of this wizard"));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.WIZARD_PARAMETER_DEFINITIONS.toString(), "Wizard Parameters", "The parameters that will be compiled throughout the steps in the wizard and returned as a result."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.INITIALIZATION_SCRIPT.toString(), "Initialization Script", "The script that will be executed when this wizard is initialized."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Header Background Painter", "The Background Painter that will be use on the header."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NAVIGATOR_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Navigator Background Painter", "The Background Painter that will be used on the step navigator pane."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONTROL_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Control Background Painter", "The Background Painter that will be used on the control pane."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.WIZARD_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.STEPS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8WizardStepDefinition.GROUP_IDENTIFIER));
        else if (Datum.HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.NAVIGATOR_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.TITEL_ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.CONTROL_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.INITIALIZATION_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8WizardInitializationScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.WIZARD_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8ServerContext serverContext;
        String backgroundPainterId;
        String iconId;

        // Pre-load the header background painter.
        serverContext = context.getServerContext();
        backgroundPainterId = getHeaderBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            headerBackgroundPainterDefinition = (T8PainterDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }

        // Pre-load the control background painter.
        backgroundPainterId = getControlBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            controlBackgroundPainterDefinition = (T8PainterDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }

        // Pre-load the navigator background painter.
        backgroundPainterId = getNavigatorBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            navigatorBackgroundPainterDefinition = (T8PainterDefinition) serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }

        // Pre-load the icon definition if one is specified.
        iconId = getTitleIconIdentifier();
        if (iconId != null)
        {
            iconDefinition = (T8IconDefinition) serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.wizard.T8Wizard", new Class<?>[]{T8WizardDefinition.class, T8ComponentController.class}, this, controller);
    }

    public T8WizardStep getWizardSteps(T8ComponentController controller)
    {
        T8WizardStep rootStep;

        rootStep = new T8WizardStep(getIdentifier());
        for (T8WizardStepDefinition stepDefinition : getStepDefinitions())
        {
            rootStep.addSubStep(stepDefinition.createWizardStep(controller));
        }

        return rootStep;
    }

    public List<T8WizardStepDefinition> getAllStepDefinitions()
    {
        List<T8WizardStepDefinition> definitions;

        definitions = new ArrayList<T8WizardStepDefinition>();
        for (T8WizardStepDefinition stepDefinition : getStepDefinitions())
        {
            definitions.add(stepDefinition);
            definitions.addAll(stepDefinition.getDescendantStepDefinitions());
        }

        return definitions;
    }

    public List<T8DataParameterDefinition> getWizardParameterDefinitions()
    {
        return (List<T8DataParameterDefinition>)getDefinitionDatum(Datum.WIZARD_PARAMETER_DEFINITIONS.toString());
    }

    public void setWizardParameterDefinitions(List<T8DataParameterDefinition> definitions)
    {
        setDefinitionDatum(Datum.WIZARD_PARAMETER_DEFINITIONS.toString(), definitions);
    }

    public T8WizardInitializationScriptDefinition getInitializationScriptDefinition()
    {
        return (T8WizardInitializationScriptDefinition)getDefinitionDatum(Datum.INITIALIZATION_SCRIPT.toString());
    }

    public void setInitializationScriptDefinition(T8WizardInitializationScriptDefinition initializationScriptDefinition)
    {
        setDefinitionDatum(Datum.INITIALIZATION_SCRIPT.toString(), initializationScriptDefinition);
    }

    public String getControlBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONTROL_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setControlBackgroundPainterIdentifier(String painter)
    {
        setDefinitionDatum(Datum.CONTROL_BACKGROUND_PAINTER_IDENTIFIER.toString(), painter);
    }

    public T8PainterDefinition getControlBackgroundPainterDefinition()
    {
        return controlBackgroundPainterDefinition;
    }

    public String getHeaderBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setHeaderBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.HEADER_BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public T8PainterDefinition getHeaderBackgroundPainterDefinition()
    {
        return headerBackgroundPainterDefinition;
    }

    public String getNavigatorBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.NAVIGATOR_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setNavigatorBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.NAVIGATOR_BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getNavigatorBackgroundPainterDefinition()
    {
        return navigatorBackgroundPainterDefinition;
    }

    public List<T8WizardStepDefinition> getStepDefinitions()
    {
        return (List<T8WizardStepDefinition>)getDefinitionDatum(Datum.STEPS.toString());
    }

    public void setStepDefinitions(List<T8WizardStepDefinition> steps)
    {
        setDefinitionDatum(Datum.STEPS.toString(), steps);
    }

    public T8WizardStepDefinition getStepDefinition(String identifier)
    {
        for (T8WizardStepDefinition setpDefinition : getStepDefinitions())
        {
            if (setpDefinition.getIdentifier().equals(identifier)) return setpDefinition;
        }

        return null;
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8WizardAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8WizardAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        List<T8WizardStepDefinition> stepDefinitions;

        stepDefinitions = getStepDefinitions();
        if (stepDefinitions != null)
        {
            ArrayList<T8ComponentDefinition> componentDefinitions;

            componentDefinitions = new ArrayList<T8ComponentDefinition>();
            for (T8Definition stepDefinition : stepDefinitions)
            {
                componentDefinitions.addAll(((T8WizardStepDefinition)stepDefinition).getChildComponentDefinitions());
            }

            return componentDefinitions;
        }
        else return new ArrayList<T8ComponentDefinition>();
    }

    public String getTitle()
    {
        return (String) getDefinitionDatum(Datum.TITLE.toString());
    }

    public void setTitle(String title)
    {
        setDefinitionDatum(Datum.TITLE.toString(), title);
    }

    public String getTitleIconIdentifier()
    {
        return (String) getDefinitionDatum(Datum.TITEL_ICON_IDENTIFIER.toString());
    }

    public void setTitleIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TITEL_ICON_IDENTIFIER.toString(), identifier);
    }

    public T8IconDefinition getTitleIconDefinition()
    {
        return iconDefinition;
    }
    public String getWizardDescription()
    {
        return (String) getDefinitionDatum(Datum.WIZARD_DESCRIPTION.toString());
    }
        List<T8ComponentDefinition> childDefinitions;

    public void setWizardDescription(String description)
    {
        setDefinitionDatum(Datum.WIZARD_DESCRIPTION.toString(), description);
    }
}
