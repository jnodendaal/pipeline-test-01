package com.pilog.t8.definition.ui.datasearch.datepicker;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchDatePickerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_SEARCH_DATE_PICKER";
    public static final String DISPLAY_NAME = "Data Search Date Picker";
    public static final String DESCRIPTION = "A date picker component that can be used to create a data filter for the user";
    public static final String IDENTIFIER_PREFIX = "C_SEARCH_";

    public enum Datum
    {
        SEARCH_DISPLAY_NAME,
        DATA_ENTITY_IDENTIFIER,
        SEARCH_FIELD_IDENTIFIERS,
        TARGET_TABLE_IDENTIFIER,
        USE_NUMERIC_DATE_VALUES
    };
// -------- Definition Meta-Data -------- //
    public T8DataSearchDatePickerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.SEARCH_DISPLAY_NAME.toString(), "Search Display Name", "The display name that will indicate what this date picker will search on."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The data entity on which the search will be performed."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.SEARCH_FIELD_IDENTIFIERS.toString(), "Search Fields", "The entity fields on which the search will be performed.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.TARGET_TABLE_IDENTIFIER.toString(), "Target Table Identifier", "The table that will receive the user filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.USE_NUMERIC_DATE_VALUES.toString(), "Use Numeric Date Value", "Specifies whether or not numeric (Long) values should be used for the filter.", false));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.SEARCH_FIELD_IDENTIFIERS.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = entityDefinition.getFieldDefinitions();
                    if (fieldDefinitions != null)
                    {
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            optionList.add(new T8DefinitionDatumOption(fieldDefinition.getPublicIdentifier(), fieldDefinition.getPublicIdentifier()));
                        }
                    }

                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else if (Datum.TARGET_TABLE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> definitionDatumOptions;
            List<T8Definition> tableDefinitions;

            tableDefinitions = getLocalDefinitionsOfType(T8TableDefinition.TYPE_IDENTIFIER);
            definitionDatumOptions = createLocalIdentifierOptionsFromDefinitions(tableDefinitions);

            definitionDatumOptions.add(0, new T8DefinitionDatumOption("Use Event Handler", null));

            return definitionDatumOptions;
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.datasearch.datepicker.T8DataSearchDatePicker", new Class<?>[]{T8ComponentController.class, T8DataSearchDatePickerDefinition.class}, controller, this);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataSearchDatePickerAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataSearchDatePickerAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getSearchDisplayName()
    {
        return getDefinitionDatum(Datum.SEARCH_DISPLAY_NAME);
    }

    public void setSearchDisplayName(String label)
    {
        setDefinitionDatum(Datum.SEARCH_DISPLAY_NAME, label);
    }

    public String getDataEntityIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER);
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER, identifier);
    }

    public List<String> getSearchFieldIdentifiers()
    {
        return getDefinitionDatum(Datum.SEARCH_FIELD_IDENTIFIERS);
    }

    public void setSearchFieldIdentifiers(List<String> fieldIdentifiers)
    {
        setDefinitionDatum(Datum.SEARCH_FIELD_IDENTIFIERS, fieldIdentifiers);
    }

    public String getTargetTableIdentifier()
    {
        return getDefinitionDatum(Datum.TARGET_TABLE_IDENTIFIER);
    }

    public void setTargetTableIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_TABLE_IDENTIFIER, identifier);
    }

    public boolean isUseNumericDateValues()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.USE_NUMERIC_DATE_VALUES));
    }

    public void setUseNumericDateValues(boolean useNumericDateValues)
    {
        setDefinitionDatum(Datum.USE_NUMERIC_DATE_VALUES, useNumericDateValues);
    }
}
