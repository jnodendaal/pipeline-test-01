package com.pilog.t8.definition.ui.tree;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Tree and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8TreeAPIHandler implements T8DefinitionResource
{
    public static final String EVENT_TREE_NODE_SELECTION_CHANGED = "$CE_TREE_NODE_SELECTION_CHANGED";
    public static final String EVENT_TREE_NODE_CLICKED = "$CE_TREE_NODE_CLICKED";
    public static final String EVENT_TREE_NODE_DOUBLE_CLICKED = "$CE_TREE_NODE_DOUBLE_CLICKED";

    public static final String SERVER_OPERATION_LOAD_TREE_NODES = "@OS_LOAD_TREE_NODES";

    public static final String OPERATION_GET_FIRST_SELECTED_DATA_ENTITY = "$CO_GET_FIRST_SELECTED_DATA_ENTITY";
    public static final String OPERATION_GET_SELECTED_DATA_ENTITIES = "$CO_GET_SELECTED_DATA_ENTITIES";
    public static final String OPERATION_SET_NODE_PREFILTER = "$CO_SET_NODE_PREFILTER";
    public static final String OPERATION_GET_PREFILTER = "$CO_GET_PREFILTER";
    public static final String OPERATION_REFRESH = "$CO_REFRESH";
    public static final String OPERATION_REFRESH_SELECTED_NODE = "$CO_REFRESH_SELECTED_NODE";
    public static final String OPERATION_REFRESH_SELECTED_NODE_PARENT = "$CO_REFRESH_SELECTED_NODE_PARENT";

    public static final String PARAMETER_KEY_MAP = "$P_KEY_MAP";
    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";
    public static final String PARAMETER_DATA_ENTITY_LIST = "$P_DATA_ENTITY_LIST";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$P_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_PREFILTER_INDEX = "$P_PREFILTER_INDEX";
    public static final String PARAMETER_REFRESH_PARENT_NODES = "$P_REFRESH_PARENT_NODES";
    public static final String PARAMETER_REFRESH_TREE = "$P_REFRESH_TREE";
    public static final String PARAMETER_TREE_NODE_IDENTIFIER = "$P_TREE_NODE_IDENTIFIER";
    public static final String PARAMETER_TREE_NODE_IDENTIFIER_LIST = "$P_TREE_NODE_IDENTIFIER_LIST";
    public static final String PARAMETER_TREE_NODE_DEFINITION = "$P_TREE_NODE_DEFINITION";
    public static final String PARAMETER_TREE_NODE = "$P_TREE_NODE";
    public static final String PARAMETER_TREE_NODE_LIST = "$P_TREE_NODE_LIST";
    public static final String PARAMETER_TREE_NODE_PAGE_OFFSET = "$P_TREE_NODE_PAGE_OFFSET";
    public static final String PARAMETER_TREE_NODE_PAGE_SIZE = "$P_TREE_NODE_PAGE_SIZE";
    public static final String PARAMETER_TREE_NODE_COUNT = "$P_TREE_NODE_COUNT";
    public static final String PARAMETER_TREE_NODE_FILTER_MAP = "$P_TREE_NODE_FILTER_MAP";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TREE_NODE_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Tree Node Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the node selection on the tree changes.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE_IDENTIFIER_LIST, "Tree Node Types", "A List of the currently selected tree node types.", T8DataType.DEFINITION_IDENTIFIER_LIST));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE_LIST, "Tree Nodes", "A List of the currently selected tree nodes.", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Selected Data Entities", "A List of the currently selected data entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TREE_NODE_CLICKED);
        newEventDefinition.setMetaDisplayName("Tree Node Clicked");
        newEventDefinition.setMetaDescription("This event occurs when the node is clicked.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE_IDENTIFIER, "Tree Node Identifier", "The currently selected tree node identifier.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE, "Tree Node", "A List of the currently selected tree nodes.", T8DataType.CUSTOM_OBJECT));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Selected Data Entitie", "A List of the currently selected data entities.", T8DataType.DATA_ENTITY));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_TREE_NODE_DOUBLE_CLICKED);
        newEventDefinition.setMetaDisplayName("Tree Node Double Clicked");
        newEventDefinition.setMetaDescription("This event occurs when the node is double clicked.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE_IDENTIFIER, "Tree Node Identifier", "The currently selected tree node identifier.", T8DataType.DEFINITION_IDENTIFIER));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE, "Tree Node", "A List of the currently selected tree nodes.", T8DataType.CUSTOM_OBJECT));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Selected Data Entitie", "A List of the currently selected data entities.", T8DataType.DATA_ENTITY));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_NODE_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets a pre-filter on a specified tree node.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TREE_NODE_IDENTIFIER, "Node Identifier", "The identifier of the tree node on which to set the prefilter.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Prefilter Identifier", "The Identifier of the prefilter to set.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set on the tree node.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_PARENT_NODES, "Refresh Parent Nodes", "A boolean value indicating whether the parent tree nodes of the affected nodes should be refreshed after the pre-filter has been set (default=false).", T8DataType.BOOLEAN));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_TREE, "Refresh Tree", "A boolean value indicating whether the tree should be refreshed after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Get Prefilter");
        newOperationDefinition.setMetaDescription("This operation returns one of the prefilters currently set on a tree node.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Prefilter Identifier", "The Identifier of the prefilter to fetch.", T8DataType.DEFINITION_IDENTIFIER));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Prefilter", "The specified data filter fetched from the table.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_FIRST_SELECTED_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get First Selected Data Entity");
        newOperationDefinition.setMetaDescription("This operation returns the first selected data entity on the tree.  If no nodes are selected a null value is returned.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The first selected entity on the tree.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Get Selected Data Entities");
        newOperationDefinition.setMetaDescription("This operation returns a list of all the selected data entities in the tree.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "A List of data entities currently selected on the tree.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH);
        newOperationDefinition.setMetaDisplayName("Refresh");
        newOperationDefinition.setMetaDescription("This operation re-retrieves the data displayed by the tree.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH_SELECTED_NODE);
        newOperationDefinition.setMetaDisplayName("Refresh Seleced Node");
        newOperationDefinition.setMetaDescription("This operation re-retrieves the child nodes of the selected node.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH_SELECTED_NODE_PARENT);
        newOperationDefinition.setMetaDisplayName("Refresh Seleced Node Parent");
        newOperationDefinition.setMetaDescription("This operation re-retrieves the child nodes of the selected node's parent.");
        operations.add(newOperationDefinition);

        return operations;
    }

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();

            definition = new T8JavaServerOperationDefinition(SERVER_OPERATION_LOAD_TREE_NODES);
            definition.setMetaDescription("Load Tree Nodes");
            definition.setClassName("com.pilog.t8.ui.tree.server.T8TreeServer$LoadTreeNodesOperation");
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
