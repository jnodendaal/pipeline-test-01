package com.pilog.t8.definition.ui.celleditor.table.checkbox;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.celleditor.T8TableCellEditorDefinition;
import com.pilog.t8.definition.ui.checkbox.T8CheckBoxDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxTableCellEditorDefinition extends T8CheckBoxDefinition implements T8TableCellEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_EDITOR_CHECK_BOX";
    public static final String DISPLAY_NAME = "Check Box Cell Editor";
    public static final String DESCRIPTION = "A cell editor that uses a checkbox to allow the user to set a selected or deselected value.";
    public static final String VERSION = "0";
    public enum Datum {TARGET_ENTITY_IDENTIFIER,
                       TARGET_FIELD_IDENTIFIER,
                       SELECTED_VALUE_EXPRESSION,
                       DESELECTED_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8CheckBoxTableCellEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_ENTITY_IDENTIFIER.toString(), "Target Entity", "The data entity that will be modified/acted on by this cell editor."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_FIELD_IDENTIFIER.toString(), "Target Field", "The data entity field that will be modified/acted on by this cell editor."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.SELECTED_VALUE_EXPRESSION.toString(), "Selected Value Expression", "An expression that evaluates to the value to use for this checkbox's selected state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DESELECTED_VALUE_EXPRESSION.toString(), "Deselected Value Expression", "An expression that evaluates to the value to use for this checkbox's deselected state."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TARGET_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TARGET_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String targetEntityId;

            optionList = new ArrayList<>();

            targetEntityId = getTargetEntityIdentifier();
            if (targetEntityId != null)
            {
                T8DataEntityDefinition targetEntityDefinition;

                targetEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), targetEntityId);

                if (targetEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> targetFieldDefinitions;

                    targetFieldDefinitions = targetEntityDefinition.getFieldDefinitions();
                    for (T8DataEntityFieldDefinition targetFieldDefinition : targetFieldDefinitions)
                    {
                        optionList.add(new T8DefinitionDatumOption(targetFieldDefinition.getPublicIdentifier(), targetFieldDefinition.getPublicIdentifier()));
                    }

                    return optionList;
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.celleditor.table.checkbox.T8CheckBoxTableCellEditor", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    public String getTargetEntityIdentifier()
    {
        return getDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER);
    }

    public void setTargetEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER, identifier);
    }

    public String getTargetFieldIdentifier()
    {
        return getDefinitionDatum(Datum.TARGET_FIELD_IDENTIFIER);
    }

    public void setTargetFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_FIELD_IDENTIFIER, identifier);
    }

    public String getSelectedValueExpression()
    {
        return getDefinitionDatum(Datum.SELECTED_VALUE_EXPRESSION);
    }

    public void setSelectedValueExpression(String expression)
    {
        setDefinitionDatum(Datum.SELECTED_VALUE_EXPRESSION, expression);
    }

    public String getDeselectedValueExpression()
    {
        return getDefinitionDatum(Datum.DESELECTED_VALUE_EXPRESSION);
    }

    public void setDeselectedValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DESELECTED_VALUE_EXPRESSION, expression);
    }
}
