package com.pilog.t8.ui.tree;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8TreeNodeEditorListener extends EventListener
{
    public void pageIncremented(T8TreeNodeEditorEvent event);
    public void pageDecremented(T8TreeNodeEditorEvent event);
    public void filtersRemoved(T8TreeNodeEditorEvent event);
    public void goToPage(T8TreeNodeEditorEvent event, int pageNumber);
}
