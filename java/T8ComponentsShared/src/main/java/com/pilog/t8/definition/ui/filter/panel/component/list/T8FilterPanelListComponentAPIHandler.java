package com.pilog.t8.definition.ui.filter.panel.component.list;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.T8FilterPanelComponentAPIHandler;
import java.util.ArrayList;


/**
 * @author Hennie Brink
 */
public class T8FilterPanelListComponentAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGED = "$CE_SELECTION_CHANGED";

    public static final String OPERATION_SET_DATA_SOURCE_FILTER = "$OC_SET_DATA_SOURCE_FILTER";

    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_REFRESH = "$CP_REFRESH";
    public static final String PARAMETER_NEW_VALUE = "$CP_NEW_VALUE";
    public static final String PARAMETER_NEW_VALUE_LIST = "$CP_NEW_VALUE_LIST";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8FilterPanelComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Selection Changes");
        newEventDefinition.setMetaDescription("This event occurs when the user selects a new item.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NEW_VALUE, "New Value", "The new value selected by the user. The object of the value will be the same as that of the data entity field specified.", T8DataType.CUSTOM_OBJECT));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_NEW_VALUE_LIST, "New Value List", "If the component supports list selection returns a list of selected objects. The object of the value will be the same as that of the data entity field specified.", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        operations = new ArrayList<T8ComponentOperationDefinition>(T8FilterPanelComponentAPIHandler.getOperationDefinitions());

        T8ComponentOperationDefinition newOperationDefinition;

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_DATA_SOURCE_FILTER);
        newOperationDefinition.setMetaDisplayName("Set Data Source Filter");
        newOperationDefinition.setMetaDescription("Set a new filter for the data source of this list component.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The new data filter that will be used to filter the data source items of this component.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH, "Refresh", "If the component should be refreshed after the new filter has been set.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        return operations;
    }
}
