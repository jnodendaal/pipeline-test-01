package com.pilog.t8.definition.ui.table.script;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.entity.T8DataEntityValidationError;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import java.util.ArrayList;
import java.util.List;

import static com.pilog.t8.definition.ui.table.T8TableAPIHandler.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TableCommitScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_TABLE_COMMIT";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_TABLE_COMMIT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "T8Table: Commit Script";
    public static final String DESCRIPTION = "A script that is executed on the server-side when table must be persisted.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_TABLE_CHANGE_LIST = "$P_TABLE_CHANGE_LIST";
    public static final String PARAMETER_OUTPUT_ENTITY_LIST = "$P_OUTPUT_ENTITY_LIST";

    public T8TableCommitScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterDefinitions;
        T8DataParameterDefinition parameterDefinition;

        parameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_TABLE_CHANGE_LIST, PARAMETER_TABLE_CHANGE_LIST, "The input list of data changes to be persisted.", T8DataType.LIST);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        return parameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterDefinitions;
        T8DataParameterDefinition parameterDefinition;

        parameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_OUTPUT_ENTITY_LIST, PARAMETER_OUTPUT_ENTITY_LIST, "The output list of data entities that will be updated on the table to reflect any changes that may have occurred during the commit operation.", T8DataType.LIST);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition); // I think this might be a problem
        parameterDefinitions.add(parameterDefinition);

        parameterDefinition = new T8DataParameterDefinition(PARAMETER_VALIDATION_REPORT_LIST, PARAMETER_VALIDATION_REPORT_LIST, "The list of validation reports (one for each entity where applicable) created during execution of the commit script.", T8DataType.LIST);
        T8DefinitionUtilities.assignDefinitionParent(this, parameterDefinition);
        parameterDefinitions.add(parameterDefinition);

        return parameterDefinitions;
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<T8ScriptClassImport>();
        classImports.add(new T8ScriptClassImport(T8DataEntityValidationReport.class.getSimpleName(), T8DataEntityValidationReport.class));
        classImports.add(new T8ScriptClassImport(T8DataEntityValidationError.class.getSimpleName(), T8DataEntityValidationError.class));
        return classImports;
    }
}
