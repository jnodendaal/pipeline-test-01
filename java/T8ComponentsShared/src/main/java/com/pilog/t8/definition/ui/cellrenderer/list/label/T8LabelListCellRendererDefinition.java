package com.pilog.t8.definition.ui.cellrenderer.list.label;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.cellrenderer.T8LabelCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8ListCellRendererDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8LabelListCellRendererDefinition extends T8LabelCellRendererDefinition implements T8ListCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_LIST_CELL_RENDERER_LABEL";
    public static final String DISPLAY_NAME = "Label List Cell Renderer";
    public static final String DESCRIPTION = "A cell editor that displays the content of a list cell as a simple text string.";
    public enum Datum {DISPLAY_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8LabelListCellRendererDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DISPLAY_VALUE_EXPRESSION.toString(), "Display Value Expression", "An optional expression that is evaluated using the renderer data in order to determine the value to display."));
        return datumTypes;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.ui.cellrenderer.list.label.T8LabelListCellRenderer").getConstructor(this.getClass(), T8ComponentController.class);
            return (T8Component)constructor.newInstance(this, controller);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public String getDisplayValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION.toString());
    }

    public void setDisplayValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION.toString(), expression);
    }    
}
