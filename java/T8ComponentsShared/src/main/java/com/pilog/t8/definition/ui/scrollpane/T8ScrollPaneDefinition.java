package com.pilog.t8.definition.ui.scrollpane;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ScrollPaneDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_SCROLL_PANE";
    public static final String DISPLAY_NAME = "Scroll Pane";
    public static final String DESCRIPTION = "A container to which another component can be added.  The contained provides handles for scrolling the view over the view component it contains.";
    public static final String IDENTIFIER_PREFIX = "C_SCROLL_PANE_";
    public enum Datum {VIEW_COMPONENT_DEFINITION};
    // -------- Definition Meta-Data -------- //
    
    public T8ScrollPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.VIEW_COMPONENT_DEFINITION.toString(), "View Component", "The component contained by this scrollpane."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.VIEW_COMPONENT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;
            
        constructor = Class.forName("com.pilog.t8.ui.scrollpane.T8ScrollPane").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }
    
    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<T8DataValueDefinition>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return new ArrayList<T8ComponentEventDefinition>();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return new ArrayList<T8ComponentOperationDefinition>();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;
        T8ComponentDefinition viewComponentDefinition;
        
        viewComponentDefinition = getViewComponentDefinition();
        childComponentDefinitions = new ArrayList<T8ComponentDefinition>();
        if (viewComponentDefinition != null) childComponentDefinitions.add(viewComponentDefinition);
        return childComponentDefinitions;
    }     
    
    public T8ComponentDefinition getViewComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.VIEW_COMPONENT_DEFINITION.toString());
    }

    public void setViewComponentDefinition(T8ComponentDefinition componentDefinition)
    {
         setDefinitionDatum(Datum.VIEW_COMPONENT_DEFINITION.toString(), componentDefinition);
    }
}
