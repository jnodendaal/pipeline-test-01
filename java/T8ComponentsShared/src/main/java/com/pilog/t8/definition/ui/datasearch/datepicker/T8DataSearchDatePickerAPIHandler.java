package com.pilog.t8.definition.ui.datasearch.datepicker;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataSearch and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataSearchDatePickerAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SEARCH = "$CE_SEARCH";
    public static final String COMPONENT_OPERATION_GET_DATA_FILTER = "$CO_GET_DATA_FILTER";
    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SEARCH);
        newEventDefinition.setMetaDisplayName("Search");
        newEventDefinition.setMetaDescription("This event occurs when the user performs the search action.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Search Data Filter", "The data filter object that will be produced by this search as output.", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(COMPONENT_OPERATION_GET_DATA_FILTER);
        newOperationDefinition.setMetaDisplayName("Get Data Filter");
        newOperationDefinition.setMetaDescription("This operation gets the data filter containing the user criteria.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Search Data Filter", "The data filter object that will be produced by this search as output.", T8DataType.CUSTOM_OBJECT));
        operations.add(newOperationDefinition);

        return operations;
    }
}
