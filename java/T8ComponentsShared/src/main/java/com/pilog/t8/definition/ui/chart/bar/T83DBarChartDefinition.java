package com.pilog.t8.definition.ui.chart.bar;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.chart.T8ChartDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T83DBarChartDefinition extends T8ChartDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CHART_3DBAR";
    public static final String DISPLAY_NAME = "3D Bar Chart";
    public static final String DESCRIPTION = "A component that displays a three dimensional bar chart.";
    public static final String IDENTIFIER_PREFIX = "C_CHART_3DBAR_";
    public enum Datum { X_AXIS_LABEL,
                        Y_AXIS_LABEL,
                        BAR_ORIENTATION,
                        RANGE_INTEGER_VALUES_ONLY,
                        RANGE_MIN_SIZE,
                        RANGE_LOWER_BOUND,
                        RANGE_UPPER_BOUND};
    // -------- Definition Meta-Data -------- //

    public enum BarOrientation {HORIZONTAL, VERTICAL};

    public T83DBarChartDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.X_AXIS_LABEL.toString(), "X-Axis Label", "The display label on the x-axis of the chart."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.Y_AXIS_LABEL.toString(), "Y-Axis Label", "The display label on the y-axis of the chart."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BAR_ORIENTATION.toString(), "Bar Orientation", "The orientation of the bars on the chart.", BarOrientation.VERTICAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RANGE_INTEGER_VALUES_ONLY.toString(), "Range Integer Values Only", "If set, the closest integer values are used and decimals are ignored."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.RANGE_MIN_SIZE.toString(), "Range Minimum Size", "The minimum size to be displayed for the range."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.RANGE_LOWER_BOUND.toString(), "Range Lower Bound", "The lower bound value for the range."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.RANGE_UPPER_BOUND.toString(), "Range Upper Bound", "The upper bound value for the range."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.BAR_ORIENTATION.toString().equals(datumIdentifier)) return createStringOptions(BarOrientation.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.chart.bar.T83DBarChart", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T83DBarChartAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T83DBarChartAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getXAxisLabel()
    {
        return getDefinitionDatum(Datum.X_AXIS_LABEL);
    }

    public void setXAxisLabel(String label)
    {
        setDefinitionDatum(Datum.X_AXIS_LABEL, label);
    }

    public String getYAxisLabel()
    {
        return getDefinitionDatum(Datum.Y_AXIS_LABEL);
    }

    public void setYAxisLabel(String label)
    {
        setDefinitionDatum(Datum.Y_AXIS_LABEL, label);
    }

    public BarOrientation getBarOrientation()
    {
        return BarOrientation.valueOf(getDefinitionDatum(Datum.BAR_ORIENTATION));
    }

    public void setBarOrientation(BarOrientation barOrientation)
    {
        setDefinitionDatum(Datum.BAR_ORIENTATION, barOrientation.toString());
    }

    public void setRangeIntegerValuesOnly(Boolean integersOnly)
    {
        setDefinitionDatum(Datum.RANGE_INTEGER_VALUES_ONLY, integersOnly);
    }

    public boolean isRangeIntegerValuesOnly()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.RANGE_INTEGER_VALUES_ONLY));
    }

    public void setMinimumRangeSize(Integer rangeMinimum)
    {
        setDefinitionDatum(Datum.RANGE_MIN_SIZE, rangeMinimum);
    }

    public Integer getMinimumRangeSize()
    {
        return getDefinitionDatum(Datum.RANGE_MIN_SIZE);
    }

    public void setRangeLowerBound(Integer rangeLowerBound)
    {
        setDefinitionDatum(Datum.RANGE_LOWER_BOUND, rangeLowerBound);
    }

    public Integer getRangeLowerBound()
    {
        return getDefinitionDatum(Datum.RANGE_LOWER_BOUND);
    }

    public void setRangeUpperBound(Integer rangeUpperBound)
    {
        setDefinitionDatum(Datum.RANGE_UPPER_BOUND, rangeUpperBound);
    }

    public Integer getRangeUpperBound()
    {
        return getDefinitionDatum(Datum.RANGE_UPPER_BOUND);
    }
}
