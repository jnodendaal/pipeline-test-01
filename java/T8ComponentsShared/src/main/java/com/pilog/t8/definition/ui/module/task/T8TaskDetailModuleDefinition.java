package com.pilog.t8.definition.ui.module.task;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8TaskDetailModuleDefinition extends T8ModuleDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_MODULE_TASK_DETAIL";
    public static final String DISPLAY_NAME = "Task Detail";
    public static final String DESCRIPTION = "A container to which other UI components can be added.  The task detail module acts the same as the module with additional functionality for it to display as a task detail.";
    // -------- Definition Meta-Data -------- //
    public enum Datum {OPEN_MODE};

    public enum OpenMode
    {
        ADDITIONAL_PANEL_ON_TASK,
        POPUP
    }

    public static final String P_FLOW_TASK_INSTANCE_IDENTIFIER = "$P_FLOW_TASK_INSTANCE_IDENTIFIER";
    public static final String P_FLOW_INSTANCE_IDENTIFIER = "$P_FLOW_INSTANCE_IDENTIFIER";

    public T8TaskDetailModuleDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPEN_MODE.toString(), "Open Mode", "The mode that will be used to determine how this module will be opened and displayed.", OpenMode.POPUP.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPEN_MODE.toString().equals(datumIdentifier)) return createStringOptions(OpenMode.values());
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }



    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DataParameterDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataParameterDefinition> inputParameterDefinitions = super.getInputParameterDefinitions();
        if(inputParameterDefinitions == null) inputParameterDefinitions = new ArrayList<T8DataParameterDefinition>();

        inputParameterDefinitions.add(new T8DataParameterDefinition(P_FLOW_TASK_INSTANCE_IDENTIFIER, "Flow Task Instance Identifier", "The instance identifier of the task", T8DataType.IDENTIFIER_STRING));
        inputParameterDefinitions.add(new T8DataParameterDefinition(P_FLOW_INSTANCE_IDENTIFIER, "Flow Instance Identifier", "The instance identifier of the flow", T8DataType.IDENTIFIER_STRING));

        return inputParameterDefinitions;
    }

    public OpenMode getOpenMode()
    {
        String openMode = (String)getDefinitionDatum(Datum.OPEN_MODE.toString());

        return openMode == null ? OpenMode.POPUP : OpenMode.valueOf(openMode);
    }

    public void setOpenMode(OpenMode mode)
    {
        setDefinitionDatum(Datum.OPEN_MODE.toString(), mode.toString());
    }
}
