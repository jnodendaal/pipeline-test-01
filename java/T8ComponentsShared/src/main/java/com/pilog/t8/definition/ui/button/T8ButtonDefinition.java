package com.pilog.t8.definition.ui.button;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.menu.T8MenuDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ButtonDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_BUTTON";
    public static final String DISPLAY_NAME = "Button";
    public static final String DESCRIPTION = "A component that allows the user to click on it in order to activate or trigger a specific action.";
    public static final String IDENTIFIER_PREFIX = "C_BUTTON_";
    public enum Datum
    {
        TEXT,
        ICON_IDENTIFIER,
        BACKGROUND_PAINTER_IDENTIFIER,
        BACKGROUND_FOCUSED_PAINTER_IDENTIFIER,
        BACKGROUND_SELECTED_PAINTER_IDENTIFIER,
        MENU_ITEM
    };
    // -------- Definition Meta-Data -------- //

    private T8IconDefinition iconDefinition;
    private T8PainterDefinition backgroundPainterDefinition;
    private T8PainterDefinition backgroundFocusedPainterDefinition;
    private T8PainterDefinition backgroundSelectedPainterDefinition;

    public T8ButtonDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.TEXT.toString(), "Text", "The text displayed on the button."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ICON_IDENTIFIER.toString(), "Icon", "The icon to render next to this button's text."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.MENU_ITEM.toString(), "Menu Item", "Optional Menu Item. If this item is set it will show up as a popup menu when the button is clicked."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), "Button Background Painter", "The painter to use for painting the background of the button in its default state."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString(), "Button Focused Background Painter", "The painter to use for painting the background of the button in its focused state (keyboard or mouse focus)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), "Button Selected Background Painter", "The painter to use for painting the background of the button in its selected state (when pressed)."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "No Icon");
        else if (Datum.MENU_ITEM.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8MenuDefinition.TYPE_IDENTIFIER));
        else if (Datum.BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext,datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8ServerContext serverContext;
        String definitionId;

        // Pre-load the icon definition if one is specified.
        serverContext = context.getServerContext();
        definitionId = getIconIdentifier();
        if (definitionId != null)
        {
            iconDefinition = (T8IconDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getBackgroundPainterIdentifier();
        if (definitionId != null)
        {
            backgroundPainterDefinition = (T8PainterDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getBackgroundFocusedPainterIdentifier();
        if (definitionId != null)
        {
            backgroundFocusedPainterDefinition = (T8PainterDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }

        definitionId = getBackgroundSelectedPainterIdentifier();
        if (definitionId != null)
        {
            backgroundSelectedPainterDefinition = (T8PainterDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), definitionId, inputParameters);
        }
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if ((getEventHandlerScriptDefinitions() == null || (getEventHandlerScriptDefinitions() != null && getEventHandlerScriptDefinitions().isEmpty())) && getMenuDefinition() == null)
        {
            validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getIdentifier(), "N/A", "No Event Handlers for the button pressed event registered.", T8DefinitionValidationError.ErrorType.WARNING));
        }

        return validationErrors;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.button.T8Button", new Class<?>[]{T8ButtonDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8ButtonAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8ButtonAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        ArrayList<T8ComponentDefinition> childComponentDefinitions;

        childComponentDefinitions = new ArrayList<>();
        if(getMenuDefinition() != null) childComponentDefinitions.add(getMenuDefinition());

        return childComponentDefinitions;
    }

    public T8IconDefinition getIconDefinition()
    {
        return iconDefinition;
    }

    public T8PainterDefinition getBackgroundPainterDefinition()
    {
        return backgroundPainterDefinition;
    }

    public T8PainterDefinition getBackgroundFocusedPainterDefinition()
    {
        return backgroundFocusedPainterDefinition;
    }

    public T8PainterDefinition getBackgroundSelectedPainterDefinition()
    {
        return backgroundSelectedPainterDefinition;
    }

    public String getButtonText()
    {
        return (String)getDefinitionDatum(Datum.TEXT.toString());
    }

    public void setButtonText(String buttonText)
    {
        setDefinitionDatum(Datum.TEXT.toString(), buttonText);
    }

    public String getIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ICON_IDENTIFIER.toString());
    }

    public void setIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ICON_IDENTIFIER.toString(), identifier);
    }

    public void setMenuDefinition(T8MenuDefinition menuDefinition)
    {
        setDefinitionDatum(Datum.MENU_ITEM.toString(), menuDefinition);
    }

    public T8MenuDefinition getMenuDefinition()
    {
        return (T8MenuDefinition)getDefinitionDatum(Datum.MENU_ITEM.toString());
    }

    public String getBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BACKGROUND_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getBackgroundFocusedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundFocusedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BACKGROUND_FOCUSED_PAINTER_IDENTIFIER.toString(), identifier);
    }

    public String getBackgroundSelectedPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString());
    }

    public void setBackgroundSelectedPainterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.BACKGROUND_SELECTED_PAINTER_IDENTIFIER.toString(), identifier);
    }
}
