package com.pilog.t8.definition.ui.passwordfield;

import com.pilog.t8.datatype.T8DtPassword;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8TextField and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8PasswordFieldAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_PASSWORD_CHANGED = "$CE_PASSWORD_CHANGED";
    public static final String EVENT_ENTER_KEY_PRESSED = "$CE_ENTER_KEY_PRESSED";
    public static final String OPERATION_SET_PASSWORD = "$CO_SET_PASSWORD";
    public static final String OPERATION_GET_PASSWORD = "$CO_GET_PASSWORD";
    public static final String PARAMETER_PASSWORD = "$P_PASSWORD";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_PASSWORD_CHANGED);
        newEventDefinition.setMetaDisplayName("Password Changed");
        newEventDefinition.setMetaDescription("This event occurs every time the text entered in the password field is changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PASSWORD, "Password", "The String value currently entered in the password field.", T8DtPassword.IDENTIFIER));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_ENTER_KEY_PRESSED);
        newEventDefinition.setMetaDisplayName("Enter Key Pressed");
        newEventDefinition.setMetaDescription("This event occurs every time the Enter key is pressed while the password field has input focus.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PASSWORD);
        newOperationDefinition.setMetaDisplayName("Set Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed in the password field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PASSWORD, "Password", "A character array representing the password to set on the password field.", T8DtPassword.IDENTIFIER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_PASSWORD);
        newOperationDefinition.setMetaDisplayName("Get Text");
        newOperationDefinition.setMetaDescription("This operations returns the text displayed in the password field.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_PASSWORD, "Password", "A character array representing the password entered in the password field.", T8DtPassword.IDENTIFIER));
        operations.add(newOperationDefinition);

        return operations;
    }
}
