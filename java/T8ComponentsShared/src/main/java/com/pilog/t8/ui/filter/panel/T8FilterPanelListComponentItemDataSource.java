package com.pilog.t8.ui.filter.panel;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public interface T8FilterPanelListComponentItemDataSource
{
    void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception;
    void initialize(T8Context context, Map<String, Object> inputParameters);
    T8FilterPanelListComponentItem next() throws Exception;
    boolean hasNext() throws Exception;
}
