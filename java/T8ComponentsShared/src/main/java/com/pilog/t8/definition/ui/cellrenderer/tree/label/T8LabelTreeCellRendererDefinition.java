package com.pilog.t8.definition.ui.cellrenderer.tree.label;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.cellrenderer.T8LabelCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8TreeCellRendererDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8LabelTreeCellRendererDefinition extends T8LabelCellRendererDefinition implements T8TreeCellRendererDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_TREE_CELL_RENDERER_LABEL";
    public static final String DISPLAY_NAME = "Label Tree Cell Renderer";
    public static final String DESCRIPTION = "A cell editor that displays the content of a tree cell as a simple text string.";
    public enum Datum {DISPLAY_VALUE_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8LabelTreeCellRendererDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DISPLAY_VALUE_EXPRESSION.toString(), "Display Value Expression", "An optional expression that is evaluated using the renderer data in order to determine the value to display."));
        return datumTypes;
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.cellrenderer.tree.label.T8LabelTreeCellRenderer", new Class<?>[]{this.getClass(), T8ComponentController.class}, this, controller);
    }

    public String getDisplayValueExpression()
    {
        return getDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION);
    }

    public void setDisplayValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DISPLAY_VALUE_EXPRESSION, expression);
    }
}
