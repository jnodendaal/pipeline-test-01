package com.pilog.t8.definition.ui.labelmatrix;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8Label and responds to specific requests regarding these
 * definitions.  It is extremely important that only new definitions are added
 * to this class and none of the old definitions removed or altered, since this
 * will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8LabelMatrixAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_CLEAR = "$CO_CLEAR";
    public static final String OPERATION_SET_TEXT = "$CO_SET_TEXT";
    public static final String OPERATION_GET_TEXT = "$CO_GET_TEXT";
    public static final String OPERATION_ADD_LABEL_ROW = "$CO_ADD_LABEL_ROW";
    public static final String OPERATION_ADD_LABEL_COLUMN = "$CO_ADD_LABEL_COLUMN";
    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_TEXT_LIST = "$CP_TEXT_LIST";
    public static final String PARAMETER_ROW_INDEX = "$CP_ROW_INDEX";
    public static final String PARAMETER_COLUMN_INDEX = "$CP_COLUMN_INDEX";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;

        events = new ArrayList<T8ComponentEventDefinition>(T8ComponentAPIHandler.getEventDefinitions());
        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR);
        newOperationDefinition.setMetaDisplayName("Clear");
        newOperationDefinition.setMetaDescription("This operations clears all labels from the matrix.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Label Text");
        newOperationDefinition.setMetaDescription("This operations sets the text displayed on the label at a specific coordinate in the matrix.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value to display on the label.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ROW_INDEX, "Row Index", "The vertical index of the label to set the text on.", T8DataType.INTEGER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COLUMN_INDEX, "Column Index", "The horizontal index of the label to set the text on.", T8DataType.INTEGER));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_TEXT);
        newOperationDefinition.setMetaDisplayName("Get Label Text");
        newOperationDefinition.setMetaDescription("This operations returns the text displayed on the label at a specific coordinate in the matrix.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ROW_INDEX, "Row Index", "The vertical index of the label to set the text on.", T8DataType.INTEGER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_COLUMN_INDEX, "Column Index", "The horizontal index of the label to set the text on.", T8DataType.INTEGER));
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text String", "The String value displayed on the label.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_LABEL_ROW);
        newOperationDefinition.setMetaDisplayName("Add Label Row");
        newOperationDefinition.setMetaDescription("This operations adds a row of labels to the matrix.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT_LIST, "Text List", "The list of text/Strings to add to the matrix.", new T8DtList(T8DataType.STRING)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_ADD_LABEL_COLUMN);
        newOperationDefinition.setMetaDisplayName("Add Label Column");
        newOperationDefinition.setMetaDescription("This operations adds a column of labels to the matrix.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT_LIST, "Text List", "The list of text/Strings to add to the matrix.", new T8DtList(T8DataType.STRING)));
        operations.add(newOperationDefinition);

        return operations;
    }
}
