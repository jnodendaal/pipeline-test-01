package com.pilog.t8.definition.ui.functionality.view;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityViewPaneDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_FUNCTIONALITY_VIEW";
    public static final String IDENTIFIER_PREFIX = "C_FUNC_VIEW_";
    public static final String DISPLAY_NAME = "Functionality View";
    public static final String DESCRIPTION = "A container in which system functionalities are executed.";
    public enum Datum {NAVIGATION_BAR_BACKGROUND_PAINTER_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    private T8PainterDefinition navBarBackgroundPainterDefinition;

    public T8FunctionalityViewPaneDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.NAVIGATION_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString(), "Navigation Bar Background Painter", "The painter to use for painting the backgorund of the navigation bar."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.NAVIGATION_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8PainterDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String backgroundPainterId;

        backgroundPainterId = getNavigationBarBackgroundPainterIdentifier();
        if (backgroundPainterId != null)
        {
            navBarBackgroundPainterDefinition = (T8PainterDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), backgroundPainterId, inputParameters);
        }
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.functionality.view.T8FunctionalityViewPane").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return null;
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public String getNavigationBarBackgroundPainterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.NAVIGATION_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString());
    }

    public void setNavigationBarBackgroundPainterIdentifier(String title)
    {
        setDefinitionDatum(Datum.NAVIGATION_BAR_BACKGROUND_PAINTER_IDENTIFIER.toString(), title);
    }

    public T8PainterDefinition getNavigationBarBackgroundPainterDefinition()
    {
        return navBarBackgroundPainterDefinition;
    }
}
