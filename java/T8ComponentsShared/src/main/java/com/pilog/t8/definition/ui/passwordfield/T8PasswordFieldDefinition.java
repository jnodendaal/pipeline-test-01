package com.pilog.t8.definition.ui.passwordfield;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PasswordFieldDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_PASSWORD_FIELD";
    public static final String DISPLAY_NAME = "Password Field";
    public static final String DESCRIPTION = "A text field that allows for the entry of a password.  The entered phrase is masked by marker characters.";
    public static final String VERSION = "0";
    public static final String IDENTIFIER_PREFIX = "C_PASSWORD_FIELD_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8PasswordFieldDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.passwordfield.T8PasswordField", new Class<?>[]{T8PasswordFieldDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8PasswordFieldAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8PasswordFieldAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }
}
