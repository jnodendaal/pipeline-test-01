package com.pilog.t8.definition.ui.table.object;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ClientContextScriptDefinition;
import com.pilog.t8.definition.script.T8DefaultScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TableDataObjectProviderScriptDefinition extends T8DefaultScriptDefinition implements T8ClientContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_TABLE_DATA_OBJECT_PROVIDER";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_TABLE_DATA_OBJECT_PROVIDER";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Object Validation Script";
    public static final String DESCRIPTION = "A script that is executed using a Data Object as input in order to verify the structure of the object.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_DATA_OBJECT_IDENTIFIER = "$P_DATA_OBJECT_IDENTIFIER";
    public static final String PARAMETER_DATA_ENTITY = "$P_DATA_ENTITY";
    public static final String PARAMETER_DATA_OBJECT = "$P_DATA_OBJECT";

    public T8TableDataObjectProviderScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8ClientContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultClientContextScript").getConstructor(T8Context.class, T8ClientContextScriptDefinition.class);
        return (T8ClientContextScript)constructor.newInstance(context, this);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IDENTIFIER, "Data Object Identifier", "The identifier of the requested Data Object to create.", T8DataType.DEFINITION_IDENTIFIER));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The Data Entity currently selected in the table.", T8DataType.DATA_ENTITY));
        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT, "Date Object", "The newly created Data Object of the requested type or null if the type is not available.", T8DataType.CUSTOM_OBJECT));
        return definitions;
    }
}
