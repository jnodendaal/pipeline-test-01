package com.pilog.t8.definition.ui.datasearch;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentAPIHandler;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchDefinition.T8SearchType;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataSearch and responds to specific requests regarding
 * these definitions.  It is extremely important that only new definitions are
 * added to this class and none of the old definitions removed or altered, since
 * this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataSearchAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SEARCH = "$CE_SEARCH";
    public static final String OPERATION_CLEAR_QUICK_SEARCH_TEXT = "$CO_CLEAR_QUICK_SEARCH_TEXT";
    public static final String OPERATION_SET_QUICK_SEARCH_TEXT = "$CO_SET_QUICK_SEARCH_TEXT";
    public static final String OPERATION_GET_QUICK_SEARCH_TEXT = "$CO_GET_QUICK_SEARCH_TEXT";
    public static final String OPERATION_SET_SEARCH_FIELDS = "$CO_SET_SEARCH_FIELDS";
    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_FIELD_IDS = "$P_FIELD_IDS";
    public static final String PARAMETER_TEXT = "$CP_TEXT";
    public static final String PARAMETER_SEARCH = "$P_SEARCH";
    public static final String PARAMETER_SEARCH_TYPE = "$P_SEARCH_TYPE";
    public static final String PARAMETER_SEARCH_TERMS = "$P_SEARCH_TERMS";

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>(T8ComponentAPIHandler.getEventDefinitions());

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SEARCH);
        newEventDefinition.setMetaDisplayName("Search");
        newEventDefinition.setMetaDescription("This event occurs when the user clicks the search button.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH_TYPE, "Search Type", "The type of search filter created.  String Values: " + T8SearchType.values(), T8DataType.STRING));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH_TERMS, "Search Terms", "A list of all the words or phrases used in the search expression.", T8DataType.LIST));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Search Data Filter", "The data filter object that will be produced by this search as output.", T8DataType.CUSTOM_OBJECT));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<>(T8ComponentAPIHandler.getOperationDefinitions());

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_CLEAR_QUICK_SEARCH_TEXT);
        newOperationDefinition.setMetaDisplayName("Clear Quick Search Text");
        newOperationDefinition.setMetaDescription("This operation clears the text value in the quick search text field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SEARCH, "Search", "Boolean flag to indicate whether or not to perform a search action after the search text has been cleared.  Deafult is false.", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_QUICK_SEARCH_TEXT);
        newOperationDefinition.setMetaDisplayName("Set Quick Search Text");
        newOperationDefinition.setMetaDescription("This operation sets the text value in the quick search text field.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text value to set in the quick search text field.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_QUICK_SEARCH_TEXT);
        newOperationDefinition.setMetaDisplayName("Get Quick Search Text");
        newOperationDefinition.setMetaDescription("This operation gets the text value in the quick search text field.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TEXT, "Text", "The text value to get in the quick search text field.", T8DataType.STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_SEARCH_FIELDS);
        newOperationDefinition.setMetaDisplayName("Set Search Fields");
        newOperationDefinition.setMetaDescription("Sets the fields on which the filter created by this search component is based.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FIELD_IDS, "Fields Ids", "The list of entity fields on which the search is based.", T8DataType.LIST));
        operations.add(newOperationDefinition);

        return operations;
    }
}
