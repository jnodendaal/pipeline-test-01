package com.pilog.t8.definition.ui.datarequirementeditor;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataRequirementEditor and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataRequirementEditorAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String EVENT_DR_CHANGES_SAVED = "$CE_DR_CHANGES_SAVED";
    public static final String EVENT_DR_DELETED = "$CE_DR_DELETED";
    public static final String OPERATION_GET_DR_ID = "$CO_GET_DR_ID";
    public static final String PARAMETER_DR_ID = "$CP_DR_ID";
    public static final String PARAMETER_DR_INSTANCE_ID = "$CP_DR_INSTANCE_ID";

    // Server operations.
    public static final String OPERATION_RETRIEVE_DR = "@OS_DATA_REQUIREMENT_EDITOR_RETRIEVE_DR";
    public static final String OPERATION_SAVE_DR = "@OS_DATA_REQUIREMENT_EDITOR_SAVE_DR";
    public static final String OPERATION_DELETE_DR = "@OS_DATA_REQUIREMENT_EDITOR_DELETE_DR";
    public static final String OS_PARAMETER_ODT_ID = "$P_ODT_ID";
    public static final String OS_PARAMETER_DR_ENTITY_ID = "$P_DR_ENTITY_ID";
    public static final String OS_PARAMETER_DR_INSTANCE_ENTITY_ID = "$P_DR_INSTANCE_ENTITY_ID";
    public static final String OS_PARAMETER_DR_DETAILS_ENTITY_ID = "$P_DR_DETAILS_ENTITY_ID";
    public static final String OS_PARAMETER_CONCEPT_TERMINOLOGY_ENTITY_ID = "$P_CONCEPT_TERMINOLOGY_ID";
    public static final String OS_PARAMETER_DR_DOCUMENT = "$P_DR_DOCUMENT";
    public static final String OS_PARAMETER_DR_ID = "$P_DR_ID";
    public static final String OS_PARAMETER_DR_INSTANCE_ID = "$P_DR_INSTANCE_ID";
    public static final String OS_PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String OS_PARAMETER_CONCEPT_TERMINOLOGY_LIST = "$P_CONCEPT_TERMINOLOGY_LIST";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();

            // Data Requirement Operations.
            definition = new T8JavaServerOperationDefinition(OPERATION_RETRIEVE_DR);
            definition.setMetaDescription("Retrieve Data Requirement");
            definition.setClassName("com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditorOperations$RetrieveDataRequirementOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_SAVE_DR);
            definition.setMetaDescription("Save Data Requirement");
            definition.setClassName("com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditorOperations$SaveDataRequirementOperation");
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_DELETE_DR);
            definition.setMetaDescription("Delete Data Requirement");
            definition.setClassName("com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditorOperations$DeleteDataRequirementOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_ID, "Data Requirement ID", "The unique identifier of the Data Requirement to delete.", T8DataType.GUID));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DR_CHANGES_SAVED);
        newEventDefinition.setMetaDisplayName("Data Requirement Changes Saved");
        newEventDefinition.setMetaDescription("This event occurs when the user successfully saves changes to a data requirement.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_ID, "Data Requirement ID", "The unique identifier of the Data Requirement to which changes were saved.", T8DataType.GUID));
        events.add(newEventDefinition);

        newEventDefinition = new T8ComponentEventDefinition(EVENT_DR_DELETED);
        newEventDefinition.setMetaDisplayName("Data Requirement Deleted");
        newEventDefinition.setMetaDescription("This event occurs when the user successfully deletes a data requirement.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_ID, "Data Requirement ID", "The unique identifier of the Data Requirement that was deleted.", T8DataType.GUID));
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_DR_ID);
        newOperationDefinition.setMetaDisplayName("Get Data Requirement ID");
        newOperationDefinition.setMetaDescription("This operation returns the ID of the Data Requirement currently loaded in the editor.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_ID, "Data Requirement ID", "The unique identifier of the data record currently loaded in the record editor.", T8DataType.GUID));
        operations.add(newOperationDefinition);

        return operations;
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataValueDefinition> inputParameters;

        inputParameters = new ArrayList<T8DataValueDefinition>();

        return inputParameters;
    }
}
