package com.pilog.t8.definition.ui.datalookupdialog;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.definition.ui.dialog.T8DialogAPIHandler;
import java.util.ArrayList;

/**
 * This static class simply creates static lists of all the definitions that are
 * applicable to the T8DataLookupDialog and responds to specific requests
 * regarding these definitions.  It is extremely important that only new
 * definitions are added to this class and none of the old definitions removed
 * or altered, since this will affect backwards compatibility.
 *
 * @author Bouwer du Preez
 */
public class T8DataLookupDialogAPIHandler extends T8DialogAPIHandler
{
    // These should NOT be changed, only added to.
    public static final String EVENT_SELECTION_CHANGED = "$CE_SELECTION_CHANGED";

    public static final String OPERATION_GET_SELECTED_DATA_ENTITY = "$CO_GET_SELECTED_DATA_ENTITY";
    public static final String OPERATION_GET_SELECTED_DATA_ENTITIES = "$CO_GET_SELECTED_DATA_ENTITIES";
    public static final String OPERATION_SET_PREFILTER = "$CO_SET_PREFILTER";

    public static final String PARAMETER_DATA_FILTER = "$CP_DATA_FILTER";
    public static final String PARAMETER_DATA_ENTITY = "$CP_DATA_ENTITY";
    public static final String PARAMETER_DATA_ENTITY_LIST = "$CP_DATA_ENTITY_LIST";
    public static final String PARAMETER_DATA_FILTER_IDENTIFIER = "$CP_DATA_FILTER_IDENTIFIER";
    public static final String PARAMETER_REFRESH_DATA = "$CP_REFRESH_DATA";

    public static ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<T8ComponentEventDefinition>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_SELECTION_CHANGED);
        newEventDefinition.setMetaDisplayName("Selection Changed");
        newEventDefinition.setMetaDescription("This event occurs when the selected entity has changed.");
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The selected data entity (or the first if more than one is selected).", T8DataType.DATA_ENTITY));
        newEventDefinition.addParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entity List", "A list of all selected data entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        events.add(newEventDefinition);

        // Add the events inherited from the T8Dialog.
        events.addAll(T8DialogAPIHandler.getEventDefinitions());

        return events;
    }

    public static ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;

        operations = new ArrayList<T8ComponentOperationDefinition>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITY);
        newOperationDefinition.setMetaDisplayName("Get Selected Entity");
        newOperationDefinition.setMetaDescription("This operations returns the data entity currently selected and displayed by die component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY, "Data Entity", "The selected data entity.", T8DataType.DATA_ENTITY));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_GET_SELECTED_DATA_ENTITIES);
        newOperationDefinition.setMetaDisplayName("Get Selected Entities");
        newOperationDefinition.setMetaDescription("This operations returns the data entities currently selected and displayed by die component.");
        newOperationDefinition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_LIST, "Data Entities", "The selected data entities.", new T8DtList(T8DataType.DATA_ENTITY)));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_PREFILTER);
        newOperationDefinition.setMetaDisplayName("Set Prefilter");
        newOperationDefinition.setMetaDescription("This operation sets the pre-filter by which data available in the lookup dialog will be filtered.  This method removes all previously added prefilters.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter object to set as the new prefilter for the data lookup dialog.", T8DataType.CUSTOM_OBJECT));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER_IDENTIFIER, "Data Filter Identifier", "The identifier of the filter on the table that will be replaced by the supplied filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH_DATA, "Refresh Data", "A boolean value indicating whether the table should refresh its data after the pre-filter has been set (default=true).", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        // Add the operations inherited from the T8Dialog.
        operations.addAll(T8DialogAPIHandler.getOperationDefinitions());

        return operations;
    }
}
