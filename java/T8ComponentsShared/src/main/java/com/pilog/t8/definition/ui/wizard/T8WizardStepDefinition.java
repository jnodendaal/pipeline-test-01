package com.pilog.t8.definition.ui.wizard;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public class T8WizardStepDefinition extends T8Definition
{
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_WIZARD_STEP";
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_WIZARD_STEP_COMPONENT";
    public static final String DISPLAY_NAME = "Step";
    public static final String DESCRIPTION = "A Wizard Step representing one in a sequence of actions that have to be taken in order to complete the wizard goal.";
    public static final String IDENTIFIER_PREFIX = "STEP_";
    public enum Datum
    {
        STEP_NAME,
        STEP_DESCRIPTION,
        STEP_ICON_IDENTIFIER,
        BACK_ACTION,
        FINISH_ACTION,
        CANCEL_ACTION,
        INITIALIZATION_SCRIPT,
        FINALIZATION_SCRIPT,
        VALIDATION_SCRIPT,
        STEP_COMPONENT,
        STEPS
    };

    private T8IconDefinition stepIconDefinition;

    public T8WizardStepDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.STEP_NAME.toString(), "Name", "The name for this step that will be displayed on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.STEP_DESCRIPTION.toString(), "Description", "The description for this step that will be displayed on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.STEP_ICON_IDENTIFIER.toString(), "Step Icon", "The Icon that will represent this step on the UI."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.BACK_ACTION.toString(), "Back Action", "If ticked, adds the Back action to this step.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.FINISH_ACTION.toString(), "Finish Action", "If ticked, adds the Finish action to this step.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CANCEL_ACTION.toString(), "Cancel Action", "If ticked, adds the Cancel action to this step.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.INITIALIZATION_SCRIPT.toString(), "Initialization Script", "The script that will be executed when this step is opened in the wizard."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FINALIZATION_SCRIPT.toString(), "Finalization Script", "The script that will be executed when the user completes this step in the wizard (by navigating to a different step)."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.VALIDATION_SCRIPT.toString(), "Validation Script", "This script will be run to validate if the user is allowed to move on to the next step."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.STEP_COMPONENT.toString(), "Component", "The component that will be displayed for this step."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.STEPS.toString(), "Steps", "The sub-steps that make up this step."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.STEP_ICON_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IconDefinition.GROUP_IDENTIFIER), true, "Default");
        else if (Datum.INITIALIZATION_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8WizardStepInitializationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.FINALIZATION_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8WizardStepFinalizationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.VALIDATION_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8WizardStepFinalizationScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.STEP_COMPONENT.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComponentDefinition.GROUP_IDENTIFIER));
        else if (Datum.STEPS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8WizardStepDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        String iconId;

        // Pre-load the icon definition if one is specified.
        iconId = getStepIconIdentifier();
        if (iconId != null)
        {
            stepIconDefinition = (T8IconDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, getRootProjectId(), iconId, inputParameters);
        }
    }

    public T8WizardStep createWizardStep(T8ComponentController controller)
    {
        return new T8WizardStep(this, controller);
    }

    public List<T8WizardStepDefinition> getDescendantStepDefinitions()
    {
        List<T8WizardStepDefinition> definitions;

        definitions = new ArrayList<T8WizardStepDefinition>();
        for (T8WizardStepDefinition stepDefinition : getStepDefinitions())
        {
            definitions.add(stepDefinition);
            definitions.addAll(stepDefinition.getDescendantStepDefinitions());
        }

        return definitions;
    }

    public List<T8ComponentDefinition> getChildComponentDefinitions()
    {
        List<T8WizardStepDefinition> stepDefinitions;
        List<T8ComponentDefinition> componentDefinitions;
        T8ComponentDefinition stepComponentDefinition;

        componentDefinitions = new ArrayList<T8ComponentDefinition>();
        stepComponentDefinition = getStepComponentDefinition();
        if (stepComponentDefinition != null) componentDefinitions.add(stepComponentDefinition);

        stepDefinitions = getStepDefinitions();
        for (T8WizardStepDefinition stepDefinition : stepDefinitions)
        {
            componentDefinitions.addAll(stepDefinition.getChildComponentDefinitions());
        }

        return componentDefinitions;
    }

    public String getStepName()
    {
        return (String)getDefinitionDatum(Datum.STEP_NAME.toString());
    }

    public void setStepName(String categoryName)
    {
        setDefinitionDatum(Datum.STEP_NAME.toString(), categoryName);
    }

    public String getStepDescription()
    {
        return (String)getDefinitionDatum(Datum.STEP_DESCRIPTION.toString());
    }

    public void setStepDescription(String categoryName)
    {
        setDefinitionDatum(Datum.STEP_DESCRIPTION.toString(), categoryName);
    }

    public T8IconDefinition getStepIconDefinition()
    {
        return stepIconDefinition;
    }

    public ImageIcon getStepIcon()
    {
        return stepIconDefinition != null ? stepIconDefinition.getImage().getImageIcon() : null;
    }

    public String getStepIconIdentifier()
    {
        return (String)getDefinitionDatum(Datum.STEP_ICON_IDENTIFIER.toString());
    }

    public void setStepIconIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.STEP_ICON_IDENTIFIER.toString(), identifier);
    }

    public boolean isBackAction()
    {
        Boolean canSkip = (Boolean)getDefinitionDatum(Datum.BACK_ACTION.toString());
        return canSkip == null ? false : canSkip;
    }

    public void setBackAction(boolean backAction)
    {
        setDefinitionDatum(Datum.BACK_ACTION.toString(), backAction);
    }

    public boolean isCancelAction()
    {
        Boolean canSkip = (Boolean)getDefinitionDatum(Datum.CANCEL_ACTION.toString());
        return canSkip == null ? false : canSkip;
    }

    public void setCancelAction(boolean cancelAction)
    {
        setDefinitionDatum(Datum.CANCEL_ACTION.toString(), cancelAction);
    }

    public boolean isFinishAction()
    {
        Boolean canSkip = (Boolean)getDefinitionDatum(Datum.FINISH_ACTION.toString());
        return canSkip == null ? false : canSkip;
    }

    public void setFinishAction(boolean finishAction)
    {
        setDefinitionDatum(Datum.FINISH_ACTION.toString(), finishAction);
    }

    public String getInitializationScriptIdentifier()
    {
        T8WizardStepInitializationScriptDefinition definition;

        definition = getInitializationScriptDefinition();
        return definition != null ? definition.getIdentifier() : null;
    }

    public String getFinalizationScriptIdentifier()
    {
        T8WizardStepFinalizationScriptDefinition definition;

        definition = getFinalizationScriptDefinition();
        return definition != null ? definition.getIdentifier() : null;
    }

    public T8WizardStepInitializationScriptDefinition getInitializationScriptDefinition()
    {
        return (T8WizardStepInitializationScriptDefinition)getDefinitionDatum(Datum.INITIALIZATION_SCRIPT.toString());
    }

    public void setInitializationScriptDefinition(T8WizardStepInitializationScriptDefinition initializationScriptDefinition)
    {
        setDefinitionDatum(Datum.INITIALIZATION_SCRIPT.toString(), initializationScriptDefinition);
    }

    public T8WizardStepFinalizationScriptDefinition getFinalizationScriptDefinition()
    {
        return (T8WizardStepFinalizationScriptDefinition)getDefinitionDatum(Datum.FINALIZATION_SCRIPT.toString());
    }

    public void setFinalizationScriptDefinition(T8WizardStepFinalizationScriptDefinition validationScriptDefinition)
    {
        setDefinitionDatum(Datum.FINALIZATION_SCRIPT.toString(), validationScriptDefinition);
    }

    public T8ComponentDefinition getStepComponentDefinition()
    {
        return (T8ComponentDefinition)getDefinitionDatum(Datum.STEP_COMPONENT.toString());
    }

    public void setStepComponentDefinition(T8ComponentDefinition componentDefinition)
    {
        setDefinitionDatum(Datum.STEP_COMPONENT.toString(), componentDefinition);
    }

    public List<T8WizardStepDefinition> getStepDefinitions()
    {
        return (List<T8WizardStepDefinition>)getDefinitionDatum(Datum.STEPS.toString());
    }

    public void setStepDefinitions(List<T8WizardStepDefinition> steps)
    {
        setDefinitionDatum(Datum.STEPS.toString(), steps);
    }
}
