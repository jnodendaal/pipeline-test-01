/**
 * Created on 08 Nov 2016, 10:43:11 AM
 */
package com.pilog.t8.definition.ui.datepicker;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8DatePickerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATE_PICKER";
    public static final String DISPLAY_NAME = "Date Picker";
    public static final String DESCRIPTION = "A component that allows the user to selected a specific date from a calendar display.";
    public static final String IDENTIFIER_PREFIX = "C_DATE_";
    public enum Datum { EDITABLE,
                        MANUAL_ENTRY_ENABLED};
    // -------- Definition Meta-Data -------- //

    public T8DatePickerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EDITABLE.toString(), "Editable", "If this field should be editable or not. If false, the popup selection is also disabled.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MANUAL_ENTRY_ENABLED.toString(), "Manual Entry Enabled", "If false, only the popup can be used to select a value.", true));

        return datumTypes;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.ui.datepicker.T8DatePicker", new Class<?>[]{T8DatePickerDefinition.class, T8ComponentController.class}, this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DatePickerApiHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DatePickerApiHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public boolean isEditable()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.EDITABLE));
    }

    public void setEditable(boolean editable)
    {
        setDefinitionDatum(Datum.EDITABLE, editable);
    }

    public boolean isManualEntryEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.MANUAL_ENTRY_ENABLED));
    }

    public void setManualEntryEnabled(boolean manualEntryEnabled)
    {
        setDefinitionDatum(Datum.MANUAL_ENTRY_ENABLED, manualEntryEnabled);
    }
}