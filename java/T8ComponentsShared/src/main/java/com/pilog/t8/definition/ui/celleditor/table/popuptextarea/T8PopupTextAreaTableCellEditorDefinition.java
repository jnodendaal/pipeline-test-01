package com.pilog.t8.definition.ui.celleditor.table.popuptextarea;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.celleditor.T8TableCellEditorDefinition;
import com.pilog.t8.definition.ui.combobox.T8ComboBoxDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8PopupTextAreaTableCellEditorDefinition extends T8ComboBoxDefinition implements T8TableCellEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_CELL_EDITOR_POPUP_TEXT_AREA";
    public static final String DISPLAY_NAME = "Popup Text Area Cell Editor";
    public static final String DESCRIPTION = "A cell editor that uses a popup text area to allow the user to enter long text values.";
    public enum Datum {TARGET_ENTITY_IDENTIFIER,
                       TARGET_FIELD_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public T8PopupTextAreaTableCellEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_ENTITY_IDENTIFIER.toString(), "Target Entity", "The data entity that will be modified/acted on by this cell editor."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.TARGET_FIELD_IDENTIFIER.toString(), "Target Field", "The data entity field that will be modified/acted on by this cell editor."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.TARGET_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.TARGET_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String targetEntityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            targetEntityId = getTargetEntityIdentifier();
            if (targetEntityId != null)
            {
                T8DataEntityDefinition targetEntityDefinition;

                targetEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), targetEntityId);

                if (targetEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> targetFieldDefinitions;

                    targetFieldDefinitions = targetEntityDefinition.getFieldDefinitions();
                    for (T8DataEntityFieldDefinition targetFieldDefinition : targetFieldDefinitions)
                    {
                        optionList.add(new T8DefinitionDatumOption(targetFieldDefinition.getPublicIdentifier(), targetFieldDefinition.getPublicIdentifier()));
                    }

                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.celleditor.table.popuptextarea.T8PopupTextAreaTableCellEditor").getConstructor(this.getClass(), T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    public String getTargetEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER.toString());
    }

    public void setTargetEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public String getTargetFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.TARGET_FIELD_IDENTIFIER.toString());
    }

    public void setTargetFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.TARGET_FIELD_IDENTIFIER.toString(), identifier);
    }
}
