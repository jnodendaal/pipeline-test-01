package com.pilog.t8.definition.ui.celleditor;

/**
 * @author Bouwer du Preez
 */
public interface T8TreeCellEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_UI_COMPONENT_CELL_EDITOR_TREE";
    public static final String STORAGE_PATH = "/tree_cell_editors"; 
    // -------- Definition Meta-Data -------- //
}
