package com.pilog.t8.ui.datarecord;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.KeyStroke;

/**
 * @author Bouwer du Preez
 */
public interface DataRecordEditor
{
    /**
     * Returns the container context where this editor is housed.
     * @return The container of this editor.
     */
    public DataRecordEditorContainer getDataRecordEditorContainer();

    /**
     * Instructs the editor to check the underlying data record object model for
     * dependency changes that may have occurred and to update its content
     * accordingly.
     */
    public void refreshDataDependency();
    
    /**
     * Instructs the editor to check the underlying data record object and to
     * update the editor content if any new data changes are available.
     */
    public void refreshEditor();
    
    /**
     * Instructs the editor to check the current context access layer and to
     * update the editor content based on any access changes that may have
     * occurred.
     */
    public void refreshAccess();
    
    /**
     * Returns a boolean flag indicating whether or not this editor contains
     * uncommitted changes, i.e. changes that were made since the last time the
     * commitChanges() method was invoked or a new DataRecord was set on the
     * editor.
     * @return A boolean flag indicating whether or not this editor contains
     * uncommitted changes.
     */
    public boolean hasChanges();
    
    /**
     * Commits all outstanding changes to the underlying DataRecord object.  If
     * any of the outstanding changes are not valid, the editor must handle user
     * feedback internally and return a Boolean false value to indicate the all
     * changes could not be commit successfully.
     * @return A Boolean value indicating whether or not all changes were 
     * successfully committed or not.  Failure could be due to invalid changes.
     */
    public boolean commitChanges();
    
    /**
     * Returns the DataRecord object on which the editor is acting.  This method
     * returns the DataRecord in its current state without committing any 
     * outstanding changes on the editor.
     * @return The DataRecord object on which the editor is currently acting.
     */
    public DataRecord getDataRecord();
    
    /**
     * Returns the current value of the editor.
     * @return The current value of the editor.
     */
    public Value getValue();
    
    /**
     * Returns the requirement that defines the behavior of this editor.
     * @return The requirement that defines the behavior of this editor.
     */
    public Requirement getRequirement();
    
    /**
     * Returns the current list of requirement validation errors on the editor.
     * @return The list of requirement validation errors.
     */
    public List<T8DataValidationError> getValidationErrors();
    
    /**
     * Sets validation errors applicable to the current editor DataRecord.  The
     * editor is responsible for display of these validation errors in a way
     * that is applicable to its layout.
     * @param validationErrors The errors to set on the editor.
     */
    public void setValidationErrors(List<T8DataValidationError> validationErrors);
    
    // This method is a hack that simply delegates key strokes from the panel 
    // (the editor) to the actual editor component so that keystrokes are 
    // properly registered when this class is used in a table cell.
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed);
}
