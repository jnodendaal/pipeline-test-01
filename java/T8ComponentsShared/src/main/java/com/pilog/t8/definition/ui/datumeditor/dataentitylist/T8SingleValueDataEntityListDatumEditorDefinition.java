package com.pilog.t8.definition.ui.datumeditor.dataentitylist;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.script.T8DatumEditorConfigurationScriptDefinition;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListDefinition;
import com.pilog.t8.definition.ui.datumeditor.T8DefinitionDatumEditorDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8SingleValueDataEntityListDatumEditorDefinition extends T8DataEntityListDefinition implements T8DefinitionDatumEditorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATUM_EDITOR_DATA_ENTITY_LIST_SINGLE_VALUE";
    public static final String DISPLAY_NAME = "Single Value Data Entity List Datum Editor";
    public static final String DESCRIPTION = "A datum editor that allows the user to search for and select data to be added to an ordered list.";
    public enum Datum {DATUM_IDENTIFIER,
                       VALUE_FIELD_IDENTIFIER,
                       CONFIGURATION_SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8SingleValueDataEntityListDatumEditorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATUM_IDENTIFIER.toString(), "Datum", "The datum to which this editor applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.VALUE_FIELD_IDENTIFIER.toString(), "Value Field", "The field from which the datum value is retrieved."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CONFIGURATION_SCRIPT_DEFINITION.toString(), "Configuration Script", "The script that configures the datum editor during initialization and refresh operations."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.VALUE_FIELD_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String entityId;

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataEntityFieldDefinition> fieldDefinitions;

                    fieldDefinitions = dataEntityDefinition.getFieldDefinitions();
                    return createPublicIdentifierOptionsFromDefinitions((List)fieldDefinitions);
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.CONFIGURATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DatumEditorConfigurationScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.CONFIGURATION_SCRIPT_DEFINITION.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.dataentitylist.T8DataEntityList").getConstructor(T8DataEntityListDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public T8DefinitionDatumEditor getNewDatumEditorInstance(T8DefinitionContext definitionContext, T8Definition targetDefinition, T8DefinitionDatumType datumType) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.datumeditor.dataentitylist.T8SingleValueDataEntityListDatumEditor").getConstructor(T8DefinitionContext.class, T8SingleValueDataEntityListDatumEditorDefinition.class, T8Definition.class, T8DefinitionDatumType.class);
        return (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, targetDefinition, datumType);
    }

    @Override
    public String getDatumIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATUM_IDENTIFIER.toString());
    }

    public void setDatumIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATUM_IDENTIFIER.toString(), identifier);
    }

    public String getValueFieldIdentifier()
    {
        return (String)getDefinitionDatum(Datum.VALUE_FIELD_IDENTIFIER.toString());
    }

    public void setValueFieldIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.VALUE_FIELD_IDENTIFIER.toString(), identifier);
    }

    public T8DatumEditorConfigurationScriptDefinition getConfigurationScriptDefinition()
    {
        return (T8DatumEditorConfigurationScriptDefinition)getDefinitionDatum(Datum.CONFIGURATION_SCRIPT_DEFINITION.toString());
    }

    public void setConfigurationScriptDefinition(T8DatumEditorConfigurationScriptDefinition definition)
    {
        setDefinitionDatum(Datum.CONFIGURATION_SCRIPT_DEFINITION.toString(), definition);
    }
}
