package com.pilog.t8.definition.ui.datarecordviewer;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordViewerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_UI_COMPONENT_DATA_RECORD_VIEWER";
    public static final String IDENTIFIER_PREFIX = "C_DATA_RECORD_VIEWER_";
    public static final String DISPLAY_NAME = "Data Record Viewer";
    public static final String DESCRIPTION = "A component that displays one or more data records.";
    public enum Datum {EXPORT_ENABLED,
                       LANGUAGE_SWITCH_ENABLED,
                       DATA_RECORD_ACCESS_IDENTIFIER,
                       COLUMN_HEADER_TYPE,
                       COLUMN_HEADER_CODE_TYPE_IDENTIFIER,
                       COLUMN_HEADER_PROPERTY_IDENTIFIER,
                       COMPARISON_TYPE_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public enum ColumnHeaderType {RECORD_ID, PROPERTY_VALUE_STRING, CODE, ALTERED_DATE};

    public T8DataRecordViewerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EXPORT_ENABLED.toString(), "Export Enabled", "A boolean setting that enables (if true) the option to export the document displayed by the editor.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LANGUAGE_SWITCH_ENABLED.toString(), "Language Switch Enabled", "A boolean setting that enables (if true) the option to switch the dictionary language used to display the document in the editor.", false));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString(), "Data Record Access", "The access rights to data displayed/edited in the editor."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.COLUMN_HEADER_TYPE.toString(), "Column Header Type", "The type of header to use for columns representing a record in the data viewer.", ColumnHeaderType.RECORD_ID.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.COLUMN_HEADER_CODE_TYPE_IDENTIFIER.toString(), "Column Header Code Type ID", "The identifier of a code type in the root records to use as the column header.", null));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.COLUMN_HEADER_PROPERTY_IDENTIFIER.toString(), "Column Header Property ID", "The identifier of a property in the root records to use as the column header.", null));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COMPARISON_TYPE_DEFINITIONS.toString(), "Comparison Types", "The types of comparisons that will be used when records are displayed in the viewer."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataRecordAccessDefinition.GROUP_IDENTIFIER), true, "No Restriction");
        else if (Datum.COLUMN_HEADER_TYPE.toString().equals(datumIdentifier)) return createStringOptions(ColumnHeaderType.values());
        else if (Datum.COMPARISON_TYPE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ComparisonTypeDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8Component getNewComponentInstance(T8ComponentController controller) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.ui.datarecordviewer.T8DataRecordViewer").getConstructor(T8DataRecordViewerDefinition.class, T8ComponentController.class);
        return (T8Component)constructor.newInstance(this, controller);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return T8DataRecordViewerAPIHandler.getInputParameterDefinitions();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8DataRecordViewerAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8DataRecordViewerAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return null;
    }

    public boolean isExportEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.EXPORT_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setExportEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.EXPORT_ENABLED.toString(), enabled);
    }

    public boolean isLanguageSwitchEnabled()
    {
        Boolean enabled;

        enabled = (Boolean)getDefinitionDatum(Datum.LANGUAGE_SWITCH_ENABLED.toString());
        return ((enabled != null) && enabled);
    }

    public void setLanguageSwitchEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.LANGUAGE_SWITCH_ENABLED.toString(), enabled);
    }

    public String getDataRecordAccessIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString());
    }

    public void setDataRecordAccessIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_ACCESS_IDENTIFIER.toString(), identifier);
    }

    public String getColumnHeaderPropertyIdentifier()
    {
        return (String)getDefinitionDatum(Datum.COLUMN_HEADER_PROPERTY_IDENTIFIER.toString());
    }

    public void setColumnHeaderPropertyIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.COLUMN_HEADER_PROPERTY_IDENTIFIER.toString(), identifier);
    }

    public String getColumnHeaderCodeTypeIdentifier()
    {
        return (String)getDefinitionDatum(Datum.COLUMN_HEADER_CODE_TYPE_IDENTIFIER.toString());
    }

    public void setColumnHeaderCodeTypeIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.COLUMN_HEADER_CODE_TYPE_IDENTIFIER.toString(), identifier);
    }

    public ColumnHeaderType getColumnHeaderType()
    {
        String value;

        value = (String)getDefinitionDatum(Datum.COLUMN_HEADER_TYPE.toString());
        return value != null ? ColumnHeaderType.valueOf(value) : ColumnHeaderType.RECORD_ID;
    }

    public void setColumnHeaderType(ColumnHeaderType headerType)
    {
        setDefinitionDatum(Datum.COLUMN_HEADER_TYPE.toString(), headerType.toString());
    }

    public List<T8ComparisonTypeDefinition> getComparisionTypeDefinitions()
    {
        return (List<T8ComparisonTypeDefinition>)getDefinitionDatum(Datum.COMPARISON_TYPE_DEFINITIONS.toString());
    }

    public void setComparisonTypeDefinitions(List<T8ComparisonTypeDefinition> definitions)
    {
        setDefinitionDatum(Datum.COMPARISON_TYPE_DEFINITIONS.toString(), definitions);
    }
}
