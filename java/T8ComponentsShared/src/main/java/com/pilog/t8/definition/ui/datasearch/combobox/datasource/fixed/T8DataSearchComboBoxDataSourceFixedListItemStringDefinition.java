package com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxDataSourceFixedListItemStringDefinition extends T8DataSearchComboBoxDataSourceFixedListItemDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SEARCH_COMBO_BOX_DATA_SOURCE_FIXED_LIST_NODE_ITEM";
    public static final String DESCRIPTION = "A Item node containing the information for a string item";

    public enum Datum
    {
        ITEM_VALUE
    };
// -------- Definition Meta-Data -------- //
    public T8DataSearchComboBoxDataSourceFixedListItemStringDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.ITEM_VALUE.toString(), "Item Value", "The String value for this item that will be used in the filter"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public String getItemValue()
    {
        return getDefinitionDatum(Datum.ITEM_VALUE);
    }

    public void setItemValue(String identifier)
    {
        setDefinitionDatum(Datum.ITEM_VALUE, identifier);
    }
}
