package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentsSharedVersion
{
    public final static String VERSION = "261";
}
