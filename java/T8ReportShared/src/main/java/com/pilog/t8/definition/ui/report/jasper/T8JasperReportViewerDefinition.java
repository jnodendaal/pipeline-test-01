package com.pilog.t8.definition.ui.report.jasper;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.report.jasper.T8MainJasperReportDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportViewerDefinition extends T8ComponentDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_JASPER_VIEWER";
    public static final String DISPLAY_NAME = "Jasper Report Viewer";
    public static final String DESCRIPTION = "Jasper Report Viewer";
    public static final String IDENTIFIER_PREFIX = "C_REPORT_VIEW_JASPER_";
    public static final String VERSION = "0";
    public enum Datum {
        REPORT_DEFINITION_IDENTIFIER,
        LOAD_ON_START,
        SAVE_ENABLED,
        PRINT_ENABLED
    };
    // -------- Definition Meta-Data -------- //

    public T8JasperReportViewerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REPORT_DEFINITION_IDENTIFIER.toString(), "Report Identifier", "The report identifier that will be used to determine the report that should be displayed."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOAD_ON_START.toString(), "Load on Start", "Sets whether the report will be loaded when the module is started.",true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SAVE_ENABLED.toString(), "Save Enabled", "Sets whether the save button is enabled for the client.",true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.PRINT_ENABLED.toString(), "Print Enabled", "Sets whether the print button is enabled for the client.",true));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REPORT_DEFINITION_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8MainJasperReportDefinition.TYPE_IDENTIFIER));
        } else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <C extends T8Component> C getNewComponentInstance(T8ComponentController controller)
    {
        return T8Reflections.getInstance("com.pilog.t8.ui.report.jasper.T8JasperReportViewer", new Class<?>[]{T8ComponentController.class, T8JasperReportViewerDefinition.class}, controller, this);
    }

    @Override
    public ArrayList<T8DataValueDefinition> getComponentInputParameterDefinitions()
    {
        return T8JasperReportViewerAPIHandler.getInputParameterDefinitions();
    }

    @Override
    public ArrayList<T8ComponentEventDefinition> getComponentEventDefinitions()
    {
        return T8JasperReportViewerAPIHandler.getEventDefinitions();
    }

    @Override
    public ArrayList<T8ComponentOperationDefinition> getComponentOperationDefinitions()
    {
        return T8JasperReportViewerAPIHandler.getOperationDefinitions();
    }

    @Override
    public ArrayList<T8ComponentDefinition> getChildComponentDefinitions()
    {
        return new ArrayList<>(0);
    }

    public String getReportDefinitionIdentifier()
    {
        return getDefinitionDatum(Datum.REPORT_DEFINITION_IDENTIFIER);
    }

    public void setReportDefinitionIdentifier(String reportDefinitionIdentifier)
    {
        setDefinitionDatum(Datum.REPORT_DEFINITION_IDENTIFIER, reportDefinitionIdentifier);
    }

    public boolean isLoadOnStart()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.LOAD_ON_START));
    }

    public void setLoadOnStart(Boolean loadOnStart)
    {
        setDefinitionDatum(Datum.LOAD_ON_START, loadOnStart);
    }

    public Boolean isSaveEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.SAVE_ENABLED));
    }

    public void setSaveEnabled(Boolean saveEnabled)
    {
        setDefinitionDatum(Datum.SAVE_ENABLED, saveEnabled);
    }

    public Boolean isPrintEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.PRINT_ENABLED));
    }

    public void setPrintEnabled(Boolean printEnabled)
    {
        setDefinitionDatum(Datum.PRINT_ENABLED, printEnabled);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("T8JasperReportViewerDefinition: [");
        stringBuilder.append("Identifier: ").append(getIdentifier());
        stringBuilder.append(", Report Definition Identifier: ").append(getReportDefinitionIdentifier());
        stringBuilder.append("]");
        stringBuilder.append(super.toString());
        return stringBuilder.toString();
    }

}
