/**
 * Created on Mar 13, 2017, 10:51:22 PM
 */
package com.pilog.t8.definition.communication.message.template.email.attachment;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import static com.pilog.t8.definition.communication.message.template.email.attachment.T8JasperReportAttachmentDefinition.PARAMETER_JASPER_REPORT_PARAMETERS;
import static com.pilog.t8.definition.communication.message.template.email.attachment.T8JasperReportAttachmentDefinition.PARAMETER_TEMPLATE_PARAMETERS;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportAttachmentParameterScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_JSPR_ATT_PARAMETERS";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_JSPR_ATT_PARAMETERS";
    public static final String STORAGE_PATH = null;
    public static final String IDENTIFIER_PREFIX = "SCRIPT_JSPR_ATT_";
    public static final String DISPLAY_NAME = "Jasper Attachment Report Script";
    public static final String DESCRIPTION = "A script that can be used to generate additional parameters for jasper reports to use. Basic detail from the current message is passed as input.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8JasperReportAttachmentParameterScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> inputParameterDefinitions;

        // Add the default parameters
        inputParameterDefinitions = new ArrayList<>();
        inputParameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_TEMPLATE_PARAMETERS, "Template Parameters", "The template parameters from the current message.", T8DataType.MAP));

        // Return the complete list of valid parameter definitions
        return inputParameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> outputParameterDefinitions;

        // Add the default parameters
        outputParameterDefinitions = new ArrayList<>();
        outputParameterDefinitions.add(new T8DataParameterDefinition(PARAMETER_JASPER_REPORT_PARAMETERS, "Jasper Report Parameters", "A Map containing the parameter values for the report in the format <ReportParameterName,ParameterValue>.", T8DataType.MAP));

        // Return the complete list of valid parameter definitions
        return outputParameterDefinitions;
    }

}