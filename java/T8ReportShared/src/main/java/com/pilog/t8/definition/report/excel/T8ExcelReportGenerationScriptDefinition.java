package com.pilog.t8.definition.report.excel;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelReportGenerationScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REPORT_EXCEL_GENERATION_SCRIPT";
    public static final String GROUP_IDENTIFIER = "@DG_REPORT_EXCEL_GENERATION_SCRIPT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Excel Report Generation Script";
    public static final String DESCRIPTION = "A script that uses the supplied input parameters to generate an Excel report.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_NAME = "$P_NAME";
    public static final String PARAMETER_DESCRIPTION = "$P_DESCRIPTION";

    public T8ExcelReportGenerationScriptDefinition(String id)
    {
        super(id);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8ReportDefinition reportDefinition;

        reportDefinition = (T8ReportDefinition)getParentDefinition();
        if (reportDefinition != null)
        {
            return reportDefinition.getReportParameterDefinitions();
        }
        else return new ArrayList<>();
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        // Add the definitions of the output parameters expected from this script.
        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_NAME, "Report Name", "The name that will be assigned to the report generated from the specified set of input parameters.", T8DataType.STRING));
        definitions.add(new T8DataParameterDefinition(PARAMETER_DESCRIPTION, "Report Description", "The description that will be assigned to the report generated from the specified set of input parameters.", T8DataType.STRING));

        // Return the final list of output parameter definitions.
        return definitions;
    }
}