package com.pilog.t8.definition.report.jasper;

import static com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerAPIHandler.*;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportServerAPIHandler implements T8DefinitionResource
{
    // Server operations.
    public static final String OPERATION_GENERATE_REPORT = "@OS_REPORTS_JASPER_GENERATE_REPORT";

    public static final String PARAMETER_REPORT_DEFINITION_IDENTIFIER = "$P_REPORT_DEFINITION_IDENTIFER";
    public static final String PARAMETER_PROJECT_ID = "$P_PROJECT_ID";
    public static final String PARAMETER_REPORT_ALTERNATE_FILTER = "$P_REPORT_ALTERNATE_FILTER";
    public static final String PARAMETER_REPORT_ASYNC_GENERATION = "$P_REPORT_ASYNC_GENERATION";
    public static final String PARAMETER_REPORT = "$P_REPORT";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            // Jasper Report Operations.
            definition = new T8JavaServerOperationDefinition(OPERATION_GENERATE_REPORT);
            definition.setMetaDisplayName("Generate Report");
            definition.setMetaDescription("Generate's a new report based on the report identifier.");
            definition.setClassName("com.pilog.t8.report.jasper.T8JasperReportServer$GenerateReport");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT_DEFINITION_IDENTIFIER, "Report Definition Identifier", "The definition identifier used to identify the report to generate.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROJECT_ID, "Project Id", "The id of the project to which the report to generated belongs.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT_PARAMETERS, "Report Parameters", "A HashMap containing the parameter values for the report in the format <ReportParameterName,ParameterValue>.", T8DataType.MAP));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT_ALTERNATE_FILTER, "Alternate Report Filter", "A Alternitive filter that will be used to filter the report data, this filter will override the filter attached to the definition.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT_ASYNC_GENERATION, "Async Generation", "If this option is set to true then the report will be generated asyncronously and the client will receive a notification when it has completed.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT_TITLE, "Report Title", "Overrides the title for the report specified in the report definition.", T8DataType.STRING, true));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT_DESCRIPTION, "Report Description", "Overrides the description for the report specified in the report definition.", T8DataType.STRING, true));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPORT, "Report", "The generated report object.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        return new ArrayList<>();
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        return new ArrayList<>();
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        return new ArrayList<>();
    }

    public static final T8ComponentEventDefinition getEventDefinition(String eventKey)
    {
        for (T8ComponentEventDefinition eventDefinition : getEventDefinitions())
        {
            if (eventDefinition.getIdentifier().equals(eventKey)) return eventDefinition;
        }
        throw new IllegalArgumentException("Event " + eventKey + " not found.");
    }
}
