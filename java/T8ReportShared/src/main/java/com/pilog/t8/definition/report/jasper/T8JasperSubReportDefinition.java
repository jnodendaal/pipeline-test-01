package com.pilog.t8.definition.report.jasper;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;

/**
 * @author Gavin Boshoff
 */
public class T8JasperSubReportDefinition extends T8JasperReportDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_JASPER_SUB_REPORTS";
    public static final String TYPE_IDENTIFIER = "@DT_JASPER_SUB_REPORT";
    public static final String DISPLAY_NAME = "Jasper Sub Report";
    public static final String DESCRIPTION = "Definition to set up a jasper sub report";
    public static final String IDENTIFIER_PREFIX = "REPORT_JASPER_SUB_";
    public enum Datum
    {
        REPORT_PARAMETER_NAME
    };
    // -------- Definition Meta-Data -------- //

    public T8JasperSubReportDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();

        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REPORT_PARAMETER_NAME.toString(), "Report Parameter Name", "The parameter name that will be used when sending the sub report to the main report."));

        return datumTypes;
    }

    @Override
    public T8ReportGenerator getReportGenerator(T8Context context)
    {
        throw new UnsupportedOperationException("Generator cannt be constructed from sub-report definition.");
    }

    public String getReportParameterName()
    {
        return getDefinitionDatum(Datum.REPORT_PARAMETER_NAME);
    }

    public void setReportParameterName(String dataFilterDefinition)
    {
        setDefinitionDatum(Datum.REPORT_PARAMETER_NAME, dataFilterDefinition);
    }
}
