package com.pilog.t8.definition.report.jasper;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8MainJasperReportDefinition extends T8JasperReportDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_JASPER";
    public static final String DISPLAY_NAME = "Jasper Report";
    public static final String DESCRIPTION = "Definition to set up a jasper report";
    public static final String IDENTIFIER_PREFIX = "REPORT_JASPER_";
    public enum Datum
    {
        SWAPFILE_ENABLED,
        REPORT_DATA_FILTER,
        REPORT_IMAGES,
        SUB_REPORTS
    };
    // -------- Definition Meta-Data -------- //

    public T8MainJasperReportDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();

        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.REPORT_DATA_FILTER.toString(), "Data Filter", "The data filter that will be used when retrieving data for the report."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SWAPFILE_ENABLED.toString(), "Swapfile Enabled", "If true, the report will be generated using a swapfile as opposed to in-memory.  For large reprots, this can save on memory usage."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.REPORT_IMAGES.toString(), "Report Images", "The images that will be used in the report."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SUB_REPORTS.toString(), "Sub Reports", "The Sub reports that will be used be used by the jasper report."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.REPORT_DATA_FILTER.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.REPORT_IMAGES.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8JasperImageDefinition.GROUP_IDENTIFIER));
        else if (Datum.SUB_REPORTS.toString().equals(datumId)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8JasperSubReportDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8ReportGenerator getReportGenerator(T8Context context)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.report.jasper.T8JasperReportGenerator", new Class<?>[]{T8Context.class, T8MainJasperReportDefinition.class}, context, this);
    }

    public boolean isSwapfileEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.SWAPFILE_ENABLED);
        return value != null && value;
    }

    public void setSwapfileEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.SWAPFILE_ENABLED, enabled);
    }

    public T8DataFilterDefinition getReportDataFilter()
    {
        return getDefinitionDatum(Datum.REPORT_DATA_FILTER);
    }

    public void setReportDataFilter(T8DataFilterDefinition dataFilterDefinition)
    {
        setDefinitionDatum(Datum.REPORT_DATA_FILTER, dataFilterDefinition);
    }

    public void setReportImages(List<T8JasperImageDefinition> reportImages)
    {
        setDefinitionDatum(Datum.REPORT_IMAGES, reportImages);
    }

    public List<T8JasperImageDefinition>  getReportImages()
    {
        return getDefinitionDatum(Datum.REPORT_IMAGES);
    }

    public void setSubReports(List<T8JasperSubReportDefinition> subReports)
    {
        setDefinitionDatum(Datum.SUB_REPORTS, subReports);
    }

    public List<T8JasperSubReportDefinition>  getSubReports()
    {
        return getDefinitionDatum(Datum.SUB_REPORTS);
    }
}
