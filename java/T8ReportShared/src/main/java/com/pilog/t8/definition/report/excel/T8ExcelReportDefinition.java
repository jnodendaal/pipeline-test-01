package com.pilog.t8.definition.report.excel;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelReportDefinition extends T8ReportDefinition
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ExcelReportDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REPORT_EXCEL";
    public static final String DISPLAY_NAME = "Excel Report";
    public static final String DESCRIPTION = "Definition to set up a Excel report.";
    public static final String IDENTIFIER_PREFIX = "REPORT_EXCEL_";
    public enum Datum
    {
        DATA_ENTITY_ID
    };
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_COLUMN_MAPPING = "$P_COLUMN_MAPPING";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";

    public T8ExcelReportDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, configurationParameters, configurationParameters);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();

        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_ID.toString(), "Data Entity Id", "The identifier of the data entity that will be used to retrieve information for the report."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8ReportGenerator getReportGenerator(T8Context context)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.report.excel.T8ExcelReportGenerator", new Class<?>[]{T8Context.class, T8ExcelReportDefinition.class}, context, this);
    }

    public String getDataEntityId()
    {
        return getDefinitionDatum(Datum.DATA_ENTITY_ID);
    }

    public void setDataEntityId(String id)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_ID, id);
    }
}
