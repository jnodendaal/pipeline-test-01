/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.report.jasper;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.gfx.image.defined.T8DefinedImageDefinition;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8JasperImageDefinition extends T8DefinedImageDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_JASPER_REPORT_IMAGES";
    public static final String TYPE_IDENTIFIER = "@DT_JASPER_REPORT_IMAGE";
    public static final String DISPLAY_NAME = "Image";
    public static final String DESCRIPTION = "Report Image";
    public static final String IDENTIFIER_PREFIX = "IMAGE_";

    public enum Datum
    {
        REPORT_PARAMETER_NAME
    };
// -------- Definition Meta-Data -------- //
    public T8JasperImageDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes(); //To change body of generated methods, choose Tools | Templates.
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REPORT_PARAMETER_NAME.toString(), "Report Parameter Name", "The paremeter that will be used in the report to retrieve this image."));
        return datumTypes;
    }

    public String getReportParameterName()
    {
        return (String) getDefinitionDatum(Datum.REPORT_PARAMETER_NAME.toString());
    }

    public void setReportParameterName(String dataFilterDefinition)
    {
        setDefinitionDatum(Datum.REPORT_PARAMETER_NAME.toString(), dataFilterDefinition);
    }
}
