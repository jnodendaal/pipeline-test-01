package com.pilog.t8.definition.ui.report.jasper;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.T8DataValueDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.T8ComponentOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportViewerAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String EVENT_REPORT_LOADED = "$CE_REPORT_LOADED";

    // Operations.
    public static final String OPERATION_REFRESH_REPORT = "$OC_REFRESH_REPORT";
    public static final String OPERATION_SET_REPORT_FILTER = "$OC_SET_REPORT_FILTER";
    public static final String OPERATION_RELOAD_REPORT_PARAMETERS = "$OC_RELOAD_REPORT_PARAMETERS";
    public static final String OPERATION_SET_REPORT_IDENTIFIER = "$OC_SET_REPORT_IDENTIFIER";
    public static final String OPERATION_SET_REPORT_TITLE = "$CO_SET_REPORT_TITLE";
    public static final String OPERATION_SET_REPORT_DESCRIPTION = "$CO_SET_REPORT_DESCRIPTION";

    public static final String PARAMETER_REPORT_FILTER = "$P_REPORT_FILTER";
    public static final String PARAMETER_FILTER_IDENTIFIER = "$P_FILTER_IDENTIFIER";
    public static final String PARAMETER_REPORT_PARAMETERS = "$P_REPORT_PARAMETERS";
    public static final String PARAMETER_REFRESH = "$P_REFRESH";
    public static final String PARAMETER_REPORT_IDENTIFIER = "$P_REPORT_IDENTIFIER";
    public static final String PARAMETER_REPORT_TITLE = "$P_REPORT_TITLE";
    public static final String PARAMETER_REPORT_DESCRIPTION = "$P_REPORT_DESCRIPTION";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        return null;
    }

    public static final ArrayList<T8ComponentEventDefinition> getEventDefinitions()
    {
        ArrayList<T8ComponentEventDefinition> events;
        T8ComponentEventDefinition newEventDefinition;

        events = new ArrayList<>();

        newEventDefinition = new T8ComponentEventDefinition(EVENT_REPORT_LOADED);
        newEventDefinition.setMetaDisplayName("Report Loaded");
        newEventDefinition.setMetaDescription("This event occurs when the report has finished rendering on the client.");
        events.add(newEventDefinition);

        return events;
    }

    public static final ArrayList<T8ComponentOperationDefinition> getOperationDefinitions()
    {
        ArrayList<T8ComponentOperationDefinition> operations;
        T8ComponentOperationDefinition newOperationDefinition;
        operations = new ArrayList<>();

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_REFRESH_REPORT);
        newOperationDefinition.setMetaDisplayName("Refresh Report");
        newOperationDefinition.setMetaDescription("Refreshes the report. Acts the same as if the user pressed the refresh button.");
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_REPORT_FILTER);
        newOperationDefinition.setMetaDisplayName("Set the report data filter");
        newOperationDefinition.setMetaDescription("This operation will reset the filter on the report datasource and reload the report.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_FILTER, "Filter", "The new data filter to be used by the report.", T8DataType.DATA_FILTER));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILTER_IDENTIFIER, "Filter Identifier", "The identifier to uniquely identify this filter.", T8DataType.STRING));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH, "Refresh?", "Should the report refresh after the new values was added", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_RELOAD_REPORT_PARAMETERS);
        newOperationDefinition.setMetaDisplayName("Set the report parameters");
        newOperationDefinition.setMetaDescription("This operation will reset the report generation parameters and optionally refreshed the report with the new parameters.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_PARAMETERS, "Parameter HashMap", "HashMap conatianing the parameter data in the form <ReportParameterName, ParameterValue>.", T8DataType.MAP));
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REFRESH, "Refresh?", "Should the report refresh after the new values was added", T8DataType.BOOLEAN));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_REPORT_TITLE);
        newOperationDefinition.setMetaDisplayName("Set Report Title");
        newOperationDefinition.setMetaDescription("Sets the report title to the given title value.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_TITLE, "Title", "The title to be set for the report.", T8DataType.DISPLAY_STRING));
        operations.add(newOperationDefinition);

        newOperationDefinition = new T8ComponentOperationDefinition(OPERATION_SET_REPORT_DESCRIPTION);
        newOperationDefinition.setMetaDisplayName("Set Report Description");
        newOperationDefinition.setMetaDescription("Sets the report description to the given description value.");
        newOperationDefinition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_REPORT_DESCRIPTION, "Description", "The description to be set for the report.", T8DataType.DISPLAY_STRING));
        operations.add(newOperationDefinition);

        return operations;
    }

    public static final ArrayList<T8DataValueDefinition> getInputParameterDefinitions()
    {
        ArrayList<T8DataValueDefinition> inputParameters;
        T8DataValueDefinition newInputParameter;
        inputParameters = new ArrayList<>();

        newInputParameter = new T8DataParameterDefinition(OPERATION_SET_REPORT_IDENTIFIER);
        newInputParameter.setMetaDisplayName("Jasper Report Identifier");
        newInputParameter.setMetaDescription("The report identifier used to determine the jasper report that should be loaded.");
        inputParameters.add(newInputParameter);

        return inputParameters;
    }

    public static final T8ComponentEventDefinition getEventDefinition(String eventKey)
    {
        for (T8ComponentEventDefinition eventDefinition : getEventDefinitions())
        {
            if (eventDefinition.getIdentifier().equals(eventKey)) return eventDefinition;
        }

        throw new IllegalArgumentException("Event " + eventKey + " not found.");
    }
}
