/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.message.template.email.attachment;

import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8JasperReportAttachmentDefinition extends T8EmailMessageTemplateAttachementDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_EMAIL_MESSAGE_TEMPLATE_ATTACHMENT_JASPER";
    public static final String DISPLAY_NAME = "Jasper Report";
    public static final String DESCRIPTION = "A Jasper Generated Report";
    public static final String IDENTIFIER_PREFIX = "EMAIL_ATTACH_JASPER_";

    public enum Datum { REPORT_IDENTIFIER,
                        REPORT_PARAMETER_SCRIPT,
                        REPORT_DATA_FILTER
                      };
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_TEMPLATE_PARAMETERS = "$P_TEMPLATE_PARAMETERS";
    public static final String PARAMETER_JASPER_REPORT_PARAMETERS = "$P_JASPER_REPORT_PARAMETERS";

    public T8JasperReportAttachmentDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REPORT_IDENTIFIER.toString(), "Report Identifier", "The Report to generate before attaching to communication."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.REPORT_PARAMETER_SCRIPT.toString(), "Report Parameter Script", "Script to be used to generate additional parameters for Jasper Report."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.REPORT_DATA_FILTER.toString(), "Report Data Filter", "The data filter to send to the report."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REPORT_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ReportDefinition.GROUP_IDENTIFIER));
        else if (Datum.REPORT_DATA_FILTER.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        else if (Datum.REPORT_PARAMETER_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8JasperReportAttachmentParameterScriptDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8CommunicationMessageAttachment getInstance()
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.template.attachment.T8JasperReportEmailAttachment", new Class<?>[]{T8JasperReportAttachmentDefinition.class}, this);
    }

    public void setReportIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.REPORT_IDENTIFIER, identifier);
    }

    public String getReportIdentifier()
    {
        return getDefinitionDatum(Datum.REPORT_IDENTIFIER);
    }

    public void setReportDataFilter(T8DataFilterDefinition reportDataFilter)
    {
        setDefinitionDatum(Datum.REPORT_DATA_FILTER, reportDataFilter);
    }

    public T8DataFilterDefinition getReportDataFilter()
    {
        return getDefinitionDatum(Datum.REPORT_DATA_FILTER);
    }

    public T8JasperReportAttachmentParameterScriptDefinition getReportParameterScriptDefinition()
    {
        return getDefinitionDatum(Datum.REPORT_PARAMETER_SCRIPT);
    }

    public void setReportParameterScriptDefinition(T8JasperReportAttachmentParameterScriptDefinition scriptDefinition)
    {
        setDefinitionDatum(Datum.REPORT_PARAMETER_SCRIPT, scriptDefinition);
    }
}
