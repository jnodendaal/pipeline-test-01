package com.pilog.t8.definition.report.jasper;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public abstract class T8JasperReportDefinition extends T8ReportDefinition
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperReportDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_JASPER";
    public static final String DISPLAY_NAME = "Jasper Report.";
    public static final String DESCRIPTION = "Definition to set up a jasper report";
    public static final String IDENTIFIER_PREFIX = "REPORT_JASPER_";
    public enum Datum
    {
        REPORT,
        REPORT_DATA_ENTITY_IDENTIFER
    };
    // -------- Definition Meta-Data -------- //

    public T8JasperReportDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, configurationParameters, configurationParameters);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes = super.getDatumTypes();

        datumTypes.add(new T8DefinitionDatumType(T8DataType.BYTE_ARRAY, Datum.REPORT.toString(), "Report", "The jasper report to use."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REPORT_DATA_ENTITY_IDENTIFER.toString(), "Data Entity Identifier", "The identifier of the data entity that will be used to retrieve information for the report."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.REPORT.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REPORT_DATA_ENTITY_IDENTIFER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.REPORT.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.reports.jasper.datumeditor.T8JasperReportDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        }
        else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.test.harness.T8JasperReportTestHarness", new Class<?>[]{});
    }

    public String getReportDataEntityIdentifier()
    {
        return getDefinitionDatum(Datum.REPORT_DATA_ENTITY_IDENTIFER);
    }

    public void setReportDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.REPORT_DATA_ENTITY_IDENTIFER, identifier);
    }

    public byte[] getReport()
    {
        return getDefinitionDatum(Datum.REPORT);
    }

    public void setReport(Serializable jasperReport) throws IOException
    {
        try
        {
            setDefinitionDatum(Datum.REPORT.toString(), SerializationUtilities.serializeObject(jasperReport));
        }
        catch (IOException ex)
        {
            LOGGER.log("Failed to serialize jasper report.", ex);
            throw ex;
        }
    }
}
