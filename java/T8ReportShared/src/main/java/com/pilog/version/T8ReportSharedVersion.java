package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8ReportSharedVersion
{
    public final static String VERSION = "38";
}
