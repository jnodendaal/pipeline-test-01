/*
 * Created on Jul 1, 2015, 8:12:21 AM
 */
package com.pilog.t8.utilities.date;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Gavin Boshoff
 */
public class DatesTest
{
//<editor-fold defaultstate="collapsed" desc="Test Init Methods">
    public DatesTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }
//</editor-fold>

    /**
     * Test of toZeroTime method, of class Dates.
     */
    @Test
    public void testToZeroTime()
    {
        System.out.println("toZeroTime");
        String expResult;
        String dateTime;
        String result;

        dateTime = "2015-06-24 09:42:25.777";
        expResult = "2015-06-24 00:00:00";
        result = Dates.toZeroTime(dateTime);
        assertEquals("Unexpected Date Format.", expResult, result);

        dateTime = "2015-06-24";
        expResult = "2015-06-24 00:00:00";
        result = Dates.toZeroTime(dateTime);
        assertEquals("Unexpected Date Format.", expResult, result);
    }

    /**
     * Test of stripTime method, of class Dates.
     */
    @Test
    public void testStripTime()
    {
        System.out.println("stripTime");
        String expResult;
        String dateTime;
        String result;

        dateTime = "2015-06-24 09:42:25.777";
        expResult = "2015-06-24";
        result = Dates.stripTime(dateTime);
        assertEquals("Unexpected Date Format.", expResult, result);

        dateTime = "2015-06-24";
        expResult = "2015-06-24";
        result = Dates.stripTime(dateTime);
        assertEquals("Unexpected Date Format.", expResult, result);
    }

    /**
     * Test of nextDay method, of class Dates.
     */
    @Test
    public void testNextDay()
    {
        System.out.println("nextDay");
        String expResult;
        String dateTime;
        String result;

        dateTime = "2015-06-24 09:42:25.777";
        expResult = "2015-06-25";
        result = Dates.nextDay(dateTime, "yyyy-MM-dd");
        assertEquals("Unexpected Date Format.", expResult, result);

        dateTime = "2015-06-24";
        expResult = "2015-06-25";
        result = Dates.nextDay(dateTime, "yyyy-MM-dd");
        assertEquals("Unexpected Date Format.", expResult, result);

        dateTime = "2015-06-30";
        expResult = "2015-07-01";
        result = Dates.nextDay(dateTime, "yyyy-MM-dd");
        assertEquals("Unexpected Date Format.", expResult, result);
    }
}
