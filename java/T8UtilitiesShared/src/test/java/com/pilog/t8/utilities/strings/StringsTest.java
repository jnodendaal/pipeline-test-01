package com.pilog.t8.utilities.strings;

import static org.junit.Assert.*;

import javax.swing.text.Segment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Gavin Boshoff
 */
public class StringsTest
{
    public StringsTest()
    {
        //Test Class constructor. Used if there is anything relevant to the
        //current test class instance.
    }

    @BeforeClass
    public static void setUpClass()
    {
        // Static setup/configuration which may be required before the test
        // class is even instantiated
    }

    @AfterClass
    public static void tearDownClass()
    {
        // Static cleanup/finishing tasks which may be required after the test
        // class is no longer in use
    }

    @Before
    public void setUp()
    {
        // Setup required for the current tests to be performed
    }

    @After
    public void tearDown()
    {
        // Finalization required for the current tests which were performed
    }

    /**
     * Test of isDouble method, of class Strings.
     */
    @Test
    public void testIsDouble()
    {
        System.out.println("isDouble");

        assertTrue(Strings.isDouble("0"));
        assertTrue(Strings.isDouble("1234.23456"));
        assertTrue(Strings.isDouble("1234.0"));
        assertTrue(Strings.isDouble("0.000000001"));
        assertTrue(Strings.isDouble("-12"));
        assertTrue(Strings.isDouble("-1.0000"));
        assertTrue(Strings.isDouble("-0.0000"));
        assertTrue(Strings.isDouble("-0.000000001"));
        assertFalse(Strings.isDouble("xxx"));
        assertFalse(Strings.isDouble(null));
        assertFalse(Strings.isDouble("0.0.1"));
        assertFalse(Strings.isDouble(".0"));
        assertFalse(Strings.isDouble("1.0a"));
        assertFalse(Strings.isDouble("a1.0"));
        assertFalse(Strings.isDouble(""));
    }

    /**
     * Test of isInteger method, of class Strings.
     */
    @Test
    public void testIsInteger()
    {
        System.out.println("isInteger");

        assertTrue(Strings.isInteger("1"));
        assertTrue(Strings.isInteger("-1"));
        assertTrue(Strings.isInteger("99999"));
        assertTrue(Strings.isInteger("-99999"));
        assertFalse(Strings.isInteger(null));
        assertFalse(Strings.isInteger(""));
        assertFalse(Strings.isInteger("0.1"));
        assertFalse(Strings.isInteger("ax"));
        assertFalse(Strings.isInteger("1ax"));
        assertFalse(Strings.isInteger("ax1"));
        assertFalse(Strings.isInteger("1ax1"));
    }

    /**
     * Test of isPositiveInteger method, of class Strings.
     */
    @Test
    public void testIsPositiveInteger()
    {
        System.out.println("isPositiveInteger");

        assertTrue(Strings.isPositiveInteger("1"));
        assertTrue(Strings.isPositiveInteger("99999"));
        assertFalse(Strings.isPositiveInteger(null));
        assertFalse(Strings.isPositiveInteger(""));
        assertFalse(Strings.isPositiveInteger("0.1"));
        assertFalse(Strings.isPositiveInteger("ax"));
        assertFalse(Strings.isPositiveInteger("1ax"));
        assertFalse(Strings.isPositiveInteger("ax1"));
        assertFalse(Strings.isPositiveInteger("1ax1"));
        assertFalse(Strings.isPositiveInteger("-1"));
        assertFalse(Strings.isPositiveInteger("-99999"));
    }

    /**
     * Test of isNegativeInteger method, of class Strings.
     */
    @Test
    public void testIsNegativeInteger()
    {
        System.out.println("isNegativeInteger");

        assertTrue(Strings.isNegativeInteger("-1"));
        assertTrue(Strings.isNegativeInteger("-99999"));
        assertFalse(Strings.isNegativeInteger(null));
        assertFalse(Strings.isNegativeInteger(""));
        assertFalse(Strings.isNegativeInteger("0.1"));
        assertFalse(Strings.isNegativeInteger("ax"));
        assertFalse(Strings.isNegativeInteger("1ax"));
        assertFalse(Strings.isNegativeInteger("ax1"));
        assertFalse(Strings.isNegativeInteger("1ax1"));
        assertFalse(Strings.isNegativeInteger("1"));
        assertFalse(Strings.isNegativeInteger("99999"));
    }

    /**
     * Test of isNullOrEmpty method, of class Strings.
     */
    @Test
    public void testIsNullOrEmpty_String()
    {
        System.out.println("isNullOrEmpty");

        assertTrue(Strings.isNullOrEmpty(null));
        assertTrue(Strings.isNullOrEmpty(""));
        assertTrue(Strings.isNullOrEmpty("  "));
        assertTrue(Strings.isNullOrEmpty("\t\n"));
        assertTrue(Strings.isNullOrEmpty("\r"));
        assertTrue(Strings.isNullOrEmpty("\t"));
        assertTrue(Strings.isNullOrEmpty("\n"));
        assertTrue(Strings.isNullOrEmpty("\n\r"));
        assertFalse(Strings.isNullOrEmpty("a"));
        assertFalse(Strings.isNullOrEmpty("bb"));
        assertFalse(Strings.isNullOrEmpty(" bb"));
        assertFalse(Strings.isNullOrEmpty("bb "));
        assertFalse(Strings.isNullOrEmpty(" bb "));
        assertFalse(Strings.isNullOrEmpty("\tbb\t"));
        assertFalse(Strings.isNullOrEmpty("\tbb\n"));
    }

    /**
     * Test of isEmpty method, of class Strings.
     */
    @Test
    public void testIsEmpty()
    {
        System.out.println("isEmpty");

        assertTrue(Strings.isEmpty(""));
        assertTrue(Strings.isEmpty("  "));
        assertTrue(Strings.isEmpty("\t\n"));
        assertTrue(Strings.isEmpty("\r"));
        assertTrue(Strings.isEmpty("\t"));
        assertTrue(Strings.isEmpty("\n"));
        assertTrue(Strings.isEmpty("\n\r"));
        assertFalse(Strings.isEmpty("a"));
        assertFalse(Strings.isEmpty("bb"));
        assertFalse(Strings.isEmpty(" bb"));
        assertFalse(Strings.isEmpty("bb "));
        assertFalse(Strings.isEmpty(" bb "));
        assertFalse(Strings.isEmpty("\tbb\t"));
        assertFalse(Strings.isEmpty("\tbb\n"));
    }

    @Test(expected = NullPointerException.class)
    public void testIsEmpty_Null()
    {
        System.out.println("isEmpty null");

        Strings.isEmpty(null);
    }

    /**
     * Test of isNullOrEmpty method, of class Strings.
     */
    @Test
    public void testIsNullOrEmpty_CharSequence()
    {
        System.out.println("isNullOrEmpty");

        assertTrue(Strings.isNullOrEmpty(null));
        assertTrue(Strings.isNullOrEmpty(""));
        assertTrue(Strings.isNullOrEmpty(new StringBuffer()));
        assertTrue(Strings.isNullOrEmpty(new StringBuffer("")));
        assertTrue(Strings.isNullOrEmpty(new StringBuffer("").append("  ")));
        assertTrue(Strings.isNullOrEmpty(new StringBuffer("").append("\t\n")));
        assertTrue(Strings.isNullOrEmpty(new StringBuilder()));
        assertTrue(Strings.isNullOrEmpty(new StringBuilder("")));
        assertTrue(Strings.isNullOrEmpty(new StringBuilder("").append("  ")));
        assertTrue(Strings.isNullOrEmpty(new StringBuilder("").append("\t\n")));
        assertTrue(Strings.isNullOrEmpty(new Segment()));
        assertTrue(Strings.isNullOrEmpty(new Segment(new char[]{}, 0, 0)));
    }

    /**
     * Test of trimToNull method, of class Strings.
     */
    @Test
    public void testTrimToNull()
    {
        System.out.println("trimToNull");

        assertEquals(null, Strings.trimToNull(null));
        assertEquals(null, Strings.trimToNull(""));
        assertEquals(null, Strings.trimToNull("  "));
        assertEquals(null, Strings.trimToNull(" \t\n\r"));
        assertEquals(null, Strings.trimToNull(" \t"));
        assertEquals(null, Strings.trimToNull(" \n"));
        assertEquals(null, Strings.trimToNull(" \r"));
        assertEquals(null, Strings.trimToNull("\t\t"));
        assertEquals("a", Strings.trimToNull(" a "));
        assertEquals("a", Strings.trimToNull("a"));
        assertEquals("a", Strings.trimToNull("a\t"));
        assertEquals("a", Strings.trimToNull("\na"));
        assertEquals("xyz", Strings.trimToNull("xyz"));
        assertEquals("x y z", Strings.trimToNull(" x y z "));
    }
}
