/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.utilities.strings;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Gavin Boshoff
 */
public class FormattedStringTest
{
    private FormattedString formattedString;

    public FormattedStringTest()
    {
        //Test Class constructor. Used if there is anything relevant to the
        //current test class instance.
    }

    @BeforeClass
    public static void setUpClass()
    {
        // Static setup/configuration which may be required before the test
        // class is even instantiated
    }

    @AfterClass
    public static void tearDownClass()
    {
        // Static cleanup/finishing tasks which may be required after the test
        // class is no longer in use
    }

    @Before
    public void setUp()
    {
        // Setup required for the current tests to be performed
        this.formattedString = new FormattedString();
    }

    @After
    public void tearDown()
    {
        // Finalization required for the current tests which were performed
        this.formattedString = null;
    }

    /**
     * Test of setToLowerCase and isToLowerCase methods, of class FormattedString.
     */
    @Test
    public void testToLowerCase()
    {
        System.out.println("ToLowerCase");

        this.formattedString.setToLowerCase(true);
        assertTrue(this.formattedString.isToLowerCase());
        this.formattedString.setToLowerCase(false);
        assertFalse(this.formattedString.isToLowerCase());
    }

    /**
     * Test of setToUpperCase and isToUpperCase methods, of class FormattedString.
     */
    @Test
    public void testToUpperCase()
    {
        System.out.println("ToUpperCase");

        this.formattedString.setToUpperCase(true);
        assertTrue(this.formattedString.isToUpperCase());
        this.formattedString.setToUpperCase(false);
        assertFalse(this.formattedString.isToUpperCase());
    }

    /**
     * Test of setAppendNullString and isAppendNullString methods, of class
     * FormattedString.
     */
    @Test
    public void testAppendNullString()
    {
        System.out.println("AppendNullString");

        this.formattedString.setAppendNullString(true);
        assertTrue(this.formattedString.isAppendNullString());
        this.formattedString.setAppendNullString(false);
        assertFalse(this.formattedString.isAppendNullString());
    }

    /**
     * Test of setAppendTrimmed and isAppendTrimmed methods, of class FormattedString.
     */
    @Test
    public void testAppendTrimmed()
    {
        System.out.println("AppendTrimmed");

        this.formattedString.setAppendTrimmed(true);
        assertTrue(this.formattedString.isAppendTrimmed());
        this.formattedString.setAppendTrimmed(false);
        assertFalse(this.formattedString.isAppendTrimmed());
    }

    /**
     * Test of setAppendSeparator and getAppendSeparator methods, of class
     * FormattedString.
     */
    @Test
    public void testAppendSeparator()
    {
        System.out.println("AppendSeparator");

        assertEquals(this.formattedString.getAppendSeparator(), " ");
        this.formattedString.setAppendSeparator(null);
        assertNull(this.formattedString.getAppendSeparator());
        this.formattedString.setAppendSeparator(",");
        assertEquals(this.formattedString.getAppendSeparator(), ",");
        this.formattedString.setAppendSeparator("hello");
        assertEquals(this.formattedString.getAppendSeparator(), "hello");
    }

    /**
     * Test of setNullString and getNullString methods, of class FormattedString.
     */
    @Test
    public void testSetNullString()
    {
        System.out.println("NullString");
        assertEquals(this.formattedString.getNullString(), "null");
        this.formattedString.setNullString(null);
        assertNull(this.formattedString.getNullString());
        this.formattedString.setNullString("XXX");
        assertEquals(this.formattedString.getNullString(), "XXX");
        this.formattedString.setNullString("hello");
        assertEquals(this.formattedString.getNullString(), "hello");
    }

    @Test
    public void testFormattedStringInstance()
    {
        this.formattedString = new FormattedString();

        // Set initial value
        this.formattedString.append("hello");
        // Check the basic functions
        assertEquals(this.formattedString.toString(), "hello");
        assertEquals(this.formattedString.charAt(0), 'h');
        assertEquals(this.formattedString.charAt(4), 'o');
        assertEquals(this.formattedString.length(), 5);

        // Test the appendSeparator functionality
        this.formattedString.append("World");
        assertEquals(this.formattedString.toString(), "hello"+this.formattedString.getAppendSeparator()+"World");
        this.formattedString.setAppendSeparator(";");
        this.formattedString.append("World");
        assertEquals(this.formattedString.toString(), "hello World"+this.formattedString.getAppendSeparator()+"World");

        // Reset the formatted string, by setting the value
        this.formattedString.setBuffer(new StringBuffer());
        assertEquals(this.formattedString.toString(), "");

        // Test the append Null Appending functionality
        this.formattedString.setAppendNullString(false);
        this.formattedString.append(null);
        assertEquals(this.formattedString.toString(), "");
        this.formattedString.setAppendNullString(true);
        this.formattedString.append(null);
        assertEquals(this.formattedString.toString(), "null");
        this.formattedString.setNullString("help");
        this.formattedString.append(null);
        assertEquals(this.formattedString.toString(), "null"+this.formattedString.getNullString());

        // Test the append trimmed functionality (default set to true)
        this.formattedString.append(" hello ");
        assertEquals(this.formattedString.toString(), "nullhelp;hello");
        this.formattedString.setAppendTrimmed(false);
        this.formattedString.append(" xxx ");
        assertEquals(this.formattedString.toString(), "nullhelp;hello; xxx ");

        // Reset the formatted string, by setting the value
        this.formattedString.setBuffer(new StringBuffer());
        assertEquals(this.formattedString.toString(), "");

        // Test the toUpper and toLower functionalities (default of both is set to false).
        this.formattedString.append("xXx");
        assertEquals(this.formattedString.toString(), "xXx");
        this.formattedString.setToUpperCase(true);
        this.formattedString.append("xXx");
        assertEquals(this.formattedString.toString(), "xXx;XXX");
        this.formattedString.setToUpperCase(false);
        this.formattedString.setToLowerCase(true);
        this.formattedString.append("xXx");
        assertEquals(this.formattedString.toString(), "xXx;XXX;xxx");
        // toLowerCase gets presendence
        this.formattedString.setToUpperCase(true);
        this.formattedString.setToLowerCase(true);
        this.formattedString.append("xXx");
        assertEquals(this.formattedString.toString(), "xXx;XXX;xxx;xxx");
    }

    @Test(expected = NullPointerException.class)
    public void testLengthNullPointer()
    {
        getNullBufferFormattedString().length();
    }

    @Test(expected = NullPointerException.class)
    public void testSubSequenceNullPointer()
    {
        getNullBufferFormattedString().subSequence(0, 0);
    }

    @Test(expected = NullPointerException.class)
    public void testCharAtNullPointer()
    {
        getNullBufferFormattedString().charAt(0);
    }

    @Test(expected = NullPointerException.class)
    public void testToStringNullPointer()
    {
        getNullBufferFormattedString().toString();
    }

    private FormattedString getNullBufferFormattedString()
    {
        FormattedString temp;

        temp = new FormattedString();
        temp.setBuffer(null);

        assertNull(temp.getBuffer());

        return temp;
    }
}
