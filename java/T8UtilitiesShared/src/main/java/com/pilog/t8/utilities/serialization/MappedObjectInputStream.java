/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.utilities.serialization;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is a simple extension to an ObjectInputStream that can be used to de-serialize objects that have
 * been moved to different packages or that have been renamed since the time of serialization.  The class
 * takes a map of class names as input parameter and uses this map to determine which class names read from
 * the underlying input stream should be mapped to new names before initialization.
 * @author bouwe_000
 */
public class MappedObjectInputStream extends ObjectInputStream
{
    private final Map<String, String> classNameMap;

    public MappedObjectInputStream(InputStream in, Map<String, String> classNameMap) throws IOException
    {
        super(in);
        this.classNameMap = new HashMap<>(classNameMap);
    }

    @Override
    protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException
    {
        ObjectStreamClass resultClassDescriptor;
        String className;

        resultClassDescriptor = super.readClassDescriptor();
        className = resultClassDescriptor.getName();
        if (classNameMap.containsKey(className))
        {
            resultClassDescriptor = ObjectStreamClass.lookup(Class.forName(classNameMap.get(className)));
        }

        return resultClassDescriptor;
    }
}
