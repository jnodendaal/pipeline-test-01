/**
 * Created on 19 Jul 2016, 9:02:13 AM
 */
package com.pilog.t8.utilities.collections;

import java.util.Collections;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public class Lists
{
    private Lists() {}

    /**
     * This is a convenience method to create a {@code List} with a single item
     * contained within it. This list cannot be expanded. As such, when this
     * convenience method is used, it should be a matter-of-fact that the list
     * will only ever be used as is.
     *
     * @param <T> The {@code T} type of the element to be added to the list
     *
     * @param element The {@code T} element to be added to the list as the only
     *      item within the list
     *
     * @return The non-expandable {@code List} containing the single item
     */
    public static <T> List<T> createSingular(T element)
    {
        return Collections.singletonList(element);
    }
}