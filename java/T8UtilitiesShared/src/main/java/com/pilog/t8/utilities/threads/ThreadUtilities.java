package com.pilog.t8.utilities.threads;

/**
 * @author Bouwer du Preez
 */
public class ThreadUtilities
{
    /**
     * Returns the most recent method invocation from the stack trace of the
     * specified thread.  This method returns null if the thread is not active.
     * @param thread The thread for which the stack element will be returned.
     * @return The most recent method invocation from the top of the stack for
     * the specified thread.
     */
    public static StackTraceElement getTopStackTraceElement(Thread thread)
    {
        StackTraceElement[] elements;
        
        elements = thread.getStackTrace();
        return (elements.length > 2) ? elements[2] : null;
    }

    /**
     * Returns the invoker of most recent method invocation from the stack trace 
     * of the specified thread.  This method returns null if the thread is not 
     * active or if the current method is at the top of the stack.
     * @param thread The thread for which the stack element will be returned.
     * @return The invoker of the most recent method invocation from the top of 
     * the stack for the specified thread.
     */
    public static StackTraceElement getInvokerStackTraceElement(Thread thread)
    {
        StackTraceElement[] elements;
        
        elements = thread.getStackTrace();
        return (elements.length > 3) ? elements[3] : null;
    }
}
