package com.pilog.t8.utilities.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Bouwer du Preez
 */
public class JDBCUtilities
{
    public static final void close(Object... objects)
    {
        for (Object object : objects)
        {
            try
            {
                if (object instanceof Statement) ((Statement)object).close();
                else if (object instanceof ResultSet) ((ResultSet)object).close();
                else if (object instanceof Connection) ((Connection)object).close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public static final boolean tryRollback(Connection connection)
    {
        try
        {
            connection.rollback();
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
