package com.pilog.t8.utilities.strings;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Bouwer.duPreez
 */
public class StringUtilities
{
    /**
     * Returns true if the 'inputString' matches any of the 'stringsToMatch' in
     * the var-args array passed to this method.
     *
     * @param caseSensitive Specified whether or not case sensitivity must be
     * applicable when attempting to match the StringUtilities.
     * @param inputString The input String that will be used during matching.
     * @param stringsToMatch One or more StringUtilities to which the input String will
     * be matched.
     * @return true if the input String matches one of the parameters StringUtilities,
     * false otherwise.
     */
    public static boolean stringMatch(boolean caseSensitive, String inputString, String... stringsToMatch)
    {
        if (inputString == null)
        {
            for (String string : stringsToMatch) if (string == null) return true;

            return false;
        }
        else
        {
            for (String string : stringsToMatch)
            {
                if ((caseSensitive) && (inputString.equals(string))) return true;
                else if (inputString.equalsIgnoreCase(string)) return true;
            }

            return false;
        }
    }

    /**
     * This method will format the input String and apply word wrapping so that
     * the output String contains only lines of text that are of less than or
     * equal length as the specified width.  All existing new-line characters
     * are maintained and text on each line will be left justified.  The output
     * String is wrapped in HTML tags and <br> tags are used to replace all
     * new-line characters so that the output text can be used as a Swing tool
     * tip.
     * @param maxCharactersPerLine The maximum character length of each line in
     * the tool tip.
     * @param inputString The input String that will be formatted.
     * @return HTML text that can be used as text on a Swing Tool tip.
     */
    public static final String createToolTip(int maxCharactersPerLine, String inputString)
    {
        return inputString != null ? createToolTipHTML(wrapText(maxCharactersPerLine, inputString)) : null;
    }

    /**
     * Create a simple HTML formatted String from the supplied input String. The
     * type of formatting that this method applies is specifically suited to
     * creating simple multi-line tool tips from a String that includes '\n'
     * newline characters.
     *
     * @param inputString The input String to format.
     * @return A formatted output String.
     */
    public static final String createToolTipHTML(String inputString)
    {
        StringBuffer textBuffer;

        textBuffer = new StringBuffer();
        textBuffer.append("<html>");
        for (int characterIndex = 0; characterIndex < inputString.length(); characterIndex++)
        {
            char character;

            character = inputString.charAt(characterIndex);
            if (character == '\n')
            {
                textBuffer.append("<br>");
            }
            else textBuffer.append(character);
        }
        textBuffer.append("</html>");

        return textBuffer.toString();
    }

    /**
     * This method will format the input String and apply word wrapping so that
     * the output String contains only lines of text that are of less than or
     * equal length as the specified width.  All existing new-line characters
     * are maintained and text on each line will be left justified.
     *
     * @param maxTextWidth The maximum character length of each line in the
     * output text.
     * @param inputString The input String that will be formatted.
     * @return A formatted version of the input String value.
     */
    public static final String wrapText(int maxTextWidth, String inputString)
    {
        StringBuffer textBuffer;
        int lastSpaceIndex;
        int lineStartIndex;
        int characterIndex;

        lastSpaceIndex = -1;
        lineStartIndex = 0;
        characterIndex = 0;
        textBuffer = new StringBuffer(inputString);
        while (characterIndex < textBuffer.length())
        {
            // If a space is reached, store the position.
            if (textBuffer.charAt(characterIndex) == ' ')
            {
                lastSpaceIndex = characterIndex;
            }

            // If a new-line character is reached, reset the line start and last space locations.
            if (textBuffer.charAt(characterIndex) == '\n')
            {
                lastSpaceIndex = -1;
                lineStartIndex = characterIndex + 1;
            }

            // If the current line is longer than the max text width, insert a new-line character at the last space position.
            if (characterIndex > (lineStartIndex + maxTextWidth - 1))
            {
                if (lastSpaceIndex != -1)
                {
                    textBuffer.setCharAt(lastSpaceIndex, '\n');
                    lineStartIndex = lastSpaceIndex + 1;
                    lastSpaceIndex = -1;
                }
                else
                {
                    textBuffer.insert(characterIndex, '\n');
                    lineStartIndex = characterIndex + 1;
                }
            }

            // Increment the location in the text buffer.
            characterIndex++;
        }

        // Return the wrapped text.
        return textBuffer.toString();
    }

    /**
     * Takes a single comma-separated-variable String and parses the individual
     * variables from it.  The parsed variables are then returned in a list.
     *
     * @param csvString The Comma-Separated-Variable String.
     * @return An ArrayList containing the parsed String variables.
     */
    public static final ArrayList<String> buildCSVList(String csvString)
    {
        ArrayList<String> variableList;
        String[] variableStrings;

        // Check that the parameter String contains data.
        if (csvString == null)
        {
            return new ArrayList<>();
        }

        // Split the variable String into separate variables.
        variableList = new ArrayList<>();
        variableStrings = csvString.split(",");

        // Add all of the Variables to the list.
        for (int i = 0; i < variableStrings.length; i++)
        {
            // Extract the parameter name and value.
            variableList.add(variableStrings[i].trim());
        }

        return variableList;
    }

    /**
     * This method builds a StringBuffer of comma separated variables from the
     * supplied List of variables.
     *
     * @param variableList The list of variables that will be used to build the
     * CSV String.
     * @return A CSV StringBuffer in the format "var1, var2, var3, var4m .. varx";
     */
    public static final StringBuffer buildCSVString(List variableList)
    {
        StringBuffer csvString;

        // Create the new CSV String and add all variable followed by a comma to it.
        csvString = new StringBuffer();
        for (int variableIndex = 0; variableIndex < variableList.size() - 1; variableIndex++)
        {
            csvString.append(variableList.get(variableIndex));
            csvString.append(", ");
        }

        // Add the last variable without a comma.
        if (variableList.size() > 0) csvString.append(variableList.get(variableList.size() - 1));

        return csvString;
    }

    /**
     * This method builds a StringBuffer of comma separated variables from the
     * supplied Collection of variables.
     *
     * @param variableCollection The collection of variables that will be used
     * to build the CSV String.
     * @return A CSV StringBuffer in the format "var1, var2, var3, var4m .. varx";
     */
    public static final StringBuffer buildCSVString(Collection variableCollection)
    {
        StringBuffer csvString;
        Iterator varIterator;

        // Create the new CSV String and add all variable followed by a comma to it.
        csvString = new StringBuffer();
        varIterator = variableCollection.iterator();
        while (varIterator.hasNext())
        {
            csvString.append(varIterator.next());
            if (varIterator.hasNext()) csvString.append(", ");
        }

        return csvString;
    }

    /**
     * This method creates a StringBuilder by appending the .toString() values of all the objects in the supplied Collection, separated by the specified string.
     *
     * @param collection The collection of objects that will be used to build the output String.
     * @param separator The separator to append between two objects in the string.
     * @return A StringBuilder in the format "obj1<<-separator->>obj2<<-separator->>obj3 .. objn";
     */
    public static final StringBuilder buildString(Collection collection, String separator)
    {
        StringBuilder result;
        Iterator varIterator;

        // Create the new String and add all objects, separated by the supplied string.
        result = new StringBuilder();
        varIterator = collection.iterator();
        while (varIterator.hasNext())
        {
            result.append(varIterator.next());
            if (varIterator.hasNext()) result.append(separator);
        }

        return result;
    }

    /**
     * This method builds a StringBuffer of comma separated values each of which
     * is wrapped in the specified StringUtilities.
     *
     * @param variableList The list of variables that will be used to build the
     * CSV String.
     * @return A CSV StringBuffer.
     */
    public static final StringBuffer buildCSVString(List<String> values, String leftWrapCharacters, String rightWrapCharacters)
    {
        StringBuffer csvString;

        // Create the new CSV String and add all variable followed by a comma to it.
        csvString = new StringBuffer();
        for (int index = 0; index < values.size() - 1; index++)
        {
            csvString.append(leftWrapCharacters);
            csvString.append(values.get(index));
            csvString.append(rightWrapCharacters);
            csvString.append(", ");
        }

        csvString.append(leftWrapCharacters);
        csvString.append(values.get(values.size()-1));
        csvString.append(rightWrapCharacters);

        return csvString;
    }

    /**
     * This method takes a single parameter String and parses the individual
     * parameters from it.  Parameter name-value pairs are separated by the
     * specified separatorString, however the '=' sign must be used for the
     * assignment of a value to the parameter name ie. 'name=value'.  The parsed
     * parameters are then stored and returned in a HashMap consisting of
     * (parameterName, parameterValue) pairs.
     *
     * @param parameterString The String containing the parameters to parse.
     * @param separatorString The String separating different parameter
     * name-value assignments.
     * @return A LinkedHashMap containing the parsed parameters.  The parameters
     * are returned specifically as a LinkedHashMap so that the order is
     * maintained, making it possible to identify parameter pairs.
     */
    public static LinkedHashMap buildParameterMap(String parameterString, String separatorString)
    {
        LinkedHashMap parameterMap;
        String[] parameterStrings;

        // Check that the parameter String contains data.
        if ((parameterString == null) || (parameterString.length() < 3))
        {
            return new LinkedHashMap();
        }

        // Remove all of the white spaces from the paramter String and break it into parts containing individual parameter name-value pairs.
        parameterMap = new LinkedHashMap();
        parameterStrings = parameterString.split(separatorString);

        for (int i = 0; i < parameterStrings.length; i++)
        {
            String parameterName;
            String parameterValue;
            String strParameter;

            // Extract the parameter name and value.
            strParameter = parameterStrings[i];
            parameterName = strParameter.substring(0, strParameter.indexOf("=")).trim();
            parameterValue = strParameter.substring(strParameter.indexOf("=") + 1).trim();

            // Add the extracted parameter to the map.
            parameterMap.put(parameterName, parameterValue);
        }

        return parameterMap;
    }

    /**
     * Builds a parameter String of the format 'par1=val1,par2=val2...parn=valn'
     * from the supplied map of key-value pairs.  Null values are simply skipped
     * resulting in a parameter pair of the format 'par1=,...parn=valn'.  If
     * the input Map is null or contains now values, a null value is returned.
     *
     * @param parameterMap The Map from which to build the parameter String.
     * @param separatorString The separator to append between key-value pairs
     * in the map.
     * @return The constructed parameter String.
     */
    public static String buildParameterString(Map<String, Object> parameterMap, String separatorString)
    {
        if ((parameterMap == null) || (parameterMap.size() == 0))
        {
            return null;
        }
        else
        {
            StringBuffer parameterString;
            Iterator<String> keyIterator;

            parameterString = new StringBuffer();
            keyIterator = parameterMap.keySet().iterator();
            while (keyIterator.hasNext())
            {
                String key;
                Object value;

                key = keyIterator.next();
                value = parameterMap.get(key);
                parameterString.append(key);
                parameterString.append("=");
                if (value != null) parameterString.append(value.toString());
                if (keyIterator.hasNext()) parameterString.append(separatorString);
            }

            return parameterString.toString();
        }
    }

    /**
     * Takes the supplied String and replaces all <<-parameterValue->> patterns
     * found, with the corresponding parameter values obtained from the supplied
     * parameter map.
     *
     * @deprecated Use replaceStringParameters.
     * @param originalMessage The original String into which the parameter
     * values will be parsed.
     * @param parameters A HashMap containing the available parameter values.
     * The HashMap must consist of (parameterName, parameterValue) pairs.
     * @return The resultant String after all <<-parameterName->> matches were
     * replaced with parameter values.
     */
    @Deprecated
    public static String parseStringParameters(String originalMessage, HashMap<String, String> parameters)
    {
        return replaceStringParameters(originalMessage, parameters);
    }

    /**
     * Takes the supplied String and replaces all <<-parameterValue->> patterns
     * found, with the corresponding parameter values obtained from the supplied
     * parameter map.
     *
     * @param originalMessage The original String into which the parameter
     * values will be parsed.
     * @param parameters A HashMap containing the available parameter values.
     * The Map must consist of (parameterName, parameterValue) pairs.
     * @return The resultant String after all <<-parameterName->> matches were
     * replaced with parameter values.
     */
    public static String replaceStringParameters(String originalMessage, Map<String, String> parameters)
    {
        if (originalMessage != null)
        {
            Iterator<String> parameterIterator;
            String message;

            message = originalMessage;
            parameterIterator = parameters.keySet().iterator();

            while (parameterIterator.hasNext())
            {
                String parameterName;

                parameterName = parameterIterator.next();
                while (message.contains("<<-" + parameterName + "->>"))
                {
                    message = message.replaceFirst("<<-" + parameterName + "->>", "" + parameters.get(parameterName));
                }
            }

            return message;
        }
        else
        {
            return null;
        }
    }

    /**
     * This method is used to replace a variable amount of parameters(identified by the 'beginString''endString' surrounding it) in a
     * inside a String literal with a variable amount of parameters inside the supplied maps.
     *
     * @param beginString the String the Parameter is prefixed with.
     * @param endString the String the Parameter is suffixed with.
     * @param textToBeReplaces The String literal that needs the contained Parameters
     * (identified by the 'beginString''endString' surrounding it) to be replaced.
     * @param quotes a Boolean variable that indicates if quotes needs to surround the replaced variable.
     * @param maps a Variable amount of maps that contains a variable amount of variables.
     * @return String the String literal that the variables contained was replaced by values inside the Maps
     * @exception throws an Exception if there is a variable defined that is not contained in one of the Maps.
     */
    public static String replaceValues(String beginString, String endString, String textToBeReplaced, boolean quotes, HashMap... maps)
    {
        while (textToBeReplaced.indexOf(beginString) != -1)
        {
            boolean wasReplaced = false;
            String variableToBeReplaced = textToBeReplaced.substring(textToBeReplaced.indexOf(beginString) + 3, textToBeReplaced.indexOf(endString));
            for (HashMap map : maps)
            {
                if (map.containsKey(variableToBeReplaced))
                {
                    if (quotes)
                    {
                        textToBeReplaced = textToBeReplaced.replaceAll(beginString + variableToBeReplaced + endString,
                                "'" + (String) map.get(variableToBeReplaced) + "'");
                    }
                    else
                    {
                        textToBeReplaced = textToBeReplaced.replaceAll(beginString + variableToBeReplaced + endString,
                                (String) map.get(variableToBeReplaced));
                    }
                    wasReplaced = true;
                }
            }

            if (!wasReplaced)
            {
                textToBeReplaced = textToBeReplaced.replaceAll(beginString + variableToBeReplaced + endString, "");
            }
        }

        return textToBeReplaced;
    }

    /**
     * Creates an array of StringUtilities by iterating over the input object array and
     * calling the toString() function on each element in the input array.  If
     * an element in the input array is null, the resultant String in the output
     * array will also be null.
     *
     * @param objectArray The input array from which to build a String array.
     * @return An array of StringUtilities created from the objects in the input array.
     */
    public static String[] toStringArray(Object[] objectArray)
    {
        String[] stringArray;

        stringArray = new String[objectArray.length];
        for (int objectIndex = 0; objectIndex < objectArray.length; objectIndex++)
        {
            Object object;

            object = objectArray[objectIndex];
            stringArray[objectIndex] = object == null ? null : object.toString();
        }

        return stringArray;
    }

    /**
     * This method implements a much more accurate String replacement algorithm
     * than the default ones available as part of the String and Matcher
     * classes.  Client code can supply three patterns that are used to
     * accurately match target character sequences in the supplied StringBuffer.
     * Character sequences are matched using the pre-pattern to match the prefix
     * of the target String, the in-pattern to match the target String itself
     * and a post-pattern to match the suffix of the target String.  For all
     * matches only the group matched by the in-pattern is replaced by the
     * specified replacement String.
     *
     * @param prePattern The pattern to match the target String prefix.  Can be
     * null if no prefix needs to be matched.
     * @param inPattern The pattern to match the target String.
     * @param postPattern The pattern to match the target String suffix.  Can be
     * null if no suffix needs to be matched.
     * @param inputString The input StringBuffer on which replacement will be
     * performed.
     * @param replacementString The replacement String to replace all identified
     * matches.
     * @return The number of replacements performed.
     */
    public static int replaceAll(String prePattern, String inPattern, String postPattern, StringBuffer inputString, String replacementString)
    {
        String prePatternStr;
        String inPatternStr;
        String postPatternStr;
        Pattern matchPattern;
        Matcher matcher;
        int position;
        int replacementLength;
        int replacementCount;

        prePatternStr = (prePattern == null ? "" : prePattern);
        inPatternStr = (inPattern == null ? "" : inPattern);
        postPatternStr = (postPattern == null ? "" : postPattern);

        // Compile the pattern.
        matchPattern = Pattern.compile("(" + prePatternStr + ")(" + inPatternStr + ")(" + postPatternStr + ")");

        // Match all occurences and replace it with the suitable phrase/terminology.
        position = 0;
        replacementCount = 0;
        replacementLength = replacementString.length();
        matcher = matchPattern.matcher(inputString);
        while (matcher.find(position))
        {
            int replaceStart;
            int replaceEnd;

            replaceStart = matcher.start(2);
            replaceEnd = matcher.end(2);

            inputString.delete(replaceStart, replaceEnd);
            inputString.insert(replaceStart, replacementString);

            position = replaceStart + replacementLength;
            replacementCount++;
        }

        return replacementCount;
    }

    /**
     * A version of the standard string replacement method designed for
     * replacement of the same recurring pattern, with a different value for
     * each occurrence.  For each occurrence of the matched pattern, the
     * replacement string at the corresponding index of the supplied list will
     * be used as replacement i.e. the first occurrence of the pattern will be
     * replaced by the replacement string at index 0 in the supplied list, the
     * second occurrence will be replacement by the replacement string at index
     * 1 etc.  If more occurrences of the pattern is matched, than the number of
     * replacement strings in the list, an IndexOutOfBoundsException will be
     * thrown.
     *
     * @param prePattern The pattern to match the target String prefix.  Can be
     * null if no prefix needs to be matched.
     * @param inPattern The pattern to match the target String.
     * @param postPattern The pattern to match the target String suffix.  Can be
     * null if no suffix needs to be matched.
     * @param inputString The input StringBuffer on which replacement will be
     * performed.
     * @param replacementStrings The list of replacement strings to replace all
     * identified pattern matches.  The number of Strings in this list must be
     * equal to or more than the number of pattern matches found.
     * @return The number of replacements performed.
     */
    public static int replaceAll(String prePattern, String inPattern, String postPattern, StringBuffer inputString, List<String> replacementStrings)
    {
        String prePatternStr;
        String inPatternStr;
        String postPatternStr;
        Pattern matchPattern;
        Matcher matcher;
        int position;
        int replacementCount;

        prePatternStr = (prePattern == null ? "" : prePattern);
        inPatternStr = (inPattern == null ? "" : inPattern);
        postPatternStr = (postPattern == null ? "" : postPattern);

        // Compile the pattern.
        matchPattern = Pattern.compile("(" + prePatternStr + ")(" + inPatternStr + ")(" + postPatternStr + ")");

        // Match all occurences and replace it with the suitable phrase/terminology.
        position = 0;
        replacementCount = 0;
        matcher = matchPattern.matcher(inputString);
        while (matcher.find(position))
        {
            String replacementString;
            int replacementLength;
            int replaceStart;
            int replaceEnd;

            replacementString = replacementStrings.get(replacementCount);
            replacementLength = replacementString.length();

            replaceStart = matcher.start(2);
            replaceEnd = matcher.end(2);

            inputString.delete(replaceStart, replaceEnd);
            inputString.insert(replaceStart, replacementString);

            position = replaceStart + replacementLength;
            replacementCount++;
        }

        return replacementCount;
    }

    /**
     * This method find all String matching a specified pattern within the
     * supplied input String and then returns the list of matches.  Character
     * sequences are matched using the pre-pattern to match the prefix
     * of the target String, the in-pattern to match the target String itself
     * and a post-pattern to match the suffix of the target String.  For all
     * matches only the group matched by the in-pattern is replaced by the
     * specified replacement String.
     *
     * @param prePattern The pattern to match the target String prefix.  Can be
     * null if no prefix needs to be matched.
     * @param inPattern The pattern to match the target String.
     * @param postPattern The pattern to match the target String suffix.  Can be
     * null if no suffix needs to be matched.
     * @param inputString The input StringBuffer on which replacement will be
     * performed.
     * @return The list of String that were matched using the supplied patterns.
     */
    public static List<String> findStrings(String prePattern, String inPattern, String postPattern, StringBuffer inputString)
    {
        List<String> foundStrings;
        String prePatternStr;
        String inPatternStr;
        String postPatternStr;
        Pattern matchPattern;
        Matcher matcher;
        int position;

        prePatternStr = (prePattern == null ? "" : prePattern);
        inPatternStr = (inPattern == null ? "" : inPattern);
        postPatternStr = (postPattern == null ? "" : postPattern);

        // Compile the pattern.
        matchPattern = Pattern.compile("(" + prePatternStr + ")(" + inPatternStr + ")(" + postPatternStr + ")");

        // Match all occurences and extract the String matching in the 'inPattern'.
        position = 0;
        foundStrings = new ArrayList<String>();
        matcher = matchPattern.matcher(inputString);
        while (matcher.find(position))
        {
            String foundString;
            int foundStringStart;
            int foundStringEnd;

            foundStringStart = matcher.start(2);
            foundStringEnd = matcher.end(2);

            foundString = inputString.substring(foundStringStart, foundStringEnd);
            foundStrings.add(foundString);

            position = foundStringEnd;
        }

        return foundStrings;
    }


    /**
     * This will convert the byte representation of the file size to a human readable file size
     * in the form of kB, MB etc
     * @param fileSize The file size in bytes
     * @return A String representation of the human readable file size example: 10MB
     */
    public static String getFileSizeString(long fileSize)
    {
        double calculatedFileSize;
        StringBuilder sizeString;
        int iterations;

        calculatedFileSize = fileSize;
        iterations = 0;

        while (calculatedFileSize > 1000)
        {
            iterations++;
            calculatedFileSize /= 1024;
        }

        sizeString = new StringBuilder();
        sizeString.append(new DecimalFormat("##.00").format(calculatedFileSize));

        switch (iterations)
        {
            case 1:
                sizeString.append("KB");
                break;
            case 2:
                sizeString.append("MB");
                break;
            case 3:
                sizeString.append("GB");
                break;
            case 4:
                sizeString.append("TB");
                break;
            case 5:
                sizeString.append("PB");
                break;
            default:
                sizeString.append(" bytes");
        }

        return sizeString.toString();
    }


    /**
     * Builds a human readable time string of the time in milliseconds.
     * If the hours is longer than 100 then undeterminable will be shown.
     * @param millis The millisecond representation of the time
     * @param undeterminableString The translated version of "undeterminable"
     * @param hoursString The translated version of "hours"
     * @param minutesString The translated version of "minutes"
     * @param secondsString The translated version of "seconds"
     * @return A human representable string of the time, example: 1 hours, 15 minutes, 2 seconds
     */
    public static String buildTimeString(long millis, String undeterminableString, String hoursString, String minutesString, String secondsString)
    {
        StringBuilder timeString;
        long secondInMillis = TimeUnit.SECONDS.toMillis(1);
        long minuteInMillis = TimeUnit.MINUTES.toMillis(1);
        long hourInMillis = TimeUnit.HOURS.toMillis(1);
        long elapsedHours;
        long elapsedMinutes;
        long elapsedSeconds;
        long diff;

        diff = millis;
        elapsedHours = diff / hourInMillis;
        diff %= hourInMillis;
        elapsedMinutes = diff / minuteInMillis;
        diff %= minuteInMillis;
        elapsedSeconds = diff / secondInMillis;

        timeString = new StringBuilder();
        if (elapsedHours > 100)
        {
            timeString.append(undeterminableString);
        }
        else
        {
            if (elapsedHours > 0)
            {
                timeString.append(elapsedHours);
                timeString.append(" ");
                timeString.append(hoursString);
                timeString.append(", ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0))
            {
                timeString.append(elapsedMinutes);
                timeString.append(" ");
                timeString.append(minutesString);
                timeString.append(", ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0) || (elapsedSeconds > 0))
            {
                timeString.append(elapsedSeconds);
                timeString.append(" ");
                timeString.append(secondsString);
            }
        }

        return timeString.toString();
    }

    /**
     * Assumes that the {@code String} supplied is in singular form and attempts
     * to convert it to a plural form.
     *
     * @param singular The {@code String} to be made plural
     *
     * @return The plural form of the supplied {@code String}
     */
    public static String getPluralString(String singular)
    {
        if (singular.endsWith("Y")) return singular.substring(0, singular.length() - 1).concat("IES");
        if (singular.endsWith("S")) return singular;
        return singular + "S";
    }

    /**
     * Extracts the first character of every word from a specified
     * {@code String}. In other words the first character, and every subsequent
     * character that follows a whitespace, will be concatenated.
     *
     * @param completeString The complete {@code String} from which the initial
     *      characters should be extracted
     *
     * @return The {@code String} containing the concatenated initial characters
     */
    public static String getInitChars(String completeString)
    {
        StringBuilder initBuilder;

        initBuilder = new StringBuilder();
        initBuilder.append(completeString.charAt(0));
        for (int cIdx = 1; cIdx < completeString.length(); cIdx++)
        {
            if (Character.isWhitespace(completeString.charAt(cIdx)))
            {
                do cIdx++;
                while (Character.isWhitespace(completeString.charAt(cIdx)));

                initBuilder.append(completeString.charAt(cIdx));
            }
        }

        return initBuilder.toString();
    }

    /**
     * Equivalent to calling {@code getListString(list, ",")}.
     *
     * @see #getListString(java.util.List, java.lang.String)
     *
     * @param list The {@code List} of values to be concatenated into a single
     *      {@code String} separated by a comma (",")
     *
     * @return The {@code String} of comma separated list values
     */
    public static String getListString(List<?> list)
    {
        return getListString(list, ",");
    }

    /**
     * Builds a {@code String} representation of the specified list of values.
     * This {@code String} will have each value in the list, in the order which
     * they occur listed as a single {@code String} using the separator
     * specified to delimit the values. The separator can be an empty
     * {@code String}, in which case the list of values will form a single
     * {@code String} with values occurring head-to-tail.
     *
     * @param list The {@code List} of values to be concatenated into a single
     *      {@code String} separated by the specified separator
     * @param separator The {@code String} specifying the separator to use when
     *      concatenating the values in the list
     *
     * @return The {@code String} of list values, separated using the specified
     *      separator
     */
    public static String getListString(List<?> list, String separator)
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder();

        if (list != null)
        {
            for (Iterator<?> it = list.iterator(); it.hasNext();)
            {
                toStringBuilder.append(it.next());
                if (it.hasNext()) toStringBuilder.append(separator);
            }
        }

        return toStringBuilder.toString();
    }
}
