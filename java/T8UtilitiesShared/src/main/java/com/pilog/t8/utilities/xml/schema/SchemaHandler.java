package com.pilog.t8.utilities.xml.schema;

import com.sun.org.apache.xerces.internal.impl.xs.XSModelImpl;
import com.sun.org.apache.xerces.internal.xs.XSAttributeDeclaration;
import com.sun.org.apache.xerces.internal.xs.XSAttributeUse;
import com.sun.org.apache.xerces.internal.xs.XSComplexTypeDefinition;
import com.sun.org.apache.xerces.internal.xs.XSConstants;
import com.sun.org.apache.xerces.internal.xs.XSElementDeclaration;
import com.sun.org.apache.xerces.internal.xs.XSModel;
import com.sun.org.apache.xerces.internal.xs.XSModelGroup;
import com.sun.org.apache.xerces.internal.xs.XSNamedMap;
import com.sun.org.apache.xerces.internal.xs.XSObject;
import com.sun.org.apache.xerces.internal.xs.XSObjectList;
import com.sun.org.apache.xerces.internal.xs.XSParticle;
import com.sun.org.apache.xerces.internal.xs.XSSimpleTypeDefinition;
import com.sun.org.apache.xerces.internal.xs.XSTerm;
import com.sun.org.apache.xerces.internal.xs.XSTypeDefinition;
import java.util.ArrayList;
import java.util.HashSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Bouwer du Preez
 */
public class SchemaHandler
{
    public static final short CONTENT_TYPE_ELEMENT = XSComplexTypeDefinition.CONTENTTYPE_ELEMENT;
    public static final short CONTENT_TYPE_EMPTY = XSComplexTypeDefinition.CONTENTTYPE_EMPTY;
    public static final short CONTENT_TYPE_MIXED = XSComplexTypeDefinition.CONTENTTYPE_MIXED;
    public static final short CONTENT_TYPE_SIMPLE = XSComplexTypeDefinition.CONTENTTYPE_SIMPLE;

    public static boolean elementMayContainText(XSModel schemaModel, Element element)
    {
        XSElementDeclaration elementDeclaration;

        elementDeclaration = getElementDeclaration(schemaModel, element);
        if (elementDeclaration != null)
        {
            XSTypeDefinition typeDefinition;

            typeDefinition = elementDeclaration.getTypeDefinition();
            if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
            {
                XSComplexTypeDefinition complexType;

                complexType = (XSComplexTypeDefinition)typeDefinition;
                return ((complexType.getContentType() == XSComplexTypeDefinition.CONTENTTYPE_MIXED) || (complexType.getContentType() == XSComplexTypeDefinition.CONTENTTYPE_SIMPLE));
            }
            else return true;
        }
        else throw new RuntimeException("No element declaration found for element '" + element.getTagName() + "'.");
    }

    public static XSElementDeclaration getElementDeclaration(XSModel schemaModel, Element element)
    {
        return getElementDeclaration(schemaModel, element.getLocalName(), element.getNamespaceURI());
    }

    public static HashSet<XSElementDeclaration> getRootElementDeclarations(XSModel schemaModel)
    {
        HashSet<XSElementDeclaration> topLevelElementDeclarations;
        HashSet<XSElementDeclaration> nonRootElementDeclarations;

        topLevelElementDeclarations = getTopLevelElementDeclarations(schemaModel);
        nonRootElementDeclarations = new HashSet<XSElementDeclaration>();
        for (XSElementDeclaration topLevelDeclaration : topLevelElementDeclarations)
        {
            addSubElementDeclarations(schemaModel, topLevelDeclaration, nonRootElementDeclarations);
        }

        topLevelElementDeclarations.removeAll(nonRootElementDeclarations);
        return topLevelElementDeclarations;
    }

    public static HashSet<XSElementDeclaration> getTopLevelElementDeclarations(XSModel schemaModel)
    {
        XSNamedMap elementMap;
        HashSet<XSElementDeclaration> elementList;

        elementMap = schemaModel.getComponents(XSConstants.ELEMENT_DECLARATION);
        elementList = new HashSet<XSElementDeclaration>();
        for (int elementIndex = 0; elementIndex < elementMap.getLength(); elementIndex++)
        {
            elementList.add((XSElementDeclaration)elementMap.item(elementIndex));
        }

        return elementList;
    }

    public static HashSet<XSElementDeclaration> getAllElementDeclarations(XSModel schemaModel)
    {
        XSNamedMap elementMap;
        HashSet<XSElementDeclaration> elementDeclarationSet;

        elementDeclarationSet = new HashSet<XSElementDeclaration>();
        elementMap = schemaModel.getComponents(XSConstants.ELEMENT_DECLARATION);
        for (int elementIndex = 0; elementIndex < elementMap.getLength(); elementIndex++)
        {
            XSElementDeclaration elementDeclaration;

            elementDeclaration = (XSElementDeclaration)elementMap.item(elementIndex);
            elementDeclarationSet.add(elementDeclaration);
            elementDeclarationSet.addAll(getSubElementDeclarations(schemaModel, elementDeclaration));
        }

        return elementDeclarationSet;
    }

    public static XSElementDeclaration getSubElementDeclaration(XSModel schemaModel, XSElementDeclaration elementDeclaration, String elementName, String namespace)
    {
        HashSet<XSElementDeclaration> subElementDeclarations;

        subElementDeclarations = getSubElementDeclarations(schemaModel, elementDeclaration);
        return getElementDeclaration(subElementDeclarations, elementName, namespace);
    }

    public static HashSet<XSElementDeclaration> getSubElementDeclarations(XSModel schemaModel, XSElementDeclaration elementDeclaration)
    {
        HashSet<XSElementDeclaration> subElementDeclarations;

        subElementDeclarations = new HashSet<XSElementDeclaration>();
        addSubElementDeclarations(schemaModel, elementDeclaration, subElementDeclarations);
        return subElementDeclarations;
    }

    public static HashSet<XSAttributeDeclaration> getAllAttributeDeclarations(XSModel schemaModel)
    {
        HashSet<XSAttributeDeclaration> attributeDeclarationSet;
        HashSet<XSElementDeclaration> elementDeclarations;

        attributeDeclarationSet = new HashSet<XSAttributeDeclaration>();
        elementDeclarations = getAllElementDeclarations(schemaModel);
        for (XSElementDeclaration elementDeclaration : elementDeclarations)
        {
            attributeDeclarationSet.addAll(getElementDeclarationAttributeDeclarations(schemaModel, elementDeclaration));
        }

        return attributeDeclarationSet;
    }

    public static void addSubElementDeclarations(XSModel schemaModel, XSElementDeclaration elementDeclaration, HashSet<XSElementDeclaration> subElementDeclarations)
    {
        XSTypeDefinition typeDefinition;
        XSObjectList substitutionGroup;

        typeDefinition = elementDeclaration.getTypeDefinition();
        substitutionGroup = ((XSModelImpl)schemaModel).getSubstitutionGroup(elementDeclaration);

        if ((substitutionGroup != null) && (substitutionGroup.getLength() > 0))
        {
            for (int objectIndex = 0; objectIndex < substitutionGroup.getLength(); objectIndex++)
            {
                XSElementDeclaration substitutionElementDeclaration;

                substitutionElementDeclaration = (XSElementDeclaration)substitutionGroup.item(objectIndex);
                if (!subElementDeclarations.contains(substitutionElementDeclaration))
                {
                    subElementDeclarations.add(substitutionElementDeclaration);
                    addSubElementDeclarations(schemaModel, substitutionElementDeclaration, subElementDeclarations);
                }
            }
        }

        if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
        {
            XSComplexTypeDefinition complexType;
            short contentType;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            contentType = complexType.getContentType();
            if ((contentType == XSComplexTypeDefinition.CONTENTTYPE_ELEMENT) || (contentType == XSComplexTypeDefinition.CONTENTTYPE_MIXED))
            {
                XSTerm term;

                term = complexType.getParticle().getTerm();
                if (term instanceof XSElementDeclaration)
                {
                    XSElementDeclaration subElementDeclaration;

                    subElementDeclaration = (XSElementDeclaration)term;
                    if (!subElementDeclarations.contains(subElementDeclaration))
                    {
                        subElementDeclarations.add(subElementDeclaration);
                        addSubElementDeclarations(schemaModel, subElementDeclaration, subElementDeclarations);
                    }
                }
                else if (term instanceof XSModelGroup)
                {
                    addModelGroupElementDeclarations(schemaModel, (XSModelGroup)term, subElementDeclarations);
                }
            }
        }
    }

    private static HashSet<XSElementDeclaration> addModelGroupElementDeclarations(XSModel schemaModel, XSModelGroup modelGroup, HashSet<XSElementDeclaration> elementDeclarations)
    {
        XSObjectList objectList;

        objectList = modelGroup.getParticles();
        for (int objectIndex = 0; objectIndex < objectList.getLength(); objectIndex++)
        {
            XSTerm term;

            term = ((XSParticle)objectList.item(objectIndex)).getTerm();
            if (term instanceof XSElementDeclaration)
            {
                XSElementDeclaration elementDeclaration;
                
                elementDeclaration = (XSElementDeclaration)term;
                if (!elementDeclarations.contains(elementDeclaration))
                {
                    elementDeclarations.add(elementDeclaration);
                    addSubElementDeclarations(schemaModel, elementDeclaration, elementDeclarations);
                }
            }
            else if (term instanceof XSModelGroup)
            {
                elementDeclarations.addAll(addModelGroupElementDeclarations(schemaModel, (XSModelGroup)term, elementDeclarations));
            }
        }

        return elementDeclarations;
    }

    public static XSElementDeclaration getElementDeclaration(XSModel schemaModel, String elementName, String namespace)
    {
        XSElementDeclaration elementDeclaration;

        elementDeclaration = schemaModel.getElementDeclaration(elementName, namespace);
        if (elementDeclaration != null) return elementDeclaration;
        else
        {
            HashSet<XSElementDeclaration> elementDeclarations;

            elementDeclarations = getAllElementDeclarations(schemaModel);
            for (XSElementDeclaration subElementDeclaration : elementDeclarations)
            {
                if (namespace != null)
                {
                    if (namespace.equalsIgnoreCase(subElementDeclaration.getNamespace()))
                    {
                        if (elementName.equalsIgnoreCase(subElementDeclaration.getName()))
                        {
                            return subElementDeclaration;
                        }
                    }
                }
                else if (elementName.equalsIgnoreCase(subElementDeclaration.getName()))
                {
                    return subElementDeclaration;
                }
            }

            return null;
        }
    }

    public static XSElementDeclaration getElementDeclaration(HashSet<XSElementDeclaration> elementDeclarations, String elementName, String namespace)
    {
        for (XSElementDeclaration subElementDeclaration : elementDeclarations)
        {
            if (namespace != null)
            {
                if (namespace.equalsIgnoreCase(subElementDeclaration.getNamespace()))
                {
                    if (elementName.equalsIgnoreCase(subElementDeclaration.getName()))
                    {
                        return subElementDeclaration;
                    }
                }
            }
            else if (elementName.equalsIgnoreCase(subElementDeclaration.getName()))
            {
                return subElementDeclaration;
            }
        }

        return null;
    }

    public static ArrayList<String> getElementAttributeNames(XSModel schemaModel, String elementName, String namespace)
    {
        XSElementDeclaration elementDeclaration;

        elementDeclaration = getElementDeclaration(schemaModel, elementName, namespace);
        if (elementDeclaration != null) return getElementDeclarationAttributeNames(schemaModel, elementDeclaration);
        else return new ArrayList<String>();
    }

    public static boolean mayContainSubElements(XSElementDeclaration elementDeclaration)
    {
        XSTypeDefinition typeDefinition;

        typeDefinition = elementDeclaration.getTypeDefinition();
        System.out.println("Type definition: " + typeDefinition);
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.SIMPLE_TYPE)
        {
            return false;
        }
        else if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
        {
            XSComplexTypeDefinition complexType;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            return ((complexType.getContentType() == XSComplexTypeDefinition.CONTENTTYPE_ELEMENT) || (complexType.getContentType() == XSComplexTypeDefinition.CONTENTTYPE_MIXED));
        }
        else
        {
            return false;
        }
    }

    public static ArrayList<String> getAllPossibleElementNames(XSModel schemaModel)
    {
        ArrayList<String> elementNames;
        HashSet<XSElementDeclaration> elementDeclarations;

        elementNames = new ArrayList<String>();
        elementDeclarations = getAllElementDeclarations(schemaModel);
        for (XSElementDeclaration elementDeclaration : elementDeclarations)
        {
            elementNames.add(elementDeclaration.getName());
        }

        return elementNames;
    }

    public static String getElementNamespace(XSModel schemaModel, String elementName)
    {
        ArrayList<String> namespaces;
        XSNamedMap map;

        namespaces = new ArrayList<String>();
        map = schemaModel.getComponents(XSConstants.ELEMENT_DECLARATION);
        for (int elementIndex = 0; elementIndex < map.getLength(); elementIndex++)
        {
            XSObject element;

            element = map.item(elementIndex);
            if (element.getName().equalsIgnoreCase(elementName))
            {
                return element.getNamespace();
            }
        }

        return null;
    }

    public static boolean isAttributeRequired(XSModel schemaModel, XSElementDeclaration elementDeclaration, String attributeName)
    {
        XSTypeDefinition typeDefinition;

        typeDefinition = elementDeclaration.getTypeDefinition();
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
        {
            XSComplexTypeDefinition complexType;
            XSObjectList attributeList;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            attributeList = complexType.getAttributeUses();
            for (int attributeIndex = 0; attributeIndex < attributeList.getLength(); attributeIndex++)
            {
                XSAttributeUse attributeUse;
                String attrName;

                attributeUse = (XSAttributeUse)attributeList.item(attributeIndex);
                attrName = attributeUse.getAttrDeclaration().getName();
                if (attrName.equalsIgnoreCase(attributeName))
                {
                    return attributeUse.getRequired();
                }
            }
            return false;
        }
        else return false;
    }

    public static boolean isAttributeBoolean(XSModel schemaModel, XSElementDeclaration elementDeclaration, String attributeName)
    {
        XSTypeDefinition typeDefinition;

        typeDefinition = elementDeclaration.getTypeDefinition();
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
        {
            XSComplexTypeDefinition complexType;
            XSObjectList attributeList;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            attributeList = complexType.getAttributeUses();
            for (int attributeIndex = 0; attributeIndex < attributeList.getLength(); attributeIndex++)
            {
                XSAttributeUse attributeUse;
                String attrName;

                attributeUse = (XSAttributeUse)attributeList.item(attributeIndex);
                attrName = attributeUse.getAttrDeclaration().getName();
                if (attrName.equalsIgnoreCase(attributeName))
                {
                    return attributeUse.getAttrDeclaration().getTypeDefinition().getBuiltInKind() == XSConstants.BOOLEAN_DT;
                }
            }
            return false;
        }
        else return false;
    }

    public static Element buildDOMElement(XSModel schemaModel, Document domDocument, XSElementDeclaration elementDeclaration)
    {
        XSTypeDefinition typeDefinition;

        typeDefinition = elementDeclaration.getTypeDefinition();
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.SIMPLE_TYPE)
        {
            XSSimpleTypeDefinition simpleType;
            Element newElement;

            simpleType = (XSSimpleTypeDefinition)typeDefinition;
            newElement = domDocument.createElementNS(elementDeclaration.getNamespace(), elementDeclaration.getName());

            return newElement;
        }
        else
        {
            XSComplexTypeDefinition complexType;
            XSObjectList attributeList;
            Element newElement;
            String elementNamespace;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            elementNamespace = elementDeclaration.getNamespace();

            newElement = domDocument.createElementNS(elementNamespace, elementDeclaration.getName());
            newElement.setPrefix(domDocument.lookupPrefix(elementNamespace));
            attributeList = complexType.getAttributeUses();
            for (int attributeIndex = 0; attributeIndex < attributeList.getLength(); attributeIndex++)
            {
                XSAttributeUse attributeUse;

                attributeUse = (XSAttributeUse)attributeList.item(attributeIndex);
                if (attributeUse.getRequired())
                {
                    XSAttributeDeclaration attributeDeclaration;
                    String attributeNamespace;
                    String attributeName;
                    
                    attributeDeclaration = attributeUse.getAttrDeclaration();
                    attributeName = attributeDeclaration.getName();
                    attributeNamespace = attributeDeclaration.getNamespace();
                    newElement.setAttributeNS(attributeNamespace, attributeName, null);
                }
            }

            return newElement;
        }
    }
    
    public static ArrayList<String> getElementDeclarationAttributeNames(XSModel schemaModel, XSElementDeclaration elementDeclaration)
    {
        XSTypeDefinition typeDefinition;
        ArrayList<String> attributeNames;

        attributeNames = new ArrayList<String>();
        typeDefinition = elementDeclaration.getTypeDefinition();
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
        {
            XSComplexTypeDefinition complexType;
            XSObjectList attributeList;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            attributeList = complexType.getAttributeUses();
            for (int attributeIndex = 0; attributeIndex < attributeList.getLength(); attributeIndex++)
            {
                XSAttributeUse attributeUse;

                attributeUse = (XSAttributeUse)attributeList.item(attributeIndex);
                attributeNames.add(attributeUse.getAttrDeclaration().getName());
            }
        }
        
        return attributeNames;
    }

    public static ArrayList<XSAttributeDeclaration> getElementDeclarationAttributeDeclarations(XSModel schemaModel, XSElementDeclaration elementDeclaration)
    {
        XSTypeDefinition typeDefinition;
        ArrayList<XSAttributeDeclaration> attributeDeclarations;

        attributeDeclarations = new ArrayList<XSAttributeDeclaration>();
        typeDefinition = elementDeclaration.getTypeDefinition();
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE)
        {
            XSComplexTypeDefinition complexType;
            XSObjectList attributeList;

            complexType = (XSComplexTypeDefinition)typeDefinition;
            attributeList = complexType.getAttributeUses();
            for (int attributeIndex = 0; attributeIndex < attributeList.getLength(); attributeIndex++)
            {
                XSAttributeUse attributeUse;

                attributeUse = (XSAttributeUse)attributeList.item(attributeIndex);
                attributeDeclarations.add(attributeUse.getAttrDeclaration());
            }
        }

        return attributeDeclarations;
    }

    public static Object getAttributePrimitiveType(XSModel schemaModel, XSAttributeDeclaration attributeDeclaration)
    {
        return getSimpleTypeDefinitionPrimitiveType(schemaModel, attributeDeclaration.getTypeDefinition());
    }

    public static Object getSimpleTypeDefinitionPrimitiveType(XSModel schemaModel, XSSimpleTypeDefinition simpleType)
    {
        short variety;

        variety = simpleType.getVariety();
        if (variety == XSSimpleTypeDefinition.VARIETY_ATOMIC)
        {
            return simpleType.getPrimitiveType().getBuiltInKind();
        }
        else if (variety == XSSimpleTypeDefinition.VARIETY_LIST)
        {
            XSSimpleTypeDefinition listSimpleType;
            ArrayList listType;

            listSimpleType = simpleType.getItemType();
            listType = new ArrayList();
            listType.add(getSimpleTypeDefinitionPrimitiveType(schemaModel, listSimpleType));

            return listType;
        }
        else if (variety == XSSimpleTypeDefinition.VARIETY_UNION)
        {
            // To Do: Add validation of UNION variety.
            return false;
        }
        else return false;
    }

    public static Element getChildElement(XSModel schemaModel, Element parentElement, XSElementDeclaration elementDeclaration)
    {
        NodeList childNodes;
        SchemaHandler schemaHandler;

        childNodes = parentElement.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode instanceof Element)
            {
                Element childElement;
                XSElementDeclaration childElementDeclaration;

                childElement = (Element)childNode;
                childElementDeclaration = getElementDeclaration(schemaModel, childElement);
                if (elementDeclaration.equals(childElementDeclaration)) return childElement;
            }
        }

        return null;
    }
}
