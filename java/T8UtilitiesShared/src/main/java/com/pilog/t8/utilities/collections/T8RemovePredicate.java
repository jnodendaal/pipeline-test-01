/**
 * Created on 13 May 2015, 10:50:42 AM
 */
package com.pilog.t8.utilities.collections;

import java.util.function.Predicate;

/**
 * A simple abstracted implementation of the {@code Predicate} class. This is
 * simply to make code using a {@link Predicate} during a
 * {@link java.util.Collection#removeIf(java.util.function.Predicate)} call more
 * readable and understandable, since the
 * {@link Predicate#test(java.lang.Object)} method is too generic.
 *
 * @author Gavin Boshoff
 *
 * @param <T> The type of object which will be handled by the predicate
 */
public abstract class T8RemovePredicate<T> implements Predicate<T>
{
    /**
     * Specifies whether or not the item should be removed from the collection
     * being iterated through.
     *
     * @param iterItem The current item in the collection
     *
     * @return {@code true} if the item should be removed. {@code false} if not
     */
    public abstract boolean remove(T iterItem);

    @Override
    public boolean test(T t)
    {
        return remove(t);
    }
}