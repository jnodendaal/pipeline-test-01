package com.pilog.t8.utilities.unicode;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.io.Reader;

/**
 * Generic Unicode text reader, which will use BOM mark to identify the encoding
 * to be used. If BOM is not found then a given default or system will be
 * used encoding.
 *
 * @author Bouwer du Preez
 */
public class UnicodeReader extends Reader
{
    private PushbackInputStream pbInputStream;
    private InputStreamReader inputStream = null;
    private String defaultEncoding;

    private static final int BOM_SIZE = 4;

    /**
     * Constructor.
     * @param inputStream InputStream to be read.
     * @param defaultEncoding Default encoding to be used if stream does not
     * have BOM marker.  If null is supplied the system-level default will be
     * used if no BOM is found.
     */
    public UnicodeReader(InputStream inputStream, String defaultEncoding)
    {
        this.pbInputStream = new PushbackInputStream(inputStream, BOM_SIZE);
        this.defaultEncoding = defaultEncoding;
    }

    /**
     * Returns the default encoding set on this reader.
     *
     * @return This reader's default encoding.
     */
    public String getDefaultEncoding()
    {
        return defaultEncoding;
    }

    /**
     * Returns null if the stream is uninitialized.  Returns the encoding that
     * has been resolved from the input stream and is currently in use by this
     * reader.  This may be the encoding specified by the input stream BOM or it
     * could be the default encoding if no BOM was found.
     */
    public String getEncoding()
    {
        if (inputStream == null)
        {
            return null;
        }
        else
        {
            return inputStream.getEncoding();
        }
    }

    /**
     * Read-ahead four bytes and check for BOM marks. Extra bytes are unread
     * back to the stream, only BOM bytes are skipped.
     */
    protected void init() throws IOException
    {
        String encoding;
        byte[] bom;
        int bytesRead;
        int bytesUnread;

        // If the default input stream has been initialized, return now.
        if (inputStream != null)
        {
            return;
        }

        // Read the BOM bytes and evaluate them.  Evaluations are performed according to Unicode specification.
        bom = new byte[BOM_SIZE];
        bytesRead = pbInputStream.read(bom, 0, bom.length);
        if ((bom[0] == (byte) 0x00) && (bom[1] == (byte) 0x00) && (bom[2] == (byte) 0xFE) && (bom[3] == (byte) 0xFF))
        {
            encoding = "UTF-32BE";
            bytesUnread = bytesRead - 4;
        }
        else if ((bom[0] == (byte) 0xFF) && (bom[1] == (byte) 0xFE) && (bom[2] == (byte) 0x00) && (bom[3] == (byte) 0x00))
        {
            encoding = "UTF-32LE";
            bytesUnread = bytesRead - 4;
        }
        else if ((bom[0] == (byte) 0xEF) && (bom[1] == (byte) 0xBB) && (bom[2] == (byte) 0xBF))
        {
            encoding = "UTF-8";
            bytesUnread = bytesRead - 3;
        }
        else if ((bom[0] == (byte) 0xFE) && (bom[1] == (byte) 0xFF))
        {
            encoding = "UTF-16BE";
            bytesUnread = bytesRead - 2;
        }
        else if ((bom[0] == (byte) 0xFF) && (bom[1] == (byte) 0xFE))
        {
            encoding = "UTF-16LE";
            bytesUnread = bytesRead - 2;
        }
        else
        {
            // Unicode BOM mark not found, unread all bytes.
            encoding = defaultEncoding;
            bytesUnread = bytesRead;
        }

        // If there are any bytes to be unread, do it.
        if (bytesUnread > 0)
        {
            pbInputStream.unread(bom, (bytesRead - bytesUnread), bytesUnread);
        }

        // If no specific encoding could be determined from a BOM, use the system encoding or specified default.
        if (encoding == null)
        {
            inputStream = new InputStreamReader(pbInputStream);
        }
        else
        {
            inputStream = new InputStreamReader(pbInputStream, encoding);
        }
    }

    @Override
    public void close() throws IOException
    {
        init();
        inputStream.close();
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException
    {
        init();
        return inputStream.read(cbuf, off, len);
    }
}
