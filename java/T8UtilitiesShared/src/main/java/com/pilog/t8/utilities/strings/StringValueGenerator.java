package com.pilog.t8.utilities.strings;

import com.mifmif.common.regex.Generex;

/**
 * @author Willem de Bruyn
 */
public class StringValueGenerator 
{
    private Generex randomValueGenerator;
    private StringBuilder randomValue;
    private String pattern;
    private String allowedChars;
    
    public StringValueGenerator(String allowedChars) throws IllegalArgumentException
    {
        this.allowedChars = allowedChars;
        
        // If the allowed characters is not one of the expected cases, throw an exception
        switch (allowedChars)
        {
            case"A-Za-z0-9":
            case"A-Z0-9":
            case"A-Za-z":
            case"a-z0-9":
            case"A-Z":
            case"a-z":
            case"0-9":
                break;
            default:
                throw new IllegalArgumentException("Illegal argument: allowedChars = " + allowedChars);
        }
    }
    
    /**
     * A method to generate random string values
     * @param prefix - The prefix to be added at the beginning of the string value
     * @param suffix - The suffix to be added at the end of the string value
     * @param minLength - The minimum allowed length of the string value
     * @param maxLength - The maximum allowed length of the string value
     * @return  - The generated string value
     */
    public String generateRandomValue(String prefix, String suffix, int minLength, int maxLength)
    {
        // Build the pattern
        pattern = prefix + "[" + allowedChars + "]" + "{" + minLength + "," + maxLength + "}" + suffix;
        
        // Construct the genrator using the pattern
        randomValueGenerator = new Generex(pattern);
        
        // Construct the string builder and generate a random property value
        randomValue = new StringBuilder();
        randomValue.append(randomValueGenerator.random());
        
        return randomValue.toString();
    }
    
    public String generateRandomValue()
    {
        pattern = "[" + allowedChars + "]";
                
        randomValueGenerator = new Generex(pattern);
        randomValue = new StringBuilder();
        
        randomValue.append(randomValueGenerator.random());
        
        return randomValue.toString();
    }
    
    public String generateRandomValue(int length)
    {   
       pattern = "[" + allowedChars + "]" + "{" + length + "}";

       randomValueGenerator = new Generex(pattern);
       randomValue = new StringBuilder();

       randomValue.append(randomValueGenerator.random());

       return randomValue.toString();
    }
    
    public String generateRandomValue(int minLength, int maxLength)
    {
       pattern = "[" + allowedChars + "]" + "{" + minLength + "," + maxLength + "}";

       randomValueGenerator = new Generex(pattern);
       randomValue = new StringBuilder();

       randomValue.append(randomValueGenerator.random());

       return randomValue.toString();
    }
    
    public String generateRandomValue(int length, String suffix)
    {
       pattern = "[" + allowedChars + "]" + "{" + length + "}" + suffix;

       randomValueGenerator = new Generex(pattern);
       randomValue = new StringBuilder();

       randomValue.append(randomValueGenerator.random());

       return randomValue.toString();
    }
    
    public String generateRandomValue(String prefix, int length)
    {
        pattern = prefix + "[" + allowedChars + "]" + "{" + length + "}";
        
        randomValueGenerator = new Generex(pattern);
        
        randomValue = new StringBuilder();
        randomValue.append(randomValueGenerator.random());
        
        return randomValue.toString();
    }       
}
