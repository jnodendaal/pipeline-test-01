package com.pilog.t8.utilities.cache;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class is a basic implementation of a Least Recently Used cache.  The
 * cache implements the Map interface.  Every time a key from this cache is
 * accessed it is moved to the front of a priority queue.  Whenever new keys are
 * added to the cache and the cache's size is at maximum capacity, keys that are
 * last on the priority queue will be removed from the cache to make room for
 * the new keys that need to be added.
 *
 * This means that the cache will never exceed its maximum capacity and will
 * always retain keys that have been used most recently while dropping keys that
 * have been used least recently to make room for new keys.
 *
 * @author Bouwer du Preez
 */
public class LRUCache<K, V> implements Map<K, V>
{
    private final Map<K, V> cache;

    public LRUCache(final int maximumEntries)
    {
        this.cache = Collections.synchronizedMap(new LinkedHashMap<K, V>(maximumEntries, .75F, true)
        {
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest)
            {
                return size() > maximumEntries;
            }
        });
    }

    @Override
    public int size()
    {
        return cache.size();
    }

    @Override
    public boolean isEmpty()
    {
        return cache.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return cache.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return cache.containsValue(value);
    }

    @Override
    public V get(Object key)
    {
        return cache.get(key);
    }

    @Override
    public V put(K key, V value)
    {
        return cache.put(key, value);
    }

    @Override
    public V remove(Object key)
    {
        return cache.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m)
    {
        cache.putAll(m);
    }

    @Override
    public void clear()
    {
        cache.clear();
    }

    @Override
    public Set<K> keySet()
    {
        return cache.keySet();
    }

    @Override
    public Collection<V> values()
    {
        return cache.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet()
    {
        return cache.entrySet();
    }

    @Override
    public String toString()
    {
        return "LRUCache{" + cache + '}';
    }
}
