package com.pilog.t8.utilities.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author Bouwer du Preez
 */
public class ExceptionUtilities
{
    /**
     * Returns the root cause of the supplied throwable.  If the supplied
     * throwable has no cause, it is returned.
     * @param throwable The throwable for which to find a root cause.
     * @return The root cause of the supplied throwable.  The throwable supplied
     * to this method is returned if it is the root.
     */
    public static Throwable getRootCause(Throwable throwable)
    {
        Throwable cause;

        cause = throwable;
        while (cause.getCause() != null)
        {
            cause = cause.getCause();
        }

        return cause;
    }

    /**
     * Returns the root cause message of the supplied throwable.  If the
     * supplied throwable has no cause, its own message is returned.
     * @param throwable The throwable for which to find a root cause message;
     * @return The root cause message of the supplied throwable.  The
     * throwable's own message is returned if it is the root.
     */
    public static String getRootCauseMessage(Throwable throwable)
    {
        Throwable cause;

        cause = getRootCause(throwable);
        return cause.getMessage();
    }

    /**
     * Returns the stack trace of the supplied Throwable as a String value.  A length limit (expressed in number of characters) can be specified
     * in which case the resultant String will be truncated in such a way that the specified number of characters from the start of
     * the stack trace remain in the output along with the specified number of characters from the end of the String.
     * Truncation in the middle of the String is implemented in order to allow the result to be shortened while preserving
     * the root cause at the bottom of the stack, which will be printed at the end of the String, and/or the initial method invocation
     * at the top of the stack, which is printed at the beginning of the String.
     *
     * @param e
     * @param lengthLimitFromStart The length limit from the start (top) of the stack for the String value to return.  -1 indicates no limit.
     * @param lengthLimitFromEnd The length limit from the end (bottom) of the stack for the String value to return.  -1 indicates no limit.
     * @return The stack trace of this exception as a String value.
     */
    public static String getStackTraceString(Throwable e, int lengthLimitFromStart, int lengthLimitFromEnd)
    {
        StringWriter stackTraceWriter;
        String stackTrace;
        int length;

        stackTraceWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stackTraceWriter));
        stackTrace = stackTraceWriter.toString();
        length = stackTrace.length();

        if (lengthLimitFromStart > 0)
        {
            if (lengthLimitFromEnd > 0)
            {
                if (length > (lengthLimitFromStart + lengthLimitFromEnd - 5))
                {
                    String startPart;
                    String endPart;

                    startPart = stackTrace.substring(0, lengthLimitFromStart - 5);
                    endPart = stackTrace.substring(length - lengthLimitFromEnd + 5, length);
                    return startPart + "\n\t.\n\t.\n\t.\n" + endPart;
                }
                else
                {
                    return stackTrace;
                }
            }
            else
            {
                return stackTrace.substring(0, lengthLimitFromStart);
            }
        }
        else if (lengthLimitFromEnd > 0)
        {
            return stackTrace.substring(length - lengthLimitFromEnd, length);
        }
        else
        {
            return stackTrace;
        }
    }
}
