package com.pilog.t8.utilities.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Bouwer du Preez
 */
public class CollectionUtilities
{
    /**
     * This method groups the list of input data rows according to a specified
     * key.  The result is a map of data rows lists, with the following
     * key-value pairs: (Group Value:List of all rows containing the group
     * value).
     * @param dataRows The input data rows to group.
     * @param groupKeyName The key identifier according to which the rows will
     * be grouped.
     * @return A map of grouped data rows.
     */
    public static final Map<String, List<Map<String, Object>>> groupDataRows(List<Map<String, Object>> dataRows, String groupKeyName)
    {
        Map<String, List<Map<String, Object>>> groupedMap;

        groupedMap = new HashMap<String, List<Map<String, Object>>>();
        for (Map<String, Object> dataRow : dataRows)
        {
            List<Map<String, Object>> groupRows;
            String groupKey;

            groupKey = (String)dataRow.get(groupKeyName);
            groupRows = groupedMap.get(groupKey);
            if (groupRows == null)
            {
                groupRows = new ArrayList<Map<String, Object>>();
                groupedMap.put(groupKey, groupRows);
            }

            groupRows.add(dataRow);
        }

        return groupedMap;
    }

    /**
     * This method will loop through all of the supplied data rows and return
     * only those that contain filter value for the specified column.
     *
     * @param dataRows The data rows to filter.
     * @param filterKeyName The name of the key in the data rows, that will be
     * used to filter on.
     * @param filterValue The data value that will be used to filter with.
     * @return The subset of the supplied data row collection that contain the
     * filter values supplied.
     */
    public static final ArrayList<HashMap<String, Object>> getFilteredDataRows(ArrayList<HashMap<String, Object>> dataRows, String filterKeyName, Object filterValue)
    {
        ArrayList<HashMap<String, Object>> filteredDataRows;

        filteredDataRows = new ArrayList();
        for (HashMap dataRow : dataRows)
        {
            Object dataValue;

            dataValue = dataRow.get(filterKeyName);

            // If the data value does not correspond to the key value, return false.
            if (filterValue == null)
            {
                if (dataValue == null)
                {
                    filteredDataRows.add(dataRow);
                }
            }
            else if (filterValue.equals(dataValue))
            {
                filteredDataRows.add(dataRow);
            }
        }

        return filteredDataRows;
    }

    /**
     * This method will loop through all of the supplied data rows and return
     * only those that contain filter values.
     *
     * @param dataRows The data rows to filter.
     * @param filterColumnValues The column values that will be used to filter
     * the supplied data rows.
     * @return The subset of the supplied data row collection that contain the
     * filter values supplied.
     */
    public static final ArrayList<Map<String, Object>> getFilteredDataRows(List<Map<String, Object>> dataRows, HashMap<String, Object> filterColumnValues)
    {
        ArrayList<Map<String, Object>> filteredDataRows;

        filteredDataRows = new ArrayList();
        for (Map dataRow : dataRows)
        {
            if (CollectionUtilities.dataRowFitsKey(dataRow, filterColumnValues))
            {
                filteredDataRows.add(dataRow);
            }
        }

        return filteredDataRows;
    }

    /**
     * Compares the supplied data row to the specified key values and returns
     * a boolean value indicating whether or not the data values in the data row
     * correspond to the key values.
     *
     * @param dataRow A HashMap containing a data row in the form of
     * (columnName, columnValue) pairs.
     * @param keyValues A HashMap containing the key in the form of
     * (keyName, keyValue) pairs.
     * @return Returns true if all of the key values are equal to the
     * corresponding values in the supplied data row, else returns false.
     */
    public static final boolean dataRowFitsKey(Map<String, Object> dataRow, Map<String, Object> keyValues)
    {
        Iterator keyIterator;

        keyIterator = keyValues.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String keyName;
            Object keyValue;
            Object dataValue;

            // Read the key value and the data value.
            keyName = (String)keyIterator.next();
            keyValue = keyValues.get(keyName);
            dataValue = dataRow.get(keyName);

            // If the data value does not correspond to the key value, return false.
            if (keyValue == null)
            {
                if (dataValue != null) return false;
            }
            else if (!keyValue.equals(dataValue)) return false;
        }

        // The data rows fits the key.
        return true;
    }

    /**
     * Iterates over the supplied list of data rows and returns the first one
     * matching the specified filter values.
     *
     * @param dataRows The data rows to evaluate.
     * @param filterColumnValues The filter values to use for evaluation.
     * @return The first row from the specified collection that matches the
     * filter values.
     */
    public static final Map<String, Object> findDataRow(List<Map<String, Object>> dataRows, Map<String, Object> filterColumnValues)
    {
        for (Map dataRow : dataRows)
        {
            if (CollectionUtilities.dataRowFitsKey(dataRow, filterColumnValues))
            {
                return dataRow;
            }
        }

        return null;
    }

    /**
     * Returns a new HashMap created from the supplied HashMap but containing
     * only the keys specified in the supplied list.
     *
     * @param inputMap The map from which the new HashMap will be created.
     * @param keys The list of keys to include in the new map.
     * @return A new HashMap created from the input map, but containing only the
     * specified keys.
     */
    public static final HashMap<String, Object> getHashMapValues(HashMap<String, Object> inputMap, String[] keys)
    {
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<String, Object>();
        for (String key : keys)
        {
            filterValues.put(key, inputMap.get(key));
        }

        return filterValues;
    }

    /**
     * Returns a new HashMap created from the supplied HashMap but containing
     * only the keys specified in the supplied list.
     *
     * @param inputMap The map from which the new HashMap will be created.
     * @param keys The list of keys to include in the new map.
     * @return A new HashMap created from the input map, but containing only the
     * specified keys.
     */
    public static final Map<String, Object> getMapValues(Map<String, Object> inputMap, String[] keys)
    {
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<String, Object>();
        for (String key : keys)
        {
            filterValues.put(key, inputMap.get(key));
        }

        return filterValues;
    }

    /**
     * This method will evaluate all rows in the newRows collection and return
     * a list of rows that are present in the newRows collection but not in the
     * oldRows collection.  Rows are evaluated based on the values of their key
     * columns, as supplied in the keys list.
     *
     * @param oldRows The 'old' collection of data rows.
     * @param newRows The 'new' collection of data rows.
     * @param keys The list of key column names to use for evaluation.
     * @return A list of data rows that are in the newRows collection but not in
     * the old rows collection.
     */
    public static final List<Map<String, Object>> getAddedRows(List<Map<String, Object>> oldRows, List<Map<String, Object>> newRows, String[] keys)
    {
        List<Map<String, Object>> addedRows;

        addedRows = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> newRow : newRows)
        {
            Map<String, Object> oldRow;

            oldRow = findDataRow(oldRows, getMapValues(newRow, keys));
            if (oldRow == null)
            {
                addedRows.add(newRow);
            }
        }

        return addedRows;
    }

    /**
     * This method will evaluate all rows in the newRows collection and return
     * a list of rows that are present in the newRows collection and also in the
     * oldRows collection but have one or more non-key field values that have
     * been updated.  Rows are evaluated based on the values of their key
     * columns, as supplied in the keys list.
     *
     * @param oldRows The 'old' collection of data rows.
     * @param newRows The 'new' collection of data rows.
     * @param keys The list of key column names to use for evaluation.
     * @return A list of data rows that are in the newRows collection but not in
     * the old rows collection.
     */
    public static final List<Map<String, Object>> getUpdatedRows(List<Map<String, Object>> oldRows, List<Map<String, Object>> newRows, String[] keys)
    {
        List<Map<String, Object>> updatedRows;

        updatedRows = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> newRow : newRows)
        {
            Map<String, Object> oldRow;

            oldRow = findDataRow(oldRows, getMapValues(newRow, keys));
            if ((oldRow != null) && (!oldRow.equals(newRow)))
            {
                updatedRows.add(newRow);
            }
        }

        return updatedRows;
    }

    /**
     * Removes the specified keys from each of the maps in the input collection.
     *
     * @param maps The input collection of maps.
     * @param keys The list of keys by which a value will be extracted from each input
     * map.
     */
    public static void removeKeys(Collection<? extends Map<String, ? extends Object>> maps, List<String> keys)
    {
        if (maps != null)
        {
            for (Map<String, ? extends Object> map : maps)
            {
                for (String key : keys)
                {
                    map.remove(key);
                }
            }
        }
    }

    /**
     * Ensures that all of the maps in the input collection have key mappings for the specified keys.
     * If one of the keys does not exist in a map, it is added to the map with a null value.
     * @param maps The maps to update.
     * @param keys The keys that will be checked for in all of the input maps.
     */
    public static void addMissingKeys(Collection<? extends Map<String, ? extends Object>> maps, List<String> keys)
    {
        if (maps != null)
        {
            for (Map<String, ? extends Object> map : maps)
            {
                Set<String> existingKeySet;

                existingKeySet = map.keySet();
                for (String key : keys)
                {
                    if (!existingKeySet.contains(key))
                    {
                        map.put(key, null);
                    }
                }
            }
        }
    }

    /**
     * Extracts the specified key value from each of the maps in the input
     * collection, adds it to a list and returns the list.
     *
     * @param maps The input collection of maps.
     * @param keyName The key by which a value will be extracted from each input
     * map.
     * @return A list of all extracted values.
     */
    public static ArrayList getKeyValues(ArrayList<HashMap<String, Object>> maps, String keyName)
    {
        ArrayList<Object> list;

        list = new ArrayList<Object>();
        if (maps != null)
        {
            for (HashMap<String, Object> dataRow : maps)
            {
                list.add(dataRow.get(keyName));
            }
        }

        return list;
    }

    /**
     * This method checks collection A for the existence of any of the items in
     * collection B.  If one of the items in collection B is found in collection
     * A, this method returns true.
     * @param colA The first collection to be checked.
     * @param colB The collection from which objects will be fetched and checked
     * for in collection A.
     * @return true if one of the items in collection B is found in collection
     * A, this method returns true.
     */
    public static boolean containsAny(Collection colA, Collection colB)
    {
        for (Object object : colB)
        {
            if (colA.contains(object)) return true;
        }

        return false;
    }

    /**
     * Returns the size of the intersection between collection A and B i.e. the
     * number of objects that are in both collections.  This method uses the
     * standard Collection iterator to iterate over objects in collection A and
     * then uses Collection.contains() method to determine if the object exists
     * in collection B.
     * @param collectionA The first collection to evaluate.
     * @param collectionB The second collection to evaluate.
     * @return The number of objects that are in both collections.
     */
    public static int countIntersection(Collection collectionA, Collection collectionB)
    {
        int count;

        count = 0;
        if ((collectionA != null) && (collectionB != null))
        {
            for (Object objectA : collectionA)
            {
                if (collectionB.contains(objectA)) count++;
            }
        }

        return count;
    }

    /**
     * Returns the sum of the sizes of all input collections.  This method is
     * null-safe in every way i.e. null collections in as input parameters will simply
     * be disregarded.
     * @param collections The collections of which sizes will be added.
     * @return The total sum of the sizes of all input collections.
     */
    public static int sumSize(Collection... collections)
    {
        if (collections != null)
        {
            int size;

            size = 0;
            for (Collection collection : collections)
            {
                if (collection != null)
                {
                    size += collection.size();
                }
            }

            return size;
        }
        else return 0;
    }

    /**
     * A null-safe method for checking whether or not a collection has content
     * elements.
     * @param collection The collection to check.
     * @return boolean false if the supplied collection is null or has no
     * content elements.
     */
    public static boolean hasContent(Collection collection)
    {
        if (collection == null) return false;
        else return collection.size() > 0;
    }

    /**
     * A null-safe method for checking whether or not a collection is empty or
     * null.
     * @param collection The collection to check.
     * @return boolean true if the supplied collection is null or has no
     * content elements.
     */
    public static boolean isNullOrEmpty(Collection collection)
    {
        if (collection == null) return true;
        else return collection.isEmpty();
    }

    public static void sortMaps(List<Map<String, Object>> maps, boolean ascending, String... sortColumnNames)
    {
        Collections.sort(maps, new MapComparator(Arrays.asList(sortColumnNames), ascending));
    }

    private static class MapComparator implements Comparator
    {
        private List<String> keyNames;
        private boolean ascending;

        public MapComparator(List<String> keyNames, boolean ascending)
        {
            this.keyNames = keyNames;
            this.ascending = ascending;
        }

        @Override
        public int compare(Object o1, Object o2)
        {
            Map<String, Object> row1;
            Map<String, Object> row2;
            Iterator<String> keyNameIterator;

            row1 = (Map<String, Object>)o1;
            row2 = (Map<String, Object>)o2;
            keyNameIterator = keyNames.iterator();
            while (keyNameIterator.hasNext())
            {
                Comparable value1;
                Comparable value2;
                String keyName;

                keyName = keyNameIterator.next();
                value1 = (Comparable)row1.get(keyName);
                value2 = (Comparable)row2.get(keyName);
                if (value1 == null)
                {
                    if (value2 == null)
                    {
                        if (keyNameIterator.hasNext())
                        {
                            continue;
                        }
                        else return 0;
                    }
                    else
                    {
                        return ascending ? -1 : 1;
                    }
                }
                else
                {
                    if (value1.compareTo(value2) == 0)
                    {
                        if (keyNameIterator.hasNext())
                        {
                            continue;
                        }
                        else return 0;
                    }
                    else
                    {
                        return ascending ? value1.compareTo(value2) : (value1.compareTo(value2) * -1);
                    }
                }
            }

            // Execution should never reach this point.
            throw new RuntimeException("Comparison Failure.");
        }
    }
}
