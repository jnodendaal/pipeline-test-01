package com.pilog.t8.utilities.compression;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 *
 * @author Bouwer du Preez
 */
public class ByteCompressor
{
   /**
     * Compresses data.
     *
     * @param bytesToCompress A byte array that will be compressed.
     * @return The compressed byte array.
     * @throws java.io.IOException
     */
    public static final byte[] compressBytes(byte[] bytesToCompress) throws IOException
    {
        Deflater compressor = null;
        ByteArrayOutputStream bos = null;

        try
        {
            byte[] buffer;

            // Initialize the compressor with the highest level of compression.
            compressor = new Deflater(Deflater.BEST_COMPRESSION);
            compressor.setInput(bytesToCompress); // Give the compressor the data to compress.
            compressor.finish();

            // Create an expandable byte array to hold the compressed data.  Compressed data may be smaller than the uncompressed data.
            bos = new ByteArrayOutputStream(bytesToCompress.length);

            // Compress the data.
            buffer = new byte[bytesToCompress.length + 100];
            while (!compressor.finished())
            {
                bos.write(buffer, 0, compressor.deflate(buffer));
            }

            // Close the output stream.
            bos.close();

            // Return the compressed data.
            return bos.toByteArray();
        }
        finally
        {
            if (compressor != null) compressor.end();
            if (bos != null) bos.close();
        }
    }

    /**
     * Decompresses data.
     *
     * @param compressedBytes A byte array of compressed data.
     * @return The decompressed byte array.
     * @throws java.io.IOException
     * @throws java.util.zip.DataFormatException
     */
    public static final byte[] decompressBytes(byte[] compressedBytes) throws IOException, DataFormatException
    {
        Inflater decompressor = null;
        ByteArrayOutputStream bos = null;

        try
        {
            byte[] buffer;

            // Initialize decompressor.
            decompressor = new Inflater();
            decompressor.setInput(compressedBytes);  // Give the decompressor the data to decompress.
            decompressor.finished();

            // Create an expandable byte array to hold the decompressed data.  Decompressed data may be larger than the compressed data.
            bos = new ByteArrayOutputStream(compressedBytes.length);

            // Decompress the data.
            buffer = new byte[compressedBytes.length + 100];
            while (!decompressor.finished())
            {
                bos.write(buffer, 0, decompressor.inflate(buffer));
            }

            // Close the output stream.
            bos.close();

            // Get the decompressed data.
            return bos.toByteArray();
        }
        finally
        {
            if (decompressor != null) decompressor.end();
            if (bos != null) bos.close();
        }
    }
}
