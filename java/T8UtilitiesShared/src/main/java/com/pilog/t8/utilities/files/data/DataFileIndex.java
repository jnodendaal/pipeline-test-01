package com.pilog.t8.utilities.files.data;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * @author Bouwer du Preez
 */
public class DataFileIndex implements Serializable
{
    private HashMap<String, ArrayList<String>> tableColumns;
    private HashMap<String, ArrayList<TableDataLocation>> tableDataIndex;

    public DataFileIndex()
    {
        tableColumns = new HashMap<String, ArrayList<String>>();
        tableDataIndex = new HashMap<String, ArrayList<TableDataLocation>>();
    }

    public int getTableRecordCount(String tableName)
    {
        if (tableDataIndex.containsKey(tableName))
        {
            return tableDataIndex.get(tableName).size();
        }
        else
        {
            return -1;
        }
    }

    public TableDataLocation getTableDataLocation(String tableName, int recordNumber)
    {
        ArrayList<TableDataLocation> tableIndex;

        tableIndex = tableDataIndex.get(tableName);
        return recordNumber < tableIndex.size() ? tableIndex.get(recordNumber) : null;
    }

    public void addTableDataLocation(String tableName, long offset, int size)
    {
        tableDataIndex.get(tableName).add(new TableDataLocation(offset, size));
    }

    public void clear()
    {
        tableColumns.clear();
        tableDataIndex.clear();
    }

    public void addTable(String tableName, ArrayList<String> columnNames)
    {
        for (String columnName : columnNames)
        {
            addTableColumn(tableName, columnName);
        }
    }

    public void addTableColumn(String tableName, String columnName)
    {
        ArrayList<String> columns;

        if (tableColumns.containsKey(tableName))
        {
            columns = tableColumns.get(tableName);
        }
        else
        {
            ArrayList<TableDataLocation> locationList;

            // Initialize the list that will contain the table data index.
            locationList = new ArrayList<TableDataLocation>();
            tableDataIndex.put(tableName, locationList);

            // Initialize the list that will contain the table column names.
            columns = new ArrayList<String>();
            tableColumns.put(tableName, columns);
        }

        if (!columns.contains(columnName))
        {
            columns.add(columnName);
        }
    }

    public ArrayList<String> getTableList()
    {
        ArrayList<String> tableList;
        Iterator<String> hashIterator;

        hashIterator = tableColumns.keySet().iterator();
        tableList = new ArrayList<String>();
        while (hashIterator.hasNext())
        {
            tableList.add(hashIterator.next());
        }

        return tableList;
    }

    public ArrayList<String> getTableColumnList(String tableName)
    {
        return tableColumns.get(tableName);
    }

    public boolean containsTable(String tableName)
    {
        return tableColumns.containsKey(tableName);
    }

    public boolean containsColumn(String columnName)
    {
        Iterator<String> hashIterator;

        hashIterator = tableColumns.keySet().iterator();
        while (hashIterator.hasNext())
        {
            if (tableColumns.get(hashIterator.next()).contains(columnName))
            {
                return true;
            }
        }

        return false;
    }

    public boolean containsTableColumn(String tableName, String columnName)
    {
        Iterator<String> hashIterator;

        hashIterator = tableColumns.keySet().iterator();
        while (hashIterator.hasNext())
        {
            String table;

            table = hashIterator.next();
            if (table.equalsIgnoreCase(tableName))
            {
                if (tableColumns.get(table).contains(columnName))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
