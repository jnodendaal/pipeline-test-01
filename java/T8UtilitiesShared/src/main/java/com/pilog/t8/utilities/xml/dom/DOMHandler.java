package com.pilog.t8.utilities.xml.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.XMLConstants;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * @author Bouwer du Preez
 */
public class DOMHandler
{
    /**
     * Returns the index of the specified DOM Element in its parent's collection
     * of child elements.  In determining this index only elements are taken
     * into account, ie. all other DOM node types are ignored.
     *
     * @param element The element for which to determine an index.
     * @return The element's index.
     */
    public static final int getElementIndex(Element element)
    {
        Node parentNode;

        parentNode = element.getParentNode();
        if (parentNode != null)
        {
            ArrayList<Element> childElements;

            childElements = getChildElements((Element)parentNode);
            return childElements.indexOf(element);
        }
        else
        {
            return 0;
        }
    }

    /**
     * Returns the next sibling element of the specified element or null if none
     * exists.
     *
     * @param element The element for which the next sibling will be determined.
     * @return The next DOM element.
     */
    public static final Element getNextElement(Element element)
    {
        Node parentNode;

        parentNode = element.getParentNode();
        if (parentNode != null)
        {
            ArrayList<Element> childElements;
            int index;

            childElements = getChildElements((Element)parentNode);
            index = childElements.indexOf(element);
            if (index < childElements.size()-1)
            {
                return childElements.get(index + 1);
            }
            else return null;
        }
        else
        {
            return null;
        }
    }

    /**
     * Returns the preceding sibling element to the specified element or null if
     * none exists.
     *
     * @param element The element for which to determine the preceding sibling.
     * @return The preceding sibling element.
     */
    public static final Element getPreviousElement(Element element)
    {
        Node parentNode;

        parentNode = element.getParentNode();
        if (parentNode != null)
        {
            ArrayList<Element> childElements;
            int index;

            childElements = getChildElements((Element)parentNode);
            index = childElements.indexOf(element);
            if (index > 0)
            {
                return childElements.get(index - 1);
            }
            else return null;
        }
        else
        {
            return null;
        }
    }

    public static final ArrayList<Element> getElementsByTagName(Document parentDocument, String namespace, String localName)
    {
        ArrayList<Element> elements;
        NodeList nodeList;

        elements = new ArrayList<Element>();
        nodeList = parentDocument.getElementsByTagNameNS(namespace, localName);
        for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++)
        {
            elements.add((Element)nodeList.item(nodeIndex));
        }

        return elements;
    }

    public static final ArrayList<Element> getElementsByTagName(Element parentElement, String namespace, String localName)
    {
        ArrayList<Element> elements;
        NodeList nodeList;

        elements = new ArrayList<Element>();
        nodeList = parentElement.getElementsByTagNameNS(namespace, localName);
        for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++)
        {
            elements.add((Element)nodeList.item(nodeIndex));
        }

        return elements;
    }

    /**
     * Returns a list of all the elements in the specified DOM Document.
     *
     * @param document The document from which to extract all elements.
     * @return A list of DOM Elements.
     */
    public static ArrayList<Element> getAllElements(Document document)
    {
        ArrayList<Element> elements;
        Element documentElement;

        elements = new ArrayList<Element>();
        documentElement = document.getDocumentElement();
        if (documentElement != null)
        {
            elements.addAll(getAllDescendentElements(documentElement));
        }
        
        return elements;
    }

    /**
     * Returns all of the attribute nodes of the specified element.
     *
     * @param element The DOM element for which attributes will be returned.
     * @return A list of all the attribute nodes of the specified element.
     */
    public static ArrayList<Attr> getAllAttributes(Element element)
    {
        ArrayList<Attr> nodeList;
        NamedNodeMap attributeMap;

        nodeList = new ArrayList<Attr>();
        attributeMap = element.getAttributes();
        for (int nodeIndex = 0; nodeIndex < attributeMap.getLength(); nodeIndex++)
        {
            nodeList.add((Attr)attributeMap.item(nodeIndex));
        }

        return nodeList;
    }

    /**
     * Returns all of the descendent elements of the specified parent.
     *
     * @param parentElement
     * @return A list of DOM Elements.
     */
    public static ArrayList<Element> getAllDescendentElements(Element parentElement)
    {
        ArrayList<Element> childElements;
        NodeList childNodes;

        childElements = new ArrayList<Element>();
        childNodes = parentElement.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                Element childElement;

                childElement = (Element)childNode;
                childElements.add(childElement);
                childElements.addAll(getAllDescendentElements(childElement));
            }
        }

        return childElements;
    }

    /**
     * Returns all of the descendent elements of the specified parent that has
     * the specified local name.
     *
     * @param parentElement The parent element from which descendents will be
     * retrieved.
     * @param localName The local name of the elements to retrieve.
     * @return A list of DOM Elements.
     */
    public static ArrayList<Element> getAllDescendentElements(Element parentElement, String localName)
    {
        ArrayList<Element> childElements;
        NodeList childNodes;

        childElements = new ArrayList<Element>();
        childNodes = parentElement.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                Element childElement;

                childElement = (Element)childNode;
                if (childElement.getLocalName().equals(localName)) childElements.add(childElement);
                childElements.addAll(getAllDescendentElements(childElement, localName));
            }
        }

        return childElements;
    }

    /**
     * Returns the specified Element's first child element, if any exists.
     * 
     * @param parentElement The parent element for which to return the first
     * child element.
     * @return The first child element of the specified element.  Null if no
     * child elements were found.
     */
    public static Element getFirstChildElement(Element parentElement)
    {
        ArrayList<Element> childElements;

        childElements = getChildElements(parentElement);
        return childElements.size() > 0 ? childElements.get(0) : null;
    }

    /**
     * Returns the first child element with the specified local name from the
     * specified parent element.
     *
     * @param parentElement The element from which the child element will be
     * retrieved.
     * @param childLocalName The child local name to search for.
     * @return The child element fitting the specified local name or null if no
     * element is found.
     */
    public static Element getChildElement(Element parentElement, String childLocalName)
    {
        NodeList childNodes;

        childNodes = parentElement.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                Element childElement;

                childElement = (Element)childNode;
                if (childElement.getLocalName().equalsIgnoreCase(childLocalName)) return childElement;
            }
        }

        return null;
    }

    /**
     * Returns all the child elements with the specified local name from the
     * specified parent element.
     *
     * @param parentElement The element from which the child elements will be
     * retrieved.
     * @param childLocalName The child local name to search for.
     * @return A list of child elements fitting the specified local name.
     */
    public static ArrayList<Element> getChildElements(Element parentElement, String childLocalName)
    {
        ArrayList<Element> matchingElements;
        NodeList childNodes;

        matchingElements = new ArrayList<Element>();
        childNodes = parentElement.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                Element childElement;

                childElement = (Element)childNode;
                if (childElement.getLocalName().equalsIgnoreCase(childLocalName))
                {
                    matchingElements.add(childElement);
                }
            }
        }

        return matchingElements;
    }

    /**
     * Returns all the child elements with the specified local name from the
     * specified parent element.
     *
     * @param parentElement The element from which the child elements will be
     * retrieved.
     * @param childLocalName The child local name to search for.
     * @param attributeFilterValues A HashMap of (attributeName, attributeValue)
     * that will be used to filter all elements found.
     * @return A list of child elements fitting the specified local name.
     */
    public static ArrayList<Element> getChildElements(Element parentElement, String childLocalName, HashMap<String, String> attributeFilterValues)
    {
        ArrayList<Element> childElements;

        childElements = getChildElements(parentElement);
        return filterElements(childElements, childLocalName, attributeFilterValues);
    }

    /**
     * Returns a list of all the elements from the supplied list that have the
     * specified local name and all of the specified attribute values.
     *
     * @param elements The list of elements to filter.
     * @param childLocalName The local name to use as a filter.
     * @param attributeFilterValues The attribute values to use as a filter.
     * @return The list of elements matching both the specified local name and
     * set of attribute values.
     */
    public static ArrayList<Element> filterElements(ArrayList<Element> elements, String childLocalName, HashMap<String, String> attributeFilterValues)
    {
        ArrayList<Element> matchingElements;

        matchingElements = new ArrayList<Element>();
        for (Element element : elements)
        {
            // If the element's name matches the filter value, proceed to check its attributes.
            if (element.getLocalName().equalsIgnoreCase(childLocalName))
            {
                boolean matched;

                // Check the child element for each of the attribute values to filter by.
                matched = true;
                for (String filterAttribute : attributeFilterValues.keySet())
                {
                    // Make sure the child element has the required attribute
                    if (element.hasAttribute(filterAttribute))
                    {
                        Object filterValue;

                        filterValue = attributeFilterValues.get(filterAttribute);
                        if (filterValue == null)
                        {
                            if (element.getAttribute(filterAttribute) != null)
                            {
                                matched = false;
                                break;
                            }
                        }
                        else if (!filterValue.equals(element.getAttribute(filterAttribute)))
                        {
                            matched = false;
                            break;
                        }
                    }
                    else
                    {
                        matched = false;
                        break;
                    }
                }

                // All filter values checked out, so add the child element to the list of matches.
                if (matched) matchingElements.add(element);
            }
        }

        return matchingElements;
    }

    /**
     * Returns all the child elements of the specified parent element.
     *
     * @param parentElement The parent element for which to return the child
     * elements.
     * @return A list of DOM Elements.
     */
    public static ArrayList<Element> getChildElements(Element parentElement)
    {
        ArrayList<Element> childElements;
        NodeList childNodes;

        childElements = new ArrayList<Element>();
        childNodes = parentElement.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                Element childElement;

                childElement = (Element)childNode;
                childElements.add(childElement);
            }
        }

        return childElements;
    }

    /**
     * Returns the text value of the element.  This method concatenates all
     * text child nodes belonging to the specified element and returns the
     * result.
     *
     * @param element The element from which to extract the text value.
     * @return The concatenated text content of the specified element.
     */
    public static String getElementTextValue(Element element)
    {
        StringBuffer stringValue;
        NodeList childNodes;

        stringValue = new StringBuffer();
        childNodes = element.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;

            childNode = childNodes.item(childIndex);
            if (childNode.getNodeType() == Node.TEXT_NODE)
            {
                stringValue.append(childNode.getNodeValue());
            }
        }

        return stringValue.toString();
    }

    /**
     * Returns a boolean value indicating whether or not the specified element
     * is a leaf element.
     *
     * @param element The DOM Element to check.
     * @return true if the specified element is a leaf, false otherwise.
     */
    public static boolean isLeafElement(Element element)
    {
        return getAllDescendentElements(element).size() == 0;
    }

    /**
     * Returns a boolean value indicating whether or not the specified attribute
     * is a namespace attribute.
     *
     * @param attribute The attribute to check.
     * @return true if the specified attribute is a namespace declaration, false
     * otherwise.
     */
    public static final boolean isNamespaceAttribute(Attr attribute)
    {
        return ((XMLConstants.XMLNS_ATTRIBUTE_NS_URI.equals(attribute.getNamespaceURI())) || (attribute.getName().startsWith("xmlns")));
    }

    /**
     * Returns a list of all attribute nodes belonging to the specified element.
     *
     * @param element The element from which the attribute nodes will be
     * retrieved.
     * @return A list of attribute nodes.
     */
    public static final ArrayList<Attr> getAttributeNodes(Element element)
    {
        NamedNodeMap nodeMap;
        ArrayList<Attr> attributeList;

        attributeList = new ArrayList<Attr>();
        nodeMap = element.getAttributes();
        for (int attributeIndex = 0; attributeIndex < nodeMap.getLength(); attributeIndex++)
        {
            attributeList.add((Attr)nodeMap.item(attributeIndex));
        }

        return attributeList;
    }

    /**
     * Returns a list of all the attribute names contained by the specified DOM
     * Element.
     *
     * @param element The DOM Element from which the Attribute names will be
     * extracted.
     * @return A list of DOM Attribute names.
     */
    public static ArrayList<String> getAttributeNames(Element element)
    {
        NamedNodeMap nodeMap;
        ArrayList<String> attributeNames;

        nodeMap = element.getAttributes();
        attributeNames = new ArrayList<String>();
        for (int attributeIndex = 0; attributeIndex < nodeMap.getLength(); attributeIndex++)
        {
            attributeNames.add(nodeMap.item(attributeIndex).getLocalName());
        }

        return attributeNames;
    }

    /**
     * Returns a LinkedHashMap containing the attribute (name, value) pairs
     * extracted from the specified DOM Element.
     *
     * @param element The DOM Element from which to extract the attribute
     * values.
     * @return A LinkedHashMap containing (attribute name, attribute value)
     * pairs.
     */
    public static LinkedHashMap<String, String> getAttributeValues(Element element)
    {
        LinkedHashMap<String, String> attributeValues;
        NamedNodeMap nodeMap;

        attributeValues = new LinkedHashMap<String, String>();
        nodeMap = element.getAttributes();
        for (int attributeIndex = 0; attributeIndex < nodeMap.getLength(); attributeIndex++)
        {
            Node attributeNode;
            String name;

            attributeNode = nodeMap.item(attributeIndex);
            name = attributeNode.getLocalName();
            if (name == null) name = attributeNode.getNodeName();
            attributeValues.put(name, attributeNode.getNodeValue());
        }

        return attributeValues;
    }

    /**
     * Removes all child elements from the specified element.  Attributes and
     * other node types are not removed.
     *
     * @param element The element from which the child elements will be removed.
     */
    public static void removeAllChildElements(Element element)
    {
        ArrayList<Element> childElements;

        childElements = getChildElements(element);
        for (Element childElement : childElements)
        {
            element.removeChild(childElement);
        }
    }

    /**
     * Returns all namespace URIs gathered from the specified element and its
     * descendents.
     *
     * @param element The element from which the namespace URIs will be
     * retrieved (including its descendents).
     * @return A set of all namespace URIs found.
     */
    public static final HashSet<String> getAllNamespaceURIs(Element element)
    {
        HashSet<String> namespaces;
        LinkedList<Element> elementQueue;

        namespaces = new HashSet<String>();
        elementQueue = new LinkedList<Element>();
        elementQueue.add(element);
        while (elementQueue.size() > 0)
        {
            Element nextElement;

            nextElement = elementQueue.pop();
            namespaces.add(nextElement.getNamespaceURI());
            elementQueue.addAll(getChildElements(nextElement));
        }

        return namespaces;
    }

    /**
     * Returns a map of all namespace prefixes and their corresponding URI's
     * used by the specified element and its descendents.
     *
     * @param element The element from which the namespaces prefixes will by
     * retrieved (including its descendents).
     * @return A map of all non-null namespaces prefixes and their corresponding
     * URIs.
     */
    public static final HashMap<String, String> getAllNamespacePrefixes(Element element)
    {
        HashMap<String, String> namespaces;
        LinkedList<Element> elementQueue;

        namespaces = new HashMap<String, String>();
        elementQueue = new LinkedList<Element>();
        elementQueue.add(element);
        while (elementQueue.size() > 0)
        {
            Element nextElement;
            String prefix;

            nextElement = elementQueue.pop();
            prefix = nextElement.getPrefix();
            if (prefix != null)
            {
                namespaces.put(prefix, nextElement.getNamespaceURI());
            }

            elementQueue.addAll(getChildElements(nextElement));
        }

        return namespaces;
    }

    /**
     * Removes all namespace declarations from the specified element.
     *
     * @param element The element from which to remove the namespace
     * declarations.
     */
    public static final void removeNamespaceAttributes(Element element)
    {
        ArrayList<Attr> attributeNodes;

        attributeNodes = getAttributeNodes(element);
        for (Attr attributeNode : attributeNodes)
        {
            if (attributeNode != null)
            {
                if (isNamespaceAttribute(attributeNode))
                {
                    element.removeAttributeNode(attributeNode);
                }
            }
        }
    }

    /**
     * Removes all attribute from the specified element.
     *
     * @param element The element from which to remove the attributes.
     */
    public static final void removeAllAttributes(Element element)
    {
        ArrayList<Attr> attributeNodes;

        attributeNodes = getAttributeNodes(element);
        for (Attr attributeNode : attributeNodes)
        {
            element.removeAttributeNode(attributeNode);
        }
    }

    /**
     * Returns a map of all namespace attribute names and values.
     *
     * @param element The element from which to extract the attribute values.
     * @return A map of all namespace attribute names and values.
     */
    public static final HashMap<String, String> getNamespaceAttributeValues(Element element)
    {
        HashMap<String, String> attributeValues;
        NamedNodeMap attributeNodes;

        attributeValues = new HashMap<String, String>();
        attributeNodes = element.getAttributes();
        for (int nodeIndex = 0; nodeIndex < attributeNodes.getLength(); nodeIndex++)
        {
            Attr attributeNode;

            attributeNode = (Attr)attributeNodes.item(nodeIndex);
            if (isNamespaceAttribute(attributeNode))
            {
                attributeValues.put(attributeNode.getName(), attributeNode.getValue());
            }
        }

        return attributeValues;
    }

    /**
     * Normalizes the namespace declarations in the document.  All declarations
     * are moved to the document element and standard prefixes are used (ns1,
     * ns2, ns3..).
     *
     * @param document The document to normalize.
     */
    public static final void normalizeNamespaces(Document document)
    {
        HashMap<String, String> declaredNamespaces;
        HashMap<String, String> namespaceAttributeValues;
        LinkedList<Element> elementQueue;
        Element documentElement;

        System.out.println("Normalizing namespaces.");
        
        // Get the document element and create a map of all declared namespaces.
        documentElement = document.getDocumentElement();
        declaredNamespaces = new HashMap<String, String>();
        namespaceAttributeValues = getNamespaceAttributeValues(documentElement);
        for (String namespaceAttributeName : namespaceAttributeValues.keySet())
        {
            String prefix;

            prefix = namespaceAttributeName.indexOf(":") > -1 ? namespaceAttributeName.substring(namespaceAttributeName.indexOf(":") + 1) : null;
            declaredNamespaces.put(namespaceAttributeValues.get(namespaceAttributeName), prefix);
        }

        elementQueue = new LinkedList<Element>();
        elementQueue.addAll(getChildElements(documentElement));
        while (elementQueue.size() > 0)
        {
            Element nextElement;
            String namespaceURI;

            nextElement = elementQueue.pop();
            namespaceURI = nextElement.getNamespaceURI();

            // Remove the namespace attribute from the element.
            removeNamespaceAttributes(nextElement);

            // If the namespace has not been declared on the document element, declare it now.
            if (declaredNamespaces.containsKey(namespaceURI))
            {
                nextElement.setPrefix(declaredNamespaces.get(namespaceURI));
            }
            else
            {
                String prefix;

                prefix = nextElement.getPrefix();
                if (prefix == null)
                {
                    int nsIndex;

                    nsIndex = 1;
                    while (documentElement.hasAttribute(("xmlns:ns" + nsIndex))) nsIndex++;
                    prefix = "ns" + nsIndex;
                }

                documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:" + prefix, namespaceURI);
                nextElement.setPrefix(prefix);
                declaredNamespaces.put(namespaceURI, prefix);
            }

            // Add all chid elements to the queueu.
            elementQueue.addAll(getChildElements(nextElement));
        }
    }

    /**
     * Removes all current text nodes of the specified element and then adds a
     * single new text node containing the specified text value.
     *
     * @param element The element on which the text value will be set.
     * @param textContent The text value that will be set on the specified
     * element.
     */
    public static final void setElementTextContent(Element element, String textContent)
    {
        DOMHandler.removeChildNodes(element, Node.TEXT_NODE);
        if (textContent != null)
        {
            element.appendChild(element.getOwnerDocument().createTextNode(textContent));
        }
    }

    /**
     * Removes all child nodes of the specified type from the specified parent
     * node.
     *
     * @param parentNode The parent node from which the child nodes will be
     * removed.
     * @param childNodeType The type of child nodes to removed.  This type is
     * defined as constant short values in the class org.w3c.dom.Node.
     */
    public static final void removeChildNodes(Node parentNode, short childNodeType)
    {
        ArrayList<Node> nodesToRemove;

        nodesToRemove = DOMHandler.getChildNodes(parentNode, childNodeType);
        DOMHandler.removeChildNodes(parentNode, nodesToRemove);
    }

    /**
     * Removes each Node in the supplied list from the specified parent node.
     *
     * @param parentNode The parent node from which the child nodes will be
     * removed.
     * @param childNodes The list of child nodes that will be removed from the
     * specified parent node.
     */
    public static final void removeChildNodes(Node parentNode, List<Node> childNodes)
    {
        for (Node childNode : childNodes)
        {
            parentNode.removeChild(childNode);
        }
    }

    /**
     * Returns a list of all the child nodes of the specified parent node that
     * are of the specified not type.  The node type is defined as constant
     * short values in the class org.w3c.dom.Node.
     *
     * @param parentNode The parent node for which a list of child nodes will be
     * returned.
     * @param childNodeType The type of child nodes to include in the list.
     * @return A list of Node objects.
     */
    public static final ArrayList<Node> getChildNodes(Node parentNode, short childNodeType)
    {
        ArrayList<Node> childNodes;
        ArrayList<Node> selectedNodes;

        selectedNodes = new ArrayList<Node>();
        childNodes = DOMHandler.getChildNodes(parentNode);
        for (Node childNode : childNodes)
        {
            if (childNode.getNodeType() == childNodeType) selectedNodes.add(childNode);
        }

        return selectedNodes;
    }

    /**
     * Returns a list of all the child nodes belonging to the specified parent
     * node.
     *
     * @param node The parent node for which all child nodes will be returned.
     * @return A list of Node objects.
     */
    public static final ArrayList<Node> getChildNodes(Node node)
    {
        NodeList childNodes;
        ArrayList<Node> childNodeList;

        childNodeList = new ArrayList<Node>();
        childNodes = node.getChildNodes();
        for (int childNodeIndex = 0; childNodeIndex < childNodes.getLength(); childNodeIndex++)
        {
            childNodeList.add(childNodes.item(childNodeIndex));
        }

        return childNodeList;
    }

    /**
     * This method will traverse all of the descendents of the supplied node and
     * will remove all empty text nodes.  Empty text nodes are defined as nodes
     * with text content that contains only whitespace or has a character count
     * of 0.
     *
     * @param node The node from which all descendent empty text nodes will be
     * removed.
     */
    public static void trimEmptyTextNodes(Node node)
    {
        List<Node> nodesToRemove;
        NodeList children;
        Element element;

        // If the input Element is a Document, get its document Element.
        element = null;
        if (node instanceof Document)
        {
            element = ((Document)node).getDocumentElement();
        }
        else if (node instanceof Element)
        {
            element = (Element) node;
        }
        else
        {
            return;
        }

        // Create a list of all direct child nodes that are empty text nodes.
        nodesToRemove = new ArrayList<Node>();
        children = element.getChildNodes();
        for (int childIndex = 0; childIndex < children.getLength(); childIndex++)
        {
            Node childNode;

            childNode = children.item(childIndex);
            if (childNode instanceof Element)
            {
                trimEmptyTextNodes(childNode);
            }
            else if (childNode instanceof Text)
            {
                Text textNode;

                textNode = (Text)childNode;
                if (textNode.getData().trim().length() == 0)
                {
                    nodesToRemove.add(childNode);
                }
            }
        }

        // Remove the list of empty text nodes from the input node.
        removeChildNodes(element, nodesToRemove);
    }

    /**
     * This method will traverse all of the descendents of the supplied node and
     * will remove all comment nodes.
     *
     * @param node The node from which all descendent comment nodes will be
     * removed.
     */
    public static void removeAllCommentNodes(Node node)
    {
        List<Node> nodesToRemove;
        NodeList children;
        Element element;

        // If the input Element is a Document, get its document Element.
        element = null;
        if (node instanceof Document)
        {
            element = ((Document)node).getDocumentElement();
        }
        else if (node instanceof Element)
        {
            element = (Element) node;
        }
        else
        {
            return;
        }

        // Create a list of all direct child nodes that are empty text nodes.
        nodesToRemove = new ArrayList<Node>();
        children = element.getChildNodes();
        for (int childIndex = 0; childIndex < children.getLength(); childIndex++)
        {
            Node childNode;

            childNode = children.item(childIndex);
            if (childNode instanceof Element)
            {
                removeAllCommentNodes(childNode);
            }
            else if (childNode instanceof Comment)
            {
                nodesToRemove.add(childNode);
            }
        }

        // Remove the list of empty text nodes from the input node.
        removeChildNodes(element, nodesToRemove);
    }

    /**
     * This method counts the number of non-namespace attributes in the supplied
     * node map.
     *
     * @param attributes The attribute map to evaluate.
     * @return The number of non-namespace attributes in the supplied node map.
     */
    public static int countNonNamespaceAttribures(NamedNodeMap attributes)
    {
        int count;

        count = 0;
        for (int attributeIndex = 0; attributeIndex < attributes.getLength(); attributeIndex++)
        {
            Attr attribute;

            attribute = (Attr)attributes.item(attributeIndex);
            if (!attribute.getName().startsWith("xmlns"))
            {
                count++;
            }
        }

        return count;
    }

    /**
     * Returns the level of the specified element in its parent DOM.  The level
     * of an element is defined as the number of hierarchical child nodes that
     * have to be traversed from the root node of the tree in order to reach
     * the specified element.  The root node is therefore at level 0.
     *
     * @param element The level of the specified element.
     * @return The level of the specified element (root is at level 0).
     */
    public static int getElementLevel(Element element)
    {
        Node currentElement;
        int level;

        level = 0;
        currentElement = element;
        while ((currentElement = currentElement.getParentNode()) != null)
        {
            level++;
        }

        return level;
    }
}
