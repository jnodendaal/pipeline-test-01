package com.pilog.t8.utilities.files.data;

import com.pilog.t8.utilities.files.RandomAccessFileStream;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import java.io.File;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Bouwer du Preez
 */
public class DataFileHandler
{
    private boolean dataAdded;
    private RandomAccessStream dataStream;
    private SecretKeySpec secretKey;
    private DataFileIndex index;
    private Cipher encryptCipher;
    private Cipher decryptCipher;

    public DataFileHandler()
    {
        dataStream = null;
        index = new DataFileIndex();
    }

    public void addTableColumn(String tableName, String columnName) throws Exception
    {
        index.addTableColumn(tableName, columnName);
    }

    public void addTable(String tableName, ArrayList<String> columnNames) throws Exception
    {
        index.addTable(tableName, columnNames);
    }

    public int getTableRecordCount(String tableName)
    {
        return index.getTableRecordCount(tableName);
    }

    public boolean containsTable(String tableName)
    {
        return index.containsTable(tableName);
    }

    public boolean openFile(File file) throws Exception
    {
        // Check that an encryption key has been set.
        if (secretKey == null) throw new Exception("No encryption key set.");
        
        if (file.exists())
        {
            // Read the data file index from the opened data file.
            dataStream = new RandomAccessFileStream(file);
            readIndex();
            return true;
        }
        else
        {
            // Write a Long value to the start of the new file, to reserve a location where the data file index offset will later be stored.
            dataStream = new RandomAccessFileStream(file);
            dataStream.seek(0);
            dataStream.writeLong(100);
            return true;
        }
    }
    
    public boolean openFile(RandomAccessStream stream, boolean newFile) throws Exception
    {
        // Check that an encryption key has been set.
        if (secretKey == null) throw new Exception("No encryption key set.");

        if (!newFile)
        {
            // Read the data file index from the opened data file.
            dataStream = stream;
            readIndex();
            return true;
        }
        else
        {
            // Write a Long value to the start of the new file, to reserve a location where the data file index offset will later be stored.
            dataStream = stream;
            dataStream.seek(0);
            dataStream.writeLong(100);
            return true;
        }
    }

    private void readIndex() throws Exception
    {
        long dataFileIndexOffset;
        int dataFileIndexSize;
        byte[] dataBytes;

        dataStream.seek(0);
        dataFileIndexOffset = dataStream.readLong();
        dataStream.seek(dataFileIndexOffset);

        dataFileIndexSize = (int)(dataStream.length() - dataFileIndexOffset);
        dataBytes = new byte[dataFileIndexSize];
        dataStream.read(dataBytes);
        dataBytes = decryptBytes(dataBytes);
        
        dataStream.seek(dataFileIndexOffset); // Return to the position where the index starts, so that any addition of data rows will occur from there.

        index = (DataFileIndex)SerializationUtilities.deserializeObject(dataBytes);
    }

    private void writeIndex() throws Exception
    {
        long dataFileIndexOffset;
        byte[] dataBytes;

        // Write the data file index to the data file and record its location.
        dataFileIndexOffset = dataStream.getDataPointer();
        dataBytes = SerializationUtilities.serializeObject(index);
        dataBytes = encryptBytes(dataBytes);
        dataStream.write(dataBytes);

        // Write the offset of the dataFileDefintion at the start of the file.
        dataStream.seek(0);
        dataStream.writeLong(dataFileIndexOffset);
    }

    public void writeDataRow(String tableName, HashMap<String, Object> dataRow) throws Exception
    {
        byte[] dataBytes;
        long startOffset;
        int dataSize;

        dataBytes = SerializationUtilities.serializeObject(createValueList(tableName, dataRow));
        dataBytes = encryptBytes(dataBytes);
        startOffset = dataStream.getDataPointer();
        dataSize = dataBytes.length;
        dataStream.write(dataBytes);
        dataAdded = true;

        index.addTableDataLocation(tableName, startOffset, dataSize);
    }

    public HashMap<String, Object> readDataRow(String tableName, int recordNumber) throws Exception
    {
        TableDataLocation dataLocation;

        dataLocation = index.getTableDataLocation(tableName, recordNumber);
        if (dataLocation != null)
        {
            ArrayList<Object> valueList;
            byte[] dataBytes;

            dataStream.seek(dataLocation.offset);
            dataBytes = new byte[dataLocation.size];
            dataStream.read(dataBytes);
            dataBytes = decryptBytes(dataBytes);

            valueList = (ArrayList<Object>)SerializationUtilities.deserializeObject(dataBytes);
            return createDataRow(tableName, valueList);
        }
        else return null;
    }

    public void closeFile() throws Exception
    {
        // If data was added to the file, write the updated file index to the file.
        if (dataAdded)
        {
            writeIndex();
        }

        dataStream.close();
        index = null;
    }

    public boolean isFileOpen()
    {
        return dataStream != null;
    }

    public ArrayList<String> getTableNames()
    {
        return index.getTableList();
    }

    public ArrayList<String> getTableColumnNames(String tableName)
    {
        return index.getTableColumnList(tableName);
    }

    public DataFileIndex getIndex()
    {
        return index;
    }
    
    public void setSecretKey(byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
        secretKey = new SecretKeySpec(keyBytes, "AES");

        encryptCipher = Cipher.getInstance("AES");
        encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);

        decryptCipher = Cipher.getInstance("AES");
        decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
    }

    private byte[] encryptBytes(byte[] unencryptedBytes) throws IllegalBlockSizeException, BadPaddingException
    {
        return encryptCipher.doFinal(unencryptedBytes);
    }

    private byte[] decryptBytes(byte[] encryptedBytes) throws IllegalBlockSizeException, BadPaddingException
    {
        return decryptCipher.doFinal(encryptedBytes);
    }

    private ArrayList<Object> createValueList(String tableName, HashMap<String, Object> dataRow)
    {
        ArrayList<Object> valueList;
        ArrayList<String> columnList;

        valueList = new ArrayList<Object>();
        columnList = index.getTableColumnList(tableName);
        for (String column : columnList)
        {
            valueList.add(dataRow.get(column));
        }

        return valueList;
    }

    public HashMap<String, Object> createDataRow(String tableName, ArrayList<Object> valueList)
    {
        HashMap<String, Object> dataRow;
        ArrayList<String> columnList;

        dataRow = new HashMap<String, Object>();
        columnList = index.getTableColumnList(tableName);
        for (int columnIndex = 0; columnIndex < columnList.size(); columnIndex++)
        {
            dataRow.put(columnList.get(columnIndex), valueList.get(columnIndex));
        }

        return dataRow;
    }
}
