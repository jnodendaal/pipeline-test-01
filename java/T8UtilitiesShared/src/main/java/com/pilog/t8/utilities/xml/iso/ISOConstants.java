package com.pilog.t8.utilities.xml.iso;

/**
 * @author Bouwer du Preez
 */
public class ISOConstants
{
    // Schema DB GUIDs.
    public static final String R_XML_SCHEMA_GUID = "B9B9A98AFFDF4D9C91265392D24C24FA";

    // ISO Namespaces:
    public static final String RENDERING_GUIDE_NAMESPACE = "urn:x-eotd:xml-schema:rendering-guide";
    public static final String IDENTIFIER_NAMESPACE = "urn:iso:std:iso:ts:29002:-5:ed-1:tech:xml-schema:identifier";
    public static final String VALUE_NAMESPACE = "urn:iso:std:iso:ts:29002:-10:ed-1:tech:xml-schema:value";
    public static final String DATA_TYPE_NAMESPACE = "urn:iso:std:iso:ts:22745:-30:ed-1:tech:xml-schema:data-type";
    public static final String CATALOGUE_NAMESPACE = "urn:iso:std:iso:ts:29002:-10:ed-1:tech:xml-schema:catalogue";
    public static final String BASIC_NAMESPACE = "urn:iso:std:iso:ts:29002:-4:ed-1:tech:xml-schema:basic";
    public static final String IDENTIFICATION_GUIDE_NAMESPACE = "urn:iso:std:iso:ts:22745:-30:ed-1:tech:xml-schema:identification-guide";
    public static final String QUERY_NAMESPACE = "urn:iso:std:iso:ts:29002:-31:ed-1:tech:xml-schema:query";

    // Default namespace prefixes.
    public static final String VALUE_NAMESPACE_PREFIX = "val";
    public static final String CATALOGUE_NAMESPACE_PREFIX = "cat";
    public static final String BASIC_NAMESPACE_PREFIX = "bas";
    public static final String DATA_TYPE_NAMESPACE_PREFIX = "dt";

// Data Types as defined in the i-XML schema:
//    	<xs:element name="bag_type" type="dt:bag_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="boolean_type" type="dt:boolean_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="choice_type" type="dt:choice_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="composite_type" type="dt:composite_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="controlled_value_type" type="dt:controlled_value_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="currency_type" type="dt:currency_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="datatype" type="dt:datatype_Type" abstract="true"/>
//	<xs:element name="date_type" type="dt:date_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="date_time_type" type="dt:date_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="field" type="dt:field_specification_type_Type"/>
//	<xs:element name="file_type" type="dt:file_type_Type"/>
//	<xs:element name="integer_format" type="dt:integer_format_Type"/>
//	<xs:element name="integer_type" type="dt:integer_type_Type" substitutionGroup="dt:numeric_type"/>
//	<xs:element name="item_reference_type" type="dt:item_reference_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="localized_text_type" type="dt:localized_text_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="measure_type" type="dt:measure_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="measure_number_type" type="dt:measure_number_type_Type" substitutionGroup="dt:measure_type"/>
//	<xs:element name="measure_range_type" type="dt:measure_range_type_Type" substitutionGroup="dt:measure_type"/>
//	<xs:element name="numeric_type" type="dt:numeric_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="prescribed_currency" type="dt:prescribed_currency_Type"/>
//	<xs:element name="prescribed_qualifier_of_measure" type="dt:prescribed_qualifier_of_measure_Type"/>
//	<xs:element name="prescribed_unit_of_measure" type="dt:prescribed_unit_of_measure_Type"/>
//	<xs:element name="rational_type" type="dt:rational_type_Type" substitutionGroup="dt:numeric_type"/>
//	<xs:element name="real_format" type="dt:real_format_Type"/>
//	<xs:element name="real_type" type="dt:real_type_Type" substitutionGroup="dt:numeric_type"/>
//	<xs:element name="sequence_type" type="dt:sequence_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="set_type" type="dt:set_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="string_format" type="dt:string_format_Type"/>
//	<xs:element name="string_type" type="dt:string_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="time_type" type="dt:time_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="value_of_property" type="dt:value_of_property_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="year_type" type="dt:year_type_Type" substitutionGroup="dt:datatype"/>
//	<xs:element name="year_month_type" type="dt:year_month_type_Type" substitutionGroup="dt:datatype"/>

    public static final int BAG_TYPE = 1;
    public static final int BOOLEAN_TYPE = 2;
    public static final int CHOICE_TYPE = 3;
    public static final int COMPOSITE_TYPE = 4;
    public static final int CONTROLLED_VALUE_TYPE = 5;
    public static final int CURRENCY_TYPE = 6;
    public static final int DATATYPE_TYPE = 7;
    public static final int DATE_TYPE = 8;
    public static final int DATE_TIME_TYPE = 9;
    public static final int FILE_TYPE = 10;
    public static final int INTEGER_TYPE = 11;
    public static final int ITEM_REFERENCE_TYPE = 12;
    public static final int LOCALIZED_TEXT_TYPE = 13;
    public static final int MEASURE_TYPE = 14;
    public static final int MEASURE_NUMBER_TYPE = 15;
    public static final int MEASURE_RANGE_TYPE = 16;
    public static final int NUMERIC_TYPE = 17;
    public static final int RATIONAL_TYPE = 18;
    public static final int REAL_TYPE = 19;
    public static final int SEQUENCE_TYPE = 20;
    public static final int SET_TYPE = 21;
    public static final int STRING_TYPE = 22;
    public static final int TIME_TYPE = 23;
    public static final int YEAR_TYPE = 24;
    public static final int YEAR_MONTH_TYPE = 25;

// Values as defined in the r-XML schema:
//	<xs:element name="combination" type="val:combination_Type"/>
//	<xs:element name="bag_value" type="val:bag_value_Type"/>
//	<xs:element name="boolean_value" type="val:boolean_value_Type"/>
//	<xs:element name="complex_value" type="val:complex_value_Type"/>
//	<xs:element name="composite_value" type="val:composite_value_Type"/>
//	<xs:element name="controlled_value" type="val:controlled_value_Type"/>
//	<xs:element name="currency_value" type="val:currency_value_Type"/>
//	<xs:element name="date_value" type="val:date_value_Type"/>
//	<xs:element name="date_time_value" type="val:date_time_value_Type"/>
//	<xs:element name="environment" type="val:environment_Type"/>
//	<xs:element name="file_value" type="val:file_value_Type"/>
//	<xs:element name="integer_value" type="val:integer_value_Type"/>
//	<xs:element name="item_reference_value" type="val:item_reference_value_Type"/>
//	<xs:element name="localized_text_value" type="val:localized_text_value_Type"/>
//	<xs:element name="measure_qualified_number_value" type="val:measure_qualified_number_value_Type"/>
//	<xs:element name="measure_range_value" type="val:measure_range_value_Type"/>
//	<xs:element name="measure_single_number_value" type="val:measure_single_number_value_Type"/>
//	<xs:element name="null_value" type="val:null_value_Type"/>
//	<xs:element name="one_of" type="val:one_of_Type"/>
//	<xs:element name="rational_value" type="val:rational_value_Type"/>
//	<xs:element name="real_value" type="val:real_value_Type"/>
//	<xs:element name="sequence_value" type="val:sequence_value_Type"/>
//	<xs:element name="set_value" type="val:set_value_Type"/>
//	<xs:element name="string_value" type="val:string_value_Type"/>
//	<xs:element name="time_value" type="val:time_value_Type"/>
//	<xs:element name="year_month_value" type="val:year_month_value_Type"/>
//	<xs:element name="year_value" type="val:year_value_Type"/>

    public static final int BAG_VALUE = 1;
    public static final int BOOLEAN_VALUE = 2;
    public static final int COMPLEX_VALUE = 3;
    public static final int COMPOSITE_VALUE = 4;
    public static final int CONTROLLED_VALUE = 5;
    public static final int CURRENCY_VALUE = 6;
    public static final int DATE_VALUE = 7;
    public static final int DATE_TIME_VALUE = 8;
    public static final int FILE_VALUE = 9;
    public static final int INTEGER_VALUE = 10;
    public static final int ITEM_REFERENCE_VALUE = 11;
    public static final int LOCALIZED_TEXT_VALUE = 12;
    public static final int MEASURE_QUALIFIED_NUMBER_VALUE = 13;
    public static final int MEASURE_RANGE_VALUE = 14;
    public static final int MEASURE_SINGLE_NUMBER_VALUE = 15;
    public static final int NULL_VALUE = 16;
    public static final int RATIONAL_VALUE = 17;
    public static final int REAL_VALUE = 18;
    public static final int SEQUENCE_VALUE = 19;
    public static final int SET_VALUE = 20;
    public static final int STRING_VALUE = 21;
    public static final int TIME_VALUE = 22;
    public static final int YEAR_MONTH_VALUE = 23;
    public static final int YEAR_VALUE = 24;

    public static final String D_ELEMENT_INCLUDE = "include";
    public static final String D_ATTRIBUTE_RENDERING_GUIDE_REF = "rendering_guide_ref";

    public static final String R_ELEMENT_FILE_VALUE = "file_value";
    public static final String R_ELEMENT_ITEM_REFERENCE_VALUE = "item_reference_value";
    public static final String R_ELEMENT_ITEM = "item";
    public static final String R_ELEMENT_FIELD = "field";
    public static final String R_ELEMENT_PROPERTY_VALUE = "property_value";
    public static final String R_ELEMENT_BAG_VALUE = "bag_value";
    public static final String R_ELEMENT_BOOLEAN_VALUE = "boolean_value";
    public static final String R_ELEMENT_COMPLEX_VALUE = "complex_value";
    public static final String R_ELEMENT_COMPOSITE_VALUE = "composite_value";
    public static final String R_ELEMENT_CONTENT = "content";
    public static final String R_ELEMENT_CONTROLLED_VALUE = "controlled_value";
    public static final String R_ELEMENT_CURRENCY_VALUE = "currency_value";
    public static final String R_ELEMENT_DATE_VALUE = "date_value";
    public static final String R_ELEMENT_DATE_TIME_VALUE = "date_time_value";
    public static final String R_ELEMENT_DENOMINATOR = "denominator";
    public static final String R_ELEMENT_IMAGINARY_PART = "imaginary_part";
    public static final String R_ELEMENT_INTEGER_VALUE = "integer_value";
    public static final String R_ELEMENT_LANGUAGE_REF = "language_ref";
    public static final String R_ELEMENT_LOCALIZED_TEXT_VALUE = "localized_text_value";
    public static final String R_ELEMENT_LOCAL_STRING = "local_string";
    public static final String R_ELEMENT_LOWER_VALUE = "lower_value";
    public static final String R_ELEMENT_MEASURE_RANGE_VALUE = "measure_range_value";
    public static final String R_ELEMENT_MEASURE_SINGLE_NUMBER_VALUE = "measure_single_number_value";
    public static final String R_ELEMENT_MEASURE_QUALIFIED_NUMBER_VALUE = "measure_qualified_number_value";
    public static final String R_ELEMENT_NUMERATOR = "numerator";
    public static final String R_ELEMENT_QUALIFIED_VALUE = "qualified_value";
    public static final String R_ELEMENT_RATIONAL_VALUE = "rational_value";
    public static final String R_ELEMENT_REAL_VALUE = "real_value";
    public static final String R_ELEMENT_REAL_PART = "real_part";
    public static final String R_ELEMENT_REFERENCE = "reference";
    public static final String R_ELEMENT_STRING_VALUE = "string_value";
    public static final String R_ELEMENT_SET_VALUE = "set_value";
    public static final String R_ELEMENT_SEQUENCE_VALUE = "sequence_value";
    public static final String R_ELEMENT_TIME_VALUE = "time_value";
    public static final String R_ELEMENT_UPPER_VALUE = "upper_value";
    public static final String R_ELEMENT_YEAR_MONTH_VALUE = "year_month_value";
    public static final String R_ELEMENT_YEAR_VALUE = "year_value";
    public static final String R_ELEMENT_WHOLE_PART = "whole_part";
    public static final String R_ATTRIBUTE_DATA_SPECIFICATION_REF = "data_specification_ref";
    public static final String R_ATTRIBUTE_INFORMATION_SUPPLIER_REFERENCE_STRING = "information_supplier_reference_string";
    public static final String R_ATTRIBUTE_ITEM_LOCAL_REF = "item_local_ref";
    public static final String R_ATTRIBUTE_CLASS_REF = "class_ref";
    public static final String R_ATTRIBUTE_CURRENCY_REF = "currency_ref";
    public static final String R_ATTRIBUTE_LOCAL_ID = "local_id";
    public static final String R_ATTRIBUTE_ORGANIZTION_REF = "organization_ref";
    public static final String R_ATTRIBUTE_UOM_REF = "UOM_ref";
    public static final String R_ATTRIBUTE_PROPERTY_REF = "property_ref";
    public static final String R_ATTRIBUTE_QUALIFIER_REF = "qualifier_ref";
    public static final String R_ATTRIBUTE_REFERENCE_NUMBER = "reference_number";
    public static final String R_ATTRIBUTE_VALUE_REF = "value_ref";

    public static final String I_ELEMENT_BAG_TYPE = "bag_type";
    public static final String I_ELEMENT_BOOLEAN_TYPE = "boolean_type";
    public static final String I_ELEMENT_CHOICE_TYPE = "choice_type";
    public static final String I_ELEMENT_COMPLEX_TYPE = "complex_type";
    public static final String I_ELEMENT_COMPOSITE_TYPE = "composite_type";
    public static final String I_ELEMENT_CONTROLLED_VALUE_TYPE = "controlled_value_type";
    public static final String I_ELEMENT_CURRENCY_TYPE = "currency_type";
    public static final String I_ELEMENT_DATE_TIME_TYPE = "date_time_type";
    public static final String I_ELEMENT_DATE_TYPE = "date_type";
    public static final String I_ELEMENT_FILE_TYPE = "file_type";
    public static final String I_ELEMENT_INTEGER_FORMAT = "integer_format";
    public static final String I_ELEMENT_INTEGER_TYPE = "integer_type";
    public static final String I_ELEMENT_ITEM = "prescribed_item";
    public static final String I_ELEMENT_ITEM_REFERENCE_TYPE = "item_reference_type";
    public static final String I_ELEMENT_LOCALIZED_TEXT_TYPE = "localized_text_type";
    public static final String I_ELEMENT_MEASURE_TYPE = "measure_type";
    public static final String I_ELEMENT_MEASURE_NUMBER_TYPE = "measure_number_type";
    public static final String I_ELEMENT_MEASURE_RANGE_TYPE = "measure_range_type";
    public static final String I_ELEMENT_PRESCRIBED_CURRENCY = "prescribed_currency";
    public static final String I_ELEMENT_PRESCRIBED_UNIT_OF_MEASURE = "prescribed_unit_of_measure";
    public static final String I_ELEMENT_PRESCRIBED_QUALIFIER_OF_MEASURE = "prescribed_qualifier_of_measure";
    public static final String I_ELEMENT_PROPERTY = "prescribed_property";
    public static final String I_ELEMENT_RATIONAL_TYPE = "rational_type";
    public static final String I_ELEMENT_REAL_FORMAT = "real_format";
    public static final String I_ELEMENT_REAL_TYPE = "real_type";
    public static final String I_ELEMENT_SET_TYPE = "set_type";
    public static final String I_ELEMENT_SEQUENCE_TYPE = "sequence_type";
    public static final String I_ELEMENT_STRING_FORMAT = "string_format";
    public static final String I_ELEMENT_STRING_TYPE = "string_type";
    public static final String I_ELEMENT_TIME_TYPE = "time_type";
    public static final String I_ELEMENT_VALUE_OF_PROPERTY = "value_of_property";
    public static final String I_ELEMENT_YEAR_MONTH_TYPE = "year_month_type";
    public static final String I_ELEMENT_YEAR_TYPE = "year_type";
    public static final String I_ELEMENT_FIELD = "field";
    public static final String I_ATTRIBUTE_CLASS_REF = "class_ref";
    public static final String I_ATTRIBUTE_CURRENCY_REF = "currency_ref";
    public static final String I_ATTRIBUTE_LOCAL_ID = "local_id";
    public static final String I_ATTRIBUTE_LOWER_BOUND = "lower_bound";
    public static final String I_ATTRIBUTE_PATTERN = "pattern";
    public static final String I_ATTRIBUTE_PRESCRIBED_ITEM_LOCAL_REF = "prescribed_item_local_ref";
    public static final String I_ATTRIBUTE_PROPERTY_REF = "property_ref";
    public static final String I_ATTRIBUTE_QUALIFIER_REF = "qualifier_ref";
    public static final String I_ATTRIBUTE_REPRESENTATION_REF = "representation_ref";
    public static final String I_ATTRIBUTE_UOM_REF = "UOM_ref";
    public static final String I_ATTRIBUTE_UPPER_BOUND = "upper_bound";
    public static final String I_ATTRIBUTE_VALUE_REF = "value_ref";
    public static final String I_ATTRIBUTE_IS_REQUIRED = "is_required";
}

