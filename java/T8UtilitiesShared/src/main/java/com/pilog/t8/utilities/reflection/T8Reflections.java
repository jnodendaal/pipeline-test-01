/**
 * Created on 05 May 2015, 9:46:51 AM
 */
package com.pilog.t8.utilities.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Gavin Boshoff
 */
public class T8Reflections
{
    private T8Reflections() {}

    /**
     * This convenience method performs the exact same function as
     * {@link #getInstance(java.lang.String, java.lang.Class..., java.lang.Object...)}
     * with the only exception that instead of returning {@code null}, a runtime
     * error will be raised. The use of this method should be limited to where
     * it is critical that the application fails on something other than a
     * {@code NullPointerException}, which would generally be the case with
     * the sister method.
     *
     * @see #getInstance(java.lang.String, java.lang.Class..., java.lang.Object...)
     *
     * @param <T> The type {@code T} which defines the type implicitly of the
     *      returned instance value
     *
     * @param clazzName The {@code String} fully qualified class name of the
     *      class instance to be instantiated
     * @param parameterTypes The array of {@code Class} values which will be
     *      used in the determination of the constructor to be invoked
     * @param parameters The set of parameter values to be passed to the
     *      instance object during instantiation
     *
     * @return The newly instantiated instance of the specified {@code String}
     *      class name specified
     *
     * @throws RuntimeException If there was any error during the instantiation
     *      of the requested class instance
     */
    @SuppressWarnings("unchecked")
    public static <T> T getFastFailInstance(String clazzName, Class<?>[] parameterTypes, Object... parameters)
    {
        try
        {
            return (T)getInstance(Class.forName(clazzName), parameterTypes, parameters);
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex)
        {
            throw new RuntimeException(buildErrorMessage(clazzName, parameterTypes, parameters), ex);
        }
    }

    /**
     * Creates a new instance of the specified class name. If the class is
     * accessible, the constructor taking the parameter types specified will be
     * invoked, using the specified parameters. From here the new instance is
     * then returned to the calling method as a generic type, which allows it
     * to be implicitly cast (through assignment) to the correct type.<br/>
     * <br/>
     * If the instance fails to be instantiated for whatever reason, the
     * returned value will be {@code null}.<br/>
     * <br/>
     * Due to the location of the {@code T8Logger}, it is impossible to use the
     * logger in the utility class, and therefore any exception will simply be
     * printed to the default output.<br/>
     * <br/>
     * <b>Special Notes:</b><br/>
     * 1. If the constructor takes no arguments, the {@code parameterTypes}
     *      array can be null, but should preferably be empty.<br/>
     * 2. If the constructor takes no arguments, the {@code parameters} value
     *      should be left out completely.<br/>
     * 3. If the constructor takes a single argument, and the value for such
     *      argument is {@code null}, it should still be passed.
     *
     * @param <T> The type {@code T} which defines the type implicitly of the
     *      returned instance value
     *
     * @param clazzName The {@code String} fully qualified class name of the
     *      class instance to be instantiated
     * @param parameterTypes The array of {@code Class} values which will be
     *      used in the determination of the constructor to be invoked
     * @param parameters The set of parameter values to be passed to the
     *      instance object during instantiation
     *
     * @return The newly instantiated instance of the specified {@code String}
     *      class name specified. Or {@code null} if there is any error during
     *      the instantiation of the requested instance
     */
    @SuppressWarnings({"unchecked", "CallToPrintStackTrace", "UseOfSystemOutOrSystemErr"})
    public static <T> T getInstance(String clazzName, Class<?>[] parameterTypes, Object... parameters)
    {
        try
        {
            return (T)getInstance(Class.forName(clazzName), parameterTypes, parameters);
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex)
        {
            //An exception should never occur, except if there is a spelling mistake.
            System.err.println("Error : " + buildErrorMessage(clazzName, parameterTypes, parameters));
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Gets an instance of a singleton instance. This is to ensure that
     * definitions which have a singleton type implementation that doesn't need
     * to be rebuilt from scratch every time can be retrieved as well. The
     * primary use is definition instance classes, but this is not in any way
     * limited to such cases.<br/>
     * <br/>
     * The main point to take into consideration is that the class being called
     * and the singleton expected does not have to be the same. If the class
     * which supplies the singleton has a different name, it is that class which
     * should be provided through the {@code clazzName} parameter, since it is
     * there where the method providing the instance will be called.<br/>
     * <br/>
     * <b>Special Notes:</b><br/>
     * 1. If the instance provider method takes no arguments, the
     *      {@code parameterTypes} array can be null, but should preferably be
     *      empty.<br/>
     * 2. If the instance provider method takes no arguments, the
     *      {@code parameters} value should be left out completely.<br/>
     * 3. If the instance provider method takes a single argument, and the value
     *      for such argument is {@code null}, it should still be passed.
     *
     * @param <T> The {@code T} instance of the singleton expected
     * @param clazzName The {@code String} class name on which to invoke the
     *      instance provider method
     * @param instanceProviderMethodName The {@code String} method which is
     *      responsible for supplying the singleton instance
     * @param parameterTypes The array of {@code Class} values which will be
     *      used in the determination of the instance provider method to be
     *      invoked
     * @param parameters The set of parameter values to be passed to the
     *      instance provider method
     *
     * @return The instance of the singleton as expected. Whether or not the
     *      correct value is returned is purely the responsibility of the
     *      instance provider method
     *
     * @throws RuntimeException If there is any error during the reflective
     *      invocation of the method to retrieve the singleton instance to be
     *      returned
     */
    @SuppressWarnings("unchecked")
    public static <T> T getSingletonInstance(String clazzName, String instanceProviderMethodName, Class<?>[] parameterTypes, Object... parameters)
    {
        Method instanceProviderMethod;

        try
        {
            instanceProviderMethod = Class.forName(clazzName).getMethod(instanceProviderMethodName, parameterTypes);
            return (T)instanceProviderMethod.invoke(null, parameters);
        }
        catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
        {
            throw new RuntimeException(buildSingletonErrorMessage(clazzName, instanceProviderMethodName, parameterTypes, parameters), ex);
        }
    }

    private static <T> T getInstance(Class<T> clazz, Class<?>[] parameterTypes, Object... parameters) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException
    {
        return getConstructor(clazz, parameterTypes).newInstance(parameters);
    }

    private static <T> Constructor<T> getConstructor(Class<T> clazz, Class<?>... parameterTypes) throws NoSuchMethodException
    {
        return clazz.getConstructor(parameterTypes);
    }

    private static String buildErrorMessage(String clazzName, Class<?>[] parameterTypes, Object... parameters)
    {
        StringBuilder errorBuilder;

        errorBuilder = new StringBuilder("Failed to initialize an instance of the requested class [");
        errorBuilder.append(clazzName);
        errorBuilder.append("] using parameter types [");
        appendArrayValues(errorBuilder, parameterTypes);
        errorBuilder.append("] and parameter values [");
        appendArrayValues(errorBuilder, parameters);
        errorBuilder.append(']');

        return errorBuilder.toString();
    }

    private static String buildSingletonErrorMessage(String clazzName, String instanceProviderMethod, Class<?>[] parameterTypes, Object... parameters)
    {
        StringBuilder errorBuilder;

        errorBuilder = new StringBuilder("Failed to obtain singleton instance on requested class [");
        errorBuilder.append(clazzName);
        errorBuilder.append("] using provider method [");
        errorBuilder.append(instanceProviderMethod);
        errorBuilder.append("] using parameter types [");
        appendArrayValues(errorBuilder, parameterTypes);
        errorBuilder.append("] and parameter values [");
        appendArrayValues(errorBuilder, parameters);
        errorBuilder.append(']');

        return errorBuilder.toString();
    }

    private static void appendArrayValues(StringBuilder errorBuilder, Object[] appends)
    {
        if (appends != null)
        {
            for (Object append : appends)
            {
                errorBuilder.append(append).append(',');
            }
        }
        errorBuilder.deleteCharAt(errorBuilder.length()-1); // Remove the last comma
    }
}