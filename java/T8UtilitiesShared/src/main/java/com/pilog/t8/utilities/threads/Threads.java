package com.pilog.t8.utilities.threads;

import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class Threads
{
    public static final void ensureEDT()
    {
        if (!SwingUtilities.isEventDispatchThread())
        {
            throw new RuntimeException("Execution may only occur on EDT.");
        }
    }

    public static final void ensureNotEDT()
    {
        if (SwingUtilities.isEventDispatchThread())
        {
            throw new RuntimeException("Execution may not occur on EDT.");
        }
    }
}