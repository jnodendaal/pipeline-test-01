package com.pilog.t8.utilities.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * @author Bouwer du Preez
 */
public class HashSets
{
    /**
     * Creates a new HashSet from the supplied parameters.
     *
     * @param values The values from which to create the new set.
     * @return The newly created map.
     */
    public static HashSet newHashSet(Object... values)
    {
        HashSet<Object> newSet;

        newSet = new HashSet<>();
        for (int index = 0; index < values.length; index += 1)
        {
            newSet.add(values[index]);
        }

        return newSet;
    }

    /**
     * Creates a new type-safe {@code HashSet} from the supplied parameters.
     *
     * @param <T> The {@code T} type of the {@code HashSet} which will be
     *      created
     *
     * @param values The {@code T} values which will be added to the
     *      {@code HashSet} created
     *
     * @return The {@code HashSet} containing the specified parameters
     */
    @SafeVarargs
    public static <T> HashSet<T> buildSet(T... values)
    {
        return new HashSet<>(Arrays.asList(values));
    }

    /**
     * Creates a new type-safe {@code LinkedHashSet} from the supplied
     * parameters. This set will have the items ordered in the order which they
     * were received in the parameters, or array passed.
     *
     * @param <T> The {@code T} type of the {@code LinkedHashSet} which will be
     *      created
     *
     * @param values The {@code T} values which will be added to the
     *      {@code LinkedHashSet} created, ordered in the same order which they
     *      were received
     *
     * @return The {@code HashSet} containing the specified parameters
     */
    @SafeVarargs
    public static <T> LinkedHashSet<T> buildOrderedSet(T... values)
    {
        return new LinkedHashSet<>(Arrays.asList(values));
    }
}
