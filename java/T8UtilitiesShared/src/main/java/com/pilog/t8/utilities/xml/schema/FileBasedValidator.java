package com.pilog.t8.utilities.xml.schema;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Bouwer du Preez
 */
public class FileBasedValidator
{
    private Schema schema;

    public final void loadSchema(String filePath) throws SAXException
    {
        SchemaFactory factory;
        Source schemaFile;

        // Create a SchemaFactory.
        factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // Load a WXS schema.
        schemaFile = new StreamSource(new File(filePath));
        schema = factory.newSchema(schemaFile);
    }

    public final void loadSchemaFromURL(String urlString) throws SAXException, MalformedURLException
    {
        SchemaFactory factory;
        URL schemaURL;

        // Create a SchemaFactory.
        factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // Load a WXS schema.
        schemaURL = new URL(urlString);
        schema = factory.newSchema(schemaURL);
    }

    public boolean isXMLDocumentValid(Document xmlDocument) throws IOException
    {
        try
        {
            Validator validator;

            // Create a Validator instance, which can be used to validate an instance document.
            validator = schema.newValidator();
            validator.validate(new DOMSource(xmlDocument));
            return true;
        }
        catch (SAXException e)
        {
            return false;
        }
    }
    
    public ArrayList<String> validateXMLDocument(Document xmlDocument, ErrorHandler errorHandler) throws IOException
    {
        try
        {
            XMLSAXErrorHandler defaultErrorHandler;
            Validator validator;

            defaultErrorHandler = new XMLSAXErrorHandler();
            
            // Create a Validator instance, which can be used to validate an instance document.
            validator = schema.newValidator();
            validator.setErrorHandler(errorHandler != null ? errorHandler : defaultErrorHandler);
            validator.validate(new DOMSource(xmlDocument));
            return defaultErrorHandler.getValidationErrors();
        }
        catch (SAXException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private static class XMLSAXErrorHandler implements ErrorHandler
    {
        private ArrayList<String> validationErrors;

        public XMLSAXErrorHandler()
        {
            validationErrors = new ArrayList<String>();
        }

        public ArrayList<String> getValidationErrors()
        {
            return validationErrors;
        }

        @Override
        public void warning(SAXParseException e) throws SAXException
        {
            show("Warning", e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException
        {
            validationErrors.add("Line: " + e.getLineNumber() + ", Column: " + e.getColumnNumber() + " Message: " + e.getMessage());
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException
        {
            validationErrors.add("Line: " + e.getLineNumber() + ", Column: " + e.getColumnNumber() + " Message: " + e.getMessage());
        }

        private void show(String type, SAXParseException e)
        {
            System.out.println(type + ": " + e.getMessage());
            System.out.println("Line " + e.getLineNumber() + " Column " + e.getColumnNumber());
            System.out.println("System ID: " + e.getSystemId());
        }
    }
}
