package com.pilog.t8.utilities.xml.iso;

import com.pilog.t8.utilities.xml.dom.DOMHandler;
import java.util.ArrayList;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Bouwer du Preez
 */
public class RXMLHandler
{
    public static final Document createNewRXMLDocument()
    {
        try
        {
            DocumentBuilderFactory dbFactory;
            DocumentBuilder docBuilder;
            Document newDocument;
            Element documentElement;

            dbFactory = DocumentBuilderFactory.newInstance();
            docBuilder = dbFactory.newDocumentBuilder();
            newDocument = docBuilder.newDocument();
            newDocument.appendChild(newDocument.createElementNS(ISOConstants.CATALOGUE_NAMESPACE, "catalogue"));

            documentElement = newDocument.getDocumentElement();
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:val", ISOConstants.VALUE_NAMESPACE);
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:id", ISOConstants.IDENTIFIER_NAMESPACE);
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:bas", ISOConstants.BASIC_NAMESPACE);
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:cat", ISOConstants.CATALOGUE_NAMESPACE);

            return newDocument;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static final Element appendItemElement(Element catalogueElement, String classRef)
    {
        Element itemElement;

        itemElement = catalogueElement.getOwnerDocument().createElementNS(ISOConstants.CATALOGUE_NAMESPACE, ISOConstants.R_ELEMENT_ITEM);
        itemElement.setPrefix(ISOConstants.CATALOGUE_NAMESPACE_PREFIX);
        itemElement.setAttribute(ISOConstants.R_ATTRIBUTE_CLASS_REF, classRef);
        catalogueElement.appendChild(itemElement);
        return itemElement;
    }

    public static final Element appendItemElement(Document rxmlDocument, String classRef)
    {
        Element itemElement;

        itemElement = rxmlDocument.createElementNS(ISOConstants.CATALOGUE_NAMESPACE, ISOConstants.R_ELEMENT_ITEM);
        itemElement.setPrefix(ISOConstants.CATALOGUE_NAMESPACE_PREFIX);
        itemElement.setAttribute(ISOConstants.R_ATTRIBUTE_CLASS_REF, classRef);
        rxmlDocument.getDocumentElement().appendChild(itemElement);
        return itemElement;
    }

    public static final void appendItemElement(Document rxmlDocument, Element itemElement)
    {
        Node copiedElement;

        copiedElement = rxmlDocument.importNode(itemElement, true);
        rxmlDocument.getDocumentElement().appendChild(copiedElement);
    }

    public static final Element addReferenceElement(Element itemElement, String referenceNumber, String referenceType, String organizationRef)
    {
        Element referenceElement;
        ArrayList<Element> propertyElements;

        referenceElement = itemElement.getOwnerDocument().createElementNS(ISOConstants.CATALOGUE_NAMESPACE, ISOConstants.R_ELEMENT_REFERENCE);
        referenceElement.setPrefix(ISOConstants.CATALOGUE_NAMESPACE_PREFIX);
        referenceElement.setAttribute(ISOConstants.R_ATTRIBUTE_REFERENCE_NUMBER, referenceNumber);
        referenceElement.setAttribute("reference_type", referenceType);
        referenceElement.setAttribute(ISOConstants.R_ATTRIBUTE_ORGANIZTION_REF, organizationRef);
        propertyElements = DOMHandler.getChildElements(itemElement, ISOConstants.R_ELEMENT_PROPERTY_VALUE);
        if (propertyElements.size() > 0)
        {
            itemElement.insertBefore(referenceElement, propertyElements.get(0));
            return referenceElement;
        }
        else
        {
            itemElement.appendChild(referenceElement);
            return referenceElement;
        }
    }

    public static final Element addPropertyElement(Element itemElement, String propertyRef)
    {
        Element propertyElement;

        propertyElement = itemElement.getOwnerDocument().createElementNS(ISOConstants.CATALOGUE_NAMESPACE, ISOConstants.R_ELEMENT_PROPERTY_VALUE);
        propertyElement.setPrefix(ISOConstants.CATALOGUE_NAMESPACE_PREFIX);
        propertyElement.setAttribute(ISOConstants.R_ATTRIBUTE_PROPERTY_REF, propertyRef);
        itemElement.appendChild(propertyElement);
        return propertyElement;
    }

    public static final String getInformationSupplierReferenceString(Element itemElement)
    {
        return itemElement.getAttribute("information_supplier_reference_string");
    }

    public static final ArrayList<String> getClassRefs(Document ixmlDocument)
    {
        ArrayList<Element> itemElements;
        ArrayList<String> classRefs;

        itemElements = getItemElements(ixmlDocument);
        classRefs = new ArrayList<String>();
        for (Element itemElement : itemElements)
        {
            classRefs.add(itemElement.getAttribute("class_ref"));
        }

        return classRefs;
    }

    public static final Element getFirstItemElement(Document rxmlDocument)
    {
        ArrayList<Element> itemElements;

        itemElements = RXMLHandler.getItemElements(rxmlDocument);
        return itemElements.size() > 0 ? itemElements.get(0) : null;
    }

    public static final String getDataSpecificationRef(Document rxmlDocument)
    {
        Element firstItemElement;

        firstItemElement = RXMLHandler.getFirstItemElement(rxmlDocument);
        return firstItemElement != null ? firstItemElement.getAttribute(ISOConstants.R_ATTRIBUTE_DATA_SPECIFICATION_REF) : null;
    }

    public static final String getDataSpecificationRef(Element itemElement)
    {
        return itemElement != null ? itemElement.getAttribute(ISOConstants.R_ATTRIBUTE_DATA_SPECIFICATION_REF) : null;
    }

    public static final void setDataSpecificationRef(Document rxmlDocument, String dataSpecificationRef)
    {
        Element firstItemElement;

        firstItemElement = RXMLHandler.getFirstItemElement(rxmlDocument);
        if (firstItemElement != null)
        {
            firstItemElement.setAttribute(ISOConstants.R_ATTRIBUTE_DATA_SPECIFICATION_REF, dataSpecificationRef);
        }
    }

    public static final String getFirstItemReferenceString(Document rxmlDocument)
    {
        Element firstItemElement;

        firstItemElement = RXMLHandler.getFirstItemElement(rxmlDocument);
        return firstItemElement != null ? firstItemElement.getAttribute(ISOConstants.R_ATTRIBUTE_INFORMATION_SUPPLIER_REFERENCE_STRING) : null;
    }

    public static final ArrayList<Element> getItemElements(Document rxmlDocument)
    {
        Element documentElement;

        documentElement = rxmlDocument.getDocumentElement();
        return DOMHandler.getChildElements(documentElement, "item");
    }

    public static final ArrayList<Element> getReferenceElements(Element itemElement)
    {
        return DOMHandler.getChildElements(itemElement, "reference");
    }

    public static final Element getItemElement(Document rxmlDocument, String classRef)
    {
        return getItemElement(rxmlDocument, classRef, null);
    }

    public static final Element getItemElement(Document rxmlDocument, String classRef, String localID)
    {
        NodeList nodeList;

        nodeList = rxmlDocument.getElementsByTagNameNS(ISOConstants.CATALOGUE_NAMESPACE, "item");
        for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++)
        {
            Element itemElement;
            String classRefAttributeValue;

            itemElement = (Element)nodeList.item(nodeIndex);
            classRefAttributeValue = itemElement.getAttribute("class_ref");
            if ((classRefAttributeValue != null) && (classRefAttributeValue.equalsIgnoreCase(classRef)))
            {
                String localIDAttributeValue;

                localIDAttributeValue = itemElement.getAttribute("local_id");
                if ((localID == null) || (localID.equals(localIDAttributeValue)))
                {
                    return itemElement;
                }
            }
        }

        return null;
    }

    public static final int getPropertyValueType(Element propertyElement)
    {
        ArrayList<Element> childElements;

        childElements = DOMHandler.getChildElements(propertyElement);
        if (childElements.size() > 0)
        {
            Element childElement;

            childElement = childElements.get(0);
            return getElementValueType(childElement);
        }
        else return -1;
    }

    public static final int getElementValueType(Element rxmlElement)
    {
        String elementName;

        elementName = rxmlElement.getLocalName();
        if (elementName.equalsIgnoreCase("bag_value")) return ISOConstants.BAG_VALUE;
        else if (elementName.equalsIgnoreCase("boolean_value")) return ISOConstants.BOOLEAN_VALUE;
        else if (elementName.equalsIgnoreCase("complex_value")) return ISOConstants.COMPLEX_VALUE;
        else if (elementName.equalsIgnoreCase("composite_value")) return ISOConstants.COMPOSITE_VALUE;
        else if (elementName.equalsIgnoreCase("controlled_value")) return ISOConstants.CONTROLLED_VALUE;
        else if (elementName.equalsIgnoreCase("currency_value")) return ISOConstants.CURRENCY_VALUE;
        else if (elementName.equalsIgnoreCase("date_value")) return ISOConstants.DATE_VALUE;
        else if (elementName.equalsIgnoreCase("date_time_value")) return ISOConstants.DATE_TIME_VALUE;
        else if (elementName.equalsIgnoreCase("file_value")) return ISOConstants.FILE_VALUE;
        else if (elementName.equalsIgnoreCase("integer_value")) return ISOConstants.INTEGER_VALUE;
        else if (elementName.equalsIgnoreCase("item_reference_value")) return ISOConstants.ITEM_REFERENCE_VALUE;
        else if (elementName.equalsIgnoreCase("localized_text_value")) return ISOConstants.LOCALIZED_TEXT_VALUE;
        else if (elementName.equalsIgnoreCase("measure_qualified_number_value")) return ISOConstants.MEASURE_QUALIFIED_NUMBER_VALUE;
        else if (elementName.equalsIgnoreCase("measure_range_value")) return ISOConstants.MEASURE_RANGE_VALUE;
        else if (elementName.equalsIgnoreCase("measure_single_number_value")) return ISOConstants.MEASURE_SINGLE_NUMBER_VALUE;
        else if (elementName.equalsIgnoreCase("null_value")) return ISOConstants.NULL_VALUE;
        else if (elementName.equalsIgnoreCase("rational_value")) return ISOConstants.RATIONAL_VALUE;
        else if (elementName.equalsIgnoreCase("real_value")) return ISOConstants.REAL_VALUE;
        else if (elementName.equalsIgnoreCase("sequence_value")) return ISOConstants.SEQUENCE_VALUE;
        else if (elementName.equalsIgnoreCase("set_value")) return ISOConstants.SET_VALUE;
        else if (elementName.equalsIgnoreCase("string_value")) return ISOConstants.STRING_VALUE;
        else if (elementName.equalsIgnoreCase("time_value")) return ISOConstants.TIME_VALUE;
        else if (elementName.equalsIgnoreCase("year_month_value")) return ISOConstants.YEAR_MONTH_VALUE;
        else if (elementName.equalsIgnoreCase("year_value")) return ISOConstants.YEAR_VALUE;
        else return -1;
    }

    public static final ArrayList<Element> getPropertyElements(Element itemElement)
    {
        ArrayList<Element> elements;
        NodeList nodeList;

        elements = new ArrayList<Element>();
        nodeList = itemElement.getElementsByTagNameNS(ISOConstants.CATALOGUE_NAMESPACE, "property_value");
        for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++)
        {
            elements.add((Element)nodeList.item(nodeIndex));
        }

        return elements;
    }

    public static final ArrayList<Element> getAllPropertyElements(Document document)
    {
        return DOMHandler.getAllDescendentElements(document.getDocumentElement(), ISOConstants.R_ELEMENT_PROPERTY_VALUE);
    }

    public static final Element getPropertyElement(Element itemElement, String propertyRef)
    {
        ArrayList<Element> propertyElements;
        
        propertyElements = getPropertyElements(itemElement);
        for (Element propertyElement : propertyElements)
        {
            if (propertyRef.equalsIgnoreCase(propertyElement.getAttribute("property_ref")))
            {
                return propertyElement;
            }
        }
        
        return null;
    }

    public static final String getItemConceptID(Element itemElement)
    {
        return itemElement.getAttribute(ISOConstants.R_ATTRIBUTE_CLASS_REF);
    }

    public static final String getPropertyClassConceptID(Element propertyElement)
    {
        Element itemElement;

        itemElement = (Element)propertyElement.getParentNode();
        return itemElement.getAttribute(ISOConstants.R_ATTRIBUTE_CLASS_REF);
    }

    public static final String getPropertyConceptID(Element propertyElement)
    {
        return propertyElement.getAttribute(ISOConstants.R_ATTRIBUTE_PROPERTY_REF);
    }

    public static String getPropertyClassRef(Element propertyElement)
    {
        Element element;

        element = propertyElement;
        while (element != null)
        {
            if (element.getLocalName().equals(ISOConstants.R_ELEMENT_ITEM))
            {
                return element.getAttribute(ISOConstants.R_ATTRIBUTE_CLASS_REF);
            }
            else element = (Element)element.getParentNode();
        }

        return null;
    }

    public static String getDataTypePropertyRef(Element dataTypeElement)
    {
        Element element;

        element = dataTypeElement;
        while (element != null)
        {
            if (element.getLocalName().equals(ISOConstants.R_ELEMENT_PROPERTY_VALUE))
            {
                return element.getAttribute(ISOConstants.R_ATTRIBUTE_PROPERTY_REF);
            }
            else element = (Element)element.getParentNode();
        }

        return null;
    }
}
