package com.pilog.t8.datastructures.tuples;

import java.util.Comparator;

/**
 * This comparator compares the two input pairs using the value1 of each pair.
 * It is assumed that both pairs contain a value1 of type Comparable.
 * 
 * @author Bouwer du Preez
 */
public class DefaultPairComparator implements Comparator
{
    private boolean includeValue1;
    private boolean includeValue2;
    private boolean swapPriority;
    
    /**
     * Constructs a comparator to be used for comparing two Pair objects 
     * according to the values each contains.
     * 
     * @param includeValue1 Boolean flag to indicate whether or not value1 must
     * be taken into account when comparing the pairs.
     * @param includeValue2 Boolean flag to indicate whether or not value2 must
     * be taken into account when comparing the pairs.
     * @param swapPriority If this boolean flag is false, value1 from each pair
     * will have first priority i.e. value2 will only be taken into account if
     * the value1 of the pairs are equal.  If the flag is true, the inverse will
     * apply.
     */
    public DefaultPairComparator(boolean includeValue1, boolean includeValue2, boolean swapPriority)
    {
        this.includeValue1 = includeValue1;
        this.includeValue2 = includeValue2;
        this.swapPriority = swapPriority;
    }
    
    @Override
    public int compare(Object o1, Object o2)
    {
        int result;
        
        // Get the first result.
        if ((includeValue1) && (!swapPriority))
        {
            result = compareValues((Comparable)((Pair)o1).getValue1(), (Comparable)((Pair)o2).getValue1());
        }
        else
        {
            result = compareValues((Comparable)((Pair)o1).getValue2(), (Comparable)((Pair)o2).getValue2());
        }
        
        // Only calculate a second result if the first two values were equal.
        if ((result == 0) && (includeValue1) && (includeValue2))
        {
            // If the priority is swapped, return the comparison between value1, else value2.
            if (swapPriority)
            {
                return compareValues((Comparable)((Pair)o1).getValue1(), (Comparable)((Pair)o2).getValue1());
            }
            else
            {
                return compareValues((Comparable)((Pair)o1).getValue2(), (Comparable)((Pair)o2).getValue2());
            }
        }
        else return result;
    }
    
    private int compareValues(Comparable value1, Comparable value2)
    {
        if (value1 == null)
        {
            if (value2 == null) return 0;
            else return -1;
        }
        else if (value2 == null)
        {
            return 1;
        }
        else return value1.compareTo(value2);
    }
}
