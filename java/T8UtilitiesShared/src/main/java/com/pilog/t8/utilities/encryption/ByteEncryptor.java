package com.pilog.t8.utilities.encryption;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Bouwer du Preez
 */
public class ByteEncryptor
{
    public static final byte[] encryptBytes(byte[] unencryptedBytes, String keyString) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException
    {
        Cipher cipher;
        SecretKeySpec secretKey;

        // Create the key.
        secretKey = new SecretKeySpec(keyString.getBytes(), "AES");

        // Instantiate the cipher
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return cipher.doFinal(unencryptedBytes);
    }

    public static final byte[] decryptBytes(byte[] encryptedBytes, String keyString) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher;
        SecretKeySpec secretKey;

        // Create the key.
        secretKey = new SecretKeySpec(keyString.getBytes(), "AES");

        // Instantiate the cipher
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return cipher.doFinal(encryptedBytes);
    }
}
