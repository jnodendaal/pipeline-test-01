package com.pilog.t8.commons.stream;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Hennie Brink
 */
public class MonitorOuputStream extends OutputStream
{
    private final OutputStream outputStream;
    private int byteCount;

    public MonitorOuputStream(OutputStream outputStream)
    {
        this.outputStream = outputStream;
    }

    @Override
    public synchronized void write(int b) throws IOException
    {
        byteCount++;
        outputStream.write(b);
    }

    @Override
    public synchronized void write(byte[] b, int off, int len) throws IOException
    {
        byteCount += len;
        outputStream.write(b, off, len);
    }

    public int getByteCount()
    {
        return byteCount;
    }

    @Override
    public void flush() throws IOException
    {
        outputStream.flush();
    }

    @Override
    public void close() throws IOException
    {
        outputStream.close();
    }

}
