package com.pilog.t8.utilities.objects;

import java.util.function.Supplier;

/**
 * @author Bouwer du Preez
 */
public class Objects
{
    /**
     * Evaluates the arguments supplied in sequence and returns the first one
     * that is not {@code null}. If all arguments are null, null is returned.
     *
     * @param <T> The type of objects which are provided and determine the
     *      result type
     * @param objects The arguments to evaluate.
     *
     * @return The first argument that is not {@code null} or {@code null} if
     *      all received arguments are {@code null}
     */
    @SafeVarargs
    public static <T> T coalesce(T... objects)
    {
        if (objects != null)
        {
            for (T object : objects)
            {
                if (object != null) return object;
            }

            return null;
        } else return null;
    }

    /**
     * This performs exactly the same function as
     * {@link #coalesce(java.lang.Object...)} with the exception that, if the
     * arguments require evaluation, they can be passed as functional supplier
     * arguments to only execute the required calls when the value is to be
     * evaluated.
     *
     * @see #coalesce(java.lang.Object...)
     *
     * @param <T> The type of the objects supplied by the suppliers and
     *      determines the result type
     * @param suppliers The set of {@code Supplier} objects which will provide
     *      the values required
     *
     * @return The first value supplied that is not {@code null} or {@code null}
     *      if all arguments provided by the suppliers are {@code null}
     */
    @SafeVarargs
    public static <T> T coalesce(Supplier<T>... suppliers)
    {
        if (suppliers == null) return null;
        T value;

        for (Supplier<T> supplier : suppliers)
        {
            if ((value = supplier.get()) != null) return value;
        }

        return null;
    }

    /**
     * Checks whether or not the specified value is in the set of potential
     * values.
     *
     * @param value The value to check for
     * @param potentials The potential value set
     *
     * @return {@code true} if the specified value is present in the set of
     *      potentials. {@code false} otherwise
     */
    public static boolean in(Enum<?> value, Enum<?>... potentials)
    {
        for (Enum<?> potential : potentials)
        {
            if (value == potential) return true;
        }

        return false;
    }

    /**
     * Checks whether or not the specified value is <b>NOT</b> in the set of
     * potential values.<br/>
     * <br/>
     * This is the equivalent of calling {@code !in(value, potentials)}.
     *
     * @param value The value to check for
     * @param potentials The potential value set
     *
     * @return {@code true} if the specified value is not present in the set of
     *      potentials. {@code false} otherwise
     */
    public static boolean notIn(Enum<?> value, Enum<?>... potentials)
    {
        return !in(value, potentials);
    }
}
