package com.pilog.t8.utilities.xml.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * @author Bouwer du Preez
 */
public class DOMComparator
{
    public static void compareNodes(Node expected, Node actual, boolean trimEmptyTextNodes) throws Exception
    {
        if (trimEmptyTextNodes)
        {
            DOMHandler.trimEmptyTextNodes(expected);
            DOMHandler.trimEmptyTextNodes(actual);
        }
        
        compareNodes(expected, actual);
    }

    private static void compareNodes(Node expected, Node actual) throws Exception
    {
        if (expected.getNodeType() != actual.getNodeType())
        {
            throw new Exception("Different types of nodes: " + expected + " " + actual);
        }
        else if(expected instanceof Document)
        {
            Document expectedDoc;
            Document actualDoc;

            expectedDoc = (Document)expected;
            actualDoc = (Document)actual;
            compareNodes(expectedDoc.getDocumentElement(), actualDoc.getDocumentElement());
        }
        else if (expected instanceof Element)
        {
            Element expectedElement;
            Element actualElement;
            String elementName;

            expectedElement = (Element)expected;
            actualElement = (Element)actual;
            elementName = "{" + expectedElement.getNamespaceURI() + "}" + actualElement.getLocalName();

            // Compare element names.
            if (!expectedElement.getLocalName().equals(actualElement.getLocalName()))
            {
                throw new Exception("Element names do not match: " + expectedElement.getLocalName() + " " + actualElement.getLocalName());
            }

            // Compare element namespace.
            {
                String expectedNS = expectedElement.getNamespaceURI();
                String actualNS = actualElement.getNamespaceURI();
                if ((expectedNS == null && actualNS != null)
                        || (expectedNS != null && !expectedNS.equals(actualNS)))
                {
                    throw new Exception("Element namespaces names do not match: " + expectedNS + " " + actualNS);
                }
            }

            // Compare element attributes.
            {
                NamedNodeMap expectedAttributes;
                NamedNodeMap actualAttributes;

                expectedAttributes = expectedElement.getAttributes();
                actualAttributes = actualElement.getAttributes();
                if (DOMHandler.countNonNamespaceAttribures(expectedAttributes) != DOMHandler.countNonNamespaceAttribures(actualAttributes))
                {
                    throw new Exception(elementName + ": Number of attributes do not match up: " + DOMHandler.countNonNamespaceAttribures(expectedAttributes) + " " + DOMHandler.countNonNamespaceAttribures(actualAttributes));
                }

                // Compare each attribute individually.
                for (int attributeIndex = 0; attributeIndex < expectedAttributes.getLength(); attributeIndex++)
                {
                    Attr expectedAttribute;

                    expectedAttribute = (Attr)expectedAttributes.item(attributeIndex);
                    if (expectedAttribute.getName().startsWith("xmlns"))
                    {
                        continue;
                    }
                    else
                    {
                        Attr actualAttribute;

                        actualAttribute = null;
                        if (expectedAttribute.getNamespaceURI() == null)
                        {
                            actualAttribute = (Attr)actualAttributes.getNamedItem(expectedAttribute.getName());
                        }
                        else
                        {
                            actualAttribute = (Attr)actualAttributes.getNamedItemNS(expectedAttribute.getNamespaceURI(), expectedAttribute.getLocalName());
                        }

                        if (actualAttribute == null)
                        {
                            throw new Exception(elementName + ": No attribute found:" + expectedAttribute);
                        }

                        if (!expectedAttribute.getValue().equals(actualAttribute.getValue()))
                        {
                            throw new Exception(elementName + ": Attribute values do not match: " + expectedAttribute.getValue() + " " + actualAttribute.getValue());
                        }
                    }
                }
            }

            // Compare element children.
            {
                NodeList expectedChildren;
                NodeList actualChildren;

                expectedChildren = expectedElement.getChildNodes();
                actualChildren = actualElement.getChildNodes();
                if (expectedChildren.getLength() != actualChildren.getLength())
                {
                    throw new Exception(elementName + ": Number of children do not match up: " + expectedChildren.getLength() + " " + actualChildren.getLength());
                }
                else
                {
                    for (int childIndex = 0; childIndex < expectedChildren.getLength(); childIndex++)
                    {
                        Node expectedChild;
                        Node actualChild;

                        expectedChild = expectedChildren.item(childIndex);
                        actualChild = actualChildren.item(childIndex);
                        compareNodes(expectedChild, actualChild);
                    }
                }
            }
        }
        else if (expected instanceof Text)
        {
            String expectedData;
            String actualData;

            expectedData = ((Text) expected).getData().trim();
            actualData = ((Text) actual).getData().trim();
            if (!expectedData.equals(actualData))
            {
                throw new Exception("Text does not match: " + expectedData + " " + actualData);
            }
        }
    }
}
