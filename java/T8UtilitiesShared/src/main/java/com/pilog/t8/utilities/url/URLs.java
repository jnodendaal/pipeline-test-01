package com.pilog.t8.utilities.url;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @author Gavin Boshoff
 */
public class URLs
{
    private static final String UTF_8 = "UTF-8";

    /**
     * Encodes a URL parameter string to the UTF-8 standard.
     *
     * @param parameter The {@code String} url parameter to be encoded
     *
     * @return The encoded {@code String} parameter
     */
    public static String encodeParameter(String parameter)
    {
        try
        {
            return URLEncoder.encode(parameter, UTF_8);
        }
        catch (UnsupportedEncodingException ex)
        {
            //This should never happen
            ex.printStackTrace();
            return parameter;
        }
    }

    public static String decodeParameter(String parameter)
    {
        try
        {
            return URLDecoder.decode(parameter, UTF_8);
        }
        catch (UnsupportedEncodingException ex)
        {
            //This should never happen
            ex.printStackTrace();
            return parameter;
        }
    }

    /**
     * private constructor to avoid instantiation of utility class.
     */
    private URLs() {}
}
