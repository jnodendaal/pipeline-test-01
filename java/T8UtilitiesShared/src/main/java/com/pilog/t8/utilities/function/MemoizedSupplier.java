package com.pilog.t8.utilities.function;

import java.util.function.Supplier;

/**
 * This class provides a wrapper to a Supplier function that allows the supplied
 * value to be cached after it is first requested so that no subsequent requests
 * to the wrapped supplier will be performed.
 *
 * @author Bouwer du Preez
 */
public class MemoizedSupplier<S> implements Supplier<S>
{
    private final Supplier<S> supplier;
    private S cachedObject;
    private boolean fetched;

    public MemoizedSupplier(Supplier<S> supplier)
    {
        this.supplier = supplier;
        this.fetched = false;
    }

    @Override
    public S get()
    {
        if (fetched)
        {
            return cachedObject;
        }
        else
        {
            cachedObject = supplier.get();
            fetched = true;
            return this.cachedObject;
        }
    }
}
