package com.pilog.t8.utilities.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ArrayLists
{
    /**
     * Creates a new ArrayList from the supplied parameters.
     *
     * @param values The values from which to create the new list.
     * @return The newly created ArrayList.
     *
     * @deprecated Use {@link #typeSafeList(java.lang.Object...)} instead
     */
    @Deprecated
    public static ArrayList newArrayList(Object... values)
    {
        ArrayList newList;

        newList = new ArrayList();
        for (int index = 0; index < values.length; index += 1)
        {
            newList.add(values[index]);
        }

        return newList;
    }

    /**
     * Creates a new type-safe list using the type specified by the values
     * parameter.
     *
     * @param <T> The type of the list to be built and returned
     *
     * @param values The values of type {@code T} to be added to the new list
     *
     * @return The {@code ArrayList} containing the values specified in the
     *      order which they were specified
     */
    @SafeVarargs
    public static <T> ArrayList<T> typeSafeList(T... values)
    {
        return new ArrayList<>(Arrays.asList(values)); //Manual copying of array tends to be slower
    }

    /**
     * A convenience method to enable method chaining in certain cases. The
     * convenience is the list returned instead of a {@code boolean} as is the
     * case for {@code ArrayList#addAll}.<br/>
     * <br/>
     * An example of this<br/>
     * {@code ArrayLists.typeSafeList("FirstString").appendAll(otherStringCollection)}.
     *
     * @param <T> The type specification of the specified list
     *
     * @param existingList The {@code ArrayList} to which the collection items
     *      will be added
     * @param elements The {@code Collection} of elements to be added to the
     *      existing list
     *
     * @return The {@code ArrayList} containing the original and the additional
     *      values
     */
    public static <T> ArrayList<T> appendAll(ArrayList<T> existingList, Collection<? extends T> elements)
    {
        existingList.addAll(elements);
        return existingList;
    }

    /**
     * Creates a new empty {@code ArrayList}. The list will always be
     * assign-able to any generic list type. The list is created with an initial
     * capacity of 0, and therefore this method is not optimal for lists which
     * will be added to.
     *
     * @param <T> The type {@code T} for the list which will be created
     *
     * @return The empty {@code ArrayList} of the correct generic type, with an
     *      initial capacity of 0
     */
    public static <T> ArrayList<T> emptyList()
    {
        return new ArrayList<>(0);
    }

    /**
     * Creates a copy of the specified list and returns it. The copied list has
     * each value as a {@code String}. This is a convenience method since it
     * simply performs a {@link String#valueOf(java.lang.Object)} function on
     * each of the values in the list.<br/>
     * <br/>
     * The method is {@code null}-safe. A {@code null} list will result in an
     * empty list returned. A {@code null} value in the list will be a
     * {@code null} value in the returning list.
     *
     * @param anyList The {@code List} of values to convert
     *
     * @return The {@code List} of {@code String} representations of the items
     *      found in the original list
     */
    public static ArrayList<String> asStringList(List<?> anyList)
    {
        if (anyList == null || anyList.isEmpty()) return new ArrayList<>();
        ArrayList<String> newList;

        newList = new ArrayList<>();
        anyList.stream().forEach((item) ->
        {
            if (item == null) newList.add(null);
            else newList.add(String.valueOf(item));
        });

        return newList;
    }

    /**
     * Convenience method to sort {@code ArrayList}s in-line.
     *
     * @param <T> The type of content for the {@code ArrayList}
     * @param original The {@code ArrayList} to be sorted
     * @param comparator The {@code Comparator} to be used for sorting
     *
     * @return A reference to the original {@code ArrayList} sorted according
     *      to the {@code Comparator} implementation
     */
    public static <T> ArrayList<T> sortList(ArrayList<T> original, Comparator<T> comparator)
    {
        Collections.sort(original, comparator);
        return original;
    }
}
