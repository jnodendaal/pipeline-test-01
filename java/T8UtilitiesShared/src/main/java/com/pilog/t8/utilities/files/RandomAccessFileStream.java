package com.pilog.t8.utilities.files;

import com.pilog.t8.commons.io.RandomAccessStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * @author Bouwer du Preez
 */
public class RandomAccessFileStream extends RandomAccessFile implements RandomAccessStream
{
    public RandomAccessFileStream(File file) throws FileNotFoundException
    {
        super(file, "rw");
    }

    @Override
    public long getDataPointer() throws IOException
    {
        return super.getFilePointer();
    }
}
