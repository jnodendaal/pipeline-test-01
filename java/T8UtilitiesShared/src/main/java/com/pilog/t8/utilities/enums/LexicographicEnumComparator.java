package com.pilog.t8.utilities.enums;

import java.util.Comparator;

/**
 * @author Bouwer du Preez
 */
public class LexicographicEnumComparator<E extends Enum> implements Comparator<E>
{
    @Override
    public int compare(E enum1, E enum2)
    {
        if (enum1 == null)
        {
            return (enum2 == null) ? 0 : -1;
        }
        else if (enum2 == null)
        {
            return 1;
        }
        else
        {
            return enum1.toString().compareTo(enum2.toString());
        }
    }
}
