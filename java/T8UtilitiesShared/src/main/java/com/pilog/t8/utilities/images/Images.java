package com.pilog.t8.utilities.images;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * @author Bouwer du Preez
 */
public class Images
{
    /**
     * Converts a given Image into a BufferedImage.
     *
     * @param image The Image to be converted.
     * @return The converted BufferedImage.
     */
    public static BufferedImage toBufferedImage(Image image)
    {
        if (image instanceof BufferedImage)
        {
            return (BufferedImage)image;
        }
        else
        {
            BufferedImage bufferedImage;
            Graphics2D g2;

            // Create a buffered image with transparency.
            bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);

            // Draw the image on to the buffered image.
            g2 = bufferedImage.createGraphics();
            g2.drawImage(image, 0, 0, null);
            g2.dispose();

            // Return the buffered image.
            return bufferedImage;
        }
    }
}
