/**
 * Created on 26 Jan 2016, 3:20:10 PM
 */
package com.pilog.t8.utilities.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * The {@code SLRUCache} makes use of the Segmented LRU caching principle. It
 * contains multiple inner data structures to manage this effectively. These
 * data structures share references to objects and therefore remains relatively
 * small, with all additional memory usage for reference data being stored.<br/>
 * <br/>
 * Segmented LRU (SLRU): [Description taken from Wikipedia].<br/>
 * <blockquote>
 * An SLRU cache is divided into two segments, a probationary segment and a
 * protected segment. Lines in each segment are ordered from the most to the
 * least recently accessed. Data from misses is added to the cache at the most
 * recently accessed end of the probationary segment. Hits are removed from
 * wherever they currently reside and added to the most recently accessed end of
 * the protected segment. Lines in the protected segment have thus been accessed
 * at least twice.
 * </blockquote>
 * <br/>
 * There is a difference however from the typical SLRU algorithm. Once an item
 * in the protected segment is discarded, it is not added back to the probation
 * segment. This could change at a later stage, with little impact.<br/>
 * <br/>
 * The probation segment is only a portion of the full cache size and is defined
 * as a percentage by {@link #PROBATION_CACHE_RATIO}. This means that the size
 * specified for the cache would result in a total cache size of that specified
 * with the addition of the size of the probation cache.<br/>
 * <b>Example:</b> If the cache size is specified as 1000, and the probation
 * ratio is set at 25%, the total cache size will be 1000 + 25% = 1250.<br/>
 * <br/>
 * To ensure efficiency within the cache structure it is important to have a key
 * with a hash function that disperses the elements properly among the buckets
 * within the underlying hashing data structures.
 *
 * @author Gavin Boshoff
 *
 * @param <K> The key type of the data values stored in the cache
 * @param <V> The value type of the data values stored in the cache
 */
public class SLRUCache<K, V> extends HashMap<K, V>
{
    private static final long serialVersionUID = -5782822006640372532L;

    /**
     * We don't want the probation cache to be quite as large, so we only use
     * a percentage of the full cache size.
     */
    private static final double PROBATION_CACHE_RATIO = 0.25;
    /** The error margin is to ensure the prevention of collection resizing. */
    private static final int CACHE_SIZE_ERROR_MARGIN = 5;

    private final ProbationaryCache probationCache;
    private final LRUQueue lruQueue;

    public SLRUCache(int cacheSize)
    {
        super(cacheSize+CACHE_SIZE_ERROR_MARGIN, 1f);
        this.probationCache = new ProbationaryCache((int)(cacheSize*PROBATION_CACHE_RATIO));
        this.lruQueue = new LRUQueue(cacheSize);
    }

    public boolean isCached(K key)
    {
        V probationValue;
        K removedKey;

        if (super.containsKey(key))
        {
            if ((removedKey = this.lruQueue.cache(key)) != null) remove(removedKey);
            return true;
        }
        else if ((probationValue = this.probationCache.get(key)) != null)
        {
            super.put(key, probationValue);
            if ((removedKey = this.lruQueue.cache(key)) != null) remove(removedKey);
            return true;
        } else return false;
    }

    public V getCached(K key)
    {
        return super.get(key);
    }

    public void cacheNew(K key, V value)
    {
        this.probationCache.put(key, value);
    }

    /**
     * An {@code LRUQueue} is not an extension of an actual queue, but rather an
     * {@code ArrayList} with a backing {@code HashSet} which simply enables the
     * management of key values in an LRU manner.<br/>
     * <br/>
     * Only a single method here is required for the queue to be managed and
     * maintained. The {@link #cache(java.lang.Object)} method ensures that the
     * queue remains with its items, having the least recently accessed items to
     * the back of the queue and the most recently accessed items to the front
     * of the queue.
     */
    private class LRUQueue extends ArrayList<K>
    {
        private static final long serialVersionUID = 2724284745152579777L;

        private final HashSet<K> keySet;
        private final int cacheSize;

        private K mru;

        /**
         * Creates a new {@code LRUQueue} with the specified size.
         *
         * @param initialCapacity The {@code int} size of the queue to be
         *      created
         */
        private LRUQueue(int initialCapacity)
        {
            super(initialCapacity+CACHE_SIZE_ERROR_MARGIN);
            // The size and load factor combination prevents resizing
            this.keySet = new HashSet<>(initialCapacity+CACHE_SIZE_ERROR_MARGIN, 1f);
            this.cacheSize = initialCapacity;
        }

        /**
         * Updates the access of the specified {@code TerminologyKey} key value.
         * <br/><br/>
         * If the key value is not yet contained in the queue, it is simply
         * added to the front of the queue as the most recently accessed value.
         * <br/><br/>
         * If the key value is contained in the queue, but is not the most
         * recently accessed value, it is removed from its current position and
         * added back to the front of the queue.<br/>
         * <br/>
         * If the queue has reached capacity and a non-existing item needs to be
         * added, the item at the back of the queue is removed, and the
         * reference to the key is returned.
         *
         * @param e The {@code TerminologyKey} value
         *
         * @return The {@code Terminology} of the LRU value, if such value
         *      needed to be discarded
         */
        private K cache(K e)
        {
            K removed;

            if (this.keySet.contains(e))
            {
                // If MRU, then ignore
                if ((this.mru == e) || (this.mru != null && this.mru.equals(e))) return null;

                remove(e);
                this.mru = e;
                add(e);
                return null;
            }
            else
            {
                if (this.size() > this.cacheSize)
                {
                    synchronized (this.keySet)
                    {
                        removed = this.remove(0);
                        this.keySet.remove(removed);
                    }
                } else removed = null;

                this.mru = e;
                this.keySet.add(e);
                add(e);

                return removed;
            }
        }
    }

    private class ProbationaryCache extends HashMap<K, V>
    {
        private static final long serialVersionUID = 6069717316855364070L;

        private final LRUQueue lruQueue;

        private ProbationaryCache(int probationCacheSize)
        {
            super(probationCacheSize, 1f);
            this.lruQueue = new LRUQueue(probationCacheSize);
        }

        @Override
        public V put(K key, V value)
        {
            K removedKey;

            if ((removedKey = this.lruQueue.cache(key)) != null)
            {
                return remove(removedKey);
            } else return super.put(key, value);
        }
    }
}