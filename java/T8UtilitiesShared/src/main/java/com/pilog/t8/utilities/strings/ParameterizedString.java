package com.pilog.t8.utilities.strings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ParameterizedString implements Serializable
{
    protected StringBuffer stringBuffer;
    protected final List<Object> parameterValues;

    public ParameterizedString()
    {
        stringBuffer = new StringBuffer();
        parameterValues = new ArrayList<Object>();
    }

    public ParameterizedString(String stringValue)
    {
        stringBuffer = new StringBuffer(stringValue);
        parameterValues = new ArrayList<Object>();
    }

    public ParameterizedString(String stringValue, Object... parameterValues)
    {
        this(new StringBuffer(stringValue), Arrays.asList(parameterValues));
    }

    public ParameterizedString(String stringValue, List<Object> parameterValues)
    {
        this(new StringBuffer(stringValue), parameterValues);
    }

    public ParameterizedString(StringBuffer stringValue, List<Object> parameterValues)
    {
        this.stringBuffer = stringValue != null ? stringValue : new StringBuffer();
        this.parameterValues = new ArrayList<Object>();
        setParameterValues(parameterValues);
    }

    public List<Object> getParameterValues()
    {
        return new ArrayList<Object>(parameterValues);
    }

    public final void setParameterValues(List<Object> parameterValues)
    {
        this.parameterValues.clear();
        addParameterValues(parameterValues);
    }

    public void addParameterValues(Collection<Object> parameterValues)
    {
        if (parameterValues != null)
        {
            for (Object parameter : parameterValues)
            {
                addParameter(parameter);
            }
        }
    }

    public void addParameter(Object parameter)
    {
        parameterValues.add(parameter);
    }

    public StringBuffer getStringValue()
    {
        return stringBuffer;
    }

    public void setStringValue(StringBuffer stringValue)
    {
        this.stringBuffer = stringValue;
    }

    public void append(ParameterizedString parameterizedString)
    {
        stringBuffer.append(parameterizedString.getStringValue());
        addParameterValues(parameterizedString.getParameterValues());
    }

    public void append(StringBuffer stringValue, Object... parameter)
    {
        stringBuffer.append(stringValue);
        addParameterValues(Arrays.asList(parameter));
    }

    public void append(StringBuffer stringValue, Collection parameters)
    {
        stringBuffer.append(stringValue);
        addParameterValues(parameters);
    }

    public void append(String stringValue, Object... parameters)
    {
        stringBuffer.append(stringValue);
        addParameterValues(Arrays.asList(parameters));
    }

    public void append(String stringValue, Collection parameters)
    {
        stringBuffer.append(stringValue);
        addParameterValues(parameters);
    }

    public void append(StringBuffer stringValue)
    {
        stringBuffer.append(stringValue);
    }

    public void append(String stringValue)
    {
        stringBuffer.append(stringValue);
    }

    /**
     * Returns true if this string contains no characters or only whitespace.
     * @return true if this string contains no characters or only whitespace.
     */
    public boolean isEmpty()
    {
        for (int charIndex = 0; charIndex < stringBuffer.length(); charIndex++)
        {
            if (!Character.isWhitespace(stringBuffer.charAt(charIndex))) return false;
        }

        return true;
    }

    @Override
    public String toString()
    {
        return "SQL: " + stringBuffer + " Parameters: " + parameterValues;
    }
}
