package com.pilog.t8.utilities.xml.xpath;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Bouwer du Preez
 */
public class XPathHandler
{
    public static final boolean evaluateBooleanXPathExpression(Document xmlDocument, String xPathExpressionString) throws XPathExpressionException
    {
        XPathExpression expression;
        XPathFactory factory;
        XPath xPath;

        factory = XPathFactory.newInstance();
        xPath = factory.newXPath();

        // Compile the xpath expression and use it to find the DOM Element.
        expression = xPath.compile(xPathExpressionString); // E.g. "//book[author='Neal Stephenson']/title/text()"
        return (Boolean)expression.evaluate(xmlDocument, XPathConstants.BOOLEAN);
    }

    public static final Element findElement(Document xmlDocument, String xPathExpressionString) throws XPathExpressionException
    {
        XPathExpression expression;
        XPathFactory factory;
        XPath xPath;

        factory = XPathFactory.newInstance();
        xPath = factory.newXPath();

        // Compile the xpath expression and use it to find the DOM Element.
        expression = xPath.compile(xPathExpressionString); // E.g. "//book[author='Neal Stephenson']/title/text()"
        return (Element)expression.evaluate(xmlDocument, XPathConstants.NODE);
    }

    public static final String buildIndexedXPathExpression(Element inputElement)
    {
        StringBuffer path;
        Node node;

        path = new StringBuffer();
        node = inputElement;
        while (node != null)
        {
            String nodeName;
            Node parentNode;

            nodeName = node.getNamespaceURI() + ":" + node.getLocalName();
            parentNode = node.getParentNode();
            if (parentNode != null)
            {
                NodeList childNodes;
                int nodeIndex;

                nodeIndex = 1;
                childNodes = parentNode.getChildNodes();
                for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
                {
                    Node childNode;

                    childNode = childNodes.item(childIndex);
                    if (childNode instanceof Element)
                    {
                        String childNodeName;

                        childNodeName = childNode.getNamespaceURI() + ":" + childNode.getLocalName();
                        if (childNodeName.equalsIgnoreCase(nodeName))
                        {
                            if (childNode == node)
                            {
                                path.insert(0, "]");
                                path.insert(0, nodeIndex);
                                path.insert(0, "[");
                                path.insert(0, nodeName);
                                path.insert(0, "/");

                                break;
                            }
                            else nodeIndex++;
                        }
                    }
                }
            }

            // Move one level higher in the node hierarchy.
            node = parentNode;
        }

        return path.toString();
    }
}
