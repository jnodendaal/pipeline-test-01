package com.pilog.t8.datastructures.stacks;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * This is a simple thread-safe FIFO stack data structure with a fixed size.
 * If an element is added to the stack once maximum capacity has been reached,
 * the element at the lowest position in the stack is discarded.
 *
 * @author Bouwer du preez
 * @param <T> The type of the elements to be stored on the stack.
 */
public class FixedSizeStack<T> implements Serializable
{
    private final LinkedList<T> list; // Array of stack elements.
    private int capacity;

    public FixedSizeStack(int capacity)
    {
        this.capacity = capacity;
        this.list = new LinkedList<T>();
    }

    public synchronized T push(T element)
    {
        list.push(element);
        if (list.size() > capacity)
        {
            return list.removeLast();
        }
        else return null;
    }

    public synchronized T pop()
    {
        return list.pop();
    }

    public synchronized T peek()
    {
        return list.peek();
    }

    public synchronized int size()
    {
        return list.size();
    }

    public T get(int index)
    {
        return list.get(index);
    }

    public int capacity()
    {
        return capacity;
    }

    @Override
    public String toString()
    {
        return list.toString();
    }
}
