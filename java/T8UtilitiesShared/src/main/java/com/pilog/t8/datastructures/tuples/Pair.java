package com.pilog.t8.datastructures.tuples;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 * @param <V1> Type of value1.
 * @param <V2> Type of value2.
 */
public class Pair<V1, V2> implements Serializable
{
    private V1 value1;
    private V2 value2;
    
    public Pair(V1 value1, V2 value2)
    {
        this.value1 = value1;
        this.value2 = value2;
    }

    public V1 getValue1()
    {
        return value1;
    }

    public V2 getValue2()
    {
        return value2;
    }
    
    public V1 setValue1(V1 newValue)
    {
        V1 oldValue;
        
        oldValue = value1;
        value1 = newValue;
        return oldValue;
    }
    
    public V2 setValue2(V2 newValue)
    {
        V2 oldValue;
        
        oldValue = value2;
        value2 = newValue;
        return oldValue;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.value1);
        hash = 83 * hash + Objects.hashCode(this.value2);
        return hash;
    }
    
    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Pair)) return false;
        else
        {
            Pair pair;
            
            pair = (Pair)object;
            return Objects.equals(value1, pair.getValue1()) && Objects.equals(value2, pair.getValue2());
        }
    }

    @Override
    public String toString()
    {
        return "Pair{" + "value1=" + value1 + ", value2=" + value2 + '}';
    }
}
