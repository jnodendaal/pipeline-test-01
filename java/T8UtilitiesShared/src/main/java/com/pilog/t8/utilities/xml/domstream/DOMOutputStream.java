package com.pilog.t8.utilities.xml.domstream;

import com.pilog.t8.utilities.xml.dom.DOMHandler;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class DOMOutputStream
{
    private XMLStreamWriter writer;
    private OutputStream outputStream;
    private boolean indent;
    private int indentAmount;
    private int currentIndent;

    public DOMOutputStream()
    {
        this.indent = false;
        this.indentAmount = 0;
        this.currentIndent = 0;
    }

    public boolean isIndent()
    {
        return indent;
    }

    public void setIndent(boolean indent)
    {
        this.indent = indent;
    }

    public int getIndentAmount()
    {
        return indentAmount;
    }

    public void setIndentAmount(int indentAmount)
    {
        this.indentAmount = indentAmount;
    }

    public void open(File outputFile) throws XMLStreamException, FileNotFoundException
    {
        open(new FileOutputStream(outputFile));
    }
    
    public void open(OutputStream outputStream) throws XMLStreamException, FileNotFoundException
    {
        XMLOutputFactory outputFactory;

        outputFactory = XMLOutputFactory.newInstance();
        outputStream = new BufferedOutputStream(outputStream);
        writer = outputFactory.createXMLStreamWriter(outputStream);

        // Write the start of the document.
        writer.writeStartDocument("UTF-8", "1.0");
        currentIndent = 0;
    }

    public void close() throws XMLStreamException, IOException
    {
        // Write the end of the document element.
        decIndent();
        if (indent) writer.writeCharacters(createIndentString());
        writer.writeEndElement();
        
        // Write the end of the document.
        writer.writeEndDocument();

        // Close the event writer.
        if (writer != null)
        {
            writer.flush();
            writer.close();
        }

        // Close the underlying output stream.
        if (outputStream != null)
        {
            outputStream.flush();
            outputStream.close();
        }
    }

    public void writeDocumentElement(String elementName) throws Exception
    {
        if (indent) writer.writeCharacters(createIndentString());
        incIndent();
        writer.writeStartElement(elementName);
    }

    public void writeDocumentElement(Element element) throws Exception
    {
        ArrayList<Attr> domAttributes;
        String elementName;

        elementName = element.getNodeName();
        domAttributes = DOMHandler.getAttributeNodes(element);

        if (indent) writer.writeCharacters(createIndentString());
        incIndent();
        writer.writeStartElement(elementName);

        // Write the attribute of the element.
        if (domAttributes != null)
        {
            for (Attr attribute : domAttributes)
            {
                writer.writeAttribute(attribute.getName(), attribute.getValue());
            }
        }
    }

    public void writeElement(Element element) throws Exception
    {
        ArrayList<Attr> domAttributes;
        ArrayList<Element> childElements;
        String elementName;
        String elementText;
        boolean emptyElement;
        boolean hasTextContent;

        elementName = element.getNodeName();
        elementText = DOMHandler.getElementTextValue(element);
        domAttributes = DOMHandler.getAttributeNodes(element);
        childElements = DOMHandler.getChildElements(element);
        hasTextContent = ((elementText != null) && (elementText.length() > 0));
        emptyElement = ((childElements.size() == 0) && (!hasTextContent));

        // If indentation is enabled, write the indentation white-space.
        if (indent) writer.writeCharacters(createIndentString());

        // Write the start of the element.  If now child elements are available, write an empty element.
        if (emptyElement)
        {
            writer.writeEmptyElement(elementName);
        }
        else
        {
            writer.writeStartElement(elementName);
        }

        // Write the attribute of the element.
        if (domAttributes != null)
        {
            for (Attr attribute : domAttributes)
            {
                writer.writeAttribute(attribute.getName(), attribute.getValue());
            }
        }

        // Write the child elements if there are any, else write the text content if there is any.
        if (childElements.size() > 0)
        {
            incIndent();
            for (Element childElement : childElements)
            {
                writeElement(childElement);
            }
            decIndent();
        }
        else if (hasTextContent)
        {
            writer.writeCharacters(elementText);
        }

        // If indentation is enabled, write the indentation before the closing tag.
        if (indent && (childElements.size() > 0)) writer.writeCharacters(createIndentString());

        // Write the closing tag.
        if (!emptyElement) writer.writeEndElement();
    }

    private void incIndent()
    {
        currentIndent += indentAmount;
    }

    private void decIndent()
    {
        currentIndent -= indentAmount;
    }

    private String createIndentString()
    {
        StringBuffer indentString;

        indentString = new StringBuffer("\n");
        for (int count = 0; count < currentIndent; count++)
        {
            indentString.append(" ");
        }

        return indentString.toString();
    }
}
