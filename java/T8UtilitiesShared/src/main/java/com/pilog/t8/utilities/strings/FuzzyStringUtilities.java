package com.pilog.t8.utilities.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a very simple fuzzy-logic-based String matching
 * algorithm based on Dice's coefficient.  The basic theorem is given as
 * follows:
 * 
 *     2|X ∩ Y|
 * s = --------
 *      |X|+|Y|
 *
 * The theorem states that the similarity between two sets of elements is
 * defined as two times the number of elements in the intersection of the two
 * sets divided by the number of elements in the sets combined.
 *
 * @author Bouwer.duPreez
 */
public class FuzzyStringUtilities
{
    public static List<char[]> createBigram(String input)
    {
        ArrayList<char[]> bigram;

        bigram = new ArrayList<char[]>();
        for (int charIndex = 0; charIndex < input.length() - 1; charIndex++)
        {
            char[] chars;

            chars = new char[2];
            chars[0] = input.charAt(charIndex);
            chars[1] = input.charAt(charIndex + 1);
            bigram.add(chars);
        }

        return bigram;
    }

    public static double computeSimilarity(List<char[]> bigram1, List<char[]> bigram2)
    {
        List<char[]> copy;
        int matches;

        matches = 0;
        copy = new ArrayList<char[]>(bigram2);
        for (int index1 = bigram1.size(); --index1 >= 0;)
        {
            char[] charPair1;
            
            charPair1 = bigram1.get(index1);
            for (int index2 = copy.size(); --index2 >= 0;)
            {
                char[] charPair2;

                charPair2 = copy.get(index2);
                if (charPair1[0] == charPair2[0] && charPair1[1] == charPair2[1])
                {
                    copy.remove(index2);
                    matches += 2;
                    break;
                }
            }
        }

        return (double) matches / (bigram1.size() + bigram2.size());
    }

    public static double computeSimilarity(String string1, String string2)
    {
        if (string1 == null)
        {
            return string2 == null ? 1.0 : 0.0;
        }
        else if (string2 == null)
        {
            return string1 == null ? 1.0 : 0.0;
        }
        else return computeSimilarity(createBigram(string1), createBigram(string2));
    }
}
