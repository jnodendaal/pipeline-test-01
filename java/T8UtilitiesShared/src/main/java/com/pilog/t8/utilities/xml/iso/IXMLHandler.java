package com.pilog.t8.utilities.xml.iso;

import com.pilog.t8.utilities.xml.dom.DOMHandler;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class IXMLHandler
{
    public static final Document createNewIXMLDocument(String igID, String igTitle, String mainClassRef)
    {
        try
        {
            DocumentBuilderFactory dbFactory;
            DocumentBuilder docBuilder;
            Document newDocument;
            Element documentElement;

            dbFactory = DocumentBuilderFactory.newInstance();
            docBuilder = dbFactory.newDocumentBuilder();
            newDocument = docBuilder.newDocument();
            newDocument.appendChild(newDocument.createElementNS(ISOConstants.IDENTIFICATION_GUIDE_NAMESPACE, "identification_guide"));

            documentElement = newDocument.getDocumentElement();
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:dt", ISOConstants.DATA_TYPE_NAMESPACE);
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:id", ISOConstants.IDENTIFIER_NAMESPACE);
            documentElement.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:ig", ISOConstants.IDENTIFICATION_GUIDE_NAMESPACE);

            if (igID != null) documentElement.setAttribute("id", igID);
            if (igTitle != null) documentElement.setAttribute("title", igTitle);

            if (mainClassRef != null)
            {
                Element itemElement;

                itemElement = newDocument.createElementNS(ISOConstants.IDENTIFICATION_GUIDE_NAMESPACE, "prescribed_item");
                itemElement.setAttribute("class_ref", mainClassRef);
                documentElement.appendChild(itemElement);
            }

            return newDocument;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static final Element addItemElement(Document ixmlDocument, String classRef, String localID)
    {
        Element itemElement;

        itemElement = ixmlDocument.createElementNS(ISOConstants.IDENTIFICATION_GUIDE_NAMESPACE, "prescribed_item");
        itemElement.setAttribute("class_ref", classRef);
        if (localID != null) itemElement.setAttribute("local_id", localID);
        ixmlDocument.getDocumentElement().appendChild(itemElement);
        return itemElement;
    }

    public static final Element addPropertyElement(Document ixmlDocument, Element itemElement, String propertyRef, boolean isRequired)
    {
        Element propertyElement;

        propertyElement = ixmlDocument.createElementNS(ISOConstants.IDENTIFICATION_GUIDE_NAMESPACE, "prescribed_property");
        propertyElement.setAttribute("property_ref", propertyRef);
        propertyElement.setAttribute("is_required", isRequired ? "true" : "false");
        itemElement.appendChild(propertyElement);
        return propertyElement;
    }

    public static final Element addPropertyElement(Element itemElement, String propertyRef, boolean isRequired)
    {
        Element propertyElement;

        propertyElement = itemElement.getOwnerDocument().createElementNS(ISOConstants.IDENTIFICATION_GUIDE_NAMESPACE, "prescribed_property");
        propertyElement.setAttribute("property_ref", propertyRef);
        propertyElement.setAttribute("is_required", isRequired ? "true" : "false");
        itemElement.appendChild(propertyElement);
        return propertyElement;
    }

    public static String getManagerReferenceString(Document ixmlDocument)
    {
        Element documentElement;

        documentElement = ixmlDocument.getDocumentElement();
        if (documentElement != null) return documentElement.getAttribute("manager_reference_string");
        else return null;
    }

    public static String getIGID(Document ixmlDocument)
    {
        Element documentElement;

        documentElement = ixmlDocument.getDocumentElement();
        if (documentElement != null) return documentElement.getAttribute("id");
        else return null;
    }

    public static boolean setIGID(Document ixmlDocument, String id)
    {
        if (ixmlDocument != null)
        {
            Element documentElement;

            documentElement = ixmlDocument.getDocumentElement();
            if (documentElement != null)
            {
                documentElement.setAttribute("id", id);
                return true;
            }
            else return false;
        }
        else return false;
    }

    public static String getIGTitle(Document ixmlDocument)
    {
        Element documentElement;

        documentElement = ixmlDocument.getDocumentElement();
        if (documentElement != null) return documentElement.getAttribute("title");
        else return null;
    }

    public static boolean setIGTitle(Document ixmlDocument, String title)
    {
        if (ixmlDocument != null)
        {
            Element documentElement;

            documentElement = ixmlDocument.getDocumentElement();
            if (documentElement != null)
            {
                documentElement.setAttribute("title", title);
                return true;
            }
            else return false;
        }
        else return false;
    }

    public static String getMainClassRef(Document ixmlDocument)
    {
        ArrayList<Element> itemElements;

        itemElements = getItemElements(ixmlDocument);
        if (itemElements.size() > 0) return itemElements.get(0).getAttribute("class_ref");
        else return null;
    }

    public static ArrayList<String> getClassRefs(Document ixmlDocument)
    {
        ArrayList<Element> itemElements;
        ArrayList<String> classRefs;

        itemElements = getItemElements(ixmlDocument);
        classRefs = new ArrayList<String>();
        for (Element itemElement : itemElements)
        {
            classRefs.add(itemElement.getAttribute("class_ref"));
        }

        return classRefs;
    }

    public static ArrayList<String> getPropertyRefs(Document ixmlDocument, String classRef)
    {
        ArrayList<Element> propertyElements;
        ArrayList<String> propertyRefs;

        propertyElements = getPropertyElements(ixmlDocument, classRef);
        propertyRefs = new ArrayList<String>();
        for (Element propertyElement : propertyElements)
        {
            propertyRefs.add(propertyElement.getAttribute("property_ref"));
        }

        return propertyRefs;
    }

    public static ArrayList<Element> getItemElements(Document ixmlDocument)
    {
        Element documentElement;

        documentElement = ixmlDocument.getDocumentElement();
        return DOMHandler.getChildElements(documentElement, ISOConstants.I_ELEMENT_ITEM);
    }

    public static Element getItemElement(Document ixmlDocument, String classRef, String localID)
    {
        HashMap<String, String> filter;
        ArrayList<Element> childElements;
        Element documentElement;

        filter = new HashMap<String, String>();
        filter.put(ISOConstants.I_ATTRIBUTE_CLASS_REF, classRef);
        if (localID != null) filter.put(ISOConstants.I_ATTRIBUTE_LOCAL_ID, localID);

        documentElement = ixmlDocument.getDocumentElement();
        childElements = DOMHandler.getChildElements(documentElement, ISOConstants.I_ELEMENT_ITEM, filter);
        return childElements.size() > 0 ? childElements.get(0) : null;
    }

    public static int getPropertyBaseDataType(Element propertyElement)
    {
        ArrayList<Element> childElements;

        childElements = DOMHandler.getChildElements(propertyElement);
        for (Element childElement : childElements)
        {
            String localName;

            localName = childElement.getLocalName();
            if (localName.equals("bag_type")) return ISOConstants.BAG_TYPE;
            else if(localName.equals("boolean_type")) return ISOConstants.BOOLEAN_TYPE;
            else if(localName.equals("choice_type")) return ISOConstants.CHOICE_TYPE;
            else if(localName.equals("composite_type")) return ISOConstants.COMPOSITE_TYPE;
            else if(localName.equals("controlled_value_type")) return ISOConstants.CONTROLLED_VALUE_TYPE;
            else if(localName.equals("currency_type")) return ISOConstants.CURRENCY_TYPE;
            else if(localName.equals("date_type")) return ISOConstants.DATE_TYPE;
            else if(localName.equals("date_time_type")) return ISOConstants.DATE_TIME_TYPE;
            else if(localName.equals("file_type")) return ISOConstants.FILE_TYPE;
            else if(localName.equals("integer_type")) return ISOConstants.INTEGER_TYPE;
            else if(localName.equals("item_reference_type")) return ISOConstants.ITEM_REFERENCE_TYPE;
            else if(localName.equals("localized_text_type")) return ISOConstants.LOCALIZED_TEXT_TYPE;
            else if(localName.equals("measure_number_type")) return ISOConstants.MEASURE_NUMBER_TYPE;
            else if(localName.equals("measure_range_type")) return ISOConstants.MEASURE_RANGE_TYPE;
            else if(localName.equals("rational_type")) return ISOConstants.RATIONAL_TYPE;
            else if(localName.equals("real_type")) return ISOConstants.REAL_TYPE;
            else if(localName.equals("sequence_type")) return ISOConstants.SEQUENCE_TYPE;
            else if(localName.equals("set_type")) return ISOConstants.SET_TYPE;
            else if(localName.equals("string_type")) return ISOConstants.STRING_TYPE;
            else if(localName.equals("time_type")) return ISOConstants.TIME_TYPE;
            else if(localName.equals("year_type")) return ISOConstants.YEAR_TYPE;
            else if(localName.equals("year_month_type")) return ISOConstants.YEAR_MONTH_TYPE;
        }

        return -1;
    }

    public static ArrayList<Element> getPropertyElements(Document ixmlDocument, String classRef)
    {
        ArrayList<Element> classElements;
        Element documentElement;

        documentElement = ixmlDocument.getDocumentElement();
        if (classRef == null)
        {
            ArrayList<Element> elements;

            elements = new ArrayList<Element>();
            classElements = DOMHandler.getChildElements(documentElement, "prescribed_item");
            for (Element classElement : classElements)
            {
                elements.addAll(DOMHandler.getChildElements(classElement, "prescribed_property"));
            }
            
            return elements;
        }
        else
        {
            HashMap<String, String> filterValues;
            filterValues = new HashMap<String, String>();
            filterValues.put("class_ref", classRef);

            classElements = DOMHandler.getChildElements(documentElement, "prescribed_item", filterValues);
            if (classElements.size() > 0)
            {
                return DOMHandler.getChildElements(classElements.get(0), "prescribed_property");
            }
            else return new ArrayList<Element>();
        }
    }

    public static ArrayList<Element> getPropertyElements(Element classElement)
    {
        return DOMHandler.getChildElements(classElement, ISOConstants.I_ELEMENT_PROPERTY);
    }

    public static Element getPropertyElement(Element classElement, String propertyRef)
    {
        ArrayList<Element> propertyElements;
        HashMap<String, String> filter;

        filter = new HashMap<String, String>();
        filter.put(ISOConstants.I_ATTRIBUTE_PROPERTY_REF, propertyRef);

        propertyElements = DOMHandler.getChildElements(classElement, ISOConstants.I_ELEMENT_PROPERTY, filter);
        return propertyElements.size() > 0 ? propertyElements.get(0) : null;
    }

    public static ArrayList<String> getRequiredPropertyRefs(Document ixmlDocument, String classRef)
    {
        ArrayList<Element> propertyElements;
        ArrayList<String> requiredPropertyRefs;

        propertyElements = getPropertyElements(ixmlDocument, classRef);
        requiredPropertyRefs = new ArrayList<String>();
        for (Element propertyElement : propertyElements)
        {
            if ("true".equalsIgnoreCase(propertyElement.getAttribute("is_required")))
            {
                requiredPropertyRefs.add(propertyElement.getAttribute("property_ref"));
            }
        }

        return requiredPropertyRefs;
    }

    public static ArrayList<Element> getConceptUseElements(Document ixmlDocument)
    {
        Element documentElement;

        documentElement = ixmlDocument.getDocumentElement();
        return DOMHandler.getChildElements(documentElement, "concept_use");
    }

    public static String getPropertyClassRef(Element propertyElement)
    {
        Element element;

        element = propertyElement;
        while (element != null)
        {
            if (element.getLocalName().equals(ISOConstants.I_ELEMENT_ITEM))
            {
                return element.getAttribute(ISOConstants.I_ATTRIBUTE_CLASS_REF);
            }
            else element = (Element)element.getParentNode();
        }

        return null;
    }

    public static String getDataTypePropertyRef(Element dataTypeElement)
    {
        Element element;

        element = dataTypeElement;
        while (element != null)
        {
            if (element.getLocalName().equals(ISOConstants.I_ELEMENT_PROPERTY))
            {
                return element.getAttribute(ISOConstants.I_ATTRIBUTE_PROPERTY_REF);
            }
            else element = (Element)element.getParentNode();
        }

        return null;
    }  
}
