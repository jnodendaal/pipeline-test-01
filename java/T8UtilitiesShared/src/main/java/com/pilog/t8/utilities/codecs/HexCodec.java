package com.pilog.t8.utilities.codecs;

/**
 * @author Bouwer du Preez
 */
public class HexCodec
{
    private static final String HEX_CHARACTERS = "0123456789ABCDEF";

    /**
     * Converts the supplied array of bytes to a Hexadecimal String
     * representation.
     *
     * @param byteArray The array of bytes to convert.
     * @return A String of hexadecimal characters representing the bytes in the
     * array.
     */
    public static final String convertBytesToHexString(byte[] byteArray)
    {
        if (byteArray == null)
        {
            return null;
        }
        else
        {
            final StringBuilder hexString;

            hexString = new StringBuilder(2 * byteArray.length);
            for (final byte b : byteArray)
            {
                hexString.append(HEX_CHARACTERS.charAt((b & 0xF0) >> 4)); // Append leading 4 bits.
                hexString.append(HEX_CHARACTERS.charAt((b & 0x0F))); // Append trailing 4 bits.
            }

            return hexString.toString();
        }
    }

    /**
     * Converts the supplied String of hexadecimal characters to the
     * corresponding byte values.
     *
     * @param hexString The hexadecimal String to convert.
     * @return The array of bytes converted from the String of hex characters.
     */
    public static final byte[] convertHexStringToBytes(String hexString)
    {
        int stringLength;
        byte[] byteBuffer;

        stringLength = hexString.length();
        byteBuffer = new byte[stringLength / 2];

        for (int i = 0; i < stringLength; i += 2)
        {
            byteBuffer[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
        }

        return byteBuffer;
    }
}
