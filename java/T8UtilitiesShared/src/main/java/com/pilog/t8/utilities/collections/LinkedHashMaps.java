package com.pilog.t8.utilities.collections;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class LinkedHashMaps
{
    /**
     * This method sorts the input map according to the list of keys supplied.
     * Only keys that are both in the input map as well as the keys list will be
     * added to the output map.
     * @param inputMap The input map to sort.
     * @param keys The list of keys that will be used for sorting.
     * @return The sorted output map.  If either of the input parameters are 
     * null, this map will be empty.
     */
    public static LinkedHashMap sortMap(Map inputMap, List keys)
    {
        LinkedHashMap outputMap;
        
        outputMap = new LinkedHashMap();
        if (inputMap != null)
        {
            if (keys != null)
            {
                for (Object key : keys)
                {
                    if (inputMap.containsKey(key))
                    {
                        outputMap.put(key, inputMap.get(key));
                    }
                }
            }
            else outputMap.putAll(inputMap);
        }
        
        return outputMap;
    }
}
