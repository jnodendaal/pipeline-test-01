package com.pilog.t8.utilities.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;

/**
 * @author Bouwer du Preez
 */
public class JSON
{
    public static JsonObject getObject(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonObject ? (JsonObject)value : null;
        }
        else return null;
    }

    public static JsonArray getArray(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonArray ? (JsonArray)value : null;
        }
        else return null;
    }

    public static String getString(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonString ? ((JsonString)value).getString() : null;
        }
        else return null;
    }

    public static Boolean getBoolean(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            if (value == JsonValue.TRUE) return true;
            else if (value == JsonValue.FALSE) return false;
            else return false;
        }
        else return null;
    }

    public static JsonNumber getJsonNumber(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonNumber ? ((JsonNumber)value) : null;
        }
        else return null;
    }

    public static Integer getInteger(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonNumber ? ((JsonNumber)value).intValue() : null;
        }
        else return null;
    }

    public static Long getLong(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonNumber ? ((JsonNumber)value).longValue() : null;
        }
        else return null;
    }

    public static Double getDouble(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonNumber ? ((JsonNumber)value).doubleValue() : null;
        }
        else return null;
    }

    public static BigInteger getBigInteger(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonNumber ? ((JsonNumber)value).bigIntegerValue() : null;
        }
        else return null;
    }

    public static BigDecimal getBigDecimal(JsonObject object, String identifier)
    {
        if (object.containsKey(identifier))
        {
            JsonValue value;

            value = object.get(identifier);
            return value instanceof JsonNumber ? ((JsonNumber)value).bigDecimalValue() : null;
        }
        else return null;
    }

    public static void add(JsonObjectBuilder objectBuilder, String objectIdentifier, Object javaObject)
    {
        if (javaObject == null)
        {
            objectBuilder.addNull(objectIdentifier);
        }
        else if (javaObject instanceof String)
        {
            objectBuilder.add(objectIdentifier, (String)javaObject);
        }
        else if (javaObject instanceof Boolean)
        {
            objectBuilder.add(objectIdentifier, (Boolean)javaObject);
        }
        else if (javaObject instanceof Integer)
        {
            objectBuilder.add(objectIdentifier, (Integer)javaObject);
        }
        else if (javaObject instanceof Long)
        {
            objectBuilder.add(objectIdentifier, (Long)javaObject);
        }
        else if (javaObject instanceof Float)
        {
            objectBuilder.add(objectIdentifier, (Float)javaObject);
        }
        else if (javaObject instanceof Double)
        {
            objectBuilder.add(objectIdentifier, (Double)javaObject);
        }
        else if (javaObject instanceof BigInteger)
        {
            objectBuilder.add(objectIdentifier, (BigInteger)javaObject);
        }
        else if (javaObject instanceof BigDecimal)
        {
            objectBuilder.add(objectIdentifier, (BigDecimal)javaObject);
        }
        else if (javaObject instanceof JsonArrayBuilder)
        {
            objectBuilder.add(objectIdentifier, (JsonArrayBuilder)javaObject);
        }
        else if (javaObject instanceof JsonArray)
        {
            objectBuilder.add(objectIdentifier, (JsonArray)javaObject);
        }
        else if (javaObject instanceof JsonObjectBuilder)
        {
            objectBuilder.add(objectIdentifier, (JsonObjectBuilder)javaObject);
        }
        else if (javaObject instanceof JsonObject)
        {
            objectBuilder.add(objectIdentifier, (JsonObject)javaObject);
        }
        else throw new RuntimeException("Invalid JSON object type '" + objectIdentifier + "': " + javaObject.getClass().getCanonicalName());
    }

    public static void add(JsonArrayBuilder arrayBuilder, Object javaObject)
    {
        if (javaObject == null)
        {
            arrayBuilder.addNull();
        }
        else if (javaObject instanceof String)
        {
            arrayBuilder.add((String)javaObject);
        }
        else if (javaObject instanceof Boolean)
        {
            arrayBuilder.add((Boolean)javaObject);
        }
        else if (javaObject instanceof Integer)
        {
            arrayBuilder.add((Integer)javaObject);
        }
        else if (javaObject instanceof Long)
        {
            arrayBuilder.add((Long)javaObject);
        }
        else if (javaObject instanceof Float)
        {
            arrayBuilder.add((Float)javaObject);
        }
        else if (javaObject instanceof Double)
        {
            arrayBuilder.add((Double)javaObject);
        }
        else if (javaObject instanceof BigInteger)
        {
            arrayBuilder.add((BigInteger)javaObject);
        }
        else if (javaObject instanceof BigDecimal)
        {
            arrayBuilder.add((BigDecimal)javaObject);
        }
        else if (javaObject instanceof JsonArrayBuilder)
        {
            arrayBuilder.add((JsonArrayBuilder)javaObject);
        }
        else if (javaObject instanceof JsonArray)
        {
            arrayBuilder.add((JsonArray)javaObject);
        }
        else if (javaObject instanceof JsonObjectBuilder)
        {
            arrayBuilder.add((JsonObjectBuilder)javaObject);
        }
        else if (javaObject instanceof JsonObject)
        {
            arrayBuilder.add((JsonObject)javaObject);
        }
        else throw new RuntimeException("Invalid JSON object type: " + javaObject.getClass().getCanonicalName());
    }
}
