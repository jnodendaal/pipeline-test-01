package com.pilog.t8.commons.stream;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class MonitorInputStream extends InputStream
{
    private final InputStream inputStream;
    private final List<InputStreamListener> listeners;

    private int byteCount;

    public MonitorInputStream(InputStream InputStream)
    {
        this.inputStream = InputStream;
        this.listeners = new ArrayList<>();
    }

    public void addInputStreamListener(InputStreamListener listener)
    {
        listeners.add(listener);
    }

    public void removeInputStreamListener(InputStreamListener listener)
    {
        listeners.remove(listener);
    }


    @Override
    public synchronized int read() throws IOException
    {
        byteCount++;
        return inputStream.read();
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) throws IOException
    {
        byteCount += len;
        return inputStream.read(b, off, len);
    }

    public int getByteCount()
    {
        return byteCount;
    }

    @Override
    public void close() throws IOException
    {
        inputStream.close();

        notifyStreamClosed();
    }

    @Override
    public int available() throws IOException
    {
        return inputStream.available();
    }

    @Override
    public synchronized void mark(int readlimit)
    {
        inputStream.mark(readlimit);
    }

    @Override
    public boolean markSupported()
    {
        return inputStream.markSupported();
    }

    @Override
    public synchronized void reset() throws IOException
    {
        inputStream.reset();
    }

    @Override
    public long skip(long n) throws IOException
    {
        return inputStream.skip(n);
    }

    private void notifyStreamClosed()
    {
        //Loop from the inwards because we want to allow listener to remove themselves and not fail if they do
        for(int i = listeners.size() - 1; i >= 0; i--)
        {
            InputStreamListener listener;

            listener = listeners.get(i);

            if(listener != null)
            {
                listener.streamClosed();
            }
        }
    }

    public static interface InputStreamListener
    {
        public void streamClosed();
    }
}
