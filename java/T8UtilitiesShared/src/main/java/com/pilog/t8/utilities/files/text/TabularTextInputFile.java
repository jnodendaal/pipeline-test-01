package com.pilog.t8.utilities.files.text;

import com.pilog.t8.utilities.unicode.UnicodeReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class TabularTextInputFile
{
    private final String defaultEncoding;
    private final String separator;
    private BufferedReader inputReader;
    private ArrayList<String> columnNames;
    private int rowCount;
    private final String escapedSeparator;

    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String ENCODING_UTF_16 = "UTF-16";

    public TabularTextInputFile(String defaultEncoding, String separatorString)
    {
        this.defaultEncoding = defaultEncoding;
        this.separator = separatorString;
        this.rowCount = -1;
        this.escapedSeparator = Pattern.quote(separator);
    }

    public void openFile(File inputFile) throws Exception
    {
        openFile(new FileInputStream(inputFile));
    }

    public void openFile(InputStream inputStream) throws Exception
    {
        if (inputReader == null)
        {
            // Open the input file.
            inputReader = new BufferedReader(new UnicodeReader(inputStream, defaultEncoding));

            // Read a line from the text file and assign the values to the column names collection.
            this.columnNames = readRowValues();
        }
    }

    public ArrayList<String> getColumnNames()
    {
        return new ArrayList<String>(columnNames);
    }

    public HashMap<String, Object> readDataRow() throws Exception
    {
        ArrayList<String> rowValues;

        // Get the specified worksheet and if it doesnt excist, create it.
        if (inputReader == null) throw new Exception("Attempt to read from an unopened file encountered.");

        // Read a new data row from the text file.
        rowValues = readRowValues();
        if (rowValues != null)
        {
            try
            {
                HashMap<String, Object> dataRow;

                dataRow = new HashMap<String, Object>();
                for (int columnIndex = 0; columnIndex < columnNames.size(); columnIndex++)
                {
                    dataRow.put(columnNames.get(columnIndex), rowValues.get(columnIndex));
                }

                return dataRow;
            }
            catch (IndexOutOfBoundsException e)
            {
                e.printStackTrace();
                throw new Exception("Text data row does not match column count.  Number of columns: " + columnNames.size() + ", number of values: " + rowValues.size() + ", values: " + rowValues);
            }
        }
        else return null;
    }

    private ArrayList<String> readRowValues() throws Exception
    {
        String inputLine;

        inputLine = inputReader.readLine();
        if (inputLine != null)
        {
            ArrayList<String> stringValues;
            String[] strings;

            strings = inputLine.split(escapedSeparator, -1);
            stringValues = new ArrayList<String>();
            for (String string : strings)
            {
                if (string.length() == 0)
                {
                    stringValues.add(null);
                }
                else
                {
                    stringValues.add(string);
                }
            }

            return stringValues;
        }
        else return null;
    }

    public int countRows(InputStream inputStream) throws Exception
    {
        try
        {
            openFile(inputStream);
            if (inputReader != null)
            {
                if (rowCount == -1)
                {
                    rowCount = 0;
                    while (inputReader.readLine() != null)
                    {
                        rowCount++;
                    }

                    // Return the calculated row count.
                    return rowCount;
                }
                else return rowCount;
            }
            else throw new Exception("File not open.");
        }
        finally
        {
            closeFile();
        }
    }

    public void closeFile() throws Exception
    {
        if (inputReader != null)
        {
            inputReader.close();
            inputReader = null;
        }
    }
}
