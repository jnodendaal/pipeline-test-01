package com.pilog.t8.utilities.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Bouwer du Preez
 */
public class SerializationUtilities
{
    /**
     * This method returns a copy of the supplied object.  The copy is created
     * through serialization of the input object.
     * 
     * @param serializableObject The object to copy.
     * @return A copy of the input object.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object copyObjectBySerialization(Object serializableObject) throws IOException, ClassNotFoundException
    {
        return deserializeObject(serializeObject(serializableObject));
    }

    /**
     * Deserializes an object from the supplied byte array.
     *
     * @param serializedObjectBytes A byte array containing the serialized
     * object.
     * @return The deserialized object.
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public static Object deserializeObject(byte[] serializedObjectBytes) throws IOException, ClassNotFoundException
    {
        ObjectInputStream objectInputStream;
        Object deserializedObject;

        objectInputStream = new ObjectInputStream(new ByteArrayInputStream(serializedObjectBytes));
        deserializedObject  = objectInputStream.readObject();
        objectInputStream.close();

        return deserializedObject;
    }

    /**
     * Serializes the supplied object to a byte array.
     *
     * @param serializableObject The object to serialize.
     * @return The byte array containing the serialized state of the supplied
     * object.
     * @throws java.io.IOException
     */
    public static byte[] serializeObject(Object serializableObject) throws IOException
    {
        ByteArrayOutputStream byteArrayOutputStream;
        ObjectOutputStream objectOutputStream;

        byteArrayOutputStream = new ByteArrayOutputStream();
        objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(serializableObject);
        objectOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }
}
