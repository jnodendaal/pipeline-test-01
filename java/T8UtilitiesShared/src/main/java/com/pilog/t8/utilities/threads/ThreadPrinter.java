package com.pilog.t8.utilities.threads;

import java.io.PrintStream;
import java.lang.management.LockInfo;
import static java.lang.management.ManagementFactory.getThreadMXBean;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * @author Bouwer du Preez
 */
public class ThreadPrinter
{
    private ThreadMXBean threadBean;
    private PrintStream printStream;

    private static String INDENT = "    ";

    public ThreadPrinter(ThreadMXBean threadBean, PrintStream printStream)
    {
        this.threadBean = threadBean;
        this.printStream = printStream;
    }

    public ThreadPrinter(PrintStream printStream)
    {
        this(getThreadMXBean(), printStream);
    }

    public ThreadPrinter()
    {
        this(getThreadMXBean(), System.out);
    }

    /**
     * Prints the thread dump information to the stream.
     */
    public void threadDump()
    {
        if (threadBean.isObjectMonitorUsageSupported() && threadBean.isSynchronizerUsageSupported())
        {
            dumpThreadInfoWithLocks();
        }
        else
        {
            dumpThreadInfo();
        }
    }

    private void dumpThreadInfo()
    {
        ThreadInfo[] threadInfos;
        long[] threadIDs;

        printStream.println("Thread Dump");
        threadIDs = threadBean.getAllThreadIds();
        threadInfos = threadBean.getThreadInfo(threadIDs, Integer.MAX_VALUE);
        for (ThreadInfo threadInfo : threadInfos)
        {
            printThreadInfo(threadInfo);
        }
    }

    /**
     * Prints the thread dump information with locks.
     */
    private void dumpThreadInfoWithLocks()
    {
        ThreadInfo[] threadInfos;

        printStream.println("Thread Dump with Locks");

        threadInfos = threadBean.dumpAllThreads(true, true);
        for (ThreadInfo threadInfo : threadInfos)
        {
            LockInfo[] synchronizers;

            printThreadInfo(threadInfo);
            synchronizers = threadInfo.getLockedSynchronizers();
            printLockInfo(synchronizers);
        }

        printStream.println();
    }

    private void printThreadInfo(ThreadInfo threadInfo)
    {
        StackTraceElement[] stacktrace;
        MonitorInfo[] monitors;

        // Print thread information.
        printThread(threadInfo);

        // Print stack trace with locks.
        stacktrace = threadInfo.getStackTrace();
        monitors = threadInfo.getLockedMonitors();
        for (int stackDepth = 0; stackDepth < stacktrace.length; stackDepth++)
        {
            StackTraceElement stackElement;

            stackElement = stacktrace[stackDepth];
            printStream.println(INDENT + "at " + stackElement.toString());
            for (MonitorInfo monitorInfo : monitors)
            {
                if (monitorInfo.getLockedStackDepth() == stackDepth)
                {
                    printStream.println(INDENT + "  - locked " + monitorInfo);
                }
            }
        }

        printStream.println();
    }

    private void printThread(ThreadInfo threadInfo)
    {
        // Print the general thread information.
        printStream.print("\"" + threadInfo.getThreadName() + "\"" + " Id=" + threadInfo.getThreadId() + " in " + threadInfo.getThreadState());

        // Print the thread lock if available.
        if (threadInfo.getLockName() != null)
        {
            printStream.print(" on lock=" + threadInfo.getLockName());
        }

        // Print the suspended state if applicable.
        if (threadInfo.isSuspended())
        {
            printStream.print(" (suspended)");
        }

        // Print native label if applicable.
        if (threadInfo.isInNative())
        {
            printStream.print(" (running in native)");
        }

        // Print the owner of the thread if available.
        printStream.println();
        if (threadInfo.getLockOwnerName() != null)
        {
            printStream.println(INDENT + " owned by " + threadInfo.getLockOwnerName() + " Id=" + threadInfo.getLockOwnerId());
        }
    }

    private void printMonitorInfo(ThreadInfo ti, MonitorInfo[] monitors)
    {
        printStream.println(INDENT + "Locked monitors: count = " + monitors.length);
        for (MonitorInfo mi : monitors)
        {
            printStream.println(INDENT + "  - " + mi + " locked at ");
            printStream.println(INDENT + "      " + mi.getLockedStackDepth() + " " + mi.getLockedStackFrame());
        }
    }

    private void printLockInfo(LockInfo[] locks)
    {
        printStream.println(INDENT + "Locked synchronizers: count = " + locks.length);
        for (LockInfo lock : locks)
        {
            printStream.println(INDENT + "  - " + lock);
        }
        printStream.println();
    }

    /**
     * Checks if any threads are deadlocked.  If any, prints the thread dump
     * information.
     * @return
     */
    public boolean findDeadlock()
    {
        long[] threadIDs;

        // Get the locked thread ID's.
        if (threadBean.isSynchronizerUsageSupported())
        {
            threadIDs = threadBean.findDeadlockedThreads();
        }
        else
        {
            threadIDs = threadBean.findMonitorDeadlockedThreads();
        }

        // Print information of locked threads.
        if (threadIDs == null)
        {
            return false;
        }
        else
        {
            ThreadInfo[] threadInfos;

            printStream.println("Deadlock found :-");
            threadInfos = threadBean.getThreadInfo(threadIDs, true, true);
            for (ThreadInfo threadInfo : threadInfos)
            {
                printThreadInfo(threadInfo);
                printLockInfo(threadInfo.getLockedSynchronizers());
                printStream.println();
            }

            return true;
        }
    }
}
