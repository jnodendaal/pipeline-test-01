package com.pilog.t8.datastructures.graph;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class GraphVertex implements Serializable
{
    private final Map<String, GraphVertex> inputs;
    private final Map<String, GraphVertex> outputs;
    private final String id;
    
    public GraphVertex(String id)
    {
        this.id = id;
        this.inputs = new HashMap<String, GraphVertex>();
        this.outputs = new HashMap<String, GraphVertex>();
    }
    
    public String getID()
    {
        return id;
    }
    
    void addInput(GraphVertex vertex)
    {
        inputs.put(vertex.getID(), vertex);
    }
    
    GraphVertex removeInput(String id)
    {
        return inputs.remove(id);
    }
    
    public Map<String, GraphVertex> getInputs()
    {
        return new HashMap<String, GraphVertex>(inputs);
    }
    
    public int getInputCount()
    {
        return inputs.size();
    }
    
    public void clearInputs()
    {
        inputs.clear();
    }
    
    public GraphVertex getInput(String id)
    {
        return inputs.get(id);
    }
    
    void addOutput(GraphVertex vertex)
    {
        outputs.put(vertex.getID(), vertex);
    }
    
    GraphVertex removeOutput(String id)
    {
        return outputs.remove(id);
    }
    
    public Map<String, GraphVertex> getOutputs()
    {
        return new HashMap<String, GraphVertex>(outputs);
    }
    
    public int getOutputCount()
    {
        return outputs.size();
    }
    
    public void clearOutputs()
    {
        outputs.clear();
    }
    
    public GraphVertex getOutput(String id)
    {
        return outputs.get(id);
    }
    
    void addLinks(Map<String, GraphVertex> resultMap)
    {
        if (!resultMap.containsKey(id))
        {
            for (GraphVertex input : inputs.values())
            {
                input.addLinks(resultMap);
            }
                
            for (GraphVertex output : outputs.values())
            {
                output.addLinks(resultMap);
            }
        }
    }
    
    @Override
    public String toString()
    {
        StringBuffer buffer;
        
        buffer = new StringBuffer();
        buffer.append("[Vertex:");
        buffer.append(id);
        buffer.append("]");
        return buffer.toString();
    }
}
