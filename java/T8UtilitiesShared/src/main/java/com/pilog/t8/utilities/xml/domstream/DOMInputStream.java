package com.pilog.t8.utilities.xml.domstream;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class DOMInputStream
{
    private XMLEventReader reader;
    private InputStream inputStream;

    public DOMInputStream()
    {
    }

    public void open(File inputFile) throws XMLStreamException, FileNotFoundException
    {
        open(new FileInputStream(inputFile));
    }

    public void open(InputStream inputStream) throws XMLStreamException, FileNotFoundException
    {
        XMLInputFactory factory;

        factory = XMLInputFactory.newInstance();
        inputStream = new BufferedInputStream(inputStream);
        reader = factory.createXMLEventReader(inputStream);
    }

    public void closeSilently()
    {
        try {close();}
        catch (XMLStreamException | IOException ex) {}
    }

    public void close() throws XMLStreamException, IOException
    {
        if (reader != null) reader.close();
        if (inputStream != null) inputStream.close();
    }

    public Element readElement(String elementName) throws Exception
    {
        DocumentBuilderFactory dbFactory;
        DocumentBuilder docBuilder;
        Document newDocument;

        dbFactory = DocumentBuilderFactory.newInstance();
        docBuilder = dbFactory.newDocumentBuilder();
        newDocument = docBuilder.newDocument();
        return extractElement(newDocument, elementName);
    }

    private Element extractElement(Document document, String elementName) throws Exception
    {
        XMLEvent nextEvent;
        int eventType;

        // Loop through all of available events until the start of the specified element is found.
        while (reader.hasNext())
        {
            nextEvent = reader.peek();
            eventType = nextEvent.getEventType();
            if (eventType == XMLEvent.START_ELEMENT)
            {
                StartElement startElement;

                startElement = (StartElement)nextEvent;
                if (startElement.getName().getLocalPart().equals(elementName))
                {
                    return extractElement(document);
                }
                else
                {
                    // Consume the event, since it is not the required type for extraction.
                    reader.nextEvent();
                }
            }
            else
            {
                // Consume the event, since it is not the required type for extraction.
                reader.nextEvent();
            }
        }

        // No more elements to read.
        return null;
    }

    private Element extractElement(Document document) throws Exception
    {
        XMLEvent nextEvent;
        int nextEventType;
        int eventType;

        nextEvent = reader.nextEvent();
        eventType = nextEvent.getEventType();
        if (eventType == XMLEvent.START_ELEMENT)
        {
            Element newElement;
            Iterator<Attribute> attributeIterator;
            StartElement startElement;
            StringBuffer value;

            startElement = (StartElement)nextEvent;
            newElement = document.createElement(startElement.getName().getLocalPart());
            attributeIterator = startElement.getAttributes();
            value = new StringBuffer();

            // Add all non-namespace declared attributes.
            while (attributeIterator.hasNext())
            {
                Attribute attribute;

                attribute = attributeIterator.next();
                newElement.setAttribute(attribute.getName().getLocalPart(), attribute.getValue());
            }

            // Parse any remaining events that are part of this element's content.
            while ((nextEventType = reader.peek().getEventType()) != XMLEvent.END_ELEMENT)
            {
                if (nextEventType == XMLEvent.CHARACTERS)
                {
                    Characters charEvent;

                    nextEvent = reader.nextEvent();
                    charEvent = (Characters)nextEvent;
                    value.append(charEvent.getData());
                }
                else if (nextEventType == XMLEvent.ATTRIBUTE)
                {
                    newElement.appendChild(extractAttribute(document));
                }
                else if (nextEventType == XMLEvent.START_ELEMENT)
                {
                    newElement.appendChild(extractElement(document));
                }
            }

            // Add the text value of this element.
            if (value.toString().trim().length() > 0) newElement.setTextContent(value.toString());

            // Consume the END_ELEMENT event and then return the extracted element.
            nextEvent = reader.nextEvent();
            return newElement;
        }
        else throw new Exception("Invalid start of element: " + nextEvent.getLocation());
    }

    private Attr extractAttribute(Document document) throws Exception
    {
        XMLEvent nextEvent;
        int eventType;

        nextEvent = reader.nextEvent();
        eventType = nextEvent.getEventType();
        if (eventType == XMLEvent.ATTRIBUTE)
        {
            Attr newAttribute;
            Attribute attribute;

            attribute = (Attribute)nextEvent;
            newAttribute = document.createAttribute(attribute.getName().getLocalPart());
            newAttribute.setValue(attribute.getValue());
            return newAttribute;
        }
        else throw new Exception("Invalid start of attribute: " + nextEvent.getLocation());
    }
}
