package com.pilog.t8.commons.io;

import java.io.Closeable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Bouwer du Preez
 */
public interface RandomAccessStream extends DataOutput, DataInput, Closeable
{
    /**
     * Sets the data-pointer offset, measured from the beginning of this
     * stream, at which the next read or write occurs.  The offset may be
     * set beyond the end of the stream. Setting the offset beyond the end
     * of the Stream does not change the stream length.  The stream length will
     * change only by writing after the offset has been set beyond the end
     * of the stream.
     *
     * @param      pos   The offset position, measured in bytes from the
     *                   beginning of the stream, at which to set the data
     *                   pointer.
     * @exception  IOException  If <code>pos</code> is less than
     *                          <code>0</code> or if an I/O error occurs.
     */
    public void seek(long pos) throws IOException;
    
    /**
     * Returns the current offset in this stream.
     *
     * @return     The offset from the beginning of the stream, in bytes,
     *             at which the next read or write will occur.
     * @exception  IOException  if an I/O error occurs.
     */
    public long getDataPointer() throws IOException;
    
    /**
     * Reads up to <code>len</code> bytes of data from this stream into an
     * array of bytes. This method blocks until at least one byte of input
     * is available.
     * <p>
     * Although <code>RandomAccessStream</code> is not a subclass of
     * <code>InputStream</code>, this method behaves in exactly the
     * same way as the {@link InputStream#read(byte[], int, int)} method of
     * <code>InputStream</code>.
     *
     * @param      b     the buffer into which the data is read.
     * @param      off   the start offset in array <code>b</code>
     *                   at which the data is written.
     * @param      len   the maximum number of bytes read.
     * @return     the total number of bytes read into the buffer, or
     *             <code>-1</code> if there is no more data because the end of
     *             the stream has been reached.
     * @exception  IOException If the first byte cannot be read for any reason
     * other than end of stream, or if the random access stream has been closed,
     * or if some other I/O error occurs.
     * @exception  NullPointerException If <code>b</code> is <code>null</code>.
     * @exception  IndexOutOfBoundsException If <code>off</code> is negative,
     * <code>len</code> is negative, or <code>len</code> is greater than
     * <code>b.length - off</code>
     */
    public int read(byte b[], int off, int len) throws IOException;
    
    /**
     * Reads up to <code>b.length</code> bytes of data from this stream
     * into an array of bytes. This method blocks until at least one byte
     * of input is available.
     * <p>
     * Although <code>RandomAccessStream</code> is not a subclass of
     * <code>InputStream</code>, this method behaves in exactly the
     * same way as the {@link InputStream#read(byte[])} method of
     * <code>InputStream</code>.
     *
     * @param      b   the buffer into which the data is read.
     * @return     the total number of bytes read into the buffer, or
     *             <code>-1</code> if there is no more data because the end of
     *             this stream has been reached.
     * @exception  IOException If the first byte cannot be read for any reason
     * other than end of stream, or if the random access stream has been closed,
     * or if some other I/O error occurs.
     * @exception  NullPointerException If <code>b</code> is <code>null</code>.
     */
    public int read(byte b[]) throws IOException;
    
    /**
     * Returns the length of this stream.
     *
     * @return     The total length of this stream, measured in bytes.
     * @exception  IOException  if an I/O error occurs.
     */
    public long length() throws IOException;
}
