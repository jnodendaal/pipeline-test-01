/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.utilities.sql;

import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class BatchParameterizedString extends ParameterizedString
{
    private List<List<Object>> batchParameters = new ArrayList<List<Object>>();

    public BatchParameterizedString()
    {
    }

    public BatchParameterizedString(List<List<Object>> batchParameters)
    {
        this.batchParameters = batchParameters;
    }

    public BatchParameterizedString(String stringValue)
    {
        super(stringValue);
    }

    public BatchParameterizedString(String stringValue,
                                    List<Object> parameterValues)
    {
        super(stringValue, parameterValues);
    }

    public BatchParameterizedString(StringBuffer stringValue,
                                    List<Object> parameterValues)
    {
        super(stringValue, parameterValues);
    }

    /**
     * Take the current set of parameters, adds it to the batch list, and then clear the list for the next batch
     */
    public void addBatch()
    {
        if(!getParameterValues().isEmpty()) batchParameters.add(getParameterValues());
        setParameterValues(new ArrayList<Object>());
    }

    /**
     * Internally calls the addBatch() method, and adds the provided set of parameters to the current internal parameter value list
     * @param parameters
     */
    public void addBatch(Collection<Object> parameters)
    {
        addBatch();
        setParameterValues(new ArrayList<Object>(parameters));
    }

    public List<List<Object>> getBatchParameters()
    {
        List<List<Object>> currentBatchParameters;

        currentBatchParameters = new ArrayList<List<Object>>(batchParameters);
        if(!getParameterValues().isEmpty())
            currentBatchParameters.add(getParameterValues());
        return currentBatchParameters;
    }

    public boolean hasBatchParameters()
    {
        return !batchParameters.isEmpty();
    }
}
