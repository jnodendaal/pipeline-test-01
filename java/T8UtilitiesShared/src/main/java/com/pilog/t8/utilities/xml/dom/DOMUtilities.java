package com.pilog.t8.utilities.xml.dom;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Bouwer du Preez
 */
public class DOMUtilities
{
    public static void saveXML(Document xmldoc, String filePath, int indentNumber) throws TransformerConfigurationException, TransformerException, FileNotFoundException, UnsupportedEncodingException
    {
        TransformerFactory transformerFactory;
        Transformer transformer;
        StreamResult streamResult;
        PrintWriter printWriter;
        DOMSource domSource;

        // Create a new print writer to output the XML text using UTF-8 encoding.
        printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"));

        // Create a DOMSource from the supplied XML Document.
        domSource = new DOMSource(xmldoc);

        // Create a StreamResult to hold the resultant serialized XML.
        streamResult = new StreamResult(printWriter);

        // Create a new TransformerFactory and use it to construct a Transformer (serializer).
        transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 3);
        transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, streamResult);

        printWriter.flush();
        printWriter.close();
    }

    public static final String getXMLString(Node domNode, boolean indent, int indentNumber, boolean omitXMLDeclaration) throws Exception
    {
        TransformerFactory transformerFactory;
        Transformer transformer;
        StringWriter stringWriter;

        transformerFactory = TransformerFactory.newInstance();
        if (indent) transformerFactory.setAttribute("indent-number", indentNumber);

        transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, indent ? "yes" : "no");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, omitXMLDeclaration ? "yes" : "no");
        stringWriter = new StringWriter();

        transformer.transform(new DOMSource(domNode), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

    public static final String formatXMLString(String xmlDocumentString, boolean indent, int indentNumber, boolean omitXMLDeclaration) throws Exception
    {
        TransformerFactory tranformerFactory;
        Transformer transformer;
        StringWriter stringWriter;

        tranformerFactory = TransformerFactory.newInstance();
        if (indent) tranformerFactory.setAttribute("indent-number", indentNumber);

        transformer = tranformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, indent ? "yes" : "no");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, omitXMLDeclaration ? "yes" : "no");
        stringWriter = new StringWriter();

        transformer.transform(new StreamSource(new StringReader(xmlDocumentString)), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

    public static final Document parseXMLDocumentFromString(String xmlDocumentString, ErrorHandler errorHandler) throws Exception
    {
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;
        InputSource inputSource;

        // Create a document builder factory and set its properties.
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setSchema(null);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);

        // Create a new document builder using the factory and use it to parse the file content.
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(errorHandler == null ? new XMLSAXErrorHandler() : errorHandler);
        inputSource = new InputSource(new StringReader(xmlDocumentString));
        inputSource.setEncoding("UTF-8");
        return documentBuilder.parse(inputSource);
    }

    public static final Document parseXMLDocumentFromFile(String filePath, boolean namespaceAware, ErrorHandler errorHandler) throws Exception
    {
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;
        InputSource inputSource;

        // Create a document builder factory and set its properties.
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setNamespaceAware(namespaceAware);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);

        // Create a new document builder using the factory and use it to parse the file content.
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(errorHandler == null ? new XMLSAXErrorHandler() : errorHandler);
        inputSource = new InputSource(filePath);
        inputSource.setEncoding("UTF-8");
        return documentBuilder.parse(inputSource);
    }

    public static final Document parseXMLDocumentFromFile(String filePath, ErrorHandler errorHandler) throws Exception
    {
        return parseXMLDocumentFromFile(filePath, true, errorHandler);
    }

    public static final Document parseXMLDocumentFromResource(String resourcePath, ErrorHandler errorHandler) throws Exception
    {
        return parseXMLDocumentFromResource(resourcePath, true, errorHandler);
    }

    public static final Document parseXMLDocumentFromResource(String resourcePath, boolean namespaceAware, ErrorHandler errorHandler) throws Exception
    {
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;

        // Create a document builder factory and set its properties.
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setNamespaceAware(namespaceAware);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);

        // Create a new document builder using the factory and use it to parse the file content.
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(errorHandler == null ? new XMLSAXErrorHandler() : errorHandler);
        return documentBuilder.parse(DOMUtilities.class.getResourceAsStream(resourcePath));
    }

    public static final Document parseXMLDocumentFromInputStream(InputStream inputStream, ErrorHandler errorHandler) throws Exception
    {
        return parseXMLDocumentFromInputStream(inputStream, true, errorHandler);
    }

    public static final Document parseXMLDocumentFromInputStream(InputStream inputStream, boolean namespaceAware, ErrorHandler errorHandler) throws Exception
    {
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;

        // Create a document builder factory and set its properties.
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setNamespaceAware(namespaceAware);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);

        // Create a new document builder using the factory and use it to parse the file content.
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(errorHandler == null ? new XMLSAXErrorHandler() : errorHandler);
        return documentBuilder.parse(inputStream);
    }

    public static final Document parseXMLDocumentFromInputSource(InputSource inputSource, ErrorHandler errorHandler) throws Exception
    {
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;

        // Create a document builder factory and set its properties.
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);

        // Create a new document builder using the factory and use it to parse the file content.
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(errorHandler == null ? new XMLSAXErrorHandler() : errorHandler);
        return documentBuilder.parse(inputSource);
    }

    private static class XMLSAXErrorHandler implements ErrorHandler
    {
        @Override
        public void warning(SAXParseException e) throws SAXException
        {
            show("Warning", e);
            throw (e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException
        {
            show("Error", e);
            throw (e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException
        {
            show("Fatal Error", e);
            throw (e);
        }

        private void show(String type, SAXParseException e)
        {
            System.out.println(type + ": " + e.getMessage());
            System.out.println("Line " + e.getLineNumber() + " Column " + e.getColumnNumber());
            System.out.println("System ID: " + e.getSystemId());
        }
    }

    public static final void printDocument(Document document)
    {
        printElement(document.getDocumentElement());
    }

    public static final void printElement(Element element)
    {
        ArrayList<Element> childElements;
        StringBuffer text;
        int level;

        text = new StringBuffer();
        level = DOMHandler.getElementLevel(element);
        for (int l = 0; l < level-1; l++)
        {
            text.append(" ");
        }
        text.append("-");
        text.append(element.getLocalName());
        text.append(" ");
        text.append(DOMHandler.getAttributeValues(element).toString());
        System.out.println(text.toString());

        childElements = DOMHandler.getChildElements(element);
        for (Element childElement : childElements)
        {
            printElement(childElement);
        }
    }
}
