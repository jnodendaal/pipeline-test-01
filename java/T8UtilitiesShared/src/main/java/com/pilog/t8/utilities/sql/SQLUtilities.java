package com.pilog.t8.utilities.sql;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * @author Bouwer du Preez
 */
public class SQLUtilities
{
    /**
     * This method builds a StringBuffer of comma separated '?' characters that
     * can be used in an SQL query/statement String where a list of parameters
     * will be used (such as an IN clause).
     *
     * @param variableList The list of variables that will be used to build the
     * CSV String.
     * @return A CSV StringBuffer in the format "var1, var2, var3, var4m .. varx";
     */
    public static final StringBuffer buildCSVParameterString(int count)
    {
        StringBuffer csvString;

        // Create the new CSV String and add all variable followed by a comma to it.
        csvString = new StringBuffer();
        for (int index = 0; index < count - 1; index++)
        {
            csvString.append("?, ");
        }

        csvString.append("?");

        return csvString;
    }

    /**
     * This method takes a data row that has column names of the format
     * 'tableName.columnName' and removes the 'tableName.' part from all of the
     * column names.
     *
     * @param dataRow The data row to format.
     * @return The formatted data row as a HashMap consisting of
     * (columnName, columnValue) pairs.
     */
    public static HashMap removeTableNameFromDataRow(HashMap dataRow)
    {
        HashMap<String, Object> formattedDataRow;
        Iterator hashIterator;

        formattedDataRow = new HashMap<>();
        hashIterator = dataRow.keySet().iterator();
        while (hashIterator.hasNext())
        {
            String key;

            key = (String)hashIterator.next();
            formattedDataRow.put(key.substring(key.indexOf('.') + 1), dataRow.get(key));
        }

        return formattedDataRow;
    }

    /**
     * Constructs and SQL WHERE clause from the supplied data row.  The SQL
     * LIKE operator is used in the WHERE clause.
     *
     * @param dataRow A HashMap containing the data from which the WHERE clause
     * will be constructed.  The HashMap must contain (columnName, columnValue)
     * pairs.
     * @param caseSensitive Boolean value to indicate whether or not the WHERE clause
     * should take the case of column values into account.
     * @return The newly constructed SQL WHERE clause.
     */
    public static String buildLikeWhereClause(HashMap dataRow, boolean caseSensitive)
    {
        String whereClause;
        Iterator hashIterator;

        whereClause = "WHERE ";
        hashIterator = dataRow.keySet().iterator();
        while (hashIterator.hasNext())
        {
            String columnName;
            Object columnValue;

            columnName = (String)hashIterator.next();
            columnValue = dataRow.get(columnName);
            whereClause += caseSensitive ? columnName : "UPPER(" + columnName + ")";

            if (columnValue == null)
            {
                whereClause += " IS NULL";
            }
            else
            {
                whereClause += " LIKE '" + columnValue + "'";
            }

            if (hashIterator.hasNext())
            {
                whereClause += " AND ";
            }
        }

        return whereClause;
    }

    /**
     * Constructs and SQL WHERE clause from the supplied data row.
     *
     * @param dataRow A HashMap containing the data from which the WHERE clause
     * will be constructed.  The HashMap must contain (columnName, columnValue)
     * pairs.
     * @param caseSensitive Boolean value to indicate whether or not the WHERE clause
     * should take the case of column values into account.
     * @return The newly constructed SQL WHERE clause.
     */
    public static String buildWhereClause(HashMap dataRow, boolean caseSensitive)
    {
        String whereClause;
        Iterator hashIterator;

        whereClause = "WHERE ";
        hashIterator = dataRow.keySet().iterator();
        while (hashIterator.hasNext())
        {
            String columnName;
            Object columnValue;

            columnName = (String)hashIterator.next();
            columnValue = dataRow.get(columnName);
            whereClause += caseSensitive ? columnName : "UPPER(" + columnName + ")";

            if (columnValue == null)
            {
                whereClause += " IS NULL";
            }
            else if (columnValue instanceof String)
            {
                whereClause += " = '" + columnValue + "'";
            }
            else
            {
                whereClause += " = " + columnValue;
            }

            if (hashIterator.hasNext())
            {
                whereClause += " AND ";
            }
        }

        return whereClause;
    }

    /**
     * Constructs and SQL WHERE clause from the supplied parameters.
     *
     * @param dataRow A HashMap containing the data from which the WHERE clause
     * will be constructed.  The HashMap must contain (columnName, columnValue)
     * pairs.
     * @param primaryKeyList An ArrayList specifying the primary key columns in
     * the data row.
     * @param caseSensitive Boolean value to indicate whether or not the WHERE clause
     * should take the case of column values into account.
     * @return The newly constructed SQL WHERE clause.
     */
    public static String buildWhereClause(HashMap dataRow, ArrayList<String> primaryKeyList, boolean caseSensitive)
    {
        String whereClause;

        whereClause = "WHERE ";
        for (int i = 0; i < primaryKeyList.size(); i++)
        {
            String columnName;
            Object columnValue;

            columnName = primaryKeyList.get(i);
            columnValue = dataRow.get(columnName);
            whereClause += caseSensitive ? columnName : "UPPER(" + columnName + ")";

            if (columnValue == null)
            {
                whereClause += " IS NULL";
            }
            else if (columnValue instanceof String)
            {
                whereClause += " = '" + columnValue + "'";
            }
            else
            {
                whereClause += " = " + columnValue;
            }

            if (i < primaryKeyList.size() - 1)
            {
                whereClause += " AND ";
            }
        }

        return whereClause;
    }

    /**
     * Constructs and SQL WHERE clause from the supplied parameters.  The
     * resultant WHERE clause will be parameterized using the '?' symbol so that
     * it is suitable for use in a PreparedStatement.
     *
     * @param primaryKeyList An ArrayList specifying the primary key columns in
     * the data row.
     * @param caseSensitive Boolean value to indicate whether or not the WHERE clause
     * should take the case of column values into account.
     * @return The newly constructed SQL WHERE clause.
     */
    public static String buildWhereClause(ArrayList<String> primaryKeyList, boolean caseSensitive)
    {
        StringBuffer whereClause;

        whereClause = new StringBuffer();
        whereClause.append("WHERE ");
        for (int primaryKeyIndex = 0; primaryKeyIndex < primaryKeyList.size(); primaryKeyIndex++)
        {
            whereClause.append(caseSensitive ? primaryKeyList.get(primaryKeyIndex) : "UPPER(" + primaryKeyList.get(primaryKeyIndex) + ")");
            whereClause.append("=?");
            if (primaryKeyIndex < primaryKeyList.size() - 1) whereClause.append(" AND ");
        }

        return whereClause.toString();
    }

    /**
     * Constructs a SQL WHERE clause from the supplied parameters.
     *
     * @param parentDataRow The data row from which to obtain the filter values.
     * This HashMap must consist of (columnName, columnValue) pairs.
     * @param relationshipColumns The HashMap from which to obtain the mapping
     * of parent columns to child columns.  The HashMap must contain
     * (parentColumn, childColumn) pairs.
     * @return The constructed SQL WHERE clause.  Returns null, if either of the
     * supplied parameters are null or empty.
     */
    public static String buildWhereClause(HashMap parentDataRow, HashMap relationshipColumns, boolean caseSensitive) throws Exception
    {
        Iterator hashIterator;
        String whereClause;

        // Check that the parameters are not null.
        if ((parentDataRow == null) || (parentDataRow.size() == 0))
        {
            String message;

            message = "A WHERE clause could not be constructed, because an empty data row was supplied.";
            throw new Exception(message);
        }

        // Check that the parameter HashMap contain data.
        if ((relationshipColumns == null) || (relationshipColumns.isEmpty()))
        {
            String message;

            message = "A WHERE clause could not be constructed, because an invalid relationship column definition was supplied.";
            throw new Exception(message);
        }

        // Construct the WHERE clause.
        whereClause = "WHERE ";
        hashIterator = relationshipColumns.keySet().iterator();
        while (hashIterator.hasNext())
        {
            String parentColumn;
            String childColumn;
            Object parentColumnValue;

            parentColumn = (String)hashIterator.next();
            childColumn = (String)relationshipColumns.get(parentColumn);
            parentColumnValue = parentDataRow.get(parentColumn);

            whereClause += caseSensitive ? childColumn : "UPPER(" + childColumn + ")";
            if (parentColumnValue == null)
            {
                whereClause += " IS NULL";
            }
            else if (parentColumnValue instanceof String)
            {
                whereClause += " = '" + parentColumnValue + "'";
            }
            else
            {
                whereClause += " = " + parentColumnValue;
            }
            whereClause += hashIterator.hasNext() ? " AND " : "";
        }

        return whereClause;
    }

    /**
     * Constructs a SQL WHERE clause from the supplied parameters.  The
     * resultant WHERE clause makes use of the SQL IN condition.
     *
     * @param parentDataRows The data rows from which to obtain the filter
     * values.  The HashMap must consist of (columnName, columnValue) pairs.
     * @param relationshipColumns The HashMap from which to obtain the mapping
     * of parent columns to child columns.  The HashMap must contain
     * (parentColumn, childColumn) pairs.
     * @return The constructed SQL WHERE clause.  Returns null, if either of the
     * supplied parameters are null or empty.
     */
    public static String buildWhereClause(ArrayList<HashMap> parentDataRows, HashMap relationshipColumns) throws Exception
    {
        Iterator<String> mapIterator;
        String whereClause;

        // Check that the parameters are not null.
        if ((parentDataRows == null) || (parentDataRows.size() == 0))
        {
            String message;

            message = "A WHERE clause could not be constructed, because an empty data row was supplied.";
            throw new Exception(message);
        }

        // Check that the parameter HashMap contain data.
        if ((relationshipColumns == null) || (relationshipColumns.size() == 0))
        {
            String message;

            message = "A WHERE clause could not be constructed, because an invalid relationship column definition was supplied.";
            throw new Exception(message);
        }

        // Construct the WHERE clause.
        whereClause = "WHERE ";
        mapIterator = relationshipColumns.keySet().iterator();
        while (mapIterator.hasNext())
        {
            String parentColumn;
            String childColumn;
            Iterator<HashMap> listIterator;

            parentColumn = mapIterator.next();
            childColumn = (String)relationshipColumns.get(parentColumn);

            whereClause += (childColumn + " IN (");
            listIterator = parentDataRows.iterator();
            while (listIterator.hasNext())
            {
                HashMap parentDataRow;
                Object parentColumnValue;

                parentDataRow = removeTableNameFromDataRow(listIterator.next());
                parentColumnValue = parentDataRow.get(parentColumn);

                if (parentColumnValue instanceof Number)
                {
                    whereClause += parentColumnValue.toString();
                }
                else
                {
                    whereClause += "'" + parentColumnValue + "'";
                }

                whereClause += listIterator.hasNext() ? ", " : ")";
            }

            whereClause += mapIterator.hasNext() ? " AND " : "";
        }

        return whereClause;
    }

    /**
     * Constructs a SQL WHERE clause from the supplied parameters.  The
     * resultant WHERE clause makes use of the SQL LIKE condition instead of a
     * straight '=' operator to allow for different data types.
     *
     * @param parentDataRow The data row from which to obtain the filter values.
     * This HashMap must consist of (columnName, columnValue) pairs.
     * @param relationship1Columns The HashMap from which to obtain the mapping
     * of parent columns (table 1) to child columns (link table).  The HashMap
     * must contain (parentColumn, childColumn) pairs.
     * @param linkTableName The name of the link table to use.
     * @param relationship2Columns The HashMap from which to obtain the mapping
     * of parent columns (link table) to child columns (table 2).  The HashMap
     * must contain (parentColumn, childColumn) pairs.
     * @return The constructed SQL WHERE clause.  Returns null, if either of the
     * supplied parameters are null or empty.
     */
    public static String buildLinkWhereClause(HashMap parentDataRow, HashMap relationship1Columns, String linkTableName, HashMap relationship2Columns, boolean caseSensitive) throws Exception
    {
        String whereClause;
        Iterator hashIterator;

        // Construct the WHERE clause.
        whereClause = "WHERE ";
        hashIterator = relationship2Columns.keySet().iterator();
        while (hashIterator.hasNext())
        {
            String parentColumn;
            String childColumn;
            String innerSelect;

            parentColumn = (String)hashIterator.next();
            childColumn = (String)relationship2Columns.get(parentColumn);

            // Construct the inner-SELECT clause.
            innerSelect = "SELECT " + parentColumn + " FROM " + linkTableName + " " + buildWhereClause(parentDataRow, relationship1Columns, caseSensitive);

            // Concatenate the inner-SELECT to the WHERE clause.
            whereClause += ("(" + childColumn + " IN (" + innerSelect + "))");
            whereClause += hashIterator.hasNext() ? " AND " : "";
        }

        return whereClause;
    }

    /**
     * This method builds an SQL INSERT statement for the specified table name
     * and data row.
     *
     * @param tableName The name of the database table into which the data will
     * be inserted.
     * @param dataRow The data row from which to create the insert statement.
     * @return The newly constructed SQL INSERT statement.
     */
    public static String buildInsertStatement(String tableName, HashMap dataRow)
    {
        if (dataRow != null)
        {
            String insertScript;
            Iterator hashIterator;
            ArrayList columnValues;

            insertScript = "INSERT INTO " + tableName + "\n(";

            columnValues = new ArrayList();
            hashIterator = dataRow.keySet().iterator();
            while (hashIterator.hasNext())
            {
                String columnName;

                columnName = (String)hashIterator.next();
                columnValues.add(dataRow.get(columnName));

                insertScript += columnName;
                if (hashIterator.hasNext())
                {
                    insertScript += ", ";
                }
            }

            insertScript += ")\nVALUES (";
            for (int i = 0; i < columnValues.size(); i++)
            {
                Object columnValue;

                columnValue = columnValues.get(i);
                if (columnValue == null)
                {
                    insertScript += "NULL";
                }
                else if (columnValue instanceof String)
                {
                    insertScript += "'" + columnValue + "'";
                }
                else
                {
                    insertScript += columnValue;
                }

                if (i < columnValues.size() - 1)
                {
                    insertScript += ", ";
                }
            }

            insertScript += ")";

            return insertScript;
        }
        else
        {
            return new String();
        }
    }

    /**
     * This method builds an SQL UPDATE statement for the specified table name,
     * data row and primary keys.
     *
     * @param tableName The name of the database table into which the data will
     * be inserted.
     * @param dataRow The data row from which to create the insert statement.
     * @param primaryKeyList An ArrayList containing all of the primary key
     * column names to use for the WHERE clause of the UPDATE statement.
     * @return The newly constructed SQL UPDATE statement.
     */
    public static String buildUpdateStatement(String tableName, HashMap dataRow, ArrayList<String> primaryKeyList)
    {
        if (dataRow != null)
        {
            String updateScript;
            Iterator hashIterator;

            // Initialize the UPDATE statement String.
            updateScript = "UPDATE " + tableName + "\nSET ";

            hashIterator = dataRow.keySet().iterator();
            while (hashIterator.hasNext())
            {
                String columnName;
                Object columnValue;

                columnName = (String)hashIterator.next();
                columnValue = dataRow.get(columnName);
                if (columnValue == null)
                {
                    updateScript += columnName + "=NULL";
                }
                else if (columnValue instanceof String)
                {
                    updateScript += columnName + "='" + columnValue + "'";
                }
                else
                {
                    updateScript += columnName + "=" + columnValue;
                }

                if (hashIterator.hasNext())
                {
                    updateScript += ", ";
                }
            }

            // Add the WHERE clause to the UPDATE statement.
            updateScript += " " + buildWhereClause(dataRow, primaryKeyList, true);

            return updateScript;
        }
        else
        {
            return new String();
        }
    }

    /**
     * This method builds an SQL UPDATE statement for the specified table name,
     * column names and primary keys.  The resultant update statement will be
     * parameterized using the '?' symbol so that it is suitable for use in a
     * PreparedStatement.
     *
     * @param tableName The name of the database table into which the data will
     * be inserted.
     * @param columnNameList An ArrayList containing all of the column names to
     * be updated.
     * @param primaryKeyList An ArrayList containing all of the primary key
     * column names to use for the WHERE clause of the UPDATE statement.
     * @return The newly constructed SQL UPDATE statement.
     */
    public static String buildUpdateStatement(String tableName, ArrayList<String> columnNameList, ArrayList<String> primaryKeyList)
    {
        StringBuffer updateStatement;

        // Initialize the UPDATE statement StringBuffer.
        updateStatement = new StringBuffer();
        updateStatement.append("UPDATE ");
        updateStatement.append(tableName);
        updateStatement.append(" SET ");

        for (int columnIndex = 0; columnIndex < columnNameList.size(); columnIndex++)
        {
            updateStatement.append(columnNameList.get(columnIndex));
            updateStatement.append("=?");
            if (columnIndex < columnNameList.size() -1) updateStatement.append(", ");
        }

        // Add the WHERE clause to the UPDATE statement.
        updateStatement.append(buildWhereClause(primaryKeyList, true));

        return updateStatement.toString();
    }

    /**
     * Constructs an SQL ORDER BY clause from the supplied list of column names.
     *
     * @param orderByColumns The list of column names that will be included in
     * the ORDER BY clause.
     * @return The constructed clause in the form 'ORDER BY column1, column2...'
     * or an empty String if no columns were supplied.
     */
    public static final String buildOrderByClause(ArrayList<String> orderByColumns)
    {
        if ((orderByColumns != null) && (orderByColumns.size() > 0))
        {
            String orderByClause;

            orderByClause = "ORDER BY " + orderByColumns.get(0);
            for (int index = 1; index < orderByColumns.size(); index++)
            {
                orderByClause += (", " + orderByColumns.get(index));
            }

            return orderByClause;
        }
        else
        {
            return new String();
        }
    }

    /**
     * This method converts the supplied String value into a Java Type object
     * that corresponds to the specified JDBC SQL type.
     *
     * SQL Type     -> Java Type
     * -------------------------
     * CHAR         -> String
     * VARCHAR      -> String
     * LONGVARCHAR  -> String
     * NUMERIC      -> BigDecimal
     * DECIMAL      -> BigDecimal
     * BIT          -> boolean
     * TINYINT      -> byte
     * SMALLINT     -> short
     * INTEGER      -> int
     * BIGINT       -> long
     * REAL         -> float
     * FLOAT        -> double
     * DOUBLE       -> double
     * BINARY       -> byte[]
     * VARBINARY    -> byte[]
     * DATE         -> java.sql.Date
     * TIME         -> java.sql.Time
     * TIMESTAMP    -> java.sql.TimeStamp
     * CLOB         -> java.sql.Clob
     * BLOB         -> java.sql.Blob
     *
     * @param stringValue The String value to parse.
     * @param sqlType The java.sql.Types constant that will determine the type
     * to which the String value will be converted.
     * @return An Object of a type that is consistent with the specified SQL
     * type containing the parsed value of the supplied String.
     */
    public static final Object parseSQLTypeString(String stringValue, int sqlType)
    {
        if (stringValue == null)
        {
            return null;
        }
        else if (stringValue.length() == 0)
        {
            return null;
        }
        else if (sqlType == java.sql.Types.CHAR)
        {
            return new String(stringValue);
        }
        else if (sqlType == java.sql.Types.VARCHAR)
        {
            return new String(stringValue);
        }
        else if (sqlType == java.sql.Types.LONGVARCHAR)
        {
            return new String(stringValue);
        }
        else if (sqlType == java.sql.Types.NUMERIC)
        {
            return new BigDecimal(stringValue);
        }
        else if (sqlType == java.sql.Types.DECIMAL)
        {
            return new BigDecimal(stringValue);
        }
        else if (sqlType == java.sql.Types.BIT)
        {
            return (stringValue.equalsIgnoreCase("1") || stringValue.equalsIgnoreCase("Y"));
        }
        else if (sqlType == java.sql.Types.TINYINT)
        {
            return Byte.parseByte(stringValue);
        }
        else if (sqlType == java.sql.Types.SMALLINT)
        {
            return Short.parseShort(stringValue);
        }
        else if (sqlType == java.sql.Types.INTEGER)
        {
            return Integer.parseInt(stringValue);
        }
        else if (sqlType == java.sql.Types.BIGINT)
        {
            return Long.parseLong(stringValue);
        }
        else if (sqlType == java.sql.Types.REAL)
        {
            return Float.parseFloat(stringValue);
        }
        else if (sqlType == java.sql.Types.FLOAT)
        {
            return Double.parseDouble(stringValue);
        }
        else if (sqlType == java.sql.Types.DOUBLE)
        {
            return Double.parseDouble(stringValue);
        }
        else
        {
            return stringValue;
        }
    }

    /**
     * This method is a best-effort attempt to remove characters from a
     * {@code String} which aren't accepted by all database engines and convert
     * it to a value compatible with DB engines.<br/>
     * <br/>
     * This is mainly intended for use when generic table and column names are
     * to be generated for a process.
     *
     * @param stringToConvert The {@code String} to convert to a SQL compatible
     *      version
     *
     * @return The modified {@code String}, or the original if the string was
     *      found to be a non-issue
     */
    public static String getSQLCompatibleString(String stringToConvert)
    {
        String compatibleString;

        compatibleString = stringToConvert.replaceAll("[^A-z0-9]", "_").replaceAll("__", "_");

        if (compatibleString.endsWith("_"))
        {
            compatibleString = compatibleString.substring(0, compatibleString.length() - 1);
        }

        return compatibleString.toUpperCase();
    }
}
