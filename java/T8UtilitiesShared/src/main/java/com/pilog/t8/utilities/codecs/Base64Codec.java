package com.pilog.t8.utilities.codecs;

import java.io.UnsupportedEncodingException;

/**
 * @author Bouwer du Preez
 */
public class Base64Codec
{
    // Mapping table from 6-bit nibbles to Base64 characters.
    private static char[] toBase64Map = new char[64];

    // Mapping table from Base64 characters to 6-bit nibbles.
    private static byte[] fromBase64Map = new byte[128];

    // Initialize the static maps.
    static
    {
        buildToBase64Map();
        buildFromBase64Map();
    }

    /**
     * Constructor for the class Base64Codec.
     */
    private Base64Codec() {}

    /**
     * Constructs a data table to map 6-bit nibbles to Base64 characters.
     */
    private static final void buildToBase64Map()
    {
        int i = 0;

        // Add all capital letters to the map.
        for (char c = 'A'; c <= 'Z'; c++)
        {
            toBase64Map[i++] = c;
        }

        // Add all lower case letters to the map.
        for (char c = 'a'; c <= 'z'; c++)
        {
            toBase64Map[i++] = c;
        }

        // Add all digits to the map.
        for (char c = '0'; c <= '9'; c++)
        {
            toBase64Map[i++] = c;
        }

        // Add all allowed special characters to the map.
        toBase64Map[i++] = '+';
        toBase64Map[i++] = '/';
    }

    /**
     * Constructs a data table to map Base64 characters to 6-bit nibbles.
     */
    private static final void buildFromBase64Map()
    {
        // Initialize the entire map to -1 values.
        for (int i = 0; i < fromBase64Map.length; i++)
        {
            fromBase64Map[i] = -1;
        }

        // Add the byte value of each of all the allowed string characters to the map.
        for (int i = 0; i < 64; i++)
        {
            fromBase64Map[toBase64Map[i]] = (byte)i;
        }
    }

    /**
     * Encodes a string into Base64 format.
     * No blanks or line breaks are inserted.
     *
     * @param s  a String to be encoded.
     * @return   A String with the Base64 encoded data.
     */
    public static final String encodeString(String s)
    {
        return new String(encode(s.getBytes()));
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     *
     * @param inputCharacters  an array containing the data bytes to be encoded.
     * @return    A character array with the Base64 encoded data.
     */
    public static final char[] encode(byte[] in)
    {
        return encode(in, in.length);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     *
     * @param inputCharacters   an array containing the data bytes to be encoded.
     * @param inputLength number of bytes to process in <code>in</code>.
     * @return A character array with the Base64 encoded data.
     */
    public static final char[] encode(byte[] inputArray, int inputLength)
    {
        int oDataLen;
        int oLen;
        char[] out;
        int inputIndex;
        int outputIndex;

        // Calculate the data lengths and create a character array to hold the output data.
        oDataLen = (inputLength * 4 + 2) / 3; // Output length without padding.
        oLen = ((inputLength + 2) / 3) * 4; // Output length including padding.
        out = new char[oLen];

        // Loop while the input index is in the valid range.
        inputIndex = 0;
        outputIndex = 0;
        while (inputIndex < inputLength)
        {
            int inputByte1;
            int intputByte2;
            int inputByte3;
            int outputByte1;
            int outputByte2;
            int outputByte3;
            int outputByte4;

            // Get the next three input bytes.
            inputByte1 = inputArray[inputIndex++] & 0xff;
            intputByte2 = inputIndex < inputLength ? inputArray[inputIndex++] & 0xff : 0;
            inputByte3 = inputIndex < inputLength ? inputArray[inputIndex++] & 0xff : 0;

            // Expand the three input bytes into four output bytes.
            outputByte1 = inputByte1 >>> 2; // 6 bits from the first byte.
            outputByte2 = ((inputByte1 & 3) << 4) | (intputByte2 >>> 4); // 2 bits from the first byte and 4 bits from the second.
            outputByte3 = ((intputByte2 & 0xf) << 2) | (inputByte3 >>> 6); // 4 bits from the second byte and 2 from the third.
            outputByte4 = inputByte3 & 0x3F; // 6 bits from the third byte.

            // For each output byte, retrieve the mapped character and add the character to the output array.
            out[outputIndex++] = toBase64Map[outputByte1];
            out[outputIndex++] = toBase64Map[outputByte2];
            out[outputIndex] = outputIndex < oDataLen ? toBase64Map[outputByte3] : '=';
            outputIndex++;
            out[outputIndex] = outputIndex < oDataLen ? toBase64Map[outputByte4] : '=';
            outputIndex++;
        }

        // Return the output character array.
        return out;
    }

    /**
     * Decodes a string from Base64 format.
     *
     * @param s A Base64 String to be decoded.
     * @param charSetName The name of a supported {@linkplain java.nio.charset.Charset}
     * @return A String containing the decoded data.
     * @throws IllegalArgumentException if the input is not valid Base64 encoded data.
     * @throws UnsupportedEncodingException
     */
    public static final String decodeString(String s, String charSetName) throws UnsupportedEncodingException
    {
        return new String(decode(s), charSetName);
    }

    /**
     * Decodes a byte array from Base64 format.
     *
     * @param s A Base64 String to be decoded.
     * @return An array containing the decoded data bytes.
     * @throws IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public static final byte[] decode(String s)
    {
        return decode(s.toCharArray());
    }

    /**
     * Decodes a byte array from Base64 format.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     *
     * @param inputCharacters A character array containing the Base64 encoded data.
     * @return An array containing the decoded data bytes.
     * @throws IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public static final byte[] decode(char[] inputCharacters)
    {
        int inputLength;
        int outputLength;
        int inputIndex;
        int outputIndex;
        byte[] outputArray;

        // Get the length of the input character array and check that it is a multiple of 4.
        inputLength = inputCharacters.length;
        if ((inputLength % 4) != 0)
        {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }

        // Determine the length of the actual encoded data in the input array (minus the padding characters).
        while ((inputLength > 0) && (inputCharacters[inputLength - 1] == '='))
        {
            inputLength--;
        }

        // Calculate the length of the output array and initialize the array.
        outputLength = (inputLength * 3) / 4;
        outputArray = new byte[outputLength];

        // Loop while the input index is within the valid range.
        inputIndex = 0;
        outputIndex = 0;
        while (inputIndex < inputLength)
        {
            int inputCharacter1;
            int inputCharacter2;
            int inputCharacter3;
            int inputCharacter4;
            int byte1;
            int byte2;
            int byte3;
            int byte4;

            // Read the next four input characters.
            inputCharacter1 = inputCharacters[inputIndex++];
            inputCharacter2 = inputCharacters[inputIndex++];
            inputCharacter3 = inputIndex < inputLength ? inputCharacters[inputIndex++] : 'A';
            inputCharacter4 = inputIndex < inputLength ? inputCharacters[inputIndex++] : 'A';

            // Check that all four input characters fall within the correct ASCII range.
            if (inputCharacter1 > 127 || inputCharacter2 > 127 || inputCharacter3 > 127 || inputCharacter4 > 127)
            {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }

            // Get the byte value of each input character from the map.
            byte1 = fromBase64Map[inputCharacter1];
            byte2 = fromBase64Map[inputCharacter2];
            byte3 = fromBase64Map[inputCharacter3];
            byte4 = fromBase64Map[inputCharacter4];

            // Check that all four bytes are valid (invalid input characters will result in a byte value of -1 retrieved from the map).
            if (byte1 < 0 || byte2 < 0 || byte3 < 0 || byte4 < 0)
            {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            else
            {
                int outputByte1;
                int outputByte2;
                int outputByte3;

                // Use 6 bits from each byte (24 bits total) and shift them into 3 resultant bytes.
                outputByte1 = (byte1 << 2) | (byte2 >>> 4); // 6 bits from the first byte and 2 bits from the second.
                outputByte2 = ((byte2 & 0xf) << 4) | (byte3 >>> 2); // 4 bits from the second byte and 4 bits from the third.
                outputByte3 = ((byte3 & 3) << 6) | byte4; // 2 bits from the third byte and 6 bits from the fourth.

                // Add the three output bytes to the output byte array.
                outputArray[outputIndex++] = (byte)outputByte1;
                if (outputIndex < outputLength)
                {
                    outputArray[outputIndex++] = (byte)outputByte2;
                }
                if (outputIndex < outputLength)
                {
                    outputArray[outputIndex++] = (byte)outputByte3;
                }
            }
        }

        // Return the resultant output byte array.
        return outputArray;
    }
}
