package com.pilog.t8.utilities.files.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author Bouwer du Preez
 */
public class TextOutputFile
{
    private File file;
    private String encoding;
    private OutputStream outputStream;
    private BufferedWriter outputWriter;

    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String ENCODING_UTF_16 = "UTF-16";

    public TextOutputFile(File file, String encoding)
    {
        this.file = file;
        this.outputStream = null;
        this.encoding = encoding;
    }

    public TextOutputFile(OutputStream fileOutputStream, String encoding)
    {
        this.file = null;
        this.outputStream = fileOutputStream;
        this.encoding = encoding;
    }

    public void openFile() throws Exception
    {
        if (file != null)
        {
            outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding));
        }
        else if (outputStream != null)
        {
            outputWriter = new BufferedWriter(new OutputStreamWriter(outputStream, encoding));
        }
        else throw new RuntimeException("No output stream or file set.");
    }

    public void write(String text) throws IOException
    {
        outputWriter.write(text);
    }

    public void writeLine(String text) throws IOException
    {
        outputWriter.write(text);
        outputWriter.newLine();
    }

    public void closeFile() throws Exception
    {
        if (outputWriter != null) outputWriter.close();
    }
}
