package com.pilog.t8.utilities.collections;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class HashMaps extends Maps
{
    /**
     * A convenience method to allow for instantiation of an empty map with an
     * initial capacity of 0.<br/>
     * <br/>
     * This map uses dynamic generics to allow for the implicit casting to the
     * required map type.
     *
     * @param <K> The dynamic key type
     * @param <V> The dynamic value type
     *
     * @return A new empty {@code HashMap} with an initial capacity of 0
     */
    public static <K, V> HashMap<K, V> emptyMap()
    {
        return new HashMap<>(0);
    }

    /**
     * Creates a new HashMap from the supplied parameters.  Starting from index
     * 0 in the supplied parameter list, each second parameter is added as a key
     * to the new map and each parameter succeeding a key is added as its value
     * in the map.
     *
     * @param values The values from which to create the new map.
     * @return The newly created map.
     *
     * @deprecated since 1.7.6
     */
    @Deprecated
    public static HashMap newHashMap(Object... values)
    {
        HashMap<Object, Object> newMap;

        newMap = new HashMap<>();
        for (int index = 0; index < values.length; index += 2)
        {
            newMap.put(values[index], values[index+1]);
        }

        return newMap;
    }

    /**
     * Creates a new HashMap from the supplied parameters.  Starting from index
     * 0 in the supplied parameter list, each second parameter is added as a key
     * to the new map and each parameter succeeding a key is added as its value
     * in the map.
     *
     * @param <K> The type of the HashMap keys.
     * @param <V> The type of the HashMap values.
     * @param values The values from which to create the new map.
     * @return The newly created map.
     *
     * @deprecated since 1.7.6
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    public static <K, V> HashMap<K, V> create(Object... values)
    {
        HashMap<K, V> newMap;

        newMap = new HashMap<>();
        for (int index = 0; index < values.length; index += 2)
        {
            newMap.put((K)values[index], (V)values[index+1]);
        }

        return newMap;
    }

    /**
     * Since we use many maps with a single key-value pair, this method is is
     * intended for these cases. The map created will have an initial size of
     * 1 and a load factor of 1.<br/>
     * <br/>
     * Inspection of the JDK 8 implementation of {@code HashMap} shows that the
     * map threshold will only increase once another item is added to the
     * map.<br/>
     * <br/>
     * If you are going to add more values to the map, look at
     * {@link #createTypeSafeMap(K[], V[])}, otherwise it completely defeats the
     * purpose of this method and its usage.
     *
     * @param <K> The {@code K} to be added
     * @param <V> The associated {@code V} value to be added
     *
     * @param key The key to be set for the map
     * @param value The value associated with the specific key to be set
     *
     * @return A {@code HashMap} with a size of 1, a load factor of 1, and a
     *      threshold of 1, containing the specified key-value pair
     */
    public static <K, V> HashMap<K, V> createSingular(K key, V value)
    {
        HashMap<K, V> singleValueMap;

        singleValueMap = new HashMap<>(1, 1);
        singleValueMap.put(key, value);

        return singleValueMap;
    }

    /**
     * Creates a new type-safe {@code HashMap}. They keys array is responsible
     * for managing the key type of the map, and the same with the varargs
     * values parameter. Each key should have a corresponding value.
     *
     * @param <K> The type of the keys in the returning map
     * @param <V> The type of the values in the returning map
     *
     * @param keys The keys of type {@code K} to add to the map
     * @param values The values of type {@code V} to add to map
     *
     * @return The new {@code HashMap<K, V>}
     */
    public static <K, V> HashMap<K, V> createTypeSafeMap(K[] keys, V[] values)
    {
        HashMap<K, V> typeSafeMap;

        typeSafeMap = new HashMap<>();
        for (int index = 0; index < keys.length; index++)
        {
            typeSafeMap.put(keys[index], values[index]);
        }

        return typeSafeMap;
    }

    /**
     * A null-safe map converter which will take a given map and create a new
     * {@code HashMap} using identical keys and convert the values to their
     * {@code String} equivalent.
     *
     * @param oldMap The original map with non-string value entries
     *
     * @return The {@code HashMap} containing the same keys, but {@code String}
     *      values
     */
    public static HashMap<String, String> toStringValueMap(Map<String, ?> oldMap)
    {
        HashMap<String, String> stringValueMap;
        Object entryValue;

        stringValueMap = new HashMap<>();
        for (Map.Entry<String, ? extends Object> entrySet : oldMap.entrySet())
        {
            entryValue = entrySet.getValue();

            if (entryValue == null) stringValueMap.put(entrySet.getKey(), null);
            else stringValueMap.put(entrySet.getKey(), String.valueOf(entryValue));
        }

        return stringValueMap;
    }

    /**
     * A convenience method to return a value from a map or a default value if
     * the value or key does not exist in the map. The returned value is the
     * same as that of the default value, which means no explicit casting is
     * required.
     *
     * @param <T> The type of the returned value
     *
     * @param valueMap The map in which the value will be checked for and
     *      returned from if available
     * @param key The {@code String} key for which the value should be returned
     * @param defaultValue The default value of type {@code T} to be returned if
     *      the key does not exist, or if the value in the map is {@code null}
     *
     * @return The value mapped to the specified key, or the default value if
     *      the key does not exist or points to a {@code null} value
     *
     * @throws ClassCastException If the specified map uses keys which aren't of
     *      type {@code String}
     * @throws NullPointerException If the specified key is {@code null} and the
     *      map does not allow null keys
     */
    @SuppressWarnings({"unchecked", "NestedAssignment"})
    public static <T> T getOrDefault(Map<String, ?> valueMap, String key, T defaultValue)
    {
        T value;

        return ((value = (T)valueMap.get(key)) == null) ? defaultValue : value;
    }
}
