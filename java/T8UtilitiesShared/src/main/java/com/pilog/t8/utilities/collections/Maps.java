package com.pilog.t8.utilities.collections;

import com.pilog.t8.utilities.strings.Strings;
import java.util.Map;

/**
 *
 * @author Bouwer.duPreez
 */
public class Maps
{
    /**
     * A null-safe method for checking whether or not a map is empty or null.
     * @param map The map to check.
     * @return boolean true if the supplied map is null or has no content keys.
     */
    public static boolean isNullOrEmpty(Map map)
    {
        if (map == null) return true;
        else return map.size() == 0;
    }

    /**
     * This method safely checks if the map is null, or if all of the values in the map are null.
     * The map can have valid keys, but if all of the values associated with the keys are null then true will be returned, false otherwise
     * @param map The map to check
     * @return true is the map is null, or if all of the values in the map is null
     */
    public static boolean isNullOrValuesAllNull(Map map)
    {
        if(map == null) return true;

        for (Object value : map.values())
        {
            if(value != null) return false;
        }
        return true;
    }

    /**
     * This method iterates over the key set of the input map and checks the
     * value for each key.  If a value is found to be an instance of String and
     * to contain only whitespace, the value for the specific key is set to
     * null.
     * @param inputMap The input map that will be altered by this method.
     */
    public static void setEmptyStringValuesToNull(Map inputMap)
    {
        for (Object key : inputMap.keySet())
        {
            Object value;

            value = inputMap.get(key);
            if ((value instanceof String) && (Strings.isEmpty((String)value)))
            {
                inputMap.put(key, null);
            }
        }
    }

    /**
     * This method iterates over all values of the input map and trims those
     * that are found to be instances of String.
     * @param inputMap The input map that will be altered by this method.
     * @param setEmptyValuesToNull If this Boolean flag is set to TRUE, any
     * values that are found to be empty after the trim operation, will be set
     * to null.
     */
    public static void trimStringValues(Map inputMap, boolean setEmptyValuesToNull)
    {
        for (Object key : inputMap.keySet())
        {
            Object value;

            value = inputMap.get(key);
            if (value instanceof String)
            {
                String newValue;

                newValue = ((String)value).trim();
                if (setEmptyValuesToNull)
                {
                    inputMap.put(key, (newValue.length() == 0 ? null : newValue));
                }
                else inputMap.put(key, newValue);
            }
        }
    }
}
