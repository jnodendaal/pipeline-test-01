package com.pilog.t8.utilities.files.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class TabularTextOutputFile
{
    private final String encoding;
    private final String separator;
    private BufferedWriter outputWriter;
    private ArrayList<String> columnNames;
    private int rowCount;

    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String ENCODING_UTF_16 = "UTF-16";

    public TabularTextOutputFile(String encoding, String separatorString)
    {
        this.encoding = encoding;
        this.separator = separatorString;
        this.rowCount = 0;
    }

    public void openFile(File outputFile) throws Exception
    {
        if (outputWriter != null) throw new Exception("Text output file already open.");
        outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), encoding));
    }

    public void openFile(OutputStream outputStream) throws Exception
    {
        if (outputWriter != null) throw new Exception("Text output file already open.");
        outputWriter = new BufferedWriter(new OutputStreamWriter(outputStream, encoding));
    }

    public void writeColumnNames(List<String> columnNames) throws Exception
    {
        HashMap<String, Object> columnNameDataRow;

        // Get the specified worksheet and if it doesnt excist, create it.
        if (outputWriter == null) throw new Exception("Attempt to write to unopened file encountered.");

        // Set the column names collection.
        this.columnNames = new ArrayList<String>(columnNames);

        // Create a list of data containing only the column names.
        columnNameDataRow = new HashMap<String, Object>();
        for (String columnName : columnNames)
        {
            columnNameDataRow.put(columnName, columnName);
        }

        // Write the first row of data to the sheet.
        TabularTextOutputFile.this.writeRow(columnNameDataRow);
    }

    public void writeRow(Map<String, Object> dataRow) throws Exception
    {
        Iterator<String> columnNameIterator;

        // Get the specified worksheet and if it doesnt excist, create it.
        if (outputWriter == null) throw new Exception("Attempt to write to unopened file encountered.");

        // If the column names have not been written out yet, do it now.
        if (columnNames == null) writeColumnNames(new ArrayList<String>(dataRow.keySet()));

        // Write the data row to the specified sheet.
        columnNameIterator = columnNames.iterator();
        while (columnNameIterator.hasNext())
        {
            String columnName;
            Object columnValue;

            columnName = columnNameIterator.next();
            columnValue = dataRow.get(columnName);
            if (columnValue != null)
            {
                String stringValue;

                stringValue = columnValue.toString();
                stringValue = stringValue.replace("\n", "");
                stringValue = stringValue.replace("\r", "");
                outputWriter.write(stringValue);
            }

            // Write the separator if there are more columns to follow.
            if (columnNameIterator.hasNext())
            {
                outputWriter.write(separator);
            }
        }

        // Write a new line character.
        outputWriter.newLine();
        rowCount++;
    }

    public void writeRow(List<Object> dataValues) throws Exception
    {
        Iterator<Object> valueIterator;

        // Get the specified worksheet and if it doesnt excist, create it.
        if (outputWriter == null) throw new Exception("Attempt to write to unopened file encountered.");

        // Write the data row to the specified sheet.
        valueIterator = dataValues.iterator();
        while (valueIterator.hasNext())
        {
            Object columnValue;

            columnValue = valueIterator.next();
            if (columnValue != null)
            {
                String stringValue;

                stringValue = columnValue.toString();
                stringValue = stringValue.replace("\n", "");
                stringValue = stringValue.replace("\r", "");
                outputWriter.write(stringValue);
            }

            // Write the separator if there are more columns to follow.
            if (valueIterator.hasNext())
            {
                outputWriter.write(separator);
            }
        }

        // Write a new line character.
        outputWriter.newLine();
        rowCount++;
    }

    public int getRowCount()
    {
        return rowCount;
    }

    public void closeFile() throws Exception
    {
        outputWriter.close();
    }
}
