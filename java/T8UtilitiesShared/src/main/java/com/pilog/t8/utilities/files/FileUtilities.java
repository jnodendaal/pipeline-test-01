package com.pilog.t8.utilities.files;

import com.pilog.t8.utilities.unicode.UnicodeReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class FileUtilities
{
    /**
     * Returns the relative path of a File from a base path.
     *
     * @param target The target file.
     * @param base The base file path.
     * @return A relative path to the target file, from the base path.
     * @throws IOException An exception if canonical resolution of file names
     * fail.
     */
    public static File getRelativeFile(File target, File base) throws IOException
    {
        String[] baseComponents;
        String[] targetComponents;
        StringBuilder result;
        int index;

        // The the components of each input file.
        baseComponents = base.getCanonicalPath().split(Pattern.quote(File.separator));
        targetComponents = target.getCanonicalPath().split(Pattern.quote(File.separator));

        // Skip common components.
        index = 0;
        while (index < targetComponents.length && index < baseComponents.length)
        {
            if (!targetComponents[index].equals(baseComponents[index]))
            {
                break;
            }

            index++;
        }

        // Append all the backtrack components.
        result = new StringBuilder();
        if (index != baseComponents.length)
        {
            // Backtrack to base directory.
            for (int componentIndex = index; componentIndex < baseComponents.length; ++componentIndex)
            {
                result.append("..");
                result.append(File.separator);
            }
        }

        // Append all the remaining target components.
        while (index < targetComponents.length)
        {
            result.append(targetComponents[index]);
            result.append(File.separator);
            index++;
        }

        if ((!target.getPath().endsWith("/")) && (!target.getPath().endsWith("\\")))
        {
            // Remove final path separator.
            result.delete(result.length() - File.separator.length(), result.length());
        }

        // Return the result.
        return new File(result.toString());
    }

    public static void writeTextFile(File outputFile, String encoding, String content) throws Exception
    {
        BufferedWriter outputWriter = null;

        try
        {
            outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), encoding));
            outputWriter.write(content);
            outputWriter.flush();
        }
        finally
        {
            if (outputWriter != null) outputWriter.close();
        }
    }

    public static String readTextFile(File inputFile, String encoding) throws Exception
    {
        BufferedReader inputReader = null;

        try
        {
            StringBuffer buffer;
            int character;

            inputReader = new BufferedReader(new UnicodeReader(new FileInputStream(inputFile), encoding));
            buffer = new StringBuffer();
            while ((character = inputReader.read()) > -1)
            {
                buffer.append((char)character);
            }

            return buffer.toString();
        }
        finally
        {
            if (inputReader != null) inputReader.close();
        }
    }

    public static void copyFile(File sourceFile, File destinationFile) throws IOException
    {
        FileChannel source = null;
        FileChannel destination = null;

        // Make sure the destination file exists.
        if (!destinationFile.exists())
        {
            destinationFile.createNewFile();
        }

        try
        {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destinationFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally
        {
            if (source != null)
            {
                source.close();
            }
            if (destination != null)
            {
                destination.close();
            }
        }
    }

    /**
     * Deletes the directory specified by the supplied path an all of its
     * contents.
     *
     * @param path The path of the directory to delete.
     * @return true if all context was deleted, false if a problem occurred.
     */
    public static boolean deleteDirectory(File path)
    {
        if (path.exists())
        {
            File[] files;

            files = path.listFiles();
            for(int fileIndex = 0; fileIndex < files.length; fileIndex++)
            {
                if (files[fileIndex].isDirectory())
                {
                    deleteDirectory(files[fileIndex]);
                }
                else
                {
                    files[fileIndex].delete();
                }
            }
        }

        return(path.delete());
    }

    /**
     * Creates and returns a list of all files located at the specified path.
     * If the input path points to a single file, that file will be the only one in the output list.
     * If the input path points to a directory, all files in the directory will be returned in the output list.
     * If the {@code includeSubDirectories} flag is set to true, all files from all sub-directories will also be included in the list.
     * @param path The path from which to create a list of files.
     * @param includeSubDirectories The flag indicating whether or not to include files in sub-directories located at the specified path.
     * @return The list of files read from the specified path.
     */
    public static List<File> listFiles(File path, boolean includeSubDirectories)
    {
        List<File> fileList;

        fileList = new ArrayList<>();
        if (path.exists())
        {
            if (path.isDirectory())
            {
                LinkedList<File> directoryQueue;

                directoryQueue = new LinkedList<>();
                directoryQueue.add(path);
                while (!directoryQueue.isEmpty())
                {
                    File nextDirectory;
                    File[] files;

                    nextDirectory = directoryQueue.removeFirst();
                    files = nextDirectory.listFiles();
                    for (int fileIndex = 0; fileIndex < files.length; fileIndex++)
                    {
                        File nextFile;

                        nextFile = files[fileIndex];
                        if (nextFile.isDirectory())
                        {
                            if (includeSubDirectories) directoryQueue.add(nextFile);
                        }
                        else
                        {
                            fileList.add(nextFile);
                        }
                    }
                }
            }
            else fileList.add(path);
        }

        return fileList;
    }

    /**
     * Copies the specified source file to the designated destination location.
     * The sourceFile can be either a file or a directory.
     * @param sourceFile The file to copy.
     * @param destinationFile The destination to which the source file will be copied.
     * @throws IOException
     */
    public static void copy(File sourceFile, File destinationFile) throws IOException
    {
        // If the source file is a directory, copy its contents.
        if (sourceFile.isDirectory())
        {
            String[] files;

            // Create the destination folder if it does not exist.
            if (!destinationFile.exists())
            {
                destinationFile.mkdirs();
            }

            // Iterate over all files in the source folder and copy them individually.
            files = sourceFile.list();
            for (String file : files)
            {
                File srcFile;
                File destFile;

                // Recursively copy the files in the list.
                srcFile = new File(sourceFile, file);
                destFile = new File(destinationFile, file);
                copy(srcFile, destFile);
            }
        }
        else
        {
            // Copy the file content.
            Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }
}
