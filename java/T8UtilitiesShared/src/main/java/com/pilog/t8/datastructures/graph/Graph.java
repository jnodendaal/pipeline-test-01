package com.pilog.t8.datastructures.graph;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class Graph implements Serializable
{
    private final Map<String, GraphVertex> vertices;
    
    public Graph()
    {
        vertices = new HashMap<String, GraphVertex>();
    }
    
    public GraphVertex getVertex(String id)
    {
        return vertices.get(id);
    }
    
    public void addVertex(GraphVertex vertex)
    {
        if (vertices.containsKey(vertex.getID()))
        {
            throw new IllegalArgumentException("Vertex already exists in graph: " + vertex);
        }
        else
        {
            vertices.put(vertex.getID(), vertex);
        }
    }
    
    public GraphVertex removeVertex(String id)
    {
        GraphVertex vertexToRemove;
        
        vertexToRemove = vertices.get(id);
        if (vertexToRemove != null)
        {
            Map<String, GraphVertex> inputs;
            Map<String, GraphVertex> outputs;
            
            inputs = vertexToRemove.getInputs();
            outputs = vertexToRemove.getOutputs();
            
            // Remove the vertex from each of its inputs.
            for (GraphVertex input : inputs.values())
            {
                input.removeOutput(id);
            }
            
            // Remove the vertex from each of its outputs.
            for (GraphVertex output : outputs.values())
            {
                output.removeInput(id);
            }
            
            vertices.remove(id);
            return vertexToRemove;
        }
        else throw new IllegalArgumentException("Vertex not found in graph: " + id);
    }
    
    public GraphVertex ensureVertex(String id)
    {
        GraphVertex vertex;
        
        vertex = vertices.get(id);
        if (vertex == null)
        {
            vertex = new GraphVertex(id);
            vertices.put(id, vertex);
            return vertex;
        }
        else return vertex;
    }
    
    public void ensureVertices(String tailID, String headID)
    {
        ensureVertex(tailID);
        ensureVertex(headID);
        addEdge(tailID, headID);
    }
    
    public void addEdge(String tailID, String headID)
    {
        GraphVertex tailVertex;
        GraphVertex headVertex;
        
        tailVertex = vertices.get(tailID);
        headVertex = vertices.get(headID);
        if (tailVertex == null) throw new IllegalArgumentException("Tail Vertex not found in graph: " + tailID);
        else if (headVertex == null) throw new IllegalArgumentException("Head Vertex not found in graph: " + headID);
        else
        {
            tailVertex.addOutput(headVertex);
            headVertex.addInput(tailVertex);
        }
    }
    
    public void removeEdge(String tailID, String headID)
    {
        GraphVertex tailVertex;
        GraphVertex headVertex;
        
        tailVertex = vertices.get(tailID);
        headVertex = vertices.get(headID);
        if (tailVertex == null) throw new IllegalArgumentException("Tail Vertex not found in graph: " + tailID);
        else if (headVertex == null) throw new IllegalArgumentException("Head Vertex not found in graph: " + headID);
        else
        {
            tailVertex.removeOutput(headID);
            headVertex.removeInput(tailID);
        }
    }
    
    public Map<String, GraphVertex> getLinkedVertices(String id)
    {
        GraphVertex vertex;
        
        vertex = vertices.get(id);
        if (vertex != null)
        {
            Map<String, GraphVertex> linkedVertices;
            
            linkedVertices = new HashMap<String, GraphVertex>();
            vertex.addLinks(linkedVertices);
            return linkedVertices;
        }
        else throw new IllegalArgumentException("Vertex not found in graph: " + id);
    }
}

