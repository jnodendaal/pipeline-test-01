package com.pilog.t8.utilities.strings;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Bouwer du Preez
 */
public class StringBufferOutputStream extends OutputStream
{
    protected StringBuffer buffer; // The target buffer

    /**
     * Create an output stream that writes to the target StringBuffer.
     *
     * @param out The wrapped output stream.
     */
    public StringBufferOutputStream(StringBuffer out)
    {
        buffer = out;
    }

    // In order for this to work, we only need override the single character 
    // form, as the others funnel through this one by default.
    @Override
    public void write(int ch) throws IOException
    {
        // Just append the character.
        buffer.append((char)ch);
    }
}
