package com.pilog.t8.utilities.numbers;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Bouwer du Preez
 */
public class Numbers
{
    /**
     * Rounds the supplied double value to the specified number of decimal 
     * digits.
     * @param value The double value to round.
     * @param places The number of decimal digits to round the double to.
     * @return The rounded double.
     */
    public static double round(double value, int places)
    {
        if (places < 0)
        {
            throw new IllegalArgumentException();
        }
        else
        {
            BigDecimal decimal;
            
            decimal = new BigDecimal(value);
            decimal = decimal.setScale(places, RoundingMode.HALF_UP);
            return decimal.doubleValue();
        }
    }
}
