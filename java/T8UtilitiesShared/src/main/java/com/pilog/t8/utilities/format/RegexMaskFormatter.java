package com.pilog.t8.utilities.format;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.text.MaskFormatter;

/**
 * @author Bouwer du Preez
 */
public class RegexMaskFormatter extends MaskFormatter
{
    private Pattern regexPattern;
    private Matcher matcher;
    private boolean allowBlankField = true;
    private String blankRepresentation;
    
    /**
     * Creates a regular expression based AbstractFormatter. pattern specifies
     * the regular expression that will be used to determine if a value is
     * legal.
     * @param maskPattern
     * @param regexPattern
     * @throws java.text.ParseException
     */
    public RegexMaskFormatter(String maskPattern, String regexPattern) throws PatternSyntaxException, ParseException
    {
        super(maskPattern);
        setRegexPattern(regexPattern != null ? Pattern.compile(regexPattern) : null);
    }

    /**
     * Sets the pattern that will be used to determine if a value is legal.
     * @param pattern
     */
    public final void setRegexPattern(Pattern pattern)
    {
        this.regexPattern = pattern;
    }

    /**
     * Returns the Pattern used to determine if a value is legal.
     * @return 
     */
    public Pattern getRegexPattern()
    {
        return regexPattern;
    }

    /**
     * Sets the Matcher used in the most recent test if a value is legal.
     * @param matcher
     */
    protected void setMatcher(Matcher matcher)
    {
        this.matcher = matcher;
    }

    /**
     * Returns the Matcher from the most test.
     * @return 
     */
    protected Matcher getMatcher()
    {
        return matcher;
    }
    
    /**
     * Sets a flag which indicates whether or not the text formatter will accept
     * any empty value (only mask and placeholder characters) as valid.
     * @param allowBlankField 
     */
    public void setAllowBlankField(boolean allowBlankField)
    {
        this.allowBlankField = allowBlankField;
    }

    public boolean isAllowBlankField()
    {
        return allowBlankField;
    }
    
    /**
     * Update our blank representation whenever the mask is updated.
     * @param mask
     * @throws java.text.ParseException
     */
    @Override
    public void setMask(String mask) throws ParseException
    {
        super.setMask(mask);
        updateBlankRepresentation();
    }

    /**
     * Update our blank representation whenever the mask is updated.
     */
    @Override
    public void setPlaceholderCharacter(char placeholder)
    {
        super.setPlaceholderCharacter(placeholder);
        updateBlankRepresentation();
    }
    
    private void updateBlankRepresentation()
    {
        try
        {
            // Calling valueToString on the parent class with a null attribute will get the 'blank' representation.
            blankRepresentation = valueToString(null);
        }
        catch (ParseException e)
        {
            blankRepresentation = null;
        }
    }

    /**
     * Parses text returning an arbitrary Object. Some formatters may return
     * null.
     *
     * If a Pattern has been specified and the text completely matches the
     * regular expression this will invoke setMatcher.
     *
     * @throws ParseException if there is an error in the conversion
     * @param text String to convert
     * @return Object representation of text
     */
    @Override
    public Object stringToValue(String text) throws ParseException
    {
        if ((isAllowBlankField()) && (blankRepresentation != null) && (blankRepresentation.equals(text)))
        {
            // Return a blank value.
            return null;
        }
        else
        {
            Pattern pattern = getRegexPattern();

            if (pattern != null)
            {
                Matcher testMatcher;

                testMatcher = pattern.matcher(text);
                if (testMatcher.matches())
                {
                    setMatcher(testMatcher);
                    return super.stringToValue(text);
                }
                else throw new ParseException("Pattern did not match.", 0);
            }
            else return super.stringToValue(text);
        }
    }
}
