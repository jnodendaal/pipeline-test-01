/**
 * Created on Jul 1, 2015, 8:02:21 AM
 */
package com.pilog.t8.utilities.date;

import com.pilog.t8.utilities.strings.Strings;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Gavin Boshoff
 */
public class Dates
{
    /**
     * The standard formatter used throughout the system for formatting date
     * time values as timestamps.
     */
    public static final DateFormat STD_SYS_TS_FORMATTER = new SimpleDateFormat(Dates.STD_SYS_TS_FORMAT);
    public static final String STD_SYS_TS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String STD_SYS_DT_FORMAT = "yyyy-MM-dd";

    private static final String ZERO_TIME = "00:00:00";

    /**
     * Attempts a generic date time format conversion to strip out the time
     * section and replace it with a zero time or more commonly known as
     * midnight in 24h time format.<br/>
     * <br/>
     * The assumption is that the date and time sections in the specified date
     * time string is separated by a space, with the date value first.
     *
     * @param dateTime The date time {@code String} value
     *
     * @return The {@code String} with the original time section replaced with
     *      zero time
     */
    public static String toZeroTime(String dateTime)
    {
        if (dateTime.indexOf(' ') == -1) return dateTime.concat(" ").concat(ZERO_TIME);
        else return dateTime.substring(0, dateTime.indexOf(' ')+1).concat(ZERO_TIME);
    }

    /**
     * Attempts to strip out the time section of the date time {@code String}
     * value specified. This will fail if the date time value is not in a
     * standard format.<br/>
     * <br/>
     * The assumption is that the date and time sections in the specified date
     * time string is separated by a space, with the date value first.
     *
     * @param dateTime The {@code String} date time value where the time value
     *      should be stripped from
     *
     * @return The {@code String} date only value
     */
    public static String stripTime(String dateTime)
    {
        if (dateTime.indexOf(' ') == -1) return dateTime;
        else return dateTime.substring(0, dateTime.indexOf(' '));
    }

    /**
     * Uses the specified format to convert the provided date time
     * {@code String} to a date type object which can be manipulated to get the
     * following day date time and the return the resulting {@code String}.
     *
     * @param dateTime The {@code String} date time value
     * @param format The {@code String} format to be used to parse the date time
     *      string
     *
     * @return The date time {@code String} representing the next day of the
     *      date specified
     *
     * @see DateFormat
     * @see SimpleDateFormat
     */
    public static String nextDay(String dateTime, String format)
    {
        SimpleDateFormat formatter;
        Calendar thisDay;

        try
        {
            formatter = new SimpleDateFormat(format);
            thisDay = Calendar.getInstance();

            thisDay.setTime(formatter.parse(dateTime));
            thisDay.add(Calendar.DATE, 1);

            return formatter.format(thisDay.getTime());
        }
        catch (ParseException pe)
        {
            throw new RuntimeException("Failed to parse the date time string [" + dateTime + "] using the format [" + format + "]", pe);
        }
    }

    /**
     * A convenience method to allow for the extraction of a epoch long string
     * from a date string value, using the specified format pattern.<br/>
     * Example:<br/>
     * Date Time: 2016-06-14<br/>
     * Format Pattern: yyyy-MM-dd<br/>
     * Result: 1465862400000
     *
     * @param dateTime The {@code String} date and/or time value to be converted
     * @param format The {@code String} format patter used to parse the date
     *      string
     *
     * @return The {@code String} value representing the long milliseconds.
     *      {@code null} is returned if the datetime value is empty or
     *      {@code null}
     *
     * @throws RuntimeException if the format patter is invalid or the datetime
     *      value cannot be parsed using the given format pattern
     */
    public static String toEpochLongString(String dateTime, String format)
    {
        if (Strings.isNullOrEmpty(dateTime)) return null;

        SimpleDateFormat formatter;
        Date parsedDate;

        try
        {
            formatter = new SimpleDateFormat(format);

            // Parses the date string using the format
            parsedDate = formatter.parse(dateTime);

            // Get the long string
            return Long.toString(parsedDate.getTime());
        }
        catch (ParseException pe)
        {
            throw new RuntimeException("Failed to parse the date time string [" + dateTime + "] using the format [" + format + "]", pe);
        }
    }

    private Dates() {}
}