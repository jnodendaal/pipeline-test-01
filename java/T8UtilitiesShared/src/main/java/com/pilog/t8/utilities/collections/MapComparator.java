package com.pilog.t8.utilities.collections;

import java.util.Comparator;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class MapComparator implements Comparator
{
    private final String[] sortKeys;
    private final OrderMethod[] orderMethods;

    public enum OrderMethod {ASCENDING, DESCENDING};

    public MapComparator(Map<String, OrderMethod> fieldOrdering)
    {
        int index;

        this.sortKeys = new String[fieldOrdering.size()];
        this.orderMethods = new OrderMethod[fieldOrdering.size()];

        index = 0;
        for (String fieldIdentifier : fieldOrdering.keySet())
        {
            sortKeys[index] = fieldIdentifier;
            orderMethods[index] = fieldOrdering.get(fieldIdentifier);
            index++;
        }
    }

    @Override
    public int compare(Object o1, Object o2)
    {
        Map map1;
        Map map2;

        // Get the entities to compare.
        map1 = (Map)o1;
        map2 = (Map)o2;

        // Evaluate each of the field values to be compared in sequence.
        for (int orderSequence = 0; orderSequence < sortKeys.length; orderSequence++)
        {
            String key;
            OrderMethod orderMethod;
            Comparable value1;
            Comparable value2;

            // Get the identifier of the field to compare and the order method to use.
            key = sortKeys[orderSequence];
            orderMethod = orderMethods[orderSequence];

            // Get the two field values to compare.
            value1 = (Comparable)map1.get(key);
            value2 = (Comparable)map2.get(key);

            // Compare the field value depending on the method to use.
            if (orderMethod == OrderMethod.ASCENDING)
            {
                // If value 1 is null and value 2 is not, it means that value 1 is less than value 2 (ascending).
                if (value1 == null)
                {
                    if (value2 != null) return -1;
                }
                else return value1.compareTo(value2); // Value 1 is not null, so just do a normal compare with value 2 (ascending).
            }
            else
            {
                // If value 1 is null and value 2 is not, it means that value 1 is greater than value 2 (descending).
                if (value1 == null)
                {
                    if (value2 != null) return 1;
                }
                else return value1.compareTo(value2) * -1; // Value 1 is not null, so just do a normal compare with value 2 (descending).
            }
        }

        // If we reached this point, it means all of the comparisons returned a 0 value.
        return 0;
    }
}
