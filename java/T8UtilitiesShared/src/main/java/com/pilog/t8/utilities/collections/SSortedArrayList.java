/**
 * Created on Nov 14, 2015, 7:57:00 PM
 *
 * Copyright(c) 2015 ShadowOfLies. All Rights Reserved.
 * The code from this class and all associated code, with the exception of third
 * party library code, is the proprietary information of ShadowOfLies.
 */
package com.pilog.t8.utilities.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * @version 1
 * @author Gavin Boshoff
 * @param <E>
 */
public class SSortedArrayList<E> extends ArrayList<E>
{
    private static final long serialVersionUID = -1243741016300906802L;
    private static final int SIZE_ERROR_MARGIN = 2;

    private Comparator<? super E> comparator;
    private final int fixSize;

    /**
     * Creates a new instance of the {@code SSortedArrayList} using natural
     * ordering of the elements.
     */
    public SSortedArrayList()
    {
        this(null);
    }

    /**
     * Creates a new instance of the {@code SSortedArrayList} using the
     * specified {@code Comparator} to sort the items added to the list.
     *
     * @param comparator The {@code Comparator} to used for the sorting
     */
    public SSortedArrayList(Comparator<? super E> comparator)
    {
        this(comparator, -1);
    }

    public SSortedArrayList(Comparator<? super E> comparator, int fixSize)
    {
        super((fixSize != -1 ? fixSize+SIZE_ERROR_MARGIN : 10));
        this.comparator = comparator;
        this.fixSize = fixSize;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c)
    {
        throw new UnsupportedOperationException("Cannot specify insertion index for sorted list.");
    }

    @Override
    public void add(int index, E element)
    {
        throw new UnsupportedOperationException("Cannot specify insertion index for sorted list.");
    }

    @Override
    public void sort(Comparator<? super E> c)
    {
        if (c != this.comparator && (c == null || !c.equals(this.comparator)))
        {
            // Replace the comparator with the new one, otherwise the list will get funky
            this.comparator = c;
            super.sort(c);
        }
        // otherwise collection is already sorted
    }

    @Override
    public boolean addAll(Collection<? extends E> c)
    {
        boolean result = false;
        // In certain cases might be faster to just add all and then re-sort the entire list
        return c.stream().map((element) -> add(element)).reduce(result, (accumulator, _item) -> accumulator | _item);
    }

    /**
     * {@inheritDoc}
     *
     * @param o The element to be removed
     *
     * @return {@code true} if the item was removed. Never {@code false} since
     *      item not in list will result in error
     *
     * @throws IndexOutOfBoundsException if the specified element is not present
     *      in the list
     */
    @Override
    public boolean remove(Object o)
    {
        return (remove(indexOf(o)) != null);
    }

    @Override
    public boolean add(E e)
    {
        int insertionPoint;

        insertionPoint = Collections.binarySearch(this, e, this.comparator);
        if ((this.fixSize == -1) || (size() < this.fixSize) || (insertionPoint <= this.fixSize-1))
        {
            // Insert the item into the correct index
            super.add((insertionPoint > -1) ? insertionPoint : (-insertionPoint) - 1, e);
            // Remove a trailing item from the end of the list if required
            if (this.fixSize != -1 && size() > this.fixSize)
            {
                remove(this.fixSize);
            }

            return true;
        } else return false;
    }

    @Override
    public E set(int index, E element)
    {
        E original;

        original = remove(index);
        add(element);

        return original;
    }

    @Override
    public int lastIndexOf(Object o)
    {
        int insertionIndex;
        E nextElement;

        insertionIndex = indexOf(o);
        if (insertionIndex != -1)
        {
            while (true)
            {
                nextElement = get(insertionIndex+1);
                if (nextElement == o || (nextElement != null && nextElement.equals(o)))
                {
                    insertionIndex++;
                } else break;
            }
        }

        return insertionIndex;
    }

    @Override
    @SuppressWarnings("unchecked")
    public int indexOf(Object o)
    {
        int insertionPoint;

        insertionPoint = Collections.binarySearch(this, (E)o, this.comparator);

        // We return -1 if the item is not in the list to keep to the contract of the #indexOf method
        return insertionPoint > -1 ? insertionPoint : -1;
    }

    @Override
    public boolean contains(Object o)
    {
        return indexOf(o) != -1;
    }
}