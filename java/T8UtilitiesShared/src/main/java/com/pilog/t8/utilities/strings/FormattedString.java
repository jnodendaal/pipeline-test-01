package com.pilog.t8.utilities.strings;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class FormattedString implements CharSequence, Serializable
{
    private StringBuffer buffer;
    private boolean toLowerCase;
    private boolean toUpperCase;
    private boolean appendNullString;
    private boolean appendTrimmed;
    private String appendSeparator;
    private String nullString;
    
    public FormattedString()
    {
        buffer = new StringBuffer();
        toLowerCase = false;
        toUpperCase = false;
        appendNullString = false;
        appendTrimmed = true;
        appendSeparator = " ";
        nullString = "null";
    }
    
    public void append(CharSequence chars)
    {
        if (chars == null)
        {
            if (appendNullString)
            {
                buffer.append(nullString);
            }
        }
        else
        {
            String stringToAppend;
            
            // Get the string to append.
            stringToAppend = chars.toString();
            
            // Trim the string if required.
            if (appendTrimmed) stringToAppend = stringToAppend.trim();
            
            // Only continue if we still have something left to append.
            if (stringToAppend.length() > 0)
            {
                // Convert the string as required.
                if (toLowerCase) stringToAppend = stringToAppend.toLowerCase();
                else if (toUpperCase) stringToAppend = stringToAppend.toUpperCase();

                // Append a separator if required.
                if (buffer.length() > 0) buffer.append(appendSeparator);
                
                // Append the string.
                buffer.append(stringToAppend);
            }
        }
    }
    
    @Override
    public String toString()
    {
        return buffer.toString();
    }

    public StringBuffer getBuffer()
    {
        return buffer;
    }

    public void setBuffer(StringBuffer buffer)
    {
        this.buffer = buffer;
    }

    public boolean isToLowerCase()
    {
        return toLowerCase;
    }

    public void setToLowerCase(boolean toLowerCase)
    {
        this.toLowerCase = toLowerCase;
    }

    public boolean isToUpperCase()
    {
        return toUpperCase;
    }

    public void setToUpperCase(boolean toUpperCase)
    {
        this.toUpperCase = toUpperCase;
    }

    public boolean isAppendNullString()
    {
        return appendNullString;
    }

    public void setAppendNullString(boolean appendNullString)
    {
        this.appendNullString = appendNullString;
    }

    public boolean isAppendTrimmed()
    {
        return appendTrimmed;
    }

    public void setAppendTrimmed(boolean appendTrimmed)
    {
        this.appendTrimmed = appendTrimmed;
    }

    public String getAppendSeparator()
    {
        return appendSeparator;
    }

    public void setAppendSeparator(String appendSeparator)
    {
        this.appendSeparator = appendSeparator;
    }

    public String getNullString()
    {
        return nullString;
    }

    public void setNullString(String nullString)
    {
        this.nullString = nullString;
    }

    @Override
    public int length()
    {
        return buffer.length();
    }

    @Override
    public char charAt(int index)
    {
        return buffer.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        return buffer.subSequence(start, end);
    }
}
