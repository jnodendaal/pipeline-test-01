package com.pilog.t8.utilities.files.data;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class TableDataLocation implements Serializable
{
    public long offset;
    public int size;

    public TableDataLocation(long offset, int size)
    {
        this.offset = offset;
        this.size = size;
    }
}
