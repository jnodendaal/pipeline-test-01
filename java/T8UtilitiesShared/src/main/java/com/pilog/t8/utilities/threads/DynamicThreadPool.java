package com.pilog.t8.utilities.threads;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * This class is a very simple convenience implementation focused on a common
 * use-case of ThreadPoolExecutor to create a cached pool of threads that grows
 * and shrinks dynamically according to concurrency needs witch the additional
 * behavior of blocking the requesting thread when the pool is saturated and
 * additional requests cannot be accommodated.
 *
 * If the thread pool has not reached the core pool size, it creates new
 * threads. If the core pool size has been reached and there is no idle threads,
 * it adds tasks on the queue. If the core pool size has been reached, there is
 * no idle threads, and the queue is full, it creates new threads (until it
 * reaches the maximum pool size). If the maximum pool size has been reached,
 * there is no idle threads, and the queue is full, the invoking thread will be
 * blocked until space in the queue becomes available.
 *
 * @author Bouwer du Preez
 */
public class DynamicThreadPool extends ThreadPoolExecutor
{
    public DynamicThreadPool(int corePoolSize, int maximumPoolSize, int maximumQueueSize, long keepAliveTime, TimeUnit keepAliveTimeUnit)
    {
        super(corePoolSize, maximumPoolSize, keepAliveTime, keepAliveTimeUnit, new LimitedQueue<>(maximumQueueSize));
    }

    private static class LimitedQueue<E> extends LinkedBlockingQueue<E>
    {
        private LimitedQueue(int maxSize)
        {
            super(maxSize);
        }

        @Override
        public boolean offer(E e)
        {
            // Turn offer() and add() into a blocking calls (unless interrupted).
            try
            {
                put(e);
                return true;
            }
            catch (InterruptedException ie)
            {
                Thread.currentThread().interrupt();
            }

            return false;
        }
    }
}
