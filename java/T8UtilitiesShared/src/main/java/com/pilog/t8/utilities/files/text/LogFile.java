package com.pilog.t8.utilities.files.text;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bouwer du Preez
 */
public class LogFile extends TextOutputFile
{
    private DateFormat dateFormat;
    
    public static final String DEFAULT_TIMESTAMP_FORMAT = "HH:mm:ss,SSS";
    
    public LogFile(File file, String encoding)
    {
        super(file, encoding);
        dateFormat = new SimpleDateFormat(DEFAULT_TIMESTAMP_FORMAT);
    }
    
    public LogFile(OutputStream outputStream, String encoding)
    {
        super(outputStream, encoding);
        dateFormat = new SimpleDateFormat(DEFAULT_TIMESTAMP_FORMAT);
    }
    
    protected String getTimestamp()
    {
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }
    
    public void log(String logEntry) throws IOException
    {
        write(getTimestamp());
        write(" ");
        writeLine(logEntry);
    }
}
