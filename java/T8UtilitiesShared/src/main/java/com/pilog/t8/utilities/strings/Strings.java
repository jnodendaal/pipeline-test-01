package com.pilog.t8.utilities.strings;

import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class Strings
{
    public static final Pattern DOUBLE_PATTERN = Pattern.compile("-?\\.\\d*|-?\\d+(\\.\\d*)?");
    public static final Pattern INTEGER_PATTERN = Pattern.compile("-?[0-9]+");
    public static final Pattern POSITIVE_INTEGER_PATTERN = Pattern.compile("[0-9]+");
    public static final Pattern NEGATIVE_INTEGER_PATTERN = Pattern.compile("-[0-9]+");
    public static final Pattern TRUE_BOOLEAN_PATTERN = Pattern.compile("true|1|y|yes", Pattern.CASE_INSENSITIVE);

    /**
     * This method checks the input string and return true if it can be properly
     * parsed into a Double value.
     * @param inputString The string to evaluate.
     * @return Boolean true if the String can be parsed into a Double value.
     */
    public static boolean isDouble(String inputString)
    {
        if (inputString == null) return false;
        else return DOUBLE_PATTERN.matcher(inputString).matches();
    }

    /**
     * This method checks the input string and return true if it can be properly
     * parsed into an Integer value.
     * @param inputString The string to evaluate.
     * @return Boolean true if the String can be parsed into an Integer value.
     */
    public static boolean isInteger(String inputString)
    {
        if (inputString == null) return false;
        else return INTEGER_PATTERN.matcher(inputString).matches();
    }

    /**
     * This method checks the input string and return true if it can be properly
     * parsed into a positive Integer value.
     * @param inputString The string to evaluate.
     * @return Boolean true if the String can be parsed into a positive Integer
     * value.
     */
    public static boolean isPositiveInteger(String inputString)
    {
        if (inputString == null) return false;
        else return POSITIVE_INTEGER_PATTERN.matcher(inputString).matches();
    }

    /**
     * This method checks the input string and return true if it can be properly
     * parsed into a negative Integer value.
     * @param inputString The string to evaluate.
     * @return Boolean true if the String can be parsed into a negative Integer
     * value.
     */
    public static boolean isNegativeInteger(String inputString)
    {
        if (inputString == null) return false;
        else return NEGATIVE_INTEGER_PATTERN.matcher(inputString).matches();
    }

    /**
     * Validates a given input string as a {@code String} representation of a
     * boolean value.<br>
     * <br>
     * If no true value list is specified, or an empty list is specified, the
     * default {@link #TRUE_BOOLEAN_PATTERN} pattern is used for the check.
     *
     * @param inputString The {@code String} input value to test
     * @param trueValues An optional {@code List} of {@code String} values that
     *      represent a true value
     *
     * @return {@code true} if the input string represents a {@code true} value
     *      based on either the given true values or the default check pattern
     */
    public static boolean isTrueRepresentation(String inputString, List<String> trueValues)
    {
        if (isNullOrEmpty(inputString)) return false;
        if (trueValues != null && !trueValues.isEmpty())
        {
            return trueValues.stream().anyMatch(trueValue -> trueValue.equalsIgnoreCase(inputString));
        } else return TRUE_BOOLEAN_PATTERN.matcher(inputString).matches();
    }

    /**
     * Returns true if the input string contains only white space.  This method
     * does not check for null input.
     * @param inputString The input string to check.
     * @return Boolean true if the input string contains only white space.
     */
    public static boolean isEmpty(String inputString)
    {
        return (inputString.trim().length() == 0);
    }

    /**
     * Returns true if the input character sequence is either null or contains
     * only white space.
     * @param inputCharacterSequence The input character sequence to check.
     * @return Boolean true if the input character sequence is null or contains
     * only white space.
     */
    public static boolean isNullOrEmpty(CharSequence inputCharacterSequence)
    {
        //If sequence contains whitespace, this method is faster than trimming.
        //Otherwise no difference in performance
        int charLen;

        if (inputCharacterSequence == null || (charLen = inputCharacterSequence.length()) == 0) return true;

        for (int i = 0; i < charLen; i++)
        {
            if (!Character.isWhitespace(inputCharacterSequence.charAt(i))) return false;
        }

        return true;
    }

    /**
     * A null-safe convenience method to trim a {@code String}. In the case that
     * the trimmed string is empty, the returning value is {@code null}.
     *
     * @param inputString The {@code String} to trim
     *
     * @return {@code null} if the original string is empty, null or only
     *      consists of whitespace characters. Otherwise the trimmed
     *      {@code String}
     */
    public static String trimToNull(String inputString)
    {
        if (inputString == null) return null;
        String newString;

        newString = inputString.trim();
        if (newString.isEmpty()) return null;
        else return newString;
    }

    /**
     * Repeats the specified {@code String} for an {@code int} number of
     * occurrences.
     *
     * @param toRepeat The {@code String} to repeat
     * @param number The {@code int} number of times to repeat
     *
     * @return The specified {@code String} repeated. {@code null} if the
     *      specified {@code String} was {@code null}. An empty {@code String}
     *      if the original {@code String} has a length of 0, or the repetitions
     *      are set to 0
     */
    public static String repeat(String toRepeat, int number)
    {
        if (toRepeat == null) return null;
        if (number == 0 || toRepeat.isEmpty()) return "";
        if (number == 1) return toRepeat;

        StringBuilder repeatBuilder;

        repeatBuilder = new StringBuilder((toRepeat.length() * number));
        for (int rep = 0; rep < number; rep++)
        {
            repeatBuilder.append(toRepeat);
        }

        return repeatBuilder.toString();
    }

    /**
     * Checks whether or not a specified {@code String} ends with any one of the
     * end {@code String} values provided. Only if none of the end values match
     * the end of the string to check, will the result be {@code false}.<br/>
     * <br/>
     * If no end values are specified, or {@code null} is provided, the result
     * will always be {@code false}.
     *
     * @param string The {@code String} to be checked
     * @param ends The {@code String} array of potential end values to check
     *
     * @return {@code true} if any one of the specified end values match the
     *      end of the string to check
     */
    public static boolean endsWith(String string, String... ends)
    {
        if (ends == null || ends.length == 0) return false;
        for (String end : ends)
        {
            if (string.endsWith(end)) return true;
        }

        return false;
    }

    /**
     * Truncates the supplied input string to a length that is exactly three
     * characters shorter than the specified length.  The last three character
     * spaces will be filled up with an ellipses '...' so that the total length
     * of the returned string is exactly the limit specified.  If the input
     * string is shorter than or equal to the specified length, this method is a
     * no-op.
     * @param inputString The input string to be truncated.
     * @param length The length of the output string after truncation.
     * @return A copy of the input string, truncated to the specified length
     * with ellipses added to the end to indicate truncation.
     */
    public static String truncate(String inputString, int length)
    {
        if ((inputString != null) && (inputString.length() > length))
        {
            StringBuilder outputString;

            outputString = new StringBuilder(inputString);
            outputString.setLength(length - 3);
            outputString.append("...");
            return outputString.toString();
        }
        else return inputString;
    }

    /**
     * This method finds the index of the closing bracket in the supplied input
     * character sequence matching the opening bracket at the specified index.
     * Valid opening bracket characters are: '(', '{', '{' and '<'.
     * @param inputSequence The character sequence in which to find the closing
     * bracket.
     * @param bracketIndex The index of the opening bracket for which to find
     * the matching closing bracket.
     * @return The index of the closing bracket matching the opening bracket at
     * the specified index.  Returns -1 if no closing bracket could be found.
     */
    public static int findClosingBracket(CharSequence inputSequence, int bracketIndex)
    {
        char openingBracket;
        char closingBracket;
        int openCount;

        // Get the open bracket to determine type of bracket we are using.
        openingBracket = inputSequence.charAt(bracketIndex);

        // Set the closing bracket we are looking for.
        switch (openingBracket)
        {
            case '[':
                closingBracket = ']';
                break;
            case '(':
                closingBracket = ')';
                break;
            case '{':
                closingBracket = '}';
                break;
            case '<':
                closingBracket = '>';
                break;
            default:
                throw new IllegalArgumentException("Invalid bracket found at specified index '" + bracketIndex + "': " + openingBracket);
        }

        // Find the closing bracket.
        openCount = 1;
        for (int index = bracketIndex + 1; index < inputSequence.length(); index++)
        {
            char nextChar;

            // Evaluate the next char.
            nextChar = inputSequence.charAt(index);
            if (nextChar == openingBracket) openCount++;
            else if (nextChar == closingBracket) openCount--;

            // Check if we found the closing bracket.
            if (openCount == 0) return index;
        }

        // No closing bracket found.
        return -1;
    };

    /**
     * Strips all non-alphanumeric characters from the start and end of the input string.
     * @param inputString The input string to trim.
     * @return The trimmed input string.  Empty strings are returned as null.
     */
    public static String trimToAlphanumeric(CharSequence inputString)
    {
        int startIndex;

        // Find the first alphanumeric character in the input string.
        for (startIndex = 0; startIndex < inputString.length(); startIndex++)
        {
            if (Character.isAlphabetic(inputString.charAt(startIndex))) break;
            else if (Character.isDigit(inputString.charAt(startIndex))) break;
        }

        // If the start index is valid, proceed.
        if (startIndex < inputString.length())
        {
            int endIndex;

            // Starting from the end of the input string and moving to the front, find the first alphanumeric character.
            for (endIndex = inputString.length()-1; endIndex > -1; endIndex--)
            {
                if (Character.isAlphabetic(inputString.charAt(endIndex))) break;
                else if (Character.isDigit(inputString.charAt(endIndex))) break;
            }

            // If the end index is greater than or equal to the start index, we've found at least one alphanumeric character to proceed with.
            if (endIndex >= startIndex)
            {
                return inputString.subSequence(startIndex, endIndex+1).toString();
            }
            else return null;
        }
        else return null;
    }

    /**
     * Splits the input string around whitespace word boundaries.
     * @param inputString
     * @return The list of substrings split from the input string.
     */
    public static List<String> splitTerms(String inputString)
    {
        List<String> strings;

        strings = new ArrayList<>();
        for (String string : split(inputString, " ", true, true))
        {
            String trimmedString;

            trimmedString = Strings.trimToAlphanumeric(string);
            if (trimmedString != null) strings.add(trimmedString);
        }

        return strings;
    }

    /**
     * Splits the input string around the specified boundary string.  All substrings separated by the boundary
     * will be included according to the {@code trim} and {@ discardEmptyStrings} flag.
     * - If the {@code trim} flag is set to true, all substrings in the result list will be trimmed.
     * - If the {@code discardEmptyStrings} flag is set to true, substrings containing only whitespace will not be included in the result list.
     * @param inputString The input string to split.
     * @param boundaryString The boundary string around which the input will be split.
     * @param trim The flag indicating whether or not strings in the result list will be trimmed.
     * @param discardEmptyStrings The flag indicating whether or not empty strings must be discarded from the result list.
     * @return The list of substrings split from the input string.
     */
    public static List<String> split(String inputString, String boundaryString, boolean trim, boolean discardEmptyStrings)
    {
        List<String> strings;
        int boundaryLength;
        int startIndex;
        int endIndex;

        startIndex = 0;
        boundaryLength = boundaryString.length();
        strings = new ArrayList<>();
        while ((endIndex = inputString.indexOf(boundaryString, startIndex)) > -1)
        {
            String nextWord;

            nextWord = inputString.substring(startIndex, endIndex);
            if (trim) nextWord = nextWord.trim();
            if ((!discardEmptyStrings) || (!Strings.isEmpty(nextWord))) strings.add(nextWord);
            startIndex = endIndex + boundaryLength;
        }

        // Add the remaining part of the input string.
        if (startIndex < (inputString.length() - 1))
        {
            strings.add(inputString.substring(startIndex));
        }

        // Return the final results.
        return strings;
    }

    /**
     * Splits the given input string into a sub-set of shorter strings, limited
     * to the length specified. This means that each returned string can be
     * shorter or equal to the split length specified, but never longer.<br>
     * <br>
     * The split characters is a string of characters that are considered valid
     * "splitting points". If no characters are specified, the string is simply
     * split according the length specified.<br>
     * <br>
     * If a split character does not occur within a segment of the specified
     * maximum length, the {@code String} is split only on length.
     *
     * @param inputString The {@code String} to be split
     * @param splitLength The maximum length of the pieces into which the
     *      supplied {@code String} should be split
     * @param splitChars The valid characters on which a split will occur. Can
     *      be empty, but not {@code null}
     *
     * @return The {@code List} of split {@code String}s
     */
    public static List<String> splitByLength(String inputString, int splitLength, String splitChars)
    {
        List<String> stringParts;
        char[] splitCharacters;
        String workingString;
        int splitCharIdx;
        String segment;
        int idx;

        splitCharacters = splitChars.toCharArray();
        stringParts = new ArrayList<>();
        workingString = inputString;

        while (!workingString.isEmpty() && workingString.length() > splitLength)
        {
            segment = workingString.substring(0, splitLength);

            splitCharIdx = -1;
            for (char splitChar : splitCharacters)
            {
                idx = segment.lastIndexOf(splitChar);
                if (idx > splitCharIdx) splitCharIdx = idx;
            }

            if (splitCharIdx != -1)
            {
                splitCharIdx++;
                stringParts.add(segment.substring(0, splitCharIdx));
                workingString = workingString.substring(splitCharIdx);
            }
            else
            {
                stringParts.add(segment);
                workingString = workingString.substring(splitLength);
            }
        }

        if (!workingString.isEmpty())
        {
            stringParts.add(workingString);
        }

        return stringParts;
    }

    /**
     * Convenience method to concatenate multiple strings. Underlying is a
     * {@code StringBuilder} that appends each of the string values.
     *
     * @param strings The {@code String} values to be concatenated
     *
     * @return The concatenated {@code String} values
     */
    public static String concat(String... strings)
    {
        StringBuilder newString;

        newString = new StringBuilder();
        for (String string : strings) newString.append(string);

        return newString.toString();
    }

    /**
     * Strips all non-alphanumeric characters from the input string.
     * @param inputString The input string from which to strip the specified characters.
     * @param stripWhitespace A boolean indicator to specify whether or not whitespace should be stripped.
     * @return A copy of the input string, with with non-alphanumeric characters stripped from it.
     */
    public static String stripNonAlphanumeric(CharSequence inputString, boolean stripWhitespace)
    {
        if (inputString != null)
        {
            StringBuilder builder;
            int stripCharIndex;

            stripCharIndex = 0;
            builder = new StringBuilder(inputString);
            while (stripCharIndex < builder.length())
            {
                char nextChar;

                nextChar = builder.charAt(stripCharIndex);
                if (Character.isLetter(nextChar) || Character.isDigit(nextChar) || ((Character.isWhitespace(nextChar)) && (!stripWhitespace)))
                {
                    stripCharIndex++;
                }
                else builder.deleteCharAt(stripCharIndex);
            }

            return builder.toString();
        }
        else return null;
    }

    /**
     * Strips all of the specified characters from the input string.
     * @param inputString The input string from which to strip the specified characters.
     * @param stripCharacters The characters to strip from the input string.
     * @return A copy of the input string, with with the specified character stripped from it.
     */
    public static String strip(CharSequence inputString, CharSequence stripCharacters)
    {
        if (inputString != null)
        {
            StringBuilder builder;

            builder = new StringBuilder(inputString);
            for (int stripCharIndex = 0; stripCharIndex < stripCharacters.length(); stripCharIndex++)
            {
                String stripCharacter;
                int stripIndex;

                stripCharacter = Character.toString(stripCharacters.charAt(stripCharIndex));
                while ((stripIndex = builder.indexOf(stripCharacter)) > -1)
                {
                    builder.deleteCharAt(stripIndex);
                }
            }

            return builder.toString();
        }
        else return null;
    }

    /**
     * Strips the specified character from the start of the supplied input string until a different character is reached.
     * @param inputString The input string from which to strip the specified characters.
     * @param stripCharacter The character to strip from the start of the input string.
     * @param minimumLength The minimum length of the output string.  If this length is reach while stripping characters, to further stripping will be done.
     * @return A copy of the input string, with with the specified character stripped from the start as many times as it occurs or until the minimum string length is reached.
     */
    public static String strip(CharSequence inputString, char stripCharacter, int minimumLength)
    {
        if (inputString != null)
        {
            StringBuilder builder;

            builder = new StringBuilder(inputString);
            while ((builder.length() > minimumLength) && (builder.charAt(0) == stripCharacter))
            {
                builder.deleteCharAt(0);
            }

            return builder.toString();
        }
        else return null;
    }

    /**
     * Pads the supplied input character sequence with the specified padding character until the sequence is
     * equal to the required length.
     * @param inputString The input string to pad.
     * @param paddingCharacter The padding character to use when padding the input string.
     * @param requiredLength The required length of the padded output string.
     * @return The padded output string, equal in length to the specified requirement.
     */
    public static String pad(CharSequence inputString, char paddingCharacter, int requiredLength)
    {
        StringBuilder builder;

        builder = new StringBuilder(inputString);
        while (builder.length() < requiredLength)
        {
            builder.insert(0, paddingCharacter);
        }

        return builder.toString();
    }

    /**
     * Chops the input string into parts equal to or shorter than the specified length and returns the part at the index specified.
     * @param inputString The input String to chop.
     * @param length The length of the parts into which the input string will be chopped.
     * @param chopCharacters The characters on which the strings will be chopped.
     * @param trimStrings If true, all of the resultant strings will be trimmed.
     * @return The part of the string, equal to or shorter that the specified length.
     */
    public static List<String> chop(CharSequence inputString, int length, List<Character> chopCharacters, boolean trimStrings)
    {
        if (inputString == null) return new ArrayList<>();
        else
        {
            List<String> choppedStrings;
            StringBuilder buffer;
            int endIndex;

            buffer = new StringBuilder(inputString);
            choppedStrings = new ArrayList<>();
            while (buffer.length() > 0)
            {
                endIndex = length;
                if (endIndex >= buffer.length())
                {
                    choppedStrings.add(buffer.toString());
                    buffer.delete(0, buffer.length());
                }
                else
                {
                    // While the character before the end index is not a valid chop character, decrement the index.
                    while ((!chopCharacters.contains(buffer.charAt(endIndex-1))) && (endIndex > 1))
                    {
                        endIndex--;
                    }

                    // If the end index is still valid i.e. a chop character was found, then process the sub-string.
                    if (endIndex > 1)
                    {
                        String subString;

                        subString = buffer.substring(0, endIndex);
                        choppedStrings.add(trimStrings ? subString.trim() : subString);
                        buffer.delete(0, endIndex);
                    }
                    else // No chop character was found so we do the brute force method.
                    {
                        String subString;

                        endIndex = length;
                        if (endIndex > length) endIndex = length;

                        subString = buffer.substring(0, endIndex);
                        choppedStrings.add(trimStrings ? subString.trim() : subString);
                        buffer.delete(0, endIndex);
                    }
                }
            }

            // Return the list of chopped strings.
            return choppedStrings;
        }
    }

    /**
     * Chops the input string into parts equal to or shorter than the specified length and returns the part at the index specified.
     * This method is null-safe and index-safe meaning that any combination of input parameters is accepted.
     * @param inputString The input String to chop.
     * @param length The length of the parts into which the input string will be chopped.
     * @param partIndex The index of the part of the input string to return.  The first part is at index 0, the second part at index 1 and so forth.
     * @param chopCharacters The characters on which the strings will be chopped.
     * @param trimStrings If true, all of the resultant strings will be trimmed.
     * @return The part of the string, equal to or shorter that the specified length.
     */
    public static String chop(CharSequence inputString, int length, int partIndex, List<Character> chopCharacters, boolean trimStrings)
    {
        List<String> parts;

        // Chop the string up and return the part at the specified index.
        parts = chop(inputString, length, chopCharacters, trimStrings);
        if (partIndex < parts.size()) return parts.get(partIndex);
        else return null;
    }

    /**
     * Chops the input string into parts equal to or shorter than the specified length and returns the part at the index specified.
     * This method is null-safe and index-safe meaning that any combination of input parameters is accepted.
     * @param inputString The input String to chop.
     * @param length The length of the parts into which the input string will be chopped.
     * @param partIndex The index of the part of the input string to return.  The first part is at index 0, the second part at index 1 and so forth.
     * @return The part of the string, equal to or shorter that the specified length.
     */
    public static String chop(CharSequence inputString, int length, int partIndex)
    {
        List<String> parts;

        // Chop the string up and return the part at the specified index.
        parts = chop(inputString, length, ArrayLists.newArrayList(' ', '\n', '\t'), true);
        if (partIndex < parts.size()) return parts.get(partIndex);
        else return null;
    }

    /**
     * Chops the input string into parts equal to or shorter than the specified length and returns the part at the index specified.
     * @param inputString The input String to chop.
     * @param length The length of the parts into which the input string will be chopped.
     * @return The part of the string, equal to or shorter that the specified length.
     */
    public static List<String> chop(CharSequence inputString, int length)
    {
        return chop(inputString, length, ArrayLists.newArrayList(' ', '\n', '\t'), true);
    }

    /**
     * Checks whether or not the string arguments supplied are equal, based on
     * the actual content, ignoring leading and trailing whitespace characters.
     *
     * @param str0 The first {@code String} for comparison
     * @param str1 The second {@code String} for comparison
     *
     * @return {@code true} if the string values are equal. {@code false}
     *      otherwise
     */
    public static boolean equal(String str0, String str1)
    {
        return Objects.equals(trimToNull(str0), trimToNull(str1));
    }

    /**
     * Determines the number of times that a specific character appears in the
     * given string. The "consecutive" flag allows to specify whether or not
     * all repetitions of the given character should be consecutive.
     *
     * @param str The {@code String} to be counted from
     * @param charToCount The {@code char} to be counted within the supplied
     *      string
     * @param consecutive Whether or not the characters need to be consecutive
     *
     * @return The {@code int} number of times the given character appears in
     *      the given {@code String}
     */
    public static int count(String str, char charToCount, boolean consecutive)
    {
        if (str == null) return 0;
        int count = 0;

        for (int cIdx = 0; cIdx < str.length(); cIdx++)
        {
            if (str.charAt(cIdx) == charToCount)
            {
                count++;
            } else if (consecutive) break; // Found another character
        }

        return count;
    }

    private Strings() {}
}
