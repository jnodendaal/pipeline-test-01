package com.pilog.t8.utilities.threads;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/*
 * JBoss, Home of Professional Open Source
 * Copyright 2005, JBoss Inc., and individual contributors as indicated
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 * A reclosable gate with timeout support.
 *
 * @author Jason T. Greene
 */
public class ThreadGate
{
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    private Thread lockingThread;
    private boolean allowLockingThreadAccess;
    private boolean open;
    private int sequence;

    public ThreadGate(boolean open, boolean allowLockingThreadAccess)
    {
        // Open the gate if the initial state is open.
        if (open) open();
        
        // Set the flag that indicate whether or not the locking thread is allowed to pass the gate when closed.
        this.allowLockingThreadAccess = allowLockingThreadAccess;
    }
    
    /**
     * Open the gate.
     */
    public void open()
    {
        lock.lock();
        try
        {
            open = true;
            sequence++;
            condition.signalAll();
            lockingThread = null;
        }
        finally
        {
            lock.unlock();
        }
    }

    /**
     * Close the gate.
     */
    public void close()
    {
        lock.lock();
        try
        {
            open = false;
            
            if (allowLockingThreadAccess) lockingThread = Thread.currentThread();
            else lockingThread = null;
        }
        finally
        {
            lock.unlock();
        }
    }

    /**
     * Waits for the gate to open.
     *
     * @throws InterruptedException if this thread is interrupted
     */
    public void await() throws InterruptedException
    {
        lock.lock();
        try
        {
            if ((allowLockingThreadAccess) && (lockingThread == Thread.currentThread()))
            {
                // The current thread is the locking thread and we must allow access to it.
                return;
            }
            else
            {
                int snapshot = sequence;
                while (!open && snapshot == sequence)
                {
                    condition.await();
                }
            }
        }
        finally
        {
            lock.unlock();
        }
    }

    /**
     * Waits for the gate to open or the specified time to elapse.
     *
     * @param time the maximum time in milliseconds to wait.
     * @return false if gate timeout occurred
     * @throws InterruptedException if this thread is interrupted
     */
    public boolean await(long time) throws InterruptedException
    {
        lock.lock();
        try
        {
            if ((allowLockingThreadAccess) && (lockingThread == Thread.currentThread()))
            {
                // The current thread is the locking thread and we must allow access to it.
                return true;
            }
            else
            {
                int snapshot = sequence;
                boolean success = true;
                while (!open && snapshot == sequence && success)
                {
                    success = condition.await(time, TimeUnit.MILLISECONDS);
                }

                return success;
            }
        }
        finally
        {
            lock.unlock();
        }
    }
}
