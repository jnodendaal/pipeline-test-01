package com.pilog.version;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8CommunicationsClientVersion
{
    public final static String VERSION = "25";
}
