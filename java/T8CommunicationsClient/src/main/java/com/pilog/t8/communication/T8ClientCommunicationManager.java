package com.pilog.t8.communication;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.communication.event.T8CommunicationListener;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.communication.T8CommunicationManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClientCommunicationManager implements T8CommunicationManager
{
    private final T8Context context;

    public T8ClientCommunicationManager(T8Context context)
    {
        this.context = context;
    }

    @Override
    public void addCommunicationListener(T8CommunicationListener listener)
    {
        throw new UnsupportedOperationException("Not supported from client-side.");
    }

    @Override
    public void removeCommunicationListener(T8CommunicationListener listener)
    {
        throw new UnsupportedOperationException("Not supported from client-side.");
    }

    @Override
    public void init() throws Exception
    {
        // Nothing to do here.
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy()
    {
        // Nothing to do here.
    }

    @Override
    public void sendCommunication(T8Context context, String communicationId, Map<String, Object> communicationParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_COMMUNICATION_IDENTIFIER, communicationId);
        operationParameters.put(PARAMETER_COMMUNICATION_PARAMETERS, communicationParameters);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_SEND_COMMUNICATION, operationParameters);
    }

    @Override
    public void queueCommunication(T8Context context, String communicationId, Map<String, Object> communicationParameters) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_COMMUNICATION_IDENTIFIER, communicationId);
        operationParameters.put(PARAMETER_COMMUNICATION_PARAMETERS, communicationParameters);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_QUEUE_COMMUNICATION, operationParameters);
    }

    @Override
    public int getQueuedMessageCount(String serviceId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        return (int)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_QUEUED_MESSAGE_COUNT, operationParameters).get(PARAMETER_MESSAGE_COUNT);
    }

    @Override
    public void cancelMessage(T8Context context, String messageIID) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_MESSAGE_INSTANCE_IDENTIFIER, messageIID);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_CANCEL_COMMUNICATION_MESSAGE, operationParameters);
    }

    @Override
    public void restartService(String serviceIdentifier)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
