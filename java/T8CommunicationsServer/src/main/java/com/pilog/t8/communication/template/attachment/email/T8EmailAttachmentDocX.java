package com.pilog.t8.communication.template.attachment.email;

import com.pilog.t8.communication.template.attachment.T8DefaultAttachment;
import com.pilog.t8.communication.util.DocxTemplateAttachmentMerger;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8EmailDocxAttachmentDefinition;
import com.pilog.t8.security.T8Context;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailAttachmentDocX extends T8DefaultAttachment
{
    private final T8EmailDocxAttachmentDefinition definition;

    public T8EmailAttachmentDocX(T8EmailDocxAttachmentDefinition definition)
    {
        super(definition.getIdentifier(), definition.getAttachmentName(), definition.getAttachmentDescription());
        this.definition = definition;

        //make sure that the attachment name ends with a valid extension
        if(!attachmentName.endsWith(".docx") &&  !definition.shouldConvertToPDF()) attachmentName += ".docx";
        if(!attachmentName.endsWith(".pdf") &&  definition.shouldConvertToPDF()) attachmentName += ".pdf";
    }

    @Override
    public void generateAttachment(T8Context context, Map<String, Object> templateInputParamters) throws Exception
    {
        try
        {
            if (!definition.hasAttachmentData())
            {
                throw new Exception("No attachment data attached to definition " + definition.getPublicIdentifier());
            }
            DocxTemplateAttachmentMerger attachmentMerger = new DocxTemplateAttachmentMerger(TemplateEngineKind.Freemarker, definition.shouldConvertToPDF(), templateInputParamters);
            attachmentMerger.setAttachmentStream(definition.getAttachmentDataAsStream());
            attachmentData = attachmentMerger.getAttachment().toByteArray();
            attachmentMerger.dispose();
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to create attachment data source for " + definition.getPublicIdentifier(), ex);
        }
    }
}
