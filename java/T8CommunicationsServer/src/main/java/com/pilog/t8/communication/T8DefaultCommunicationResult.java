package com.pilog.t8.communication;

import com.pilog.t8.communication.T8CommunicationResult.Result;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8DefaultCommunicationResult implements T8CommunicationResult
{
    private boolean success;
    private Result sendResult;
    private String resultMessage;
    private Exception exception;
    private T8CommunicationMessage message;
    private String messageID;

    public T8DefaultCommunicationResult(boolean success, Result sendResult, T8CommunicationMessage message)
    {
       this(success, sendResult, "", message);
    }

    public T8DefaultCommunicationResult(boolean success, Result sendResult, String resultMessage, T8CommunicationMessage message)
    {
       this(success, sendResult, resultMessage, null , message);
    }

    public T8DefaultCommunicationResult(boolean success, Result sendResult, String resultMessage, Exception exception, T8CommunicationMessage message)
    {
       this.success = success;
       this.sendResult = sendResult;
       this.resultMessage = resultMessage;
       this.exception = exception;
       this.message = message;
    }

    @Override
    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    @Override
    public Result getSendResult()
    {
        return sendResult;
    }

    public void setSendResult(Result sendResult)
    {
        this.sendResult = sendResult;
    }

    @Override
    public String getMessage()
    {
        return resultMessage;
    }

    public void setMessage(String message)
    {
        this.resultMessage = message;
    }

    @Override
    public Exception getException()
    {
        return exception;
    }

    public void setException(Exception exception)
    {
        this.exception = exception;
    }

    @Override
    public T8CommunicationMessage getCommunicationMessage()
    {
        return message;
    }

    public void setCommunicationMessage(T8CommunicationMessage message)
    {
        this.message = message;
    }

    @Override
    public String getMessageId()
    {
        return messageID;
    }

    @Override
    public void setMessageId(String messageID)
    {
        this.messageID = messageID;
    }
}
