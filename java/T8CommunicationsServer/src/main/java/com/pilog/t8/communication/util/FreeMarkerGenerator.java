package com.pilog.t8.communication.util;

import com.pilog.t8.T8CommunicationManager;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to easily generate Free Marker Templates
 * @author hennie.brink@pilog.co.za
 */
public class FreeMarkerGenerator
{
    private static final Configuration freemarkerConfiguration;

    static
    {
        freemarkerConfiguration = new Configuration(new Version(2, 3, 20));
        freemarkerConfiguration.setWhitespaceStripping(true);
    }

    /**
     * Generates a message based on the template provided.
     * @param templateName The Name of this template, for caching purposes. Must be UNIQUE.
     * @param templateParameters The parameters that will be sent to the template
     * @param manager The communication manager
     * @param template The Template that will be used to construct the message
     * @return The generated message
     *
     * @throws Exception If the generation of the text fails
     */
    public static String getFreeMarkerConversion(String templateName, Map<String, Object> templateParameters, T8CommunicationManager manager, String template) throws Exception
    {
        ByteArrayOutputStream byteArrayOutputStream = null;
        PrintWriter out = null;
        try
        {
            Map<String,Object> templateParameterMap;
            Template freeMarkerTemplate;

            //freemarkerConfiguration.clearTemplateCache();
            freeMarkerTemplate = new Template(templateName, template, freemarkerConfiguration);
            byteArrayOutputStream = new ByteArrayOutputStream();
            out = new PrintWriter(byteArrayOutputStream);
            templateParameterMap = new HashMap<>(1);
            templateParameterMap.put("data", templateParameters);
            freeMarkerTemplate.process(templateParameterMap, out);
            return byteArrayOutputStream.toString();
        }
        catch (IOException ex)
        {
            throw new Exception("Failed to generate the subject with the freemarker template", ex);
        }
        catch (TemplateException ex)
        {
            throw new Exception("Failed to generate the subject with the freemarker template", ex);
        }
        finally
        {
            if (out != null)
            {
              out.close();
            }

            if (byteArrayOutputStream != null)
            {
                try
                {
                    byteArrayOutputStream.close();
                }
                catch (IOException ex)
                {
                }
            }
        }
    }
}
