package com.pilog.t8.communication.util;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.core.document.DocumentKind;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to generate a DOCX Document Containing template information
 * @author hennie.brink@pilog.co.za
 */
public class DocxTemplateAttachmentMerger
{
    private final TemplateEngineKind templateEngineKind;
    private final Map<String, Object> parameterInputMap;
    private final Boolean convertToPDF;
    private IXDocReport report;
    private IContext reportContext;
    private InputStream inputStream;
    private ByteArrayOutputStream outputStream;

    public DocxTemplateAttachmentMerger(TemplateEngineKind templateEngineKind, Boolean convertToPDF, Map<String,Object> parameterInputMap)
    {
        this.templateEngineKind = templateEngineKind;
        this.convertToPDF = convertToPDF;
        this.parameterInputMap = new HashMap<>(1);
        this.parameterInputMap.put("data", parameterInputMap);
    }

    public final void setAttachmentStream(InputStream value) throws Exception
    {
        this.inputStream = value;
        try
        {
            report = XDocReportRegistry.getRegistry().loadReport(inputStream, templateEngineKind);
            reportContext = report.createContext();
            if (parameterInputMap != null && !parameterInputMap.isEmpty())
            {
                for (String key : parameterInputMap.keySet())
                {
                    setPlaceHolder(key, parameterInputMap.get(key));
                }
            }
        }
        catch (IOException ex)
        {
            throw new Exception("IO Ezxception while setting attachment Stream.", ex);
        }
        catch (XDocReportException ex)
        {
            throw new Exception("Unkown EXDoc Error while setting attachment stream.", ex);
        }
    }

    /**
     * @return The Generated Attachment
     * @throws T8Exception if generation fails
     */
    public ByteArrayOutputStream getAttachment() throws Exception
    {
        if (inputStream == null)
        {
            throw new Exception("Attachment stream no set, set the attachemnt stream before attempting to get the generated attachment.");
        }
        else
        {
            try
            {
                outputStream = new ByteArrayOutputStream();
                if (!convertToPDF)
                {
                    report.process(reportContext, outputStream);
                }
                else
                {
                    report.convert(reportContext, Options.getFrom(DocumentKind.DOCX).to(ConverterTypeTo.PDF), outputStream);
                }
                return outputStream;
            }
            catch (IOException ex)
            {
                throw new Exception("IO Ezxception while setting attachment Stream.", ex);
            }
            catch (XDocReportException ex)
            {
                throw new Exception("Unkown EXDoc Error while setting attachment stream.", ex);
            }
        }
    }

    /**
     * @param name The placeholder name
     * @param value The placeholder value
     */
    public void setPlaceHolder(String name, Object value)
    {
        reportContext.put(name, value);
    }

    /**
     * Releases resources
     */
    public void dispose()
    {
        // Make sure to uncache the report.
        XDocReportRegistry.getRegistry().unregisterReport(report);
        reportContext = null;
        report = null;

        // Close input stream.
        if (inputStream != null)
        {
            try
            {
                inputStream.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        // Close output stream.
        if (outputStream != null)
        {
            try
            {
                outputStream.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
