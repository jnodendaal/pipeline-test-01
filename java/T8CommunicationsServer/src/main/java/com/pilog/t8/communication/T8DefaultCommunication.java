package com.pilog.t8.communication;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.communication.T8CommunicationTypeDefinition;
import com.pilog.t8.definition.communication.T8DefaultCommunicationDefinition;
import com.pilog.t8.definition.communication.detail.provider.T8CommunicationDetailsProviderAdapterDefinition;
import com.pilog.t8.definition.communication.detail.provider.T8CommunicationDetailsProviderDefinition;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.data.T8DataTransaction;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.T8IdentifierUtilities.mapParameters;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultCommunication implements T8Communication
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8DefaultCommunication.class.getName());

    private final String iid;
    private final T8DefaultCommunicationDefinition definition;
    private final T8CommunicationManager manager;
    private T8DataTransaction tx;
    private T8Context context;
    private Map<String, Object> communicationParameters;
    private ExpressionEvaluator expressionEvaluator;
    private T8MessageTemplate messageGenerator;
    private T8CommunicationTypeDefinition currentCommunicationTypeDefinition;
    private Iterator<T8CommunicationDetails> communicationDetailsIterator;
    private Iterator<T8CommunicationTypeDefinition> communicationTypesIterator;
    private T8PerformanceStatistics stats;

    private static final String STATS_INIT_COMMUNICATION_TYPE = "INIT_COMMUNICATION_TYPE";
    private static final String STATS_GENERATE_MESSAGE = "GENERATE_MESSAGE";

    public T8DefaultCommunication(T8DefaultCommunicationDefinition definition, T8CommunicationManager manager)
    {
        this.iid = T8IdentifierUtilities.createNewGUID();
        this.definition = definition;
        this.manager = manager;
    }

    @Override
    public String getIdentifier()
    {
        return definition.getIdentifier();
    }

    @Override
    public String getInstanceIdentifier()
    {
        return iid;
    }

    @Override
    public void init(T8DataTransaction tx, Map<String, Object> inputParameters)
    {
        // Set the context for this communication.
        this.tx = tx;
        this.stats = tx.getPerformanceStatistics();
        this.context = tx.getContext();

        // Log the start of initialization.
        stats.logExecutionStart("INIT_COMMUNICATION");

        // Populate the parameters with default null values so that the expressions that use them will not fail if they were not sent.
        communicationParameters = new HashMap<>();
        expressionEvaluator = new T8ServerExpressionEvaluator(context);
        if (definition.getInputParameters() != null)
        {
            for (T8DataParameterDefinition parameterDefinition : definition.getInputParameters())
            {
                communicationParameters.put(parameterDefinition.getPublicIdentifier(), null);
            }
        }

        // Now add the user provided parameters.
        if (inputParameters != null) communicationParameters.putAll(inputParameters);

        // Make sure the communication is marked as active.
        if (definition.isActive())
        {
            // Set the communication type iterator.
            this.communicationTypesIterator = definition.getCommunicationTypeDefinitions().iterator();
        }
        else LOGGER.log(definition.getIdentifier() + " marked inactive.");

        // Log end of initialization.
        stats.logExecutionEnd("INIT_COMMUNICATION");
    }

    @Override
    public void destroy()
    {
        // Nothing to destroy yet.
    }

    /**
     * This method attempts to initialize the next communication type for which messages must be generated.
     * If all types have been iterator over, this method returns a boolean false to indicate the end of cycle.
     * @return A boolean true if a next communication type was available, false otherwise.
     * @throws Exception
     */
    private boolean initNextCommunicationType() throws Exception
    {
        // Make sure we have more types to use.
        if (communicationTypesIterator.hasNext())
        {
            // Get the next communication type and make sure it is active.
            currentCommunicationTypeDefinition = communicationTypesIterator.next();
            if (currentCommunicationTypeDefinition.isActive())
            {
                Map<String, Object> conditionInputParameters;
                String conditionExpression;

                // Check the communication type condition expression.
                conditionExpression = currentCommunicationTypeDefinition.getConditionExpression();
                conditionInputParameters = T8IdentifierUtilities.stripNamespace(definition.getIdentifier(), communicationParameters, false);
                if (Strings.isNullOrEmpty(conditionExpression) || expressionEvaluator.evaluateBooleanExpression(conditionExpression, conditionInputParameters, null))
                {
                    if (currentCommunicationTypeDefinition.getMessageTemplateIdentifier() != null)
                    {
                        List<T8CommunicationDetailsProviderAdapterDefinition> detailProviderAdaptorDefinitions;

                        // Get the next list of detail provider adaptor definitions and use them to generate communication details.
                        detailProviderAdaptorDefinitions = currentCommunicationTypeDefinition.getCommunicationDetailProviderAdapters();
                        if (!detailProviderAdaptorDefinitions.isEmpty())
                        {
                            Map<String, Object> messageTemplateParameters;
                            T8CommunicationMessageDefinition messageDefinition;
                            Set<T8CommunicationDetails> communicationDetails;
                            T8DefinitionManager definitionManager;

                            // Get the Message Template.
                            definitionManager = context.getServerContext().getDefinitionManager();
                            messageTemplateParameters = mapParameters(communicationParameters, currentCommunicationTypeDefinition.getMessageTemplateParameterMapping());
                            messageDefinition = ((T8CommunicationMessageDefinition) definitionManager.getRawDefinition(context, currentCommunicationTypeDefinition.getRootProjectId(), currentCommunicationTypeDefinition.getMessageTemplateIdentifier()));
                            messageGenerator = messageDefinition.getMessageTemplateInstance(context, messageTemplateParameters);

                            // Initialize the message template.
                            messageGenerator.initializeGenerator(tx);

                            // Get the communication details.
                            communicationDetails = new LinkedHashSet<>();
                            for (T8CommunicationDetailsProviderAdapterDefinition detailProviderAdaptorDefinition : detailProviderAdaptorDefinitions)
                            {
                                String detailProviderId;

                                // Get the id of the provider to use, fetch it and generate details.
                                detailProviderId = detailProviderAdaptorDefinition.getCommunicationDetailProviderDefinitionIdentifier();
                                if (detailProviderId != null)
                                {
                                    T8CommunicationDetailsProviderDefinition detailProviderDefinition;
                                    Map<String, Object> detailProviderParameters;

                                    detailProviderParameters = mapParameters(communicationParameters, detailProviderAdaptorDefinition.getCommunicationDetailProviderInputMapping());
                                    detailProviderDefinition = (T8CommunicationDetailsProviderDefinition)definitionManager.getRawDefinition(context, currentCommunicationTypeDefinition.getRootProjectId(), detailProviderId);
                                    if (detailProviderDefinition.isActive())
                                    {
                                        T8CommunicationDetailsProvider provider;

                                        // Get an instance of the provider and generate all applicable details.
                                        provider = detailProviderDefinition.getCommunicationDetailProviderInstance(context, detailProviderParameters);
                                        communicationDetails.addAll(provider.getCommunicationDetails(tx, currentCommunicationTypeDefinition.getType(), detailProviderAdaptorDefinition.getMessageDetailsInputMapping()));
                                    }
                                    else
                                    {
                                        LOGGER.log("The Detail Provider " + detailProviderDefinition + " of communication " + definition.getIdentifier() + " is not active.");
                                    }
                                }
                                else
                                {
                                    LOGGER.log("The Detail Provider Identifier on the adapter " + detailProviderAdaptorDefinition.getIdentifier() + " of communication " + definition.getIdentifier() + " is not provided.");
                                }
                            }

                            // Set the communication details iterator.
                            communicationDetailsIterator = communicationDetails.iterator();
                            return true;
                        }
                        else
                        {
                            LOGGER.log("No communication detail providers set for " + currentCommunicationTypeDefinition.getPublicIdentifier());
                            return initNextCommunicationType();
                        }
                    }
                    else
                    {
                        LOGGER.log("No Message Template Identifier provided for " + currentCommunicationTypeDefinition.getPublicIdentifier() + ", it is required in order to send a message.");
                        return initNextCommunicationType();
                    }
                }
                else
                {
                    LOGGER.log(currentCommunicationTypeDefinition.getPublicIdentifier() + " not sent:  Condition expression '" + conditionExpression + "' returned false using parameters: " + conditionInputParameters);
                    return initNextCommunicationType();
                }
            }
            else
            {
                LOGGER.log(currentCommunicationTypeDefinition.getPublicIdentifier() + " not sent:  Marked inactive.");
                return initNextCommunicationType();
            }
        }
        else return false; // No more definitions types left.
    }

    @Override
    public T8CommunicationMessage generateNextMessage() throws Exception
    {
        T8CommunicationMessage message;
        T8CommunicationDetails details;

        // Make sure we have more communication details to generate a message for.  If not, return null.
        stats.logExecutionStart(STATS_INIT_COMMUNICATION_TYPE);
        while ((communicationDetailsIterator == null) || (!communicationDetailsIterator.hasNext()))
        {
            // Initialize the next communication type and if none are left, return null to indicate end of generation.
            if (!initNextCommunicationType())
            {
                stats.logExecutionEnd(STATS_INIT_COMMUNICATION_TYPE);
                return null;
            }
        }
        stats.logExecutionEnd(STATS_INIT_COMMUNICATION_TYPE);

        // Get the next communication details for which to generate a message and verify it before proceeding.
        details = communicationDetailsIterator.next();
        details.verifyRecipientAddress();

        // Create the message and add it to the applicable service queue.
        stats.logExecutionStart(STATS_GENERATE_MESSAGE);
        message = messageGenerator.generateMessage(context, details, manager, details.getAdditionalDetails());
        stats.logExecutionEnd(STATS_GENERATE_MESSAGE);
        message.setServiceIdentifier(currentCommunicationTypeDefinition.getServiceIdentifier());
        message.setCommunicationIdentifier(definition.getIdentifier());
        message.setCommunicationInstanceIdentifier(iid);
        return message;
    }
}
