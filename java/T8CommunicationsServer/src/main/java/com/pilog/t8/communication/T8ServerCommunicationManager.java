package com.pilog.t8.communication;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.communication.T8Communication.CommunicationHistoryEvent;
import com.pilog.t8.communication.event.T8MessageCancelledEvent;
import com.pilog.t8.communication.event.T8MessageFailedEvent;
import com.pilog.t8.communication.event.T8MessageQueuedEvent;
import com.pilog.t8.communication.event.T8MessageSentEvent;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.definition.event.T8DefinitionManagerEventAdapter;
import com.pilog.t8.definition.event.T8DefinitionManagerListener;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.definition.communication.T8CommunicationServiceDefinition;
import com.pilog.t8.definition.communication.T8DefaultCommunicationDefinition;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.swing.event.EventListenerList;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.communication.event.T8CommunicationServiceListener;
import com.pilog.t8.communication.event.T8CommunicationListener;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ServerCommunicationManager implements T8CommunicationManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerCommunicationManager.class);

    private final T8Context internalContext;
    private final T8ServerContext serverContext;
    private final T8DefinitionManager definitionManager;
    private final EventListenerList communicationListeners;
    private final T8CommunicationPersistenceHandler persistenceHandler;
    private final Map<String, T8CommunicationService> communicationServices;
    private final T8CommunicationServiceListener serviceListener;
    private T8DefinitionManagerListener definitionManagerListener;
    private boolean managerEnabled;

    private static final String STATS_SEND_MESSAGE = "SEND_MESSAGE";
    private static final String STATS_QUEUE_MESSAGE = "QUEUE_MESSAGE";

    public T8ServerCommunicationManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerCommunicationManager.this);
        this.definitionManager = serverContext.getDefinitionManager();
        this.communicationListeners = new EventListenerList();
        this.persistenceHandler = new T8CommunicationPersistenceHandler(internalContext);
        this.communicationServices = Collections.synchronizedMap(new HashMap<String, T8CommunicationService>());
        this.serviceListener = new CommunicationServiceListener();
    }

    @Override
    public void init()
    {
        try
        {
            T8SystemDefinition systemDefinition;

            // Get the system definition to determine configuration.
            systemDefinition = definitionManager.getSystemDefinition(internalContext);
            managerEnabled = systemDefinition != null && systemDefinition.isCommunicationManagerEnabled();

            // Start all available communication services.
            for (T8DefinitionMetaData definitionMetaData : definitionManager.getGroupDefinitionMetaData(null, T8CommunicationServiceDefinition.GROUP_IDENTIFIER))
            {
                try
                {
                    T8CommunicationServiceDefinition serviceDefinition;

                    // Get the service definition and initialize it if active.
                    serviceDefinition = (T8CommunicationServiceDefinition)definitionManager.getInitializedDefinition(null, definitionMetaData.getProjectId(), definitionMetaData.getId(), null);
                    if (serviceDefinition.isActive())
                    {
                        T8CommunicationService service;

                        // Initialize the service.
                        service = serviceDefinition.getServiceInstance(internalContext, this);
                        service.setEnabled(managerEnabled);
                        service.init();
                        service.addServiceListener(serviceListener);
                        communicationServices.put(serviceDefinition.getIdentifier(), service);
                    }
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while initializing communication service: " + definitionMetaData.getId(), e);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing communication manager.", e);
        }
    }

    @Override
    public void start() throws Exception
    {
        // Add the definition manager listener.
        definitionManagerListener = new DefinitionManagerListener();
        definitionManager.addDefinitionManagerListener(definitionManagerListener);
    }

    @Override
    public void destroy()
    {
        // Remove the definition manager listener.
        definitionManager.removeDefinitionManagerListener(definitionManagerListener);

        // Stop all communication services.
        for (T8CommunicationService service : communicationServices.values())
        {
            try
            {
                // Stop the service.
                service.removeServiceListener(serviceListener);
                service.destroy();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while stopping communication service '" + service.getIdentifier() + "'.", e);
            }
        }
    }

    @Override
    public void restartService(String serviceId)
    {
        try
        {
            T8CommunicationServiceDefinition serviceDefinition;
            T8CommunicationService service;

            // If the service is active, shut it down.
            service = communicationServices.remove(serviceId);
            if (service != null)
            {
                // Destroy the existing service.
                LOGGER.log("Destroying communication service: " + serviceId);
                service.removeServiceListener(serviceListener);
                service.destroy();
            }

            // Get the service definition and initialize it if active.
            serviceDefinition = (T8CommunicationServiceDefinition)definitionManager.getInitializedDefinition(null, null, serviceId, null);
            if (serviceDefinition.isActive())
            {
                // Initialize the new service instance.
                LOGGER.log("Initializing communication service: " + serviceId);
                service = serviceDefinition.getServiceInstance(internalContext, this);
                service.init();
                service.addServiceListener(serviceListener);
                communicationServices.put(serviceDefinition.getIdentifier(), service);
            }
        }
        catch(Exception ex)
        {
            LOGGER.log("Failed to restart service: " + serviceId, ex);
        }
    }

    @Override
    public void addCommunicationListener(T8CommunicationListener listener)
    {
        communicationListeners.add(T8CommunicationListener.class, listener);
    }

    @Override
    public void removeCommunicationListener(T8CommunicationListener listener)
    {
        communicationListeners.remove(T8CommunicationListener.class, listener);
    }

    @Override
    public void sendCommunication(T8Context senderSessionContext, String communicationId, Map<String, Object> communicationParameters)
    {
        T8DataTransaction tx;
        T8DataSession ds;
        boolean newTx = false;

        // Get the transaction to use for this operation.
        ds = serverContext.getDataManager().getCurrentSession();
        tx = ds.getTransaction();
        if (tx == null)
        {
            tx = ds.beginTransaction();
            newTx = true;
        }

        try
        {
            T8DefaultCommunicationDefinition communicationDefinition;

            // Get the communication definition and send it using a separate thread.
            communicationDefinition = (T8DefaultCommunicationDefinition)definitionManager.getRawDefinition(senderSessionContext, null, communicationId);
            if (communicationDefinition != null)
            {
                T8CommunicationMessage nextMessage;
                T8Communication communication;
                T8PerformanceStatistics stats;

                // Get the performance statistics to use during this operation.
                stats = tx.getPerformanceStatistics();

                // Initialize the communication.
                communication = communicationDefinition.getCommunicationInstance(T8ServerCommunicationManager.this);
                communication.init(tx, communicationParameters);

                // Generate all messages to be sent and route each one to the applicable service.
                nextMessage = communication.generateNextMessage();
                while (nextMessage != null)
                {
                    T8CommunicationService service;
                    String serviceId;

                    // Get the applicable service and send the message.
                    serviceId = nextMessage.getServiceIdentifier();
                    service = communicationServices.get(serviceId);
                    if (service != null)
                    {
                        stats.logExecutionStart(STATS_SEND_MESSAGE);
                        service.sendMessage(nextMessage);
                        stats.logExecutionEnd(STATS_SEND_MESSAGE);
                    }
                    else throw new Exception("Communication service not found: " + serviceId);

                    // Generate the next message.
                    nextMessage = communication.generateNextMessage();
                }

                // Destroy the communication.
                communication.destroy();

                // Log the fact that the communication has been sent.
                persistenceHandler.logCommunicationEvent(senderSessionContext, CommunicationHistoryEvent.SENT, communication, communicationParameters);
            }
            else throw new Exception("Communication not defined: " + communicationId);

            // Commit the transaction if we created it.
            if (newTx) tx.commit();
        }
        catch (Exception ex)
        {
            // Rollback the transaction if we created it.
            if (newTx) tx.rollback();
            LOGGER.log("Failed to send communication: " + communicationId, ex);
        }
    }

    @Override
    public void queueCommunication(T8Context senderContext, String communicationId, Map<String, Object> communicationParameters)
    {
        T8DataTransaction tx;
        T8DataSession ds;
        boolean newTx = false;

        // Get the transaction to use for this operation.
        ds = serverContext.getDataManager().getCurrentSession();
        tx = ds.getTransaction();
        if (tx == null)
        {
            tx = ds.beginTransaction();
            newTx = true;
        }

        try
        {
            T8DefaultCommunicationDefinition communicationDefinition;

            // Get the communication definition and send it using a separate thread.
            communicationDefinition = (T8DefaultCommunicationDefinition)definitionManager.getRawDefinition(senderContext, null, communicationId);
            if (communicationDefinition != null)
            {
                T8CommunicationMessage nextMessage;
                T8Communication communication;
                T8PerformanceStatistics stats;

                // Get the performance statistics to use during this operation.
                stats = tx.getPerformanceStatistics();

                // Initialize the communication.
                communication = communicationDefinition.getCommunicationInstance(T8ServerCommunicationManager.this);
                communication.init(tx, communicationParameters);

                // Generate all messages to be sent and route each one to the applicable service.
                nextMessage = communication.generateNextMessage();
                while (nextMessage != null)
                {
                    T8CommunicationService service;
                    String serviceId;

                    // Get the applicable service and send the message.
                    serviceId = nextMessage.getServiceIdentifier();
                    service = communicationServices.get(serviceId);
                    if (service != null)
                    {
                        stats.logExecutionStart(STATS_QUEUE_MESSAGE);
                        service.queueMessage(nextMessage);
                        stats.logExecutionEnd(STATS_QUEUE_MESSAGE);
                    }
                    else throw new Exception("Communication service not found: " + serviceId);

                    // Generate the next message.
                    nextMessage = communication.generateNextMessage();
                }

                // Destroy the communication.
                communication.destroy();

                // Log the fact that the communication has been sent.
                persistenceHandler.logCommunicationEvent(senderContext, CommunicationHistoryEvent.QUEUED, communication, communicationParameters);
            }
            else throw new Exception("Communication not defined: " + communicationId);

            // Commit the transaction if we created it.
            if (newTx) tx.commit();
        }
        catch (Exception ex)
        {
            // Rollback the transaction if we created it.
            if (newTx) tx.rollback();
            LOGGER.log("Failed to send communication: " + communicationId, ex);
        }
    }

    @Override
    public void cancelMessage(T8Context context, String messageIid) throws Exception
    {
        for (T8CommunicationService service : communicationServices.values())
        {
            service.cancelMessage(messageIid);
        }
    }

    @Override
    public int getQueuedMessageCount(String serviceId)
    {
        // TODO: Retrieve and return mesage count from state tables.
        return 0;
    }

    protected void fireCommunicationMessageSentEvent(T8CommunicationMessage message)
    {
        T8MessageSentEvent event;

        event = new T8MessageSentEvent(message);
        for (T8CommunicationListener listener : communicationListeners.getListeners(T8CommunicationListener.class))
        {
            listener.messageSent(event);
        }
    }

    protected void fireCommunicationMessageFailedEvent(T8CommunicationMessage message, Exception exception)
    {
        T8MessageFailedEvent event;

        event = new T8MessageFailedEvent(message, exception);
        for (T8CommunicationListener listener : communicationListeners.getListeners(T8CommunicationListener.class))
        {
            listener.messageFailed(event);
        }
    }

    protected void fireCommunicationMessageCancelledEvent(T8CommunicationMessage message)
    {
        T8MessageCancelledEvent event;

        event = new T8MessageCancelledEvent(message);
        for (T8CommunicationListener listener : communicationListeners.getListeners(T8CommunicationListener.class))
        {
            listener.messageCancelled(event);
        }
    }

    protected void fireCommunicationMessageQueuedEvent(T8CommunicationMessage message)
    {
        T8MessageQueuedEvent event;

        event = new T8MessageQueuedEvent(message);
        for (T8CommunicationListener listener : communicationListeners.getListeners(T8CommunicationListener.class))
        {
            listener.messageQueued(event);
        }
    }

    private class CommunicationServiceListener implements T8CommunicationServiceListener
    {
        @Override
        public void messageQueued(T8MessageQueuedEvent event)
        {
            fireCommunicationMessageQueuedEvent(event.getMessage());
        }

        @Override
        public void messageSent(T8MessageSentEvent event)
        {
            fireCommunicationMessageSentEvent(event.getMessage());
        }

        @Override
        public void messageFailed(T8MessageFailedEvent event)
        {
            fireCommunicationMessageFailedEvent(event.getMessage(), event.getException());
        }

        @Override
        public void messageCancelled(T8MessageCancelledEvent event)
        {
            fireCommunicationMessageCancelledEvent(event.getMessage());
        }
    }

    /**
     * This listener clears cached message definitions as they are updated or
     * reloaded by the definition manager.
     */
    private class DefinitionManagerListener extends T8DefinitionManagerEventAdapter
    {
        @Override
        public void definitionSaved(T8DefinitionSavedEvent event)
        {
            T8Definition eventDefinition;

            eventDefinition = event.getDefinition();
            if (eventDefinition instanceof T8CommunicationServiceDefinition)
            {
                restartService(event.getDefinition().getIdentifier());
            }
        }
    }
}
