package com.pilog.t8.communication.template;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.communication.T8DefaultMessageTemplate;
import com.pilog.t8.communication.message.T8SMSMessage;
import com.pilog.t8.communication.util.FreeMarkerGenerator;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.communication.message.template.T8SMSMessageTemplateFreeMarkerDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;

/**
 * Uses the Free Marker Templating Engine to generate SMS Templates
 * @author hennie.brink@pilog.co.za
 */
public class T8SMSMessageTemplateFreeMarker extends T8DefaultMessageTemplate
{
    private Map<String, Object> templateParameters = new HashMap<>();

    public T8SMSMessageTemplateFreeMarker(T8Context context, T8SMSMessageTemplateFreeMarkerDefinition definition, Map<String, Object> inputParameters)
    {
        super(context, definition, inputParameters);

    }

    public String generateMessageContent( T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        Map<String, Object> templateParamDetails;

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);
        //add additional detail provider details if provided
        if(additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        return FreeMarkerGenerator.getFreeMarkerConversion(getDefinition().getPublicIdentifier(),templateParamDetails, manager, getDefinition().getTemplate());
    }

    @Override
    public T8CommunicationMessage generateMessage(T8Context senderContext, T8CommunicationDetails communicationDetails, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        T8SessionContext sessionContext;
        T8SMSMessage smsMessage;
        String messageContent;

        // Generate message content.
        messageContent = generateMessageContent(communicationDetails, manager, additionDetails);

        // Create the new message.
        smsMessage = new T8SMSMessage(communicationDetails.getRecipientDisplayName(), communicationDetails.getMessagePriority(), communicationDetails.getRecipientAddress(), messageContent);
        smsMessage.setIdentifier(definition.getIdentifier());
        smsMessage.setRecipientDefinitionIdentifier(communicationDetails.getRecipientDefinitionIdentifier());
        smsMessage.setRecipientRecordID(communicationDetails.getRecipientRecordID());

        // Add sender information.
        sessionContext = senderContext.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            smsMessage.setSenderIdentifier(sessionContext.getSystemAgentIdentifier());
            smsMessage.setSenderInstanceIdentifier(sessionContext.getSystemAgentInstanceIdentifier());
        }
        else
        {
            smsMessage.setSenderIdentifier(sessionContext.getUserIdentifier());
            smsMessage.setSenderInstanceIdentifier(null);
        }

        return smsMessage;
    }

    public boolean supportsAttachment()
    {
        return false;
    }

    public List<T8CommunicationMessageAttachment> getAttachments(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails)
    {
        return null;
    }

    @Override
    public void initializeGenerator(T8DataTransaction tx) throws Exception
    {
        try
        {
            T8ServerContextScriptDefinition scriptDefinition;

            scriptDefinition = definition.getTemplateDataScript();
            if (scriptDefinition != null)
            {
                T8Script script;

                // Run the script.
                script = scriptDefinition.getNewScriptInstance(tx.getContext());
                templateParameters = script.executeScript(getInputParameters());
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to execute the script to generate the subject field template parameters", ex);
        }
    }
}
