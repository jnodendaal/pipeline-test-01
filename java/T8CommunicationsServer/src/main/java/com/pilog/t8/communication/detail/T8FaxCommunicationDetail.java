package com.pilog.t8.communication.detail;

import com.pilog.t8.utilities.strings.Strings;

/**
 * @author Hennie Brink
 */
public class T8FaxCommunicationDetail extends T8DefaultCommunicationDetails
{
    private String faxNumber;
    private String faxRecipientCompany;

    public T8FaxCommunicationDetail(String faxNumber, int priority, String recipientName)
    {
        super(priority, recipientName);
        this.faxNumber = faxNumber;
    }

    public T8FaxCommunicationDetail(String faxNumber, String recipientName)
    {
        super(recipientName);
        this.faxNumber = faxNumber;
    }

    public T8FaxCommunicationDetail(String faxNumber, int priority, String recipientName, String faxRecipientCompany)
    {
        super(priority, recipientName);
        this.faxNumber = faxNumber;
        this.faxRecipientCompany = faxRecipientCompany;
    }

    public T8FaxCommunicationDetail(String faxNumber, String recipientName, String faxRecipientCompany)
    {
        super(recipientName);
        this.faxNumber = faxNumber;
        this.faxRecipientCompany = faxRecipientCompany;
    }

    public String getFaxNumber()
    {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    public String getFaxRecipientCompany()
    {
        return faxRecipientCompany;
    }

    public void setFaxRecipientCompany(String faxRecipientCompany)
    {
        this.faxRecipientCompany = faxRecipientCompany;
    }

    @Override
    public String getRecipientAddress()
    {
        return getFaxNumber();
    }

    @Override
    public void verifyRecipientAddress() throws Exception
    {
        if(Strings.isNullOrEmpty(faxNumber) || Strings.isNullOrEmpty(getRecipientDisplayName())) throw new IllegalArgumentException("The fax number registered for " + getRecipientDisplayName() + " is empty, ensure that fax number for the recipient is provided in order to send a communication.");

    }
}
