/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.detail.provider;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.communication.detail.T8EmailCommunicationDetail;
import com.pilog.t8.communication.util.MobileNumberUtils;
import com.pilog.t8.definition.communication.detail.provider.T8FixedEmailDetailProviderDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;


/**
 * The T8 UserDetail Providers returns user details for users registered in the T8 System.
 * <p/>
 * @author hennie.brink@pilog.co.za
 */
public class T8FixedEmailDetailProvider implements
        T8CommunicationDetailsProvider
{
    private static final T8Logger logger = com.pilog.t8.T8Log.getLogger(T8FixedEmailDetailProvider.class.getName());
    private T8FixedEmailDetailProviderDefinition definition;
    private T8ServerContext serverContext;
    private T8SessionContext sessionContext;
    private Map<String, Object> providerParameters;

    public T8FixedEmailDetailProvider(Map<String, Object> providerParameters,
                                      T8FixedEmailDetailProviderDefinition definition,
                                      T8ServerContext serverContext,
                                      T8SessionContext sessionContext)
    {
        this.definition = definition;
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.providerParameters = providerParameters;
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        return definition.getSupportedCommunicationTypes();
    }

    @Override
    public Set<T8CommunicationDetails> getCommunicationDetails(T8DataTransaction tx, T8CommunicationType communicationType, Map<String, String> messageDetailsMapping)
    {
        Set<T8CommunicationDetails> communicationDetails;
        List<String> alternateEmailAddresses;
        String alternateEmailAddress;
        String parameterIdentifier = definition.getPublicIdentifier() + T8FixedEmailDetailProviderDefinition.P_ALTERNATE_EMAIL_LIST;
        try
        {
            alternateEmailAddresses = (List<String>) providerParameters.get(parameterIdentifier);
        }
        catch (ClassCastException ce)
        {
            throw new RuntimeException("The parameter " + parameterIdentifier + " was of type " + providerParameters.get(parameterIdentifier).getClass().getCanonicalName() + ", List<String> was expected.");
        }
        parameterIdentifier = definition.getPublicIdentifier() + T8FixedEmailDetailProviderDefinition.P_ALTERNATE_EMAIL;
        try
        {
            alternateEmailAddress = (String) providerParameters.get(parameterIdentifier);
        }
        catch (ClassCastException ce)
        {
            throw new RuntimeException("The parameter " + parameterIdentifier + " was of type " + providerParameters.get(parameterIdentifier).getClass().getCanonicalName() + ", List<String> was expected.");
        }
        communicationDetails = new LinkedHashSet<>();
        if (alternateEmailAddresses != null)
        {
            for (String emailAddress : alternateEmailAddresses)
            {
                try
                {
                    T8EmailCommunicationDetail t8EmailCommunicationDetail = new T8EmailCommunicationDetail(MobileNumberUtils.checkValid(emailAddress), emailAddress);
                    HashMap<String,Object> additionalDetails = HashMaps.newHashMap(definition.getIdentifier() + "EMAIL_ADDRESS", emailAddress);
                    t8EmailCommunicationDetail.setAdditionalDetails(T8IdentifierUtilities.mapParameters(additionalDetails, messageDetailsMapping));
                    communicationDetails.add(t8EmailCommunicationDetail);
                }
                catch (Exception ex)
                {
                    logger.log("Failed to load communication details for " + emailAddress, ex);
                }
            }
        }
        if (alternateEmailAddress != null)
        {
            try
            {
                T8EmailCommunicationDetail t8EmailCommunicationDetail = new T8EmailCommunicationDetail(MobileNumberUtils.checkValid(alternateEmailAddress), alternateEmailAddress);
                HashMap<String,Object> additionalDetails = HashMaps.newHashMap(definition.getIdentifier() + "EMAIL_ADDRESS", alternateEmailAddress);
                t8EmailCommunicationDetail.setAdditionalDetails(T8IdentifierUtilities.mapParameters(additionalDetails, messageDetailsMapping));
                communicationDetails.add(t8EmailCommunicationDetail);
            }
            catch (Exception ex)
            {
                logger.log("Failed to load communication details for " + alternateEmailAddress, ex);
            }
        }
        return communicationDetails;
    }
}
