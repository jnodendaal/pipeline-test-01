/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.service.clickatell;

import javax.xml.bind.annotation.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
@XmlRootElement(name = "clickAPI")
@XmlAccessorType(XmlAccessType.NONE)
public class ClickATellApi
{

    public ClickATellApi()
    {
    }
    @XmlElement(name = "sendMsg")
    private SendMessage sendMessage;
    @XmlElement(name = "auth")
    private Authentication auth;
    @XmlElement(name = "sendMsgResp")
    private SendMessageResponse sendMsgResp;
    @XmlElement(name = "authResp")
    private AuthenticationResponse authResp;
    @XmlElement(name = "xmlErrorResp")
    private XMLErrorResponse xmlErrResponse;

    /**
     * @return the sendMessage
     */
    public SendMessage getSendMessage()
    {
        return sendMessage;
    }

    /**
     * @param sendMessage the sendMessage to set
     */
    public void setSendMessage(
            SendMessage sendMessage)
    {
        this.sendMessage = sendMessage;
    }

    /**
     * @return the auth
     */
    public Authentication getAuth()
    {
        return auth;
    }

    /**
     * @param auth the auth to set
     */
    public void setAuth(
            Authentication auth)
    {
        this.auth = auth;
    }


    /**
     * @return the sendMsgResp
     */
    public SendMessageResponse getSendMsgResp()
    {
        return sendMsgResp;
    }

    /**
     * @param sendMsgResp the sendMsgResp to set
     */
    public void setSendMsgResp(
            SendMessageResponse sendMsgResp)
    {
        this.sendMsgResp = sendMsgResp;
    }

    /**
     * @return the authResp
     */
    public AuthenticationResponse getAuthResp()
    {
        return authResp;
    }

    /**
     * @param authResp the authResp to set
     */
    public void setAuthResp(
            AuthenticationResponse authResp)
    {
        this.authResp = authResp;
    }

    /**
     * @return the xmlErrResponse
     */
    public XMLErrorResponse getXmlErrResponse()
    {
        return xmlErrResponse;
    }

    /**
     * @param xmlErrResponse the xmlErrResponse to set
     */
    public void setXmlErrResponse(
                                  XMLErrorResponse xmlErrResponse)
    {
        this.xmlErrResponse = xmlErrResponse;
    }

    @XmlRootElement(name = "sendMsg")
    @XmlAccessorType(XmlAccessType.NONE)
    public static class SendMessage extends Authentication
    {
        @XmlElement(name = "session_id")
        private String session_id;
        @XmlElement(name = "to")
        private String to;
        @XmlElement(name = "text")
        private String text;
        @XmlElement(name = "from")
        private String from;
        @XmlElement(name = "concat")
        private Integer concat;
        @XmlElement(name = "callback")
        private Integer callback;
        @XmlElement(name = "queue")
        private Integer queue;

        /**
         * @return the to
         */
        public String getTo()
        {
            return to;
        }

        /**
         * @param to the to to set
         */
        public void setTo(String to)
        {
            this.to = to;
        }

        /**
         * @return the text
         */
        public String getText()
        {
            return text;
        }

        /**
         * @param text the text to set
         */
        public void setText(String text)
        {
            this.text = text;
        }

        /**
         * @return the from
         */
        public String getFrom()
        {
            return from;
        }

        /**
         * @param from the from to set
         */
        public void setFrom(String from)
        {
            this.from = from;
        }

        /**
         * @return the concat
         */
        public Integer getConcat()
        {
            return concat;
        }

        /**
         * @param concat the concat to set
         */
        public void setConcat(Integer concat)
        {
            this.concat = concat;
        }

        /**
         * @return the callback
         */
        public Integer getCallback()
        {
            return callback;
        }

        /**
         * @param callback the callback to set
         */
        public void setCallback(Integer callback)
        {
            this.callback = callback;
        }

        /**
         * @return the session_id
         */
        public String getSession_id()
        {
            return session_id;
        }

        /**
         * @param session_id the session_id to set
         */
        public void setSession_id(String session_id)
        {
            this.session_id = session_id;
        }

        /**
         * @return the queue
         */
        public Integer getQueue()
        {
            return queue;
        }

        /**
         * @param queue the queue to set
         */
        public void setQueue(Integer queue)
        {
            this.queue = queue;
        }
    }

    @XmlRootElement(name = "auth")
    @XmlAccessorType(XmlAccessType.NONE)
    public static class Authentication
    {
        @XmlElement(name = "api_id")
        private String api_id;
        @XmlElement(name = "user")
        private String user;
        @XmlElement(name = "password")
        private String password;

        /**
         * @return the api_id
         */
        public String getApi_id()
        {
            return api_id;
        }

        /**
         * @param api_id the api_id to set
         */
        public void setApi_id(String api_id)
        {
            this.api_id = api_id;
        }

        /**
         * @return the user
         */
        public String getUser()
        {
            return user;
        }

        /**
         * @param user the user to set
         */
        public void setUser(String user)
        {
            this.user = user;
        }

        /**
         * @return the password
         */
        public String getPassword()
        {
            return password;
        }

        /**
         * @param password the password to set
         */
        public void setPassword(String password)
        {
            this.password = password;
        }
    }

    @XmlRootElement(name = "response")
    @XmlAccessorType(XmlAccessType.NONE)
    public static class Response
    {
        @XmlElement(name = "fault")
        private String fault;
        @XmlElement(name = "sequence_no")
        private String password;

        /**
         * @return the fault
         */
        public String getFault()
        {
            return fault;
        }

        /**
         * @param fault the fault to set
         */
        public void setFault(String fault)
        {
            this.fault = fault;
        }

        /**
         * @return the password
         */
        public String getPassword()
        {
            return password;
        }

        /**
         * @param password the password to set
         */
        public void setPassword(String password)
        {
            this.password = password;
        }
    }

    @XmlRootElement(name = "sendMsgResp")
    @XmlAccessorType(XmlAccessType.NONE)
    public static class SendMessageResponse extends Response
    {
        @XmlElement(name = "apiMsgId")
        private String apiMsgId;

        /**
         * @return the apiMsgId
         */
        public String getApiMsgId()
        {
            return apiMsgId;
        }

        /**
         * @param apiMsgId the apiMsgId to set
         */
        public void setApiMsgId(String apiMsgId)
        {
            this.apiMsgId = apiMsgId;
        }
    }

    @XmlRootElement(name = "authResp")
    @XmlAccessorType(XmlAccessType.NONE)
    public static class AuthenticationResponse extends Response
    {
        @XmlElement(name = "session_id")
        private String session_id;

        /**
         * @return the session_id
         */
        public String getSession_id()
        {
            return session_id;
        }

        /**
         * @param session_id the session_id to set
         */
        public void setSession_id(String session_id)
        {
            this.session_id = session_id;
        }
    }

    @XmlRootElement(name = "xmlErrorResp")
    @XmlAccessorType(XmlAccessType.NONE)
    public static class XMLErrorResponse extends Response
    {
    }
}
