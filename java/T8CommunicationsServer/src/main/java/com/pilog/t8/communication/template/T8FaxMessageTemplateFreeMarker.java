package com.pilog.t8.communication.template;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8DefaultMessageTemplate;
import com.pilog.t8.communication.message.T8FaxMessage;
import com.pilog.t8.communication.util.FreeMarkerGenerator;
import com.pilog.t8.definition.communication.message.template.T8FaxMessageTemplateFreeMarkerDefinition;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8EmailMessageTemplateAttachementDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;

/**
 * @author Hennie Brink
 */
public class T8FaxMessageTemplateFreeMarker extends T8DefaultMessageTemplate
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8EmailMessageTemplateFreeMarker.class.getName());
    private final T8FaxMessageTemplateFreeMarkerDefinition definition;
    private Map<String, Object> templateParameters = new HashMap<>();

    public T8FaxMessageTemplateFreeMarker(T8Context context, T8FaxMessageTemplateFreeMarkerDefinition definition, Map<String, Object> inputParameters)
    {
        super(context, definition, inputParameters);
        this.definition = definition;
    }

    public String generateMessageNotes(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        Map<String, Object> templateParamDetails;

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);
        //add additional detail provider details if provided
        if(additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        return FreeMarkerGenerator.getFreeMarkerConversion(definition.getPublicIdentifier()+"$P_SUBJECT", templateParamDetails, manager, definition.getNotes());
    }

    public String generateMessageContent(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        Map<String, Object> templateParamDetails;

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);
        //add additional detail provider details if provided
        if(additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        return FreeMarkerGenerator.getFreeMarkerConversion(definition.getPublicIdentifier()+"$P_BODY", templateParamDetails, manager, definition.getTemplate());
    }

    public List<T8CommunicationMessageAttachment> getAttachments(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails)
    {
        ArrayList<T8CommunicationMessageAttachment> returnList = new ArrayList<>(0);
        if(definition.getAttachments() == null)
            return returnList;
        Map<String, Object> templateParamDetails;

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);
        //add additional detail provider details if provided
        if(additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        for (T8EmailMessageTemplateAttachementDefinition t8EmailMessageTemplateAttachementDefinition : definition.getAttachments())
        {
            T8CommunicationMessageAttachment attachment = t8EmailMessageTemplateAttachementDefinition.getInstance();
            try
            {
                attachment.generateAttachment(context, templateParamDetails);
                returnList.add(attachment);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to attach file " + t8EmailMessageTemplateAttachementDefinition.getPublicIdentifier(), ex);
            }
        }
        return returnList;
    }

    @Override
    public T8CommunicationMessage generateMessage(T8Context senderContext, T8CommunicationDetails communicationDetails, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        T8SessionContext sessionContext;
        T8FaxMessage faxMessage;
        String subject;
        String messageContent;

        // Generate the message subject and content.
        subject = generateMessageNotes(communicationDetails, manager, additionDetails);
        messageContent = generateMessageContent(communicationDetails, manager, additionDetails);

        // Create the new message.
        faxMessage = new T8FaxMessage(communicationDetails.getRecipientDisplayName(), communicationDetails.getMessagePriority(), communicationDetails.getRecipientAddress(), communicationDetails.getRecipientDisplayName(), subject, messageContent);
        faxMessage.setIdentifier(definition.getIdentifier());
        faxMessage.setRecipientDefinitionIdentifier(communicationDetails.getRecipientDefinitionIdentifier());
        faxMessage.setRecipientRecordID(communicationDetails.getRecipientRecordID());

        // Add sender information.
        sessionContext = senderContext.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            faxMessage.setSenderIdentifier(sessionContext.getSystemAgentIdentifier());
            faxMessage.setSenderInstanceIdentifier(sessionContext.getSystemAgentInstanceIdentifier());
        }
        else
        {
            faxMessage.setSenderIdentifier(sessionContext.getUserIdentifier());
            faxMessage.setSenderInstanceIdentifier(null);
        }

        // Add the attachments.
        if (supportsAttachment())
        {
           List<T8CommunicationMessageAttachment> attachments = getAttachments(communicationDetails, manager, additionDetails);
           if (attachments != null)
           {
               for (T8CommunicationMessageAttachment t8CommunicationMessageAttachment : attachments)
               {
                   faxMessage.addAttachment(t8CommunicationMessageAttachment);
               }
           }
        }

        return faxMessage;
    }

    public boolean supportsAttachment()
    {
        return true;
    }

    @Override
    public void initializeGenerator(T8DataTransaction tx) throws Exception
    {
        try
        {
            T8ServerContextScriptDefinition scriptDefinition;

            scriptDefinition = definition.getTemplateDataScript();
            if (scriptDefinition != null)
            {
                T8Script script;

                // Run the script.
                script = scriptDefinition.getNewScriptInstance(tx.getContext());
                templateParameters = script.executeScript(getInputParameters());
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to execute the script to generate the subject field template parameters", ex);
        }
    }
}
