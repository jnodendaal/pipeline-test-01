/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.service.clickatell;

import java.util.HashMap;

/**
 *
 * @author hennie.brink
 */
public class ClickatellCodes
{

    /**
     * HashMap<String,String> containing all error codes for Clickatell.
     * KeyValue pairs consisting of Key: ErrorCode - Value: Description.
     */
    public static HashMap<String,String> ErrorCodes;

    /**
     * HashMap<String,String> containing all message codes for Clickatell.
     * KeyValue pairs consisting of Key: messageCode - Value: Description.
     */
    public static HashMap<String,String> MessageStatus;

    static
    {
        //assign ErrorCodes
        ErrorCodes = new HashMap<>();
        ErrorCodes.put("001", "Authentication Failed");
        ErrorCodes.put("002", "Unknown username or password");
        ErrorCodes.put("003", "Session ID expired");
        ErrorCodes.put("004", "Account frozen");
        ErrorCodes.put("005", "Missing session ID");
        ErrorCodes.put("007", "IP Lockdown violation");
        ErrorCodes.put("101", "Invalid or missing parameters");
        ErrorCodes.put("102", "Invalid user data header");
        ErrorCodes.put("103", "Unknown API message ID");
        ErrorCodes.put("104", "Unknown client message ID");
        ErrorCodes.put("105", "Invalid destination address");
        ErrorCodes.put("106", "Invalid source address");
        ErrorCodes.put("107", "Empty message");
        ErrorCodes.put("108", "Invalid or missing API ID");
        ErrorCodes.put("109", "Missing message ID");
        ErrorCodes.put("110", "Error with email message");
        ErrorCodes.put("111", "Invalid protocol");
        ErrorCodes.put("112", "Invalid message type");
        ErrorCodes.put("113", "Maximum message parts exceeded");
        ErrorCodes.put("114", "Cannot route message");
        ErrorCodes.put("115", "Message expired");
        ErrorCodes.put("116", "Invalid Unicode data");
        ErrorCodes.put("120", "Invalid delivery time");
        ErrorCodes.put("121", "Destination mobile number blocked");
        ErrorCodes.put("122", "Destination mobile opted out");
        ErrorCodes.put("123", "Invalid Sender ID");
        ErrorCodes.put("128", "Number delisted");
        ErrorCodes.put("130", "Maximum MT limit exceeded until <UNIX TIME STAMP>");
        ErrorCodes.put("201", "Invalid batch ID");
        ErrorCodes.put("202", "No batch template");
        ErrorCodes.put("301", "No credit left");
        ErrorCodes.put("302", "Max allowed credit");
        ErrorCodes.put("901", "Internal Error, retry");


        //assign Message Status Codes
        MessageStatus = new HashMap<>();
        MessageStatus.put("001", "Message unknown");
        MessageStatus.put("002", "Message queued");
        MessageStatus.put("003", "Delivered to gateway");
        MessageStatus.put("004", "Received by recipient");
        MessageStatus.put("005", "Error with message");
        MessageStatus.put("006", "User cancelled message delivery");
        MessageStatus.put("007", "Error delivering message");
        MessageStatus.put("008", "OK. Message received by gateway. ");
        MessageStatus.put("009", "Routing error");
        MessageStatus.put("010", "Message expired");
        MessageStatus.put("011", "Message queued for later delivery");
        MessageStatus.put("012", "Out of credit");
        MessageStatus.put("014", "Maximum MT limit exceeded");

    }
}
