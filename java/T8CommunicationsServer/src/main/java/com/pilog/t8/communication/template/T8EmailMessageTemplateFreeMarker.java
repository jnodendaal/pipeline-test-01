package com.pilog.t8.communication.template;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8DefaultMessageTemplate;
import com.pilog.t8.communication.message.T8EmailMessage;
import com.pilog.t8.communication.util.FreeMarkerGenerator;
import com.pilog.t8.definition.communication.message.template.T8EmailTemplateDefinition;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8EmailMessageTemplateAttachementDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;

/**
 * Utilizes the Free Marker Templating Engine to generate email templates
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailMessageTemplateFreeMarker extends T8DefaultMessageTemplate
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8EmailMessageTemplateFreeMarker.class);

    private final T8EmailTemplateDefinition definition;
    private Map<String, Object> templateParameters = new HashMap<>();

    public T8EmailMessageTemplateFreeMarker(T8Context context, T8EmailTemplateDefinition definition, Map<String, Object> inputParameters)
    {
        super(context, definition, inputParameters);
        this.definition = definition;
    }

    public String generateMessageSubject(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        Map<String, Object> templateParamDetails;

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);
        //add additional detail provider details if provided
        if (additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        return FreeMarkerGenerator.getFreeMarkerConversion(definition.getPublicIdentifier()+"$P_SUBJECT", templateParamDetails, manager, definition.getSubject());
    }

    public String generateMessageContent(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        Map<String, Object> templateParamDetails;

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);
        //add additional detail provider details if provided
        if(additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        return FreeMarkerGenerator.getFreeMarkerConversion(definition.getPublicIdentifier()+"$P_BODY", templateParamDetails, manager, definition.getTemplate());
    }

    public List<T8CommunicationMessageAttachment> getAttachments(T8CommunicationDetails communicationDetail, T8CommunicationManager manager, Map<String, Object> additionDetails)
    {
        if (definition.getAttachments() == null) return new ArrayList<>();
        ArrayList<T8CommunicationMessageAttachment> returnList;
        Map<String, Object> templateParamDetails;

        returnList = new ArrayList<>();

        templateParameters.put(T8CommunicationMessageDefinition.P_COMMUNICATION_DETAILS_OBJECT, communicationDetail);
        templateParamDetails = new HashMap<>(templateParameters);

        //add additional detail provider details if provided
        if (additionDetails != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), additionDetails, false));
        if (this.getInputParameters() != null) templateParamDetails.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), this.getInputParameters(), false));
        for (T8EmailMessageTemplateAttachementDefinition t8EmailMessageTemplateAttachementDefinition : definition.getAttachments())
        {
            T8CommunicationMessageAttachment attachment = t8EmailMessageTemplateAttachementDefinition.getInstance();
            try
            {
                attachment.generateAttachment(context, templateParamDetails);
                returnList.add(attachment);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to attach file " + t8EmailMessageTemplateAttachementDefinition.getPublicIdentifier(), ex);
            }
        }
        return returnList;
    }

    @Override
    public T8CommunicationMessage generateMessage(T8Context senderContext, T8CommunicationDetails communicationDetails, T8CommunicationManager manager, Map<String, Object> additionDetails) throws Exception
    {
        T8SessionContext sessionContext;
        T8EmailMessage emailMessage;
        String subject;
        String messageContent;

        // Generate the message subject and content.
        subject = generateMessageSubject(communicationDetails, manager, additionDetails);
        messageContent = generateMessageContent(communicationDetails, manager, additionDetails);

        // Create the new message.
        emailMessage = new T8EmailMessage(communicationDetails.getRecipientDisplayName(), communicationDetails.getMessagePriority(), communicationDetails.getRecipientAddress(), subject, messageContent);
        emailMessage.setIdentifier(definition.getIdentifier());
        emailMessage.setRecipientDefinitionIdentifier(communicationDetails.getRecipientDefinitionIdentifier());
        emailMessage.setRecipientRecordID(communicationDetails.getRecipientRecordID());
        emailMessage.setBccEnabled(definition.isBccEnabled());
        emailMessage.setBccGenericRecipient(definition.getBccGenericRecipient());

        // Add sender information.
        sessionContext = senderContext.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            emailMessage.setSenderIdentifier(sessionContext.getSystemAgentIdentifier());
            emailMessage.setSenderInstanceIdentifier(sessionContext.getSystemAgentInstanceIdentifier());
        }
        else
        {
            emailMessage.setSenderIdentifier(sessionContext.getUserIdentifier());
            emailMessage.setSenderInstanceIdentifier(null);
        }

        // Add the attachments.
        if (supportsAttachment())
        {
           List<T8CommunicationMessageAttachment> attachments = getAttachments(communicationDetails, manager, additionDetails);
           if (attachments != null)
           {
               for (T8CommunicationMessageAttachment t8CommunicationMessageAttachment : attachments)
               {
                   emailMessage.addAttachment(t8CommunicationMessageAttachment);
               }
           }
        }

        // Return the completed email message ready to be sent.
        return emailMessage;
    }

    public boolean supportsAttachment()
    {
        return true;
    }

    @Override
    public void initializeGenerator(T8DataTransaction tx) throws Exception
    {
        try
        {
            T8ServerContextScriptDefinition scriptDefinition;

            scriptDefinition = definition.getTemplateDataScript();
            if (scriptDefinition != null)
            {
                T8Script script;

                // Run the script.
                script = scriptDefinition.getNewScriptInstance(tx.getContext());
                templateParameters = script.executeScript(getInputParameters());
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to execute the script to generate the subject field template parameters", ex);
        }
    }
}
