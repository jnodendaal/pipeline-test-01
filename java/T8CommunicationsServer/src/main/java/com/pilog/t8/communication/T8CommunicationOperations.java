package com.pilog.t8.communication;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.communication.T8CommunicationManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationOperations
{
    public static class SendCommunicationOperation extends T8DefaultServerOperation
    {
        public SendCommunicationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8CommunicationManager communicationManager;
            Map<String, Object> communicationParameters;
            String communicationIdentifier;

            // Get the operation parameters.
            communicationIdentifier = (String)operationParameters.get(PARAMETER_COMMUNICATION_IDENTIFIER);
            communicationParameters = (Map<String, Object>)operationParameters.get(PARAMETER_COMMUNICATION_PARAMETERS);

            // Send the communication.
            communicationManager = serverContext.getCommunicationManager();
            communicationManager.sendCommunication(context, communicationIdentifier, communicationParameters);
            return null;
        }
    }

    public static class GetQueyedMessageCountOperation extends T8DefaultServerOperation
    {
        public GetQueyedMessageCountOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8CommunicationManager communicationManager;

            // Send the communication.
            communicationManager = serverContext.getCommunicationManager();
            return HashMaps.newHashMap(PARAMETER_MESSAGE_COUNT, communicationManager.getQueuedMessageCount(null));
        }
    }

    public static class CancelCommunicationMessageOperation extends T8DefaultServerOperation
    {
        public CancelCommunicationMessageOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8CommunicationManager communicationManager;
            String messageIID;

            // Send the communication.
            communicationManager = serverContext.getCommunicationManager();
            messageIID = (String) operationParameters.get(PARAMETER_MESSAGE_INSTANCE_IDENTIFIER);
            communicationManager.cancelMessage(context, messageIID);
            return null;
        }
    }
}
