package com.pilog.t8.communication.service;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.T8DefaultCommunicationResult;
import com.pilog.t8.communication.message.T8EmailMessage;
import com.pilog.t8.definition.communication.service.T8EmailServiceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.resolver.DataSourceClassPathResolver;

/**
 * Service to send email communications.
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailService extends T8DefaultCommunicationService implements T8CommunicationService
{
    private final T8EmailServiceDefinition serviceDefinition;

    public T8EmailService(T8Context context, T8EmailServiceDefinition serviceDefinition, T8CommunicationManager manager)
    {
        super(context, serviceDefinition, manager);
        this.serviceDefinition = serviceDefinition;
    }

    @Override
    protected T8DefaultCommunicationResult sendImmediately(T8CommunicationMessage message)
    {
        T8EmailMessage emailMessage;

        emailMessage = (T8EmailMessage)message;
        emailMessage.setServiceIdentifier(identifier);

        try
        {
            // create the email message
            ImageHtmlEmail email = new ImageHtmlEmail();
            if (!Strings.isNullOrEmpty(serviceDefinition.getUserName()) && !Strings.isNullOrEmpty(serviceDefinition.getPassword()))
            {
                email.setAuthentication(serviceDefinition.getUserName(), serviceDefinition.getPassword());
            }

            if(serviceDefinition.getSMTPPort() != null) email.setSmtpPort(serviceDefinition.getSMTPPort());
            if(serviceDefinition.getSSLSMTPPort() != null) email.setSslSmtpPort(serviceDefinition.getSSLSMTPPort().toString());
            email.setSSLCheckServerIdentity(serviceDefinition.isCheckSSLServerIdentity());
            email.setSSLOnConnect(serviceDefinition.isSSLConnection());
            email.setStartTLSEnabled(serviceDefinition.isStartTLSEnabled());
            email.setStartTLSRequired(serviceDefinition.isStartTLSRequired());
            email.setHostName(serviceDefinition.getHostname());
            if (emailMessage.isBccEnabled()) {
                email.addTo(emailMessage.getBccGenericRecipient());
                email.addBcc(emailMessage.getEmailAddress());
            } else email.addTo(emailMessage.getEmailAddress());
            email.setFrom(serviceDefinition.getFromMailAddress());
            email.setSubject(emailMessage.getSubject());

            // set the html message
            email.setHtmlMsg(emailMessage.getMessageContent());

            // set the alternative message
            email.setTextMsg("Your email client does not support HTML messages.");

            // add attachments
            if (emailMessage.getAttachments() != null)
            {
                for (T8CommunicationMessageAttachment attachment : emailMessage.getAttachments())
                {
                    if(attachment.isEmbedded()) email.embed(attachment.getAttachment(), attachment.getName(), attachment.getContentID());
                    else email.attach(attachment.getAttachment(), attachment.getName(), attachment.getDescription());
                }
            }

            // A data source resolver so that embedded images will work correctly and not throw NPE's.
            email.setDataSourceResolver(new DataSourceClassPathResolver("com.pilog.t8"));

            // send the email
            String messageId = email.send();
            T8DefaultCommunicationResult result = new T8DefaultCommunicationResult(true, T8DefaultCommunicationResult.Result.Sent, emailMessage);
            result.setMessageId(messageId);
            return result;
        }
        catch (Exception ex)
        {
            T8DefaultCommunicationResult result;

            result = new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.Error, "Failed to send email message to: " + emailMessage.getEmailAddress(), ex, emailMessage);
            return result;
        }
    }

    @Override
    public String getIdentifier()
    {
        return serviceDefinition.getIdentifier();
    }
}
