package com.pilog.t8.communication.service;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.T8DefaultCommunicationResult;
import com.pilog.t8.communication.message.T8FaxMessage;
import com.pilog.t8.definition.communication.service.T8RightFaxServiceDefinition;
import com.pilog.t8.security.T8Context;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.resolver.DataSourceClassPathResolver;

/**
 * @author Hennie Brink
 */
public class T8RightFaxService extends T8DefaultCommunicationService implements T8CommunicationService
{
    private final T8RightFaxServiceDefinition serviceDefinition;

    public T8RightFaxService(T8Context context, T8RightFaxServiceDefinition serviceDefinition, T8CommunicationManager manager)
    {
        super(context, serviceDefinition, manager);
        this.serviceDefinition = serviceDefinition;
    }

    @Override
    protected T8DefaultCommunicationResult sendImmediately(T8CommunicationMessage message)
    {
        T8FaxMessage emailMessage;

        emailMessage = (T8FaxMessage) message;
        emailMessage.setServiceIdentifier(identifier);

        try
        {
            // create the email message
            ImageHtmlEmail email = new ImageHtmlEmail();
            if (serviceDefinition.getMailServerDomainAdress()!= null && serviceDefinition.getPassword() != null
                && !"".equals(serviceDefinition.getUserName()) && !"".equals(serviceDefinition.getPassword()))
            {
                email.setAuthentication(serviceDefinition.getUserName(), serviceDefinition.getPassword());
            }

            if(serviceDefinition.getSMTPPort() != null) email.setSmtpPort(serviceDefinition.getSMTPPort());
            if(serviceDefinition.getSSLSMTPPort() != null) email.setSslSmtpPort(serviceDefinition.getSSLSMTPPort().toString());
            email.setSSLCheckServerIdentity(serviceDefinition.isCheckSSLServerIdentity());
            email.setSSLOnConnect(serviceDefinition.isSSLConnection());
            email.setStartTLSEnabled(serviceDefinition.isStartTLSEnabled());
            email.setStartTLSRequired(serviceDefinition.isStartTLSRequired());
            email.setHostName(serviceDefinition.getHostname());
            email.setFrom(serviceDefinition.getFromMailAddress());
            email.setSubject(emailMessage.getSubject());

            // set the html message
            email.setHtmlMsg(emailMessage.getMessageContent());

            // set the alternative message
            email.setTextMsg("Your email client does not support HTML messages");

            // add attachments
            if (emailMessage.getAttachments() != null)
            {
                for (T8CommunicationMessageAttachment attachment : emailMessage.getAttachments())
                {
                    if (attachment.isEmbedded()) email.embed(attachment.getAttachment(), attachment.getName(), attachment.getContentID());
                    else email.attach(attachment.getAttachment(), attachment.getName(), attachment.getDescription());
                }
            }

            email.addTo(serviceDefinition.getMailServerDomainAdress(), generateFaxRecipientAdressDetails(emailMessage));

            //Just a data source resolver so that embeded images will work correctly and not throw NPE's
            email.setDataSourceResolver(new DataSourceClassPathResolver("com.pilog.t8"));

            // send the email
            String messageId = email.send();
            T8DefaultCommunicationResult result = new T8DefaultCommunicationResult(true, T8DefaultCommunicationResult.Result.Sent, emailMessage);
            result.setMessageId(messageId);
            return result;
        }
        catch (Exception ex)
        {
            T8DefaultCommunicationResult result;

            result = new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.Error, "Failed to send fax message to: " + emailMessage.getFaxNumber(), ex, emailMessage);
            return result;
        }
    }

    @Override
    public String getIdentifier()
    {
        return serviceDefinition.getIdentifier();
    }

    private String generateFaxRecipientAdressDetails(T8FaxMessage emailMessage)
    {
        StringBuilder faxMessageAdress;

        // Using RightFax Addressing Scheme for SMTP/POP3 mail.
        faxMessageAdress = new StringBuilder();
        faxMessageAdress.append("/fax=");
        faxMessageAdress.append(emailMessage.getFaxNumber());
        faxMessageAdress.append("/name=");
        faxMessageAdress.append(emailMessage.getFaxRecipientName());
        if (emailMessage.getFaxRecipientCompanyName() != null)
        {
            faxMessageAdress.append("/com=");
            faxMessageAdress.append(emailMessage.getFaxRecipientCompanyName());
        }

        return faxMessageAdress.toString();
    }
}
