package com.pilog.t8.communication.detail;

import com.pilog.t8.utilities.strings.Strings;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8SMSCommunicationDetail extends T8DefaultCommunicationDetails
{
    private String cellNumber;

    public T8SMSCommunicationDetail(String cellNumber, int priority, String recipientName)
    {
        super(priority, recipientName);
        this.cellNumber = cellNumber;
    }

    public T8SMSCommunicationDetail(String cellNumber, String recipientName)
    {
        super(recipientName);
        this.cellNumber = cellNumber;
    }

    public T8SMSCommunicationDetail(String cellNumber, int priority)
    {
        super(priority);
        this.cellNumber = cellNumber;
    }

    public T8SMSCommunicationDetail(String cellNumber)
    {
        this.cellNumber = cellNumber;
    }
    /**
     * @return the cellNumber
     */
    public String getCellNumber()
    {
        return cellNumber;
    }

    /**
     * @param cellNumber the cellNumber to set
     */
    public void setCellNumber(String cellNumber)
    {
        this.cellNumber = cellNumber;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 53 * hash + (this.cellNumber != null
                ? this.cellNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final T8SMSCommunicationDetail other = (T8SMSCommunicationDetail) obj;
        if ((this.cellNumber == null) ? (other.cellNumber != null)
                : !this.cellNumber.equals(other.cellNumber))
        {
            return false;
        }
        return true;
    }

    @Override
    public String getRecipientAddress()
    {
        return getCellNumber();
    }

    @Override
    public void verifyRecipientAddress() throws Exception
    {
        if(Strings.isNullOrEmpty(cellNumber)) throw new IllegalArgumentException("The mobile number registered for " + getRecipientDisplayName() + " is empty, ensure that mobile number for the recipient is provided in order to send a communication.");

        //if(!Pattern.matches("\\+*[0-9]+", cellNumber)) throw new IllegalArgumentException("The mobile number registered for " + getRecipientDisplayName() + " is not a valid mobile number, please correct the mobile number for this recipient in order to recieve sms messages.");
    }
}
