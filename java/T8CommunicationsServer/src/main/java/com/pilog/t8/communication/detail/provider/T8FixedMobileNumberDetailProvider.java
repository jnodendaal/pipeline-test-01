/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.detail.provider;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.communication.detail.T8SMSCommunicationDetail;
import com.pilog.t8.communication.util.MobileNumberUtils;
import com.pilog.t8.definition.communication.detail.provider.T8FixedMobileNumberDetailProviderDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * The T8 UserDetail Providers returns user details for users registered in the T8 System.
 * <p/>
 * @author hennie.brink@pilog.co.za
 */
public class T8FixedMobileNumberDetailProvider implements
        T8CommunicationDetailsProvider
{
    private static final T8Logger logger = com.pilog.t8.T8Log.getLogger(T8FixedMobileNumberDetailProvider.class.getName());
    private T8FixedMobileNumberDetailProviderDefinition definition;
    private T8ServerContext serverContext;
    private T8SessionContext sessionContext;
    private Map<String, Object> providerParameters;

    public T8FixedMobileNumberDetailProvider(
            Map<String, Object> providerParameters,
            T8FixedMobileNumberDetailProviderDefinition definition,
            T8ServerContext serverContext,
            T8SessionContext sessionContext)
    {
        this.definition = definition;
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.providerParameters = providerParameters;
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        return definition.getSupportedCommunicationTypes();
    }

    @Override
    public Set<T8CommunicationDetails> getCommunicationDetails(T8DataTransaction tx, T8CommunicationType communicationType, Map<String, String> messageDetailsMapping)
    {
        Set<T8CommunicationDetails> communicationDetails;
        List<String> alternateMobileNumbers;
        String alternateMobileNumber;
        String parameterIdentifier = definition.getPublicIdentifier() + T8FixedMobileNumberDetailProviderDefinition.P_ALTERNATE_MOBILE_NUMBER_LIST;
        try
        {
            alternateMobileNumbers = (List<String>) providerParameters.get(parameterIdentifier);
        }
        catch (ClassCastException ce)
        {
            throw new RuntimeException("The parameter " + parameterIdentifier + " was of type " + providerParameters.get(parameterIdentifier).getClass().getCanonicalName() + ", List<String> was expected.");
        }
        parameterIdentifier = definition.getPublicIdentifier() + T8FixedMobileNumberDetailProviderDefinition.P_ALTERNATE_MOBILE_NUMBER;
        try
        {
            alternateMobileNumber = (String) providerParameters.get(parameterIdentifier);
        }
        catch (ClassCastException ce)
        {
            throw new RuntimeException("The parameter " + parameterIdentifier + " was of type " + providerParameters.get(parameterIdentifier).getClass().getCanonicalName() + ", List<String> was expected.");
        }
        communicationDetails = new LinkedHashSet<>();
        if (alternateMobileNumbers != null)
        {
            for (String number : alternateMobileNumbers)
            {
                try
                {
                    T8SMSCommunicationDetail t8SMSCommunicationDetail = new T8SMSCommunicationDetail(MobileNumberUtils.checkValid(number), number);
                    HashMap<String,Object> additionalDetails = HashMaps.newHashMap(definition.getIdentifier() + "MOBILE_NUMBER", number);
                    t8SMSCommunicationDetail.setAdditionalDetails(T8IdentifierUtilities.mapParameters(additionalDetails, messageDetailsMapping));
                    communicationDetails.add(t8SMSCommunicationDetail);
                }
                catch (Exception ex)
                {
                    logger.log("Failed to load communication details for " + number, ex);
                }
            }
        }
        if(alternateMobileNumber != null)
        {
            try
            {
                T8SMSCommunicationDetail t8SMSCommunicationDetail = new T8SMSCommunicationDetail(MobileNumberUtils.checkValid(alternateMobileNumber), alternateMobileNumber);
                HashMap<String,Object> additionalDetails = HashMaps.newHashMap(definition.getIdentifier() + "MOBILE_NUMBER", alternateMobileNumber);
                t8SMSCommunicationDetail.setAdditionalDetails(T8IdentifierUtilities.mapParameters(additionalDetails, messageDetailsMapping));
                communicationDetails.add(t8SMSCommunicationDetail);
            }
            catch (Exception ex)
            {
                logger.log("Failed to load communication details for " + alternateMobileNumber, ex);
            }
        }
        return communicationDetails;
    }
}
