package com.pilog.t8.communication.detail;

import com.pilog.t8.utilities.strings.Strings;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailCommunicationDetail extends T8DefaultCommunicationDetails
{
    private String eMailAddress;

    public T8EmailCommunicationDetail(String eMailAddress, int priority, String recipientName)
    {
        super(priority, recipientName);
        this.eMailAddress = eMailAddress;
    }

    public T8EmailCommunicationDetail(String eMailAddress, String recipientName)
    {
        super(recipientName);
        this.eMailAddress = eMailAddress;
    }

    public T8EmailCommunicationDetail(String eMailAddress, int priority)
    {
        super(priority);
        this.eMailAddress = eMailAddress;
    }

    public T8EmailCommunicationDetail(String eMailAddress)
    {
        this.eMailAddress = eMailAddress;
    }

    /**
     * @return the eMailAddress
     */
    public String getEMailAddress()
    {
        return eMailAddress;
    }

    /**
     * @param eMailAddress the eMailAddress to set
     */
    public void setEMailAddress(String eMailAddress)
    {
        this.eMailAddress = eMailAddress;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 53 * hash + (this.eMailAddress != null
                ? this.eMailAddress.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final T8EmailCommunicationDetail other = (T8EmailCommunicationDetail) obj;
        if ((this.eMailAddress == null) ? (other.eMailAddress != null)
                : !this.eMailAddress.equals(other.eMailAddress))
        {
            return false;
        }
        return true;
    }

    @Override
    public String getRecipientAddress()
    {
        return getEMailAddress();
    }

    @Override
    public void verifyRecipientAddress() throws Exception
    {
        if(Strings.isNullOrEmpty(eMailAddress)) throw new IllegalArgumentException("The email address registered for " + getRecipientDisplayName() + " is empty, ensure that email address for the recipient is provided in order to send a communication.");

        //if(!Pattern.matches("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", eMailAddress)) throw new IllegalArgumentException("The email address registered for " + getRecipientDisplayName() + " is not a valid email address, please correct the email address for this recipient in order to recieve email messages.");
    }
}
