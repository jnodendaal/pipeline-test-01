package com.pilog.t8.communication;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationMessage.MessageHistoryEvent;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.communication.T8Communication.CommunicationHistoryEvent;
import com.pilog.t8.data.T8HistoryParameterPersistenceHandler;
import com.pilog.t8.definition.communication.T8CommunicationManagerResource;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.state.T8ParameterPeristenceHandler;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.communication.T8CommunicationManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationPersistenceHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private final T8DataManager dataManager;
    private final T8ParameterPeristenceHandler parameterPersistenceHandler;
    private boolean persistenceEnabled; // Flag that can be set to false to disable persistence of communication states.

    public T8CommunicationPersistenceHandler(T8Context internalContext)
    {
        this.serverContext = internalContext.getServerContext();
        this.internalContext = internalContext;
        this.definitionManager = serverContext.getDefinitionManager();
        this.dataManager = serverContext.getDataManager();
        this.persistenceEnabled = true;
        this.parameterPersistenceHandler = new T8ParameterPeristenceHandler(MESSAGE_STATE_PAR_DE_IDENTIFIER);
    }

    public void setEnabled(boolean enabled)
    {
        persistenceEnabled = enabled;
    }

    public void saveState(T8CommunicationMessageState state) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataSession session;
            T8DataTransaction xtx;
            T8DataTransaction tx;

            // Get the transaction to use (suspend any external transaction).
            session = dataManager.getCurrentSession();
            xtx = session.suspend();
            tx = session.beginTransaction();

            try
            {
                T8DataEntity entity;

                // Create a new state entity.
                entity = createStateEntity(tx, state);

                // Save the entity.
                if (!tx.update(entity))
                {
                    // Insert the message entity.
                    tx.insert(entity);

                    // Insert the state parameters.
                    parameterPersistenceHandler.insertParameters(tx, HashMaps.createSingular(MESSAGE_STATE_PAR_DE_IDENTIFIER + "$MESSAGE_IID", state.getMessageInstanceIdentifier()), null, state.getParameters());
                }

                // Commit the transaction.
                tx.commit();
            }
            catch (Exception e)
            {
                tx.rollback();
                throw e;
            }
            finally
            {
                // If an external session was present, resume it.
                if (xtx != null) session.resume(xtx);
            }
        }
    }

    public boolean deleteState(String messageIid) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataTransaction tx;
            int stateDeletions;

            // Get the transaction to use.
            tx = dataManager.getCurrentSession().instantTransaction();

            // Delete message state parameters.
            tx.delete(MESSAGE_STATE_PAR_DE_IDENTIFIER, new T8DataFilter(MESSAGE_STATE_PAR_DE_IDENTIFIER, HashMaps.newHashMap(MESSAGE_STATE_PAR_DE_IDENTIFIER + "$MESSAGE_IID", messageIid)));

            // Delete message state.
            stateDeletions = tx.delete(MESSAGE_STATE_DE_IDENTIFIER, new T8DataFilter(MESSAGE_STATE_DE_IDENTIFIER, HashMaps.newHashMap(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_IID", messageIid)));

            // Return indicating wether or not the state was found for deletion.
            return stateDeletions > 0;
        }
        else return false;
    }

    public void setMessageFailure(T8CommunicationMessage message, String failureMessage) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataTransaction tx;
            T8DataEntity entity;

            // Get the transaction to use.
            tx = dataManager.getCurrentSession().instantTransaction();

            // Create a new state entity from the message state.
            entity = createStateEntity(tx, message.getState());

            // Update the message state.
            tx.update(entity);
        }
    }

    public List<T8CommunicationMessageState> retrieveMessageStates(String serviceId, int maximumFailureCount, int pageSize) throws Exception
    {
        List<T8DataEntity> messageStateEntities;
        List<T8CommunicationMessageState> messageStates;
        T8DataTransaction tx;
        T8DataFilter filter;

        // Retrieve the message state entities.
        filter = new T8DataFilter(MESSAGE_STATE_DE_IDENTIFIER);
        filter.addFilterCriterion(MESSAGE_STATE_DE_IDENTIFIER + "$SERVICE_ID", T8DataFilterCriterion.DataFilterOperator.EQUAL, serviceId);
        filter.addFilterCriterion(MESSAGE_STATE_DE_IDENTIFIER + "$FAILURE_COUNT", T8DataFilterCriterion.DataFilterOperator.LESS_THAN, maximumFailureCount);
        filter.addFieldOrdering(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_PRIORITY", T8DataFilter.OrderMethod.DESCENDING);
        filter.addFieldOrdering(MESSAGE_STATE_DE_IDENTIFIER + "$TIME_QUEUED", T8DataFilter.OrderMethod.ASCENDING);
        tx = dataManager.getCurrentSession().instantTransaction();
        messageStateEntities = tx.select(MESSAGE_STATE_DE_IDENTIFIER, filter, 0, pageSize > -1 ? pageSize : -1);

        // Create the complete states from each entity retrieved.
        messageStates = new ArrayList<>(messageStateEntities.size());
        for (T8DataEntity messageStateEntity : messageStateEntities)
        {
            T8CommunicationMessageState messageState;

            // Construct the new message state.
            messageState = new T8CommunicationMessageState((String)messageStateEntity.getFieldValue("$MESSAGE_ID"), (String)messageStateEntity.getFieldValue("$MESSAGE_IID"));
            messageState.setCommunicationIdentifier((String)messageStateEntity.getFieldValue("$COMMUNICATION_ID"));
            messageState.setCommunicationInstanceIdentifier((String)messageStateEntity.getFieldValue("$COMMUNICATION_IID"));
            messageState.setContent((String)messageStateEntity.getFieldValue("$MESSAGE_CONTENT"));
            messageState.setFailureCount((Integer)messageStateEntity.getFieldValue("$FAILURE_COUNT"));
            messageState.setPriority((Integer)messageStateEntity.getFieldValue("$MESSAGE_PRIORITY"));
            messageState.setRecipientAddress((String)messageStateEntity.getFieldValue("$RECIPIENT_ADDRESS"));
            messageState.setRecipientDisplayName((String)messageStateEntity.getFieldValue("$RECIPIENT_DISPLAY_NAME"));
            messageState.setSenderIdentifier((String)messageStateEntity.getFieldValue("$SENDER_ID"));
            messageState.setSenderInstanceIdentifier((String)messageStateEntity.getFieldValue("$SENDER_IID"));
            messageState.setServiceIdentifier((String)messageStateEntity.getFieldValue("$SERVICE_ID"));
            messageState.setSubject((String)messageStateEntity.getFieldValue("$MESSAGE_SUBJECT"));

            // Add the message state parameters.
            messageState.setParameters(parameterPersistenceHandler.retrieveParameters(tx, HashMaps.createSingular(MESSAGE_STATE_PAR_DE_IDENTIFIER + "$MESSAGE_IID", messageState.getMessageInstanceIdentifier())));

            // Add the message state to the result list.
            messageStates.add(messageState);
        }

        // Return the retrieved message states.
        return messageStates;
    }

    public void logCommunicationEvent(T8Context senderContext, CommunicationHistoryEvent event, T8Communication communication, Map<String, Object> communicationParameters) throws Exception
    {
        T8SessionContext sessionContext;
        T8DataTransaction tx;
        T8DataEntity entity;
        String eventIid;

        // Get the transaction and entity definition.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        sessionContext = senderContext.getSessionContext();

        // Create a new proces history entity.
        eventIid = T8IdentifierUtilities.createNewGUID();
        entity = tx.create(COMMUNICATION_HISTORY_DE_IDENTIFIER, null);
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$EVENT_INSTANCE_IDENTIFIER", eventIid);
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$COMMUNICATION_IDENTIFIER", communication.getIdentifier());
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$COMMUNICATION_INSTANCE_IDENTIFIER", communication.getInstanceIdentifier());
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$INITIATOR_USER_IDENTIFIER", sessionContext != null ? sessionContext.getUserIdentifier() : null);
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$INITIATOR_USER_PROFILE_IDENTIFIER", sessionContext != null ? sessionContext.getUserProfileIdentifier() : null);
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$EVENT", event.toString());
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(COMMUNICATION_HISTORY_DE_IDENTIFIER + "$EVENT_PARAMETERS", createParameterString(communication.getIdentifier(), communicationParameters)); // We always strip the process namespace, all other parameter types will maintain their namespaces.

        // Insert the entity.
        tx.insert(entity);

        // Insert the Communication History parameters.
        T8HistoryParameterPersistenceHandler.insertParameters(tx, T8CommunicationManagerResource.COMMUNICATION_HISTORY_PAR_DE_IDENTIFIER, eventIid, T8IdentifierUtilities.stripNamespace(communicationParameters));
    }

    public void logMessageEvent(MessageHistoryEvent event, T8CommunicationMessage message) throws Exception
    {
        T8DataTransaction tx;
        T8DataEntity entity;
        String eventIid;

        // Get the transaction and entity definition.
        tx = dataManager.getCurrentSession().instantTransaction();

        // Create a new proces history entity.
        eventIid = T8IdentifierUtilities.createNewGUID();
        entity = tx.create(MESSAGE_HISTORY_DE_IDENTIFIER, null);
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$EVENT_INSTANCE_IDENTIFIER", eventIid);
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$COMMUNICATION_IDENTIFIER", message.getCommunicationIdentifier());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$COMMUNICATION_INSTANCE_IDENTIFIER", message.getCommunicationInstanceIdentifier());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$MESSAGE_IDENTIFIER", message.getIdentifier());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$MESSAGE_INSTANCE_IDENTIFIER", message.getInstanceIdentifier());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$RECIPIENT_IDENTIFIER", message.getRecipientIdentifier());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$RECIPIENT_RECORD_ID", message.getRecipientRecordID());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$RECIPIENT_DISPLAY_NAME", message.getRecipientDisplayName());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$RECIPIENT_ADDRESS", message.getRecipientAddress());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$MESSAGE_SUBJECT", message.getSubject());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$MESSAGE_CONTENT", message.getContent());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$EVENT", event.toString());
        entity.setFieldValue(MESSAGE_HISTORY_DE_IDENTIFIER + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));

        // Insert the entity.
        tx.insert(entity);
    }

    private T8DataEntity createStateEntity(T8DataTransaction tx, T8CommunicationMessageState state) throws Exception
    {
        T8DataEntity entity;

        // Create a new entity.
        entity = tx.create(MESSAGE_STATE_DE_IDENTIFIER, null);
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_ID", state.getMessageIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_IID", state.getMessageInstanceIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$COMMUNICATION_ID", state.getCommunicationIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$COMMUNICATION_IID", state.getCommunicationInstanceIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$SERVICE_ID", state.getServiceIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$SENDER_ID", state.getSenderIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$SENDER_IID", state.getSenderInstanceIdentifier());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$RECIPIENT_DISPLAY_NAME", state.getRecipientDisplayName());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$RECIPIENT_ADDRESS", state.getRecipientAddress());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_SUBJECT", state.getSubject());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_CONTENT", state.getContent());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$MESSAGE_PRIORITY", state.getPriority());
        entity.setFieldValue(MESSAGE_STATE_DE_IDENTIFIER + "$FAILURE_COUNT", state.getFailureCount());
        return entity;
    }

    private String createParameterString(String namespace, Map<String, Object> parameters)
    {
        if (parameters == null)
        {
            return null;
        }
        else if (parameters.isEmpty())
        {
            return null;
        }
        else
        {
            StringBuffer parameterString;
            Map<String, Object> namespaceStrippedParameters;

            // Strip the flow namespace from the parameters.
            namespaceStrippedParameters = T8IdentifierUtilities.stripNamespace(namespace, parameters, false);

            // Add all of the parameters to the string.
            parameterString = new StringBuffer();
            for (String parameterKey : namespaceStrippedParameters.keySet())
            {
                Object parameterValue;

                // Append the prefix and parameter key.
                parameterString.append(" ["); // Yes, there is a space before the bracket.
                parameterString.append(parameterKey);
                parameterString.append(":");

                // Append the parameter value if any.
                parameterValue = namespaceStrippedParameters.get(parameterKey);
                if (parameterValue != null) parameterString.append(parameterValue.toString());

                // Append the suffix.
                parameterString.append("]");
            }

            return parameterString.toString();
        }
    }
}
