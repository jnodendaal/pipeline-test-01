package com.pilog.t8.communication.detail;

import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationMessage.MessagePriority;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8DefaultCommunicationDetails implements T8CommunicationDetails
{
    private int priority;
    private String recipientName;
    private String recipientDefinitionIdentifier;
    private String recipientRecordID;
    private Map<String, Object> additionalDetails;

    public T8DefaultCommunicationDetails(int priority, String recipientName)
    {
        this.priority = priority;
        this.recipientName = recipientName;
    }

    public T8DefaultCommunicationDetails(String recipientName)
    {
        this.recipientName = recipientName;
    }

    public T8DefaultCommunicationDetails(int priority)
    {
        this.priority = priority;
    }

    public T8DefaultCommunicationDetails()
    {
        this(MessagePriority.NORMAL.getFloorValue());
    }

    @Override
    public int compareTo(T8CommunicationDetails detailToCompare)
    {
        return Integer.compare(priority, detailToCompare.getMessagePriority());
    }

    @Override
    public int getMessagePriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    @Override
    public String getRecipientRecordID()
    {
        return recipientRecordID;
    }

    @Override
    public void setRecipientRecordID(String recipientRecordID)
    {
        this.recipientRecordID = recipientRecordID;
    }

    @Override
    public String getRecipientDefinitionIdentifier()
    {
        return recipientDefinitionIdentifier;
    }

    @Override
    public void setRecipientDefinitionIdentifier(String recipientDefinitionIdentifier)
    {
        this.recipientDefinitionIdentifier = recipientDefinitionIdentifier;
    }

    @Override
    public String getRecipientDisplayName()
    {
        return recipientName;
    }

    public void setRecipientName(String recipientName)
    {
        this.recipientName = recipientName;
    }

    @Override
    public void setAdditionalDetails(Map<String, Object> additionalDetails)
    {
        this.additionalDetails = additionalDetails;
    }

    @Override
    public Map<String, Object> getAdditionalDetails()
    {
        return this.additionalDetails;
    }
}
