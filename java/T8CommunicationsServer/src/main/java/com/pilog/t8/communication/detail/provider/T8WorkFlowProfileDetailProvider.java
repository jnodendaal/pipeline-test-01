package com.pilog.t8.communication.detail.provider;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.communication.detail.T8EmailCommunicationDetail;
import com.pilog.t8.communication.detail.T8SMSCommunicationDetail;
import com.pilog.t8.communication.util.MobileNumberUtils;
import com.pilog.t8.communication.util.UserDefinitionAdditionalDetails;
import com.pilog.t8.definition.communication.detail.provider.T8WorkFlowProfileDetailProviderDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.data.T8DataTransaction;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.communication.T8CommunicationType.*;
import com.pilog.t8.security.T8Context;

/**
 * The T8 UserDetail Providers returns user details for users registered in the T8 System.
 * <p/>
 * @author hennie.brink@pilog.co.za
 */
public class T8WorkFlowProfileDetailProvider implements T8CommunicationDetailsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8WorkFlowProfileDetailProvider.class);

    private final T8WorkFlowProfileDetailProviderDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final Map<String, Object> providerParameters;

    public T8WorkFlowProfileDetailProvider(T8Context context, T8WorkFlowProfileDetailProviderDefinition definition, Map<String, Object> providerParameters)
    {
        this.definition = definition;
        this.context = context;
        this.serverContext = context.getServerContext();
        this.providerParameters = providerParameters;
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        return definition.getSupportedCommunicationTypes();
    }

    @Override
    public Set<T8CommunicationDetails> getCommunicationDetails(T8DataTransaction tx, T8CommunicationType communicationType, Map<String, String> messageDetailsMapping)
    {
        Set<T8CommunicationDetails> communicationDetails;
        List<String> workflowProfileIdentifiers;
        String parameterListIdentifier = definition.getPublicIdentifier() + T8WorkFlowProfileDetailProviderDefinition.P_WORKFLOW_PROFILE_IDENTIFIER_LIST;
        try
        {
            workflowProfileIdentifiers = (List<String>) providerParameters.get(parameterListIdentifier);
            if (workflowProfileIdentifiers == null && providerParameters.containsKey(definition.getPublicIdentifier() + T8WorkFlowProfileDetailProviderDefinition.P_WORKFLOW_PROFILE_IDENTIFIER))
            {
                String singleProfileIdentifier = (String) providerParameters.get(definition.getPublicIdentifier() + T8WorkFlowProfileDetailProviderDefinition.P_WORKFLOW_PROFILE_IDENTIFIER);
                workflowProfileIdentifiers = new ArrayList<>(1);
                workflowProfileIdentifiers.add(singleProfileIdentifier);
            }
        }
        catch (ClassCastException ce)
        {
            throw new RuntimeException("The parameter " + parameterListIdentifier + " was of type " + providerParameters.get(parameterListIdentifier).getClass().getCanonicalName() + ", List<String> was expected.", ce);
        }

        communicationDetails = new LinkedHashSet<>();
        if (workflowProfileIdentifiers != null)
        {
            try
            {
                for (T8Definition t8Definition : getUserDefinitions())
                {
                    T8UserDefinition userDefinition = (T8UserDefinition) t8Definition;
                    try
                    {
                        if (userDefinition != null && userDefinition.isActive() && userDefinition.getWorkFlowProfileIdentifiers() != null)
                        {
                            for (String WorkFlowProfileIdentifier : workflowProfileIdentifiers)
                            {
                                for (String userProfileIdentifier : userDefinition.getWorkFlowProfileIdentifiers())
                                {
                                    if (userProfileIdentifier.equals(WorkFlowProfileIdentifier))
                                    {

                                        T8CommunicationDetails communicationDetail;

                                        communicationDetail = getCommunicationDetails(communicationType, userDefinition);
                                        if (communicationDetail != null)
                                        {
                                            communicationDetail.setRecipientDefinitionIdentifier(userDefinition.getIdentifier());
                                            if (messageDetailsMapping != null)
                                            {
                                                communicationDetail.setAdditionalDetails(T8IdentifierUtilities.mapParameters(UserDefinitionAdditionalDetails.getAdditionalDetails(definition.getIdentifier(), userDefinition), messageDetailsMapping));
                                            }
                                            communicationDetails.add(communicationDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LOGGER.log(() -> "Failed to load communication details for " + (userDefinition != null ? userDefinition.getIdentifier() : "null") + " using " + definition.getPublicIdentifier(), ex);
                    }
                }
            }
            catch (Exception ex)
            {
                LOGGER.log(() -> "Failed to load user definitions for " + definition.getPublicIdentifier(), ex);
            }
        }
        return communicationDetails;
    }

    private List<T8Definition> getUserDefinitions() throws Exception
    {
        return serverContext.getDefinitionManager().getRawGroupDefinitions(context, null, T8UserDefinition.GROUP_IDENTIFIER);
    }

    private T8CommunicationDetails getCommunicationDetails(T8CommunicationType communicationType, T8UserDefinition userDefinition)
    {
        switch (communicationType)
        {
            case EMAIL:
            {
                if (userDefinition.getEmailAddress() == null)
                {
                    return null;
                }
                return new T8EmailCommunicationDetail(userDefinition.getEmailAddress(), userDefinition.getName());
            }
            case SMS:
            {
                if (userDefinition.getMobileNumber() == null)
                {
                    return null;
                }
                return new T8SMSCommunicationDetail(MobileNumberUtils.checkValid(userDefinition.getMobileNumber()), userDefinition.getName());
            }
            default:
            {
                return null;
            }
        }
    }
}
