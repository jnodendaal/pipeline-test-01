package com.pilog.t8.communication.service;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessage.MessageHistoryEvent;
import com.pilog.t8.communication.T8CommunicationMessageState;
import com.pilog.t8.communication.T8CommunicationResult;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.event.T8MessageCancelledEvent;
import com.pilog.t8.communication.event.T8MessageFailedEvent;
import com.pilog.t8.communication.event.T8MessageQueuedEvent;
import com.pilog.t8.communication.event.T8MessageSentEvent;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.communication.T8CommunicationServiceDefinition;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.event.EventListenerList;
import com.pilog.t8.communication.event.T8CommunicationServiceListener;
import com.pilog.t8.communication.T8CommunicationPersistenceHandler;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultCommunicationService implements T8CommunicationService
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultCommunicationService.class);

    protected final T8Context internalContext;
    protected final T8ServerContext serverContext;
    protected final EventListenerList serviceListeners;
    protected final String identifier;
    protected final T8CommunicationServiceDefinition serviceDefinition;
    private final T8CommunicationPersistenceHandler persistenceHandler;
    private final MessageQueueProcessor queueProcessor;
    private T8CommunicationManager communicationManager;
    private ScheduledExecutorService executorService;
    private boolean enabled;

    private static final int DEFAULT_QUEUE_POLLING_INTERVAL = 30;

    public T8DefaultCommunicationService(T8Context context, T8CommunicationServiceDefinition serviceDefinition, T8CommunicationManager manager)
    {
        this.internalContext = context;
        this.serverContext = context.getServerContext();
        this.identifier = serviceDefinition.getIdentifier();
        this.communicationManager = manager;
        this.serviceDefinition = serviceDefinition;
        this.serviceListeners = new EventListenerList();
        this.persistenceHandler = new T8CommunicationPersistenceHandler(internalContext);
        this.queueProcessor = new MessageQueueProcessor(internalContext);
    }

    @Override
    public void init() throws Exception
    {
        Integer interval;

        interval = serviceDefinition.getQueuePollingInterval();
        if (interval == null) interval = DEFAULT_QUEUE_POLLING_INTERVAL;
        this.executorService = Executors.newScheduledThreadPool(1, new NameableThreadFactory("T8CommunicationService-"+identifier));
        this.executorService.scheduleWithFixedDelay(queueProcessor, 180, interval, TimeUnit.SECONDS);
    }

    @Override
    public void destroy()
    {
        // Terminate the timer thread and all scheduled tasks.
        try
        {
            this.queueProcessor.stop();
            this.executorService.shutdown();
            if (!this.executorService.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Communication queue processor could not by terminated on service " + identifier);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of communication queue processor on service " + identifier, e);
        }
    }

    /**
     * Enables/disables this service.  If disabled queued messages will not be sent until re-enabled.
     * @param enabled Boolean true to set a state of enabled, false for disabled.
     */
    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    /**
     * Returns the communication manager responsible for this service.
     * @return the communication manager responsible for this service.
     */
    public T8CommunicationManager getCommunicationManager()
    {
        return communicationManager;
    }

    /**
     * Sets the communication manager responsible for this service.
     * @param communicationManager The communication manager responsible for this service.
     */
    public void setCommunicationManager(T8CommunicationManager communicationManager)
    {
        this.communicationManager = communicationManager;
    }

    @Override
    public void queueMessage(T8CommunicationMessage message) throws Exception
    {
        // Set the time when the message was queued and persist the message state.
        message.setTimeQueued(System.currentTimeMillis());
        persistenceHandler.saveState(message.getState());
        persistenceHandler.logMessageEvent(MessageHistoryEvent.QUEUED, message);

        // Fire an event notifying external listeners that the message was queued.
        fireCommunicationMessageQueuedEvent(message);
    }

    @Override
    public T8CommunicationResult sendMessage(T8CommunicationMessage message) throws Exception
    {
        T8CommunicationResult result;

        // Set the time when the message was queued and persist the message state.
        result = sendImmediately(message);
        if (result.isSuccess())
        {
            persistenceHandler.logMessageEvent(MessageHistoryEvent.SENT, message);

            // Fire an event notifying external listeners that the message was queued.
            fireCommunicationMessageSentEvent(message);
            return result;
        }
        else return result;
    }

    protected abstract T8CommunicationResult sendImmediately(T8CommunicationMessage message);

    @Override
    public void cancelMessage(String messageIid) throws Exception
    {
        // Next, make sure the message state has been removed.
        persistenceHandler.deleteState(messageIid);
    }

    @Override
    public void addServiceListener(T8CommunicationServiceListener listener)
    {
        serviceListeners.add(T8CommunicationServiceListener.class, listener);
    }

    @Override
    public void removeServiceListener(T8CommunicationServiceListener listener)
    {
        serviceListeners.remove(T8CommunicationServiceListener.class, listener);
    }

    protected void fireCommunicationMessageSentEvent(T8CommunicationMessage message)
    {
        T8MessageSentEvent event;

        event = new T8MessageSentEvent(message);
        for (T8CommunicationServiceListener listener : serviceListeners.getListeners(T8CommunicationServiceListener.class))
        {
            listener.messageSent(event);
        }
    }

    protected void fireCommunicationMessageFailedEvent(T8CommunicationMessage message, Exception exception)
    {
        T8MessageFailedEvent event;

        event = new T8MessageFailedEvent(message, exception);
        for (T8CommunicationServiceListener listener : serviceListeners.getListeners(T8CommunicationServiceListener.class))
        {
            listener.messageFailed(event);
        }
    }

    protected void fireCommunicationMessageQueuedEvent(T8CommunicationMessage message)
    {
        T8MessageQueuedEvent event;

        event = new T8MessageQueuedEvent(message);
        for (T8CommunicationServiceListener listener : serviceListeners.getListeners(T8CommunicationServiceListener.class))
        {
            listener.messageQueued(event);
        }
    }

    protected void fireCommunicationMessageCanceledEvent(T8CommunicationMessage message)
    {
        T8MessageCancelledEvent event;

        event = new T8MessageCancelledEvent(message);
        for (T8CommunicationServiceListener listener : serviceListeners.getListeners(T8CommunicationServiceListener.class))
        {
            listener.messageCancelled(event);
        }
    }

    private class MessageQueueProcessor extends T8ContextRunnable
    {
        private final Map<String, T8CommunicationMessageDefinition> messageDefinitionCache;
        private final T8DefinitionManager definitionManager;
        private final int maxFailureCount;
        private boolean stopFlag = false;

        private MessageQueueProcessor(T8Context context)
        {
            super(context, "MessageQueueLoader:" + identifier);
            this.definitionManager = serverContext.getDefinitionManager();
            this.messageDefinitionCache = new HashMap<>();
            this.maxFailureCount = serviceDefinition.getMaxRetriesAllowed();
        }

        @Override
        public void exec()
        {
            if (enabled)
            {
                try
                {
                    List<T8CommunicationMessageState> messageStates;

                    // Load the next batch of queued messages.
                    messageStates = persistenceHandler.retrieveMessageStates(identifier, maxFailureCount, serviceDefinition.getSendAmount());
                    if (!messageStates.isEmpty())
                    {
                        LOGGER.log("Sending " + messageStates.size() + " queued messages...");
                        for (T8CommunicationMessageState messageState : messageStates)
                        {
                            // If this
                            if (stopFlag)
                            {
                                break;
                            }
                            else
                            {
                                try
                                {
                                    T8CommunicationMessageDefinition messageDefinition;
                                    T8CommunicationMessage message;
                                    T8CommunicationResult result;
                                    String messageId;

                                    // Create a new message instance from the message state and queue it on the service.
                                    messageId = messageState.getMessageIdentifier();
                                    messageDefinition = getCommunicationMessageDefinition(messageId);
                                    message = messageDefinition.getMessageInstance(context, messageState);

                                    // Send the message.
                                    result = sendImmediately(message);
                                    if (result.isSuccess())
                                    {
                                        // Delete the state because the message was successfully sent.  If anything goes wrong during transmission, the state will remain to be retried later.
                                        persistenceHandler.deleteState(message.getInstanceIdentifier());

                                        // Log the result and then return it.
                                        persistenceHandler.logMessageEvent(MessageHistoryEvent.SENT, message);
                                    }
                                    else
                                    {
                                        int failureCount;

                                        failureCount = message.getFailureCount();
                                        message.setFailureCount(++failureCount);
                                        persistenceHandler.setMessageFailure(message, result.getMessage());
                                        persistenceHandler.logMessageEvent(MessageHistoryEvent.FAILED, message);
                                        fireCommunicationMessageFailedEvent(message, result.getException());
                                    }
                                }
                                catch (Exception e)
                                {
                                    LOGGER.log("Failed to send message: " + messageState.getMessageIdentifier(), e);
                                }
                            }
                        }

                        // Log the end of the operation.
                        LOGGER.log("Sent " + messageStates.size() + " queued messages.");
                    }
                }
                catch(Exception ex)
                {
                    LOGGER.log("Failed to send queued messages.", ex);
                }
                finally
                {
                    // Clear the local cache after every iteration.
                    messageDefinitionCache.clear();
                }
            }
        }

        private T8CommunicationMessageDefinition getCommunicationMessageDefinition(String messageId) throws Exception
        {
            T8CommunicationMessageDefinition messageDefinition;

            messageDefinition = messageDefinitionCache.get(messageId);
            if (messageDefinition != null)
            {
                return messageDefinition;
            }
            else
            {
                messageDefinition = (T8CommunicationMessageDefinition)definitionManager.getInitializedDefinition(internalContext, null, messageId, null);
                if (messageDefinition != null)
                {
                    messageDefinitionCache.put(messageId, messageDefinition);
                    return messageDefinition;
                }
                else throw new RuntimeException("Communication Message definition not found: " + messageId);
            }
        }

        public void stop()
        {
            stopFlag = true;
        }
    }
}
