package com.pilog.t8.communication.detail.provider.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.communication.detail.T8EmailCommunicationDetail;
import com.pilog.t8.communication.detail.T8FaxCommunicationDetail;
import com.pilog.t8.communication.detail.T8SMSCommunicationDetail;
import com.pilog.t8.communication.detail.provider.T8WorkFlowProfileDetailProvider;
import com.pilog.t8.definition.communication.detail.provider.entity.T8EntityDetailProviderDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8EntityDetailProvider implements
        T8CommunicationDetailsProvider
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8WorkFlowProfileDetailProvider.class.getName());
    private final T8EntityDetailProviderDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final Map<String, Object> providerParameters;

    public T8EntityDetailProvider(T8Context context, T8EntityDetailProviderDefinition definition, Map<String, Object> providerParameters)
    {
        this.definition = definition;
        this.serverContext = context.getServerContext();
        this.context = context;
        this.providerParameters = providerParameters;
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        return definition.getSupportedCommunicationTypes();
    }

    @Override
    public Set<T8CommunicationDetails> getCommunicationDetails(
            T8DataTransaction tx,
            T8CommunicationType communicationType,
            Map<String, String> messageDetailsMapping)
    {
        Set<T8CommunicationDetails> communicationDetailsList;
        T8DataFilter dataFilter;
        T8DataEntityResults entities;
        Boolean hasResults;

        communicationDetailsList = new HashSet<>();
        dataFilter = definition.getDataFilterDefinition() == null ? null
                     : definition.getDataFilterDefinition().getNewDataFilterInstance(context, T8IdentifierUtilities.stripNamespace(definition.getIdentifier(), providerParameters, false));
        try
        {
            entities = tx.scroll(definition.getDataEntityIdentifier(), dataFilter);
            hasResults = false;
            while (entities.next())
            {
                T8CommunicationDetails communicationDetails;
                T8DataEntity entity;

                entity = entities.get();
                try
                {
                    communicationDetails = createCommunicationDetail(entity, communicationType);

                    if (communicationDetails != null)
                    {
                        if(!Strings.isNullOrEmpty(definition.getHistoryRecordIDFieldIdentifier()))
                        {
                            String recordID;

                            recordID = (String) entity.getFieldValue(definition.getHistoryRecordIDFieldIdentifier());
                            communicationDetails.setRecipientRecordID(recordID);
                        }

                        if (messageDetailsMapping != null)
                        {
                            communicationDetails.setAdditionalDetails(T8IdentifierUtilities.mapParameters(entity.getFieldValues(), getParameterMappings(messageDetailsMapping, definition.getAdditionalFieldsMapping())));
                        }

                        communicationDetailsList.add(communicationDetails);
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to load communication details for entity " + entity, ex);
                }
                hasResults = true;
            }

            if(!hasResults) LOGGER.log("The query for detail provider " + definition.getPublicIdentifier() + " did not return any results. Parameters: " + providerParameters);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to scroll entity " + definition.getDataEntityIdentifier() + " to retrieve communication details.", ex);
        }
        return communicationDetailsList;
    }

    private T8CommunicationDetails createCommunicationDetail(T8DataEntity entity,
                                                             T8CommunicationType communicationType)
    {
        switch (communicationType)
        {
            case EMAIL:
            {
                Object entityFieldValue = entity.getFieldValue(entity.getIdentifier() + definition.getEmailFieldIdentifier());
                return entityFieldValue instanceof String
                       ? new T8EmailCommunicationDetail((String) entityFieldValue)
                       : null;
            }
            case SMS:
            {
                Object entityFieldValue = entity.getFieldValue(entity.getIdentifier() + definition.getMobileFieldIdentifier());
                return entityFieldValue instanceof String
                       ? new T8SMSCommunicationDetail((String) entityFieldValue)
                       : null;
            }
            case FAX:
            {
                String faxNumber = (String) entity.getFieldValue(entity.getIdentifier() + definition.getFaxNumberFieldIdentifier());
                String faxRecipient = (String) entity.getFieldValue(entity.getIdentifier() + definition.getFaxRecipientNameFieldIdentifier());
                String faxCompany = (String) entity.getFieldValue(entity.getIdentifier() + definition.getFaxCompanyNameFieldIdentifier());
                return faxNumber != null
                       ? new T8FaxCommunicationDetail(faxNumber, faxRecipient, faxCompany)
                       : null;
            }
        }
        return null;
    }

    private Map<String, String> getParameterMappings(
            Map<String, String> messageDetailsMapping,
            Map<String, String> additionalFieldsMapping)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        format.format(Calendar.getInstance().getTime());
        Map<String, String> mappedParameters;

        mappedParameters = new HashMap<>();

        if (messageDetailsMapping != null && additionalFieldsMapping != null)
        {
            for (String key : messageDetailsMapping.keySet())
            {
                String value = messageDetailsMapping.get(key);

                if (additionalFieldsMapping.containsKey(T8IdentifierUtilities.stripNamespace(key)))
                {
                    mappedParameters.put(additionalFieldsMapping.get(T8IdentifierUtilities.stripNamespace(key)), value);
                }
            }
        }

        return mappedParameters;
    }
}
