package com.pilog.t8.communication.template.attachment.email;

import com.pilog.t8.ui.T8Image;
import com.pilog.t8.communication.template.attachment.T8DefaultAttachment;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8EmailImageTemplateAttachmentDefinition;
import com.pilog.t8.definition.gfx.image.T8ImageDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 * @author Hennie Brink
 */
public class T8EmailAttachmentImage extends T8DefaultAttachment
{
    private final T8EmailImageTemplateAttachmentDefinition definition;

    public T8EmailAttachmentImage(T8EmailImageTemplateAttachmentDefinition definition)
    {
        super(definition.getIdentifier(), definition.getAttachmentName(), definition.getAttachmentDescription());
        this.definition = definition;
    }

    @Override
    public void generateAttachment(T8Context context, Map<String, Object> templateInputParamters) throws Exception
    {
        try
        {
            if (Strings.isNullOrEmpty(definition.getImageIdentifier()))
            {
                throw new Exception("Invalid Image Identifier user on definition " + definition.getPublicIdentifier());
            }

            T8ImageDefinition imageDefinition;
            T8Image t8Image;
            BufferedImage bufferedImage;
            ByteArrayOutputStream outputStream;

            imageDefinition = (T8ImageDefinition)context.getServerContext().getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), definition.getImageIdentifier(), null);
            t8Image = imageDefinition.getImage();
            bufferedImage = t8Image.getBufferedImage();

            outputStream = new ByteArrayOutputStream();

            ImageIO.write(bufferedImage, "png", outputStream);

            attachmentData = outputStream.toByteArray();

            outputStream.close();
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to create attachment data source for " + definition.getPublicIdentifier(), ex);
        }
    }

    @Override
    public boolean isEmbedded()
    {
        return definition.isEmbedded();
    }

    @Override
    public String getContentID()
    {
        return definition.getContentID();
    }

    @Override
    public String getContentType()
    {
        return "image/png";
    }
}
