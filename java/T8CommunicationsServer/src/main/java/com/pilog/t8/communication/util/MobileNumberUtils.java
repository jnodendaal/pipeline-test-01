/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.util;

/**
 * Utility class to check validity of mobile numbers
 * @author hennie.brink@pilog.co.za
 */
public class MobileNumberUtils
{
    /**
     * Checks if a mobile number is valid.
     * The following checks are performed:
     * 1. Trims whitespace characters
     * 2. Removes '+' characters
     * 3. Replaces number starting with zero with default country code (+27)
     * @param numberToCheck The mobile number to perform the checks on
     * @return The corrected mobile number
     */
    public static String checkValid(String numberToCheck)
    {
        String number = numberToCheck;
        if(number == null)
            return null;
        number = number.trim();
        number = number.replaceAll(" ", "");
        if(number.startsWith("+"))
            number = number.substring(1);
        if(number.startsWith("0"))
            number = addDefaultCountryCode(number);

        return number;
    }

    /**
     * Adds a default country code(+27) to a mobile number if the number starts with 0
     * @param numberToCheck The number to check
     * @return The replace mobile number
     */
    public static String addDefaultCountryCode(String numberToCheck)
    {
        String number = numberToCheck;
        if(number == null)
            return null;
        number = number.trim();
        number = number.replaceAll(" ", "");

        if(number.startsWith("0"))
            number = number.substring(1);

        number = "27" + number;
        return number;
    }
}
