/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.util;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.user.T8UserDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class UserDefinitionAdditionalDetails
{
    public static Map<String, Object> getAdditionalDetails(String identifier, T8UserDefinition userDefinition)
    {
        Map<String, Object> returnMap = new HashMap<>();
        for (T8UserDefinition.Datum datum : T8UserDefinition.Datum.values())
        {
            returnMap.put(identifier + T8Definition.getLocalIdPrefix() + datum.toString(), userDefinition.getDefinitionDatum(datum.toString()));
        }
        return returnMap;
    }
}
