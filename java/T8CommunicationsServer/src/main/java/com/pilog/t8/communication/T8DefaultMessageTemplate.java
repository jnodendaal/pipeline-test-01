package com.pilog.t8.communication;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8DefaultMessageTemplate implements T8MessageTemplate
{
    private String templateBody;
    private String templateSubject;
    protected final T8CommunicationMessageDefinition definition;
    private Map<String,Object> inputParameters;
    protected T8ServerContext serverContext;
    protected T8Context context;

    public T8DefaultMessageTemplate(T8Context context, T8CommunicationMessageDefinition definition, Map<String,Object> inputParameters)
    {
        this.definition = definition;
        this.inputParameters = inputParameters;
        if(this.inputParameters == null) this.inputParameters = new HashMap<>(0);
        this.templateBody = definition.getTemplate();
        this.serverContext = context.getServerContext();
        this.context = context;
    }

    /**
     * @return the serverContext
     */
    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    /**
     * @return the context
     */
    public T8Context getContext()
    {
        return context;
    }

    /**
     * @return the templateBody
     */
    public String getTemplateBody()
    {
        return templateBody;
    }

    /**
     * @param templateBody the templateBody to set
     */
    public void setTemplateBody(String templateBody)
    {
        this.templateBody = templateBody;
    }

    /**
     * @return the templateSubject
     */
    public String getTemplateSubject()
    {
        return templateSubject;
    }

    /**
     * @param templateSubject the templateSubject to set
     */
    public void setTemplateSubject(String templateSubject)
    {
        this.templateSubject = templateSubject;
    }

    /**
     * @return the definition
     */
    public T8CommunicationMessageDefinition getDefinition()
    {
        return definition;
    }

    /**
     * @return the inputParameters
     */
    public Map<String,Object> getInputParameters()
    {
        return inputParameters;
    }
}