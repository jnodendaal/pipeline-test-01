package com.pilog.t8.communication.detail.provider;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.communication.T8CommunicationDetails;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.communication.detail.T8EmailCommunicationDetail;
import com.pilog.t8.communication.detail.T8SMSCommunicationDetail;
import com.pilog.t8.communication.util.MobileNumberUtils;
import com.pilog.t8.communication.util.UserDefinitionAdditionalDetails;
import com.pilog.t8.definition.communication.detail.provider.T8UserDetailProviderDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.communication.T8CommunicationType.*;

/**
 * The T8 UserDetail Providers returns user details for users registered in the T8 System.
 * <p/>
 * @author hennie.brink@pilog.co.za
 */
public class T8UserDetailProvider implements T8CommunicationDetailsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8UserDetailProvider.class);

    private final T8UserDetailProviderDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final Map<String, Object> providerParameters;

    public T8UserDetailProvider(T8Context context, T8UserDetailProviderDefinition definition, Map<String, Object> providerParameters)
    {
        this.definition = definition;
        this.context = context;
        this.serverContext = context.getServerContext();
        this.providerParameters = providerParameters;
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        return definition.getSupportedCommunicationTypes();
    }

    @Override
    public Set<T8CommunicationDetails> getCommunicationDetails(T8DataTransaction tx, T8CommunicationType communicationType, Map<String, String> messageDetailsMapping)
    {
        Set<T8CommunicationDetails> communicationDetails;
        List<String> userIds;
        String parameterId;

        parameterId = definition.getPublicIdentifier() + T8UserDetailProviderDefinition.P_USER_IDENTIFIER_LIST;
        userIds = (List<String>) providerParameters.get(parameterId);
        if (userIds == null && providerParameters.containsKey(definition.getPublicIdentifier() + T8UserDetailProviderDefinition.P_USER_IDENTIFIER))
        {
            userIds = new ArrayList<>(1);
            userIds.add((String)providerParameters.get(definition.getPublicIdentifier() + T8UserDetailProviderDefinition.P_USER_IDENTIFIER));
        }

        communicationDetails = new LinkedHashSet<>();
        if (userIds != null)
        {
            for (String identifier : userIds)
            {
                if(identifier != null)
                {
                    T8UserDefinition userDefinition;
                    try
                    {
                        userDefinition = (T8UserDefinition)serverContext.getDefinitionManager().getRawDefinition(context, null, identifier);
                        if(userDefinition != null && userDefinition.isActive())
                        {
                            T8CommunicationDetails communicationDetail = getCommunicationDetails(communicationType, userDefinition);
                            if (communicationDetail != null)
                            {
                                communicationDetail.setRecipientDefinitionIdentifier(userDefinition.getIdentifier());
                                if (messageDetailsMapping != null)
                                {
                                    communicationDetail.setAdditionalDetails(T8IdentifierUtilities.mapParameters(UserDefinitionAdditionalDetails.getAdditionalDetails(definition.getIdentifier(), userDefinition), messageDetailsMapping));
                                }
                                communicationDetails.add(communicationDetail);

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LOGGER.log("Failed to load communication details for user " + identifier, ex);
                    }
                }
                else LOGGER.log(T8Logger.Level.WARNING, "The user identifier provided for communication detail provider " + definition.getPublicIdentifier() + " is null.");
            }
        }
        return communicationDetails;
    }

    private T8CommunicationDetails getCommunicationDetails(T8CommunicationType communicationType, T8UserDefinition userDefinition)
    {
        switch (communicationType)
        {
            case EMAIL:
            {
                if (userDefinition.getEmailAddress() == null)
                {
                    return null;
                }
                return new T8EmailCommunicationDetail(userDefinition.getEmailAddress(), userDefinition.getName());
            }
            case SMS:
            {
                if (userDefinition.getMobileNumber() == null)
                {
                    return null;
                }
                return new T8SMSCommunicationDetail(MobileNumberUtils.checkValid(userDefinition.getMobileNumber()), userDefinition.getName());
            }
            default:
            {
                return null;
            }
        }
    }
}
