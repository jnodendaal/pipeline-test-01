package com.pilog.t8.communication.service.clickatell;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationResult;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8DefaultCommunicationResult;
import com.pilog.t8.communication.message.T8SMSMessage;
import com.pilog.t8.communication.service.T8DefaultCommunicationService;
import com.pilog.t8.communication.util.MobileNumberUtils;
import com.pilog.t8.definition.communication.service.T8ClickATellSMSServiceDefinition;
import com.pilog.t8.security.T8Context;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * Service to Sens SMS notifications using the Click-A-Tell provider.
 * @author hennie.brink@pilog.co.za
 */
public class T8ClickATellSMSService extends T8DefaultCommunicationService
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8ClickATellSMSService.class.getName());
    private T8ClickATellSMSServiceDefinition serviceDefinition;
    private String sessionId;
    private long lastRequest;

    public T8ClickATellSMSService(T8Context context, T8ClickATellSMSServiceDefinition serviceDefinition, T8CommunicationManager manager)
    {
        super(context, serviceDefinition, manager);
        this.serviceDefinition = serviceDefinition;
    }

    @Override
    public String getIdentifier()
    {
        return serviceDefinition.getIdentifier();
    }

    @Override
    protected T8CommunicationResult sendImmediately(T8CommunicationMessage message)
    {
        ClickATellApi.SendMessage sendMessage;
        T8SMSMessage smsMessage;
        ClickATellApi api;

        smsMessage = (T8SMSMessage)message;
        smsMessage.setServiceIdentifier(identifier);

        sendMessage = new ClickATellApi.SendMessage();
        sendMessage.setApi_id(serviceDefinition.getApiID());
        sendMessage.setUser(serviceDefinition.getUserName());
        sendMessage.setPassword(serviceDefinition.getPassword());
        sendMessage.setConcat(3);
        sendMessage.setCallback(0);
        sendMessage.setFrom(MobileNumberUtils.checkValid(serviceDefinition.getFromNumber()));
        sendMessage.setTo(smsMessage.getCellphoneNumber());
        sendMessage.setText(smsMessage.getMessageContent());
        sendMessage.setQueue(smsMessage.getPriority());

        api = new ClickATellApi();
        api.setSendMessage(sendMessage);

        try
        {
            authenticateSession();
            ClickATellApi apiResult = makeApiRequest(api);
            String messageID = apiResult.getSendMsgResp().getApiMsgId();
            if (messageID == null)
            {
                String faultId = apiResult.getSendMsgResp().getFault();
                return new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.NotSent, ClickatellCodes.ErrorCodes.get(faultId), smsMessage);
            }
            T8DefaultCommunicationResult commResult = new T8DefaultCommunicationResult(true, T8DefaultCommunicationResult.Result.Sent, smsMessage);
            commResult.setMessageId(messageID);
            return commResult;
        }
        catch (MalformedURLException ex)
        {
            return new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.Error, "The URL  is not valid.", ex, smsMessage);
        }
        catch (IOException ex)
        {
            return new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.Error, "Unkown IO Error", ex, smsMessage);
        }
        catch (JAXBException ex)
        {
            return new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.Error, "JAXB Failed to marshall/unmarshall the click a tell api object", ex, smsMessage);
        }
        catch (Exception ex)
        {
            return new T8DefaultCommunicationResult(false, T8DefaultCommunicationResult.Result.Error, "Unkown Error while communicating with click a tell service", ex, smsMessage);
        }
    }

    private void authenticateSession() throws MalformedURLException, IOException, JAXBException, Exception
    {
        //Re-authenticate after ten minutes
        if (sessionId == null
            || (lastRequest < (System.currentTimeMillis() - (1000 * 60 * 10))))
        {
            ClickATellApi api = new ClickATellApi();
            ClickATellApi.Authentication authentication = new ClickATellApi.Authentication();
            authentication.setApi_id(serviceDefinition.getApiID());
            authentication.setUser(serviceDefinition.getUserName());
            authentication.setPassword(serviceDefinition.getPassword());
            api.setAuth(authentication);

            ClickATellApi apiResult = makeApiRequest(api);
            sessionId = apiResult.getAuthResp().getSession_id();
            if (sessionId == null)
            {
                String faultId = apiResult.getAuthResp().getFault();
                throw new Exception("ClickATellAuthFailure-" + faultId + " " + ClickatellCodes.ErrorCodes.get(faultId));
            }
        }
    }

    private ClickATellApi makeApiRequest(ClickATellApi apiRequest) throws MalformedURLException, IOException, JAXBException, Exception
    {
        String url = "http://api.clickatell.com/xml/xml";
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        JAXBContext jAXBContext = JAXBContext.newInstance(ClickATellApi.class);
        Marshaller marshaller = jAXBContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        //marshaller.marshal(apiRequest,new OutputStreamWriter(connection.getOutputStream(),"ISO-8859-1"));
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write("data= " + URLEncoder.encode(marshalToString(marshaller, apiRequest), "UTF-8"));
        out.flush();
        out.close();
        ClickATellApi retObj = (ClickATellApi) jAXBContext.createUnmarshaller().unmarshal(new InputStreamReader(connection.getInputStream()));
        connection.disconnect();
        lastRequest = System.currentTimeMillis();
        if (retObj.getXmlErrResponse() != null)
        {
            throw new Exception(retObj.getXmlErrResponse().getFault() + " " + marshalToString(marshaller, apiRequest));
        }
        return retObj;
    }

    public String marshalToString(Marshaller marshaller, Object obj) throws JAXBException, UnsupportedEncodingException
    {
        StringWriter sw = new StringWriter();
        marshaller.marshal(obj, sw);
        return sw.toString();
    }
}
