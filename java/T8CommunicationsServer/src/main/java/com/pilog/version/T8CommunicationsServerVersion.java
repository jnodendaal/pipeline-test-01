package com.pilog.version;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8CommunicationsServerVersion
{
    public final static String VERSION = "109";
}
