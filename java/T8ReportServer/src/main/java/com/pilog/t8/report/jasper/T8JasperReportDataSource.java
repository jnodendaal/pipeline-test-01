package com.pilog.t8.report.jasper;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8JasperReportDataSource implements JRDataSource
{
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8DataFilter dataFilter;
    private final T8DataTransaction tx;
    private final String entityId;
    private T8DataEntityResults entityResults;

    public T8JasperReportDataSource(T8Context context, String entityId, T8DataFilter filter) throws Exception
    {
        this.dataFilter = (filter == null ? new T8DataFilter(entityId) : filter);
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.entityId = entityId;
        this.tx = this.serverContext.getDataManager().getCurrentSession().instantTransaction();
    }

    @Override
    public boolean next() throws JRException
    {
        try
        {
            if (entityResults == null)
            {
                this.entityResults = this.tx.scroll(entityId, dataFilter);
            }

            if (!entityResults.next())
            {
                //we are done, so close the connection. We need to do this in order for the sub report connections to be closed
                this.entityResults.close();
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            Logger.getLogger(T8JasperReportDataSource.class.getName()).log(Level.SEVERE, "Failed to retieve next results in result set for entity " + entityId, ex);
            throw new JRException("Failed to retieve next results in result set for entity " + entityId, ex);
        }
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException
    {
        try
        {
            return entityResults.get().getFieldValue(entityId+"$"+jrf.getName());
        }
        catch (Exception ex)
        {
            Logger.getLogger(T8JasperReportDataSource.class.getName()).log(Level.SEVERE, "Failed to retieve field " + jrf.getName(), ex);
            throw new JRException("Failed to retieve field " + jrf.getName(), ex);
        }
    }
}
