package com.pilog.t8.communication.template.attachment;

import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8JasperReportAttachmentDefinition;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8JasperReportAttachmentParameterScriptDefinition;
import com.pilog.t8.definition.report.jasper.T8JasperReportServerAPIHandler;
import com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerAPIHandler;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8JasperReportEmailAttachment extends T8DefaultAttachment
{
    private final T8JasperReportAttachmentDefinition definition;
    private boolean reportGenerated = false;

    public T8JasperReportEmailAttachment(T8JasperReportAttachmentDefinition definition)
    {
        super(definition.getIdentifier(), definition.getAttachmentName(), definition.getAttachmentDescription());
        this.definition = definition;
    }

    @Override
    public void generateAttachment(T8Context context, Map<String, Object> templateInputParamters) throws Exception
    {
        Map<String, Object> additionalReportParameters;

        try
        {
            if (!reportGenerated)
            {
                Map<String, Object> reportOperationParams = new HashMap<>();
                reportOperationParams.put(T8JasperReportServerAPIHandler.PARAMETER_REPORT_DEFINITION_IDENTIFIER, definition.getReportIdentifier());

                if (definition.getReportDataFilter() != null)
                {
                    reportOperationParams.put(T8JasperReportServerAPIHandler.PARAMETER_REPORT_ALTERNATE_FILTER, definition.getReportDataFilter().getNewDataFilterInstance(context, null));
                }

                additionalReportParameters = extractParametersFromScript(context, definition.getReportParameterScriptDefinition(), templateInputParamters);
                if (additionalReportParameters != null)
                {
                    reportOperationParams.put(T8JasperReportViewerAPIHandler.PARAMETER_REPORT_PARAMETERS, additionalReportParameters);
                }

                JasperPrint jasperReport = (JasperPrint)context.getServerContext().executeSynchronousOperation(context, T8JasperReportServerAPIHandler.OPERATION_GENERATE_REPORT, reportOperationParams).get(T8JasperReportServerAPIHandler.PARAMETER_REPORT);
                attachmentData = JasperExportManager.exportReportToPdf(jasperReport);
                reportGenerated = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to create attachment data source for " + definition.getPublicIdentifier(), ex);
        }
    }

    protected Map<String, Object> extractParametersFromScript(T8Context context, T8JasperReportAttachmentParameterScriptDefinition scriptDefinition, Map<String, Object> templateInputParameters) throws Exception
    {
        if (scriptDefinition == null) return null; // We don't want to continuously check for nulls

        HashMap<String, Object> inputParameters;
        Map<String, Object> outputParameters;
        T8ServerContextScript script;
        Object outputObject;

        // Add the input parameters.
        inputParameters = new HashMap<>();
        inputParameters.put(T8JasperReportAttachmentDefinition.PARAMETER_TEMPLATE_PARAMETERS, templateInputParameters);

        // Create the script instance and execute it.
        script = scriptDefinition.getNewScriptInstance(context);
        outputParameters = script.executeScript(inputParameters);

        // Check the script output.
        if (outputParameters == null) throw new IllegalStateException("Jasper Report parameter script for email attachments returned no output.");
        else if ((outputObject = outputParameters.get(definition.getRootDefinition().getIdentifier() + T8JasperReportAttachmentDefinition.PARAMETER_JASPER_REPORT_PARAMETERS)) == null || !(outputObject instanceof Map))
        {
            throw new IllegalStateException("The Jasper Report parameter script for email attachments returned invalid output for the report parameters. Output data : " + outputObject);
        }
        else return (Map<String, Object>) outputObject;
    }
}
