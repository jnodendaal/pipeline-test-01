package com.pilog.t8.report.excel;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.report.T8ReportDescriptionScriptDefinition;
import com.pilog.t8.definition.report.excel.T8ExcelReportDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.file.handler.T8ExcelOutputFileHandler;
import com.pilog.t8.report.T8ReportDescription;
import com.pilog.t8.report.T8ReportGenerationResult;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.utilities.strings.Strings;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelReportGenerator implements T8ReportGenerator
{
    private final T8ExcelReportDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;

    public T8ExcelReportGenerator(T8Context context, T8ExcelReportDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = new T8ProjectContext(context, definition.getRootProjectId());
        this.definition = definition;
    }

    @Override
    public T8ReportDescription generateReportDescription(String reportIid, Map<String, Object> parameters)
    {
        try
        {
            T8ServerContextScriptDefinition scriptDefinition;

            // Get the definition of the script to use for generation of report descrioption.
            scriptDefinition = definition.getDescriptionScript();
            if (scriptDefinition != null)
            {
                Map<String, Object> outputParameters;
                String description;
                T8Script script;
                String name;

                // Execute the script.
                script = scriptDefinition.getNewScriptInstance(context);
                outputParameters = script.executeScript(parameters);
                outputParameters = T8IdentifierUtilities.stripNamespace(outputParameters);

                // Return the report description.
                name = (String)outputParameters.get(T8ReportDescriptionScriptDefinition.PARAMETER_NAME);
                description = (String)outputParameters.get(T8ReportDescriptionScriptDefinition.PARAMETER_DESCRIPTION);
                return new T8ReportDescription(name, description);
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while generating report description: " + definition, e);
        }
    }

    @Override
    public T8ReportGenerationResult generateReport(String reportIid, Map<String, Object> parameters, String fileContextId, String filepath) throws Exception
    {
        Map<String, String> columnMapping;
        T8FileManager fileManager;
        T8DataFilter reportFilter;
        String fileContextIid;

        // Generate a file context iid to use for this operation.
        fileContextIid = T8IdentifierUtilities.createNewGUID();

        // Get the required parameters for report generation.
        columnMapping = (Map<String, String>)parameters.get(definition.getIdentifier() + T8ExcelReportDefinition.PARAMETER_COLUMN_MAPPING);
        reportFilter = (T8DataFilter)parameters.get(definition.getIdentifier() + T8ExcelReportDefinition.PARAMETER_DATA_FILTER);

        // Open a file context to which the report will be exported.
        fileManager = serverContext.getFileManager();
        fileManager.openFileContext(context, fileContextIid, fileContextId, new T8Minute(30));

        try
        {
            T8ExcelOutputFileHandler fileHandler;
            T8ReportDescription description;
            String reportName;
            String reportFilepath;

            // Evaluate the name of the report.
            description = generateReportDescription(reportIid, parameters);
            reportName = description.getName();
            if (Strings.trimToNull(reportName) == null) throw new Exception("Invalid name defined for report: " + definition);

            // Set the file paths to be used during generation.
            if (filepath != null)
            {
                reportFilepath = filepath + File.separator + reportName + ".xlsx";
            }
            else
            {
                reportFilepath = reportName + ".xlsx";
            }

            // Get the file handler.
            fileHandler = new T8ExcelOutputFileHandler(context, fileManager, fileContextIid, reportFilepath);

            // Export the entity data.
            try
            {
                T8DataEntityResults results;
                T8DataTransaction tx;
                String sheetName;
                String entityId;

                // Open the file handler to the excel output file and get the transaction to use for data retrieval.
                fileHandler.open();
                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
                entityId = definition.getDataEntityId();
                sheetName = "Report";

                // Create the sheet to which the data will be written.
                fileHandler.createSheet(sheetName, new ArrayList<>(columnMapping.keySet()));
                fileHandler.writeSheet(sheetName);

                // Scroll through all results and write each entity to the output file.
                results = tx.scroll(entityId, reportFilter);
                try
                {
                    while (results.next())
                    {
                        Map<String, Object> dataRow;
                        T8DataEntity entity;

                        entity = results.get();
                        dataRow = new HashMap<>();
                        for (String columnName : columnMapping.keySet())
                        {
                            dataRow.put(columnName, entity.getFieldValue(columnMapping.get(columnName)));
                        }

                        fileHandler.writeRow(sheetName, dataRow);
                    }

                    // Auto-size the columns now that all data has been written.
                    fileHandler.autoSizeColumns(sheetName);
                }
                finally
                {
                    results.close();
                }
            }
            finally
            {
                fileHandler.close();
            }

            // Return the final result.
            return new T8ReportGenerationResult(reportIid, fileContextId, reportFilepath);
        }
        finally
        {
            fileManager.closeFileContext(context, fileContextIid);
        }
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public void cancel()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
