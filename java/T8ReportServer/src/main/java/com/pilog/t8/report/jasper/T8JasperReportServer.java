//package com.pilog.t8.report.jasper;
//
//import com.pilog.t8.T8FileManager;
//import com.pilog.t8.T8Log;
//import com.pilog.t8.T8ServerContext;
//import com.pilog.t8.T8SessionContext;
//import com.pilog.t8.data.filter.T8DataFilter;
//import com.pilog.t8.data.filter.T8DataFilterClause;
//import com.pilog.t8.data.filter.groupby.T8GroupByDataFilter;
//import com.pilog.t8.definition.T8IdentifierUtilities;
//import com.pilog.t8.log.T8Logger;
//import com.pilog.t8.operation.progressreport.T8DefaultProgressReport;
//import com.pilog.t8.operation.progressreport.T8ProgressReport;
//import com.pilog.t8.performance.T8PerformanceStatistics;
//import com.pilog.t8.time.T8Day;
//import com.pilog.t8.definition.notification.T8JasperReportNotificationDefinition;
//import com.pilog.t8.definition.notification.T8JasperReportNotificationDefinition.ReportType;
//import com.pilog.t8.definition.operation.T8DefaultServerOperation;
//import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
//import com.pilog.t8.definition.report.jasper.T8JasperImageDefinition;
//import com.pilog.t8.definition.report.jasper.T8JasperReportDefinition;
//import com.pilog.t8.definition.report.jasper.T8JasperReportServerAPIHandler;
//import com.pilog.t8.definition.report.jasper.T8JasperSubReportDefinition;
//import com.pilog.t8.definition.report.jasper.T8MainJasperReportDefinition;
//import com.pilog.t8.security.T8Context;
//import com.pilog.t8.thread.T8ContextRunnable;
//import com.pilog.t8.utilities.collections.HashMaps;
//import java.io.ByteArrayInputStream;
//import java.io.File;
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import net.sf.jasperreports.engine.JRParameter;
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperReport;
//import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
//import net.sf.jasperreports.engine.util.JRSwapFile;
//
//import static com.pilog.t8.definition.ui.report.jasper.T8JasperReportViewerAPIHandler.PARAMETER_REPORT_PARAMETERS;
//
///**
// * @author hennie.brink@pilog.co.za
// */
//public class T8JasperReportServer
//{
//    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperReportServer.class);
//
//    public static class GenerateReport extends T8DefaultServerOperation
//    {
//        private String progressMessage;
//        private Double progress;
//
//        public GenerateReport(T8ServerContext serverContext, T8SessionContext sessionContext, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
//        {
//            super(serverContext, sessionContext, definition, operationInstanceIdentifier);
//            progress = 0.0;
//        }
//
//        @Override
//        public Map<String, JasperPrint> execute(T8Context accessContext, Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
//        {
//            T8MainJasperReportDefinition reportDefinition;
//            Map<String, Object> reportParameters;
//            JasperReport jasperReport;
//            T8DataFilter reportFilter;
//            boolean asyncGeneration;
//            String projectId;
//            String reportId;
//
//            projectId = (String)operationParameters.get(T8JasperReportServerAPIHandler.PARAMETER_PROJECT_ID);
//            reportId = (String)operationParameters.get(T8JasperReportServerAPIHandler.PARAMETER_REPORT_DEFINITION_IDENTIFIER);
//            reportParameters = (Map<String, Object>) operationParameters.get(PARAMETER_REPORT_PARAMETERS);
//            asyncGeneration = Boolean.TRUE.equals(operationParameters.get(T8JasperReportServerAPIHandler.PARAMETER_REPORT_ASYNC_GENERATION));
//
//            if (reportId == null || "".equals(reportId)) throw new Exception("Report Definition identifier is empty, you must provide it in order to generate a report.");
//            if (reportParameters == null) reportParameters = new HashMap<>();
//            reportParameters.put("T8ServerContext", this.serverContext);
//
//            try
//            {
//                progressMessage = "Retrieving report template";
//                LOGGER.log("Retrieving report definition " + reportId);
//
//                //Get the report definition and compile the report
//                reportDefinition = (T8MainJasperReportDefinition) serverContext.getDefinitionManager().getInitializedDefinition(sessionContext, projectId, reportId, new HashMap<>());
//                byte[] reportBs = reportDefinition.getReport();
//                jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(reportBs));
//                this.progress = 10D;
//
//                // Build the report filter and add it to the report parameters
//                reportFilter = buildReportFilter(reportDefinition, operationParameters);
//                reportParameters.put("ReportFilter", reportFilter);
//                this.progress = 20D;
//
//                //Set Report Images
//                if (reportDefinition.getReportImages() != null)
//                {
//                    for (T8JasperImageDefinition imageDefinition : reportDefinition.getReportImages())
//                    {
//                        reportParameters.put(imageDefinition.getReportParameterName(), imageDefinition.getImage().getBufferedImage());
//                    }
//                }
//                this.progress = 25D;
//
//                //Set the report sub reports
//                if (reportDefinition.getSubReports() != null)
//                {
//                    for (T8JasperSubReportDefinition subReportDefinition : reportDefinition.getSubReports())
//                    {
//                        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(subReportDefinition.getReport()))
//                        {
//                            JasperReport subReport = JasperCompileManager.compileReport(inputStream);
//                            reportParameters.put(subReportDefinition.getReportParameterName(), subReport);
//                            reportParameters.put(subReportDefinition.getReportParameterName() + "_EID", subReportDefinition.getReportDataEntityIdentifier());
//                        }
//                    }
//                }
//                this.progress = 30D;
//
//                return generateReport(jasperReport, reportDefinition, reportParameters, reportFilter, asyncGeneration);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Failed to create report " + reportId, ex);
//            }
//        }
//
//        /**
//         * Builds a combined filter, using the preset filter, or a default
//         * filter, together with the filter which may have been passed from the
//         * caller.
//         *
//         * @param mainReportDefinition The {@code T8MainJasperReportDefinition}
//         *      containing the preset data filter, if any
//         * @param operationParameters The {@code Map} of operation parameters
//         *      which contain the caller filter, if any
//         *
//         * @return The {@code T8DataFilter} to be used for the report
//         */
//        private T8DataFilter buildReportFilter(T8MainJasperReportDefinition mainReportDefinition, Map<String, Object> operationParameters)
//        {
//            T8DataFilter reportFilter;
//            T8DataFilter inputFilter;
//
//            //Create the report filter
//            if (mainReportDefinition.getReportDataFilter() == null)
//            {
//                // TODO: GBO - This continuously breaks things. This should be a basic filter
//                reportFilter = new T8GroupByDataFilter(mainReportDefinition.getReportDataEntityIdentifier());
//            } else reportFilter = mainReportDefinition.getReportDataFilter().getNewDataFilterInstance(this.serverContext, this.sessionContext, null);
//
//            // Add any filter that came from the caller
//            inputFilter = (T8DataFilter) operationParameters.get(T8JasperReportServerAPIHandler.PARAMETER_REPORT_ALTERNATE_FILTER);
//            if (inputFilter != null)
//            {
//                reportFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, inputFilter);
//            }
//
//            return reportFilter;
//        }
//
//        private HashMap<String, JasperPrint> generateReport(JasperReport jasperReport, T8JasperReportDefinition reportDefinition, Map<String, Object> reportParameters, T8DataFilter reportFilter, boolean asyncGeneration) throws Exception
//        {
//            progress = 40D;
//
//            // Check if the report should be generated in the background
//            if (asyncGeneration)
//            {
//                T8JasperServerSession serverSessionContext;
//
//                // Build the necessary to enable server side generation once the user data session is closed
//                serverSessionContext = buildServerSessionContext(reportDefinition, serverContext, sessionContext);
//                new Thread(new ReportGenerator(this.serverContext, serverSessionContext, jasperReport, reportDefinition, reportFilter, reportParameters)).start();
//
//                return null;
//            }
//            else
//            {
//                T8JasperReportDataSource dataSource;
//                JasperPrint jasperPrint;
//
//                progressMessage = "Creating report data source";
//                LOGGER.log("Creating report datasource " + reportDefinition.getReportDataEntityIdentifier());
//                progress = 50.0;
//
//                progressMessage = "Creating the report";
//                LOGGER.log("Filling report.");
//
//                //Create the report data source
//                dataSource = new T8JasperReportDataSource(this.serverContext, this.sessionContext, reportDefinition.getReportDataEntityIdentifier(), reportFilter);
//
//                // Add the current user session context to the report parameters
//                reportParameters.put("T8SessionContext", this.sessionContext);
//
//                jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, dataSource);
//                return HashMaps.createSingular(T8JasperReportServerAPIHandler.PARAMETER_REPORT, jasperPrint);
//            }
//        }
//
//        private T8JasperServerSession buildServerSessionContext(T8JasperReportDefinition reportDefinition, T8ServerContext serverContext, T8SessionContext userSessionContext)
//        {
//            return new T8JasperServerSession(serverContext.createSystemSessionContext(userSessionContext.getOrganizationIdentifier(), reportDefinition.getIdentifier(), null), userSessionContext);
//        }
//
//        public Double getProgress()
//        {
//            return progress;
//        }
//
//        @Override
//        public double getProgress(T8SessionContext sessionContext)
//        {
//            return getProgress();
//        }
//
//        @Override
//        public T8ProgressReport getProgressReport(T8SessionContext sessionContext)
//        {
//            return new T8DefaultProgressReport(progressMessage, progress);
//        }
//
//        private class ReportGenerator extends T8ContextRunnable
//        {
//            private final Map<String, Object> reportParameters;
//            private final T8JasperReportDefinition reportDefinition;
//            private final T8FileManager fileManager;
//            private final JasperReport jasperReport;
//            private final T8DataFilter reportFilter;
//            private final String swapFileContextId;
//
//            // Data source cannot be created in the constructor since it will
//            // then be linked to the wrong thread
//            private T8JasperReportDataSource dataSource;
//
//            private ReportGenerator(T8ServerContext serverContext, T8JasperServerSession serverSessionContext, JasperReport jasperReport, T8JasperReportDefinition reportDefinition, T8DataFilter reportFilter, Map<String, Object> reportParameters) throws Exception
//            {
//                super(serverContext, serverSessionContext, "JRG-"+reportDefinition.getIdentifier());
//                this.reportParameters = reportParameters;
//                this.reportDefinition = reportDefinition;
//                this.jasperReport = jasperReport;
//                this.reportFilter = reportFilter;
//
//                //Open the swap file context
//                this.fileManager = serverContext.getFileManager();
//                this.swapFileContextId = T8IdentifierUtilities.createNewGUID();
//                this.fileManager.openFileContext(serverSessionContext, this.swapFileContextId, null, new T8Day(1));
//
//                this.reportParameters.put("T8SessionContext", this.sessionContext);
//            }
//
//            @Override
//            public void exec()
//            {
//                JRSwapFileVirtualizer virtualizer;
//                String absolutePDFPath;
//                String reportFileName;
//                ReportType reportType;
//                File reportPath;
//
//                reportFileName = this.swapFileContextId + ".rpt";
//                reportType = ReportType.PDF;
//
//                try
//                {
//                    //Create the virtualizer to prevent out of memory issues
//                    virtualizer = new JRSwapFileVirtualizer(50, new JRSwapFile(serverContext.getFileManager().getContextPath(this.sessionContext, this.swapFileContextId), 1024 * 8, 50));
//                    reportParameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
//
//                    // Get the path for the report file
//                    reportPath = new File(fileManager.getContextPath(this.sessionContext, this.swapFileContextId) + File.separator + reportFileName);
//
//                    // Create the data source in the new thread
//                    this.dataSource = new T8JasperReportDataSource(serverContext, this.sessionContext, reportDefinition.getReportDataEntityIdentifier(), this.reportFilter);
//
//                    // Write the report data to the file
//                    JasperFillManager.fillReportToFile(jasperReport, reportPath.getAbsolutePath(), reportParameters, dataSource);
//
//                    // We always make it PDF to be downloaded, otherwise the report is lost
//                    absolutePDFPath = new File(fileManager.getContextPath(this.sessionContext, this.swapFileContextId) + File.separator + this.swapFileContextId + ".pdf").getAbsolutePath();
//                    JasperExportManager.exportReportToPdfFile(reportPath.getAbsolutePath(), absolutePDFPath);
//
//                    // We want the client to download the pdf instead
//                    reportFileName = this.swapFileContextId + ".pdf";
//
//                    // Send the success notification
//                    sendNotification(reportType, reportFileName, true);
//
//                    virtualizer.cleanup();
//                }
//                catch (Exception ex)
//                {
//                    // Send a failure notification
//                    sendNotification(ReportType.JASPER, reportFileName, false);
//                    LOGGER.log("Failed to create report " + reportDefinition.getPublicIdentifier(), ex);
//                }
//            }
//
//            private void sendNotification(ReportType reportType, String reportFileName, boolean success)
//            {
//                HashMap<String, Object> notificationParameters;
//
//                notificationParameters = new HashMap<>();
//                try
//                {
//                    notificationParameters.put(T8JasperReportNotificationDefinition.PARAMETER_REPORT_NAME, reportDefinition.getName());
//                    notificationParameters.put(T8JasperReportNotificationDefinition.PARAMETER_FILE_CONTEXT_IID, this.swapFileContextId);
//                    notificationParameters.put(T8JasperReportNotificationDefinition.PARAMETER_REPORT_TYPE, reportType.toString());
//                    notificationParameters.put(T8JasperReportNotificationDefinition.PARAMETER_REPORT_FILE_NAME, reportFileName);
//                    notificationParameters.put(T8JasperReportNotificationDefinition.PARAMETER_GENERATION_TIME, System.currentTimeMillis());
//                    notificationParameters.put(T8JasperReportNotificationDefinition.PARAMETER_GENERATION_SUCCESS, success);
//
//                    // Send the notification
//                    serverContext.getNotificationManager().sendNotification(this.sessionContext, T8JasperReportNotificationDefinition.IDENTIFIER, notificationParameters);
//                }
//                catch (Exception ex)
//                {
//                    LOGGER.log(() -> String.format("Failed to complete the notification for asynchronous report generation. Report Success: %b - Report Name: %s - Report Type: %s - Context ID: %s - Report File Name: %s", success, reportDefinition.getName(), reportType.toString(), this.swapFileContextId, reportFileName), ex);
//                }
//            }
//        }
//    }
//
//    /**
//     * This is a session wrapper class which is very specifically intended to
//     * be used within the context of an asynchronous Jasper report generated.<br/>
//     * <br/>
//     * This is not the optimal way for this to be done, but will only be
//     * revisited once server security is revisited.
//     */
//    public static class T8JasperServerSession implements T8SessionContext
//    {
//        private static final long serialVersionUID = -3571030440835322108L;
//
//        private final T8SessionContext basicServerSessionContext;
//        private final Map<String, Object> sessionParameterMap;
//        private final List<String> workFlowProfileIdentifiers;
//        private final String rootOrganizationIdentifier;
//        private final String contentLanguageIdentifier;
//        private final String organizationIdentifier;
//        private final String userProfileIdentifier;
//        private final String languageIdentifier;
//        private final String userIdentifier;
//        private final String userSurname;
//        private final String serverUrl;
//        private final String userName;
//
//        private T8JasperServerSession(T8SessionContext serverSessionContext, T8SessionContext userSessionContext)
//        {
//            this.basicServerSessionContext = serverSessionContext;
//            this.sessionParameterMap = new HashMap<>(userSessionContext.getSessionParameterMap());
//            this.workFlowProfileIdentifiers = new ArrayList<>(userSessionContext.getWorkFlowProfileIdentifiers());
//            this.rootOrganizationIdentifier = userSessionContext.getRootOrganizationIdentifier();
//            this.contentLanguageIdentifier = userSessionContext.getContentLanguageIdentifier();
//            this.organizationIdentifier = userSessionContext.getOrganizationIdentifier();
//            this.userProfileIdentifier = userSessionContext.getUserProfileIdentifier();
//            this.languageIdentifier = userSessionContext.getLanguageIdentifier();
//            this.userIdentifier = userSessionContext.getUserIdentifier();
//            this.userSurname = userSessionContext.getUserSurname();
//            this.serverUrl = userSessionContext.getServerURL();
//            this.userName = userSessionContext.getUserName();
//        }
//
//        @Override
//        public String getSessionIdentifier()
//        {
//            return this.basicServerSessionContext.getSessionIdentifier();
//        }
//
//        @Override
//        public String getUserIdentifier()
//        {
//            return this.userIdentifier;
//        }
//
//        @Override
//        public String getUserProfileIdentifier()
//        {
//            return this.userProfileIdentifier;
//        }
//
//        @Override
//        public String getLanguageIdentifier()
//        {
//            return this.languageIdentifier;
//        }
//
//        @Override
//        public String getContentLanguageIdentifier()
//        {
//            return this.contentLanguageIdentifier;
//        }
//
//        @Override
//        public String getUserName()
//        {
//            return this.userName;
//        }
//
//        @Override
//        public String getUserSurname()
//        {
//            return this.userSurname;
//        }
//
//        @Override
//        public String getOrganizationIdentifier()
//        {
//            return this.organizationIdentifier;
//        }
//
//        @Override
//        public String getRootOrganizationIdentifier()
//        {
//            return this.rootOrganizationIdentifier;
//        }
//
//        @Override
//        public String getServerURL()
//        {
//            return this.serverUrl;
//        }
//
//        @Override
//        public List<String> getWorkFlowProfileIdentifiers()
//        {
//            return Collections.unmodifiableList(this.workFlowProfileIdentifiers);
//        }
//
//        @Override
//        public Map<String, Object> getSessionParameterMap()
//        {
//            return Collections.unmodifiableMap(this.sessionParameterMap);
//        }
//
//        @Override
//        public boolean isLoggedIn()
//        {
//            throw new UnsupportedOperationException("Not supported in Jasper Server Session Context");
//        }
//
//        @Override
//        public boolean isSystemSession()
//        {
//            return true;
//        }
//
//        @Override
//        public String getSystemAgentIdentifier()
//        {
//            return this.basicServerSessionContext.getSystemAgentIdentifier();
//        }
//
//        @Override
//        public String getSystemAgentInstanceIdentifier()
//        {
//            return this.basicServerSessionContext.getSystemAgentInstanceIdentifier();
//        }
//
//        @Override
//        public boolean isAnonymousSession()
//        {
//            return false;
//        }
//
//        @Override
//        public Serializable getSessionData(String dataId)
//        {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public Serializable setSessionData(String dataId, Serializable data)
//        {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public T8PerformanceStatistics getPerformanceStatistics()
//        {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//    }
//}
