package com.pilog.t8.report.jasper.scriptlet;

import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.cache.LRUCache;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDefaultScriptlet;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8JasperScriptlet extends JRDefaultScriptlet
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8JasperScriptlet.class);

    private final LRUCache<String, T8Definition> definitions = new LRUCache<>(50);
    private TerminologyProvider terminologyProvider;

    public TerminologyProvider getTerminologyProvider(T8Context context)
    {
        if (terminologyProvider == null)
        {
            T8DataTransaction tx;

            tx = context.getServerContext().getDataManager().getCurrentSession().instantTransaction();
            terminologyProvider = ((T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER)).getTerminologyProvider();
        }
        return terminologyProvider;
    }

    public String getValueStringTerm(T8Context context, String valueString)
    {
        T8DataRecordValueStringGenerator generator;

        generator = new T8DataRecordValueStringGenerator();

        generator.setTerminologyProvider(getTerminologyProvider(context));
        generator.setLanguageID(context.getLanguageId());
        try
        {
            return generator.replaceValueStringConcepts(new StringBuffer(valueString), context.getLanguageId(), Boolean.TRUE).toString();
        }
        catch (Exception ex)
        {
            Logger.getLogger(T8JasperScriptlet.class.getName()).log(Level.SEVERE, "Failed to convert value string", ex);
            return valueString;
        }
    }

    public String getDefinitionDatumValue(T8Context context, String definitionId, String datumId)
    {
        Object definitionDatum;
        try
        {
            if (!definitions.containsKey(definitionId))definitions.put(definitionId, context.getServerContext().getDefinitionManager().getRawDefinition(context, null, definitionId));
            T8Definition definition;

            definition = definitions.get(definitionId);
            definitionDatum = definition == null ? null : definition.getDefinitionDatum(datumId);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to get datum value for definition " + definitionId + " and datum " + datumId, ex);
            return null;
        }
        return definitionDatum == null ? null : definitionDatum.toString();
    }
}
