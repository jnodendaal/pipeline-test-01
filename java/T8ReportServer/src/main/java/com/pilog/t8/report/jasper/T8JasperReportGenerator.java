package com.pilog.t8.report.jasper;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.groupby.T8GroupByDataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.report.T8ReportDescriptionScriptDefinition;
import com.pilog.t8.definition.report.jasper.T8JasperImageDefinition;
import com.pilog.t8.definition.report.jasper.T8JasperReportServerAPIHandler;
import com.pilog.t8.definition.report.jasper.T8JasperSubReportDefinition;
import com.pilog.t8.definition.report.jasper.T8MainJasperReportDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.report.T8ReportDescription;
import com.pilog.t8.report.T8ReportGenerationResult;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.utilities.strings.Strings;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

/**
 * @author Bouwer du Preez
 */
public class T8JasperReportGenerator implements T8ReportGenerator
{
    private final T8MainJasperReportDefinition definition;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;

    public T8JasperReportGenerator(T8Context context, T8MainJasperReportDefinition definition)
    {
        this.context = new T8ProjectContext(context, definition.getRootProjectId());
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definition = definition;
    }

    @Override
    public T8ReportDescription generateReportDescription(String reportIid, Map<String, Object> parameters)
    {
        try
        {
            T8ServerContextScriptDefinition scriptDefinition;

            // Get the definition of the script to use for generation of report descrioption.
            scriptDefinition = definition.getDescriptionScript();
            if (scriptDefinition != null)
            {
                Map<String, Object> outputParameters;
                String description;
                T8Script script;
                String name;

                // Execute the script.
                script = scriptDefinition.getNewScriptInstance(context);
                outputParameters = script.executeScript(parameters);
                outputParameters = T8IdentifierUtilities.stripNamespace(outputParameters);

                // Return the report description.
                name = (String)outputParameters.get(T8ReportDescriptionScriptDefinition.PARAMETER_NAME);
                description = (String)outputParameters.get(T8ReportDescriptionScriptDefinition.PARAMETER_DESCRIPTION);
                return new T8ReportDescription(name, description);
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while generating report description: " + definition, e);
        }
    }

    @Override
    public T8ReportGenerationResult generateReport(String reportIid, Map<String, Object> parameters, String fileContextId, String filepath) throws Exception
    {
        Map<String, Object> reportParameters;
        T8FileManager fileManager;
        T8DataFilter reportFilter;
        String fileContextIid;

        // Create the collection of report input parameters.
        reportParameters = parameters != null ? T8IdentifierUtilities.stripNamespace(parameters) : new HashMap<>();
        reportParameters.put("T8Context", this.context);
        reportParameters.put("T8ServerContext", this.serverContext);
        reportParameters.put("T8SessionContext", this.sessionContext);

        // Generate a file context iid to use for this operation.
        fileContextIid = T8IdentifierUtilities.createNewGUID();

        // Open a file context to which the report will be exported.
        fileManager = serverContext.getFileManager();
        fileManager.openFileContext(context, fileContextIid, fileContextId, new T8Minute(30));

        try
        {
            T8JasperReportDataSource dataSource;
            T8ReportDescription description;
            JasperReport jasperReport;
            byte[] binaryReport;
            String reportName;

            // Evaluate the name of the report.
            description = generateReportDescription(reportIid, parameters);
            reportName = description.getName();
            if (Strings.trimToNull(reportName) == null) throw new Exception("Invalid name defined for report: " + definition);

            // Read the report in binary form from the definition and create the jasper report object.
            binaryReport = definition.getReport();
            jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(binaryReport));

            // Build the report filter and add it to the report parameters
            reportFilter = buildReportFilter(definition, reportParameters);
            reportParameters.put("ReportFilter", reportFilter);

            // Set Report Images.
            if (definition.getReportImages() != null)
            {
                for (T8JasperImageDefinition imageDefinition : definition.getReportImages())
                {
                    reportParameters.put(imageDefinition.getReportParameterName(), imageDefinition.getImage().getBufferedImage());
                }
            }

            //Set the report sub reports.
            if (definition.getSubReports() != null)
            {
                for (T8JasperSubReportDefinition subReportDefinition : definition.getSubReports())
                {
                    try (ByteArrayInputStream inputStream = new ByteArrayInputStream(subReportDefinition.getReport()))
                    {
                        JasperReport subReport = JasperCompileManager.compileReport(inputStream);
                        reportParameters.put(subReportDefinition.getReportParameterName(), subReport);
                        reportParameters.put(subReportDefinition.getReportParameterName() + "_EID", subReportDefinition.getReportDataEntityIdentifier());
                    }
                }
            }

            // Create the report data source.
            dataSource = new T8JasperReportDataSource(context, definition.getReportDataEntityIdentifier(), reportFilter);

            // Create the report print.
            if (definition.isSwapfileEnabled())
            {
                OutputStream reportOutputStream = null;
                InputStream reportInputStream = null;
                OutputStream pdfOutputStream = null;

                try
                {
                    JRSwapFileVirtualizer virtualizer;
                    String tempFilepath;
                    String reportFilepath;
                    String pdfFilepath;

                    // Set the file paths to be used during generation.
                    if (filepath != null)
                    {
                        tempFilepath = filepath + File.separator + reportName + ".tmp";
                        reportFilepath = filepath + File.separator + reportName + ".rpt";
                        pdfFilepath = filepath + File.separator + reportName + ".pdf";
                    }
                    else
                    {
                        tempFilepath = reportName + ".tmp";
                        reportFilepath = reportName + ".rpt";
                        pdfFilepath = reportName + ".pdf";
                    }

                    // Create the virtualizer to prevent out of memory issues.
                    virtualizer = new JRSwapFileVirtualizer(50, new JRSwapFile(fileManager.getFile(context, fileContextIid, tempFilepath).getAbsolutePath(), 1024 * 8, 50));
                    reportParameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

                    // Write the report data to a report path.
                    reportOutputStream = fileManager.getFileOutputStream(context, fileContextIid, reportFilepath, false);
                    JasperFillManager.fillReportToStream(jasperReport, reportOutputStream, reportParameters, dataSource);
                    reportOutputStream.close();

                    // We always make it PDF to be downloaded, otherwise the report is lost.
                    reportInputStream = fileManager.getFileInputStream(context, fileContextIid, reportFilepath);
                    pdfOutputStream = fileManager.getFileOutputStream(context, fileContextIid, pdfFilepath, true);
                    JasperExportManager.exportReportToPdfStream(reportInputStream, pdfOutputStream);

                    // Release resources used by swapfile.
                    virtualizer.cleanup();

                    // Return the final result.
                    return new T8ReportGenerationResult(reportIid, fileContextId, pdfFilepath);
                }
                finally
                {
                    if (reportOutputStream != null) reportOutputStream.close();
                    if (reportInputStream != null) reportInputStream.close();
                    if (pdfOutputStream != null) pdfOutputStream.close();
                }
            }
            else
            {
                OutputStream pdfOutputStream = null;

                try
                {
                    String pdfFilepath;
                    JasperPrint print;

                    // Set the file path to be used during generation.
                    if (filepath != null)
                    {
                        pdfFilepath = filepath + File.separator + reportName + ".pdf";
                    }
                    else
                    {
                        pdfFilepath = reportName + ".pdf";
                    }

                    // File the report and export the result to pdf.
                    print = JasperFillManager.fillReport(jasperReport, reportParameters, dataSource);
                    pdfOutputStream = fileManager.getFileOutputStream(context, fileContextIid, pdfFilepath, true);
                    JasperExportManager.exportReportToPdfStream(print, pdfOutputStream);

                    // Return the final result.
                    return new T8ReportGenerationResult(reportIid, fileContextId, pdfFilepath);
                }
                finally
                {
                    if (pdfOutputStream != null) pdfOutputStream.close();
                }
            }
        }
        finally
        {
            fileManager.closeFileContext(context, fileContextIid);
        }
    }

    /**
     * Builds a combined filter, using the preset filter, or a default
     * filter, together with the filter which may have been passed from the
     * caller.
     *
     * @param mainReportDefinition The {@code T8MainJasperReportDefinition}
     *      containing the preset data filter, if any
     * @param reportParameters The {@code Map} of operation parameters
     *      which contain the caller filter, if any
     *
     * @return The {@code T8DataFilter} to be used for the report
     */
    private T8DataFilter buildReportFilter(T8MainJasperReportDefinition mainReportDefinition, Map<String, Object> reportParameters)
    {
        T8DataFilter reportFilter;
        T8DataFilter inputFilter;

        // Create the report filter.
        if (mainReportDefinition.getReportDataFilter() == null)
        {
            reportFilter = new T8GroupByDataFilter(mainReportDefinition.getReportDataEntityIdentifier());
        }
        else reportFilter = mainReportDefinition.getReportDataFilter().getNewDataFilterInstance(context, reportParameters);

        // Add any filter that came from the caller
        inputFilter = (T8DataFilter) reportParameters.get(T8JasperReportServerAPIHandler.PARAMETER_REPORT_ALTERNATE_FILTER);
        if (inputFilter != null)
        {
            reportFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, inputFilter);
        }

        return reportFilter;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public void cancel()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
