package com.pilog.t8.report.leadtime.jasper;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.security.T8Context;
import net.sf.jasperreports.engine.JRDefaultScriptlet;

/**
 * @author Gavin Boshoff
 */
public class T8LeadTimeJasperScriptlet extends JRDefaultScriptlet
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8LeadTimeJasperScriptlet.class);

    private T8OperationalHoursCalculator getOperationalHoursCalculator(T8Context context)
    {
        try
        {
            return context.getServerContext().getConfigurationManager().getOperationalHoursCalculator(context);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while resolving operational hours calculator.", e);
        }
    }

    public Long getNormalHoursMillis(T8Context context, Long startTime, Long endTime)
    {
        return getOperationalHoursCalculator(context).calculateTimeDifference(startTime, endTime == null ? System.currentTimeMillis() : endTime);
    }

    public Long getBusinessHoursMillis(T8Context context, Long startTime, Long endTime)
    {
        return getOperationalHoursCalculator(context).calculateOperationalTimeDifference(startTime, endTime == null || endTime < 1 ? System.currentTimeMillis() : endTime);
    }

    public String getHumanReadableString(T8Context context, Long intervalInMillis)
    {
        return getOperationalHoursCalculator(context).prettyPrintDuration(intervalInMillis);
    }
}
