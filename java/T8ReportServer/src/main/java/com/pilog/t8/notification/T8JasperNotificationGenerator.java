package com.pilog.t8.notification;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.notification.T8JasperReportNotificationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8JasperNotificationGenerator implements T8NotificationGenerator
{
    private final T8JasperReportNotificationDefinition definition;

    public T8JasperNotificationGenerator(T8JasperReportNotificationDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public List<T8Notification> generateNotifications(T8Context context, Map<String, ? extends Object> inputParameters) throws Exception
    {
        T8Notification notification;
        String identifier;

        identifier = this.definition.getIdentifier();

        notification = new T8Notification(identifier, definition.getRootProjectId(), T8IdentifierUtilities.createNewGUID());
        notification.setSenderDetail(context.getSessionContext());
        notification.setUserIdentifier(context.getUserId());
        notification.setParameters(inputParameters);
        return ArrayLists.typeSafeList(notification);
    }
}