package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8ReportServerVersion
{
    public final static String VERSION = "60";
}
