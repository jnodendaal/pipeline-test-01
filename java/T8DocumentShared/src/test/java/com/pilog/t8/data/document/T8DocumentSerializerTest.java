package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.value.AttachmentListRequirement;
import com.pilog.t8.data.document.datarequirement.value.BooleanRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateTimeRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.datarequirement.value.IntegerRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.datarequirement.value.RealRequirement;
import com.pilog.t8.data.document.datarequirement.value.StringRequirement;
import com.pilog.t8.data.document.datarequirement.value.TextRequirement;
import com.pilog.t8.data.document.datarequirement.value.TimeRequirement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentSerializerTest
{

    public T8DocumentSerializerTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of serializeToJSON method, of class T8DocumentSerializer.
     */
    @Test
    public void testSerializeToJSON_DataRecord()
    {
        //System.out.println("serializeToJSON");
        //DataRecord dataRecord = null;
        //JsonValue expResult = null;
        //JsonValue result = T8DocumentSerializer.serializeToJSON(dataRecord);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deserializeDataRecordFromJSON method, of class T8DocumentSerializer.
     */
    @Test
    public void testDeserializeDataRecordFromJSON_String()
    {
        //System.out.println("deserializeDataRecordFromJSON");
        //String jsonString = "";
        //DataRecord expResult = null;
        //DataRecord result = T8DocumentSerializer.deserializeDataRecordFromJSON(jsonString);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    private DataRecord createTestRecordType1(String recordID)
    {
        DataRecord record;
        DataRequirementInstance drInstance;

        drInstance = createTestDRInstanceType1();
        record = new DataRecord(drInstance);
        record.setID(recordID);
        return record;
    }

    private DataRecord createTestRecordType2(String recordID)
    {
        return null;
    }

    private DataRequirementInstance createTestDRInstanceType1()
    {
        DataRequirementInstance drInstance;
        DataRequirement dataRequirement;
        PropertyRequirement propertyRequirement;
        SectionRequirement sectionRequirement;

        dataRequirement = new DataRequirement("3FE3D2CBEB6549D0AD0691B2957AE7BE");
        sectionRequirement = new SectionRequirement("1D47CBB39D27446983AD293CBF9E7ADB");
        dataRequirement.addSectionRequirement(sectionRequirement);

        // Add Property 1.
        propertyRequirement = new PropertyRequirement("2BC928D6279448EB9AD2EA348FABDA0B");
        propertyRequirement.setValueRequirement(createAttachmentListRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 2.
        propertyRequirement = new PropertyRequirement("1D098D6640CF4006B8B5DC66E894BDA3");
        propertyRequirement.setValueRequirement(createBooleanRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 3.
        propertyRequirement = new PropertyRequirement("43F08A6398CB4A1FBF72E2E3AF2394BD");
        propertyRequirement.setValueRequirement(createCompositeRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 4.
        propertyRequirement = new PropertyRequirement("302ADE27359E440F9EAC716089EE0BF9");
        propertyRequirement.setValueRequirement(createControlledConceptRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 5.
        propertyRequirement = new PropertyRequirement("01B919403BF2408596E36C404BE44DAF");
        propertyRequirement.setValueRequirement(createDateTimeRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 6.
        propertyRequirement = new PropertyRequirement("31A7C95CA8F244F4AD516F29FDFA36F6");
        propertyRequirement.setValueRequirement(createDocumentReferenceRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 7.
        propertyRequirement = new PropertyRequirement("6CB5A8298BAF43749823A7CA44CDA045");
        propertyRequirement.setValueRequirement(createIntegerRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 8.
        propertyRequirement = new PropertyRequirement("E51B78CE72564846BAC5E816A98C0C3C");
        propertyRequirement.setValueRequirement(createMeasuredNumberRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 9.
        propertyRequirement = new PropertyRequirement("937256E73A7845E7B94AC38B840BE76F");
        propertyRequirement.setValueRequirement(createMeasuredRangeRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 10.
        propertyRequirement = new PropertyRequirement("DFC4AA62E19E48988162AE4E18CDF65F");
        propertyRequirement.setValueRequirement(createRealRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 11.
        propertyRequirement = new PropertyRequirement("CF383AE7CD6543F49AD66EB7CC627D41");
        propertyRequirement.setValueRequirement(createStringRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 12.
        propertyRequirement = new PropertyRequirement("9A773864A3574F9C8B913AD798CF7048");
        propertyRequirement.setValueRequirement(createTextRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        // Add Property 13.
        propertyRequirement = new PropertyRequirement("00C5FC9FD2994F4C85AE9360B8D8179F");
        propertyRequirement.setValueRequirement(createTimeRequirement());
        sectionRequirement.addPropertyRequirement(propertyRequirement);

        drInstance = new DataRequirementInstance("4F04BF884531403F93D2AED76CFE1C90", dataRequirement);
        drInstance.setIndependent(true);
        return drInstance;
    }

    private AttachmentListRequirement createAttachmentListRequirement()
    {
        AttachmentListRequirement requirement;

        requirement = new AttachmentListRequirement();
        return requirement;
    }

    private BooleanRequirement createBooleanRequirement()
    {
        return new BooleanRequirement();
    }

    private CompositeRequirement createCompositeRequirement()
    {
        CompositeRequirement requirement;

        requirement = new CompositeRequirement();
        return requirement;
    }

    private ControlledConceptRequirement createControlledConceptRequirement()
    {
        ControlledConceptRequirement requirement;

        requirement = new ControlledConceptRequirement();
        return requirement;
    }

    private DateTimeRequirement createDateTimeRequirement()
    {
        return new DateTimeRequirement();
    }

    private DocumentReferenceRequirement createDocumentReferenceRequirement()
    {
        DocumentReferenceRequirement requirement;

        requirement = new DocumentReferenceRequirement();
        return requirement;
    }

    private IntegerRequirement createIntegerRequirement()
    {
        return new IntegerRequirement();
    }

    private MeasuredNumberRequirement createMeasuredNumberRequirement()
    {
        MeasuredNumberRequirement requirement;

        requirement = new MeasuredNumberRequirement();
        return requirement;
    }

    private MeasuredRangeRequirement createMeasuredRangeRequirement()
    {
        MeasuredRangeRequirement requirement;

        requirement = new MeasuredRangeRequirement();
        return requirement;
    }

    private RealRequirement createRealRequirement()
    {
        return new RealRequirement();
    }

    private StringRequirement createStringRequirement()
    {
        return new StringRequirement();
    }

    private TextRequirement createTextRequirement()
    {
        return new TextRequirement();
    }

    private TimeRequirement createTimeRequirement()
    {
        return new TimeRequirement();
    }
}
