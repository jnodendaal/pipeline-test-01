package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentSharedVersion
{
    public final static String VERSION = "1090";
}
