/**
 * Created on 20 Aug 2015, 2:10:05 PM
 */
package com.pilog.t8.data.document.datastring;

import java.io.Serializable;

/**
 * @author Gavin Boshoff
 */
public class T8DataStringType implements Serializable
{
    private static final long serialVersionUID = -5034267295873981720L;

    private String rendererAbbreviation;
    private String rendererDescription;
    private String rendererTerm;
    private boolean autoRender;
    private String languageID;

    public T8DataStringType() {}

    public String getRendererAbbreviation()
    {
        return this.rendererAbbreviation;
    }

    public void setRendererAbbreviation(String rendererAbbreviation)
    {
        this.rendererAbbreviation = rendererAbbreviation;
    }

    public String getRendererDescription()
    {
        return this.rendererDescription;
    }

    public void setRendererDescription(String rendererDescription)
    {
        this.rendererDescription = rendererDescription;
    }

    public String getRendererTerm()
    {
        return this.rendererTerm;
    }

    public void setRendererTerm(String rendererTerm)
    {
        this.rendererTerm = rendererTerm;
    }

    public String getLanguageID()
    {
        return this.languageID;
    }

    public void setLanguageID(String languageID)
    {
        this.languageID = languageID;
    }

    public boolean isAutoRender()
    {
        return this.autoRender;
    }

    public void setAutoRender(boolean autoRender)
    {
        this.autoRender = autoRender;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8RenderingType{");
        toStringBuilder.append("rendererAbbreviation=").append(this.rendererAbbreviation);
        toStringBuilder.append(",rendererDescription=").append(this.rendererDescription);
        toStringBuilder.append(",rendererTerm=").append(this.rendererTerm);
        toStringBuilder.append(",autoRender=").append(this.autoRender);
        toStringBuilder.append(",languageID=").append(this.languageID);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}