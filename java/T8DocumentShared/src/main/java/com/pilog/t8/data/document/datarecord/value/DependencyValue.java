package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.T8HierarchicalSetType;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.ontology.T8ConceptGraphPath;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public abstract class DependencyValue extends RecordValue
{
    public DependencyValue(ValueRequirement valueRequirement, String value)
    {
        super(valueRequirement, value);
    }

    public DependencyValue(String valueId, ValueRequirement valueRequirement, String value)
    {
        super(valueId, valueRequirement, value);
    }

    public DependencyValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public abstract List<String> findDependentConceptIdList(RecordValue parentValue, DataRecordProvider recordProvider);

    /**
     * Returns all RecordValue objects in the current document object model that
     * are dependent on this record value.
     * @return All RecordValue objects in the current document object model that
     * are dependent on this record value.
     */
    public List<DependencyValue> getDependentValues()
    {
        DataRecord parentDataRecord;

        parentDataRecord = getParentDataRecord();

        return (parentDataRecord != null) ? parentDataRecord.getDependentValues(this, true) : new ArrayList<>();
    }

    /**
     * Returns the DataDependency that links this record value to the specified
     * parent value.  If not such a dependency exists, null is returned.
     * @param parentValue The parent value to which the dependency will be
     * determined.
     * @return The DataDependency that links this record value to the specified
     * parent value.
     */
    public DataDependency getDataDependencyOn(RecordValue parentValue)
    {
        for (DataDependency dataDependency : valueRequirement.getDataDependencies())
        {
            if (dataDependency.isDependencyParentValue(parentValue, this))
            {
                return dataDependency;
            }
        }

        return null;
    }

    /**
     * Returns true if this record value is dependent on the specified parent
     * record value.
     * @param parentValue The record value to check as a parent on which this
     * record value is dependent.
     * @return True if this record value is dependent on the specified parent
     * record value.
     */
    public boolean isDependentOn(RecordValue parentValue)
    {
        for (DataDependency dataDependency : valueRequirement.getDataDependencies())
        {
            if (dataDependency.isDependencyParentValue(parentValue, this))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Sets all values in the current document object model that this value is
     * dependent on by using the dependency path supplied.
     * @param path The path of concept ID's to set on the document object model.
     */
    public void setDependencyValues(T8ConceptGraphPath path)
    {
        List<DataDependency> dataDependencies;
        String conceptID;

        conceptID = path.getLastConceptId();
        dataDependencies = valueRequirement.getDataDependencies();
        if (!dataDependencies.isEmpty())
        {
            getParentDataRecord().setDependencyValues(dataDependencies, path.getParentPath(conceptID));
        }
    }

    /**
     * Returns all concept ID's from the current document object model that this
     * value is dependent on.
     * @param recordProvider The record provider to use when one of the
     * documents involved in a dependency cannot be found within the document
     * model.
     * @return All concept ID's from the current document object model that this
     * value is dependent on.
     */
    public Map<DataDependency, String> getDependencyParentConceptIds(DataRecordProvider recordProvider)
    {
        Map<DataDependency, String> dependencyConceptIds;

        dependencyConceptIds = new HashMap<>();
        for (DataDependency dataDependency : valueRequirement.getDataDependencies())
        {
            String dependencyConceptId;

            // Get the dependency concept ID and add it to the result map.
            dependencyConceptId = getDependencyParentConceptId(recordProvider, dataDependency);
            dependencyConceptIds.put(dataDependency, dependencyConceptId);
        }

        return dependencyConceptIds;
    }

    /**
     * This method attempts to find the required dependency value matching the
     * supplied data dependency requirement.
     * @param recordProvider
     * @param dataDependency The dependency requirement for which to retrieve
     * the required value.
     * @param dependantValue The dependant value for which the dependency will
     * be determined.
     * @return The concept ID matching the dependency requirement.  Returns null
     * if the dependency value does not exist.
     */
    public String getDependencyParentConceptId(DataRecordProvider recordProvider, DataDependency dataDependency)
    {
        Value dependencyParent;

        dependencyParent = getDependencyParent(recordProvider, dataDependency);
        if (dependencyParent instanceof DependencyValue)
        {
            return ((DependencyValue)dependencyParent).findDependencyConceptId(((DependencyValue)dependencyParent), this, recordProvider);
        }
        else if (dependencyParent instanceof DataRecord)
        {
            return ((DataRecord)dependencyParent).getDataRequirementInstanceID();
        }
        else return null;
    }

    /**
     * This method attempts to find the content object matching the supplied data dependency requirement,
     * i.e. this method will return the object from the current model on which the this value is dependent
     * as specified by the input dataDependency.
     * @param recordProvider
     * @param dataDependency The dependency requirement for which to retrieve
     * the required value.
     * @return The content object (DataRecord/RecordValue) that is the parent of the dependency relationship, specified.
     */
    public Value getDependencyParent(DataRecordProvider recordProvider, DataDependency dataDependency)
    {
        DataRecord requiredDocument;
        DataRecord parentRecord;

        parentRecord = getParentDataRecord();
        requiredDocument = null;
        if (dataDependency.isDrInstanceSpecified())
        {
            Set<DataRecord> requiredDocuments;

            // Find all documents matching the Data Requirement Instance ID specified by the dependency requirement.
            requiredDocuments = parentRecord.findDataRecordsByDrInstance(T8HierarchicalSetType.LINE, dataDependency.getDrInstanceId());
            if (requiredDocuments.size() > 1) throw new RuntimeException("DR Instance dependency refers to multiple ancestor records: " + dataDependency.getDrInstanceId());
            else if (requiredDocuments.size() > 0) requiredDocument = requiredDocuments.iterator().next();
        }
        else if (dataDependency.isDrSpecified())
        {
            Set<DataRecord> requiredDocuments;

            // Find all documents matching the Data Requirement ID specified by the dependency requirement.
            requiredDocuments = parentRecord.findDataRecordsByDr(T8HierarchicalSetType.LINE, dataDependency.getDrId());
            if (requiredDocuments.size() > 1) throw new RuntimeException("DR dependency refers to multiple ancestor records: " + dataDependency.getDrId());
            else if (requiredDocuments.size() > 0) requiredDocument = requiredDocuments.iterator().next();
        }
        else // Property dependency.
        {
            // The dependency is on this record.
            requiredDocument = parentRecord;
        }

        // Now use the required documents and find the required property values in each.
        if (requiredDocument != null)
        {
            if (dataDependency.isFieldSpecified())
            {
                RecordProperty requiredProperty;

                // Get the required property from the section.
                requiredProperty = requiredDocument.getRecordProperty(dataDependency.getPropertyId());
                if (requiredProperty != null)
                {
                    RecordValue requiredField;
                    DependencyValue dependencyValue;

                    requiredField = requiredProperty.getField(dataDependency.getFieldId());
                    dependencyValue = (DependencyValue)(requiredField != null ? requiredField.getFirstSubValue() : null);
                    return dependencyValue;
                }
                else return null;
            }
            else if (dataDependency.isPropertySpecified())
            {
                RecordProperty requiredProperty;
                DependencyValue dependencyValue;

                // Get the required property from the section.
                requiredProperty = requiredDocument.getRecordProperty(dataDependency.getPropertyId());
                dependencyValue = (DependencyValue)(requiredProperty != null ? requiredProperty.getRecordValue() : null);
                return dependencyValue;
            }
            else
            {
                return requiredDocument;
            }
        }
        else return null;
    }

    /**
     * Returns the concept id from the specified parent value on which this
     * record value is dependent.
     * @param parentValue The parent record value from which to fetch the
     * concept ID.
     * @param dependentValue The dependant value for which to determine the
     * dependency concept ID.
     * @param recordProvider The record provider to use in case a required
     * document cannot be found in the current object model.
     * @return The concept id from the specified parent value on which this
     * record value is dependent.
     */
    public String findDependencyConceptId(RecordValue parentValue, RecordValue dependentValue, DataRecordProvider recordProvider)
    {
        List<String> pathRecordIdList;
        DataRecord dataRecord;

        dataRecord = dependentValue.getParentDataRecord();
        pathRecordIdList = dataRecord.getPathRecordIDList();
        if (parentValue instanceof DocumentReferenceList)
        {
            DocumentReferenceList documentReferenceList;

            documentReferenceList = (DocumentReferenceList)parentValue;
            for (String recordId : documentReferenceList.getReferenceIds())
            {
                DataRecord localDocument;

                // Make sure the Record is part of the path to this record.
                if (pathRecordIdList.contains(recordId))
                {
                    // First try to find the Record from the record graph.
                    localDocument = dataRecord.findDataRecord(recordId);
                    if (localDocument != null)
                    {
                        return localDocument.getDataRequirementInstance().getConceptID();
                    }
                    else
                    {
                        // We couldn't find the document within the local context so we have to retrieve.
                        return recordProvider.getRecordDrInstanceId(recordId);
                    }
                }
                else if (pathRecordIdList.contains(parentValue.getParentDataRecord().getID()))
                {
                    // This is a special case, where only one reference value is available in this property and the parent of the reference is part of the lineage.
                    if (documentReferenceList.getReferenceCount() == 1)
                    {
                        // First try to find the Record from the record graph.
                        localDocument = dataRecord.findDataRecord(recordId);
                        if (localDocument != null)
                        {
                            return localDocument.getDataRequirementInstance().getConceptID();
                        }
                        else
                        {
                            // We couldn't find the document within the local context so we have to retrieve.
                            return recordProvider.getRecordDrInstanceId(recordId);
                        }
                    }
                }
            }

            // No applicable reference found.
            return null;
        }
        else if (parentValue instanceof ControlledConcept)
        {
            return ((ControlledConcept)parentValue).getConceptId();
        }
        else return null;
    }
}
