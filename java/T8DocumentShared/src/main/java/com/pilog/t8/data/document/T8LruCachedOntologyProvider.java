package com.pilog.t8.data.document;

import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8LruCachedOntologyProvider implements OntologyProvider
{
    private final OntologyProvider ontologyProvider;

    public T8LruCachedOntologyProvider(OntologyProvider ontologyProvider, int size)
    {
        this.ontologyProvider = ontologyProvider;
    }

    @Override
    public List<T8OntologyCode> getOntologyCodes(String conceptId)
    {
        return ontologyProvider.getOntologyCodes(conceptId);
    }

    @Override
    public List<T8OntologyLink> getOntologyLinks(String conceptId)
    {
        return ontologyProvider.getOntologyLinks(conceptId);
    }

    @Override
    public List<T8OntologyLink> getOntologyLinks(List<String> osIdList, String conceptId)
    {
        return ontologyProvider.getOntologyLinks(osIdList, conceptId);
    }

    @Override
    public T8OntologyConcept getOntologyConcept(String conceptId)
    {
        return ontologyProvider.getOntologyConcept(conceptId);
    }

    @Override
    public T8DataIterator<String> getConceptIDIterator(String ocId, boolean includeDescendantClasses)
    {
        return ontologyProvider.getConceptIDIterator(ocId, includeDescendantClasses);
    }
}
