package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.math.BigDecimal;

/**
 * @author Bouwer du Preez
 */
public class RealRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public RealRequirement()
    {
        super(RequirementType.REAL_TYPE);
    }

    @Override
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public String getFormatPattern()
    {
        return (String)getAttribute(RequirementAttribute.FORMAT_PATTERN.toString());
    }

    public void setFormatPattern(String formatPattern)
    {
        setAttribute(RequirementAttribute.FORMAT_PATTERN.toString(), formatPattern);
    }

    public BigDecimal getMinimumValue()
    {
        return (BigDecimal)getAttribute(RequirementAttribute.MINIMUM_NUMERIC_VALUE.toString());
    }

    public BigDecimal getMaximumValue()
    {
        return (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_NUMERIC_VALUE.toString());
    }
}
