package com.pilog.t8.definition.data.document.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.service.T8Service;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.definition.service.T8ServiceOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordServiceDefinition extends T8ServiceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SERVICE_DATA_RECORD";
    public static final String DISPLAY_NAME = "Data Record Service";
    public static final String DESCRIPTION = "A service that processes asynchronous record updates.";
    public enum Datum {ROOT_ORG_ID}
    // -------- Definition Meta-Data -------- //

    public T8DataRecordServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.ROOT_ORG_ID.toString(), "Root Organization Id", "The id of the root organization to which this service is applicable."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    @Override
    public T8Service getNewServiceInstance(T8ServerContext serverContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.T8DataRecordService").getConstructor(T8ServerContext.class, this.getClass());
            return (T8Service)constructor.newInstance(serverContext, this);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Service.", e);
        }
    }

    @Override
    public ArrayList<T8ServiceOperationDefinition> getOperationDefinitions()
    {
        return T8DataRecordServiceResources.getOperationDefinitions();
    }

    public String getRootOrgId()
    {
        return getDefinitionDatum(Datum.ROOT_ORG_ID);
    }

    public void setRootOrgId(String orgId)
    {
        setDefinitionDatum(Datum.ROOT_ORG_ID, orgId);
    }
}
