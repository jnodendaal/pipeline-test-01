package com.pilog.t8.definition.data.source.datarecord;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.source.datarecord.T8AttachmentHandler;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAttachmentHandlerDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_ATTACHMENT_HANDLER";
    public static final String GROUP_IDENTIFIER = "@DG_ATTACHMENT_HANDLER";
    public static final String GROUP_NAME = "Attachment Handlers";
    public static final String GROUP_DESCRIPTION = "Configurations of services handling the storage of data record attachments.";
    public static final String DISPLAY_NAME = "Attachment Handler";
    public static final String STORAGE_PATH = "/attachment_handlers";
    public static final String DESCRIPTION = "A definition used to instantiate an attachment handler capable of storing attachment files.";
    public static final String IDENTIFIER_PREFIX = "ATTACHMENT_HANDLER_";
    public enum Datum {CONNECTION_IDENTIFIER,
                       ATTACHMENT_TABLE_NAME,
                       ATTACHMENT_STORAGE_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DataRecordAttachmentHandlerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection this attachment handler uses."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ATTACHMENT_TABLE_NAME.toString(), "Attachment Table Name", "The name of the database table where Data Record attachment data is stored.", "DATA_RECORD_ATTACHMENT"));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ATTACHMENT_STORAGE_DEFINITIONS.toString(), "Attachment Storage",  "A definition that specifies how specific types of attachments are to be stored."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else if (Datum.ATTACHMENT_STORAGE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8AttachmentStorageDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public T8AttachmentHandler getNewAttachmentHandlerInstance(T8DataTransaction tx) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.document.datarecord.attachment.T8DataRecordAttachmentHandler").getConstructor(this.getClass(), T8DataTransaction.class);
        return (T8AttachmentHandler)constructor.newInstance(this, tx);
    }

    public String getConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString());
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString(), connectionIdentifier);
    }

    public String getAttachmentTableName()
    {
        return (String)getDefinitionDatum(Datum.ATTACHMENT_TABLE_NAME.toString());
    }

    public void setAttachmentTableName(String tableName)
    {
        setDefinitionDatum(Datum.ATTACHMENT_TABLE_NAME.toString(), tableName);
    }

    public List<T8AttachmentStorageDefinition> getAttachmentStorageDefinitions()
    {
        return (List<T8AttachmentStorageDefinition>)getDefinitionDatum(Datum.ATTACHMENT_STORAGE_DEFINITIONS.toString());
    }

    public void addAttachmentStorageDefinition(T8AttachmentStorageDefinition definition)
    {
        addSubDefinition(Datum.ATTACHMENT_STORAGE_DEFINITIONS.toString(), definition);
    }

    public void setAttachmentStorageDefinitions(List<T8AttachmentStorageDefinition> definitions)
    {
        setDefinitionDatum(Datum.ATTACHMENT_STORAGE_DEFINITIONS.toString(), definitions);
    }
}
