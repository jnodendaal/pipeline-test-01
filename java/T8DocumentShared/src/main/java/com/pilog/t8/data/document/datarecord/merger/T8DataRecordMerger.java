package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordMerger extends T8DataRecordMergeContext
{
    public enum RecordMatchType
    {
        DEFINITION,
        CHARACTERISTIC_PROPERTIES,
        RECORD_ID
    };

    public enum AttachmentMatchType
    {
        ATTACHMENT_ID,
        CHECKSUM,
        FILENAME
    };

    public enum AttachmentDetailSource
    {
        DATA_RECORD,
        DATA_RECORD_PROVIDER
    };

    public enum MergeConflictPolicy
    {
        USE_SOURCE_DATA,
        USE_DESTINATION_DATA,
        USE_SOURCE_DATA_ADD_FFT,
        USE_DESTINATION_DATA_ADD_FFT
    };

    public static final String SOURCE_RECORD_ID_ATTRIBUTE_IDENTIFIER = "SOURCE_RECORD_ID";

    public void reset();
    public Map<String, String> getRecordIdMapping();
    public Map<String, String> getAttachmentIdentifierMapping();
    public Set<DataRecord> getNewDataRecordSet();
    public Set<DataRecord> getUpdatedDataRecordSet();

    public void mergeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord);
    public DataRecord synchronizeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord);
}
