package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class UnitOfMeasureAccessLayer extends ValueAccessLayer implements ConceptAccessLayer
{
    private boolean allowNewValue;

    public UnitOfMeasureAccessLayer()
    {
        super(RequirementType.UNIT_OF_MEASURE);
        this.allowNewValue = true;
    }

    @Override
    public boolean isAllowNewValue()
    {
        return allowNewValue;
    }

    @Override
    public void setAllowNewValue(boolean allowNewValue)
    {
        this.allowNewValue = allowNewValue;
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer valueAccess)
    {
        if (valueAccess instanceof UnitOfMeasureAccessLayer)
        {
            UnitOfMeasureAccessLayer uomAccess;

            uomAccess = (UnitOfMeasureAccessLayer)valueAccess;
            if (!Objects.equals(allowNewValue, uomAccess.isAllowNewValue())) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public ValueAccessLayer copy()
    {
        UnitOfMeasureAccessLayer copy;

        copy = new UnitOfMeasureAccessLayer();
        copy.setAllowNewValue(allowNewValue);
        return copy;
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
        UnitOfMeasureAccessLayer newUomAccess;

        newUomAccess = (UnitOfMeasureAccessLayer)newAccess;
        setAllowNewValue(newUomAccess.isAllowNewValue());
    }
}
