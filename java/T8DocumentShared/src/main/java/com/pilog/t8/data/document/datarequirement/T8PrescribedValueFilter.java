package com.pilog.t8.data.document.datarequirement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8PrescribedValueFilter implements Serializable
{
    private final List<String> drIds;
    private final List<String> drIids;
    private final List<String> propertyIds;
    private final List<String> fieldIds;
    private final List<String> valueIds;

    public T8PrescribedValueFilter()
    {
        drIds = new ArrayList<String>();
        drIids = new ArrayList<String>();
        propertyIds = new ArrayList<String>();
        fieldIds = new ArrayList<String>();
        valueIds = new ArrayList<String>();
    }

    public List<String> getDrIds()
    {
        return new ArrayList<String>(drIds);
    }

    public void setDrIds(List<String> drIds)
    {
        this.drIds.clear();
        if (drIds != null) this.drIds.addAll(drIds);
    }

    public void addDrIds(String... drIds)
    {
        this.drIds.addAll(Arrays.asList(drIds));
    }

    public boolean hasDrCriteria()
    {
        return !drIds.isEmpty();
    }

    public List<String> getDrIids()
    {
        return new ArrayList<String>(drIids);
    }

    public void setDrIids(List<String> drIids)
    {
        this.drIids.clear();
        if (drIids != null) this.drIids.addAll(drIids);
    }

    public void addDrIids(String... drIids)
    {
        this.drIids.addAll(Arrays.asList(drIids));
    }

    public boolean hasDrInstanceCriteria()
    {
        return !drIids.isEmpty();
    }

    public List<String> getPropertyIds()
    {
        return new ArrayList<String>(propertyIds);
    }

    public void setPropertyIds(List<String> propertyIds)
    {
        this.propertyIds.clear();
        if (propertyIds != null) this.propertyIds.addAll(propertyIds);
    }

    public void addPropertyIds(String... propertyIds)
    {
        this.propertyIds.addAll(Arrays.asList(propertyIds));
    }

    public boolean hasPropertyCriteria()
    {
        return !propertyIds.isEmpty();
    }

    public List<String> getFieldIds()
    {
        return new ArrayList<String>(fieldIds);
    }

    public void setFieldIds(List<String> fieldIds)
    {
        this.fieldIds.clear();
        if (fieldIds != null) this.fieldIds.addAll(fieldIds);
    }

    public void addFieldIds(String... fieldIds)
    {
        this.fieldIds.addAll(Arrays.asList(fieldIds));
    }

    public boolean hasFieldCriteria()
    {
        return !fieldIds.isEmpty();
    }

    public List<String> getValueIds()
    {
        return new ArrayList<String>(valueIds);
    }

    public void setValueIds(List<String> valueIds)
    {
        this.valueIds.clear();
        if (valueIds != null) this.valueIds.addAll(valueIds);
    }

    public void addValueIds(String... valueIds)
    {
        this.valueIds.addAll(Arrays.asList(valueIds));
    }

    public boolean hasValueCriteria()
    {
        return !valueIds.isEmpty();
    }
}
