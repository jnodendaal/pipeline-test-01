package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.AttachmentDetailSource;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.AttachmentMatchType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordValueMerger;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAttachmentMergerDefinition extends T8DataRecordValueMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_VALUE_MERGER_ATTACHMENT";
    public static final String DISPLAY_NAME = "Attachment Merger";
    public static final String DESCRIPTION = "A definition that specifies merging rules applicable to Data Record Attachment values.";
    public static final String IDENTIFIER_PREFIX = "VALUE_MERGER_ATTACHMENT_";
    public enum Datum {ATTACHMENT_MATCH_TYPE,
                       ATTACHMENT_DETAIL_SOURCE,
                       ASSIGN_NEW_ATTACHMENT_IDENTIFIERS,
                       FILENAME_CONVERSION_SCRIPT_DEFINITION};
    // -------- Definition Meta-Data -------- //

    public T8DataRecordAttachmentMergerDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ATTACHMENT_MATCH_TYPE.toString(), "Attachment Match Type", "The method to use when measuring attachment equivalence during merging.", AttachmentMatchType.CHECKSUM.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ATTACHMENT_DETAIL_SOURCE.toString(), "Attachment Detail Source", "The source from which attachment details will be fetched during merging.", AttachmentDetailSource.DATA_RECORD.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString(), "Assign new Attachment Identifiers", "If enabled, all attachments that are merged by this instance will get new identifiers assigned to them.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.FILENAME_CONVERSION_SCRIPT_DEFINITION.toString(), "Filename Conversion", "The script that will be used to convert filenames of record transferred to a new destination record during the merge process."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILENAME_CONVERSION_SCRIPT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8AttachmentFilenameConversionScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.ATTACHMENT_MATCH_TYPE.toString().equals(datumIdentifier)) return createStringOptions(AttachmentMatchType.values());
        else if (Datum.ATTACHMENT_DETAIL_SOURCE.toString().equals(datumIdentifier)) return createStringOptions(AttachmentDetailSource.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }
    
    @Override
    public T8DataRecordValueMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.merger.T8DataRecordAttachmentMerger").getConstructor(T8DataRecordMergeContext.class, this.getClass());
            return (T8DataRecordValueMerger)constructor.newInstance(mergeContext, this);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Value Merger.", e);
        }
    }
    
    public AttachmentMatchType getAttachmentMatchType()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.ATTACHMENT_MATCH_TYPE.toString());
        return value != null ? AttachmentMatchType.valueOf(value) : AttachmentMatchType.CHECKSUM;
    }
    
    public void setAttachmentMatchType(AttachmentMatchType matchType)
    {
        setDefinitionDatum(Datum.ATTACHMENT_MATCH_TYPE.toString(), matchType.toString());
    }
    
    public AttachmentDetailSource getAttachmentDetailSource()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.ATTACHMENT_DETAIL_SOURCE.toString());
        return value != null ? AttachmentDetailSource.valueOf(value) : AttachmentDetailSource.DATA_RECORD;
    }
    
    public void setAttachmentDetailSource(AttachmentDetailSource source)
    {
        setDefinitionDatum(Datum.ATTACHMENT_DETAIL_SOURCE.toString(), source.toString());
    }
    
    public boolean isAssignNewAttachmentIdentifiers()
    {
        Boolean value;
        
        value = (Boolean)getDefinitionDatum(Datum.ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString());
        return value == null || value;
    }
    
    public void setAssignNewAttachmentIdentifiers(Boolean assignNewAttachmentIdentifiers)
    {
        setDefinitionDatum(Datum.ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString(), assignNewAttachmentIdentifiers);
    }
    
    public T8AttachmentFilenameConversionScriptDefinition getFilenameConversionScriptDefinition()
    {
        return (T8AttachmentFilenameConversionScriptDefinition)getDefinitionDatum(Datum.FILENAME_CONVERSION_SCRIPT_DEFINITION.toString());
    }
    
    public void setFilenameConversionScriptDefinition(T8AttachmentFilenameConversionScriptDefinition definition)
    {
        setDefinitionDatum(Datum.FILENAME_CONVERSION_SCRIPT_DEFINITION.toString(), definition);
    }
}
