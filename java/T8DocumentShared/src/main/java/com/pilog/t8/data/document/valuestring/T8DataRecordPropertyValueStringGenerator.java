package com.pilog.t8.data.document.valuestring;

/**
 * This is the value string generator used by the data record document data
 * source to generate a value string for each property persisted.
 * @author Bouwer du Preez
 */
public class T8DataRecordPropertyValueStringGenerator extends T8DataRecordValueStringGenerator
{
    public T8DataRecordPropertyValueStringGenerator()
    {
        super.setIncludePropertyConcepts(false);
        super.setIncludePropertyFFT(false);
    }
}
