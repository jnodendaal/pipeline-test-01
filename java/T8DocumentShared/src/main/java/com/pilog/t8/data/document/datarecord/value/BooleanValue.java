package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;

/**
 * @author Bouwer du Preez
 */
public class BooleanValue extends RecordValue
{
    public BooleanValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    /**
     * Returns the Boolean content of this value i.e. true/false/null.
     * @return true/false/null
     */
    public Boolean getBooleanValue()
    {
        String booleanValue;

        booleanValue = getValue();
        return booleanValue != null ? Boolean.parseBoolean(getValue()) : null;
    }

    /**
     * Sets the Boolean content of this value i.e. true/false/null.
     * @param value true/false/null
     */
    public void setBooleanValue(Boolean value)
    {
        setValue(value != null ? value.toString() : null);
    }

    @Override
    public BooleanValueContent getContent()
    {
        BooleanValueContent content;

        content = new BooleanValueContent();
        content.setValue(getValue());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        BooleanValueContent content;
        String contentValue;

        content = (BooleanValueContent)valueContent;
        contentValue = content.getValue();
        setBooleanValue(contentValue != null ? Boolean.parseBoolean(contentValue) : null);
    }
}
