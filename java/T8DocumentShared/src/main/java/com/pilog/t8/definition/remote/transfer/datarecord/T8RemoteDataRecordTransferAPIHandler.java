package com.pilog.t8.definition.remote.transfer.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Hennie Brink
 */
public class T8RemoteDataRecordTransferAPIHandler implements T8DefinitionResource
{
    // TODO: GBO - Fix this up, using the JSON capability
    public static final String OPERATION_REMOTE_TRANSFER_DATA_RECORDS = "@OS_REMOTE_TRANSFER_DATA_RECORDS";

    public static final String PARAMETER_REMOTE_CONNECTION_IDENTIFIER = "$P_REMOTE_CONNECTION_IDENTIFIER";
    public static final String PARAMETER_REMOTE_FILE_CONTEXT_IDENTIFIER = "$P_REMOTE_FILE_CONTEXT_IDENTIFIER";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_RECORD_ID_FIELD_IDENTIFIER = "$P_RECORD_ID_FIELD_IDENTIFIER";
    public static final String PARAMETER_THREAD_COUNT = "$P_THREAD_COUNT";
    public static final String PARAMETER_CALL_BACK_OPERATION_IDENTIFIER = "$P_CALL_BACK_OPERATION_IDENTIFIER";
    public static final String PARAMETER_CALL_BACK_RECORD_ID_PARAMETER_IDENTIFIER = "$P_CALL_BACK_RECORD_ID_PARAMETER_IDENTIFIER";
    public static final String PARAMETER_INCLUDE_RECORD_ONTOLOGY = "$P_INCLUDE_RECORD_ONTOLOGY";
    public static final String PARAMETER_INCLUDE_SUB_RECORDS = "$P_INCLUDE_SUB_RECORDS";
    public static final String PARAMETER_REMOTE_OPERATION_IDENTIFIER = "$P_REMOTE_OPERATION_IDENTIFIER";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;

            definition = new T8JavaServerOperationDefinition(OPERATION_REMOTE_TRANSFER_DATA_RECORDS);
            definition.setMetaDescription("Transfer Data Records To Remote Server");
            definition.setClassName("com.pilog.t8.operation.remote.transfer.datarecord.T8RemoteDataRecordTransferOperation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REMOTE_CONNECTION_IDENTIFIER, "Remote Connection Identifier", "The report connection to which the web service call will be made.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REMOTE_FILE_CONTEXT_IDENTIFIER, "Remote File Context Identifier", "The file context identifier that will be used to transfer local attachments to.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_FILTER, "Data Filter", "The data filter that will be used to filter the data records that should be transfered.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID_FIELD_IDENTIFIER, "Record ID Field Identifier", "The field identifier of the record id field that will be used.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_THREAD_COUNT, "Thread Count", "The amount of threads that will be used to perform the transfer.", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CALL_BACK_OPERATION_IDENTIFIER, "Call Back Operation Identifier", "The identifier of the operation that will be call when a record has been transfered.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CALL_BACK_RECORD_ID_PARAMETER_IDENTIFIER, "Call Back Operation Data Record Parameter Identifier", "The public identifier of the parameter on the callback operation that will be used to provided the transfered record.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_RECORD_ONTOLOGY, "Include Record Ontology", "If the record ontology should be included in the transfer.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_SUB_RECORDS, "Include Sub Records", "If sub records should be included in the transfer.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REMOTE_OPERATION_IDENTIFIER, "Remote Operation Identifier", "The server operation identifier to be invoked on the remote system.", T8DataType.DEFINITION_IDENTIFIER, false));
            definitions.add(definition);
        }

        return definitions;
    }
}
