package com.pilog.t8.data.document.datarecord.match;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8RecordMatchCheck implements Serializable
{
    private String id;
    private String name;
    private String description;
    private String dataExpression;
    private T8RecordMatchCheckType type;
    private double percentage;
    private T8RecordMatchCase matchCase;

    public enum T8RecordMatchCheckType
    {
        CORRESPONDING, // Value lists obtained from record1 and record2 must have equals size.  Values at each index in list1 will be compared to the corresponding value at the same index in list2.
        ANY, // Value lists obtained from record1 and record2 do not need to be the same size.  All values from list1 will be compared to all values from list2, with any match counting towards the match percentage.
        NONE // Value lists obtained from record1 and record2 do not need to be the same size.  All values from list1 will be compared to all values from list2, with any match immediately eliminating the record match.
    };

    public T8RecordMatchCheck(String id)
    {
        this.id = id;
        this.type = T8RecordMatchCheckType.CORRESPONDING;
    }

    void setParentMatchCase(T8RecordMatchCase matchCase)
    {
        this.matchCase = matchCase;
    }

    public T8RecordMatchCase getMatchCase()
    {
        return matchCase;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDataExpression()
    {
        return dataExpression;
    }

    public void setDataExpression(String dataExpression)
    {
        this.dataExpression = dataExpression;
    }

    public int getIndex()
    {
        return matchCase != null ? matchCase.getCheckIndex(this) : -1;
    }

    public int incrementIndex()
    {
        if (matchCase != null)
        {
            return matchCase.incrementCheckIndex(this);
        }
        else return -1;
    }

    public int decrementIndex()
    {
        if (matchCase != null)
        {
            return matchCase.decrementCheckIndex(this);
        }
        else return -1;
    }

    public T8RecordMatchCheckType getType()
    {
        return type;
    }

    public void setType(T8RecordMatchCheckType type)
    {
        this.type = type;
    }

    public void setType(String typeId)
    {
        this.type = T8RecordMatchCheckType.valueOf(typeId);
    }

    public double getPercentage()
    {
        return percentage;
    }

    public void setPercentage(double percentage)
    {
        this.percentage = percentage;
    }
}
