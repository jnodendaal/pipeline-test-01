package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyCode implements Serializable
{
    private String id;
    private String codeTypeId;
    private String conceptId;
    private String code;
    private boolean primary;
    private boolean standard;
    private boolean active;

    public T8OntologyCode(String id, String conceptId, String codeTypeId, String code, boolean primary, boolean standard, boolean active)
    {
        this.id = id;
        this.codeTypeId = codeTypeId;
        this.conceptId = conceptId;
        this.code = code;
        this.primary = primary;
        this.standard = standard;
        this.active = active;
    }

    public T8OntologyCode(String id, String conceptId, String codeTypeId, String code, boolean standard, boolean active)
    {
        this(id, conceptId, codeTypeId, code, false, standard, active);
    }

    public T8OntologyCode copy()
    {
        T8OntologyCode copy;

        // Copy the code.
        copy = new T8OntologyCode(id, conceptId, codeTypeId, code, primary, standard, active);
        return copy;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public String getCodeTypeID()
    {
        return codeTypeId;
    }

    public void setCodeTypeID(String codeTypeID)
    {
        this.codeTypeId = codeTypeID;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public boolean isPrimary()
    {
        return primary;
    }

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public boolean isStandard()
    {
        return standard;
    }

    public void setStandard(boolean standard)
    {
        this.standard = standard;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isEqualTo(T8OntologyCode inputCode)
    {
        if (inputCode == null) return false;
        else if (!Objects.equals(id, inputCode.getID())) return false;
        else if (!Objects.equals(codeTypeId, inputCode.getCodeTypeID())) return false;
        else if (!Objects.equals(conceptId, inputCode.getConceptID())) return false;
        else if (!Objects.equals(code, inputCode.getCode())) return false;
        else if (!Objects.equals(primary, inputCode.isPrimary())) return false;
        else if (!Objects.equals(standard, inputCode.isStandard())) return false;
        else if (!Objects.equals(active, inputCode.isActive())) return false;
        else return true;
    }

    @Override
    public String toString()
    {
        return "T8OntologyCode{" + "id=" + id + ", code=" + code + '}';
    }
}
