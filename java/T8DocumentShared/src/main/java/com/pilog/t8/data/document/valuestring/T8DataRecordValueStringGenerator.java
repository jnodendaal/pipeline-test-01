package com.pilog.t8.data.document.valuestring;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.time.T8TimestampFormatter;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.PropertyContent;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.AttachmentListContent;
import com.pilog.t8.data.document.datarecord.value.BooleanValueContent;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.CompositeContent;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.ControlledConceptContent;
import com.pilog.t8.data.document.datarecord.value.DateTimeValue;
import com.pilog.t8.data.document.datarecord.value.DateTimeValueContent;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceListContent;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.FieldContent;
import com.pilog.t8.data.document.datarecord.value.IntegerValueContent;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumberContent;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.MeasuredRangeContent;
import com.pilog.t8.data.document.datarecord.value.NumberContent;
import com.pilog.t8.data.document.datarecord.value.RealValueContent;
import com.pilog.t8.data.document.datarecord.value.StringValueContent;
import com.pilog.t8.data.document.datarecord.value.TextValueContent;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasureContent;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordValueStringGenerator
{
    public static final String DEFAULT_DR_INSTANCE_VALUE_SEPARATOR = ":";
    public static final String DEFAULT_SECTION_VALUE_SEPARATOR = ":";
    public static final String DEFAULT_PROPERTY_SEPARATOR = ",";
    public static final String DEFAULT_PROPERTY_VALUE_SEPARATOR = ":";
    public static final String DEFAULT_ATTACHMT_SEPARATOR = ",";
    public static final String DEFAULT_SET_ELEMENT_SEPARATOR = "/";
    public static final String DEFAULT_RANGE_VALUE_SEPARATOR = "-";
    public static final String DEFAULT_UOM_SEPARATOR = " ";
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private DataRecordProvider recordProvider;
    protected TerminologyProvider terminologyProvider;

    private boolean includeDataRequirementInstanceConcepts;
    private boolean includeDataRequirementConcepts;
    private boolean includeSectionConcepts;
    private boolean includePropertyConcepts;
    private boolean includeFieldConcepts;
    private boolean includeSubDocumentReferences;
    private boolean includeSubDocumentValueStrings;
    private boolean includeRecordFft;
    private boolean includePropertyFft;
    private boolean includeUnresolvedTerms;
    private boolean includeOntologyLinks;
    private final ConceptRenderType drInstanceConceptRenderType;
    private ConceptRenderType sectionConceptRenderType;
    private ConceptRenderType propertyConceptRenderType;
    private ConceptRenderType valueConceptRenderType;
    private AttachmentRenderType attachmentRenderType;
    private String dateTimeFormatPattern;
    private String propertySeparator;
    private String languageId;

    private final String drInstanceValueSeparator = DEFAULT_DR_INSTANCE_VALUE_SEPARATOR;
    private String sectionValueSeparator = DEFAULT_SECTION_VALUE_SEPARATOR;
    private String propertyValueSeparator = DEFAULT_PROPERTY_VALUE_SEPARATOR;
    private String attachmentSeparator = DEFAULT_ATTACHMT_SEPARATOR;
    private String setElementSeparator = DEFAULT_SET_ELEMENT_SEPARATOR;
    private String rangeValueSeparator = DEFAULT_RANGE_VALUE_SEPARATOR;
    private String uomSeparator = DEFAULT_UOM_SEPARATOR;

    private final Set<RequirementType> excludedRequirementTypes; // Types to be excluded from the value string.
    private final Map<String, List<String>> propertySequences; // Override for default property sequence (as they occur in the Data Record).

    public enum ConceptRenderType {CONCEPT_ID, TERM, ABBREVIATION};
    public enum AttachmentRenderType {ATTACHMENT_ID, FILE_NAME, CHECKSUM};

    public T8DataRecordValueStringGenerator()
    {
        recordProvider = null;
        terminologyProvider = null;
        includeDataRequirementInstanceConcepts = true;
        includeDataRequirementConcepts = false;
        includeSectionConcepts = false;
        includePropertyConcepts = true;
        includeFieldConcepts = true;
        includeSubDocumentReferences = false;
        includeSubDocumentValueStrings = false;
        includeRecordFft = false;
        includePropertyFft = false;
        includeUnresolvedTerms = false;
        includeOntologyLinks = false;
        drInstanceConceptRenderType = ConceptRenderType.TERM;
        sectionConceptRenderType = ConceptRenderType.TERM;
        propertyConceptRenderType = ConceptRenderType.TERM;
        valueConceptRenderType = ConceptRenderType.TERM;
        attachmentRenderType = AttachmentRenderType.ATTACHMENT_ID;
        propertySeparator = DEFAULT_PROPERTY_SEPARATOR;
        excludedRequirementTypes = new HashSet<>();
        propertySequences = new HashMap<>();
    }

    public boolean isIncludeDataRequirementInstanceConcepts()
    {
        return includeDataRequirementInstanceConcepts;
    }

    public void setIncludeDataRequirementInstanceConcepts(boolean includeDataRequirementInstanceConcepts)
    {
        this.includeDataRequirementInstanceConcepts = includeDataRequirementInstanceConcepts;
    }

    public boolean isIncludeDataRequirementConcepts()
    {
        return includeDataRequirementConcepts;
    }

    public void setIncludeDataRequirementConcepts(boolean includeDataRequirementConcepts)
    {
        this.includeDataRequirementConcepts = includeDataRequirementConcepts;
    }

    public DataRecordProvider getDataRecordProvider()
    {
        return recordProvider;
    }

    public void setDataRecordProvider(DataRecordProvider recordProvider)
    {
        this.recordProvider = recordProvider;
    }

    public TerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
    }

    public boolean isIncludeSectionConcepts()
    {
        return includeSectionConcepts;
    }

    public void setIncludeSectionConcepts(boolean includeSectionConcepts)
    {
        this.includeSectionConcepts = includeSectionConcepts;
    }

    public boolean isIncludePropertyConcepts()
    {
        return includePropertyConcepts;
    }

    public void setIncludePropertyConcepts(boolean includePropertyConcepts)
    {
        this.includePropertyConcepts = includePropertyConcepts;
    }

    public boolean isIncludeFieldConcepts()
    {
        return includeFieldConcepts;
    }

    public void setIncludeFieldConcepts(boolean includeFieldConcepts)
    {
        this.includeFieldConcepts = includeFieldConcepts;
    }

    public boolean isIncludeSubDocumentReferences()
    {
        return includeSubDocumentReferences;
    }

    public void setIncludeSubDocumentReferences(boolean includeSubDocumentReferences)
    {
        this.includeSubDocumentReferences = includeSubDocumentReferences;
    }

    public boolean isIncludeSubDocumentValueStrings()
    {
        return includeSubDocumentValueStrings;
    }

    public void setIncludeSubDocumentValueStrings(boolean includeSubDocumentValueStrings)
    {
        this.includeSubDocumentValueStrings = includeSubDocumentValueStrings;
    }

    public boolean isIncludeRecordFFT()
    {
        return includeRecordFft;
    }

    public void setIncludeRecordFFT(boolean includeRecordFFT)
    {
        this.includeRecordFft = includeRecordFFT;
    }

    public boolean isIncludePropertyFFT()
    {
        return includePropertyFft;
    }

    public void setIncludePropertyFFT(boolean includePropertyFFT)
    {
        this.includePropertyFft = includePropertyFFT;
    }

    public ConceptRenderType getPropertyConceptRenderType()
    {
        return propertyConceptRenderType;
    }

    public void setPropertyConceptRenderType(ConceptRenderType propertyConceptRenderType)
    {
        this.propertyConceptRenderType = propertyConceptRenderType;
    }

    public String getPropertySeparator()
    {
        return propertySeparator;
    }

    public void setPropertySeparator(String propertySeparator)
    {
        this.propertySeparator = propertySeparator;
    }

    public String getLanguageID()
    {
        return languageId;
    }

    public void setLanguageID(String languageID)
    {
        this.languageId = languageID;
    }

    public DataRecordProvider getRecordProvider()
    {
        return recordProvider;
    }

    public void setRecordProvider(DataRecordProvider recordProvider)
    {
        this.recordProvider = recordProvider;
    }

    public ConceptRenderType getSectionConceptRenderType()
    {
        return sectionConceptRenderType;
    }

    public void setSectionConceptRenderType(ConceptRenderType sectionConceptRenderType)
    {
        this.sectionConceptRenderType = sectionConceptRenderType;
    }

    public ConceptRenderType getValueConceptRenderType()
    {
        return valueConceptRenderType;
    }

    public void setValueConceptRenderType(ConceptRenderType valueConceptRenderType)
    {
        this.valueConceptRenderType = valueConceptRenderType;
    }

    public AttachmentRenderType getAttachmentRenderType()
    {
        return attachmentRenderType;
    }

    public void setAttachmentRenderType(AttachmentRenderType attachmentRenderType)
    {
        this.attachmentRenderType = attachmentRenderType;
    }

    public String getSectionValueSeparator()
    {
        return sectionValueSeparator;
    }

    public void setSectionValueSeparator(String sectionValueSeparator)
    {
        this.sectionValueSeparator = sectionValueSeparator;
    }

    public String getPropertyValueSeparator()
    {
        return propertyValueSeparator;
    }

    public void setPropertyValueSeparator(String propertyValueSeparator)
    {
        this.propertyValueSeparator = propertyValueSeparator;
    }

    public String getSetElementSeparator()
    {
        return setElementSeparator;
    }

    public void setSetElementSeparator(String setElementSeparator)
    {
        this.setElementSeparator = setElementSeparator;
    }

    public String getRangeValueSeparator()
    {
        return rangeValueSeparator;
    }

    public void setRangeValueSeparator(String rangeValueSeparator)
    {
        this.rangeValueSeparator = rangeValueSeparator;
    }

    public String getUomSeparator()
    {
        return uomSeparator;
    }

    public void setUomSeparator(String uomSeparator)
    {
        this.uomSeparator = uomSeparator;
    }

    public String getAttachmentSeparator()
    {
        return attachmentSeparator;
    }

    public void setAttachmentSeparator(String attachmentSeparator)
    {
        this.attachmentSeparator = attachmentSeparator;
    }

    public List<String> getPropertySequence(String drID)
    {
        return propertySequences.get(drID);
    }

    public void setPropertySequence(String drID, List<String> customPropertySequence)
    {
        propertySequences.put(drID, customPropertySequence);
    }

    public void clearPropertySequences()
    {
        propertySequences.clear();
    }

    public void clearExecludedRequirementTypes()
    {
        excludedRequirementTypes.clear();
    }

    public void setExcludedRequirementTypes(Set<RequirementType> requirementTypes)
    {
        excludedRequirementTypes.clear();
        excludedRequirementTypes.addAll(requirementTypes);
    }

    public void addExcludedRequirementType(RequirementType requirementType)
    {
        excludedRequirementTypes.add(requirementType);
    }

    public void setDateTimeFormatPattern(String dateTimeFormatPattern)
    {
        this.dateTimeFormatPattern = dateTimeFormatPattern;
    }

    public boolean isIncludeUnresolvedTerms()
    {
        return includeUnresolvedTerms;
    }

    public void setIncludeUnresolvedTerms(boolean includeUnresolvedTerms)
    {
        this.includeUnresolvedTerms = includeUnresolvedTerms;
    }

    public boolean isIncludeOntologyLinks()
    {
        return includeOntologyLinks;
    }

    public void setIncludeOntologyLinks(boolean includeOntologyLinks)
    {
        this.includeOntologyLinks = includeOntologyLinks;
    }

    public final StringBuilder generateValueString(DataRecord dataRecord)
    {
        StringBuilder propertyValueString;
        StringBuilder valueString;
        String recordFft;

        // Create the record value String buffer.
        valueString = new StringBuilder();
        if (includeDataRequirementInstanceConcepts)
        {
            String drInstanceID;

            // Append the Data Requirement Instance concept.
            drInstanceID = dataRecord.getDataRequirementInstance().getConceptID();
            valueString.append(generateConceptValueString(drInstanceID, drInstanceConceptRenderType));
            valueString.append(drInstanceValueSeparator);
        }

        // Generate and append the property value String.
        propertyValueString = generatePropertyValueString(dataRecord);
        if ((propertyValueString != null) && (propertyValueString.length() > 0))
        {
            valueString.append(propertyValueString);
        }

        // Append the Record FFT if required.
        recordFft = dataRecord.getFFT();
        if ((includeRecordFft) && (!Strings.isNullOrEmpty(recordFft)))
        {
            valueString.append(recordFft);
        }

        // Include ontology links if required.
        if (includeOntologyLinks)
        {
            for (T8OntologyLink link : dataRecord.getOntologyLinks())
            {
                valueString.append(",");
                valueString.append(generateConceptValueString(link.getOrganizationID(), ConceptRenderType.TERM));
                valueString.append(":");
                valueString.append(generateConceptValueString(link.getOntologyStructureID(), ConceptRenderType.TERM));
                valueString.append(":");
                valueString.append(generateConceptValueString(link.getOntologyClassID(), ConceptRenderType.TERM));
            }
        }

        return valueString;
    }

    private StringBuilder generatePropertyValueString(DataRecord dataRecord)
    {
        List<String> propertySequence;
        StringBuilder valueString;
        int propertyStringsAdded;

        valueString = new StringBuilder();

        // Append all property value strings.
        propertySequence = propertySequences.get(dataRecord.getDataRequirement().getConceptID());
        if (propertySequence != null)
        {
            propertyStringsAdded = 0;
            for (String propertyID : propertySequence)
            {
                // Find the next property in the sequence from the properties in the record.
                for (RecordProperty recordProperty : dataRecord.getRecordProperties())
                {
                    // Only add the property value string if it is the next one in the specified sequence.
                    if (recordProperty.getPropertyRequirement().getConceptID().equals(propertyID))
                    {
                        StringBuilder propertyValueString;

                        // Generate the property value string and add it to the section value string if it is not empty.
                        propertyValueString = generateValueString(recordProperty);
                        if ((propertyValueString != null) && (propertyValueString.length() > 0))
                        {
                            // Add a property separator if required.
                            if (propertyStringsAdded > 0) valueString.append(propertySeparator);

                            // Add the property String.
                            valueString.append(propertyValueString);
                            propertyStringsAdded++;
                        }
                    }
                }
            }
        }
        else // Generate and add all properties from the record in the sequence that they appear.
        {
            propertyStringsAdded = 0;
            for (RecordProperty recordProperty : dataRecord.getRecordProperties())
            {
                StringBuilder propertyValueString;

                // Generate the property value string and add it to the section value string if it is not empty.
                propertyValueString = generateValueString(recordProperty);
                if ((propertyValueString != null) && (propertyValueString.length() > 0))
                {
                    // Add a property separator if required.
                    if (propertyStringsAdded > 0) valueString.append(propertySeparator);

                    // Add the property String.
                    valueString.append(propertyValueString);
                    propertyStringsAdded++;
                }
            }
        }

        return valueString;
    }

    public final StringBuilder generateValueString(RecordContent record)
    {
        StringBuilder propertyValueString;
        StringBuilder valueString;
        String recordFft;

        // Create the record value String buffer.
        valueString = new StringBuilder();
        if (includeDataRequirementInstanceConcepts)
        {
            String drInstanceId;

            // Append the Data Requirement Instance concept.
            drInstanceId = record.getDrInstanceId();
            valueString.append(generateConceptValueString(drInstanceId, drInstanceConceptRenderType));
            valueString.append(drInstanceValueSeparator);
        }

        // Generate and append the property value String.
        propertyValueString = generatePropertyValueString(record);
        if ((propertyValueString != null) && (propertyValueString.length() > 0))
        {
            valueString.append(propertyValueString);
        }

        // Append the Record FFT if required.
        recordFft = record.getFft();
        if ((includeRecordFft) && (!Strings.isNullOrEmpty(recordFft)))
        {
            valueString.append(recordFft);
        }

        return valueString;
    }

    private StringBuilder generatePropertyValueString(RecordContent record)
    {
        StringBuilder valueString;
        int propertyStringsAdded;

        valueString = new StringBuilder();

        // Append all property value strings.
        propertyStringsAdded = 0;
        for (PropertyContent recordProperty : record.getProperties())
        {
            StringBuilder propertyValueString;

            // Generate the property value string and add it to the section value string if it is not empty.
            propertyValueString = generateValueString(recordProperty);
            if ((propertyValueString != null) && (propertyValueString.length() > 0))
            {
                // Add a property separator if required.
                if (propertyStringsAdded > 0) valueString.append(propertySeparator);

                // Add the property String.
                valueString.append(propertyValueString);
                propertyStringsAdded++;
            }
        }

        return valueString;
    }

    public final StringBuilder generateValueString(RecordSection recordSection)
    {
        SectionRequirement sectionRequirement;
        List<String> propertySequence;
        StringBuilder sectionString;
        String conceptID;
        int propertyStringsAdded;

        sectionString = new StringBuilder();
        sectionRequirement = recordSection.getSectionRequirement();
        conceptID = sectionRequirement.getConceptID();

        if (includeSectionConcepts)
        {
            sectionString.append(generateConceptValueString(conceptID, sectionConceptRenderType));
            sectionString.append(sectionValueSeparator);
        }

        // Append all property value strings.
        propertySequence = propertySequences.get(recordSection.getSectionRequirement().getParentDataRequirement().getConceptID());
        if (propertySequence != null)
        {
            propertyStringsAdded = 0;
            for (String propertyID : propertySequence)
            {
                // Find the next property in the sequence from the properties in the record.
                for (RecordProperty recordProperty : recordSection.getRecordProperties())
                {
                    // Only add the property value string if it is the next one in the specified sequence.
                    if (recordProperty.getPropertyRequirement().getConceptID().equals(propertyID))
                    {
                        StringBuilder propertyValueString;

                        // Generate the property value string and add it to the section value string if it is not empty.
                        propertyValueString = generateValueString(recordProperty);
                        if ((propertyValueString != null) && (propertyValueString.length() > 0))
                        {
                            // Add a property separator if required.
                            if (propertyStringsAdded > 0) sectionString.append(propertySeparator);

                            // Add the property String.
                            sectionString.append(propertyValueString);
                            propertyStringsAdded++;
                        }
                    }
                }
            }
        }
        else // Generate and add all properties from the record in the sequence that they appear.
        {
            propertyStringsAdded = 0;
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                StringBuilder propertyValueString;

                // Generate the property value string and add it to the section value string if it is not empty.
                propertyValueString = generateValueString(recordProperty);
                if ((propertyValueString != null) && (propertyValueString.length() > 0))
                {
                    // Add a property separator if required.
                    if (propertyStringsAdded > 0) sectionString.append(propertySeparator);

                    // Add the property String.
                    sectionString.append(propertyValueString);
                    propertyStringsAdded++;
                }
            }
        }

        return sectionString;
    }

    public final StringBuilder generateValueString(RecordProperty recordProperty)
    {
        StringBuilder propertyString;
        PropertyRequirement propertyRequirement;
        StringBuilder valueString;
        String conceptID;
        String fft;

        propertyString = new StringBuilder();
        propertyRequirement = recordProperty.getPropertyRequirement();
        conceptID = propertyRequirement.getConceptID();
        valueString = (recordProperty.getRecordValue() != null) ? generateValueString(recordProperty.getRecordValue()) : null;
        fft = recordProperty.getFFT();
        if ((fft != null) && (fft.trim().length() == 0)) fft = null;

        // Append the prefix if necessary.
        if ((valueString != null) || ((fft != null) && (includePropertyFft)))
        {
            if (includePropertyConcepts)
            {
                propertyString.append(generateConceptValueString(conceptID, propertyConceptRenderType));
                propertyString.append(propertyValueSeparator);
            }
        }

        // Append the value String.
        if (valueString != null)
        {
            propertyString.append(valueString);
        }

        // Append the Property FFT if required.
        if ((fft != null) && (includePropertyFft))
        {
            propertyString.append(fft);
        }

        return propertyString.length() > 0 ? propertyString : null;
    }

    public final StringBuilder generateValueString(PropertyContent property)
    {
        StringBuilder propertyString;
        StringBuilder valueString;
        String conceptID;
        String fft;

        propertyString = new StringBuilder();
        conceptID = property.getId();
        valueString = (property.getValue() != null) ? generateValueString(property.getValue()) : null;
        fft = property.getFft();
        if ((fft != null) && (fft.trim().length() == 0)) fft = null;

        // Append the prefix if necessary.
        if ((valueString != null) || ((fft != null) && (includePropertyFft)))
        {
            if (includePropertyConcepts)
            {
                propertyString.append(generateConceptValueString(conceptID, propertyConceptRenderType));
                propertyString.append(propertyValueSeparator);
            }
        }

        // Append the value String.
        if (valueString != null)
        {
            propertyString.append(valueString);
        }

        // Append the Property FFT if required.
        if ((fft != null) && (includePropertyFft))
        {
            propertyString.append(fft);
        }

        return propertyString.length() > 0 ? propertyString : null;
    }

    public final StringBuilder generateValueString(RecordValue recordValue)
    {
        StringBuilder valueString;

        // Make sure this type is not excluded.
        if (excludedRequirementTypes.contains(recordValue.getValueRequirement().getRequirementType())) return null;

        valueString = new StringBuilder();

        // If the type is a document reference, we need to handle it in a specific way depending on the configuration options that are set.
        if (recordValue instanceof DocumentReferenceList)
        {
            for (String referencedRecordId : ((DocumentReferenceList)recordValue).getReferenceIds())
            {
                if ((includeSubDocumentValueStrings) && (recordProvider != null))
                {
                    try
                    {
                        DataRecord referencedRecord;

                        referencedRecord = recordProvider.getDataRecord(referencedRecordId, null, false, false, false, false, false, false, false);
                        valueString.append(generateValueString(referencedRecord));
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while trying to retrieve record from provider.", e);
                    }
                }
                else if (includeSubDocumentReferences)
                {
                    valueString.append(generateConceptValueString(referencedRecordId, valueConceptRenderType));
                }
            }
        }
        else if (recordValue instanceof AttachmentList)
        {
            DataRecord parentRecord;

            switch (attachmentRenderType)
            {
                case FILE_NAME:

                    parentRecord = recordValue.getParentDataRecord();
                    if (parentRecord != null)
                    {
                        for (String attachmentId : ((AttachmentList)recordValue).getAttachmentIds())
                        {
                            T8AttachmentDetails fileDetails;

                            if (valueString.length() > 0) valueString.append(attachmentSeparator);

                            fileDetails = parentRecord.getAttachmentDetails(attachmentId);
                            if (fileDetails != null)
                            {
                                String fileName;

                                fileName = fileDetails.getOriginalFileName();
                                if (!Strings.isNullOrEmpty(fileName))
                                {
                                    valueString.append(fileName);
                                }
                                else valueString.append(attachmentId);
                            }
                            else valueString.append(attachmentId);
                        }

                        break;
                    }
                case CHECKSUM:
                    parentRecord = recordValue.getParentDataRecord();
                    if (parentRecord != null)
                    {
                        for (String attachmentID : ((AttachmentList)recordValue).getAttachmentIds())
                        {
                            T8FileDetails fileDetails;

                            if (valueString.length() > 0) valueString.append(attachmentSeparator);

                            fileDetails = parentRecord.getAttachmentDetails(attachmentID);
                            if (fileDetails != null)
                            {
                                String checksum;

                                checksum = fileDetails.getMD5Checksum();
                                if (!Strings.isNullOrEmpty(checksum))
                                {
                                    valueString.append(checksum);
                                }
                                else valueString.append(attachmentID);
                            }
                            else valueString.append(attachmentID);
                        }

                        break;
                    }
                default:
                    for (String attachmentID : ((AttachmentList)recordValue).getAttachmentIds())
                    {
                        if (valueString.length() > 0) valueString.append(attachmentSeparator);
                        valueString.append(attachmentID);
                    }
            }
        }
        else if (recordValue instanceof MeasuredNumber)
        {
            MeasuredNumber measuredNumber;
            String number;
            String uomId;

            measuredNumber = (MeasuredNumber)recordValue;
            number = measuredNumber.getNumber();
            uomId = measuredNumber.getUomId();

            // Append the numeric if available
            if (!Strings.isNullOrEmpty(number)) valueString.append(number);
            // Append the UOM if available
            if (!Strings.isNullOrEmpty(uomId))
            {
                if (uomSeparator != null) valueString.append(uomSeparator);
                valueString.append(generateConceptValueString(uomId, valueConceptRenderType));
            }
        }
        else if (recordValue instanceof MeasuredRange)
        {
            MeasuredRange measuredRange;
            String upperBoundNumber;
            String lowerBoundNumber;
            String uomId;

            measuredRange = (MeasuredRange)recordValue;
            upperBoundNumber = measuredRange.getUpperBoundNumber();
            lowerBoundNumber = measuredRange.getLowerBoundNumber();
            uomId = measuredRange.getUomId();
            if (!Strings.isNullOrEmpty(lowerBoundNumber))
            {
                valueString.append(lowerBoundNumber);
                // We only want the separator if both the lower and upper bound have values
                if (!Strings.isNullOrEmpty(upperBoundNumber))
                {
                    valueString.append(rangeValueSeparator);
                    valueString.append(upperBoundNumber);
                }
            }
            else if (!Strings.isNullOrEmpty(upperBoundNumber))
            {
                valueString.append(upperBoundNumber);
            }

            // We cannot append the UOM if not specified
            if (!Strings.isNullOrEmpty(uomId))
            {
                if (uomSeparator != null) valueString.append(uomSeparator);
                valueString.append(generateConceptValueString(uomId, valueConceptRenderType));
            }
        }
        else if (recordValue instanceof ControlledConcept)
        {
            ControlledConcept controlledConcept;
            String conceptId;

            controlledConcept = (ControlledConcept)recordValue;
            conceptId = controlledConcept.getConceptId();
            if (conceptId == null)
            {
                String term;

                term = controlledConcept.getTerm();
                if ((includeUnresolvedTerms) && (!Strings.isNullOrEmpty(term)))
                {
                    valueString.append(term);
                }
            }
            else
            {
                valueString.append(generateConceptValueString(conceptId, valueConceptRenderType));
            }
        }
        else if (recordValue instanceof UnitOfMeasure)
        {
            UnitOfMeasure unitOfMeasure;
            String conceptId;

            unitOfMeasure = (UnitOfMeasure)recordValue;
            conceptId = unitOfMeasure.getConceptId();
            if (conceptId == null)
            {
                String term;

                term = unitOfMeasure.getTerm();
                if ((includeUnresolvedTerms) && (!Strings.isNullOrEmpty(term)))
                {
                    valueString.append(term);
                }
            }
            else
            {
                valueString.append(generateConceptValueString(conceptId, valueConceptRenderType));
            }
        }
        else if (recordValue instanceof Composite)
        {
            Composite composite;
            int fieldStringsAdded;

            fieldStringsAdded = 0;
            composite = (Composite)recordValue;
            for (Field field : composite.getFields())
            {
                RecordValue fieldValue;

                fieldValue = field.getFieldValue();
                if (fieldValue != null)
                {
                    StringBuilder fieldValueString;

                    // Generate the field value string and add it to the value string if it is not empty.
                    fieldValueString = generateValueString(fieldValue);
                    if ((fieldValueString != null) && (fieldValueString.length() > 0))
                    {
                        // Add a property separator if required.
                        if (fieldStringsAdded > 0) valueString.append(propertySeparator);

                        // Add the field value String.
                        valueString.append(fieldValueString);
                        fieldStringsAdded++;
                    }
                }
            }
        }
        else if (recordValue instanceof DateTimeValue)
        {
            DateTimeValue dateTimeValue;
            String value;

            dateTimeValue = (DateTimeValue)recordValue;
            value = dateTimeFormatPattern != null ? dateTimeValue.getValue(dateTimeFormatPattern) : dateTimeValue.getValue();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }
        else
        {
            String value;

            value = recordValue.getValue();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }

        return valueString.length() > 0 ? valueString : null;
    }

    public final StringBuilder generateValueString(ValueContent recordValue)
    {
        StringBuilder valueString;

        // Make sure this type is not excluded.
        if (excludedRequirementTypes.contains(recordValue.getType())) return null;

        valueString = new StringBuilder();

        // If the type is a document reference, we need to handle it in a specific way depending on the configuration options that are set.
        if (recordValue instanceof DocumentReferenceListContent)
        {
            for (String referenceId : ((DocumentReferenceListContent)recordValue).getReferenceIds())
            {
                if ((includeSubDocumentValueStrings) && (recordProvider != null))
                {
                    try
                    {
                        DataRecord referencedRecord;

                        referencedRecord = recordProvider.getDataRecord(referenceId, null, false, false, false, false, false, false, false);
                        valueString.append(generateValueString(referencedRecord));
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while trying to retrieve record from provider.", e);
                    }
                }
                else if (includeSubDocumentReferences)
                {
                    valueString.append(generateConceptValueString(referenceId, valueConceptRenderType));
                }
            }
        }
        else if (recordValue instanceof AttachmentListContent)
        {
            RecordContent parentRecord;

            switch (attachmentRenderType)
            {
                case FILE_NAME:

                    parentRecord = recordValue.getRecord();
                    if (parentRecord != null)
                    {
                        AttachmentListContent attachmentList;

                        attachmentList = (AttachmentListContent)recordValue;
                        for (String attachmentId : attachmentList.getAttachmentIdList())
                        {
                            T8AttachmentDetails fileDetails;

                            if (valueString.length() > 0) valueString.append(attachmentSeparator);

                            fileDetails = attachmentList.getAttachmentDetails(attachmentId);
                            if (fileDetails != null)
                            {
                                String fileName;

                                fileName = fileDetails.getOriginalFileName();
                                if (!Strings.isNullOrEmpty(fileName))
                                {
                                    valueString.append(fileName);
                                }
                                else valueString.append(attachmentId);
                            }
                            else valueString.append(attachmentId);
                        }

                        break;
                    }
                case CHECKSUM:
                    parentRecord = recordValue.getRecord();
                    if (parentRecord != null)
                    {
                        AttachmentListContent attachmentList;

                        attachmentList = (AttachmentListContent)recordValue;
                        for (String attachmentId : attachmentList.getAttachmentIdList())
                        {
                            T8FileDetails fileDetails;

                            if (valueString.length() > 0) valueString.append(attachmentSeparator);

                            fileDetails = attachmentList.getAttachmentDetails(attachmentId);
                            if (fileDetails != null)
                            {
                                String checksum;

                                checksum = fileDetails.getMD5Checksum();
                                if (!Strings.isNullOrEmpty(checksum))
                                {
                                    valueString.append(checksum);
                                }
                                else valueString.append(attachmentId);
                            }
                            else valueString.append(attachmentId);
                        }

                        break;
                    }
                default:
                    for (String attachmentId : ((AttachmentListContent)recordValue).getAttachmentIdList())
                    {
                        if (valueString.length() > 0) valueString.append(attachmentSeparator);
                        valueString.append(attachmentId);
                    }
            }
        }
        else if (recordValue instanceof MeasuredNumberContent)
        {
            MeasuredNumberContent measuredNumber;
            NumberContent numberContent;
            UnitOfMeasureContent uomContent;

            measuredNumber = (MeasuredNumberContent)recordValue;

            // Append the numeric if available.
            numberContent = measuredNumber.getNumber();
            if (numberContent != null)
            {
                String number;

                number = numberContent.getValue();
                if (!Strings.isNullOrEmpty(number)) valueString.append(number);
            }

            // Append the UOM if available.
            uomContent = measuredNumber.getUnitOfMeasure();
            if (uomContent != null)
            {
                String uomId;

                uomId = uomContent.getConceptId();
                if (!Strings.isNullOrEmpty(uomId))
                {
                    if (uomSeparator != null) valueString.append(uomSeparator);
                    valueString.append(generateConceptValueString(uomId, valueConceptRenderType));
                }
            }
        }
        else if (recordValue instanceof MeasuredRangeContent)
        {
            MeasuredRangeContent measuredRange;
            UnitOfMeasureContent uomContent;

            measuredRange = (MeasuredRangeContent)recordValue;
            if (measuredRange.hasLowerBoundNumberContent())
            {
                valueString.append(measuredRange.getLowerBoundNumber().getValue());

                // We only want the separator if both the lower and upper bound have values
                if (measuredRange.hasUpperBoundNumberContent())
                {
                    valueString.append(rangeValueSeparator);
                    valueString.append(measuredRange.getUpperBoundNumber().getValue());
                }
            }
            else if (measuredRange.hasUpperBoundNumberContent())
            {
                valueString.append(measuredRange.getUpperBoundNumber().getValue());
            }

            // We cannot append the UOM if not specified
            uomContent = measuredRange.getUnitOfMeasure();
            if (uomContent != null)
            {
                String uomId;

                uomId = uomContent.getConceptId();
                if (!Strings.isNullOrEmpty(uomId))
                {
                    if (uomSeparator != null) valueString.append(uomSeparator);
                    valueString.append(generateConceptValueString(uomId, valueConceptRenderType));
                }
            }
        }
        else if (recordValue instanceof ControlledConceptContent)
        {
            ControlledConceptContent controlledConcept;
            String conceptId;

            controlledConcept = (ControlledConceptContent)recordValue;
            conceptId = controlledConcept.getConceptId();
            if (conceptId == null)
            {
                String term;

                term = controlledConcept.getTerm();
                if ((includeUnresolvedTerms) && (!Strings.isNullOrEmpty(term)))
                {
                    valueString.append(term);
                }
            }
            else
            {
                valueString.append(generateConceptValueString(conceptId, valueConceptRenderType));
            }
        }
        else if (recordValue instanceof UnitOfMeasureContent)
        {
            UnitOfMeasureContent unitOfMeasure;
            String conceptId;

            unitOfMeasure = (UnitOfMeasureContent)recordValue;
            conceptId = unitOfMeasure.getConceptId();
            if (conceptId == null)
            {
                String term;

                term = unitOfMeasure.getTerm();
                if ((includeUnresolvedTerms) && (!Strings.isNullOrEmpty(term)))
                {
                    valueString.append(term);
                }
            }
            else
            {
                valueString.append(generateConceptValueString(conceptId, valueConceptRenderType));
            }
        }
        else if (recordValue instanceof CompositeContent)
        {
            CompositeContent composite;
            int fieldStringsAdded;

            fieldStringsAdded = 0;
            composite = (CompositeContent)recordValue;
            for (FieldContent field : composite.getFields())
            {
                ValueContent fieldValue;

                fieldValue = field.getValue();
                if (fieldValue != null)
                {
                    StringBuilder fieldValueString;

                    // Generate the field value string and add it to the value string if it is not empty.
                    fieldValueString = generateValueString(fieldValue);
                    if ((fieldValueString != null) && (fieldValueString.length() > 0))
                    {
                        // Add a property separator if required.
                        if (fieldStringsAdded > 0) valueString.append(propertySeparator);

                        // Add the field value String.
                        valueString.append(fieldValueString);
                        fieldStringsAdded++;
                    }
                }
            }
        }
        else if (recordValue instanceof DateTimeValueContent)
        {
            DateTimeValueContent dateTimeValue;
            String value;

            dateTimeValue = (DateTimeValueContent)recordValue;
            value = dateTimeValue.getDateTime();
            if (!Strings.isNullOrEmpty(value))
            {
                if (dateTimeFormatPattern != null) value = new T8TimestampFormatter(dateTimeFormatPattern).format(Long.parseLong(value));
                valueString.append(value);
            }
        }
        else if (recordValue instanceof StringValueContent)
        {
            String value;

            value = ((StringValueContent)recordValue).getString();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }
        else if (recordValue instanceof TextValueContent)
        {
            String value;

            value = ((TextValueContent)recordValue).getText();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }
        else if (recordValue instanceof BooleanValueContent)
        {
            String value;

            value = ((BooleanValueContent)recordValue).getValue();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }
        else if (recordValue instanceof NumberContent)
        {
            String value;

            value = ((NumberContent)recordValue).getValue();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }
        else if (recordValue instanceof RealValueContent)
        {
            String value;

            value = ((RealValueContent)recordValue).getValue();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }
        else if (recordValue instanceof IntegerValueContent)
        {
            String value;

            value = ((IntegerValueContent)recordValue).getValue();
            if (!Strings.isNullOrEmpty(value))
            {
                valueString.append(value);
            }
        }

        return valueString.length() > 0 ? valueString : null;
    }

    public final StringBuilder generateConceptValueString(String conceptId, ConceptRenderType renderType)
    {
        StringBuilder valueString;

        valueString = new StringBuilder();

        // Append the value concept.
        if (terminologyProvider != null)
        {
            if (renderType == ConceptRenderType.TERM)
            {
                try
                {
                    T8ConceptTerminology terminology;

                    terminology = terminologyProvider.getTerminology(languageId, conceptId);
                    valueString.append(terminology != null ? terminology.getTerm() : conceptId);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while retrieving terminology for concept: " + conceptId + " in language: " + languageId, e);
                }
            }
            else valueString.append(conceptId);
        }
        else valueString.append(conceptId);

        return valueString;
    }

    /**
     * Replaces the concept id's in the value String with their terms (Also removes delimeters)
     * @param valueString The value string
     * @param languageId the language id that will be used to select the term
     * @param useAbbreviation if abbreviation should be used instead of term (if available)
     * @return The value string with the replaced concepts
     */
    public StringBuffer replaceValueStringConcepts(StringBuffer valueString, String languageId, Boolean useAbbreviation)
    {
        StringBuffer replacementBuffer = new StringBuffer(valueString.length());
        String matchPattern = T8IdentifierUtilities.CONCEPT_ID_PATTERN;
        Pattern pattern = Pattern.compile(matchPattern);
        Matcher matcher = pattern.matcher(valueString.toString());
        while(matcher.find() && !matcher.group().isEmpty())
        {
            String conceptId = matcher.group();
            String replacementTerm = getTerminologyProvider().getTerm(languageId, conceptId);
            if(useAbbreviation)
            {
                String abbreviation = getTerminologyProvider().getAbbreviation(languageId, conceptId);
                if(!Strings.isNullOrEmpty(abbreviation))
                    replacementTerm = abbreviation;
            }
            //if terminology could not be found set back to original
            if(replacementTerm == null)replacementTerm = matcher.group();
            matcher.appendReplacement(replacementBuffer, Matcher.quoteReplacement(replacementTerm));
        }
        matcher.appendTail(replacementBuffer);

        return replacementBuffer;
    }
}
