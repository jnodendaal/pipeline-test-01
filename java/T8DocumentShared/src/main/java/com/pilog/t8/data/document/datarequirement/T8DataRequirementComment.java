package com.pilog.t8.data.document.datarequirement;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementComment implements Serializable
{
    private String id;
    private String typeId;
    private String orgId;
    private String drId;
    private String drInstanceId;
    private String propertyId;
    private String fieldId;
    private String languageId;
    private String comment;

    public T8DataRequirementComment()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String commentId)
    {
        this.id = commentId;
    }

    public String getTypeId()
    {
        return typeId;
    }

    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getDrID()
    {
        return drId;
    }

    public void setDrId(String drId)
    {
        this.drId = drId;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
}
