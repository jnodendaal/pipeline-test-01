package com.pilog.t8.data.document.datarecord;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class RecordPropertyPointer implements Serializable
{
    private String recordID;
    private String drInstanceID;
    private String propertyID;
    private String sectionID;

    public RecordPropertyPointer(String recordID, String drInstanceID, String sectionID, String propertyID)
    {
        this.recordID = recordID;
        this.drInstanceID = drInstanceID;
        this.sectionID = sectionID;
        this.propertyID = propertyID;
    }

    public String getRecordID()
    {
        return recordID;
    }

    public void setRecordID(String recordID)
    {
        this.recordID = recordID;
    }

    public String getDrInstanceID()
    {
        return drInstanceID;
    }

    public void setDrInstanceID(String drInstanceID)
    {
        this.drInstanceID = drInstanceID;
    }
    
    public String getPropertyID()
    {
        return propertyID;
    }

    public void setPropertyID(String propertyID)
    {
        this.propertyID = propertyID;
    }

    @Override
    public String toString()
    {
        return "[DATA_RECORD_PROPERTY:" + recordID + ":" + sectionID + ":" + propertyID + "]";
    }
}
