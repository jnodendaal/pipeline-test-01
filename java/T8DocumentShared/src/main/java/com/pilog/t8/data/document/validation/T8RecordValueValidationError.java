package com.pilog.t8.data.document.validation;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;

/**
 * @author Bouwer du Preez
 */
public class T8RecordValueValidationError extends T8DataValidationError
{
    private String propertyId;
    private String fieldId;

    public T8RecordValueValidationError(ValidationErrorType errorType, String recordId, String propertyId, String fieldId, String errorMessage)
    {
        super(errorType, recordId, errorMessage);
        this.propertyId = propertyId;
        this.fieldId = fieldId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    @Override
    public boolean isApplicableTo(Requirement requirement)
    {
        if (requirement instanceof PropertyRequirement)
        {
            PropertyRequirement proeprtyRequirement;

            proeprtyRequirement = (PropertyRequirement)requirement;
            return propertyId == null || propertyId.equals(proeprtyRequirement.getConceptID());
        }
        else if (requirement instanceof FieldRequirement)
        {
            FieldRequirement fieldRequirement;

            fieldRequirement = (FieldRequirement)requirement;
            if (isApplicableTo(fieldRequirement.getParentPropertyRequirement()))
            {
                return fieldId == null || fieldId.equals(fieldRequirement.getFieldID());
            }
            else return false;
        }
        else if (requirement instanceof ValueRequirement)
        {
            ValueRequirement valueRequirement;
            FieldRequirement fieldRequirement;

            valueRequirement = (ValueRequirement)requirement;
            fieldRequirement = valueRequirement.getFieldRequirement();
            if ((fieldRequirement != null) && (!isApplicableTo(fieldRequirement))) return false;
            else return (isApplicableTo(valueRequirement.getParentPropertyRequirement()));
        }
        else return false;
    }

    @Override
    public boolean isApplicableTo(Value value)
    {
        if (value instanceof DataRecord)
        {
            return recordId.equals(((DataRecord)value).getID());
        }
        else if (value instanceof RecordProperty)
        {
            RecordProperty property;

            property = (RecordProperty)value;
            if (isApplicableTo(property.getParentDataRecord()))
            {
                return propertyId == null || propertyId.equals(property.getPropertyID());
            }
            else return false;
        }
        else if (value instanceof Field)
        {
            Field field;

            field = (Field)value;
            if (isApplicableTo(field.getParentRecordProperty()))
            {
                return fieldId == null || fieldId.equals(field.getFieldID());
            }
            else return false;
        }
        else if (value instanceof RecordValue)
        {
            RecordValue recordValue;
            Field field;

            recordValue = (RecordValue)value;
            field = recordValue.getField();
            if ((field != null) && (!isApplicableTo(field))) return false;
            else return (isApplicableTo(recordValue.getParentRecordProperty()));
        }
        else return false;
    }

    @Override
    public String toString()
    {
        return "T8RecordValueValidationError{" + "errorType=" + errorType + ", recordID=" + recordId + ", propertyID=" + propertyId + ", fieldID=" + fieldId + ", errorMessage=" + errorMessage + '}';
    }
}
