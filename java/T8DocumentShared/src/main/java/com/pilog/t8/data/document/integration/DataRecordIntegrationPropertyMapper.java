package com.pilog.t8.data.document.integration;

import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import java.util.List;

/**
 * @author Hennie Brink
 * @param <T> The Data Record Property Mapping Object
 */
 public interface DataRecordIntegrationPropertyMapper <T>
{
    public void initialize( TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, T8DataRecordPropertyValueStringGenerator valueStringGenerator);
    public T doPropertyMapping(DataRecord dataRecord) throws Exception;
    public List<T> doPropertyMappings(DataRecord dataRecord) throws Exception;
}
