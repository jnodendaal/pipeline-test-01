package com.pilog.t8.data.document.datarecord;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Bouwer du Preez
 */
public class RecordAttribute implements Serializable
{
    private String rootRecordId;
    private String recordId;
    private String attributeId;
    private Object value;

    public enum RecordAttributeType{NUMBER, BOOLEAN, STRING}

    public RecordAttribute(String attributeId, Object value)
    {
        this.attributeId = attributeId;
        this.value = value;
    }

    public String getRootRecordId()
    {
        return rootRecordId;
    }

    public void setRootRecordId(String rootRecordId)
    {
        this.rootRecordId = rootRecordId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getAttributeId()
    {
        return attributeId;
    }

    public void setAttributeId(String attributeId)
    {
        this.attributeId = attributeId;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    public String getValueString()
    {
        return value != null ? value.toString() : null;
    }

    public RecordAttributeType getValueType()
    {
        return RecordAttribute.getType(value);
    }

    public String getValueTypeString()
    {
        return RecordAttribute.getTypeString(value);
    }

    public static Object getValue(String type, Object value)
    {
        return getValue(RecordAttributeType.valueOf(type), value);
    }

    public static Object getValue(RecordAttributeType type, Object value)
    {
        if (value == null)
        {
            return null;
        }
        else
        {
            switch (type)
            {
                case NUMBER:
                    return new BigDecimal(value.toString());
                case BOOLEAN:
                    return Boolean.parseBoolean(value.toString());
                default:
                    return value.toString();
            }
        }
    }

    public static RecordAttributeType getType(Object value)
    {
        if (value instanceof BigDecimal) return RecordAttributeType.NUMBER;
        else if (value instanceof Boolean) return RecordAttributeType.BOOLEAN;
        else return RecordAttributeType.STRING;
    }

    public static String getTypeString(Object value)
    {
        if (value instanceof BigDecimal) return RecordAttributeType.NUMBER.toString();
        else if (value instanceof Boolean) return RecordAttributeType.BOOLEAN.toString();
        else return RecordAttributeType.STRING.toString();
    }
}
