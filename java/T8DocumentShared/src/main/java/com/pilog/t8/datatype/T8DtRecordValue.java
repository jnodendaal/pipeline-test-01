package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarecord.RecordValue;

/**
 * @author Bouwer du Preez
 */
public class T8DtRecordValue extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@RECORD_VALUE";

    public T8DtRecordValue(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return RecordValue.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof RecordValue)
            {
                RecordValue recordValue;

                recordValue = (RecordValue)object;
                return T8DocumentSerializer.serializeRecordValue(recordValue);
            }
            else throw new IllegalArgumentException("Type expected: " + getDataTypeClassName() + " Type found: " + object.getClass().getCanonicalName());
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            //return T8DocumentSerializer.deserializeRecordValue(dr, valueRequirement, object);
            throw new UnsupportedOperationException("Deserialization of RecordValue not yet implemented.");
        }
        else return null;
    }
}
