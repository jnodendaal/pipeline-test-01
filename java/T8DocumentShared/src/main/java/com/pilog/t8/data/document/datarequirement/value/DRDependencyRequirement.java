package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class DRDependencyRequirement extends ValueRequirement
{
    public DRDependencyRequirement()
    {
        super(RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE);
    }
}
