package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceRequirement extends ValueRequirement
{
    public DocumentReferenceRequirement()
    {
        super(RequirementType.DOCUMENT_REFERENCE);
    }

    public String getOntologyClassId()
    {
        ValueRequirement ontologyClassRequirement;

        ontologyClassRequirement = this.getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        if (ontologyClassRequirement != null)
        {
            return ontologyClassRequirement.getValue();
        }
        else return null;
    }

    public void setOntologyClassId(String ocId)
    {
        ValueRequirement ontologyClassRequirement;

        ontologyClassRequirement = this.getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        if (ontologyClassRequirement != null)
        {
            ontologyClassRequirement.setValue(ocId);
        }
        else
        {
            ontologyClassRequirement = ValueRequirement.createValueRequirement(RequirementType.ONTOLOGY_CLASS);
            ontologyClassRequirement.setValue(ocId);
            addSubRequirement(ontologyClassRequirement);
        }
    }

    public boolean isLocal()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.LOCAL.toString()));
    }

    public boolean isIndependent()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.INDEPENDENT.toString()));
    }

    public int getMinimumReferenceCount()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MINIMUM_REFERENCES.toString());
        return value != null ? value.intValue() : -1;
    }

    public int getMaximumReferenceCount()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_REFERENCES.toString());
        return value != null ? value.intValue() : -1;
    }

    public int getMaximumReferencesPerDRInstanceCount()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_REFERENCES_PER_DR_INSTANCE.toString());
        return value != null ? value.intValue() : -1;
    }

    @Override
    public ArrayList<RecordValue> buildRecordValues(List<Map<String, Object>> valueData, Integer parentValueSequence)
    {
        ArrayList<RecordValue> recordValues;
        ArrayList<Map<String, Object>> dataRows;
        HashMap<String, Object> filterValues;
        PropertyRequirement propertyRequirement;
        DocumentReferenceList referenceList;

        propertyRequirement = getParentPropertyRequirement();
        referenceList = new DocumentReferenceList(this);

        filterValues = new HashMap<>();
        filterValues.put("SECTION_ID", propertyRequirement.getParentSectionRequirement().getConceptID());
        filterValues.put("PROPERTY_ID", propertyRequirement.getConceptID());
        filterValues.put("DATA_TYPE_ID", getRequirementType().getID());
        filterValues.put("DATA_TYPE_SEQUENCE", getSequence());
        filterValues.put("PARENT_VALUE_SEQUENCE", parentValueSequence);
        dataRows = CollectionUtilities.getFilteredDataRows(valueData, filterValues);

        recordValues = new ArrayList<>();
        recordValues.add(referenceList);
        for (Map<String, Object> dataRow : dataRows)
        {
            String value;

            value = (String)dataRow.get("VALUE");
            if (!Strings.isNullOrEmpty(value))
            {
                referenceList.addReference(value);
            }
        }

        // Return the constructed value.
        return recordValues;
    }
}
