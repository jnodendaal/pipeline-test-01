/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.document.integration.custom;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Andre Scheepers
 */
public class T8CustomFieldMappingDefinition extends T8Definition
{
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_CUSTOM_FIELD_MAPPING";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_CUSTOM_FIELD_MAPPING";
    public static final String DISPLAY_NAME = "Custom Field Mapping";
    public static final String DESCRIPTION = "A Custom field mapping to the system which the Integration process will map to.";
    public static final String IDENTIFIER_PREFIX = "CUST_FLD_";
    
    public enum Datum
    {
        ENABLED_MAPPING_EXPRESSION,
        INTERFACE_TERM,
        INTERFACE_TERM_SIZE
    };

    public T8CustomFieldMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_MAPPING_EXPRESSION.toString(), "Enabled Mapping Expression", "The Expression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INTERFACE_TERM.toString(), "Interface Term", "The term that will be assigned to this property on the interface."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INTERFACE_TERM_SIZE.toString(), "Interface Term Size", "The term size limit that will be assigned to this property on the interface."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }
    
    public String getEnabledMappingExpression()
    {
        return (String) getDefinitionDatum(Datum.ENABLED_MAPPING_EXPRESSION.toString());
    }
    
    public void setEnabledMappingExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_MAPPING_EXPRESSION.toString(), expression);
    }
    
    public String getInterfaceTerm()
    {
        return (String) getDefinitionDatum(Datum.INTERFACE_TERM.toString());
    }
    
    public void setInterfaceTerm(String interfaceTerm)
    {   
        setDefinitionDatum(Datum.INTERFACE_TERM.toString(), interfaceTerm);
    }
    
    public String getInterfaceTermSize()
    {
        return (String) getDefinitionDatum(Datum.INTERFACE_TERM_SIZE.toString());
    }
    
    public void setInterfaceTermSize(String interfaceTermSize)
    {   
        setDefinitionDatum(Datum.INTERFACE_TERM_SIZE.toString(), interfaceTermSize);
    }
}
