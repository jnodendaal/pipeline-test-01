package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class TextValueContent extends ValueContent
{
    private String text;

    public TextValueContent()
    {
        super(RequirementType.TEXT_TYPE);
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof TextValueContent)
        {
            TextValueContent toTextContent;

            toTextContent = (TextValueContent)toContent;
            return Objects.equals(text, toTextContent.getText());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof TextValueContent)
        {
            TextValueContent toTextContent;

            toTextContent = (TextValueContent)toContent;
            return Objects.equals(text, toTextContent.getText());
        }
        else return false;
    }
}
