/**
 * Created on 21 Aug 2015, 10:52:18 AM
 */
package com.pilog.t8.data.document.datastring;

import java.io.Serializable;

/**
 * @author Gavin Boshoff
 */
public class T8DataStringFormatLink implements Serializable
{
    private T8DataStringFormat.Type type;
    private String drInstanceId;
    private String rootOrgId;
    private String orgId;
    private String drId;
    private String dsTypeId;
    private String dataTypeId;
    private String languageId;
    private String propertyId;
    private String dataKeyId;
    private String rootRecordId;
    private String fieldId;
    private String linkId;
    private String dsFormatId;

    public T8DataStringFormatLink() {}

    public T8DataStringFormat.Type getType()
    {
        return this.type;
    }

    public void setType(T8DataStringFormat.Type type)
    {
        this.type = type;
    }

    /**
     * Convenience method to be used in scripts to set the format type using
     * its {@code String} representation.
     *
     * @param type The {@code String} type value
     */
    public void setType(String type)
    {
        this.type = T8DataStringFormat.Type.valueOf(type);
    }

    public String getDrInstanceId()
    {
        return this.drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getOrgId()
    {
        return this.orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getRootOrgId()
    {
        return this.rootOrgId;
    }

    public void setRootOrgId(String rootOrgId)
    {
        this.rootOrgId = rootOrgId;
    }

    public String getDrId()
    {
        return this.drId;
    }

    public void setDrId(String drId)
    {
        this.drId = drId;
    }

    public String getDsTypeId()
    {
        return this.dsTypeId;
    }

    public void setDataStringTypeID(String dsTypeId)
    {
        this.dsTypeId = dsTypeId;
    }

    public String getDataTypeId()
    {
        return this.dataTypeId;
    }

    public void setDataTypeId(String dataTypeId)
    {
        this.dataTypeId = dataTypeId;
    }

    public String getLanguageId()
    {
        return this.languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getPropertyId()
    {
        return this.propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getDataKeyId()
    {
        return this.dataKeyId;
    }

    public void setDataKeyId(String dataKeyId)
    {
        this.dataKeyId = dataKeyId;
    }

    public String getRootRecordId()
    {
        return rootRecordId;
    }

    public void setRootRecordId(String rootRecordId)
    {
        this.rootRecordId = rootRecordId;
    }

    public String getFieldId()
    {
        return this.fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getLinkId()
    {
        return this.linkId;
    }

    public void setLinkId(String linkId)
    {
        this.linkId = linkId;
    }

    public String getDsFormatId()
    {
        return this.dsFormatId;
    }

    public void setDsFormatId(String dsFormatId)
    {
        this.dsFormatId = dsFormatId;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8DataStringFormatLink{");
        toStringBuilder.append("drInstanceId=").append(this.drInstanceId);
        toStringBuilder.append(",type=").append(this.type);
        toStringBuilder.append(",orgId=").append(this.orgId);
        toStringBuilder.append(",rootOrgId=").append(this.rootOrgId);
        toStringBuilder.append(",drId=").append(this.drId);
        toStringBuilder.append(",dsTypeId=").append(this.dsTypeId);
        toStringBuilder.append(",dataTypeId=").append(this.dataTypeId);
        toStringBuilder.append(",languageId=").append(this.languageId);
        toStringBuilder.append(",propertyId=").append(this.propertyId);
        toStringBuilder.append(",dataKeyId=").append(this.dataKeyId);
        toStringBuilder.append(",rootRecordId=").append(this.rootRecordId);
        toStringBuilder.append(",fieldId=").append(this.fieldId);
        toStringBuilder.append(",linkId=").append(this.linkId);
        toStringBuilder.append(",dsFormatId=").append(this.dsFormatId);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}