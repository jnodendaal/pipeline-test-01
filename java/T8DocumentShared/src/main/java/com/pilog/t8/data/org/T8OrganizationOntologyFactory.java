package com.pilog.t8.data.org;

import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationOntologyFactory implements Serializable
{
    // The language to use for terminology construction.
    private String languageId;

    // The ontology structure to use when creating ontology links.
    private String ontologyStructureId;

    // ODT_ID's to use for construction of ontology object links.
    private String conceptOcId;

    // ORG_ID's to use for construction of ontology object links.
    private String conceptOrgId;
    private String termOrgId;
    private String definitionOrgId;
    private String abbreviationOrgId;
    private String codeOrgId;
    private String terminologyOrgId;

    // Boolean flags to set whether or not ontology links should be added by default when a new ontology object is constructed.
    private boolean createConceptLinks;

    public T8OrganizationOntologyFactory(T8Context context)
    {
        this.createConceptLinks = true;
        this.termOrgId = context.getRootOrganizationId();
        this.abbreviationOrgId = context.getRootOrganizationId();
        this.codeOrgId = context.getRootOrganizationId();
        this.definitionOrgId = context.getRootOrganizationId();
        this.terminologyOrgId = context.getRootOrganizationId();
    }

    public String getLanguageID()
    {
        return languageId;
    }

    public void setLanguageID(String languageID)
    {
        this.languageId = languageID;
    }

    public String getDataStructureID()
    {
        return ontologyStructureId;
    }

    public void setDataStructureID(String dataStructureID)
    {
        this.ontologyStructureId = dataStructureID;
    }

    public String getConceptODTID()
    {
        return conceptOcId;
    }

    public void setConceptODTID(String conceptODTID)
    {
        this.conceptOcId = conceptODTID;
    }

    public String getConceptORGID()
    {
        return conceptOrgId;
    }

    public void setConceptORGID(String conceptORGID)
    {
        this.conceptOrgId = conceptORGID;
    }

    public String getTermORGID()
    {
        return termOrgId;
    }

    public void setTermORGID(String termORGID)
    {
        this.termOrgId = termORGID;
    }

    public String getDefinitionORGID()
    {
        return definitionOrgId;
    }

    public void setDefinitionORGID(String definitionORGID)
    {
        this.definitionOrgId = definitionORGID;
    }

    public String getAbbreviationORGID()
    {
        return abbreviationOrgId;
    }

    public void setAbbreviationORGID(String abbreviationORGID)
    {
        this.abbreviationOrgId = abbreviationORGID;
    }

    public String getCodeORGID()
    {
        return codeOrgId;
    }

    public void setCodeORGID(String codeORGID)
    {
        this.codeOrgId = codeORGID;
    }

    public String getTerminologyORGID()
    {
        return terminologyOrgId;
    }

    public void setTerminologyORGID(String terminologyORGID)
    {
        this.terminologyOrgId = terminologyORGID;
    }

    public boolean isCreateConceptLinks()
    {
        return createConceptLinks;
    }

    public void setCreateConceptLinks(boolean createConceptLinks)
    {
        this.createConceptLinks = createConceptLinks;
    }

    public T8ConceptTerminology constructTerminology(T8OntologyConcept concept)
    {
        return concept.createTerminology(languageId, terminologyOrgId);
    }

    public List<T8ConceptTerminology> constructTerminologies(T8OntologyConcept concept)
    {
        return concept.createTerminologies(terminologyOrgId);
    }

    public T8OntologyConcept constructConcept(T8OntologyConceptType conceptType)
    {
        return constructConcept(conceptType, T8IdentifierUtilities.createNewGUID());
    }

    public T8OntologyConcept constructConcept(String conceptTypeId)
    {
        return constructConcept(T8OntologyConceptType.getConceptType(conceptTypeId), T8IdentifierUtilities.createNewGUID());
    }

    public T8OntologyTerm addTerm(T8OntologyConcept concept, String term)
    {
        T8OntologyTerm newTerm;

        newTerm = constructTerm(term);
        if (concept.getTermCountByLanguage(newTerm.getLanguageID()) == 0) newTerm.setPrimary(true);
        concept.addTerm(newTerm);
        return newTerm;
    }

    public T8OntologyTerm addTerm(T8OntologyConcept concept, String termId, String term)
    {
        T8OntologyTerm newTerm;

        newTerm = constructTerm(termId, term);
        if (concept.getTermCountByLanguage(newTerm.getLanguageID()) == 0) newTerm.setPrimary(true);
        concept.addTerm(newTerm);
        return newTerm;
    }

    public T8OntologyDefinition addDefinition(T8OntologyConcept concept, String definition)
    {
        T8OntologyDefinition newDefinition;

        newDefinition = constructDefinition(definition);
        if (concept.getDefinitionCountByLanguage(newDefinition.getLanguageID()) == 0) newDefinition.setPrimary(true);
        concept.addDefinition(newDefinition);
        return newDefinition;
    }

    public T8OntologyDefinition addDefinition(T8OntologyConcept concept, String definitionID, String definition)
    {
        T8OntologyDefinition newDefinition;

        newDefinition = constructDefinition(definitionID, definition);
        if (concept.getDefinitionCountByLanguage(newDefinition.getLanguageID()) == 0) newDefinition.setPrimary(true);
        concept.addDefinition(newDefinition);
        return newDefinition;
    }

    public T8OntologyAbbreviation addAbbreviation(T8OntologyConcept concept, String abbreviation)
    {
        T8OntologyAbbreviation newAbbreviation;
        T8OntologyTerm parentTerm;

        newAbbreviation = constructAbbreviation(abbreviation);
        concept.addAbbreviation(newAbbreviation);
        parentTerm = concept.getTerm(newAbbreviation.getTermID());
        if (parentTerm.getAbbreviationCount() == 1) newAbbreviation.setPrimary(true);
        return newAbbreviation;
    }

    /**
     * Adds a new abbreviation to the given concept, using the provided
     * abbreviation ID. The abbreviation will be linked to the available term,
     * as long as only a single term exists for the given concept.
     *
     * @param concept The {@code T8OntologyConcept} to which the abbreviation
     *      should be linked
     * @param abbreviationId The {@code String} ID to be allocated to the
     *      abbreviation
     * @param abbreviation The {@code String} abbreviation
     *
     * @return The {@code T8OntologyAbbreviation} object representing the
     *      constructed abbreviation
     *
     * @throws IllegalArgumentException If no proper link can be determined
     *      between the abbreviation and the term(s) associated with the concept
     */
    public T8OntologyAbbreviation addAbbreviation(T8OntologyConcept concept, String abbreviationId, String abbreviation)
    {
        T8OntologyAbbreviation newAbbreviation;
        T8OntologyTerm parentTerm;

        newAbbreviation = constructAbbreviation(abbreviationId, abbreviation);
        concept.addAbbreviation(newAbbreviation);
        parentTerm = concept.getTerm(newAbbreviation.getTermID());
        if (parentTerm.getAbbreviationCount() == 1) newAbbreviation.setPrimary(true);

        return newAbbreviation;
    }

    public T8OntologyAbbreviation addAbbreviation(T8OntologyTerm term, String abbreviation)
    {
        T8OntologyAbbreviation newAbbreviation;

        newAbbreviation = constructAbbreviation(abbreviation);
        if (term.getAbbreviationCount() == 0) newAbbreviation.setPrimary(true);
        term.addAbbreviation(newAbbreviation);
        return newAbbreviation;
    }

    public T8OntologyCode addCode(T8OntologyConcept concept, String codeTypeId, String code)
    {
        T8OntologyCode newCode;

        newCode = constructCode(codeTypeId, code);
        if (concept.getCodeCount() == 0) newCode.setPrimary(true);
        concept.addCode(newCode);
        return newCode;
    }

    public T8OntologyTerm constructTerm(String term)
    {
        return constructTerm(T8IdentifierUtilities.createNewGUID(), term);
    }

    public T8OntologyConcept constructConcept(T8OntologyConceptType conceptType, String conceptID)
    {
        T8OntologyConcept newConcept;

        // Construct the new concept.
        newConcept = new T8OntologyConcept(conceptID, conceptType, false, true);

        // Add the concept organization link if required.
        if ((createConceptLinks) && (conceptOcId != null))
        {
            newConcept.addOntologyLink(new T8OntologyLink(conceptOrgId, ontologyStructureId, conceptOcId, conceptID));
        }

        // Return the constructed concept.
        return newConcept;
    }

    public T8OntologyTerm constructTerm(String termId, String term)
    {
        T8OntologyTerm newTerm;

        // Construct the new term.
        newTerm = new T8OntologyTerm(termId, null, languageId, term, false, true);

        // Return the constructed term.
        return newTerm;
    }

    public T8OntologyAbbreviation constructAbbreviation(String abbreviation)
    {
        return constructAbbreviation(T8IdentifierUtilities.createNewGUID(), abbreviation);
    }

    public T8OntologyAbbreviation constructAbbreviation(String abbreviationID, String abbreviation)
    {
        T8OntologyAbbreviation newAbbreviation;

        // Construct the new abbreviation.
        newAbbreviation = new T8OntologyAbbreviation(abbreviationID, null, abbreviation, false, true);

        // Return the constructed abbreviation.
        return newAbbreviation;
    }

    public T8OntologyDefinition constructDefinition(String definition)
    {
        return constructDefinition(T8IdentifierUtilities.createNewGUID(), definition);
    }

    public T8OntologyDefinition constructDefinition(String definitionID, String definition)
    {
        T8OntologyDefinition newDefinition;

        // Construct the new definition.
        newDefinition = new T8OntologyDefinition(definitionID, null, languageId, definition, false, true);

        // Return the constructed definition.
        return newDefinition;
    }

    public T8OntologyCode constructCode(String codeTypeId, String code)
    {
        return constructCode(T8IdentifierUtilities.createNewGUID(), codeTypeId, code);
    }

    public T8OntologyCode constructCode(String codeId, String codeTypeId, String code)
    {
        T8OntologyCode newCode;

        // Construct the new code.
        newCode = new T8OntologyCode(codeId, null, codeTypeId, code, false, true);

        // Return the constructed code.
        return newCode;
    }

    /**
     * This method will set the term of the concept in the current language.
     * If a term in the current language does not exist, a new term will be created.
     * If a term already exists in the current language, the term will be updated.
     * If more than one term in the current language exists, the first term will be updated and all subsequent terms of the same language will be discarded.
     * @param concept The concept on which to set the term.
     * @param termString The term value to set.
     * @return The updated/created term.
     */
    public T8OntologyTerm setTerm(T8OntologyConcept concept, String termString)
    {
        T8OntologyTerm resultTerm;

        resultTerm = null;
        for (T8OntologyTerm term : concept.getTerms())
        {
            // If the term's language corresponds to the one we are looking for, update/remove it.
            if (languageId.equals(term.getLanguageID()))
            {
                // If we have not yet found a term to update, update this one.
                if (resultTerm == null)
                {
                    resultTerm = term;
                    resultTerm.setTerm(termString);
                }
                else
                {
                    // If we've already found and updated one of the existing terms, remove all of the others of the same language.
                    concept.removeTerm(term);
                }
            }
        }

        // If no term was found to update, create a new one.
        if (resultTerm == null)
        {
            resultTerm = addTerm(concept, termString);
            return resultTerm;
        }
        else return resultTerm;
    }

    /**
     * This method will set the definition of the concept in the current language.
     * If a definition in the current language does not exist, a new definition will be created.
     * If a definition already exists in the current language, the definition will be updated.
     * If more than one definition in the current language exists, the first definition will be updated and all subsequent definitions of the same language will be discarded.
     * @param concept The concept on which to set the definition.
     * @param definitionString The definition value to set.
     * @return The updated/created definition.
     */
    public T8OntologyDefinition setDefinition(T8OntologyConcept concept, String definitionString)
    {
        T8OntologyDefinition resultDefinition;

        resultDefinition = null;
        for (T8OntologyDefinition definition : concept.getDefinitions())
        {
            // If the definition's language corresponds to the one we are looking for, update/remove it.
            if (languageId.equals(definition.getLanguageID()))
            {
                // If we have not yet found a definition to update, update this one.
                if (resultDefinition == null)
                {
                    resultDefinition = definition;
                    resultDefinition.setDefinition(definitionString);
                }
                else
                {
                    // If we've already found and updated one of the existing definitions, remove all of the others of the same language.
                    concept.removeDefinition(definition);
                }
            }
        }

        // If no definition was found to update, create a new one.
        if (resultDefinition == null)
        {
            resultDefinition = addDefinition(concept, definitionString);
            return resultDefinition;
        }
        else return resultDefinition;
    }
}
