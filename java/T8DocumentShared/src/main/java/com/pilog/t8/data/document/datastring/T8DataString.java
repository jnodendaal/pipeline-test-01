package com.pilog.t8.data.document.datastring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is a simple wrapper for a Data String that carries the details
 * about the wrapped string.
 *
 * @author Bouwer du Preez
 */
public class T8DataString implements Serializable
{
    private String instanceId;
    private String recordId; // The ID of the data file from which this data string was generated.
    private String typeId; // The type of this data string.
    private String languageId; // The language used to generate the data string.
    private String string; // The string content.
    private final Set<String> organizationLinks; // The organizations to which this data string is linked via its instance.
    private final List<T8DataStringFormat> formats; // Formats used to generate this data string.

    public T8DataString(String instanceId, String typeId, String languageId, String recordId, String string)
    {
        this.instanceId = instanceId;
        this.typeId = typeId;
        this.languageId = languageId;
        this.recordId = recordId;
        this.string = string;
        this.organizationLinks = new HashSet<String>();
        this.formats = new ArrayList<T8DataStringFormat>();
    }

    public T8DataString(String recordId, String string)
    {
        this.recordId = recordId;
        this.string = string;
        this.organizationLinks = new HashSet<String>();
        this.formats = new ArrayList<T8DataStringFormat>();
    }

    public String getString()
    {
        return string;
    }

    public void setString(String string)
    {
        this.string = string;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getTypeId()
    {
        return typeId;
    }

    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public Set<String> getOrganizationLinkSet()
    {
        return new HashSet<String>(organizationLinks);
    }

    public List<String> getOrganizationLinks()
    {
        return new ArrayList<String>(organizationLinks);
    }

    public void addOrganizationLink(String orgId)
    {
        organizationLinks.add(orgId);
    }

    public void setOrganizationLinks(Collection<String> orgIds)
    {
        organizationLinks.clear();
        if (orgIds != null)
        {
            organizationLinks.addAll(orgIds);
        }
    }

    public void addFormat(T8DataStringFormat format)
    {
        formats.add(format);
    }

    public boolean containsFormat(String formatId)
    {
        return getFormat(formatId) != null;
    }

    public T8DataStringFormat getFormat(String formatId)
    {
        for (T8DataStringFormat format : formats)
        {
            if (format.getID().equals(formatId))
            {
                return format;
            }
        }

        return null;
    }

    public List<T8DataStringFormat> getFormats()
    {
        return new ArrayList<T8DataStringFormat>(formats);
    }

    public void setFormats(List<T8DataStringFormat> formats)
    {
        this.formats.clear();
        if (formats != null)
        {
            this.formats.addAll(formats);
        }
    }

    public T8DataString copy()
    {
        T8DataString copy;

        copy = new T8DataString(recordId, string);
        copy.setString(string);
        copy.setInstanceId(instanceId);
        copy.setLanguageId(languageId);
        copy.setRecordId(recordId);
        copy.setTypeId(typeId);
        copy.setFormats(formats);
        return copy;
    }

    /**
     * @deprecated Use getInstanceId
     */
    @Deprecated
    public String getInstanceID()
    {
        return instanceId;
    }

    /**
     * @deprecated Use setInstanceId
     */
    @Deprecated
    public void setInstanceID(String instanceID)
    {
        this.instanceId = instanceID;
    }

    /**
     * @deprecated Use getLanguageId
     */
    @Deprecated
    public String getLanguageID()
    {
        return languageId;
    }

    /**
     * @deprecated Use setLanguageId
     */
    @Deprecated
    public void setLanguageID(String languageID)
    {
        this.languageId = languageID;
    }

    /**
     * @deprecated Use getTypeId
     */
    @Deprecated
    public String getTypeID()
    {
        return typeId;
    }

    /**
     * @deprecated Use setTypeId
     */
    @Deprecated
    public void setTypeID(String typeID)
    {
        this.typeId = typeID;
    }

    /**
     * @deprecated Use getRecordId
     */
    @Deprecated
    public String getRecordID()
    {
        return recordId;
    }

    /**
     * @deprecated Use setRecordId
     */
    @Deprecated
    public void setRecordID(String recordID)
    {
        this.recordId = recordID;
    }
}
