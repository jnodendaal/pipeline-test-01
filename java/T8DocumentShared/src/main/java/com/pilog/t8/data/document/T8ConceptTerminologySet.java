package com.pilog.t8.data.document;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptTerminologySet implements Serializable
{
    private final T8OntologyConceptType conceptType;
    private final String conceptId;
    private final Map<String, T8ConceptTerminology> terminologies; // Key: Language ID.

    public T8ConceptTerminologySet(T8OntologyConceptType conceptType, String conceptID)
    {
        this.conceptType = conceptType;
        this.conceptId = conceptID;
        this.terminologies = new HashMap<String, T8ConceptTerminology>();
    }

    public T8OntologyConceptType getConceptType()
    {
        return conceptType;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public T8ConceptTerminology getTerminology(String languageID)
    {
        return terminologies.get(languageID);
    }

    /**
     * Sets the new terminology of the specified concept-language combination
     * to the supplied values, overwriting all existing values for the combination.
     * @param newTerminology The terminology to put into this set.
     */
    public void setTerminology(T8ConceptTerminology newTerminology)
    {
        if (newTerminology.getLanguageId() == null) throw new IllegalArgumentException("Language ID of terminology is null.");
        else if (!conceptId.equals(newTerminology.getConceptId())) throw new IllegalArgumentException("Concept ID of terminology argument is '" + newTerminology.getConceptId() + "' and does not correspond to the Concept ID of the set where it is being added: " +  conceptId);
        else terminologies.put(newTerminology.getLanguageId(), newTerminology);
    }

    /**
     * Adds the supplied terminology to the set for the specified concept-language combination,
     * overwriting existing values where there are new non-null terminology values supplied, but
     * leaving all existing values for which no new values (non-null) are present in the supplied
     * terminology object.
     * @param newTerminology The terminology to add to this set.
     */
    public void addTerminology(T8ConceptTerminology newTerminology)
    {
        String languageId;

        languageId = newTerminology.getLanguageId();
        if (languageId == null) throw new IllegalArgumentException("Language ID of terminology is null.");
        else if (!conceptId.equals(newTerminology.getConceptId())) throw new IllegalArgumentException("Concept ID of terminology argument is '" + newTerminology.getConceptId() + "' and does not correspond to the Concept ID of the set where it is being added: " +  conceptId);
        else
        {
            T8ConceptTerminology existingTerminology;

            existingTerminology = terminologies.get(languageId);
            if (existingTerminology != null)
            {
                String codeId;
                String code;
                String termId;
                String term;
                String definitionId;
                String definition;
                String abbreviationId;
                String abbreviation;

                codeId = newTerminology.getCodeId();
                code = newTerminology.getCode();
                termId = newTerminology.getTermId();
                term = newTerminology.getTerm();
                definitionId = newTerminology.getDefinitionId();
                definition = newTerminology.getDefinition();
                abbreviationId = newTerminology.getAbbreviationId();
                abbreviation = newTerminology.getAbbreviation();

                if (codeId != null) existingTerminology.setCodeId(codeId);
                if (code != null) existingTerminology.setCode(code);
                if (termId != null) existingTerminology.setTermId(termId);
                if (term != null) existingTerminology.setTerm(term);
                if (abbreviationId != null) existingTerminology.setAbbreviationId(abbreviationId);
                if (abbreviation != null) existingTerminology.setAbbreviation(abbreviation);
                if (definitionId != null) existingTerminology.setDefinitionId(definitionId);
                if (definition != null) existingTerminology.setDefinition(definition);
            }
            else terminologies.put(languageId, newTerminology);
        }
    }

    public boolean containsLanguage(String languageID)
    {
        return terminologies.containsKey(languageID);
    }

    public String getTerm(String languageID)
    {
        T8ConceptTerminology terminology;

        terminology = terminologies.get(languageID);
        return terminology != null ? terminology.getTerm() : null;
    }

    public String getDefinition(String languageID)
    {
        T8ConceptTerminology terminology;

        terminology = terminologies.get(languageID);
        return terminology != null ? terminology.getDefinition() : null;
    }

    public String getAbbreviation(String languageID)
    {
        T8ConceptTerminology terminology;

        terminology = terminologies.get(languageID);
        return terminology != null ? terminology.getAbbreviation() : null;
    }

    public String getCode()
    {
        for (T8ConceptTerminology terminology : terminologies.values())
        {
            String code;

            // Just return the first code.  We might do some checking here later.
            code = terminology.getCode();
            return code;
        }

        return null;
    }
}
