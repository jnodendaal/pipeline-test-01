package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8PropertyFilterCriterion implements Serializable
{
    private String propertyId;
    private String fieldId;
    private boolean allowRemoval;
    private final List<T8ValueFilterCriterion> valueCriteria;

    public T8PropertyFilterCriterion(String propertyId)
    {
        this.propertyId = propertyId;
        this.allowRemoval = true;
        this.valueCriteria = new ArrayList<>();
    }

    public T8PropertyFilterCriterion(String propertyId, String fieldId)
    {
        this.propertyId = propertyId;
        this.fieldId = fieldId;
        this.allowRemoval = true;
        this.valueCriteria = new ArrayList<>();
    }

    public T8PropertyFilterCriterion copy()
    {
        T8PropertyFilterCriterion copiedCriterion;

        copiedCriterion = new T8PropertyFilterCriterion(propertyId, fieldId);
        copiedCriterion.setAllowRemoval(allowRemoval);

        for (T8ValueFilterCriterion valueCriterion : valueCriteria)
        {
            copiedCriterion.addValueCriterion(valueCriterion.copy());
        }

        return copiedCriterion;
    }

    public boolean hasCriterion()
    {
        for (T8ValueFilterCriterion criterion : valueCriteria)
        {
            if (criterion.hasCriterion()) return true;
        }

        return false;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public boolean isAllowRemoval()
    {
        return allowRemoval;
    }

    public void setAllowRemoval(boolean allowRemoval)
    {
        this.allowRemoval = allowRemoval;
    }

    public List<T8ValueFilterCriterion> getValueCriteria()
    {
        return new ArrayList<>(valueCriteria);
    }

    public void setValueCriteria(List<T8ValueFilterCriterion> criteria)
    {
        this.valueCriteria.clear();
        if (criteria != null)
        {
            this.valueCriteria.addAll(criteria);
        }
    }

    public void addValueCriterion(T8ValueFilterCriterion criterion)
    {
        valueCriteria.add(criterion);
    }

    public String getRequiredCteId()
    {
        // All value criteria should use the same CTE, so just get it from the first one.
        return valueCriteria.size() > 0 ? valueCriteria.get(0).getRequiredCteId() : null;
    }

    public StringBuffer getWhereClause(T8DataTransaction tx, T8DatabaseAdaptor adaptor)
    {
        StringBuffer whereClause;

        // Add all of the property criteria.
        whereClause = new StringBuffer();

        // Append the value filter clause.
        for (int criterionIndex = 0; criterionIndex < valueCriteria.size(); criterionIndex++)
        {
            T8ValueFilterCriterion criterion;
            
            if (criterionIndex > 0)
            {
                whereClause.append(" AND ");
            }

            criterion = valueCriteria.get(criterionIndex);
            whereClause.append(criterion.getWhereClause(tx, adaptor));
        }

        // Return the completed where clause.
        return whereClause;
    }

    public List<Object> getWhereClauseParameters(T8DataTransaction tx, T8DatabaseAdaptor adaptor)
    {
        ArrayList<Object> parameterList;

        // Add property criteria parameters.
        parameterList = new ArrayList<>();

        // Append the value filter parameters.
        for (T8ValueFilterCriterion criterion : valueCriteria)
        {
            parameterList.addAll(criterion.getWhereClauseParameters(tx, adaptor));
        }

        // Return the complete parameter list.
        return parameterList;
    }
}
