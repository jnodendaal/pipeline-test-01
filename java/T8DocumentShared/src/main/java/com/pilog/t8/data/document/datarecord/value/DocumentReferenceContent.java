package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.value.DocumentReference.DocumentReferenceType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceContent implements Serializable
{
    private DocumentReferenceType referenceType;
    private String conceptId;
    private String code;
    private String term;
    private String definition;
    private String abbreviation;

    public DocumentReferenceContent(DocumentReferenceType type, String recordId)
    {
        this.referenceType = type;
        this.conceptId = recordId;
    }

    public DocumentReferenceType getReferenceType()
    {
        return referenceType;
    }

    public void setReferenceType(DocumentReferenceType referenceType)
    {
        this.referenceType = referenceType;
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public void setConceptId(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        if (conceptId != null)
        {
            terminologyCollector.addTerminology(null, T8OntologyConceptType.DATA_RECORD, conceptId, null, code, null, term, null, abbreviation, null, definition);
        }
    }

    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        terminology = terminologyProvider.getTerminology(null, conceptId);
        if (terminology != null)
        {
            this.code = terminology.getCode();
            this.term = terminology.getTerm();
            this.definition = terminology.getDefinition();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.code = null;
            this.term = null;
            this.definition = null;
            this.abbreviation = null;
        }
    }

    public boolean isEqualTo(DocumentReferenceContent toContent)
    {
        if (!Objects.equals(referenceType, toContent.getReferenceType())) return false;
        else if (!Objects.equals(conceptId, toContent.getConceptId())) return false;
        else if (!Objects.equals(term, toContent.getTerm())) return false;
        else if (!Objects.equals(code, toContent.getCode())) return false;
        else if (!Objects.equals(abbreviation, toContent.getAbbreviation())) return false;
        else if (!Objects.equals(definition, toContent.getDefinition())) return false;
        else return true;
    }
}
