package com.pilog.t8.data.document.conformance;

import com.pilog.t8.data.document.datarecord.RecordValue;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordConformanceError implements Serializable
{
    private String recordID;
    private String dataRequirementID;
    private String dataRequirementInstanceID;
    private String classID;
    private String sectionID;
    private String propertyID;
    private String value;
    private String errorMessage;
    private ConformanceErrorType errorType;
    
    public enum ConformanceErrorType
    {
        INVALID_REQUIREMENT, 
        REQUIRED_DATA_NOT_FOUND, 
        INVALID_DATA_FORMAT, 
        INVALID_DATA,
        CONCEPT_MATCH_NOT_FOUND
    };
    
    public T8DataRecordConformanceError(ConformanceErrorType type, String recordID, String drInstanceID, String drID, String classID, String sectionID, String propertyID, String value, String errorMessage)
    {
        this.errorType = type;
        this.recordID = recordID;
        this.dataRequirementID = drID;
        this.dataRequirementInstanceID = drInstanceID;
        this.classID = classID;
        this.sectionID = sectionID;
        this.propertyID = propertyID;
        this.value = value;
        this.errorMessage = errorMessage;
    }
    
    public T8DataRecordConformanceError(ConformanceErrorType type, RecordValue recordValue, String errorMessage)
    {
        this(type, recordValue.getRecordID(), recordValue.getDataRequirementInstance().getConceptID(), recordValue.getDataRequirement().getConceptID(), recordValue.getDataRequirement().getClassConceptID(), recordValue.getSectionID(), recordValue.getPropertyID(), recordValue.getValue(), errorMessage);
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public String getRecordID()
    {
        return recordID;
    }

    public String getSectionID()
    {
        return sectionID;
    }

    public String getPropertyID()
    {
        return propertyID;
    }

    public String getDataRequirementID()
    {
        return dataRequirementID;
    }

    public String getDataRequirementInstanceID()
    {
        return dataRequirementInstanceID;
    }

    public String getClassID()
    {
        return classID;
    }

    public String getValue()
    {
        return value;
    }

    public ConformanceErrorType getErrorType()
    {
        return errorType;
    }
}
