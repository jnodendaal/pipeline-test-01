package com.pilog.t8.definition.data.document.datarecord.access.scripted;

import com.pilog.t8.data.document.datarecord.access.scripted.T8DataRecordAccessScript;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ClientContextScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAccessScriptDefinition extends T8Definition implements T8ServerContextScriptDefinition, T8ClientContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_RECORD_ACCESS_SCRIPT";
    public static final String GROUP_NAME = "Data Record Access Scripts";
    public static final String GROUP_DESCRIPTION = "Scripted rules that apply specific business logic and access rules to data records they are applied to.";
    public static final String STORAGE_PATH = "/data_record_access_logic";
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_ACCESS_SCRIPT";
    public static final String DISPLAY_NAME = "Record Access Logic Script";
    public static final String DESCRIPTION = "A script that is executed to apply specific business logic and access rules to a target record.";
    public static final String IDENTIFIER_PREFIX = "ACCESS_SCRIPT_";
    public enum Datum {EPIC_SCRIPT};
    // -------- Definition Meta-Data -------- //

    public static final String SCRIPT_PARAMETER_ROOT_DATA_RECORD = "$P_ROOT_DATA_RECORD";
    public static final String SCRIPT_PARAMETER_FOCUS_DATA_RECORD = "$P_FOCUS_DATA_RECORD";
    public static final String SCRIPT_PARAMETER_ONTOLOGY_DATA_STRUCTURE = "$P_ONTOLOGY_DATA_STRUCTURE";
    public static final String SCRIPT_PARAMETER_ORGANIZATION_STRUCTURE = "$P_ORGANIZATION_STRUCTURE";

    public T8DataRecordAccessScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.EPIC_SCRIPT.toString(), "EPIC Script",  "The  script the will be executed to refresh the Record Access."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.EPIC_SCRIPT.toString().equals(datumIdentifier))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ScriptDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public T8DataRecordAccessScript createScript(T8Context context) throws Exception
    {
        return (T8DataRecordAccessScript)getNewScriptInstance(context);
    }

    @Override
    public T8Script getNewScriptInstance(T8Context context) throws Exception
    {
        if (context.isServer())
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.access.scripted.T8ServerDataRecordAccessScript").getConstructor(T8Context.class, T8ServerContextScriptDefinition.class);
            return (T8Script)constructor.newInstance(context, this);
        }
        else
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.access.scripted.T8ClientDataRecordAccessScript").getConstructor(T8Context.class, T8ClientContextScriptDefinition.class);
            return (T8Script)constructor.newInstance(context, this);
        }
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<T8DataParameterDefinition>();
        parameterList.add(new T8DataParameterDefinition(SCRIPT_PARAMETER_ROOT_DATA_RECORD, "Root Data Record", "The root record to which the access layer is applied.", T8DataType.CUSTOM_OBJECT));
        parameterList.add(new T8DataParameterDefinition(SCRIPT_PARAMETER_FOCUS_DATA_RECORD, "Data Record", "The record which is the focus of the state update (only applicable during initialization).", T8DataType.CUSTOM_OBJECT));
        parameterList.add(new T8DataParameterDefinition(SCRIPT_PARAMETER_ONTOLOGY_DATA_STRUCTURE, "Ontology Data Structure", "The ontology data structure applicable to this script.", T8DataType.CUSTOM_OBJECT));
        parameterList.add(new T8DataParameterDefinition(SCRIPT_PARAMETER_ORGANIZATION_STRUCTURE, "Organization Structure", "The organization structure applicable to this script.", T8DataType.CUSTOM_OBJECT));
        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        return null;
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        return null;
    }

    @Override
    public String getScript()
    {
        return getEPICScript();
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }

    public String getEPICScript()
    {
        return (String)getDefinitionDatum(Datum.EPIC_SCRIPT.toString());
    }

    public void setEPICScript(String script)
    {
        setDefinitionDatum(Datum.EPIC_SCRIPT.toString(), script);
    }
}
