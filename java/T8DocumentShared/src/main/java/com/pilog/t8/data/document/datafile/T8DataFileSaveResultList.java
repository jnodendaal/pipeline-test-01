package com.pilog.t8.data.document.datafile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileSaveResultList implements Serializable
{
    private final List<T8DataFileSaveResult> resultList;

    public T8DataFileSaveResultList()
    {
        this.resultList = new ArrayList<T8DataFileSaveResult>();
    }

    public void addSaveResult(T8DataFileSaveResult result)
    {
        resultList.add(result);
    }

    public List<T8DataFileSaveResult> getSaveResults()
    {
        return new ArrayList<T8DataFileSaveResult>();
    }

    public T8DataFileSaveResult getSaveResult(String dataFileID)
    {
        for (T8DataFileSaveResult result : resultList)
        {
            if (dataFileID.equals(result.getDataFileID()))
            {
                return result;
            }
        }

        return null;
    }

    /**
     * Returns true if all records in the result list have been saved
     * successfully.
     * @return true if all records in the result list have been saved
     * successfully.
     */
    public boolean isSuccess()
    {
        for (T8DataFileSaveResult result : resultList)
        {
            if (!result.isSaved())
            {
                return false;
            }
        }

        return true;
    }
}
