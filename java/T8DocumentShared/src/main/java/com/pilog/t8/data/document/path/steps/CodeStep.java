package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.ontology.T8OntologyCode;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class CodeStep extends DefaultPathStep implements PathStep
{
    public CodeStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, ontologyProvider, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getID());
        }
        else if (input instanceof RecordProperty)
        {
            return evaluateStep(((RecordProperty)input).getPropertyID());
        }
        else if (input instanceof RecordValue)
        {
            RecordValue recordValue;

            recordValue = (RecordValue)input;
            if (recordValue.isField())
            {
                return evaluateStep(recordValue.getFieldID());
            }
            else
            {
                return evaluateStep(recordValue.getValueConceptID());
            }
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateStep(((DataRequirementInstance)input).getConceptID());
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateStep(((DataRequirement)input).getConceptID());
        }
        else if (input instanceof PropertyRequirement)
        {
            return evaluateStep(((PropertyRequirement)input).getConceptID());
        }
        else if (input instanceof ValueRequirement)
        {
            return evaluateStep(((ValueRequirement)input).getValue());
        }
        else if (input instanceof String)
        {
            String inputConceptId;

            inputConceptId = (String)input;
            if (T8IdentifierUtilities.isConceptID(inputConceptId))
            {
                return evaluateStep(inputConceptId);
            }
            else throw new RuntimeException("Code step '" + stepString + "' cannot be executed from content object: " + input);
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Code step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(String conceptId)
    {
        List<String> codes;

        // Create a list to hold the codes.
        codes = new ArrayList<String>();

        // If we have code type ID's to use for narrowing of the code list do it now.
        if (idList != null)
        {
            if (ontologyProvider != null)
            {
                List<T8OntologyCode> ontologyCodes;

                ontologyCodes = ontologyProvider.getOntologyCodes(conceptId);
                for (T8OntologyCode ontologyCode : ontologyCodes)
                {
                    if (idList.contains(ontologyCode.getCodeTypeID()))
                    {
                        codes.add(ontologyCode.getCode());
                    }
                }

                return codes;
            }
            else throw new RuntimeException("Code step requires ontology provider, but none is set.");
        }
        else
        {
            // No code type is specified, so we use the default type (from the terminology provider for performance reasons).
            if (terminologyProvider != null)
            {
                String code;

                code = terminologyProvider.getCode(conceptId);
                if (code != null)
                {
                    codes.add(code);
                }
                else if (modifiers.isIncludeNullValues())
                {
                    codes.add(null);
                }

                return codes;
            }
            else throw new RuntimeException("Code step requires terminology provider, but none is set.");
        }
    }
}
