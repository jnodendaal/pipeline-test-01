package com.pilog.t8.definition.data.ontology.commission;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyCommissioningAPIResources implements T8DefinitionResource
{
    public static final String OPERATION_API_ONT_COMM_DETERMINE_DR_COMMISSIONING_IMPACT = "@OS_API_ONT_COMM_DETERMINE_DR_COMMISSIONING_IMPACT";
    public static final String OPERATION_API_ONT_COMM_DETERMINE_ONTOLOGY_COMMISSIONING_IMPACT = "@OS_API_ONT_COMM_DETERMINE_ONTOLOGY_COMMISSIONING_IMPACT";

    public static final String PARAMETER_DATA_REQUIREMENT_CHANGE_REQUEST = "$P_DATA_REQUIREMENT_CHANGE_REQUEST";
    public static final String PARAMETER_ONTOLOGY_CHANGE_REQUEST = "$P_ONTOLOGY_CHANGE_REQUEST";
    public static final String PARAMETER_COMMISSIONING_IMPACT_REPORT = "$P_COMMISSIONING_IMPACT_REPORT";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_COMM_DETERMINE_DR_COMMISSIONING_IMPACT);
            definition.setMetaDisplayName("Determine DR Commissioning Impact");
            definition.setMetaDescription("Calculates the impact of the proposed Data Requirement change on the existing data in the system.");
            definition.setClassName("com.pilog.t8.data.ontology.commission.T8OntologyCommissioningAPIOperations$APIDetermineDRCommissioningImpact");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_REQUIREMENT_CHANGE_REQUEST, "Change Request", "The Data Requirement changes to be commissioned.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMISSIONING_IMPACT_REPORT, "Impact Report", "A report detailing the impact of the proposed DR change.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_COMM_DETERMINE_ONTOLOGY_COMMISSIONING_IMPACT);
            definition.setMetaDisplayName("Determine Ontology Commissioning Impact");
            definition.setMetaDescription("Calculates the impact of the proposed Ontology changes on the existing data in the system.");
            definition.setClassName("com.pilog.t8.data.ontology.commission.T8OntologyCommissioningAPIOperations$APIDetermineOntologyCommissioningImpact");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ONTOLOGY_CHANGE_REQUEST, "Change Request", "The request object containing all of the proposed ontology changes.", T8DataType.CUSTOM_OBJECT));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMISSIONING_IMPACT_REPORT, "Impact Report", "A report detailing the impact of the proposed ontology changes.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
