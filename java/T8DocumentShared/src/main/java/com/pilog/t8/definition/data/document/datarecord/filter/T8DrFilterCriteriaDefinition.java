package com.pilog.t8.definition.data.document.datarecord.filter;

import com.pilog.t8.data.document.datarecord.filter.T8DrFilterCriteria;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pieter Strydom
 */
public class T8DrFilterCriteriaDefinition extends T8RecordFilterClauseDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RECORD_FILTER_DR_CRITERIA";
    public static final String DISPLAY_NAME = "DR Filter Criteria";
    public static final String DESCRIPTION = "The record filter criteria applicable to a specific Data Requirement.";
    private enum Datum {DR_ID,
                        DISPLAY_NAME,
                        ALLOW_CODE_ADDITION,
                        ALLOW_PROPERTY_ADDITION,
                        ALLOW_HISTORY_ADDITION,
                        ALLOW_REMOVAL,
                        ALLOW_INSTANCE_SELECTION,
                        INSTANCE_SELECTION_LABEL,
                        INSTANCE_OPERATOR,
                        CODE_CRITERION_DEFINITIONS,
                        PROPERTY_CRITERION_DEFINITIONS,
                        ORGANIZATION_CRITERION_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DrFilterCriteriaDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.DR_ID.toString(), "Data Requirement Id", "The concept ID of the Data Requirement to which this criteria is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.DISPLAY_NAME.toString(), "Display Name", "The display name of this set of criteria."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_CODE_ADDITION.toString(), "Allow Code Addition", "If enabled, allows the addition of more code criteria to this set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_PROPERTY_ADDITION.toString(), "Allow Property Addition", "If enabled, allows the addition of more property criteria to this set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_HISTORY_ADDITION.toString(), "Allow History Addition", "If enabled, allows the addition of more history criteria to this set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_REMOVAL.toString(), "Allow Removal", "If enabled, allows the removal of this set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_INSTANCE_SELECTION.toString(), "Allow Instance Selection", "If enabled, allows the selection of the different instances for this Dr.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INSTANCE_SELECTION_LABEL.toString(), "Instance Selection Label", "The label that will be used when the instance selection is enabled."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INSTANCE_OPERATOR.toString(), "Instance Selection Operator", "The operator used by this criterion.", DataFilterOperator.EQUAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_LIST, Datum.CODE_CRITERION_DEFINITIONS.toString(), "Code Criteria", "The code criteria defined in context of this Data Requirement Instance filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_LIST, Datum.PROPERTY_CRITERION_DEFINITIONS.toString(), "Property Criteria", "The property criteria defined in context of this Data Requirement Instance filter."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_LIST, Datum.ORGANIZATION_CRITERION_DEFINITIONS.toString(), "Organization Criteria", "The organization criteria defined in context of this Data Requirement Instance filter."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CODE_CRITERION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8CodeFilterCriterionDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROPERTY_CRITERION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8PropertyFilterCriterionDefinition.GROUP_IDENTIFIER));
        else if (Datum.ORGANIZATION_CRITERION_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8OrganizationFilterCriterionDefinition.GROUP_IDENTIFIER));
        else if (Datum.INSTANCE_OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(DataFilterOperator.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8RecordFilterClause getRecordFilterClauseInstance(T8Context context)
    {
        return getDrFilterCriteriaInstance(context);
    }

    public T8DrFilterCriteria getDrFilterCriteriaInstance(T8Context context)
    {
        T8DrFilterCriteria criteria;

        criteria = new T8DrFilterCriteria(getDrId());
        criteria.setDisplayName(getCriteriaDisplayName());
        criteria.setInstanceOperator(getInstanceOperator());
        criteria.setInstanceLabel(getInstanceSelectionLabel());
        criteria.setAllowPropertyAddition(isAllowPropertyAddition());
        criteria.setAllowHistoryAddition(isAllowHistoryAddition());
        criteria.setAllowRemoval(isAllowRemoval());
        criteria.setAllowInstanceSelection(isAllowInstanceSelection());

        for (T8CodeFilterCriterionDefinition criterionDefinition : getCodeCriterionDefinitions())
        {
            criteria.addCodeCriterion(criterionDefinition.getCodeFilterCriterionInstance(context));
        }

        for (T8PropertyFilterCriterionDefinition criterionDefinition : getPropertyCriterionDefinitions())
        {
            criteria.addPropertyCriterion(criterionDefinition.getPropertyFilterCriterionInstance(context));
        }

        for (T8OrganizationFilterCriterionDefinition criterionDefinition : getOrganizationCriterionDefinitions())
        {
            criteria.addOrganizationCriterion(criterionDefinition.getOrganizationFilterCriterionInstance(context));
        }

        return criteria;
    }

    public String getDrId()
    {
        return (String)getDefinitionDatum(Datum.DR_ID.toString());
    }

    public void setDrId(String id)
    {
        setDefinitionDatum(Datum.DR_ID.toString(), id);
    }

    public DataFilterOperator getInstanceOperator()
    {
        String stringValue;

        stringValue = (String)getDefinitionDatum(Datum.INSTANCE_OPERATOR.toString());
        if (Strings.isNullOrEmpty(stringValue)) return DataFilterOperator.EQUAL;
        else return DataFilterOperator.valueOf(stringValue);
    }

    public void setInstanceOperator(DataFilterOperator operator)
    {
        setDefinitionDatum(Datum.INSTANCE_OPERATOR.toString(), operator.toString());
    }

    public String getInstanceSelectionLabel()
    {
        return (String)getDefinitionDatum(Datum.INSTANCE_SELECTION_LABEL.toString());
    }

    public void setInstanceSelectionLabel(String label)
    {
        setDefinitionDatum(Datum.INSTANCE_SELECTION_LABEL.toString(), label);
    }

    public String getCriteriaDisplayName()
    {
        return (String)getDefinitionDatum(Datum.DISPLAY_NAME.toString());
    }

    public void setCriteriaDisplayName(String displayName)
    {
        setDefinitionDatum(Datum.DISPLAY_NAME.toString(), displayName);
    }

    public boolean isAllowCodeAddition()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_CODE_ADDITION.toString());
        return value != null && value;
    }

    public void setAllowCodeAddition(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_CODE_ADDITION.toString(), false);
    }

    public boolean isAllowPropertyAddition()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_PROPERTY_ADDITION.toString());
        return value != null && value;
    }

    public void setAllowPropertyAddition(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_PROPERTY_ADDITION.toString(), false);
    }

    public boolean isAllowHistoryAddition()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_HISTORY_ADDITION.toString());
        return value != null && value;
    }

    public void setAllowHistoryAddition(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_HISTORY_ADDITION.toString(), false);
    }

    public boolean isAllowRemoval()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_REMOVAL.toString());
        return value != null && value;
    }

    public void setAllowRemoval(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_REMOVAL.toString(), false);
    }

    public boolean isAllowInstanceSelection()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_INSTANCE_SELECTION.toString());
        return value != null && value;
    }

    public void setAllowInstanceSelection(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_INSTANCE_SELECTION.toString(), allow);
    }

    public List<T8CodeFilterCriterionDefinition> getCodeCriterionDefinitions()
    {
        return (List<T8CodeFilterCriterionDefinition>)getDefinitionDatum(Datum.CODE_CRITERION_DEFINITIONS.toString());
    }

    public void setCodeCriterionDefinitions(List<T8CodeFilterCriterionDefinition> definitions)
    {
        setDefinitionDatum(Datum.CODE_CRITERION_DEFINITIONS.toString(), definitions);
    }

    public List<T8PropertyFilterCriterionDefinition> getPropertyCriterionDefinitions()
    {
        return (List<T8PropertyFilterCriterionDefinition>)getDefinitionDatum(Datum.PROPERTY_CRITERION_DEFINITIONS.toString());
    }

    public void setPropertyCriterionDefinitions(List<T8PropertyFilterCriterionDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_CRITERION_DEFINITIONS.toString(), definitions);
    }

    public List<T8OrganizationFilterCriterionDefinition> getOrganizationCriterionDefinitions()
    {
        return (List<T8OrganizationFilterCriterionDefinition>)getDefinitionDatum(Datum.ORGANIZATION_CRITERION_DEFINITIONS.toString());
    }

    public void setOrganizationCriterionDefinitions(List<T8OrganizationFilterCriterionDefinition> definitions)
    {
        setDefinitionDatum(Datum.ORGANIZATION_CRITERION_DEFINITIONS.toString(), definitions);
    }
}
