package com.pilog.t8.api.datarecordeditor;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileRecordDeletion implements Serializable
{
    private String recordId;
    private String propertyId;
    private String subRecordId;

    public T8DataFileRecordDeletion()
    {
    }

    public T8DataFileRecordDeletion(String recordId, String propertyId, String subRecordId)
    {
        this();
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.subRecordId = subRecordId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getSubRecordId()
    {
        return subRecordId;
    }

    public void setSubRecordId(String subRecordId)
    {
        this.subRecordId = subRecordId;
    }
}
