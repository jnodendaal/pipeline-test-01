package com.pilog.t8.data.document.path;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.path.PathStepGroup.GroupOperator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import com.pilog.t8.data.object.T8DataObjectProvider;

/**
 * @author Bouwer du Preez
 */
public class DocPathExpressionEvaluator
{
    private final List<PathStepGroup> pathStepGroups;
    private boolean singleValue;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private DataRecordProvider recordProvider;
    private T8DataObjectProvider dataObjectProvider;

    public DocPathExpressionEvaluator()
    {
        pathStepGroups = new ArrayList<PathStepGroup>();
    }

    public void setLanguage(String languageId)
    {
        if (terminologyProvider != null)
        {
            terminologyProvider.setLanguage(languageId);
        }
    }

    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
        for (PathStepGroup group : pathStepGroups)
        {
            group.setTerminologyProvider(terminologyProvider);
        }
    }

    public void setOntologyProvider(OntologyProvider ontologyProvider)
    {
        this.ontologyProvider = ontologyProvider;
        for (PathStepGroup group : pathStepGroups)
        {
            group.setOntologyProvider(ontologyProvider);
        }
    }

    public void setRecordProvider(DataRecordProvider recordProvider)
    {
        this.recordProvider = recordProvider;
        for (PathStepGroup group : pathStepGroups)
        {
            group.setRecordProvider(recordProvider);
        }
    }

    public void setDataObjectProvider(T8DataObjectProvider dataObjectProvider)
    {
        this.dataObjectProvider = dataObjectProvider;
        for (PathStepGroup group : pathStepGroups)
        {
            group.setDataObjectProvider(dataObjectProvider);
        }
    }

    public void printPathSteps()
    {
        for (PathStepGroup group : pathStepGroups)
        {
            System.out.println(group + "\n");
        }
    }

    public Object evaluateExpression(Object documentObject, String expression)
    {
        parseExpression(expression);
        return evaluateExpression(documentObject);
    }

    public Object evaluateExpression(Object rootInput)
    {
        if (!pathStepGroups.isEmpty())
        {
            Object result;

            result = rootInput;
            for (PathStepGroup group : pathStepGroups)
            {
                result = group.evaluateGroup(rootInput, result);
            }

            if (singleValue)
            {
                if (result instanceof List)
                {
                    List resultList;

                    resultList = (List)result;
                    if (resultList.size() > 0) return resultList.get(0);
                    else return null;
                }
                else return null;
            }
            else return result;
        }
        else throw new RuntimeException("No expression has been parsed for execution.");
    }

    public void parseExpression(String expression)
    {
        pathStepGroups.clear();
        pathStepGroups.addAll(parseGroups(expression));
    }

    private List<PathStepGroup> parseGroups(String inputExpression)
    {
        ArrayList<PathStepGroup> groups;
        GroupOperator operator;

        // Create the list of groups.
        operator = GroupOperator.DEFAULT;
        groups = new ArrayList<PathStepGroup>();
        if (inputExpression.startsWith("("))
        {
            String expression;

            // While we have groups left in the expression, keep parsing them.
            expression = inputExpression;
            while (expression.startsWith("("))
            {
                int closingIndex;

                // Find the index of the closing bracket.
                closingIndex = Strings.findClosingBracket(expression, 0);
                if (closingIndex > 0)
                {
                    PathStepGroup group;
                    String groupExpression;

                    // Get the group expression string and parse it.
                    groupExpression = expression.substring(1, closingIndex);
                    group = new PathStepGroup(operator, terminologyProvider, ontologyProvider, recordProvider, dataObjectProvider);
                    group.parseExpression(groupExpression);
                    groups.add(group);

                    // Remove the already parsed group from the expression string.
                    if (expression.length() > closingIndex)
                    {
                        expression = expression.substring(closingIndex + 1);
                        if (expression.length() > 0)
                        {
                            if ((expression.startsWith("??")) && (expression.length() > 2))
                            {
                                operator = GroupOperator.COALESCE;
                                expression = expression.substring(2);
                            }
                            else throw new RuntimeException("Invalid start of group expression : " + expression);
                        }
                    }
                    else expression = "";
                }
                else throw new RuntimeException("No matching closing bracket found for opening bracket '(' in expression: " + expression);
            }

            return groups;
        }
        else
        {
            PathStepGroup group;

            group = new PathStepGroup(operator, terminologyProvider, ontologyProvider, recordProvider, dataObjectProvider);
            group.parseExpression(inputExpression);
            groups.add(group);
            return groups;
        }
    }
}
