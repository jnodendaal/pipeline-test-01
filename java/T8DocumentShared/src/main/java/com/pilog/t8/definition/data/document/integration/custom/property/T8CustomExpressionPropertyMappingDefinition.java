/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.document.integration.custom.property;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Andre Scheepers
 */
public class T8CustomExpressionPropertyMappingDefinition extends T8CustomPropertyMappingDefinition
{
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_CUSTOM_EXPRESSION_PROPERTY_MAPPING";
    public static final String DISPLAY_NAME = "Custom Expression Property Mapping";
    public static final String DESCRIPTION = "A Custom Expression Property mapping to the system which the Integration process will map to.";   
    
    public enum Datum
    {
        ENABLED_EXPRESSION,
        DEFAULT_VALUE_EXPRESSION,
        DOC_PATH_EXPRESSION,
        EPIC_EXPRESSION, 
        SEQUENCE
    };
    // -------- Definition Meta-Data -------- //

    public T8CustomExpressionPropertyMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_EXPRESSION.toString(), "Enabled Expression", "The xpression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DEFAULT_VALUE_EXPRESSION.toString(), "Defualt Value Expression", "The xpression that will be evaluated to determine the default value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOC_PATH_EXPRESSION.toString(), "Doc Path Expression", "The doc path expression that will be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.EPIC_EXPRESSION.toString(), "Epic Expression", "The EPIC expression that will be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEQUENCE.toString(), "Sequence", "The sequence of the proeprties in the string value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return null;
    }

    public <T extends Object> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.property.mapping.ExpressionPropertyMapping").getConstructor(T8ServerContext.class, T8SessionContext.class, T8CustomExpressionPropertyMappingDefinition.class);
            return (DataRecordIntegrationPropertyMapper<T>) constructor.newInstance(serverContext, sessionContext, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new property mapper instance", ex);
            return null;
        }
    }

    public String getEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.ENABLED_EXPRESSION.toString());
    }

    public void setEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_EXPRESSION.toString(), expression);
    }

    public String getDefaultValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString());
    }

    public void setDefaultValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString(), expression);
    }    

    public String getDocPathExpression()
    {
        return (String)getDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString());
    }

    public void setDocPathExpression(String propertyID)
    {
        setDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString(), propertyID);
    }

    public String getEpicExpression()
    {
        return (String)getDefinitionDatum(Datum.EPIC_EXPRESSION.toString());
    }

    public void setEpicExpression(String propertyID)
    {
        setDefinitionDatum(Datum.EPIC_EXPRESSION.toString(), propertyID);
    }

    public String getSequence()
    {
        return (String)getDefinitionDatum(Datum.SEQUENCE.toString());
    }

    public void setSequence(String sequence)
    {
        setDefinitionDatum(Datum.SEQUENCE.toString(), sequence);
    }
}
