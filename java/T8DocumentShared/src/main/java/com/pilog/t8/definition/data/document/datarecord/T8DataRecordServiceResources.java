package com.pilog.t8.definition.data.document.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.service.T8ServiceOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordServiceResources implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_TEST = "@OP_SRV_REC_TEST";

    public static final String PARAMETER_TEST = "$P_TEST";

    public static final String DATA_RECORD_ASYNC_ONT_UPD_DE_ID = "@E_DATA_RECORD_ASYNC_ONT_UPD";
    public static final String DATA_RECORD_ASYNC_ONT_UPD_DS_ID = "@DS_DATA_RECORD_ASYNC_ONT_UPD";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        T8SQLDataSourceDefinition sqlDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // DATA_RECORD_ASYNC_ONT_UPD Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_RECORD_ASYNC_ONT_UPD_DS_ID);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	a.ROOT_RECORD_ID,\n" +
            "	MAX(a.ASYNC_ONT_UPD_TIME) AS ASYNC_ONT_UPD_TIME\n" +
            "FROM DATA_RECORD_DOC a\n" +
            "WHERE a.ASYNC_ONT_UPD_TIME IS NOT NULL\n" +
            "GROUP BY a.ROOT_RECORD_ID "
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ASYNC_ONT_UPD_TIME, "ASYNC_ONT_UPD_TIME", false, true, T8DataType.TIMESTAMP));

        definitions.add(sqlDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // DATA_RECORD_ASYNC_ONT_UPD Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_ASYNC_ONT_UPD_DE_ID);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_ASYNC_ONT_UPD_DS_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, DATA_RECORD_ASYNC_ONT_UPD_DS_ID + EF_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ASYNC_ONT_UPD_TIME, DATA_RECORD_ASYNC_ONT_UPD_DS_ID + EF_ASYNC_ONT_UPD_TIME, false, true, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        return definitions;
    }

    public static final ArrayList<T8ServiceOperationDefinition> getOperationDefinitions()
    {
        T8ServiceOperationDefinition definition;
        ArrayList<T8ServiceOperationDefinition> definitions;

        definitions = new ArrayList<>();

        definition = new T8ServiceOperationDefinition(OPERATION_TEST);
        definition.setMetaDescription("Test Service Operation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TEST, "Test", "Test.", T8DataType.LIST));
        definitions.add(definition);

        return definitions;
    }
}
