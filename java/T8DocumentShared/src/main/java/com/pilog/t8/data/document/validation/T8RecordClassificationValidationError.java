package com.pilog.t8.data.document.validation;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.Requirement;

/**
 * @author Bouwer du Preez
 */
public class T8RecordClassificationValidationError extends T8DataValidationError
{
    private String ontologyStructureId;
    private String ontologyClassId;

    public T8RecordClassificationValidationError(ValidationErrorType errorType, String recordId, String osId, String ocId, String errorMessage)
    {
        super(errorType, recordId, errorMessage);
        this.ontologyStructureId = osId;
        this.ontologyClassId = ocId;
    }

    public String getOntologyStructureId()
    {
        return ontologyStructureId;
    }

    public void setOntologyStuctureId(String osId)
    {
        this.ontologyStructureId = osId;
    }

    public String getOntologyClassId()
    {
        return ontologyClassId;
    }

    public void setOntologyClassId(String ocId)
    {
        this.ontologyClassId = ocId;
    }

    @Override
    public boolean isApplicableTo(Requirement requirement)
    {
        return false;
    }

    @Override
    public boolean isApplicableTo(Value value)
    {
        if (value instanceof DataRecord)
        {
            return recordId.equals(((DataRecord)value).getID());
        }
        else return false;
    }

    @Override
    public String toString()
    {
        return "T8RecordClassificationError{" + "errorType=" + errorType + ", recordID=" + recordId + ", ontologyStructureId=" + ontologyStructureId + ", ontologyClassId=" + ontologyClassId + ", errorMessage=" + errorMessage + '}';
    }
}
