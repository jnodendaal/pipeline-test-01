package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.ontology.T8ConceptPair;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class UnitOfMeasure extends DependencyValue implements ConceptValue
{
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;

    public UnitOfMeasure(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public UnitOfMeasure(ValueRequirement valueRequirement, String valueConceptID)
    {
        super(null, valueRequirement, valueConceptID);
    }

    @Override
    public UnitOfMeasure copy()
    {
        UnitOfMeasure copy;

        copy = (UnitOfMeasure)super.copy();
        copy.setTermId(termId);
        copy.setTerm(term);
        copy.setCodeId(codeId);
        copy.setCode(code);
        copy.setDefinitionId(definitionId);
        copy.setDefinition(definition);
        copy.setAbbreviationId(abbreviationId);
        copy.setAbbreviation(abbreviation);
        copy.setLanguageId(languageId);
        return copy;
    }

    public void setConceptID(String uomID)
    {
        setValue(uomID);
    }

    public String getConceptID()
    {
        return getValue();
    }

    @Override
    public void setConceptId(String uomId, boolean clearDependentValues)
    {
        setValue(uomId);
    }

    @Override
    public String getConceptId()
    {
        return getValue();
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCodeId()
    {
        return codeId;
    }

    public void setCodeId(String codeId)
    {
        this.codeId = codeId;
    }

    public String getTermId()
    {
        return termId;
    }

    public void setTermId(String termId)
    {
        this.termId = termId;
    }

    @Override
    public String getTerm()
    {
        return term;
    }

    @Override
    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinitionId()
    {
        return definitionId;
    }

    public void setDefinitionId(String definitionId)
    {
        this.definitionId = definitionId;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviationId()
    {
        return abbreviationId;
    }

    public void setAbbreviationId(String abbreviationId)
    {
        this.abbreviationId = abbreviationId;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    @Override
    public UnitOfMeasureContent getContent()
    {
        UnitOfMeasureContent content;

        content = new UnitOfMeasureContent();
        content.setConceptId(getConceptId());
        content.setTermId(termId);
        content.setTerm(term);
        content.setDefinitionId(definitionId);
        content.setDefinition(definition);
        content.setCodeId(codeId);
        content.setCode(code);
        content.setAbbreviationId(abbreviationId);
        content.setAbbreviation(abbreviation);
        content.setLanguageId(languageId);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        UnitOfMeasureContent content;

        content = (UnitOfMeasureContent)valueContent;
        setConceptId(content.getConceptId(), false);
        setTermId(content.getTermId());
        setTerm(content.getTerm());
        setCodeId(content.getCodeId());
        setCode(content.getCode());
        setDefinitionId(content.getDefinitionId());
        setDefinition(content.getDefinition());
        setAbbreviationId(content.getAbbreviationId());
        setAbbreviation(content.getAbbreviation());
        setLanguageId(content.getLanguageId());
    }

    public List<RecordValue> clearDependentValues()
    {
        List<RecordValue> removedValues;
        List<DependencyValue> dependentValues;

        // Create the list of removed values.
        removedValues = getSubValues();
        dependentValues = getDependentValues();
        for (RecordValue dependentValue : dependentValues)
        {
            RecordValue dependentParent;

            dependentParent = dependentValue.getParentRecordValue();
            if (dependentParent instanceof ControlledConcept)
            {
                removedValues.addAll(((ControlledConcept)dependentParent).clearConcept(true));
            }
            else if (dependentParent instanceof DocumentReferenceList)
            {
                removedValues.addAll(((DocumentReferenceList)dependentParent).clearReferences(true));
            }
            else throw new RuntimeException("Invalid dependent value type found: " + dependentValue);
        }

        // Return the list of removed values.
        return removedValues;
    }

    /**
     * Returns the list of concept graph edges defining the concept links
     * between this value and its dependants.
     * @return The list of concept graph edges defining the concept links
     * between this value and its dependants.
     */
    public List<T8ConceptPair> getDependantLinks()
    {
        List<DependencyValue> dependentValues;
        List<T8ConceptPair> links;

        // Create a list of all paths linking this value to its dependants.
        links = new ArrayList<T8ConceptPair>();
        dependentValues = getDependentValues();
        for (DependencyValue dependentValue : dependentValues)
        {
            DataDependency dependency;
            T8ConceptPair link;
            String tailConceptID;
            List<String> headConceptIDList;

            // Get the head and tail concept ID's.
            tailConceptID = dependentValue.findDependencyConceptId(this, dependentValue, null);
            headConceptIDList = dependentValue.findDependentConceptIdList(this, null);

            // Get the dependency linking this value to the dependant.
            dependency = dependentValue.getDataDependencyOn(this);
            for (String headConceptID : headConceptIDList)
            {
                link = new T8ConceptPair(dependency.getRelationId(), tailConceptID, headConceptID);
                links.add(link);
            }
        }

        // Return the list of results.
        return links;
    }

    /**
     * Returns the concept id's from this record value, that are dependent on
     * the specified parent value (head concepts).
     * @param parentValue The record value on which the returned concept ID's
     * are dependent.
     * @param recordProvider The record provider to use in case a required
     * document cannot be found in the current object model.
     * @return The concept id's from this record value, that are dependent on
     * the specified parent value (head concepts).
     */
    @Override
    public List<String> findDependentConceptIdList(RecordValue parentValue, DataRecordProvider recordProvider)
    {
        return Arrays.asList(getConceptID());
    }

    @Override
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        String conceptId;

        conceptId = getConceptId();
        if (conceptId != null)
        {
            T8ConceptTerminology terminology;

            super.setContentTerminology(terminologyProvider);
            terminology = terminologyProvider.getTerminology(null, getConceptId());
            if (terminology != null)
            {
                this.languageId = terminology.getLanguageId();
                this.codeId = terminology.getCodeId();
                this.code = terminology.getCode();
                this.termId = terminology.getTermId();
                this.term = terminology.getTerm();
                this.definitionId = terminology.getDefinitionId();
                this.definition = terminology.getDefinition();
                this.abbreviationId = terminology.getAbbreviationId();
                this.abbreviation = terminology.getAbbreviation();
            }
            else
            {
                this.languageId = null;
                this.codeId = null;
                this.code = null;
                this.termId = null;
                this.term = null;
                this.definitionId = null;
                this.definition = null;
                this.abbreviationId = null;
                this.abbreviation = null;
            }
        }
    }

    @Override
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        String conceptId;

        // Do the super implementation first.
        super.addContentTerminology(terminologyCollector);

        // If a concept id is available for this uom, add its terminology.
        conceptId = getConceptId();
        if (conceptId != null)
        {
            terminologyCollector.addTerminology(languageId, T8OntologyConceptType.UNIT_OF_MEASURE, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);
        }
    }
}
