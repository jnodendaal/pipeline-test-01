package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileNewRecord;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordCreation;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.T8DocumentSerializer;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileNewRecord extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE_NEW_RECORD";

    public T8DtDataFileNewRecord(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileRecordCreation.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFileNewRecord newRecord;
            JsonObject jsonNewRecord;

            newRecord = (T8DataFileNewRecord)object;

            // Create the json object.
            jsonNewRecord = new JsonObject();
            jsonNewRecord.add("recordId", newRecord.getRecordId());
            jsonNewRecord.add("propertyId", newRecord.getPropertyId());
            jsonNewRecord.add("fieldId", newRecord.getFieldId());
            jsonNewRecord.add("temporaryId", newRecord.getTemporaryId());
            jsonNewRecord.add("newRecord", T8DocumentSerializer.serializeDataRecord(newRecord.getNewRecord(), true, true, true));
            return jsonNewRecord;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileNewRecord deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                T8DataFileNewRecord newRecord;
                JsonObject jsonNewRecord;

                jsonNewRecord = (JsonObject)jsonValue;
                newRecord = new T8DataFileNewRecord(null, null);
                newRecord.setRecordId(jsonNewRecord.getString("recordId"));
                newRecord.setPropertyId(jsonNewRecord.getString("propertyId"));
                newRecord.setFieldId(jsonNewRecord.getString("fieldId"));
                newRecord.setTemporaryId(jsonNewRecord.getString("temporaryId"));
                newRecord.setNewRecord(T8DocumentSerializer.deserializeDataRecord(jsonNewRecord.getJsonObject("newRecord")));
                return newRecord;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while parsing input data record from JSON: " + jsonValue, e);
            }
        }
        else return null;
    }
}
