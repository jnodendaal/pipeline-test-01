package com.pilog.t8.data.document.datastring;

import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface T8DataStringFormatProvider
{
    public T8DataStringFormat getDataStringFormat(T8DataStringKey key);
    public T8DataStringParticleSettingsList getDataStringParticleSettings(List<T8DataStringKey> keys);
}
