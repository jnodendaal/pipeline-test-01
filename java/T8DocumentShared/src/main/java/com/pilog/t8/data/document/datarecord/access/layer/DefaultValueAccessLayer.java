package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * This class is a placeholder for those values for which no specific access layer implementation exists yet.
 *
 * @author Bouwer du Preez
 */
public class DefaultValueAccessLayer extends ValueAccessLayer
{
    public DefaultValueAccessLayer(RequirementType requirementType)
    {
        super(requirementType);
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer value)
    {
        return true;
    }

    @Override
    public ValueAccessLayer copy()
    {
        return new DefaultValueAccessLayer(requirementType);
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
    }
}
