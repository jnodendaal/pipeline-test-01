package com.pilog.t8.definition.data.document.integration.property;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.definition.data.document.integration.T8DataRecordPropertyMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8ExpressionPropertyMappingDefinition extends T8DataRecordPropertyMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_PROPERTY_EXPRESSION";
    public static final String DISPLAY_NAME = "Expression Property Mapping";
    public static final String DESCRIPTION = "A Data Record property mapping that defines how properties for this record will be mapped";

    public enum Datum
    {
        INTERFACE_TERM,
        ENABLED_EXPRESSION,
        DEFAULT_VALUE_EXPRESSION,
        DOC_PATH_EXPRESSION,
        EPIC_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public T8ExpressionPropertyMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INTERFACE_TERM.toString(), "Interface Term", "The term that will be assigned to this property on the interface."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_EXPRESSION.toString(), "Enabled Expression", "The xpression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DEFAULT_VALUE_EXPRESSION.toString(), "Default Value Expression", "The expression that will be evaluated to determine the default value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOC_PATH_EXPRESSION.toString(), "Doc Path Expression", "The doc path expression that will be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.EPIC_EXPRESSION.toString(), "Epic Expression", "The EPIC expression that will be evaluated."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public <T extends Object> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.property.mapping.ExpressionPropertyMapping").getConstructor(T8Context.class, T8ExpressionPropertyMappingDefinition.class);
            return (DataRecordIntegrationPropertyMapper<T>) constructor.newInstance(context, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new property mapper instance", ex);
            return null;
        }
    }

    public String getInterfaceTerm()
    {
        return (String)getDefinitionDatum(Datum.INTERFACE_TERM.toString());
    }

    public void setInterfaceTerm(String term)
    {
        setDefinitionDatum(Datum.INTERFACE_TERM.toString(), term);
    }

    public String getEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.ENABLED_EXPRESSION.toString());
    }

    public void setEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_EXPRESSION.toString(), expression);
    }

    public String getDefaultValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString());
    }

    public void setDefaultValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString(), expression);
    }

    public String getDocPathExpression()
    {
        return (String)getDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString());
    }

    public void setDocPathExpression(String propertyID)
    {
        setDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString(), propertyID);
    }

    public String getEpicExpression()
    {
        return (String)getDefinitionDatum(Datum.EPIC_EXPRESSION.toString());
    }

    public void setEpicExpression(String propertyID)
    {
        setDefinitionDatum(Datum.EPIC_EXPRESSION.toString(), propertyID);
    }

}
