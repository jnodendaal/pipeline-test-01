package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class DRInstanceDependencyRequirement extends ValueRequirement
{
    public DRInstanceDependencyRequirement()
    {
        super(RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE);
    }
}
