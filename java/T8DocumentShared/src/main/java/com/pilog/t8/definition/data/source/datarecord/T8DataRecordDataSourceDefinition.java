package com.pilog.t8.definition.data.source.datarecord;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8SQLQueryDataSourceDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordDataSourceDefinition extends T8DataSourceDefinition implements T8SQLQueryDataSourceDefinition, T8DataConnectionBasedSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATA_RECORD";
    public static final String DISPLAY_NAME = "Data Record Data Source";
    public static final String DESCRIPTION = "A data source for accessing Data Record Documents.";
    public enum Datum
    {
        CONNECTION_IDENTIFIER,
        ROOT_LEVEL_SOURCE,
        DOCUMENT_TABLE_NAME,
        CLASS_TABLE_NAME,
        PROPERTY_TABLE_NAME,
        VALUE_TABLE_NAME,
        ATTACHMENT_TABLE_NAME,
        TAG_TABLE_NAME,
        ATTACHMENT_HANDLER_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public static final String KEY_FIELD_IDENTIFIER = "$ID";
    public static final String DOCUMENT_FIELD_IDENTIFIER = "$DOCUMENT";
    public static final String FILTER_FIELD_RECORD_VALUE_STRING = "$RECORD_VALUE_STRING";
    public static final String FILTER_FIELD_TAGS = "$TAGS";
    public static final String FILTER_FIELD_FFT = "$FFT";
    public static final String FILTER_FIELD_DR_ID = "$DR_ID";
    public static final String FILTER_FIELD_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String ORDER_FIELD_INSERTED_AT = "$UPDATED_AT";
    public static final String ORDER_FIELD_UPDATED_AT = "$INSERTED_AT";

    public static final String KEY_FIELD_SOURCE_IDENTIFIER = "RECORD_ID";
    public static final String DOCUMENT_FIELD_SOURCE_IDENTIFIER = "RECORD_DOCUMENT";
    public static final String FILTER_SOURCE_RECORD_VALUE_STRING = "VALUE_STRING";
    public static final String FILTER_SOURCE_FFT = "FFT";
    public static final String FILTER_SOURCE_DR_ID = "DR_ID";
    public static final String FILTER_SOURCE_DR_INSTANCE_ID = "DR_INSTANCE_ID";
    public static final String ORDER_SOURCE_INSERTED_AT = "UPDATED_AT";
    public static final String ORDER_SOURCE_UPDATED_AT = "INSERTED_AT";

    public T8DataRecordDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection this data source uses."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ROOT_LEVEL_SOURCE.toString(), "Root-level Source", "A Boolean flag indicating whether or not this source acts on Root-level documents (independent) only.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOCUMENT_TABLE_NAME.toString(), "Document Table Name", "The name of the database table where Data Record document header data is stored.", "DAT_REC"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PROPERTY_TABLE_NAME.toString(), "Property Table Name", "The name of the database table where Data Record property data is stored.", "DAT_REC_PRP"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.VALUE_TABLE_NAME.toString(), "Value Table Name", "The name of the database table where Data Record value data is stored.", "DAT_REC_VAL"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ATTACHMENT_TABLE_NAME.toString(), "Attachment Table Name", "The name of the database table where Data Record attachment data is stored.", "DAT_REC_ATC"));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ATTACHMENT_HANDLER_IDENTIFIER.toString(), "Attachment Handler", "The attachment handler to use for storing data record attachments."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else if (Datum.ATTACHMENT_HANDLER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataRecordAttachmentHandlerDefinition.GROUP_IDENTIFIER), true, "Default");
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction dataAccessProvider)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.source.datarecord.T8DataRecordDataSource", new Class<?>[]{T8DataRecordDataSourceDefinition.class, T8DataTransaction.class}, this, dataAccessProvider);
    }

    @Override
    public StringBuilder createQuery(T8DataTransaction dataAccessProvider, T8DataFilter dataFilter, Map<String, String> fieldMapping)
    {
        StringBuilder subQuery;

        subQuery = new StringBuilder("SELECT ");
        if ((fieldMapping != null) && (fieldMapping.size() > 0))
        {
            T8DataEntityDefinition dataEntityDefinition;
            T8DataSourceDefinition dataSourceDefinition;
            Iterator<String> fieldIterator;

            fieldIterator = fieldMapping.keySet().iterator();
            while (fieldIterator.hasNext())
            {
                String fieldIdentifier;
                String fieldSourceIdentifier;

                fieldIdentifier = fieldIterator.next();
                dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier));
                dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
                fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
                subQuery.append(fieldSourceIdentifier);
                subQuery.append(" AS ");

                fieldIdentifier = fieldMapping.get(fieldIdentifier);
                dataEntityDefinition = dataAccessProvider.getDataEntityDefinition(T8IdentifierUtilities.getGlobalIdentifierPart(fieldIdentifier));
                dataSourceDefinition = dataAccessProvider.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
                fieldSourceIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(fieldIdentifier);
                fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(fieldSourceIdentifier);
                subQuery.append(fieldSourceIdentifier);

                if (fieldIterator.hasNext()) subQuery.append(", ");
            }
        }
        else // No Field mapping so just retrieve all fields using original identifiers.
        {
            subQuery.append("*");
        }

        // We use a normal query approach because the actual data record is never retrieved when using this method.
        subQuery.append(" FROM DAT_REC ");
        if ((dataFilter != null) && (dataFilter.hasFilterCriteria()))
        {
            subQuery.append(" ");
            subQuery.append(dataFilter.getWhereClause(dataAccessProvider, "DAT_REC"));
        }

        return subQuery;
    }

    public boolean isRootLevelSource()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ROOT_LEVEL_SOURCE.toString());
        return ((value != null) && (value));
    }

    public void setRootLevelSource(boolean rootLevel)
    {
        setDefinitionDatum(Datum.ROOT_LEVEL_SOURCE.toString(), rootLevel);
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return getConnectionIdentifier();
    }

    public String getConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString());
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString(), connectionIdentifier);
    }

    public String getDocumentTableName()
    {
        return (String)getDefinitionDatum(Datum.DOCUMENT_TABLE_NAME.toString());
    }

    public void setDocumentTableName(String tableName)
    {
        setDefinitionDatum(Datum.DOCUMENT_TABLE_NAME.toString(), tableName);
    }

    public String getPropertyTableName()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_TABLE_NAME.toString());
    }

    public void setPropertyTableName(String tableName)
    {
        setDefinitionDatum(Datum.PROPERTY_TABLE_NAME.toString(), tableName);
    }

    public String getValueTableName()
    {
        return (String)getDefinitionDatum(Datum.VALUE_TABLE_NAME.toString());
    }

    public void setValueTableName(String tableName)
    {
        setDefinitionDatum(Datum.VALUE_TABLE_NAME.toString(), tableName);
    }

    public String getAttachmentTableName()
    {
        return (String)getDefinitionDatum(Datum.ATTACHMENT_TABLE_NAME.toString());
    }

    public void setAttachmentTableName(String tableName)
    {
        setDefinitionDatum(Datum.ATTACHMENT_TABLE_NAME.toString(), tableName);
    }

    public String getAttachmentHandlerIdentifier()
    {
        return (String)getDefinitionDatum(Datum.ATTACHMENT_HANDLER_IDENTIFIER.toString());
    }

    public void setAttachmentHandlerIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ATTACHMENT_HANDLER_IDENTIFIER.toString(), identifier);
    }
}