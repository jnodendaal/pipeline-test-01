package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.api.T8DataRecordEditorApiResource.*;
import static com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator.*;
import static com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordValueFilterHandler
{
    private final T8OrganizationStructure organizationStructure;
    private final T8OntologyStructure ontologyStructure;
    private final T8Context context;

    public T8DataRecordValueFilterHandler(T8Context context, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure)
    {
        this.context = context;
        this.organizationStructure = organizationStructure;
        this.ontologyStructure = ontologyStructure;
    }

    /**
     * Constructs a data filter used to retrieve concept values for a specific
     * record property using the supplied input parameters.
     * @param propertyRequirement The property requirement for which to
     * construct the filter.
     * @param entityId The entity to use.
     * @param conceptIdSet The set of Concept ID's (if any) to be added to the
     * filter on the CONCEPT_ID field.
     * @param rootOcIdSet The root groups (if any) to be added to the filter on
     * the ODT_ID field.
     * @param conceptDependencies A list of the concept dependencies to be used
     * in the filter (if any).  Each element in the list is a pair consisting of
     * the CONCEPT_GRAPH_ID and TAIL_CONCEPT_ID to use in the filter.  If the
     * TAIL_CONCEPT_ID is null, it will be disregarded and only the
     * CONCEPT_GRAPH_ID will be added to the filter, effectively limiting the
     * retrieval to any head concept in the specified graph.
     * @param restrictToApprovedValues A boolean flag indicating whether or not only
     * standardized values must be filtered.
     * @return The constructed filter.
     */
    public T8DataFilter getConceptFilter(PropertyRequirement propertyRequirement, String entityId, Set<String> conceptIdSet, Set<String> rootOcIdSet, List<Pair<String,String>> conceptDependencies, boolean restrictToApprovedValues)
    {
        T8DataFilterCriteria filterCriteria;
        Set<String> odtIDSet;
        Set<String> orgIDSet;
        List<T8OntologyLink> drInstanceLinks;
        T8OntologyConcept drInstanceOntology;
        String drInstanceOrgID;
        String drInstanceID;
        String drID;
        String propertyID;

        // Get the standardization flag from the property requirement.
        drInstanceID = propertyRequirement.getParentDataRequirementInstance().getConceptID();
        drID = propertyRequirement.getParentDataRequirement().getConceptID();
        propertyID = propertyRequirement.getConceptID();
        drInstanceOntology = (T8OntologyConcept)propertyRequirement.getParentDataRequirementInstance().getOntology();
        if (drInstanceOntology == null) throw new RuntimeException("No Ontology Found in DR Instance object: " + propertyRequirement.getParentDataRequirementInstance());
        drInstanceLinks = drInstanceOntology.getOntologyLinks();
        if (drInstanceLinks.isEmpty()) throw new RuntimeException("No Organization Links Found in DR Instance Ontology object: " + drInstanceOntology);
        drInstanceOrgID = drInstanceLinks.get(0).getOrganizationID();

        // Create a set of all groups to include (roots and their descendants).
        odtIDSet = new HashSet<String>();
        if (!CollectionUtilities.isNullOrEmpty(rootOcIdSet))
        {
            for (String odtID : rootOcIdSet)
            {
                // Get the list of ODT_ID's to use for retrieval.
                odtIDSet.add(odtID); // Add the group itself.
                odtIDSet.addAll(ontologyStructure.getDescendantClassIDList(odtID)); // Add the descendants of the group.
            }
        }

        // Create the list of accesible organization levels.
        orgIDSet = new HashSet<String>();
        orgIDSet.addAll(organizationStructure.getOrganizationLineageIDList(context.getSessionContext().getOrganizationIdentifier())); // Add a filter to include all concepts linked to the user's session organization lineage.
        orgIDSet.retainAll(organizationStructure.getOrganizationLineageIDList(drInstanceOrgID)); // Add a filter to include only concepts linked to the record's organization lineage.
        T8Log.log("Including organizations: " + orgIDSet);

        // Create a filter criteria.
        filterCriteria = new T8DataFilterCriteria();

        // Add filter criteria for the concepts to include (if any).
        if (!CollectionUtilities.isNullOrEmpty(conceptIdSet))
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.IN, new ArrayList<String>(conceptIdSet)));
        }

        // Add filter criteria for the value groups to include.
        if (!CollectionUtilities.isNullOrEmpty(odtIDSet))
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ODT_ID, T8DataFilterCriterion.DataFilterOperator.IN, new ArrayList<String>(odtIDSet)));
        }

        // Add filter criterion to include only active concepts.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ACTIVE_INDICATOR, T8DataFilterCriterion.DataFilterOperator.EQUAL, "Y"));

        // Add a filter to include all accsible organization levels.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ORG_ID, T8DataFilterCriterion.DataFilterOperator.IN, new ArrayList<String>(orgIDSet)));

        // Add concept dependency filters if required.
        if ((conceptDependencies != null) && (conceptDependencies.size() > 0))
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, createConceptDependencyFilters(entityId, conceptDependencies));
        }

        // If the property is standardized add the standardization checks to the filter.
        if (restrictToApprovedValues)
        {
            T8DataFilterCriteria standardizationFilterCriteria;
            T8DataFilterCriteria drInstanceFilterCriteria;

            // Create filter criteria to wrap all standardization requirements.
            standardizationFilterCriteria = new T8DataFilterCriteria();

            // Create filter criteria to filter rows with either a specific DR Instance ID of no DR Instance ID.
            drInstanceFilterCriteria = new T8DataFilterCriteria();
            drInstanceFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_STANDARDIZED_DR_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null));
            drInstanceFilterCriteria.addFilterClause(DataFilterConjunction.OR, new T8DataFilterCriterion(entityId + F_STANDARDIZED_DR_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, drInstanceID));

            // Add all the rest of the standardization field checks.
            standardizationFilterCriteria.addFilterClause(DataFilterConjunction.AND, drInstanceFilterCriteria);
            standardizationFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_STANDARDIZED_DR_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, drID));
            standardizationFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_STANDARDIZED_PROPERTY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, propertyID));

            // Add the standardization criteria to the main filter criteria object.
            filterCriteria.addFilterClause(DataFilterConjunction.AND, standardizationFilterCriteria);
        }

        // Create the filter using the filter criteria.
        return new T8DataFilter(entityId, filterCriteria);
    }

    public T8DataFilter createSuggestionConceptFilter(RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, String searchPrefix)
    {
        T8DataFilter dataFilter;

        // Create a normal filter without using the search prefix yet.
        dataFilter = this.createConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList);

        // If we have a valid search prefix, use it now.
        if (!Strings.isNullOrEmpty(searchPrefix))
        {
            dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_TERM, STARTS_WITH, searchPrefix, false);
            dataFilter.addFieldOrdering(E_CONCEPT_LOOKUP + F_TERM, T8DataFilter.OrderMethod.ASCENDING);
        }

        // Return the complete filter.
        return dataFilter;
    }

    public T8DataFilter createSearchConceptFilter(RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, String searchTerms, List<T8ConceptElementType> elementTypes)
    {
        T8DataFilterCriteria criteria;
        T8DataFilter dataFilter;

        // Create a normal filter without using the search string yet.
        dataFilter = this.createConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList);
        criteria = new T8DataFilterCriteria();
        dataFilter.addFilterCriteria(AND, criteria);

        // If a search on code is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.CODE))
        {
            criteria.addFilterCriterion(OR, E_CONCEPT_LOOKUP + F_CODE, LIKE, searchTerms, false);
        }

        // If a search on term is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.TERM))
        {
            criteria.addFilterCriterion(OR, E_CONCEPT_LOOKUP + F_TERM, LIKE, searchTerms, false);
        }

        // If a search on abbreviation is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.ABBREVIATION))
        {
            criteria.addFilterCriterion(OR, E_CONCEPT_LOOKUP + F_ABBREVIATION, LIKE, searchTerms, false);
        }

        // Return the complete filter.
        return dataFilter;
    }

    public T8DataFilter createExactMatchConceptFilter(RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, String matchString, List<T8ConceptElementType> elementTypes)
    {
        T8DataFilterCriteria criteria;
        T8DataFilter dataFilter;

        // Create a normal filter without using the search string yet.
        dataFilter = this.createConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList);
        criteria = new T8DataFilterCriteria();
        dataFilter.addFilterCriteria(AND, criteria);

        // If a search on code is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.CODE))
        {
            criteria.addFilterCriterion(OR, E_CONCEPT_LOOKUP + F_CODE, EQUAL, matchString, false);
        }

        // If a search on term is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.TERM))
        {
            criteria.addFilterCriterion(OR, E_CONCEPT_LOOKUP + F_TERM, EQUAL, matchString, false);
        }

        // If a search on abbreviation is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.ABBREVIATION))
        {
            criteria.addFilterCriterion(OR, E_CONCEPT_LOOKUP + F_ABBREVIATION, EQUAL, matchString, false);
        }

        // Return the complete filter.
        return dataFilter;
    }

    public T8DataFilter createConceptFilter(RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList)
    {
        // Do the setup of the dialog according to the input parameters.
        switch (requirementType)
        {
            case CONTROLLED_CONCEPT:
            case UNIT_OF_MEASURE:
            case QUALIFIER_OF_MEASURE:
            case DOCUMENT_REFERENCE:
                if (CollectionUtilities.isNullOrEmpty(conceptDependencyList))
                {
                    T8DataFilter dataFilter;

                    // Create the date filter to use for filtering of results.
                    dataFilter = new T8DataFilter(E_CONCEPT_LOOKUP);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_ROOT_ORG_ID, EQUAL, organizationStructure.getRootOrganizationID(), false);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_ORG_ID, IN, orgIdList, false);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_ODS_ID, EQUAL, ontologyStructure.getID(), false);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_ODT_ID, IN, ocIdList, false);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_LANGUAGE_ID, EQUAL, context.getSessionContext().getContentLanguageIdentifier(), false);
                    if (!CollectionUtilities.isNullOrEmpty(conceptIdList)) dataFilter.addFilterCriterion(AND, E_CONCEPT_LOOKUP + F_CONCEPT_ID, IN, conceptIdList, false);
                    return dataFilter;
                }
                else
                {
                    T8DataFilter dataFilter;

                    // Create the date filter to use for filtering of results.
                    dataFilter = new T8DataFilter(E_CONCEPT_RELATION_LOOKUP);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_RELATION_LOOKUP + F_ORG_ID, IN, orgIdList, false);
                    dataFilter.addFilterCriterion(AND, E_CONCEPT_RELATION_LOOKUP + F_ODT_ID, IN, ocIdList, false);
                    dataFilter.addFilterCriteria(AND, createConceptDependencyFilters(E_CONCEPT_RELATION_LOOKUP, conceptDependencyList));
                    if (!CollectionUtilities.isNullOrEmpty(conceptIdList)) dataFilter.addFilterCriterion(AND, E_CONCEPT_RELATION_LOOKUP + F_CONCEPT_ID, IN, conceptIdList, false);
                    return dataFilter;
                }
            default:
                throw new RuntimeException("Cannot create concept filter for Requirement Type: " + requirementType);
        }
    }

    public T8DataFilter createPrescribedValueFilter(String dataTypeId, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, String searchPrefix, boolean restrictToApprovedValues)
    {
        return this.createPrescribedValueFilter(RequirementType.valueOf(dataTypeId), drId, drInstanceId, propertyId, fieldId, conceptIdList, searchPrefix, restrictToApprovedValues);
    }

    public T8DataFilter createPrescribedValueFilter(RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, String searchPrefix, boolean restrictToApprovedValues)
    {
        T8DataFilter dataFilter;

        // Create a normal filter without using the search prefix yet.
        dataFilter = this.createPrescribedValueFilter(requirementType, drId, drInstanceId, propertyId, fieldId, conceptIdList, restrictToApprovedValues);

        // If we have a valid search prefix, use it now.
        if (!Strings.isNullOrEmpty(searchPrefix))
        {
            switch (requirementType)
            {
                case CONTROLLED_CONCEPT:
                    dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_TERM, STARTS_WITH, searchPrefix, false);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_APPROVED_INDICATOR, T8DataFilter.OrderMethod.DESCENDING);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_TERM, T8DataFilter.OrderMethod.ASCENDING);
                    return dataFilter;
                case UNIT_OF_MEASURE:
                    dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_UOM_TERM, STARTS_WITH, searchPrefix, false);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_APPROVED_INDICATOR, T8DataFilter.OrderMethod.DESCENDING);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_UOM_TERM, T8DataFilter.OrderMethod.ASCENDING);
                    return dataFilter;
                case QUALIFIER_OF_MEASURE:
                    dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_QOM_TERM, STARTS_WITH, searchPrefix, false);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_APPROVED_INDICATOR, T8DataFilter.OrderMethod.DESCENDING);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_QOM_TERM, T8DataFilter.OrderMethod.ASCENDING);
                    return dataFilter;
                case MEASURED_RANGE:
                    dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_LOWER_BOUND_VALUE, STARTS_WITH, searchPrefix, false);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_APPROVED_INDICATOR, T8DataFilter.OrderMethod.DESCENDING);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_LOWER_BOUND_VALUE, T8DataFilter.OrderMethod.ASCENDING);
                    return dataFilter;
                default:
                    dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_VALUE, STARTS_WITH, searchPrefix, false);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_APPROVED_INDICATOR, T8DataFilter.OrderMethod.DESCENDING);
                    dataFilter.addFieldOrdering(E_PRESCRIBED_VALUE_LOOKUP + F_VALUE, T8DataFilter.OrderMethod.ASCENDING);
                    return dataFilter;
            }
        }

        // Return the complete filter.
        return dataFilter;
    }

    public T8DataFilter createPrescribedValueFilter(String dataTypeID, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, boolean restrictToApprovedValues)
    {
        return this.createPrescribedValueFilter(RequirementType.valueOf(dataTypeID), drId, drInstanceId, propertyId, fieldId, conceptIdList, restrictToApprovedValues);
    }

    public T8DataFilter createExactMatchPrescribedValueFilter(RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, boolean restrictToApprovedValues, String matchString, List<T8ConceptElementType> elementTypes)
    {
        T8DataFilterCriteria criteria;
        T8DataFilter dataFilter;

        // Create a normal filter without using the search string yet.
        dataFilter = this.createPrescribedValueFilter(requirementType, drId, drInstanceId, propertyId, fieldId, conceptIdList, restrictToApprovedValues);
        criteria = new T8DataFilterCriteria();
        dataFilter.addFilterCriteria(AND, criteria);

        // If a search on code is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.CODE))
        {
            if (requirementType == RequirementType.CONTROLLED_CONCEPT)
            {
                criteria.addFilterCriterion(OR, E_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_CODE, EQUAL, matchString, false);
            }
            else if (requirementType == RequirementType.UNIT_OF_MEASURE)
            {
                criteria.addFilterCriterion(OR, E_PRESCRIBED_VALUE_LOOKUP + F_UOM_CODE, EQUAL, matchString, false);
            }
        }

        // If a search on term is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.TERM))
        {
            if (requirementType == RequirementType.CONTROLLED_CONCEPT)
            {
                criteria.addFilterCriterion(OR, E_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_TERM, EQUAL, matchString, false);
            }
            else if (requirementType == RequirementType.UNIT_OF_MEASURE)
            {
                criteria.addFilterCriterion(OR, E_PRESCRIBED_VALUE_LOOKUP + F_UOM_TERM, EQUAL, matchString, false);
            }
        }

        // If a search on abbreviation is specified, add it to the criteria.
        if (elementTypes.contains(T8ConceptElementType.ABBREVIATION))
        {
            if (requirementType == RequirementType.CONTROLLED_CONCEPT)
            {
                criteria.addFilterCriterion(OR, E_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ABBREVIATION, EQUAL, matchString, false);
            }
            else if (requirementType == RequirementType.UNIT_OF_MEASURE)
            {
                criteria.addFilterCriterion(OR, E_PRESCRIBED_VALUE_LOOKUP + F_UOM_ABBREVIATION, EQUAL, matchString, false);
            }
        }

        // Return the complete filter.
        return dataFilter;
    }

    public T8DataFilter createPrescribedValueFilter(RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, boolean restrictToApprovedValues)
    {
        T8DataFilter dataFilter;

        // Create the filter.
        dataFilter = new T8DataFilter(E_PRESCRIBED_VALUE_LOOKUP);

        // Add the root organization and DR filter.
        dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_ORG_ID, EQUAL, organizationStructure.getRootOrganizationID(), false);
        dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_DR_ID, EQUAL, drId, false);

        // Add the DR_INSTANCE.
        if (!Strings.isNullOrEmpty(drInstanceId))
        {
            T8DataFilterCriteria drInstanceCriteria;

            // The DR_INSTANCE_ID is an optional filter, so we need to also retrieve rows where it is null.
            drInstanceCriteria = new T8DataFilterCriteria();
            drInstanceCriteria.addFilterClause(AND, E_PRESCRIBED_VALUE_LOOKUP + F_DR_INSTANCE_ID, EQUAL, drInstanceId, false);
            drInstanceCriteria.addFilterClause(OR, E_PRESCRIBED_VALUE_LOOKUP + F_DR_INSTANCE_ID, IS_NULL, null, false);
            dataFilter.addFilterCriteria(AND, drInstanceCriteria);
        }

        // Add the property, field and datat type filter.
        dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_PROPERTY_ID, EQUAL, propertyId, false);
        if (!Strings.isNullOrEmpty(fieldId)) dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_FIELD_ID, EQUAL, fieldId, false);
        dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_DATA_TYPE_ID, EQUAL, requirementType.toString(), false);

        // Add any specific concept filters if required.
        if (!CollectionUtilities.isNullOrEmpty(conceptIdList)) dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ID, IN, conceptIdList, false);

        // Add the language filter.
        dataFilter.addFilterCriterion(AND, E_PRESCRIBED_VALUE_LOOKUP + F_LANGUAGE_ID, EQUAL, context.getSessionContext().getContentLanguageIdentifier(), false);
        return dataFilter;
    }

    /**
     * Creates filter criteria based on the supplied concept dependencies.
     * @param entityId The entity for which to create the criteria.
     * @param conceptDependencies A list of the concept dependencies to be used
     * in the filter (if any).  Each element in the list is a pair consisting of
     * the CONCEPT_GRAPH_ID and TAIL_CONCEPT_ID to use in the filter.  If the
     * TAIL_CONCEPT_ID is null, it will be disregarded and only the
     * CONCEPT_GRAPH_ID will be added to the filter, effectively limiting the
     * retrieval to any head concept in the specified graph.
     * @return The construct filter criteria.
     */
    private T8DataFilterCriteria createConceptDependencyFilters(String entityId, List<Pair<String,String>> conceptDependencies)
    {
        T8DataFilterCriteria newGraphFilterCriteria;
        T8DataFilterCriteria lastFilterCriteria;
        Pair<String, String> nextDependency;

        // Get the next Concept dependency for which we are going to add a filter clause.
        nextDependency = conceptDependencies.remove(0);

        // Construct the next filter clause using the Concept Graph ID.
        newGraphFilterCriteria = new T8DataFilterCriteria();
        newGraphFilterCriteria.addFilterClause(AND, entityId + "$CONCEPT_GRAPH_ID", EQUAL, nextDependency.getValue1(), false);
        if (nextDependency.getValue2() != null) newGraphFilterCriteria.addFilterClause(AND, entityId + "$TAIL_CONCEPT_ID", EQUAL, nextDependency.getValue2(), false);

        // While there are still mode dependencies left, add each one as a sub-filter.
        lastFilterCriteria = newGraphFilterCriteria;
        while (conceptDependencies.size() > 0)
        {
            T8DataFilterCriteria subFilterCriteria;
            T8DataFilter subFilter;

            // Get the next dependency in the list.
            nextDependency = conceptDependencies.remove(0);

            // Construct the next filter clause using the Concept Graph ID.
            subFilterCriteria = new T8DataFilterCriteria();
            subFilterCriteria.addFilterClause(AND, entityId + "$CONCEPT_GRAPH_ID", EQUAL, nextDependency.getValue1(), false);
            if (nextDependency.getValue2() != null) subFilterCriteria.addFilterClause(AND, entityId + "$TAIL_CONCEPT_ID", EQUAL, nextDependency.getValue2(), false);

            // Construct the sub-filter.
            subFilter = new T8DataFilter(entityId, subFilterCriteria);

            // Add the sub-filter to the last filter criteria.
            lastFilterCriteria.addFilterClause(AND, entityId + "$CONCEPT_ID", IN, subFilter, false);

            // Set the last filter criteria to the sub-filter criteria (because we are moving one level down).
            lastFilterCriteria = subFilterCriteria;
        }

        // Return the new graph.
        return newGraphFilterCriteria;
    }
}
