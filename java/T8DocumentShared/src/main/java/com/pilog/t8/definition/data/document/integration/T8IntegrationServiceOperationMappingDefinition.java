package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.remote.server.connection.T8SoapRemoteEndpointConnectionDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public abstract class T8IntegrationServiceOperationMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTEGRATION_SERVICE_OPERATION_MAPPING";
    public static final String IDENTIFIER_PREFIX = "IF_OP_";

    public enum Datum
    {
        REMOTE_SERVER_CONNECTION_IDENTIFIER,
        RECORD_MAPPING_IDENTIFIER,
        INPUT_PARAMETERS
    };
    // -------- Definition Meta-Data -------- //

    public static final String P_DATA_RECORDS = "$P_DATA_RECORDS";
    public static final String P_OPERATION_IID = "$P_OPERATION_IID";
    public static final String P_INPUT_PARAMETERS = "$P_INPUT_PARAMETERS";

    public T8IntegrationServiceOperationMappingDefinition(String identifier)
    {
        super(identifier);
    }

    protected abstract List<T8DataParameterDefinition> getOperationInputParameters();

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString(), "Remote Endpoint Connection Identifier", "The remote connection identifier that will be used to perform the service call."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.RECORD_MAPPING_IDENTIFIER.toString(), "Record Mapping Identifier", "The identifier of the record mapping that will be used to map the record properties to the fields of the service."));
        datumTypes.add(getInputParameterDatumType());

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8SoapRemoteEndpointConnectionDefinition.TYPE_IDENTIFIER));
        else if(Datum.RECORD_MAPPING_IDENTIFIER.toString().equals(datumIdentifier))
        {
            T8IntegrationServiceDefinition serviceDefinition;

            serviceDefinition = (T8IntegrationServiceDefinition) getAncestorDefinition(T8IntegrationServiceDefinition.TYPE_IDENTIFIER);

            if(serviceDefinition.getDataRecordMappingDefinitions() != null)
                return createLocalIdentifierOptionsFromDefinitions(serviceDefinition.getDataRecordMappingDefinitions());
            else return new ArrayList<>();
        }
        else return null;
    }

    private T8DefinitionDatumType getInputParameterDatumType()
    {
        T8DefinitionDatumType definitionList;

        definitionList = T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETERS.toString(), "Input Parameters", "The input parameters that this operation will accept.");
        definitionList.setHidden(true);
        definitionList.setPersisted(false);
        definitionList.setDefaultValue(getOperationInputParameters());

        return definitionList;
    }

    public String getRemoteServerConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString());
    }

    public void setRemoteServerConnectionIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.REMOTE_SERVER_CONNECTION_IDENTIFIER.toString(), identifier);
    }

    public String getRecordMappingIdentifier()
    {
        return (String)getDefinitionDatum(Datum.RECORD_MAPPING_IDENTIFIER.toString());
    }

    public void setRecordMappingIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.RECORD_MAPPING_IDENTIFIER.toString(), identifier);
    }

    public List<T8DataParameterDefinition> getInputParameters()
    {
        return (List<T8DataParameterDefinition>)getDefinitionDatum(Datum.INPUT_PARAMETERS.toString());
    }

    public void setInputParameters(List<T8DataParameterDefinition> parameters)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETERS.toString(), parameters);
    }

}
