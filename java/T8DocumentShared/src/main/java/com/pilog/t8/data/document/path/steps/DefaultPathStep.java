package com.pilog.t8.data.document.path.steps;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;

/**
 * This class defines the default behavior of all path steps that form part of
 * a Document Path expression.  Each step in a document path acts on a set of
 * objects (inputs) and results in a second output set of objects that may or
 * may not be of the same type as the original set.  A path step has the
 * following three characteristics:
 * 1. A type, that defines the type of the output objects that will be generated
 * from the input set on which the step acts.
 * 2. An ID list, that specifies the exact identifiers of the items to be
 * grouped in the output set.
 * 3. A predicate expression, which if present, will be executed to determine
 * the members of the output set.
 *
 * @author Bouwer du Preez
 */
public abstract class DefaultPathStep implements PathStep
{
    protected String stepString;
    protected List<String> idList;
    protected String predicateExpression;
    protected ExpressionEvaluator predicateExpressionEvaluator;
    protected TerminologyProvider terminologyProvider;
    protected OntologyProvider ontologyProvider;
    protected DataRecordProvider recordProvider;
    protected T8DataObjectProvider dataObjectProvider;
    protected StepModifiers modifiers;

    public DefaultPathStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, DataRecordProvider recordProvider, T8DataObjectProvider dataObjectProvider, String stepString)
    {
        this.terminologyProvider = terminologyProvider;
        this.ontologyProvider = ontologyProvider;
        this.recordProvider = recordProvider;
        this.dataObjectProvider = dataObjectProvider;
        this.stepString = stepString;
        this.modifiers = modifiers;
        parseStepString(stepString);
    }

    @Override
    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
    }

    @Override
    public void setOntologyProvider(OntologyProvider ontologyProvider)
    {
        this.ontologyProvider = ontologyProvider;
    }

    @Override
    public void setRecordProvider(DataRecordProvider recordProvider)
    {
        this.recordProvider = recordProvider;
    }

    @Override
    public void setDataObjectProvider(T8DataObjectProvider dataObjectProvider)
    {
        this.dataObjectProvider = dataObjectProvider;
    }

    private void parseStepString(String stepString)
    {
        parseIDList(stepString);
        parsePredicateExpression(stepString);
    }

    private void parseIDList(String stepString)
    {
        int colonIndex;

        colonIndex = stepString.indexOf(":");
        if (colonIndex > -1)
        {
            int startIndex;
            int endIndex;
            String idListString;

            startIndex = colonIndex + 1;
            endIndex = stepString.indexOf("[");
            if (endIndex == -1) endIndex = stepString.length();

            idListString = stepString.substring(startIndex, endIndex);
            if (idListString.equals("*")) idList = null;
            else idList = Arrays.asList(idListString.split("\\|"));
        }
        else idList = null;
    }

    private void parsePredicateExpression(String stepString)
    {
        int startIndex;

        startIndex = stepString.indexOf("[");
        if (startIndex > -1)
        {
            int endIndex;

            startIndex++; // We don't want to include the square bracket.
            endIndex = stepString.lastIndexOf("]");
            if ((endIndex > -1) && (startIndex < endIndex))
            {
                try
                {
                    predicateExpression = stepString.substring(startIndex, endIndex);
                    predicateExpressionEvaluator = new ExpressionEvaluator();
                    predicateExpressionEvaluator.compileExpression(predicateExpression);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Predicate expression compilation exception in path step '" + stepString + "'.", e);
                }
            }
            else throw new RuntimeException("Predicate expression without a closing bracket found: " + stepString);
        }
    }

    protected Map<String, Object> getPredicateExpressionParameters(Map<String, Object> inputParameters)
    {
        Map<String, Object> parameters;

        parameters = new HashMap<String, Object>();
        for (String variableName : predicateExpressionEvaluator.getVariableNames())
        {
            parameters.put(variableName, null);
        }

        if (inputParameters != null) parameters.putAll(inputParameters);
        return parameters;
    }

    @Override
    public String toString()
    {
        return stepString;
    }
}
