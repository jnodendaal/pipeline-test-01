package com.pilog.t8.data.document.dataquality;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8GroupedDataQualityReport implements Serializable
{
    private final LinkedHashMap<String, T8DataQualityMetricsGroup> groups;

    public T8GroupedDataQualityReport()
    {
        this.groups = new LinkedHashMap<>();
    }

    public List<String> getGroupIds()
    {
        return new ArrayList<String>(groups.keySet());
    }

    public List<T8DataQualityMetricsGroup> getGroups()
    {
        return new ArrayList<>(groups.values());
    }

    public T8DataQualityMetricsGroup getGroup(String groupId)
    {
        return groups.get(groupId);
    }

    public void addGroup(T8DataQualityMetricsGroup group)
    {
        groups.put(group.getId(), group);
    }

    public boolean removeGroup(T8DataQualityMetricsGroup group)
    {
        return groups.remove(group.getId()) != null;
    }

    public T8DataQualityMetricsGroup removeGroup(String groupId)
    {
        return groups.remove(groupId);
    }

    @Override
    public String toString()
    {
        return "T8GroupedDataQualityReport{" + "groups=" + groups + '}';
    }
}
