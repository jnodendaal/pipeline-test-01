package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import java.math.BigDecimal;

/**
 * @author Bouwer du Preez
 */
public class RealValue extends RecordValue
{
    public RealValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public void setValue(Double value)
    {
        if (value != null)
        {
            super.setValue(value.toString());
        }
        else super.setValue(null);
    }

    public void setValue(BigDecimal value)
    {
        if (value != null)
        {
            super.setValue(value.toString());
        }
        else super.setValue(null);
    }

    public BigDecimal getBigDecimalValue()
    {
        String stringValue;

        stringValue = getValue();
        return stringValue != null ? new BigDecimal(stringValue) : null;
    }

    public Double getRealValue()
    {
        String stringValue;

        return (stringValue = getValue()) != null ? Double.parseDouble(stringValue) : null;
    }

    @Override
    public RealValueContent getContent()
    {
        RealValueContent content;

        content = new RealValueContent();
        content.setValue(value);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        RealValueContent content;

        content = (RealValueContent)valueContent;
        setValue(content.getValue());
    }
}
