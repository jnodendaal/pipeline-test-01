package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.path.PathRoot;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class InstanceRequirementStep extends DefaultPathStep implements PathStep
{
    public InstanceRequirementStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof PathRoot)
        {
            return evaluateInstanceStep(ArrayLists.newArrayList(((PathRoot)input).getDataRecord().getDataRequirementInstance()));
        }
        else if (input instanceof DataRecord)
        {
            return evaluateInstanceStep(ArrayLists.newArrayList(((DataRecord)input).getDataRequirementInstance()));
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateInstanceStep(((DataRequirementInstance)input).getSubInstances());
        }
        else if (input instanceof RecordProperty)
        {
            List<DataRecord> subRecords;
            List<DataRequirementInstance> subInstances;
            RecordProperty inputProperty;

            inputProperty = (RecordProperty)input;
            subRecords = inputProperty.getParentDataRecord().getSubRecordsReferencedFromProperty(inputProperty.getPropertyID());
            subInstances = new ArrayList<DataRequirementInstance>();
            for (DataRecord subRecord : subRecords)
            {
                subInstances.add(subRecord.getDataRequirementInstance());
            }

            return evaluateInstanceStep(subInstances);
        }
        else if (input instanceof List)
        {
            List<DataRequirementInstance> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<DataRequirementInstance>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<DataRequirementInstance>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<DataRequirementInstance> evaluateInstanceStep(List<DataRequirementInstance> requirementList)
    {
        // If we have ID's to use for narrowing of the requirement list do it now.
        if (idList != null)
        {
            Iterator<DataRequirementInstance> requirementIterator;

            requirementIterator = requirementList.iterator();
            while (requirementIterator.hasNext())
            {
                DataRequirementInstance nextRequirement;

                nextRequirement = requirementIterator.next();
                if (!idList.contains(nextRequirement.getConceptID()))
                {
                    requirementIterator.remove();
                }
            }
        }

        // Now run through the remaining requirements and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<DataRequirementInstance> requirementIterator;

            // Iterator over the requirements and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            requirementIterator = requirementList.iterator();
            while (requirementIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                DataRequirementInstance nextRequirement;

                // Create a map containing the input parameters available to the predicate expression.
                nextRequirement = requirementIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("DR_INSTANCE", nextRequirement);
                inputParameters.put("DR_INSTANCE_ID", nextRequirement.getConceptID());
                inputParameters.put("DR_ID", nextRequirement.getDataRequirement().getConceptID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        requirementIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return requirementList;
    }
}
