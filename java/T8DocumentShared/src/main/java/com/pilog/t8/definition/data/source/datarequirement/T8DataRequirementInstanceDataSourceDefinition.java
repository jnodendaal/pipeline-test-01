package com.pilog.t8.definition.data.source.datarequirement;

import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementInstanceDataSourceDefinition extends T8DataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATA_REQUIREMENT_INSTANCE";
    public static final String DISPLAY_NAME = "Data Requirement Instance Data Source";
    public static final String DESCRIPTION = "A data source for accessing Data Requirement Instance Documents.";
    public static final String VERSION = "0";
    public enum Datum {CONNECTION_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public static final String KEY_FIELD_IDENTIFIER = "$ID";
    public static final String DOCUMENT_FIELD_IDENTIFIER = "$DOCUMENT";
    public static final String KEY_FIELD_SOURCE_IDENTIFIER = "DR_INSTANCE_ID";
    public static final String DOCUMENT_FIELD_SOURCE_IDENTIFIER = "DR_INSTANCE_DOCUMENT";

    public T8DataRequirementInstanceDataSourceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.CONNECTION_IDENTIFIER.toString(), "Connection", "The connection this data source uses."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONNECTION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataConnectionDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public String getConnectionIdentifier()
    {
        return (String)getDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString());
    }

    public void setConnectionIdentifier(String connectionIdentifier)
    {
        setDefinitionDatum(Datum.CONNECTION_IDENTIFIER.toString(), connectionIdentifier);
    }

    @Override
    public T8DataSource getNewDataSourceInstance(T8DataTransaction dataAccessProvider)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.source.datarequirement.T8DataRequirementInstanceDataSource").getConstructor(T8DataRequirementInstanceDataSourceDefinition.class, T8DataTransaction.class);
            return (T8DataSource)constructor.newInstance(this, dataAccessProvider);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}