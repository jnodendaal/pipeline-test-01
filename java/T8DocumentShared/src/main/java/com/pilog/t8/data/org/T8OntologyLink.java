package com.pilog.t8.data.org;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyLink implements Serializable
{
    private String organizationId;
    private String ontologyStructureId;
    private String ontologyClassId;
    private String conceptId;
    private boolean active;

    public T8OntologyLink(String organizationId, String ontologyStructureId, String ontologyClassId, String conceptId)
    {
        this.organizationId = organizationId;
        this.ontologyStructureId = ontologyStructureId;
        this.ontologyClassId = ontologyClassId;
        this.conceptId = conceptId;
        this.active = true;
    }

    public T8OntologyLink copy()
    {
        return new T8OntologyLink(organizationId, ontologyStructureId, ontologyClassId, conceptId);
    }

    public String getOrganizationID()
    {
        return organizationId;
    }

    public void setOrganizationID(String organizationID)
    {
        this.organizationId = organizationID;
    }

    public String getOntologyStructureID()
    {
        return ontologyStructureId;
    }

    public void setOntologyStructureID(String dataStructureID)
    {
        this.ontologyStructureId = dataStructureID;
    }

    /**
     * Returns the ID of the ontology structure to which this link belongs.
     * @return The ID of the ontology structure to which this link belongs.
     * @deprecated Use {@code getOntologyStructureID}.
     */
    @Deprecated
    public String getDataStructureID()
    {
        return ontologyStructureId;
    }

    /**
     * Sets the ontology structure to which this link belongs.
     * @param dataStructureID The ID of the ontology structure to which this link belongs.
     * @deprecated Use {@code setOntologyStructureID}.
     */
    @Deprecated
    public void setDataStructureID(String dataStructureID)
    {
        this.ontologyStructureId = dataStructureID;
    }

    public String getOntologyClassID()
    {
        return ontologyClassId;
    }

    public void setOntologyClassID(String ontologyClassID)
    {
        this.ontologyClassId = ontologyClassID;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isEquivalentLink(T8OntologyLink link)
    {
        if (!Objects.equals(link.getConceptID(), conceptId))
        {
            return false;
        }
        else if (!Objects.equals(link.getDataStructureID(), ontologyStructureId))
        {
            return false;
        }
        else if (!Objects.equals(link.getOntologyClassID(), ontologyClassId))
        {
            return false;
        }
        else if (!Objects.equals(link.getOrganizationID(), organizationId))
        {
            return false;
        }
        else return true;
    }

    @Override
    public String toString()
    {
        return "T8OntologyLink{" + "organizationID=" + organizationId + ", ontologyStructureID=" + ontologyStructureId + ", ontologyClassID=" + ontologyClassId + ", conceptID=" + conceptId + '}';
    }
}
