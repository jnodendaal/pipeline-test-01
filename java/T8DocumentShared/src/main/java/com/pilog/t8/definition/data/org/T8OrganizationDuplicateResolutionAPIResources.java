package com.pilog.t8.definition.data.org;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDuplicateResolutionAPIResources implements T8DefinitionResource
{
    public static final String OPERATION_API_ORG_DUP_RES_SUBMIT_RESOLUTION = "@OS_API_ORG_DUP_RES_SUBMIT_RESOLUTION";
    public static final String OPERATION_API_ORG_DUP_RES_CANCEL_RESOLUTION = "@OS_API_ORG_DUP_RES_CANCEL_RESOLUTION";
    public static final String OPERATION_API_ORG_DUP_RES_FINALIZE_RESOLUTION = "@OS_API_ORG_DUP_RES_FINALIZE_RESOLUTION";

    public static final String OPERATION_API_ORG_DUP_RES_SET_MASTER = "@OS_API_ORG_DUP_RES_SET_MASTER";
    public static final String OPERATION_API_ORG_DUP_RES_SET_EXCLUDED = "@OS_API_ORG_DUP_RES_SET_EXCLUDED";
    public static final String OPERATION_API_ORG_DUP_RES_SET_SUPERSEDED = "@OS_API_ORG_DUP_RES_SET_SUPERSEDED";
    public static final String OPERATION_API_ORG_DUP_RES_SET_PENDING = "@OS_API_ORG_DUP_RES_SET_PENDING";
    
    public static final String PARAMETER_FAMILY_ID = "$P_FAMILY_ID";
    public static final String PARAMETER_RECORD_ID = "$P_RECORD_ID";
    public static final String PARAMETER_MATCH_INSTANCE_ID = "$P_MATCH_INSTANCE_ID";  
    public static final String PARAMETER_RESOLUTION_ID = "$P_RESOLUTION_ID";  
    public static final String PARAMETER_DATA_OBJECT_IDENTIFIER = "$P_DATA_OBJECT_IDENTIFIER";  
    public static final String PARAMETER_RESOLUTION_STATE_IDENTIFIER = "$P_RESOLUTION_STATE_IDENTIFIER";
    public static final String PARAMETER_MASTER_STATE_IDENTIFIER = "$P_MASTER_STATE_IDENTIFIER";
    public static final String PARAMETER_NON_DUPLICATE_STATE_IDENTIFIER = "$P_NON_DUPLICATE_STATE_IDENTIFIER";
    public static final String PARAMETER_SUPERSEDED_STATE_IDENTIFIER = "$P_SUPERSEDED_STATE_IDENTIFIER";
    public static final String PARAMETER_DATA_RECORD_MERGER_IDENTIFIER = "$P_DATA_RECORD_MERGER_IDENTIFIER";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_SUBMIT_RESOLUTION);
            definition.setMetaDisplayName("Submit Match Results for Resolution");
            definition.setMetaDescription("Moves the specified match results to the duplicate resolution table, taking into account already resolved record matches.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationSubmitResolution");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAMILY_ID, "Family ID", "The ID of the family of record matches to submit for resolution.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_INSTANCE_ID, "Match Instance ID", "The ID of the match instance from which to submit results for resolution.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IDENTIFIER, "Data Object ID", "The ID of the Data Object that represents the records.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RESOLUTION_STATE_IDENTIFIER, "Resolution State ID", "The ID of the state to which the records will be set for resolution.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RESOLUTION_ID, "Resolution ID", "The ID of the resolution batch created by the submission.", T8DataType.GUID));
            definitions.add(definition); 
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_CANCEL_RESOLUTION);
            definition.setMetaDisplayName("Cancel Resolution");
            definition.setMetaDescription("Cancels the in-progress resolution.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationCancelResolution");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RESOLUTION_ID, "Resolution ID", "The ID of the resolution batch to cancel.", T8DataType.GUID));
            definitions.add(definition); 
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_FINALIZE_RESOLUTION);
            definition.setMetaDisplayName("Finalize Resolution");
            definition.setMetaDescription("Finalizes the in-progress resolution, merging all superseded records with the family master record.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationFinalizeResolution");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_RECORD_MERGER_IDENTIFIER, "Merger ID", "The ID of the Data Record merger to use during finalization.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RESOLUTION_ID, "Resolution ID", "The ID of the resolution batch to finalize.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IDENTIFIER, "Data Object ID", "The ID of the Data Object that represents the records.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MASTER_STATE_IDENTIFIER, "Master State ID", "The ID of the state to which the records will be set if resolved to master.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NON_DUPLICATE_STATE_IDENTIFIER, "Non-Duplicate State ID", "The ID of the state to which the records will be set if resolved as non-duplicates.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUPERSEDED_STATE_IDENTIFIER, "Superseded State ID", "The ID of the state to which the records will be set if resolved as superseded.", T8DataType.DEFINITION_IDENTIFIER));
            definitions.add(definition); 
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_SET_MASTER);
            definition.setMetaDisplayName("Set Master Record");
            definition.setMetaDescription("Sets one of the records in the specified family to be the new master.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationSetMaster");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAMILY_ID, "Family ID", "The ID of the family of record matches on which the master record will be set.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The ID the record to set as the new master of the family.", T8DataType.GUID));
            definitions.add(definition); 

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_SET_EXCLUDED);
            definition.setMetaDisplayName("Set Excluded Record");
            definition.setMetaDescription("Sets one of the records in the specified family as excluded from the family.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationSetExcluded");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAMILY_ID, "Family ID", "The ID of the family of record matches from which the record will be set as excluded.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The ID the record to set as excluded from the family.", T8DataType.GUID));
            definitions.add(definition); 
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_SET_SUPERSEDED);
            definition.setMetaDisplayName("Set Superseded Record");
            definition.setMetaDescription("Sets one of the records in the specified family as superseded by the family master record.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationSetSuperseded");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAMILY_ID, "Family ID", "The ID of the family of record matches in which the record will be set as superseded.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The ID the record to set as superseded.", T8DataType.GUID));
            definitions.add(definition); 
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DUP_RES_SET_PENDING);
            definition.setMetaDisplayName("Set Pending Record");
            definition.setMetaDescription("Sets one of the records in the specified family as pending resolution.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDuplicateResolutionAPIOperations$APIOperationSetPending");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FAMILY_ID, "Family ID", "The ID of the family of record matches in which the record will be set as pending resolution.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The ID the record to set as pending resolution.", T8DataType.GUID));
            definitions.add(definition); 
            
            return definitions;
        }
        else return null;
    }
}
