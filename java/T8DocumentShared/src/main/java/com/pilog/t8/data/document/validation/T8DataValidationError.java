package com.pilog.t8.data.document.validation;

import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.Requirement;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataValidationError implements Serializable
{
    protected ValidationErrorType errorType;
    protected String recordId;
    protected String errorMessage;

    public enum ValidationErrorType
    {
        REQUIREMENT, // Validation against Data Requirement failed.
        ACCESS, // Validation for current access context failed.
        REFERENCE // Validation of referenced document failed.
    };

    public T8DataValidationError(ValidationErrorType errorType, String recordId, String errorMessage)
    {
        this.recordId = recordId;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }

    public ValidationErrorType getErrorType()
    {
        return errorType;
    }

    public void setErrorType(ValidationErrorType errorType)
    {
        this.errorType = errorType;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public abstract boolean isApplicableTo(Requirement requirement);
    public abstract boolean isApplicableTo(Value requirement);
}
