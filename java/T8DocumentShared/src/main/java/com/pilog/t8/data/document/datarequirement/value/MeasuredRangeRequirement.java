package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredRangeRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public MeasuredRangeRequirement()
    {
        super(RequirementType.MEASURED_RANGE);
    }

    public LowerBoundNumberRequirement getLowerBoundNumberRequirement()
    {
        return (LowerBoundNumberRequirement)getSubRequirement(RequirementType.LOWER_BOUND_NUMBER, null, null);
    }

    public UpperBoundNumberRequirement getUpperBoundNumberRequirement()
    {
        return (UpperBoundNumberRequirement)getSubRequirement(RequirementType.UPPER_BOUND_NUMBER, null, null);
    }

    public UnitOfMeasureRequirement getUomRequirement()
    {
        return (UnitOfMeasureRequirement)getSubRequirement(RequirementType.UNIT_OF_MEASURE, null, null);
    }

    public QualifierOfMeasureRequirement getQomRequirement()
    {
        return (QualifierOfMeasureRequirement)getSubRequirement(RequirementType.QUALIFIER_OF_MEASURE, null, null);
    }

    @Override
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public String getUomOntologyClassId()
    {
        UnitOfMeasureRequirement uomRequirement;

        uomRequirement = getUomRequirement();
        return uomRequirement != null ? uomRequirement.getOntologyClassID() : null;
    }

    public String getQomOntologyClassId()
    {
        QualifierOfMeasureRequirement qomRequirement;

        qomRequirement = getQomRequirement();
        return qomRequirement != null ? qomRequirement.getOntologyClassID() : null;
    }
}
