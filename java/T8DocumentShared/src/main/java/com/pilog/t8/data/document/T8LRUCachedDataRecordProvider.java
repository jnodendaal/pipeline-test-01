package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.utilities.cache.LRUCache;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LRUCachedDataRecordProvider implements CachedDataRecordProvider
{
    private final LRUCache<String, DataRecord> recordCache;
    private final LRUCache<String, DataRecord> dataFileCache;
    private final LRUCache<String, String> recordDRInstanceIDCache;
    private final LRUCache<String, T8AttachmentDetails> attachmentDetailsCache;
    private final LRUCache<String, Map<String, T8AttachmentDetails>> recordAttachmentDetailsCache;
    private final DataRecordProvider recordProvider;

    public T8LRUCachedDataRecordProvider(DataRecordProvider recordProvider, int size)
    {
        this.recordCache = new LRUCache<String, DataRecord>(size);
        this.dataFileCache = new LRUCache<String, DataRecord>(size);
        this.recordDRInstanceIDCache = new LRUCache<String, String>(size);
        this.attachmentDetailsCache = new LRUCache<String, T8AttachmentDetails>(size);
        this.recordAttachmentDetailsCache = new LRUCache<String, Map<String, T8AttachmentDetails>>(size);
        this.recordProvider = recordProvider;
    }

    @Override
    public String getRecordDrInstanceId(String recordId)
    {
        String drInstanceID;

        // Try to find the required record in the cache.  Iterating over the cached records does not affect their "cache-age".
        for (DataRecord cachedRecord : recordCache.values())
        {
            if (cachedRecord.getID().equals(recordId))
            {
                return cachedRecord.getDataRequirementInstance().getConceptID();
            }
        }

        // Try to find the required ID from the ID cache.
        drInstanceID = recordDRInstanceIDCache.get(recordId);
        if (drInstanceID != null)
        {
            return drInstanceID;
        }
        else
        {
            // Ok so we could not find the required record among those already cached.  We now need to retrieve the DR Instance ID.
            drInstanceID = recordProvider.getRecordDrInstanceId(recordId);
            recordDRInstanceIDCache.put(recordId, drInstanceID);
            return drInstanceID;
        }
    }

    @Override
    public T8AttachmentDetails getAttachmentDetails(String attachmentID)
    {
        T8AttachmentDetails attachmentDetails;

        // Try to get the details from the cache.
        attachmentDetails = attachmentDetailsCache.get(attachmentID);
        if (attachmentDetails != null)
        {
            return attachmentDetails;
        }
        else
        {
            // If the attachment details were not found, fetch them.
            attachmentDetails = recordProvider.getAttachmentDetails(attachmentID);

            // Add the retrieved details to the cache for individual attachments.
            attachmentDetailsCache.put(attachmentID, attachmentDetails);
            return attachmentDetails;
        }
    }

    @Override
    public Map<String, T8AttachmentDetails> getRecordAttachmentDetails(String recordId)
    {
        Map<String, T8AttachmentDetails> attachmentDetails;

        attachmentDetails = recordAttachmentDetailsCache.get(recordId);
        if (attachmentDetails != null)
        {
            return attachmentDetails;
        }
        else
        {
            // First try to get the file details from the cached records.  Iterating over the cached records does not affect their "cache-age".
            for (DataRecord cachedRecord : recordCache.values())
            {
                attachmentDetails = cachedRecord.getAttachmentDetails();
                if (attachmentDetails != null) return attachmentDetails;
            }

            // If the attachment details were not found, fetch them.
            attachmentDetails = recordProvider.getRecordAttachmentDetails(recordId);

            // Add the retrieved details to the cache for individual attachments.
            if (attachmentDetails != null) attachmentDetailsCache.putAll(attachmentDetails);

            // Add the retrieved details to the cache for attachments per data record.
            recordAttachmentDetailsCache.put(recordId, attachmentDetails);
            return attachmentDetails;
        }
    }

    @Override
    public DataRecord getDataRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendentRecords)
    {
        if (recordCache.containsKey(recordId))
        {
            return recordCache.get(recordId);
        }
        else
        {
            DataRecord dataRecord;

            dataRecord = recordProvider.getDataRecord(recordId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendentRecords);
            if (dataRecord != null) recordCache.put(recordId, dataRecord);
            return dataRecord;
        }
    }

    @Override
    public List<DataRecord> getDataRecords(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendentRecords)
    {
        List<String> nonCachedRecordIdList;
        List<DataRecord> requestedRecordList;
        List<DataRecord> fetchedRecordList;

        // Get all cached records and add them to the result list.
        nonCachedRecordIdList = new ArrayList<String>();
        requestedRecordList = new ArrayList<DataRecord>();
        for (String recordId : recordIdList)
        {
            DataRecord dataRecord;

            dataRecord = recordCache.get(recordId);
            if (dataRecord != null)
            {
                requestedRecordList.add(dataRecord);
            }
            else if (!nonCachedRecordIdList.contains(recordId))
            {
                nonCachedRecordIdList.add(recordId);
            }
        }

        // Now fetch all of the records that were not found in the cache.
        fetchedRecordList = recordProvider.getDataRecords(nonCachedRecordIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendentRecords);
        for (DataRecord fetchedRecord : fetchedRecordList)
        {
            recordCache.put(fetchedRecord.getID(), fetchedRecord);
            requestedRecordList.add(fetchedRecord);
        }

        // Return the list of all requested records.
        return requestedRecordList;
    }

    @Override
    public DataRecord getDataFile(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        if (dataFileCache.containsKey(recordId))
        {
            return dataFileCache.get(recordId);
        }
        else
        {
            DataRecord dataRecord;

            dataRecord = recordProvider.getDataFile(recordId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
            if (dataRecord != null) dataFileCache.put(recordId, dataRecord);
            return dataRecord;
        }
    }

    @Override
    public List<DataRecord> getDataFiles(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        List<String> nonCachedFileIDList;
        List<DataRecord> requestedFileList;
        List<DataRecord> retrievedFiles;

        // Get all cached records and add them to the result list.
        nonCachedFileIDList = new ArrayList<String>();
        requestedFileList = new ArrayList<DataRecord>();
        for (String recordID : recordIdList)
        {
            DataRecord dataFile;

            dataFile = dataFileCache.get(recordID);
            if (dataFile != null)
            {
                requestedFileList.add(dataFile);
            }
            else if (!nonCachedFileIDList.contains(recordID))
            {
                nonCachedFileIDList.add(recordID);
            }
        }

        // Now fetch all of the records that were not found in the cache.
        retrievedFiles = recordProvider.getDataFiles(nonCachedFileIDList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        for (DataRecord retrievedFile : retrievedFiles)
        {
            dataFileCache.put(retrievedFile.getID(), retrievedFile);
            requestedFileList.add(retrievedFile);
        }

        // Return the list of all requested records.
        return requestedFileList;
    }

    @Override
    public DataRecord getDataFileByContentRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        List<DataRecord> retrievedFiles;

        retrievedFiles = getDataFilesByContentRecord(Arrays.asList(recordId), languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        return retrievedFiles.size() > 0 ? retrievedFiles.get(0) : null;
    }

    @Override
    public List<DataRecord> getDataFilesByContentRecord(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        List<String> nonCachedRecordIDList;
        List<DataRecord> retrievedFiles;
        List<DataRecord> requestedFileList;

        // Create a list to hold all of the files to be returned from the method.
        requestedFileList = new ArrayList<DataRecord>();

        // Create a list of all record ID's that are not found in the cache.
        nonCachedRecordIDList = new ArrayList<String>();
        for (String recordID : recordIdList)
        {
            boolean cached;

            // First check the cached records.
            cached = false;
            for (DataRecord cachedFile : dataFileCache.values())
            {
                if ((requestedFileList.contains(cachedFile)) && (cachedFile.findDataRecord(recordID) != null))
                {
                    cached = true;
                    requestedFileList.add(cachedFile);
                    break;
                }
            }

            // If the record was not found in the cache, add it to the list.
            if (!cached)
            {
                nonCachedRecordIDList.add(recordID);
            }
        }

        // Retrieve the records that were not found in the cache.
        retrievedFiles = recordProvider.getDataFilesByContentRecord(recordIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);

        // Add all of the retrieved files to the cache.
        for (DataRecord retrievedFile : retrievedFiles)
        {
            dataFileCache.put(retrievedFile.getID(), retrievedFile);
            requestedFileList.add(retrievedFile);
        }

        // Return the requested files.
        return requestedFileList;
    }

    @Override
    public void clearCache()
    {
        recordCache.clear();
    }

    @Override
    public void addDataRecord(DataRecord dataRecord)
    {
        recordCache.put(dataRecord.getID(), dataRecord);
    }

    @Override
    public List<DataRecord> getCachedDataRecords()
    {
        return new ArrayList<DataRecord>(recordCache.values());
    }
}
