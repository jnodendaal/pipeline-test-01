package com.pilog.t8.data.document.requirementtype;

import java.math.BigDecimal;

/**
 * @author Bouwer du Preez
 */
public enum RequirementAttribute
{
    DR_ID(RequirementAttributeType.STRING, "Data Requirement ID"),
    CLASS_ID(RequirementAttributeType.STRING, "Class ID"),
    SECTION_ID(RequirementAttributeType.STRING, "Section ID"),
    PROPERTY_ID(RequirementAttributeType.STRING, "Property ID"),
    REQUIREMENT_CONCEPT_ID(RequirementAttributeType.STRING, "Concept ID"),
    CONCEPT_TYPE_ID(RequirementAttributeType.STRING, "Concept Type ID"),
    CHARACTERISTIC(RequirementAttributeType.STRING, "Characteristic"),
    UNIQUE(RequirementAttributeType.BOOLEAN, "Unique"),
    PRESCRIBED_VALUE(RequirementAttributeType.BOOLEAN, "Prescribed Value"),
    RESTRICT_TO_APPROVED_VALUE(RequirementAttributeType.BOOLEAN, "Restrict To Approved Value"),
    ALLOW_NEW_VALUE(RequirementAttributeType.BOOLEAN, "Allow New Value"),
    FORMAT_MASK(RequirementAttributeType.STRING, "Format Mask"),
    FORMAT_PATTERN(RequirementAttributeType.STRING, "Format Pattern"),
    DATE_TIME_FORMAT_PATTERN(RequirementAttributeType.STRING, "Date Time Format Pattern"),
    MAXIMUM_STRING_LENGTH(RequirementAttributeType.NUMBER, "Maximum Length"),
    MINIMUM_NUMERIC_VALUE(RequirementAttributeType.NUMBER, "Minimum Numeric Value"),
    MAXIMUM_NUMERIC_VALUE(RequirementAttributeType.NUMBER, "Maximum Numeric Value"),
    FORMAT_DESCRIPTION(RequirementAttributeType.STRING, "Format Description"),
    MINIMUM_REFERENCES(RequirementAttributeType.NUMBER, "Minimum References"),
    MAXIMUM_REFERENCES(RequirementAttributeType.NUMBER, "Maximum References"),
    MAXIMUM_REFERENCES_PER_DR_INSTANCE(RequirementAttributeType.NUMBER, "Maximum References per DR Instance"),
    MAXIMUM_ATTACHMENTS(RequirementAttributeType.NUMBER, "Maximum Attachments"),
    ALLOW_DUPLICATE_FILENAMES(RequirementAttributeType.BOOLEAN, "Allow Duplicate Filenames"),
    INDEPENDENT(RequirementAttributeType.BOOLEAN, "Independent"),
    LOCAL(RequirementAttributeType.BOOLEAN, "Local"),
    FILE_SIZE_RESTRICTION(RequirementAttributeType.NUMBER, "File Size Restriction"),
    FILE_TYPE_RESTRICTION(RequirementAttributeType.STRING, "File Type Restriction"),
    CASE(RequirementAttributeType.STRING, "Case");

    private final RequirementAttributeType type;
    private final String displayName;

    public enum RequirementAttributeType {NUMBER, STRING, BOOLEAN};

    RequirementAttribute(RequirementAttributeType type, String displayName)
    {
        this.type = type;
        this.displayName = displayName;
    }

    public RequirementAttributeType getType()
    {
        return type;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getIdentifier()
    {
        return this.toString();
    }

    public static RequirementAttributeType getValueType(Object attributeValue)
    {
        if (attributeValue instanceof BigDecimal) return RequirementAttribute.RequirementAttributeType.NUMBER;
        else if (attributeValue instanceof Boolean) return RequirementAttribute.RequirementAttributeType.BOOLEAN;
        else return RequirementAttribute.RequirementAttributeType.STRING;
    }

    public static String getValueTypeString(Object attributeValue)
    {
        return getValueType(attributeValue).toString();
    }

    /**
     * A safe method for converting a String to one of the value of this enum.
     * If no enum is found matching the specified identifier, null is returned
     * instead of the usual exception that is thrown when valueOf() is used.
     * @param identifier The identifier of the enum to find.
     * @return The RequirementType matching the specified identifier.
     */
    public static RequirementAttribute getRequirementAttribute(String identifier)
    {
        for (RequirementAttribute attribute : RequirementAttribute.values())
        {
            if (attribute.getIdentifier().equals(identifier))
            {
                return attribute;
            }
        }

        return null;
    }
}
