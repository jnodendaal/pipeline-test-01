package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.path.PathRoot;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class DataRequirementAccessStep extends DefaultPathStep implements PathStep
{
    public DataRequirementAccessStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof PathRoot)
        {
            return evaluateInstanceStep(ArrayLists.newArrayList(((PathRoot)input).getDataRecord().getAccessLayer()));
        }
        else if (input instanceof DataRecord)
        {
            return evaluateInstanceStep(ArrayLists.newArrayList(((DataRecord)input).getAccessLayer()));
        }
        else if (input instanceof RecordProperty)
        {
            RecordProperty recordProperty;
            DataRecordAccessLayer recordAccessLayer;

            recordProperty = (RecordProperty)input;
            recordAccessLayer = recordProperty.getParentDataRecord().getAccessLayer();
            return evaluateInstanceStep(recordAccessLayer.getSubRecordAccessLayersReferencedFromProperty(recordProperty.getId()));
        }
        else if (input instanceof DataRecordAccessLayer)
        {
            return evaluateInstanceStep(((DataRecordAccessLayer)input).getSubRecordAccessLayers());
        }
        else if (input instanceof PropertyAccessLayer)
        {
            PropertyAccessLayer propertyAccessLayer;
            DataRecordAccessLayer recordAccessLayer;

            propertyAccessLayer = (PropertyAccessLayer)input;
            recordAccessLayer = propertyAccessLayer.getParentAccessLayer().getParentAccessLayer();
            return evaluateInstanceStep(recordAccessLayer.getSubRecordAccessLayersReferencedFromProperty(propertyAccessLayer.getPropertyId()));
        }
        else if (input instanceof List)
        {
            List<DataRecordAccessLayer> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<DataRecordAccessLayer>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<DataRecordAccessLayer>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("DR Access step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<DataRecordAccessLayer> evaluateInstanceStep(List<DataRecordAccessLayer> accessLayerList)
    {
        // If we have ID's to use for narrowing of the list do it now.
        if (idList != null)
        {
            Iterator<DataRecordAccessLayer> accessLayerIterator;

            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                DataRecordAccessLayer nextAccessLayer;

                nextAccessLayer = accessLayerIterator.next();
                if (!idList.contains(nextAccessLayer.getDataRequirementID()))
                {
                    accessLayerIterator.remove();
                }
            }
        }

        // Now run through the remaining items in the list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<DataRecordAccessLayer> accessLayerIterator;

            // Iterator over the access layers and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                DataRecordAccessLayer nextAccessLayer;

                // Create a map containing the input parameters available to the predicate expression.
                nextAccessLayer = accessLayerIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("DR_ID", nextAccessLayer.getDataRequirementID());
                inputParameters.put("DR_INSTANCE_ID", nextAccessLayer.getDataRequirementInstanceId());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        accessLayerIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return accessLayerList;
    }
}
