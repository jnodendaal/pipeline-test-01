package com.pilog.t8.data.document.datastring;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringParticleSettingsList
{
    private final List<T8DataStringParticleSettings> particleList;

    public T8DataStringParticleSettingsList()
    {
        this.particleList = new ArrayList<T8DataStringParticleSettings>();
    }

    public void addParticleSettings(T8DataStringParticleSettings particle)
    {
        particleList.add(particle);
    }

    public void addParticleSettings(T8DataStringParticleSettingsList particles)
    {
        this.particleList.addAll(particles.getParticleSettings());
    }

    public T8DataStringParticleSettings getParticleSettings(T8DataStringGeneratorContext dsContext, T8DataStringInstance dsInstance, Value inputValue)
    {
        if (inputValue instanceof DataRecord)
        {
            DataRecord record;

            record = (DataRecord)inputValue;
            return T8DataStringParticleSettingsList.this.getRecordParticleSettings(dsContext, dsInstance, record);
        }
        else if (inputValue instanceof RecordProperty)
        {
            RecordProperty property;

            property = (RecordProperty)inputValue;
            return getPropertyParticleSettings(dsContext, dsInstance, property);
        }
        else // instanceof RecordValue
        {
            RecordValue value;

            value = (RecordValue)inputValue;
            return null; // TODO: Implement particle search for record values.
        }
    }

    public List<T8DataStringParticleSettings> getParticleSettings()
    {
        return new ArrayList<T8DataStringParticleSettings>(particleList);
    }

    private T8DataStringParticleSettings getRecordParticleSettings(T8DataStringGeneratorContext dsContext, T8DataStringInstance dsInstance, DataRecord record)
    {
        T8DataStringParticleSettings drInstanceSpecificParticle = null;
        T8DataStringParticleSettings drSpecificParticle = null;
        String recordId;
        String drInstanceId;
        String drId;

        recordId = record.getID();
        drId = record.getDataRequirementID();
        drInstanceId = record.getDataRequirementInstanceID();

        // Find the most applicable particle.
        for (T8DataStringParticleSettings particle : particleList)
        {
            // First make sure this particle is applicable to the rendering instance.
            if (particle.matchesDataStringInstance(dsContext, dsInstance))
            {
                // No make sure that the particle is a record type particle.
                if (particle.isRecordParticle())
                {
                    if (recordId.equals(particle.getRecordId()))
                    {
                        // If it is record-specific we can always return it because there is no better option.
                        return particle;
                    }
                    else if (drInstanceId.equals(particle.getDrInstanceId()))
                    {
                        // We have to keep track of this particle because we may still find a record-specific one further down in the list.
                        drInstanceSpecificParticle = particle;
                    }
                    else if (drId.equals(particle.getDrId()))
                    {
                        // We have to keep track of this particle because we may still find a more specific one further down in the list.
                        drSpecificParticle = particle;
                    }
                }
            }
        }

        // Return the most applicable particle.
        if (drInstanceSpecificParticle != null) return drInstanceSpecificParticle;
        else return drSpecificParticle;
    }

    private T8DataStringParticleSettings getPropertyParticleSettings(T8DataStringGeneratorContext dsContext, T8DataStringInstance dsInstance, RecordProperty property)
    {
        T8DataStringParticleSettings drInstanceSpecificParticle = null;
        T8DataStringParticleSettings drSpecificParticle = null;
        String recordId;
        String drInstanceId;
        String drId;
        String propertyId;

        recordId = property.getRecordID();
        drId = property.getDataRequirementID();
        drInstanceId = property.getDataRequirementInstanceID();
        propertyId = property.getPropertyID();

        // Find the most applicable particle.
        for (T8DataStringParticleSettings particle : particleList)
        {
            // First make sure this particle is applicable to the rendering instance.
            if (particle.matchesDataStringInstance(dsContext, dsInstance))
            {
                // Now make sure that the particle is a property type particle.
                if (particle.isPropertyParticle())
                {
                    // Check the particle's property ID against the property being rendered.
                    if (particle.getPropertyId().equals(propertyId))
                    {
                        if (recordId.equals(particle.getRecordId()))
                        {
                            // If it is record-specific we can always return it because there is no better option.
                            return particle;
                        }
                        else if (drInstanceId.equals(particle.getDrInstanceId()))
                        {
                            // We have to keep track of this particle because we may still find a record-specific one further down in the list.
                            drInstanceSpecificParticle = particle;
                        }
                        else if (drId.equals(particle.getDrId()))
                        {
                            // We have to keep track of this particle because we may still find a more specific one further down in the list.
                            drSpecificParticle = particle;
                        }
                    }
                }
            }
        }

        // Return the most applicable particle.
        if (drInstanceSpecificParticle != null) return drInstanceSpecificParticle;
        else return drSpecificParticle;
    }

    public T8DataStringParticleSettingsList getRecordParticleSettingsList(String recordId)
    {
        T8DataStringParticleSettingsList recordParticles;

        recordParticles = new T8DataStringParticleSettingsList();
        for (T8DataStringParticleSettings particle : particleList)
        {
            if (recordId.equals(particle.getRecordId()))
            {
                recordParticles.addParticleSettings(particle);
            }
        }

        return recordParticles;
    }

    public T8DataStringParticleSettingsList getDrInstanceParticleSettingsList(String drInstanceId)
    {
        T8DataStringParticleSettingsList drInstanceParticles;

        drInstanceParticles = new T8DataStringParticleSettingsList();
        for (T8DataStringParticleSettings particle : particleList)
        {
            if (drInstanceId.equals(particle.getDrInstanceId()))
            {
                drInstanceParticles.addParticleSettings(particle);
            }
        }

        return drInstanceParticles;
    }

    public T8DataStringParticleSettingsList getDrParticleSettingsList(String drId)
    {
        T8DataStringParticleSettingsList drParticles;

        drParticles = new T8DataStringParticleSettingsList();
        for (T8DataStringParticleSettings particle : particleList)
        {
            if (drId.equals(particle.getDrId()))
            {
                drParticles.addParticleSettings(particle);
            }
        }

        return drParticles;
    }
}
