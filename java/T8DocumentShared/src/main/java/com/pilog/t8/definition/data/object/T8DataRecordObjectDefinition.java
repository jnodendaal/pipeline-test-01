package com.pilog.t8.definition.data.object;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectHandler;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordObjectDefinition extends T8DataObjectDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_RECORD";
    public static final String DISPLAY_NAME = "Data Record Object";
    public static final String DESCRIPTION = "An object referencing a Data Record.";
    public enum Datum
    {
        DR_IIDS,
        DR_IDS,
        DATA_FILE_DR_IIDS,
        DATA_FILE_DR_IDS,
        LANGUAGE_IDS,
        STATELESS,
        DATA_ENTITY_ID,
        ENTITY_FIELD_MAPPING,
        REFRESH_TRIGGER,
        CONSTRUCTOR_SCRIPT
    };
    // -------- Definition Meta-Data -------- //

    public enum RefreshTrigger {CONTENT, CONTENT_OR_DESCENDANT, LINEAGE, FILE};

    public T8DataRecordObjectDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DR_IIDS.toString(), "DR Instance Ids",  "The DR Instances applicable to this object."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DR_IDS.toString(), "DR Ids",  "The DRs applicable to this object."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DATA_FILE_DR_IIDS.toString(), "Data File DR Instance Ids",  "The Data File DR Instances applicable to this object (if empty, no filter will be applied)."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DATA_FILE_DR_IDS.toString(), "Data File DR Ids",  "The Data File DRs applicable to this object (if empty, no filter will be applied)."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.LANGUAGE_IDS.toString(), "Language Ids",  "The languages applicable to this object."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.STATELESS.toString(), "Stateless",  "If enabled, this object's state will not be created or tracked.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REFRESH_TRIGGER.toString(), "Refresh Trigger",  "The trigger that will cause the persisted state of this object to be refreshed.", RefreshTrigger.CONTENT_OR_DESCENDANT.toString(), T8DefinitionDatumType.T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_ID.toString(), "Data Entity", "The identifier of the data entity to which the object data will be written."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.ENTITY_FIELD_MAPPING.toString(), "Field Mapping:  Object to Entity", "A mapping of data object fields the corresponding entity fields where values are persisted."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CONSTRUCTOR_SCRIPT.toString(), "Constructor Script",  "The script used to extract and compile data from data records into data objects."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_ENTITY_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REFRESH_TRIGGER.toString().equals(datumId)) return createStringOptions(RefreshTrigger.values());
        else if (Datum.CONSTRUCTOR_SCRIPT.toString().equals(datumId)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataObjectConstructorScriptDefinition.TYPE_IDENTIFIER));
        else if (Datum.ENTITY_FIELD_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityId();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataObjectFieldDefinition> objectFieldDefinitions;
                    List<T8DataEntityFieldDefinition> entityFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    objectFieldDefinitions = getFieldDefinitions();
                    entityFieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((objectFieldDefinitions != null) && (entityFieldDefinitions != null))
                    {
                        ArrayList<String> fieldIds;

                        fieldIds = new ArrayList<String>();
                        for (T8DataEntityFieldDefinition fieldDefinition : entityFieldDefinitions)
                        {
                            fieldIds.add(fieldDefinition.getPublicIdentifier());
                        }
                        Collections.sort(fieldIds);

                        identifierMap.put("$ID", fieldIds);
                        for (T8DataObjectFieldDefinition fieldDefinition : objectFieldDefinitions)
                        {
                            identifierMap.put(fieldDefinition.getIdentifier(), fieldIds);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DataObjectHandler getDataObjectHandler(T8DataTransaction tx)
    {
        return T8Reflections.getInstance("com.pilog.t8.data.object.T8DataRecordObjectHandler", new Class<?>[]{T8DataRecordObjectDefinition.class, T8DataTransaction.class}, this, tx);
    }

    public boolean isStateless()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.STATELESS);
        return value != null && value;
    }

    public void setStateless(boolean stateless)
    {
        setDefinitionDatum(Datum.STATELESS, stateless);
    }

    public List<String> getLanguageIds()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.LANGUAGE_IDS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setLanguageIds(List<String> languageIds)
    {
        setDefinitionDatum(Datum.LANGUAGE_IDS.toString(), languageIds);
    }

    public List<String> getDrIds()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DR_IDS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDrIds(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DR_IDS.toString(), identifiers);
    }

    public List<String> getDrIids()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DR_IIDS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDrIids(List<String> drIids)
    {
        setDefinitionDatum(Datum.DR_IIDS.toString(), drIids);
    }

    public List<String> getDataFileDrIds()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DATA_FILE_DR_IDS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDataFileDrIds(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DATA_FILE_DR_IDS.toString(), identifiers);
    }

    public List<String> getDataFileDrIids()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DATA_FILE_DR_IIDS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDataFileDrIids(List<String> drIids)
    {
        setDefinitionDatum(Datum.DATA_FILE_DR_IIDS.toString(), drIids);
    }

    public String getDataEntityId()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_ID.toString());
    }

    public void setDataEntityId(String id)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_ID.toString(), id);
    }

    public Map<String, String> getEntityFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.ENTITY_FIELD_MAPPING.toString());
    }

    public void setEntityFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.ENTITY_FIELD_MAPPING.toString(), mapping);
    }

    public RefreshTrigger getViewRefreshTrigger()
    {
        String value;

        value = (String)getDefinitionDatum(Datum.REFRESH_TRIGGER.toString());
        return value != null ? RefreshTrigger.valueOf(value) : RefreshTrigger.CONTENT_OR_DESCENDANT;
    }

    public void setViewRefreshTrigger(RefreshTrigger trigger)
    {
        setDefinitionDatum(Datum.REFRESH_TRIGGER.toString(), trigger.toString());
    }

    public T8DataObjectConstructorScriptDefinition getConstructorScript()
    {
        return getDefinitionDatum(Datum.CONSTRUCTOR_SCRIPT);
    }

    public void setConstructorScript(T8DataObjectConstructorScriptDefinition definition)
    {
        setDefinitionDatum(Datum.CONSTRUCTOR_SCRIPT, definition);
    }
}
