package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileSaveResult implements Serializable
{
    private DataRecord existingFile;
    private DataRecord newFile;
    private T8DataFileValidationReport validationReport;
    private T8DataFileAlteration alteration;
    private boolean saved;

    public T8DataFileSaveResult(DataRecord existingFile, DataRecord newFile, T8DataFileValidationReport validationReport, boolean saved, T8DataFileAlteration alteration)
    {
        this.existingFile = existingFile;
        this.newFile = newFile;
        this.validationReport = validationReport;
        this.saved = saved;
        this.alteration = alteration;
    }

    public String getDataFileID()
    {
        return newFile != null ? newFile.getID() : null;
    }

    public DataRecord getExistingFile()
    {
        return existingFile;
    }

    public void setExistingFile(DataRecord existingFile)
    {
        this.existingFile = existingFile;
    }

    public DataRecord getNewFile()
    {
        return newFile;
    }

    public void setNewFile(DataRecord newFile)
    {
        this.newFile = newFile;
    }

    public T8DataFileValidationReport getValidationReport()
    {
        return validationReport;
    }

    public void setValidationReport(T8DataFileValidationReport validationReport)
    {
        this.validationReport = validationReport;
    }

    public boolean isSaved()
    {
        return saved;
    }

    public void setSaved(boolean saved)
    {
        this.saved = saved;
    }

    public T8DataFileAlteration getAlteration()
    {
        return alteration;
    }

    public void setAlteration(T8DataFileAlteration alteration)
    {
        this.alteration = alteration;
    }
}
