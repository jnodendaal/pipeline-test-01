package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.value.DocumentReference.DocumentReferenceType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceListContent extends ValueContent
{
    private final List<DocumentReferenceContent> references;
    private boolean independent;

    public DocumentReferenceListContent()
    {
        super(RequirementType.DOCUMENT_REFERENCE);
        this.references = new ArrayList<DocumentReferenceContent>();
    }

    public boolean isIndependent()
    {
        return independent;
    }

    public void setIndependent(boolean independent)
    {
        this.independent = independent;
    }

    public void addReference(DocumentReferenceContent reference)
    {
        if (!containsReference(reference.getConceptId()))
        {
            references.add(reference);
        }
        else throw new IllegalArgumentException("Input list " + this + " already contains reference: " + reference);
    }

    public DocumentReferenceContent addReference(String recordId)
    {
        return addReference(DocumentReferenceType.RECORD, recordId);
    }

    public DocumentReferenceContent addReference(DocumentReferenceType referenceType, String conceptId)
    {
        if (conceptId != null)
        {
            if (!containsReference(conceptId))
            {
                DocumentReferenceContent newReference;

                // Add the new referfenced record ID to the list.
                newReference = new DocumentReferenceContent(referenceType, conceptId);
                references.add(newReference);
                return newReference;
            }
            else return null;
        }
        else throw new IllegalArgumentException("Cannot add null reference to reference list: " + this);
    }

    public void addReferences(Collection<String> newReferences)
    {
        if (!newReferences.contains(null))
        {
            for (String newReference : newReferences)
            {
                addReference(newReference);
            }
        }
        else throw new IllegalArgumentException("Input list " + newReferences + " contains a null reference that cannot be added to reference list: " + this);
    }

    public boolean replaceReference(String oldConceptId, String newConceptId)
    {
        DocumentReferenceContent oldReference;

        oldReference = getReference(oldConceptId);
        if (oldReference != null)
        {
            oldReference.setConceptId(newConceptId);
            return true;
        }
        else return false;
    }

    public List<String> getReferenceIds()
    {
        List<String> ids;

        ids = new ArrayList<>();
        for (DocumentReferenceContent reference : references)
        {
            ids.add(reference.getConceptId());
        }

        return ids;
    }

    public DocumentReferenceContent getReference(String conceptId)
    {
        for (DocumentReferenceContent reference : references)
        {
            if (reference.getConceptId().equals(conceptId)) return reference;
        }

        return null;
    }

    public List<DocumentReferenceContent> getReferences()
    {
        return new ArrayList<DocumentReferenceContent>(references);
    }

    public boolean containsReference(String conceptId)
    {
        for (DocumentReferenceContent reference : references)
        {
            if (reference.getConceptId().equals(conceptId)) return true;
        }

        return false;
    }

    public int getReferenceCount()
    {
        return references.size();
    }

    public DocumentReferenceContent removeReference(String conceptId)
    {
        DocumentReferenceContent reference;

        reference = getReference(conceptId);
        references.remove(reference);
        return reference;
    }

    public void clear()
    {
        references.clear();
    }

    @Override
    public boolean isEqualTo(ValueContent toValueContent)
    {
        if (toValueContent instanceof DocumentReferenceListContent)
        {
            DocumentReferenceListContent toReferenceListContent;

            toReferenceListContent = (DocumentReferenceListContent)toValueContent;
            if (toReferenceListContent.getReferenceCount() != getReferenceCount()) return false;
            else
            {
                for (DocumentReferenceContent reference : references)
                {
                    DocumentReferenceContent toReference;

                    toReference = toReferenceListContent.getReference(reference.getConceptId());
                    if (toReference != null)
                    {
                        if (!reference.isEqualTo(toReference)) return false;
                    }
                    else return false;
                }

                return true;
            }
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toValueContent)
    {
        if (toValueContent instanceof DocumentReferenceListContent)
        {
            DocumentReferenceListContent toReferenceListContent;

            toReferenceListContent = (DocumentReferenceListContent)toValueContent;
            if (toReferenceListContent.getReferenceCount() != getReferenceCount()) return false;
            else
            {
                for (DocumentReferenceContent reference : references)
                {
                    DocumentReferenceContent toReference;

                    toReference = toReferenceListContent.getReference(reference.getConceptId());
                    if (toReference != null)
                    {
                        if (!reference.isEqualTo(toReference)) return false;
                    }
                    else return false;
                }

                return true;
            }
        }
        else return false;
    }

    @Override
    public String toString()
    {
        return "DocumentReferenceListContent{" + "references=" + references + '}';
    }
}
