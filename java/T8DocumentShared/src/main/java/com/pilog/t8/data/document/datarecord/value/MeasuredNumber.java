package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.NumberRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredNumber extends RecordValue implements MeasurableValue
{
    public MeasuredNumber(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public MeasuredNumberRequirement getRequirement()
    {
        return (MeasuredNumberRequirement)valueRequirement;
    }

    @Override
    public MeasuredNumberContent getContent()
    {
        MeasuredNumberContent content;
        UnitOfMeasure uom;
        QualifierOfMeasure qom;
        Number number;

        number = getNumberObject();
        uom = getUom();
        qom = getQom();

        content = new MeasuredNumberContent();
        if (number != null) content.getNumber().setContent(number.getContent());
        if (uom != null) content.getUnitOfMeasure().setContent(uom.getContent());
        if (qom != null) content.getQualifierOfMeasure().setContent(qom.getContent());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        MeasuredNumberContent mnContent;
        NumberContent number;
        UnitOfMeasureContent uom;
        QualifierOfMeasureContent qom;

        mnContent = (MeasuredNumberContent)valueContent;
        number = mnContent.getNumber();
        uom = mnContent.getUnitOfMeasure();
        qom = mnContent.getQualifierOfMeasure();

        if ((number != null) && (number.hasContent())) getOrCreateNumber().setContent(mnContent.getNumber());
        if ((uom != null) && (uom.hasContent())) getOrCreateUom().setContent(mnContent.getUnitOfMeasure());
        if ((qom != null) && (qom.hasContent())) getOrCreateQom().setContent(mnContent.getQualifierOfMeasure());
    }

    @Override
    public UnitOfMeasure getUom()
    {
        for (RecordValue subValue : subValues)
        {
            if (subValue instanceof UnitOfMeasure) return (UnitOfMeasure)subValue;
        }

        return null;
    }

    @Override
    public QualifierOfMeasure getQom()
    {
        for (RecordValue subValue : subValues)
        {
            if (subValue instanceof QualifierOfMeasure) return (QualifierOfMeasure)subValue;
        }

        return null;
    }

    @Override
    public String getUomId()
    {
        UnitOfMeasure uom;

        uom = getUom();
        return uom != null ? uom.getValue() : null;
    }

    @Override
    public String getQomId()
    {
        QualifierOfMeasure qom;

        qom = getQom();
        return qom != null ? qom.getValue() : null;
    }

    @Override
    public UnitOfMeasure setUomId(String conceptId)
    {
        if (conceptId != null)
        {
            UnitOfMeasure uomValue;

            uomValue = getUom();
            if (uomValue != null)
            {
                uomValue.setValue(conceptId);
                return uomValue;
            }
            else
            {
                ValueRequirement uomRequirement;

                // Get the UOM requirement and construct the value from it.
                uomRequirement = ((MeasuredNumberRequirement)valueRequirement).getUomRequirement();
                if (uomRequirement != null)
                {
                    uomValue = (UnitOfMeasure)RecordValue.createValue(uomRequirement);
                    uomValue.setValue(conceptId);
                    setSubValue(uomValue);
                    return uomValue;
                }
                else throw new IllegalStateException("No UOM Requirement specified for measured number type: " + this);
            }
        }
        else
        {
            clearUom();
            return null;
        }
    }

    public UnitOfMeasure getOrCreateUom()
    {
        UnitOfMeasure uomValue;

        uomValue = getUom();
        if (uomValue != null)
        {
            return uomValue;
        }
        else
        {
            ValueRequirement uomRequirement;

            // Get the UOM requirement and construct the value from it.
            uomRequirement = ((MeasuredNumberRequirement)valueRequirement).getUomRequirement();
            if (uomRequirement != null)
            {
                uomValue = (UnitOfMeasure)RecordValue.createValue(uomRequirement);
                setSubValue(uomValue);
                return uomValue;
            }
            else throw new IllegalStateException("No UOM Requirement specified for measured number type: " + this);
        }
    }

    @Override
    public RecordValue clearUom()
    {
        RecordValue uom;

        uom = getSubValue(RequirementType.UNIT_OF_MEASURE, null, null);
        if (uom != null)
        {
            this.removeSubValue(uom);
            return uom;
        }
        else return null;
    }

    @Override
    public RecordValue setQomId(String conceptID)
    {
        if (conceptID != null)
        {
            RecordValue qomValue;

            qomValue = getQom();
            if (qomValue != null)
            {
                qomValue.setValue(conceptID);
                return qomValue;
            }
            else
            {
                ValueRequirement qomRequirement;

                // Get the QOM requirement and construct the value from it.
                qomRequirement = ((MeasuredNumberRequirement)valueRequirement).getQomRequirement();
                if (qomRequirement != null)
                {
                    qomValue = RecordValue.createValue(qomRequirement);
                    qomValue.setValue(conceptID);
                    setSubValue(qomValue);
                    return qomValue;
                }
                else throw new IllegalStateException("No QOM Requirement specified for measured number type: " + this);
            }
        }
        else
        {
            clearQom();
            return null;
        }
    }

    public QualifierOfMeasure getOrCreateQom()
    {
        QualifierOfMeasure qomValue;

        qomValue = getQom();
        if (qomValue != null)
        {
            return qomValue;
        }
        else
        {
            ValueRequirement qomRequirement;

            // Get the QOM requirement and construct the value from it.
            qomRequirement = ((MeasuredNumberRequirement)valueRequirement).getQomRequirement();
            if (qomRequirement != null)
            {
                qomValue = (QualifierOfMeasure)RecordValue.createValue(qomRequirement);
                setSubValue(qomValue);
                return qomValue;
            }
            else throw new IllegalStateException("No QOM Requirement specified for measured number type: " + this);
        }
    }

    @Override
    public RecordValue clearQom()
    {
        RecordValue qom;

        qom = getSubValue(RequirementType.QUALIFIER_OF_MEASURE, null, null);
        if (qom != null)
        {
            this.removeSubValue(qom);
            return qom;
        }
        else return null;
    }

    public Number getNumberObject()
    {
        for (RecordValue subValue : subValues)
        {
            if (subValue instanceof Number) return (Number)subValue;
        }

        return null;
    }

    public String getNumber()
    {
        RecordValue numeric;

        numeric = getNumberObject();
        return numeric != null ? numeric.getValue() : null;
    }

    public Number setNumber(String value)
    {
        NumberRequirement numericRequirement;

        numericRequirement = ((MeasuredNumberRequirement)valueRequirement).getNumberRequirement();
        if (numericRequirement != null)
        {
            Number number;

            // Get the current numeric RecordValue or create it if it does not exist.
            number = getNumberObject();
            if (number == null)
            {
                number = (Number)RecordValue.createValue(numericRequirement);
                number.setValue(value);
                setSubValue(number);
                return number;
            }
            else
            {
                number.setValue(value);
                return number;
            }
        }
        else throw new IllegalStateException("No value requirement specified for numeric value in Measured Number Value: " + this);
    }

    public Number getOrCreateNumber()
    {
        Number number;

        number = getNumberObject();
        if (number != null)
        {
            return number;
        }
        else
        {
            NumberRequirement numericRequirement;

            numericRequirement = ((MeasuredNumberRequirement)valueRequirement).getNumberRequirement();
            if (numericRequirement != null)
            {
                number = (Number)RecordValue.createValue(numericRequirement);
                setSubValue(number);
                return number;
            }
            else throw new IllegalStateException("No value requirement specified for numeric value in Measured Number Value: " + this);
        }
    }
}
