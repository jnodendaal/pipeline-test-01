package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.RecordProperty;

/**
 * @author Bouwer du Preez
 */
public class T8RecordPropertyStateChange
{
    private RecordProperty fromState;
    private RecordProperty toState;

    public T8RecordPropertyStateChange(RecordProperty fromState, RecordProperty toState)
    {
        evaluateStateChange(fromState, toState);
    }

    public RecordProperty getFromState()
    {
        return fromState;
    }

    public RecordProperty getToState()
    {
        return toState;
    }

    
    private void evaluateStateChange(RecordProperty fromStateProperty, RecordProperty toStateProperty)
    {
        this.fromState = fromStateProperty;
        this.toState = toStateProperty;
        if (fromStateProperty == null)
        {

        }
        else if (toStateProperty == null)
        {

        }
        else
        {

        }
    }
}
