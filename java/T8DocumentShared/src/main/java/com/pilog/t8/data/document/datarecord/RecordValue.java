package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.BooleanValue;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.DateTimeValue;
import com.pilog.t8.data.document.datarecord.value.DateValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.IntegerValue;
import com.pilog.t8.data.document.datarecord.value.LocalizedStringList;
import com.pilog.t8.data.document.datarecord.value.LocalizedStringValue;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.document.datarecord.value.RealValue;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarecord.value.TextValue;
import com.pilog.t8.data.document.datarecord.value.TimeValue;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.document.datarecord.value.ValueSet;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueStructureChangedEvent;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.LowerBoundNumber;
import com.pilog.t8.data.document.datarecord.value.UpperBoundNumber;
import com.pilog.t8.data.document.datarecord.value.Number;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public abstract class RecordValue implements Value, Serializable
{
    private RecordProperty parentRecordProperty;
    private RecordValue parentRecordValue;
    protected ValueRequirement valueRequirement;
    protected String valueID;
    protected String value;
    protected String fft;
    private boolean standard;
    private boolean approved;
    private String prescribedValueId;
    protected ArrayList<RecordValue> subValues;

    public RecordValue(String valueID, ValueRequirement valueRequirement, String value)
    {
        this.valueID = valueID;
        this.value = value;
        this.parentRecordProperty = null;
        this.parentRecordValue = null;
        this.valueRequirement = valueRequirement;
        this.subValues = new ArrayList<>();
        this.standard = false;
        if (valueRequirement == null) throw new RuntimeException("Cannot construct RecordValue with null ValueRequirement.");
    }

    public RecordValue(ValueRequirement valueRequirement)
    {
        this(T8IdentifierUtilities.createNewGUID(), valueRequirement, null);
    }

    public RecordValue(ValueRequirement valueRequirement, String value)
    {
        this(T8IdentifierUtilities.createNewGUID(), valueRequirement, value);
    }

    void setParentRecordProperty(RecordProperty recordProperty)
    {
        this.parentRecordProperty = recordProperty;
        for (RecordValue subValue : subValues)
        {
            subValue.setParentRecordProperty(recordProperty);
        }
    }

    public String getValueID()
    {
        return valueID;
    }

    @Override
    public int getSubValueCount()
    {
        return subValues.size();
    }

    void setParentRecordValue(RecordValue recordValue)
    {
        this.parentRecordValue = recordValue;
    }

    public DataRecord getParentDataRecord()
    {
        return parentRecordProperty != null ? parentRecordProperty.getParentDataRecord() : null;
    }

    public RecordProperty getParentRecordProperty()
    {
        return parentRecordProperty;
    }

    public RecordValue getParentRecordValue()
    {
        return parentRecordValue;
    }

    public ValueRequirement getValueRequirement()
    {
        return valueRequirement;
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return valueRequirement != null ? valueRequirement.getParentPropertyRequirement() : null;
    }

    public SectionRequirement getSectionRequirement()
    {
        PropertyRequirement propertyRequirement;

        propertyRequirement = getPropertyRequirement();
        return propertyRequirement != null ? propertyRequirement.getParentSectionRequirement() : null;
    }

    public DataRequirement getDataRequirement()
    {
        SectionRequirement sectionRequirement;

        sectionRequirement = getSectionRequirement();
        return sectionRequirement != null ? sectionRequirement.getParentDataRequirement() : null;
    }

    public DataRequirementInstance getDataRequirementInstance()
    {
        DataRequirement dataRequirement;

        dataRequirement = getDataRequirement();
        return dataRequirement != null ? dataRequirement.getParentDataRequirementInstance() : null;
    }

    public String getRecordID()
    {
        DataRecord parentDataRecord;

        parentDataRecord = getParentDataRecord();
        return parentDataRecord != null ? parentDataRecord.getID() : null;
    }

    public String getDataRequirementID()
    {
        DataRequirement dataRequirement;

        dataRequirement = getDataRequirement();
        return dataRequirement != null ? dataRequirement.getConceptID() : null;
    }

    public String getDataRequirementInstanceID()
    {
        DataRequirementInstance drInstance;

        drInstance = getDataRequirementInstance();
        return drInstance != null ? drInstance.getConceptID() : null;
    }

    public String getSectionID()
    {
        SectionRequirement sectionRequirement;

        sectionRequirement = getSectionRequirement();
        return sectionRequirement != null ? sectionRequirement.getConceptID() : null;
    }

    public String getPropertyID()
    {
        PropertyRequirement propertyRequirement;

        propertyRequirement = getPropertyRequirement();
        return propertyRequirement != null ? propertyRequirement.getConceptID() : null;
    }

    public boolean isComposite()
    {
        return valueRequirement.isComposite();
    }

    /**
     * Returns the field RecordVaue to which this value belongs.  If this value
     * is a field, it is returned.
     * @return The field RecordVaue to which this value belongs.
     */
    public Field getField()
    {
        if (isField())
        {
            return (Field)this;
        }
        else
        {
            return (Field)getAncestorValue(RequirementType.FIELD_TYPE, null, null);
        }
    }

    public String getFieldID()
    {
        return valueRequirement.getFieldID();
    }

    public String getRequirementTypeID()
    {
        return valueRequirement.getRequirementType().getID();
    }

    public RequirementType getRequirementType()
    {
        return valueRequirement.getRequirementType();
    }

    public void setRequirement(ValueRequirement requirement)
    {
        this.valueRequirement = requirement;
        for (RecordValue subValue : subValues)
        {
            ValueRequirement currentRequirement;
            ValueRequirement newRequirement;

            currentRequirement = subValue.getValueRequirement();
            newRequirement = requirement.getSubRequirement(currentRequirement.getRequirementType(), currentRequirement.getValue(), currentRequirement.getIndex());
            if (newRequirement != null)
            {
                subValue.setRequirement(newRequirement);
            }
            else throw new IllegalArgumentException("No replacement requirement found for: " + currentRequirement);
        }
    }

    public RecordValue copy(ValueRequirement valueRequirement)
    {
        RecordValue newValue;

        newValue = RecordValue.createValue(valueRequirement);
        newValue.setValue(value);
        for (RecordValue subValue : subValues)
        {
            ValueRequirement subValueRequirement;
            ValueRequirement newSubValueRequirement;

            subValueRequirement = subValue.getValueRequirement();
            newSubValueRequirement = valueRequirement.getDescendentRequirement(subValueRequirement.getRequirementType(), subValueRequirement.getValue(), subValueRequirement.getIndex());
            if (newSubValueRequirement != null)
            {
                RecordValue newSubValue;

                newSubValue = subValue.copy(newSubValueRequirement);
                newValue.setSubValue(newSubValue);
            }
        }

        return newValue;
    }

    public String getFFT()
    {
        return fft;
    }

    public void setFFT(String fft)
    {
        this.fft = fft;
    }

    public boolean hasFFT()
    {
        return ((fft != null) && (fft.trim().length() > 0));
    }

    public void appendFFT(String fft)
    {
        if (this.fft == null)
        {
            this.fft = fft;
        }
        else
        {
            this.fft += fft;
        }
    }

    public void appendFFTChecked(String fft, String separator)
    {
        if (!Strings.isNullOrEmpty(fft))
        {
            String newFFT;

            newFFT = fft.trim();
            if (Strings.isNullOrEmpty(this.fft))
            {
                this.fft = newFFT;
            }
            else if (!this.fft.toUpperCase().contains(newFFT.toUpperCase()))
            {
                this.fft += (separator + newFFT);
            }
        }
    }

    public String getValue()
    {
        return value;
    }

    /**
     * Returns a boolean value indicating whether or not this value is a standard prescribed value.
     * @return A boolean value indicating whether or not this value is a standard prescribed value.
     */
    public boolean isStandard()
    {
        return standard;
    }

    /**
     * Sets the flag indicating whether or not this value is a standard prescribed value.
     * @param standard
     */
    public void setStandard(boolean standard)
    {
        this.standard = standard;
    }

    /**
     * Returns a boolean value indicating whether or not this value is an approved prescribed value.
     * @return A boolean value indicating whether or not this value is an approved prescribed value.
     */
    public boolean isApproved()
    {
        return approved;
    }

    /**
     * Sets the flag indicating whether or not this value is an approved prescribed value.
     * @param approved
     */
    public void setApproved(boolean approved)
    {
        this.approved = approved;
    }

    /**
     * Returns the Prescribed Value ID of this field.  This method will return
     * null for all non-field values.
     * @return The Prescribed value ID of this field.
     */
    public String getPrescribedValueID()
    {
        return prescribedValueId;
    }

    /**
     * Sets the Prescribed value ID of this value and its descendants.
     * @param prescribedValueID The Prescribed value ID to set on this value.
     */
    public void setPrescribedValueID(String prescribedValueID) throws IllegalStateException
    {
        this.prescribedValueId = prescribedValueID;
        for (RecordValue subValue : subValues)
        {
            subValue.setPrescribedValueID(prescribedValueID);
        }
    }

    /**
     * Returns the plain value of this RecordValue.  In most cases the plain
     * value is simply the String value of this object but in some cases
     * depending on the value requirement, this may differ.  A plain value can
     * never be an empty string and will return as a null instead.
     * @return The plain value of this RecordValue.
     */
    public String getPlainValue()
    {
        if (valueRequirement.getRequirementType() == RequirementType.FIELD_TYPE)
        {
            RecordValue fieldValue;

            fieldValue = this.getFirstSubValue();
            return fieldValue != null ? fieldValue.getPlainValue() : null;
        }
        else
        {
            if (Strings.isNullOrEmpty(value))
            {
                return null;
            }
            else return value;
        }
    }

    public void setValue(String value)
    {
        if (!Objects.equals(this.value, value))
        {
            // Set the new value.
            this.value = value;

            // Fire the event to signal the changed.
            fireRecordValueChangedEvent(null);
        }
    }

    public int getIndex()
    {
        if (parentRecordValue != null)
        {
            return parentRecordValue.getSubValueIndex(this);
        }
        else if (parentRecordProperty != null)
        {
            return 0;
        }
        else return -1;
    }

    public int getSequence()
    {
        if (parentRecordProperty != null)
        {
            return parentRecordProperty.getAllRecordValues().indexOf(this);
        }
        else return -1;
    }

    public RecordValue getFirstSubValue()
    {
        return !subValues.isEmpty() ? subValues.get(0) : null;
    }

    public List<RecordValue> getRecordValues(List<ValueRequirement> pathRequirements)
    {
        List<RecordValue> valueList;

        valueList = new ArrayList<>();
        if (CollectionUtilities.isNullOrEmpty(pathRequirements))
        {
            return valueList;
        }
        else if (pathRequirements.get(0).isEquivalentRequirement(valueRequirement))
        {
            if (pathRequirements.size() > 1)
            {
                for (RecordValue subValue : subValues)
                {
                    valueList.addAll(subValue.getRecordValues(pathRequirements.subList(1, pathRequirements.size())));
                }

                return valueList;
            }
            else // We've reached the last requirement in the path, so just add this value (since we already know it matches the value requirement).
            {
                valueList.add(this);
                return valueList;
            }
        }
        else return valueList;
    }

    public ArrayList<RecordValue> getPathRecordValues()
    {
        ArrayList<RecordValue> pathValues;
        RecordValue currentValue;

        pathValues = new ArrayList<>();
        currentValue = this;
        while (currentValue != null)
        {
            pathValues.add(currentValue);
            currentValue = currentValue.getParentRecordValue();
        }

        Collections.reverse(pathValues);
        return pathValues;
    }

    public RecordValue getSubRecordValue(String value)
    {
        ArrayList<RecordValue> values;

        values = getSubRecordValues(value);
        return values.size() > 0 ? values.get(0) : null;
    }

    public ArrayList<RecordValue> getSubRecordValues(String value)
    {
        ArrayList<RecordValue> recordValues;

        recordValues = new ArrayList<>();
        for (RecordValue subValue : subValues)
        {
            if ((value == null) || (Objects.equals(subValue.getValue(), value)))
            {
                recordValues.add(subValue);
            }
        }

        return recordValues;
    }

    /**
     * Returns a list of all descendant values of this value, created in a
     * depth-first fashion.
     * @return A list of all descendant values of this value, created in a
     * depth-first fashion.
     */
    public ArrayList<RecordValue> getDescendantValues()
    {
        ArrayList<RecordValue> values;

        values = new ArrayList<>();
        for (RecordValue subValue : subValues)
        {
            values.add(subValue);
            values.addAll(subValue.getDescendantValues());
        }

        return values;
    }

    public ArrayList<RecordValue> getSubValues()
    {
        return new ArrayList<>(subValues);
    }

    public int getLastValueCollectionIndex(RequirementType requirementType, String conceptID)
    {
        List<Value> values;

        values = getSubValues(requirementType, conceptID, null);
        return values.size() > 0 ? subValues.indexOf(values.get(values.size()-1)) : -1;
    }

    @Override
    public RecordValue getSubValue(RequirementType requirementType, String conceptID, Integer index)
    {
        ArrayList<Value> values;

        values = getSubValues(requirementType, conceptID, index);
        return values.size() > 0 ? (RecordValue)values.get(0) : null;
    }

    @Override
    public ArrayList<Value> getSubValues(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        ArrayList<Value> recordValues;

        recordValues = new ArrayList<>();
        for (RecordValue subValue : subValues)
        {
            ValueRequirement subValueRequirement;

            subValueRequirement = subValue.getValueRequirement();
            if ((requirementType == null) || (subValueRequirement.getRequirementType() == requirementType))
            {
                if ((valueIndex == null) || (valueIndex == subValue.getIndex()))
                {
                    if ((requirementConceptID == null) || (requirementConceptID.equals(subValueRequirement.getValue())))
                    {
                        recordValues.add(subValue);
                    }
                }
            }
        }

        return recordValues;
    }

    public ArrayList<Value> removeSubValues(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        ArrayList<Value> valuesToRemove;

        valuesToRemove = getSubValues(requirementType, requirementConceptID, valueIndex);
        for (Value valueToRemove : valuesToRemove)
        {
            removeSubValue(valueToRemove);
        }

        return valuesToRemove;
    }

    public RecordValue getAncestorValue(RequirementType requirementType, String conceptID, Integer index)
    {
        if (parentRecordValue != null)
        {
            if ((requirementType == null) || (parentRecordValue.getValueRequirement().getRequirementType().equals(requirementType)))
            {
                if ((index == null) || (index == parentRecordValue.getIndex()))
                {
                    if ((conceptID == null) || (conceptID.equals(parentRecordValue.getValueRequirement().getValue())))
                    {
                        return parentRecordValue;
                    }
                }
            }

            // The parent is not the required ancestor, so check its ancestors.
            return parentRecordValue.getAncestorValue(requirementType, conceptID, index);
        }
        else return null;
    }

    @Override
    public RecordValue getDescendantValue(RequirementType typeID, String conceptID, Integer index)
    {
        for (RecordValue subValue : getDescendantValues())
        {
            ValueRequirement subValueRequirement;

            subValueRequirement = subValue.getValueRequirement();
            if ((typeID == null) || (subValueRequirement.getRequirementType().equals(typeID)))
            {
                if ((index == null) || (index == subValue.getIndex()))
                {
                    if ((conceptID == null) || (conceptID.equals(subValueRequirement.getValue())))
                    {
                        return subValue;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public ArrayList<Value> getDescendantValues(RequirementType typeID, String conceptID, Integer index)
    {
        ArrayList<Value> recordValues;

        recordValues = new ArrayList<>();
        for (RecordValue subValue : getDescendantValues())
        {
            ValueRequirement subValueRequirement;

            subValueRequirement = subValue.getValueRequirement();
            if ((typeID == null) || (subValueRequirement.getRequirementType().equals(typeID)))
            {
                if ((index == null) || (index == subValue.getIndex()))
                {
                    if ((conceptID == null) || (conceptID.equals(subValueRequirement.getValue())))
                    {
                        recordValues.add(subValue);
                    }
                }
            }
        }

        return recordValues;
    }

    public void clearSubValues()
    {
        if (subValues.size() > 0)
        {
            List<RecordValue> valuesToRemove;

            // Create a list to hold all values that will be removed.
            valuesToRemove = new ArrayList<>(subValues);

            // Remove the sub-values.
            subValues.clear();

            // Fire the change event.
            fireRecordValueStructureChangedEvent(null, valuesToRemove, null);
        }
    }

    public void replaceSubValue(RecordValue newValue)
    {
        if (subValues.size() < 2)
        {
            List<RecordValue> valuesToRemove;
            RecordValue valueToRemove;

            // Create a list to hold all value that will be removed.
            valuesToRemove = new ArrayList<>();
            valueToRemove = getFirstSubValue();
            if (valueToRemove != null)
            {
                valuesToRemove.add(valueToRemove);
                valueToRemove.setParentRecordProperty(null);
                valueToRemove.setParentRecordValue(null);
            }

            // Remove the sub-value.
            subValues.clear();

            // Add the new value.
            subValues.add(newValue);
            newValue.setParentRecordValue(this);
            newValue.setParentRecordProperty(parentRecordProperty);

            // Fire the change event.
            fireRecordValueStructureChangedEvent(ArrayLists.newArrayList(newValue), valuesToRemove, null);
        }
        else throw new IllegalStateException("Method can only be used on values with a single sub-value.");
    }

    /**
     * This is the main method for addition of sub-values.  All other additions
     * direct to this method because it does the proper cleanup and fires the
     * necessary events.  This method will replace existing values that
     * correspond with the value supplied.  The methodology for value matching
     * is as follows:
     *  1.  The ValueRequirement of the new value is compared to that of each
     *      existing sub-value.
     *  2.  If a matching sub-value is found, that value is removed before
     *      addition of the new value.
     *  3.  If no matching sub-value is found, the new value is simply appended
     *      to the sub-value collection.
     *
     * @param newValue The value to set.
     */
    @Override
    public void setSubValue(Value newValue)
    {
        if (newValue instanceof RecordValue)
        {
            RecordValue recordValue;

            // First make sure that the new value is based on the same Data Requirement as this value.
            recordValue = (RecordValue)newValue;
            if (recordValue.getDataRequirement() == this.getDataRequirement())
            {
                RecordValue valueToBeReplaced;
                RequirementType requirementType;

                // Get this value's requirement type.
                requirementType = valueRequirement.getRequirementType();

                // Set the new value at the index.
                recordValue.setParentRecordValue(this);
                recordValue.setParentRecordProperty(parentRecordProperty);

                // Find the value to be replaced.
                valueToBeReplaced = null;
                if (!requirementType.isCollectionValue())
                {
                    for (RecordValue subValue : subValues)
                    {
                        if (subValue.getValueRequirement() == recordValue.getValueRequirement())
                        {
                            valueToBeReplaced = subValue;
                        }
                    }
                }

                // If we found a value to be replaced, replace it.
                if (valueToBeReplaced != null)
                {
                    int valueIndex;

                    // Add the new value.
                    valueIndex = subValues.indexOf(valueToBeReplaced);
                    subValues.remove(valueIndex);
                    subValues.add(valueIndex, recordValue);

                    // Clear references to this value from the value that has been removed.
                    valueToBeReplaced.setParentRecordProperty(null);
                    valueToBeReplaced.setParentRecordValue(null);
                }
                else
                {
                    int lastValueIndex;

                    // Get the index of the last value of this type.
                    lastValueIndex = getLastValueCollectionIndex(recordValue.getRequirementType(), recordValue.getValueRequirement().getValue());
                    if (lastValueIndex == -1)
                    {
                        // Set the new value at the end of the sub values collection.
                        subValues.add(recordValue);
                    }
                    else
                    {
                        // Now add the supplied value at the next index (after the last value of this type).
                        subValues.add(lastValueIndex + 1, recordValue);
                    }
                }

                // Fire the structure change event.
                if (valueToBeReplaced != null)
                {
                    // Fire the change event.
                    fireRecordValueStructureChangedEvent(ArrayLists.newArrayList(newValue), ArrayLists.newArrayList(valueToBeReplaced), null);
                }
                else
                {
                    // Fire the change event.
                    fireRecordValueStructureChangedEvent(ArrayLists.newArrayList(newValue), null, null);
                }
            }
            else throw new IllegalArgumentException("Attempted addition of sub-value based on a different Data Requirement: " + recordValue);
        }
        else throw new IllegalArgumentException("Incompatible value type: " + newValue);
    }

    public void addSubValues(List<RecordValue> values)
    {
        for (RecordValue subValue : values)
        {
            setSubValue(subValue);
        }
    }

    public void removeFromParent()
    {
        if (parentRecordValue != null)
        {
            parentRecordValue.removeSubValue(this);
        }
        else if (parentRecordProperty != null)
        {
            parentRecordProperty.setRecordValue(null);
        }
    }

    @Override
    public boolean removeSubValue(Value subValue)
    {
        if (subValue instanceof RecordValue)
        {
            return removeSubValue((RecordValue)subValue);
        }
        else throw new IllegalArgumentException("Incompatible value type: " + subValue);
    }

    /**
     * This is the main method for removing of sub-values.  All other removals
     * direct to this method because it does the proper cleanup and fires the
     * necessary events.
     *
     * @param valueToRemove The record value to remove.
     * @return A boolean flag indicating whether or not the specified value was
     * found and successfully removed.
     */
    public boolean removeSubValue(RecordValue valueToRemove)
    {
        if (subValues.remove(valueToRemove))
        {
            // Clear references to this document from the removed value.
            valueToRemove.setParentRecordProperty(null);
            valueToRemove.setParentRecordValue(null);

            // Fire the change event.
            fireRecordValueStructureChangedEvent(null, ArrayLists.newArrayList(valueToRemove), null);
            return true;
        }
        else return false;
    }

    public int getSubValueIndex(RecordValue subValue)
    {
        List<Value> setValues;

        // Get all sub-values with the same requirement type and concept ID.
        setValues = getSubValues(subValue.getValueRequirement().getRequirementType(), subValue.getValueRequirement().getValue(), null);
        return setValues.indexOf(subValue);
    }

    protected void fireRecordValueChangedEvent(Object actor)
    {
        DataRecord parentRecord;

        // Only fire the event if this value belongs to a parent data record.
        parentRecord = getParentDataRecord();
        if (parentRecord != null)
        {
            T8RecordValueChangedEvent event;

            // Fire an event signalling a value change on this record value.
            event = new T8RecordValueChangedEvent(this, actor);
            parentRecord.fireDataRecordChangeEventPropagated(event, actor);
        }
    }

    protected void fireRecordValueStructureChangedEvent(List<RecordValue> valuesAdded, List<RecordValue> valuesRemoved, Object actor)
    {
        DataRecord parentRecord;

        // Only fire the event if this value belongs to a parent data record.
        parentRecord = getParentDataRecord();
        if (parentRecord != null)
        {
            T8RecordValueStructureChangedEvent event;

            // Fire an event signalling a value structure change on this record value.
            event = new T8RecordValueStructureChangedEvent(this, valuesAdded, valuesRemoved, actor);
            parentRecord.fireDataRecordChangeEventPropagated(event, actor);
        }
    }

    public RecordValue getEquivalentSubValue(RecordValue recordValueToCheck)
    {
        for (RecordValue subValue : subValues)
        {
            if (subValue.isEquivalentValue(recordValueToCheck)) return subValue;
        }

        return null;
    }

    /**
     * This method returns the Concept ID value of this Record Value object. If
     * this record value is not a type that contains a Concept ID as value, null
     * is returned.
     * @return The Concept ID value of this Record Value or null if the type of
     * this value does not allow for a Concept ID.
     */
    public String getValueConceptID()
    {
        if (RequirementType.ONTOLOGY_CLASS.equals(valueRequirement.getRequirementType()))
        {
            // Controlled values have not value in this section, but the concept ID may be found in the required.
            return valueRequirement.getValue();
        }
        else if (valueRequirement.getRequirementType().isConceptRecordValue())
        {
            return value;
        }
        else return null;
    }

    /**
     * Returns true if this is the first-level value linked to its parent
     * property.
     * @return True if this is the first-level value linked to its parent
     * property.
     */
    public boolean isPropertyValue()
    {
        return parentRecordProperty != null && parentRecordValue == null;
    }

    /**
     * Returns true if this value is the one and only direct dependant of a
     * field (i.e. a RecordValue with a FIELD_TYPE requirement).
     * @return True if this value is the one and only direct dependant of a
     * field.
     */
    public boolean isFieldValue()
    {
        if (parentRecordValue != null)
        {
            return parentRecordValue.isField();
        }
        else return false;
    }

    /**
     * Returns true if this record value is a field (i.e. has a FIELD_TYPE
     * requirement).
     * @return
     */
    public boolean isField()
    {
        return getValueRequirement().getRequirementType() == RequirementType.FIELD_TYPE;
    }

    @Override
    public boolean hasContent(boolean includeFFT)
    {
        if (!Strings.isNullOrEmpty(value))
        {
            return true;
        }
        else
        {
            // Check each sub-value for content.
            for (RecordValue subValue : subValues)
            {
                if (subValue.hasContent(includeFFT)) return true;
            }

            // No content found.
            return false;
        }
    }

    @Override
    public void normalize()
    {
        if (RequirementType.BOOLEAN_TYPE.equals(valueRequirement.getRequirementType()))
        {
            // Boolean types are tri-state: true/false/null.
            if (value != null)
            {
                Boolean booleanValue;

                // Make sure that boolean values are always stored in the save format (the case is important for value String matching).
                booleanValue = Boolean.parseBoolean(value);
                value = booleanValue.toString();
            }
        }
        else if (RequirementType.COMPOSITE_TYPE.equals(valueRequirement.getRequirementType()))
        {
            Composite compositeValue;

            // Check each of the fields of the composite value for content.
            compositeValue = (Composite)this;
            for (Field field : compositeValue.getFields())
            {
                // Normalize the field.
                field.normalize();

                // If the field has not content, remove it.
                if (!field.hasContent(true))
                {
                    compositeValue.removeSubValue(field);
                }
            }
        }
        else
        {
            // Normalize sub-values.
            for (RecordValue subValue : subValues)
            {
                subValue.normalize();
            }
        }
    }

    @Override
    public boolean isEquivalentValue(Value comparedValue)
    {
        if (!(comparedValue instanceof RecordValue))
        {
            return false;
        }
        else
        {
            RecordValue recordValue;

            recordValue = (RecordValue)comparedValue;
            if ((Objects.equals(value, recordValue.getValue())) && (valueRequirement.isEquivalentRequirement(recordValue.getValueRequirement())))
            {
                List<RecordValue> subValueList;

                subValueList = recordValue.getSubValues();
                if (subValues.size() == subValueList.size())
                {
                    // Check each of the sub-values in this value against those in the supplied value.
                    for (int subValueIndex = 0; subValueIndex < subValues.size(); subValueIndex++)
                    {
                        if (!subValues.get(subValueIndex).isEquivalentValue(subValueList.get(subValueIndex)))
                        {
                            return false;
                        }
                    }

                    // No problems found, so return true;
                    return true;
                }
                else return false;
            }
            else return false;
        }
    }

    public RecordValue copy()
    {
        RecordValue copy;

        copy = RecordValue.createValue(valueID, valueRequirement, value);
        copy.setFFT(fft);
        for (RecordValue subValue : subValues)
        {
            copy.setSubValue(subValue.copy());
        }

        return copy;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("RecordValue: [");
        builder.append("Value: ");
        builder.append(getValue());
        builder.append(",Fft: ");
        builder.append(getFFT());
        builder.append(",ValueRequirement: ");
        builder.append(getValueRequirement());
        builder.append("]");
        return builder.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getContentHashCode(boolean includeFFT)
    {
        int hash;

        hash = 5;
        hash = 53 * hash + java.util.Objects.hashCode(this.value);
        hash = 53 * hash + java.util.Objects.hashCode(this.fft);
        for (RecordValue subValue : subValues)
        {
            hash = 53 * hash + subValue.getContentHashCode(includeFFT);
        }

        return hash;
    }

    public static RecordValue createValue(ValueRequirement valueRequirement)
    {
        RequirementType requirementType;

        requirementType = valueRequirement.getRequirementType();
        switch (requirementType)
        {
            case ATTACHMENT:
            {
                return new AttachmentList(valueRequirement);
            }
            case BOOLEAN_TYPE:
            {
                return new BooleanValue(valueRequirement);
            }
            case COMPOSITE_TYPE:
            {
                return new Composite(valueRequirement);
            }
            case CONTROLLED_CONCEPT:
            {
                return new ControlledConcept(valueRequirement);
            }
            case DATE_TIME_TYPE:
            {
                return new DateTimeValue(valueRequirement);
            }
            case DATE_TYPE:
            {
                return new DateValue(valueRequirement);
            }
            case DOCUMENT_REFERENCE:
            {
                return new DocumentReferenceList(valueRequirement);
            }
            case FIELD_TYPE:
            {
                return new Field(valueRequirement);
            }
            case INTEGER_TYPE:
            {
                return new IntegerValue(valueRequirement);
            }
            case LOCALIZED_TEXT_TYPE:
            {
                return new LocalizedStringList(valueRequirement);
            }
            case LOCAL_STRING_TYPE:
            {
                return new LocalizedStringValue(valueRequirement);
            }
            case LOWER_BOUND_NUMBER:
            {
                return new LowerBoundNumber(valueRequirement);
            }
            case MEASURED_NUMBER:
            {
                return new MeasuredNumber(valueRequirement);
            }
            case MEASURED_RANGE:
            {
                return new MeasuredRange(valueRequirement);
            }
            case NUMBER:
            {
                return new Number(valueRequirement);
            }
            case QUALIFIER_OF_MEASURE:
            {
                return new QualifierOfMeasure(valueRequirement);
            }
            case REAL_TYPE:
            {
                return new RealValue(valueRequirement);
            }
            case SET_TYPE:
            {
                return new ValueSet(valueRequirement);
            }
            case STRING_TYPE:
            {
                return new StringValue(valueRequirement);
            }
            case TEXT_TYPE:
            {
                return new TextValue(valueRequirement);
            }
            case TIME_TYPE:
            {
                return new TimeValue(valueRequirement);
            }
            case UNIT_OF_MEASURE:
            {
                return new UnitOfMeasure(valueRequirement);
            }
            case UPPER_BOUND_NUMBER:
            {
                return new UpperBoundNumber(valueRequirement);
            }
            default:
            {
                throw new RuntimeException("Invalid record value type: " + requirementType);
            }
        }
    }

    public List<HashMap<String, Object>> getDataRows()
    {
        ArrayList<HashMap<String, Object>> dataRows;
        HashMap<String, Object> dataRow;
        RecordSection recordSection;
        RecordProperty recordProperty;
        DataRecord record;

        dataRows = new ArrayList<>();

        // Get the values from which data will be extracted.
        recordProperty = getParentRecordProperty();
        recordSection = recordProperty.getParentRecordSection();
        record = recordSection.getParentDataRecord();

        // Extract the data row.
        dataRow = new HashMap<>();
        dataRow.put("RECORD_ID", record.getID());
        dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
        dataRow.put("DR_ID", record.getDataRequirement().getConceptID());
        dataRow.put("SECTION_ID", recordSection.getSectionRequirement().getConceptID());
        dataRow.put("PROPERTY_ID", recordProperty.getPropertyRequirement().getConceptID());
        dataRow.put("FIELD_ID", getFieldID());
        dataRow.put("DATA_TYPE_ID", getValueRequirement().getRequirementType().getID());
        dataRow.put("DATA_TYPE_SEQUENCE", getValueRequirement().getSequence());
        dataRow.put("VALUE_SEQUENCE", getSequence());
        dataRow.put("PARENT_VALUE_SEQUENCE", parentRecordValue != null ? parentRecordValue.getSequence() : null);
        dataRow.put("VALUE", getValue());
        dataRow.put("VALUE_CONCEPT_ID", getValueConceptID());
        dataRow.put("FFT", getFFT());
        dataRow.put("VALUE_ID", getPrescribedValueID());

        // Add the extracted data row to the list.
        dataRows.add(dataRow);

        return dataRows;
    }

    public static RecordValue createValue(String valueID, ValueRequirement valueRequirement, String value)
    {
        RecordValue newValue;

        newValue = RecordValue.createValue(valueRequirement);
        newValue.valueID = valueID;
        newValue.setValue(value);
        return newValue;
    }

    /**
     * Adds the terminology of this value Requirement and of all its
     * descendants to the supplied list.
     * @param terminologyCollector The list to which terminology will be added.
     */
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        // Add the descendants' terminology to the list.
        for (RecordValue subValue : subValues)
        {
            subValue.addContentTerminology(terminologyCollector);
        }
    }

    /**
     * Sets the terminology of this value Requirement and all of its descendants
     * by retrieval from the supplied terminology provider.
     * @param terminologyProvider The terminology provider form which to fetch terminology.
     */
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        // Set the terminology of the sub-values.
        for (RecordValue subValue : subValues)
        {
            subValue.setContentTerminology(terminologyProvider);
        }
    }

    /**
     * Returns the content of the RecordValue as it currently exists in the object model.
     * Content includes all sub-values.
     * Content excludes requirement information.
     * All content values are strings.
     * @return The content of the record value.
     */
    public abstract ValueContent getContent();

    /**
     * Sets the new content of the record value.  All input content values are strings to
     * accommodate dirty and incorrectly formatted values.  When this method is invoked,
     * all input content values will be converted to the native format as used by the corresponding
     * RecordValue type.  All values that cannot be parsed or converted successfully, will simply be set
     * on the RecordValue in order to be available for subsequent conformance validation and possible shift to
     * FFT.
     * @param content The new content to set on the RecordValue and its sub-values.
     */
    public abstract void setContent(ValueContent content);
}
