package com.pilog.t8.data.document.integration;

import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Hennie Brink
 */
public interface DataRecordIntegrationService<T>
{
    public void initialize(T8DataTransaction tx, String operationIdentifier);

    public String executeOperation(Map<String, Object> parameters) throws Exception;
}
