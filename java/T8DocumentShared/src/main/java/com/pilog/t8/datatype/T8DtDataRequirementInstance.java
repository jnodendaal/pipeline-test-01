package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataRequirementInstance extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_REQUIREMENT_INSTANCE";

    public T8DtDataRequirementInstance()
    {
    }

    public T8DtDataRequirementInstance(T8DefinitionManager context)
    {
    }

    public T8DtDataRequirementInstance(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return DataRequirementInstance.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            DataRequirementInstance drInstance;

            drInstance = (DataRequirementInstance)object;
            return T8DocumentSerializer.serializeDrInstance(drInstance);
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                return T8DocumentSerializer.deserializeDrInstance((JsonObject)jsonValue);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deserializing Data Requirement Instance.", e);
            }
        }
        else return null;
    }
}
