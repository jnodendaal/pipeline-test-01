package com.pilog.t8.definition.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion.CodeCriterionTarget;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CodeFilterCriterionDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RECORD_FILTER_CODE_CRITERION";
    public static final String GROUP_IDENTIFIER = "@DT_RECORD_FILTER_CODE_CRITERION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Code Filter Criterion";
    public static final String DESCRIPTION = "A filter criterion applicable to a specific Data Record concept code.";
    private enum Datum {CODE_TYPE_ID,
                        TARGET,
                        OPERATOR,
                        FILTER_VALUE_EXPRESSION,
                        ALLOW_REMOVAL};
    // -------- Definition Meta-Data -------- //

    public T8CodeFilterCriterionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.CODE_TYPE_ID.toString(), "Code Type ID", "The Code Type ID of the concept code to which this criterion is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TARGET.toString(), "Target", "Specifies the type of document on which the code criterion will be applied.", CodeCriterionTarget.RECORD.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATOR.toString(), "Operator", "The operator used by this criterion.", DataFilterOperator.EQUAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.FILTER_VALUE_EXPRESSION.toString(), "Filter Value Expresssion", "The expression that will be evaluated to determine the filter value of this criterion."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_REMOVAL.toString(), "Allow Removal", "If enabled, allows the removal of this criterion.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(DataFilterOperator.values());
        else if (Datum.TARGET.toString().equals(datumIdentifier)) return createStringOptions(CodeCriterionTarget.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8CodeFilterCriterion getCodeFilterCriterionInstance(T8Context context)
    {
        T8CodeFilterCriterion criteria;

        criteria = new T8CodeFilterCriterion(getCodeTypeId());
        criteria.setOperator(getOperator());
        criteria.setAllowRemoval(isAllowRemoval());
        criteria.setTarget(getTarget());
        return criteria;
    }

    public String getCodeTypeId()
    {
        return (String)getDefinitionDatum(Datum.CODE_TYPE_ID.toString());
    }

    public void setCodeTypeId(String codeTypeId)
    {
        setDefinitionDatum(Datum.CODE_TYPE_ID.toString(), codeTypeId);
    }

    public CodeCriterionTarget getTarget()
    {
        String stringValue;

        stringValue = (String)getDefinitionDatum(Datum.TARGET.toString());
        if (Strings.isNullOrEmpty(stringValue)) return CodeCriterionTarget.RECORD;
        else return CodeCriterionTarget.valueOf(stringValue);
    }

    public void setTarget(CodeCriterionTarget target)
    {
        setDefinitionDatum(Datum.TARGET.toString(), target.toString());
    }

    public DataFilterOperator getOperator()
    {
        String stringValue;

        stringValue = (String)getDefinitionDatum(Datum.OPERATOR.toString());
        if (Strings.isNullOrEmpty(stringValue)) return DataFilterOperator.EQUAL;
        else return DataFilterOperator.valueOf(stringValue);
    }

    public void setOperator(DataFilterOperator operator)
    {
        setDefinitionDatum(Datum.OPERATOR.toString(), operator.toString());
    }

    public String getFilterValueExpression()
    {
        return (String)getDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString());
    }

    public void setFilterValueExpression(String expression)
    {
        setDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString(), expression);
    }

    public boolean isAllowRemoval()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_REMOVAL.toString());
        return value != null && value;
    }

    public void setAllowRemoval(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_REMOVAL.toString(), false);
    }
}
