package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.strings.Strings;
import java.io.PrintStream;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordPrinter
{
    private final TerminologyProvider terminologyProvider;
    private final PrintStream printStream;

    public T8DataRecordPrinter(PrintStream printStream, TerminologyProvider terminologyProvider)
    {
        this.printStream = printStream;
        this.terminologyProvider = terminologyProvider;
    }

    public void printDataRecord(DataRecord dataRecord)
    {
        printDataRecord(dataRecord, 0);
    }

    public void printDataRecord(DataRecord dataRecord, int indent)
    {
        printStream.println(createString("RECORD:" + dataRecord.getID(), indent));
        for (RecordSection section : dataRecord.getRecordSections())
        {
            printRecordSection(section, indent+1);
        }
    }

    public void printRecordSection(RecordSection section, int indent)
    {
        String term;
        String conceptID;
        String label;

        conceptID = section.getSectionRequirement().getConceptID();
        term = terminologyProvider != null ? terminologyProvider.getTerm(null, conceptID) : null;
        label = term != null ? term + "(" + conceptID + ")" : conceptID;
        printStream.println(createString("SECTION:" + label, indent));
        for (RecordProperty property : section.getRecordProperties())
        {
            printRecordProperty(property, indent+1);
        }
    }

    public void printRecordProperty(RecordProperty property, int indent)
    {
        String term;
        String conceptID;
        String label;

        conceptID = property.getPropertyRequirement().getConceptID();
        term = terminologyProvider != null ? terminologyProvider.getTerm(null, conceptID) : null;
        label = term != null ? term + "(" + conceptID + ")" : conceptID;
        printStream.println(createString("PROPERTY:" + label, indent));
        if (property.getRecordValue() != null)
        {
            printRecordValue(property.getRecordValue(), indent+1);
        }
    }

    public void printRecordValue(RecordValue value, int indent)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;
        String term;
        String conceptID;
        String label;

        valueRequirement = value.getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        label = requirementType.toString() + ":";

        if ((requirementType.isConceptRequirementValue()) && (terminologyProvider != null))
        {
            conceptID = valueRequirement.getValue();
            term = conceptID != null ? terminologyProvider.getTerm(null, conceptID) : null;
            label += (term != null) ? term + "(" + conceptID + ")" : conceptID;
            label += ":";
        }
        else
        {
            label += valueRequirement.getValue() + ":";
        }

        if ((requirementType.isConceptRecordValue()) && (terminologyProvider != null))
        {
            conceptID = value.getValue();
            term = conceptID != null ? terminologyProvider.getTerm(null, conceptID) : null;
            label += (term != null) ? term + "(" + conceptID + ")" : conceptID;
        }
        else
        {
            label += value.getValue();
        }

        printStream.println(createString("VALUE:" + label, indent));
        for (RecordValue subValue : value.getSubValues())
        {
            printRecordValue(subValue, indent+1);
        }

        // Print the sub-record referenced from this value (if any).
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            String subRecordID;

            subRecordID = value.getValue();
            if (!Strings.isNullOrEmpty(subRecordID))
            {
                DataRecord subRecord;

                subRecord = value.getParentDataRecord().getSubRecord(subRecordID);
                if (subRecord != null)
                {
                    printDataRecord(subRecord, indent+1);
                }
            }
        }
    }

    private StringBuffer createString(String inputString, int indent)
    {
        StringBuffer string;

        string = new StringBuffer();
        for (int i = 0; i < indent; i++)
        {
            string.append(" ");
        }

        string.append(inputString);
        return string;
    }
}
