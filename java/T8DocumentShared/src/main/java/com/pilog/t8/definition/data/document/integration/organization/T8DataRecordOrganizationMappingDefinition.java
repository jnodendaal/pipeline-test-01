package com.pilog.t8.definition.data.document.integration.organization;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public abstract class T8DataRecordOrganizationMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_DATA_RECORD_ORG";
    public static final String IDENTIFIER_PREFIX = "DROM_";

    public enum Datum
    {
        ORG_ID,
        ORG_TERM,
        ORG_VALUE
    };
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public T8DataRecordOrganizationMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ORG_ID.toString(), "Organization ID", "The interfacing Organization ID."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ORG_TERM.toString(), "Organization Term", "The interfacing Organization Term."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ORG_VALUE.toString(), "Organization Value", "The interfacing Organization Value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return null;
    }

    public String getOrganizationID()
    {
        return (String)getDefinitionDatum(Datum.ORG_ID.toString());
    }

    public void setOrganizationID(String organizationID)
    {
        setDefinitionDatum(Datum.ORG_ID.toString(), organizationID);
    }

    public String getOrganizationTerm()
    {
        return (String)getDefinitionDatum(Datum.ORG_TERM.toString());
    }

    public void setOrganizationTerm(String organizationTerm)
    {
        setDefinitionDatum(Datum.ORG_TERM.toString(), organizationTerm);
    }

    public String getOrganizationValue()
    {
        return (String)getDefinitionDatum(Datum.ORG_VALUE.toString());
    }

    public void setOrganizationValue(String organizationValue)
    {
        setDefinitionDatum(Datum.ORG_VALUE.toString(), organizationValue);
    }
}
