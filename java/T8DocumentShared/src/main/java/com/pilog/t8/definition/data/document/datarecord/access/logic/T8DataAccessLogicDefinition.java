package com.pilog.t8.definition.data.document.datarecord.access.logic;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.access.logic.T8DataAccessLogic;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DataAccessLogicDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_DATA_RECORD_ACCESS_LOGIC";
    public static final String GROUP_NAME = "Data Record Access Logic";
    public static final String GROUP_DESCRIPTION = "Scripted rules that apply specific business logic access rules to data records they are applied to.";
    public static final String DISPLAY_NAME = "Data Record Access Logic";
    public static final String STORAGE_PATH = "/data_record_access_logic";
    public static final String DESCRIPTION = "A definition that specifies a data access rule.";
    public static final String IDENTIFIER_PREFIX = "ACCESS_LOGIC_";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8DataAccessLogicDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    public abstract T8DataAccessLogic getNewDataAccessLogicInstance(T8Context context);
}
