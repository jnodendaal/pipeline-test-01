package com.pilog.t8.data.document.datarecord.structure;

import java.util.Iterator;

/**
 * This is a standard depth-first iterator that can be used to traverse a
 * Data Record Structure.
 * @author Bouwer du Preez
 */
public class DataRecordStructureDFIterator implements Iterator<DataRecordStructureNode>
{
    private DataRecordStructure structure;
    private DataRecordStructureNode currentNode;
    private DataRecordStructureNode nextNode;
    
    public DataRecordStructureDFIterator(DataRecordStructure structure)
    {
        this.structure = structure;
        this.currentNode = null;
        this.nextNode = structure.getRootNode();
    }
    
    @Override
    public boolean hasNext()
    {
        return nextNode != null;
    }

    @Override
    public DataRecordStructureNode next()
    {
        if (nextNode != null)
        {
            currentNode = nextNode;
            nextNode = findNextNode(currentNode);
            return currentNode;
        }
        else return null;
    }

    @Override
    public void remove()
    {
        structure.removeNode(currentNode);
    }
    
    private DataRecordStructureNode findNextNode(DataRecordStructureNode startNode)
    {
        DataRecordStructureNode node;
        
        node = startNode;
        if (startNode.getChildNodeCount() > 0)
        {
            return node.getChildNodeAt(0);
        }
        else
        {
            DataRecordStructureNode parentNode;

            // Continue up the tree if needed.
            while ((parentNode = node.getParentNode()) != null)
            {
                int currentNodeIndex;

                currentNodeIndex = parentNode.getChildIndex(node);
                if (parentNode.getChildNodeCount() > currentNodeIndex + 1)
                {
                    return parentNode.getChildNodeAt(currentNodeIndex + 1);
                }
                else node = parentNode; // Go one level higher.
            }
            
            // No next node available.
            return null;
        }
    }
}
