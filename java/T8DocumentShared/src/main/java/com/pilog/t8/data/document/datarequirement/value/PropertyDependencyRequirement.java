package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class PropertyDependencyRequirement extends ValueRequirement
{
    public PropertyDependencyRequirement()
    {
        super(RequirementType.PROPERTY_DEPENDENCY_TYPE);
    }
}
