package com.pilog.t8.data.alteration;

import com.pilog.t8.definition.T8IdentifierUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements an index for a specific data alteration instance.
 * - An index consists of a number of {@code T8DataAlterationPackageIndexEntry} objects called entries.
 * - Each index entry specifies a type of alteration to be performed in sequence for a specified range of steps.
 * - The sequence of entries in an alteration is critical to ensure that related data is changed in the correct order when an alteration is applied.
 * - When a specific step in an alteration is required, the index is queried to determine the type of alteration and the correct handler with which to manage it.
 *
 * @author Bouwer du Preez
 */
public class T8DataAlterationPackageIndex implements Serializable
{
    private final String packageIid;
    private final List<T8DataAlterationPackageIndexEntry> entries;

    public T8DataAlterationPackageIndex(String packageIid)
    {
        this.packageIid = packageIid;
        this.entries = new ArrayList<T8DataAlterationPackageIndexEntry>();
    }

    /**
     * Returns the id of the alteration instance to which this index belongs.
     * @return The id of the alteration instance to which this index belongs.
     */
    public String getPackageIid()
    {
        return packageIid;
    }

    /**
     * Returns an exact copy of this index.
     * @return An exact copy of this index.
     */
    public T8DataAlterationPackageIndex copy()
    {
        T8DataAlterationPackageIndex copy;

        copy = new T8DataAlterationPackageIndex(packageIid);
        for (T8DataAlterationPackageIndexEntry entry : entries)
        {
            copy.addEntry(entry.copy());
        }

        return copy;
    }

    /**
     * Returns the size of all entries in the index combined
     * @return
     */
    public int getSize()
    {
        int size;

        size = 0;
        for (T8DataAlterationPackageIndexEntry entry : entries)
        {
            size += entry.getSize();
        }

        return size;
    }

    /**
     * Adds the supplied entry to this index.
     * @param entry
     */
    public void addEntry(T8DataAlterationPackageIndexEntry entry)
    {
        this.entries.add(entry);
    }

    /**
     * Returns the index entry for the specified step.
     * @param step The step required.
     * @return The index entry for the specified step or null if the step is out of the bounds of the index.
     */
    public T8DataAlterationPackageIndexEntry getEntry(int step)
    {
        for (T8DataAlterationPackageIndexEntry entry : entries)
        {
            if (entry.containsStep(step))
            {
                return entry;
            }
        }

        return null;
    }

    /**
     * Returns the entries for the specific index.
     * @return
     */
    public List<T8DataAlterationPackageIndexEntry> getEntries()
    {
        return new ArrayList<>(entries);
    }

    /**
     * Returns the list entry in the index or null if the index is empty.
     * @return The list entry in the index or null if the index is empty.
     */
    public T8DataAlterationPackageIndexEntry getLastEntry()
    {
        return entries.size() > 0 ? entries.get(entries.size()-1) : null;
    }

    /**
     * Returns the last step in the last entry of this index.
     * @return The last step in the last entry of this index or -1 if the index is empty.
     */
    public int getLastStep()
    {
        T8DataAlterationPackageIndexEntry lastEntry;

        lastEntry = getLastEntry();
        return lastEntry != null ? (lastEntry.getStep() + lastEntry.getSize() - 1) : -1;
    }

    /**
     * Clears all entries from this index.
     */
    public void clearEntries()
    {
        this.entries.clear();
    }

    /**
     * Deletes the specified step from the index, decrementing the step of each
     * entry in the index.  This method returns a list of all entries affected
     * by the step deletion.  If, after the step deletion, an entry has no
     * remaining step (a size of 0), the entry will be at the first index of the
     * returned list and will be removed from the index after completion of this
     * method.
     * @param step The step to delete.
     * @return The list of entries in the index, that were affected by the
     * deletion of the specified step.
     */
    public T8DataAlterationPackageIndexChanges deleteStep(int step)
    {
        T8DataAlterationPackageIndexChanges changes;

        // Create an object to hold the changes we make to index entries.
        changes = new T8DataAlterationPackageIndexChanges();
        for (int entryIndex = 0; entryIndex < entries.size(); entryIndex++)
        {
            T8DataAlterationPackageIndexEntry entry;

            // Find the entry containing the step to delete.
            entry = entries.get(entryIndex);
            if (entry.containsStep(step))
            {
                // Delete the step from the entry.
                entry.deleteStep();

                // If the entry is now empty after the deletion, remove the entry.
                if (entry.isEmpty())
                {
                    entries.remove(entry);
                    changes.addEntryDeletion(entry);
                }
                else
                {
                    changes.addEntryUpdate(entry, step, -1);
                }

                // Decrement the steps in all subsequent entries.
                for (int subsequentEntryIndex = entryIndex; subsequentEntryIndex < entries.size(); subsequentEntryIndex++)
                {
                    T8DataAlterationPackageIndexEntry subsequentEntry;

                    subsequentEntry = entries.get(subsequentEntryIndex);
                    subsequentEntry.decrementStep();
                    changes.addEntryUpdate(subsequentEntry, 0, -1);
                }

                // Return the list of entries affected by the step deletion.
                return changes;
            }
        }

        // If this point is reached, no changes were made so return the empty result.
        return changes;
    }

    /**
     * Inserts a new step in the alteration index at the specified index.
     * @param alterationId The alteration type of the step to insert.
     * @param step The index at which to insert the step.
     * @return The changes made to the index.
     */
    public T8DataAlterationPackageIndexChanges insertStep(String alterationId, int step)
    {
        T8DataAlterationPackageIndexChanges changes;
        T8DataAlterationPackageIndexEntry lastEntry;

        // Create an object to hold the changes we make to index entries.
        changes = new T8DataAlterationPackageIndexChanges();

        // Get the last entry in the index.
        lastEntry = getLastEntry();
        if (lastEntry == null) // If the index is empty, add the step as a new entry.
        {
            // Make sure we are adding the new step at the start of the index.
            if (step == 0)
            {
                T8DataAlterationPackageIndexEntry newEntry;

                // Create the new entry and add it at the start of the index.
                newEntry = new T8DataAlterationPackageIndexEntry(T8IdentifierUtilities.createNewGUID(), packageIid, alterationId, step, 1);
                entries.add(newEntry);

                // Add the change to the change list.
                changes.addEntryInsert(newEntry);
            }
            else throw new IllegalArgumentException("Index for alteration '" + this + "' is empty, so new step cannot be inserted at index: " + step);
        }
        else if (step == (lastEntry.getStep() + lastEntry.getSize())) // If the step to insert is at the end of the index, just add it to the end after the last entry.
        {
            if (lastEntry.getAlterationId().equals(alterationId))
            {
                // The last entry in the index is the same type as the one to be added, so just add an additional step to the last entry.
                lastEntry.addStep();
                changes.addEntryUpdate(lastEntry, lastEntry.getStep(), 0);
            }
            else
            {
                T8DataAlterationPackageIndexEntry newEntry;

                // Create the new entry and add it at the end of the index.
                newEntry = new T8DataAlterationPackageIndexEntry(T8IdentifierUtilities.createNewGUID(), packageIid, alterationId, step, 1);
                entries.add(newEntry);

                // Add the change to the change list.
                changes.addEntryInsert(newEntry);
            }
        }
        else
        {
            // Ok, so the new step needs to be inserted somewhere in the middle of the existing entries.
            for (int entryIndex = 0; entryIndex < entries.size(); entryIndex++)
            {
                T8DataAlterationPackageIndexEntry entry;

                // Find the entry containing the range to which the step must be added.
                entry = entries.get(entryIndex);
                if (entry.containsStep(step))
                {
                    // If the entry is of the same type as the step we need to add, then just add the step to the existing entry.
                    if (entry.getAlterationId().equals(alterationId))
                    {
                        // Add the step to the entry.
                        entry.addStep();

                        // Add the change to the change list.
                        changes.addEntryUpdate(entry, step, +1);

                        // Increment the steps in all subsequent entries.
                        for (int subsequentEntryIndex = entryIndex; subsequentEntryIndex < entries.size(); subsequentEntryIndex++)
                        {
                            T8DataAlterationPackageIndexEntry subsequentEntry;

                            subsequentEntry = entries.get(subsequentEntryIndex);
                            subsequentEntry.incrementStep();
                            changes.addEntryUpdate(subsequentEntry, 0, +1);
                        }

                        // Return the list of entries affected by the step insertion.
                        return changes;
                    }
                    else if (step == entry.getStep()) // If the index of the step to insert is at the start of this entry, we can shift the entire entry by one step without a split.
                    {
                        T8DataAlterationPackageIndexEntry newEntry;

                        // Create a new entry at the specified step index, of size 1.
                        newEntry = new T8DataAlterationPackageIndexEntry(T8IdentifierUtilities.createNewGUID(), packageIid, alterationId, step, 1);
                        entries.add(entryIndex, newEntry);

                        // Add the change to the change list.
                        changes.addEntryInsert(newEntry);

                        // Increment the steps in all subsequent entries.
                        for (int subsequentEntryIndex = entryIndex; subsequentEntryIndex < entries.size(); subsequentEntryIndex++)
                        {
                            T8DataAlterationPackageIndexEntry subsequentEntry;

                            subsequentEntry = entries.get(subsequentEntryIndex);
                            subsequentEntry.incrementStep();
                            changes.addEntryUpdate(subsequentEntry, 0, +1);
                        }

                        // Return the list of entries affected by the step insertion.
                        return changes;
                    }
                    else // Curses, the step is somewhere in the middle of this entry so now we abslutely have to split the entry.
                    {
                        throw new UnsupportedOperationException("Insertion of a new alteration, splitting an existing range, is not yet supported.");
                    }
                }
            }
        }

        // If this point is reached, no changes were made so return the empty result.
        return changes;
    }
}
