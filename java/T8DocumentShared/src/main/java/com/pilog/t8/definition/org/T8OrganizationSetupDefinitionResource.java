package com.pilog.t8.definition.org;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.resolver.T8JavaDefinitionResolverDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationSetupDefinitionResource implements T8DefinitionResource
{
    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaDefinitionResolverDefinition.TYPE_IDENTIFIER))
        {
            T8JavaDefinitionResolverDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaDefinitionResolverDefinition("@DRESOLVE_ORGANIZATION_SETUP");
            definition.setMetaDisplayName("Organization Setup Definition Resolver");
            definition.setMetaDescription("Resolves the most applicable organization setup definition within a specific session context.");
            definition.setClassName("com.pilog.t8.org.T8OrganizationSetupDefinitionResolver");
            definition.setDefinitionTypeIdentifiers(ArrayLists.newArrayList(T8OrganizationSetupDefinition.TYPE_IDENTIFIER));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
