package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class FftStep extends DefaultPathStep implements PathStep
{
    public FftStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getFFT());
        }
        else if (input instanceof RecordProperty)
        {
            return evaluateStep(((RecordProperty)input).getFFT());
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                if (inputObject != null)
                {
                    outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
                }
                else if (modifiers.isIncludeNullValues())
                {
                    outputList.add(null);
                }
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(String fft)
    {
        List<String> fftList;

        // Create a list to hold the FFT values.
        fftList = new ArrayList<String>();
        if (Strings.isNullOrEmpty(fft))
        {
            // Only include the null values if the applicable modifier is set.
            if (modifiers.isIncludeNullValues())
            {
                fftList.add(null);
            }
        }
        else
        {
            fftList.add(fft);
        }

        return fftList;
    }
}
