package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DataDependency implements Serializable
{
    private final String drInstanceId;
    private final String drId;
    private final String propertyId;
    private final String fieldId;
    private final String relationId;
    private final T8OntologyConceptType conceptType;
    private final List<DataDependency> parentDependencies;
    private final DataDependencyType type;

    public enum DataDependencyType
    {
        DR_INSTANCE, // A dependency on a DR Instance ID.
        PROPERTY, // A dependency on a specific property value.
        FIELD // A dependency on a specific field value.
    };

    public DataDependency(DataDependencyType type, String drInstanceId, String drId, String propertyId, String fieldId, String relationId, T8OntologyConceptType conceptType)
    {
        this.type = type;
        this.drInstanceId = drInstanceId;
        this.drId = drId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
        this.relationId = relationId;
        this.conceptType = conceptType;
        this.parentDependencies = new ArrayList<>();
    }

    public void addParentDependency(DataDependency dependency)
    {
        parentDependencies.add(dependency);
    }

    public void addParentDependencies(Collection<DataDependency> dependencies)
    {
        parentDependencies.addAll(dependencies);
    }

    public boolean removeParentDependency(DataDependency dependency)
    {
        return parentDependencies.remove(dependency);
    }

    public DataDependencyType getType()
    {
        return type;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public boolean isDrInstanceSpecified()
    {
        return drInstanceId != null;
    }

    public boolean isDrInstanceDependency()
    {
        return type == DataDependencyType.DR_INSTANCE;
    }

    public String getDrId()
    {
        return drId;
    }

    public boolean isDrSpecified()
    {
        return drId != null;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public boolean isPropertySpecified()
    {
        return propertyId != null;
    }

    public boolean isPropertyDependency()
    {
        return type == DataDependencyType.PROPERTY;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public boolean isFieldSpecified()
    {
        return fieldId != null;
    }

    public boolean isFieldDependency()
    {
        return type == DataDependencyType.FIELD;
    }

    public String getRelationId()
    {
        return relationId;
    }

    public List<String> getRelationIdPath()
    {
        List<String> idPath;

        if (parentDependencies.isEmpty())
        {
            idPath = new ArrayList<String>();
            idPath.add(relationId);
            return idPath;
        }
        else if (parentDependencies.size() == 1)
        {
            idPath = parentDependencies.get(0).getRelationIdPath();
            idPath.add(relationId);
            return idPath;
        }
        else throw new IllegalStateException("Cannot get Concept Graph ID path from a dependency with multiple parents.");
    }

    public T8OntologyConceptType getConceptType()
    {
        return conceptType;
    }

    /**
     * Determines whether the specified value is dependent on the supplied
     * parent value.
     * @param parentValue The parent value to test for dependency.
     * @param childValue The value to test for dependency on the parent.
     * @return Boolean true if the value is dependent on the supplied parent.
     */
    public boolean isDependencyParentValue(RecordValue parentValue, RecordValue childValue)
    {
        if (type == DataDependencyType.DR_INSTANCE)
        {
            // If the dependency is only based on a DR Instance then no parent value is involved and this method will always return false.
            return false;
        }
        else
        {
            RequirementType requirementType;

            if (isFieldSpecified())
            {
                    // If a field is specified and parent value's field ID does not match the specified field then the child is not dependent on the parent.
                if (!fieldId.equals(parentValue.getFieldID())) return false;
            }

            if (isPropertySpecified())
            {
                    // If a property is specified and parent value's property ID does not match the specified property then the child is not dependent on the parent.
                if (!propertyId.equals(parentValue.getPropertyID())) return false;
            }

            if (isDrInstanceSpecified())
            {
                    // If a DR Instance is specified and parent value's DR Instance ID does not match the specified DR Instance then the child is not dependent on the parent.
                if (!drInstanceId.equals(parentValue.getDataRequirementInstanceID())) return false;
            }

            if (isDrSpecified())
            {
                    // If a DR is specified and parent value's DR ID does not match the specified DR then the child is not dependent on the parent.
                if (!drId.equals(parentValue.getDataRequirementID())) return false;
            }

            // Get the requirement type of the value.
            requirementType = parentValue.getRequirementType();
            if (requirementType == RequirementType.DOCUMENT_REFERENCE)
            {
                List<String> pathRecordIdList;
                String parentValueRecordId;
                String parentValueReferenceID;

                // Get the record ID for which we need to find a DR Instance ID.
                pathRecordIdList = childValue.getParentDataRecord().getPathRecordIDList();
                parentValueRecordId = parentValue.getParentDataRecord().getID();
                parentValueReferenceID = parentValue.getValue();

                // Make sure the Record is part of the path to this record.
                if (pathRecordIdList.contains(parentValueReferenceID))
                {
                    return true;
                }
                else if (pathRecordIdList.contains(parentValueRecordId))
                {
                    // This is a special case, where only one reference value is available in this property and the parent of the reference is part of the lineage.
                    return (parentValue.getParentRecordValue() != null && parentValue.getParentRecordValue().getSubValueCount() == 1);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // Finally check that the value is a field value if required or else a property value.
                return isFieldSpecified() ? parentValue.isFieldValue() : parentValue.isPropertyValue();
            }
        }
    }

    public ValueRequirement createDependencyRequirement()
    {
        if (!Strings.isNullOrEmpty(drId))
        {
            ValueRequirement drDependencyRequirement;

            drDependencyRequirement = ValueRequirement.createValueRequirement(RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE);
            drDependencyRequirement.setValue(drId);
            if (!Strings.isNullOrEmpty(propertyId))
            {
                drDependencyRequirement.addSubRequirement(createPropertyDependencyRequirement());
            }
            else
            {
                ValueRequirement conceptRelationshipRequirement;
                ValueRequirement conceptDependency;

                // Create the concept dependency requirement.
                conceptDependency = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_DEPENDENCY_TYPE);

                // Create the requirement specifying the concept graph to use for the relationship.
                conceptRelationshipRequirement = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_RELATIONSHIP_TYPE);
                conceptRelationshipRequirement.setValue(relationId);
                conceptDependency.addSubRequirement(conceptRelationshipRequirement);

                // Create the requirement specifying the concept type on which the relationship is based.
                if (conceptType != null)
                {
                    ValueRequirement conceptTypeRequirement;

                    conceptTypeRequirement = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_TYPE);
                    conceptTypeRequirement.setValue(conceptType.getConceptTypeID());
                    conceptRelationshipRequirement.addSubRequirement(conceptTypeRequirement);
                }

                drDependencyRequirement.addSubRequirement(conceptDependency);
                return drDependencyRequirement;
            }

            return drDependencyRequirement;
        }
        else if (!Strings.isNullOrEmpty(drInstanceId))
        {
            ValueRequirement drInstanceDependencyRequirement;

            drInstanceDependencyRequirement = ValueRequirement.createValueRequirement(RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE);
            if (!Strings.isNullOrEmpty(propertyId))
            {
                drInstanceDependencyRequirement.addSubRequirement(createPropertyDependencyRequirement());
            }
            else
            {
                ValueRequirement conceptRelationshipRequirement;
                ValueRequirement conceptDependency;

                // Create the concept dependency requirement.
                conceptDependency = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_DEPENDENCY_TYPE);

                // Create the requirement specifying the concept graph to use for the relationship.
                conceptRelationshipRequirement = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_RELATIONSHIP_TYPE);
                conceptRelationshipRequirement.setValue(relationId);
                conceptDependency.addSubRequirement(conceptRelationshipRequirement);

                // Create the requirement specifying the concept type on which the relationship is based.
                if (conceptType != null)
                {
                    ValueRequirement conceptTypeRequirement;

                    conceptTypeRequirement = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_TYPE);
                    conceptTypeRequirement.setValue(conceptType.getConceptTypeID());
                    conceptRelationshipRequirement.addSubRequirement(conceptTypeRequirement);
                }

                drInstanceDependencyRequirement.addSubRequirement(conceptDependency);
                return drInstanceDependencyRequirement;
            }

            return drInstanceDependencyRequirement;
        }
        else return createPropertyDependencyRequirement();
    }

    private ValueRequirement createPropertyDependencyRequirement()
    {
        ValueRequirement propertyDependencyRequirement;
        ValueRequirement conceptRelationshipRequirement;
        ValueRequirement conceptDependency;

        // Create the concept dependency requirement.
        conceptDependency = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_DEPENDENCY_TYPE);

        // Create the requirement specifying the concept graph to use for the relationship.
        conceptRelationshipRequirement = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_RELATIONSHIP_TYPE);
        conceptRelationshipRequirement.setValue(relationId);
        conceptDependency.addSubRequirement(conceptRelationshipRequirement);

        // Create the requirement specifying the concept type on which the relationship is based.
        if (conceptType != null)
        {
            ValueRequirement conceptTypeRequirement;

            conceptTypeRequirement = ValueRequirement.createValueRequirement(RequirementType.CONCEPT_TYPE);
            conceptTypeRequirement.setValue(conceptType.getConceptTypeID());
            conceptRelationshipRequirement.addSubRequirement(conceptTypeRequirement);
        }

        // Create the property and potentially the field dependency.
        propertyDependencyRequirement = ValueRequirement.createValueRequirement(RequirementType.PROPERTY_DEPENDENCY_TYPE);
        propertyDependencyRequirement.setValue(propertyId);
        if (!Strings.isNullOrEmpty(fieldId))
        {
            ValueRequirement fieldDependencyRequirement;

            fieldDependencyRequirement = ValueRequirement.createValueRequirement(RequirementType.FIELD_DEPENDENCY_TYPE);
            fieldDependencyRequirement.setValue(fieldId);
            propertyDependencyRequirement.addSubRequirement(fieldDependencyRequirement);
            fieldDependencyRequirement.addSubRequirement(conceptDependency);
            return propertyDependencyRequirement;
        }
        else
        {
            propertyDependencyRequirement.addSubRequirement(conceptDependency);
            return propertyDependencyRequirement;
        }
    }

    @Override
    public String toString()
    {
        return "DataDependency{" + "drInstanceID=" + drInstanceId + ", drID=" + drId + ", propertyID=" + propertyId + ", fieldID=" + fieldId + ", conceptGraphID=" + relationId + ", conceptType=" + conceptType + '}';
    }
}
