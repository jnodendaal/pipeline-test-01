package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.PrintStream;

/**
 * @author Bouwer du Preez
 */
public class T8AccessLayerPrinter
{
    private final TerminologyProvider terminologyProvider;
    private final PrintStream printStream;
    
    public T8AccessLayerPrinter(PrintStream printStream, TerminologyProvider terminologyProvider)
    {
        this.printStream = printStream;
        this.terminologyProvider = terminologyProvider;
    }
    
    public void printAccessLayer(DataRecordAccessLayer accessLayer)
    {
        printAccessLayer(accessLayer, 0);
    }
    
    private void printAccessLayer(DataRecordAccessLayer accessLayer, int indent)
    {
        DataRecord record;
        String label;
        String term;
        String recordID;
        
        record = accessLayer.getDataRecord();
        recordID = record.getID();
        term = terminologyProvider != null ? terminologyProvider.getTerm(null, record.getDataRequirementInstanceID()) : null;
        label = (term != null ? term + "(" + recordID + ")" : recordID) + accessLayer.toString();
        printStream.println(createString("RECORD:" + label, indent));
        for (SectionAccessLayer sectionAccessLayer : accessLayer.getSectionAccessLayers())
        {
            printAccessLayer(sectionAccessLayer, indent+1);
        }
        
        for (DataRecord subRecord : record.getSubRecords())
        {
            printAccessLayer(subRecord.getAccessLayer(), indent+1);
        }
    }
    
    private void printAccessLayer(SectionAccessLayer sectionAccessLayer, int indent)
    {
        String term;
        String conceptID;
        String label;
        
        conceptID = sectionAccessLayer.getSectionID();
        term = terminologyProvider != null ? terminologyProvider.getTerm(null, conceptID) : null;
        label = (term != null ? term + "(" + conceptID + ")" : conceptID) + sectionAccessLayer.toString();
        printStream.println(createString("SECTION:" + label, indent));
        for (PropertyAccessLayer propertyAccessLayer : sectionAccessLayer.getPropertyAccessLayers())
        {
            printAccessLayer(propertyAccessLayer, indent+1);
        }
    }
    
    private void printAccessLayer(PropertyAccessLayer propertyAccessLayer, int indent)
    {
        String term;
        String conceptID;
        String label;
        
        conceptID = propertyAccessLayer.getPropertyId();
        term = terminologyProvider != null ? terminologyProvider.getTerm(null, conceptID) : null;
        label = (term != null ? term + "(" + conceptID + ")" : conceptID) + propertyAccessLayer.toString();
        printStream.println(createString("PROPERTY:" + label, indent));
        for (FieldAccessLayer fieldAccessLayer : propertyAccessLayer.getFieldAccessLayers())
        {
            printAccessLayer(fieldAccessLayer, indent+1);
        }
    }
    
    private void printAccessLayer(FieldAccessLayer fieldAccessLayer, int indent)
    {
        String term;
        String conceptID;
        String label;
        
        conceptID = fieldAccessLayer.getFieldID();
        term = terminologyProvider != null ? terminologyProvider.getTerm(null, conceptID) : null;
        label = (term != null ? term + "(" + conceptID + ")" : conceptID) + fieldAccessLayer.toString();
        printStream.println(createString("FIELD:" + label, indent));
    }
    
    private StringBuffer createString(String inputString, int indent)
    {
        StringBuffer string;
        
        string = new StringBuffer();
        for (int i = 0; i < indent; i++)
        {
            string.append(" ");
        }
        
        string.append(inputString);
        return string;
    }
}
