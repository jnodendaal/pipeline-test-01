package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileNewAttachment;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.utilities.codecs.Base64Codec;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileNewAttachment extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE_NEW_ATTACHMENT";

    public T8DtDataFileNewAttachment(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileNewAttachment.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFileNewAttachment newAttachment;
            JsonObject jsonNewRecord;
            byte[] fileData;

            newAttachment = (T8DataFileNewAttachment)object;
            fileData = newAttachment.getFileData();

            // Create the json object.
            jsonNewRecord = new JsonObject();
            jsonNewRecord.add("recordId", newAttachment.getRecordId());
            jsonNewRecord.add("propertyId", newAttachment.getPropertyId());
            jsonNewRecord.add("fieldId", newAttachment.getFieldId());
            jsonNewRecord.add("filename", newAttachment.getFilename());
            jsonNewRecord.add("fileData", fileData != null ? Base64Codec.encode(fileData) : null);
            return jsonNewRecord;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileNewAttachment deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                T8DataFileNewAttachment newAttachment;
                JsonObject jsonNewRecord;
                String fileDataString;

                jsonNewRecord = (JsonObject)jsonValue;
                fileDataString = jsonNewRecord.getString("fileData");
                newAttachment = new T8DataFileNewAttachment();
                newAttachment.setRecordId(jsonNewRecord.getString("recordId"));
                newAttachment.setPropertyId(jsonNewRecord.getString("propertyId"));
                newAttachment.setFieldId(jsonNewRecord.getString("fieldId"));
                newAttachment.setFilename(jsonNewRecord.getString("filename"));
                newAttachment.setFileData(fileDataString != null ? Base64Codec.decode(fileDataString) : null);
                return newAttachment;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while parsing new data file attachment update from JSON: " + jsonValue, e);
            }
        }
        else return null;
    }
}
