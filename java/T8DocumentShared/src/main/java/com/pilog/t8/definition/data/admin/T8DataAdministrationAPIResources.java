package com.pilog.t8.definition.data.admin;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataAdministrationAPIResources implements T8DefinitionResource
{
    public static final String OPERATION_API_ORG_DATA_INSERT_ROOT_ORGANIZATION = "@OS_API_ORG_DATA_INSERT_ROOT_ORGANIZATION";
    
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_ORG_ID = "$P_ORG_ID";
    public static final String PARAMETER_ORG_TYPE_ID = "$P_ORG_TYPE_ID";
    public static final String PARAMETER_PARENT_ORG_ID = "$P_PARENT_ORG_ID";
    public static final String PARAMETER_ORG_ID_LIST = "$P_ORG_ID_LIST";
    public static final String PARAMETER_ODT_ID = "$P_ODT_ID";
    public static final String PARAMETER_ODS_ID = "$P_ODS_ID";
    public static final String PARAMETER_PARENT_ODT_ID = "$P_PARENT_ODT_ID";
    public static final String PARAMETER_ODT_ID_LIST = "$P_ODT_ID_LIST";
    public static final String PARAMETER_CONCEPT_ODT_ID = "$P_CONCEPT_ODT_ID";
    public static final String PARAMETER_CONCEPT_ODT_ID_LIST = "$P_CONCEPT_ODT_ID_LIST";
    public static final String PARAMETER_CONCEPT = "$P_CONCEPT";
    public static final String PARAMETER_CONCEPT_LIST = "$P_CONCEPT_LIST";
    public static final String PARAMETER_CONCEPT_ID = "$P_CONCEPT_ID";
    public static final String PARAMETER_CONCEPT_ID_LIST = "$P_CONCEPT_ID_LIST";
    public static final String PARAMETER_TERM = "$P_TERM";
    public static final String PARAMETER_TERM_LIST = "$P_TERM_LIST";
    public static final String PARAMETER_TERM_ID = "$P_TERM_ID";
    public static final String PARAMETER_TERM_ID_LIST = "$P_TERM_ID_LIST";
    public static final String PARAMETER_DEFINITION = "$P_DEFINITION";
    public static final String PARAMETER_DEFINITION_LIST = "$P_DEFINITION_LIST";
    public static final String PARAMETER_DEFINITION_ID = "$P_DEFINITION_ID";
    public static final String PARAMETER_DEFINITION_ID_LIST = "$P_DEFINITION_ID_LIST";
    public static final String PARAMETER_ABBREVIATION = "$P_ABBREVIATION";
    public static final String PARAMETER_ABBREVIATION_LIST = "$P_ABBREVIATION_LIST";
    public static final String PARAMETER_ABBREVIATION_ID = "$P_ABBREVIATION_ID";
    public static final String PARAMETER_ABBREVIATION_ID_LIST = "$P_ABBREVIATION_ID_LIST";
    public static final String PARAMETER_CODE = "$P_CODE";
    public static final String PARAMETER_CODE_LIST = "$P_CODE_LIST";
    public static final String PARAMETER_CODE_ID = "$P_CODE_ID";
    public static final String PARAMETER_CODE_ID_LIST = "$P_CODE_ID_LIST";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_LANGUAGE_ID_LIST = "$P_LANGUAGE_ID_LIST";
    public static final String PARAMETER_DR_ID = "$P_DR_ID";
    public static final String PARAMETER_DR_ID_LIST = "$P_DR_ID_LIST";
    public static final String PARAMETER_DR_INSTANCE_ID = "$P_DR_INSTANCE_ID";
    public static final String PARAMETER_DR_INSTANCE_ID_LIST = "$P_DR_INSTANCE_ID_LIST";
    public static final String PARAMETER_RECORD_ID = "$P_RECORD_ID";
    public static final String PARAMETER_RECORD_ID_LIST = "$P_RECORD_ID_LIST";
    public static final String PARAMETER_DELETED_RECORD_ID_LIST = "$P_DELETED_RECORD_ID_LIST";
    public static final String PARAMETER_VALIDATION_IDENTIFIER_LIST = "$P_VALIDATION_IDENTIFIER_LIST";
    public static final String PARAMETER_VALIDATION_REPORT_LIST = "$P_VALIDATION_REPORT_LIST";
    public static final String PARAMETER_DATA_REQUIREMENT = "$P_DATA_REQUIREMENT";
    public static final String PARAMETER_DATA_REQUIREMENT_LIST = "$P_DATA_REQUIREMENT_LIST";
    public static final String PARAMETER_DATA_REQUIREMENT_INSTANCE = "$P_DATA_REQUIREMENT_INSTANCE";
    public static final String PARAMETER_DATA_REQUIREMENT_INSTANCE_LIST = "$P_DATA_REQUIREMENT_INSTANCE_LIST";
    public static final String PARAMETER_DATA_RECORD = "$P_DATA_RECORD";
    public static final String PARAMETER_DATA_RECORD_LIST = "$P_DATA_RECORD_LIST";
    public static final String PARAMETER_INSERTED_DATA_RECORD_LIST = "$P_INSERTED_DATA_RECORD_LIST";
    public static final String PARAMETER_UPDATED_DATA_RECORD_LIST = "$P_UPDATED_DATA_RECORD_LIST";
    public static final String PARAMETER_TERMINOLOGY = "$P_TERMINOLOGY";
    public static final String PARAMETER_TERMINOLOGY_LIST = "$P_TERMINOLOGY_LIST";
    public static final String PARAMETER_ORGANIZATION_CONCEPT_LINK_LIST = "$P_ORGANIZATION_CODE_LINK_LIST";
    public static final String PARAMETER_ORGANIZATION_TERM_LINK_LIST = "$P_ORGANIZATION_TERM_LINK_LIST";
    public static final String PARAMETER_ORGANIZATION_ABBREVIATION_LINK_LIST = "$P_ORGANIZATION_ABBREVIATION_LINK_LIST";
    public static final String PARAMETER_ORGANIZATION_DEFINITION_LINK_LIST = "$P_ORGANIZATION_DEFINITION_LINK_LIST";
    public static final String PARAMETER_ORGANIZATION_CODE_LINK_LIST = "$P_ORGANIZATION_CODE_LINK_LIST";
    public static final String PARAMETER_ORGANIZATION_STRUCTURE = "$P_ORGANIZATION_STRUCTURE";
    public static final String PARAMETER_ONTOLOGY_DATA_STRUCTURE = "$P_ORGANIZATION_DATA_STRUCTURE";
    public static final String PARAMETER_DESCRIPTION_FILTER_LIST = "$P_DESCRIPTION_FILTER_LIST";
    public static final String PARAMETER_DATA_RECORD_DESCRIPTION_LIST = "$P_DATA_RECORD_DESCRIPTION_LIST";
    public static final String PARAMETER_DATA_RECORD_VALUE_STRING_LIST = "$P_DATA_RECORD_VALUE_STRING_LIST";
    public static final String PARAMETER_DATA_RECORD_ATTACHMENT_DETAILS_LIST = "$P_DATA_RECORD_ATTACHMENT_DETAILS_LIST";
    public static final String PARAMETER_ACTIVE_INDICATOR = "$P_ACTIVE_INDICATOR";
    public static final String PARAMETER_GENERATE_ONTOLOGY = "$P_GENERATE_ONTOLOGY";
    public static final String PARAMETER_GENERATE_DESCRIPTIONS = "$P_GENERATE_DESCRIPTIONS";
    public static final String PARAMETER_SYNCHRONOUS_DESCRIPTION_GENERATION = "$P_SYNCHRONOUS_DESCRIPTION_GENERATION";
    public static final String PARAMETER_SAVE_ONTOLOGY = "$P_SAVE_ONTOLOGY";
    public static final String PARAMETER_DELETE_ONTOLOGY = "$P_DELETE_ONTOLOGY";
    public static final String PARAMETER_INCLUDE_ONTOLOGY = "$P_INCLUDE_ONTOLOGY";
    public static final String PARAMETER_INCLUDE_TERMINOLOGY = "$P_INCLUDE_TERMINOLOGY";
    public static final String PARAMETER_INCLUDE_DESCRIPTIONS = "$P_INCLUDE_DESCRIPTIONS";
    public static final String PARAMETER_INCLUDE_ATTACHMENT_DETAILS = "$P_INCLUDE_ATTACHMENT_DETAILS";
    public static final String PARAMETER_INCLUDE_DESCENDANT_RECORDS = "$P_INCLUDE_DESCENDANT_RECORDS";
    public static final String PARAMETER_INCLUDE_ORGANIZATION_LINKS = "$P_INCLUDE_ORGANIZATION_LINKS";
    public static final String PARAMETER_INCLUDE_TERMS = "$P_INCLUDE_TERMS";
    public static final String PARAMETER_INCLUDE_ABBREVIATIONS = "$P_INCLUDE_ABBREVIATIONS";
    public static final String PARAMETER_INCLUDE_DEFINITIONS = "$P_INCLUDE_DEFINITIONS";
    public static final String PARAMETER_INCLUDE_CODES = "$P_INCLUDE_CODES";
    public static final String PARAMETER_UPDATE_COUNT = "$P_UPDATE_COUNT";
    public static final String PARAMETER_UPDATE_TERMS = "$P_UPDATE_TERMS";
    public static final String PARAMETER_UPDATE_ABBREVIATIONS = "$P_UPDATE_ABBREVIATIONS";
    public static final String PARAMETER_UPDATE_DEFINITIONS = "$P_UPDATE_DEFINITIONS";
    public static final String PARAMETER_UPDATE_CODES = "$P_UPDATE_CODES";
    public static final String PARAMETER_UPDATE_TERMINOLOGY = "$P_UPDATE_TERMINOLOGY";
    public static final String PARAMETER_INSERT_TERMS = "$P_INSERT_TERMS";
    public static final String PARAMETER_INSERT_ABBREVIATIONS = "$P_INSERT_ABBREVIATIONS";
    public static final String PARAMETER_INSERT_DEFINITIONS = "$P_INSERT_DEFINITIONS";
    public static final String PARAMETER_INSERT_CODES = "$P_INSERT_CODES";
    public static final String PARAMETER_INSERT_TERMINOLOGY = "$P_INSERT_TERMINOLOGY";
    public static final String PARAMETER_SAVE_TERMS = "$P_SAVE_TERMS";
    public static final String PARAMETER_SAVE_ABBREVIATIONS = "$P_SAVE_ABBREVIATIONS";
    public static final String PARAMETER_SAVE_DEFINITIONS = "$P_SAVE_DEFINITIONS";
    public static final String PARAMETER_SAVE_CODES = "$P_SAVE_CODES";
    public static final String PARAMETER_SAVE_TERMINOLOGY = "$P_SAVE_TERMINOLOGY";
    public static final String PARAMETER_SAVE_DOCUMENT = "$P_SAVE_DOCUMENT";
    public static final String PARAMETER_DELETION_ENABLED = "$P_DELETION_ENABLED";
    public static final String PARAMETER_INDEPENDENT = "$P_INDEPENDENT";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<T8Definition>();
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_DATA_INSERT_ROOT_ORGANIZATION);
            definition.setMetaDisplayName("Insert Root Organization");
            definition.setMetaDescription("Inserts a new Root Organization in the organization structure.");
            definition.setClassName("com.pilog.t8.data.org.T8OrganizationDataAPIOperations$APIOperationInsertRootOrganization");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ODS_ID, "Ontology Data Structure ID", "The concept ID of the Ontology Data Structure that will be used by the new organization.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ORG_TYPE_ID, "Organization Type ID", "The concept ID of the organization type of the new organization to be added.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT, "Organization Concept", "The complete ontology concept of the new organization to be added.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INSERT_TERMINOLOGY, "Insert Terminology", "A boolean flag to indicate whether or not the terminology for the new concept should be inserted.", T8DataType.BOOLEAN));
            definitions.add(definition);
            
            return definitions;
        }
        else return null;
    }
}
