package com.pilog.t8.data.document.datastring;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringElement implements Serializable
{
    private String id;
    private boolean active;
    private double priority;
    private String expression;
    private String conditionExpression;
    private String prefix;
    private String suffix;
    private String separator;
    private String name;
    private String description;
    private String dependencyElementId;
    private String groupId;
    private int lengthRestriction;
    private TruncationType truncationType;
    private boolean strictParticleInclusion;
    private T8DataStringElement parentElement;
    private final List<T8DataStringElement> subElements;

    /**
     * The truncation types that can be set on an element.
     */
    public enum TruncationType
    {
        PARTICLE, // Particles will be dropped according to priority.
        WORD, // Words will be dropped from the end of the rendered character sequence.
        CHARACTER // Charactesr will be dropped from the end of the rendered character sequence.
    };

    public T8DataStringElement()
    {
        this.subElements = new ArrayList<>();
        this.strictParticleInclusion = false;
        this.groupId = null;
    }

    public T8DataStringElement getParentElement()
    {
        return parentElement;
    }

    void setParentElement(T8DataStringElement element)
    {
        this.parentElement = element;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public boolean isActive()
    {
        return this.active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDependencyElementID()
    {
        return dependencyElementId;
    }

    public void setDependencyElementID(String dependencyElementID)
    {
        this.dependencyElementId = dependencyElementID;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public double getIndex()
    {
        if (parentElement != null)
        {
            return parentElement.getSubElementIndex(this);
        }
        else return 0;
    }

    public int incrementIndex()
    {
        if (parentElement != null)
        {
            return parentElement.incrementSubElementIndex(this);
        }
        else return -1;
    }

    public int decrementIndex()
    {
        if (parentElement != null)
        {
            return parentElement.decrementSubElementIndex(this);
        }
        else return -1;
    }

    public int incrementSubElementIndex(T8DataStringElement subElement)
    {
        int index;

        index = subElements.indexOf(subElement);
        if (index > -1)
        {
            if (index < (subElements.size() - 1))
            {
                int newIndex;

                newIndex = index + 1;
                subElements.remove(subElement);
                subElements.add(newIndex, subElement);
                return newIndex;
            }
            else return index;
        }
        else return -1;
    }

    public int decrementSubElementIndex(T8DataStringElement subElement)
    {
        int index;

        index = subElements.indexOf(subElement);
        if (index > -1)
        {
            if (index > 0)
            {
                int newIndex;

                newIndex = index - 1;
                subElements.remove(subElement);
                subElements.add(newIndex, subElement);
                return newIndex;
            }
            else return 0;
        }
        else return -1;
    }

    public double getPriority()
    {
        return priority;
    }

    public void setPriority(double priority)
    {
        this.priority = priority;
    }

    public String getExpression()
    {
        return expression;
    }

    public void setExpression(String expression)
    {
        this.expression = expression;
    }
    
    public String getConditionExpression()
    {
        return conditionExpression;
    }

    public void setConditionExpression(String conditionExpression)
    {
        this.conditionExpression = conditionExpression;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    public String getSuffix()
    {
        return suffix;
    }

    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public int getLengthRestriction()
    {
        return lengthRestriction;
    }

    public void setLengthRestriction(int lengthRestriction)
    {
        this.lengthRestriction = lengthRestriction;
    }

    public TruncationType getTruncationType()
    {
        return truncationType;
    }

    public void setTruncationType(TruncationType truncationType)
    {
        this.truncationType = truncationType;
    }

    /**
     * Convenience method to be used in scripts to set the truncation type using
     * its {@code String} representation.
     *
     * @param truncationType The {@code String} truncation type value
     */
    public void setTruncationType(String truncationType)
    {
        if (truncationType != null)
        {
            this.truncationType = TruncationType.valueOf(truncationType);
        }
    }

    public boolean isStrictParticleInclusion()
    {
        return strictParticleInclusion;
    }

    public void setStrictParticleInclusion(boolean strictParticleInclusion)
    {
        this.strictParticleInclusion = strictParticleInclusion;
    }

    /**
     * Checks whether or not the current format element has any sub-elements.
     *
     * @return {@code true} if the current format element contains any
     *      sub-elements. {@code false} otherwise
     */
    public boolean hasSubElements()
    {
        return !this.subElements.isEmpty();
    }

    public void addSubElement(T8DataStringElement subElement)
    {
        if (subElement != null)
        {
            subElements.add(subElement);
            subElement.setParentElement(this);
        }
        else throw new IllegalArgumentException("Cannot add null sub-element.");
    }

    public boolean removeSubElement(T8DataStringElement subElement)
    {
        if (subElements.remove(subElement))
        {
            subElement.setParentElement(null);
            return true;
        }
        else return false;
    }

    public int getSubElementIndex(T8DataStringElement subElement)
    {
        return subElements.indexOf(subElement);
    }

    /**
     * Returns the sub-elements belonging to this element.
     * @return The list of sub-elements belonging to this element.
     */
    public List<T8DataStringElement> getSubElements()
    {
        return new ArrayList<>(subElements);
    }

    /**
     * This method assigns new id's to this element and all of its
     * descendants. It also fix the element dependency id's.
     */
    public void assignNewIds()
    {
        Map<String, String> newIdChangeMap;
        
        newIdChangeMap = new HashMap<>();
        this.id = T8IdentifierUtilities.createNewGUID();    
        
        for (T8DataStringElement subElement : subElements)
        {
            String oldId;
            String newId;
            
            oldId = subElement.getID();
            subElement.assignNewIds();
            newId = subElement.getID();
            newIdChangeMap.put(oldId, newId);            
        }
                 
        for (T8DataStringElement subElement : subElements)
        {
            if (newIdChangeMap.containsKey(subElement.getDependencyElementID()))
            {
                subElement.setDependencyElementID(newIdChangeMap.get(subElement.getDependencyElementID()));
            }
        }
    }

    /**
     * Searches this element and all descendants for the specified element and
     * returns the result.
     * @param elementID The ID of the element to find.
     * @return The element with an ID matching the requested ID or null if such
     * an element cannot be found.
     */
    public T8DataStringElement findElement(String elementID)
    {
        for (T8DataStringElement childElement : subElements)
        {
            if (childElement.getID().equals(elementID))
            {
                return childElement;
            }
            else
            {
                T8DataStringElement foundElement;

                foundElement = childElement.findElement(elementID);
                if (foundElement != null) return foundElement;
            }
        }

        // Element not found.
        return null;
    }

    /**
     * This method renders a string representation of the supplied document
     * object by applying the rendering formatting rules specified by this
     * element.
     * @param renderingContext The context within which all rendering operations
     * are performed.
     * @param renderingInstance The instance to generateDataString.
     * @param documentObject The document object to be rendered.
     * @return A string representation of the supplied document object.
     */
    public T8DataStringParticle generateParticle(T8DataStringGeneratorContext renderingContext, T8DataStringInstance renderingInstance, Value documentObject, String dataKeyId)
    {
        // Only process this element if it is active.
        if (active)
        {
            List<T8DataStringParticle> particleSequence;
            Object result;
            
            if (!Strings.isNullOrEmpty(conditionExpression))
            {
                // Evaluate the expression.
                try
                {
                    T8ExpressionEvaluator expressionEvaluator;

                    expressionEvaluator = renderingContext.getExpressionEvaluator();
                    expressionEvaluator.setLanguage(renderingInstance.getLanguageID());
                    result = expressionEvaluator.evaluateExpression(conditionExpression, null, documentObject);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Content condition expression in element " + this + " caused exception during evaluation.", e);
                }
                    
                if (!(Boolean)result)
                {
                    return null;
                }
            }
            
            // Construct the particle sequence.
            particleSequence = generateParticleSequence(renderingContext, renderingInstance, documentObject, dataKeyId, strictParticleInclusion);
            for (T8DataStringElement subElement : subElements)
            {
                T8DataStringParticle childParticle;

                childParticle = subElement.generateParticle(renderingContext, renderingInstance, documentObject, dataKeyId);
                if (childParticle != null)
                {
                    particleSequence.add(childParticle);
                }
            }

            // Sort the particles according to their sequence.
            Collections.sort(particleSequence, new ParticleComparator());

            // If we have only one particle return it, else group all particle together.
            if (particleSequence.size() == 1)
            {
                T8DataStringParticle singleParticle;

                // If the single particle in our output particle sequence was generated from this element, return it.
                // If the particle was generated from a sub-element, wrap it in a particle generated from this element to maintain the proper hierarchy.
                singleParticle = particleSequence.get(0);
                if (singleParticle.getElementId().equals(id))
                {
                    return singleParticle;
                }
                else return new T8DataStringParticle("Group:" + name, this, null, particleSequence);
            }
            else return new T8DataStringParticle("Group:" + name, this, null, particleSequence);
        }
        else return null;
    }

    /**
     * Evaluates the value expression for this element in order to construct
     * a rendered value.  The expression may result in a single object or a list
     * of objects.  In either case, each object that is obtained from the
     * expression result is checked to find out if it is a character sequence.
     * If it is, then it is added to the output list of this method as a new
     * rendered particle.  If it is not a character sequence, then the result
     * object is passed to the rendering context to be rendered and afterwards
     * is added to the output list of this method.
     * @param dsContext The rendering context where objects that are not
     * character sequences are passed for rendering.
     * @param documentObject The document object on which the value expression
     * will be evaluated.
     * @return The list of rendered particles constructed from the results of
     * the expression.
     */
    private List<T8DataStringParticle> generateParticleSequence(T8DataStringGeneratorContext dsContext, T8DataStringInstance dsInstance, Value documentObject, String dataKeyId, boolean strictParticleInclusion)
    {
        if (!Strings.isNullOrEmpty(expression))
        {
            T8DataStringParticleSettingsList particleSettingsList;
            Object result;

            // Evaluate the expression.
            try
            {
                T8ExpressionEvaluator expressionEvaluator;

                expressionEvaluator = dsContext.getExpressionEvaluator();
                expressionEvaluator.setLanguage(dsInstance.getLanguageID());
                result = expressionEvaluator.evaluateExpression(expression, null, documentObject);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Content expression in element " + this + " caused exception during evaluation.", e);
            }

            // Get particle settings and apply them to the identified particles.
            particleSettingsList = getDataStringParticleSettings(dsContext, dsInstance, result, dataKeyId);
            if (result instanceof CharSequence)
            {
                CharSequence resultString;

                resultString = (CharSequence)result;
                if (!Strings.isNullOrEmpty(resultString))
                {
                    T8DataStringParticle newParticle;

                    // If the expression evaluates to a character sequence, we simply add it as a rendered particle.
                    newParticle = new T8DataStringParticle(name, this, resultString, null);
                    return ArrayLists.typeSafeList(newParticle);
                }
                else return new ArrayList<>();
            }
            else if (result instanceof Value)
            {
                T8DataStringParticleSettings particleSettings;
                boolean includeParticle;
                Value value;

                // Get the particle settings matching the DocPath result.
                value = (Value)result;
                particleSettings = particleSettingsList != null ? particleSettingsList.getParticleSettings(dsContext, dsInstance, value) : null;
                if (particleSettings != null)
                {
                    includeParticle = particleSettings.isInclude();
                }
                else
                {
                    includeParticle = !strictParticleInclusion;
                }

                // If the particle is to be included, generate it.
                if (includeParticle)
                {
                    T8DataStringParticle particle;

                    // The result of the expression is a single value but not a character sequence, so we need to generateDataString it as such.
                    particle = dsContext.generateDataString(dsInstance, value, dataKeyId);
                    if (particle != null)
                    {
                        if (particleSettings != null)
                        {
                            particle.setPriority(particleSettings.getPriority());
                            particle.setSequence(particleSettings.getSequence());
                        }

                        return ArrayLists.typeSafeList(particle);
                    }
                    else return new ArrayList<>();
                }
                else return new ArrayList<>();
            }
            else if (result instanceof List)
            {
                List<T8DataStringParticle> particleList;

                // If the expression evaluates to a list of objects, we need to check each object in the list and handle it appropriately.
                particleList = new ArrayList<>();
                for (Object listElement : (List)result)
                {
                    if (listElement instanceof CharSequence)
                    {
                        CharSequence resultString;

                        resultString = (CharSequence)listElement;
                        if (!Strings.isNullOrEmpty(resultString))
                        {
                            T8DataStringParticle newParticle;
                            int newParticleIndex;

                            // Get the index where the new particle will be added in the list.
                            newParticleIndex = particleList.size();

                            // The list element is a character sequence, so add it to the particle list.
                            newParticle = new T8DataStringParticle(name + "[" + newParticleIndex + "]", this, resultString, null);

                            // Since this particle is part of a list and therefore has no direct priority and sequence specified by its parent element,
                            // we set the priority and sequence both to the index of the particle in the list.
                            newParticle.setSequence(newParticleIndex);
                            newParticle.setPriority(newParticleIndex);

                            // Add the particle to the result list.
                            particleList.add(newParticle);
                        }
                        else return new ArrayList<>();
                    }
                    else if (listElement instanceof Value) // The list element is not a character sequence, so we need to generateDataString it as such.
                    {
                        T8DataStringParticleSettings particleSettings;
                        boolean includeParticle;
                        Value value;

                        // Get the particle settings matching the DocPath result.
                        value = (Value)listElement;
                        particleSettings = particleSettingsList != null ? particleSettingsList.getParticleSettings(dsContext, dsInstance, value) : null;
                        if (particleSettings != null)
                        {
                            includeParticle = particleSettings.isInclude();
                        }
                        else
                        {
                            includeParticle = !strictParticleInclusion;
                        }

                        // If the particle is to be included, generate it.
                        if (includeParticle)
                        {
                            T8DataStringParticle newParticle;

                            // The result of the expression is a single value but not a character sequence, so we need to generateDataString it as such.
                            newParticle = dsContext.generateDataString(dsInstance, value, dataKeyId);
                            if (newParticle != null)
                            {
                                if (particleSettings != null)
                                {
                                    newParticle.setPriority(particleSettings.getPriority());
                                    newParticle.setSequence(particleSettings.getSequence());
                                }
                                else
                                {
                                    int newParticleIndex;

                                    // Get the index where the new particle will be added in the list.
                                    newParticleIndex = particleList.size();

                                    // Since this particle is part of a list and therefore has no direct priority and sequence specified by its parent element,
                                    // we set the priority and sequence both to the index of the particle in the list.
                                    newParticle.setSequence(newParticleIndex);
                                    newParticle.setPriority(newParticleIndex);
                                }

                                // Add the particle to the result list.
                                particleList.add(newParticle);
                            }
                        }
                    }
                }

                // Return the lsit of rendered particles.
                return particleList;
            }
            else return new ArrayList<>();
        }
        else return new ArrayList<>();
    }

    private T8DataStringParticleSettingsList getDataStringParticleSettings(T8DataStringGeneratorContext renderingContext, T8DataStringInstance dsInstance, Object resultObject, String dataKeyId)
    {
        if (resultObject instanceof Value)
        {
            ArrayList<Value> valueList;

            valueList = new ArrayList<>();
            valueList.add((Value)resultObject);
            return renderingContext.getDataStringParticleSettings(dsInstance, valueList, dataKeyId);
        }
        else if (resultObject instanceof List)
        {
            ArrayList<Value> valueList;

            valueList = new ArrayList<>();
            for (Object object : (List)resultObject)
            {
                if (object instanceof Value)
                {
                    valueList.add((Value)object);
                }
            }

            return renderingContext.getDataStringParticleSettings(dsInstance, valueList, dataKeyId);
        }
        else return null;
    }

    private class ParticleComparator implements Comparator<T8DataStringParticle>
    {
        @Override
        public int compare(T8DataStringParticle particle1, T8DataStringParticle particle2)
        {
            return ((Double)particle1.getSequence()).compareTo(particle2.getSequence());
        }
    }
}
