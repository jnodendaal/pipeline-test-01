package com.pilog.t8.api.datarecordeditor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileUpdate implements Serializable
{
    private final String fileId;
    private final List<T8DataFileValueUpdate> valueUpdates;
    private final List<T8DataFileRecordCreation> recordCreations;
    private final List<T8DataFileNewRecord> newRecords;
    private final List<T8DataFileRecordDeletion> recordDeletions;
    private final List<T8DataFileNewAttachment> newAttachments;

    private final List<T8DataFilePropertyAccessUpdate> propertyAccessUpdates;

    public T8DataFileUpdate(String fileId)
    {
        this.fileId = fileId;
        this.valueUpdates = new ArrayList<>();
        this.recordCreations = new ArrayList<>();
        this.newRecords = new ArrayList<>();
        this.recordDeletions = new ArrayList<>();
        this.newAttachments = new ArrayList<>();
        this.propertyAccessUpdates = new ArrayList<>();
    }

    /**
     * For each update in the input instance, removes the equivalent update from this instance.
     * @param updatesToRemove The input instance containing updates to remove from this instance.
     */
    public void remove(T8DataFileUpdate updatesToRemove)
    {
        // Remove equivalent value updates.
        for (T8DataFileValueUpdate updateToRemove : updatesToRemove.getValueUpdates())
        {
            Iterator<T8DataFileValueUpdate> updateIterator;

            updateIterator = valueUpdates.iterator();
            while (updateIterator.hasNext())
            {
                if (updateIterator.next().isEquivalent(updateToRemove))
                {
                    updateIterator.remove();
                }
            }
        }
    }

    public String getFileId()
    {
        return this.fileId;
    }

    public List<T8DataFileValueUpdate> getValueUpdates()
    {
        return new ArrayList<>(valueUpdates);
    }

    public void setValueUpdates(List<T8DataFileValueUpdate> updates)
    {
        this.valueUpdates.clear();
        if (updates != null)
        {
            this.valueUpdates.addAll(updates);
        }
    }

    public void addValueUpdate(T8DataFileValueUpdate update)
    {
        this.valueUpdates.add(update);
    }

    public List<T8DataFileRecordCreation> getRecordCreations()
    {
        return new ArrayList<>(recordCreations);
    }

    public void setRecordCreations(List<T8DataFileRecordCreation> creations)
    {
        this.recordCreations.clear();
        if (creations != null)
        {
            this.recordCreations.addAll(creations);
        }
    }

    public void addRecordCreation(T8DataFileRecordCreation creation)
    {
        this.recordCreations.add(creation);
    }

    public List<T8DataFileNewRecord> getNewRecords()
    {
        return new ArrayList<>(newRecords);
    }

    public void setNewRecords(List<T8DataFileNewRecord> newRecords)
    {
        this.newRecords.clear();
        if (newRecords != null)
        {
            this.newRecords.addAll(newRecords);
        }
    }

    public void addNewRecord(T8DataFileNewRecord newRecord)
    {
        this.newRecords.add(newRecord);
    }

    public List<T8DataFileRecordDeletion> getRecordDeletions()
    {
        return new ArrayList<>(recordDeletions);
    }

    public void setRecordDeletions(List<T8DataFileRecordDeletion> deletions)
    {
        this.recordDeletions.clear();
        if (deletions != null)
        {
            this.recordDeletions.addAll(deletions);
        }
    }

    public void addRecordDeletion(T8DataFileRecordDeletion deletion)
    {
        this.recordDeletions.add(deletion);
    }

    public List<T8DataFileNewAttachment> getNewAttachments()
    {
        return new ArrayList<>(newAttachments);
    }

    public void setNewAttachments(List<T8DataFileNewAttachment> newAttachments)
    {
        this.newAttachments.clear();
        if (newAttachments != null)
        {
            this.newAttachments.addAll(newAttachments);
        }
    }

    public void addNewAttachment(T8DataFileNewAttachment newAttachment)
    {
        this.newAttachments.add(newAttachment);
    }

    public List<T8DataFilePropertyAccessUpdate> getPropertyAccessUpdates()
    {
        return new ArrayList<>(propertyAccessUpdates);
    }

    public void addPropertyAccessUpdate(T8DataFilePropertyAccessUpdate update)
    {
        this.propertyAccessUpdates.add(update);
    }

    public void setPropertyAccessUpdates(List<T8DataFilePropertyAccessUpdate> updates)
    {
        this.propertyAccessUpdates.clear();
        if (updates != null)
        {
            this.propertyAccessUpdates.addAll(updates);
        }
    }
}
