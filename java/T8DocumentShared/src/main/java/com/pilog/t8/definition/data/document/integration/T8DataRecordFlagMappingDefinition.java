package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8DataRecordFlagMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_RECORD_FLAG";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_FLAG";
    public static final String DISPLAY_NAME = "Flag Mapping";
    public static final String DESCRIPTION = "Flag Mapping";
    public static final String IDENTIFIER_PREFIX = "RP_MAP_";

    public enum Datum
    {
        FLAG_TYPE,
        FLAG_VALUE_EXPRESSION,
        FLAG_KEYS
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRecordFlagMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FLAG_TYPE.toString(), "Flag Type", "The flag type."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.FLAG_VALUE_EXPRESSION.toString(), "Flag Value Expression", "The flag value."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FLAG_KEYS.toString(), "Flag Keys", "The additional flag keys."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        if(Datum.FLAG_KEYS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFlagKeyMappingDefinition.GROUP_IDENTIFIER));
        return null;
    }

    public String getFlagType()
    {
        return (String)getDefinitionDatum(Datum.FLAG_TYPE.toString());
    }

    public void setFlagType(String expression)
    {
        setDefinitionDatum(Datum.FLAG_TYPE.toString(), expression);
    }

    public String getFlagValueExpression()
    {
        return (String)getDefinitionDatum(Datum.FLAG_VALUE_EXPRESSION.toString());
    }

    public void setFlagValueExpression(String expression)
    {
        setDefinitionDatum(Datum.FLAG_VALUE_EXPRESSION.toString(), expression);
    }

    public List<T8DataRecordFlagKeyMappingDefinition> getFlagKeys()
    {
        return (List<T8DataRecordFlagKeyMappingDefinition>)getDefinitionDatum(Datum.FLAG_KEYS.toString());
    }

    public void setFlagKeys(List<T8DataRecordFlagKeyMappingDefinition> dataRecordFlagKeyMappingDefinitions)
    {
        setDefinitionDatum(Datum.FLAG_KEYS.toString(), dataRecordFlagKeyMappingDefinitions);
    }
}
