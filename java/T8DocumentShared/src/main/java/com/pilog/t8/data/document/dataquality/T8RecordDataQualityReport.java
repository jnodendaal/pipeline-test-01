package com.pilog.t8.data.document.dataquality;

/**
 * @author Bouwer du Preez
 */
public class T8RecordDataQualityReport extends T8DataQualityMetrics
{
    private final String recordId;

    public T8RecordDataQualityReport(String recordId)
    {
        this.recordId = recordId;
    }

    public String getRecordId()
    {
        return recordId;
    }
}
