package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.ontology.T8ConceptAlteration;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordStateChange
{
    private DataRecord fromState;
    private DataRecord toState;
    private T8ConceptAlteration conceptAlteration;
    private boolean update;
    private final List<T8RecordPropertyStateChange> propertyStateChanges;
    private final List<T8DataStringParticleSettings> insertedParticleSettings;
    private final List<T8DataStringParticleSettings> updatedParticleSettings;
    private final List<T8DataStringParticleSettings> deletedParticleSettings;

    public T8DataRecordStateChange(DataRecord fromState, DataRecord toState)
    {
        this.update = false;
        this.propertyStateChanges = new ArrayList<>();
        this.insertedParticleSettings = new ArrayList<>();
        this.updatedParticleSettings = new ArrayList<>();
        this.deletedParticleSettings = new ArrayList<>();
        evaluateStateChange(fromState, toState);
    }

    public boolean isInsert()
    {
        return fromState == null && toState != null;
    }

    public boolean isUpdate()
    {
        return update;
    }

    public boolean isDeletion()
    {
        return fromState != null && toState == null;
    }

    public DataRecord getFromState()
    {
        return fromState;
    }

    public DataRecord getToState()
    {
        return toState;
    }

    public T8ConceptAlteration getConceptAlteration()
    {
        return conceptAlteration;
    }

    public List<T8DataStringParticleSettings> getInsertedParticleSettings()
    {
        return insertedParticleSettings;
    }

    public List<T8DataStringParticleSettings> getUpdatedParticleSettings()
    {
        return updatedParticleSettings;
    }

    public List<T8DataStringParticleSettings> getDeletedParticleSettings()
    {
        return deletedParticleSettings;
    }

    private void evaluateStateChange(DataRecord fromStateRecord, DataRecord toStateRecord)
    {
        this.fromState = fromStateRecord;
        this.toState = toStateRecord;
        if (fromStateRecord == null)
        {
            // Register the toStateRecord's concept as an insert.
            conceptAlteration = T8ConceptAlteration.getConceptAlteration(null, toStateRecord.getOntology());

            // Add all property state changes.
            for (RecordProperty toStateProperty : toStateRecord.getRecordProperties())
            {
                propertyStateChanges.add(new T8RecordPropertyStateChange(null, toStateProperty));
            }

            // Add inserted data string particle settings.
            for (T8DataStringParticleSettings toStateSettings : toStateRecord.getDataStringParticleSettings())
            {
                insertedParticleSettings.add(toStateSettings);
            }
        }
        else if (toStateRecord == null)
        {
            // Register the fromStateRecord's concept as a deletion.
            conceptAlteration = T8ConceptAlteration.getConceptAlteration(fromStateRecord.getOntology(), null);

            // Add all property state changes.
            for (RecordProperty fromStateProperty : fromStateRecord.getRecordProperties())
            {
                propertyStateChanges.add(new T8RecordPropertyStateChange(fromStateProperty, null));
            }

            // Add deleted data string particle settings.
            for (T8DataStringParticleSettings fromStateSettings : fromStateRecord.getDataStringParticleSettings())
            {
                deletedParticleSettings.add(fromStateSettings);
            }
        }
        else
        {
            T8OntologyConcept fromStateRecordConcept;
            T8OntologyConcept toStateRecordConcept;

            // If any of the content of the record changed, register the toStateRecord as an update.
            update = isUpdate(fromStateRecord, toStateRecord);

            // Determine the record concept alteration.
            fromStateRecordConcept = fromStateRecord.getOntology();
            toStateRecordConcept = toStateRecord.getOntology();
            conceptAlteration = T8ConceptAlteration.getConceptAlteration(fromStateRecordConcept, toStateRecordConcept);

            // Add all property state changes (updates and inserts).
            for (RecordProperty toStateProperty : toStateRecord.getRecordProperties())
            {
                RecordProperty fromStateProperty;

                fromStateProperty = fromStateRecord.getRecordProperty(toStateProperty.getPropertyID());
                propertyStateChanges.add(new T8RecordPropertyStateChange(fromStateProperty, toStateProperty));
            }

            // Add all property state changes (deletions).
            for (RecordProperty fromStateProperty : fromStateRecord.getRecordProperties())
            {
                RecordProperty toStateProperty;

                toStateProperty = toStateRecord.getRecordProperty(fromStateProperty.getPropertyID());
                if (toStateProperty == null) propertyStateChanges.add(new T8RecordPropertyStateChange(fromStateProperty, fromStateProperty));
            }

            // Find the updated data string particle settings.
            for (T8DataStringParticleSettings toStateSettings : toStateRecord.getDataStringParticleSettings())
            {
                String settingsId;

                settingsId = toStateSettings.getId();
                if (fromStateRecord.getDataStringParticleSettings(settingsId) != null)
                {
                    updatedParticleSettings.add(toStateSettings);
                }
                else
                {
                    insertedParticleSettings.add(toStateSettings);
                }
            }

            // Find the deleted data string particle settings.
            for (T8DataStringParticleSettings fromStateSettings : fromStateRecord.getDataStringParticleSettings())
            {
                String settingsId;

                settingsId = fromStateSettings.getId();
                if (toStateRecord.getDataStringParticleSettings(settingsId) == null)
                {
                    deletedParticleSettings.add(fromStateSettings);
                }
            }
        }
    }

    private static boolean isUpdate(DataRecord fromState, DataRecord toState)
    {
        // Check for a difference in content between the states in order to identify an update.
        if (!fromState.getContent(false).isEqualTo(toState.getContent(false), false)) return true;
        else return (!fromState.getAttributes().equals(toState.getAttributes())); // If attributes do not match, the record is also considered an update.
    }
}
