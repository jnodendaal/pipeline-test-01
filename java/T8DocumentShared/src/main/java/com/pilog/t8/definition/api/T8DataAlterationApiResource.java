package com.pilog.t8.definition.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationApiResource implements T8DefinitionResource
{
    private static final T8Logger logger = T8Log.getLogger(T8DataAlterationApiResource.class);

    // Operation Identifiers
    public static final String OPERATION_API_ALT_RETRIEVE_ALTERATION = "@OP_API_ALT_RETRIEVE_ALTERATION";
    public static final String OPERATION_API_ALT_INSERT_ALTERATION = "@OP_API_ALT_INSERT_ALTERATION";
    public static final String OPERATION_API_ALT_DELETE_ALTERATION = "@OP_API_ALT_DELETE_ALTERATION";
    public static final String OPERATION_API_ALT_UPDATE_ALTERATION = "@OP_API_ALT_UPDATE_ALTERATION";
    public static final String OPERATION_API_ALT_INSERT_ALTERATION_PACKAGE = "@OP_API_ALT_INSERT_ALTERATION_PACKAGE";
    public static final String OPERATION_API_ALT_DELETE_ALTERATION_PACKAGE = "@OP_API_ALT_DELETE_ALTERATION_PACKAGE";
    public static final String OPERATION_API_ALT_UPDATE_ALTERATION_PACKAGE = "@OP_API_ALT_UPDATE_ALTERATION_PACKAGE";
    public static final String OPERATION_API_ALT_RETRIEVE_ALTERATION_PACKAGE = "@OP_API_ALT_RETRIEVE_ALTERATION_PACKAGE";
    public static final String OPERATION_API_ALT_GET_ALTERATION_PACKAGE_INDEX = "@OP_API_ALT_GET_ALTERATION_PACKAGE_INDEX";
    public static final String OPERATION_API_ALT_APPLY_ALTERATION = "@OP_API_ALT_APPLY_ALTERATION";

    // Operation Paramater Identifiers
    public static final String PARAMETER_ALTERATION_ID = "$P_ALTERATION_ID";
    public static final String PARAMETER_PACKAGE_IID = "$P_PACKAGE_IID";
    public static final String PARAMETER_ALTERATION = "$P_ALTERATION";
    public static final String PARAMETER_ALTERATION_LIST = "$P_ALTERATION_LIST";
    public static final String PARAMETER_ALTERATION_IMPACT = "$P_ALTERATION_IMPACT";
    public static final String PARAMETER_PACKAGE_INDEX = "$P_ALTERATION_INDEX";
    public static final String PARAMETER_START_STEP = "$P_START_STEP";
    public static final String PARAMETER_END_STEP = "$P_END_STEP";
    public static final String PARAMETER_STEP = "$P_STEP";
    public static final String PARAMETER_STEP_OFFSET = "$P_START_STEP";
    public static final String PARAMETER_STEP_RANGE = "$P_END_STEP";
    public static final String PARAMETER_ALTERATION_PACKAGE = "$P_PACKAGE";

    // Field Identifiers
    public static final String EF_PACKAGE_IID = "$PACKAGE_IID";
    public static final String EF_PACKAGE_ID = "$PACKAGE_ID";
    public static final String EF_PACKAGE_VERSION = "$PACKAGE_VERSION";
    public static final String EF_PACKAGE_INDEX = "$ALTERATION_INDEX";
    public static final String EF_PACKAGE_VERSION_REQUIRED = "$PACKAGE_VERSION_REQUIRED";

    public static final String EF_ALTERATION_ID = "$ALTERATION_ID";
    public static final String EF_ALTERATION = "$ALTERATION";
    public static final String EF_ALTERATION_LIST = "$ALTERATION_LIST";
    public static final String EF_ALTERATION_IMPACT = "$ALTERATION_IMPACT";

    public static final String EF_STEP = "$STEP";
    public static final String EF_STEP_OFFSET = "$START_STEP";
    public static final String EF_STEP_RANGE = "$END_STEP";
    public static final String EF_APPLIED_STEP = "$APPLIED_STEP";
    public static final String EF_SIZE = "$SIZE";
    public static final String EF_METHOD = "$METHOD";

    public static final String EF_SOURCE_ID = "$SOURCE_ID";
    public static final String EF_SOURCE_IID = "$SOURCE_IID";

    public static final String EF_DISPLAY_NAME = "$DISPLAY_NAME";
    public static final String EF_TYPE = "$TYPE";
    public static final String EF_DESCRIPTION = "$DESCRIPTION";
    public static final String EF_ENTRY_IID = "$ENTRY_IID";
    public static final String EF_DR_ID = "$DR_ID";
    public static final String EF_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String EF_CLASS_ID = "$CLASS_ID";
    public static final String EF_ATTRIBUTE_ID = "$ATTRIBUTE_ID";
    public static final String EF_VALUE = "$VALUE";
    public static final String EF_VALUE_ID = "$VALUE_ID";
    public static final String EF_ORG_ID = "$ORG_ID";
    public static final String EF_FIELD_ID = "$FIELD_ID";
    public static final String EF_LOWER_BOUND_VALUE = "$LOWER_BOUND_VALUE";
    public static final String EF_UPPER_BOUND_VALUE = "$UPPER_BOUND_VALUE";
    public static final String EF_CONCEPT_ID = "$CONCEPT_ID";
    public static final String EF_CONCEPT_TYPE_ID = "$CONCEPT_TYPE_ID";
    public static final String EF_UOM_ID = "$UOM_ID";
    public static final String EF_QOM_ID = "$QOM_ID";
    public static final String EF_ODS_ID = "$ODS_ID";
    public static final String EF_IRDI = "$IRDI";
    public static final String EF_ABBREVIATION_ID = "$ABBREVIATION_ID";
    public static final String EF_ABBREVIATION = "$ABBREVIATION";
    public static final String EF_DEFINITION_ID = "$DEFINITION_ID";
    public static final String EF_DEFINITION = "$DEFINITION";
    public static final String EF_COMMENT_ID = "$COMMENT_ID";
    public static final String EF_COMMENT = "$COMMENT";
    public static final String EF_CODE_ID = "$CODE_ID";
    public static final String EF_CODE = "$CODE";
    public static final String EF_CODE_TYPE_ID = "$CODE_TYPE_ID";
    public static final String EF_TERM_ID = "$TERM_ID";
    public static final String EF_TERM = "$TERM";
    public static final String EF_ODT_ID = "$ODT_ID";
    public static final String EF_PARENT_ODT_ID = "$PARENT_ODT_ID";
    public static final String EF_SECTION_ID = "$SECTION_ID";
    public static final String EF_LANGUAGE_ID = "$LANGUAGE_ID";
    public static final String EF_PROPERTY_ID = "$PROPERTY_ID";
    public static final String EF_CHARACTERISTIC = "$CHARACTERISTIC";
    public static final String EF_SEQUENCE = "$SEQUENCE";
    public static final String EF_PROPERTY_SEQUENCE = "$PROPERTY_SEQUENCE";
    public static final String EF_ACTIVE_INDICATOR = "$ACTIVE_INDICATOR";
    public static final String EF_STANDARD_INDICATOR = "$STANDARD_INDICATOR";
    public static final String EF_INDEPENDENT_INDICATOR = "$INDEPENDENT_INDICATOR";
    public static final String EF_DATA_TYPE_ID = "$DATA_TYPE_ID";
    public static final String EF_DATA_TYPE_SEQUENCE = "$DATA_TYPE_SEQUENCE";
    public static final String EF_PARENT_DATA_TYPE_ID = "$PARENT_DATA_TYPE_ID";
    public static final String EF_PARENT_DATA_TYPE_SEQ = "$PARENT_DATA_TYPE_SEQ";
    public static final String EF_ATTRIBUTES = "$ATTRIBUTES";

    public static final String EF_INSERTED_BY_ID = "$INSERTED_BY_ID";
    public static final String EF_INSERTED_BY_IID = "$INSERTED_BY_IID";
    public static final String EF_INSERTED_AT = "$INSERTED_AT";
    public static final String EF_UPDATED_BY_ID = "$UPDATED_BY_ID";
    public static final String EF_UPDATED_BY_IID = "$UPDATED_BY_IID";
    public static final String EF_UPDATED_AT = "$UPDATED_AT";

    // Data source and Data entity identifiers.
    public static final String ALT_PACKAGE_DS_IDENTIFIER = "@DS_ALT_PACKAGE";
    public static final String ALT_PACKAGE_DE_IDENTIFIER = "@E_ALT_PACKAGE";
    public static final String ALT_PACKAGE_INDEX_DS_IDENTIFIER = "@DS_ALT_PACKAGE_INDEX";
    public static final String ALT_PACKAGE_INDEX_DE_IDENTIFIER = "@E_ALT_PACKAGE_INDEX";
    public static final String ALT_DR_DOC_DS_IDENTIFIER = "@DS_ALT_DR_DOC";
    public static final String ALT_DR_DOC_DE_IDENTIFIER = "@E_ALT_DR_DOC";
    public static final String ALT_DR_DOC_ATR_DS_IDENTIFIER = "@DS_ALT_DR_DOC_ATR";
    public static final String ALT_DR_DOC_ATR_DE_IDENTIFIER = "@E_ALT_DR_DOC_ATR";
    public static final String ALT_DR_PROPERTY_DS_IDENTIFIER = "@DS_ALT_DR_PROPERTY";
    public static final String ALT_DR_PROPERTY_DE_IDENTIFIER = "@E_ALT_DR_PROPERTY";
    public static final String ALT_DR_PROPERTY_ATR_DS_IDENTIFIER = "@DS_ALT_DR_PROPERTY_ATR";
    public static final String ALT_DR_PROPERTY_ATR_DE_IDENTIFIER = "@E_ALT_DR_PROPERTY_ATR";
    public static final String ALT_DR_DATA_TYPE_DS_IDENTIFIER = "@DS_ALT_DR_DATA_TYPE";
    public static final String ALT_DR_DATA_TYPE_DE_IDENTIFIER = "@E_ALT_DR_DATA_TYPE";
    public static final String ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER = "@DS_ALT_DR_DATA_TYPE_ATR";
    public static final String ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER = "@E_ALT_DR_DATA_TYPE_ATR";
    public static final String ALT_DR_VALUE_DS_IDENTIFIER = "@DS_ALT_DR_VALUE";
    public static final String ALT_DR_VALUE_DE_IDENTIFIER = "@E_ALT_DR_VALUE";
    public static final String ALT_DR_INSTANCE_DS_IDENTIFIER = "@DS_ALT_DR_INSTANCE";
    public static final String ALT_DR_INSTANCE_DE_IDENTIFIER = "@E_ALT_DR_INSTANCE";
    public static final String ALT_ORG_ONTOLOGY_DS_IDENTIFIER = "@DS_ALT_ORG_ONTOLOGY";
    public static final String ALT_ORG_ONTOLOGY_DE_IDENTIFIER = "@E_ALT_ORG_ONTOLOGY";
    public static final String ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_CONCEPT";
    public static final String ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_CONCEPT";
    public static final String ALT_ONTOLOGY_TERM_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_TERM";
    public static final String ALT_ONTOLOGY_TERM_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_TERM";
    public static final String ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_ABBREVIATION";
    public static final String ALT_ONTOLOGY_ABBREVIATION_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_ABBREVIATION";
    public static final String ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_DEFINITION";
    public static final String ALT_ONTOLOGY_DEFINITION_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_DEFINITION";
    public static final String ALT_ONTOLOGY_CODE_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_CODE";
    public static final String ALT_ONTOLOGY_CODE_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_CODE";
    public static final String ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_COMMENT";
    public static final String ALT_ONTOLOGY_COMMENT_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_COMMENT";
    public static final String ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_STRUCTURE";
    public static final String ALT_ONTOLOGY_STRUCTURE_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_STRUCTURE";
    public static final String ALT_ONTOLOGY_CLASS_DS_IDENTIFIER = "@DS_ALT_ONTOLOGY_CLASS";
    public static final String ALT_ONTOLOGY_CLASS_DE_IDENTIFIER = "@E_ALT_ONTOLOGY_CLASS";

    // Table Names.
    public static final String ALT_PACKAGE = "ALT_PKG";
    public static final String ALT_PACKAGE_INDEX = "ALT_PKG_IND";
    public static final String ALT_DR_DOC = "ALT_DAT_REQ";
    public static final String ALT_DR_DOC_ATR = "ALT_DAT_REQ_ATR";
    public static final String ALT_DR_PROPERTY = "ALT_DAT_REQ_PRP";
    public static final String ALT_DR_PROPERTY_ATR = "ALT_DAT_REQ_PRP_ATR";
    public static final String ALT_DR_DATA_TYPE = "ALT_DAT_REQ_TYP";
    public static final String ALT_DR_DATA_TYPE_ATR = "ALT_DAT_REQ_TYP_ATR";
    public static final String ALT_DR_VALUE = "ALT_DAT_REQ_VAL";
    public static final String ALT_DR_INSTANCE = "ALT_DAT_REQ_INS";
    public static final String ALT_ORG_ONTOLOGY = "ALT_ORG_ONT";
    public static final String ALT_ONTOLOGY_CONCEPT = "ALT_ONT_CPT";
    public static final String ALT_ONTOLOGY_TERM = "ALT_ONT_TRM";
    public static final String ALT_ONTOLOGY_ABBREVIATION = "ALT_ONT_ABR";
    public static final String ALT_ONTOLOGY_DEFINITION = "ALT_ONT_DEF";
    public static final String ALT_ONTOLOGY_CODE = "ALT_ONT_CDE";
    public static final String ALT_ONTOLOGY_COMMENT = "ALT_ONT_CMT";
    public static final String ALT_ONTOLOGY_STRUCTURE = "ALT_ONT_STR";
    public static final String ALT_ONTOLOGY_CLASS = "ALT_ONT_CLS";


    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());

        return definitions;
    }

    private List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_APPLY_ALTERATION);
        definition.setMetaDisplayName("Apply Alteration");
        definition.setMetaDescription("Apply the specified alteration in a persisted package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$ApplyAlterationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION, "Alteration", "The new alteration to apply.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION_IMPACT, "Alteration Impact", "The alteration impact on the existing database.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_INSERT_ALTERATION);
        definition.setMetaDisplayName("Insert Alteration");
        definition.setMetaDescription("Inserts the specified alteration in a persisted package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$InsertAlterationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION, "Alteration", "The new alteration to insert.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_INDEX, "Package Index", "The updated alteration package index.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_RETRIEVE_ALTERATION);
        definition.setMetaDisplayName("Retrieve Alteration");
        definition.setMetaDescription("Retrieve the data alteration for the specified package and step.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$RetrieveAlterationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_IID, "Alteration Package Instance Id", "The instance id of the alteration package from which to retrieve the alterations.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_STEP, "Step", "The spesific step in the package for which the alteration should be retrieved.", T8DataType.INTEGER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION, "Alteration", "The spesific alteration for the given package instance Id and step.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_DELETE_ALTERATION);
        definition.setMetaDisplayName("Delete Alteration");
        definition.setMetaDescription("Deletes the specified alteration in a persisted package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$DeleteAlterationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_IID, "Package IID", "The package instance id of the package that the alteration should be deleted from.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_STEP, "Step", "The step of the alteration in the index.", T8DataType.INTEGER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_INDEX, "Package Index", "The updated alteration package index.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_UPDATE_ALTERATION);
        definition.setMetaDisplayName("Update Alteration");
        definition.setMetaDescription("Updates the specified alteration in a persisted package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$UpdateAlterationOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION, "Alteration", "The alteration that needs to be updated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_INSERT_ALTERATION_PACKAGE);
        definition.setMetaDisplayName("Insert Alteration Package");
        definition.setMetaDescription("Inserts the new alteration package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$InsertAlterationPackageOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION_PACKAGE, "Alteration Package", "The alteration package that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_UPDATE_ALTERATION_PACKAGE);
        definition.setMetaDisplayName("Update Alteration Package");
        definition.setMetaDescription("Updates the alteration package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$UpdateAlterationPackageOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION_PACKAGE, "Alteration Package", "The alteration package that needs to be updated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_DELETE_ALTERATION_PACKAGE);
        definition.setMetaDisplayName("Delete Alteration Package");
        definition.setMetaDescription("Deletes the alteration package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$DeleteAlterationPackageOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_IID, "Alteration Package Instance id", "The alteration package instance id that needs to be deleted.", T8DataType.GUID));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_RETRIEVE_ALTERATION_PACKAGE);
        definition.setMetaDisplayName("Retrieve Alteration Package");
        definition.setMetaDescription("Retrieves the specified alteration package.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$RetrieveAlterationPackageOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_IID, "Package Instance Id", "The alteration package Iid that needs to be retrieved.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ALTERATION_PACKAGE, "Alteration Package", "The alteration package for the given packageIid.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_ALT_GET_ALTERATION_PACKAGE_INDEX);
        definition.setMetaDisplayName("Get Alteration Package Index");
        definition.setMetaDescription("Gets the specified alteration package index.");
        definition.setClassName("com.pilog.t8.api.T8DataAlterationApiOperations$GetAlterationPackageIndexOperation");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_IID, "Package Instance Id", "The alteration package Iid that needs to be retrieved.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PACKAGE_INDEX, "Alteration Package Index", "The alteration package Index for the given packageIid.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        return definitions;
    }

    private List<T8TableDataSourceResourceDefinition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceResourceDefinition tableDataSourceDefinition;
        List<T8TableDataSourceResourceDefinition> definitions;
        String connectionIdentifier;

        definitions = new ArrayList<>();

        connectionIdentifier = serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId();
        if (!Strings.isNullOrEmpty(connectionIdentifier))
        {
            // ALT_PACKAGE Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_PACKAGE_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_PACKAGE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_ID, "PACKAGE_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_VERSION, "PACKAGE_VERSION", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_VERSION_REQUIRED, "PACKAGE_VERSION_REQUIRED", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SOURCE_ID, "SOURCE_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SOURCE_IID, "SOURCE_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DISPLAY_NAME, "DISPLAY_NAME", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DESCRIPTION, "DESCRIPTION", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_APPLIED_STEP, "APPLIED_STEP", false, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_PACKAGE_INDEX Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_PACKAGE_INDEX_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_PACKAGE_INDEX);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ENTRY_IID, "ENTRY_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ALTERATION_ID, "ALTERATION_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", false, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SIZE, "SIZE", false, true, T8DataType.INTEGER));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_DOC Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_DOC_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_DOC);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CLASS_ID, "CLASS_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_PACKAGE_INDEX Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_DOC_ATR_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_DOC_ATR);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTRIBUTE_ID, "ATTRIBUTE_ID", true, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TYPE, "TYPE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_PROPERTY Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_PROPERTY_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_PROPERTY);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SECTION_ID, "SECTION_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHARACTERISTIC, "CHARACTERISTIC", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_SEQUENCE, "PROPERTY_SEQUENCE", false, false, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_PROPERTY_ATR Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_PROPERTY_ATR_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_PROPERTY_ATR);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTRIBUTE_ID, "ATTRIBUTE_ID", true, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TYPE, "TYPE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_DATA_TYPE Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_DATA_TYPE_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_DATA_TYPE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", false, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SECTION_ID, "SECTION_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", true, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, "DATA_TYPE_SEQUENCE", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_DATA_TYPE_ID, "PARENT_DATA_TYPE_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_DATA_TYPE_SEQ, "PARENT_DATA_TYPE_SEQ", false, false, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTRIBUTES, "ATTRIBUTES", false, false, T8DataType.STRING));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_DATA_TYPE_ATR Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_DATA_TYPE_ATR);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", true, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, "DATA_TYPE_SEQUENCE", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTRIBUTE_ID, "ATTRIBUTE_ID", true, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TYPE, "TYPE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_VALUE Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_VALUE_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_VALUE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_ID, "VALUE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LOWER_BOUND_VALUE, "LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPPER_BOUND_VALUE, "UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UOM_ID, "UOM_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_QOM_ID, "QOM_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_DR_INSTANCE Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_DR_INSTANCE_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_DR_INSTANCE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INDEPENDENT_INDICATOR, "INDEPENDENT_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ORG_ONTOLOGY Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ORG_ONTOLOGY_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ORG_ONTOLOGY);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_CONCEPT Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_CONCEPT);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_TERM Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_TERM_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_TERM);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM, "TERM", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_ABBREVIATION Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_ABBREVIATION);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION_ID, "ABBREVIATION_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION, "ABBREVIATION", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_DEFINITION Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_DEFINITION);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION_ID, "DEFINITION_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_CODE Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_CODE_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_CODE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_ID, "CODE_ID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_TYPE_ID, "CODE_TYPE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE, "CODE", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_COMMENT Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_COMMENT);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_COMMENT_ID, "COMMENT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_COMMENT, "COMMENT", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_STRUCTURE Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_STRUCTURE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

            // ALT_ONTOLOGY_CLASS Data Source.
            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ALT_ONTOLOGY_CLASS_DS_IDENTIFIER);
            tableDataSourceDefinition.setConnectionIdentifier(connectionIdentifier);
            tableDataSourceDefinition.setTableName(ALT_ONTOLOGY_CLASS);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PACKAGE_IID, "PACKAGE_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STEP, "STEP", true, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_METHOD, "METHOD", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_ODT_ID, "PARENT_ODT_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SEQUENCE, "SEQUENCE", false, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.setAutoGenerate(false);
            definitions.add(tableDataSourceDefinition);

        } else logger.log(T8Logger.Level.WARNING, "No connection identifier specified. Data Alteration META tables will not be available.");

        return definitions;
    }

    private List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        if (!Strings.isNullOrEmpty(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId()))
        {
            // ALT_PACKAGE Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_PACKAGE_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_PACKAGE_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_PACKAGE_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_ID, ALT_PACKAGE_DS_IDENTIFIER + EF_PACKAGE_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_VERSION, ALT_PACKAGE_DS_IDENTIFIER + EF_PACKAGE_VERSION, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_VERSION_REQUIRED, ALT_PACKAGE_DS_IDENTIFIER + EF_PACKAGE_VERSION_REQUIRED, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SOURCE_ID, ALT_PACKAGE_DS_IDENTIFIER + EF_SOURCE_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SOURCE_IID, ALT_PACKAGE_DS_IDENTIFIER + EF_SOURCE_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DISPLAY_NAME, ALT_PACKAGE_DS_IDENTIFIER + EF_DISPLAY_NAME, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DESCRIPTION, ALT_PACKAGE_DS_IDENTIFIER + EF_DESCRIPTION, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_APPLIED_STEP, ALT_PACKAGE_DS_IDENTIFIER + EF_APPLIED_STEP, false, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_PACKAGE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_PACKAGE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_PACKAGE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_PACKAGE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_PACKAGE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_PACKAGE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_PACKAGE_INDEX Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_PACKAGE_INDEX_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_PACKAGE_INDEX_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ENTRY_IID, ALT_PACKAGE_INDEX_DS_IDENTIFIER + EF_ENTRY_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_PACKAGE_INDEX_DS_IDENTIFIER + EF_PACKAGE_IID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ALTERATION_ID, ALT_PACKAGE_INDEX_DS_IDENTIFIER + EF_ALTERATION_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_PACKAGE_INDEX_DS_IDENTIFIER + EF_STEP, false, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SIZE, ALT_PACKAGE_INDEX_DS_IDENTIFIER + EF_SIZE, false, true, T8DataType.INTEGER));
            definitions.add(entityDefinition);

            // ALT_DR_DOC Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_DOC_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_DOC_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_DOC_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_DOC_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_DOC_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_DOC_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CLASS_ID, ALT_DR_DOC_DS_IDENTIFIER + EF_CLASS_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_DR_DOC_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_DR_DOC_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_DR_DOC_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_DR_DOC_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_DR_DOC_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_DR_DOC_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_DR_DOC_ATR Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_DOC_ATR_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_DOC_ATR_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTRIBUTE_ID, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_ATTRIBUTE_ID, true, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TYPE, ALT_DR_DOC_ATR_DS_IDENTIFIER + EF_TYPE, false, false, T8DataType.STRING));
            definitions.add(entityDefinition);

            // ALT_DR_PROPERTY Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_PROPERTY_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_PROPERTY_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SECTION_ID, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_SECTION_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHARACTERISTIC, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_CHARACTERISTIC, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_SEQUENCE, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_PROPERTY_SEQUENCE, false, false, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_DR_PROPERTY_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            definitions.add(entityDefinition);

            // ALT_DR_PROPERTY_ATR Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_PROPERTY_ATR_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTRIBUTE_ID, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_ATTRIBUTE_ID, true, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TYPE, ALT_DR_PROPERTY_ATR_DS_IDENTIFIER + EF_TYPE, false, false, T8DataType.STRING));
            definitions.add(entityDefinition);

            // ALT_DR_DATA_TYPE Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_DATA_TYPE_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_DATA_TYPE_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SECTION_ID, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_SECTION_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_DATA_TYPE_ID, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_PARENT_DATA_TYPE_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_DATA_TYPE_SEQ, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_PARENT_DATA_TYPE_SEQ, false, false, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTRIBUTES, ALT_DR_DATA_TYPE_DS_IDENTIFIER + EF_ATTRIBUTES, false, false, T8DataType.STRING));
            definitions.add(entityDefinition);

            // ALT_DR_DATA_TYPE_ATR Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTRIBUTE_ID, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_ATTRIBUTE_ID, true, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TYPE, ALT_DR_DATA_TYPE_ATR_DS_IDENTIFIER + EF_TYPE, false, false, T8DataType.STRING));
            definitions.add(entityDefinition);

            // ALT_DR_VALUE Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_VALUE_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_VALUE_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_VALUE_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_VALUE_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_VALUE_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_VALUE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_ORG_ID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_DR_INSTANCE_ID, true, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FIELD_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_FIELD_ID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_DATA_TYPE_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, ALT_DR_VALUE_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LOWER_BOUND_VALUE, ALT_DR_VALUE_DS_IDENTIFIER + EF_LOWER_BOUND_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPPER_BOUND_VALUE, ALT_DR_VALUE_DS_IDENTIFIER + EF_UPPER_BOUND_VALUE, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_CONCEPT_ID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UOM_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_UOM_ID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_QOM_ID, ALT_DR_VALUE_DS_IDENTIFIER + EF_QOM_ID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_DR_VALUE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
            definitions.add(entityDefinition);

            // ALT_DR_INSTANCE Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_DR_INSTANCE_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_DR_INSTANCE_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INDEPENDENT_INDICATOR, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_INDEPENDENT_INDICATOR, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_DR_INSTANCE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ORG_ONTOLOGY Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ORG_ONTOLOGY_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ORG_ONTOLOGY_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_ORG_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_ODS_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_ODT_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_CONCEPT_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ORG_ONTOLOGY_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_CONCEPT Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_CONCEPT_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_TYPE_ID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_CONCEPT_TYPE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_TERM Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_TERM_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_TERM_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_TERM_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_TERM, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_TERM_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_ABBREVIATION Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_ABBREVIATION_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION_ID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_ABBREVIATION_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_TERM_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_ABBREVIATION, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_DEFINITION Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_DEFINITION_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION_ID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_DEFINITION_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_DEFINITION, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_CODE Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_CODE_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_CODE_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_ID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_CODE_ID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_TYPE_ID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_CODE_TYPE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_CODE, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_CODE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_COMMENT Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_COMMENT_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_COMMENT_ID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_COMMENT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_COMMENT, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_COMMENT, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_COMMENT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_STRUCTURE Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_STRUCTURE_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_ODS_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);

            // ALT_ONTOLOGY_CLASS Data Entity.
            entityDefinition = new T8DataEntityResourceDefinition(ALT_ONTOLOGY_CLASS_DE_IDENTIFIER);
            entityDefinition.setDataSourceIdentifier(ALT_ONTOLOGY_CLASS_DS_IDENTIFIER);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PACKAGE_IID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_PACKAGE_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STEP, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_STEP, true, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_METHOD, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_METHOD, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_ODS_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_ODT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_ODT_ID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_PARENT_ODT_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SEQUENCE, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_SEQUENCE, false, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ALT_ONTOLOGY_CLASS_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
            definitions.add(entityDefinition);
        }

        return definitions;
    }
}
