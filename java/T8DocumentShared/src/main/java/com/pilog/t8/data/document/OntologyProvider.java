package com.pilog.t8.data.document;

import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface OntologyProvider
{
    /**
     * Returns a list of all ontology codes linked to the specified concept.
     * @param conceptId The concept for which to fetch ontology codes.
     * @return The list of codes belonging to the specified concept.
     */
    public List<T8OntologyCode> getOntologyCodes(String conceptId);

    /**
     * Returns all of the ontology links between the specified concept and the SYSTEM ontology structure (default).
     * @param conceptId The concept Id for which to retrieve the links.
     * @return The list of links retrieved.
     */
    public List<T8OntologyLink> getOntologyLinks(String conceptId);

    /**
     * Returns all of the ontology links between the specified concept and the list of ontology structures.
     * @param osIdList The list of ontology structures to include.  If null, all structures will be included.
     * @param conceptId The concept Id for which to retrieve the links.
     * @return The list of links retrieved.
     */
    public List<T8OntologyLink> getOntologyLinks(List<String> osIdList, String conceptId);

    /**
     * Retrieves the complete ontology of the specified concept, including terms, definitions, codes, abbreviations and ontology links.
     * @param conceptId The id of the concept to retrieve.
     * @return The complete ontology of the specified concept.
     */
    public T8OntologyConcept getOntologyConcept(String conceptId);

    /**
     * Returns a data iterator over the concepts belonging to the specified ontology class.
     * @param ocId The ontology class to which the iterator will be applicable.
     * @param includeDescendantClasses A boolean flag to indicate whether or not descendant classes of the specified class must be included.
     * @return The data iterator of concept Id's in the specified ontology class.
     */
    public T8DataIterator<String> getConceptIDIterator(String ocId, boolean includeDescendantClasses);
}
