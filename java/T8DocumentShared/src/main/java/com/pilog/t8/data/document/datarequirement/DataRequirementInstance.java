package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.T8HierarchicalSetType;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class DataRequirementInstance implements Requirement, Serializable
{
    private T8OntologyConcept ontology;
    private String conceptId;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;
    private String recordOcId;
    private boolean independent;
    private DataRequirement dataRequirement;
    private DataRequirementInstance parentInstance;
    private final List<T8DataRequirementComment> comments;
    private final List<DataRequirementInstance> subInstances;

    public DataRequirementInstance(String conceptId, DataRequirement dataRequirement)
    {
        this.conceptId = conceptId;
        this.independent = true;
        this.dataRequirement = dataRequirement;
        this.comments = new ArrayList<T8DataRequirementComment>();
        this.subInstances = new ArrayList<DataRequirementInstance>();
        if (dataRequirement != null) dataRequirement.setParentDataRequirementInstance(this);
    }

    void setParentInstance(DataRequirementInstance parentInstance)
    {
        this.parentInstance = parentInstance;
    }

    public DataRequirementInstance getParentInstance()
    {
        return parentInstance;
    }

    /**
     * Returns a copy of this object and its logical descendants.  No references to logical parents are copied.
     * @return A copy of this object and its logical descendants.
     */
    public DataRequirementInstance copy()
    {
        DataRequirementInstance newInstance;

        newInstance = new DataRequirementInstance(conceptId, dataRequirement.copy());
        newInstance.setComments(comments);
        newInstance.setIndependent(independent);
        newInstance.setCode(code);
        newInstance.setTerm(term);
        newInstance.setAbbreviation(abbreviation);
        newInstance.setDefinition(definition);
        newInstance.setOntology(ontology != null ? ontology.copy(true, true, true, true, true) : null);
        return newInstance;
    }

    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.DATA_REQUIREMENT_INSTANCE;
    }

    public String getId()
    {
        return conceptId;
    }

    public void setId(String id)
    {
        this.conceptId = id;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public String getRecordOcId()
    {
        return recordOcId;
    }

    public void setRecordOcId(String recordOcId)
    {
        this.recordOcId = recordOcId;
    }

    public T8OntologyConcept getOntology()
    {
        return ontology;
    }

    public void setOntology(T8OntologyConcept ontology)
    {
        this.ontology = ontology;
    }

    /**
     * Adds the terminology of this Data Requirement Instance and of all its
     * contents to the supplied list.
     * @param terminologyCollector The list to which terminology will be added.
     */
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        // Add this instance's terminology.
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);

        // Add the terminology from the Data Requirement.
        if (dataRequirement != null) dataRequirement.addContentTerminology(terminologyCollector);
    }

    /**
     * Sets the terminology of this Data Requirement Instance and all of its
     * content elements.
     * @param terminologyProvider The terminology provider from which to fetch this
     * Data Requirement Instance's terminology.
     */
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        terminology = terminologyProvider.getTerminology(null, conceptId);
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }

        // Set the terminology of the data requirement.
        if (dataRequirement != null) dataRequirement.setContentTerminology(terminologyProvider);
    }

    public boolean isIndependent()
    {
        return independent;
    }

    public void setIndependent(boolean independent)
    {
        this.independent = independent;
    }

    public DataRequirement getDataRequirement()
    {
        return dataRequirement;
    }

    public void setDataRequirement(DataRequirement dataRequirement)
    {
        dataRequirement.setParentDataRequirementInstance(this);
        this.dataRequirement = dataRequirement;
    }

    public List<DataRequirementInstance> getSubInstances()
    {
        return new ArrayList<DataRequirementInstance>(subInstances);
    }

    public DataRequirementInstance getSubInstance(String subDRInstanceID)
    {
        for (DataRequirementInstance subInstance : subInstances)
        {
            if (subInstance.getConceptID().equals(subDRInstanceID))
            {
                return subInstance;
            }
        }

        return null;
    }

    public Set<DataRequirementInstance> getSubInstanceSet()
    {
        return new HashSet<DataRequirementInstance>(subInstances);
    }

    public Set<DataRequirementInstance> getDescendantInstanceSet()
    {
        Set<DataRequirementInstance> instanceSet;

        instanceSet = new HashSet<>(subInstances);
        for (DataRequirementInstance subInstance : subInstances)
        {
            instanceSet.addAll(subInstance.getSubInstanceSet());
        }

        return instanceSet;
    }

    public DataRequirement findDataRequirementByDrInstance(T8HierarchicalSetType setType, String drInstanceId)
    {
        for (DataRequirementInstance drInstance : getInstanceSet(setType))
        {
            if (drInstance.getConceptID().equals(drInstanceId))
            {
                return drInstance.getDataRequirement();
            }
        }

        return null;
    }

    public DataRequirement findDataRequirement(T8HierarchicalSetType setType, String drId)
    {
        for (DataRequirementInstance drInstance : getInstanceSet(setType))
        {
            if (drInstance.getDataRequirement().getConceptID().equals(drId))
            {
                return drInstance.getDataRequirement();
            }
        }

        return null;
    }

    /**
     * Adds the specified DR Instance to the sub-instance collection of this
     * DR Instance, updating is parent reference to reflect the new hierarchy.
     * @param drInstance The DR Instance to add to this instance as a
     * sub-instance.
     */
    public void addSubInstance(DataRequirementInstance drInstance)
    {
        if (!containsSubInstance(drInstance.getConceptID()))
        {
            this.subInstances.add(drInstance);
            drInstance.setParentInstance(this);
        }
        else throw new IllegalArgumentException("Cannot add DR Instance '" + drInstance + "' as sub-instance since it already exists in sub-instance collection: " + subInstances);
    }

    public boolean containsSubInstance(String drInstanceID)
    {
        for (DataRequirementInstance subInstance : subInstances)
        {
            if (subInstance.getConceptID().equals(drInstanceID)) return true;
        }

        return false;
    }

    public void setSubInstances(List<DataRequirementInstance> subInstances)
    {
        removeAllSubInstances();
        if (subInstances != null)
        {
            for (DataRequirementInstance subInstance : subInstances)
            {
                addSubInstance(subInstance);
            }
        }
    }

    public void removeFromParent()
    {
        if (parentInstance != null)
        {
            parentInstance.removeSubInstance(this);
        }
    }

    public void removeSubInstance(DataRequirementInstance subInstance)
    {
        // Remove the sub-instance from this instance's list of sub-instances.
        if (subInstances.remove(subInstance))
        {
            // If we successfully removed the sub-instance, make sure this instance is no longer listed as its parent.
            subInstance.setParentInstance(null);
        }
    }

    public DataRequirementInstance removeSubInstance(String drInstanceID)
    {
        DataRequirementInstance instanceToRemove;

        instanceToRemove = getSubInstance(drInstanceID);
        if (instanceToRemove != null)
        {
            // Remove the sub-instance from this instance's list of sub-instances.
            if (subInstances.remove(instanceToRemove))
            {
                // If we successfully remove the sub-instance, make sure this instance is no longer listed as its parent.
                instanceToRemove.setParentInstance(null);
                return instanceToRemove;
            }
            else return null;
        }
        else return null;
    }

    public void removeAllSubInstances()
    {
        // First remove sub instances.
        for (DataRequirementInstance subInstance : subInstances)
        {
            subInstance.setParentInstance(null);
        }

        // Clear the collection of sub-instances.
        subInstances.clear();
    }


    /**
     * Returns the specified set of DR Instances from the current object model.
     * @param setType The type of set to retrieve.
     * @return The set of DR Instances requested.
     */
    public Set<DataRequirementInstance> getInstanceSet(T8HierarchicalSetType setType)
    {
        Set<DataRequirementInstance> drInstanceSet;

        drInstanceSet = new HashSet<DataRequirementInstance>();
        switch (setType)
        {
            case LINEAGE:    // From a specific node, all ancestors, all descendants including the node itself.
            {
                DataRequirementInstance currentDrInstance;

                // Add all ancestors and this record itself.
                currentDrInstance = this;
                drInstanceSet.add(currentDrInstance);
                while (currentDrInstance.getParentInstance() != null)
                {
                    currentDrInstance = currentDrInstance.getParentInstance();
                    drInstanceSet.add(currentDrInstance);
                }

                // Add all descendants.
                drInstanceSet.addAll(getDescendantInstanceSet());

                // Return the result.
                return drInstanceSet;
            }
            case ANCESTORS:  // From a specific node, all ancestors, excluding the node itself.
            {
                DataRequirementInstance currentDrInstance;

                currentDrInstance = this;
                while (currentDrInstance.getParentInstance() != null)
                {
                    currentDrInstance = currentDrInstance.getParentInstance();
                    drInstanceSet.add(currentDrInstance);
                }

                return drInstanceSet;
            }
            case LINE:       // From a specific node, all ancestors, including the node itself.
            {
                DataRequirementInstance currentDrInstance;

                currentDrInstance = this;
                drInstanceSet.add(currentDrInstance);
                while (currentDrInstance.getParentInstance() != null)
                {
                    currentDrInstance = currentDrInstance.getParentInstance();
                    drInstanceSet.add(currentDrInstance);
                }

                return drInstanceSet;
            }
            case SINGLE:     // From a specific node, only the specific node itself.
            {
                drInstanceSet.add(this);
                return drInstanceSet;
            }
            case HOUSE:      // From a specific node, all nodes with the same parent, including the node itself and its parent.
            {
                DataRequirementInstance parentDrInstance;

                parentDrInstance = getParentInstance();
                if (parentDrInstance != null)
                {
                    drInstanceSet = parentDrInstance.getSubInstanceSet();
                    drInstanceSet.add(parentDrInstance);
                    return drInstanceSet;
                }
                else return drInstanceSet;
            }
            case SIBLINGS:   // From a specific node, all nodes with the same parent, excluding the node itself.
            {
                DataRequirementInstance parentDrInstance;

                parentDrInstance = getParentInstance();
                if (parentDrInstance != null)
                {
                    drInstanceSet = parentDrInstance.getSubInstanceSet();
                    drInstanceSet.remove(this);
                    return drInstanceSet;
                }
                else return drInstanceSet;
            }
            case BROOD:      // From a specific node, all nodes with the same parent, including the node itself.
            {
                DataRequirementInstance parentDrInstance;

                parentDrInstance = getParentInstance();
                if (parentDrInstance != null)
                {
                    return parentDrInstance.getSubInstanceSet();
                }
                else return drInstanceSet;
            }
            case FAMILY:     // From a specific node, all descendants, including the node itself.
            {
                drInstanceSet.add(this);
                drInstanceSet.addAll(getDescendantInstanceSet());
                return drInstanceSet;
            }
            case DESCENDANTS: // From a specific node, all descendants, excluding the node itself.
            {
                return getDescendantInstanceSet();
            }
            default:
                throw new IllegalArgumentException("Unsupported set type: " + setType);
        }
    }

    public DataRequirement findAncestorDataRequirementByDRInstance(String drInstanceID)
    {
        if (parentInstance != null)
        {
            if (drInstanceID.equals(parentInstance.getConceptID()))
            {
                return parentInstance.getDataRequirement();
            }
            else return parentInstance.findAncestorDataRequirementByDRInstance(drInstanceID);
        }
        else return null;
    }

    public DataRequirement findAncestorDataRequirement(String drID)
    {
        if (parentInstance != null)
        {
            DataRequirement parentDR;

            parentDR = parentInstance.getDataRequirement();
            if (drID.equals(parentDR.getConceptID()))
            {
                return parentDR;
            }
            else return parentInstance.findAncestorDataRequirement(drID);
        }
        else return null;
    }

    public void addComment(T8DataRequirementComment comment)
    {
        comments.add(comment);
    }

    public List<T8DataRequirementComment> getComments()
    {
        return new ArrayList<T8DataRequirementComment>(comments);
    }

    public void setComments(List<T8DataRequirementComment> comments)
    {
        this.comments.clear();
        if (comments != null)
        {
            this.comments.addAll(comments);
        }
    }

    public boolean containsComment(String commentID)
    {
        for (T8DataRequirementComment comment : comments)
        {
            if (commentID.equals(comment.getId()))
            {
                return true;
            }
        }

        return false;
    }

    public int getPropertyCount()
    {
        return dataRequirement.getPropertyCount();
    }

    public int getCharacteristicCount()
    {
        return dataRequirement.getCharacteristicCount();
    }

    public List<T8DataRequirementComment> getPropertyComments(String propertyID)
    {
        List<T8DataRequirementComment> propertyComments;

        propertyComments = new ArrayList<T8DataRequirementComment>();
        for (T8DataRequirementComment comment : comments)
        {
            if (propertyID.equals(comment.getPropertyId()))
            {
                propertyComments.add(comment);
            }
        }

        return propertyComments;
    }

    public List<T8DataRequirementComment> getFieldComments(String propertyID, String fieldID)
    {
        List<T8DataRequirementComment> fieldComments;

        fieldComments = new ArrayList<T8DataRequirementComment>();
        for (T8DataRequirementComment comment : comments)
        {
            if (propertyID.equals(comment.getPropertyId()))
            {
                if (fieldID.equals(comment.getFieldId()))
                {
                    fieldComments.add(comment);
                }
            }
        }

        return fieldComments;
    }

    @Override
    public boolean isEquivalentRequirement(Requirement requirement)
    {
        if (!(requirement instanceof DataRequirementInstance))
        {
            return false;
        }
        else
        {
            DataRequirementInstance dataRequirementInstance;

            dataRequirementInstance = (DataRequirementInstance)requirement;
            if (!Objects.equals(conceptId, dataRequirementInstance.getRequirementType()))
            {
                return false;
            }
            else
            {
                return dataRequirement.isEquivalentRequirement(dataRequirementInstance.getDataRequirement());
            }
        }
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("DataRequirementInstance: [");
        builder.append("ID: ");
        builder.append(getConceptID());
        builder.append("]");
        return builder.toString();
    }
}
