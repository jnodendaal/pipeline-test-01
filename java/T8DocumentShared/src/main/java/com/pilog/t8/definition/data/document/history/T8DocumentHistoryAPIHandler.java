package com.pilog.t8.definition.data.document.history;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.*;

/**
 * @author Hennie Brink
 */
public class T8DocumentHistoryAPIHandler implements T8DefinitionResource
{
    // These should NOT be changed, only added to.
    public static final String OPERATION_GET_ROOT_RECORD_HISTORY_SNAPSHOTS = "@OS_GET_ROOT_RECORD_HISTORY_SNAPSHOTS";

    public static final String PARAMETER_ROOT_RECORD_ID = "$P_ROOT_RECORD_ID";
    public static final String PARAMETER_TRANSACTION_ID_LIST = "$P_TRANSACTION_ID_LIST";
    public static final String PARAMETER_DATA_RECORD_MAP = "$P_DATA_RECORD_MAP";

    public static final String DE_HISTORY_DATA_RECORD = "@E_HISTORY_DATA_RECORD";
    public static final String DS_HISTORY_DATA_RECORD = "@DS_HISTORY_DATA_RECORD";

    // Entity fields defined by the description renderer for entities from which record descriptions are retrieved.
    public static final String EF_EVENT_IID = "$EVENT_IID";
    public static final String EF_TRANSACTION_IID = "$TRANSACTION_IID";
    public static final String EF_RECORD_DATA = "$RECORD_DATA";

    // Table Names.
    private static final String TABLE_HISTORY_DATA_RECORD = "HIS_DAT_REC";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;

            // Data Record Operations.
            definition = new T8JavaServerOperationDefinition(OPERATION_GET_ROOT_RECORD_HISTORY_SNAPSHOTS);
            definition.setMetaDisplayName("Get Root Record History Snapshots");
            definition.setMetaDescription("Creates data records for every transaction of the root record history");
            definition.setClassName("com.pilog.t8.data.document.history.T8DocumentHistoryOperations$GetRootRecordHistorySnapshots");
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ROOT_RECORD_ID, "Root Record ID", "The record ID of the root record for which the history will be retrieved.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TRANSACTION_ID_LIST, "Transaction ID List", "The list of transaction ID's for which history will be retrieved, if no values are given the full history across all transactions will be retrieved", new T8DtList(T8DataType.GUID)));
            definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD_MAP, "Data Record History Map", "A map containing the data record as at was represented at each transaction. The root record as it is currently will be in the map with a null key.", new T8DtMap(T8DataType.GUID, T8DataType.CUSTOM_OBJECT)));
            definitions.add(definition);
        }

         // Add data source definitions if needed.
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))
        {
            T8TableDataSourceDefinition tableDataSourceDefinition;

            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DS_HISTORY_DATA_RECORD);
            tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
            tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_RECORD);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TRANSACTION_IID, "TRANSACTION_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT, "EVENT", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_DATA, "RECORD_DATA", false, true, T8DataType.LONG_STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TIME, "TIME", false, true, T8DataType.TIMESTAMP));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_AGENT_ID, "AGENT_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_AGENT_IID, "AGENT_IID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_USER_ID, "USER_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SESSION_ID, "SESSION_ID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OPERATION_ID, "OPERATION_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OPERATION_IID, "OPERATION_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FLOW_ID, "FLOW_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TASK_ID, "TASK_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TASK_IID, "TASK_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FUNCTIONALITY_ID, "FUNCTIONALITY_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FUNCTIONALITY_IID, "FUNCTIONALITY_IID", false, false, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROCESS_ID, "PROCESS_ID", false, false, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROCESS_IID, "PROCESS_IID", false, false, T8DataType.GUID));
            definitions.add(tableDataSourceDefinition);
        }

        // Added entity definitions if needed.
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = new T8DataEntityResourceDefinition(DE_HISTORY_DATA_RECORD);
            entityDefinition.setDataSourceIdentifier(DS_HISTORY_DATA_RECORD);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT_IID, DS_HISTORY_DATA_RECORD + EF_EVENT_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TRANSACTION_IID, DS_HISTORY_DATA_RECORD + EF_TRANSACTION_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, DS_HISTORY_DATA_RECORD + EF_ROOT_RECORD_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DS_HISTORY_DATA_RECORD + EF_RECORD_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DS_HISTORY_DATA_RECORD + EF_DR_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DS_HISTORY_DATA_RECORD + EF_DR_INSTANCE_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT, DS_HISTORY_DATA_RECORD + EF_EVENT, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TIME, DS_HISTORY_DATA_RECORD + EF_TIME, false, true, T8DataType.TIMESTAMP));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_AGENT_ID, DS_HISTORY_DATA_RECORD + EF_AGENT_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_AGENT_IID, DS_HISTORY_DATA_RECORD + EF_AGENT_IID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_DATA, DS_HISTORY_DATA_RECORD + EF_RECORD_DATA, false, true, T8DataType.LONG_STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_ID, DS_HISTORY_DATA_RECORD + EF_USER_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SESSION_ID, DS_HISTORY_DATA_RECORD + EF_SESSION_ID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OPERATION_ID, DS_HISTORY_DATA_RECORD + EF_OPERATION_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OPERATION_IID, DS_HISTORY_DATA_RECORD + EF_OPERATION_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FLOW_ID, DS_HISTORY_DATA_RECORD + EF_FLOW_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FLOW_IID, DS_HISTORY_DATA_RECORD + EF_FLOW_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TASK_ID, DS_HISTORY_DATA_RECORD + EF_TASK_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TASK_IID, DS_HISTORY_DATA_RECORD + EF_TASK_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FUNCTIONALITY_ID, DS_HISTORY_DATA_RECORD + EF_FUNCTIONALITY_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FUNCTIONALITY_IID, DS_HISTORY_DATA_RECORD + EF_FUNCTIONALITY_IID, false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROCESS_ID, DS_HISTORY_DATA_RECORD + EF_PROCESS_ID, false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROCESS_IID, DS_HISTORY_DATA_RECORD + EF_PROCESS_IID, false, false, T8DataType.GUID));
            definitions.add(entityDefinition);
        }

        return definitions;
    }
}
