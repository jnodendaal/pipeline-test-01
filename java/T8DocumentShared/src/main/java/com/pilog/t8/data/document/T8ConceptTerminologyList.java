package com.pilog.t8.data.document;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptTerminologyList extends ArrayList<T8ConceptTerminology> implements TerminologyProvider, TerminologyCollector, Serializable
{
    private String languageId; // Default language to use when none is specified.

    public T8ConceptTerminologyList()
    {
    }

    public T8ConceptTerminologyList(Collection<T8ConceptTerminology> terminologyCollection)
    {
        super(terminologyCollection);
    }

    @Override
    public void setLanguage(String languageId)
    {
        this.languageId = languageId;
    }

    public void setTerminology(List<T8ConceptTerminology> terminologyList)
    {
        clear();
        addAll(terminologyList);
    }

    public void addTerminology(List<T8ConceptTerminology> terminologyList)
    {
        addAll(terminologyList);
    }

    public void addTerminology(T8ConceptTerminology conceptTerminology)
    {
        add(conceptTerminology);
    }

    @Override
    public void addTerminology(String languageId, T8OntologyConceptType conceptType, String conceptId, String codeId, String code, String termId, String term, String abbreviationId, String abbreviation, String definitionId, String definition)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageId != null ? languageId : this.languageId, conceptId);
        if (terminology == null)
        {
            terminology = new T8ConceptTerminology(conceptType, conceptId);
            terminology.setLanguageId(languageId != null ? languageId : this.languageId);
        }

        if (codeId != null) terminology.setCodeId(codeId);
        if (code != null) terminology.setCode(code);
        if (termId != null) terminology.setTermId(termId);
        if (term != null) terminology.setTerm(term);
        if (abbreviationId != null) terminology.setAbbreviationId(abbreviationId);
        if (abbreviation != null) terminology.setAbbreviation(abbreviation);
        if (definitionId != null) terminology.setDefinitionId(definitionId);
        if (definition != null) terminology.setDefinition(definition);
        addTerminology(terminology);
    }

    @Override
    public String getTerm(String languageId, String conceptId)
    {
        for (T8ConceptTerminology terminology : this)
        {
            if (terminology.getConceptId().equals(conceptId))
            {
                if ((languageId == null) || (terminology.getLanguageId().equals(languageId)))
                {
                    return terminology.getTerm();
                }
            }
        }

        return null;
    }

    @Override
    public String getCode(String conceptID)
    {
        for (T8ConceptTerminology terminology : this)
        {
            if (terminology.getConceptId().equals(conceptID))
            {
                return terminology.getCode();
            }
        }

        return null;
    }

    @Override
    public String getAbbreviation(String languageID, String conceptID)
    {
        for (T8ConceptTerminology terminology : this)
        {
            if (terminology.getConceptId().equals(conceptID))
            {
                if ((languageID == null) || (terminology.getLanguageId().equals(languageID)))
                {
                    return terminology.getAbbreviation();
                }
            }
        }

        return null;
    }

    @Override
    public String getDefinition(String languageID, String conceptID)
    {
        for (T8ConceptTerminology terminology : this)
        {
            if (terminology.getConceptId().equals(conceptID))
            {
                if ((languageID == null) || (terminology.getLanguageId().equals(languageID)))
                {
                    return terminology.getDefinition();
                }
            }
        }

        return null;
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageId, String conceptId)
    {
        for (T8ConceptTerminology terminology : this)
        {
            if (terminology.getConceptId().equals(conceptId))
            {
                if ((languageId == null) || (terminology.getLanguageId().equals(languageId)))
                {
                    return terminology;
                }
            }
        }

        return null;
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIDList, Collection<String> conceptIDList)
    {
        return null;
    }

    /**
     * Checks whether or not a concept terminology entry exists for the specified concept
     * in the specified language.
     * @param languageId The language for which to check.
     * @param conceptId The concept for which to check.
     * @return Boolean true if the specified entry exists, else null.
     */
    public boolean containsTerminology(String languageId, String conceptId)
    {
        for (T8ConceptTerminology conceptTerminology : this)
        {
            if (conceptTerminology.getConceptId().equals(conceptId))
            {
                if (conceptTerminology.getLanguageId().equals(languageId))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns all terminology for the specified concept.
     * @param conceptID The concept ID to search for.
     * @return The list of terminologies for the specified concept.
     */
    public T8ConceptTerminologyList getTerminology(String conceptID)
    {
        T8ConceptTerminologyList terminologyList;

        terminologyList = new T8ConceptTerminologyList();
        for (T8ConceptTerminology conceptTerminology : this)
        {
            if (conceptTerminology.getConceptId().equals(conceptID))
            {
                terminologyList.add(conceptTerminology);
            }
        }

        return terminologyList;
    }

    /**
     * Returns the list of terminologies matching the specified criteria.
     * @param conceptType The concept type to search for (ignored if null).
     * @param term The term to search for.  Cannot be null.
     * @return The list of terminologies matching the specified criteria.
     */
    public T8ConceptTerminologyList getTerminologyByTerm(T8OntologyConceptType conceptType, String term)
    {
        T8ConceptTerminologyList terminologyList;

        terminologyList = new T8ConceptTerminologyList();
        for (T8ConceptTerminology conceptTerminology : this)
        {
            if ((conceptType == null) || (conceptType == conceptTerminology.getConceptType()))
            {
                if (term.equals(conceptTerminology.getTerm()))
                {
                    terminologyList.add(conceptTerminology);
                }
            }
        }

        return terminologyList;
    }

    /**
     * Returns the list of terminologies matching the specified criteria.
     * @param conceptType The concept type to search for (ignored if null).
     * @param code The code to search for.  Cannot be null.
     * @return The list of terminologies matching the specified criteria.
     */
    public T8ConceptTerminologyList getTerminologyByCode(T8OntologyConceptType conceptType, String code)
    {
        T8ConceptTerminologyList terminologyList;

        terminologyList = new T8ConceptTerminologyList();
        for (T8ConceptTerminology conceptTerminology : this)
        {
            if ((conceptType == null) || (conceptType == conceptTerminology.getConceptType()))
            {
                if (code.equals(conceptTerminology.getCode()))
                {
                    terminologyList.add(conceptTerminology);
                }
            }
        }

        return terminologyList;
    }

    /**
     * Returns the first concept ID from the list of terminologies, that has the
     * specified concept type and term combination.
     * @param conceptType The concept type to search for (ignored if null).
     * @param term The term to search for.  Cannot be null.
     * @return The first concept ID matching the specified criteria.
     */
    public String getConceptIDByTerm(T8OntologyConceptType conceptType, String term)
    {
        for (T8ConceptTerminology conceptTerminology : this)
        {
            if ((conceptType == null) || (conceptType == conceptTerminology.getConceptType()))
            {
                if (term.equals(conceptTerminology.getTerm()))
                {
                    return conceptTerminology.getConceptId();
                }
            }
        }

        return null;
    }

    /**
     * Returns the first concept ID from the list of terminologies, that has the
     * specified concept type and code combination.
     * @param conceptType The concept type to search for (ignored if null).
     * @param code The code to search for.  Cannot be null.
     * @return The first concept ID matching the specified criteria.
     */
    public String getConceptIDByCode(T8OntologyConceptType conceptType, String code)
    {
        for (T8ConceptTerminology conceptTerminology : this)
        {
            if ((conceptType == null) || (conceptType == conceptTerminology.getConceptType()))
            {
                if (code.equals(conceptTerminology.getCode()))
                {
                    return conceptTerminology.getConceptId();
                }
            }
        }

        return null;
    }
}
