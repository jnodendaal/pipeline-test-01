package com.pilog.t8.data.document.datarecord.match;

import java.io.Serializable;

/**
 * This class represents the link between two data records that have been
 * matched using some arbitrary methodology.
 *
 * @author Bouwer du Preez
 */
public class T8RecordMatch implements Serializable
{
    private final String instanceId;
    private final String criteriaId;
    private final String caseId;
    private final String familyId;
    private final String familyCode;
    private final String recordId1;
    private final String recordId2;

    public T8RecordMatch(String instanceId, String criteriaId, String caseId, String familyId, String familyCode, String recordId1, String recordId2)
    {
        this.familyId = familyId;
        this.familyCode = familyCode;
        this.recordId1 = recordId1;
        this.recordId2 = recordId2;
        this.instanceId = instanceId;
        this.criteriaId = criteriaId;
        this.caseId = caseId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    public String getCriteriaId()
    {
        return criteriaId;
    }

    public String getCaseId()
    {
        return caseId;
    }

    public String getFamilyId()
    {
        return familyId;
    }

    public String getFamilyCode()
    {
        return familyCode;
    }

    public String getRecordId1()
    {
        return recordId1;
    }

    public String getRecordId2()
    {
        return recordId2;
    }
}
