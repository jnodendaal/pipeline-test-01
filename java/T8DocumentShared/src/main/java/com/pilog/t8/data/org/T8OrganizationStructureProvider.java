package com.pilog.t8.data.org;

/**
 * @author Bouwer du Preez
 */
public interface T8OrganizationStructureProvider
{
    public T8OrganizationStructure getOrganizationStructure();
}
