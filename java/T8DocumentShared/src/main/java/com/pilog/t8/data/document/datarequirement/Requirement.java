package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public interface Requirement
{
    /**
     * Returns the ID of the Requirement.
     * @return The ID of the requirement.
     */
    public RequirementType getRequirementType();
    
    /**
     * Returns a Boolean value indicating whether or not the supplied 
     * requirement and its descendant structure is equivalent to this 
     * requirement and its descendant structure.
     * @param requirement The requirement to compare to this requirement.
     * @return A Boolean value indicating equivalence if true.
     */
    public boolean isEquivalentRequirement(Requirement requirement);
}
