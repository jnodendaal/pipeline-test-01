package com.pilog.t8.data.document.datarecord.match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8RecordMatchCase implements Serializable
{
    private String id;
    private String name;
    private String description;
    private T8RecordMatchCaseType type;
    private String dataExpression;
    private T8RecordMatchCriteria matchCriteria;
    private final List<T8RecordMatchCheck> checks;

    public enum T8RecordMatchCaseType
    {
        HASH, // Compares the Hash Codes generated from the checks in the case.
        VALUE // Compares the individual values generated from the checks in the case.
    };

    public T8RecordMatchCase(String id)
    {
        this.id = id;
        this.type = T8RecordMatchCaseType.VALUE;
        this.checks = new ArrayList<>();
    }

    void setParentMatchCriteria(T8RecordMatchCriteria criteria)
    {
        this.matchCriteria = criteria;
    }

    public T8RecordMatchCriteria getMatchCriteria()
    {
        return matchCriteria;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDataExpression()
    {
        return dataExpression;
    }

    public void setDataExpression(String dataExpression)
    {
        this.dataExpression = dataExpression;
    }

    public int getCheckIndex(T8RecordMatchCheck check)
    {
        return checks.indexOf(check);
    }

    public int incrementCheckIndex(T8RecordMatchCheck matchCheck)
    {
        int index;

        index = checks.indexOf(matchCheck);
        if (index > -1)
        {
            if (index < (checks.size() - 1))
            {
                int newIndex;

                newIndex = index + 1;
                checks.remove(matchCheck);
                checks.add(newIndex, matchCheck);
                return newIndex;
            }
            else return index;
        }
        else return -1;
    }

    public int decrementCheckIndex(T8RecordMatchCheck matchCheck)
    {
        int index;

        index = checks.indexOf(matchCheck);
        if (index > -1)
        {
            if (index > 0)
            {
                int newIndex;

                newIndex = index - 1;
                checks.remove(matchCheck);
                checks.add(newIndex, matchCheck);
                return newIndex;
            }
            else return 0;
        }
        else return -1;
    }

    public T8RecordMatchCheck getCheck(String checkID)
    {
        for (T8RecordMatchCheck check : checks)
        {
            if (check.getId().equals(checkID))
            {
                return check;
            }
        }

        return null;
    }

    public int getIndex()
    {
        return matchCriteria != null ? matchCriteria.getCaseIndex(this) : -1;
    }

    public int incrementIndex()
    {
        if (matchCriteria != null)
        {
            return matchCriteria.incrementCaseIndex(this);
        }
        else return -1;
    }

    public int decrementIndex()
    {
        if (matchCriteria != null)
        {
            return matchCriteria.decrementCaseIndex(this);
        }
        else return -1;
    }

    public T8RecordMatchCaseType getType()
    {
        return type;
    }

    public void setType(T8RecordMatchCaseType type)
    {
        this.type = type;
    }

    public void setType(String typeId)
    {
        this.type = T8RecordMatchCaseType.valueOf(typeId);
    }

    public void addCheck(T8RecordMatchCheck check)
    {
        checks.add(check);
        check.setParentMatchCase(this);
    }

    public boolean removeCheck(T8RecordMatchCheck check)
    {
        if (checks.remove(check))
        {
            check.setParentMatchCase(null);
            return true;
        }
        else return false;
    }

    public List<T8RecordMatchCheck> getChecks()
    {
        return new ArrayList<T8RecordMatchCheck>(checks);
    }
}
