package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class AttributeStep extends DefaultPathStep implements PathStep
{
    public AttributeStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getAttributes());
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateStep(((DataRequirement)input).getAttributes());
        }
        else if (input instanceof PropertyRequirement)
        {
            return evaluateStep(((PropertyRequirement)input).getAttributes());
        }
        else if (input instanceof ValueRequirement)
        {
            return evaluateStep(((ValueRequirement)input).getAttributes());
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Attribute step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<Object> evaluateStep(Map<String, Object> attributes)
    {
        // If we have valid ID's to use for narrowing of the value list do it now.
        if (idList != null)
        {
            Iterator<String> attributeIdIterator;

            attributeIdIterator = attributes.keySet().iterator();
            while (attributeIdIterator.hasNext())
            {
                String attributeID;

                attributeID = attributeIdIterator.next();
                if (!idList.contains(attributeID))
                {
                    attributeIdIterator.remove();
                }
            }
        }

        // Now run through the remaining value list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<String> attributeIdIterator;

            // Iterator over the requirements and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            attributeIdIterator = attributes.keySet().iterator();
            while (attributeIdIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                String attributeId;

                // Create a map containing the input parameters available to the predicate expression.
                attributeId = attributeIdIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("ID", attributeId);
                inputParameters.put("VALUE", attributes.get(attributeId));

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        attributeIdIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the attribute values.
        return new ArrayList(attributes.values());
    }
}
