package com.pilog.t8.data.document.datarecord.access;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAccessChangedEvent extends EventObject
{
    private final T8DataRecordAccessHandler accessHandler;
    private final DataRecord dataRecord;
    
    public T8DataRecordAccessChangedEvent(T8DataRecordAccessHandler accessHandler, DataRecord dataRecord)
    {
        super(accessHandler);
        this.accessHandler = accessHandler;
        this.dataRecord = dataRecord;
    }

    public T8DataRecordAccessHandler getAccessHandler()
    {
        return accessHandler;
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }
}
