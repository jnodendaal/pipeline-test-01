package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class StringValueContent extends ValueContent
{
    private String string;

    public StringValueContent()
    {
        super(RequirementType.STRING_TYPE);
    }

    public String getString()
    {
        return string;
    }

    public void setString(String string)
    {
        this.string = string;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof StringValueContent)
        {
            StringValueContent toStringContent;

            toStringContent = (StringValueContent)toContent;
            return Objects.equals(string, toStringContent.getString());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof StringValueContent)
        {
            StringValueContent toStringContent;

            toStringContent = (StringValueContent)toContent;
            return Objects.equals(string, toStringContent.getString());
        }
        else return false;
    }
}
