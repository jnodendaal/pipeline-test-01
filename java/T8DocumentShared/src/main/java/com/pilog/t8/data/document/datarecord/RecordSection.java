package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyAddedEvent;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class RecordSection implements Value, Serializable
{
    private DataRecord parentDataRecord;
    private final ArrayList<RecordProperty> recordProperties;
    private SectionRequirement sectionRequirement;

    public RecordSection(SectionRequirement sectionRequirement)
    {
        this.parentDataRecord = null;
        this.sectionRequirement = sectionRequirement;
        this.recordProperties = new ArrayList<RecordProperty>();
    }

    void setParent(DataRecord dataRecord)
    {
        this.parentDataRecord = dataRecord;
    }

    public int getIndex()
    {
        if (parentDataRecord != null)
        {
            return parentDataRecord.getRecordSectionIndex(this);
        }
        else return -1;
    }

    public DataRecord getParentDataRecord()
    {
        return parentDataRecord;
    }

    public SectionRequirement getSectionRequirement()
    {
        return sectionRequirement;
    }

    public String getSectionID()
    {
        return sectionRequirement.getConceptID();
    }

    public String getId()
    {
        return sectionRequirement.getConceptID();
    }

    public int getRecordPropertyIndex(RecordProperty recordProperty)
    {
        if (recordProperties.contains(recordProperty)) return 0; // Only one property with the same Concept ID is allowed, therefore its index is always 0.
        else return -1;
    }

    public RecordProperty getOrAddRecordProperty(String propertyId)
    {
        RecordProperty property;

        property = getRecordProperty(propertyId);
        if (property != null)
        {
            return property;
        }
        else
        {
            PropertyRequirement propertyRequirement;

            propertyRequirement = sectionRequirement.getPropertyRequirement(propertyId);
            if (propertyRequirement != null)
            {
                RecordProperty newProperty;

                newProperty = new RecordProperty(propertyRequirement);
                addRecordProperty(newProperty);
                return newProperty;
            }
            else throw new IllegalArgumentException("Property not found in Section '" + sectionRequirement + "': " + propertyId);
        }
    }

    public RecordProperty getRecordProperty(String propertyId)
    {
        for (RecordProperty recordProperty : recordProperties)
        {
            PropertyRequirement propertyRequirement;

            propertyRequirement = recordProperty.getPropertyRequirement();
            if (propertyRequirement.getConceptID().equals(propertyId))
            {
                return recordProperty;
            }
        }

        return null;
    }

    public ArrayList<RecordProperty> getRecordProperties()
    {
        return new ArrayList<RecordProperty>(recordProperties);
    }

    public void addRecordProperty(RecordProperty newProperty)
    {
        PropertyRequirement newPropertyRequirement;
        String newPropertyID;

        // Make sure the property does not already exist in the section.
        newPropertyRequirement = newProperty.getPropertyRequirement();
        newPropertyID = newPropertyRequirement.getConceptID();
        if (getRecordProperty(newPropertyID) == null)
        {
            int newPropertyIndex;

            // Get the index of the new property requirement.
            newPropertyIndex = sectionRequirement.getPropertyRequirementIndex(newPropertyRequirement);
            if (newPropertyIndex > -1)
            {
                int insertionIndex;

                // Iterate over existing properties and try to find the index at which the new property must be inserted (if any).
                insertionIndex = -1;
                for (RecordProperty existingProperty : recordProperties)
                {
                    int existingPropertyIndex;

                    // Get the existing property's index (according to the section requirement) and compare it to the new property's index
                    // to see if the new property must be inserted before the existing property.
                    existingPropertyIndex = sectionRequirement.getPropertyRequirementIndex(existingProperty.getPropertyRequirement());
                    if (existingPropertyIndex > newPropertyIndex)
                    {
                        // Ok so now we know that the existing property's index in the section requirement is larger than the new property's,
                        // but the actual insertion index is the index of the existing property in the recordProperties collection.
                        insertionIndex = recordProperties.indexOf(existingProperty);
                        break;
                    }
                }

                // If we did not find an insertion index, use simple addition to the end of the collection.
                if (insertionIndex == -1) recordProperties.add(newProperty);
                else recordProperties.add(insertionIndex, newProperty);

                // Set the new property's parent reference.
                newProperty.setParent(this);

                // Signal the event.
                fireRecordPropertyAddedEvent(newProperty, null);
            }
            else throw new IllegalArgumentException("Attempt to add property '" + newProperty + "' for which no corresponding requirement could be found in section: " + sectionRequirement + " DR: " + sectionRequirement.getParentDataRequirement());
        }
        else throw new RuntimeException("Attempt to add duplicate property '" + newPropertyID + "' to section: " + sectionRequirement);
    }

    public boolean removeRecordProperty(String propertyID)
    {
        RecordProperty recordProperty;

        recordProperty = getRecordProperty(propertyID);
        if (recordProperty != null)
        {
            return removeRecordProperty(recordProperty);
        }
        else return false;
    }

    public boolean containsRecordProperty(RecordProperty recordProperty)
    {
        return recordProperties.contains(recordProperty);
    }

    public boolean removeRecordProperty(RecordProperty recordProperty)
    {
        if (recordProperties.remove(recordProperty))
        {
            recordProperty.setParent(null);
            return true;
        }
        else return false;
    }

    public void removeAllRecordProperties()
    {
        // Clear the parent reference on all properties.
        for (RecordProperty recordProperty : recordProperties)
        {
            recordProperty.setParent(null);
        }

        // Now clear all properties from the collection.
        recordProperties.clear();
    }

    private void fireRecordPropertyAddedEvent(RecordProperty newProperty, Object actor)
    {
        DataRecord parentRecord;

        // Only fire the event if this section belongs to a parent data record.
        parentRecord = getParentDataRecord();
        if (parentRecord != null)
        {
            T8RecordPropertyAddedEvent event;

            // Fire an event signalling a property was added on this section.
            event = new T8RecordPropertyAddedEvent(this, newProperty, actor);
            parentRecord.fireDataRecordChangeEventPropagated(event, actor);
        }
    }

    /**
     * Fills this record section, ensuring that all properties specified by the section requirement
     * exist as {@code RecordPropert} objects in this section.
     * This method also fills each of the properties in the section (those that contain fields).
     */
    public void fill()
    {
        for (PropertyRequirement propertyRequirement : sectionRequirement.getPropertyRequirements())
        {
            RecordProperty recordProperty;

            recordProperty = getOrAddRecordProperty(propertyRequirement.getConceptID());
            recordProperty.fill();
        }
    }

    @Override
    public void normalize()
    {
        List<RecordProperty> propertiesToRemove;

        propertiesToRemove = new ArrayList<RecordProperty>();
        for (RecordProperty recordProperty : recordProperties)
        {
            // Normalize the property first.
            recordProperty.normalize();

            // If the property has no content, add it to the list to be removed.
            if (!recordProperty.hasContent(true)) propertiesToRemove.add(recordProperty);
        }

        // Now remove all properties identified as redundant.
        for (RecordProperty propertyToRemove : propertiesToRemove)
        {
            removeRecordProperty(propertyToRemove);
        }
    }

    @Override
    public boolean hasContent(boolean includeFFT)
    {
        // Check each property for content.
        for (RecordProperty recordProperty : recordProperties)
        {
            if (recordProperty.hasContent(includeFFT)) return true;
        }

        // No content found.
        return false;
    }

    @Override
    public boolean isEquivalentValue(Value comparedValue)
    {
        if (!(comparedValue instanceof RecordProperty))
        {
            return false;
        }
        else
        {
            RecordSection recordSection;

            recordSection = (RecordSection)comparedValue;
            if (sectionRequirement.isEquivalentRequirement(recordSection.getSectionRequirement()))
            {
                List<RecordProperty> propertyList;

                propertyList = recordSection.getRecordProperties();
                if (recordProperties.size() == propertyList.size())
                {
                    // Check each of the properties in this value against those in the supplied value.
                    for (int propertyIndex = 0; propertyIndex < recordProperties.size(); propertyIndex++)
                    {
                        if (!recordProperties.get(propertyIndex).isEquivalentValue(propertyList.get(propertyIndex)))
                        {
                            return false;
                        }
                    }

                    // No problems found, so return true;
                    return true;
                }
                else return false;
            }
            else return false;
        }
    }

    @Override
    public Value getSubValue(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        for (RecordProperty recordProperty : recordProperties)
        {
            if ((requirementConceptID == null) || (recordProperty.getPropertyID().equals(requirementConceptID)))
            {
                if ((valueIndex == null) || (recordProperty.getIndex() == valueIndex))
                {
                    return recordProperty;
                }
            }
        }

        return null;
    }

    @Override
    public List<Value> getSubValues(RequirementType typeID, String conceptID, Integer index)
    {
        ArrayList<Value> values;

        values = new ArrayList<Value>();
        for (RecordProperty recordProperty : recordProperties)
        {
            if ((conceptID == null) || (recordProperty.getPropertyID().equals(conceptID)))
            {
                if ((index == null) || (recordProperty.getIndex() == index))
                {
                    values.add(recordProperty);
                }
            }
        }

        return values;
    }

    @Override
    public Value getDescendantValue(RequirementType typeID, String conceptID, Integer index)
    {
        for (RecordProperty recordProperty : recordProperties)
        {
            Value descendantValue;

            if ((conceptID == null) || (recordProperty.getPropertyID().equals(conceptID)))
            {
                if ((index == null) || (recordProperty.getIndex() == index))
                {
                    return recordProperty;
                }
            }

            descendantValue = recordProperty.getDescendantValue(typeID, conceptID, index);
            if (descendantValue != null) return descendantValue;
        }

        return null;
    }

    @Override
    public List<Value> getDescendantValues(RequirementType requirementType, String conceptID, Integer index)
    {
        ArrayList<Value> values;

        values = new ArrayList<Value>();
        for (RecordProperty recordProperty : recordProperties)
        {
            if ((requirementType == null) || (requirementType == RequirementType.PROPERTY))
            {
                if ((conceptID == null) || (recordProperty.getPropertyID().equals(conceptID)))
                {
                    if ((index == null) || (recordProperty.getIndex() == index))
                    {
                        values.add(recordProperty);
                    }
                }
            }

            values.addAll(recordProperty.getDescendantValues(requirementType, conceptID, index));
        }

        return values;
    }

    @Override
    public void setSubValue(Value subValue)
    {
        if (subValue instanceof RecordProperty)
        {
            RecordProperty propertyToRemove;
            RecordProperty newProperty;

            // Find the property to replace (if any).
            newProperty = (RecordProperty)subValue;
            propertyToRemove = getRecordProperty(newProperty.getPropertyID());
            if (propertyToRemove != null)
            {
                // Remove the existing property.
                removeRecordProperty(propertyToRemove);
            }

            // Add the new property.
            addRecordProperty(newProperty);
        }
        else throw new IllegalArgumentException("Cannot add value to to Record Section: " + subValue);
    }

    @Override
    public boolean removeSubValue(Value subValue)
    {
        if (subValue instanceof RecordProperty)
        {
            return removeRecordProperty((RecordProperty)subValue);
        }
        else throw new IllegalArgumentException("Incompatible value type: " + subValue);
    }

    @Override
    public int getSubValueCount()
    {
        return recordProperties.size();
    }

    public void setRequirement(SectionRequirement requirement)
    {
        this.sectionRequirement = requirement;
        for (RecordProperty property : recordProperties)
        {
            property.setRequirement(sectionRequirement.getPropertyRequirement(property.getId()));
        }
    }

    public RecordSection copy()
    {
        RecordSection copy;

        copy = new RecordSection(sectionRequirement);
        for (RecordProperty property : recordProperties)
        {
            copy.addRecordProperty(property.copy());
        }

        return copy;
    }

    public RecordSection copy(SectionRequirement newRequirement)
    {
        RecordSection copy;

        copy = new RecordSection(newRequirement);
        for (RecordProperty property : recordProperties)
        {
            PropertyRequirement newPropertyRequirement;

            newPropertyRequirement = newRequirement.getPropertyRequirement(property.getId());
            copy.addRecordProperty(property.copy(newPropertyRequirement));
        }

        return copy;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("RecordSection: [");
        builder.append("ID: ");
        builder.append(getSectionID());
        builder.append(",SectionRequirement: ");
        builder.append(getSectionRequirement());
        builder.append(",RecordProperties: ");
        builder.append(getRecordProperties());
        builder.append("]");
        return builder.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getContentHashCode(boolean includeFFT)
    {
        int hash;

        hash = 5;
        for (RecordProperty recordProperty : recordProperties)
        {
            hash = 53 * hash + recordProperty.getContentHashCode(includeFFT);
        }

        return hash;
    }

    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        for (RecordProperty recordProperty : recordProperties)
        {
            recordProperty.setContentTerminology(terminologyProvider);
        }
    }

    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        for (RecordProperty recordProperty : recordProperties)
        {
            recordProperty.addContentTerminology(terminologyCollector);
        }
    }
}
