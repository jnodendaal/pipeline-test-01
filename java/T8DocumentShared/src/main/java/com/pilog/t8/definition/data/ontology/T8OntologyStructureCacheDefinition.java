package com.pilog.t8.definition.data.ontology;

import com.pilog.t8.data.T8DataCache;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.cache.T8DataCacheDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyStructureCacheDefinition extends T8DataCacheDefinition
{
// -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_CACHE_ONTOLOGY_DATA_STRUCTURE";
    public static final String DISPLAY_NAME = "Ontology Structure Cache";
    public static final String DESCRIPTION = "A definition of a data cache used to store Ontology Structures.";
    public enum Datum
    {
        ENABLED,
        CACHE_SIZE
    };
    // -------- Definition Meta-Data -------- //

    public static final String DATA_IDENTIFIER = "@CACHE_ONTOLOGY_STRUCTURE";
    public static final String DATA_PARAMETER_ODS_ID = "$P_ODS_ID";

    public T8OntologyStructureCacheDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ENABLED.toString(), "Enabled", "A boolean setting to enabled/disable this cache.  If disabled, all data contained will not be available to requesting clients."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.CACHE_SIZE.toString(), "Cache Size", "The number of Ontology Data Structures to cache.  A value of 0 will indicate that all available structures must be cached."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public T8DataCache createNewDataCacheInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.ontology.T8OntologyStructureCache").getConstructor(T8Context.class, T8OntologyStructureCacheDefinition.class);
        return (T8DataCache)constructor.newInstance(context, this);
    }

    public boolean isEnabled()
    {
        Boolean value;

        value = getDefinitionDatum(Datum.ENABLED);
        return value == null || value;
    }

    public void setEnabled(boolean enabled)
    {
        setDefinitionDatum(Datum.ENABLED, enabled);
    }

    public int getCacheSize()
    {
        Integer value;

        value = (Integer)getDefinitionDatum(Datum.CACHE_SIZE.toString());
        return value != null ? value : 0;
    }

    public void setCacheSize(int size)
    {
        setDefinitionDatum(Datum.CACHE_SIZE.toString(), size);
    }
}
