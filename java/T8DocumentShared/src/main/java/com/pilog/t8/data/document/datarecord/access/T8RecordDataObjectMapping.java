package com.pilog.t8.data.document.datarecord.access;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8RecordDataObjectMapping implements Serializable
{
    private final String recordID;
    private final String dataObjectIdentifier;
    private final String stateIdentifier;
    
    public T8RecordDataObjectMapping(String recordID, String dataObjectIdentifier, String stateIdentifier)
    {
        this.recordID = recordID;
        this.dataObjectIdentifier = dataObjectIdentifier;
        this.stateIdentifier = stateIdentifier;
    }

    public String getRecordID()
    {
        return recordID;
    }

    public String getDataObjectIdentifier()
    {
        return dataObjectIdentifier;
    }

    public String getStateIdentifier()
    {
        return stateIdentifier;
    }
}
