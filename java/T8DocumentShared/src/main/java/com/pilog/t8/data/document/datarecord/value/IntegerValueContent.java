package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class IntegerValueContent extends ValueContent
{
    private String value;

    public IntegerValueContent()
    {
        super(RequirementType.INTEGER_TYPE);
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof IntegerValueContent)
        {
            IntegerValueContent toIntegerContent;

            toIntegerContent = (IntegerValueContent)toContent;
            return Objects.equals(value, toIntegerContent.getValue());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof IntegerValueContent)
        {
            IntegerValueContent toIntegerContent;

            toIntegerContent = (IntegerValueContent)toContent;
            return Objects.equals(value, toIntegerContent.getValue());
        }
        else return false;
    }
}
