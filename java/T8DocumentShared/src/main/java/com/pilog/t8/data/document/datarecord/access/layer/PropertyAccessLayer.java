package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.org.T8OrganizationStructure.OrganizationSetType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class PropertyAccessLayer implements Serializable
{
    private final String propertyId;
    private boolean editable;
    private boolean visible;
    private boolean required;
    private boolean insertEnabled;
    private boolean updateEnabled;
    private boolean deleteEnabled;
    private boolean fftEditable;
    private String layoutIdentifier;
    private Set<String> filterConceptIDSet;
    private Set<String> filterConceptGroupIDSet;
    private T8PropertyLayout layout;
    private SectionAccessLayer parentAccessLayer;
    private final Map<String, FieldAccessLayer> fieldAccessLayers;
    private ValueAccessLayer valueAccessLayer;
    private OrganizationSetType filterOrganizationSetType;
    private final Map<Integer, OrganizationSetType> organizationRestrictions;

    public PropertyAccessLayer(String propertyId)
    {
        this.propertyId = propertyId;
        this.editable = true;
        this.visible = true;
        this.required = false;
        this.fftEditable = true;
        this.insertEnabled = true;
        this.updateEnabled = true;
        this.deleteEnabled = true;
        this.layoutIdentifier = null;
        this.fieldAccessLayers = new HashMap<String, FieldAccessLayer>();
        this.filterOrganizationSetType = T8OrganizationStructure.OrganizationSetType.LINEAGE;
        this.organizationRestrictions = new HashMap<Integer, OrganizationSetType>();
    }

    void setParentAccessLayer(SectionAccessLayer sectionAccessLayer)
    {
        this.parentAccessLayer = sectionAccessLayer;
    }

    public SectionAccessLayer getParentAccessLayer()
    {
        return parentAccessLayer;
    }

    public DataRecordAccessLayer getParentRecordAccessLayer()
    {
        return parentAccessLayer != null ? parentAccessLayer.getParentAccessLayer() : null;
    }

    public DataRecord getParentDataRecord()
    {
        return parentAccessLayer != null ? parentAccessLayer.getParentDataRecord() : null;
    }

    public PropertyAccessLayer copy()
    {
        PropertyAccessLayer copy;

        copy = new PropertyAccessLayer(propertyId);
        copy.setEditable(editable);
        copy.setVisible(visible);
        copy.setRequired(required);
        copy.setFFTEditable(fftEditable);
        copy.setInsertEnabled(insertEnabled);
        copy.setUpdateEnabled(updateEnabled);
        copy.setDeleteEnabled(deleteEnabled);
        copy.setFilterOrganizationSetType(filterOrganizationSetType);
        copy.setFilterConceptIDSet(filterConceptIDSet);
        copy.setFilterConceptGroupIDSet(filterConceptGroupIDSet);

        for (FieldAccessLayer fieldAccessLayer : fieldAccessLayers.values())
        {
            copy.addFieldAccessLayer(fieldAccessLayer.copy());
        }

        if (valueAccessLayer != null)
        {
            copy.setValueAccessLayer(valueAccessLayer.copy());
        }

        return copy;
    }

    public void setAccess(PropertyAccessLayer newAccess)
    {
        setEditable(newAccess.isEditable());
        setVisible(newAccess.isVisible());
        setRequired(newAccess.isRequired());
        setFFTEditable(newAccess.isFftEditable());
        setInsertEnabled(newAccess.isInsertEnabled());
        setUpdateEnabled(newAccess.isUpdateEnabled());
        setDeleteEnabled(newAccess.isDeleteEnabled());
        setFilterOrganizationSetType(newAccess.getFilterOrganizationSetType());
        setFilterConceptIDSet(newAccess.getFilterConceptIDSet());
        setFilterConceptGroupIDSet(newAccess.getFilterConceptGroupIDSet());

        for (FieldAccessLayer fieldAccessLayer : fieldAccessLayers.values())
        {
            FieldAccessLayer newFieldAccess;

            newFieldAccess = newAccess.getFieldAccessLayer(fieldAccessLayer.getFieldID());
            if (newFieldAccess != null)
            {
                fieldAccessLayer.setAccess(newFieldAccess);
            }
        }

        if (valueAccessLayer != null)
        {
            ValueAccessLayer newValueAccess;

            newValueAccess = newAccess.getValueAccessLayer();
            if (newValueAccess != null)
            {
                valueAccessLayer.setAccess(newValueAccess);
            }
        }
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public String getLayoutIdentifier()
    {
        return layoutIdentifier;
    }

    public void setLayoutIdentifier(String layoutIdentifier)
    {
        this.layoutIdentifier = layoutIdentifier;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setPathEditable()
    {
        this.editable = true;
        parentAccessLayer.setPathEditable();
    }

    public void setEditable(boolean editable)
    {
        // Set the editability of this access layer.
        this.editable = editable;

        // Propagate the editability to parent access layers.
        if (editable) parentAccessLayer.setPathEditable();

        // Set the editability of all the fields.
        for (FieldAccessLayer fieldAccessLayer : fieldAccessLayers.values())
        {
            fieldAccessLayer.setEditable(editable);
        }
    }

    public boolean isFftEditable()
    {
        return fftEditable;
    }

    public void setFFTEditable(boolean fftEditable)
    {
        this.fftEditable = fftEditable;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setPathVisible()
    {
        this.visible = true;
        parentAccessLayer.setPathVisible();
    }

    public void setVisible(boolean visible)
    {
        // Set the visibility of this access layer.
        this.visible = visible;

        // Propagate the visibility to parent access layers.
        if (visible) parentAccessLayer.setPathVisible();

        // Set the visibility of all the fields.
        for (FieldAccessLayer fieldAccessLayer : fieldAccessLayers.values())
        {
            fieldAccessLayer.setVisible(visible);
        }
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public void setDescendentsRequired(boolean required)
    {
        // Set the required attribute of all the fields
        for (FieldAccessLayer fieldAccessLayer : fieldAccessLayers.values())
        {
            fieldAccessLayer.setRequired(required);
        }
    }

    public boolean isInsertEnabled()
    {
        return insertEnabled;
    }

    public void setInsertEnabled(boolean insertEnabled)
    {
        this.insertEnabled = insertEnabled;
    }

    public boolean isUpdateEnabled()
    {
        return updateEnabled;
    }

    public void setUpdateEnabled(boolean updateEnabled)
    {
        this.updateEnabled = updateEnabled;
    }

    public boolean isDeleteEnabled()
    {
        return deleteEnabled;
    }

    public void setDeleteEnabled(boolean deleteEnabled)
    {
        this.deleteEnabled = deleteEnabled;
    }

    public Set<String> getFilterConceptIDSet()
    {
        return filterConceptIDSet;
    }

    public void setFilterConceptIDSet(Set<String> filterConceptIDSet)
    {
        this.filterConceptIDSet = filterConceptIDSet;
    }

    public Set<String> getFilterConceptGroupIDSet()
    {
        return filterConceptGroupIDSet;
    }

    public void setFilterConceptGroupIDSet(Set<String> filterConceptGroupIDSet)
    {
        this.filterConceptGroupIDSet = filterConceptGroupIDSet;
    }

    public OrganizationSetType getFilterOrganizationSetType()
    {
        return filterOrganizationSetType;
    }

    public void setFilterOrganizationSetType(OrganizationSetType organizationFilterSet)
    {
        this.filterOrganizationSetType = organizationFilterSet;
    }

    public void setFilterOrganizationSetType(String organizationFilterSet)
    {
        this.filterOrganizationSetType = OrganizationSetType.valueOf(organizationFilterSet);
    }

    public Map<Integer, OrganizationSetType> getOrganizationRestrictions()
    {
        return new HashMap<Integer, OrganizationSetType>(organizationRestrictions);
    }

    public OrganizationSetType getOrganizationRestriction(Integer level)
    {
        return organizationRestrictions.get(level);
    }

    public void setOrganizationRestriction(Integer level, OrganizationSetType setType)
    {
        this.organizationRestrictions.put(level, setType);
    }

    public void setOrganizationRestriction(Integer level, String setType)
    {
        this.organizationRestrictions.put(level, OrganizationSetType.valueOf(setType));
    }

    public T8PropertyLayout getLayout()
    {
        return layout;
    }

    public void setLayout(T8PropertyLayout layout)
    {
        this.layout = layout;
    }

    public FieldAccessLayer getFieldAccessLayer(String fieldID)
    {
        return fieldAccessLayers.get(fieldID);
    }

    public List<FieldAccessLayer> getFieldAccessLayers()
    {
        return new ArrayList<FieldAccessLayer>(fieldAccessLayers.values());
    }

    public FieldAccessLayer removeFieldAccessLayer(String fieldID)
    {
        FieldAccessLayer fieldAccessLayer;

        fieldAccessLayer = fieldAccessLayers.get(fieldID);
        if (fieldAccessLayer != null) fieldAccessLayer.setParentAccessLayer(null);
        return fieldAccessLayer;
    }

    public void addFieldAccessLayer(FieldAccessLayer fieldAccessLayer)
    {
        fieldAccessLayer.setParentAccessLayer(this);
        fieldAccessLayers.put(fieldAccessLayer.getFieldID(), fieldAccessLayer);
    }

    public ValueAccessLayer getValueAccessLayer()
    {
        return valueAccessLayer;
    }

    public void setValueAccessLayer(ValueAccessLayer valueAccessLayer)
    {
        // Clear the parent references of the existing value access layer.
        if (this.valueAccessLayer != null)
        {
            this.valueAccessLayer.setParentFieldAccessLayer(null);
            this.valueAccessLayer.setParentPropertyAccessLayer(null);
        }

        // Set the new value access layer and its parent references.
        this.valueAccessLayer = valueAccessLayer;
        if (this.valueAccessLayer != null)
        {
            this.valueAccessLayer.setParentFieldAccessLayer(null);
            this.valueAccessLayer.setParentPropertyAccessLayer(this);
        }
    }

    public List<ValueAccessLayer> getValueAccessLayers()
    {
        if (valueAccessLayer != null)
        {
            List<ValueAccessLayer> valueAccessLayers;

            valueAccessLayers = new ArrayList<ValueAccessLayer>();
            valueAccessLayers.add(valueAccessLayer);
            return valueAccessLayers;
        }
        else
        {
            List<ValueAccessLayer> valueAccessLayers;

            valueAccessLayers = new ArrayList<ValueAccessLayer>();
            for (FieldAccessLayer fieldAccessLayer : fieldAccessLayers.values())
            {
                ValueAccessLayer fieldValueAccessLayer;

                fieldValueAccessLayer = fieldAccessLayer.getValueAccessLayer();
                if (fieldValueAccessLayer != null)
                {
                    valueAccessLayers.add(fieldValueAccessLayer);
                }
            }

            return valueAccessLayers;
        }
    }

    public boolean isPropertyAccessEquivalent(PropertyAccessLayer propertyAccess)
    {
        if (!Objects.equals(this.editable, propertyAccess.isEditable())) return false;
        else if (!Objects.equals(this.visible, propertyAccess.isVisible())) return false;
        else if (!Objects.equals(this.required, propertyAccess.isRequired())) return false;
        else if (!Objects.equals(this.insertEnabled, propertyAccess.isInsertEnabled())) return false;
        else if (!Objects.equals(this.updateEnabled, propertyAccess.isUpdateEnabled())) return false;
        else if (!Objects.equals(this.deleteEnabled, propertyAccess.isDeleteEnabled())) return false;
        else if (!Objects.equals(this.fftEditable, propertyAccess.isFftEditable())) return false;
        else return true;
    }

    @Override
    public String toString()
    {
        return "PropertyAccessLayer{" + "propertyID=" + propertyId + ", editable=" + editable + ", visible=" + visible + ", required=" + required + ", insertEnabled=" + insertEnabled + ", updateEnabled=" + updateEnabled + ", deleteEnabled=" + deleteEnabled + ", layoutIdentifier=" + layoutIdentifier + ", filterConceptIDSet=" + filterConceptIDSet + ", filterConceptGroupIDSet=" + filterConceptGroupIDSet + '}';
    }
}


