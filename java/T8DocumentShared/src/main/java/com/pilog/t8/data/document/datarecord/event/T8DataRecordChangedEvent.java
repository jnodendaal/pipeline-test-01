package com.pilog.t8.data.document.datarecord.event;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordChangedEvent extends EventObject
{
    private boolean adjusting; // A boolean flag to indicate that this event is part of a sequence of changes to the source record.
    
    public T8DataRecordChangedEvent(DataRecord changedRecord)
    {
        super(changedRecord);
        adjusting = false;
    }
    
    public DataRecord getDataRecord()
    {
        return (DataRecord)getSource();
    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    @Override
    public String toString()
    {
        return "T8DataRecordChangedEvent{" + "dataRecord=" + source + '}';
    }
}
