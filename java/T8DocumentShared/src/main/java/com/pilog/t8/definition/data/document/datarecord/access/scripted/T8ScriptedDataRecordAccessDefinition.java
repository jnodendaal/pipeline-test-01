package com.pilog.t8.definition.data.document.datarecord.access.scripted;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.access.scripted.T8ScriptedDataRecordAccessHandler;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptedDataRecordAccessDefinition extends T8Definition implements T8DataRecordAccessDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_ACCESS_SCRIPTED";
    public static final String DISPLAY_NAME = "Scripted Data Record Access";
    public static final String DESCRIPTION = "A definition that specifies access rules and business logic using a script language.";
    public enum Datum
    {
        DATA_FILE_DR_IIDS,
        INCLUDED_ACCESS_IDS,
        INITIAL_STATE_SCRIPT,
        STATIC_STATE_SCRIPT,
        DYNAMIC_STATE_SCRIPT,
        VALIDATION_SCRIPT
    };
    // -------- Definition Meta-Data -------- //

    private List<T8DataRecordAccessScriptDefinition> initialStateScriptDefinitions;
    private List<T8DataRecordAccessScriptDefinition> staticStateScriptDefinitions;
    private List<T8DataRecordAccessScriptDefinition> dynamicStateScriptDefinitions;
    private List<T8DataRecordAccessScriptDefinition> validationScriptDefinitions;

    public T8ScriptedDataRecordAccessDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.DATA_FILE_DR_IIDS.toString(), "Data File Structures",  "The Data File Structures to which this acccess layer is applicable."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.INCLUDED_ACCESS_IDS.toString(), "Included Access",  "The identifiers of access layers to include in this one (sequence is important)."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.INITIAL_STATE_SCRIPT.toString(), "Initial State Script",  "The data access script that is applied to a record when first created."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.STATIC_STATE_SCRIPT.toString(), "Static State Script",  "The data access script that is applied only once to a record when it is loaded from its persisted state."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.DYNAMIC_STATE_SCRIPT.toString(), "Dynamic State Script",  "The data access script that is applied to a record every time its content changes."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.VALIDATION_SCRIPT.toString(), "Validation Script",  "The data access script that is applied to a record to verify its validity."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INCLUDED_ACCESS_IDS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8ScriptedDataRecordAccessDefinition.TYPE_IDENTIFIER));
        else if (Datum.INITIAL_STATE_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordAccessScriptDefinition.GROUP_IDENTIFIER));
        else if (Datum.STATIC_STATE_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordAccessScriptDefinition.GROUP_IDENTIFIER));
        else if (Datum.DYNAMIC_STATE_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordAccessScriptDefinition.GROUP_IDENTIFIER));
        else if (Datum.VALIDATION_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordAccessScriptDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        T8DataRecordAccessScriptDefinition scriptDefinition;
        T8DefinitionManager definitionManager;
        List<String> includedAccessIds;

        // Get the definition manager.
        definitionManager = context.getServerContext().getDefinitionManager();

        // Initialze collections.
        initialStateScriptDefinitions = new ArrayList<>();
        staticStateScriptDefinitions = new ArrayList<>();
        dynamicStateScriptDefinitions = new ArrayList<>();
        validationScriptDefinitions = new ArrayList<>();

        // Load the included definitions.
        includedAccessIds = getIncludedAccessIds();
        if (includedAccessIds != null)
        {
            for (String accessId : includedAccessIds)
            {
                T8ScriptedDataRecordAccessDefinition accessDefinition;

                accessDefinition = (T8ScriptedDataRecordAccessDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), accessId, inputParameters);
                if (accessDefinition != null)
                {
                    // Include initial state logic.
                    scriptDefinition = accessDefinition.getInitialStateScript();
                    if (scriptDefinition != null) initialStateScriptDefinitions.add(scriptDefinition);

                    // Include static state logic.
                    scriptDefinition = accessDefinition.getStaticStateScript();
                    if (scriptDefinition != null) staticStateScriptDefinitions.add(scriptDefinition);

                    // Include dynamic state logic.
                    scriptDefinition = accessDefinition.getDynamicStateScript();
                    if (scriptDefinition != null) dynamicStateScriptDefinitions.add(scriptDefinition);

                    // Include validation logic.
                    scriptDefinition = accessDefinition.getValidationScript();
                    if (scriptDefinition != null) validationScriptDefinitions.add(scriptDefinition);
                }
                else throw new Exception("Included access definition not foud: " + accessId);
            }
        }

        // Lastly, add the scripts from this definition.
        // Include initial state logic.
        scriptDefinition = getInitialStateScript();
        if (scriptDefinition != null) initialStateScriptDefinitions.add(scriptDefinition);

        // Include static state logic.
        scriptDefinition = getStaticStateScript();
        if (scriptDefinition != null) staticStateScriptDefinitions.add(scriptDefinition);

        // Include dynamic state logic.
        scriptDefinition = getDynamicStateScript();
        if (scriptDefinition != null) dynamicStateScriptDefinitions.add(scriptDefinition);

        // Include validation logic.
        scriptDefinition = getValidationScript();
        if (scriptDefinition != null) validationScriptDefinitions.add(scriptDefinition);
    }

    @Override
    public T8DataRecordAccessHandler getNewDataRecordAccessHandlerInstance(T8Context context)
    {
        return new T8ScriptedDataRecordAccessHandler(context, this);
    }

    public List<T8DataRecordAccessScriptDefinition> getIncludedInitialStateScripts()
    {
        return new ArrayList<>(initialStateScriptDefinitions);
    }

    public List<T8DataRecordAccessScriptDefinition> getIncludedStaticStateScripts()
    {
        return new ArrayList<>(staticStateScriptDefinitions);
    }

    public List<T8DataRecordAccessScriptDefinition> getIncludedDynamicStateScripts()
    {
        return new ArrayList<>(dynamicStateScriptDefinitions);
    }

    public List<T8DataRecordAccessScriptDefinition> getIncludedValidationScripts()
    {
        return new ArrayList<>(validationScriptDefinitions);
    }

    public List<String> getFileDrIids()
    {
        return getDefinitionDatum(Datum.DATA_FILE_DR_IIDS);
    }

    public void setFileDrIids(List<String> drIids)
    {
        setDefinitionDatum(Datum.DATA_FILE_DR_IIDS, drIids);
    }

    public List<String> getIncludedAccessIds()
    {
        return (List<String>)getDefinitionDatum(Datum.INCLUDED_ACCESS_IDS);
    }

    public void setIncludedAccessIds(List<String> ids)
    {
        setDefinitionDatum(Datum.INCLUDED_ACCESS_IDS, ids);
    }

    public T8DataRecordAccessScriptDefinition getInitialStateScript()
    {
        return getDefinitionDatum(Datum.INITIAL_STATE_SCRIPT);
    }

    public void setInitialStateScript(T8DataRecordAccessScriptDefinition definition)
    {
        setDefinitionDatum(Datum.INITIAL_STATE_SCRIPT, definition);
    }

    public T8DataRecordAccessScriptDefinition getStaticStateScript()
    {
        return getDefinitionDatum(Datum.STATIC_STATE_SCRIPT);
    }

    public void setStaticStateScript(T8DataRecordAccessScriptDefinition definition)
    {
        setDefinitionDatum(Datum.STATIC_STATE_SCRIPT, definition);
    }

    public T8DataRecordAccessScriptDefinition getDynamicStateScript()
    {
        return getDefinitionDatum(Datum.DYNAMIC_STATE_SCRIPT);
    }

    public void setDynamicStateScript(T8DataRecordAccessScriptDefinition definition)
    {
        setDefinitionDatum(Datum.DYNAMIC_STATE_SCRIPT, definition);
    }

    public T8DataRecordAccessScriptDefinition getValidationScript()
    {
        return getDefinitionDatum(Datum.VALIDATION_SCRIPT);
    }

    public void setValidationScript(T8DataRecordAccessScriptDefinition definition)
    {
        setDefinitionDatum(Datum.VALIDATION_SCRIPT, definition);
    }
}
