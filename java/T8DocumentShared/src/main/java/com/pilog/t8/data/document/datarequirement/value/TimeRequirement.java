package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class TimeRequirement extends ValueRequirement
{
    public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    public TimeRequirement()
    {
        super(RequirementType.TIME_TYPE);
    }

    public String getTimeFormat()
    {
        String format;

        format = (String)this.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.toString());
        return format != null ? format : DEFAULT_TIME_FORMAT;
    }
}
