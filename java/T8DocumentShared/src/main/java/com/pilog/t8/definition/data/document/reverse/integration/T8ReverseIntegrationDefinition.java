package com.pilog.t8.definition.data.document.reverse.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.data.document.integration.DataRecordIntegrationService;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Willem De Bruyn
 */
public class T8ReverseIntegrationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REVERSE_INTEGRATION_SERVICE";
    public static final String GROUP_IDENTIFIER = "@DG_REVERSE_INTEGRATION_SERVICE";
    public static final String STORAGE_PATH = "/reverse_integration_services";
    public static final String DISPLAY_NAME = "Reverse Intergation Service";
    public static final String DESCRIPTION = "A definition that specifies the detail involved in an reverse interface service call.";
    public static final String IDENTIFIER_PREFIX = "XFACE_REV_";
    
    public enum Datum
    {
        ORG_ID,
        DOMAINS
    }
    // -------- Definition Meta-Data -------- //
    
    public T8ReverseIntegrationDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ORG_ID.toString(), "Organization ID", "The interfacing Organization ID."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DOMAINS.toString(), "Domains",  "This will define the domain."));
        return datumTypes;
    }
    
    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8ReverseIntegrationDefinition.Datum.DOMAINS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ReverseIntegrationDomainsDefinition.GROUP_IDENTIFIER));
        else return null;
    }
    
    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }
    
    public <T> DataRecordIntegrationService<T> getNewIntegrationServiceInstance(T8ServerContext serverContex, T8SessionContext sessionContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.document.reverse.integration.T8ReverseIntegrationDefinition").getConstructor(T8ServerContext.class, T8SessionContext.class, T8ReverseIntegrationDefinition.class);
        return (DataRecordIntegrationService<T>)constructor.newInstance(serverContex, sessionContext, this);
    }
    
    public String getOrganizationID()
    {
        return (String)getDefinitionDatum(Datum.ORG_ID.toString());
    }
    
    public void setOrganizationID(String organizationID)
    {
        setDefinitionDatum(Datum.ORG_ID.toString(), organizationID);
    }
    
    public List<T8ReverseIntegrationDomainsDefinition> getDomainsDefinitions()
    {
        return (List<T8ReverseIntegrationDomainsDefinition>) getDefinitionDatum(Datum.DOMAINS.toString());
    }
    
    public void setDomainsDefinition(List<T8ReverseIntegrationDomainsDefinition> definitions)
    {
        setDefinitionDatum(Datum.DOMAINS.toString(), definitions);
    }
}
