package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.math.BigDecimal;

/**
 * @author Bouwer du Preez
 */
public class StringRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public StringRequirement()
    {
        super(RequirementType.STRING_TYPE);
    }

    @Override
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public String getFormatPattern()
    {
        return (String)getAttribute(RequirementAttribute.FORMAT_PATTERN.toString());
    }

    public void setFormatPattern(String formatPattern)
    {
        setAttribute(RequirementAttribute.FORMAT_PATTERN.toString(), formatPattern);
    }

    public String getMask()
    {
        return (String)getAttribute(RequirementAttribute.FORMAT_MASK.toString());
    }

    public void setMask(String mask)
    {
        setAttribute(RequirementAttribute.FORMAT_MASK.toString(), mask);
    }

    public int getMaximumStringLength()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_STRING_LENGTH.toString());
        return value != null ? value.intValue() : -1;
    }
}
