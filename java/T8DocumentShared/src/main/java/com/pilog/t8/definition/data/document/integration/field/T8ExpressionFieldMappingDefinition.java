package com.pilog.t8.definition.data.document.integration.field;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationFieldMapper;
import com.pilog.t8.definition.data.document.integration.T8DataRecordFieldMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8ExpressionFieldMappingDefinition extends T8DataRecordFieldMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_FIELD_EXPRESSION";
    public static final String DISPLAY_NAME = "Expression Field Mapping";
    public static final String DESCRIPTION = "A Data Record Field mapping that defines how properties for this record will be mapped";

    public enum Datum
    {
        DOC_PATH_EXPRESSION,
        EPIC_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public T8ExpressionFieldMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOC_PATH_EXPRESSION.toString(), "Doc Path Expression", "The doc path expression that will be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.EPIC_EXPRESSION.toString(), "Epic Expression", "The EPIC expression that will be evaluated."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <T extends Object> DataRecordIntegrationFieldMapper<T> createNewFieldMapperInstance(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.field.mapping.ExpressionFieldMapping").getConstructor(T8ServerContext.class, T8SessionContext.class, T8ExpressionFieldMappingDefinition.class);
            return (DataRecordIntegrationFieldMapper<T>) constructor.newInstance(serverContext, sessionContext, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new field mapper instance", ex);
            return null;
        }
    }

    public String getDocPathExpression()
    {
        return (String)getDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString());
    }

    public void setDocPathExpression(String propertyID)
    {
        setDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString(), propertyID);
    }

    public String getEpicExpression()
    {
        return (String)getDefinitionDatum(Datum.EPIC_EXPRESSION.toString());
    }

    public void setEpicExpression(String propertyID)
    {
        setDefinitionDatum(Datum.EPIC_EXPRESSION.toString(), propertyID);
    }

}
