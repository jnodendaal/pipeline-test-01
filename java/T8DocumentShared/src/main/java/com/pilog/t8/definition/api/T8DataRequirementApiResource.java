package com.pilog.t8.definition.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_DRQ_RETRIEVE_DRI = "@OP_API_DRQ_RETRIEVE_DRI";
    public static final String OPERATION_API_DRQ_RETRIEVE_DR = "@OP_API_DRQ_RETRIEVE_DR";
    public static final String OPERATION_API_DRQ_INSERT_DRI = "@OP_API_DRQ_INSERT_DRI";
    public static final String OPERATION_API_DRQ_SAVE_DRI = "@OP_API_DRQ_SAVE_DRI";
    public static final String OPERATION_API_DRQ_DELETE_DRI = "@OP_API_DRQ_DELETE_DRI";
    public static final String OPERATION_API_DRQ_SAVE_DR = "@OP_API_DRQ_SAVE_DR";
    public static final String OPERATION_API_DRQ_DELETE_DR = "@OP_API_DRQ_DELETE_DR";
    public static final String OPERATION_API_DRQ_RETRIEVE_DR_COMMENTS = "@OP_API_DRQ_RETRIEVE_DR_COMMENTS";

    public static final String PARAMETER_DR_IID = "$P_DR_IID";
    public static final String PARAMETER_DR_IIDS = "$P_DR_IIDS";
    public static final String PARAMETER_DRI = "$P_DRI";
    public static final String PARAMETER_DR_ID = "$P_DR_ID";
    public static final String PARAMETER_DR_IDS = "$P_DR_IDS";
    public static final String PARAMETER_DR_COMMENTS = "$P_DR_COMMENTS";
    public static final String PARAMETER_DR = "$P_DR";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_LANGUAGE_IDS = "$P_LANGUAGE_IDS";
    public static final String PARAMETER_ORG_IDS = "$P_ORG_IDS";
    public static final String PARAMETER_INDEPENDENT = "$P_INDEPENDENT";
    public static final String PARAMETER_CONCEPT = "$P_CONCEPT";
    public static final String PARAMETER_COMMENT_TYPE_IDS = "$P_COMMENT_TYPE_IDS";
    public static final String PARAMETER_INCLUDE_ONTOLOGY = "$P_INCLUDE_ONTOLOGY";
    public static final String PARAMETER_INCLUDE_TERMINOLOGY = "$P_INCLUDE_TERMINOLOGY";
    public static final String PARAMETER_SAVE_ONTOLOGY = "$P_SAVE_ONTOLOGY";
    public static final String PARAMETER_DELETE_ONTOLOGY = "$P_DELETE_ONTOLOGY";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            T8DefinitionManager definitionManager;
            List<T8Definition> definitions;

            definitionManager = serverContext.getDefinitionManager();
            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_RETRIEVE_DRI);
            definition.setMetaDisplayName("Retrieve Data Requirement Instance");
            definition.setMetaDescription("Retrieves the specified Data Requirement Instance and the required associated data.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiRetrieveDataRequirementInstance");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_IID, "Data Requirement Instance Id", "The id of the Data Requirement Instance to retrieve.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language Id", "The id of the lanuage to use, should terminology or ontology be retrieved.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology Flag", "A flag to indicate whether or not ontology data should be included in the retrieved Data Requirement Instance.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology Flag", "A flag to indicate whether or not terminology data should be included in the retrieved Data Requirement Instance.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DRI, "Data Requirement Instance", "The Data Requirement Instance retrieved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_RETRIEVE_DR);
            definition.setMetaDisplayName("Retrieve Data Requirement");
            definition.setMetaDescription("Retrieves the specified Data Requirement and the required associated data.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiRetrieveDataRequirement");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_ID, "Data Requirement Id", "The id of the Data Requirement to retrieve.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language Id", "The id of the lanuage to use, should terminology or ontology be retrieved.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology Flag", "A flag to indicate whether or not ontology data should be included in the retrieved Data Requirement.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology Flag", "A flag to indicate whether or not terminology data should be included in the retrieved Data Requirement.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR, "Data Requirement", "The Data Requirement retrieved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_RETRIEVE_DR_COMMENTS);
            definition.setMetaDisplayName("Retrieve Data Requirement Comments");
            definition.setMetaDescription("Retrieves the specified Data Requirement comments.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiRetrieveDataRequirementComments");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ORG_IDS, "Organization Ids", "The list of organizations for which to retrieve linked comments.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_IDS, "Language Ids", "The list of languages for which to retrieve comments.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_IDS, "DR Ids", "The list of Data Requirement ID's for which to retrieve comments.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_IIDS, "DR Instance Ids", "The list of Data Requirement Instance ID's for which to retrieve comments.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_COMMENT_TYPE_IDS, "Comment Type Id", "A list of comment types to retrieve.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_COMMENTS, "Data Requirement Comments", "The list of retrieved Data Requirement Comments.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_INSERT_DRI);
            definition.setMetaDisplayName("Insert Data Requirement Instance");
            definition.setMetaDescription("Inserts the supplied Data Requirement Instance in the ontology data store.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiInsertDataRequirementInstance");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT, "Concept", "The Data Requirement Instance concept object to insert.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_ID, "Data Requirement Id", "The Id of the Data Requirement on which the Instance is based.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INDEPENDENT, "Independent", "A boolean flag indicating whether or not the Data Requirement Instance is independent or not.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_SAVE_DRI);
            definition.setMetaDisplayName("Save Data Requirement Instance");
            definition.setMetaDescription("Inserts the supplied Data Requirement Instance in the ontology data store if it does not exist or updates the existing data if it does.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiSaveDataRequirementInstance");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT, "Concept", "The Data Requirement Instance concept object to save.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_ID, "Data Requirement Id", "The Id of the Data Requirement on which the Instance is based.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INDEPENDENT, "Independent", "A boolean flag indicating whether or not the Data Requirement Instance is independent or not.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_SAVE_DR);
            definition.setMetaDisplayName("Save Data Requirement");
            definition.setMetaDescription("Inserts the supplied Data Requirement in the ontology data store if it does not exist or updates the existing data if it does.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiSaveDataRequirement");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR, "Data Requirement", "The Data Requirement document to save.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SAVE_ONTOLOGY, "Save Ontology Flag", "A boolean flag indicating whether or not the ontology contained in the supplied Data Requirement document should also be saved.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_DELETE_DRI);
            definition.setMetaDisplayName("Delete Data Requirement Instance");
            definition.setMetaDescription("Delete the specified Data Requirement Instance.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiDeleteDataRequirementInstance");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_IID, "DR Instance Id", "The concept id of the Data Requirement Instance to delete.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETE_ONTOLOGY, "Delete Ontology Flag", "A boolean flag indicating whether or not the concept for the DR Instance should also be deleted.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_DRQ_DELETE_DR);
            definition.setMetaDisplayName("Delete Data Requirement");
            definition.setMetaDescription("Delete the specified Data Requirement.");
            definition.setClassName("com.pilog.t8.api.T8DataRequirementApiOperations$ApiDeleteDataRequirement");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_IID, "DR Id", "The concept id of the Data Requirement to delete.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETE_ONTOLOGY, "Delete Ontology Flag", "A boolean flag indicating whether or not the concept for the DR should also be deleted.", T8DataType.BOOLEAN));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
