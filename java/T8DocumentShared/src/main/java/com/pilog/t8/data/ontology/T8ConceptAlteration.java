package com.pilog.t8.data.ontology;

import com.pilog.t8.data.org.T8OntologyLink;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptAlteration implements Serializable
{
    private final List<T8OntologyConcept> insertedConcepts;
    private final List<T8OntologyConcept> updatedConcepts;
    private final List<T8OntologyConcept> deletedConcepts;
    private final List<T8OntologyTerm> insertedTerms;
    private final List<T8OntologyTerm> updatedTerms;
    private final List<T8OntologyTerm> deletedTerms;
    private final List<T8OntologyAbbreviation> insertedAbbreviations;
    private final List<T8OntologyAbbreviation> updatedAbbreviations;
    private final List<T8OntologyAbbreviation> deletedAbbreviations;
    private final List<T8OntologyDefinition> insertedDefinitions;
    private final List<T8OntologyDefinition> updatedDefinitions;
    private final List<T8OntologyDefinition> deletedDefinitions;
    private final List<T8OntologyCode> insertedCodes;
    private final List<T8OntologyCode> updatedCodes;
    private final List<T8OntologyCode> deletedCodes;
    private final List<T8OntologyLink> insertedOntologyLinks;
    private final List<T8OntologyLink> deletedOntologyLinks;

    public T8ConceptAlteration()
    {
        this.insertedConcepts = new ArrayList<>();
        this.updatedConcepts = new ArrayList<>();
        this.deletedConcepts = new ArrayList<>();
        this.insertedTerms = new ArrayList<>();
        this.updatedTerms = new ArrayList<>();
        this.deletedTerms = new ArrayList<>();
        this.insertedAbbreviations = new ArrayList<>();
        this.updatedAbbreviations = new ArrayList<>();
        this.deletedAbbreviations = new ArrayList<>();
        this.insertedDefinitions = new ArrayList<>();
        this.updatedDefinitions = new ArrayList<>();
        this.deletedDefinitions = new ArrayList<>();
        this.insertedCodes = new ArrayList<>();
        this.updatedCodes = new ArrayList<>();
        this.deletedCodes = new ArrayList<>();
        this.insertedOntologyLinks = new ArrayList<T8OntologyLink>();
        this.deletedOntologyLinks = new ArrayList<T8OntologyLink>();
    }

    public void clear()
    {
        this.insertedConcepts.clear();
        this.updatedConcepts.clear();
        this.deletedConcepts.clear();
        this.insertedTerms.clear();
        this.updatedTerms.clear();
        this.deletedTerms.clear();
        this.insertedDefinitions.clear();
        this.updatedDefinitions.clear();
        this.deletedDefinitions.clear();
        this.insertedCodes.clear();
        this.updatedCodes.clear();
        this.deletedCodes.clear();
        this.insertedOntologyLinks.clear();
        this.deletedOntologyLinks.clear();
    }

    public void clearTerms()
    {
        this.insertedTerms.clear();
        this.updatedTerms.clear();
        this.deletedTerms.clear();
    }

    public void clearInsertedTerms()
    {
        this.insertedTerms.clear();
    }

    public void clearUpdatedTerms()
    {
        this.updatedTerms.clear();
    }

    public void clearDefinitions()
    {
        this.insertedDefinitions.clear();
        this.updatedDefinitions.clear();
        this.deletedDefinitions.clear();
    }

    public void clearInsertedDefinitions()
    {
        this.insertedDefinitions.clear();
    }

    public void clearUpdatedDefinitions()
    {
        this.insertedDefinitions.clear();
    }

    public void add(T8ConceptAlteration alteration)
    {
        this.insertedConcepts.addAll(alteration.getInsertedConcepts());
        this.updatedConcepts.addAll(alteration.getUpdatedConcepts());
        this.deletedConcepts.addAll(alteration.getDeletedConcepts());
        this.insertedTerms.addAll(alteration.getInsertedTerms());
        this.updatedTerms.addAll(alteration.getUpdatedTerms());
        this.deletedTerms.addAll(alteration.getDeletedTerms());
        this.insertedDefinitions.addAll(alteration.getInsertedDefinitions());
        this.updatedDefinitions.addAll(alteration.getUpdatedDefinitions());
        this.deletedDefinitions.addAll(alteration.getDeletedDefinitions());
        this.insertedCodes.addAll(alteration.getInsertedCodes());
        this.updatedCodes.addAll(alteration.getUpdatedCodes());
        this.deletedCodes.addAll(alteration.getDeletedCodes());
        this.insertedOntologyLinks.addAll(alteration.getInsertedOntologyLinks());
        this.deletedOntologyLinks.addAll(alteration.getDeletedOntologyLinks());
    }

    public void addInsertedConcept(T8OntologyConcept concept)
    {
        insertedConcepts.add(concept);
    }

    public List<T8OntologyConcept> getInsertedConcepts()
    {
        return new ArrayList<>(insertedConcepts);
    }

    public void addUpdatedConcept(T8OntologyConcept concept)
    {
        updatedConcepts.add(concept);
    }

    public List<T8OntologyConcept> getUpdatedConcepts()
    {
        return new ArrayList<>(updatedConcepts);
    }

    public void addDeletedConcept(T8OntologyConcept concept)
    {
        deletedConcepts.add(concept);
    }

    public List<T8OntologyConcept> getDeletedConcepts()
    {
        return new ArrayList<>(deletedConcepts);
    }

    public void addInsertedTerm(T8OntologyTerm term)
    {
        insertedTerms.add(term);
    }

    public void addInsertedTerms(Collection<T8OntologyTerm> terms)
    {
        insertedTerms.addAll(terms);
    }

    public List<T8OntologyTerm> getInsertedTerms()
    {
        return new ArrayList<>(insertedTerms);
    }

    public void addUpdatedTerm(T8OntologyTerm term)
    {
        updatedTerms.add(term);
    }

    public void addUpdatedTerms(Collection<T8OntologyTerm> terms)
    {
        updatedTerms.addAll(terms);
    }

    public List<T8OntologyTerm> getUpdatedTerms()
    {
        return new ArrayList<>(updatedTerms);
    }

    public void addDeletedTerm(T8OntologyTerm term)
    {
        deletedTerms.add(term);
    }

    public void addDeletedTerms(Collection<T8OntologyTerm> terms)
    {
        deletedTerms.addAll(terms);
    }

    public List<T8OntologyTerm> getDeletedTerms()
    {
        return new ArrayList<>(deletedTerms);
    }

    public void addInsertedAbbreviation(T8OntologyAbbreviation abbreviation)
    {
        insertedAbbreviations.add(abbreviation);
    }

    public void addInsertedAbbreviations(Collection<T8OntologyAbbreviation> abbreviations)
    {
        insertedAbbreviations.addAll(abbreviations);
    }

    public List<T8OntologyAbbreviation> getInsertedAbbreviations()
    {
        return new ArrayList<>(insertedAbbreviations);
    }

    public void addUpdatedAbbreviation(T8OntologyAbbreviation abbreviation)
    {
        updatedAbbreviations.add(abbreviation);
    }

    public void addUpdatedAbbreviations(Collection<T8OntologyAbbreviation> abbreviations)
    {
        updatedAbbreviations.addAll(abbreviations);
    }

    public List<T8OntologyAbbreviation> getUpdatedAbbreviations()
    {
        return new ArrayList<>(updatedAbbreviations);
    }

    public void addDeletedAbbreviation(T8OntologyAbbreviation abbreviation)
    {
        deletedAbbreviations.add(abbreviation);
    }

    public void addDeletedAbbreviations(Collection<T8OntologyAbbreviation> abbreviations)
    {
        deletedAbbreviations.addAll(abbreviations);
    }

    public List<T8OntologyAbbreviation> getDeletedAbbreviations()
    {
        return new ArrayList<>(deletedAbbreviations);
    }

    public void addInsertedDefinition(T8OntologyDefinition definition)
    {
        insertedDefinitions.add(definition);
    }

    public void addInsertedDefinitions(Collection<T8OntologyDefinition> definitions)
    {
        insertedDefinitions.addAll(definitions);
    }

    public List<T8OntologyDefinition> getInsertedDefinitions()
    {
        return new ArrayList<>(insertedDefinitions);
    }

    public void addUpdatedDefinition(T8OntologyDefinition definition)
    {
        updatedDefinitions.add(definition);
    }

    public void addUpdatedDefinitions(Collection<T8OntologyDefinition> definitions)
    {
        updatedDefinitions.addAll(definitions);
    }

    public List<T8OntologyDefinition> getUpdatedDefinitions()
    {
        return new ArrayList<>(updatedDefinitions);
    }

    public void addDeletedDefinition(T8OntologyDefinition definition)
    {
        deletedDefinitions.add(definition);
    }

    public void addDeletedDefinitions(Collection<T8OntologyDefinition> definitions)
    {
        deletedDefinitions.addAll(definitions);
    }

    public List<T8OntologyDefinition> getDeletedDefinitions()
    {
        return new ArrayList<>(deletedDefinitions);
    }

    public void addInsertedCode(T8OntologyCode code)
    {
        insertedCodes.add(code);
    }

    public void addInsertedCodes(Collection<T8OntologyCode> codes)
    {
        insertedCodes.addAll(codes);
    }

    public List<T8OntologyCode> getInsertedCodes()
    {
        return new ArrayList<>(insertedCodes);
    }

    public void addUpdatedCode(T8OntologyCode code)
    {
        updatedCodes.add(code);
    }

    public void addUpdatedCodes(Collection<T8OntologyCode> codes)
    {
        updatedCodes.addAll(codes);
    }

    public List<T8OntologyCode> getUpdatedCodes()
    {
        return new ArrayList<>(updatedCodes);
    }

    public void addDeletedCode(T8OntologyCode code)
    {
        deletedCodes.add(code);
    }

    public void addDeletedCodes(Collection<T8OntologyCode> codes)
    {
        deletedCodes.addAll(codes);
    }

    public List<T8OntologyCode> getDeletedCodes()
    {
        return new ArrayList<>(deletedCodes);
    }

    public List<T8OntologyLink> getInsertedOntologyLinks()
    {
        return new ArrayList<T8OntologyLink>(insertedOntologyLinks);
    }

    public void addInsertedOntologyLink(T8OntologyLink link)
    {
        insertedOntologyLinks.add(link);
    }

    public void addInsertedOntologyLinks(Collection<T8OntologyLink> links)
    {
        insertedOntologyLinks.addAll(links);
    }

    public List<T8OntologyLink> getDeletedOntologyLinks()
    {
        return new ArrayList<T8OntologyLink>(deletedOntologyLinks);
    }

    public void addDeletedOntologyLink(T8OntologyLink link)
    {
        deletedOntologyLinks.add(link);
    }

    public void addDeletedOntologyLinks(Collection<T8OntologyLink> links)
    {
        deletedOntologyLinks.addAll(links);
    }

    /**
     * Returns the set of id's of inserted concepts in this alteration.
     * @return The set of id's of inserted concepts in this alteration.
     */
    public Set<String> getInsertedConceptIdSet()
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (T8OntologyConcept insertedConcept : insertedConcepts)
        {
            idSet.add(insertedConcept.getID());
        }

        return idSet;
    }

    /**
     * Returns the set of id's of all concepts that are not new inserts but
     * that have been affected by this alteration i.e. concepts that have
     * terms, definitions, codes or ontology links that are listed as
     * inserts, updates or deletes in this alteration.
     * @return The set of id's of all concepts that are not new inserts but
     * that have been affected by this alteration
     */
    public Set<String> getContentAlteredConceptIdSet()
    {
        Set<String> idSet;

        idSet = new HashSet<>();

        // This adds all concept id's that were directly updated.  The following loops adds concept id's that were indirectly affected.
        for (T8OntologyConcept concept : updatedConcepts)
        {
            idSet.add(concept.getID());
        }

        for (T8OntologyTerm term : insertedTerms)
        {
            idSet.add(term.getConceptID());
        }

        for (T8OntologyTerm term : updatedTerms)
        {
            idSet.add(term.getConceptID());
        }

        for (T8OntologyTerm term : deletedTerms)
        {
            idSet.add(term.getConceptID());
        }

        for (T8OntologyDefinition definition : insertedDefinitions)
        {
            idSet.add(definition.getConceptID());
        }

        for (T8OntologyDefinition definition : updatedDefinitions)
        {
            idSet.add(definition.getConceptID());
        }

        for (T8OntologyDefinition definition : deletedDefinitions)
        {
            idSet.add(definition.getConceptID());
        }

        for (T8OntologyCode code : insertedCodes)
        {
            idSet.add(code.getConceptID());
        }

        for (T8OntologyCode code : updatedCodes)
        {
            idSet.add(code.getConceptID());
        }

        for (T8OntologyCode code : deletedCodes)
        {
            idSet.add(code.getConceptID());
        }

        for (T8OntologyLink link : insertedOntologyLinks)
        {
            idSet.add(link.getConceptID());
        }

        for (T8OntologyLink link : deletedOntologyLinks)
        {
            idSet.add(link.getConceptID());
        }

        // Remove all concept id's of concepts that are listed as inserts as they have very obviously been affected.
        idSet.removeAll(getInsertedConceptIdSet());
        return idSet;
    }

    public static final T8ConceptAlteration getConceptAlteration(T8OntologyConcept fromState, T8OntologyConcept toState)
    {
        if (fromState == null)
        {
            T8ConceptAlteration alteration;

            // Create the new alteration.
            alteration = new T8ConceptAlteration();
            alteration.addInsertedConcept(toState);
            alteration.addTermAlterations(null, toState);
            alteration.addDefinitionAlterations(null, toState);
            alteration.addCodeAlterations(null, toState);
            alteration.addInsertedOntologyLinks(toState.getOntologyLinks());

            // Return the final alteration result.
            return alteration;
        }
        else if (toState == null)
        {
            T8ConceptAlteration alteration;

            // Create the new alteration.
            alteration = new T8ConceptAlteration();
            alteration.addDeletedConcept(fromState);
            alteration.addTermAlterations(fromState, null);
            alteration.addDefinitionAlterations(fromState, null);
            alteration.addCodeAlterations(fromState, null);
            alteration.addDeletedOntologyLinks(fromState.getOntologyLinks());

            // Return the final alteration result.
            return alteration;
        }
        else
        {
            T8ConceptAlteration alteration;

            // Create the new alteration.
            alteration = new T8ConceptAlteration();
            alteration.addTermAlterations(fromState, toState);
            alteration.addDefinitionAlterations(fromState, toState);
            alteration.addCodeAlterations(fromState, toState);

            // Only update the concept if it is different from the existing concept.
            if (!fromState.isEqualTo(toState))
            {
                alteration.addUpdatedConcept(toState);
            }

            // Determine the record concept alteration.
            for (T8OntologyLink toStateOntologyLink : toState.getOntologyLinks())
            {
                if (!fromState.containsEquivalentLink(toStateOntologyLink))
                {
                    alteration.addInsertedOntologyLink(toStateOntologyLink);
                }
            }

            // Find the deleted ontology links.
            for (T8OntologyLink fromStateOntologyLink : fromState.getOntologyLinks())
            {
                if (!toState.containsEquivalentLink(fromStateOntologyLink))
                {
                    alteration.addDeletedOntologyLink(fromStateOntologyLink);
                }
            }

            // Return the final alteration result.
            return alteration;
        }
    }

    public void addTermAlterations(T8OntologyConcept fromState, T8OntologyConcept toState)
    {
        if (fromState == null)
        {
            // Add inserts.
            for (T8OntologyTerm toStateTerm : toState.getTerms())
            {
                addTermAlteration(null, toStateTerm);
            }
        }
        else if (toState == null)
        {
            // Add inserts.
            for (T8OntologyTerm fromStateTerm : fromState.getTerms())
            {
                addTermAlteration(fromStateTerm, null);
            }
        }
        else
        {
            // Add inserts and updates.
            for (T8OntologyTerm toStateTerm : toState.getTerms())
            {
                T8OntologyTerm fromStateTerm;

                fromStateTerm = fromState.getTerm(toStateTerm.getID());
                addTermAlteration(fromStateTerm, toStateTerm);
            }

            // Add deletions.
            for (T8OntologyTerm fromStateTerm : fromState.getTerms())
            {
                T8OntologyTerm toStateTerm;

                toStateTerm = toState.getTerm(fromStateTerm.getID());
                if (toStateTerm == null)
                {
                    addTermAlteration(fromStateTerm, null);
                }
            }
        }
    }

    private void addTermAlteration(T8OntologyTerm fromState, T8OntologyTerm toState)
    {
        if (fromState == null)
        {
            String termId;

            termId = toState.getID();
            addInsertedTerm(toState);
            addAbbreviationAlterations(fromState, toState);
        }
        else if (toState == null)
        {
            String termId;

            termId = fromState.getID();
            addDeletedTerm(fromState);
            addAbbreviationAlterations(fromState, toState);
        }
        else
        {
            // Only update the term if it is different from the existing term.
            if (!fromState.isEqualTo(toState))
            {
                addUpdatedTerm(toState);
            }

            // Add abbreviation alterations.
            addAbbreviationAlterations(fromState, toState);
        }
    }

    public void addAbbreviationAlterations(T8OntologyTerm fromState, T8OntologyTerm toState)
    {
        if (fromState == null)
        {
            // Add inserts.
            for (T8OntologyAbbreviation toStateAbbreviation : toState.getAbbreviations())
            {
                addAbbreviationAlteration(null, toStateAbbreviation);
            }
        }
        else if (toState == null)
        {
            // Add deletes.
            for (T8OntologyAbbreviation fromStateAbbreviation : fromState.getAbbreviations())
            {
                addAbbreviationAlteration(fromStateAbbreviation, null);
            }
        }
        else
        {
            // Add inserts and updates.
            for (T8OntologyAbbreviation toStateAbbreviation : toState.getAbbreviations())
            {
                T8OntologyAbbreviation fromStateAbbreviation;

                fromStateAbbreviation = fromState.getAbbreviation(toStateAbbreviation.getID());
                addAbbreviationAlteration(fromStateAbbreviation, toStateAbbreviation);
            }

            // Add deletions.
            for (T8OntologyAbbreviation fromStateAbbreviation : fromState.getAbbreviations())
            {
                T8OntologyAbbreviation toStateAbbreviation;

                toStateAbbreviation = toState.getAbbreviation(fromStateAbbreviation.getID());
                if (toStateAbbreviation == null)
                {
                    addAbbreviationAlteration(fromStateAbbreviation, null);
                }
            }
        }
    }

    private void addAbbreviationAlteration(T8OntologyAbbreviation fromState, T8OntologyAbbreviation toState)
    {
        if (fromState == null)
        {
            addInsertedAbbreviation(toState);
        }
        else if (toState == null)
        {
            addDeletedAbbreviation(fromState);
        }
        else
        {
            String abbreviationId;

            // Only update the abbreviation if it is different from the existing abbreviation.
            if (!fromState.isEqualTo(toState))
            {
                addUpdatedAbbreviation(toState);
            }
        }
    }

    public void addDefinitionAlterations(T8OntologyConcept fromState, T8OntologyConcept toState)
    {
        if (fromState == null)
        {
            // Add inserts.
            for (T8OntologyDefinition toStateDefinition : toState.getDefinitions())
            {
                addDefinitionAlteration(null, toStateDefinition);
            }
        }
        else if (toState == null)
        {
            // Add deletes.
            for (T8OntologyDefinition fromStateDefinition : fromState.getDefinitions())
            {
                addDefinitionAlteration(fromStateDefinition, null);
            }
        }
        else
        {
            // Add inserts and updates.
            for (T8OntologyDefinition toStateDefinition : toState.getDefinitions())
            {
                T8OntologyDefinition fromStateDefinition;

                fromStateDefinition = fromState.getDefinition(toStateDefinition.getID());
                addDefinitionAlteration(fromStateDefinition, toStateDefinition);
            }

            // Add deletions.
            for (T8OntologyDefinition fromStateDefinition : fromState.getDefinitions())
            {
                T8OntologyDefinition toStateDefinition;

                toStateDefinition = toState.getDefinition(fromStateDefinition.getID());
                if (toStateDefinition == null)
                {
                    addDefinitionAlteration(fromStateDefinition, null);
                }
            }
        }
    }

    private void addDefinitionAlteration(T8OntologyDefinition fromState, T8OntologyDefinition toState)
    {
        if (fromState == null)
        {
            addInsertedDefinition(toState);
        }
        else if (toState == null)
        {
            addDeletedDefinition(fromState);
        }
        else
        {
            // Only update the definition if it is different from the existing definition.
            if (!fromState.isEqualTo(toState))
            {
                addUpdatedDefinition(toState);
            }
        }
    }

    public void addCodeAlterations(T8OntologyConcept fromState, T8OntologyConcept toState)
    {
        if (fromState == null)
        {
            // Add inserts.
            for (T8OntologyCode toStateCode : toState.getCodes())
            {
                addCodeAlteration(null, toStateCode);
            }
        }
        else if (toState == null)
        {
            // Add inserts.
            for (T8OntologyCode fromStateCode : fromState.getCodes())
            {
                addCodeAlteration(fromStateCode, null);
            }
        }
        else
        {
            // Add inserts and updates.
            for (T8OntologyCode toStateCode : toState.getCodes())
            {
                T8OntologyCode fromStateCode;

                fromStateCode = fromState.getCode(toStateCode.getID());
                addCodeAlteration(fromStateCode, toStateCode);
            }

            // Add deletions.
            for (T8OntologyCode fromStateCode : fromState.getCodes())
            {
                T8OntologyCode toStateCode;

                toStateCode = toState.getCode(fromStateCode.getID());
                if (toStateCode == null)
                {
                    addCodeAlteration(fromStateCode, null);
                }
            }
        }
    }

    private void addCodeAlteration(T8OntologyCode fromState, T8OntologyCode toState)
    {
        if (fromState == null)
        {
            addInsertedCode(toState);
        }
        else if (toState == null)
        {
            addDeletedCode(fromState);
        }
        else
        {
            // Only update the code if it is different from the existing code.
            if (!fromState.isEqualTo(toState))
            {
                addUpdatedCode(toState);
            }
        }
    }

    @Override
    public String toString()
    {
        return "T8ConceptAlteration{" + "insertedConcepts=" + insertedConcepts + ", updatedConcepts=" + updatedConcepts + ", deletedConcepts=" + deletedConcepts + ", insertedTerms=" + insertedTerms + ", updatedTerms=" + updatedTerms + ", deletedTerms=" + deletedTerms + ", insertedAbbreviations=" + insertedAbbreviations + ", updatedAbbreviations=" + updatedAbbreviations + ", deletedAbbreviations=" + deletedAbbreviations + ", insertedDefinitions=" + insertedDefinitions + ", updatedDefinitions=" + updatedDefinitions + ", deletedDefinitions=" + deletedDefinitions + ", insertedCodes=" + insertedCodes + ", updatedCodes=" + updatedCodes + ", deletedCodes=" + deletedCodes + ", insertedOntologyLinks=" + insertedOntologyLinks + ", deletedOntologyLinks=" + deletedOntologyLinks + "}";
    }
}
