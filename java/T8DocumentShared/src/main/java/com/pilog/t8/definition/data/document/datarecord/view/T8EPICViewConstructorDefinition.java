package com.pilog.t8.definition.data.document.datarecord.view;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.data.document.datarecord.view.T8DataRecordViewConstructor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICViewConstructorDefinition extends T8DataRecordViewConstructorDefinition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_VIEW_CONSTRUCTOR_EPIC";
    public static final String DISPLAY_NAME = "EPIC View Constructor";
    public static final String DESCRIPTION = "A definition that specifies a view constructor that uses EPIC to compile view data from input data files.";
    public enum Datum
    {
        EPIC_SCRIPT,
        OUTPUT_PARAMETER_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    // EPIC script input parameters.
    public static final String PARAMETER_DATA_RECORD = "$P_DATA_RECORD";
    public static final String PARAMETER_ALTERATION = "$P_ALTERATION";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";

    public T8EPICViewConstructorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.EPIC_SCRIPT.toString(), "EPIC Script",  "The script the will be executed to compile view data."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OUTPUT_PARAMETER_DEFINITIONS.toString(), "Output Parameters", "The output data parameters returned by this script."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if (Datum.OUTPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    public String getEPICScript()
    {
        return getDefinitionDatum(Datum.EPIC_SCRIPT);
    }

    public void setEPICScript(String script)
    {
        setDefinitionDatum(Datum.EPIC_SCRIPT, script);
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.script.T8DefaultServerContextScript", new Class<?>[]{T8Context.class, T8ServerContextScriptDefinition.class}, context, this);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_DATA_RECORD, "Data Record", "The data record for which view data must be constructed.", T8DataType.CUSTOM_OBJECT));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_ALTERATION, "Alteration", "The alteration (if any) that the record is involved in.", T8DataType.CUSTOM_OBJECT));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The language to use for any terminology data.", T8DataType.GUID));
        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return getOutputParameterDefinitions();
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        return new ArrayList<>();
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        return new ArrayList<>();
    }

    @Override
    public String getScript()
    {
        return getEPICScript();
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }

    @Override
    public List<String> getOutputParameterIdentifiers()
    {
        List<String> identifierList;

        identifierList = new ArrayList<>();
        for (T8DataParameterDefinition outputParameterDefinition : getOutputParameterDefinitions())
        {
            identifierList.add(outputParameterDefinition.getIdentifier());
        }

        return identifierList;
    }

    @Override
    public T8DataRecordViewConstructor createNewViewConstructor(T8DataTransaction tx)
    {
        try
        {
            return T8Reflections.getFastFailInstance("com.pilog.t8.data.document.datarecord.view.T8EPICDataRecordViewConstructor", new Class<?>[]{T8DataTransaction.class, this.getClass()}, tx, this);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing View Constructor.", e);
        }
    }

    public ArrayList<T8DataParameterDefinition> getOutputParameterDefinitions()
    {
        return getDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS);
    }

    public void setOutputParameterDefinitions(ArrayList<T8DataParameterDefinition> parameterDefinitions)
    {
        setDefinitionDatum(Datum.OUTPUT_PARAMETER_DEFINITIONS, parameterDefinitions);
    }
}