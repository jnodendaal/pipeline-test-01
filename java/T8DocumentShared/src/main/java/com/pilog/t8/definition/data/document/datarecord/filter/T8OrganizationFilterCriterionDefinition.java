package com.pilog.t8.definition.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.filter.T8OrganizationFilterCriterion;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 *
 * @author Pieter Strydom
 */
public class T8OrganizationFilterCriterionDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RECORD_FILTER_ORGANIZATION_CRITERION";
    public static final String GROUP_IDENTIFIER = "@DT_RECORD_FILTER_ORGANIZATION_CRITERION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Organization Filter Criterion";
    public static final String DESCRIPTION = "A filter criterion applicable to a specific Data Record organization.";
    private enum Datum {ORGANIZATION_TYPE_ID_LIST,
                        OPERATOR,
                        LABEL
                        }

    public T8OrganizationFilterCriterionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.ORGANIZATION_TYPE_ID_LIST.toString(), "Organization Types",  "The organization types to which this search is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATOR.toString(), "Operator", "The operator used by this criterion.", DataFilterOperator.EQUAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DISPLAY_STRING, Datum.LABEL.toString(), "Label", "The label displayed on the organization search component."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(DataFilterOperator.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8OrganizationFilterCriterion getOrganizationFilterCriterionInstance(T8Context context)
    {
        T8OrganizationFilterCriterion criteria;

        criteria = new T8OrganizationFilterCriterion(getOrganizationTypeIdList(), getLabel());
        criteria.setOperator(getOperator());
        return criteria;
    }

    public List<String> getOrganizationTypeIdList()
    {
        return (List<String>)getDefinitionDatum(Datum.ORGANIZATION_TYPE_ID_LIST.toString());
    }

    public void setOrganizationTypeIdList(List<String> organizationTypeIdList)
    {
        setDefinitionDatum(Datum.ORGANIZATION_TYPE_ID_LIST.toString(), organizationTypeIdList);
    }

    public DataFilterOperator getOperator()
    {
        String stringValue;

        stringValue = (String)getDefinitionDatum(Datum.OPERATOR.toString());
        if (Strings.isNullOrEmpty(stringValue)) return DataFilterOperator.EQUAL;
        else return DataFilterOperator.valueOf(stringValue);
    }

    public void setOperator(DataFilterOperator operator)
    {
        setDefinitionDatum(Datum.OPERATOR.toString(), operator.toString());
    }

    public String getLabel()
    {
        return (String)getDefinitionDatum(Datum.LABEL.toString());
    }

    public void setLabel(String label)
    {
        setDefinitionDatum(Datum.LABEL.toString(), label);
    }
}
