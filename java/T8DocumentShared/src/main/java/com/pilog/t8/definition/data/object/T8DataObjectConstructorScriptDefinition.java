package com.pilog.t8.definition.data.object;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectConstructorScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_CONSTRUCTOR_SCRIPT";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_OBJECT_CONSTRUCTOR_SCRIPT";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Data Object Constructor Script";
    public static final String DESCRIPTION = "A script that is used to assemble a data object from the various sources required.";
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_SOURCE = "$P_SOURCE";
    public static final String PARAMETER_ORG_ID = "$P_ORG_ID";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";

    public T8DataObjectConstructorScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;

        definitions = new ArrayList<T8DataParameterDefinition>();
        definitions.add(new T8DataParameterDefinition(PARAMETER_SOURCE, "Source", "The source data from which the object is to be created.  The type of this parameter depends on the data objec type.", T8DataType.CUSTOM_OBJECT));
        definitions.add(new T8DataParameterDefinition(PARAMETER_ORG_ID, "Organization Id", "The root organization to use when constructing the object.", T8DataType.GUID));
        definitions.add(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language Id", "The language to use when constructing the object.", T8DataType.GUID));
        return definitions;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> definitions;
        T8DataObjectDefinition objectDefinition;

        // Add all of the fields of the parent data object as output parameter definitions to the script.
        objectDefinition = (T8DataObjectDefinition)getParentDefinition();
        definitions = new ArrayList<T8DataParameterDefinition>();
        for (T8DataObjectFieldDefinition fieldDefinition : objectDefinition.getFieldDefinitions())
        {
            definitions.add(new T8DataParameterDefinition(fieldDefinition.getIdentifier(), fieldDefinition.getMetaDisplayName(), fieldDefinition.getMetaDescription(), fieldDefinition.getDataType()));
        }

        // Return the final list of output parameter definitions.
        return definitions;
    }
}