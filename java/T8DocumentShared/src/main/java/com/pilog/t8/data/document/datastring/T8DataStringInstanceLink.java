package com.pilog.t8.data.document.datastring;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringInstanceLink implements Serializable
{
    private String drInstanceId;
    private String dsInstanceId;
    private String conditionExpression;
    private String dataKeyExpression;
    private int sequence;

    public T8DataStringInstanceLink(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getDsInstanceId()
    {
        return dsInstanceId;
    }

    public void setDsInstanceId(String dsInstanceId)
    {
        this.dsInstanceId = dsInstanceId;
    }

    public String getConditionExpression()
    {
        return conditionExpression;
    }

    public void setConditionExpression(String conditionExpression)
    {
        this.conditionExpression = conditionExpression;
    }

    public String getDataKeyExpression()
    {
        return dataKeyExpression;
    }

    public void setDataKeyExpression(String dataKeyExpression)
    {
        this.dataKeyExpression = dataKeyExpression;
    }

    public int getSequence()
    {
        return sequence;
    }

    public void setSequence(int sequence)
    {
        this.sequence = sequence;
    }

    @Override
    public String toString()
    {
        return "T8DataStringInstanceLink{" + "drInstanceId=" + drInstanceId + ", dsInstanceId=" + dsInstanceId + ", conditionExpression=" + conditionExpression + ", dataKeyExpression=" + dataKeyExpression + ", sequence=" + sequence + '}';
    }
}
