package com.pilog.t8.api.datarecordeditor;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileRecordCreation implements Serializable
{
    private String recordId;
    private String propertyId;
    private String fieldId;
    private String drInstanceId;
    private String temporaryId;
    private String newRecordId;

    public T8DataFileRecordCreation()
    {
    }

    public T8DataFileRecordCreation(String recordId, String propertyId, String fieldId)
    {
        this();
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getTemporaryId()
    {
        return temporaryId;
    }

    public void setTemporaryId(String temporaryId)
    {
        this.temporaryId = temporaryId;
    }

    public String getNewRecordId()
    {
        return newRecordId;
    }

    public void setNewRecordId(String newRecordId)
    {
        this.newRecordId = newRecordId;
    }
}
