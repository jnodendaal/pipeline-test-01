package com.pilog.t8.data.document.validation;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordPropertyPointer;
import com.pilog.t8.data.document.validation.T8DataValidationError.ValidationErrorType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileValidationReport implements Serializable
{
    private final Map<String, List<T8DataValidationError>> validationErrors;

    public T8DataFileValidationReport()
    {
        this.validationErrors = new HashMap<>();
    }

    public boolean hasErrors()
    {
        for (List<T8DataValidationError> errorList : validationErrors.values())
        {
            if (errorList.size() > 0) return true;
        }

        return false;
    }

    public int getErrorCount()
    {
        int errorCount;

        errorCount = 0;
        for (List<T8DataValidationError> errorList : validationErrors.values())
        {
            errorCount += (errorList == null ? 0 : errorList.size());
        }

        return errorCount;
    }

    public boolean hasErrors(String recordId)
    {
        List<T8DataValidationError> errorList;

        errorList = validationErrors.get(recordId);
        return ((errorList != null) && (errorList.size() > 0));
    }

    /**
     * Gets the number of records within the report which contain errors.
     *
     * @return The {@code int} number of records in the report with errors
     */
    public int getErrorRecordCount()
    {
        int errorRecordCount;

        errorRecordCount = 0;
        for (Map.Entry<String, List<T8DataValidationError>> recordEntry : validationErrors.entrySet())
        {
            List<T8DataValidationError> errorList = recordEntry.getValue();
            // If the error list exists and isn't empty, the record can be counted
            errorRecordCount += ((errorList == null || errorList.isEmpty()) ? 0 : 1);
        }

        return errorRecordCount;
    }

    public boolean hasNonReferenceErrors(String recordId)
    {
        List<T8DataValidationError> errorList;

        errorList = validationErrors.get(recordId);
        if ((errorList != null) && (errorList.size() > 0))
        {
            for (T8DataValidationError error : errorList)
            {
                if (error.getErrorType() != ValidationErrorType.REFERENCE)
                {
                    return true;
                }
            }

            return false;
        }
        else return false;
    }

    /**
     * Returns the number of records which have errors which are not simply a
     * reference to a sub-record with an error.
     *
     * @return The {@code int} number of records with non-reference errors
     */
    public int getNonReferenceErrorRecordCount()
    {
        int nonReferenceErrorRecordCount;

        nonReferenceErrorRecordCount = 0;
        for (String recordID : this.validationErrors.keySet())
        {
            if (hasNonReferenceErrors(recordID)) nonReferenceErrorRecordCount++;
        }

        return nonReferenceErrorRecordCount;
    }

    public List<String> getRecordIdList()
    {
        return new ArrayList<>(validationErrors.keySet());
    }

    public List<T8DataValidationError> getValidationErrors(String recordId)
    {
        return validationErrors.get(recordId);
    }

    public  Map<String, List<T8DataValidationError>> getValidationErrors()
    {
        Map<String, List<T8DataValidationError>> errors;

        errors = new HashMap<>();
        for (String recordID : validationErrors.keySet())
        {
            errors.put(recordID, new ArrayList<>(validationErrors.get(recordID)));
        }

        return errors;
    }

    public List<T8DataValidationError> getValidationErrorList()
    {
        List<T8DataValidationError> errors;

        errors = new ArrayList<>();
        for (String recordID : validationErrors.keySet())
        {
            errors.addAll(validationErrors.get(recordID));
        }

        return errors;
    }

    public void addValidationErrors(Collection<T8DataValidationError> errors)
    {
        for (T8DataValidationError error : errors)
        {
            addValidationError(error);
        }
    }

    public void addValidationError(T8DataValidationError error)
    {
        List<T8DataValidationError> recordErrors;
        String recordID;

        recordID = error.getRecordId();
        recordErrors = validationErrors.get(recordID);
        if (recordErrors == null)
        {
            recordErrors = new ArrayList<>();
            validationErrors.put(recordID, recordErrors);
        }

        recordErrors.add(error);
    }

    public void addValidationReport(T8DataFileValidationReport report)
    {
        for (String recordID : report.validationErrors.keySet())
        {
            for (T8DataValidationError error : report.validationErrors.get(recordID))
            {
                addValidationError(error);
            }
        }
    }

    public void addRecordClassificationError(DataRecord dataRecord, String osId, String ocId, String errorMessage)
    {
        List<RecordPropertyPointer> documentPath;
        String recordId;

        // Get the data record path from the root in the context.
        recordId = dataRecord.getID();
        documentPath = getDocumentPath(dataRecord);

        // Add the error.
        addValidationError(new T8RecordClassificationValidationError(ValidationErrorType.ACCESS, recordId, osId, ocId, errorMessage));

        // For each of the property pointers in the document path, add a validation error so that we can trace the cause of the error from the root document.
        for (RecordPropertyPointer propertyPointer : documentPath)
        {
            String pointerRecordID;

            // Get the record ID of the pointer.
            pointerRecordID = propertyPointer.getRecordID();

            // Add the error.
            addValidationError(new T8RecordValueValidationError(ValidationErrorType.REFERENCE, pointerRecordID, propertyPointer.getPropertyID(), null, "Referenced documents contain invalid data."));
        }
    }

    public void addRecordValueError(DataRecord dataRecord, String propertyId, String fieldId, String errorMessage)
    {
        List<RecordPropertyPointer> documentPath;
        String recordID;

        // Get the data record path from the root in the context.
        recordID = dataRecord.getID();
        documentPath = getDocumentPath(dataRecord);

        // Add the error.
        addValidationError(new T8RecordValueValidationError(ValidationErrorType.ACCESS, recordID, propertyId, fieldId, errorMessage));

        // For each of the property pointers in the document path, add a validation error so that we can trace the cause of the error from the root document.
        for (RecordPropertyPointer propertyPointer : documentPath)
        {
            String pointerRecordID;

            // Get the record ID of the pointer.
            pointerRecordID = propertyPointer.getRecordID();

            // Add the error.
            addValidationError(new T8RecordValueValidationError(ValidationErrorType.REFERENCE, pointerRecordID, propertyPointer.getPropertyID(), null, "Referenced documents contain invalid data."));
        }
    }

    private List<RecordPropertyPointer> getDocumentPath(DataRecord targetRecord)
    {
        List<RecordPropertyPointer> documentPath;
        DataRecord currentRecord;
        DataRecord lastChild;

        documentPath = new ArrayList<>();
        currentRecord = targetRecord;
        lastChild = targetRecord;
        while ((currentRecord = currentRecord.getParentRecord()) != null)
        {
            List<RecordProperty> referenceProperties;

            referenceProperties = currentRecord.getDataRecordReferenceProperties(lastChild.getID());
            for (RecordProperty referenceProperty : referenceProperties)
            {
                documentPath.add(referenceProperty.getPointer());
            }

            lastChild = currentRecord;
        }

        return documentPath;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder;

        stringBuilder = new StringBuilder("T8DataValidationReport");
        stringBuilder.append("{\n");
        for (List<T8DataValidationError> errorList : validationErrors.values())
        {
            for (T8DataValidationError error : errorList)
            {
                stringBuilder.append(error).append(",\n");
            }
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}
