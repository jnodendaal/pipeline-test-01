package com.pilog.t8.data.ontology;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8PhraseAbbreviation implements Serializable
{
    private String languageId;
    private String phrase;
    private String abbreviation;

    public T8PhraseAbbreviation(String languageId, String phrase, String abbreviation)
    {
        this.languageId = languageId;
        this.phrase = phrase;
        this.abbreviation = abbreviation;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getPhrase()
    {
        return phrase;
    }

    public void setPhrase(String phrase)
    {
        this.phrase = phrase;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }
}
