package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;

/**
 * @author Bouwer du Preez
 */
public class StringValue extends RecordValue
{
    public StringValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public String getString()
    {
        return getValue();
    }

    public void setString(String string)
    {
        setValue(string);
    }

    @Override
    public StringValueContent getContent()
    {
        StringValueContent content;

        content = new StringValueContent();
        content.setString(value);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        StringValueContent content;

        content = (StringValueContent)valueContent;
        setString(content.getString());
    }
}
