package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Pieter Strydom
 */
public class T8DocumentReferenceFilterCriterion extends T8ValueFilterCriterion
{
    public T8DocumentReferenceFilterCriterion()
    {
        super(RequirementType.DOCUMENT_REFERENCE);
    }

    public T8DocumentReferenceFilterCriterion(T8DataFilterCriterion.DataFilterOperator operator, Object filterValue)
    {
        super(RequirementType.DOCUMENT_REFERENCE, operator, filterValue);
    }

    @Override
    public T8ValueFilterCriterion copy()
    {
        T8ValueFilterCriterion copiedCriterion;

        copiedCriterion = new T8DocumentReferenceFilterCriterion(operator, filterValue);
        return copiedCriterion;
    }

    @Override
    public String getRequiredCteId()
    {
        // Determine the CTE from which the sub-query will retrieve as well as the value columns that will be used.
        if (operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL)
        {
            return T8RecordFilterCriteria.CTE_NULL_VALUES_IDENTIFIER;
        }
        else
        {
            return T8RecordFilterCriteria.CTE_SEARCH_RECORD_DOCUMENT_REFERENCE;
        }
    }

    @Override
    public StringBuffer getWhereClause(T8DataTransaction tx, T8DatabaseAdaptor adaptor)
    {
        StringBuffer whereClause;
        boolean negationOperator;

        // Set flag indicating negation operators.
        // Add all of the property criteria.
        whereClause = new StringBuffer();
        negationOperator = (operator == T8DataFilterCriterion.DataFilterOperator.NOT_EQUAL) || (operator == T8DataFilterCriterion.DataFilterOperator.NOT_LIKE);

        // Determine the CTE from which the sub-query will retrieve as well as the value columns that will be used.
        if (operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL)
        {
            // No need to add anything, since the inner join to the required CTE will already filter out non-null values.
            return whereClause;
        }
        else if ((filterValue instanceof String) && (T8IdentifierUtilities.isConceptId((String)filterValue)))
        {
            whereClause.append(getValueFilterWhereClause(tx, adaptor, "V.REF_DR_INSTANCE_ID", operator, filterValue, true));
        }
        else
        {
            whereClause.append("(");
            whereClause.append(getValueFilterWhereClause(tx, adaptor, "V.TERM", operator, filterValue, false));
            whereClause.append(negationOperator ? " AND " : " OR ");
            whereClause.append(getValueFilterWhereClause(tx, adaptor, "V.CODE", operator, filterValue, false));
            whereClause.append(")");
        }

        // Return the completed where clause.
        return whereClause;
    }

    @Override
    public List<Object> getWhereClauseParameters(T8DataTransaction tx, T8DatabaseAdaptor adaptor)
    {
        List<Boolean> valueColumnGuidFlags;
        ArrayList<Object> parameterList;
        int valueColumnCount;

        // Add property criteria parameters.
        parameterList = new ArrayList<>();

        // Determine the number of value columns to be used.
        if (operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL)
        {
            valueColumnCount = 0;
            valueColumnGuidFlags = new ArrayList<>();
        }
        else if ((filterValue instanceof String) && (T8IdentifierUtilities.isConceptID((String)filterValue)))
        {
            valueColumnCount = 1;
            valueColumnGuidFlags = Arrays.asList(true);
        }
        else
        {
            valueColumnCount = 2;
            valueColumnGuidFlags = Arrays.asList(false, false);
        }

         // This value must correspond to the size of the value columns list in method {@code getWhereClause}.
        for (int valueColumnIndex = 0; valueColumnIndex < valueColumnCount; valueColumnIndex++)
        {
            boolean guidValue;

            guidValue = valueColumnGuidFlags.get(valueColumnIndex);
            parameterList.addAll(getValueFilterParameters(tx, adaptor, operator, filterValue, guidValue, caseInsensitive));
        }

        // Return the complete parameter list.
        return parameterList;
    }
}
