package com.pilog.t8.data.document.datarecord.access.layer;

/**
 * @author Bouwer du Preez
 */
public interface ConceptAccessLayer
{
    public boolean isAllowNewValue();
    public void setAllowNewValue(boolean allowNewValue);
}
