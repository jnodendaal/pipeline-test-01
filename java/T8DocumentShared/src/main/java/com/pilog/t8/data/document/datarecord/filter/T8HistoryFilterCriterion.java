package com.pilog.t8.data.document.datarecord.filter;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8HistoryFilterCriterion implements Serializable
{
    private String agentId;
    private String agentIid;
    private Long filterTime;
    private HistoryFilterOperator operator;

    public enum HistoryFilterOperator
    {
        ADDED("Added"),
        ADDED_ON("Added On"),
        ADDED_AFTER("Added After"),
        ADDED_BEFORE("Added Before"),
        UPDATED("Updated"),
        UPDATED_ON("Updated On"),
        UPDATED_AFTER("Updated After"),
        UPDATED_BEFORE("Updated Before");

        private final String displayString;

        HistoryFilterOperator(String displayString)
        {
            this.displayString = displayString;
        }

        public String getDisplayString()
        {
            return displayString;
        }
    };

    public T8HistoryFilterCriterion(String agentId)
    {
        this.agentId = agentId;
        this.operator = HistoryFilterOperator.UPDATED;
        this.filterTime = null;
    }

    public T8HistoryFilterCriterion(String agentId, String agentIid, HistoryFilterOperator operator, Long filterTime)
    {
        this.agentId = agentId;
        this.agentIid = agentIid;
        this.operator = operator;
        this.filterTime = filterTime;
    }

    public T8HistoryFilterCriterion copy()
    {
        return new T8HistoryFilterCriterion(agentId, agentIid, operator, filterTime);
    }

    public boolean hasCriterion()
    {
        if (operator == HistoryFilterOperator.ADDED) return true; // This operator does not require a filter time.
        else if (operator == HistoryFilterOperator.UPDATED) return true; // This operator does not require a filter time.
        else return (filterTime != null);
    }

    public String getAgentId()
    {
        return agentId;
    }

    public void setAgentId(String agentId)
    {
        this.agentId = agentId;
    }

    public String getAgentIid()
    {
        return agentIid;
    }

    public void setAgentIid(String agentIid)
    {
        this.agentIid = agentIid;
    }

    public HistoryFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(HistoryFilterOperator operator)
    {
        this.operator = operator;
    }

    public Long getFilterTime()
    {
        return filterTime;
    }

    public void setFilterTime(Long filterTime)
    {
        this.filterTime = filterTime;
    }
}
