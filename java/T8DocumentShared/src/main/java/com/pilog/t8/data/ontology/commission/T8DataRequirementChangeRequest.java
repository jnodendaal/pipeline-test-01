package com.pilog.t8.data.ontology.commission;

import com.pilog.t8.data.document.datarequirement.DataRequirement;
import java.io.Serializable;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementChangeRequest implements Serializable
{
    private DataRequirement changedDataRequirement;
    private Map<String, String> propertySupersessions;
    
    public T8DataRequirementChangeRequest()
    {
    }

    public DataRequirement getChangedDataRequirement()
    {
        return changedDataRequirement;
    }

    public void setChangedDataRequirement(DataRequirement changedDataRequirement)
    {
        this.changedDataRequirement = changedDataRequirement;
    }

    public Map<String, String> getPropertySupersessions()
    {
        return propertySupersessions;
    }

    public void setPropertySupersessions(Map<String, String> propertySupersessions)
    {
        this.propertySupersessions = propertySupersessions;
    }
}
