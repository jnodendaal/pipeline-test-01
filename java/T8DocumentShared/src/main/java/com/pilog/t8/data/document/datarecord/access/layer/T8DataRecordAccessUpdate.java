package com.pilog.t8.data.document.datarecord.access.layer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAccessUpdate implements Serializable
{
    private final DataRecordAccessLayer accessLayer;
    private final List<PropertyAccessLayer> updatedPropertyAccess;
    private final List<PropertyAccessLayer> insertedPropertyAccess;
    private final List<PropertyAccessLayer> deletedPropertyAccess;
    private final List<ValueAccessLayer> updatedValueAccess;
    private final List<ValueAccessLayer> insertedValueAccess;
    private final List<ValueAccessLayer> deletedValueAccess;
    private boolean recordAccessUpdated;

    public T8DataRecordAccessUpdate(DataRecordAccessLayer accessLayer)
    {
        this.accessLayer = accessLayer;
        this.recordAccessUpdated = false;
        this.updatedPropertyAccess = new ArrayList<>();
        this.insertedPropertyAccess = new ArrayList<>();
        this.deletedPropertyAccess = new ArrayList<>();
        this.updatedValueAccess = new ArrayList<>();
        this.insertedValueAccess = new ArrayList<>();
        this.deletedValueAccess = new ArrayList<>();
    }

    public String getRecordId()
    {
        return this.accessLayer.getRecordId();
    }

    public boolean isRecordAccessUpdated()
    {
        return recordAccessUpdated;
    }

    public void setRecordAccessUpdated(boolean recordAccessUpdated)
    {
        this.recordAccessUpdated = recordAccessUpdated;
    }

    public DataRecordAccessLayer getRecordAccessLayer()
    {
        return this.accessLayer;
    }

    public List<PropertyAccessLayer> getUpdatedPropertyAccess()
    {
        return new ArrayList<>(updatedPropertyAccess);
    }

    public void addUpdatedPropertyAccess(PropertyAccessLayer property)
    {
        this.updatedPropertyAccess.add(property);
    }

    public List<PropertyAccessLayer> getInsertedPropertyAccess()
    {
        return new ArrayList<>(insertedPropertyAccess);
    }

    public void addInsertedPropertyAccess(PropertyAccessLayer property)
    {
        this.insertedPropertyAccess.add(property);
    }

    public List<PropertyAccessLayer> getDeletedPropertyAccess()
    {
        return new ArrayList<>(deletedPropertyAccess);
    }

    public void addDeletedPropertyAccess(PropertyAccessLayer property)
    {
        this.deletedPropertyAccess.add(property);
    }

    public List<ValueAccessLayer> getUpdatedValueAccess()
    {
        return new ArrayList<>(updatedValueAccess);
    }

    public void addUpdatedValueAccess(ValueAccessLayer valueAccess)
    {
        this.updatedValueAccess.add(valueAccess);
    }

    public List<ValueAccessLayer> getInsertedValueAccess()
    {
        return new ArrayList<>(insertedValueAccess);
    }

    public void addInsertedValueAccess(ValueAccessLayer valueAccess)
    {
        this.insertedValueAccess.add(valueAccess);
    }

    public List<ValueAccessLayer> getDeletedValueAccess()
    {
        return new ArrayList<>(deletedValueAccess);
    }

    public void addDeletedValueAccess(ValueAccessLayer value)
    {
        this.deletedValueAccess.add(value);
    }

    public static final T8DataRecordAccessUpdate getAccessUpdate(DataRecordAccessLayer fromState, DataRecordAccessLayer toState)
    {
        T8DataRecordAccessUpdate update;

        // Create the update.
        update = new T8DataRecordAccessUpdate(toState);

        // First check for record access updates.
        update.setRecordAccessUpdated(!fromState.isRecordAccessEquivalent(toState));

        // Check each property in the toState against the corresponding property in the fromState.
        for (PropertyAccessLayer toStateProperty : toState.getPropertyAccessLayers())
        {
            PropertyAccessLayer fromStateProperty;

            // If the toStateProperty is found in the fromStateRecord, it is registered as an update.
            fromStateProperty = fromState.getPropertyAccessLayer(toStateProperty.getPropertyId());
            if (fromStateProperty != null)
            {
                ValueAccessLayer fromStateValue;
                ValueAccessLayer toStateValue;

                // If the property access changed, register the toStateProperty as an update.
                if (!fromStateProperty.isPropertyAccessEquivalent(toStateProperty))
                {
                    update.addUpdatedPropertyAccess(toStateProperty);
                }

                // Check for a value access update.
                fromStateValue = fromStateProperty.getValueAccessLayer();
                toStateValue = toStateProperty.getValueAccessLayer();
                if (!fromStateValue.isValueAccessEquivalent(toStateValue))
                {
                    update.addUpdatedValueAccess(toStateValue);
                }
            }
            else
            {
                // The toStateProperty was not found in the fromState, so it is registered as an insert.
                update.addInsertedPropertyAccess(toStateProperty);
                update.addInsertedValueAccess(toStateProperty.getValueAccessLayer());
            }
        }

        // Find all deleted properties (properties present in the fromState but not the toState).
        for (PropertyAccessLayer fromStateProperty : fromState.getPropertyAccessLayers())
        {
            // If the fromStateProperty does not exist in the toState, it is registered as a deletion.
            if (toState.getPropertyAccessLayer(fromStateProperty.getPropertyId()) == null)
            {
                update.addDeletedPropertyAccess(fromStateProperty);
                update.addDeletedValueAccess(fromStateProperty.getValueAccessLayer());
            }
        }

        // Return the final update.
        return update;
    }
}
