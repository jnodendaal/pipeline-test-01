package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class AttachmentListContent extends ValueContent
{
    private final List<String> attachmentIds;
    private final Map<String, T8AttachmentDetails> attachmentDetails;

    public AttachmentListContent()
    {
        super(RequirementType.ATTACHMENT);
        this.attachmentIds = new ArrayList<String>();
        this.attachmentDetails = new HashMap<>();
    }

    public void addAttachment(String attachmentId)
    {
        if (attachmentId != null)
        {
            if (!attachmentIds.contains(attachmentId))
            {
                // Add the new attachment ID to the list.
                attachmentIds.add(attachmentId);
            }
        }
        else throw new IllegalArgumentException("Cannot add null attachment ID to attachment list: " + this);
    }

    public void addAttachments(Collection<String> newAttachments)
    {
        if (!newAttachments.contains(null))
        {
            for (String newAttachment : newAttachments)
            {
                if (!attachmentIds.contains(newAttachment))
                {
                    attachmentIds.add(newAttachment);
                }
            }
        }
        else throw new IllegalArgumentException("Input list " + newAttachments + " contains a null attachment ID that cannot be added to attachment list: " + this);
    }

    public boolean replaceAttachment(String oldAttachmentId, String newAttachmentId, T8AttachmentDetails newAttachmentFileDetails)
    {
        int index;

        index = attachmentIds.indexOf(oldAttachmentId);
        if (index > -1)
        {
            // Replace the attachment reference.
            attachmentIds.remove(index);
            attachmentIds.add(index, newAttachmentId);

            // Replace the attachment details.
            attachmentDetails.remove(oldAttachmentId);
            attachmentDetails.put(newAttachmentId, newAttachmentFileDetails);
            return true;
        }
        else return false;
    }

    public boolean removeAttachment(String attachmentId)
    {
        if (attachmentIds.contains(attachmentId))
        {
            // Add the new attachment ID to the list.
            attachmentIds.remove(attachmentId);
            return true;
        }
        else return false;
    }

    public void clearAttachments()
    {
        // Clear the list of attachments.
        attachmentIds.clear();
        attachmentDetails.clear();
    }

    public void clearAttachmentDetails()
    {
        attachmentDetails.clear();
    }

    public void addAttachmentDetails(Map<String, T8AttachmentDetails> details)
    {
        for (String attachmentId : details.keySet())
        {
            addAttachmentDetails(attachmentId, details.get(attachmentId));
        }
    }

    public void addAttachmentDetails(String attachmentID, T8AttachmentDetails details)
    {
        attachmentDetails.put(attachmentID, details);
    }

    public Map<String, T8AttachmentDetails> getAttachmentDetails()
    {
        return new HashMap<String, T8AttachmentDetails>(attachmentDetails);
    }

    public T8AttachmentDetails getAttachmentDetails(String attachmentId)
    {
        return attachmentDetails.get(attachmentId);
    }

    public boolean containsAttachment(String attachmentID)
    {
        return attachmentIds.contains(attachmentID);
    }

    public List<String> getAttachmentIdList()
    {
        return new ArrayList<String>(attachmentIds);
    }

    public int getAttachmentCount()
    {
        return attachmentIds.size();
    }

    @Override
    public boolean isEqualTo(ValueContent toValueContent)
    {
        if (toValueContent instanceof AttachmentListContent)
        {
            AttachmentListContent toListContent;
            List<String> toList;

            toListContent = (AttachmentListContent)toValueContent;
            toList = toListContent.getAttachmentIdList();
            return ((attachmentIds.size() == toList.size()) && (attachmentIds.containsAll(toList)));
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toValueContent)
    {
        if (toValueContent instanceof AttachmentListContent)
        {
            AttachmentListContent toListContent;
            List<String> toList;

            toListContent = (AttachmentListContent)toValueContent;
            toList = toListContent.getAttachmentIdList();
            return ((attachmentIds.size() == toList.size()) && (attachmentIds.containsAll(toList)));
        }
        else return false;
    }

    @Override
    public String toString()
    {
        return "AttachmentListContent{" + "attachments=" + attachmentIds + '}';
    }
}
