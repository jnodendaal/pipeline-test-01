package com.pilog.t8.data.document.datastring;

import java.util.Comparator;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringParticlePriorityComparator implements Comparator
{
    @Override
    public int compare(Object o1, Object o2)
    {
        Double p1;
        Double p2;

        p1 = ((T8DataStringParticle)o1).getPriority();
        p2 = ((T8DataStringParticle)o2).getPriority();
        return p1.compareTo(p2);
    }
}
