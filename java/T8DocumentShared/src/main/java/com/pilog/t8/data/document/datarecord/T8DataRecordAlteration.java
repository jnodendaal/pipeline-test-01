package com.pilog.t8.data.document.datarecord;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAlteration
{
    private final DataRecord dataRecord;
    private final List<RecordProperty> insertedProperties;
    private final List<RecordProperty> updatedProperties;
    private final List<RecordProperty> deletedProperties;

    public T8DataRecordAlteration(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
        this.insertedProperties = new ArrayList<>();
        this.updatedProperties = new ArrayList<>();
        this.deletedProperties = new ArrayList<>();
    }

    public DataRecord getDataRecord()
    {
        return this.dataRecord;
    }

    public List<RecordProperty> getInsertedProperties()
    {
        return new ArrayList<>(insertedProperties);
    }

    public void addInsertedProperty(RecordProperty property)
    {
        this.insertedProperties.add(property);
    }

    public List<RecordProperty> getUpdatedProperties()
    {
        return new ArrayList<>(updatedProperties);
    }

    public void addUpdatedProperty(RecordProperty property)
    {
        this.updatedProperties.add(property);
    }

    public List<RecordProperty> getDeletedProperties()
    {
        return new ArrayList<>(deletedProperties);
    }

    public void addDeletedProperty(RecordProperty property)
    {
        this.deletedProperties.add(property);
    }

    public static final T8DataRecordAlteration getDataRecordAlteration(DataRecord fromStateRecord, DataRecord toStateRecord)
    {
        T8DataRecordAlteration alteration;

        // Create the alteration.
        alteration = new T8DataRecordAlteration(toStateRecord);

        // Check each property in the toState against the corresponding property in the fromState.
        for (RecordProperty toStateProperty : toStateRecord.getRecordProperties())
        {
            RecordProperty fromStateProperty;

            // If the toStateProperty is found in the fromStateRecord, it is registered as an update.
            fromStateProperty = fromStateRecord.getRecordProperty(toStateProperty.getPropertyID());
            if (fromStateProperty != null)
            {
                // If any of the content of the property changed, register the toStateProperty as an update.
                if (isUpdate(fromStateProperty, toStateProperty))
                {
                    alteration.addUpdatedProperty(toStateProperty);
                }
            }
            else
            {
                // The toStateProperty was not found in the fromStateRecord, so it is registered as an insert.
                alteration.addInsertedProperty(toStateProperty);
            }
        }

        // Find all deleted properties (properties present in the fromState but not the toState).
        for (RecordProperty fromStateProperty : fromStateRecord.getRecordProperties())
        {
            // If the fromStateProperty does not exist in the toStateRecord, it is registered as a deletion.
            if (toStateRecord.getRecordProperty(fromStateProperty.getPropertyID()) == null)
            {
                alteration.addDeletedProperty(fromStateProperty);
            }
        }

        // Return the final alteration.
        return alteration;
    }

    private static boolean isUpdate(RecordProperty fromState, RecordProperty toState)
    {
        // Check for a difference in content between the states in order to identify an update.
        return !fromState.getContent().isEqualTo(toState.getContent());
    }
}
