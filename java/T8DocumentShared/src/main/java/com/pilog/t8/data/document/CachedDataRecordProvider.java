package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface CachedDataRecordProvider extends DataRecordProvider
{
    public void addDataRecord(DataRecord dataRecord);
    public void clearCache();
    
    public List<DataRecord> getCachedDataRecords();
}
