package com.pilog.t8.data.document.datarecord.access;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordAccessListener extends EventListener
{
    public void accessChanged(T8DataRecordAccessChangedEvent event);
}
