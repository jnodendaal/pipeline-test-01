package com.pilog.t8.data.alteration.type;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlteration.T8DataAlterationMethod;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.datatype.T8DtOntologyConcept;

/**
 * @author Bouwer du Preez
 */
public class T8CompleteConceptAlteration implements T8DataAlteration
{
    private final String packageIid;
    private T8OntologyConcept concept;
    private T8DataAlterationMethod method;
    private int step;

    public static final String ALTERATION_ID = "@ALTERATION_COMPLETE_CONCEPT";

    public T8CompleteConceptAlteration(String packageIid, int step, T8OntologyConcept concept, T8DataAlterationMethod method)
    {
        this.packageIid = packageIid;
        this.step = step;
        this.concept = concept;
        this.method = method;
    }

    public T8CompleteConceptAlteration(JsonObject alterationObject)
    {
        this.packageIid = alterationObject.getString("packageIid");
        this.step = alterationObject.getInteger("step");
        this.method = T8DataAlterationMethod.valueOf(alterationObject.getString("method"));
        this.concept = new T8DtOntologyConcept(null).deserialize(alterationObject.getJsonObject("concept"));
    }

    @Override
    public T8DataAlterationMethod getAlterationMethod()
    {
        return method;
    }

    public void setAlterationMethod(T8DataAlterationMethod method)
    {
        this.method = method;
    }

    public T8OntologyConcept getConcept()
    {
        return concept;
    }

    public void setConcept(T8OntologyConcept concept)
    {
        this.concept = concept;
    }

    @Override
    public String getAlterationId()
    {
        return ALTERATION_ID;
    }

    @Override
    public String getPackageIid()
    {
        return packageIid;
    }

    @Override
    public int getStep()
    {
        return step;
    }

    public void setStep(int step)
    {
        this.step = step;
    }

    @Override
    public JsonObject serialize()
    {
        JsonObject conceptAlterationObject;
        JsonObject conceptObject;
        T8DtOntologyConcept ontologyConceptDataType;

        ontologyConceptDataType = new T8DtOntologyConcept(null);

        // Retrieve the concept json object.
        conceptObject = (JsonObject) ontologyConceptDataType.serialize(concept);

        // Create Json object of the complete concept alteration.
        conceptAlterationObject = new JsonObject();
        conceptAlterationObject.add("alterationId", ALTERATION_ID);
        conceptAlterationObject.add("packageIid", packageIid);
        conceptAlterationObject.add("method", method.toString());
        conceptAlterationObject.add("step", step);
        conceptAlterationObject.add("concept", conceptObject);

        // Return the complete concept alteration object.
        return conceptAlterationObject;
    }
}
