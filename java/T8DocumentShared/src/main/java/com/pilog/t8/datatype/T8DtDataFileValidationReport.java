package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.document.validation.T8RecordValueValidationError;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileValidationReport extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_VALIDATION_REPORT";

    public T8DtDataFileValidationReport() {}

    public T8DtDataFileValidationReport(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileValidationReport.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFileValidationReport report;
            JsonObject reportObject;
            JsonArray reportArray;

            // Create the validation report object.
            report = (T8DataFileValidationReport)object;
            reportObject = new JsonObject();
            reportArray = new JsonArray();
            reportObject.add("recordReports", reportArray);

            // Add a record report for each record in the validation report.
            for (String recordId : report.getRecordIdList())
            {
                List<T8DataValidationError> errors;
                JsonObject recordObject;
                JsonArray valueErrorArray;

                // Create the record report object.
                recordObject = new JsonObject();
                recordObject.add("recordId", recordId);
                valueErrorArray = new JsonArray();
                recordObject.add("valueErrors", valueErrorArray);

                // Serialize value errors.
                errors = report.getValidationErrors(recordId);
                for (T8DataValidationError error : errors)
                {
                    if (error instanceof T8RecordValueValidationError)
                    {
                        T8RecordValueValidationError valueError;
                        JsonObject errorObject;

                        // Create the value error object.
                        valueError = (T8RecordValueValidationError)error;
                        errorObject = new JsonObject();
                        errorObject.add("recordId", valueError.getRecordId());
                        errorObject.add("propertyId", valueError.getPropertyId());
                        errorObject.addIfNotNull("fieldId", valueError.getFieldId());
                        errorObject.add("message", valueError.getErrorMessage());

                        // Add the value error object to the array in the record report.
                        valueErrorArray.add(errorObject);
                    }
                }

                // Add the record report to the array in the validation report object.
                reportArray.add(recordObject);
            }

            // Serialize the validation report.
            return reportObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileValidationReport deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DataFileValidationReport report;
            JsonObject object;

            object = (JsonObject)jsonValue;
            report = new T8DataFileValidationReport();

            // TODO:  Implement deserialization of report.
            return report;
        }
        else return null;
    }
}
