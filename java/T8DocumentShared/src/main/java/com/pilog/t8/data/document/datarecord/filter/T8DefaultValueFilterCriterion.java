package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultValueFilterCriterion extends T8ValueFilterCriterion
{
    public T8DefaultValueFilterCriterion(RequirementType dataType)
    {
        super(dataType);
    }

    public T8DefaultValueFilterCriterion(RequirementType dataType, T8DataFilterCriterion.DataFilterOperator operator, Object filterValue)
    {
        super(dataType, operator, filterValue);
    }

    @Override
    public T8ValueFilterCriterion copy()
    {
        T8ValueFilterCriterion copiedCriterion;

        copiedCriterion = new T8DefaultValueFilterCriterion(dataType, operator, filterValue);
        return copiedCriterion;
    }

    @Override
    public String getRequiredCteId()
    {
        // Determine the CTE from which the sub-query will retrieve as well as the value columns that will be used.
        if (operator == DataFilterOperator.IS_NULL)
        {
            return T8RecordFilterCriteria.CTE_NULL_VALUES_IDENTIFIER;
        }
        else if ((filterValue instanceof String) && (T8IdentifierUtilities.isConceptID((String)filterValue)))
        {
            return T8RecordFilterCriteria.CTE_VALUE_CONCEPT_IDS_IDENTIFIER;
        }
        else
        {
            return T8RecordFilterCriteria.CTE_VALUE_TERMS_IDENTIFIER;
        }
    }

    @Override
    public StringBuffer getWhereClause(T8DataTransaction tx, T8DatabaseAdaptor adaptor)
    {
        List<String> valueColumns;
        List<Boolean> valueColumnGuidFlags;
        StringBuffer whereClause;
        boolean prefixAnd;

        // Add all of the property criteria.
        whereClause = new StringBuffer();
        prefixAnd = false;

        // Determine the CTE from which the sub-query will retrieve as well as the value columns that will be used.
        if (operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL)
        {
            valueColumns = new ArrayList<>();
            valueColumnGuidFlags = new ArrayList<>();
        }
        else if ((filterValue instanceof String) && (T8IdentifierUtilities.isConceptID((String)filterValue)))
        {
            valueColumns = Arrays.asList("V.VALUE_CONCEPT_ID");
            valueColumnGuidFlags = Arrays.asList(true);
        }
        else
        {
            // Set the columns that will be checked using the search string.
            // If the valueColumns list is updated, the corresponding value column count in method {@code getWhereClauseParameters} must also be updated.
            valueColumns = Arrays.asList("V.VALUE", "V.TEXT", "V.TERM", "V.CODE");
            valueColumnGuidFlags = Arrays.asList(false, false, false, false);
        }

        // Append the value columns.
        if (valueColumns.size() > 0)
        {
            boolean negationOperator;

            // Set flag indicating negation operators.
            negationOperator = (operator == T8DataFilterCriterion.DataFilterOperator.NOT_EQUAL) || (operator == T8DataFilterCriterion.DataFilterOperator.NOT_LIKE);

            if (prefixAnd) whereClause.append(" AND ");
            whereClause.append("(");
            for (int valueColumnIndex = 0; valueColumnIndex < valueColumns.size(); valueColumnIndex++)
            {
                String valueColumn;
                boolean guidValue;

                // Get the value column name and if this is not the first value column, prepend an OR/AND to subsequent value column names.
                valueColumn = valueColumns.get(valueColumnIndex);
                guidValue = valueColumnGuidFlags.get(valueColumnIndex);
                if (valueColumnIndex > 0)
                {
                    if (negationOperator)
                    {
                        whereClause.append(" AND ");
                    }
                    else
                    {
                        whereClause.append(" OR ");
                    }
                }

                // Append the where clause for the value column.
                whereClause.append(getValueFilterWhereClause(tx, adaptor, valueColumn, operator, filterValue, guidValue));
            }

            // Close the value-column clause.
            whereClause.append(")");
        }

        // Return the completed where clause.
        return whereClause;
    }

    @Override
    public List<Object> getWhereClauseParameters(T8DataTransaction tx, T8DatabaseAdaptor adaptor)
    {
        List<Boolean> valueColumnGuidFlags;
        ArrayList<Object> parameterList;
        int valueColumnCount;

        // Add property criteria parameters.
        parameterList = new ArrayList<>();

        // Determine the number of value columns to be used.
        if (operator == DataFilterOperator.IS_NULL)
        {
            valueColumnCount = 0;
            valueColumnGuidFlags = new ArrayList<>();
        }
        else if ((filterValue instanceof String) && (T8IdentifierUtilities.isConceptID((String)filterValue)))
        {
            valueColumnCount = 1;
            valueColumnGuidFlags = Arrays.asList(true);
        }
        else
        {
            valueColumnCount = 4;
            valueColumnGuidFlags = Arrays.asList(false, false, false, false);
        }

         // This value must correspond to the size of the value columns list in method {@code getWhereClause}.
        for (int valueColumnIndex = 0; valueColumnIndex < valueColumnCount; valueColumnIndex++)
        {
            boolean guidValue;

            guidValue = valueColumnGuidFlags.get(valueColumnIndex);
            parameterList.addAll(getValueFilterParameters(tx, adaptor, operator, filterValue, guidValue, caseInsensitive));
        }

        // Return the complete parameter list.
        return parameterList;
    }
}
