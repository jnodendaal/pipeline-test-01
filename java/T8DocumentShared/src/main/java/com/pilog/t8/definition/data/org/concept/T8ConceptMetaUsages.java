/**
 * Created on 07 Aug 2015, 7:31:09 AM
 */
package com.pilog.t8.definition.data.org.concept;

import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public class T8ConceptMetaUsages implements Serializable
{
    private static final long serialVersionUID = 5721214954398082823L;

    private final T8OntologyConceptType conceptType;
    private final String conceptID;

    private List<T8DefinitionHandle> usageDefinitionHandles;

    public T8ConceptMetaUsages(String conceptID, T8OntologyConceptType conceptType)
    {
        this.conceptID = conceptID;
        this.conceptType = conceptType;
    }

    /**
     * Gets the concept {@code T8OntologyConceptType} as determined from the
     * concept ID. This is to provide addition detail when processing usage
     * results.
     *
     * @return The {@code T8OntologyConceptType} for the concept ID
     */
    public T8OntologyConceptType getConceptType()
    {
        return this.conceptType;
    }

    /**
     * The {@code String} concept ID for which the usages were found.
     *
     * @return The {@code String} concept ID
     */
    public String getConceptID()
    {
        return this.conceptID;
    }

    /**
     * Sets the {@code List} of definition identifiers which each have a
     * reference to the specified concept ID. This will reference the original
     * {@code List}.
     *
     * @param definitionHandles The {@code List} of definition identifiers
     */
    public void setMetaUsages(List<T8DefinitionHandle> definitionHandles)
    {
        this.usageDefinitionHandles = definitionHandles;
    }

    /**
     * Returns a {@code List} of definition identifiers which each have a
     * reference to the specified concept ID. The reference to the list is
     * returned, which means that any changes to the list will be reflected
     * here.
     *
     * @return The {@code List} of definition identifiers
     */
    public List<T8DefinitionHandle> getUsageDefinitionIdentifiers()
    {
        return this.usageDefinitionHandles;
    }

    /**
     * Convenience method to check whether or not the list of definition
     * identifiers is empty. A non-empty list means that there are usages for
     * the specified concept ID.
     *
     * @return {@code true} if the list of definition identifier is not empty.
     *      {@code false} if it is
     */
    public boolean hasUsages()
    {
        return this.usageDefinitionHandles != null && !this.usageDefinitionHandles.isEmpty();
    }

    /**
     * Convenience method to determine the number of usages which is exactly
     * equivalent to the number of definition identifier in the contained list.
     *
     * @return The {@code int} number of definitions referencing the associated
     *      concept ID
     */
    public int getUsageCount()
    {
        return !hasUsages() ? 0 : this.usageDefinitionHandles.size();
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8ConceptMetaUsages{");
        toStringBuilder.append("conceptType=").append(this.conceptType);
        toStringBuilder.append(",conceptID=").append(this.conceptID);
        toStringBuilder.append(",usageDefinitionIdentifiers=").append(this.usageDefinitionHandles);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}