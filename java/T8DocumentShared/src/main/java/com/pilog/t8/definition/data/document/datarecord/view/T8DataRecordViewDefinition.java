package com.pilog.t8.definition.data.document.datarecord.view;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordViewDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_VIEW";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_RECORD_VIEW";
    public static final String DISPLAY_NAME = "Data Record View";
    public static final String STORAGE_PATH = "/data_record_views";
    public static final String DESCRIPTION = "A definition of a view containing data extracted from data records.";
    public static final String IDENTIFIER_PREFIX = "VIEW_";
    public enum Datum {DR_INSTANCE_IDENTIFIERS,
                       DR_IDENTIFIERS,
                       DATA_FILE_DR_INSTANCE_IDENTIFIERS,
                       DATA_FILE_DR_IDENTIFIERS,
                       LANGUAGE_IDENTIFIERS,
                       DATA_ENTITY_IDENTIFIER,
                       REFRESH_TRIGGER,
                       CONSTRUCTOR_DEFINITIONS,
                       CONSTRUCTOR_FIELD_MAPPING};
    // -------- Definition Meta-Data -------- //

    public enum ViewRefreshTrigger {CONTENT, CONTENT_OR_DESCENDANT, LINEAGE, FILE};

    public T8DataRecordViewDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DR_INSTANCE_IDENTIFIERS.toString(), "DR Instance Identifiers",  "The DR Instances applicable to this view."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DR_IDENTIFIERS.toString(), "DR Identifiers",  "The DR's applicable to this view."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DATA_FILE_DR_INSTANCE_IDENTIFIERS.toString(), "Data File DR Instance Identifiers",  "The Data File DR Instances applicable to this view (if empty, no filter will be applied)."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.DATA_FILE_DR_IDENTIFIERS.toString(), "Data File DR Identifiers",  "The Data File DR's applicable to this view (if empty, no filter will be applied)."));
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.STRING), Datum.LANGUAGE_IDENTIFIERS.toString(), "Language Identifiers",  "The languages applicable to this view."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REFRESH_TRIGGER.toString(), "Refresh Trigger",  "The trigger that will cause this view to be refreshed.", ViewRefreshTrigger.CONTENT_OR_DESCENDANT.toString(), T8DefinitionDatumType.T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity", "The identifier of the data entity to which the view data will be written."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.CONSTRUCTOR_DEFINITIONS.toString(), "Constructors",  "The constructors used to extract and compile data from data files into this view."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.CONSTRUCTOR_FIELD_MAPPING.toString(), "Field Mapping:  Constructor Values to Entity", "A mapping of constructor output values to the corresponding entity fields of the view."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if (Datum.REFRESH_TRIGGER.toString().equals(datumIdentifier)) return createStringOptions(ViewRefreshTrigger.values());
        else if (Datum.CONSTRUCTOR_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordViewConstructorDefinition.GROUP_IDENTIFIER));
        else if (Datum.CONSTRUCTOR_FIELD_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataRecordViewConstructorDefinition> constructorDefinitions;
                    List<T8DataEntityFieldDefinition> fieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    constructorDefinitions = getConstructorDefinitions();
                    fieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((constructorDefinitions != null) && (fieldDefinitions != null))
                    {
                        ArrayList<String> fieldIdentifierList;

                        fieldIdentifierList = new ArrayList<String>();
                        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            fieldIdentifierList.add(fieldDefinition.getPublicIdentifier());
                        }
                        Collections.sort(fieldIdentifierList);

                        for (T8DataRecordViewConstructorDefinition constructorDefinition : constructorDefinitions)
                        {
                            for (String outputParameterIdentifier : constructorDefinition.getOutputParameterIdentifiers())
                            {
                                identifierMap.put(outputParameterIdentifier, fieldIdentifierList);
                            }
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public T8DefinitionTestHarness getTestHarness(T8ClientContext clientContext, T8SessionContext sessionContext)
    {
        try
        {
            T8DefinitionTestHarness testHarness;
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.developer.definitions.test.harness.datarecordview.T8DataRecordViewTestHarness").getConstructor();
            testHarness = (T8DefinitionTestHarness)constructor.newInstance();
            return testHarness;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public List<String> getLanguageIdentifiers()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.LANGUAGE_IDENTIFIERS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setLanguageIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.LANGUAGE_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getDrIdentifiers()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DR_IDENTIFIERS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDrIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DR_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getDrInstanceIdentifiers()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DR_INSTANCE_IDENTIFIERS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDrInstanceIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getDataFileDrIdentifiers()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DATA_FILE_DR_IDENTIFIERS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDataFileDrIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DATA_FILE_DR_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getDataFileDrInstanceIdentifiers()
    {
        List<String> identifiers;

        identifiers = (List<String>)getDefinitionDatum(Datum.DATA_FILE_DR_INSTANCE_IDENTIFIERS.toString());
        return identifiers != null ? identifiers : new ArrayList<String>();
    }

    public void setDataFileDrInstanceIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DATA_FILE_DR_INSTANCE_IDENTIFIERS.toString(), identifiers);
    }

    public String getDataEntityIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setDataEntityIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), identifier);
    }

    public List<T8DataRecordViewConstructorDefinition> getConstructorDefinitions()
    {
        return getDefinitionDatum(Datum.CONSTRUCTOR_DEFINITIONS.toString());
    }

    public void setConstructorDefinitions(List<T8DataRecordViewConstructorDefinition> definitions)
    {
        setDefinitionDatum(Datum.CONSTRUCTOR_DEFINITIONS.toString(), definitions);
    }

    public Map<String, String> getConstructorFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.CONSTRUCTOR_FIELD_MAPPING.toString());
    }

    public void setConstructorFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.CONSTRUCTOR_FIELD_MAPPING.toString(), mapping);
    }

    public ViewRefreshTrigger getViewRefreshTrigger()
    {
        String value;

        value = (String)getDefinitionDatum(Datum.REFRESH_TRIGGER.toString());
        return value != null ? ViewRefreshTrigger.valueOf(value) : ViewRefreshTrigger.CONTENT_OR_DESCENDANT;
    }

    public void setViewRefreshTrigger(ViewRefreshTrigger trigger)
    {
        setDefinitionDatum(Datum.REFRESH_TRIGGER.toString(), trigger.toString());
    }
}
