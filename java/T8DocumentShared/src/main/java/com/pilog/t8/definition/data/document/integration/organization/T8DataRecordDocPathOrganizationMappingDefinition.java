package com.pilog.t8.definition.data.document.integration.organization;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8DataRecordDocPathOrganizationMappingDefinition extends T8DataRecordOrganizationMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_DATA_RECORD_ORG__DOC_PATH";
    public static final String DISPLAY_NAME = "Data Record Doc Path Organization Mapping";
    public static final String DESCRIPTION = "Data Record Doc Path Organization Mapping";
    public static final String IDENTIFIER_PREFIX = "DRDPOM_";
    
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public T8DataRecordDocPathOrganizationMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        return super.getDatumTypes();
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
}
