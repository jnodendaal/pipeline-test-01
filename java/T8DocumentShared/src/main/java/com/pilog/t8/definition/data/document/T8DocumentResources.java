package com.pilog.t8.definition.data.document;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementInstanceDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentResources implements T8DefinitionResource
{
    // Entity fields defined by the API for entities belonging to the organization structure.
    public static final String EF_PROPERTY_ID = "$PROPERTY_ID";
    public static final String EF_FIELD_ID = "$FIELD_ID";
    public static final String EF_CLASS_ID = "$CLASS_ID";
    public static final String EF_CONCEPT_ID = "$CONCEPT_ID";
    public static final String EF_IRDI = "$IRDI";
    public static final String EF_CONCEPT_TYPE_ID = "$CONCEPT_TYPE_ID";
    public static final String EF_CONCEPT_GRAPH_ID = "$CONCEPT_GRAPH_ID";
    public static final String EF_TAIL_CONCEPT_ID = "$TAIL_CONCEPT_ID";
    public static final String EF_HEAD_CONCEPT_ID = "$HEAD_CONCEPT_ID";
    public static final String EF_TERM_ID = "$TERM_ID";
    public static final String EF_CONCEPT_TERM_ID = "$CONCEPT_TERM_ID";
    public static final String EF_ABBREVIATION_ID = "$ABBREVIATION_ID";
    public static final String EF_CONCEPT_ABBREVIATION_ID = "$CONCEPT_ABBREVIATION_ID";
    public static final String EF_DEFINITION_ID = "$DEFINITION_ID";
    public static final String EF_CONCEPT_DEFINITION_ID = "$CONCEPT_DEFINITION_ID";
    public static final String EF_TERM = "$TERM";
    public static final String EF_CONCEPT_TERM = "$CONCEPT_TERM";
    public static final String EF_ABBREVIATION = "$ABBREVIATION";
    public static final String EF_CONCEPT_ABBREVIATION = "$CONCEPT_ABBREVIATION";
    public static final String EF_DEFINITION = "$DEFINITION";
    public static final String EF_CONCEPT_DEFINITION = "$CONCEPT_DEFINITION";
    public static final String EF_CODE = "$CODE";
    public static final String EF_CONCEPT_CODE = "$CONCEPT_CODE";
    public static final String EF_CODE_ID = "$CODE_ID";
    public static final String EF_CONCEPT_CODE_ID = "$CONCEPT_CODE_ID";
    public static final String EF_CODE_TYPE_ID = "$CODE_TYPE_ID";
    public static final String EF_LANGUAGE_ID = "$LANGUAGE_ID";
    public static final String EF_NAME = "$NAME";
    public static final String EF_PRIMARY_INDICATOR = "$PRIMARY_INDICATOR";
    public static final String EF_ACTIVE_INDICATOR = "$ACTIVE_INDICATOR";
    public static final String EF_STANDARD_INDICATOR = "$STANDARD_INDICATOR";
    public static final String EF_APPROVED_INDICATOR = "$APPROVED_INDICATOR";
    public static final String EF_LFT = "$LFT";
    public static final String EF_RGT = "$RGT";
    public static final String EF_ODS_ID = "$ODS_ID";
    public static final String EF_ODT_ID = "$ODT_ID";
    public static final String EF_ODT_TERM = "$ODT_TERM";
    public static final String EF_ODT_DEFINITION = "$ODT_DEFINITION";
    public static final String EF_PARENT_ODT_ID = "$PARENT_ODT_ID";
    public static final String EF_ORG_ID = "$ORG_ID";
    public static final String EF_PATH_ORG_ID = "$PATH_ORG_ID";
    public static final String EF_ROOT_ORG_ID = "$ROOT_ORG_ID";
    public static final String EF_ORG_LEVEL = "$ORG_LEVEL";
    public static final String EF_ORG_TYPE_ID = "$ORG_TYPE_ID";
    public static final String EF_DATA_TYPE_NAME = "$DATA_TYPE_NAME";
    public static final String EF_CONCEPT_ODT_ID = "$CONCEPT_ODT_ID";
    public static final String EF_PARENT_ORG_ID = "$PARENT_ORG_ID";
    public static final String EF_TAGS = "$TAGS";
    public static final String EF_ATTRIBUTE_ID = "$ATTRIBUTE_ID";
    public static final String EF_TYPE = "$TYPE";
    public static final String EF_DR_ID = "$DR_ID";
    public static final String EF_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String EF_COMMENT_ID = "$COMMENT_ID";
    public static final String EF_COMMENT_TYPE_ID = "$COMMENT_TYPE_ID";
    public static final String EF_COMMENT = "$COMMENT";
    public static final String EF_CONDITION_EXPRESSION = "$CONDITION_EXPRESSION";
    public static final String EF_DATA_KEY_EXPRESSION = "$DATA_KEY_EXPRESSION";
    public static final String EF_INDEPENDENT_INDICATOR = "$INDEPENDENT_INDICATOR";
    public static final String EF_ROOT_RECORD_ID = "$ROOT_RECORD_ID";
    public static final String EF_RECORD_ID = "$RECORD_ID";
    public static final String EF_RECORD_ID1 = "$RECORD_ID1";
    public static final String EF_RECORD_ID2 = "$RECORD_ID2";
    public static final String EF_MASTER_RECORD_ID = "$MASTER_RECORD_ID";
    public static final String EF_FAMILY_ID = "$FAMILY_ID";
    public static final String EF_FAMILY_CODE = "$FAMILY_CODE";
    public static final String EF_RESOLVED_STATE = "$RESOLVED_STATE";
    public static final String EF_RESOLUTION_ID = "$RESOLUTION_ID";
    public static final String EF_SUBMITTED_AT = "$SUBMITTED_AT";
    public static final String EF_SUBMITTED_BY = "$SUBMITTED_BY";
    public static final String EF_FINALIZED_AT = "$FINALIZED_AT";
    public static final String EF_FINALIZED_BY = "$FINALIZED_BY";
    public static final String EF_FINALIZED_INDICATOR = "$FINALIZED_INDICATOR";
    public static final String EF_INSTANCE_ID = "$INSTANCE_ID";
    public static final String EF_MATCH_INSTANCE_ID = "$MATCH_INSTANCE_ID";
    public static final String EF_CRITERIA_ID = "$CRITERIA_ID";
    public static final String EF_CASE_ID = "$CASE_ID";
    public static final String EF_PARENT_RECORD_ID = "$PARENT_RECORD_ID";
    public static final String EF_CHILD_RECORD_ID = "$CHILD_RECORD_ID";
    public static final String EF_VALUE_STRING = "$VALUE_STRING";
    public static final String EF_ATTACHMENT_ID = "$ATTACHMENT_ID";
    public static final String EF_FILE_CONTEXT_ID = "$FILE_CONTEXT_ID";
    public static final String EF_FILE_DATA = "$FILE_DATA";
    public static final String EF_FILE_NAME = "$FILE_NAME";
    public static final String EF_MEDIA_TYPE = "$MEDIA_TYPE";
    public static final String EF_FILE_SIZE = "$FILE_SIZE";
    public static final String EF_MD5_CHECKSUM = "$MD5_CHECKSUM";
    public static final String EF_DATA_RECORD_ID = "$ID";
    public static final String EF_DATA_RECORD_DOCUMENT = "$DOCUMENT";
    public static final String EF_DATA_REQUIREMENT_ID = "$ID";
    public static final String EF_DATA_REQUIREMENT_PARENT_ID = "$PARENT_DR_ID";
    public static final String EF_DATA_REQUIREMENT_CHILD_ID = "$CHILD_DR_ID";
    public static final String EF_DATA_REQUIREMENT_INSTANCE_ID = "$ID";
    public static final String EF_DATA_REQUIREMENT_INSTANCE_DOCUMENT = "$DOCUMENT";
    public static final String EF_DATA_REQUIREMENT_INSTANCE_PARENT_ID = "$PARENT_DR_INSTANCE_ID";
    public static final String EF_DATA_REQUIREMENT_INSTANCE_CHILD_ID = "$CHILD_DR_INSTANCE_ID";
    public static final String EF_DATA_REQUIREMENT_DOCUMENT = "$DOCUMENT";
    public static final String EF_IDENTIFIER = "$IDENTIFIER";
    public static final String EF_DATA_STRING = "$DATA_STRING";
    public static final String EF_DATA_STRING_TYPE_ID = "$DATA_STRING_TYPE_ID";
    public static final String EF_DATA_STRING_FORMAT_ID = "$DATA_STRING_FORMAT_ID";
    public static final String EF_DATA_STRING_INSTANCE_ID = "$DATA_STRING_INSTANCE_ID";
    public static final String EF_AUTO_RENDER = "$AUTO_RENDER";
    public static final String EF_DATA_TYPE_ID = "$DATA_TYPE_ID";
    public static final String EF_PARENT_DATA_TYPE_ID = "$PARENT_DATA_TYPE_ID";
    public static final String EF_DATA_TYPE_SEQUENCE = "$DATA_TYPE_SEQUENCE";
    public static final String EF_VALUE_SEQUENCE = "$VALUE_SEQUENCE";
    public static final String EF_SEQUENCE = "$SEQUENCE";
    public static final String EF_VALUE = "$VALUE";
    public static final String EF_VALUE_CONCEPT_ID = "$VALUE_CONCEPT_ID";
    public static final String EF_LOWER_BOUND_VALUE = "$LOWER_BOUND_VALUE";
    public static final String EF_UPPER_BOUND_VALUE = "$UPPER_BOUND_VALUE";
    public static final String EF_STANDARD_VALUE_ID = "$STANDARD_VALUE_ID";
    public static final String EF_VALUE_ID = "$VALUE_ID";
    public static final String EF_OLD_VALUE_ID = "$OLD_VALUE_ID";
    public static final String EF_UOM_ID = "$UOM_ID";
    public static final String EF_UOM_TERM = "$UOM_TERM";
    public static final String EF_QOM_ID = "$QOM_ID";
    public static final String EF_QOM_TERM = "$QOM_TERM";
    public static final String EF_LOWER_BOUND_VALUE_ID = "$LOWER_BOUND_VALUE_ID";
    public static final String EF_UPPER_BOUND_VALUE_ID = "$UPPER_BOUND_VALUE_ID";
    public static final String EF_PACKAGE_IID = "$ALTERATION_IID";
    public static final String EF_PACKAGE_ID = "$ALTERATION_ID";
    public static final String EF_ALTERATION_ID = "$ALTERATION_ID";
    public static final String EF_ALTERATION_TYPE = "$ALTERATION_TYPE";
    public static final String EF_DATA_TYPE = "$DATA_TYPE";
    public static final String EF_STEP = "$STEP";
    public static final String EF_SIZE = "$SIZE";
    public static final String EF_INSERTED_AT = "$INSERTED_AT";
    public static final String EF_UPDATED_AT = "$UPDATED_AT";
    public static final String EF_OLD_VALUE = "$OLD_VALUE";
    public static final String EF_OLD_LOWER_BOUND_VALUE = "$OLD_LOWER_BOUND_VALUE";
    public static final String EF_OLD_UPPER_BOUND_VALUE = "$OLD_UPPER_BOUND_VALUE";
    public static final String EF_OLD_CONCEPT_ID = "$OLD_CONCEPT_ID";
    public static final String EF_OLD_CONCEPT_TERM = "$OLD_CONCEPT_TERM";
    public static final String EF_OLD_UOM_ID = "$OLD_UOM_ID";
    public static final String EF_OLD_UOM_TERM = "$OLD_UOM_TERM";
    public static final String EF_OLD_QOM_ID = "$OLD_QOM_ID";
    public static final String EF_OLD_QOM_TERM = "$OLD_QOM_TERM";
    public static final String EF_PHRASE = "$PHRASE";
    public static final String EF_INSERTED_BY_ID = "$INSERTED_BY_ID";
    public static final String EF_INSERTED_BY_IID = "$INSERTED_BY_IID";
    public static final String EF_UPDATED_BY_ID = "$UPDATED_BY_ID";
    public static final String EF_UPDATED_BY_IID = "$UPDATED_BY_IID";
    public static final String EF_ASYNC_ONT_UPD_TIME = "$ASYNC_ONT_UPD_TIME";
    public static final String EF_ASYNC_ATR_UPD_TIME = "$ASYNC_ATR_UPD_TIME";
    public static final String EF_PROPERTY_COUNT = "$PROPERTY_COUNT";
    public static final String EF_PROPERTY_DATA_COUNT = "$PROPERTY_DATA_COUNT";
    public static final String EF_PROPERTY_VALID_COUNT = "$PROPERTY_VALID_COUNT";
    public static final String EF_PROPERTY_VERIFICATION_COUNT = "$PROPERTY_VERIFICATION_COUNT";
    public static final String EF_PROPERTY_ACCURATE_COUNT = "$PROPERTY_ACCURATE_COUNT";
    public static final String EF_CHRCTRSTC_COUNT = "$CHRCTRSTC_COUNT";
    public static final String EF_CHRCTRSTC_DATA_COUNT = "$CHRCTRSTC_DATA_COUNT";
    public static final String EF_CHRCTRSTC_VALID_COUNT = "$CHRCTRSTC_VALID_COUNT";
    public static final String EF_CHRCTRSTC_VERIFICATION_COUNT = "$CHRCTRSTC_VERIFICATION_COUNT";
    public static final String EF_CHRCTRSTC_ACCURATE_COUNT = "$CHRCTRSTC_ACCURATE_COUNT";

    public static final String DATA_ALTERATION_PACKAGE_INDEX_DE_IDENTIFIER = "@E_DATA_ALTERATION_PACKAGE_INDEX";
    public static final String ONTOLOGY_CONCEPT_DE_IDENTIFIER = "@E_ONTOLOGY_CONCEPT";
    public static final String ONTOLOGY_CONCEPT_DS_IDENTIFIER = "@DS_ONTOLOGY_CONCEPT";
    public static final String ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER = "@E_ONTOLOGY_CONCEPT_GRAPH";
    public static final String ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER = "@DS_ONTOLOGY_CONCEPT_GRAPH";
    public static final String ONTOLOGY_TERM_DE_IDENTIFIER = "@E_ONTOLOGY_TERM";
    public static final String ONTOLOGY_TERM_DS_IDENTIFIER = "@DS_ONTOLOGY_TERM";
    public static final String ONTOLOGY_CODE_DE_IDENTIFIER = "@E_ONTOLOGY_CODE";
    public static final String ONTOLOGY_CODE_DS_IDENTIFIER = "@DS_ONTOLOGY_CODE";
    public static final String ONTOLOGY_DEFINITION_DE_IDENTIFIER = "@E_ONTOLOGY_DEFINITION";
    public static final String ONTOLOGY_DEFINITION_DS_IDENTIFIER = "@DS_ONTOLOGY_DEFINITION";
    public static final String ONTOLOGY_ABBREVIATION_DE_IDENTIFIER = "@E_ONTOLOGY_ABBREVIATION";
    public static final String ONTOLOGY_ABBREVIATION_DS_IDENTIFIER = "@DS_ONTOLOGY_ABBREVIATION";
    public static final String ONTOLOGY_PHRASE_ABBREVIATION_DE_IDENTIFIER = "@E_ONTOLOGY_PHRASE_ABBREVIATION";
    public static final String ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER = "@DS_ONTOLOGY_PHRASE_ABBREVIATION";
    public static final String ONTOLOGY_CLASS_DE_IDENTIFIER = "@E_ONTOLOGY_CLASS";
    public static final String ONTOLOGY_CLASS_DS_IDENTIFIER = "@DS_ONTOLOGY_CLASS";
    public static final String DATA_STRING_DE_IDENTIFIER = "@E_DATA_STRING";
    public static final String DATA_STRING_DETAILS_DE_IDENTIFIER = "@E_DATA_STRING_DETAILS";
    public static final String DATA_STRING_DETAILS_DS_IDENTIFIER = "@DS_DATA_STRING_DETAILS";
    public static final String DATA_STRING_INSTANCE_LINK_DETAILS_DE_IDENTIFIER = "@E_DATA_STRING_INSTANCE_LINK_DETAILS";
    public static final String DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER = "@DS_DATA_STRING_INSTANCE_LINK_DETAILS";
    public static final String DATA_STRING_INSTANCE_DETAILS_DE_IDENTIFIER = "@E_DATA_STRING_INSTANCE_DETAILS";
    public static final String DATA_RECORD_DE_IDENTIFIER = "@E_DATA_RECORD_DOC";
    public static final String DATA_RECORD_DS_IDENTIFIER = "@DS_DATA_RECORD_DOC";
    public static final String DATA_RECORD_DOC_ATR_DE_IDENTIFIER = "@E_DATA_RECORD_DOC_ATR";
    public static final String DATA_RECORD_DOC_ATR_DS_IDENTIFIER = "@DS_DATA_RECORD_DOC_ATR";
    public static final String DATA_RECORD_PROPERTY_DE_IDENTIFIER = "@E_DATA_RECORD_PROPERTY";
    public static final String DATA_RECORD_PROPERTY_DS_IDENTIFIER = "@DS_DATA_RECORD_PROPERTY";
    public static final String DATA_RECORD_PROPERTY_DATA_TYPE_DE_IDENTIFIER = "@E_COMMUNICATION_EMAIL_SERVICE_EDITOR_DATA_SOURCE";
    public static final String DATA_RECORD_VALUE_DE_IDENTIFIER = "@E_DATA_RECORD_VALUE";
    public static final String DATA_RECORD_VALUE_DS_IDENTIFIER = "@DS_DATA_RECORD_VALUE";
    public static final String DATA_RECORD_CHILDREN_DE_IDENTIFIER = "@E_DATA_RECORD_CHILDREN";
    public static final String DATA_RECORD_CHILDREN_DS_IDENTIFIER = "@DS_DATA_RECORD_CHILDREN";
    public static final String DATA_RECORD_DOCUMENT_DE_IDENTIFIER = "@E_DATA_RECORD_DOCUMENT";
    public static final String DATA_RECORD_DOCUMENT_DS_IDENTIFIER = "@DS_DATA_RECORD_DOCUMENT";
    public static final String DATA_RECORD_DOCUMENT_CLASS_DE_IDENTIFIER = "@E_DATA_RECORD_DOCUMENT_CLASS";
    public static final String DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER = "@DS_DATA_RECORD_DOCUMENT_CLASS";
    public static final String DATA_RECORD_DOCUMENT_ROOT_DE_IDENTIFIER = "@E_DATA_RECORD_DOCUMENT_ROOT";
    public static final String DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER = "@DS_DATA_RECORD_DOCUMENT_ROOT";
    public static final String DATA_RECORD_ATTACHMENT_DE_IDENTIFIER = "@E_DATA_RECORD_ATTACHMENT";
    public static final String DATA_RECORD_ATTACHMENT_DS_IDENTIFIER = "@DS_DATA_RECORD_ATTACHMENT";
    public static final String DATA_RECORD_ATTACHMENT_DETAILS_DE_IDENTIFIER = "@E_DATA_RECORD_ATTACHMENT_DETAILS";
    public static final String DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER = "@DS_DATA_RECORD_ATTACHMENT_DETAILS";
    public static final String DATA_RECORD_MATCH_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH";
    public static final String DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_RESOLVED";
    public static final String STANDARD_VALUE_DE_IDENTIFIER = "@E_COMMUNICATION_EMAIL_SERVICE_EDITOR_DATA_SOURCE";
    public static final String DATA_RECORD_SEARCH_DE_IDENTIFIER = "@E_DATA_RECORD_SEARCH";
    public static final String DATA_RECORD_SEARCH_DS_IDENTIFIER = "@DS_DATA_RECORD_SEARCH";
    public static final String DATA_RECORD_TAG_DE_IDENTIFIER = "@E_DATA_RECORD_TAG";
    public static final String DATA_REQUIREMENT_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_DOC";
    public static final String DATA_REQUIREMENT_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_DOC";
    public static final String DATA_REQUIREMENT_CHILDREN_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_CHILDREN";
    public static final String DATA_REQUIREMENT_CHILDREN_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_CHILDREN";
    public static final String DATA_REQUIREMENT_DATA_TYPE_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_DATA_TYPE";
    public static final String DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_DATA_TYPE";
    public static final String DATA_REQUIREMENT_DESCENDANTS_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_DESCENDANTS";
    public static final String DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_DOCUMENT";
    public static final String DATA_REQUIREMENT_DOCUMENT_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_DOCUMENT";
    public static final String DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_INSTANCE";
    public static final String DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_INSTANCE";
    public static final String DATA_REQUIREMENT_INSTANCE_DESCENDANTS_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_INSTANCE_DESCENDANTS";
    public static final String DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_INSTANCE_DOCUMENT";
    public static final String DATA_REQUIREMENT_INSTANCE_DOCUMENT_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_INSTANCE_DOCUMENT";
    public static final String DATA_REQUIREMENT_PROPERTY_DE_IDENTIFIER = "@E_COMMUNICATION_EMAIL_SERVICE_EDITOR_DATA_SOURCE";
    public static final String DATA_REQUIREMENT_VALUE_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_VALUE";
    public static final String DATA_REQUIREMENT_VALUE_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_VALUE";
    public static final String DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_VALUE_OLD";
    public static final String DATA_REQUIREMENT_VALUE_OLD_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_VALUE_OLD";
    public static final String DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DE_IDENTIFIER = "@E_DATA_REQUIREMENT_VALUE_OLD_AND_NEW";
    public static final String DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER = "@DS_DATA_REQUIREMENT_VALUE_OLD_AND_NEW";
    public static final String ORG_ROOT_DE_IDENTIFIER = "@E_ORG_ROOT";
    public static final String ORG_ROOT_DS_IDENTIFIER = "@DS_ORG_ROOT";
    public static final String ORG_STRUCTURE_DE_IDENTIFIER = "@E_ORG_STRUCTURE";
    public static final String ORG_STRUCTURE_DS_IDENTIFIER = "@DS_ORG_STRUCTURE";
    public static final String ORG_STRUCTURE_PATH_DE_IDENTIFIER = "@E_ORG_STRUCTURE_PATH";
    public static final String ORG_DR_COMMENT_DE_IDENTIFIER = "@E_ORG_DR_COMMENT";
    public static final String ORG_DR_COMMENT_DS_IDENTIFIER = "@DS_ORG_DR_COMMENT";
    public static final String ORG_ONTOLOGY_DE_IDENTIFIER = "@E_ORG_ONTOLOGY";
    public static final String ORG_ONTOLOGY_DS_IDENTIFIER = "@DS_ORG_ONTOLOGY";
    public static final String ORG_ONTOLOGY_TERMINOLOGY_DE_IDENTIFIER = "@E_ORG_ONTOLOGY_TERMINOLOGY";
    public static final String ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER = "@DS_ORG_ONTOLOGY_TERMINOLOGY";
    public static final String ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER = "@DS_ORG_ONTOLOGY_STRUCTURE";
    public static final String ORG_ONTOLOGY_STRUCTURE_DE_IDENTIFIER = "@E_ORG_ONTOLOGY_STRUCTURE";
    public static final String ORG_TERMINOLOGY_DE_IDENTIFIER = "@E_ORG_TERMINOLOGY";
    public static final String ORG_TERMINOLOGY_DS_IDENTIFIER = "@DS_ORG_TERMINOLOGY";
    public static final String ORG_ONTOLOGY_CONCEPT_TERM_DE_IDENTIFIER = "@E_ORG_ONTOLOGY_CONCEPT_TERM";
    public static final String ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER = "@DS_ORG_ONTOLOGY_CONCEPT_TERM";
    public static final String ORG_ONTOLOGY_CONCEPT_CODE_DE_IDENTIFIER = "@E_ORG_ONTOLOGY_CONCEPT_CODE";
    public static final String ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER = "@DS_ORG_ONTOLOGY_CONCEPT_CODE";
    public static final String ORG_ONTOLOGY_CONCEPT_DEFINITION_DE_IDENTIFIER = "@E_ORG_ONTOLOGY_CONCEPT_DEFINITION";
    public static final String ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER = "@DS_ORG_ONTOLOGY_CONCEPT_DEFINITION";
    public static final String ORG_ONTOLOGY_CONCEPT_COMPLETE_DE_IDENTIFIER = "@E_ORG_ONTOLOGY_CONCEPT_COMPLETE";

    // Table names.
    private static final String TABLE_ORG_ONTOLOGY_STRUCTURE = "ORG_ONT_STR";
    private static final String TABLE_ONTOLOGY_PHRASE_ABBREVIATION = "ONT_PHR_ABR";
    private static final String TABLE_DATA_RECORD_DOC_ATR = "DAT_REC_ATR";
    private static final String TABLE_DATA_REQUIREMENT_VALUE_OLD = "DAT_REQ_VAL_OLD";
    private static final String TABLE_DATA_REQUIREMENT_COMMENT = "DAT_REQ_CMT";
    private static final String TABLE_ONTOLOGY_CONCEPT = "ONT_CPT";
    private static final String TABLE_ONTOLOGY_CONCEPT_GRAPH = "ONT_REL";
    private static final String TABLE_ONTOLOGY_TERM = "ONT_TRM";
    private static final String TABLE_ONTOLOGY_CODE = "ONT_CDE";
    private static final String TABLE_ONTOLOGY_DEFINITION = "ONT_DEF";
    private static final String TABLE_ONTOLOGY_ABBREVIATION = "ONT_ABR";
    private static final String TABLE_ONTOLOGY_CLASS = "ONT_CLS";
    private static final String TABLE_DATA_RECORD_DOC = "DAT_REC";
    private static final String TABLE_DATA_RECORD_PROPERTY = "DAT_REC_PRP";
    private static final String TABLE_DATA_RECORD_VALUE = "DAT_REC_VAL";
    private static final String TABLE_DATA_RECORD_ATTACHMENT = "DAT_REC_ATC";
    private static final String TABLE_DATA_REQUIREMENT_DOC = "DAT_REQ";
    private static final String TABLE_DATA_REQUIREMENT_DATA_TYPE = "DAT_REQ_TYP";
    private static final String TABLE_DATA_REQUIREMENT_INSTANCE = "DAT_REQ_INS";
    private static final String TABLE_DATA_REQUIREMENT_VALUE = "DAT_REQ_VAL";
    private static final String TABLE_ORG_ROOT = "ORG";
    private static final String TABLE_ORG_STRUCTURE = "ORG_STR";
    private static final String TABLE_ORG_ONTOLOGY = "ORG_ONT";
    private static final String TABLE_ORG_TERMINOLOGY = "ORG_TMN";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        T8DataRequirementDataSourceResourceDefinition dataRequirementDataSourceDefinition;
        T8DataRequirementInstanceDataSourceResourceDefinition dataRequirementInstanceDataSourceDefinition;
        T8DataRecordDataSourceResourceDefinition dataRecordDataSourceDefinition;
        T8SQLDataSourceDefinition sqlDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // TABLE_ORG_ONTOLOGY_STRUCTURE Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ORG_ONTOLOGY_STRUCTURE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ORG_ID", "ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ODS_ID", "ODS_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_ID", "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_IID", "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_AT", "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_ID", "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_IID", "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_AT", "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // TABLE_ONTOLOGY_PHRASE_ABBREVIATION Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_PHRASE_ABBREVIATION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ROOT_ORG_ID", "ROOT_ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$LANGUAGE_ID", "LANGUAGE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PHRASE", "PHRASE", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ABBREVIATION", "ABBREVIATION", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, true, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_ID", "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_IID", "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_AT", "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_ID", "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_IID", "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_AT", "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_RECORD_DOC_ATR Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_DOC_ATR_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_DOC_ATR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID", "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTRIBUTE_ID, "ATTRIBUTE_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE", "TYPE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ROOT_RECORD_ID", "ROOT_RECORD_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_ID", "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_IID", "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_AT", "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_ID", "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_IID", "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_AT", "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_REQUIREMENT_VALUE_OLD Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_REQUIREMENT_VALUE_OLD);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_VALUE_ID", "OLD_VALUE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE_ID", "VALUE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_VALUE", "OLD_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_LOWER_BOUND_VALUE", "OLD_LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_UPPER_BOUND_VALUE", "OLD_UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_CONCEPT_ID", "OLD_CONCEPT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_CONCEPT_TERM", "OLD_CONCEPT_TERM", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_UOM_ID", "OLD_UOM_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_UOM_TERM", "OLD_UOM_TERM", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_QOM_ID", "OLD_QOM_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_QOM_TERM", "OLD_QOM_TERM", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_ID", "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_IID", "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_AT", "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_ID", "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_IID", "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_AT", "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_REQUIREMENT_VALUE_NEW_AND_OLD Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	PV.ORG_ID,\n" +
            "	PV.VALUE_ID,\n" +
            "	PV.DR_ID,\n" +
            "	PV.DR_INSTANCE_ID,\n" +
            "	PV.PROPERTY_ID,\n" +
            "	PV.FIELD_ID,\n" +
            "	PV.DATA_TYPE_ID,\n" +
            "	PV.VALUE,\n" +
            "	PV.LOWER_BOUND_VALUE,\n" +
            "	PV.UPPER_BOUND_VALUE,\n" +
            "	PV.CONCEPT_ID,\n" +
            "	PV.UOM_ID,\n" +
            "	PV.QOM_ID,\n" +
            "	PV.STANDARD_INDICATOR,\n" +
            "	PV.APPROVED_INDICATOR,\n" +
            "	PVO.OLD_VALUE_ID,\n" +
            "	PVO.OLD_VALUE,\n" +
            "	PVO.OLD_LOWER_BOUND_VALUE,\n" +
            "	PVO.OLD_UPPER_BOUND_VALUE,\n" +
            "	PVO.OLD_CONCEPT_ID,\n" +
            "	PVO.OLD_CONCEPT_TERM,\n" +
            "	PVO.OLD_UOM_ID,\n" +
            "	PVO.OLD_UOM_TERM,\n" +
            "	PVO.OLD_QOM_ID,\n" +
            "	PVO.OLD_QOM_TERM\n" +
            "FROM DAT_REQ_VAL PV\n" +
            "LEFT JOIN DAT_REQ_VAL_OLD PVO\n" +
            "	ON PV.VALUE_ID = PVO.VALUE_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ORG_ID", "ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_ID", "DR_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_INSTANCE_ID", "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PROPERTY_ID", "PROPERTY_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FIELD_ID", "FIELD_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_TYPE_ID", "DATA_TYPE_ID", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE_ID", "VALUE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALUE", "VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$LOWER_BOUND_VALUE", "LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPPER_BOUND_VALUE", "UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CONCEPT_ID", "CONCEPT_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UOM_ID", "UOM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$QOM_ID", "QOM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$STANDARD_INDICATOR", "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$APPROVED_INDICATOR", "APPROVED_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_VALUE_ID", "OLD_VALUE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_VALUE", "OLD_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_LOWER_BOUND_VALUE", "OLD_LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_UPPER_BOUND_VALUE", "OLD_UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_CONCEPT_ID", "OLD_CONCEPT_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_CONCEPT_TERM", "OLD_CONCEPT_TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_UOM_ID", "OLD_UOM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_UOM_TERM", "OLD_UOM_TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_QOM_ID", "OLD_QOM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$OLD_QOM_TERM", "OLD_QOM_TERM", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // DATA_RECORD_SEARCH_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_RECORD_SEARCH_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	DRD.DR_INSTANCE_ID, \n" +
            "	DRV.RECORD_ID,  \n" +
            "	DRV.ROOT_RECORD_ID,  \n" +
            "	DRV.DR_ID,  \n" +
            "	DRV.PROPERTY_ID,  \n" +
            "	DRV.SECTION_ID,  \n" +
            "	DRV.DATA_TYPE_ID,  \n" +
            "	DRV.DATA_TYPE_SEQUENCE,  \n" +
            "	DRV.VALUE_SEQUENCE,  \n" +
            "	COALESCE(REPLACE(REPLACEMENT_VALUE.DR_INSTANCE_ID, '-', ''), DRV.VALUE) AS VALUE,  \n" +
            "	DRV.FFT  \n" +
            "FROM DAT_REQ_TYP DRDT \n" +
            "LEFT JOIN DAT_REC_VAL DRV    \n" +
            "   ON DRDT.DR_ID = DRV.DR_ID    \n" +
            "   AND DRDT.SECTION_ID = DRV.SECTION_ID   \n" +
            "   AND DRDT.PROPERTY_ID = DRV.PROPERTY_ID    \n" +
            "   AND DRDT.DATA_TYPE_ID = DRV.DATA_TYPE_ID    \n" +
            "   AND DRDT.DATA_TYPE_SEQUENCE = DRV.DATA_TYPE_SEQUENCE    \n" +
            "LEFT JOIN DAT_REC DRD    \n" +
            "   ON DRV.RECORD_ID = DRD.RECORD_ID    \n" +
            "LEFT JOIN DAT_REC REPLACEMENT_VALUE    \n" +
            "   ON DRV.DATA_TYPE_ID = 'DOCUMENT_REFERENCE'    \n" +
            "   AND REPLACEMENT_VALUE.RECORD_ID = <<-DRV.VALUE->>    \n"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SECTION_ID", "SECTION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, "DATA_TYPE_SEQUENCE", false, false, T8DataType.DOUBLE));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_SEQUENCE, "VALUE_SEQUENCE", false, false, T8DataType.DOUBLE));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.setSQLParameterExpressionMap(HashMaps.newHashMap("DRV.VALUE", "dbAdaptor.getSQLColumnHexToGUID(\"DRV.VALUE\")"));
        definitions.add(sqlDataSourceDefinition);

        // ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	STR.ROOT_ORG_ID,\n" +
            "	ORG.ORG_ID, \n" +
            "   ORG.ODS_ID, \n" +
            "   ORG.ODT_ID, \n" +
            "   ORG.CONCEPT_ID, \n" +
            "   OT.LANGUAGE_ID, \n" +
            "   OT.TERM_ID, \n" +
            "   OT.TERM, \n" +
            "   OT.STANDARD_INDICATOR, \n" +
            "   OT.ACTIVE_INDICATOR, \n" +
            "   OT.UPDATED_AT, \n" +
            "   OT.UPDATED_BY_ID, \n" +
            "   OT.INSERTED_AT, \n" +
            "   OT.INSERTED_BY_ID \n" +
            "FROM ONT_TRM OT \n" +
            "INNER JOIN ORG_ONT ORG \n" +
            "   ON OT.CONCEPT_ID = ORG.CONCEPT_ID \n" +
            "INNER JOIN ORG_STR STR \n" +
            "   ON ORG.ORG_ID = STR.ORG_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM, "TERM", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "CHILD_RECORD_ID", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "CHILD_RECORD_ID", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "CHILD_RECORD_ID", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "CHILD_RECORD_ID", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	STR.ROOT_ORG_ID,\n" +
            "	ORG.ORG_ID, \n" +
            "   ORG.ODS_ID, \n" +
            "   ORG.ODT_ID, \n" +
            "   ORG.CONCEPT_ID, \n" +
            "   OC.CODE_ID, \n" +
            "   OC.CODE_TYPE_ID, \n" +
            "   OC.CODE, \n" +
            "   OC.STANDARD_INDICATOR, \n" +
            "   OC.ACTIVE_INDICATOR, \n" +
            "   OC.UPDATED_AT, \n" +
            "   OC.UPDATED_BY_ID, \n" +
            "   OC.INSERTED_AT, \n" +
            "   OC.INSERTED_BY_ID \n" +
            "FROM ONT_CDE OC \n" +
            "INNER JOIN ORG_ONT ORG \n" +
            "   ON OC.CONCEPT_ID = ORG.CONCEPT_ID \n" +
            "INNER JOIN ORG_STR STR \n" +
            "   ON ORG.ORG_ID = STR.ORG_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_ID, "CODE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_TYPE_ID, "CODE_TYPE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE, "CODE", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "CHILD_RECORD_ID", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "CHILD_RECORD_ID", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "CHILD_RECORD_ID", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "CHILD_RECORD_ID", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	STR.ROOT_ORG_ID,\n" +
            "	ORG.ORG_ID, \n" +
            "   ORG.ODS_ID, \n" +
            "   ORG.ODT_ID, \n" +
            "   ORG.CONCEPT_ID, \n" +
            "   OD.LANGUAGE_ID, \n" +
            "   OD.DEFINITION_ID, \n" +
            "   OD.DEFINITION, \n" +
            "   OD.STANDARD_INDICATOR, \n" +
            "   OD.ACTIVE_INDICATOR, \n" +
            "   OD.UPDATED_AT, \n" +
            "   OD.UPDATED_BY_ID, \n" +
            "   OD.INSERTED_AT, \n" +
            "   OD.INSERTED_BY_ID \n" +
            "FROM ONT_DEF OD \n" +
            "INNER JOIN ORG_ONT ORG \n" +
            "   ON OD.CONCEPT_ID = ORG.CONCEPT_ID \n" +
            "INNER JOIN ORG_STR STR \n" +
            "   ON ORG.ORG_ID = STR.ORG_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION_ID, "DEFINITION_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "CHILD_RECORD_ID", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "CHILD_RECORD_ID", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "CHILD_RECORD_ID", false, false, T8DataType.TIMESTAMP));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "CHILD_RECORD_ID", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // DATA_RECORD_CHILDREN_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_RECORD_CHILDREN_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	RECORD_ID AS PARENT_RECORD_ID,\n" +
            "	<<-VALUE->> AS CHILD_RECORD_ID \n" +
            "FROM DAT_REC \n" +
            "WHERE DATA_TYPE_ID = 'DOCUMENT_REFERENCE'"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_RECORD_ID, "PARENT_RECORD_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHILD_RECORD_ID, "CHILD_RECORD_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.setSQLParameterExpressionMap(HashMaps.newHashMap("VALUE", "dbAdaptor.getSQLColumnHexToGUID(\"VALUE\")"));
        definitions.add(sqlDataSourceDefinition);

        // DATA_REQUIREMENT_CHILDREN_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_REQUIREMENT_CHILDREN_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "	DTP.DR_ID AS PARENT_DR_ID,\n" +
            "	DRI.DR_ID AS CHILD_DR_ID \n" +
            "FROM DAT_REQ_TYP DTP \n" +
            "JOIN DAT_REQ_INS DRI \n" +
            "   ON DTP.VALUE = DRI.DR_INSTANCE_ID \n" +
            "WHERE DATA_TYPE_ID = 'DOCUMENT_REFERENCE' \n"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARENT_DR_ID", "PARENT_DR_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CHILD_DR_ID", "CHILD_DR_ID", false, false, T8DataType.GUID));
        definitions.add(sqlDataSourceDefinition);

        // DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "   DOC.RECORD_ID, \n" +
            "	REQ.DR_ID,  \n" +
            "   REQ.CLASS_ID  \n" +
            "FROM DAT_REQ REQ \n" +
            "JOIN DAT_REC DOC \n" +
            "   ON REQ.DR_ID = DOC.DR_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CLASS_ID, "CLASS_ID", false, true, T8DataType.GUID));
        definitions.add(sqlDataSourceDefinition);

        // DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "   RECORD_ID, \n" +
            "	ATTACHMENT_ID,  \n" +
            "   FILE_CONTEXT_ID,  \n" +
            "   FILE_NAME, \n" +
            "   FILE_SIZE, \n" +
            "   MD5_CHECKSUM, \n" +
            "   MEDIA_TYPE \n" +
            "FROM DAT_REC_ATC"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTACHMENT_ID, "ATTACHMENT_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_CONTEXT_ID, "FILE_CONTEXT_ID", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_NAME, "FILE_NAME", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_SIZE, "FILE_SIZE", false, true, T8DataType.LONG));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_MD5_CHECKSUM, "MD5_CHECKSUM", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_MEDIA_TYPE, "MEDIA_TYPE", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // DATA_STRING_DETAILS_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_STRING_DETAILS_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "   a.RECORD_ID, \n" +
            "	a.DATA_STRING_INSTANCE_ID,  \n" +
            "   a.DATA_STRING,  \n" +
            "   b.AUTO_RENDER, \n" +
            "   b.LANGUAGE_ID, \n" +
            "   b.DATA_STRING_TYPE_ID \n" +
            "FROM DAT_STR a \n" +
            "LEFT JOIN DAT_STR_INS b \n" +
            "   ON a.DATA_STRING_INSTANCE_ID = b.DATA_STRING_INSTANCE_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_STRING_INSTANCE_ID, "DATA_STRING_INSTANCE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_STRING, "DATA_STRING", false, false, T8DataType.LONG_STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_AUTO_RENDER, "AUTO_RENDER", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_STRING_TYPE_ID, "DATA_STRING_TYPE_ID", false, false, T8DataType.GUID));
        definitions.add(sqlDataSourceDefinition);

        // DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "   DSL.ROOT_ORG_ID, \n" +
            "	DSO.ORG_ID,  \n" +
            "   DSL.DR_INSTANCE_ID,  \n" +
            "   DSL.DATA_STRING_INSTANCE_ID, \n" +
            "   DSI.DATA_STRING_TYPE_ID, \n" +
            "   DSI.LANGUAGE_ID, \n" +
            "   DSI.AUTO_RENDER, \n" +
            "   DSL.CONDITION_EXPRESSION, \n" +
            "   DSL.DATA_KEY_EXPRESSION, \n" +
            "   DSL.SEQUENCE \n" +
            "FROM DAT_STR_INS_LNK DSL \n" +
            "INNER JOIN DAT_STR_INS DSI \n" +
            "   ON DSL.DATA_STRING_INSTANCE_ID = DSI.DATA_STRING_INSTANCE_ID \n" +
            "INNER JOIN ORG_ONT DSO \n" +
            "   ON DSI.DATA_STRING_INSTANCE_ID = DSO.CONCEPT_ID"

        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_STRING_INSTANCE_ID, "DATA_STRING_INSTANCE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_STRING_TYPE_ID, "DATA_STRING_TYPE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_AUTO_RENDER, "AUTO_RENDER", false, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONDITION_EXPRESSION, "CONDITION_EXPRESSION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_KEY_EXPRESSION, "DATA_KEY_EXPRESSION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SEQUENCE, "SEQUENCE", false, true, T8DataType.INTEGER));
        definitions.add(sqlDataSourceDefinition);

        // ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "   OS.ROOT_ORG_ID, \n" +
            "	OO.ORG_ID,  \n" +
            "   OO.ODS_ID,  \n" +
            "   OO.ODT_ID, \n" +
            "   OO.CONCEPT_ID, \n" +
            "   OO.ACTIVE_INDICATOR, \n" +
            "   T.LANGUAGE_ID, \n" +
            "   T.CONCEPT_TYPE_ID, \n" +
            "   T.TERM_ID, \n" +
            "   T.TERM, \n" +
            "   T.DEFINITION_ID, \n" +
            "   T.DEFINITION, \n" +
            "   T.ABBREVIATION_ID, \n" +
            "   T.ABBREVIATION, \n" +
            "   T.CODE_ID, \n" +
            "   T.CODE, \n" +
            "   T.ORG_ID AS TERMINOLOGY_ORG_ID \n" +
            "FROM ORG_ONT OO \n" +
            "INNER JOIN ORG_STR OS \n" +
            "   ON OO.ORG_ID = OS.ORG_ID \n" +
            "INNER JOIN ORG_TMN T \n" +
            "   ON T.CONCEPT_ID = OO.CONCEPT_ID \n" +
            "   AND T.ORG_ID = OS.ROOT_ORG_ID"

        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_ORG_ID, "ROOT_ORG_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM, "TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION_ID, "DEFINITION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION, "DEFINITION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION_ID, "ABBREVIATION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION, "ABBREVIATION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_ID, "CODE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE, "CODE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TERMINOLOGY_ORG_ID", "TERMINOLOGY_ORG_ID", true, true, T8DataType.GUID));
        definitions.add(sqlDataSourceDefinition);

        // DATA_REQUIREMENT_DOCUMENT_DS_IDENTIFIER Data Source.
        dataRequirementDataSourceDefinition = new T8DataRequirementDataSourceResourceDefinition(DATA_REQUIREMENT_DOCUMENT_DS_IDENTIFIER);
        dataRequirementDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        dataRequirementDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ID", "ID", true, true, T8DataType.GUID));
        dataRequirementDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DOCUMENT", "DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        definitions.add(dataRequirementDataSourceDefinition);

        // DATA_REQUIREMENT_INSTANCE_DOCUMENT_DS_IDENTIFIER Data Source.
        dataRequirementInstanceDataSourceDefinition = new T8DataRequirementInstanceDataSourceResourceDefinition(DATA_REQUIREMENT_INSTANCE_DOCUMENT_DS_IDENTIFIER);
        dataRequirementInstanceDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        dataRequirementInstanceDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ID", "ID", true, true, T8DataType.GUID));
        dataRequirementInstanceDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DOCUMENT", "DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        definitions.add(dataRequirementInstanceDataSourceDefinition);

        // DATA_RECORD_DOCUMENT_DS_IDENTIFIER Data Source.
        dataRecordDataSourceDefinition = new T8DataRecordDataSourceResourceDefinition(DATA_RECORD_DOCUMENT_DS_IDENTIFIER);
        dataRecordDataSourceDefinition.setRootLevelSource(false);
        dataRecordDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ID", "ID", true, true, T8DataType.GUID));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DOCUMENT", "DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_VALUE_STRING", "RECORD_VALUE_STRING", false, true, T8DataType.STRING));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TAGS, "TAGS", false, true, T8DataType.STRING));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, true, T8DataType.STRING));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        definitions.add(dataRecordDataSourceDefinition);

        // DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER Data Source.
        dataRecordDataSourceDefinition = new T8DataRecordDataSourceResourceDefinition(DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER);
        dataRecordDataSourceDefinition.setRootLevelSource(true);
        dataRecordDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ID", "ID", true, true, T8DataType.GUID));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DOCUMENT", "DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_VALUE_STRING", "RECORD_VALUE_STRING", false, true, T8DataType.STRING));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TAGS, "TAGS", false, true, T8DataType.STRING));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, true, T8DataType.STRING));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        dataRecordDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        definitions.add(dataRecordDataSourceDefinition);

        // ONTOLOGY_CONCEPT_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_CONCEPT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_CONCEPT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_CONCEPT_GRAPH);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_GRAPH_ID, "CONCEPT_GRAPH_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TAIL_CONCEPT_ID, "TAIL_CONCEPT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_HEAD_CONCEPT_ID, "HEAD_CONCEPT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ONTOLOGY_TERM_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_TERM_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_TERM);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM, "TERM", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PRIMARY_INDICATOR, "PRIMARY_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ONTOLOGY_CODE_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_CODE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_CODE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_ID, "CODE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_TYPE_ID, "CODE_TYPE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE, "CODE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PRIMARY_INDICATOR, "PRIMARY_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ONTOLOGY_DEFINITION_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_DEFINITION_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_DEFINITION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION_ID, "DEFINITION_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PRIMARY_INDICATOR, "PRIMARY_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ONTOLOGY_ABBREVIATION_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_ABBREVIATION_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_ABBREVIATION);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION_ID, "ABBREVIATION_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION, "ABBREVIATION", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_IRDI, "IRDI", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PRIMARY_INDICATOR, "PRIMARY_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ONTOLOGY_CLASS_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ONTOLOGY_CLASS_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ONTOLOGY_CLASS);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_ODT_ID, "PARENT_ODT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SEQUENCE, "SEQUENCE", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LFT, "LFT", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RGT, "RGT", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_RECORD_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_DOC);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_STRING, "VALUE_STRING", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TAGS, "TAGS", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_COUNT, "PROPERTY_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_DATA_COUNT, "PROPERTY_DATA_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_VALID_COUNT, "PROPERTY_VALID_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_VERIFICATION_COUNT, "PROPERTY_VERIFICATION_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ACCURATE_COUNT, "PROPERTY_ACCURATE_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHRCTRSTC_COUNT, "CHRCTRSTC_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHRCTRSTC_DATA_COUNT, "CHRCTRSTC_DATA_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHRCTRSTC_VALID_COUNT, "CHRCTRSTC_VALID_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHRCTRSTC_VERIFICATION_COUNT, "CHRCTRSTC_VERIFICATION_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CHRCTRSTC_ACCURATE_COUNT, "CHRCTRSTC_ACCURATE_COUNT", false, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ASYNC_ONT_UPD_TIME, "ASYNC_ONT_UPD_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ASYNC_ATR_UPD_TIME, "ASYNC_ATR_UPD_TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_RECORD_PROPERTY_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_PROPERTY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_PROPERTY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SECTION_ID", "SECTION_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_STRING, "VALUE_STRING", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$VALIDITY_INDICATOR", "VALIDITY_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_RECORD_VALUE_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_VALUE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_VALUE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SECTION_ID", "SECTION_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, "DATA_TYPE_SEQUENCE", true, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_SEQUENCE, "VALUE_SEQUENCE", true, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARENT_VALUE_SEQUENCE", "PARENT_VALUE_SEQUENCE", false, false, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_ID, "VALUE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_CONCEPT_ID, "VALUE_CONCEPT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TEXT", "TEXT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_RECORD_ATTACHMENT_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_ATTACHMENT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_ATTACHMENT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTACHMENT_ID, "ATTACHMENT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_CONTEXT_ID, "FILE_CONTEXT_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_NAME, "FILE_NAME", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_SIZE, "FILE_SIZE", false, true, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_DATA, "FILE_DATA", false, false, T8DataType.BYTE_ARRAY));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_MD5_CHECKSUM, "MD5_CHECKSUM", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_REQUIREMENT_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_REQUIREMENT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_REQUIREMENT_DOC);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CLASS_ID, "CLASS_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_REQUIREMENT_DATA_TYPE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SECTION_ID", "SECTION_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, "DATA_TYPE_SEQUENCE", true, true, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_DATA_TYPE_ID, "PARENT_DATA_TYPE_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARENT_DATA_TYPE_SEQ", "PARENT_DATA_TYPE_SEQ", false, false, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ATTRIBUTES", "ATTRIBUTES", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_REQUIREMENT_INSTANCE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INDEPENDENT_INDICATOR, "INDEPENDENT_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_REQUIREMENT_VALUE_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_REQUIREMENT_VALUE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_REQUIREMENT_VALUE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_ID, "VALUE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LOWER_BOUND_VALUE, "LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPPER_BOUND_VALUE, "UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UOM_ID, "UOM_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_QOM_ID, "QOM_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_APPROVED_INDICATOR, "APPROVED_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ORG_ROOT_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ORG_ROOT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ORG_ROOT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ORG_STRUCTURE_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ORG_STRUCTURE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ORG_STRUCTURE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_ORG_ID, "PARENT_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_TYPE_ID, "ORG_TYPE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_LEVEL, "ORG_LEVEL", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LFT, "LFT", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RGT, "RGT", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ORG_DR_COMMENT_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ORG_DR_COMMENT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_REQUIREMENT_COMMENT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_COMMENT_ID, "COMMENT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_COMMENT_TYPE_ID, "COMMENT_TYPE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_COMMENT, "COMMENT", false, true, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ORG_ONTOLOGY_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ORG_ONTOLOGY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ORG_ONTOLOGY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODS_ID, "ODS_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ODT_ID, "ODT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // ORG_TERMINOLOGY_DS_IDENTIFIER Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(ORG_TERMINOLOGY_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_ORG_TERMINOLOGY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_LANGUAGE_ID, "LANGUAGE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM_ID, "TERM_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TERM, "TERM", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION_ID, "ABBREVIATION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ABBREVIATION, "ABBREVIATION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION_ID, "DEFINITION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DEFINITION, "DEFINITION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE_ID, "CODE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CODE, "CODE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // TABLE_ORG_ONTOLOGY_STRUCTURE Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ONTOLOGY_STRUCTURE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ORG_ID", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$ORG_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ODS_ID", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$ODS_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_ID", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_IID", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$INSERTED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_AT", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_ID", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_IID", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$UPDATED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_AT", ORG_ONTOLOGY_STRUCTURE_DS_IDENTIFIER + "$UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // TABLE_ONTOLOGY_PHRASE_ABBREVIATION Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_PHRASE_ABBREVIATION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ROOT_ORG_ID", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$ROOT_ORG_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$LANGUAGE_ID", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$LANGUAGE_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PHRASE", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$PHRASE", true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ABBREVIATION", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$ABBREVIATION", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$SEQUENCE", false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_ID", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_IID", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$INSERTED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_AT", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_ID", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_IID", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$UPDATED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_AT", ONTOLOGY_PHRASE_ABBREVIATION_DS_IDENTIFIER + "$UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_RECORD_DOC_ATR Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_DOC_ATR_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_DOC_ATR_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_ID", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$RECORD_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTRIBUTE_ID, DATA_RECORD_DOC_ATR_DS_IDENTIFIER + EF_ATTRIBUTE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", DATA_RECORD_DOC_ATR_DS_IDENTIFIER+ "$TYPE", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ROOT_RECORD_ID", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$ROOT_RECORD_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_ID", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_IID", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$INSERTED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_AT", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_ID", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_IID", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$UPDATED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_AT", DATA_RECORD_DOC_ATR_DS_IDENTIFIER + "$UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_RECORD_CHILDREN_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_CHILDREN_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_CHILDREN_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_RECORD_ID, DATA_RECORD_CHILDREN_DS_IDENTIFIER + EF_PARENT_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHILD_RECORD_ID, DATA_RECORD_CHILDREN_DS_IDENTIFIER + EF_CHILD_RECORD_ID, true, true, T8DataType.GUID));
        definitions.add(entityDefinition);

        // ORG_ONTOLOGY_CONCEPT_TERM_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ONTOLOGY_CONCEPT_TERM_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_ORG_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_ODS_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_ODT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_TERM_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_CHILD_RECORD_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_PARENT_RECORD_ID, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_ONTOLOGY_CONCEPT_TERM_DS_IDENTIFIER + EF_CHILD_RECORD_ID, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // ORG_ONTOLOGY_CONCEPT_CODE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ONTOLOGY_CONCEPT_CODE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_ORG_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_ODS_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_ODT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_CODE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_TYPE_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_CODE_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_CODE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_CHILD_RECORD_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_PARENT_RECORD_ID, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_ONTOLOGY_CONCEPT_CODE_DS_IDENTIFIER + EF_CHILD_RECORD_ID, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // ORG_ONTOLOGY_CONCEPT_DEFINITION_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ONTOLOGY_CONCEPT_DEFINITION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_ORG_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_ODS_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_ODT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_DEFINITION_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_DEFINITION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_CHILD_RECORD_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_PARENT_RECORD_ID, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_ONTOLOGY_CONCEPT_DEFINITION_DS_IDENTIFIER + EF_CHILD_RECORD_ID, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_CHILDREN_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_CHILDREN_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_CHILDREN_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARENT_DR_ID", DATA_REQUIREMENT_CHILDREN_DS_IDENTIFIER + "$PARENT_DR_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CHILD__DR_ID", DATA_REQUIREMENT_CHILDREN_DS_IDENTIFIER + "$CHILD__DR_ID", false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_STRING_DETAILS_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_DETAILS_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_DETAILS_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_STRING_DETAILS_DS_IDENTIFIER + EF_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_STRING_INSTANCE_ID, DATA_STRING_DETAILS_DS_IDENTIFIER + EF_DATA_STRING_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_STRING, DATA_STRING_DETAILS_DS_IDENTIFIER + EF_DATA_STRING, false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_AUTO_RENDER, DATA_STRING_DETAILS_DS_IDENTIFIER + EF_AUTO_RENDER, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, DATA_STRING_DETAILS_DS_IDENTIFIER + EF_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_STRING_TYPE_ID, DATA_STRING_DETAILS_DS_IDENTIFIER + EF_DATA_STRING_TYPE_ID, false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_RECORD_ATTACHMENT_DETAILS_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_ATTACHMENT_DETAILS_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTACHMENT_ID, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_ATTACHMENT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_CONTEXT_ID, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_FILE_CONTEXT_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_NAME, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_FILE_NAME, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_SIZE, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_FILE_SIZE, false, true, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_MD5_CHECKSUM, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_MD5_CHECKSUM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_MEDIA_TYPE, DATA_RECORD_ATTACHMENT_DETAILS_DS_IDENTIFIER + EF_MEDIA_TYPE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_RECORD_SEARCH_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_SEARCH_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_SEARCH_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_DR_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_ROOT_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SECTION_ID", DATA_RECORD_SEARCH_DS_IDENTIFIER + "$SECTION_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, true, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_SEQUENCE, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_VALUE_SEQUENCE, true, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, DATA_RECORD_SEARCH_DS_IDENTIFIER + EF_VALUE, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DATA_RECORD_SEARCH_DS_IDENTIFIER + "$FFT", true, true, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_RECORD_DOCUMENT_CLASS_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_DOCUMENT_CLASS_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER + EF_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CLASS_ID, DATA_RECORD_DOCUMENT_CLASS_DS_IDENTIFIER + EF_CLASS_ID, false, true, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_STRING_INSTANCE_LINK_DETAILS_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_INSTANCE_LINK_DETAILS_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_ORG_ID, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_STRING_INSTANCE_ID, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_DATA_STRING_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_STRING_TYPE_ID, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_DATA_STRING_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_AUTO_RENDER, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_AUTO_RENDER, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONDITION_EXPRESSION, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_CONDITION_EXPRESSION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_KEY_EXPRESSION, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_DATA_KEY_EXPRESSION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SEQUENCE, DATA_STRING_INSTANCE_LINK_DETAILS_DS_IDENTIFIER + EF_SEQUENCE, false, true, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        // ORG_ONTOLOGY_TERMINOLOGY_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ONTOLOGY_TERMINOLOGY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_ORG_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ROOT_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ODS_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ODT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_LANGUAGE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_TYPE_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_CONCEPT_TYPE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_ID, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE, ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + EF_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TERMINOLOGY_ORG_ID", ORG_ONTOLOGY_TERMINOLOGY_DS_IDENTIFIER + "$TERMINOLOGY_ORG_ID", true, true, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_DOCUMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ID", DATA_REQUIREMENT_DOCUMENT_DS_IDENTIFIER + "$ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DOCUMENT", DATA_REQUIREMENT_DOCUMENT_DS_IDENTIFIER + "$DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_INSTANCE_DOCUMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ID", DATA_REQUIREMENT_INSTANCE_DOCUMENT_DS_IDENTIFIER + "$ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DOCUMENT", DATA_REQUIREMENT_INSTANCE_DOCUMENT_DS_IDENTIFIER + "$DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        definitions.add(entityDefinition);

        // DATA_RECORD_DOCUMENT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_DOCUMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_DOCUMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ID", DATA_RECORD_DOCUMENT_DS_IDENTIFIER + "$ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DOCUMENT", DATA_RECORD_DOCUMENT_DS_IDENTIFIER + "$DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_VALUE_STRING", DATA_RECORD_DOCUMENT_DS_IDENTIFIER + "$RECORD_VALUE_STRING", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TAGS, DATA_RECORD_DOCUMENT_DS_IDENTIFIER + EF_TAGS, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DATA_RECORD_DOCUMENT_DS_IDENTIFIER + "$FFT", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_DOCUMENT_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_RECORD_DOCUMENT_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, DATA_RECORD_DOCUMENT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, DATA_RECORD_DOCUMENT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_RECORD_DOCUMENT_ROOT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_DOCUMENT_ROOT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ID", DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + "$ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DOCUMENT", DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + "$DOCUMENT", false, true, T8DataType.CUSTOM_OBJECT));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_VALUE_STRING", DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + "$RECORD_VALUE_STRING", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TAGS, DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + EF_TAGS, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + "$FFT", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_RECORD_DOCUMENT_ROOT_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_VALUE_OLD Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_VALUE_OLD_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_VALUE_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_VALUE_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$VALUE_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_VALUE", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_LOWER_BOUND_VALUE", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_UPPER_BOUND_VALUE", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_CONCEPT_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_CONCEPT_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_CONCEPT_TERM", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_CONCEPT_TERM", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_UOM_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_UOM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_UOM_TERM", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_UOM_TERM", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_QOM_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_QOM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_QOM_TERM", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$OLD_QOM_TERM", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_IID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$INSERTED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_AT", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_ID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_IID", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$UPDATED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_AT", DATA_REQUIREMENT_VALUE_OLD_DS_IDENTIFIER + "$UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_VALUE_OLD_AND_NEW Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ORG_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$ORG_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DR_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$DR_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DR_INSTANCE_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$DR_INSTANCE_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PROPERTY_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$PROPERTY_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FIELD_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$FIELD_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_TYPE_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$DATA_TYPE_ID", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$VALUE_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALUE", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$LOWER_BOUND_VALUE", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPPER_BOUND_VALUE", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CONCEPT_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$CONCEPT_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UOM_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$UOM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$QOM_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$QOM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$STANDARD_INDICATOR", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$STANDARD_INDICATOR", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$APPROVED_INDICATOR", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$APPROVED_INDICATOR", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_VALUE_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_VALUE_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_VALUE", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_LOWER_BOUND_VALUE", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_UPPER_BOUND_VALUE", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_CONCEPT_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_CONCEPT_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_CONCEPT_TERM", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_CONCEPT_TERM", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_UOM_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_UOM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_UOM_TERM", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_UOM_TERM", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_QOM_ID", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_QOM_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$OLD_QOM_TERM", DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DS_IDENTIFIER + "$OLD_QOM_TERM", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // ONTOLOGY_CONCEPT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_CONCEPT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_CONCEPT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_TYPE_ID, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_CONCEPT_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_CONCEPT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_GRAPH_ID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_CONCEPT_GRAPH_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TAIL_CONCEPT_ID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_TAIL_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_HEAD_CONCEPT_ID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_HEAD_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_CONCEPT_GRAPH_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ONTOLOGY_TERM_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_TERM_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_TERM_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_TERM_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM, ONTOLOGY_TERM_DS_IDENTIFIER + EF_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ONTOLOGY_TERM_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PRIMARY_INDICATOR, ONTOLOGY_TERM_DS_IDENTIFIER + EF_PRIMARY_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_TERM_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ONTOLOGY_TERM_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_TERM_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_TERM_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_TERM_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ONTOLOGY_CODE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_CODE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_CODE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_ID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_CODE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_TYPE_ID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_CODE_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE, ONTOLOGY_CODE_DS_IDENTIFIER + EF_CODE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PRIMARY_INDICATOR, ONTOLOGY_CODE_DS_IDENTIFIER + EF_PRIMARY_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_CODE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ONTOLOGY_CODE_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_CODE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_CODE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_CODE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ONTOLOGY_DEFINITION_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_DEFINITION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_DEFINITION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION_ID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_DEFINITION_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_DEFINITION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PRIMARY_INDICATOR, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_PRIMARY_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_DEFINITION_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ONTOLOGY_ABBREVIATION_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_ABBREVIATION_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_ABBREVIATION_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION_ID, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_ABBREVIATION_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_TERM_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_ABBREVIATION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_IRDI, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_IRDI, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PRIMARY_INDICATOR, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_PRIMARY_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_ABBREVIATION_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ONTOLOGY_CLASS_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ONTOLOGY_CLASS_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ONTOLOGY_CLASS_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_ODS_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_ODT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_ODT_ID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_PARENT_ODT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SEQUENCE, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_SEQUENCE, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LFT, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_LFT, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RGT, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_RGT, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ONTOLOGY_CLASS_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_RECORD_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_DS_IDENTIFIER + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_RECORD_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DATA_RECORD_DS_IDENTIFIER + "$FFT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_STRING, DATA_RECORD_DS_IDENTIFIER + EF_VALUE_STRING, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TAGS, DATA_RECORD_DS_IDENTIFIER + EF_TAGS, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, DATA_RECORD_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, DATA_RECORD_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, DATA_RECORD_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, DATA_RECORD_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, DATA_RECORD_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, DATA_RECORD_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, DATA_RECORD_DS_IDENTIFIER + EF_ROOT_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_PROPERTY_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_DATA_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_PROPERTY_DATA_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_VALID_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_PROPERTY_VALID_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_VERIFICATION_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_PROPERTY_VERIFICATION_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ACCURATE_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_PROPERTY_ACCURATE_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHRCTRSTC_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_CHRCTRSTC_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHRCTRSTC_DATA_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_CHRCTRSTC_DATA_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHRCTRSTC_VALID_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_CHRCTRSTC_VALID_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHRCTRSTC_VERIFICATION_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_CHRCTRSTC_VERIFICATION_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CHRCTRSTC_ACCURATE_COUNT, DATA_RECORD_DS_IDENTIFIER + EF_CHRCTRSTC_ACCURATE_COUNT, false, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ASYNC_ONT_UPD_TIME, DATA_RECORD_DS_IDENTIFIER + EF_ASYNC_ONT_UPD_TIME, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ASYNC_ATR_UPD_TIME, DATA_RECORD_DS_IDENTIFIER + EF_ASYNC_ATR_UPD_TIME, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_RECORD_PROPERTY_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_PROPERTY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_PROPERTY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_PROPERTY_DS_IDENTIFIER + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_PROPERTY_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SECTION_ID", DATA_RECORD_PROPERTY_DS_IDENTIFIER + "$SECTION_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, DATA_RECORD_PROPERTY_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DATA_RECORD_PROPERTY_DS_IDENTIFIER + "$FFT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_STRING, DATA_RECORD_PROPERTY_DS_IDENTIFIER + EF_VALUE_STRING, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$VALIDITY_INDICATOR", DATA_RECORD_PROPERTY_DS_IDENTIFIER + "$VALIDITY_INDICATOR", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, DATA_RECORD_PROPERTY_DS_IDENTIFIER + EF_ROOT_RECORD_ID, false, true, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_RECORD_VALUE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_VALUE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_VALUE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_ROOT_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SECTION_ID", DATA_RECORD_VALUE_DS_IDENTIFIER + "$SECTION_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, true, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_SEQUENCE, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_VALUE_SEQUENCE, true, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARENT_VALUE_SEQUENCE", DATA_RECORD_VALUE_DS_IDENTIFIER + "$PARENT_VALUE_SEQUENCE", false, false, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DATA_RECORD_VALUE_DS_IDENTIFIER + "$FFT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_VALUE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_CONCEPT_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_VALUE_CONCEPT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FIELD_ID, DATA_RECORD_VALUE_DS_IDENTIFIER + EF_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TEXT", DATA_RECORD_VALUE_DS_IDENTIFIER + "$TEXT", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_RECORD_ATTACHMENT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_ATTACHMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_ATTACHMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTACHMENT_ID, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_ATTACHMENT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_CONTEXT_ID, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_FILE_CONTEXT_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_NAME, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_FILE_NAME, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_SIZE, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_FILE_SIZE, false, true, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_DATA, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_FILE_DATA, false, false, T8DataType.BYTE_ARRAY));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_MD5_CHECKSUM, DATA_RECORD_ATTACHMENT_DS_IDENTIFIER + EF_MD5_CHECKSUM, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_REQUIREMENT_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CLASS_ID, DATA_REQUIREMENT_DS_IDENTIFIER + EF_CLASS_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, DATA_REQUIREMENT_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, DATA_REQUIREMENT_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, DATA_REQUIREMENT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, DATA_REQUIREMENT_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, DATA_REQUIREMENT_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, DATA_REQUIREMENT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_DATA_TYPE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_DATA_TYPE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_DR_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SECTION_ID", DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + "$SECTION_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_PROPERTY_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, true, true, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_DATA_TYPE_ID, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_PARENT_DATA_TYPE_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARENT_DATA_TYPE_SEQ", DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + "$PARENT_DATA_TYPE_SEQ", false, false, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ATTRIBUTES", DATA_REQUIREMENT_DATA_TYPE_DS_IDENTIFIER + "$ATTRIBUTES", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_DR_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INDEPENDENT_INDICATOR, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_INDEPENDENT_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, DATA_REQUIREMENT_INSTANCE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_REQUIREMENT_VALUE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_REQUIREMENT_VALUE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_REQUIREMENT_VALUE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_VALUE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_PROPERTY_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FIELD_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LOWER_BOUND_VALUE, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_LOWER_BOUND_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPPER_BOUND_VALUE, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_UPPER_BOUND_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_CONCEPT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UOM_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_UOM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_QOM_ID, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_QOM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_APPROVED_INDICATOR, DATA_REQUIREMENT_VALUE_DS_IDENTIFIER + EF_APPROVED_INDICATOR, false, true, T8DataType.STRING));
        definitions.add(entityDefinition);

        // ORG_ROOT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ROOT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ROOT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_ROOT_DS_IDENTIFIER + EF_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ORG_ROOT_DS_IDENTIFIER + EF_ODS_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_ROOT_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ORG_ROOT_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_ROOT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_ROOT_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ORG_ROOT_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_ROOT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ORG_STRUCTURE_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_STRUCTURE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_STRUCTURE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_STRUCTURE_DS_IDENTIFIER + EF_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_ORG_ID, ORG_STRUCTURE_DS_IDENTIFIER + EF_PARENT_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_TYPE_ID, ORG_STRUCTURE_DS_IDENTIFIER + EF_ORG_TYPE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_ORG_ID, ORG_STRUCTURE_DS_IDENTIFIER + EF_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_LEVEL, ORG_STRUCTURE_DS_IDENTIFIER + EF_ORG_LEVEL, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LFT, ORG_STRUCTURE_DS_IDENTIFIER + EF_LFT, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RGT, ORG_STRUCTURE_DS_IDENTIFIER + EF_RGT, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_STRUCTURE_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ORG_STRUCTURE_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_STRUCTURE_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_STRUCTURE_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ORG_STRUCTURE_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_STRUCTURE_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ORG_DR_COMMENT_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_DR_COMMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_DR_COMMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_COMMENT_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_COMMENT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_COMMENT_TYPE_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_COMMENT_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_DR_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_PROPERTY_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FIELD_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_COMMENT, ORG_DR_COMMENT_DS_IDENTIFIER + EF_COMMENT, false, true, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STANDARD_INDICATOR, ORG_DR_COMMENT_DS_IDENTIFIER + EF_STANDARD_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ORG_DR_COMMENT_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_DR_COMMENT_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ORG_DR_COMMENT_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_DR_COMMENT_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ORG_ONTOLOGY_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_ONTOLOGY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_ONTOLOGY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODS_ID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_ODS_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ODT_ID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_ODT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ACTIVE_INDICATOR, ORG_ONTOLOGY_DS_IDENTIFIER + EF_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_ONTOLOGY_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ORG_ONTOLOGY_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_ONTOLOGY_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // ORG_TERMINOLOGY_DE_IDENTIFIER Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(ORG_TERMINOLOGY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(ORG_TERMINOLOGY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ORG_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_LANGUAGE_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_LANGUAGE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TERM, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ABBREVIATION, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DEFINITION, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CODE, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_CONCEPT_TYPE_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_CONCEPT_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_INSERTED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_ID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_UPDATED_BY_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_BY_IID, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_UPDATED_AT, ORG_TERMINOLOGY_DS_IDENTIFIER + EF_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        return definitions;
    }
}
