package com.pilog.t8.data.alteration;

import com.pilog.json.JsonObject;
import java.io.Serializable;

/**
 * This interface defines a data change operation that must be applied as an atomic action.
 * - An alteration forms part of a sequence of steps in an alteration package (packageIid).
 * - An alteration is always derived from the alteration definition (alterationId).
 * - An alteration can only ever employ one of three alteration methods: INSERT/UPDATE/DELETE {@code T8DataAlterationMethod}.
 * - Each alteration must provide support for serialization to JSON.
 * - An alteration is not concerned with its own persistence of where it fits into any larger operation.
 *   It is simply an isolated unit of work with a label.
 *
 * @author Bouwer du Preez
 */
public interface T8DataAlteration extends Serializable
{
    public enum T8DataAlterationMethod {INSERT, UPDATE, DELETE};

    public T8DataAlterationMethod getAlterationMethod();
    public String getPackageIid();
    public String getAlterationId();
    public int getStep();
    public JsonObject serialize();
}
