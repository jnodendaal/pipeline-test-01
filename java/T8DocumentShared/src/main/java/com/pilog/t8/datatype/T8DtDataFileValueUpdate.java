package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileValueUpdate;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarecord.ValueContent;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileValueUpdate extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE_VALUE";

    public T8DtDataFileValueUpdate(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileValueUpdate.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFileValueUpdate valueUpdate;
            JsonObject updateObject;
            ValueContent newValue;

            valueUpdate = (T8DataFileValueUpdate)object;
            newValue = valueUpdate.getNewValue();

            // Create the json object.
            updateObject = new JsonObject();
            updateObject.add("recordId", valueUpdate.getRecordId());
            updateObject.add("propertyId", valueUpdate.getPropertyId());
            updateObject.add("fieldId", valueUpdate.getFieldId());
            updateObject.add("newValue", T8DocumentSerializer.serializeValueContent(newValue));
            return updateObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileValueUpdate deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DataFileValueUpdate valueUpdate;
            JsonObject object;

            object = (JsonObject)jsonValue;
            valueUpdate = new T8DataFileValueUpdate();
            valueUpdate.setRecordId(object.getString("recordId"));
            valueUpdate.setPropertyId(object.getString("propertyId"));
            valueUpdate.setFieldId(object.getString("fieldId"));
            valueUpdate.setNewValue(T8DocumentSerializer.deserializeValueContent(object.getJsonObject("newValue")));
            return valueUpdate;
        }
        else return null;
    }
}
