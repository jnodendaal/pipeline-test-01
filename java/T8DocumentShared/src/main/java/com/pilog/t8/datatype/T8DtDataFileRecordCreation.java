package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordCreation;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileRecordCreation extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE_RECORD_CREATION";

    public T8DtDataFileRecordCreation(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileRecordCreation.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFileRecordCreation creation;
            JsonObject jsonCreation;

            creation = (T8DataFileRecordCreation)object;

            // Create the json object.
            jsonCreation = new JsonObject();
            jsonCreation.add("recordId", creation.getRecordId());
            jsonCreation.add("propertyId", creation.getPropertyId());
            jsonCreation.add("fieldId", creation.getFieldId());
            jsonCreation.add("drInstanceId", creation.getDrInstanceId());
            jsonCreation.add("temporaryId", creation.getTemporaryId());
            return jsonCreation;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileRecordCreation deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DataFileRecordCreation creation;
            JsonObject object;

            object = (JsonObject)jsonValue;
            creation = new T8DataFileRecordCreation();
            creation.setRecordId(object.getString("recordId"));
            creation.setPropertyId(object.getString("propertyId"));
            creation.setFieldId(object.getString("fieldId"));
            creation.setDrInstanceId(object.getString("drInstanceId"));
            creation.setTemporaryId(object.getString("temporaryId"));
            return creation;
        }
        else return null;
    }
}
