package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;

/**
 * @author Bouwer du Preez
 */
public class FieldRequirement extends ValueRequirement
{
    private String code;
    private String term;
    private String definition;
    private String abbreviation;

    public FieldRequirement()
    {
        super(RequirementType.FIELD_TYPE);
    }

    public FieldRequirement(String conceptID)
    {
        super(RequirementType.FIELD_TYPE, conceptID);
    }

    public String getFieldId()
    {
        return getValue();
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    /**
     * Adds the terminology of this value Requirement and of all its
     * descendants to the supplied list.
     * @param terminologyCollector The list to which terminology will be added.
     */
    @Override
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        super.addContentTerminology(terminologyCollector);
        terminologyCollector.addTerminology(null, T8OntologyConceptType.PROPERTY, getFieldId(), null, code, null, term, null, abbreviation, null, definition);
    }

    /**
     * Sets the terminology of this value Requirement and all of its descendants
     * by retrieval from the supplied terminology provider.
     * @param terminologyProvider The terminology provider form which to fetch terminology.
     */
    @Override
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        super.setContentTerminology(terminologyProvider);
        terminology = terminologyProvider.getTerminology(null, getFieldId());
        if (terminology != null)
        {
            this.code = terminology.getCode();
            this.term = terminology.getTerm();
            this.definition = terminology.getDefinition();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.code = null;
            this.term = null;
            this.definition = null;
            this.abbreviation = null;
        }
    }
}
