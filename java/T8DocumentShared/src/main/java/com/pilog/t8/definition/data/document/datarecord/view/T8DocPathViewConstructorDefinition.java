package com.pilog.t8.definition.data.document.datarecord.view;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.view.T8DataRecordViewConstructor;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DocPathViewConstructorDefinition extends T8DataRecordViewConstructorDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_VIEW_CONSTRUCTOR_DOCPATH";
    public static final String DISPLAY_NAME = "DocPath View Constructor";
    public static final String DESCRIPTION = "A definition that specifies a view constructor that uses DocPath to extract data from input data files.";
    public enum Datum {DOCPATH_EXPRESSION};
    // -------- Definition Meta-Data -------- //

    public T8DocPathViewConstructorDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOCPATH_EXPRESSION.toString(), "DocPath Expression",  "The DocPath expression to extract a data value from an input record"));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DataRecordViewConstructor createNewViewConstructor(T8DataTransaction tx)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.view.T8DocPathDataRecordViewConstructor").getConstructor(T8DataTransaction.class, this.getClass());
            return (T8DataRecordViewConstructor)constructor.newInstance(tx, this);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Merger.", e);
        }
    }

    @Override
    public List<String> getOutputParameterIdentifiers()
    {
        return ArrayLists.newArrayList(getIdentifier());
    }

    public String getDocPathExpression()
    {
        return (String)getDefinitionDatum(Datum.DOCPATH_EXPRESSION.toString());
    }

    public void setDocPathExpression(String expression)
    {
        setDefinitionDatum(Datum.DOCPATH_EXPRESSION.toString(), expression);
    }
}
