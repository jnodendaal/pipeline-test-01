package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class RecordValueStep extends DefaultPathStep implements PathStep
{
    public RecordValueStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateInstanceStep(((DataRecord)input).getAllPropertyValues());
        }
        else if (input instanceof RecordProperty)
        {
            RecordProperty inputProperty;
            RecordValue inputPropertyValue;

            inputProperty = (RecordProperty)input;
            inputPropertyValue = inputProperty.getRecordValue();
            return inputPropertyValue != null ? evaluateInstanceStep(ArrayLists.newArrayList(inputPropertyValue)) : new ArrayList<RecordValue>();
        }
        else if (input instanceof RecordValue)
        {
            RecordValue inputValue;

            inputValue = (RecordValue)input;
            return evaluateInstanceStep(inputValue.getSubValues());
        }
        else if (input instanceof List)
        {
            List<RecordValue> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<RecordValue>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<RecordValue>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Record Value step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<RecordValue> evaluateInstanceStep(List<RecordValue> valueList)
    {
        // If we have valid ID's to use for narrowing of the value list do it now.
        if (idList != null)
        {
            Iterator<RecordValue> valueIterator;

            valueIterator = valueList.iterator();
            while (valueIterator.hasNext())
            {
                RecordValue nextValue;

                nextValue = valueIterator.next();
                if (!idList.contains(nextValue.getValueRequirement().getRequirementType().toString()))
                {
                    valueIterator.remove();
                }
            }
        }

        // Now run through the remaining value list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<RecordValue> recordIterator;

            // Iterator over the records and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            recordIterator = valueList.iterator();
            while (recordIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                RecordValue nextValue;

                // Create a map containing the input parameters available to the predicate expression.
                nextValue = recordIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("VALUE", nextValue);
                inputParameters.put("REQUIREMENT_TYPE_ID", nextValue.getRequirementTypeID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        recordIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return valueList;
    }
}
