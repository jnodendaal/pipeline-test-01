package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordValueMerger;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordReferenceMergerDefinition extends T8DataRecordValueMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_VALUE_MERGER_REFERENCE";
    public static final String DISPLAY_NAME = "Reference Merger";
    public static final String DESCRIPTION = "A definition that specifies merging rules applicable to Data Record Reference values.";
    public static final String IDENTIFIER_PREFIX = "VALUE_MERGER_REFERENCE_";
    public enum Datum {CONDITION_EXPRESSION,
                       REFERENCE_MATCH_TYPE,
                       REFERENCE_MATCH_EXPRESSION,
                       REFERENCE_MERGE_METHOD,
                       REFERENCE_MISMATCH_ACTION,
                       MAXIMUM_REFERENCE_COUNT,
                       INCLUDE_DR_INSTANCE_ONTOLOGY};
    // -------- Definition Meta-Data -------- //

    public enum ReferenceMatchType {RECORD_ID, SOURCE_RECORD_ID_TAG, DEFINITION, EXPRESSION};
    public enum ReferenceMergeMethod {MERGE_REFERENCE_CONTENT, CHANGE_DR_INSTANCE_AND_MERGE_REFERENCE_CONTENT, SUBSTITUTE_REFERENCE, SYNCHRONIZE_REFERENCE, IGNORE};
    public enum ReferenceMismatchAction {ADD_REFERENCE, ADD_FFT, IGNORE, SUBSTITUTE_ALL_REFERENCES};

    public T8DataRecordReferenceMergerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will be evaluated to determine whether or not this merger is applicable to a RecordValue."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REFERENCE_MATCH_TYPE.toString(), "Reference Match Type",  "The method to use when identifying a destination and source reference match to be merged.", ReferenceMatchType.RECORD_ID.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.REFERENCE_MATCH_EXPRESSION.toString(), "Reference Match Expression", "The expression that will be evaluated on each existing destination reference to determine a match with a source reference."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REFERENCE_MERGE_METHOD.toString(), "Reference Merge Method",  "The method to use when merging matched references.", ReferenceMergeMethod.MERGE_REFERENCE_CONTENT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.REFERENCE_MISMATCH_ACTION.toString(), "Reference Mismatch Action",  "The action to be performed when a source reference could not be matched with one of the existing destination references.", ReferenceMismatchAction.ADD_REFERENCE.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.MAXIMUM_REFERENCE_COUNT.toString(), "Maximum Reference Count",  "The maximum number of references allowed, before all additional references will be merged straight to FFT (a value of 0 indicates that reference count is unlimited).", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.INCLUDE_DR_INSTANCE_ONTOLOGY.toString(), "Include DR Instance Ontology",  "If set, any new DR Instances retrieved for merging operations, will have their ontology included.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.REFERENCE_MATCH_TYPE.toString().equals(datumIdentifier)) return createStringOptions(ReferenceMatchType.values());
        else if (Datum.REFERENCE_MERGE_METHOD.toString().equals(datumIdentifier)) return createStringOptions(ReferenceMergeMethod.values());
        else if (Datum.REFERENCE_MISMATCH_ACTION.toString().equals(datumIdentifier)) return createStringOptions(ReferenceMismatchAction.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
    }

    @Override
    public T8DataRecordValueMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.merger.T8DataRecordReferenceMerger").getConstructor(this.getClass(), T8DataRecordMergeContext.class);
            return (T8DataRecordValueMerger)constructor.newInstance(this, mergeContext);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Value Merger.", e);
        }
    }

    public String getConditionExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITION_EXPRESSION.toString());
    }

    public void setConditionExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION.toString(), expression);
    }

    public ReferenceMatchType getReferenceMatchType()
    {
        String datumValue;

        datumValue = (String)getDefinitionDatum(Datum.REFERENCE_MATCH_TYPE.toString());
        return datumValue != null ? ReferenceMatchType.valueOf(datumValue) : null;
    }

    public void setReferenceMatchType(ReferenceMatchType matchType)
    {
        setDefinitionDatum(Datum.REFERENCE_MATCH_TYPE.toString(), matchType != null ? matchType.toString() : null);
    }

    public String getReferenceMatchExpression()
    {
        return (String)getDefinitionDatum(Datum.REFERENCE_MATCH_EXPRESSION.toString());
    }

    public void setReferenceMatchExpression(String expression)
    {
        setDefinitionDatum(Datum.REFERENCE_MATCH_EXPRESSION.toString(), expression);
    }

    public ReferenceMergeMethod getReferenceMergeMethod()
    {
        String datumValue;

        datumValue = (String)getDefinitionDatum(Datum.REFERENCE_MERGE_METHOD.toString());
        return datumValue != null ? ReferenceMergeMethod.valueOf(datumValue) : null;
    }

    public void setReferenceMergeMethod(ReferenceMergeMethod mergeMethod)
    {
        setDefinitionDatum(Datum.REFERENCE_MERGE_METHOD.toString(), mergeMethod != null ? mergeMethod.toString() : null);
    }

    public ReferenceMismatchAction getReferenceMismatchAction()
    {
        String datumValue;

        datumValue = (String)getDefinitionDatum(Datum.REFERENCE_MISMATCH_ACTION.toString());
        return datumValue != null ? ReferenceMismatchAction.valueOf(datumValue) : null;
    }

    public void setReferenceMismatchAction(ReferenceMismatchAction mismatchAction)
    {
        setDefinitionDatum(Datum.REFERENCE_MISMATCH_ACTION.toString(), mismatchAction != null ? mismatchAction.toString() : null);
    }

    public int getMaximumReferenceCount()
    {
        Integer datumValue;

        datumValue = (Integer)getDefinitionDatum(Datum.MAXIMUM_REFERENCE_COUNT.toString());
        return datumValue != null ? datumValue : 0;
    }

    public void setMaximumReferenceCount(int count)
    {
        setDefinitionDatum(Datum.MAXIMUM_REFERENCE_COUNT.toString(), count);
    }

    public boolean isIncludeDRInstanceOntology()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.INCLUDE_DR_INSTANCE_ONTOLOGY.toString());
        return value != null && value;
    }

    public void setIncludeDRInstanceOntology(boolean include)
    {
        setDefinitionDatum(Datum.INCLUDE_DR_INSTANCE_ONTOLOGY.toString(), include);
    }
}
