package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.dataquality.T8DataQualityMetrics;
import com.pilog.t8.data.document.dataquality.T8DataQualityMetricsGroup;
import com.pilog.t8.data.document.dataquality.T8GroupedDataQualityReport;

/**
 * @author Bouwer du Preez
 */
public class T8DtGroupedDataQualityReport extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_QUALITY_REPORT_GROUPED";

    public T8DtGroupedDataQualityReport(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8GroupedDataQualityReport.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8GroupedDataQualityReport dqReport;
            JsonArray groupArray;
            JsonObject updateObject;

            // Create the file update object.
            dqReport = (T8GroupedDataQualityReport)object;
            updateObject = new JsonObject();
            groupArray = new JsonArray();
            updateObject.add("groups", groupArray);

            // Serialize the value updates.
            for (T8DataQualityMetricsGroup dqGroup : dqReport.getGroups())
            {
                T8DataQualityMetrics metrics;
                JsonObject dqMetricsObject;
                JsonObject dqGroupObject;

                // Serialize the group object.
                dqGroupObject = new JsonObject();
                dqGroupObject.add("id", dqGroup.getId());
                dqGroupObject.add("code", dqGroup.getCode());
                dqGroupObject.add("term", dqGroup.getTerm());
                dqGroupObject.add("definition", dqGroup.getDefinition());
                dqGroupObject.add("ocId", dqGroup.getOcId());
                dqGroupObject.add("ocTerm", dqGroup.getOcTerm());

                // Serialize the metrics object.
                metrics = dqGroup.getMetrics();
                dqMetricsObject = new JsonObject();
                dqMetricsObject.add("recordCount", metrics.getRecordCount());
                dqMetricsObject.add("propertyCount", metrics.getPropertyCount());
                dqMetricsObject.add("propertyDataCount", metrics.getPropertyDataCount());
                dqMetricsObject.add("propertyValidCount", metrics.getPropertyValidCount());
                dqMetricsObject.add("propertyVerificationCount", metrics.getPropertyVerificationCount());
                dqMetricsObject.add("propertyAccurateCount", metrics.getPropertyAccurateCount());
                dqMetricsObject.add("characteristicCount", metrics.getCharacteristicCount());
                dqMetricsObject.add("characteristicDataCount", metrics.getCharacteristicDataCount());
                dqMetricsObject.add("characteristicValidCount", metrics.getCharacteristicValidCount());
                dqMetricsObject.add("characteristicVerificationCount", metrics.getCharacteristicVerificationCount());
                dqMetricsObject.add("characteristicAccurateCount", metrics.getCharacteristicAccurateCount());

                // Add the metrics to the group.
                dqGroupObject.add("metrics", dqMetricsObject);

                // Add the final group to the array.
                groupArray.add(dqGroupObject);
            }

            return updateObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8GroupedDataQualityReport deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8GroupedDataQualityReport dqReport;
            JsonArray groupArray;
            JsonObject object;

            object = (JsonObject)jsonValue;
            dqReport = new T8GroupedDataQualityReport();
            groupArray = object.getJsonArray("groups");
            if (groupArray != null)
            {
                for (JsonValue dqGroupValue : groupArray.values())
                {
                    T8DataQualityMetricsGroup group;
                    JsonObject dqGroupObject;
                    JsonObject dqMetricsObject;

                    // Deserialize the group.
                    dqGroupObject = dqGroupValue.asObject();
                    group = new T8DataQualityMetricsGroup(dqGroupObject.getString("id"));
                    group.setCode(dqGroupObject.getString("code"));
                    group.setTerm(dqGroupObject.getString("term"));
                    group.setDefinition(dqGroupObject.getString("definition"));
                    group.setOcId(dqGroupObject.getString("ocId"));
                    group.setOcTerm(dqGroupObject.getString("ocTerm"));
                    dqReport.addGroup(group);

                    // Deserialize the group metrics.
                    dqMetricsObject = dqGroupObject.getJsonObject("metrics");
                    if (dqMetricsObject != null)
                    {
                        T8DataQualityMetrics metrics;

                        metrics = new T8DataQualityMetrics();
                        metrics.setRecordCount(dqMetricsObject.getInt("recordCount", -1));
                        metrics.setPropertyCount(dqMetricsObject.getInt("propertyCount", -1));
                        metrics.setPropertyDataCount(dqMetricsObject.getInt("propertyDataCount", -1));
                        metrics.setPropertyValidCount(dqMetricsObject.getInt("propertyValidCount", -1));
                        metrics.setPropertyVerificationCount(dqMetricsObject.getInt("propertyVerificationCount", -1));
                        metrics.setPropertyAccurateCount(dqMetricsObject.getInt("propertyAccurateCount", -1));
                        metrics.setCharacteristicCount(dqMetricsObject.getInt("characteristicCount", -1));
                        metrics.setCharacteristicDataCount(dqMetricsObject.getInt("characteristicDataCount", -1));
                        metrics.setCharacteristicValidCount(dqMetricsObject.getInt("characteristicValidCount", -1));
                        metrics.setCharacteristicVerificationCount(dqMetricsObject.getInt("characteristicVerificationCount", -1));
                        metrics.setCharacteristicAccurateCount(dqMetricsObject.getInt("characteristicAccurateCount", -1));
                        group.setMetrics(metrics);
                    }
                }
            }

            // Return the final deserialized report.
            return dqReport;
        }
        else return null;
    }
}
