package com.pilog.t8.data.ontology;

/**
 * @author Bouwer du Preez
 */
public enum T8StandardConceptRelationship 
{
    CONCEPT_TYPE_CODE_TYPES("2B5DA04408E04153A4009D3F206661AD");

    private String relationshipID;

    T8StandardConceptRelationship(String relationshipID)
    {
        this.relationshipID = relationshipID;
    }

    public String getRelationshipID()
    {
        return relationshipID;
    }
};
