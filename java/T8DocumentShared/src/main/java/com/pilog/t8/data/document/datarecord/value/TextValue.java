package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.HashMap;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class TextValue extends RecordValue
{
    public TextValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public String getText()
    {
        return getValue();
    }

    public void setText(String text)
    {
        setValue(text);
    }

    @Override
    public TextValueContent getContent()
    {
        TextValueContent content;

        content = new TextValueContent();
        content.setText(value);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        TextValueContent content;

        content = (TextValueContent)valueContent;
        setText(content.getText());
    }

    @Override
    public List<HashMap<String, Object>> getDataRows()
    {

        HashMap<String, Object> dataRow;
        RecordSection recordSection;
        RecordProperty recordProperty;
        RecordValue parentRecordValue;
        RequirementType requirementType;
        DataRecord record;

        // Get the values from which data will be extracted.
        parentRecordValue = getParentRecordValue();
        requirementType = RequirementType.TEXT_TYPE;
        recordProperty = getParentRecordProperty();
        recordSection = recordProperty.getParentRecordSection();
        record = recordSection.getParentDataRecord();

        // Extract the data row.
        dataRow = new HashMap<>();
        dataRow.put("RECORD_ID", record.getID());
        dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
        dataRow.put("DR_ID", record.getDataRequirement().getConceptID());
        dataRow.put("SECTION_ID", getSectionID());
        dataRow.put("PROPERTY_ID", getPropertyID());
        dataRow.put("FIELD_ID", getFieldID());
        dataRow.put("DATA_TYPE_ID", requirementType.toString());
        dataRow.put("DATA_TYPE_SEQUENCE", getValueRequirement().getSequence());
        dataRow.put("VALUE_SEQUENCE", getSequence());
        dataRow.put("PARENT_VALUE_SEQUENCE", parentRecordValue != null ? parentRecordValue.getSequence() : null);
        dataRow.put("VALUE", null);
        dataRow.put("TEXT", getText());
        dataRow.put("FFT", getFFT());
        dataRow.put("VALUE_ID", getPrescribedValueID());

        return ArrayLists.newArrayList(dataRow);
    }
}
