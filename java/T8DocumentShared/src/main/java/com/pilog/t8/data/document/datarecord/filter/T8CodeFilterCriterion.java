package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8CodeFilterCriterion implements Serializable
{
    private String codeTypeId;
    private Object filterValue;
    private List<Object> dataTypes;
    private boolean caseInsensitive;
    private DataFilterOperator operator;
    private boolean allowRemoval;
    private CodeCriterionTarget target;

    public enum CodeCriterionTarget
    {
        DR_INSTANCE, // Criterion will be matched on DR Instance codes.
        RECORD // Criterion will be matched on Record codes.
    };

    public T8CodeFilterCriterion(String codeTypeId)
    {
        this.codeTypeId = codeTypeId;
        this.operator = DataFilterOperator.EQUAL;
        this.dataTypes = new ArrayList<Object>();
        this.allowRemoval = true;
        this.target = CodeCriterionTarget.RECORD;
    }

    public T8CodeFilterCriterion(CodeCriterionTarget target, String codeTypeId, List<Object> dataTypes, DataFilterOperator operator, Object filterValue)
    {
        this.target = target;
        this.codeTypeId = codeTypeId;
        this.dataTypes = new ArrayList<Object>();
        this.operator = operator;
        this.filterValue = filterValue;
        this.allowRemoval = true;

        if (dataTypes != null) this.dataTypes.addAll(dataTypes);
    }

    public T8CodeFilterCriterion copy()
    {
        T8CodeFilterCriterion copiedCriterion;

        copiedCriterion = new T8CodeFilterCriterion(target, codeTypeId, dataTypes, operator, filterValue);
        copiedCriterion.setAllowRemoval(allowRemoval);
        return copiedCriterion;
    }

    public boolean hasCriterion()
    {
        if (operator == DataFilterOperator.IS_NULL) return true; // This operator does not require a filter value.
        else if (operator == DataFilterOperator.IS_NOT_NULL) return true; // This operator does not require a filter value.
        else if (filterValue == null) return false; // All other operators require a filter value.
        else if (filterValue instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return !Strings.isNullOrEmpty((String)filterValue);
        }
        else return true;
    }

    public CodeCriterionTarget getTarget()
    {
        return target;
    }

    public void setTarget(CodeCriterionTarget target)
    {
        this.target = target;
    }

    public String getCodeTypeId()
    {
        return codeTypeId;
    }

    public void setCodeTypeId(String codeTypeId)
    {
        this.codeTypeId = codeTypeId;
    }

    public Object getFilterValue()
    {
        return filterValue;
    }

    public void setFilterValue(Object filterValue)
    {
        this.filterValue = filterValue;
    }

    public List<Object> getDataTypes()
    {
        return dataTypes;
    }

    public void setDataTypes(List<Object> dataTypes)
    {
        this.dataTypes = dataTypes;
    }

    public boolean isCaseInsensitive()
    {
        return caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        this.caseInsensitive = caseInsensitive;
    }

    public DataFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(DataFilterOperator operator)
    {
        this.operator = operator;
    }

    public boolean isAllowRemoval()
    {
        return allowRemoval;
    }

    public void setAllowRemoval(boolean allowRemoval)
    {
        this.allowRemoval = allowRemoval;
    }
}
