package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyDefinition implements Serializable
{
    private String id;
    private String irdi;
    private String conceptId;
    private String languageId;
    private String definition;
    private boolean primary;
    private boolean standard;
    private boolean active;

    /**
     * Creates a new instance of the {@code T8OntologyDefinition} linked to the
     * specified concept ID for the specified language identifier.
     *
     * @param id The {@code String} unique definition identifier
     * @param conceptId The {@code String} concept ID to which the current
     *      definition is linked
     * @param languageId The {@code String} language identifier associated with
     *      the current definition
     * @param definition The actual definition {@code String} value
     * @param primary Indicates whether or not this definition is the primary definition of its parent concept.
     * @param standard Whether or not the definition value has been standardized
     *      externally to the current client data
     * @param active {@code true} if the definition should be active within the
     *      current client data structure
     */
    public T8OntologyDefinition(String id, String conceptId, String languageId, String definition, boolean primary, boolean standard, boolean active)
    {
        this.id = id;
        this.conceptId = conceptId;
        this.languageId = languageId;
        this.definition = definition;
        this.primary = primary;
        this.standard = standard;
        this.active = active;
    }

    public T8OntologyDefinition(String id, String conceptId, String languageId, String definition, boolean standard, boolean active)
    {
        this(id, conceptId, languageId, definition, false, standard, active);
    }

    public T8OntologyDefinition copy()
    {
        T8OntologyDefinition copy;

        // Copy the definition.
        copy = new T8OntologyDefinition(id, conceptId, languageId, definition, primary, standard, active);
        return copy;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getLanguageID()
    {
        return languageId;
    }

    public void setLanguageID(String languageID)
    {
        this.languageId = languageID;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public boolean isPrimary()
    {
        return primary;
    }

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public boolean isStandard()
    {
        return standard;
    }

    public void setStandard(boolean standard)
    {
        this.standard = standard;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isEqualTo(T8OntologyDefinition inputDefinition)
    {
        if (inputDefinition == null) return false;
        else if (!Objects.equals(id, inputDefinition.getID())) return false;
        else if (!Objects.equals(irdi, inputDefinition.getIrdi())) return false;
        else if (!Objects.equals(languageId, inputDefinition.getLanguageID())) return false;
        else if (!Objects.equals(conceptId, inputDefinition.getConceptID())) return false;
        else if (!Objects.equals(definition, inputDefinition.getDefinition())) return false;
        else if (!Objects.equals(standard, inputDefinition.isStandard())) return false;
        else if (!Objects.equals(active, inputDefinition.isActive())) return false;
        else return true;
    }

    @Override
    public String toString()
    {
        return "T8OntologyDefinition{" + "id=" + id + ", irdi=" + irdi + ", definition=" + definition + '}';
    }
}
