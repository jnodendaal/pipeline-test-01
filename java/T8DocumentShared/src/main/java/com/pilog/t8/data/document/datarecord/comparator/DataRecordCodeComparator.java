/**
 * Created on Apr 12, 2017, 3:45:59 PM
 */
package com.pilog.t8.data.document.datarecord.comparator;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.ontology.T8OntologyCode;
import java.util.Comparator;

/**
 * @author Gavin Boshoff
 */
public class DataRecordCodeComparator implements Comparator<DataRecord>
{
    private final String codeTypeId;

    public DataRecordCodeComparator(String codeTypeId)
    {
        if (!T8IdentifierUtilities.isConceptID(codeTypeId)) throw new RuntimeException("Code Type ID : {"+codeTypeId+"} is not a valid Concept ID.");
        this.codeTypeId = codeTypeId;
    }

    @Override
    public int compare(DataRecord drOne, DataRecord drTwo)
    {
        T8OntologyCode codeOne;
        T8OntologyCode codeTwo;

        codeOne = drOne.getOntology().getCodeByType(codeTypeId);
        codeTwo = drTwo.getOntology().getCodeByType(codeTypeId);

        if (codeOne == null)
        {
            if (codeTwo == null) return 0;
            else return -1;
        } else if (codeTwo == null) return 1;
        else return codeOne.getCode().compareTo(codeTwo.getCode());
    }
}