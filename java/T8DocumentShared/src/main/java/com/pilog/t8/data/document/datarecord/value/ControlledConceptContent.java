package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class ControlledConceptContent extends ValueContent
{
    private String conceptId;
    private String irdi;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;

    public ControlledConceptContent()
    {
        super(RequirementType.CONTROLLED_CONCEPT);
    }

    public ControlledConceptContent(String conceptId)
    {
        this();
        this.conceptId = conceptId;
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public void setConceptId(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public String getCodeId()
    {
        return codeId;
    }

    public void setCodeId(String codeId)
    {
        this.codeId = codeId;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTermId()
    {
        return termId;
    }

    public void setTermId(String termId)
    {
        this.termId = termId;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinitionId()
    {
        return definitionId;
    }

    public void setDefinitionId(String definitionId)
    {
        this.definitionId = definitionId;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviationId()
    {
        return abbreviationId;
    }

    public void setAbbreviationId(String abbreviationId)
    {
        this.abbreviationId = abbreviationId;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof ControlledConceptContent)
        {
            ControlledConceptContent toConceptContent;

            toConceptContent = (ControlledConceptContent)toContent;
            if (!Objects.equals(conceptId, toConceptContent.getConceptId())) return false;
            else if (!Objects.equals(irdi, toConceptContent.getIrdi())) return false;
            else if (!Objects.equals(term, toConceptContent.getTerm())) return false;
            else if (!Objects.equals(code, toConceptContent.getCode())) return false;
            else if (!Objects.equals(abbreviation, toConceptContent.getAbbreviation())) return false;
            else if (!Objects.equals(definition, toConceptContent.getDefinition())) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof ControlledConceptContent)
        {
            ControlledConceptContent toConceptContent;

            toConceptContent = (ControlledConceptContent)toContent;
            if (!Objects.equals(conceptId, toConceptContent.getConceptId())) return false;
            else return true;
        }
        else return false;
    }
}
