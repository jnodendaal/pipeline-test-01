package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredNumberContent extends ValueContent
{
    private NumberContent number;
    private UnitOfMeasureContent unitOfMeasure;
    private QualifierOfMeasureContent qualifierOfMeasure;

    public MeasuredNumberContent()
    {
        super(RequirementType.MEASURED_NUMBER);
        this.number = new NumberContent();
        this.unitOfMeasure = new UnitOfMeasureContent();
        this.qualifierOfMeasure = new QualifierOfMeasureContent();
    }

    public NumberContent getNumber()
    {
        return number;
    }

    public void setNumber(NumberContent number)
    {
        this.number = number;
    }

    public UnitOfMeasureContent getUnitOfMeasure()
    {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasureContent unitOfMeasure)
    {
        this.unitOfMeasure = unitOfMeasure;
    }

    public QualifierOfMeasureContent getQualifierOfMeasure()
    {
        return qualifierOfMeasure;
    }

    public void setQualifierOfMeasure(QualifierOfMeasureContent qualifierOfMeasure)
    {
        this.qualifierOfMeasure = qualifierOfMeasure;
    }

    public boolean hasNumberContent()
    {
        return number != null && number.hasContent();
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof MeasuredNumberContent)
        {
            MeasuredNumberContent toMeasuredNumberContent;
            UnitOfMeasureContent toUnitOfMeasure;
            QualifierOfMeasureContent toQualifierOfMeasure;
            NumberContent toNumber;

            toMeasuredNumberContent = (MeasuredNumberContent)toContent;
            toNumber = toMeasuredNumberContent.getNumber();
            toUnitOfMeasure = toMeasuredNumberContent.getUnitOfMeasure();
            toQualifierOfMeasure = toMeasuredNumberContent.getQualifierOfMeasure();

            if (number == null)
            {
                if (toNumber != null && toNumber.hasContent()) return false;
            }
            else if (!number.isEqualTo(toNumber)) return false;

            if (unitOfMeasure == null)
            {
                if (toUnitOfMeasure != null && toUnitOfMeasure.hasContent()) return false;
            }
            else if (!unitOfMeasure.isEqualTo(toUnitOfMeasure)) return false;

            if (qualifierOfMeasure == null)
            {
                if (toQualifierOfMeasure != null && toQualifierOfMeasure.hasContent()) return false;
            }
            else if (!qualifierOfMeasure.isEqualTo(toQualifierOfMeasure)) return false;

            return true;
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof MeasuredNumberContent)
        {
            MeasuredNumberContent toMeasuredNumberContent;
            UnitOfMeasureContent toUnitOfMeasure;
            QualifierOfMeasureContent toQualifierOfMeasure;
            NumberContent toNumber;

            toMeasuredNumberContent = (MeasuredNumberContent)toContent;
            toNumber = toMeasuredNumberContent.getNumber();
            toUnitOfMeasure = toMeasuredNumberContent.getUnitOfMeasure();
            toQualifierOfMeasure = toMeasuredNumberContent.getQualifierOfMeasure();

            if (number == null)
            {
                if (toNumber != null && toNumber.hasContent()) return false;
            }
            else if (!number.isEquivalentTo(toNumber)) return false;

            if (unitOfMeasure == null)
            {
                if (toUnitOfMeasure != null && toUnitOfMeasure.hasContent()) return false;
            }
            else if (!unitOfMeasure.isEquivalentTo(toUnitOfMeasure)) return false;

            if (qualifierOfMeasure == null)
            {
                if (toQualifierOfMeasure != null && toQualifierOfMeasure.hasContent()) return false;
            }
            else if (!qualifierOfMeasure.isEquivalentTo(toQualifierOfMeasure)) return false;

            return true;
        }
        else return false;
    }
}
