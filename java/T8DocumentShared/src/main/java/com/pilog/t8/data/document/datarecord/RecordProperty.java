package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyValueChangedEvent;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class RecordProperty implements Value, Serializable
{
    private RecordSection parentRecordSection;
    private PropertyRequirement propertyRequirement;
    private RecordValue recordValue;
    private String fft;

    public RecordProperty(PropertyRequirement propertyRequirement)
    {
        this.parentRecordSection = null;
        this.propertyRequirement = propertyRequirement;
    }

    void setParent(RecordSection recordSection)
    {
        this.parentRecordSection = recordSection;
    }

    public DataRecord getParentDataRecord()
    {
        return parentRecordSection != null ? parentRecordSection.getParentDataRecord() : null;
    }

    public RecordSection getParentRecordSection()
    {
        return parentRecordSection;
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return propertyRequirement;
    }

    public ValueRequirement getValueRequirement()
    {
        return this.propertyRequirement.getValueRequirement();
    }

    public SectionRequirement getSectionRequirement()
    {
        return propertyRequirement != null ? propertyRequirement.getParentSectionRequirement() : null;
    }

    public DataRequirement getDataRequirement()
    {
        SectionRequirement sectionRequirement;

        sectionRequirement = getSectionRequirement();
        return sectionRequirement != null ? sectionRequirement.getParentDataRequirement() : null;
    }

    public DataRequirementInstance getDataRequirementInstance()
    {
        DataRequirement dataRequirement;

        dataRequirement = getDataRequirement();
        return dataRequirement != null ? dataRequirement.getParentDataRequirementInstance() : null;
    }

    public String getRecordID()
    {
        DataRecord parentRecord;

        parentRecord = getParentDataRecord();
        return parentRecord != null ? parentRecord.getID() : null;
    }

    public String getId()
    {
        return propertyRequirement.getId();
    }

    public String getPropertyID()
    {
        return propertyRequirement.getConceptID();
    }

    public String getDataRequirementID()
    {
        DataRequirement dataRequirement;

        dataRequirement = getDataRequirement();
        return dataRequirement != null ? dataRequirement.getConceptID() : null;
    }

    public String getDataRequirementInstanceID()
    {
        DataRequirementInstance drInstance;

        drInstance = getDataRequirementInstance();
        return drInstance != null ? drInstance.getConceptID() : null;
    }

    public String getSectionID()
    {
        SectionRequirement section;

        section = getSectionRequirement();
        return section != null ? section.getConceptID() : null;
    }

    public int getIndex()
    {
        if (parentRecordSection != null)
        {
            return parentRecordSection.getRecordPropertyIndex(this);
        }
        else return -1;
    }

    @Override
    public int getSubValueCount()
    {
        return recordValue != null ? 1 : 0;
    }

    public PropertyContent getContent()
    {
        PropertyContent content;

        content = new PropertyContent();
        content.setId(propertyRequirement.getConceptID());
        content.setFft(fft);
        content.setValue(recordValue != null ? recordValue.getContent() : null);
        return content;
    }

    public void setContent(PropertyContent content)
    {
        ValueContent valueContent;

        this.fft = content.getFft();
        valueContent = content.getValue();
        setContent(valueContent);
    }

    public void setContent(ValueContent valueContent)
    {
        if (valueContent != null)
        {
            ValueRequirement valueRequirement;

            valueRequirement = propertyRequirement.getValueRequirement();
            if (valueRequirement.getRequirementType().equals(valueContent.getType()))
            {
                RecordValue newValue;

                newValue = RecordValue.createValue(valueRequirement);
                newValue.setContent(valueContent);
                setRecordValue(newValue);
            }
            else throw new IllegalArgumentException("Cannot set value content of type '" + valueContent.getType() + "' on property with value requirement: " + propertyRequirement.getValueRequirement());
        }
        else setRecordValue(null);
    }

    public void setRequirement(PropertyRequirement requirement)
    {
        this.propertyRequirement = requirement;
        if (this.recordValue != null)
        {
            this.recordValue.setRequirement(propertyRequirement.getValueRequirement());
        }
    }

    public RecordPropertyPointer getPointer()
    {
        DataRecord parentRecord;

        parentRecord = getParentDataRecord();
        return new RecordPropertyPointer(parentRecord.getID(), parentRecord.getDataRequirementInstance().getConceptID(), parentRecordSection.getSectionID(), getPropertyID());
    }

    public List<Field> getFields()
    {
        if (recordValue != null)
        {
            return (List)recordValue.getSubValues(RequirementType.FIELD_TYPE, null, null);
        }
        else return null;
    }

    public Field getField(String fieldID)
    {
        if (recordValue != null)
        {
            return (Field)recordValue.getSubValue(RequirementType.FIELD_TYPE, fieldID, null);
        }
        else return null;
    }

    public Composite getOrAddComposite()
    {
        Composite composite;

        composite = (Composite)recordValue;
        if (composite != null)
        {
            return composite;
        }
        else
        {
            CompositeRequirement compositeRequirement;

            compositeRequirement = (CompositeRequirement)propertyRequirement.getValueRequirement();
            composite = (Composite)RecordValue.createValue(compositeRequirement);
            setRecordValue(composite);
            return composite;
        }
    }

    public Field getOrAddField(String fieldId)
    {
        Field field;

        field = getField(fieldId);
        if (field != null)
        {
            return field;
        }
        else
        {
            Composite composite;

            composite = getOrAddComposite();
            return composite.getOrAddField(fieldId);
        }
    }

    public RecordValue getOrAddValue()
    {
        if (recordValue != null)
        {
            return recordValue;
        }
        else
        {
            ValueRequirement valueRequirement;

            valueRequirement = propertyRequirement.getValueRequirement();
            if (valueRequirement != null)
            {
                setRecordValue(RecordValue.createValue(valueRequirement));
                return recordValue;
            }
            else throw new IllegalStateException("No value requirement specified for property: " + this.getId());
        }
    }

    public RecordValue getOrAddValue(String fieldId)
    {
        Field field;

        field = getOrAddField(fieldId);
        return field.getOrAddValue();
    }

    public RecordValue getRecordValue()
    {
        return recordValue;
    }

    public void setRecordValue(RecordValue value)
    {
        RecordValue oldValue;

        // Store the old value.
        oldValue = this.recordValue;
        if (oldValue != null)
        {
            oldValue.setParentRecordProperty(null);
        }

        // Set the new value.
        this.recordValue = value;
        if (this.recordValue != null)
        {
            this.recordValue.setParentRecordProperty(this);
        }

        // Fire a change event.
        fireRecordPropertyValueChangedEvent(oldValue, value, null);
    }

    public void clearContent()
    {
        fft = null;
        if (recordValue != null)
        {
            if (recordValue.isComposite())
            {
                ((Composite)recordValue).clearContent();
            }
            else if (recordValue.getValueRequirement().getRequirementType() != RequirementType.DOCUMENT_REFERENCE)
            {
                setRecordValue(null);
            }
        }
    }

    /**
     * Returns the plain value from the root RecordValue object (if any) of
     * this RecordProperty.
     * @return The plain value from the root RecordValue object (if any) of
     * this RecordProperty.
     */
    public String getPlainValue()
    {
        if (recordValue != null)
        {
            return recordValue.getPlainValue();
        }
        else return null;
    }

    public List<String> getPlainValueList()
    {
        List<String> plainValueList;
        List<RecordValue> valueList;

        valueList = getAllRecordValues();
        plainValueList = new ArrayList<String>();
        for (RecordValue value : valueList)
        {
            plainValueList.add(value.getPlainValue());
        }

        return plainValueList;
    }

    public RecordValue setPlainValue(String fieldID, String plainValue)
    {
        return setValue(fieldID, plainValue, false);
    }

    public RecordValue setValue(String fieldId, String plainValue, boolean clearDependants)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;

        valueRequirement = getPropertyRequirement().getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        if (RequirementType.getSimpleDataTypes().contains(requirementType))
        {
            RecordValue simpleValue;

            // Get the simple value to update (or create it if it does not yet exist).
            simpleValue = getRecordValue();
            if (simpleValue == null)
            {
                simpleValue = RecordValue.createValue(valueRequirement);
                simpleValue.setValue(plainValue);
                setRecordValue(simpleValue);
                return simpleValue;
            }
            else
            {
                simpleValue.setValue(plainValue);
                return simpleValue;
            }
        }
        else if (requirementType == RequirementType.CONTROLLED_CONCEPT)
        {
            ControlledConcept controlledConcept;

            // Make sure a controlled concept record value exists.
            controlledConcept = (ControlledConcept)getRecordValue();
            if (controlledConcept == null)
            {
                controlledConcept = new ControlledConcept(valueRequirement);
                setSubValue(controlledConcept);
            }

            // Set the new concept ID.
            controlledConcept.setConceptId(plainValue, clearDependants);
            return controlledConcept;
        }
        else if (requirementType == RequirementType.COMPOSITE_TYPE)
        {
            Composite compositeValue;

            // Get composite value or create it if it does not yet exist.
            compositeValue = (Composite)getRecordValue();
            if (compositeValue == null)
            {
                compositeValue = new Composite(valueRequirement);
                setRecordValue(compositeValue);
            }

            // Set the plain value on the field.
            return compositeValue.setFieldValue(fieldId, plainValue, false);
        }
        else throw new RuntimeException("Property value is not of simple type: " + valueRequirement.getRequirementType());
    }

    public void removeDocumentReference(String fieldID, String drInstanceID, String recordID)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;

        valueRequirement = getPropertyRequirement().getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            DocumentReferenceList referenceList;

            // Get the document reference remove it if it exists
            referenceList = (DocumentReferenceList)this.getSubValue(RequirementType.DOCUMENT_REFERENCE, null, null);
            if (referenceList != null)
            {
                referenceList.removeReference(recordID, true);
            }
        }
        else if (requirementType == RequirementType.COMPOSITE_TYPE)
        {
            Composite compositeValue;

            // Get the composite value and remove the document reference if is exists
            compositeValue = (Composite)getRecordValue();
            if (compositeValue != null)
            {
                compositeValue.removeRecordReference(fieldID, recordID);
            }
        } else throw new RuntimeException("Property Value Requirement is not Document Reference Type: " + requirementType);
    }

    public RecordValue addDocumentReference(String fieldID, String drInstanceID, String drGroupID, String recordID)
    {
        return addRecordReference(fieldID, drInstanceID, recordID);
    }

    public DocumentReferenceList addRecordReference(String fieldID, String drInstanceID, String recordID)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;

        valueRequirement = getPropertyRequirement().getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            DocumentReferenceList referenceList;

            // Get the document reference or create it if it does not yet exist.
            referenceList = (DocumentReferenceList)this.getSubValue(RequirementType.DOCUMENT_REFERENCE, null, null);
            if (referenceList == null)
            {
                referenceList = new DocumentReferenceList(valueRequirement);
                setRecordValue(referenceList);
            }

            // Now add the new reference.
            referenceList.addReference(recordID);
            return referenceList;
        }
        else if (requirementType == RequirementType.COMPOSITE_TYPE)
        {
            Composite compositeValue;

            // Get the composite value or create it if it does not yet exist.
            compositeValue = (Composite)getRecordValue();
            if (compositeValue == null)
            {
                compositeValue = new Composite(valueRequirement);
                setRecordValue(compositeValue);
            }

            // Add the reference.
            return compositeValue.addRecordReference(fieldID, recordID);
        }
        else throw new RuntimeException("Property Value Requirement is not Document Reference Type: " + requirementType);
    }

    public ArrayList<RecordValue> getAllRecordValues()
    {
        ArrayList<RecordValue> recordValues;

        recordValues = new ArrayList<RecordValue>();
        if (recordValue != null)
        {
            recordValues.add(recordValue);
            recordValues.addAll(recordValue.getDescendantValues());
        }

        return recordValues;
    }

    public List<RecordValue> getRecordValues(List<ValueRequirement> pathRequirements)
    {
        if (recordValue == null)
        {
            return new ArrayList<RecordValue>();
        }
        else
        {
            return recordValue.getRecordValues(pathRequirements);
        }
    }

    public String getFFT()
    {
        return fft;
    }

    public void setFFT(String fft)
    {
        this.fft = fft;
    }

    public boolean hasFFT()
    {
        return ((fft != null) && (fft.trim().length() > 0));
    }

    public void appendFFT(String fft)
    {
        if (this.fft == null)
        {
            this.fft = fft;
        }
        else
        {
            this.fft += fft;
        }
    }

    /**
     * Appends the specified FFT to the already existing property FFT after
     * performing required checks to ensure that the existing FFT remains valid.
     * The following checks are applied:
     *  -   The supplied FFT will not be added if the existing FFT already
     *      contains the string to be added.
     *  -   The specified separator will be used only if needed i.e. there is
     *      existing FFT to which the newly supplied FFT will be added.
     *  -   The supplied FFT will be trimmed before appended.
     *  -   If the supplied FFT is null or an empty string (whitespace only) it
     *      will not be appended.
     * @param fft The FFT to append to this property.
     * @param separator The separator insert between existing property FFT and
     * the new FFT supplied.
     */
    public void appendFFTChecked(String fft, String separator)
    {
        if (!Strings.isNullOrEmpty(fft))
        {
            String newFFT;

            newFFT = fft.trim();
            if (Strings.isNullOrEmpty(this.fft))
            {
                this.fft = newFFT;
            }
            else if (!this.fft.toUpperCase().contains(newFFT.toUpperCase()))
            {
                this.fft += (separator + newFFT);
            }
        }
    }

    public boolean isCharacteristic()
    {
        return propertyRequirement.isCharacteristic();
    }

    private void fireRecordPropertyValueChangedEvent(RecordValue oldValue, RecordValue newValue, Object actor)
    {
        // Only fire the event if the two values are different.
        if (!Objects.equals(oldValue, newValue))
        {
            DataRecord parentRecord;

            // Only fire the event if this property belongs to a parent data record.
            parentRecord = getParentDataRecord();
            if (parentRecord != null)
            {
                T8RecordPropertyValueChangedEvent event;

                // Fire an event signalling a value change on this property.
                event = new T8RecordPropertyValueChangedEvent(this, oldValue, newValue, actor);
                parentRecord.fireDataRecordChangeEventPropagated(event, actor);
            }
        }
    }

    @Override
    public RecordValue getSubValue(RequirementType typeID, String conceptID, Integer index)
    {
        if (recordValue != null)
        {
            ValueRequirement subValueRequirement;

            // Check if we can add the record value to the list.
            subValueRequirement = recordValue.getValueRequirement();
            if ((typeID == null) || (subValueRequirement.getRequirementType().equals(typeID)))
            {
                if ((index == null) || (index == 0))
                {
                    if ((conceptID == null) || (conceptID.equals(subValueRequirement.getValue())))
                    {
                        return recordValue;
                    }
                }
            }

            // Record Value does not comply to the criteria.
            return null;
        }
        else return null;
    }

    @Override
    public List<Value> getSubValues(RequirementType typeID, String conceptID, Integer index)
    {
        ArrayList<Value> recordValues;

        recordValues = new ArrayList<Value>();
        if (recordValue != null)
        {
            ValueRequirement subValueRequirement;

            // Check if we can add the record value to the list.
            subValueRequirement = recordValue.getValueRequirement();
            if ((typeID == null) || (subValueRequirement.getRequirementType().equals(typeID)))
            {
                if ((index == null) || (index == 0))
                {
                    if ((conceptID == null) || (conceptID.equals(subValueRequirement.getValue())))
                    {
                        recordValues.add(recordValue);
                    }
                }
            }
        }

        return recordValues;
    }

    @Override
    public RecordValue getDescendantValue(RequirementType typeID, String conceptID, Integer index)
    {
        if (recordValue != null)
        {
            ValueRequirement subValueRequirement;

            // Check if we can return the record value.
            subValueRequirement = recordValue.getValueRequirement();
            if ((typeID == null) || (subValueRequirement.getRequirementType().equals(typeID)))
            {
                if ((index == null) || (index == 0))
                {
                    if ((conceptID == null) || (conceptID.equals(subValueRequirement.getValue())))
                    {
                        return recordValue;
                    }
                }
            }

            // Add all descendants of the record value to the list.
            return recordValue.getDescendantValue(typeID, conceptID, index);
        }
        else return null;
    }

    @Override
    public List<Value> getDescendantValues(RequirementType typeID, String conceptID, Integer index)
    {
        ArrayList<Value> recordValues;

        recordValues = new ArrayList<Value>();
        if (recordValue != null)
        {
            ValueRequirement subValueRequirement;

            // Check if we can add the record value to the list.
            subValueRequirement = recordValue.getValueRequirement();
            if ((typeID == null) || (subValueRequirement.getRequirementType().equals(typeID)))
            {
                if ((index == null) || (index == 0))
                {
                    if ((conceptID == null) || (conceptID.equals(subValueRequirement.getValue())))
                    {
                        recordValues.add(recordValue);
                    }
                }
            }

            // Add all descendants of the record value to the list.
            recordValues.addAll(recordValue.getDescendantValues(typeID, conceptID, index));
        }

        return recordValues;
    }

    @Override
    public void setSubValue(Value subValue)
    {
        if (subValue instanceof RecordValue)
        {
            // Add the new property.
            setRecordValue((RecordValue)subValue);
        }
        else throw new IllegalArgumentException("Incompatible value: " + subValue);
    }

    @Override
    public boolean removeSubValue(Value subValue)
    {
        if (subValue instanceof RecordValue)
        {
            if (recordValue == subValue)
            {
                setRecordValue(null);
                return true;
            }
            else return false;
        }
        else throw new IllegalArgumentException("Incompatible value type: " + subValue);
    }

    /**
     * Fills this property, ensuring that any composite values specified by this property's requirement
     * exist in the object model.  This method does not fill the fields, it just ensures that
     * all fields exist as objects.
     */
    public void fill()
    {
        ValueRequirement valueRequirement;

        valueRequirement = propertyRequirement.getValueRequirement();
        if (valueRequirement instanceof CompositeRequirement)
        {
            CompositeRequirement compositeRequirement;
            Composite composite;

            compositeRequirement = (CompositeRequirement)valueRequirement;
            composite = (Composite)getRecordValue();
            if (composite == null)
            {
                composite = new Composite(compositeRequirement);
                composite.fill();
                setRecordValue(composite);
            }
            else
            {
                composite.fill();
            }
        }
    }

    @Override
    public void normalize()
    {
        // Set the FFT to null if it contains no useful text.
        if (Strings.isNullOrEmpty(fft)) fft = null;

        // Check the value for content.
        if (recordValue != null)
        {
            // Normalize the value.
            recordValue.normalize();

            // If the value has not content, remove it.
            if (!recordValue.hasContent(true))
            {
                setRecordValue((RecordValue)null);
            }
        }
    }

    @Override
    public boolean hasContent(boolean includeFFT)
    {
        // If the property contains an FFT value it has content.
        if ((!Strings.isNullOrEmpty(fft)) && (includeFFT)) return true;
        else
        {
            // If even one of the descendent values has content, the property has content.
            if (recordValue != null)
            {
                return recordValue.hasContent(includeFFT);
            }

            // No content.
            return false;
        }
    }

    @Override
    public boolean isEquivalentValue(Value comparedValue)
    {
        if (!(comparedValue instanceof RecordProperty))
        {
            return false;
        }
        else
        {
            RecordProperty comparedProperty;

            comparedProperty = (RecordProperty)comparedValue;
            if (!Objects.equals(fft, comparedProperty.getFFT()))
            {
                return false;
            }
            else if (recordValue == null)
            {
                return comparedProperty.getRecordValue() == null;
            }
            else
            {
                return recordValue.isEquivalentValue(comparedProperty.getRecordValue());
            }
        }
    }

    public RecordProperty copy()
    {
        RecordProperty copy;

        copy = new RecordProperty(propertyRequirement);
        copy.setFFT(fft);
        if (recordValue != null) copy.setRecordValue(recordValue.copy());
        return copy;
    }

    public RecordProperty copy(PropertyRequirement propertyRequirement)
    {
        RecordProperty newProperty;

        newProperty = new RecordProperty(propertyRequirement);
        if (recordValue != null)
        {
            ValueRequirement valueRequirement;
            ValueRequirement newValueRequirement;

            // Find the correct value requirement to copy the value to.
            valueRequirement = recordValue.getValueRequirement();
            newValueRequirement = propertyRequirement.getValueRequirement();

            // Copy the value and set it on the new property.
            if ((newValueRequirement != null) && (valueRequirement.isEquivalentRequirement(newValueRequirement, false)))
            {
                newProperty.setRecordValue(recordValue != null ? recordValue.copy(newValueRequirement) : null);
            }
            else throw new IllegalArgumentException("Cannot copy value in property " + this + " from value requirement " + valueRequirement + " to incompatible requirement " + newValueRequirement);
        }

        // Copy the FFT and return the new property.
        newProperty.setFFT(fft);
        return newProperty;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("RecordProperty: [");
        builder.append("ID: ");
        builder.append(getPropertyID());
        builder.append(",Fft: ");
        builder.append(getFFT());
        builder.append(",RecordValue: ");
        builder.append(getRecordValue());
        builder.append(",PropertyRequirement: ");
        builder.append(getPropertyRequirement());
        builder.append("]");
        return builder.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getContentHashCode(boolean includeFFT)
    {
        int hash;

        hash = 5;
        hash = 53 * hash + java.util.Objects.hashCode(this.fft);
        if (recordValue != null)
        {
            hash = 53 * hash + recordValue.getContentHashCode(includeFFT);
        }

        return hash;
    }

    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        if (recordValue != null)
        {
            recordValue.setContentTerminology(terminologyProvider);
        }
    }

    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        if (recordValue != null)
        {
            recordValue.addContentTerminology(terminologyCollector);
        }
    }
}
