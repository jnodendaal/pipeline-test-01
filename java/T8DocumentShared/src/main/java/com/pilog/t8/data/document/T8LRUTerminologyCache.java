package com.pilog.t8.data.document;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.utilities.cache.LRUCache;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8LRUTerminologyCache implements TerminologyProvider, TerminologyCollector, Serializable
{
    private final Map<String, T8ConceptTerminologySet> conceptTerminologySets;
    private String defaultLanguageId;

    public T8LRUTerminologyCache(int conceptCacheSize)
    {
        conceptTerminologySets = Collections.synchronizedMap(new LRUCache<String, T8ConceptTerminologySet>(conceptCacheSize));
    }

    public void clearCache()
    {
        conceptTerminologySets.clear();
    }

    public T8ConceptTerminology getConceptTerminology(String conceptId, String languageId)
    {
        T8ConceptTerminologySet terminologySet;

        terminologySet = conceptTerminologySets.get(conceptId);
        return terminologySet != null ? terminologySet.getTerminology(languageId) : null;
    }

    public void setConceptTerminology(T8ConceptTerminology terminology)
    {
        synchronized (conceptTerminologySets)
        {
            T8ConceptTerminologySet terminologySet;
            String conceptId;

            conceptId = terminology.getConceptId();
            terminologySet = conceptTerminologySets.get(conceptId);
            if (terminologySet == null)
            {
                terminologySet = new T8ConceptTerminologySet(terminology.getConceptType(), conceptId);
                conceptTerminologySets.put(conceptId, terminologySet);
            }

            terminologySet.setTerminology(terminology);
        }
    }

    public void addConceptTerminology(T8ConceptTerminology terminology)
    {
        synchronized (conceptTerminologySets)
        {
            T8ConceptTerminologySet terminologySet;
            String conceptId;

            conceptId = terminology.getConceptId();
            terminologySet = conceptTerminologySets.get(conceptId);
            if (terminologySet == null)
            {
                terminologySet = new T8ConceptTerminologySet(terminology.getConceptType(), conceptId);
                conceptTerminologySets.put(conceptId, terminologySet);
            }

            terminologySet.addTerminology(terminology);
        }
    }

    public boolean isConceptCached(String conceptID)
    {
        return conceptTerminologySets.containsKey(conceptID);
    }

    public boolean isTerminologyCached(String languageID, String conceptID)
    {
        T8ConceptTerminologySet terminologySet;

        terminologySet = conceptTerminologySets.get(conceptID);
        if (terminologySet != null)
        {
            return terminologySet.containsLanguage(languageID);
        }
        else return false;
    }

    public Set<String> getCachedConceptIDs(String languageID)
    {
        HashSet<String> cachedConceptIDs;

        cachedConceptIDs = new HashSet<String>();
        synchronized (conceptTerminologySets)
        {
            for (T8ConceptTerminologySet concept : conceptTerminologySets.values())
            {
                if (concept.containsLanguage(languageID)) cachedConceptIDs.add(concept.getConceptID());
            }
        }

        return cachedConceptIDs;
    }

    public boolean containsTerm(String languageID, String conceptID)
    {
        T8ConceptTerminologySet conceptTerminology;

        conceptTerminology = conceptTerminologySets.get(conceptID);
        return conceptTerminology != null ? conceptTerminology.containsLanguage(languageID) : false;
    }

    @Override
    public String getTerm(String languageID, String conceptID)
    {
        T8ConceptTerminologySet conceptTerminology;

        conceptTerminology = conceptTerminologySets.get(conceptID);
        return conceptTerminology != null ? conceptTerminology.getTerm(languageID) : null;
    }

    @Override
    public String getAbbreviation(String languageID, String conceptID)
    {
        T8ConceptTerminologySet conceptTerminology;

        conceptTerminology = conceptTerminologySets.get(conceptID);
        return conceptTerminology != null ? conceptTerminology.getAbbreviation(languageID) : null;
    }

    @Override
    public String getDefinition(String languageID, String conceptID)
    {
        T8ConceptTerminologySet conceptTerminology;

        conceptTerminology = conceptTerminologySets.get(conceptID);
        return conceptTerminology != null ? conceptTerminology.getDefinition(languageID) : null;
    }

    @Override
    public String getCode(String conceptID)
    {
        T8ConceptTerminologySet conceptTerminology;

        conceptTerminology = conceptTerminologySets.get(conceptID);
        return conceptTerminology != null ? conceptTerminology.getCode() : null;
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIDSet, Collection<String> conceptIDSet)
    {
        T8ConceptTerminologyList terminologies;

        terminologies = new T8ConceptTerminologyList();
        for (String conceptID : conceptIDSet)
        {
            for (String languageID : languageIDSet)
            {
                terminologies.add(getTerminology(languageID, conceptID));
            }
        }

        return terminologies;
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageID, String conceptID)
    {
        return getConceptTerminology(conceptID, languageID);
    }

    @Override
    public void setLanguage(String languageId)
    {
        this.defaultLanguageId = languageId;
    }

    @Override
    public void addTerminology(List<T8ConceptTerminology> terminologyList)
    {
        for (T8ConceptTerminology terminology : terminologyList)
        {
            setConceptTerminology(terminology);
        }
    }

    @Override
    public void addTerminology(String languageId, T8OntologyConceptType conceptType, String conceptId, String codeId, String code, String termId, String term, String abbreviationId, String abbreviation, String definitionId, String definition)
    {
        T8ConceptTerminology terminology;

        terminology = new T8ConceptTerminology(conceptType, conceptId);
        terminology.setLanguageId(languageId);
        terminology.setCodeID(codeId);
        terminology.setCode(code);
        terminology.setTermId(termId);
        terminology.setTerm(term);
        terminology.setAbbreviationId(abbreviationId);
        terminology.setAbbreviation(abbreviation);
        terminology.setDefinitionId(definitionId);
        terminology.setDefinition(definition);
        addConceptTerminology(terminology);
    }
}
