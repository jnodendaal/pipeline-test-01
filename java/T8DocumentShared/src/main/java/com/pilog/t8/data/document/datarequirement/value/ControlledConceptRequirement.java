package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class ControlledConceptRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public ControlledConceptRequirement()
    {
        super(RequirementType.CONTROLLED_CONCEPT);
    }

    @Override
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public boolean isAllowNewValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.ALLOW_NEW_VALUE.toString()));
    }

    public String getOntologyClassID()
    {
        OntologyClassRequirement classRequirement;

        classRequirement = (OntologyClassRequirement)getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        return classRequirement != null ? classRequirement.getOntologyClassID() : null;
    }

    public void setOntologyClassID(String ontologyClassID)
    {
        OntologyClassRequirement classRequirement;

        classRequirement = (OntologyClassRequirement)getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        if (classRequirement != null)
        {
            classRequirement.setValue(ontologyClassID);
        }
        else
        {
            classRequirement = (OntologyClassRequirement)ValueRequirement.createValueRequirement(RequirementType.ONTOLOGY_CLASS);
            classRequirement.setValue(ontologyClassID);
            addSubRequirement(classRequirement);
        }
    }
}
