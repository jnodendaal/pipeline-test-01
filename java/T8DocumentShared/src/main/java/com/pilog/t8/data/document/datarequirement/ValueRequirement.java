package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.DataDependency.DataDependencyType;
import com.pilog.t8.data.document.datarequirement.value.AttachmentListRequirement;
import com.pilog.t8.data.document.datarequirement.value.BooleanRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.data.document.datarequirement.value.ConceptDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.ConceptRelationshipRequirement;
import com.pilog.t8.data.document.datarequirement.value.ConceptTypeRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.OntologyClassRequirement;
import com.pilog.t8.data.document.datarequirement.value.DRDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.DRInstanceDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateTimeRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.datarequirement.value.IntegerRequirement;
import com.pilog.t8.data.document.datarequirement.value.LocalizedStringListRequirement;
import com.pilog.t8.data.document.datarequirement.value.LocalizedStringRequirement;
import com.pilog.t8.data.document.datarequirement.value.LowerBoundNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.datarequirement.value.NumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.PropertyDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.QualifierOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.RealRequirement;
import com.pilog.t8.data.document.datarequirement.value.StringRequirement;
import com.pilog.t8.data.document.datarequirement.value.TextRequirement;
import com.pilog.t8.data.document.datarequirement.value.TimeRequirement;
import com.pilog.t8.data.document.datarequirement.value.UnitOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.UpperBoundNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.ValueSetRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public abstract class ValueRequirement implements Requirement, Serializable
{
    private RequirementType requirementType;
    private String value;
    private PropertyRequirement parentPropertyRequirement;
    private ValueRequirement parentValueRequirement;
    private final LinkedHashMap<String, Object> attributes;
    private final ArrayList<ValueRequirement> subRequirements;
    protected boolean prescribed;

    public ValueRequirement()
    {
        this.parentPropertyRequirement = null;
        this.parentValueRequirement = null;
        this.attributes = new LinkedHashMap<String, Object>();
        this.subRequirements = new ArrayList<ValueRequirement>();
        this.prescribed = false;
    }

    public ValueRequirement(RequirementType requirementType)
    {
        this();
        if (requirementType == null) throw new IllegalArgumentException("Cannot set a null requirement type.");
        else this.requirementType = requirementType;
    }

    public ValueRequirement(RequirementType requirementType, String value)
    {
        this(requirementType);
        this.value = value;
    }

    void setParentPropertyRequirement(PropertyRequirement propertyRequirement)
    {
        this.parentPropertyRequirement = propertyRequirement;
        for (ValueRequirement subRequirement : subRequirements)
        {
            subRequirement.setParentPropertyRequirement(propertyRequirement);
        }
    }

    void setParentValueRequirement(ValueRequirement valueRequirement)
    {
        this.parentValueRequirement = valueRequirement;
    }

    public DataRequirementInstance getParentDataRequirementInstance()
    {
        return parentPropertyRequirement != null ? parentPropertyRequirement.getParentDataRequirementInstance() : null;
    }

    public DataRequirement getParentDataRequirement()
    {
        return parentPropertyRequirement != null ? parentPropertyRequirement.getParentDataRequirement() : null;
    }

    public SectionRequirement getParentSectionRequirement()
    {
        return parentPropertyRequirement != null ? parentPropertyRequirement.getParentSectionRequirement() : null;
    }

    /**
     * Returns a copy of this object and its logical descendants.  No references to logical parents are copied.
     * @return A copy of this object and its logical descendants.
     */
    public ValueRequirement copy()
    {
        ValueRequirement newRequirement;

        newRequirement = ValueRequirement.createValueRequirement(requirementType);
        newRequirement.setValue(value);
        newRequirement.setAttributes(attributes);

        for (ValueRequirement subRequirement : subRequirements)
        {
            newRequirement.addSubRequirement(subRequirement.copy());
        }

        return newRequirement;
    }

    public boolean isComposite()
    {
        return requirementType == RequirementType.COMPOSITE_TYPE;
    }

    /**
     * Returns a boolean value indicating whether or not this value is a standard prescribed value.
     * @return A boolean value indicating whether or not this value is a standard prescribed value.
     */
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(attributes.get(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    /**
     * Sets the flag indicating whether or not this value is a standard prescribed value.
     * @param prescribed
     */
    public void setPrescribed(boolean prescribed)
    {
        attributes.put(RequirementAttribute.PRESCRIBED_VALUE.toString(), prescribed + "");
    }

    public ValueRequirement getEquivalentDescendantRequirement(Requirement requirement)
    {
        if (requirement instanceof ValueRequirement)
        {
            for (ValueRequirement descendantRequirement : getDescendentRequirements())
            {
                if (descendantRequirement.isEquivalentRequirement(requirement))
                {
                    return descendantRequirement;
                }
            }

            return null;
        }
        else return null;
    }

    public ValueRequirement getEquivalentSubRequirement(Requirement requirement)
    {
        if (requirement instanceof ValueRequirement)
        {
            for (ValueRequirement subRequirement : subRequirements)
            {
                if (subRequirement.isEquivalentRequirement(requirement))
                {
                    return subRequirement;
                }
            }

            return null;
        }
        else return null;
    }

    @Override
    public boolean isEquivalentRequirement(Requirement requirement)
    {
        return isEquivalentRequirement(requirement, true);
    }

    public boolean isEquivalentRequirement(Requirement requirement, boolean compareAncestry)
    {
        if (!(requirement instanceof ValueRequirement))
        {
            return false;
        }
        else
        {
            ValueRequirement valueRequirement;

            valueRequirement = (ValueRequirement)requirement;
            if ((Objects.equals(requirementType, requirement.getRequirementType())) && (Objects.equals(value, valueRequirement.getValue())))
            {
                List<ValueRequirement> subRequirementList;

                subRequirementList = valueRequirement.getSubRequirements();
                if (subRequirements.size() == subRequirementList.size())
                {
                    // Check each of the sub requirements in this value requirement agains those in the supplied value requirement.
                    for (int subRequirementIndex = 0; subRequirementIndex < subRequirements.size(); subRequirementIndex++)
                    {
                        if (!subRequirements.get(subRequirementIndex).isEquivalentRequirement(subRequirementList.get(subRequirementIndex), false))
                        {
                            return false;
                        }
                    }

                    // No problems found, so return check the ancestry if required.
                    if (compareAncestry)
                    {
                        ValueRequirement parentRequirement;

                        parentRequirement = this.getParentValueRequirement();
                        if (parentRequirement != null)
                        {

                        }
                    }

                    // No differences found, so return true.
                    return true;
                }
                else return false;
            }
            else return false;
        }
    }

    public ValueRequirement findFirstEquivalentDescendant(ValueRequirement requirement)
    {
        for (ValueRequirement descendant : getDescendentRequirements())
        {
            if (descendant.isEquivalentRequirement(requirement))
            {
                return descendant;
            }
        }

        return null;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * Adds the terminology of this value Requirement and of all its
     * descendants to the supplied list.
     * @param terminologyCollector The list to which terminology will be added.
     */
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        // Add the descendants' terminology to the list.
        for (ValueRequirement subRequirement : subRequirements)
        {
            subRequirement.addContentTerminology(terminologyCollector);
        }
    }

    /**
     * Sets the terminology of this value Requirement and all of its descendants
     * by retrieval from the supplied terminology provider.
     * @param terminologyProvider The terminology provider form which to fetch terminology.
     */
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        // Set the terminology of the sub-requirements.
        for (ValueRequirement subRequirement : subRequirements)
        {
            subRequirement.setContentTerminology(terminologyProvider);
        }
    }

    @Override
    public RequirementType getRequirementType()
    {
        return requirementType;
    }

    public void setRequirementType(RequirementType requirementType)
    {
        if (requirementType == null) throw new IllegalArgumentException("Cannot set null requirement type.");
        this.requirementType = requirementType;
    }

    public int getIndex()
    {
        if (parentValueRequirement != null)
        {
            return parentValueRequirement.getSubRequirementIndex(this);
        }
        else if (parentPropertyRequirement != null)
        {
            return 0;
        }
        else return -1;
    }

    public int getSubRequirementIndex(ValueRequirement requirement)
    {
        return subRequirements.indexOf(requirement);
    }

    public int getSubRequirementCount()
    {
        return subRequirements.size();
    }

    public int getSequence()
    {
        if (parentPropertyRequirement != null)
        {
            return parentPropertyRequirement.getAllValueRequirements().indexOf(this);
        }
        else return -1;
    }

    public int getDepth()
    {
        if (parentPropertyRequirement != null)
        {
            return 0;
        }
        else if (parentValueRequirement != null)
        {
            return parentValueRequirement.getDepth() + 1;
        }
        else return 0;
    }

    public boolean containsDescendentRequirement(RequirementType requirementType, String conceptID)
    {
        return getDescendentRequirements(requirementType, conceptID, null).size() > 0;
    }

    public ArrayList<ValueRequirement> getDescendentRequirements()
    {
        ArrayList<ValueRequirement> requirements;

        requirements = new ArrayList<>();
        for (ValueRequirement subRequirement : subRequirements)
        {
            requirements.add(subRequirement);
            requirements.addAll(subRequirement.getDescendentRequirements());
        }

        return requirements;
    }

    public ValueRequirement getAncestorRequirement(RequirementType requirementType, String conceptID, Integer index)
    {
        if (parentValueRequirement != null)
        {
            if ((requirementType == null) || (parentValueRequirement.getRequirementType().equals(requirementType)))
            {
                if ((index == null) || (index == parentValueRequirement.getIndex()))
                {
                    if ((conceptID == null) || (conceptID.equals(parentValueRequirement.getValue())))
                    {
                        return parentValueRequirement;
                    }
                }
            }

            // The parent is not the required ancestor, so check its ancestors.
            return parentValueRequirement.getAncestorRequirement(requirementType, conceptID, index);
        }
        else return null;
    }

    public boolean containsAttribute(String attributeIdentifier)
    {
        return attributes.containsKey(attributeIdentifier);
    }

    public Object findRequirementAttribute(String attributeIdentifier)
    {
        if (containsAttribute(attributeIdentifier))
        {
            return attributes.get(attributeIdentifier);
        }
        else
        {
            for (ValueRequirement descendant : getDescendentRequirements())
            {
                if (descendant.containsAttribute(attributeIdentifier))
                {
                    return descendant.getAttribute(attributeIdentifier);
                }
            }

            return null;
        }
    }

    public ValueRequirement getDescendentRequirement(RequirementType requirementType, String conceptID, Integer index)
    {
        ArrayList<ValueRequirement> requirements;

        requirements = getDescendentRequirements(requirementType, conceptID, index);
        return requirements.size() > 0 ? requirements.get(0) : null;
    }

    public ArrayList<ValueRequirement> getDescendentRequirements(RequirementType requirementType, String conceptID, Integer index)
    {
        ArrayList<ValueRequirement> valueRequirements;

        valueRequirements = new ArrayList<>();
        for (ValueRequirement subRequirement : getDescendentRequirements())
        {
            if ((requirementType == null) || (subRequirement.getRequirementType().equals(requirementType)))
            {
                if ((index == null) || (index == subRequirement.getIndex()))
                {
                    if ((conceptID == null) || (conceptID.equals(subRequirement.getValue())))
                    {
                        valueRequirements.add(subRequirement);
                    }
                }
            }
        }

        return valueRequirements;
    }

    public ValueRequirement getFirstSubRequirement()
    {
        return subRequirements.size() > 0 ? subRequirements.get(0) : null;
    }

    public boolean containsSubRequirement(RequirementType requirementType, String conceptID)
    {
        return getSubRequirements(requirementType, conceptID, null).size() > 0;
    }

    public ValueRequirement getPathValueRequirement(RequirementType requirementType)
    {
        ValueRequirement currentRequirement;

        currentRequirement = this;
        while (currentRequirement != null)
        {
            if (currentRequirement.getRequirementType() == requirementType)
            {
                return currentRequirement;
            }
            else currentRequirement = currentRequirement.getParentValueRequirement();
        }

        return null;
    }

    public ArrayList<ValueRequirement> getPathValueRequirements()
    {
        ArrayList<ValueRequirement> pathRequirements;
        ValueRequirement currentRequirement;

        pathRequirements = new ArrayList<ValueRequirement>();
        currentRequirement = this;
        while (currentRequirement != null)
        {
            pathRequirements.add(currentRequirement);
            currentRequirement = currentRequirement.getParentValueRequirement();
        }

        Collections.reverse(pathRequirements);
        return pathRequirements;
    }

    public ArrayList<ValueRequirement> getSubRequirements()
    {
        return new ArrayList<ValueRequirement>(subRequirements);
    }

    public ValueRequirement getSubRequirement(RequirementType requirementType, String value, Integer index)
    {
        for (ValueRequirement subRequirement : subRequirements)
        {
            if ((requirementType == null) || (subRequirement.getRequirementType().equals(requirementType)))
            {
                if ((index == null) || (index == subRequirement.getIndex()))
                {
                    if ((value == null) || (value.equals(subRequirement.getValue())))
                    {
                        return subRequirement;
                    }
                }
            }
        }

        return null;
    }

    public ArrayList<ValueRequirement> getSubRequirements(RequirementType requirementType, String value, Integer index)
    {
        ArrayList<ValueRequirement> valueRequirements;

        valueRequirements = new ArrayList<ValueRequirement>();
        for (ValueRequirement subRequirement : subRequirements)
        {
            if ((requirementType == null) || (subRequirement.getRequirementType().equals(requirementType)))
            {
                if ((index == null) || (index == subRequirement.getIndex()))
                {
                    if ((value == null) || (value.equals(subRequirement.getValue())))
                    {
                        valueRequirements.add(subRequirement);
                    }
                }
            }
        }

        return valueRequirements;
    }

    public void addSubRequirement(ValueRequirement valueRequirement)
    {
        addSubRequirement(valueRequirement, subRequirements.size());
    }

    public void addSubRequirement(ValueRequirement valueRequirement, int index)
    {
        valueRequirement.setParentValueRequirement(this);
        valueRequirement.setParentPropertyRequirement(parentPropertyRequirement);
        subRequirements.add(index, valueRequirement);
    }

    public void removeSubRequirement(ValueRequirement requirement)
    {
        if ((requirement != null) && (subRequirements.contains(requirement)))
        {
            requirement.setParentValueRequirement(null);
            requirement.setParentPropertyRequirement(null);
            subRequirements.remove(requirement);
        }
    }

    public void removeAllSubRequirements()
    {
        ArrayList<ValueRequirement> requirementList;

        requirementList = getSubRequirements();
        for (ValueRequirement subRequirement : requirementList)
        {
            removeSubRequirement(subRequirement);
        }
    }

    public void addSubRequirements(List<ValueRequirement> subRequirementList)
    {
        for (ValueRequirement subRequirement : subRequirementList)
        {
            addSubRequirement(subRequirement);
        }
    }

    public PropertyRequirement getParentPropertyRequirement()
    {
        return parentPropertyRequirement;
    }

    /**
     * Returns the FieldRequirement to which this ValueRequirement belongs.  If
     * this ValueRequirement does not belong to a field i.e. is not part of a
     * composite value, null will be returned.  If this ValueRequirement is
     * itself, this object will be returned.
     * @return The FieldRequirement to which this ValueRequirement belongs.
     */
    public FieldRequirement getFieldRequirement()
    {
        if (this instanceof FieldRequirement) return (FieldRequirement)this;
        else if (parentValueRequirement != null) return parentValueRequirement.getFieldRequirement();
        else return null;
    }

    public ValueRequirement getParentValueRequirement()
    {
        return parentValueRequirement;
    }

    public Object getAttribute(String attributeIdentifier)
    {
        return attributes.get(attributeIdentifier);
    }

    public void setAttribute(String attributeIdentifier, Object attributeValue)
    {
        attributes.put(attributeIdentifier, attributeValue);
    }

    public Map<String, Object> getAttributes()
    {
        return new LinkedHashMap<String, Object>(attributes);
    }

    public void setAttributes(Map<String, Object> newAttributes)
    {
        attributes.clear();
        if (newAttributes != null)
        {
            attributes.putAll(newAttributes);
        }
    }

    public String getDataRequirementInstanceID()
    {
        DataRequirementInstance parentDataRequirementInstance;

        parentDataRequirementInstance = getParentDataRequirementInstance();
        return parentDataRequirementInstance != null ? parentDataRequirementInstance.getConceptID() : null;
    }

    public String getDataRequirementID()
    {
        DataRequirement parentDataRequirement;

        parentDataRequirement = getParentDataRequirement();
        return parentDataRequirement != null ? parentDataRequirement.getConceptID() : null;
    }

    public String getPropertyID()
    {
        return parentPropertyRequirement != null ? parentPropertyRequirement.getConceptID() : null;
    }

    public String getFieldID()
    {
        if (requirementType == RequirementType.FIELD_TYPE)
        {
            return value;
        }
        else
        {
            ValueRequirement fieldRequirement;

            fieldRequirement = getAncestorRequirement(RequirementType.FIELD_TYPE, null, null);
            return fieldRequirement != null ? fieldRequirement.getValue() : null;
        }
    }

    /**
     * Returns the data dependencies specified on this requirement.
     * @return The data dependencies specified on this requirement.
     */
    public List<DataDependency> getDataDependencies()
    {
        if ((requirementType == RequirementType.CONTROLLED_CONCEPT) || (requirementType == RequirementType.DOCUMENT_REFERENCE))
        {
            List<ValueRequirement> requirementList;
            List<DataDependency> dataDependencies;

            // Create a list of all dependency requirements.
            requirementList = getSubRequirements(RequirementType.PROPERTY_DEPENDENCY_TYPE, null, null);
            requirementList.addAll(getSubRequirements(RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE, null, null));
            requirementList.addAll(getSubRequirements(RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE, null, null));

            // Compile dependencies.
            dataDependencies = new ArrayList<DataDependency>();
            for (ValueRequirement dependencyRequirement : requirementList)
            {
                ValueRequirement fieldRequirement;
                ValueRequirement propertyRequirement;
                ValueRequirement graphRequirement;
                ValueRequirement conceptTypeRequirement;
                T8OntologyConceptType conceptType;
                DataDependencyType type;
                DataDependency dataDependency;
                String drInstanceId;
                String drId;
                String propertyId;
                String fieldId;
                String relationId;

                // Set the default dependency type.
                type = DataDependencyType.DR_INSTANCE;

                // Get the DR Instance ID.
                if (dependencyRequirement.getRequirementType() == RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE)
                {
                    drInstanceId = dependencyRequirement.getValue();
                    type = DataDependencyType.DR_INSTANCE;
                }
                else
                {
                    drInstanceId = null;
                }

                // Get the DR ID.
                if (dependencyRequirement.getRequirementType() == RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE)
                {
                    drId = dependencyRequirement.getValue();
                }
                else
                {
                    drId = null;
                }

                // Get the property ID.
                if (dependencyRequirement.getRequirementType() == RequirementType.PROPERTY_DEPENDENCY_TYPE)
                {
                    propertyId = dependencyRequirement.getValue();
                    type = DataDependencyType.PROPERTY;
                }
                else
                {
                    propertyRequirement = dependencyRequirement.getDescendentRequirement(RequirementType.PROPERTY_DEPENDENCY_TYPE, null, null);
                    if (propertyRequirement != null)
                    {
                        propertyId = propertyRequirement.getValue();
                        type = DataDependencyType.PROPERTY;
                    }
                    else propertyId = null;
                }

                // Get the field ID.
                fieldRequirement = dependencyRequirement.getDescendentRequirement(RequirementType.FIELD_DEPENDENCY_TYPE, null, null);
                if (fieldRequirement != null)
                {
                    fieldId = fieldRequirement.getValue();
                    type = DataDependencyType.FIELD;
                }
                else fieldId = null;

                // Try to find a Concept Relationship in this dependency requirement.
                graphRequirement = dependencyRequirement.getDescendentRequirement(RequirementType.CONCEPT_RELATIONSHIP_TYPE, null, null);
                relationId = graphRequirement != null ? graphRequirement.getValue() : null;

                // Ok so we found a concept relationship, now also check to see if a specific concept type is specified.
                conceptTypeRequirement = dependencyRequirement.getDescendentRequirement(RequirementType.CONCEPT_TYPE, null, null);
                conceptType = conceptTypeRequirement != null ? T8OntologyConceptType.getConceptType(conceptTypeRequirement.getValue()) : null;

                // Add a new dependency requirement using the data extracted from the value requirement.
                dataDependency = new DataDependency(type, drInstanceId, drId, propertyId, fieldId, relationId, conceptType);

                // Find possible parent dependencies.
                if (type != DataDependencyType.DR_INSTANCE)
                {
                    ValueRequirement dependencyParentRequirement;
                    DataRequirement parentDataRequirement;

                    parentDataRequirement = getParentDataRequirement();
                    dependencyParentRequirement = parentDataRequirement.getDependencyRequirement(dataDependency);
                    if (dependencyParentRequirement != null)
                    {
                        dataDependency.addParentDependencies(dependencyParentRequirement.getDataDependencies());
                    }
                }

                // Add the completed data dependency to the result list.
                dataDependencies.add(dataDependency);
            }

            // Return the list of dependencies.
            return dataDependencies;
        }
        else return new ArrayList<DataDependency>();
    }

    public static ValueRequirement createValueRequirement(RequirementType requirementType)
    {
        switch (requirementType)
        {
            case ATTACHMENT:
            {
                return new AttachmentListRequirement();
            }
            case BOOLEAN_TYPE:
            {
                return new BooleanRequirement();
            }
            case COMPOSITE_TYPE:
            {
                return new CompositeRequirement();
            }
            case CONCEPT_DEPENDENCY_TYPE:
            {
                return new ConceptDependencyRequirement();
            }
            case CONCEPT_TYPE:
            {
                return new ConceptTypeRequirement();
            }
            case CONCEPT_RELATIONSHIP_TYPE:
            {
                return new ConceptRelationshipRequirement();
            }
            case CONTROLLED_CONCEPT:
            {
                return new ControlledConceptRequirement();
            }
            case DATA_REQUIREMENT_DEPENDENCY_TYPE:
            {
                return new DRDependencyRequirement();
            }
            case DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE:
            {
                return new DRInstanceDependencyRequirement();
            }
            case DATE_TIME_TYPE:
            {
                return new DateTimeRequirement();
            }
            case DATE_TYPE:
            {
                return new DateRequirement();
            }
            case DOCUMENT_REFERENCE:
            {
                return new DocumentReferenceRequirement();
            }
            case ONTOLOGY_CLASS:
            {
                return new OntologyClassRequirement();
            }
            case FIELD_TYPE:
            {
                return new FieldRequirement();
            }
            case FIELD_DEPENDENCY_TYPE:
            {
                return new FieldDependencyRequirement();
            }
            case INTEGER_TYPE:
            {
                return new IntegerRequirement();
            }
            case LOCALIZED_TEXT_TYPE:
            {
                return new LocalizedStringListRequirement();
            }
            case LOCAL_STRING_TYPE:
            {
                return new LocalizedStringRequirement();
            }
            case LOWER_BOUND_NUMBER:
            {
                return new LowerBoundNumberRequirement();
            }
            case MEASURED_NUMBER:
            {
                return new MeasuredNumberRequirement();
            }
            case MEASURED_RANGE:
            {
                return new MeasuredRangeRequirement();
            }
            case NUMBER:
            {
                return new NumberRequirement();
            }
            case PROPERTY_DEPENDENCY_TYPE:
            {
                return new PropertyDependencyRequirement();
            }
            case QUALIFIER_OF_MEASURE:
            {
                return new QualifierOfMeasureRequirement();
            }
            case REAL_TYPE:
            {
                return new RealRequirement();
            }
            case STRING_TYPE:
            {
                return new StringRequirement();
            }
            case TEXT_TYPE:
            {
                return new TextRequirement();
            }
            case TIME_TYPE:
            {
                return new TimeRequirement();
            }
            case UNIT_OF_MEASURE:
            {
                return new UnitOfMeasureRequirement();
            }
            case UPPER_BOUND_NUMBER:
            {
                return new UpperBoundNumberRequirement();
            }
            case SET_TYPE:
            {
                return new ValueSetRequirement();
            }
            default:
            {
                throw new RuntimeException("Invalid record value type: " + requirementType);
            }
        }
    }

    public ArrayList<RecordValue> buildRecordValues(List<Map<String, Object>> valueData, Integer parentValueSequence)
    {
        ArrayList<RecordValue> recordValues;
        ArrayList<Map<String, Object>> dataRows;
        HashMap<String, Object> filterValues;
        PropertyRequirement propertyRequirement;

        propertyRequirement = getParentPropertyRequirement();

        filterValues = new HashMap<>();
        filterValues.put("SECTION_ID", propertyRequirement.getParentSectionRequirement().getConceptID());
        filterValues.put("PROPERTY_ID", propertyRequirement.getConceptID());
        filterValues.put("DATA_TYPE_ID", getRequirementType().getID());
        filterValues.put("DATA_TYPE_SEQUENCE", getSequence());
        filterValues.put("PARENT_VALUE_SEQUENCE", parentValueSequence);
        dataRows = CollectionUtilities.getFilteredDataRows(valueData, filterValues);

        if (dataRows.size() > 0)
        {
            recordValues = new ArrayList<>();
            for (Map<String, Object> dataRow : dataRows)
            {
                RecordValue recordValue;
                int valueSequence;
                String valueID;
                String value;
                String fft;

                valueSequence = (Integer)dataRow.get("VALUE_SEQUENCE");
                value = (String)dataRow.get("VALUE");
                fft = (String)dataRow.get("FFT");
                valueID = (String)dataRow.get("VALUE_ID");

                recordValue = RecordValue.createValue(this);
                recordValue.setValue(value);
                recordValue.setFFT(fft);
                recordValue.setPrescribedValueID(valueID);

                recordValues.add(recordValue);
                for (ValueRequirement subRequirement : getSubRequirements())
                {
                    recordValue.addSubValues(subRequirement.buildRecordValues(valueData, valueSequence));
                }
            }

            return recordValues;
        }
        else
        {
            recordValues = new ArrayList<>();
            for (ValueRequirement subRequirement : getSubRequirements())
            {
                recordValues.addAll(subRequirement.buildRecordValues(valueData, parentValueSequence));
            }

            return recordValues;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("ValueRequirement: [");
        builder.append("Type: ");
        builder.append(getRequirementType());
        builder.append(",Value: ");
        builder.append(getValue());
        builder.append("]");
        return builder.toString();
    }
}
