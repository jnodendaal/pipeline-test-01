package com.pilog.t8.data.ontology;

import com.pilog.t8.definition.T8IdentifierUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyTerm implements Serializable
{
    private String id;
    private String irdi;
    private String conceptId;
    private String languageId;
    private String term;
    private boolean primary;
    private boolean standard;
    private boolean active;
    private final List<T8OntologyAbbreviation> abbreviations;

    /**
     * Creates a new instance of the {@code T8OntologyTerm} referencing the
     * specified concept ID, for the specified language.
     *
     * @param id The {@code String} unique term identifier
     * @param conceptId The {@code String} concept ID to which the current term
     *      is linked
     * @param languageId The {@code String} language identifier for the current
     *      term
     * @param term The term's actual {@code String} value
     * @param primary Indicates whether or not this term is the primary term of its parent concept.
     * @param standard Whether or not the term has been standardized externally
     *      to the current client system
     * @param active {@code true} for the term to be deemed active within the
     *      organization data structure
     */
    public T8OntologyTerm(String id, String conceptId, String languageId, String term, boolean primary, boolean standard, boolean active)
    {
        this.id = id;
        this.conceptId = conceptId;
        this.languageId = languageId;
        this.term = term;
        this.primary = primary;
        this.standard = standard;
        this.active = active;
        this.abbreviations = new ArrayList<>();
    }

    public T8OntologyTerm(String id, String conceptId, String languageId, String term, boolean standard, boolean active)
    {
        this(id, conceptId, languageId, term, false, standard, active);
    }

    public T8OntologyTerm copy(boolean copyAbbreviations)
    {
        T8OntologyTerm copy;

        // Create a new copy of the term.
        copy = new T8OntologyTerm(id, conceptId, languageId, term, primary, standard, active);

        // Copy abbreviations if required.
        if (copyAbbreviations)
        {
            for (T8OntologyAbbreviation abbreviation : getAbbreviations())
            {
                copy.addAbbreviation(abbreviation.copy());
            }
        }

        return copy;
    }

    public void assignNewAbbreviationIDs()
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            abbreviation.setID(T8IdentifierUtilities.createNewGUID());
        }
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;

        // Set the term ID of all abbreviations.
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            abbreviation.setTermID(id);
        }
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getLanguageID()
    {
        return languageId;
    }

    public void setLanguageID(String languageID)
    {
        this.languageId = languageID;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public boolean isPrimary()
    {
        return primary;
    }

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public boolean isStandard()
    {
        return standard;
    }

    public void setStandard(boolean standard)
    {
        this.standard = standard;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isEqualTo(T8OntologyTerm inputTerm)
    {
        if (inputTerm == null) return false;
        else if (!Objects.equals(id, inputTerm.getID())) return false;
        else if (!Objects.equals(irdi, inputTerm.getIrdi())) return false;
        else if (!Objects.equals(languageId, inputTerm.getLanguageID())) return false;
        else if (!Objects.equals(conceptId, inputTerm.getConceptID())) return false;
        else if (!Objects.equals(term, inputTerm.getTerm())) return false;
        else if (!Objects.equals(standard, inputTerm.isStandard())) return false;
        else if (!Objects.equals(active, inputTerm.isActive())) return false;
        else return true;
    }

    public boolean containsAbbreviationWithID(String abbreviationID)
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            if (abbreviation.getID().equals(abbreviationID))
            {
                return true;
            }
        }

        return false;
    }

    public boolean containsAbbreviation(String abbreviationString, boolean caseInsensitive)
    {
        if (abbreviationString == null) return false;
        else
        {
            String matchString;

            matchString = abbreviationString.toUpperCase();
            for (T8OntologyAbbreviation abbreviation : abbreviations)
            {
                if (caseInsensitive)
                {
                    if (abbreviation.getAbbreviation().toUpperCase().equals(matchString)) return true;
                }
                else
                {
                    if (abbreviation.getAbbreviation().equals(abbreviationString)) return true;
                }
            }

            return false;
        }
    }

    public void clearAbbreviations()
    {
        abbreviations.clear();
    }

    public void setAbbreviations(List<T8OntologyAbbreviation> abbreviations)
    {
        clearAbbreviations();
        addAbbreviations(abbreviations);
    }

    public void addAbbreviations(List<T8OntologyAbbreviation> abbreviations)
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            addAbbreviation(abbreviation);
        }
    }

    public void addAbbreviation(T8OntologyAbbreviation abbreviation)
    {
        String abbreviationTermID;

        abbreviationTermID = abbreviation.getTermID();
        if (abbreviationTermID == null) abbreviation.setTermID(id);
        else if (!Objects.equals(id, abbreviationTermID)) throw new IllegalArgumentException("Abbreviation contains Term ID '" + abbreviationTermID + "' that does not correspond to Term where it is being added: " + id);

        abbreviations.add(abbreviation);
    }

    public List<T8OntologyAbbreviation> getAbbreviations()
    {
        return new ArrayList<>(abbreviations);
    }

    public int getAbbreviationCount()
    {
        return abbreviations.size();
    }

    public T8OntologyAbbreviation getFirstAbbreviation()
    {
        return abbreviations.size() > 0 ? abbreviations.get(0) : null;
    }

    public T8OntologyAbbreviation getAbbreviation(String abbreviationId)
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            if (abbreviationId.equals(abbreviation.getID())) return abbreviation;
        }

        return null;
    }

    public T8OntologyAbbreviation getPrimaryAbbreviation()
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            if (abbreviation.isPrimary()) return abbreviation;
        }

        return null;
    }

    public void setPrimaryAbbreviation(String abbreviationId)
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            abbreviation.setPrimary(abbreviation.getID().equals(abbreviationId));
        }
    }

    @Override
    public String toString()
    {
        return "T8OntologyTerm{" + "id=" + id + ", irdi=" + irdi + ", term=" + term + '}';
    }
}
