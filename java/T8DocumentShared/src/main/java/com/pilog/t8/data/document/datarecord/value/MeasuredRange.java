package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.LowerBoundNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.datarequirement.value.UpperBoundNumberRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredRange extends RecordValue implements MeasurableValue
{
    public MeasuredRange(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public MeasuredRangeRequirement getRequirement()
    {
        return (MeasuredRangeRequirement)valueRequirement;
    }

    @Override
    public MeasuredRangeContent getContent()
    {
        MeasuredRangeContent content;
        UnitOfMeasure uom;
        QualifierOfMeasure qom;
        LowerBoundNumber lowerBoundNumber;
        UpperBoundNumber upperBoundNumber;

        lowerBoundNumber = getLowerBoundNumberObject();
        upperBoundNumber = getUpperBoundNumberObject();
        uom = getUom();
        qom = getQom();

        content = new MeasuredRangeContent();
        if (lowerBoundNumber != null) content.getLowerBoundNumber().setContent(lowerBoundNumber.getContent());
        if (upperBoundNumber != null) content.getUpperBoundNumber().setContent(upperBoundNumber.getContent());
        if (uom != null) content.getUnitOfMeasure().setContent(uom.getContent());
        if (qom != null) content.getQualifierOfMeasure().setContent(qom.getContent());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        MeasuredRangeContent mrContent;
        LowerBoundNumberContent lowerBoundNumber;
        UpperBoundNumberContent upperBoundNumber;
        UnitOfMeasureContent uom;
        QualifierOfMeasureContent qom;

        mrContent = (MeasuredRangeContent)valueContent;
        lowerBoundNumber = mrContent.getLowerBoundNumber();
        upperBoundNumber = mrContent.getUpperBoundNumber();
        uom = mrContent.getUnitOfMeasure();
        qom = mrContent.getQualifierOfMeasure();

        if ((lowerBoundNumber != null) && (lowerBoundNumber.hasContent())) getOrCreateLowerBoundNumber().setContent(lowerBoundNumber);
        if ((upperBoundNumber != null) && (upperBoundNumber.hasContent())) getOrCreateUpperBoundNumber().setContent(upperBoundNumber);
        if ((uom != null) && (uom.hasContent())) getOrCreateUom().setContent(mrContent.getUnitOfMeasure());
        if ((qom != null) && (qom.hasContent())) getOrCreateQom().setContent(mrContent.getQualifierOfMeasure());
    }

    @Override
    public UnitOfMeasure getUom()
    {
        for (RecordValue subValue : subValues)
        {
            if (subValue instanceof UnitOfMeasure) return (UnitOfMeasure)subValue;
        }

        return null;
    }

    @Override
    public QualifierOfMeasure getQom()
    {
        for (RecordValue subValue : subValues)
        {
            if (subValue instanceof QualifierOfMeasure) return (QualifierOfMeasure)subValue;
        }

        return null;
    }

    @Override
    public String getUomId()
    {
        UnitOfMeasure uom;

        uom = getUom();
        return uom != null ? uom.getValue() : null;
    }

    @Override
    public String getQomId()
    {
        QualifierOfMeasure qom;

        qom = getQom();
        return qom != null ? qom.getValue() : null;
    }

    @Override
     public RecordValue setUomId(String conceptID)
    {
        if (conceptID != null)
        {
            RecordValue uomValue;

            uomValue = getUom();
            if (uomValue != null)
            {
                uomValue.setValue(conceptID);
                return uomValue;
            }
            else
            {
                ValueRequirement uomRequirement;

                // Get the UOM requirement and construct the value from it.
                uomRequirement = ((MeasuredRangeRequirement)valueRequirement).getUomRequirement();
                if (uomRequirement != null)
                {
                    uomValue = RecordValue.createValue(uomRequirement);
                    uomValue.setValue(conceptID);
                    setSubValue(uomValue);
                    return uomValue;
                }
                else throw new IllegalStateException("No UOM Requirement specified for measured number type: " + this);
            }
        }
        else
        {
            clearUom();
            return null;
        }
    }

    public UnitOfMeasure getOrCreateUom()
    {
        UnitOfMeasure uomValue;

        uomValue = getUom();
        if (uomValue != null)
        {
            return uomValue;
        }
        else
        {
            ValueRequirement uomRequirement;

            // Get the UOM requirement and construct the value from it.
            uomRequirement = ((MeasuredRangeRequirement)valueRequirement).getUomRequirement();
            if (uomRequirement != null)
            {
                uomValue = (UnitOfMeasure)RecordValue.createValue(uomRequirement);
                setSubValue(uomValue);
                return uomValue;
            }
            else throw new IllegalStateException("No UOM Requirement specified for measured number type: " + this);
        }
    }

    @Override
    public RecordValue setQomId(String conceptID)
    {
        if (conceptID != null)
        {
            RecordValue qomValue;

            qomValue = getQom();
            if (qomValue != null)
            {
                qomValue.setValue(conceptID);
                return qomValue;
            }
            else
            {
                ValueRequirement qomRequirement;

                // Get the QOM requirement and construct the value from it.
                qomRequirement = ((MeasuredRangeRequirement)valueRequirement).getQomRequirement();
                if (qomRequirement != null)
                {
                    qomValue = RecordValue.createValue(qomRequirement);
                    setSubValue(qomValue);
                    return qomValue;
                }
                else throw new IllegalStateException("No QOM Requirement specified for measured number type: " + this);
            }
        }
        else
        {
            clearQom();
            return null;
        }
    }

    public QualifierOfMeasure getOrCreateQom()
    {
        QualifierOfMeasure qomValue;

        qomValue = getQom();
        if (qomValue != null)
        {
            return qomValue;
        }
        else
        {
            ValueRequirement qomRequirement;

            // Get the QOM requirement and construct the value from it.
            qomRequirement = ((MeasuredRangeRequirement)valueRequirement).getQomRequirement();
            if (qomRequirement != null)
            {
                qomValue = (QualifierOfMeasure)RecordValue.createValue(qomRequirement);
                setSubValue(qomValue);
                return qomValue;
            }
            else throw new IllegalStateException("No QOM Requirement specified for measured number type: " + this);
        }
    }

    public LowerBoundNumber getLowerBoundNumberObject()
    {
        for (RecordValue subValue : getSubValues())
        {
            if (subValue instanceof LowerBoundNumber)
            {
                return (LowerBoundNumber)subValue;
            }
        }

        return null;
    }

    public String getLowerBoundNumber()
    {
        RecordValue numeric;

        numeric = getLowerBoundNumberObject();
        return numeric != null ? numeric.getValue() : null;
    }

    public UpperBoundNumber getUpperBoundNumberObject()
    {
        for (RecordValue subValue : getSubValues())
        {
            if (subValue instanceof UpperBoundNumber)
            {
                return (UpperBoundNumber)subValue;
            }
        }

        return null;
    }

    public String getUpperBoundNumber()
    {
        RecordValue numeric;

        numeric = getUpperBoundNumberObject();
        return numeric != null ? numeric.getValue() : null;
    }

    public UpperBoundNumber setUpperBoundNumber(String value)
    {
        UpperBoundNumber upperBoundNumber;

        // Set the numeric value or create it, if no RecordValue exists.
        upperBoundNumber = getUpperBoundNumberObject();
        if (upperBoundNumber != null)
        {
            upperBoundNumber.setValue(value);
            return upperBoundNumber;
        }
        else
        {
            UpperBoundNumberRequirement upperBoundRequirement;

            upperBoundRequirement = ((MeasuredRangeRequirement)valueRequirement).getUpperBoundNumberRequirement();
            upperBoundNumber = (UpperBoundNumber)RecordValue.createValue(upperBoundRequirement);
            upperBoundNumber.setValue(value);
            setSubValue(upperBoundNumber);
            return upperBoundNumber;
        }
    }

    public LowerBoundNumber setLowerBoundNumber(String value)
    {
        LowerBoundNumber lowerBoundNumber;

        // Set the numeric value or create it, if no RecordValue exists.
        lowerBoundNumber = getLowerBoundNumberObject();
        if (lowerBoundNumber != null)
        {
            lowerBoundNumber.setValue(value);
            return lowerBoundNumber;
        }
        else
        {
            LowerBoundNumberRequirement lowerBoundRequirement;

            lowerBoundRequirement = ((MeasuredRangeRequirement)valueRequirement).getLowerBoundNumberRequirement();
            lowerBoundNumber = (LowerBoundNumber)RecordValue.createValue(lowerBoundRequirement);
            lowerBoundNumber.setValue(value);
            setSubValue(lowerBoundNumber);
            return lowerBoundNumber;
        }
    }

    @Override
    public RecordValue clearUom()
    {
        RecordValue uom;

        uom = getSubValue(RequirementType.UNIT_OF_MEASURE, null, null);
        if (uom != null)
        {
            this.removeSubValue(uom);
            return uom;
        }
        else return null;
    }

    @Override
    public RecordValue clearQom()
    {
        RecordValue qom;

        qom = getSubValue(RequirementType.QUALIFIER_OF_MEASURE, null, null);
        if (qom != null)
        {
            this.removeSubValue(qom);
            return qom;
        }
        else return null;
    }

    public LowerBoundNumber getOrCreateLowerBoundNumber()
    {
        LowerBoundNumber lowerBoundNumber;

        // Set the numeric value or create it, if no RecordValue exists.
        lowerBoundNumber = getLowerBoundNumberObject();
        if (lowerBoundNumber != null)
        {
            return lowerBoundNumber;
        }
        else
        {
            LowerBoundNumberRequirement lowerBoundRequirement;

            lowerBoundRequirement = ((MeasuredRangeRequirement)valueRequirement).getLowerBoundNumberRequirement();
            lowerBoundNumber = (LowerBoundNumber)RecordValue.createValue(lowerBoundRequirement);
            setSubValue(lowerBoundNumber);
            return lowerBoundNumber;
        }
    }

    public UpperBoundNumber getOrCreateUpperBoundNumber()
    {
        UpperBoundNumber upperBoundNumber;

        // Set the numeric value or create it, if no RecordValue exists.
        upperBoundNumber = getUpperBoundNumberObject();
        if (upperBoundNumber != null)
        {
            return upperBoundNumber;
        }
        else
        {
            UpperBoundNumberRequirement upperBoundRequirement;

            upperBoundRequirement = ((MeasuredRangeRequirement)valueRequirement).getUpperBoundNumberRequirement();
            upperBoundNumber = (UpperBoundNumber)RecordValue.createValue(upperBoundRequirement);
            setSubValue(upperBoundNumber);
            return upperBoundNumber;
        }
    }
}
