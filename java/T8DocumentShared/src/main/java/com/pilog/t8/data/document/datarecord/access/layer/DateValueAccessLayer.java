package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.time.T8Date;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class DateValueAccessLayer extends ValueAccessLayer
{
    private boolean restrictToDatePickerSelection;
    private T8Date upperBound;
    private T8Date lowerBound;

    public DateValueAccessLayer()
    {
        super(RequirementType.DATE_TYPE);
        this.restrictToDatePickerSelection = false;
    }

    public T8Date getUpperBound()
    {
        return upperBound;
    }

    public void setUpperBound(T8Date upperBound)
    {
        this.upperBound = upperBound;
    }

    public T8Date getLowerBound()
    {
        return lowerBound;
    }

    public void setLowerBound(T8Date lowerBound)
    {
        this.lowerBound = lowerBound;
    }

    /**
     * Specifies whether or not only the selection from the date picker popup is
     * allowed iff the property is editable.<br>
     * <br>
     * The default for this property is {@code false} for backward
     * compatibility.
     *
     * @return {@code true} if restricted. {@code false} otherwise
     */
    public boolean isRestrictToDatePickerSelection()
    {
        return restrictToDatePickerSelection;
    }

    /**
     * Specifies whether or not only the selection from the date picker popup is
     * allowed iff the property is editable.<br>
     * <br>
     * The default for this property is {@code false} for backward
     * compatibility.
     *
     * @param restrict {@code true} to restrict
     */
    public void setRestrictToDatePickerSelection(boolean restrict)
    {
        this.restrictToDatePickerSelection = restrict;
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer valueAccess)
    {
        if (valueAccess instanceof DateValueAccessLayer)
        {
            DateValueAccessLayer dateAccess;

            dateAccess = (DateValueAccessLayer)valueAccess;
            if (!Objects.equals(restrictToDatePickerSelection, dateAccess.isRestrictToDatePickerSelection())) return false;
            else if (!Objects.equals(lowerBound, dateAccess.getLowerBound())) return false;
            else if (!Objects.equals(upperBound, dateAccess.getUpperBound())) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public ValueAccessLayer copy()
    {
        DateValueAccessLayer copy;

        copy = new DateValueAccessLayer();
        copy.setLowerBound(lowerBound);
        copy.setUpperBound(upperBound);
        copy.setRestrictToDatePickerSelection(restrictToDatePickerSelection);
        return copy;
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
        DateValueAccessLayer newDateAccess;

        newDateAccess = (DateValueAccessLayer)newAccess;
        setLowerBound(newDateAccess.getLowerBound());
        setUpperBound(newDateAccess.getUpperBound());
        setRestrictToDatePickerSelection(newDateAccess.isRestrictToDatePickerSelection());
    }
}
