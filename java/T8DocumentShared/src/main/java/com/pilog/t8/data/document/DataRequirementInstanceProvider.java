package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;

/**
 * @author Bouwer du Preez
 */
public interface DataRequirementInstanceProvider
{
    public DataRequirementInstance getDataRequirementInstance(String drInstanceID, String languageID, boolean includeOntology, boolean includeTerminology);
}
