package com.pilog.t8.definition.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.datatype.T8DtDataFileSaveResult;
import com.pilog.t8.datatype.T8DtDataFileUpdate;
import com.pilog.t8.datatype.T8DtDataRecord;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtRecordValue;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_RED_OPEN_FILE = "@OP_API_RED_OPEN_FILE";
    public static final String OPERATION_API_RED_CLOSE_FILE = "@OP_API_RED_CLOSE_FILE";
    public static final String OPERATION_API_RED_UPDATE_FILE = "@OP_API_RED_UPDATE_FILE";
    public static final String OPERATION_API_RED_SAVE_FILE = "@OP_API_RED_SAVE_FILE";
    public static final String OPERATION_API_RED_RETRIEVE_VALUE_SUGGESTIONS = "@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS";
    public static final String OPERATION_API_RED_RETRIEVE_VALUE_OPTIONS = "@OP_API_RED_RETRIEVE_VALUE_OPTIONS";
    public static final String OPERATION_API_RED_RETRIEVE_DR_INSTANCE_OPTIONS = "@OP_API_RED_RETRIEVE_DR_INSTANCE_OPTIONS";

    public static final String PARAMETER_EDITOR_SESSION_ID = "$P_EDITOR_SESSION_ID";
    public static final String PARAMETER_FILE_ID = "$P_FILE_ID";
    public static final String PARAMETER_RECORD_ID = "$P_RECORD_ID";
    public static final String PARAMETER_RECORD_VALUES = "$P_RECORD_VALUES";
    public static final String PARAMETER_FILE = "$P_FILE";
    public static final String PARAMETER_SAVE_CHANGES = "$P_SAVE_CHANGES";
    public static final String PARAMETER_SAVE_RESULT = "$P_SAVE_RESULT";
    public static final String PARAMETER_FILE_UPDATE = "$P_FILE_UPDATE";
    public static final String PARAMETER_PROPERTY_ID = "$P_PROPERTY_ID";
    public static final String PARAMETER_FIELD_ID = "$P_FIELD_ID";
    public static final String PARAMETER_DATA_TYPE_ID = "$P_DATA_TYPE_ID";
    public static final String PARAMETER_SEARCH_PREFIX = "$P_SEARCH_PREFIX";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";
    public static final String PARAMETER_SEARCH_STRING = "$P_SEARCH_STRING";
    public static final String PARAMETER_SEARCH_ELEMENTS = "$P_SEARCH_ELEMENTS";
    public static final String PARAMETER_CONCEPT_TERMINOLOGY_LIST = "$P_CONCEPT_TERMINOLOGY_LIST";

    public static final String E_CONCEPT_LOOKUP = "@E_API_RED_CONCEPT_LOOKUP";
    public static final String DS_CONCEPT_LOOKUP = "@DS_API_RED_CONCEPT_LOOKUP";

    public static final String E_CONCEPT_RELATION_LOOKUP = "@E_API_RED_CONCEPT_RELATION_LOOKUP";
    public static final String DS_CONCEPT_RELATION_LOOKUP = "@DS_API_RED_CONCEPT_RELATION_LOOKUP";

    public static final String E_PRESCRIBED_VALUE_LOOKUP = "@E_API_RED_PRESCRIBED_VALUE_LOOKUP";
    public static final String DS_PRESCRIBED_VALUE_LOOKUP = "@DS_API_RED_PRESCRIBED_VALUE_LOOKUP";

    public static final String F_PROPERTY_ID = "$PROPERTY_ID";
    public static final String F_FIELD_ID = "$FIELD_ID";
    public static final String F_CLASS_ID = "$CLASS_ID";
    public static final String F_CONCEPT_ID = "$CONCEPT_ID";
    public static final String F_IRDI = "$IRDI";
    public static final String F_CONCEPT_TYPE_ID = "$CONCEPT_TYPE_ID";
    public static final String F_CONCEPT_GRAPH_ID = "$CONCEPT_GRAPH_ID";
    public static final String F_TAIL_CONCEPT_ID = "$TAIL_CONCEPT_ID";
    public static final String F_HEAD_CONCEPT_ID = "$HEAD_CONCEPT_ID";
    public static final String F_TERM_ID = "$TERM_ID";
    public static final String F_CONCEPT_TERM_ID = "$CONCEPT_TERM_ID";
    public static final String F_ABBREVIATION_ID = "$ABBREVIATION_ID";
    public static final String F_CONCEPT_ABBREVIATION_ID = "$CONCEPT_ABBREVIATION_ID";
    public static final String F_DEFINITION_ID = "$DEFINITION_ID";
    public static final String F_CONCEPT_DEFINITION_ID = "$CONCEPT_DEFINITION_ID";
    public static final String F_TERM = "$TERM";
    public static final String F_CONCEPT_TERM = "$CONCEPT_TERM";
    public static final String F_ABBREVIATION = "$ABBREVIATION";
    public static final String F_CONCEPT_ABBREVIATION = "$CONCEPT_ABBREVIATION";
    public static final String F_DEFINITION = "$DEFINITION";
    public static final String F_CONCEPT_DEFINITION = "$CONCEPT_DEFINITION";
    public static final String F_CODE = "$CODE";
    public static final String F_CONCEPT_CODE = "$CONCEPT_CODE";
    public static final String F_CODE_ID = "$CODE_ID";
    public static final String F_CONCEPT_CODE_ID = "$CONCEPT_CODE_ID";
    public static final String F_CODE_TYPE_ID = "$CODE_TYPE_ID";
    public static final String F_LANGUAGE_ID = "$LANGUAGE_ID";
    public static final String F_NAME = "$NAME";
    public static final String F_PRIMARY_INDICATOR = "$PRIMARY_INDICATOR";
    public static final String F_ACTIVE_INDICATOR = "$ACTIVE_INDICATOR";
    public static final String F_STANDARD_INDICATOR = "$STANDARD_INDICATOR";
    public static final String F_APPROVED_INDICATOR = "$APPROVED_INDICATOR";
    public static final String F_LFT = "$LFT";
    public static final String F_RGT = "$RGT";
    public static final String F_ODS_ID = "$ODS_ID";
    public static final String F_ODT_ID = "$ODT_ID";
    public static final String F_ODT_TERM = "$ODT_TERM";
    public static final String F_ODT_DEFINITION = "$ODT_DEFINITION";
    public static final String F_PARENT_ODT_ID = "$PARENT_ODT_ID";
    public static final String F_ORG_ID = "$ORG_ID";
    public static final String F_PATH_ORG_ID = "$PATH_ORG_ID";
    public static final String F_ROOT_ORG_ID = "$ROOT_ORG_ID";
    public static final String F_ORG_LEVEL = "$ORG_LEVEL";
    public static final String F_ORG_TYPE_ID = "$ORG_TYPE_ID";
    public static final String F_DATA_TYPE_NAME = "$DATA_TYPE_NAME";
    public static final String F_CONCEPT_ODT_ID = "$CONCEPT_ODT_ID";
    public static final String F_PARENT_ORG_ID = "$PARENT_ORG_ID";
    public static final String F_TAGS = "$TAGS";
    public static final String F_ATTRIBUTE_ID = "$ATTRIBUTE_ID";
    public static final String F_TYPE = "$TYPE";
    public static final String F_DR_ID = "$DR_ID";
    public static final String F_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String F_COMMENT_ID = "$COMMENT_ID";
    public static final String F_COMMENT_TYPE_ID = "$COMMENT_TYPE_ID";
    public static final String F_COMMENT = "$COMMENT";
    public static final String F_CONDITION_EXPRESSION = "$CONDITION_EXPRESSION";
    public static final String F_DATA_KEY_EXPRESSION = "$DATA_KEY_EXPRESSION";
    public static final String F_INDEPENDENT_INDICATOR = "$INDEPENDENT_INDICATOR";
    public static final String F_ROOT_RECORD_ID = "$ROOT_RECORD_ID";
    public static final String F_RECORD_ID = "$RECORD_ID";
    public static final String F_RECORD_ID1 = "$RECORD_ID1";
    public static final String F_RECORD_ID2 = "$RECORD_ID2";
    public static final String F_MASTER_RECORD_ID = "$MASTER_RECORD_ID";
    public static final String F_FAMILY_ID = "$FAMILY_ID";
    public static final String F_FAMILY_CODE = "$FAMILY_CODE";
    public static final String F_RESOLVED_STATE = "$RESOLVED_STATE";
    public static final String F_RESOLUTION_ID = "$RESOLUTION_ID";
    public static final String F_SUBMITTED_AT = "$SUBMITTED_AT";
    public static final String F_SUBMITTED_BY = "$SUBMITTED_BY";
    public static final String F_FINALIZED_AT = "$FINALIZED_AT";
    public static final String F_FINALIZED_BY = "$FINALIZED_BY";
    public static final String F_FINALIZED_INDICATOR = "$FINALIZED_INDICATOR";
    public static final String F_INSTANCE_ID = "$INSTANCE_ID";
    public static final String F_MATCH_INSTANCE_ID = "$MATCH_INSTANCE_ID";
    public static final String F_CRITERIA_ID = "$CRITERIA_ID";
    public static final String F_CASE_ID = "$CASE_ID";
    public static final String F_PARENT_RECORD_ID = "$PARENT_RECORD_ID";
    public static final String F_CHILD_RECORD_ID = "$CHILD_RECORD_ID";
    public static final String F_VALUE_STRING = "$VALUE_STRING";
    public static final String F_ATTACHMENT_ID = "$ATTACHMENT_ID";
    public static final String F_FILE_CONTEXT_ID = "$FILE_CONTEXT_ID";
    public static final String F_FILE_DATA = "$FILE_DATA";
    public static final String F_FILE_NAME = "$FILE_NAME";
    public static final String F_MEDIA_TYPE = "$MEDIA_TYPE";
    public static final String F_FILE_SIZE = "$FILE_SIZE";
    public static final String F_MD5_CHECKSUM = "$MD5_CHECKSUM";
    public static final String F_DATA_RECORD_ID = "$ID";
    public static final String F_DATA_RECORD_DOCUMENT = "$DOCUMENT";
    public static final String F_DATA_REQUIREMENT_ID = "$ID";
    public static final String F_DATA_REQUIREMENT_PARENT_ID = "$PARENT_DR_ID";
    public static final String F_DATA_REQUIREMENT_CHILD_ID = "$CHILD_DR_ID";
    public static final String F_DATA_REQUIREMENT_INSTANCE_ID = "$ID";
    public static final String F_DATA_REQUIREMENT_INSTANCE_DOCUMENT = "$DOCUMENT";
    public static final String F_DATA_REQUIREMENT_INSTANCE_PARENT_ID = "$PARENT_DR_INSTANCE_ID";
    public static final String F_DATA_REQUIREMENT_INSTANCE_CHILD_ID = "$CHILD_DR_INSTANCE_ID";
    public static final String F_DATA_REQUIREMENT_DOCUMENT = "$DOCUMENT";
    public static final String F_IDENTIFIER = "$IDENTIFIER";
    public static final String F_DATA_STRING = "$DATA_STRING";
    public static final String F_DATA_STRING_TYPE_ID = "$DATA_STRING_TYPE_ID";
    public static final String F_DATA_STRING_FORMAT_ID = "$DATA_STRING_FORMAT_ID";
    public static final String F_DATA_STRING_INSTANCE_ID = "$DATA_STRING_INSTANCE_ID";
    public static final String F_AUTO_RENDER = "$AUTO_RENDER";
    public static final String F_DATA_TYPE_ID = "$DATA_TYPE_ID";
    public static final String F_PARENT_DATA_TYPE_ID = "$PARENT_DATA_TYPE_ID";
    public static final String F_DATA_TYPE_SEQUENCE = "$DATA_TYPE_SEQUENCE";
    public static final String F_VALUE_SEQUENCE = "$VALUE_SEQUENCE";
    public static final String F_SEQUENCE = "$SEQUENCE";
    public static final String F_VALUE = "$VALUE";
    public static final String F_VALUE_CONCEPT_ID = "$VALUE_CONCEPT_ID";
    public static final String F_LOWER_BOUND_VALUE = "$LOWER_BOUND_VALUE";
    public static final String F_UPPER_BOUND_VALUE = "$UPPER_BOUND_VALUE";
    public static final String F_STANDARD_VALUE_ID = "$STANDARD_VALUE_ID";
    public static final String F_VALUE_ID = "$VALUE_ID";
    public static final String F_OLD_VALUE_ID = "$OLD_VALUE_ID";
    public static final String F_UOM_ID = "$UOM_ID";
    public static final String F_UOM_TERM = "$UOM_TERM";
    public static final String F_UOM_CODE = "$UOM_CODE";
    public static final String F_UOM_ABBREVIATION = "$UOM_ABBREVIATION";
    public static final String F_QOM_ID = "$QOM_ID";
    public static final String F_QOM_TERM = "$QOM_TERM";
    public static final String F_QOM_CODE = "$QOM_CODE";
    public static final String F_LOWER_BOUND_VALUE_ID = "$LOWER_BOUND_VALUE_ID";
    public static final String F_UPPER_BOUND_VALUE_ID = "$UPPER_BOUND_VALUE_ID";
    public static final String F_PACKAGE_IID = "$ALTERATION_IID";
    public static final String F_PACKAGE_ID = "$ALTERATION_ID";
    public static final String F_ALTERATION_ID = "$ALTERATION_ID";
    public static final String F_ALTERATION_TYPE = "$ALTERATION_TYPE";
    public static final String F_DATA_TYPE = "$DATA_TYPE";
    public static final String F_STEP = "$STEP";
    public static final String F_SIZE = "$SIZE";
    public static final String F_INSERTED_AT = "$INSERTED_AT";
    public static final String F_UPDATED_AT = "$UPDATED_AT";
    public static final String F_OLD_VALUE = "$OLD_VALUE";
    public static final String F_OLD_LOWER_BOUND_VALUE = "$OLD_LOWER_BOUND_VALUE";
    public static final String F_OLD_UPPER_BOUND_VALUE = "$OLD_UPPER_BOUND_VALUE";
    public static final String F_OLD_CONCEPT_ID = "$OLD_CONCEPT_ID";
    public static final String F_OLD_CONCEPT_TERM = "$OLD_CONCEPT_TERM";
    public static final String F_OLD_UOM_ID = "$OLD_UOM_ID";
    public static final String F_OLD_UOM_TERM = "$OLD_UOM_TERM";
    public static final String F_OLD_QOM_ID = "$OLD_QOM_ID";
    public static final String F_OLD_QOM_TERM = "$OLD_QOM_TERM";
    public static final String F_PHRASE = "$PHRASE";
    public static final String F_INSERTED_BY_ID = "$INSERTED_BY_ID";
    public static final String F_INSERTED_BY_IID = "$INSERTED_BY_IID";
    public static final String F_UPDATED_BY_ID = "$UPDATED_BY_ID";
    public static final String F_UPDATED_BY_IID = "$UPDATED_BY_IID";
    public static final String F_ASYNC_ONT_UPD_TIME = "$ASYNC_ONT_UPD_TIME";
    public static final String F_ASYNC_ATR_UPD_TIME = "$ASYNC_ATR_UPD_TIME";
    public static final String F_PROPERTY_COUNT = "$PROPERTY_COUNT";
    public static final String F_PROPERTY_DATA_COUNT = "$PROPERTY_DATA_COUNT";
    public static final String F_PROPERTY_VALID_COUNT = "$PROPERTY_VALID_COUNT";
    public static final String F_PROPERTY_VERIFICATION_COUNT = "$PROPERTY_VERIFICATION_COUNT";
    public static final String F_PROPERTY_ACCURATE_COUNT = "$PROPERTY_ACCURATE_COUNT";
    public static final String F_CHRCTRSTC_COUNT = "$CHRCTRSTC_COUNT";
    public static final String F_CHRCTRSTC_DATA_COUNT = "$CHRCTRSTC_DATA_COUNT";
    public static final String F_CHRCTRSTC_VALID_COUNT = "$CHRCTRSTC_VALID_COUNT";
    public static final String F_CHRCTRSTC_VERIFICATION_COUNT = "$CHRCTRSTC_VERIFICATION_COUNT";
    public static final String F_CHRCTRSTC_ACCURATE_COUNT = "$CHRCTRSTC_ACCURATE_COUNT";
    public static final String F_STANDARDIZED_DR_INSTANCE_ID = "$STANDARDIZED_DR_INSTANCE_ID";
    public static final String F_STANDARDIZED_DR_ID = "$STANDARDIZED_DR_ID";
    public static final String F_STANDARDIZED_PROPERTY_ID = "$STANDARDIZED_PROPERTY_ID";
    public static final String F_CONCEPT_ORG_ID = "$CONCEPT_ORG_ID";
    public static final String F_CONCEPT_ODS_ID = "$CONCEPT_ODS_ID";
    public static final String F_CONCEPT_ACTIVE_INDICATOR = "$CONCEPT_ACTIVE_INDICATOR";
    public static final String F_UOM_TERM_ID = "$UOM_TERM_ID";
    public static final String F_UOM_ABBREVIATION_ID = "$UOM_ABBREVIATION_ID";
    public static final String F_UOM_DEFINITION_ID = "$UOM_DEFINITION_ID";
    public static final String F_UOM_DEFINITION = "$UOM_DEFINITION";
    public static final String F_UOM_CODE_ID = "$UOM_CODE_ID";
    public static final String F_QOM_TERM_ID = "$QOM_TERM_ID";
    public static final String F_QOM_ABBREVIATION_ID = "$QOM_ABBREVIATION_ID";
    public static final String F_QOM_ABBREVIATION = "$QOM_ABBREVIATION";
    public static final String F_QOM_DEFINITION_ID = "$QOM_DEFINITION_ID";
    public static final String F_QOM_DEFINITION = "$QOM_DEFINITION";
    public static final String F_QOM_CODE_ID = "$QOM_CODE_ID";

    // Table Names.

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIds)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIds.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions(serverContext));
        if (typeIds.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIds.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getOperationDefinitions(T8ServerContext serverContext)
    {
        T8DtDataFileSaveResult dtDataFileSaveResult;
        T8JavaServerOperationDefinition definition;
        T8DefinitionManager definitionManager;
        T8DtDataFileUpdate dtDataFileUpdate;
        List<T8Definition> definitions;
        T8DataType dtRecordValueList;

        definitionManager = serverContext.getDefinitionManager();
        dtDataFileUpdate = new T8DtDataFileUpdate(definitionManager);
        dtDataFileSaveResult = new T8DtDataFileSaveResult(definitionManager);
        dtRecordValueList = new T8DtList(definitionManager, new T8DtRecordValue(definitionManager));
        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_OPEN_FILE);
        definition.setMetaDisplayName("Open Data File");
        definition.setMetaDescription("Retrieves the specified data file and adds it to the specified editor session, thereby opening it for editing.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiOpenFile");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file must be opened.  Session remains open for a specific period and allow subsequent modifications to the records they contain.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_ID, "File Id", "The id of the file to open.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE, "Data File", "The data file opened for editing.", new T8DtDataRecord()));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_CLOSE_FILE);
        definition.setMetaDisplayName("Close Data File");
        definition.setMetaDescription("Closes the specified data file that is currently open in the specified session, optionally saving any uncommtted changes it may have.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiCloseFile");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file is currently open.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_ID, "File Id", "The id of the file to close.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SAVE_CHANGES, "Save Changes", "A boolean flag which specifies whether or not outstanding changes to the currently open record should be saved before it is closed.", T8DataType.BOOLEAN));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SAVE_RESULT, "Save Result", "The result of the attempt to save outstanding changes (if required).", dtDataFileSaveResult));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_SAVE_FILE);
        definition.setMetaDisplayName("Save Data File");
        definition.setMetaDescription("Saves all outstanding changes on the specified data file that is currently open in the specified session.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiSaveFile");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file is currently open.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_ID, "File Id", "The id of the file to save.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SAVE_RESULT, "Save Result", "The result of the attempt to save outstanding changes (if required).", dtDataFileSaveResult));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_UPDATE_FILE);
        definition.setMetaDisplayName("Update Data File");
        definition.setMetaDescription("Updates the specified data file that is currently open in the specified session with the supplied list of changes.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiUpdateFile");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file is currently open.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_UPDATE, "Input File Update", "The input file updates to apply to the specified file.", dtDataFileUpdate));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_UPDATE, "Output File Update", "The output file updates that resulted from the input updates (if any).", dtDataFileUpdate));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_RETRIEVE_VALUE_SUGGESTIONS);
        definition.setMetaDisplayName("Retrieve Value Suggestions");
        definition.setMetaDescription("Get value suggestions for the specified search criteria. Suggestions are limited to 20 if not specified.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiRetrieveValueSuggestions");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file is currently open.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_ID, "File Id", "The id of the target data file.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_PREFIX, "Search Prefix", "The prefix to use as search criterion when retrieving suggestions.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page of values to return.", T8DataType.INTEGER, true));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The size of the page of values to return.", T8DataType.INTEGER, true));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_VALUES, "Record Values", "A list of suggested Record Values for the specified input.", dtRecordValueList));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_RETRIEVE_VALUE_OPTIONS);
        definition.setMetaDisplayName("Retrieve Value Options");
        definition.setMetaDescription("Retrieve the value options for the specified record property/field.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiRetrieveValueOptions");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file is currently open.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_ID, "File Id", "The id of the target data file.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_STRING, "Search String", "The string of text to use as search criteria.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_ELEMENTS, "Search Elements", "The list of concept elements to search on (CODE/TERM/DEFINITION/ABBREVIATION).", T8DataType.LIST));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page of values to return.", T8DataType.INTEGER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The size of the page of values to return.", T8DataType.INTEGER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_VALUES, "Record Values", "A list of suggested Record Values for the specified input.", dtRecordValueList));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RED_RETRIEVE_DR_INSTANCE_OPTIONS);
        definition.setMetaDisplayName("Retrieve DR Instance Options");
        definition.setMetaDescription("Retrieve the DR Instance options for the specified record property/field.  These options determine the types of document references that may be added to the target property/field.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordEditorApiOperations$ApiRetrieveDrInstanceOptions");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_EDITOR_SESSION_ID, "Editor Session Id", "The id of the editor session in which this file is currently open.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_ID, "File Id", "The id of the target data file.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page of values to return.", T8DataType.INTEGER));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The size of the page of values to return.", T8DataType.INTEGER));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_TERMINOLOGY_LIST, "Concept Terminology List", "A list of DR Instance concept terminologies.", T8DataType.LIST));
        definitions.add(definition);

        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8SQLDataSourceDefinition sqlDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // DS_CONCEPT_LOOKUP Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DS_CONCEPT_LOOKUP);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "   OS.ROOT_ORG_ID, \n" +
            "	OO.ORG_ID,  \n" +
            "   OO.ODS_ID,  \n" +
            "   OO.ODT_ID, \n" +
            "   OO.CONCEPT_ID, \n" +
            "   OO.ACTIVE_INDICATOR, \n" +
            "   T.LANGUAGE_ID, \n" +
            "   T.CONCEPT_TYPE_ID, \n" +
            "   T.TERM_ID, \n" +
            "   T.TERM, \n" +
            "   T.DEFINITION_ID, \n" +
            "   T.DEFINITION, \n" +
            "   T.ABBREVIATION_ID, \n" +
            "   T.ABBREVIATION, \n" +
            "   T.CODE_ID, \n" +
            "   T.CODE \n" +
            "FROM ORG_ONT OO \n" +
            "INNER JOIN ORG_STR OS \n" +
            "   ON OO.ORG_ID = OS.ORG_ID \n" +
            "INNER JOIN ORG_TMN T \n" +
            "   ON T.CONCEPT_ID = OO.CONCEPT_ID \n" +
            "   AND T.ORG_ID = OS.ROOT_ORG_ID"

        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ROOT_ORG_ID, "ROOT_ORG_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ODS_ID, "ODS_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ODT_ID, "ODT_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM_ID, "TERM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM, "TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION_ID, "DEFINITION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION, "DEFINITION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ABBREVIATION_ID, "ABBREVIATION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ABBREVIATION, "ABBREVIATION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE_ID, "CODE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE, "CODE", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // DS_CONCEPT_RELATION_LOOKUP Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DS_CONCEPT_RELATION_LOOKUP);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT \n" +
            "    a.ORG_ID,\n" +
            "    a.ODS_ID,\n" +
            "    a.ODT_ID,\n" +
            "    a.CONCEPT_ID,\n" +
            "    d.LANGUAGE_ID,\n" +
            "    d.CONCEPT_TYPE_ID,\n" +
            "    d.TERM_ID,\n" +
            "    d.TERM,\n" +
            "    d.DEFINITION_ID,\n" +
            "    d.DEFINITION,\n" +
            "    d.ABBREVIATION_ID,\n" +
            "    d.ABBREVIATION,\n" +
            "    d.CODE_ID,\n" +
            "    d.CODE,\n" +
            "    a.ACTIVE_INDICATOR,\n" +
            "    b.CONCEPT_GRAPH_ID,\n" +
            "    b.TAIL_CONCEPT_ID,\n" +
            "    b.HEAD_CONCEPT_ID\n" +
            "FROM ORG_ONT a\n" +
            "INNER JOIN ONT_REL b\n" +
            "	ON a.CONCEPT_ID = b.HEAD_CONCEPT_ID  \n" +
            "INNER JOIN ORG_STR c\n" +
            "	ON a.ORG_ID = c.ORG_ID\n" +
            "LEFT JOIN ORG_TMN d\n" +
            "	ON c.ROOT_ORG_ID = d.ORG_ID\n" +
            "	AND a.CONCEPT_ID = d.CONCEPT_ID "
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ODS_ID, "ODS_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ODT_ID, "ODT_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ID, "CONCEPT_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ACTIVE_INDICATOR, "ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM_ID, "TERM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM, "TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION_ID, "DEFINITION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION, "DEFINITION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ABBREVIATION_ID, "ABBREVIATION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ABBREVIATION, "ABBREVIATION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE_ID, "CODE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE, "CODE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_GRAPH_ID, "CONCEPT_GRAPH_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TAIL_CONCEPT_ID, "TAIL_CONCEPT_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_HEAD_CONCEPT_ID, "HEAD_CONCEPT_ID", false, false, T8DataType.GUID));
        definitions.add(sqlDataSourceDefinition);

        // DS_PRESCRIBED_VALUE_LOOKUP Data Source.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DS_PRESCRIBED_VALUE_LOOKUP);
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLWithString
        (
            "WITH LANGUAGES AS\n" +
            "(\n" +
            "  SELECT\n" +
            "    ORG_ID,\n" +
            "    CONCEPT_ID AS LANGUAGE_ID\n" +
            "  FROM ORG_ONT WHERE ODT_ID = <<-LANGUAGE_ODT_ID->> \n" +
            ")"
        );
        sqlDataSourceDefinition.setSQLParameterExpressionMap(HashMaps.newHashMap("LANGUAGE_ODT_ID", "dbAdaptor.getSQLHexToGUID(\"" + T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID() + "\")"));
        sqlDataSourceDefinition.setSQLSelectString
        (
            "SELECT\n" +
            "    DRV.VALUE_ID,\n" +
            "    DRV.ORG_ID,\n" +
            "    DRV.DR_ID,\n" +
            "    DRV.DR_INSTANCE_ID,\n" +
            "    DRV.PROPERTY_ID,\n" +
            "    DRV.FIELD_ID,\n" +
            "    DRV.DATA_TYPE_ID,\n" +
            "    DRV.VALUE,\n" +
            "    DRV.LOWER_BOUND_VALUE,\n" +
            "    DRV.UPPER_BOUND_VALUE,\n" +
            "    DRV.CONCEPT_ID,\n" +
            "    OCON.ORG_ID AS CONCEPT_ORG_ID,\n" +
            "    OCON.ODS_ID AS CONCEPT_ODS_ID,\n" +
            "    OCON.ODT_ID AS CONCEPT_ODT_ID,\n" +
            "    OCON.ACTIVE_INDICATOR AS CONCEPT_ACTIVE_INDICATOR,\n" +
            "    TCON.TERM_ID AS CONCEPT_TERM_ID,\n" +
            "    TCON.TERM AS CONCEPT_TERM,\n" +
            "    TCON.ABBREVIATION_ID AS CONCEPT_ABBREVIATION_ID,\n" +
            "    TCON.ABBREVIATION AS CONCEPT_ABBREVIATION,\n" +
            "    TCON.DEFINITION_ID AS CONCEPT_DEFINITION_ID,\n" +
            "    TCON.DEFINITION AS CONCEPT_DEFINITION,\n" +
            "    TCON.CODE_ID AS CONCEPT_CODE_ID,\n" +
            "    TCON.CODE AS CONCEPT_CODE,\n" +
            "    TCON.CONCEPT_TYPE_ID,\n" +
            "    DRV.UOM_ID,\n" +
            "    TUOM.TERM_ID AS UOM_TERM_ID,\n" +
            "    TUOM.TERM AS UOM_TERM,\n" +
            "    TUOM.ABBREVIATION_ID AS UOM_ABBREVIATION_ID,\n" +
            "    TUOM.ABBREVIATION AS UOM_ABBREVIATION,\n" +
            "    TUOM.DEFINITION_ID AS UOM_DEFINITION_ID,\n" +
            "    TUOM.DEFINITION AS UOM_DEFINITION,\n" +
            "    TUOM.CODE_ID AS UOM_CODE_ID,\n" +
            "    TUOM.CODE AS UOM_CODE,\n" +
            "    DRV.QOM_ID,\n" +
            "    TQOM.TERM_ID AS QOM_TERM_ID,\n" +
            "    TQOM.TERM AS QOM_TERM,\n" +
            "    TQOM.ABBREVIATION_ID AS QOM_ABBREVIATION_ID,\n" +
            "    TQOM.ABBREVIATION AS QOM_ABBREVIATION,\n" +
            "    TQOM.DEFINITION_ID AS QOM_DEFINITION_ID,\n" +
            "    TQOM.DEFINITION AS QOM_DEFINITION,\n" +
            "    TQOM.CODE_ID AS QOM_CODE_ID,\n" +
            "    TQOM.CODE AS QOM_CODE,\n" +
            "    LAN.LANGUAGE_ID,\n" +
            "    DRV.STANDARD_INDICATOR,\n" +
            "    DRV.APPROVED_INDICATOR\n" +
            "FROM DAT_REQ_VAL DRV\n" +
            "INNER JOIN ORG_ONT OCON\n" +
            "	ON DRV.CONCEPT_ID = OCON.CONCEPT_ID\n" +
            "INNER JOIN LANGUAGES LAN\n" +
            "	ON DRV.ORG_ID = LAN.ORG_ID\n" +
            "LEFT JOIN ORG_TMN TCON\n" +
            "	ON DRV.ORG_ID = TCON.ORG_ID\n" +
            "	AND DRV.CONCEPT_ID = TCON.CONCEPT_ID \n" +
            "	AND TCON.LANGUAGE_ID = LAN.LANGUAGE_ID\n" +
            "LEFT JOIN ORG_TMN TUOM\n" +
            "	ON DRV.ORG_ID = TUOM.ORG_ID\n" +
            "	AND DRV.UOM_ID = TUOM.CONCEPT_ID \n" +
            "	AND TUOM.LANGUAGE_ID = LAN.LANGUAGE_ID\n" +
            "LEFT JOIN ORG_TMN TQOM\n" +
            "	ON DRV.ORG_ID = TQOM.ORG_ID\n" +
            "	AND DRV.QOM_ID = TQOM.CONCEPT_ID \n" +
            "	AND TQOM.LANGUAGE_ID = LAN.LANGUAGE_ID"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE_ID, "VALUE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_ID, "PROPERTY_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_TYPE_ID, "DATA_TYPE_ID", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE, "VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LOWER_BOUND_VALUE, "LOWER_BOUND_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPPER_BOUND_VALUE, "UPPER_BOUND_VALUE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ID, "CONCEPT_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ORG_ID, "CONCEPT_ORG_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ODS_ID, "CONCEPT_ODS_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ODT_ID, "CONCEPT_ODT_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ACTIVE_INDICATOR, "CONCEPT_ACTIVE_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_TERM_ID, "CONCEPT_TERM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_TERM, "CONCEPT_TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ABBREVIATION_ID, "CONCEPT_ABBREVIATION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_ABBREVIATION, "CONCEPT_ABBREVIATION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_DEFINITION_ID, "CONCEPT_DEFINITION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_DEFINITION, "CONCEPT_DEFINITION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_CODE_ID, "CONCEPT_CODE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_CODE, "CONCEPT_CODE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONCEPT_TYPE_ID, "CONCEPT_TYPE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_ID, "UOM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_TERM_ID, "UOM_TERM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_TERM, "UOM_TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_ABBREVIATION_ID, "UOM_ABBREVIATION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_ABBREVIATION, "UOM_ABBREVIATION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_DEFINITION_ID, "UOM_DEFINITION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_DEFINITION, "UOM_DEFINITION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_CODE_ID, "UOM_CODE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UOM_CODE, "UOM_CODE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_ID, "QOM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_TERM_ID, "QOM_TERM_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_TERM, "QOM_TERM", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_ABBREVIATION_ID, "QOM_ABBREVIATION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_ABBREVIATION, "QOM_ABBREVIATION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_DEFINITION_ID, "QOM_DEFINITION_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_DEFINITION, "QOM_DEFINITION", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_CODE_ID, "QOM_CODE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_QOM_CODE, "QOM_CODE", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STANDARD_INDICATOR, "STANDARD_INDICATOR", false, false, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_APPROVED_INDICATOR, "APPROVED_INDICATOR", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // E_CONCEPT_LOOKUP Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(E_CONCEPT_LOOKUP);
        entityDefinition.setDataSourceIdentifier(DS_CONCEPT_LOOKUP);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ROOT_ORG_ID, DS_CONCEPT_LOOKUP + F_ROOT_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_CONCEPT_LOOKUP + F_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ODS_ID, DS_CONCEPT_LOOKUP + F_ODS_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ODT_ID, DS_CONCEPT_LOOKUP + F_ODT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ID, DS_CONCEPT_LOOKUP + F_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ACTIVE_INDICATOR, DS_CONCEPT_LOOKUP + F_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_CONCEPT_LOOKUP + F_LANGUAGE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_TYPE_ID, DS_CONCEPT_LOOKUP + F_CONCEPT_TYPE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM_ID, DS_CONCEPT_LOOKUP + F_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM, DS_CONCEPT_LOOKUP + F_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION_ID, DS_CONCEPT_LOOKUP + F_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION, DS_CONCEPT_LOOKUP + F_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ABBREVIATION_ID, DS_CONCEPT_LOOKUP + F_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ABBREVIATION, DS_CONCEPT_LOOKUP + F_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE_ID, DS_CONCEPT_LOOKUP + F_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE, DS_CONCEPT_LOOKUP + F_CODE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // E_CONCEPT_RELATION_LOOKUP Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(E_CONCEPT_RELATION_LOOKUP);
        entityDefinition.setDataSourceIdentifier(DS_CONCEPT_RELATION_LOOKUP);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_CONCEPT_RELATION_LOOKUP + F_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ODS_ID, DS_CONCEPT_RELATION_LOOKUP + F_ODS_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ODT_ID, DS_CONCEPT_RELATION_LOOKUP + F_ODT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ID, DS_CONCEPT_RELATION_LOOKUP + F_CONCEPT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ACTIVE_INDICATOR, DS_CONCEPT_RELATION_LOOKUP + F_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_CONCEPT_RELATION_LOOKUP + F_LANGUAGE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_TYPE_ID, DS_CONCEPT_RELATION_LOOKUP + F_CONCEPT_TYPE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM_ID, DS_CONCEPT_RELATION_LOOKUP + F_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM, DS_CONCEPT_RELATION_LOOKUP + F_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION_ID, DS_CONCEPT_RELATION_LOOKUP + F_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION, DS_CONCEPT_RELATION_LOOKUP + F_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ABBREVIATION_ID, DS_CONCEPT_RELATION_LOOKUP + F_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ABBREVIATION, DS_CONCEPT_RELATION_LOOKUP + F_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE_ID, DS_CONCEPT_RELATION_LOOKUP + F_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE, DS_CONCEPT_RELATION_LOOKUP + F_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_GRAPH_ID, DS_CONCEPT_RELATION_LOOKUP + F_CONCEPT_GRAPH_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TAIL_CONCEPT_ID, DS_CONCEPT_RELATION_LOOKUP + F_TAIL_CONCEPT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_HEAD_CONCEPT_ID, DS_CONCEPT_RELATION_LOOKUP + F_HEAD_CONCEPT_ID, false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        entityDefinition = new T8DataEntityResourceDefinition(E_PRESCRIBED_VALUE_LOOKUP);
        entityDefinition.setDataSourceIdentifier(DS_PRESCRIBED_VALUE_LOOKUP);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_VALUE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_PROPERTY_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FIELD_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_TYPE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_DATA_TYPE_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE, DS_PRESCRIBED_VALUE_LOOKUP + F_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LOWER_BOUND_VALUE, DS_PRESCRIBED_VALUE_LOOKUP + F_LOWER_BOUND_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPPER_BOUND_VALUE, DS_PRESCRIBED_VALUE_LOOKUP + F_UPPER_BOUND_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ORG_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ODS_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ODS_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ODT_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ODT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ACTIVE_INDICATOR, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ACTIVE_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_TERM_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_TERM, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ABBREVIATION_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_ABBREVIATION, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_DEFINITION_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_DEFINITION, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_CODE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_CODE, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONCEPT_TYPE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_CONCEPT_TYPE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_TERM_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_TERM, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_ABBREVIATION_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_ABBREVIATION, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_DEFINITION_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_DEFINITION, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_CODE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UOM_CODE, DS_PRESCRIBED_VALUE_LOOKUP + F_UOM_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_TERM_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_TERM_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_TERM, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_TERM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_ABBREVIATION_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_ABBREVIATION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_ABBREVIATION, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_ABBREVIATION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_DEFINITION_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_DEFINITION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_DEFINITION, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_DEFINITION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_CODE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_CODE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_QOM_CODE, DS_PRESCRIBED_VALUE_LOOKUP + F_QOM_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_PRESCRIBED_VALUE_LOOKUP + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STANDARD_INDICATOR, DS_PRESCRIBED_VALUE_LOOKUP + F_STANDARD_INDICATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_APPROVED_INDICATOR, DS_PRESCRIBED_VALUE_LOOKUP + F_APPROVED_INDICATOR, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        return definitions;
    }
}
