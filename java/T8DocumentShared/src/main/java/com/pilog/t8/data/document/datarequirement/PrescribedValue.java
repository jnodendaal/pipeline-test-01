package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class PrescribedValue implements Serializable
{
    private RequirementType requirementType;
    private String valueId;
    private String orgId;
    private String drId;
    private String drInstanceId;
    private String propertyId;
    private String fieldId;
    private String value;
    private String lowerBoundValue;
    private String upperBoundValue;
    private String conceptId;
    private String uomId;
    private String qomId;
    private final List<OldPrescribedValue> oldValues;

    public PrescribedValue(RequirementType requirementType, String valueId)
    {
        this.requirementType = requirementType;
        this.valueId = valueId;
        this.oldValues = new ArrayList<>();
    }

    private PrescribedValue(RecordValue recordValue, String orgId)
    {
        this(recordValue.getRequirementType(), recordValue.getPrescribedValueID());
        this.orgId = orgId;
    }

    public RequirementType getRequirementType()
    {
        return requirementType;
    }

    public void setRequirementType(RequirementType requirementType)
    {
        this.requirementType = requirementType;
    }

    public String getOrganizationID()
    {
        return orgId;
    }

    public void setOrganizationID(String orgID)
    {
        this.orgId = orgID;
    }

    public String getDataRequirementID()
    {
        return drId;
    }

    public void setDataRequirementID(String drID)
    {
        this.drId = drID;
    }

    public String getDataRequirementInstanceID()
    {
        return drInstanceId;
    }

    public void setDataRequirementInstanceID(String drInstanceID)
    {
        this.drInstanceId = drInstanceID;
    }

    public String getPropertyID()
    {
        return propertyId;
    }

    public void setPropertyID(String propertyID)
    {
        this.propertyId = propertyID;
    }

    public String getFieldID()
    {
        return fieldId;
    }

    public void setFieldID(String fieldID)
    {
        this.fieldId = fieldID;
    }

    public String getValueID()
    {
        return valueId;
    }

    public void setValueID(String valueID)
    {
        this.valueId = valueID;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getLowerBoundValue()
    {
        return lowerBoundValue;
    }

    public void setLowerBoundValue(String lowerBoundNumber)
    {
        this.lowerBoundValue = lowerBoundNumber;
    }

    public String getUpperBoundValue()
    {
        return upperBoundValue;
    }

    public void setUpperBoundValue(String upperBoundNumber)
    {
        this.upperBoundValue = upperBoundNumber;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getUnitOfMeasureID()
    {
        return uomId;
    }

    public void setUnitOfMeasureID(String uomID)
    {
        this.uomId = uomID;
    }

    public String getQualifierOfMeasureID()
    {
        return qomId;
    }

    public void setQualifierOfMeasureID(String qomID)
    {
        this.qomId = qomID;
    }

    public List<OldPrescribedValue> getOldValues()
    {
        return new ArrayList<>(oldValues);
    }

    public void setOldValues(Collection<OldPrescribedValue> values)
    {
        this.oldValues.clear();
        if (values != null)
        {
            this.oldValues.addAll(values);
        }
    }

    public void addOldValue(OldPrescribedValue oldValue)
    {
        this.oldValues.add(oldValue);
    }

    public boolean isEquivalentValue(PrescribedValue other)
    {
        if (other == null) return false;
        if (this.requirementType != other.requirementType) return false;
        if (!Objects.equals(this.drId, other.drId)) return false;
        if (!Objects.equals(this.propertyId, other.propertyId)) return false;
        if (!Objects.equals(this.fieldId, other.fieldId)) return false;
        if (!Objects.equals(this.value, other.value)) return false;
        if (!Objects.equals(this.lowerBoundValue, other.lowerBoundValue)) return false;
        if (!Objects.equals(this.upperBoundValue, other.upperBoundValue)) return false;
        if (!Objects.equals(this.conceptId, other.conceptId)) return false;
        if (!Objects.equals(this.uomId, other.uomId)) return false;
        return Objects.equals(this.qomId, other.qomId);
    }

    public static PrescribedValue fromRecordValue(RecordValue recordValue, String orgId)
    {
        PrescribedValue prescribedValue;

        prescribedValue = new PrescribedValue(recordValue, orgId);
        prescribedValue.setDataRequirementID(recordValue.getDataRequirementID());
        prescribedValue.setDataRequirementInstanceID(recordValue.getDataRequirementInstanceID());
        prescribedValue.setFieldID(recordValue.getFieldID());
        prescribedValue.setPropertyID(recordValue.getPropertyID());

        switch (prescribedValue.getRequirementType())
        {
            case CONTROLLED_CONCEPT:
            {
                ControlledConcept value;

                value = (ControlledConcept)recordValue;
                if (value.getConceptId() == null) return null;
                prescribedValue.setConceptID(value.getConceptId());
                break;
            }
            case QUALIFIER_OF_MEASURE:
            {
                QualifierOfMeasure value;

                value = (QualifierOfMeasure)recordValue;
                if (value.getValue()== null) return null;
                prescribedValue.setQualifierOfMeasureID(value.getValue());
                break;
            }
            case UNIT_OF_MEASURE:
            {
                UnitOfMeasure value;

                value = (UnitOfMeasure)recordValue;
                if (value.getValue()== null) return null;
                prescribedValue.setUnitOfMeasureID(value.getValue());
                break;
            }
            case MEASURED_NUMBER:
            {
                MeasuredNumber measuredNumber;

                measuredNumber = (MeasuredNumber)recordValue;
                prescribedValue.setValue(measuredNumber.getNumber());
                prescribedValue.setUnitOfMeasureID(measuredNumber.getUomId());
                prescribedValue.setQualifierOfMeasureID(measuredNumber.getQomId());
                break;
            }
            case MEASURED_RANGE:
            {
                MeasuredRange measuredRange;

                measuredRange = (MeasuredRange)recordValue;
                prescribedValue.setLowerBoundValue(measuredRange.getLowerBoundNumber());
                prescribedValue.setUpperBoundValue(measuredRange.getUpperBoundNumber());
                prescribedValue.setQualifierOfMeasureID(measuredRange.getQomId());
                prescribedValue.setUnitOfMeasureID(measuredRange.getUomId());
                break;
            }
            case NUMBER:
            case LOWER_BOUND_NUMBER:
            case UPPER_BOUND_NUMBER:
            case INTEGER_TYPE:
            case REAL_TYPE:
            case STRING_TYPE:
            {
                if (recordValue.getValue() == null) return null;
                prescribedValue.setValue(recordValue.getValue());
                break;
            }
            default:
            {
                throw new IllegalStateException("Invalid record value type: " + prescribedValue.getRequirementType());
            }
        }

        return prescribedValue;
    }
}
