package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.MergeConflictPolicy;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordValueMerger;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordValueMergerDefinition extends T8DataRecordValueMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_VALUE_MERGER_DEFAULT";
    public static final String DISPLAY_NAME = "Default Value Merger";
    public static final String DESCRIPTION = "A definition that specifies merging rules applicable to a data record value.";
    public enum Datum {MERGE_CONFLICT_POLICY};
    // -------- Definition Meta-Data -------- //
    
    public T8DefaultDataRecordValueMergerDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MERGE_CONFLICT_POLICY.toString(), "Merge Conflict Policy", "The method of determining which value (source or destination) to add to the destination record in a merge operation when a destination value as well as a source value is present.", MergeConflictPolicy.USE_DESTINATION_DATA_ADD_FFT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MERGE_CONFLICT_POLICY.toString().equals(datumIdentifier)) return createStringOptions(MergeConflictPolicy.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, inputParameters);
    }
    
    @Override
    public T8DataRecordValueMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordValueMerger").getConstructor(this.getClass(), T8DataRecordMergeContext.class);
            return (T8DataRecordValueMerger)constructor.newInstance(this, mergeContext);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Value Merger.", e);
        }
    }
    
    public MergeConflictPolicy getMergeConflictPolicy()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.MERGE_CONFLICT_POLICY.toString());
        return value != null ? MergeConflictPolicy.valueOf(value) : MergeConflictPolicy.USE_DESTINATION_DATA_ADD_FFT;
    }
    
    public void setMergeConflictPolicy(MergeConflictPolicy policy)
    {
        setDefinitionDatum(Datum.MERGE_CONFLICT_POLICY.toString(), policy.toString());
    }
}
