package com.pilog.t8.data.alteration.type;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.ontology.T8OntologyTerm;

/**
 * @author Bouwer du Preez
 */
public class T8CompleteTermAlteration implements T8DataAlteration
{
    private final String packageIid;
    private T8OntologyTerm term;
    private T8DataAlterationMethod method;
    private int step;

    public static final String ALTERATION_ID = "@ALTERATION_COMPLETE_TERM";

    public T8CompleteTermAlteration(String packageIid, int step, T8OntologyTerm term, T8DataAlterationMethod method)
    {
        this.packageIid = packageIid;
        this.step = step;
        this.term = term;
        this.method = method;
    }

    @Override
    public T8DataAlterationMethod getAlterationMethod()
    {
        return method;
    }

    public void setAlterationMethod(T8DataAlterationMethod method)
    {
        this.method = method;
    }

    public T8OntologyTerm getTerm()
    {
        return term;
    }

    public void setTerm(T8OntologyTerm term)
    {
        this.term = term;
    }

    @Override
    public String getAlterationId()
    {
        return ALTERATION_ID;
    }

    @Override
    public String getPackageIid()
    {
        return packageIid;
    }

    @Override
    public int getStep()
    {
        return step;
    }

    public void setStep(int step)
    {
        this.step = step;
    }

    @Override
    public JsonObject serialize()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
