package com.pilog.t8.definition.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.datatype.T8DtDataFileSaveResult;
import com.pilog.t8.datatype.T8DtDataFileValidationReport;
import com.pilog.t8.datatype.T8DtDataRecord;
import com.pilog.t8.datatype.T8DtFileDetails;
import com.pilog.t8.datatype.T8DtRecordValue;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_REC_REFRESH_DATA_OBJECTS = "@OP_API_REC_REFRESH_DATA_OBJECTS";
    public static final String OPERATION_API_REC_RETRIEVE_FILES = "@OP_API_REC_RETRIEVE_FILES";
    public static final String OPERATION_API_REC_RETRIEVE_FILES_BY_CONTENT_RECORD = "@OP_API_REC_RETRIEVE_FILES_BY_CONTENT_RECORD";
    public static final String OPERATION_API_REC_RETRIEVE_RECORDS = "@OP_API_REC_RETRIEVE_RECORDS";
    public static final String OPERATION_API_REC_RETRIEVE_VALUE_SUGGESTIONS = "@OP_API_REC_RETRIEVE_VALUE_SUGGESTIONS";
    public static final String OPERATION_API_REC_RETRIEVE_VALUE_OPTIONS = "@OP_API_REC_RETRIEVE_VALUE_OPTIONS";
    public static final String OPERATION_API_REC_RETRIEVE_DR_INSTANCE_OPTIONS = "@OP_API_REC_RETRIEVE_DR_INSTANCE_OPTIONS";
    public static final String OPERATION_API_REC_RETRIEVE_ATTACHMENT_DETAILS = "@OP_API_REC_RETRIEVE_ATTACHMENT_DETAILS";
    public static final String OPERATION_API_REC_RETRIEVE_RECORD_ATTACHMENT_DETAILS = "@OP_API_REC_RETRIEVE_RECORD_ATTACHMENT_DETAILS";
    public static final String OPERATION_API_REC_RETRIEVE_RECORD_DR_INSTANCE_ID = "@OP_API_REC_RETRIEVE_RECORD_DR_INSTANCE_ID";
    public static final String OPERATION_API_REC_RESOLVE_CONCEPT_ID = "@OP_API_REC_RESOLVE_CONCEPT_ID";
    public static final String OPERATION_API_REC_CHANGE_RECORD_DR_INSTANCE = "@OP_API_REC_CHANGE_RECORD_DR_INSTANCE";
    public static final String OPERATION_API_REC_CREATE_RECORD = "@OP_API_REC_CREATE_RECORD";
    public static final String OPERATION_API_REC_SAVE_FILE_STATE_CHANGE = "@OP_API_REC_SAVE_FILE_STATE_CHANGE";
    public static final String OPERATION_API_REC_VALIDATE_FILE_STATE_CHANGE = "@OP_API_REC_VALIDATE_FILE_STATE_CHANGE";
    public static final String OPERATION_API_REC_EXPORT_ATTACHMENT = "@OP_API_REC_EXPORT_ATTACHMENT";

    public static final String PARAMETER_CONCEPT_ID = "$P_CONCEPT_ID";
    public static final String PARAMETER_PATH = "$P_PATH";
    public static final String PARAMETER_FILENAME = "$P_FILENAME";
    public static final String PARAMETER_FILE_CONTEXT_ID = "$P_FILE_CONTEXT_ID";
    public static final String PARAMETER_FILE_CONTEXT_IID = "$P_FILE_CONTEXT_IID";
    public static final String PARAMETER_FILE_IDS = "$P_FILE_IDS";
    public static final String PARAMETER_RECORD_IDS = "$P_RECORD_IDS";
    public static final String PARAMETER_DR_IIDS = "$P_DR_IIDS";
    public static final String PARAMETER_DATA_OBJECT_IDS = "$P_DATA_OBJECT_IDS";
    public static final String PARAMETER_FILE_LIST = "$P_FILE_LIST";
    public static final String PARAMETER_RECORD_LIST = "$P_RECORD_LIST";
    public static final String PARAMETER_ACCESS_CONTEXT = "$P_ACCESS_CONTEXT";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_INCLUDE_ONTOLOGY = "$P_INCLUDE_ONTOLOGY";
    public static final String PARAMETER_INCLUDE_TERMINOLOGY = "$P_INCLUDE_TERMINOLOGY";
    public static final String PARAMETER_INCLUDE_DESCRIPTIONS = "$P_INCLUDE_DESCRIPTIONS";
    public static final String PARAMETER_INCLUDE_ATTACHMENT_DETAILS = "$P_INCLUDE_ATTACHMENT_DETAILS";
    public static final String PARAMETER_INCLUDE_DR_COMMENTS = "$P_INCLUDE_DR_COMMENTS";
    public static final String PARAMETER_INCLUDE_PARTICLE_SETTINGS = "$P_INCLUDE_PARTICLE_SETTINGS";
    public static final String PARAMETER_INCLUDE_DESCENDANT_RECORDS = "$P_INCLUDE_DESCENDANT_RECORDS";
    public static final String PARAMETER_ATTACHMENT_ID = "$P_ATTACHMENT_ID";
    public static final String PARAMETER_RECORD_ID = "$P_RECORD_ID";
    public static final String PARAMETER_DR_INSTANCE_ID = "$P_DR_INSTANCE_ID";
    public static final String PARAMETER_PROPERTY_ID = "$P_PROPERTY_ID";
    public static final String PARAMETER_FIELD_ID = "$P_FIELD_ID";
    public static final String PARAMETER_FILE = "$P_FILE";
    public static final String PARAMETER_RECORD = "$P_RECORD";
    public static final String PARAMETER_TARGET_RECORD_ID = "$P_TARGET_RECORD_ID";
    public static final String PARAMETER_TARGET_PROPERTY_ID = "$P_TARGET_PROPERTY_ID";
    public static final String PARAMETER_TARGET_FIELD_ID = "$P_TARGET_FIELD_ID";
    public static final String PARAMETER_TARGET_DATA_TYPE_ID = "$P_TARGET_DATA_TYPE_ID";
    public static final String PARAMETER_SEARCH_PREFIX = "$P_SEARCH_PREFIX";
    public static final String PARAMETER_MATCH_STRING = "$P_MATCH_STRING";
    public static final String PARAMETER_CONCEPT_ELEMENT_TYPES = "$P_CONCEPT_ELEMENT_TYPES";
    public static final String PARAMETER_RECORD_VALUES = "$P_RECORD_VALUES";
    public static final String PARAMETER_PAGE_OFFSET = "$P_PAGE_OFFSET";
    public static final String PARAMETER_PAGE_SIZE = "$P_PAGE_SIZE";
    public static final String PARAMETER_CONCEPT_TERMINOLOGY_LIST = "$P_CONCEPT_TERMINOLOGY_LIST";
    public static final String PARAMETER_ATTACHMENT_DETAILS = "$P_ATTACHMENT_DETAILS";
    public static final String PARAMETER_SAVE_RESULT = "$P_SAVE_RESULT";
    public static final String PARAMETER_VALIDATION_REPORT = "$P_VALIDATION_REPORT";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            T8DefinitionManager definitionManager;
            List<T8Definition> definitions;
            T8DataType dtRecordValueList;

            definitionManager = serverContext.getDefinitionManager();
            dtRecordValueList = new T8DtList(definitionManager, new T8DtRecordValue(definitionManager));

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_FILES);
            definition.setMetaDisplayName("Retrieve Data Files");
            definition.setMetaDescription("Retrieves the specified Data Files and the required associated data.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveFiles");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_IDS, "File ID List", "The list of concept ID's of the Data Files to retrieve.", new T8DtList(T8DataType.GUID)));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ACCESS_CONTEXT, "Access Identifier", "The access Identifier applicable to the retrieved data.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The terminology language to use.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology", "A boolean flag to indicate whether or not concept ontology for the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "A boolean flag to indicate whether or not concept terminology for the content of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DESCRIPTIONS, "Include Descriptions", "A boolean flag to indicate whether or not the descriptions of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, "Include Attachment Details", "A boolean flag to indicate whether or not the attachment details of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DR_COMMENTS, "Include DR Comments", "A boolean flag to indicate whether or not the DR Comments of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_PARTICLE_SETTINGS, "Include Particle Settings", "A boolean flag to indicate whether or not the Data String Particle Settings of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_LIST, "Data File List", "The list of complete Data Record objects retrieved, containing the requested associated data.", new T8DtList(new T8DtDataRecord())));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_FILES_BY_CONTENT_RECORD);
            definition.setMetaDisplayName("Retrieve Data Files By Content Records");
            definition.setMetaDescription("Retrieves the Data Files to which the specified list of Records belong.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveFilesByContentRecord");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_IDS, "Record ID List", "The list of concept ID's of the Data Records for which parent files will be retrieved.", new T8DtList(T8DataType.GUID)));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ACCESS_CONTEXT, "Access Identifier", "The access Identifier applicable to the retrieved data.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The terminology language to use.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology", "A boolean flag to indicate whether or not concept ontology for the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "A boolean flag to indicate whether or not concept terminology for the content of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DESCRIPTIONS, "Include Descriptions", "A boolean flag to indicate whether or not the descriptions of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, "Include Attachment Details", "A boolean flag to indicate whether or not the attachment details of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DR_COMMENTS, "Include DR Comments", "A boolean flag to indicate whether or not the DR Comments of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_PARTICLE_SETTINGS, "Include Particle Settings", "A boolean flag to indicate whether or not the Data String Particle Settings of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_LIST, "Data File List", "The list of complete Data Record objects retrieved, containing the requested associated data.", new T8DtList(new T8DtDataRecord())));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_RECORDS);
            definition.setMetaDisplayName("Retrieve Data Records");
            definition.setMetaDescription("Retrieves the specified Data Records and the required associated data.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveRecords");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_IDS, "Record ID List", "The list of concept ID's of the Data Records to retrieve.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ACCESS_CONTEXT, "Access Identifier", "The access Identifier applicable to the retrieved data.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The terminology language to use.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology", "A boolean flag to indicate whether or not concept ontology for the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "A boolean flag to indicate whether or not concept terminology for the content of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DESCRIPTIONS, "Include Descriptions", "A boolean flag to indicate whether or not the descriptions of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, "Include Attachment Details", "A boolean flag to indicate whether or not the attachment details of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DR_COMMENTS, "Include DR Comments", "A boolean flag to indicate whether or not the DR Comments of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_PARTICLE_SETTINGS, "Include Particle Settings", "A boolean flag to indicate whether or not the Data String Particle Settings of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DESCENDANT_RECORDS, "Include Descendant Records", "A boolean flag to indicate whether or not the descendant records of the requested Data Record should be retrieved.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_LIST, "Data Record List", "The list of complete Data Record objects retrieved, containing the requested associated data.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_CREATE_RECORD);
            definition.setMetaDisplayName("Create Data Record");
            definition.setMetaDescription("Creates a new Data Record based on the specified Data Requirement Intance.  If required, the new document is persisted before being returned by this operation.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiCreateRecord");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE, "Root Data Record", "The root data record is required in the event of a dependent record creation.", new T8DtDataRecord(), true));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The concept ID of the Data Record to create.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PROPERTY_ID, "Property ID", "The Property ID to which the record will be linked if dependent.", T8DataType.GUID, true));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FIELD_ID, "Field ID", "The Field ID for a more complex property where the new record should be linked to a specific field of the property.", T8DataType.GUID, true));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_INSTANCE_ID, "Data Requirement Instance ID", "The concept ID of the Data Requirement Instance on which the new record will be based.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The terminology language to use (if terminology is to be included).", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "A boolean flag to indicate whether or not concept terminology for the newly created Data Record should be retrieved before it is returned.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DR_COMMENTS, "Include DR Comments", "A boolean flag to indicate whether or not the DR Comments of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD, "Data Record", "The complete Data Record object created.", new T8DtDataRecord()));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_CHANGE_RECORD_DR_INSTANCE);
            definition.setMetaDisplayName("Change Data Record Data Requirement Instance");
            definition.setMetaDescription("Changes the Data Requirement Instance of the specified data record and returns the record with all changes applied.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiChangeRecordDrInstance");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE, "Data File", "The Root Data Record to use as context for the DR Instance Change.", new T8DtDataRecord()));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The concept ID of the Data Record on which to apply the Data Requirement Instance Change.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_INSTANCE_ID, "Data Requirement Instance ID", "The concept ID of the Data Requirement Instance to which the specified record will be changed.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The terminology language to use.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ACCESS_CONTEXT, "Access Identifier", "The Identifier of the Data Access rule set that will be used when applying the DR Instance Change.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology", "A boolean flag to indicate whether or not concept ontology for the affected Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "A boolean flag to indicate whether or not concept terminology for the content of the affected Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DESCRIPTIONS, "Include Descriptions", "A boolean flag to indicate whether or not the descriptions of the affected Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, "Include Attachment Details", "A boolean flag to indicate whether or not the attachment details of the affected Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DR_COMMENTS, "Include DR Comments", "A boolean flag to indicate whether or not the DR Comments of the requested Data Records should be retrieved.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE, "Data Record", "The complete Data Record object retrieved, with the applied Data Requirement Instance change..", new T8DtDataRecord()));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_SAVE_FILE_STATE_CHANGE);
            definition.setMetaDisplayName("Save File State Change");
            definition.setMetaDescription("Saves the last state change on the supplied file.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiSaveFileStateChange");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE, "Data File", "The Data File to validate.", new T8DtDataRecord()));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SAVE_RESULT, "Save Result", "The result of the save operation.", new T8DtDataFileSaveResult()));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_VALIDATE_FILE_STATE_CHANGE);
            definition.setMetaDisplayName("Validate File State Change");
            definition.setMetaDescription("Validates the last state change on the supplied file.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiValidateFileStateChange");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE, "Data File", "The Data File to validate.", new T8DtDataRecord()));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_VALIDATION_REPORT, "Validation Report", "The Data Record validation report.", new T8DtDataFileValidationReport()));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_VALUE_SUGGESTIONS);
            definition.setMetaDisplayName("Retrieve Value Suggestions");
            definition.setMetaDescription("Get value suggestions for the specified search criteria. Suggestions are limited to 20 if not specified.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveValueSuggestions");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD, "Data Record", "The context record structure within which values will be suggested.", new T8DtDataRecord()));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_PREFIX, "Search Prefix", "The prefix to use as search criterion when retrieving suggestions.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page of values to return.", T8DataType.INTEGER, true));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The size of the page of values to return.", T8DataType.INTEGER, true));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_VALUES, "Record Values", "A list of suggested Record Values for the specified input.", dtRecordValueList));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_VALUE_OPTIONS);
            definition.setMetaDisplayName("Retrieve Value Options");
            definition.setMetaDescription("Retrieve the value options for the specified record property/field.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveValueOptions");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD, "Data Record", "The context record structure within which values will be suggested.", new T8DtDataRecord()));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page of values to return.", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The size of the page of values to return.", T8DataType.INTEGER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_VALUES, "Record Values", "A list of suggested Record Values for the specified input.", dtRecordValueList));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RESOLVE_CONCEPT_ID);
            definition.setMetaDisplayName("Resolve Concept ID");
            definition.setMetaDescription("Uses the supplied string value to resolve the matching concept applicable to the record context, matching on the specified concept element types.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiResolveConceptId");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD, "Data Record", "The context record structure within which values will be suggested.", new T8DtDataRecord()));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_STRING, "String", "The string to use when finding a matching concept.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ELEMENT_TYPES, "Concept Element Types", "The sequence of concept element types to use when matching the input string to concepts in the database.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept ID", "The concept ID resolved for the supplied context.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_DR_INSTANCE_OPTIONS);
            definition.setMetaDisplayName("Retrieve DR Instance Options");
            definition.setMetaDescription("Retrieve the DR Instance options for the specified record property/field.  These options determine the types of document references that may be added to the target property/field.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveDrInstanceOptions");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD, "Data Record", "The context record structure within which values will be suggested.", new T8DtDataRecord()));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_RECORD_ID, "Target Record ID", "The ID of the target record for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_PROPERTY_ID, "Target Property ID", "The ID of the target record property for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_FIELD_ID, "Target Field ID", "The ID of the target record property field for which suggestions will be provided.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TARGET_DATA_TYPE_ID, "Target Data Type ID", "The ID of the target data type for which suggestions will be provided.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_OFFSET, "Page Offset", "The offset of the page of values to return.", T8DataType.INTEGER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PAGE_SIZE, "Page Size", "The size of the page of values to return.", T8DataType.INTEGER));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_TERMINOLOGY_LIST, "Concept Terminology List", "A list of DR Instance concept terminologies.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_RECORD_ATTACHMENT_DETAILS);
            definition.setMetaDisplayName("Retrieve Data Record Attachment Details");
            definition.setMetaDescription("Retrieves the specified Data Record Attachment details.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveRecordAttachmentDetails");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The id of the Data Record ID for which to retrieve attachment details.", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ATTACHMENT_DETAILS, "Data Record Attachment File Details Map", "The map of Data Record attachment details retrieved (file details).", T8DataType.MAP));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_ATTACHMENT_DETAILS);
            definition.setMetaDisplayName("Retrieve Attachment Details");
            definition.setMetaDescription("Retrieves the specified Attachment details.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveAttachmentDetails");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ATTACHMENT_ID, "Attachment ID", "The id of the attachment for which to retrieve details.", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ATTACHMENT_DETAILS, "Attachment File Details", "The attachment details retrieved (file details).", new T8DtFileDetails(null)));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_RETRIEVE_RECORD_DR_INSTANCE_ID);
            definition.setMetaDisplayName("Retrieve Data Record Data Requirement Instance Id");
            definition.setMetaDescription("Retrieves the id of the Data Requirement Instance on which the specified Data Record Attachment is based.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRetrieveRecordDrInstanceId");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record ID", "The id of the Data Record Id for which to retrieve attachment details.", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_INSTANCE_ID, "Data Requirement Instance Id", "The id of the Data Requirement Instance on which the specified record is based.", T8DataType.GUID));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_EXPORT_ATTACHMENT);
            definition.setMetaDisplayName("Export Data Record Attachment");
            definition.setMetaDescription("Retrieves a specified data record attachment and exports it to the requested file location.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiExportAttachment");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ATTACHMENT_ID, "Attachment Id", "The ID of the attachment to export.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_CONTEXT_IID, "File Context Iid", "The file context instance to which the attachment file will be exported.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILE_CONTEXT_ID, "File Context Id", "The file context to which the attachment will be exported.", T8DataType.DEFINITION_IDENTIFIER));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PATH, "Path", "The directory path in the specified file context to which the attachment file will be exported.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILENAME, "Filename", "The filename to use when exporting the attachment.  If this parameter is null, the default filename of the attachment (as it was imported) will be used.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ATTACHMENT_DETAILS, "Attachment Details", "The details of the exported file.", new T8DtFileDetails(null)));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_REC_REFRESH_DATA_OBJECTS);
            definition.setMetaDisplayName("Refresh Data Objects");
            definition.setMetaDescription("Refreshes all Data Objects based on the specified data files and their contents.");
            definition.setClassName("com.pilog.t8.api.T8DataRecordApiOperations$ApiRefreshDataObjects");
            definition.setContainerManagedTransaction(false);
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_IIDS, "DR Instance ID List", "The list of DR Instance ID's to use as a filter for Data Files to process.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_IDS, "Record ID List", "The list of Record ID's to use as a filter for Data Files to process.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_OBJECT_IDS, "View ID List", "The list of ID's of views to refresh.", T8DataType.LIST));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
