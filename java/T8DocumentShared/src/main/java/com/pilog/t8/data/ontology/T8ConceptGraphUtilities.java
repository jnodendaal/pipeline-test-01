package com.pilog.t8.data.ontology;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptGraphUtilities
{
    /**
     * This method checks whether or not all of the paths in the supplied list
     * are part of a tree structure i.e. that no two paths lead to the same
     * head concept from different tail concepts as this would mean a node has
     * more than one parent.
     * @param edges The edges to check.
     * @return true if all paths are part of a tree structure.
     */
    public static final boolean treePaths(List<T8ConceptPair> edges)
    {
        List<String> headConceptSet;

        headConceptSet = new ArrayList<String>();
        for (T8ConceptPair edge : edges)
        {
            String headConceptID;

            headConceptID = edge.getRightConceptId();
            if (headConceptSet.contains(headConceptID)) return false;
            else headConceptSet.add(headConceptID);
        }

        return true;
    }
}
