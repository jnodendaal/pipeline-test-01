package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredNumberAccessLayer extends ValueAccessLayer
{
    private final UnitOfMeasureAccessLayer uomAccessLayer;

    public MeasuredNumberAccessLayer()
    {
        super(RequirementType.MEASURED_NUMBER);
        this.uomAccessLayer = new UnitOfMeasureAccessLayer();
    }

    public UnitOfMeasureAccessLayer getUomAccessLayer()
    {
        return uomAccessLayer;
    }

    public void setUomAccess(UnitOfMeasureAccessLayer newUomAccess)
    {
        this.uomAccessLayer.setAccess(newUomAccess);
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer valueAccess)
    {
        if (valueAccess instanceof MeasuredNumberAccessLayer)
        {
            UnitOfMeasureAccessLayer mnUomAccess;
            MeasuredNumberAccessLayer mnAccess;

            mnAccess = (MeasuredNumberAccessLayer)valueAccess;
            mnUomAccess = mnAccess.getUomAccessLayer();
            if (uomAccessLayer == null)
            {
                if (mnUomAccess != null) return false;
                else return true;
            }
            else if (!uomAccessLayer.isValueAccessEquivalent(mnUomAccess)) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public ValueAccessLayer copy()
    {
        MeasuredNumberAccessLayer copy;

        copy = new MeasuredNumberAccessLayer();
        copy.setUomAccess(uomAccessLayer);
        return copy;
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
        MeasuredNumberAccessLayer newMnAccess;

        newMnAccess = (MeasuredNumberAccessLayer)newAccess;
        uomAccessLayer.setAccess(newMnAccess.getUomAccessLayer());
    }
}
