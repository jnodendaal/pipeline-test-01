package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordMetaDataType;
import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public interface T8DataFileOutputStream
{
    public void setEnabled(boolean enabled);
    public void write(DataRecord dataFile, T8DataFileAlteration alteration, Collection<RecordMetaDataType> metaDataTypes) throws Exception;
}
