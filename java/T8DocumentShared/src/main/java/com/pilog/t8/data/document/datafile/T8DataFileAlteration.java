package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.ontology.T8ConceptAlteration;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileAlteration implements Serializable
{
    private DataRecord dataFile;
    private final T8ConceptAlteration conceptAlteration;
    private final List<DataRecord> insertedRecords;
    private final List<DataRecord> updatedRecords; // Contains records from the toState that have been updated.
    private final List<DataRecord> deletedRecords;
    private final List<T8DataStringParticleSettings> insertedParticleSettings;
    private final List<T8DataStringParticleSettings> updatedParticleSettings;
    private final List<T8DataStringParticleSettings> deletedParticleSettings;

    public T8DataFileAlteration(DataRecord dataFile)
    {
        this.dataFile = dataFile;
        this.conceptAlteration = new T8ConceptAlteration();
        this.insertedRecords = new ArrayList<DataRecord>();
        this.updatedRecords = new ArrayList<DataRecord>();
        this.deletedRecords = new ArrayList<DataRecord>();
        this.insertedParticleSettings = new ArrayList<T8DataStringParticleSettings>();
        this.updatedParticleSettings = new ArrayList<T8DataStringParticleSettings>();
        this.deletedParticleSettings = new ArrayList<T8DataStringParticleSettings>();
    }

    public DataRecord getDataFile()
    {
        return dataFile;
    }

    public void setDataFile(DataRecord dataFile)
    {
        this.dataFile = dataFile;
    }

    public void clear()
    {
        this.conceptAlteration.clear();
        this.insertedRecords.clear();
        this.updatedRecords.clear();
        this.deletedRecords.clear();
        this.insertedParticleSettings.clear();
        this.updatedParticleSettings.clear();
        this.deletedParticleSettings.clear();
    }

    public void add(T8DataFileAlteration alteration)
    {
        insertedRecords.addAll(alteration.getInsertedRecords());
        updatedRecords.addAll(alteration.getUpdatedRecords());
        deletedRecords.addAll(alteration.getDeletedRecords());
        insertedParticleSettings.addAll(alteration.getInsertedParticleSettings());
        updatedParticleSettings.addAll(alteration.getUpdatedParticleSettings());
        deletedParticleSettings.addAll(alteration.getDeletedParticleSettings());
    }

    public T8DataFileAlteration copy()
    {
        T8DataFileAlteration copy;

        copy = new T8DataFileAlteration(dataFile);
        copy.insertedRecords.addAll(insertedRecords);
        copy.updatedRecords.addAll(updatedRecords);
        copy.deletedRecords.addAll(deletedRecords);
        copy.insertedParticleSettings.addAll(insertedParticleSettings);
        copy.updatedParticleSettings.addAll(updatedParticleSettings);
        copy.deletedParticleSettings.addAll(deletedParticleSettings);
        return copy;
    }

    public T8ConceptAlteration getConceptAlteration()
    {
        return conceptAlteration;
    }

    public void addConceptAlteration(T8ConceptAlteration conceptAlteration)
    {
        this.conceptAlteration.add(conceptAlteration);
    }

    public List<DataRecord> getInsertedRecords()
    {
        return new ArrayList<DataRecord>(insertedRecords);
    }

    public void addInsertedRecord(DataRecord insertedRecord)
    {
        insertedRecords.add(insertedRecord);
    }

    public void addInsertedRecords(Collection<DataRecord> insertedRecordCollection)
    {
        if (insertedRecordCollection != null)
        {
            insertedRecords.addAll(insertedRecordCollection);
        }
    }

    public boolean isInsert(String recordId)
    {
        for (DataRecord insertedRecord : insertedRecords)
        {
            if (insertedRecord.getID().equals(recordId))
            {
                return true;
            }
        }

        return false;
    }

    public Set<String> getUpdatedRecordIds()
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (DataRecord record : updatedRecords)
        {
            idSet.add(record.getID());
        }

        return idSet;
    }

    /**
     * Returns the list of toState records that have been updated.
     * @return The list of toState records that have been updated.
     */
    public List<DataRecord> getUpdatedRecords()
    {
        return new ArrayList<DataRecord>(updatedRecords);
    }

    public void addUpdatedRecord(DataRecord updatedRecord)
    {
        updatedRecords.add(updatedRecord);
    }

    public void addUpdatedRecords(Collection<DataRecord> updatedRecordCollection)
    {
        if (updatedRecordCollection != null)
        {
            updatedRecords.addAll(updatedRecordCollection);
        }
    }

    public boolean isUpdate(String recordId)
    {
        for (DataRecord updatedRecord : updatedRecords)
        {
            if (updatedRecord.getID().equals(recordId))
            {
                return true;
            }
        }

        return false;
    }

    public List<DataRecord> getDeletedRecords()
    {
        return new ArrayList<DataRecord>(deletedRecords);
    }

    public void addDeletedRecord(DataRecord deletedRecord)
    {
        deletedRecords.add(deletedRecord);
    }

    public void addDeletedRecords(Collection<DataRecord> deletedRecordCollection)
    {
        if (deletedRecordCollection != null)
        {
            deletedRecords.addAll(deletedRecordCollection);
        }
    }

    public boolean isDeletion(String recordId)
    {
        for (DataRecord deletedRecord : deletedRecords)
        {
            if (deletedRecord.getID().equals(recordId))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a list of all inserted and updated records in this alteration.
     * This list will first contain all of the inserted records and then all
     * of the updated records as this sequence is required by API's that handle
     * record changes and need to persist those changes in a logical and
     * chronological order.
     * @return The list of all inserted and updated records in this alteration.
     */
    public List<DataRecord> getInsertedAndUpdatedRecords()
    {
        List<DataRecord> list;

        list = new ArrayList<DataRecord>();
        for (DataRecord insertedRecord : insertedRecords)
        {
            list.add(insertedRecord);
        }

        for (DataRecord updatedRecord : updatedRecords)
        {
            list.add(updatedRecord);
        }

        return list;
    }

    public List<String> getInsertedAndUpdatedRecordIdList()
    {
        List<String> list;

        list = new ArrayList<String>();
        for (DataRecord insertedRecord : insertedRecords)
        {
            list.add(insertedRecord.getID());
        }

        for (DataRecord updatedRecord : updatedRecords)
        {
            list.add(updatedRecord.getID());
        }

        return list;
    }

    /**
     * Returns the set of record id's that are not inserts but do have registered
     * alterations to their concept ontology.
     * @return The set of record id's that are not inserts but do have registered
     * alterations to their concept ontology.
     */
    public Set<String> getOntologyUpdatedRecordIdSet()
    {
        Set<String> idSet;

        // From the concept alteration, get the id's of all concepts that have content alterations registered.
        idSet = conceptAlteration.getContentAlteredConceptIdSet();

        // From the list of concept id's, retain only those that are record id's in the target data file.
        idSet.retainAll(dataFile.getDataRecordIDSet());

        // Return the final set.
        return idSet;
    }

    public boolean containsInsertedParticleSettings(T8DataStringParticleSettings settings)
    {
        return insertedParticleSettings.contains(settings);
    }

    public List<T8DataStringParticleSettings> getInsertedParticleSettings()
    {
        return new ArrayList<T8DataStringParticleSettings>(insertedParticleSettings);
    }

    public void addInsertedParticleSettings(List<T8DataStringParticleSettings> particleSettings)
    {
        insertedParticleSettings.addAll(particleSettings);
    }

    public void addInsertedParticleSettings(T8DataStringParticleSettings particleSettings)
    {
        insertedParticleSettings.add(particleSettings);
    }

    public boolean removeInsertedParticleSettings(T8DataStringParticleSettings settings)
    {
        return insertedParticleSettings.remove(settings);
    }

    public boolean containsUpdatedParticleSettings(T8DataStringParticleSettings settings)
    {
        return updatedParticleSettings.contains(settings);
    }

    public List<T8DataStringParticleSettings> getUpdatedParticleSettings()
    {
        return new ArrayList<T8DataStringParticleSettings>(updatedParticleSettings);
    }

    public void addUpdatedParticleSettings(List<T8DataStringParticleSettings> particleSettings)
    {
        updatedParticleSettings.addAll(particleSettings);
    }

    public void addUpdatedParticleSettings(T8DataStringParticleSettings particleSettings)
    {
        updatedParticleSettings.add(particleSettings);
    }

    public boolean removeUpdatedParticleSettings(T8DataStringParticleSettings settings)
    {
        return updatedParticleSettings.remove(settings);
    }

    public boolean containsDeletedParticleSettings(T8DataStringParticleSettings settings)
    {
        return deletedParticleSettings.contains(settings);
    }

    public List<T8DataStringParticleSettings> getDeletedParticleSettings()
    {
        return new ArrayList<T8DataStringParticleSettings>(deletedParticleSettings);
    }

    public void addDeletedParticleSettings(List<T8DataStringParticleSettings> particleSettings)
    {
        deletedParticleSettings.addAll(particleSettings);
    }

    public void addDeletedParticleSettings(T8DataStringParticleSettings particleSettings)
    {
        deletedParticleSettings.add(particleSettings);
    }

    public boolean removeDeletedParticleSettings(T8DataStringParticleSettings settings)
    {
        return deletedParticleSettings.remove(settings);
    }

    public static final T8DataFileAlteration getNormalizedDataFileAlteration(DataRecord fromStateFile, DataRecord toStateFile)
    {
        // Normalize the fromState if supplied.
        if (fromStateFile != null)
        {
            fromStateFile.normalize();
        }

        // Normalize the toState if supplied.
        if (toStateFile != null)
        {
            toStateFile.normalize();
        }

        // Create an alteration from the differences between the two states and save the changes.
        return getDataFileAlteration(fromStateFile, toStateFile);
    }

    public static final T8DataFileAlteration getDataFileAlteration(DataRecord fromStateFile, DataRecord toStateFile)
    {
        if (toStateFile == null)
        {
            T8ConceptAlteration conceptAlteration;
            T8DataFileAlteration alteration;

            // Create an alteration that lists all records as inserts.
            alteration = new T8DataFileAlteration(fromStateFile);
            conceptAlteration = alteration.getConceptAlteration();
            for (DataRecord fromStateRecord : fromStateFile.getDataFileRecords())
            {
                // Register the fromStateRecord as an delete.
                alteration.addDeletedRecord(fromStateRecord);

                // Register the fromStateRecord's concept as a deletion.
                conceptAlteration.add(T8ConceptAlteration.getConceptAlteration(fromStateRecord.getOntology(), null));

                // Add deleted data string particle settings.
                for (T8DataStringParticleSettings fromStateSettings : fromStateRecord.getDataStringParticleSettings())
                {
                    alteration.addDeletedParticleSettings(fromStateSettings);
                }
            }

            // Return the final alteration.
            return alteration;
        }
        else if (fromStateFile == null) // No existing state found, so all records are new (inserts).
        {
            T8ConceptAlteration conceptAlteration;
            T8DataFileAlteration alteration;

            // Create an alteration that lists all records as inserts.
            alteration = new T8DataFileAlteration(toStateFile);
            conceptAlteration = alteration.getConceptAlteration();
            for (DataRecord toStateRecord : toStateFile.getDataFileRecords())
            {
                // Register the toStateRecord as an insert.
                alteration.addInsertedRecord(toStateRecord);

                // Register the toStateRecord's concept as an insert.
                conceptAlteration.add(T8ConceptAlteration.getConceptAlteration(null, toStateRecord.getOntology()));

                // Add inserted data string particle settings.
                for (T8DataStringParticleSettings toStateSettings : toStateRecord.getDataStringParticleSettings())
                {
                    alteration.addInsertedParticleSettings(toStateSettings);
                }
            }

            // Return the final alteration.
            return alteration;
        }
        else // If the fromState is not null, the the alteration is an update.
        {
            T8DataFileAlteration alteration;

            // Create the alteration.
            alteration = new T8DataFileAlteration(toStateFile);

            // Check each record in the toState against the corresponding record in the fromState.
            for (DataRecord toStateRecord : toStateFile.getDataFileRecords())
            {
                DataRecord fromStateRecord;

                // If the toStateRecord is found in the fromStateFile, it is registered as an update.
                fromStateRecord = fromStateFile.findDataRecord(toStateRecord.getID());
                if (fromStateRecord != null)
                {
                    T8OntologyConcept fromStateRecordConcept;
                    T8OntologyConcept toStateRecordConcept;

                    // If any of the content of the record changed, register the toStateRecord as an update.
                    if (isUpdate(fromStateRecord, toStateRecord))
                    {
                        alteration.addUpdatedRecord(toStateRecord);
                    }

                    // Determine the record concept alteration.
                    fromStateRecordConcept = fromStateRecord.getOntology();
                    toStateRecordConcept = toStateRecord.getOntology();
                    alteration.addConceptAlteration(T8ConceptAlteration.getConceptAlteration(fromStateRecordConcept, toStateRecordConcept));

                    // Find the updated data string particle settings.
                    for (T8DataStringParticleSettings toStateSettings : toStateRecord.getDataStringParticleSettings())
                    {
                        String settingsId;

                        settingsId = toStateSettings.getId();
                        if (fromStateRecord.getDataStringParticleSettings(settingsId) != null)
                        {
                            alteration.addUpdatedParticleSettings(toStateSettings);
                        }
                        else
                        {
                            alteration.addInsertedParticleSettings(toStateSettings);
                        }
                    }

                    // Find the deleted data string particle settings.
                    for (T8DataStringParticleSettings fromStateSettings : fromStateRecord.getDataStringParticleSettings())
                    {
                        String settingsId;

                        settingsId = fromStateSettings.getId();
                        if (toStateRecord.getDataStringParticleSettings(settingsId) == null)
                        {
                            alteration.addDeletedParticleSettings(fromStateSettings);
                        }
                    }
                }
                else
                {
                    // The toStateRecord was not found in the fromStateFile, so it is registered as an insert.
                    alteration.addInsertedRecord(toStateRecord);
                    alteration.addConceptAlteration(T8ConceptAlteration.getConceptAlteration(null, toStateRecord.getOntology()));
                    for (T8DataStringParticleSettings toStateSettings : toStateRecord.getDataStringParticleSettings())
                    {
                        alteration.addInsertedParticleSettings(toStateSettings);
                    }
                }
            }

            // Find all deleted records (records present in the fromState but not the toState).
            for (DataRecord fromStateRecord : fromStateFile.getDataFileRecords())
            {
                // If the fromStateRecord does not exist in the toStateFile, it is registered as a deletion.
                if (toStateFile.findDataRecord(fromStateRecord.getID()) == null)
                {
                    T8ConceptAlteration conceptAlteration;

                    // Register the fromStateRecord as a deletion.
                    alteration.addDeletedRecord(fromStateRecord);

                    // Add all of the deleted ontology links.
                    conceptAlteration = alteration.getConceptAlteration();
                    conceptAlteration.add(T8ConceptAlteration.getConceptAlteration(fromStateRecord.getOntology(), null));

                    // Add all of the deleted particle settings.
                    for (T8DataStringParticleSettings fromStateSettings : fromStateRecord.getDataStringParticleSettings())
                    {
                        alteration.addDeletedParticleSettings(fromStateSettings);
                    }
                }
            }

            // Return the final alteration.
            return alteration;
        }
    }

    private static boolean isUpdate(DataRecord fromState, DataRecord toState)
    {
        // Check for a difference in content between the states in order to identify an update.
        if (!fromState.getContent(false).isEquivalentTo(toState.getContent(false), false)) return true;
        else return (!fromState.getAttributes().equals(toState.getAttributes())); // If attributes do not match, the record is also considered an update.
    }

    @Override
    public String toString()
    {
        return "T8DataFileAlteration{" + "dataFile=" + dataFile + ", conceptAlteration=" + conceptAlteration + ", insertedRecords=" + insertedRecords + ", updatedRecords=" + updatedRecords + ", deletedRecords=" + deletedRecords + ", insertedParticleSettings=" + insertedParticleSettings + ", updatedParticleSettings=" + updatedParticleSettings + ", deletedParticleSettings=" + deletedParticleSettings + '}';
    }
}
