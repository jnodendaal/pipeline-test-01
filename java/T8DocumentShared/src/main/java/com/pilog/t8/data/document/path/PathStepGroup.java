package com.pilog.t8.data.document.path;

import com.pilog.t8.data.document.path.steps.ConceptIDStep;
import com.pilog.t8.data.document.path.steps.InstanceRequirementStep;
import com.pilog.t8.data.document.path.steps.ClassIdStep;
import com.pilog.t8.data.document.path.steps.OrganizationIdStep;
import com.pilog.t8.data.document.path.steps.InstanceAccessStep;
import com.pilog.t8.data.document.path.steps.FieldRequirementStep;
import com.pilog.t8.data.document.path.steps.OntologyClassIdStep;
import com.pilog.t8.data.document.path.steps.DescendantRecordStep;
import com.pilog.t8.data.document.path.steps.CodeStep;
import com.pilog.t8.data.document.path.steps.AbbreviationStep;
import com.pilog.t8.data.document.path.steps.OntologyCodeStep;
import com.pilog.t8.data.document.path.steps.FftStep;
import com.pilog.t8.data.document.path.steps.FieldAccessStep;
import com.pilog.t8.data.document.path.steps.DefinitionStep;
import com.pilog.t8.data.document.path.steps.AttributeStep;
import com.pilog.t8.data.document.path.steps.DataRequirementStep;
import com.pilog.t8.data.document.path.steps.DocumentReferenceStep;
import com.pilog.t8.data.document.path.steps.DataRequirementAccessStep;
import com.pilog.t8.data.document.path.steps.LowerBoundStep;
import com.pilog.t8.data.document.path.steps.OntologyLinkStep;
import com.pilog.t8.data.document.path.steps.SectionAccessStep;
import com.pilog.t8.data.document.path.steps.RequirementAttributeStep;
import com.pilog.t8.data.document.path.steps.RecordFieldStep;
import com.pilog.t8.data.document.path.steps.RecordValueStep;
import com.pilog.t8.data.document.path.steps.PropertyAccessStep;
import com.pilog.t8.data.document.path.steps.RecordPropertyStep;
import com.pilog.t8.data.document.path.steps.StateStep;
import com.pilog.t8.data.document.path.steps.RecordSectionStep;
import com.pilog.t8.data.document.path.steps.RecordByInstanceStep;
import com.pilog.t8.data.document.path.steps.PropertyRequirementStep;
import com.pilog.t8.data.document.path.steps.SectionRequirementStep;
import com.pilog.t8.data.document.path.steps.RecordByDrStep;
import com.pilog.t8.data.document.path.steps.ValueRequirementStep;
import com.pilog.t8.data.document.path.steps.TermStep;
import com.pilog.t8.data.document.path.steps.UpperBoundStep;
import com.pilog.t8.data.document.path.steps.UnitOfMeasureStep;
import com.pilog.t8.data.document.path.steps.ValueAccessStep;
import com.pilog.t8.data.document.path.steps.ValueStep;
import com.pilog.t8.data.document.path.steps.ValueStringStep;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.document.path.steps.ParentDataFileStep;
import com.pilog.t8.data.document.path.steps.ParentRecordStep;
import com.pilog.t8.data.document.path.steps.StripNonAlphanumericStep;

/**
 * @author Bouwer du Preez
 */
public class PathStepGroup
{
    private final List<PathStep> pathSteps;
    private boolean singleValue;
    private final GroupOperator operator;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private DataRecordProvider recordProvider;
    private T8DataObjectProvider dataObjectProvider;

    public enum GroupOperator {DEFAULT, COALESCE};

    public PathStepGroup(GroupOperator operator, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, DataRecordProvider recordProvider, T8DataObjectProvider dataObjectProvider)
    {
        this.operator = operator;
        this.terminologyProvider = terminologyProvider;
        this.ontologyProvider = ontologyProvider;
        this.recordProvider = recordProvider;
        this.dataObjectProvider = dataObjectProvider;
        this.pathSteps = new ArrayList<PathStep>();
    }

    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
        for (PathStep pathStep : pathSteps)
        {
            pathStep.setTerminologyProvider(terminologyProvider);
        }
    }

    public void setOntologyProvider(OntologyProvider ontologyProvider)
    {
        this.ontologyProvider = ontologyProvider;
        for (PathStep pathStep : pathSteps)
        {
            pathStep.setOntologyProvider(ontologyProvider);
        }
    }

    public void setRecordProvider(DataRecordProvider recordProvider)
    {
        this.recordProvider = recordProvider;
        for (PathStep pathStep : pathSteps)
        {
            pathStep.setRecordProvider(recordProvider);
        }
    }

    public void setDataObjectProvider(T8DataObjectProvider dataObjectProvider)
    {
        this.dataObjectProvider = dataObjectProvider;
        for (PathStep pathStep : pathSteps)
        {
            pathStep.setDataObjectProvider(dataObjectProvider);
        }
    }

    public void printPathSteps()
    {
        for (PathStep pathStep : pathSteps)
        {
            System.out.println(pathStep + "\n");
        }
    }

    public Object evaluateGroup(Object rootInput, Object input)
    {
        switch (operator)
        {
            case COALESCE:
                if (input instanceof List)
                {
                    List inputList;

                    inputList = (List)input;
                    if (inputList.size() > 0)
                    {
                        Object firstObject;

                        firstObject = inputList.get(0);
                        if (firstObject instanceof CharSequence)
                        {
                            if (!Strings.isNullOrEmpty((CharSequence)firstObject))
                            {
                                // The input list contains at least one non-empty string so return it.
                                return input;
                            }
                        }
                        else if (firstObject != null)
                        {
                            // The input object is not null so return it.
                            return input;
                        }
                    }
                }
                else if (input instanceof CharSequence)
                {
                    if (!Strings.isNullOrEmpty((CharSequence)input))
                    {
                        // The input list contains at least one non-empty string so return it.
                        return input;
                    }
                } else if (input != null) return input;

                // At this stage no valid content was found in the input, so continue with the evaluation.
            default:
                if (!pathSteps.isEmpty())
                {
                    Object result;

                    result = rootInput;
                    for (PathStep pathStep : pathSteps)
                    {
                        result = pathStep.evaluateStep(rootInput, result);
                    }

                    if (singleValue)
                    {
                        if (result instanceof List)
                        {
                            List resultList;

                            resultList = (List)result;
                            if (resultList.size() > 0) return resultList.get(0);
                            else return null;
                        }
                        else return null;
                    }
                    else return result;
                }
                else throw new RuntimeException("No expression has been parsed for execution.");
        }
    }

    public void parseExpression(String expression)
    {
        pathSteps.clear();
        pathSteps.addAll(parseSteps(expression));
    }

    private List<PathStep> parseSteps(String inputExpression)
    {
        String typeIndicator;
        StepModifiers modifiers;
        int typeIndicatorStartIndex;
        int typeIndicatorEndIndex;
        List<PathStep> steps;
        String parseString;

        // Create a collection of parsed steps.
        steps = new ArrayList<>();

        // Check for the single value indicator.
        parseString = inputExpression;
        if (parseString.startsWith("?"))
        {
            singleValue = true;
            parseString = parseString.substring(1); // Remove the first '?' character.
        }
        else singleValue = false;

        // Check the initial operation of the input expression.
        if (parseString.startsWith("//"))
        {
            steps.add(new DescendantRecordStep(true));
            parseString = parseString.substring(2); // Remove the first two slashes.
        }
        else if (parseString.startsWith("/"))
        {
            steps.add(new RootStep());
            parseString = parseString.substring(1); // Remove the first slash.
        }

        // Now parse all steps from the string.
        while (parseString.length() > 0)
        {
            String stepString;
            int stepEndIndex;

            // Determine the end of the step string.
            stepEndIndex = parseString.indexOf('/');
            if (stepEndIndex > -1)
            {
                // Slice the step string from the start of the parse string.
                stepString = parseString.substring(0, stepEndIndex);
                if (parseString.length() > stepEndIndex + 1)
                {
                    parseString = parseString.substring(stepEndIndex + 1);
                }
                else
                {
                    parseString = "";
                }

                // If the step string is empty (which only occurs when the '//' operator is used), add the correct step and skip the rest of this step iterator.
                if (stepString.length() == 0)
                {
                    steps.add(new DescendantRecordStep(true));
                    continue;
                }
            }
            else
            {
                // Use the entire parse string (what is left of it) as the last step string.
                stepString = parseString;
                parseString = "";
            }

            // Set the start index for parsing.
            typeIndicatorStartIndex = 0;

            // First check for modifiers.
            modifiers = new StepModifiers();
            if (stepString.startsWith("<"))
            {
                int modifierEndIndex;

                // Make sure the modifier sequence is closed properly and then parse the enclosed modifier flags.
                modifierEndIndex = stepString.indexOf('>');
                if (modifierEndIndex > -1)
                {
                    String modifierString;

                    // Parse the modifiers.
                    modifierString = stepString.substring(1, modifierEndIndex);
                    modifiers.parseModifiers(modifierString);

                    // Set the start of the type parsing at the character just after the closing bracket of the modifier string.
                    typeIndicatorStartIndex = modifierEndIndex + 1;
                }
                else throw new RuntimeException("No closing bracket for modifiers in step: " + stepString);
            }

            // Secondly parse the type indicator from the step string.
            typeIndicatorEndIndex = stepString.indexOf(':', typeIndicatorStartIndex);
            if (typeIndicatorEndIndex == -1)
            {
                typeIndicatorEndIndex = stepString.indexOf('['); // Start of predicate expression.
                if (typeIndicatorEndIndex == -1) typeIndicator = stepString.substring(typeIndicatorStartIndex);
                else typeIndicator = stepString.substring(typeIndicatorStartIndex, typeIndicatorEndIndex);
            }
            else typeIndicator = stepString.substring(typeIndicatorStartIndex, typeIndicatorEndIndex);

            // Check the type indicator and parse the correct step type.
            switch (typeIndicator)
            {
                case "I":
                {
                    steps.add(new RecordByInstanceStep(modifiers, terminologyProvider, recordProvider, stepString));
                    break;
                }
                case "D":
                {
                    steps.add(new RecordByDrStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "S":
                {
                    steps.add(new RecordSectionStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "P":
                {
                    steps.add(new RecordPropertyStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "F":
                {
                    steps.add(new RecordFieldStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "V":
                {
                    steps.add(new RecordValueStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "A":
                {
                    steps.add(new AttributeStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "AR":
                {
                    steps.add(new RequirementAttributeStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "IR":
                {
                    steps.add(new InstanceRequirementStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "DR":
                {
                    steps.add(new DataRequirementStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "SR":
                {
                    steps.add(new SectionRequirementStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "PR":
                {
                    steps.add(new PropertyRequirementStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "FR":
                {
                    steps.add(new FieldRequirementStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "VR":
                {
                    steps.add(new ValueRequirementStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "IA":
                {
                    steps.add(new InstanceAccessStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "DA":
                {
                    steps.add(new DataRequirementAccessStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "SA":
                {
                    steps.add(new SectionAccessStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "PA":
                {
                    steps.add(new PropertyAccessStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "FA":
                {
                    steps.add(new FieldAccessStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "VA":
                {
                    steps.add(new ValueAccessStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "VAL":
                {
                    steps.add(new ValueStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "REF":
                {
                    steps.add(new DocumentReferenceStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "CONCEPT_ID":
                {
                    steps.add(new ConceptIDStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "OL":
                {
                    steps.add(new OntologyLinkStep(modifiers, terminologyProvider, ontologyProvider, stepString));
                    break;
                }
                case "ORG_ID":
                {
                    steps.add(new OrganizationIdStep(modifiers, terminologyProvider, ontologyProvider, stepString));
                    break;
                }
                case "OC_ID":
                {
                    steps.add(new OntologyClassIdStep(modifiers, terminologyProvider, ontologyProvider, stepString));
                    break;
                }
                case "TERM":
                {
                    steps.add(new TermStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "ABBREVIATION":
                {
                    steps.add(new AbbreviationStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "DEFINITION":
                {
                    steps.add(new DefinitionStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "CODE":
                {
                    steps.add(new CodeStep(modifiers, terminologyProvider, ontologyProvider, stepString));
                    break;
                }
                case "O_CODE":
                {
                    steps.add(new OntologyCodeStep(modifiers, terminologyProvider, ontologyProvider, stepString));
                    break;
                }
                case "VALUE_STRING":
                {
                    steps.add(new ValueStringStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "FFT":
                {
                    steps.add(new FftStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "CLASS_ID":
                {
                    steps.add(new ClassIdStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "UPPER_BOUND":
                {
                    steps.add(new UpperBoundStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "LOWER_BOUND":
                {
                    steps.add(new LowerBoundStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "UOM":
                {
                    steps.add(new UnitOfMeasureStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "STATE":
                {
                    steps.add(new StateStep(modifiers, terminologyProvider, dataObjectProvider, stepString));
                    break;
                }
                case "STRIP_NAN":
                {
                    steps.add(new StripNonAlphanumericStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "*":
                {
                    steps.add(new DescendantRecordStep(false));
                    break;
                }
                case "..":
                {
                    steps.add(new ParentRecordStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                case "...":
                {
                    steps.add(new ParentDataFileStep(modifiers, terminologyProvider, stepString));
                    break;
                }
                default:
                {
                    throw new RuntimeException("Invalid type indicator in document path step: " + stepString);
                }
            }
        }

        // Return the list of parsed steps.
        return steps;
    }
}
