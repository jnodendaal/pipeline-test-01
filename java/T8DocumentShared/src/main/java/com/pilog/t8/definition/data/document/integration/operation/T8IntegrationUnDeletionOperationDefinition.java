package com.pilog.t8.definition.data.document.integration.operation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceOperationMappingDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8IntegrationUnDeletionOperationDefinition extends T8IntegrationServiceOperationMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTEGRATION_SERVICE_OPERATION_MAPPING_UNDELETION";
    public static final String DISPLAY_NAME = "UnDeletion Operation";
    public static final String DESCRIPTION = "An operation mapping that defines the creation operation";

    public enum Datum
    {
    };
// -------- Definition Meta-Data -------- //
    public T8IntegrationUnDeletionOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    protected List<T8DataParameterDefinition> getOperationInputParameters()
    {
        List<T8DataParameterDefinition> parameterDefinitions;

        parameterDefinitions = new ArrayList<>();
        parameterDefinitions.add(new T8DataParameterDefinition(P_DATA_RECORDS, "Data Records", "A List containing the data records", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        parameterDefinitions.add(new T8DataParameterDefinition(P_OPERATION_IID, "Operation Instance ID", "The Call and Call Back unique Operation ID.", T8DataType.GUID));

        return parameterDefinitions;
    }
}
