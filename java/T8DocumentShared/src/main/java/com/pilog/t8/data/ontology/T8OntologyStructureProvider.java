package com.pilog.t8.data.ontology;

/**
 * @author Bouwer du Preez
 */
public interface T8OntologyStructureProvider
{
    public T8OntologyStructure getOntologyStructure();
}
