package com.pilog.t8.data.document.datastring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringInstance implements Serializable
{
    private String instanceId;
    private String languageId;
    private String typeId;
    private boolean autoGenerate;
    private final Set<String> organizationLinks;

    public T8DataStringInstance()
    {
        this.organizationLinks = new HashSet<>();
    }

    public T8DataStringInstance(String instanceId, String typeId, String languageId, boolean autoGenerate)
    {
        this();
        this.instanceId = instanceId;
        this.typeId = typeId;
        this.languageId = languageId;
        this.autoGenerate = autoGenerate;
    }

    public String getInstanceID()
    {
        return instanceId;
    }

    public void setInstanceID(String instanceID)
    {
        this.instanceId = instanceID;
    }

    public String getTypeID()
    {
        return typeId;
    }

    public void setTypeID(String typeID)
    {
        this.typeId = typeID;
    }

    public String getLanguageID()
    {
        return languageId;
    }

    public void setLanguageID(String languageID)
    {
        this.languageId = languageID;
    }

    /**
     * Auto generate is a synonym for auto-render.
     *
     * @return {@code true} if the rendering should be automatically rendered.
     *      {@code false} if not
     */
    public boolean isAutoGenerate()
    {
        return autoGenerate;
    }

    /**
     * Auto generate is a synonym for auto-render.
     *
     * @param autoGenerate {@code true} for the rendering to be automatically
     *      rendered. {@code false} otherwise
     */
    public void setAutoGenerate(boolean autoGenerate)
    {
        this.autoGenerate = autoGenerate;
    }

    public String getOrgId()
    {
        if (organizationLinks.size() > 0)
        {
            return organizationLinks.iterator().next();
        }
        else return null;
    }

    public boolean containsOrganizationLink(String orgId)
    {
        return organizationLinks.contains(orgId);
    }

    public Set<String> getOrganizationLinkSet()
    {
        return new HashSet<String>(organizationLinks);
    }

    public List<String> getOrganizationLinks()
    {
        return new ArrayList<String>(organizationLinks);
    }

    public void addOrganizationLink(String orgId)
    {
        organizationLinks.add(orgId);
    }

    public void setOrganizationLinks(List<String> orgIdList)
    {
        organizationLinks.clear();
        if (orgIdList != null)
        {
            organizationLinks.addAll(orgIdList);
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8DataStringInstance{");
        toStringBuilder.append("instanceId=").append(this.instanceId);
        toStringBuilder.append(",languageId=").append(this.languageId);
        toStringBuilder.append(",typeId=").append(this.typeId);
        toStringBuilder.append(",autoGenerate=").append(this.autoGenerate);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
