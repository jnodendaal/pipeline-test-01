package com.pilog.t8.definition.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.datatype.T8DtGroupedDataQualityReport;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataQualityApiResource implements T8DefinitionResource
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataQualityApiResource.class);

    // Operation Identifiers
    public static final String OPERATION_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT = "@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT";
    public static final String OPERATION_API_DQ_RETRIEVE_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE = "@OP_API_DQ_RETRIEVE_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE";
    public static final String OPERATION_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE = "@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE";
    public static final String OPERATION_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT = "@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT";

    // Operation Paramater Identifiers
    public static final String PARAMETER_DATA_QUALITY_REPORT = "$P_DATA_QUALITY_REPORT";
    public static final String PARAMETER_ROOT_DR_INSTANCE_ID_LIST = "$P_ROOT_DR_INSTANCE_ID_LIST";
    public static final String PARAMETER_ROOT_DR_INSTANCE_ID = "$P_ROOT_DR_INSTANCE_ID";
    public static final String PARAMETER_GROUP_BY_TARGET_DR_INSTANCE_ID = "$P_GROUP_BY_TARGET_DR_INSTANCE_ID";
    public static final String PARAMETER_GROUP_BY_TARGET_PROPERTY_ID = "$P_GROUP_BY_TARGET_PROPERTY_ID";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());

        return definitions;
    }

    private List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        T8DtGroupedDataQualityReport dtReport;
        List<T8Definition> definitions;

        dtReport = new T8DtGroupedDataQualityReport(null);
        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT);
        definition.setMetaDisplayName("Retrieve Root Data Quality Report");
        definition.setMetaDescription("Retrieves a data quality report aggregated from the data of the specified root record type.");
        definition.setClassName("com.pilog.t8.api.T8DataQualityApiOperations$RetrieveRootDataQualityReport");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ROOT_DR_INSTANCE_ID_LIST, "Root DR Instance ID List", "The list of DR Instance ID's of the root records to be included in the report.", T8DataType.LIST));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_QUALITY_REPORT, "Data Quality Report", "The data quality report retrieved from the database.", dtReport));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DQ_RETRIEVE_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE);
        definition.setMetaDisplayName("Retrieve Data Quality Report Grouped by DR Instance");
        definition.setMetaDescription("Retrieves a data quality report aggregated from the data of the specified root record type and grouped according to a DR Instance of a specified descendant of the root record.");
        definition.setClassName("com.pilog.t8.api.T8DataQualityApiOperations$RetrieveDataQualityReportGroupedByDrInstance");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ROOT_DR_INSTANCE_ID, "Root DR Instannce", "The DR Instance of the root records to be included in the report.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_QUALITY_REPORT, "Data Quality Report", "The data quality report retrieved from the database.", dtReport));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE);
        definition.setMetaDisplayName("Retrieve Root Data Quality Report Grouped by DR Instance");
        definition.setMetaDescription("Retrieves a data quality report aggregated from the data of the specified root record type and grouped according to a DR Instance of a specified descendant of the root record.");
        definition.setClassName("com.pilog.t8.api.T8DataQualityApiOperations$RetrieveRootDataQualityReportGroupedByDrInstance");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ROOT_DR_INSTANCE_ID, "Root DR Instannce", "The DR Instance of the root records to be included in the report.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_GROUP_BY_TARGET_DR_INSTANCE_ID, "Alteration", "The ID of the target DR from which the record to be grouped by is referenced.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_GROUP_BY_TARGET_PROPERTY_ID, "Alteration", "The ID of the target property from which the record to be grouped by is referenced.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_QUALITY_REPORT, "Data Quality Report", "The data quality report retrieved from the database.", dtReport));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT);
        definition.setMetaDisplayName("Retrieve Root Data Quality Report Grouped by Value Concept");
        definition.setMetaDescription("Retrieves a data quality report aggregated from the data of the specified root record type and grouped according to a value concept on a specified descendant of the root record.");
        definition.setClassName("com.pilog.t8.api.T8DataQualityApiOperations$RetrieveRootDataQualityReportGroupedByValueConcept");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ROOT_DR_INSTANCE_ID, "Root DR Instannce", "The DR Instance of the root records to be included in the report.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_GROUP_BY_TARGET_DR_INSTANCE_ID, "Alteration", "The ID of the target DR from which the record to be grouped by is referenced.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_GROUP_BY_TARGET_PROPERTY_ID, "Alteration", "The ID of the target property from which the record to be grouped by is referenced.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_QUALITY_REPORT, "Data Quality Report", "The data quality report retrieved from the database.", dtReport));
        definitions.add(definition);

        return definitions;
    }

    private List<T8TableDataSourceResourceDefinition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        List<T8TableDataSourceResourceDefinition> definitions;

        definitions = new ArrayList<>();
        return definitions;
    }

    private List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        return definitions;
    }
}
