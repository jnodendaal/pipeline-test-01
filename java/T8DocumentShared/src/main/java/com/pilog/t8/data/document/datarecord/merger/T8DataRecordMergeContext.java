package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordMergeContext
{
    public T8DataTransaction getTransaction();

    public DataRecord copyDataRecord(DataRecord sourceRecord);

    public String getDefaultContentLanguageID();
    public T8Context getContext();
    public T8ExpressionEvaluator getExpressionEvaluator();
    public T8DataRecordValueStringGenerator getFFTGenerator();
    public TerminologyProvider getTerminologyProvider();
    public OntologyProvider getOntologyProvider();
    public DataRequirementInstanceProvider getDRInstanceProvider();

    public T8DataRecordInstanceMerger getInstanceMerger(DataRecord destinationRecord, DataRecord sourceRecord);
    public T8DataRecordPropertyMerger getPropertyMerger(RecordProperty destinationProperty, RecordProperty sourceProperty);
    public T8DataRecordFieldMerger getFieldMerger(RecordValue destinationField, RecordValue sourceField);
    public T8DataRecordValueMerger getValueMerger(RecordValue destinationValue, RecordValue sourceValue);
}
