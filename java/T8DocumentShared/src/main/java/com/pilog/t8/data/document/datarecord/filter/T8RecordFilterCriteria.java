package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8RecordFilterCriteria extends T8DataFilterCriteria implements T8RecordFilterClause
{
    private String filterFieldId;
    private boolean allowCriteriaAddition;
    private boolean allowSubCriteriaAddition;

    public static final String CTE_VALUE_TERMS_IDENTIFIER = "SEARCH_RECORD_VALUES";
    public static final String CTE_NULL_VALUES_IDENTIFIER = "SEARCH_RECORD_NULL_VALUES";
    public static final String CTE_VALUE_CONCEPT_IDS_IDENTIFIER = "SEARCH_RECORD_VALUE_CONCEPT_ID";
    public static final String CTE_SEARCH_RECORD_DOCUMENT_REFERENCE = "SEARCH_RECORD_DOCUMENT_REFERENCE";

    public T8RecordFilterCriteria(String id)
    {
        this.id = id;
        this.allowCriteriaAddition = true;
        this.allowSubCriteriaAddition = true;
    }

    public String getRecordFilterFieldIdentifier()
    {
        return filterFieldId;
    }

    public void setRecordFilterFieldIdentifier(String identifier)
    {
        this.filterFieldId = identifier;
    }

    public boolean isAllowCriteriaAddition()
    {
        return allowCriteriaAddition;
    }

    public void setAllowCriteriaAddition(boolean allowCriteriaAddition)
    {
        this.allowCriteriaAddition = allowCriteriaAddition;
    }

    public boolean isAllowSubCriteriaAddition()
    {
        return allowSubCriteriaAddition;
    }

    public void setAllowSubCriteriaAddition(boolean allowSubCriteriaAddition)
    {
        this.allowSubCriteriaAddition = allowSubCriteriaAddition;
    }

    @Override
    public T8RecordFilterCriteria copy()
    {
        T8RecordFilterCriteria copiedCriteria;

        copiedCriteria = new T8RecordFilterCriteria(id);
        copiedCriteria.setRecordFilterFieldIdentifier(filterFieldId);
        copiedCriteria.setAllowCriteriaAddition(allowCriteriaAddition);
        copiedCriteria.setAllowSubCriteriaAddition(allowSubCriteriaAddition);
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            copiedCriteria.addFilterClause(filterClauses.get(filterClause), (T8RecordFilterClause)filterClause.copy());
        }

        return copiedCriteria;
    }

    public DataFilterConjunction getConjunction(T8RecordFilterClause clause)
    {
        return filterClauses.get(clause);
    }

    @Override
    public void setEntityIdentifier(String identifier)
    {
        this.entityId = identifier;
        if (filterFieldId != null) filterFieldId = T8IdentifierUtilities.setNamespace(filterFieldId, entityId);
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            filterClause.setEntityIdentifier(identifier);
        }
    }

    @Override
    public StringBuffer getWhereClause(T8DataTransaction tx, String sqlIdentifier)
    {
        if (!hasCriteria())
        {
            return null;
        }
        else
        {
            T8DataEntityDefinition dataEntityDefinition;
            T8DataSourceDefinition dataSourceDefinition;
            Iterator<T8DataFilterClause> clauseIterator;
            String filterColumnIdentifier;
            StringBuffer whereClause;
            int clauseCount;

            // Get the database adaptor to use for this source.
            dataEntityDefinition = tx.getDataEntityDefinition(entityId);
            dataSourceDefinition = tx.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
            filterColumnIdentifier = dataEntityDefinition.mapFieldToSourceIdentifier(filterFieldId);
            filterColumnIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(filterColumnIdentifier);

            whereClause = new StringBuffer();
            whereClause.append(filterColumnIdentifier);
            whereClause.append(" IN (");

            // Add all of the filter clauses.
            clauseCount = 0;
            clauseIterator = filterClauses.keySet().iterator();
            while (clauseIterator.hasNext())
            {
                T8DataFilterClause clause;

                // Get the next clause and only append it if it has active criteria.
                clause = clauseIterator.next();
                if (clause.hasCriteria())
                {
                    DataFilterConjunction conjunction;

                    // Get the conjunction to use when appending the clause.
                    conjunction = filterClauses.get(clause);
                    if (clauseCount == 0)
                    {
                        whereClause.append(clause.getWhereClause(tx, sqlIdentifier));
                    }
                    else // For any clause except the first one, append the conjunction and then the clause.
                    {
                        whereClause.append(" ");
                        whereClause.append(conjunction.toString());
                        whereClause.append(" ROOT_RECORD_ID IN (");
                        whereClause.append(clause.getWhereClause(tx, sqlIdentifier));
                    }

                    clauseCount++;
                }
            }

            // Close all of the clause IN's.
            for (int i = 0; i < clauseCount-1; i++)
            {
                whereClause.append(")");
            }

            // Close the outer IN clause.
            whereClause.append(")");
            System.out.println("Record Where Clause: " + whereClause);
            return whereClause;
        }
    }

    @Override
    public ArrayList<Object> getWhereClauseParameters(T8DataTransaction dataAccessProvider)
    {
        ArrayList<Object> parameters;

        parameters = new ArrayList<Object>();
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            if (filterClause.hasCriteria())
            {
                parameters.addAll(filterClause.getWhereClauseParameters(dataAccessProvider));
            }
        }

        return parameters;
    }

    @Override
    public boolean includesEntity(T8DataEntity entity)
    {
        throw new RuntimeException("Record filter criteria cannot be applied to entity.");
    }

    @Override
    public T8DataFilterCriterion getFilterCriterion(String identifier)
    {
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            T8DataFilterCriterion filterCriterion;

            filterCriterion = clause.getFilterCriterion(identifier);
            if (filterCriterion != null) return filterCriterion;
        }

        return null;
    }

    @Override
    public Set<String> getFilterFieldIdentifiers()
    {
        Set<String> fieldSet;

        fieldSet = new HashSet<String>();
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            fieldSet.addAll(filterClause.getFilterFieldIdentifiers());
        }

        return fieldSet;
    }

    /**
     * Removes the given {@code T8DataFilterClause} from the clause list.
     *
     * @param filterClause The {@code T8DataFilterClause} that needs to be removed.
     */
    public void removeFilterClause(T8DataFilterClause filterClause)
    {
        List<T8DataFilterClause> filterClauseRemovalList;

        // If the Filter clause is not found at this level check in the sub clauses.
        filterClauseRemovalList = new ArrayList<T8DataFilterClause>();
        if (filterClauses.remove(filterClause) == null)
        {
            for (T8DataFilterClause filterClauseFromCriteria : filterClauses.keySet())
            {
                if (filterClauseFromCriteria instanceof T8DataFilterCriteria)
                {
                    ((T8DataFilterCriteria)filterClauseFromCriteria).removeFilterClause(filterClause);

                    if (filterClauseFromCriteria.getFilterCriterionList().isEmpty())
                    {
                        filterClauseRemovalList.add(filterClauseFromCriteria);
                    }
                }
            }
        }

        for (T8DataFilterClause removeFilterClause : filterClauseRemovalList)
        {
            filterClauses.remove(removeFilterClause);
        }
    }

    @Override
    public List<T8DataFilterCriterion> getFilterCriterionList()
    {
        ArrayList<com.pilog.t8.data.filter.T8DataFilterCriterion> criterionList;

        criterionList = new ArrayList<com.pilog.t8.data.filter.T8DataFilterCriterion>();
        for (T8DataFilterClause filterClause : filterClauses.keySet())
        {
            criterionList.addAll(filterClause.getFilterCriterionList());
        }

        return criterionList;
    }

    public boolean equalsFilterCriteria(T8RecordFilterCriteria other)
    {
        if (other == null)
        {
            return false;
        }
        else if (!Objects.equals(id, other.id))
        {
            return false;
        }
        else if((this.filterClauses == null && other.filterClauses != null) || (this.filterClauses != null && other.filterClauses == null))
        {
            return false;
        }
        else if (this.filterClauses != null && other.filterClauses != null)
        {
            if((this.filterClauses.isEmpty() && !other.filterClauses.isEmpty()) || (!this.filterClauses.isEmpty() && other.filterClauses.isEmpty()))
            {
                return false;
            }

            for (T8DataFilterClause t8DataFilterClause : filterClauses.keySet())
            {
                if(!other.filterClauses.containsKey(t8DataFilterClause))
                {
                    return false;
                }
            }

            return true;
        }
        else return true;
    }
}
