package com.pilog.t8.data.document.path;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.ExternalExpressionParser;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class EpicDocPathParser implements ExternalExpressionParser
{
    private final DocPathEvaluatorFactory factory;

    public EpicDocPathParser(DocPathEvaluatorFactory factory)
    {
        this.factory = factory;
    }

    @Override
    public boolean isApplicableTo(String expression)
    {
        // At this stage we do not check to ensure the expression is a DocPath expression,
        // we just assume that it is since no other expression types are currently used in T8.
        return true;
    }

    @Override
    public Expression parse(String expression)
    {
        DocPathExpressionEvaluator evaluator;

        evaluator = factory.getEvaluator();
        evaluator.parseExpression(expression);
        return new EpicDocPathExpression(evaluator);
    }

    private static class EpicDocPathExpression implements Expression
    {
        private final DocPathExpressionEvaluator evaluator;

        public EpicDocPathExpression(DocPathExpressionEvaluator evaluator)
        {
            this.evaluator = evaluator;
        }

        @Override
        public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
        {
            return evaluator.evaluateExpression(contextObject);
        }

        @Override
        public String unparse()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
