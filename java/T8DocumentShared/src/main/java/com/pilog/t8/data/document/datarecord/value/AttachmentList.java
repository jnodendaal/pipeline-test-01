package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class AttachmentList extends RecordValue
{
    private final List<String> attachments;
    private final Map<String, T8AttachmentDetails> attachmentDetails;

    public AttachmentList(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
        this.attachments = new ArrayList<String>();
        this.attachmentDetails = new HashMap<>();
    }

    @Override
    public AttachmentListContent getContent()
    {
        AttachmentListContent content;

        content = new AttachmentListContent();
        content.addAttachments(attachments);
        content.addAttachmentDetails(attachmentDetails);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        AttachmentListContent content;

        content = (AttachmentListContent)valueContent;
        clearAttachments();
        clearAttachmentDetails();

        this.attachments.addAll(content.getAttachmentIdList());
        this.attachmentDetails.putAll(content.getAttachmentDetails());
    }

    public void addAttachment(String attachmentID)
    {
        if (attachmentID != null)
        {
            if (!attachments.contains(attachmentID))
            {
                // Add the new attachment ID to the list.
                attachments.add(attachmentID);

                // Fire the event to signal the changed.
                fireRecordValueChangedEvent(this);
            }
        }
        else throw new IllegalArgumentException("Cannot add null attachment ID to attachment list: " + this);
    }

    public void addAttachments(Collection<String> newAttachments)
    {
        if (!newAttachments.contains(null))
        {
            boolean added;

            added = false;
            for (String newAttachment : newAttachments)
            {
                if (!attachments.contains(newAttachment))
                {
                    attachments.add(newAttachment);
                    added = true;
                }
            }

            // Fire the event to signal the changed.
            if (added) fireRecordValueChangedEvent(this);
        }
        else throw new IllegalArgumentException("Input list " + newAttachments + " contains a null attachment ID that cannot be added to attachment list: " + this);
    }

    public boolean replaceAttachment(String oldAttachmentId, String newAttachmentId, T8AttachmentDetails newAttachmentFileDetails)
    {
        int index;

        index = attachments.indexOf(oldAttachmentId);
        if (index > -1)
        {
            // Replace the attachment reference.
            attachments.remove(index);
            attachments.add(index, newAttachmentId);

            // Replace the attachment details.
            attachmentDetails.remove(oldAttachmentId);
            attachmentDetails.put(newAttachmentId, newAttachmentFileDetails);

            // Fire the event to signal the changed.
            fireRecordValueChangedEvent(this);
            return true;
        }
        else return false;
    }

    public boolean removeAttachment(String attachmentId)
    {
        if (attachments.contains(attachmentId))
        {
            // Add the new attachment ID to the list.
            attachments.remove(attachmentId);

            // Fire the event to signal the changed.
            fireRecordValueChangedEvent(this);
            return true;
        }
        else return false;
    }

    public void clearAttachments()
    {
        // Clear the list of attachments.
        attachments.clear();
        attachmentDetails.clear();

        // Fire the event to signal the changed.
        fireRecordValueChangedEvent(this);
    }

    public void clearAttachmentDetails()
    {
        attachmentDetails.clear();
    }

    public void addAttachmentDetails(Map<String, T8AttachmentDetails> details)
    {
        for (String attachmentID : details.keySet())
        {
            addAttachmentDetails(attachmentID, details.get(attachmentID));
        }
    }

    public void addAttachmentDetails(String attachmentId, T8AttachmentDetails details)
    {
        attachmentDetails.put(attachmentId, details);
    }

    public Map<String, T8AttachmentDetails> getAttachmentDetails()
    {
        return new HashMap<String, T8AttachmentDetails>(attachmentDetails);
    }

    public T8AttachmentDetails getAttachmentDetails(String attachmentId)
    {
        return attachmentDetails.get(attachmentId);
    }

    public boolean containsAttachment(String attachmentId)
    {
        return attachments.contains(attachmentId);
    }

    public List<String> getAttachmentIds()
    {
        return new ArrayList<String>(attachments);
    }

    public int getAttachmentCount()
    {
        return attachments.size();
    }

    @Override
    public boolean hasContent(boolean includeFFT)
    {
        return attachments.size() > 0;
    }

    @Override
    public RecordValue copy()
    {
        AttachmentList copy;

        copy = (AttachmentList)RecordValue.createValue(valueID, valueRequirement, value);
        copy.setFFT(fft);
        copy.addAttachments(attachments);
        copy.addAttachmentDetails(attachmentDetails);
        return copy;
    }

    @Override
    public RecordValue copy(ValueRequirement valueRequirement)
    {
        AttachmentList copy;

        copy = (AttachmentList)RecordValue.createValue(valueRequirement);
        copy.setFFT(fft);
        copy.addAttachments(attachments);
        copy.addAttachmentDetails(attachmentDetails);
        return copy;
    }

    @Override
    public List<HashMap<String, Object>> getDataRows()
    {
        ArrayList<HashMap<String, Object>> dataRows;
        ArrayList<RecordValue> recordValues;
        int valueSequence;

        recordValues = getDescendantValues();
        recordValues.add(this);

        valueSequence = 0;
        dataRows = new ArrayList<>();
        for (String attachmentId : getAttachmentIds())
        {
            HashMap<String, Object> dataRow;
            RecordSection recordSection;
            RecordProperty recordProperty;
            RecordValue parentRecordValue;
            RequirementType requirementType;
            DataRecord record;

            // Get the values from which data will be extracted.
            parentRecordValue = getParentRecordValue();
            requirementType = RequirementType.ATTACHMENT;
            recordProperty = getParentRecordProperty();
            recordSection = recordProperty.getParentRecordSection();
            record = recordSection.getParentDataRecord();

            // Extract the data row.
            dataRow = new HashMap<>();
            dataRow.put("RECORD_ID", record.getID());
            dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
            dataRow.put("DR_ID", record.getDataRequirement().getConceptID());
            dataRow.put("SECTION_ID", recordSection.getSectionRequirement().getConceptID());
            dataRow.put("PROPERTY_ID", recordProperty.getPropertyRequirement().getConceptID());
            dataRow.put("DATA_TYPE_ID", requirementType.toString());
            dataRow.put("DATA_TYPE_SEQUENCE", getValueRequirement().getSequence());
            dataRow.put("VALUE_SEQUENCE", valueSequence++);
            dataRow.put("PARENT_VALUE_SEQUENCE", parentRecordValue != null ? parentRecordValue.getSequence() : null);
            dataRow.put("VALUE", attachmentId);
            dataRow.put("VALUE_CONCEPT_ID", attachmentId);
            dataRow.put("FFT", getFFT());
            dataRow.put("VALUE_ID", getPrescribedValueID());

            // Add the extracted data row to the list.
            dataRows.add(dataRow);
        }

        return dataRows;
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public RecordValue getSubValue(RequirementType requirementType, String conceptID, Integer index)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public ArrayList<Value> getSubValues(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public ArrayList<Value> removeSubValues(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public void replaceSubValue(RecordValue newValue)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public void setSubValue(Value newValue)
    {
        throw new RuntimeException("Invalid method.");
    }

    @Override
    public String toString()
    {
        return "AttachmentList{" + "attachments=" + attachments + '}';
    }
}
