package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class DataRecordAccessLayer implements Serializable
{
    private DataRecord dataRecord;
    private boolean editable;
    private boolean visible;
    private boolean fftEditable;
    private String dataObjectId;
    private String stateId;
    private DataRecordAccessLayer parentAccessLayer;
    private final List<DataRecordAccessLayer> subRecordAccessLayers;
    private final Map<String, SectionAccessLayer> sectionAccessLayers;
    private final Map<String, ClassificationAccessLayer> classificationAccessLayers;
    private final List<ParticleSettingsAccessLayer> particleSettingsAccessLayers;

    public DataRecordAccessLayer()
    {
        this.dataRecord = null;
        this.editable = true;
        this.visible = true;
        this.fftEditable = true;
        this.subRecordAccessLayers = new ArrayList<>();
        this.sectionAccessLayers = new HashMap<>();
        this.classificationAccessLayers = new HashMap<>();
        this.particleSettingsAccessLayers = new ArrayList<>();
    }

    public DataRecordAccessLayer(DataRecord dataRecord)
    {
        this();
        this.dataRecord = dataRecord;
        constructAccessLayers(dataRecord.getDataRequirementInstance());
    }

    private void constructAccessLayers(DataRequirementInstance drInstance)
    {
        // Add the section access layers.
        for (SectionRequirement sectionRequirement : drInstance.getDataRequirement().getSectionRequirements())
        {
            SectionAccessLayer sectionAccessLayer;

            // Create the section access layer and add all of the property access layers it contains.
            sectionAccessLayer = new SectionAccessLayer(sectionRequirement.getConceptID());
            for (PropertyRequirement propertyRequirement : sectionRequirement.getPropertyRequirements())
            {
                PropertyAccessLayer propertyAccessLayer;

                // Create the property access layer and add all of the field access layers it contains (or value access layer).
                propertyAccessLayer = new PropertyAccessLayer(propertyRequirement.getConceptID());
                if (propertyRequirement.isComposite())
                {
                    for (ValueRequirement fieldRequirement : propertyRequirement.getFieldRequirements())
                    {
                        FieldAccessLayer fieldAccessLayer;
                        String fieldID;

                        fieldID = fieldRequirement.getFieldID();

                        fieldAccessLayer = new FieldAccessLayer(fieldID);
                        fieldAccessLayer.setValueAccessLayer(createValueAccessLayer(fieldRequirement.getFirstSubRequirement()));
                        propertyAccessLayer.addFieldAccessLayer(fieldAccessLayer);
                    }
                }
                else
                {
                    propertyAccessLayer.setValueAccessLayer(createValueAccessLayer(propertyRequirement.getValueRequirement()));
                }

                sectionAccessLayer.addPropertyAccessLayer(propertyAccessLayer);
            }

            addSectionAccessLayer(sectionAccessLayer);
        }
    }

    public DataRecordAccessLayer copy()
    {
        DataRecordAccessLayer copy;

        copy = new DataRecordAccessLayer();
        copy.setEditable(editable);
        copy.setVisible(visible);
        copy.setFFTEditable(fftEditable);
        copy.setDataObjectId(dataObjectId);
        copy.setStateId(stateId);

        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            copy.addSectionAccessLayer(sectionAccessLayer.copy());
        }

        return copy;
    }

    public void setAccess(DataRecordAccessLayer newAccess)
    {
        setVisible(newAccess.isVisible());
        setEditable(newAccess.isEditable());
        setFFTEditable(newAccess.isFftEditable());
        setDataObjectId(newAccess.getDataObjectId());
        setStateId(newAccess.getStateId());

        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            SectionAccessLayer newSectionAccess;

            newSectionAccess = newAccess.getSectionAccessLayer(sectionAccessLayer.getSectionID());
            if (newSectionAccess != null)
            {
                sectionAccessLayer.setAccess(newSectionAccess);
            }
        }
    }

    private ValueAccessLayer createValueAccessLayer(ValueRequirement valueRequirement)
    {
        if (valueRequirement != null)
        {
            RequirementType requirementType;

            requirementType = valueRequirement.getRequirementType();
            switch (requirementType)
            {
                case CONTROLLED_CONCEPT:
                {
                    return new ControlledConceptAccessLayer();
                }
                case DATE_TYPE:
                {
                    return new DateValueAccessLayer();
                }
                case DATE_TIME_TYPE:
                {
                    return new DateTimeValueAccessLayer();
                }
                case MEASURED_NUMBER:
                {
                    return new MeasuredNumberAccessLayer();
                }
                case MEASURED_RANGE:
                {
                    return new MeasuredRangeAccessLayer();
                }
                default:
                {
                    return new DefaultValueAccessLayer(requirementType);
                }
            }
        }
        else return null;
    }

    void setParentAccessLayer(DataRecordAccessLayer parentAccessLayer)
    {
        this.parentAccessLayer = parentAccessLayer;
    }

    public DataRecordAccessLayer getParentAccessLayer()
    {
        return parentAccessLayer;
    }

    public void reset()
    {
        constructAccessLayers(dataRecord.getDataRequirementInstance());
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public void setDataRecord(DataRecord record)
    {
        this.dataRecord = record;
    }

    public String getRecordId()
    {
        return dataRecord.getID();
    }

    public String getDataRequirementInstanceId()
    {
        return dataRecord.getDataRequirementInstanceID();
    }

    public String getDataRequirementID()
    {
        return dataRecord.getDataRequirementID();
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setPathEditable()
    {
        this.editable = true;
    }

    public void setEditable(boolean editable)
    {
        // Set the editability of this
        this.editable = editable;

        // Set the editability of all the sections.
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            sectionAccessLayer.setEditable(editable);
        }

        // Set the editability of all classifications.
        for (ClassificationAccessLayer classificationAccessLayer : classificationAccessLayers.values())
        {
            classificationAccessLayer.setEditable(editable);
        }
    }

    public void setDescendentsRequired(boolean required)
    {
        // Set the required attribute of all the sections and descendents
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            sectionAccessLayer.setRequired(required);
            sectionAccessLayer.setDescendentsRequired(required);
        }

        // Set the required attribute of all classifications
        for (ClassificationAccessLayer classificationAccessLayer : classificationAccessLayers.values())
        {
            classificationAccessLayer.setRequired(required);
        }
    }

    public boolean isFftEditable()
    {
        return fftEditable;
    }

    public void setFFTEditable(boolean fftEditable)
    {
        this.fftEditable = fftEditable;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setPathVisible()
    {
        this.visible = true;
    }

    public void setVisible(boolean visible)
    {
        // Set the visibility of this access layer.
        this.visible = visible;

        // Set the visibility of all the sections.
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            sectionAccessLayer.setVisible(visible);
        }

        // Set the visibility of all classifications.
        for (ClassificationAccessLayer classificationAccessLayer : classificationAccessLayers.values())
        {
            classificationAccessLayer.setVisible(visible);
        }
    }

    public void setCharacteristicPropertiesRequired()
    {
        for (PropertyRequirement propertyRequirement : dataRecord.getDataRequirement().getCharacteristicPropertyRequirements())
        {
            getPropertyAccessLayer(propertyRequirement.getConceptID()).setRequired(true);
        }
    }

    public String getDataObjectId()
    {
        return dataObjectId;
    }

    public void setDataObjectId(String dataObjectId)
    {
        this.dataObjectId = dataObjectId;
    }

    public String getStateId()
    {
        return stateId;
    }

    public void setStateId(String stateId)
    {
        this.stateId = stateId;
    }

    public void setDataFileEditable(boolean editable)
    {
        setEditable(editable);
        setDescendantsEditable(editable);
    }

    /**
     * This method sets the editability of all descendant data records that are
     * independent of this record.
     * @param editable The editability value to set.
     */
    public void setDescendantsEditable(boolean editable)
    {
        for (DataRecordAccessLayer recordAccessLayer : getDescendantRecordAccessLayers())
        {
            recordAccessLayer.setEditable(editable);
            for (DataRecordAccessLayer descendantAccessLayer : recordAccessLayer.getDescendantRecordAccessLayers())
            {
                descendantAccessLayer.setEditable(editable);
            }
        }
    }

    /**
     * This method sets the editability of all descendant data records that are
     * independent of this record.
     * @param editable The editability value to set.
     */
    public void setIndependentDescendantsEditable(boolean editable)
    {
        for (DataRecordAccessLayer recordAccessLayer : getIndependentDescendantAccessLayers())
        {
            recordAccessLayer.setEditable(editable);
            for (DataRecordAccessLayer descendantAccessLayer : recordAccessLayer.getDescendantRecordAccessLayers())
            {
                descendantAccessLayer.setEditable(editable);
            }
        }
    }

    /**
     * Returns all descendant record access layers that are independent of this
     * record.
     * @return A list of all descendants record access layers that are
     * independent of this record.
     */
    public List<DataRecordAccessLayer> getIndependentDescendantAccessLayers()
    {
        List<DataRecordAccessLayer> accessLayers;

        accessLayers = new ArrayList<>();
        for (DataRecord independentRootRecord : dataRecord.getIndependentDescendantRootRecords())
        {
            DataRecordAccessLayer independentAccessLayer;

            independentAccessLayer = independentRootRecord.getAccessLayer();
            accessLayers.add(independentAccessLayer);
            accessLayers.addAll(independentAccessLayer.getDescendantRecordAccessLayers());
        }

        return accessLayers;
    }

    public List<DataRecordAccessLayer> getSubRecordAccessLayers()
    {
        return new ArrayList<>(subRecordAccessLayers);
    }

    public List<DataRecordAccessLayer> getSubRecordAccessLayersReferencedFromProperty(String propertyID)
    {
        List<DataRecordAccessLayer> accessLayers;
        List<String> recordIDList;

        recordIDList = dataRecord.getSubRecordIDsReferencedFromProperty(propertyID);
        accessLayers = new ArrayList<>();
        for (DataRecordAccessLayer subRecordAccessLayer : subRecordAccessLayers)
        {
            if (recordIDList.contains(subRecordAccessLayer.getRecordId()))
            {
                accessLayers.add(subRecordAccessLayer);
            }
        }

        return accessLayers;
    }

    public DataRecordAccessLayer getSubRecordAccessLayer(String recordID)
    {
        for (DataRecordAccessLayer subRecordAccessLayer : subRecordAccessLayers)
        {
            if (subRecordAccessLayer.getRecordId().equals(recordID))
            {
                return subRecordAccessLayer;
            }
        }

        return null;
    }

    public void addSubRecordAccessLayer(DataRecordAccessLayer accessLayer)
    {
        if (!containsSubRecordAccessLayer(accessLayer.getRecordId()))
        {
            this.subRecordAccessLayers.add(accessLayer);
            accessLayer.setParentAccessLayer(this);
        }
        else throw new IllegalArgumentException("Cannot add Sub-Record Access Layer '" + accessLayer + "' since it already exists in sub-record access layer collection: " + subRecordAccessLayers);
    }

    public boolean containsSubRecordAccessLayer(String recordID)
    {
        for (DataRecordAccessLayer subRecordAccessLayer : subRecordAccessLayers)
        {
            if (subRecordAccessLayer.getRecordId().equals(recordID)) return true;
        }

        return false;
    }

    public void setSubSubRecordAccessLayers(List<DataRecordAccessLayer> subRecordAccessLayers)
    {
        removeAllSubRecordAccessLayers();
        if (subRecordAccessLayers != null)
        {
            for (DataRecordAccessLayer subRecordAccessLayer : subRecordAccessLayers)
            {
                addSubRecordAccessLayer(subRecordAccessLayer);
            }
        }
    }

    public void removeFromParent()
    {
        if (parentAccessLayer != null)
        {
            parentAccessLayer.removeSubRecordAccessLayer(this);
            parentAccessLayer = null;
        }
    }

    public void removeSubRecordAccessLayer(DataRecordAccessLayer subRecordAccessLayer)
    {
        // Remove the sub-record access layer from this access layer's list of sub-record access layers.
        if (subRecordAccessLayers.remove(subRecordAccessLayer))
        {
            // If we successfully removed the sub-record access layer, make sure this access layer is no longer listed as its parent.
            subRecordAccessLayer.setParentAccessLayer(null);
        }
    }

    public DataRecordAccessLayer removeSubRecordAccessLayer(String recordID)
    {
        DataRecordAccessLayer accessLayerToRemove;

        accessLayerToRemove = getSubRecordAccessLayer(recordID);
        if (accessLayerToRemove != null)
        {
            // Remove the sub-record access layer from this access layer's list of sub-record access layers.
            if (subRecordAccessLayers.remove(accessLayerToRemove))
            {
                // If we successfully remove the sub-record access layer, make sure this access layer is no longer listed as its parent.
                accessLayerToRemove.setParentAccessLayer(null);
                return accessLayerToRemove;
            }
            else return null;
        }
        else return null;
    }

    public void removeAllSubRecordAccessLayers()
    {
        // First remove sub-record access layers.
        for (DataRecordAccessLayer subRecordAccessLayer : subRecordAccessLayers)
        {
            subRecordAccessLayer.setParentAccessLayer(null);
        }

        // Clear the collection of sub-record access layers.
        subRecordAccessLayers.clear();
    }

    public List<DataRecordAccessLayer> getDescendantRecordAccessLayers()
    {
        ArrayList<DataRecordAccessLayer> descendants;

        descendants = new ArrayList<>(subRecordAccessLayers);
        for (DataRecordAccessLayer subRecordAccessLayer : subRecordAccessLayers)
        {
            descendants.addAll(subRecordAccessLayer.getDescendantRecordAccessLayers());
        }

        return descendants;
    }

    public List<ClassificationAccessLayer> getClassificationAccessLayers()
    {
        return new ArrayList<>(classificationAccessLayers.values());
    }

    public ClassificationAccessLayer getClassificationAccessLayer(String ontologyStructureID)
    {
        return classificationAccessLayers.get(ontologyStructureID);
    }

    public ClassificationAccessLayer getOrCreateClassificationAccessLayer(String ontologyStructureId)
    {
        ClassificationAccessLayer accessLayer;

        accessLayer = classificationAccessLayers.get(ontologyStructureId);
        return accessLayer != null ? accessLayer : createClassificationAccessLayer(ontologyStructureId);
    }

    public ClassificationAccessLayer createClassificationAccessLayer(String ontologyStructureId)
    {
        ClassificationAccessLayer accessLayer;

        accessLayer = new ClassificationAccessLayer(ontologyStructureId);
        accessLayer.setEditable(editable);
        accessLayer.setVisible(visible);
        classificationAccessLayers.put(ontologyStructureId, accessLayer);
        return accessLayer;
    }

    public boolean containsClassificationAccessLayer(String ontologyStructureId)
    {
        return classificationAccessLayers.containsKey(ontologyStructureId);
    }

    public List<PropertyAccessLayer> getPropertyAccessLayers()
    {
        List<PropertyAccessLayer> propertyAccessLayers;

        propertyAccessLayers = new ArrayList<>();
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            propertyAccessLayers.addAll(sectionAccessLayer.getPropertyAccessLayers());
        }

        return propertyAccessLayers;
    }

    public PropertyAccessLayer getPropertyAccessLayer(String propertyId)
    {
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            PropertyAccessLayer propertyAccessLayer;

            propertyAccessLayer = sectionAccessLayer.getPropertyAccessLayer(propertyId);
            if (propertyAccessLayer != null) return propertyAccessLayer;
        }

        return null;
    }

    public List<FieldAccessLayer> getFieldAccessLayers()
    {
        List<FieldAccessLayer> fieldAccessLayers;

        fieldAccessLayers = new ArrayList<>();
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            fieldAccessLayers.addAll(sectionAccessLayer.getFieldAccessLayers());
        }

        return fieldAccessLayers;
    }

    public FieldAccessLayer getFieldAccessLayer(String propertyId, String fieldId)
    {
        PropertyAccessLayer propertyAccessLayer;

        propertyAccessLayer = getPropertyAccessLayer(propertyId);
        if (propertyAccessLayer != null)
        {
            return propertyAccessLayer.getFieldAccessLayer(fieldId);
        }

        return null;
    }

    public List<ValueAccessLayer> getValueAccessLayers()
    {
        List<ValueAccessLayer> valueAccessLayers;

        valueAccessLayers = new ArrayList<>();
        for (SectionAccessLayer sectionAccessLayer : sectionAccessLayers.values())
        {
            valueAccessLayers.addAll(sectionAccessLayer.getValueAccessLayers());
        }

        return valueAccessLayers;
    }

    public ValueAccessLayer getValueAccessLayer(String propertyId, String fieldId)
    {
        if (fieldId != null)
        {
            FieldAccessLayer fieldAccessLayer;

            fieldAccessLayer = getFieldAccessLayer(propertyId, fieldId);
            return fieldAccessLayer != null ? fieldAccessLayer.getValueAccessLayer() : null;
        }
        else
        {
            PropertyAccessLayer propertyAccessLayer;

            propertyAccessLayer = getPropertyAccessLayer(propertyId);
            return propertyAccessLayer != null ? propertyAccessLayer.getValueAccessLayer() : null;
        }
    }

    public SectionAccessLayer getSectionAccessLayer(String sectionId)
    {
        return sectionAccessLayers.get(sectionId);
    }

    public List<SectionAccessLayer> getSectionAccessLayers()
    {
        return new ArrayList<>(sectionAccessLayers.values());
    }

    public SectionAccessLayer removeSectionAccessLayer(String sectionId)
    {
        SectionAccessLayer sectionAccessLayer;

        sectionAccessLayer = sectionAccessLayers.get(sectionId);
        if (sectionAccessLayer != null) sectionAccessLayer.setParentAccessLayer(null);
        return sectionAccessLayer;
    }

    public void addSectionAccessLayer(SectionAccessLayer sectionAccessLayer)
    {
        sectionAccessLayer.setParentAccessLayer(this);
        sectionAccessLayers.put(sectionAccessLayer.getSectionID(), sectionAccessLayer);
    }

    public ParticleSettingsAccessLayer getOrCreateParticleSettingsAccessLayer(String orgId, String dsTypeId)
    {
        ParticleSettingsAccessLayer newAccessLayer;

        for (ParticleSettingsAccessLayer accessLayer : particleSettingsAccessLayers)
        {
            String accessDsTypeId;
            String accessOrgId;

            accessDsTypeId = accessLayer.getDsTypeId();
            accessOrgId = accessLayer.getOrgId();
            if ((accessDsTypeId != null) && (accessDsTypeId.equals(dsTypeId)))
            {
                if ((accessOrgId != null) && (accessOrgId.equals(orgId)))
                {
                    return accessLayer;
                }
            }
        }

        newAccessLayer = new ParticleSettingsAccessLayer(this);
        newAccessLayer.setOrgId(orgId);
        newAccessLayer.setDsTypeId(dsTypeId);
        particleSettingsAccessLayers.add(newAccessLayer);
        return newAccessLayer;
    }

    public ParticleSettingsAccessLayer getOrCreateParticleSettingsAccessLayer(String dsTypeId)
    {
        ParticleSettingsAccessLayer newAccessLayer;

        for (ParticleSettingsAccessLayer accessLayer : particleSettingsAccessLayers)
        {
            String accessDsTypeId;
            String accessOrgId;

            accessDsTypeId = accessLayer.getDsTypeId();
            accessOrgId = accessLayer.getOrgId();
            if ((accessDsTypeId != null) && (accessDsTypeId.equals(dsTypeId)))
            {
                // We have to make sure that the org id is null, so that this setting does not overlap with an organization-specific setting.
                if (accessOrgId == null)
                {
                    return accessLayer;
                }
            }
        }

        newAccessLayer = new ParticleSettingsAccessLayer(this);
        newAccessLayer.setDsTypeId(dsTypeId);
        particleSettingsAccessLayers.add(newAccessLayer);
        return newAccessLayer;
    }

    public ParticleSettingsAccessLayer getParticleSettingsAccessLayer(T8DataStringParticleSettings settings)
    {
        for (ParticleSettingsAccessLayer accessLayer : particleSettingsAccessLayers)
        {
            if (accessLayer.isApplicableTo(settings))
            {
                return accessLayer;
            }
        }

        return null;
    }

    public List<ParticleSettingsAccessLayer> getParticleSettingsAccessLayers()
    {
        return new ArrayList<>(particleSettingsAccessLayers);
    }

    public boolean isRecordAccessEquivalent(DataRecordAccessLayer recordAccess)
    {
        if (!Objects.equals(this.editable, recordAccess.isEditable())) return false;
        else if (!Objects.equals(this.visible, recordAccess.isVisible())) return false;
        else if (!Objects.equals(this.fftEditable, recordAccess.isFftEditable())) return false;
        else return true;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("DataRecordAccessLayer{");
        toStringBuilder.append("dataRecord=").append(this.dataRecord);
        toStringBuilder.append(",editable=").append(this.editable);
        toStringBuilder.append(",visible=").append(this.visible);
        toStringBuilder.append(",dataObjectIdentifier=").append(this.dataObjectId);
        toStringBuilder.append(",stateIdentifier=").append(this.stateId);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
