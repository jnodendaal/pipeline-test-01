package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Andre Scheepers
 */
public class T8DataRecordDescriptionMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_RECORD_DESCRIPTION";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_DESCRIPTION";
    public static final String DISPLAY_NAME = "Integration Record Description Mapping";
    public static final String DESCRIPTION = "Integration Record Description Mapping for all the different types of desciriptions.";
    public static final String IDENTIFIER_PREFIX = "RD_MAP_";

    public enum Datum
    {
        DESCRIPTION_TYPE,
        DESCRIPTION_TYPE_CONCEPT,
        DESCRIPTION_LANGUAGE,
        DESCRIPTION_LANGUAGE_CONCEPT,
        SPLIT_CHAR,
        CONDITIONAL_OVERRIDE_INCLUDE,
        CONDITIONAL_OVERRIDE_ENABLED_EXPRESSION,
        CONDITIONAL_OVERRIDE_DOC_PATH_EXPRESSION,
        CONDITIONAL_OVERRIDE_EPIC_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRecordDescriptionMappingDefinition(String identifier)
    {
        super(identifier);
    }
    

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DESCRIPTION_TYPE.toString(), "Description Type", "The description type for this record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DESCRIPTION_TYPE_CONCEPT.toString(), "Description Type Concept", "The description type Concept for this record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DESCRIPTION_LANGUAGE.toString(), "Description Language", "The descritpion language for this record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DESCRIPTION_LANGUAGE_CONCEPT.toString(), "Description Language Concept", "The descritpion language Concept for this record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.SPLIT_CHAR.toString(), "Split Characters", "Specifies an ammount to split the descriptions into a table rows.", 0));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CONDITIONAL_OVERRIDE_INCLUDE.toString(), "Include Conditional Override", "Include Conditional Override to intervene the generated description and replace it with an specific description.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITIONAL_OVERRIDE_ENABLED_EXPRESSION.toString(), "Conditional Override Enabled Expression", "The expression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONDITIONAL_OVERRIDE_DOC_PATH_EXPRESSION.toString(), "Conditional Override Doc Path Expression", "The doc path expression that will be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITIONAL_OVERRIDE_EPIC_EXPRESSION.toString(), "Conditional Override Epic Expression", "The EPIC expression that will be evaluated."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public String getDescriptionType()
    {
        return (String)getDefinitionDatum(Datum.DESCRIPTION_TYPE);
    }

    public void setDescriptionType(String descriptionType)
    {
        setDefinitionDatum(Datum.DESCRIPTION_TYPE, descriptionType);
    }

    public String getDescriptionTypeConcept()
    {
        return (String)getDefinitionDatum(Datum.DESCRIPTION_TYPE_CONCEPT);
    }

    public void setDescriptionTypeConcept(String descriptionTypeConcept)
    {
        setDefinitionDatum(Datum.DESCRIPTION_TYPE_CONCEPT, descriptionTypeConcept);
    }

    public String getDescriptionLanguage()
    {
        return (String)getDefinitionDatum(Datum.DESCRIPTION_LANGUAGE);
    }

    public void setDescriptionLanguage(String descriptionLanguage)
    {
        setDefinitionDatum(Datum.DESCRIPTION_LANGUAGE, descriptionLanguage);
    }    

    public String getDescriptionLanguageConcept()
    {
        return (String)getDefinitionDatum(Datum.DESCRIPTION_LANGUAGE_CONCEPT);
    }

    public void setDescriptionLanguageConcept(String descriptionLanguageConcept)
    {
        setDefinitionDatum(Datum.DESCRIPTION_LANGUAGE_CONCEPT, descriptionLanguageConcept);
    }    

    public Integer getSplitChar()
    {
        return (Integer)getDefinitionDatum(Datum.SPLIT_CHAR);
    }

    public void setSplitChar(Integer splitChar)
    {
        setDefinitionDatum(Datum.SPLIT_CHAR, splitChar);
    }  
    
    public Boolean isConditionalOverrideIncluded()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_INCLUDE));
    }

    public void setConditionalOverrideIncluded(Boolean isConditionalOverrideIncluded)
    {
        setDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_INCLUDE, isConditionalOverrideIncluded);
    }        

    public String getConditionalOverrideEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_ENABLED_EXPRESSION.toString());
    }

    public void setConditionalOverrideEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_ENABLED_EXPRESSION.toString(), expression);
    }

    public String getConditionalOverrideDocPathExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_DOC_PATH_EXPRESSION.toString());
    }

    public void setConditionalOverrideDocPathExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_DOC_PATH_EXPRESSION.toString(), expression);
    }

    public String getConditionalOverrideEpicExpression()
    {
        return (String)getDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_EPIC_EXPRESSION.toString());
    }

    public void setConditionalOverrideEpicExpression(String expression)
    {
        setDefinitionDatum(Datum.CONDITIONAL_OVERRIDE_EPIC_EXPRESSION.toString(), expression);
    }
}
