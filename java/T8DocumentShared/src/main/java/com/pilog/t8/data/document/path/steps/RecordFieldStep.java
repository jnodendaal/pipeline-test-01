package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class RecordFieldStep extends DefaultPathStep implements PathStep
{
    public RecordFieldStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(rootInput, ((DataRecord)input).getAllPropertyValues());
        }
        else if (input instanceof RecordProperty)
        {
            RecordProperty inputProperty;
            RecordValue inputPropertyValue;

            inputProperty = (RecordProperty)input;
            inputPropertyValue = inputProperty.getRecordValue();
            return inputPropertyValue != null ? evaluateFieldStep((List)inputPropertyValue.getSubValues(RequirementType.FIELD_TYPE, null, null)) : new ArrayList<RecordValue>();
        }
        else if (input instanceof RecordValue)
        {
            RecordValue inputValue;

            inputValue = (RecordValue)input;
            return evaluateFieldStep((List)inputValue.getSubValues(RequirementType.FIELD_TYPE, null, null));
        }
        else if (input instanceof List)
        {
            List<RecordValue> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<RecordValue>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<RecordValue>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<Field> evaluateFieldStep(List<Field> fieldList)
    {
        // If we have field ID's to use for narrowing of the value list do it now.
        if (idList != null)
        {
            Iterator<Field> fieldIterator;

            fieldIterator = fieldList.iterator();
            while (fieldIterator.hasNext())
            {
                RecordValue nextField;

                nextField = fieldIterator.next();
                if (!idList.contains(nextField.getFieldID()))
                {
                    fieldIterator.remove();
                }
            }
        }

        // Now run through the remaining value list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<Field> fieldIterator;

            // Iterator over the fields and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            fieldIterator = fieldList.iterator();
            while (fieldIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                Field nextField;

                // Create a map containing the input parameters available to the predicate expression.
                nextField = fieldIterator.next();
                inputParameters = getPredicateExpressionParameters(nextField.getValueRequirement().getAttributes());
                inputParameters.put("FIELD_ID", nextField.getFieldID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        fieldIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return fieldList;
    }
}
