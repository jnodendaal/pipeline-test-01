package com.pilog.t8.data.document.datarecord.access.layout;

import com.pilog.t8.definition.data.document.datarecord.access.layout.T8ControlledConceptLayoutDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.layout.T8PropertyLayoutDefinition;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8PropertyLayout implements Serializable
{
    private PropertyHeaderLayoutType headerLayoutType;
    private PropertyHeaderTextType headerTextType;
    private T8ControlledConceptLayout controlledConceptLayout;
    
    public enum PropertyHeaderTextType {TERM, DEFINITION};
    public enum PropertyHeaderLayoutType {LEFT, TOP};
    
    public T8PropertyLayout(T8PropertyLayoutDefinition definition)
    {
        this.headerLayoutType = definition.getHeaderLayoutType();
        this.headerTextType = definition.getHeaderTextType();
        constructDataTypeLayouts(definition);
    }
    
    private void constructDataTypeLayouts(T8PropertyLayoutDefinition definition)
    {
        T8ControlledConceptLayoutDefinition controlledConceptLayoutDefinition;
        
        controlledConceptLayoutDefinition = definition.getControlledConceptLayoutDefinition();
        if (controlledConceptLayoutDefinition != null) controlledConceptLayout = new T8ControlledConceptLayout(controlledConceptLayoutDefinition);
    }

    public PropertyHeaderLayoutType getHeaderLayoutType()
    {
        return headerLayoutType;
    }

    public void setHeaderLayoutType(PropertyHeaderLayoutType layoutType)
    {
        this.headerLayoutType = layoutType;
    }

    public PropertyHeaderTextType getHeaderTextType()
    {
        return headerTextType;
    }

    public void setHeaderTextType(PropertyHeaderTextType headerType)
    {
        this.headerTextType = headerType;
    }

    public T8ControlledConceptLayout getControlledConceptLayout()
    {
        return controlledConceptLayout;
    }

    public void setControlledConceptLayout(T8ControlledConceptLayout controlledConceptLayout)
    {
        this.controlledConceptLayout = controlledConceptLayout;
    }
}
