package com.pilog.t8.data.alteration;

import java.io.Serializable;

/**
 * This class implements a single entry in a {@code T8DataAlterationIndex} object.
 * - Entries in an index, define the type of steps that need to be performed in sequence when the alteration is applied.
 * - Each entry specifies a type of alteration and the step range it covers.
 * - When an alteration must be maintained or applied, the applicable index is queried and from the returned entry an applicable alteration handler is constructed.
 *
 * @author Bouwer du Preez
 */
public class T8DataAlterationPackageIndexEntry implements Serializable
{
    private String entryIid;
    private String packageIid; // The instance id of the alteration package to which this entry is applicable.
    private String alterationId; // The id of the alteration type that this entry specifies.
    private int step; // The start index of the first step in the range that this entry describes.
    private int size; // The number of steps in the range described by this entry.

    /**
     * Constructs a new entry using the specified details.
     * @param packageIid The instance id of the alteration package to which this index entry is applicable.
     * @param alterationId The id of the alteration type the new entry will describe.
     * @param step
     * @param size // The number of steps in the range described by this entry.
     */
    public T8DataAlterationPackageIndexEntry(String entryIid, String packageIid, String alterationId, int step, int size)
    {
        this.entryIid = entryIid;
        this.packageIid = packageIid;
        this.alterationId = alterationId;
        this.step = step;
        this.size = size;
    }

    public T8DataAlterationPackageIndexEntry copy()
    {
        return new T8DataAlterationPackageIndexEntry(entryIid, packageIid, alterationId, step, size);
    }

    public String getEntryIid()
    {
        return entryIid;
    }

    public String getPackageIid()
    {
        return packageIid;
    }

    public void setPackageIid(String packageIid)
    {
        this.packageIid = packageIid;
    }

    public String getAlterationId()
    {
        return alterationId;
    }

    public void setAlterationId(String alterationId)
    {
        this.alterationId = alterationId;
    }

    public int getStep()
    {
        return step;
    }

    public void setStep(int step)
    {
        this.step = step;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    /**
     * Returns true if the range this entry comprises contains the step specified.
     * For this assertion to be true, the specified step must be greater than of equal to the starting step of this entry and also less than
     * the sum of the starting step and size of this entry.
     * @param stepToCheck The step to check against the range of this entry.
     * @return True if the range of this entry contains the step specified.
     */
    public boolean containsStep(int stepToCheck)
    {
        return (stepToCheck >= this.step) && (stepToCheck < (this.step + size));
    }

    public int addStep()
    {
        this.size++;
        return size;
    }

    public int deleteStep()
    {
        this.size--;
        return size;
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public int decrementStep()
    {
        return --step;
    }

    public int incrementStep()
    {
        return ++step;
    }
}
