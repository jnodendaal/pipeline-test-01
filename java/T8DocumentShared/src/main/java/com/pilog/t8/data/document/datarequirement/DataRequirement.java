package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.T8HierarchicalSetType;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class DataRequirement implements Requirement, Serializable
{
    private T8OntologyConcept ontology;
    private String conceptId;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;
    private String classConceptId;
    private String classCodeId;
    private String classCode;
    private String classTermId;
    private String classTerm;
    private String classDefinitionId;
    private String classDefinition;
    private String classAbbreviationId;
    private String classAbbreviation;
    private String updatedById;
    private T8Timestamp updatedAt;
    private String insertedById;
    private T8Timestamp insertedAt;
    private DataRequirementInstance parentDrInstance;
    private final ArrayList<SectionRequirement> sectionRequirements;
    private final LinkedHashMap<String, Object> attributes;

    /**
     * Pattern for Attribute Identifiers.
     * - Only Capital letters, digits and underscores allowed.
     * - First character must be a letter.
     * - No leading or trailing underscores.
     * - No double underscores.
     * - No underscores followed directly preceding a digit.
     */
    public static final String ATTRIBUTE_IDENTIFIER_PATTERN = "(?:[A-Z][A-Z0-9]*)(?:_[A-Z][A-Z0-9]*)*";

    public DataRequirement()
    {
        this.sectionRequirements = new ArrayList<SectionRequirement>();
        this.attributes = new LinkedHashMap<>();
    }

    public DataRequirement(String conceptId)
    {
        this();
        this.conceptId = conceptId;
    }

    void setParentDataRequirementInstance(DataRequirementInstance drInstance)
    {
        parentDrInstance = drInstance;
    }

    public DataRequirementInstance getParentDataRequirementInstance()
    {
        return parentDrInstance;
    }

    /**
     * Returns a copy of this object and its logical descendants.  No references to logical parents are copied.
     * @return A copy of this object and its logical descendants.
     */
    public DataRequirement copy()
    {
        DataRequirement newRequirement;

        newRequirement = new DataRequirement(conceptId);
        newRequirement.setClassConceptID(classConceptId);
        newRequirement.setUpdatedAt(updatedAt);
        newRequirement.setUpdatedByID(updatedById);
        newRequirement.setInsertedAt(insertedAt);
        newRequirement.setInsertedByID(insertedById);
        newRequirement.setOntology(ontology != null ? ontology.copy(true, true, true, true, true) : null);
        newRequirement.setTerm(term);
        newRequirement.setAbbreviation(abbreviation);
        newRequirement.setDefinition(definition);
        newRequirement.setCode(code);
        newRequirement.setClassCode(classCode);
        newRequirement.setClassTerm(classTerm);
        newRequirement.setClassAbbreviation(classAbbreviation);
        newRequirement.setClassDefinition(classDefinition);
        newRequirement.setAttributes(attributes);

        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            newRequirement.addSectionRequirement(sectionRequirement.copy());
        }

        return newRequirement;
    }

    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.DATA_REQUIREMENT;
    }

    public String getId()
    {
        return conceptId;
    }

    public void setId(String id)
    {
        this.conceptId = id;
    }

    /**
     * @deprecated {@link #getId()}
     */
    @Deprecated
    public String getConceptID()
    {
        return conceptId;
    }

    /**
     * @deprecated {@link #setId(java.lang.String)}
     */
    @Deprecated
    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public String getClassConceptID()
    {
        return classConceptId;
    }

    public void setClassConceptID(String classConceptID)
    {
        this.classConceptId = classConceptID;
    }

    public String getClassCode()
    {
        return classCode;
    }

    public void setClassCode(String classCode)
    {
        this.classCode = classCode;
    }

    public String getClassTerm()
    {
        return classTerm;
    }

    public void setClassTerm(String classTerm)
    {
        this.classTerm = classTerm;
    }

    public String getClassDefinition()
    {
        return classDefinition;
    }

    public void setClassDefinition(String classDefinition)
    {
        this.classDefinition = classDefinition;
    }

    public String getClassAbbreviation()
    {
        return classAbbreviation;
    }

    public void setClassAbbreviation(String classAbbreviation)
    {
        this.classAbbreviation = classAbbreviation;
    }

    public T8OntologyConcept getOntology()
    {
        return ontology;
    }

    public void setOntology(T8OntologyConcept ontology)
    {
        this.ontology = ontology;
    }

    /**
     * Returns the terminology of this Data Requirement and of all its contents.
     * @param terminologyCollector The list to which this Data Requirement's terminology
     * will be added.
     */
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        // Add this Data Requirement's terminology.
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.DATA_REQUIREMENT, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);

        // Add this Data Requirement's class' terminology.
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.CLASS, classConceptId, classCodeId, classCode, classTermId, classTerm, classAbbreviationId, classAbbreviation, classDefinitionId, classDefinition);

        // Add the terminology from the section requirements.
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            sectionRequirement.addContentTerminology(terminologyCollector);
        }
    }

    /**
     * Sets the terminology of this Data Requirement and all of its content
     * elements.
     * @param terminologyProvider The terminology provider from which to fetch
     * this Data Requirement's terminology.
     */
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        terminology = terminologyProvider.getTerminology(null, conceptId);
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }

        terminology = terminologyProvider.getTerminology(null, classConceptId);
        if (terminology != null)
        {
            this.classCodeId = terminology.getCodeId();
            this.classCode = terminology.getCode();
            this.classTermId = terminology.getTermId();
            this.classTerm = terminology.getTerm();
            this.classDefinitionId = terminology.getDefinitionId();
            this.classDefinition = terminology.getDefinition();
            this.classAbbreviationId = terminology.getAbbreviationId();
            this.classAbbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.classCodeId = null;
            this.classCode = null;
            this.classTermId = null;
            this.classTerm = null;
            this.classDefinitionId = null;
            this.classDefinition = null;
            this.classAbbreviationId = null;
            this.classAbbreviation = null;
        }

        // Set the terminology of the section requirements.
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            sectionRequirement.setContentTerminology(terminologyProvider);
        }
    }

    public SectionRequirement getOrAddSectionRequirement(String sectionId)
    {
        SectionRequirement sectionRequirement;

        sectionRequirement = getSectionRequirement(sectionId);
        if (sectionRequirement != null)
        {
            return sectionRequirement;
        }
        else
        {
            SectionRequirement newSectionRequirement;

            newSectionRequirement = new SectionRequirement(sectionId);
            addSectionRequirement(newSectionRequirement);
            return newSectionRequirement;
        }
    }

    public SectionRequirement getSectionRequirement(String sectionId)
    {
        for (SectionRequirement classRequirement : sectionRequirements)
        {
            if (sectionId.equals(classRequirement.getConceptID()))
            {
                return classRequirement;
            }
        }

        return null;
    }

    public int getSectionRequirementIndex(SectionRequirement classRequirement)
    {
        return sectionRequirements.indexOf(classRequirement);
    }

    public int getSectionRequirementCount()
    {
        return sectionRequirements.size();
    }

    public ArrayList<SectionRequirement> getSectionRequirements()
    {
        return new ArrayList<SectionRequirement>(sectionRequirements);
    }

    public void addSectionRequirement(SectionRequirement requirement)
    {
        requirement.setParentDataRequirement(this);
        sectionRequirements.add(requirement);
    }

    public void addSectionRequirement(SectionRequirement requirement, int index)
    {
        requirement.setParentDataRequirement(this);
        sectionRequirements.add(index, requirement);
    }

    public void removeSectionRequirement(SectionRequirement requirement)
    {
        if ((requirement != null) && (sectionRequirements.contains(requirement)))
        {
            requirement.setParentDataRequirement(null);
            sectionRequirements.remove(requirement);
        }
    }

    public boolean containsPropertyRequirement(String propertyID)
    {
        return getPropertyRequirement(propertyID) != null;
    }

    public int getPropertyRequirementIndex(String propertyID)
    {
        ArrayList<PropertyRequirement> propertyRequirements;

        propertyRequirements = getPropertyRequirements();
        for (int index = 0; index < propertyRequirements.size(); index++)
        {
            if (propertyRequirements.get(index).getConceptID().equals(propertyID))
            {
                return index;
            }
        }

        return -1;
    }

    public PropertyRequirement getOrAddPropertyRequirement(String sectionId, String propertyId)
    {
        PropertyRequirement propertyRequirement;

        propertyRequirement = getPropertyRequirement(propertyId);
        if (propertyRequirement != null)
        {
            return propertyRequirement;
        }
        else
        {
            PropertyRequirement newPropertyRequirement;
            SectionRequirement sectionRequirement;

            newPropertyRequirement = new PropertyRequirement(propertyId);
            sectionRequirement = getOrAddSectionRequirement(sectionId);
            sectionRequirement.addPropertyRequirement(newPropertyRequirement);
            return newPropertyRequirement;
        }
    }

    public int getPropertyCount()
    {
        int count;

        count = 0;
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            count += sectionRequirement.getPropertyCount();
        }

        return count;
    }

    public int getCharacteristicCount()
    {
        int count;

        count = 0;
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            count += sectionRequirement.getCharacteristicCount();
        }

        return count;
    }

    public PropertyRequirement getPropertyRequirement(String propertyId)
    {
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            PropertyRequirement propertyRequirement;

            propertyRequirement = sectionRequirement.getPropertyRequirement(propertyId);
            if (propertyRequirement != null) return propertyRequirement;
        }

        return null;
    }

    public ArrayList<PropertyRequirement> getCharacteristicPropertyRequirements()
    {
        ArrayList<PropertyRequirement> propertyRequirements;

        propertyRequirements = new ArrayList<PropertyRequirement>();
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            propertyRequirements.addAll(sectionRequirement.getCharacteristicPropertyRequirements());
        }

        return propertyRequirements;
    }

    public ArrayList<PropertyRequirement> getPropertyRequirements()
    {
        ArrayList<PropertyRequirement> propertyRequirements;

        propertyRequirements = new ArrayList<PropertyRequirement>();
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            propertyRequirements.addAll(sectionRequirement.getPropertyRequirements());
        }

        return propertyRequirements;
    }

    public PropertyRequirement getPropertyRequirement(String sectionID, String propertyID)
    {
        SectionRequirement sectionRequirement;

        sectionRequirement = getSectionRequirement(sectionID);
        if (sectionRequirement != null)
        {
            return sectionRequirement.getPropertyRequirement(propertyID);
        }
        else return null;
    }

    public List<String> getPropertyRequirementIDList()
    {
        List<String> idList;

        idList = new ArrayList<String>();
        for (PropertyRequirement propertyRequirement : getPropertyRequirements())
        {
            idList.add(propertyRequirement.getConceptID());
        }

        return idList;
    }

    public boolean containsFieldRequirement(String propertyID, String fieldID)
    {
        PropertyRequirement propertyRequirement;

        propertyRequirement = getPropertyRequirement(propertyID);
        if (propertyRequirement != null)
        {
            return propertyRequirement.getFieldRequirement(fieldID) != null;
        }
        else return false;
    }

    public List<ValueRequirement> getFieldRequirements(String fieldID)
    {
        List<ValueRequirement> fieldRequirements;

        fieldRequirements = new ArrayList<ValueRequirement>();
        for (PropertyRequirement propertyRequirement : getPropertyRequirements())
        {
            ValueRequirement fieldRequirement;

            fieldRequirement = propertyRequirement.getFieldRequirement(fieldID);
            if (fieldRequirement != null) fieldRequirements.add(fieldRequirement);
        }

        return fieldRequirements;
    }

    public List<ValueRequirement> getFieldRequirements()
    {
        List<ValueRequirement> fieldRequirements;

        fieldRequirements = new ArrayList<ValueRequirement>();
        for (PropertyRequirement propertyRequirement : getPropertyRequirements())
        {
            fieldRequirements.addAll(propertyRequirement.getFieldRequirements());
        }

        return fieldRequirements;
    }

    public String getUpdatedByID()
    {
        return updatedById;
    }

    public void setUpdatedByID(String updatedBy)
    {
        this.updatedById = updatedBy;
    }

    public T8Timestamp getUpdatedAt()
    {
        return updatedAt;
    }

    public void setUpdatedAt(T8Timestamp updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getInsertedByID()
    {
        return insertedById;
    }

    public void setInsertedByID(String insertedBy)
    {
        this.insertedById = insertedBy;
    }

    public T8Timestamp getInsertedAt()
    {
        return insertedAt;
    }

    public void setInsertedAt(T8Timestamp insertedAt)
    {
        this.insertedAt = insertedAt;
    }

    public Object getAttribute(String attributeIdentifier)
    {
        return attributes.get(attributeIdentifier);
    }

    public Object setAttribute(String attributeIdentifier, Object attributeValue)
    {
        return attributes.put(attributeIdentifier, attributeValue);
    }

    public boolean containsAttribute(String attributeIdentifier)
    {
        return attributes.containsKey(attributeIdentifier);
    }

    public Map<String, Object> getAttributes()
    {
        return new LinkedHashMap<>(attributes);
    }

    public void setAttributes(Map<String, Object> newAttributes)
    {
        attributes.clear();
        if (newAttributes != null)
        {
            attributes.putAll(newAttributes);
        }
    }

    /**
     * This method attempts to find the required dependency value matching the
     * supplied data dependency requirement.
     * @param dataDependency The dependency requirement for which to retrieve
     * the required value.
     * @return The value matching the dependency requirement.  Returns null if
     * the dependency value does not exist.
     */
    public ValueRequirement getDependencyRequirement(DataDependency dataDependency)
    {
        DataRequirement requiredDocument;

        if (dataDependency.isDrInstanceSpecified())
        {
            // Find the document matching the Data Requirement Instance ID specified by the dependency requirement.
            requiredDocument = parentDrInstance.findDataRequirementByDrInstance(T8HierarchicalSetType.LINE, dataDependency.getDrInstanceId());
        }
        else if (dataDependency.isDrSpecified())
        {
            // Find all documents matching the Data Requirement ID specified by the dependency requirement.
            requiredDocument = parentDrInstance.findDataRequirementByDrInstance(T8HierarchicalSetType.LINE, dataDependency.getDrId());
        }
        else // Property dependency.
        {
            // The dependency is on this record.
            requiredDocument = this;
        }

        // Now use the required documents and find the required property values in each.
        if (requiredDocument != null)
        {
            if (dataDependency.isFieldSpecified())
            {
                PropertyRequirement requiredProperty;

                // Get the required property from the section.
                requiredProperty = requiredDocument.getPropertyRequirement(dataDependency.getPropertyId());
                if (requiredProperty != null)
                {
                    ValueRequirement requiredField;

                    requiredField = requiredProperty.getFieldRequirement(dataDependency.getFieldId());
                    return requiredField != null ? requiredField.getFirstSubRequirement() : null;
                }
                else return null;
            }
            else if (dataDependency.isPropertySpecified())
            {
                PropertyRequirement requiredProperty;

                // Get the required property from the section.
                requiredProperty = requiredDocument.getPropertyRequirement(dataDependency.getPropertyId());
                return requiredProperty != null ? requiredProperty.getValueRequirement() : null;
            }
            else return null;
        }
        else return null;
    }

    public SectionRequirement getEquivalentSectionRequirement(SectionRequirement classRequirementToCheck)
    {
        for (SectionRequirement classRequirement : sectionRequirements)
        {
            if (classRequirement.isEquivalentRequirement(classRequirementToCheck)) return classRequirement;
        }

        return null;
    }

    @Override
    public boolean isEquivalentRequirement(Requirement requirement)
    {
        if (!(requirement instanceof DataRequirement))
        {
            return false;
        }
        else
        {
            DataRequirement dataRequirement;

            dataRequirement = (DataRequirement)requirement;
            for (SectionRequirement classRequirement : sectionRequirements)
            {
                if (!classRequirement.isEquivalentRequirement(dataRequirement.getSectionRequirement(classRequirement.getConceptID())))
                {
                    return false;
                }
            }

            return true;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("DataRequirement: [");
        builder.append("ID: ");
        builder.append(getConceptID());
        builder.append("]");
        return builder.toString();
    }
}
