package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.time.T8Date;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Gavin Boshoff
 */
public class DateTimeValueAccessLayer extends ValueAccessLayer
{
    private boolean restrictToDatePickerSelection;
    private T8Date upperBound;
    private T8Date lowerBound;

    public DateTimeValueAccessLayer()
    {
        super(RequirementType.DATE_TIME_TYPE);
    }

    public boolean isRestrictToDatePickerSelection()
    {
        return restrictToDatePickerSelection;
    }

    public void setRestrictToDatePickerSelection(boolean restrictToDatePickerSelection)
    {
        this.restrictToDatePickerSelection = restrictToDatePickerSelection;
    }

    public T8Date getUpperBound()
    {
        return upperBound;
    }

    public void setUpperBound(T8Date upperBound)
    {
        this.upperBound = upperBound;
    }

    public T8Date getLowerBound()
    {
        return lowerBound;
    }

    public void setLowerBound(T8Date lowerBound)
    {
        this.lowerBound = lowerBound;
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer valueAccess)
    {
        if (valueAccess instanceof DateTimeValueAccessLayer)
        {
            DateTimeValueAccessLayer dateTimeAccess;

            dateTimeAccess = (DateTimeValueAccessLayer)valueAccess;
            if (!Objects.equals(restrictToDatePickerSelection, dateTimeAccess.isRestrictToDatePickerSelection())) return false;
            else if (!Objects.equals(lowerBound, dateTimeAccess.getLowerBound())) return false;
            else if (!Objects.equals(upperBound, dateTimeAccess.getUpperBound())) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public ValueAccessLayer copy()
    {
        DateTimeValueAccessLayer copy;

        copy = new DateTimeValueAccessLayer();
        copy.setLowerBound(lowerBound);
        copy.setUpperBound(upperBound);
        copy.setRestrictToDatePickerSelection(restrictToDatePickerSelection);
        return copy;
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
        DateTimeValueAccessLayer newDateAccess;

        newDateAccess = (DateTimeValueAccessLayer)newAccess;
        setLowerBound(newDateAccess.getLowerBound());
        setUpperBound(newDateAccess.getUpperBound());
        setRestrictToDatePickerSelection(newDateAccess.isRestrictToDatePickerSelection());
    }
}