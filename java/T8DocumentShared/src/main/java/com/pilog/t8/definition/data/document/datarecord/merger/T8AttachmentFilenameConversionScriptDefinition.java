package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8DefaultScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentFilenameConversionScriptDefinition extends T8Definition implements T8ServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_ATTACHMENT_FILENAME_CONVERSION_SCRIPT";
    public static final String GROUP_IDENTIFIER = "@DG_ATTACHMENT_FILENAME_CONVERSION_SCRIPT";
    public static final String DISPLAY_NAME = "Attachment Filename Conversion Script";
    public static final String DESCRIPTION = "A definition of an EPIC script that convert an attachment filename to a specific format.";
    public enum Datum {EPIC_SCRIPT};
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    // EPIC script input parameters.
    public static final String PARAMETER_FILE_NAME = "$P_FILE_NAME";
    public static final String PARAMETER_SOURCE_VALUE = "$P_SOURCE_VALUE";
    public static final String PARAMETER_DESTINATION_VALUE = "$P_DESTINATION_VALUE";

    public T8AttachmentFilenameConversionScriptDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_SCRIPT, Datum.EPIC_SCRIPT.toString(), "EPIC Script",  "The script the will be executed to refresh the Record Access."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getEPICScript()
    {
        return (String)getDefinitionDatum(Datum.EPIC_SCRIPT.toString());
    }

    public void setEPICScript(String script)
    {
        setDefinitionDatum(Datum.EPIC_SCRIPT.toString(), script);
    }

    @Override
    public T8ServerContextScript getNewScriptInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.script.T8DefaultServerContextScript").getConstructor(T8Context.class, T8ServerContextScriptDefinition.class);
        return (T8ServerContextScript)constructor.newInstance(context, this);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_FILE_NAME, "Filename", "The old filename before conversion.", T8DataType.STRING));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_SOURCE_VALUE, "Source Value", "The source RecordValue from which the attachment is tranferred.", T8DataType.CUSTOM_OBJECT));
        parameterList.add(new T8DataParameterDefinition(PARAMETER_DESTINATION_VALUE, "Destination Value", "The destination RecordValue to which the attachment is transferred.", T8DataType.CUSTOM_OBJECT));
        return parameterList;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        List<T8DataParameterDefinition> parameterList;

        parameterList = new ArrayList<>();
        parameterList.add(new T8DataParameterDefinition(PARAMETER_FILE_NAME, "Filename", "The new filename after conversion.", T8DataType.STRING));
        return parameterList;
    }

    @Override
    public T8ScriptCompletionHandler getScriptCompletionHandler(T8Context context)
    {
        return new T8DefaultScriptCompletionHandler(context, this.getRootDefinition());
    }

    @Override
    public List<T8ScriptClassImport> getScriptClassImports() throws Exception
    {
        List<T8ScriptClassImport> classImports;

        classImports = new ArrayList<T8ScriptClassImport>();
        return classImports;
    }

    @Override
    public List<T8ScriptMethodImport> getScriptMethodImports() throws Exception
    {
        List<T8ScriptMethodImport> methodImports;

        methodImports = new ArrayList<T8ScriptMethodImport>();
        return methodImports;
    }

    @Override
    public String getScript()
    {
        return getEPICScript();
    }

    @Override
    public boolean containsScript()
    {
        String script;

        script = getScript();
        return (script != null) && (script.trim().length() > 0);
    }
}