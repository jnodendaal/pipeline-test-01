package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Pieter Strydom
 */
public class T8DataStringApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_RETRIEVE_RENDERINGS = "@OP_API_RETRIEVE_RENDERINGS";
    public static final String OPERATION_API_RETRIEVE_DATA_STRING_FORMAT = "@OP_API_RETRIEVE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_RETRIEVE_DATA_STRING_INSTANCE_LINK_PAIRS = "@OP_API_RETRIEVE_DATA_STRING_INSTANCE_LINK_PAIRS";
    public static final String OPERATION_API_RETRIEVE_DATA_STRING_FORMAT_LINK = "@OP_API_RETRIEVE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_RETRIEVE_DATA_STRING_PARTICLE_SETTINGS = "@OP_API_RETRIEVE_DATA_STRING_PARTICLE_SETTINGS";
    public static final String OPERATION_API_INSERT_DATA_STRING_INSTANCE_LINK = "@OP_API_INSERT_DATA_STRING_INSTANCE_LINK";
    public static final String OPERATION_API_INSERT_DATA_STRING = "@OP_API_INSERT_DATA_STRING";
    public static final String OPERATION_API_INSERT_DATA_STRING_FORMAT = "@OP_API_INSERT_DATA_STRING_FORMAT";
    public static final String OPERATION_API_INSERT_DATA_STRING_FORMAT_LINK = "@OP_API_INSERT_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_INSERT_DATA_STRING_PARTICLE_SETTINGS = "@OP_API_INSERT_DATA_STRING_PARTICLE_SETTINGS";
    public static final String OPERATION_API_UPDATE_DATA_STRING_INSTANCE_LINK = "@OP_API_UPDATE_DATA_STRING_INSTANCE_LINK";
    public static final String OPERATION_API_UPDATE_DATA_STRING_FORMAT = "@OP_API_UPDATE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_UPDATE_DATA_STRING_FORMAT_LINK = "@OP_API_UPDATE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_UPDATE_DATA_STRING_PARTICLE_SETTINGS = "@OP_API_UPDATE_DATA_STRING_PARTICLE_SETTINGS";
    public static final String OPERATION_API_SAVE_DATA_STRING_INSTANCE_LINK = "@OP_API_SAVE_DATA_STRING_INSTANCE_LINK";
    public static final String OPERATION_API_SAVE_DATA_STRING = "@OP_API_SAVE_DATA_STRING";
    public static final String OPERATION_API_SAVE_DATA_STRING_FORMAT = "@OP_API_SAVE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_SAVE_DATA_STRING_FORMAT_LINK = "@OP_API_SAVE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_SAVE_DATA_STRING_PARTICLE_SETTINGS = "@OP_API_SAVE_DATA_STRING_PARTICLE_SETTINGS";
    public static final String OPERATION_API_SAVE_DATA_STRING_INSTANCE = "@OP_API_SAVE_DATA_STRING_INSTANCE";
    public static final String OPERATION_API_DELETE_DATA_STRING_PARTICLE_SETTINGS = "@OP_API_DELETE_DATA_STRING_PARTICLE_SETTINGS";
    public static final String OPERATION_API_DELETE_DATA_STRING_FORMAT = "@OP_API_DELETE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_DELETE_DATA_STRING_FORMAT_LINK = "@OP_API_DELETE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_DELETE_DATA_STRING_INSTANCE = "@OP_API_DELETE_DATA_STRING_INSTANCE";
    public static final String OPERATION_API_DELETE_DATA_STRINGS = "@OP_API_DELETE_DATA_STRINGS";
    public static final String OPERATION_RENDER = "@OS_RENDER";
    public static final String OPERATION_REFRESH_DATA_RECORD_DATA_STRINGS = "@OS_REFRESH_DATA_RECORD_DATA_STRINGS";
    public static final String OPERATION_API_DATA_STRING_INSERT_DATA_STRING_TYPE = "@OS_API_DATA_STRING_INSERT_DATA_STRING_TYPE";
    public static final String OPERATION_API_DATA_STRING_SAVE_DATA_STRING_INSTANCE = "@OS_API_DATA_STRING_SAVE_DATA_STRING_INSTANCE";
    public static final String OPERATION_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE = "@OS_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE";
    public static final String OPERATION_API_DATA_STRING_INSERT_DATA_STRING_INSTANCE_LINK = "@OS_API_DATA_STRING_LINK_DR_INSTANCE_TO_DATA_STRING_INSTANCE";
    public static final String OPERATION_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE_LINK = "@OS_API_DATA_STRING_UNLINK_DR_INSTANCE_TO_DATA_STRING_INSTANCE";
    public static final String OPERATION_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT_LINK = "@OS_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT_LINK = "@OS_API_DATA_STRING_INSERT_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT_LINK = "@OS_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_DATA_STRING_DELETE_DATA_STRING_FORMAT_LINK = "@OS_API_DATA_STRING_DELETE_DATA_STRING_FORMAT_LINK";
    public static final String OPERATION_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT = "@OS_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT = "@OS_API_DATA_STRING_INSERT_DATA_STRING_FORMAT";
    public static final String OPERATION_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT = "@OS_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_DATA_STRING_DELETE_DATA_STRING_FORMAT = "@OS_API_DATA_STRING_DELETE_DATA_STRING_FORMAT";
    public static final String OPERATION_API_DATA_STRING_DELETE_DATA_STRINGS = "@OS_DATA_STRING_DELETE_DATA_STRINGS";

    public static final String PARAMETER_DATA_STRING_FILTER_LIST = "$P_DATA_STRING_FILTER_LIST";
    public static final String PARAMETER_DATA_STRING_LIST = "$P_DATA_STRING_LIST";
    public static final String PARAMETER_DATA_STRING_FORMAT_ID = "$P_DATA_STRING_FORMAT_ID";
    public static final String PARAMETER_DATA_STRING_FORMAT = "$P_DATA_STRING_FORMAT";
    public static final String PARAMETER_DATA_STRING_FORMAT_LINK = "$P_DATA_STRING_FORMAT_LINK";
    public static final String PARAMETER_DATA_STRING_FORMAT_LINK_ID = "$P_DATA_STRING_FORMAT_LINK_ID";
    public static final String PARAMETER_DATA_STRING_INSTANCE_LINK_LIST = "$P_DATA_STRING_INSTANCE_LINK_LIST";
    public static final String PARAMETER_DATA_STRING_INSTANCE_LINK = "$P_DATA_STRING_INSTANCE_LINK";
    public static final String PARAMETER_DATA_STRING = "$P_DATA_STRING";
    public static final String PARAMETER_DATA_STRING_TYPE = "$P_DATA_STRING_TYPE";
    public static final String PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST = "$P_DATA_STRING_PARTICLE_SETTINGS_LIST";
    public static final String PARAMETER_DATA_STRING_PARTICLE_SETTINGS = "$P_DATA_STRING_PARTICLE_SETTINGS";
    public static final String PARAMETER_DATA_STRING_INSTANCE_ID = "$P_DATA_STRING_INSTANCE_ID";
    public static final String PARAMETER_DATA_STRING_INSTANCE_ID_LIST = "$P_DATA_STRING_INSTANCE_ID_LIST";
    public static final String PARAMETER_DATA_STRING_INSTANCE = "$P_DATA_STRING_INSTANCE";
    public static final String PARAMETER_DR_INSTANCE_ID_LIST = "$P_DR_INSTANCE_ID_LIST";
    public static final String PARAMETER_RECORD_ID = "$P_RECORD_ID";
    public static final String PARAMETER_RECORD_ID_LIST = "$P_RECORD_ID_LIST";
    public static final String PARAMETER_DR_ID_LIST = "$P_DR_ID_LIST";
    public static final String PARAMETER_IS_UPDATED = "$P_IS_UPDATED";
    public static final String PARAMETER_IS_DELETED = "$P_IS_DELETED";
    public static final String PARAMETER_ORGANIZATION_ID = "$P_ORGANIZATION_ID";
    public static final String PARAMETER_DATA_REQUIREMENT_IID = "$P_DATA_REQUIREMENT_IID";
    public static final String PARAMETER_DS_INSTANCE_DISPLAY_NAME = "$P_DS_INSTANCE_DISPLAY_NAME";
    public static final String PARAMETER_DELETE_FORMAT = "$P_DELETE_FORMAT";
    public static final String PARAMETER_DELETE_CONCEPT = "$P_DELETE_CONCEPT";
    public static final String PARAMETER_DELETE_COUNT = "$P_DELETE_COUNT";
    public static final String PARAMETER_DATA_RECORD_ID = "$P_DATA_RECORD_ID";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_LANGUAGE_ID_LIST = "$P_LANGUAGE_ID_LIST";
    public static final String PARAMETER_DATA_STRING_INSTANCE_DISPLAY_NAME = "$P_DATA_STRING_INSTANCE_DISPLAY_NAME";
    public static final String PARAMETER_DATA_STRING_TYPE_ID = "$P_DATA_STRING_TYPE_ID";
    public static final String PARAMETER_DATA_STRING_TYPE_ID_LIST = "$P_DATA_STRING_TYPE_ID_LIST";
    public static final String PARAMETER_DATA_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_DATA_ENTITY_IDENTIFIER = "$P_DATA_ENTITY_IDENTIFIER";
    public static final String PARAMETER_DATA_ENTITY_FIELD_IDENTIFIER = "$P_DATA_ENTITY_FIELD_IDENTIFIER";
    public static final String PARAMETER_DELETE_DATA_STRING_FORMAT = "$P_DELETE_DATA_STRING_FORMAT";
    public static final String PARAMETER_DR_IID = "$P_DR_IID";
    public static final String PARAMETER_DELETED_DATA_STRING_COUNT = "$P_DELETED_DATA_STRING_COUNT";

    // Field identifiers.
    public static final String F_AUTO_RENDER = "$AUTO_RENDER";
    public static final String F_DATA_STRING_FORMAT_ID = "$DATA_STRING_FORMAT_ID";
    public static final String F_TYPE = "$TYPE";
    public static final String F_ELEMENT_ID = "$ELEMENT_ID";
    public static final String F_PARENT_ELEMENT_ID = "$PARENT_ELEMENT_ID";
    public static final String F_DEPENDENCY_ELEMENT_ID = "$DEPENDENCY_ELEMENT_ID";
    public static final String F_PARTICLE_ID = "$PARTICLE_ID";
    public static final String F_SEQUENCE = "$SEQUENCE";
    public static final String F_PRIORITY = "$PRIORITY";
    public static final String F_EXPRESSION = "$EXPRESSION";
    public static final String F_LENGTH_RESTRICTION = "$LENGTH_RESTRICTION";
    public static final String F_TRUNCATION_TYPE = "$TRUNCATION_TYPE";
    public static final String F_PREFIX = "$PREFIX";
    public static final String F_SUFFIX = "$SUFFIX";
    public static final String F_SEPARATOR = "$SEPARATOR";
    public static final String F_ACTIVE = "$ACTIVE";
    public static final String F_INCLUDE = "$INCLUDE";
    public static final String F_STRICT_PARTICLE_INCLUSION = "$STRICT_PARTICLE_INCLUSION";
    public static final String F_LINK_ID = "$LINK_ID";
    public static final String F_ROOT_ORG_ID = "$ROOT_ORG_ID";
    public static final String F_ORG_ID = "$ORG_ID";
    public static final String F_DATA_STRING = "$DATA_STRING";
    public static final String F_DATA_STRING_TYPE_ID = "$DATA_STRING_TYPE_ID";
    public static final String F_LANGUAGE_ID = "$LANGUAGE_ID";
    public static final String F_DATA_STRING_INSTANCE_ID = "$DATA_STRING_INSTANCE_ID";
    public static final String F_DATA_KEY_ID = "$DATA_KEY_ID";
    public static final String F_CONDITION_EXPRESSION = "$CONDITION_EXPRESSION";
    public static final String F_DATA_KEY_EXPRESSION = "$DATA_KEY_EXPRESSION";
    public static final String F_DR_ID = "$DR_ID";
    public static final String F_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String F_ROOT_RECORD_ID = "$ROOT_RECORD_ID";
    public static final String F_RECORD_ID = "$RECORD_ID";
    public static final String F_PROPERTY_ID = "$PROPERTY_ID";
    public static final String F_FIELD_ID = "$FIELD_ID";
    public static final String F_DATA_TYPE_ID = "$DATA_TYPE_ID";
    public static final String F_NAME = "$NAME";
    public static final String F_DESCRIPTION = "$DESCRIPTION";
    public static final String F_GROUP_ID = "$GROUP_ID";
    public static final String F_INSERTED_BY_ID = "$INSERTED_BY_ID";
    public static final String F_INSERTED_BY_IID = "$INSERTED_BY_IID";
    public static final String F_INSERTED_AT = "$INSERTED_AT";
    public static final String F_UPDATED_BY_ID = "$UPDATED_BY_ID";
    public static final String F_UPDATED_BY_IID = "$UPDATED_BY_IID";
    public static final String F_UPDATED_AT = "$UPDATED_AT";

    // RENDERING.
    public static final String DATA_STRING_DS_IDENTIFIER = "@DS_DATA_STRING";
    public static final String DATA_STRING_DE_IDENTIFIER = "@E_DATA_STRING";

    // DATA_STRING_ELEMENT.
    public static final String DATA_STRING_ELEMENT_DS_IDENTIFIER = "@DS_DATA_STRING_ELEMENT";
    public static final String DATA_STRING_ELEMENT_DE_IDENTIFIER = "@E_DATA_STRING_ELEMENT";

    // DATA_STRING_ELEMENT_PARTICLE.
    public static final String DATA_STRING_PARTICLE_DS_IDENTIFIER = "@DS_DATA_STRING_PARTICLE";
    public static final String DATA_STRING_PARTICLE_DE_IDENTIFIER = "@E_DATA_STRING_PARTICLE";

    // DATA_STRING_FORMAT.
    public static final String DATA_STRING_FORMAT_DS_IDENTIFIER = "@DS_DATA_STRING_FORMAT";
    public static final String DATA_STRING_FORMAT_DE_IDENTIFIER = "@E_DATA_STRING_FORMAT";

    // DATA_STRING_FORMAT_LINK.
    public static final String DATA_STRING_FORMAT_LINK_DS_IDENTIFIER = "@DS_DATA_STRING_FORMAT_LINK";
    public static final String DATA_STRING_FORMAT_LINK_DE_IDENTIFIER = "@E_DATA_STRING_FORMAT_LINK";

    // DATA_STRING_INSTANCE_LINK.
    public static final String DATA_STRING_INSTANCE_DS_IDENTIFIER = "@DS_DATA_STRING_INSTANCE";
    public static final String DATA_STRING_INSTANCE_DE_IDENTIFIER = "@E_DATA_STRING_INSTANCE";

    // DATA_STRING_INSTANCE_LINK.
    public static final String DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER = "@DS_DATA_STRING_INSTANCE_LINK";
    public static final String DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER = "@E_DATA_STRING_INSTANCE_LINK";

    // Table Names.
    private static final String TABLE_DATA_STRING = "DAT_STR";
    private static final String TABLE_DATA_STRING_ELEMENT = "DAT_STR_ELM";
    private static final String TABLE_DATA_STRING_PARTICLE = "DAT_STR_PRT";
    private static final String TABLE_DATA_STRING_FORMAT = "DAT_STR_FRM";
    private static final String TABLE_DATA_STRING_FORMAT_LINK = "DAT_STR_FRM_LNK";
    private static final String TABLE_DATA_STRING_INSTANCE = "DAT_STR_INS";
    private static final String TABLE_DATA_STRING_INSTANCE_LINK = "DAT_STR_INS_LNK";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER)) definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER)) definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RETRIEVE_RENDERINGS);
        definition.setMetaDisplayName("Retrieve Renderings");
        definition.setMetaDescription("Retrieve the list of data strings, for a certain filter.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiRetrieveRenderings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FILTER_LIST, "Data String Filter List", "The list of data string filters to be used.", T8DataType.LIST));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_LIST, "Data String List", "The list of data strings retrieved.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RETRIEVE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Retrieve Data String Format");
        definition.setMetaDescription("Retrieve the data string format, for the given format id.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiRetrieveDataStringFormat");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_ID, "Data String Format Id", "The data string format id, for the data string format that needs to be retrieved.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The data string format that will be retrieved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RETRIEVE_DATA_STRING_INSTANCE_LINK_PAIRS);
        definition.setMetaDisplayName("Retrieve Data String Instance Link Pairs");
        definition.setMetaDescription("Retrieve the data string instance link pairs, for the given data requirement instance id list.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiRetrieveDataStringInstanceLinkPairs");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_INSTANCE_ID_LIST, "Data Requirement Instance Id List", "The data requirement instance id's, for which the pairs needs to be retrieved.", T8DataType.LIST));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_LINK_LIST, "Data String Instance Link List", "The list of data string instance links that was retrieved.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RETRIEVE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Retrieve Data String Format Link");
        definition.setMetaDescription("Retrieve the data string format link, for the given link id.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiRetrieveDataStringFormatLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_LINK_ID, "Data String Format Link Id", "The data string format link id, for the data string format link that needs to be retrieved.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The data string format link that will be retrieved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_RETRIEVE_DATA_STRING_PARTICLE_SETTINGS);
        definition.setMetaDisplayName("Retrieve Data String Particle Settings");
        definition.setMetaDescription("Retrieve the data string partical settings, for the given paramaters.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiRetrieveDataStringParticleSettings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_ID_LIST, "Data Requirement Id List", "The list of data requirement id's, for the data string particle settings that needs to be retrieved.", T8DataType.LIST));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DR_INSTANCE_ID_LIST, "Data Requirement Instance Id List", "The list of data requirement instance id's, for the data string particle settings that needs to be retrieved.", T8DataType.LIST));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID_LIST, "Data Record Id List", "The list of data record id's, for the data string particle settings that needs to be retrieved.", T8DataType.LIST));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Data Record Id", "The data record id, for the data string particle settings that needs to be retrieved.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST, "Data String Particle Settings List", "The data string particle settings list that will be retrieved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_INSERT_DATA_STRING_INSTANCE_LINK);
        definition.setMetaDisplayName("Insert Data String Instance Link");
        definition.setMetaDescription("Insert the data string instance link object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiInsertDataStringInstanceLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_LINK, "Data String Instance Link", "The data string instance link object that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_INSERT_DATA_STRING);
        definition.setMetaDisplayName("Insert Data String");
        definition.setMetaDescription("Insert the data string, for the spesific record id.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiInsertDataString");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record Id", "The record id for the data string that needs to be inserted.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance Id", "The data string instance id, for the data string that needs to be inserted.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING, "Data String", "The data string that needs to be inserted.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_INSERT_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Insert Data String Format");
        definition.setMetaDescription("Insert the data string format object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiInsertDataStringFormat");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The data string format object that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_INSERT_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Insert Data String Format Link");
        definition.setMetaDescription("Insert the data string format link object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiInsertDataStringFormatLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The data string format link object that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_INSERT_DATA_STRING_PARTICLE_SETTINGS);
        definition.setMetaDisplayName("Insert Data String Particle Settings");
        definition.setMetaDescription("Insert the data string particle settings object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiInsertDataStringParticleSettings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_PARTICLE_SETTINGS, "Data String Particle Settings", "The data string particle settings object that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_UPDATE_DATA_STRING_INSTANCE_LINK);
        definition.setMetaDisplayName("Update Data String Instance Link");
        definition.setMetaDescription("Update the data string instance link object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiUpdateDataStringInstanceLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_LINK, "Data String Instance Link", "The data string instance link object that needs to be updated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_UPDATE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Update Data String Format");
        definition.setMetaDescription("Update the data string format object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiUpdateDataStringFormat");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The data string format object that needs to be updated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_UPDATE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Update Data String Format Link");
        definition.setMetaDescription("Update the data string format link object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiUpdateDataStringFormatLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The data string format link object that needs to be updated.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_UPDATE_DATA_STRING_PARTICLE_SETTINGS);
        definition.setMetaDisplayName("Update Data String Particle Settings");
        definition.setMetaDescription("Update the data string particle settings object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiUpdateDataStringParticleSettings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_PARTICLE_SETTINGS, "Data String Particle Settings", "The data string particle settings object that needs to be updated.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_IS_UPDATED, "Is Updated", "Boolean flag that indicate if the particle settings was successfully updated.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_SAVE_DATA_STRING_INSTANCE_LINK);
        definition.setMetaDisplayName("Save Data String Instance Link");
        definition.setMetaDescription("Save the data string instance link object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiSaveDataStringInstanceLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_LINK, "Data String Instance Link", "The data string instance link object that needs to be saved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_SAVE_DATA_STRING);
        definition.setMetaDisplayName("Save Data String");
        definition.setMetaDescription("Save the data string, for the spesific record id.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiSaveDataString");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID, "Record Id", "The record id for the data string that needs to be saved.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance Id", "The data string instance id, for the data string that needs to be saved.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING, "Data String", "The data string that needs to be saved.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_SAVE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Save Data String Format");
        definition.setMetaDescription("Save the data string format object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiSaveDataStringFormat");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The data string format object that needs to be saved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_SAVE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Save Data String Format Link");
        definition.setMetaDescription("Save the data string format link object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiSaveDataStringFormatLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The data string format link object that needs to be saved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_SAVE_DATA_STRING_PARTICLE_SETTINGS);
        definition.setMetaDisplayName("Save Data String Particle Settings");
        definition.setMetaDescription("Save the data string particle settings objects.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiSaveDataStringParticleSettings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST, "Data String Particle Settings List", "The data string particle settings objects that needs to be saved.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_SAVE_DATA_STRING_INSTANCE);
        definition.setMetaDisplayName("Save Data String Instance");
        definition.setMetaDescription("Save the data string instance object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiSaveDataStringInstance");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE, "Data String Instance", "The data string instance object that needs to be saved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DELETE_DATA_STRING_PARTICLE_SETTINGS);
        definition.setMetaDisplayName("Delete Data String Particle Settings");
        definition.setMetaDescription("Delete the data string particle settings object.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiDeleteDataStringParticleSettings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_PARTICLE_SETTINGS, "Data String Particle Settings", "The data string particle settings object that needs to be deleted.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_IS_DELETED, "Is Deleted", "A Boolean indicator to indicate if the particle settings was successfully deleted.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DELETE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Delete Data String Format");
        definition.setMetaDescription("Delete the data string format for the given format id.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiDeleteDataStringFormat");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_ID, "Data String Format Id", "The data string format id that needs to be deleted.", T8DataType.GUID));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DELETE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Delete Data String Format Link");
        definition.setMetaDescription("Delete the data string format link.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiDeleteDataStringFormatLink");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_FORMAT_LINK_ID, "Data String Format Link Id", "The data string format link id that needs to be deleted.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETE_FORMAT, "Delete Format", "Indicator to show if the data string format should also be deleted.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DELETE_DATA_STRING_INSTANCE);
        definition.setMetaDisplayName("Delete Data String Instance");
        definition.setMetaDescription("Delete the data string instance.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiDeleteDataStringInstance");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance Id", "The data string instance id that needs to be deleted.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETE_CONCEPT, "Delete Concept", "Indicator to show if the data string instance concept should also be deleted.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DELETE_DATA_STRINGS);
        definition.setMetaDisplayName("Delete Data Strings");
        definition.setMetaDescription("Delete the data strings.");
        definition.setClassName("com.pilog.t8.api.T8DataStringApiOperations$ApiDeleteDataStrings");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_ID_LIST, "Record Id List", "The record ids, for the data strings that needs to be deleted.", T8DataType.LIST));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DATA_STRING_INSTANCE_ID_LIST, "Data String Instance Id List", "The data string instance ids, for the data strings that needs to be deleted.", T8DataType.LIST));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETE_COUNT, "Delete Count", "A count of the successfully delete data strings.", T8DataType.INTEGER));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_RENDER);
        definition.setMetaDisplayName("Render Description");
        definition.setMetaDescription("Renders the record description using the specified arguments.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$RenderOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_LANGUAGE_ID, "Language ID", "The language for which descriptions will be generated.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_RECORD_ID, "Record ID", "The data record for which descriptions will be generated.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING, "Data String", "The description which has been rendered for the specified language/record combination.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_REFRESH_DATA_RECORD_DATA_STRINGS);
        definition.setMetaDisplayName("Refresh Data Record Renderings");
        definition.setMetaDescription("Refreshes the renderings for data records based on the specified operation parameters.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$RefreshDataRecordDataStringsOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_FILTER, "Record Data Filter", "The data filter that will be used to identify records for which to generate descriptions.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_IDENTIFIER, "Record Data Entity Identifier", "The unique identifier of the entity from which record ID's will be retrieved using the supplied data filter.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_ENTITY_FIELD_IDENTIFIER, "Record ID Field Identifier", "The unique identifier of the entity field from which record ID's will be retrieved fetched.", T8DataType.DEFINITION_IDENTIFIER));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_LANGUAGE_ID_LIST, "Language ID List", "The list of languages for which descriptions will be generated.", T8DataType.LIST));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_TYPE_ID_LIST, "Data String Type ID List", "The list of rendering types that will be generated.", T8DataType.LIST));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_INSERT_DATA_STRING_TYPE);
        definition.setMetaDisplayName("Add Data String Type");
        definition.setMetaDescription("Creates a new rendering type.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$InsertDataStringTypeOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_TYPE, "Data String Type", "The rendering type details to use to create the new type.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_TYPE_ID, "Data String Type ID", "The rendering type ID for the newly created rendering type.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_SAVE_DATA_STRING_INSTANCE);
        definition.setMetaDisplayName("Save Data String Instance");
        definition.setMetaDescription("Creates or updates a rendering instance.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$SaveDataStringInstanceOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE, "Data String Instance", "The rendering instance details to be saved.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_ORGANIZATION_ID, "Organization Identifier", "The identifier of the organization to which the instance should be linked.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_IID, "Data Requirement Instance Identifier", "The data requirement instance identifier to which the data string instance should be linked.", T8DataType.STRING, true));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE_DISPLAY_NAME, "Instance Display Name", "The display name for the rendering instance which will be the term for the concept created. If existing instance is used, this value is ignored.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance ID", "The ID for the rendering instance updated, or the generated ID for a new instance created.", T8DataType.GUID));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_INSERT_DATA_STRING_INSTANCE_LINK);
        definition.setMetaDisplayName("Link DR Instance to Data String Instance");
        definition.setMetaDescription("Creates a new link between the specified DR instance and the data string instance for the current session root organization ID.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$InsertDataStringInstanceLinkOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance ID", "The data string instance identifier to which the DR instance will be linked.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_IID, "Data Requirement Instance ID", "The Data Requirement Instance Identifier of the DR instance to be linked to the specified data string instance.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE_LINK);
        definition.setMetaDisplayName("Unlink DR Instance from Data String Instance");
        definition.setMetaDescription("Removes a link between the specified DR instance and the data string instance for the current session root organization ID.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$DeleteDataStringInstanceLinkOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance ID", "The data string instance identifier from which the DR instance will be unlinked.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DR_IID, "Data Requirement Instance ID", "The Data Requirement Instance Identifier of the DR instance to be unlinked from the specified data string instance.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE);
        definition.setMetaDisplayName("Delete Data String Instance");
        definition.setMetaDescription("Deletes a data string instance, its associated instance links, and optionally its concept.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$DeleteDataStringInstanceOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE_ID, "Data String Instance ID", "The data string instance identifier to be deleted.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DELETE_CONCEPT, "Delete Concept Flag", "Whether or not to delete the concept for the specified data string instance.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Retrieve Data String Format Link");
        definition.setMetaDescription("Retrieve the rendering format link associated with the specified ID.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$RetrieveDataStringFormatLinkOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_LINK_ID, "Data String Format Link ID", "The rendering format link ID to use for the retrieval of the required data.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The Data String format link and the required data as retrieved using the specified ID.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Add Data String Format Link");
        definition.setMetaDescription("Creates a new rendering format link.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$InsertDataStringFormatLinkOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The rendering format link data object to be persisted.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The rendering format data object to be persisted.", T8DataType.CUSTOM_OBJECT, true));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_LINK_ID, "Format Link ID", "The ID for the rendering format link which was generated.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Update Data String Format Link");
        definition.setMetaDescription("Updates an existing rendering format link.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$UpdateDataStringFormatLinkOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_LINK, "Data String Format Link", "The rendering format link data object to be persisted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_DELETE_DATA_STRING_FORMAT_LINK);
        definition.setMetaDisplayName("Delete Data String Format Link");
        definition.setMetaDescription("Deletes an existing rendering format link. Optionally deletes associated format.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$DeleteDataStringFormatLinkOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_LINK_ID, "Data String Format Link ID", "The data string format link to be deleted.", T8DataType.STRING));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DELETE_DATA_STRING_FORMAT, "Delete Associated Format Flag", "If set, the associated format will also be deleted.", T8DataType.BOOLEAN));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Retrieve Data String Format");
        definition.setMetaDescription("Retrieve the rendering format associated with the specified ID.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$RetrieveDataStringFormatOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_ID, "Data String Format ID", "The rendering format ID to use for the retrieval of the required data.", T8DataType.STRING));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The Data String format and the required data as retrieved using the specified ID.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Add Data String Format");
        definition.setMetaDescription("Creates a new rendering format.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$InsertDataStringFormatOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The rendering format data object to be persisted.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_ID, "Format Link ID", "The ID for the rendering format which was generated.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Update Data String Format");
        definition.setMetaDescription("Updates an existing rendering format.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$UpdateDataStringFormatOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT, "Data String Format", "The rendering format data object to be persisted.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_DELETE_DATA_STRING_FORMAT);
        definition.setMetaDisplayName("Delete Data String Format");
        definition.setMetaDescription("Deletes an existing rendering format.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$DeleteDataStringFormatOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_FORMAT_ID, "Data String Format ID", "The data string format ID of the format to be deleted.", T8DataType.STRING));
        definitions.add(definition);

        definition = new T8JavaServerOperationDefinition(OPERATION_API_DATA_STRING_DELETE_DATA_STRINGS);
        definition.setMetaDisplayName("Delete Data Strings");
        definition.setMetaDescription("Deletes data strings associated with the details passed as parameters.");
        definition.setClassName("com.pilog.t8.data.document.datastring.T8DataStringOperations$DeleteDataStringsOperation");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_RECORD_ID_LIST, "Record ID List", "The list of record ID's for which the data strings should be deleted.", T8DataType.LIST, true));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DATA_STRING_INSTANCE_ID_LIST, "Data String Instance ID List", "The list of data string instance ID's for which the data strings should be deleted.", T8DataType.LIST, true));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_DELETED_DATA_STRING_COUNT, "Deleted Data String Count", "The number of data strings which have been deleted.", T8DataType.INTEGER));
        definitions.add(definition);

        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // DATA_STRING Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_INSTANCE_ID, "DATA_STRING_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING, "DATA_STRING", false, false, T8DataType.LONG_STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_STRING_ELEMENT Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_ELEMENT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING_ELEMENT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_FORMAT_ID, "DATA_STRING_FORMAT_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ELEMENT_ID, "ELEMENT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARENT_ELEMENT_ID, "PARENT_ELEMENT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEPENDENCY_ELEMENT_ID, "DEPENDENCY_ELEMENT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY, "PRIORITY", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_EXPRESSION, "EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LENGTH_RESTRICTION, "LENGTH_RESTRICTION", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TRUNCATION_TYPE, "TRUNCATION_TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PREFIX, "PREFIX", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SUFFIX, "SUFFIX", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEPARATOR, "SEPARATOR", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ACTIVE, "ACTIVE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STRICT_PARTICLE_INCLUSION, "STRICT_PARTICLE_INCLUSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NAME, "NAME", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DESCRIPTION, "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_GROUP_ID, "GROUP_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONDITION_EXPRESSION, "CONDITION_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_STRING_PARTICLE Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_PARTICLE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING_PARTICLE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARTICLE_ID, "PARTICLE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_ID, "RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_ID, "PROPERTY_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_TYPE_ID, "DATA_TYPE_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_TYPE_ID, "DATA_STRING_TYPE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY, "PRIORITY", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INCLUDE, "INCLUDE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_STRING_FORMAT Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_FORMAT_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING_FORMAT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_FORMAT_ID, "DATA_STRING_FORMAT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_NAME, "NAME", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DESCRIPTION, "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_STRING_FORMAT_LINK Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_FORMAT_LINK_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING_FORMAT_LINK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LINK_ID, "LINK_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_FORMAT_ID, "DATA_STRING_FORMAT_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TYPE, "TYPE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_KEY_ID, "DATA_KEY_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_ID, "PROPERTY_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_TYPE_ID, "DATA_TYPE_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_TYPE_ID, "DATA_STRING_TYPE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_STRING_INSTANCE Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_INSTANCE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING_INSTANCE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_INSTANCE_ID, "DATA_STRING_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_TYPE_ID, "DATA_STRING_TYPE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_AUTO_RENDER, "AUTO_RENDER", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_ID, "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_AT, "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPDATED_BY_ID, "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPDATED_BY_IID, "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_UPDATED_AT, "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // DATA_STRING_INSTANCE_LINK Data Source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_STRING_INSTANCE_LINK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ROOT_ORG_ID, "ROOT_ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_STRING_INSTANCE_ID, "DATA_STRING_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CONDITION_EXPRESSION, "CONDITION_EXPRESSION", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DATA_KEY_EXPRESSION, "DATA_KEY_EXPRESSION", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_SEQUENCE, "SEQUENCE", false, true, T8DataType.INTEGER));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // DATA_STRING Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_ID, DATA_STRING_DS_IDENTIFIER + F_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_INSTANCE_ID, DATA_STRING_DS_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING, DATA_STRING_DS_IDENTIFIER + F_DATA_STRING, false, false, T8DataType.LONG_STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_ID, DATA_STRING_DS_IDENTIFIER + F_INSERTED_BY_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_IID, DATA_STRING_DS_IDENTIFIER + F_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_AT, DATA_STRING_DS_IDENTIFIER + F_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPDATED_BY_ID, DATA_STRING_DS_IDENTIFIER + F_UPDATED_BY_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPDATED_BY_IID, DATA_STRING_DS_IDENTIFIER + F_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPDATED_AT, DATA_STRING_DS_IDENTIFIER + F_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_STRING_ELEMENT Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_ELEMENT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_ELEMENT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_FORMAT_ID, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_DATA_STRING_FORMAT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ELEMENT_ID, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_ELEMENT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARENT_ELEMENT_ID, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_PARENT_ELEMENT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEPENDENCY_ELEMENT_ID, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_DEPENDENCY_ELEMENT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_PRIORITY, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_EXPRESSION, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_EXPRESSION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LENGTH_RESTRICTION, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_LENGTH_RESTRICTION, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TRUNCATION_TYPE, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_TRUNCATION_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PREFIX, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_PREFIX, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SUFFIX, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_SUFFIX, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEPARATOR, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_SEPARATOR, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ACTIVE, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_ACTIVE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STRICT_PARTICLE_INCLUSION, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_STRICT_PARTICLE_INCLUSION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NAME, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_NAME, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DESCRIPTION, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_DESCRIPTION, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_GROUP_ID, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_GROUP_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONDITION_EXPRESSION, DATA_STRING_ELEMENT_DS_IDENTIFIER + F_CONDITION_EXPRESSION, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_STRING_PARTICLE Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_PARTICLE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_PARTICLE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARTICLE_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_PARTICLE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ROOT_ORG_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_PROPERTY_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FIELD_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_TYPE_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_DATA_TYPE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_TYPE_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_DATA_STRING_TYPE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_PRIORITY, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INCLUDE, DATA_STRING_PARTICLE_DS_IDENTIFIER + F_INCLUDE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_STRING_FORMAT Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_FORMAT_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_FORMAT_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_FORMAT_ID, DATA_STRING_FORMAT_DS_IDENTIFIER + F_DATA_STRING_FORMAT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, DATA_STRING_FORMAT_DS_IDENTIFIER + F_TYPE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_NAME, DATA_STRING_FORMAT_DS_IDENTIFIER + F_NAME, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DESCRIPTION, DATA_STRING_FORMAT_DS_IDENTIFIER + F_DESCRIPTION, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // DATA_STRING_FORMAT_LINK Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_FORMAT_LINK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LINK_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_LINK_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ROOT_ORG_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_ROOT_ORG_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_FORMAT_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_DATA_STRING_FORMAT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TYPE, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_TYPE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_KEY_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_DATA_KEY_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ROOT_RECORD_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_PROPERTY_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FIELD_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_TYPE_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_DATA_TYPE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_TYPE_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_DATA_STRING_TYPE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DATA_STRING_FORMAT_LINK_DS_IDENTIFIER + F_ORG_ID, false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // DATA_STRING_INSTANCE Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_INSTANCE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_INSTANCE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_INSTANCE_ID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_TYPE_ID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_DATA_STRING_TYPE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_LANGUAGE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_AUTO_RENDER, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_AUTO_RENDER, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_ID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_INSERTED_BY_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_IID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_AT, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPDATED_BY_ID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_UPDATED_BY_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPDATED_BY_IID, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_UPDATED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_UPDATED_AT, DATA_STRING_INSTANCE_DS_IDENTIFIER + F_UPDATED_AT, false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // DATA_STRING_INSTANCE_LINK Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ROOT_ORG_ID, DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER + F_ROOT_ORG_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER + F_DR_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_STRING_INSTANCE_ID, DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CONDITION_EXPRESSION, DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER + F_CONDITION_EXPRESSION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DATA_KEY_EXPRESSION, DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER + F_DATA_KEY_EXPRESSION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_SEQUENCE, DATA_STRING_INSTANCE_LINK_DS_IDENTIFIER + F_SEQUENCE, false, true, T8DataType.INTEGER));
        definitions.add(entityDefinition);

        return definitions;
    }
}
