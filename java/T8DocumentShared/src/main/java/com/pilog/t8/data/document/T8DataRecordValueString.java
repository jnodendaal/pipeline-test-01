package com.pilog.t8.data.document;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordValueString implements Serializable
{
    private String recordID;
    private String valueString;
    
    public T8DataRecordValueString(String recordID, String valueString)
    {
        this.recordID = recordID;
        this.valueString = valueString;
    }

    public String getRecordID()
    {
        return recordID;
    }

    public void setRecordID(String recordID)
    {
        this.recordID = recordID;
    }

    public String getValueString()
    {
        return valueString;
    }

    public void setValueString(String valueString)
    {
        this.valueString = valueString;
    }
}
