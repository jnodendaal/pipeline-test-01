package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8SubDataFilter;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8ValueFilterCriterion implements Serializable
{
    protected Object filterValue;
    protected RequirementType dataType;
    protected boolean caseInsensitive;
    protected DataFilterOperator operator;

    public T8ValueFilterCriterion(RequirementType dataType)
    {
        this.operator = DataFilterOperator.EQUAL;
        this.dataType = dataType;
    }

    public T8ValueFilterCriterion(RequirementType dataType, DataFilterOperator operator, Object filterValue)
    {
        this.dataType = dataType;
        this.operator = operator;
        this.filterValue = filterValue;
    }

    public boolean hasCriterion()
    {
        if (operator == DataFilterOperator.IS_NULL) return true; // This operator does not require a filter value.
        else if (operator == DataFilterOperator.IS_NOT_NULL) return true; // This operator does not require a filter value.
        else if (filterValue == null) return false; // All other operators require a filter value.
        else if (filterValue instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return !Strings.isNullOrEmpty((String)filterValue);
        }
        else return true;
    }

    public Object getFilterValue()
    {
        return filterValue;
    }

    public void setFilterValue(Object filterValue)
    {
        this.filterValue = filterValue;
    }

    public RequirementType getDataType()
    {
        return dataType;
    }

    public void setDataType(RequirementType dataType)
    {
        this.dataType = dataType;
    }

    public boolean isCaseInsensitive()
    {
        return caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive)
    {
        this.caseInsensitive = caseInsensitive;
    }

    public DataFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(DataFilterOperator operator)
    {
        this.operator = operator;
    }

    public abstract T8ValueFilterCriterion copy();

    public abstract String getRequiredCteId();

    public abstract StringBuffer getWhereClause(T8DataTransaction tx, T8DatabaseAdaptor adaptor);

    public abstract List<Object> getWhereClauseParameters(T8DataTransaction tx, T8DatabaseAdaptor adaptor);

    public static T8ValueFilterCriterion createCriterion(RequirementType type, DataFilterOperator operator, Object filterValue)
    {
        if (type == null)
        {
            return new T8DefaultValueFilterCriterion(type, operator, filterValue);
        }
        else
        {
            switch (type)
            {
                case CONTROLLED_CONCEPT:
                    return new T8ControlledConceptFilterCriterion(operator, filterValue);
                case DOCUMENT_REFERENCE:
                    return new T8DocumentReferenceFilterCriterion(operator, filterValue);
                default:
                    return new T8DefaultValueFilterCriterion(type, operator, filterValue);
            }
        }
    }

    public static StringBuilder getValueFilterWhereClause(T8DataTransaction tx, T8DatabaseAdaptor adaptor, String columnName, DataFilterOperator filterOperator, Object filterValue, boolean guidValue)
    {
        StringBuilder whereClause;

        // Append the value column name.
        whereClause = new StringBuilder();
        whereClause.append(columnName);
        whereClause.append(" ");

        // Append the filter value based on the specified operator.
        switch (filterOperator)
        {
            case EQUAL:
            {
                String parameterString;

                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                whereClause.append(filterValue == null ? " IS NULL" : (" = " + parameterString));
                break;
            }
            case NOT_EQUAL:
            {
                String parameterString;

                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                whereClause.append(filterValue == null ? " IS NOT NULL" : (" <> " + parameterString));
                break;
            }
            case GREATER_THAN:
            {
                whereClause.append(" > ?");
                break;
            }
            case GREATER_THAN_OR_EQUAL:
            {
                whereClause.append(" >= ?");
                break;
            }
            case LESS_THAN:
            {
                whereClause.append(" < ?");
                break;
            }
            case LESS_THAN_OR_EQUAL:
            {
                whereClause.append(" <= ?");
                break;
            }
            case LIKE:
            {
                whereClause.append(" LIKE ? ESCAPE '\\'");
                break;
            }
            case STARTS_WITH:
            {
                whereClause.append(" LIKE ? ESCAPE '\\'");
                break;
            }
            case ENDS_WITH:
            {
                whereClause.append(" LIKE ? ESCAPE '\\'");
                break;
            }
            case NOT_LIKE:
            {
                whereClause.append(" NOT LIKE ?");
                break;
            }
            case IS_NOT_NULL:
            {
                whereClause.append(" IS NOT NULL");
                break;
            }
            case IN:
            case NOT_IN:
            {
                if (filterValue instanceof Collection)
                {
                    Iterator<?> valueIterator;
                    Collection<?> valueCollection;
                    String parameterString;

                    parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                    valueCollection = (Collection)filterValue;
                    valueIterator = valueCollection.iterator();

                    whereClause.append((filterOperator == DataFilterOperator.IN) ? " IN (" : " NOT IN (");
                    while (valueIterator.hasNext())
                    {
                        valueIterator.next();
                        whereClause.append(parameterString);
                        if (valueIterator.hasNext()) whereClause.append(", ");
                    }
                    whereClause.append(")");
                    break;
                }
                else throw new RuntimeException("Invalid parameter type used with IN clause: " + filterValue);
            }
        }

        // Return the final where clause.
        return whereClause;
    }

    public static List<Object> getValueFilterParameters(T8DataTransaction tx, T8DatabaseAdaptor adaptor, DataFilterOperator operator, Object filterValue, boolean guidValue, boolean caseInsensitive)
    {
        List<Object> parameterList;

        parameterList = new ArrayList<>();
        switch (operator)
        {
            case LIKE:
            case NOT_LIKE:
            {
                String parameterString;

                parameterString = caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue.toString();
                parameterString = adaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;
                if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                parameterList.add(parameterString);
                break;
            }
            case STARTS_WITH:
            {
                String parameterString;

                parameterString = caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue.toString();
                parameterString = adaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                parameterList.add(parameterString);
                break;
            }
            case ENDS_WITH:
            {
                String parameterString;

                parameterString = caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue.toString();
                parameterString = adaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;

                parameterList.add(parameterString);
                break;
            }
            case IN:
            case NOT_IN:
            {
                if (filterValue != null)
                {
                    if (filterValue instanceof Collection)
                    {
                        Collection valueList;

                        // Get the value list.
                        valueList = (Collection)filterValue;

                        // If it's a guid field, we have to convert the filter value because of database vendor dependent implementations.
                        if (guidValue)
                        {
                            // Iterate over all values in the list and if a value is not null, convert it.
                            for (Object value : valueList)
                            {
                                if (value != null)
                                {
                                    parameterList.add(adaptor.convertHexStringToSQLGUIDString(value.toString()));
                                }
                                else
                                {
                                    parameterList.add(null);
                                }
                            }
                        }
                        else
                        {
                            parameterList.addAll(valueList);
                        }
                    }
                    else if (filterValue instanceof T8DataFilter) // A sub-query is used.
                    {
                        T8DataFilter subDataFilter;

                        subDataFilter = (T8DataFilter)filterValue;
                        parameterList.addAll(subDataFilter.getWhereClauseParameters(tx));
                    }
                    else if (filterValue instanceof T8SubDataFilter) // A sub-query is used.
                    {
                        T8SubDataFilter subDataFilter;

                        subDataFilter = (T8SubDataFilter)filterValue;
                        parameterList.addAll(subDataFilter.getDataFilter().getWhereClauseParameters(tx));
                    }
                }

                break;
            }
            case IS_NULL:
            case IS_NOT_NULL:
            {
                // Do nothing, since no filter value is required for this operator type.
                break;
            }
            case MATCHES:
            {
                // Do nothing, since no filter value is required for this operator type.
                break;
            }
            case EQUAL:
            {
                // Do nothing, since no filter value is required for this operator type.
                if (filterValue == null) break;
            }
            default:
            {
                if (filterValue instanceof String)
                {
                    // If it's a guid value, we have to convert the filter value because of database vendor dependent implementations.
                    if (guidValue)
                    {
                        parameterList.add(adaptor.convertHexStringToSQLGUIDString(filterValue.toString()));
                        break;
                    }
                    else
                    {
                        parameterList.add(caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue);
                        break;
                    }
                }
                else
                {
                    // If it's a guid value, we have to convert the filter value because of database vendor dependent implementations.
                    if (guidValue)
                    {
                        parameterList.add(adaptor.convertHexStringToSQLGUIDString(filterValue.toString()));
                        break;
                    }
                    else
                    {
                        parameterList.add(filterValue);
                        break;
                    }
                }
            }
        }

        return parameterList;
    }
}
