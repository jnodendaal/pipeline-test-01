package com.pilog.t8.data.document;

import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface CachedTerminologyProvider extends TerminologyProvider, TerminologyCollector
{
    /**
     * Clears all of the terminology which has been cached. Subsequent requests
     * will all require database retrieval in order to re-populate the cache.
     */
    public void clearCache();

    /**
     * Adds the specified terminology to the cache if it is not yet present.
     * Depending on the implementation, the existing cache value could be
     * replaced or left as-is.
     *
     * @param terminology The {@code T8ConceptTerminology} to add to the cache
     */
    public void addTerminology(T8ConceptTerminology terminology);

    /**
     * Adds each of the {@code T8ConceptTerminology} values to the cache without
     * any database retrieval required.
     *
     * @param terminology The list of {@code T8ConceptTerminology} objects to be
     *      cached
     */
    @Override
    public void addTerminology(List<T8ConceptTerminology> terminology);

    /**
     * Caches all of the specified terminology. Depending on the implementation,
     * the existing cached values could be replaced, or the values not yet
     * present will be added.
     *
     * @param languageIDList The {@code Collection} of language identifiers to
     *      be used for concepts to be cached
     * @param conceptIDList The {@code Collection} of concept ID's to be cached
     */
    public void cacheTerminology(Collection<String> languageIDList, Collection<String> conceptIDList);
}
