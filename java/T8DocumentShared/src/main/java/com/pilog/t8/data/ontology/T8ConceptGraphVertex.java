package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptGraphVertex implements Serializable
{
    private final List<T8ConceptGraphVertex> inVertices;
    private final List<T8ConceptGraphVertex> outVertices;
    private String conceptID;
    
    public T8ConceptGraphVertex(String conceptID)
    {
        this.conceptID = conceptID;
        this.inVertices = new ArrayList<T8ConceptGraphVertex>();
        this.outVertices = new ArrayList<T8ConceptGraphVertex>();
    }
    
    public String getConceptID()
    {
        return conceptID;
    }
    
    public boolean containsInConcept(String conceptID)
    {
        for (T8ConceptGraphVertex inVertex : inVertices)
        {
            if (conceptID.equals(inVertex.getConceptID())) return true;
        }
        
        return false;
    }
    
    public boolean containsOutConcept(String conceptID)
    {
        for (T8ConceptGraphVertex outVertex : outVertices)
        {
            if (conceptID.equals(outVertex.getConceptID())) return true;
        }
        
        return false;
    }
    
    public void addInVertex(T8ConceptGraphVertex vertex)
    {
        inVertices.add(vertex);
    }
    
    public List<T8ConceptGraphVertex> getInVertices()
    {
        return new ArrayList<T8ConceptGraphVertex>(inVertices);
    }
    
    public void addOutVertex(T8ConceptGraphVertex vertex)
    {
        outVertices.add(vertex);
    }
    
    public List<T8ConceptGraphVertex> getOutVertices()
    {
        return new ArrayList<T8ConceptGraphVertex>(outVertices);
    }
}
