package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8DataRecordExternalNumberingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "RP_MAP_EXT_NUMBER";
    public static final String IDENTIFIER_PREFIX = "RP_MAP_EXT_NUM_";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_EXTERNAL_NUMBER";
    public static final String DISPLAY_NAME = "Data Record External Numbering Mapping";
    public static final String DESCRIPTION = "A Data Record External Numbering mapping that defines an External Number for this record will be mapped";

    public enum Datum
    {
        ENABLED_EXPRESSION,
        VALUE_TYPE,
        DOC_PATH_EXPRESSION,
        VALUE_EXPRESSION
    };
    
    public enum ExternalNumberTypes
    {
        VALUE_STRING,
        INPUT_PARAMETER,
        DATA_KEY        
    };
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public T8DataRecordExternalNumberingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_EXPRESSION.toString(), "Enabled Expression", "The xpression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.VALUE_TYPE.toString(), "External Number Type", "The External Number types of mapping that will be performed on Data Record", ExternalNumberTypes.VALUE_STRING.toString(), T8DefinitionDatumType.T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOC_PATH_EXPRESSION.toString(), "Doc Path Expression", "The doc path expression that will be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.VALUE_EXPRESSION.toString(), "Value Expression", "The value expression that will be evaluated."));
        
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        if(Datum.VALUE_TYPE.toString().equals(datumIdentifier)) return createStringOptions(ExternalNumberTypes.values());
        else return null;
    }

    public String getEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.ENABLED_EXPRESSION.toString());
    }

    public void setEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_EXPRESSION.toString(), expression);
    }

    public ExternalNumberTypes getValueType()
    {
        return ExternalNumberTypes.valueOf((String)getDefinitionDatum(Datum.VALUE_TYPE.toString()));
    }

    public void setValueType(ExternalNumberTypes type)
    {
        setDefinitionDatum(Datum.VALUE_TYPE.toString(), type.toString());
    }

    public String getDocPathExpression()
    {
        return (String)getDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString());
    }

    public void setDocPathExpression(String propertyID)
    {
        setDefinitionDatum(Datum.DOC_PATH_EXPRESSION.toString(), propertyID);
    }

    public String getValueExpression()
    {
        return (String)getDefinitionDatum(Datum.VALUE_EXPRESSION.toString());
    }

    public void setEValueExpression(String valueExpression)
    {
        setDefinitionDatum(Datum.VALUE_EXPRESSION.toString(), valueExpression);
    }
}
