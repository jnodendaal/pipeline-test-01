package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class Composite extends RecordValue
{
    private final CompositeRequirement compositeRequirement;

    public Composite(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
        this.compositeRequirement = (CompositeRequirement)valueRequirement;
    }

    public void clearContent()
    {
        for (Field field : getFields())
        {
            if (field.getValueRequirement().getRequirementType() != RequirementType.DOCUMENT_REFERENCE)
            {
                field.setFieldValue(null);
            }
        }
    }

    public List<Field> getFields()
    {
        return (List)getSubValues(RequirementType.FIELD_TYPE, null, null);
    }

    public Field getField(String fieldId)
    {
        return (Field)getSubValue(RequirementType.FIELD_TYPE, fieldId, null);
    }

    public Field getOrAddField(String fieldId)
    {
        Field field;

        field = getField(fieldId);
        if (field != null)
        {
            return field;
        }
        else
        {
            return addField(fieldId);
        }
    }

    public Field addField(String fieldId)
    {
        ValueRequirement fieldRequirement;

        fieldRequirement = valueRequirement.getSubRequirement(RequirementType.FIELD_TYPE, fieldId, null);
        if (fieldRequirement != null)
        {
            Field field;

            field = new Field(fieldRequirement);
            setSubValue(field);
            return field;
        }
        else throw new RuntimeException("Property requirement '" + valueRequirement.getParentPropertyRequirement() + "' does not specify the field requirement: " + fieldId);
    }

    public void setField(Field field)
    {
        setSubValue(field);
    }

    public RecordValue getOrAddFieldValue(String fieldId)
    {
        Field field;

        field = getOrAddField(fieldId);
        return field.getOrAddValue();
    }

    public RecordValue setFieldValue(String fieldId, String value, boolean clearDependants)
    {
        Field field;

        // Get the field value or create it if it does not yet exist.
        field = getField(fieldId);
        if (field == null) field = addField(fieldId);

        // Now set the value of the field.
        return field.setValue(value, clearDependants);
    }

    public DocumentReferenceList addRecordReference(String fieldID, String recordID)
    {
        Field field;

        // Get the field value or create it if it does not yet exist.
        field = getField(fieldID);
        if (field == null) field = addField(fieldID);

        // Now set the value of the field.
        return field.addRecordReference(recordID);
    }

    public void removeRecordReference(String fieldID, String recordID)
    {
        Field field;

        // Get the field value and remove the document reference if it exists
        field = getField(fieldID);
        if (field != null) field.removeRecordReference(recordID);
    }

    /**
     * Fills this composite value ensuring that at all fields specified by the requirement
     * exist as {@code Field} objects in this composite.
     */
    public void fill()
    {
        for (FieldRequirement fieldRequirement : compositeRequirement.getFieldRequirements())
        {
            getOrAddField(fieldRequirement.getFieldID());
        }
    }

    @Override
    public CompositeContent getContent()
    {
        CompositeContent content;

        content = new CompositeContent();
        for (Field field : getFields())
        {
            content.addField(field.getContent());
        }

        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        CompositeContent content;

        content = (CompositeContent)valueContent;
        for (FieldContent fieldContent : content.getFields())
        {
            Field field;

            field = addField(fieldContent.getId());
            field.setContent(fieldContent);
        }
    }
}
