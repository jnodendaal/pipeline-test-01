package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class TimeValueContent extends ValueContent
{
    private String time;

    public TimeValueContent()
    {
        super(RequirementType.TIME_TYPE);
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof TimeValueContent)
        {
            TimeValueContent toTimeContent;

            toTimeContent = (TimeValueContent)toContent;
            return Objects.equals(time, toTimeContent.getTime());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof TimeValueContent)
        {
            TimeValueContent toTimeContent;

            toTimeContent = (TimeValueContent)toContent;
            return Objects.equals(time, toTimeContent.getTime());
        }
        else return false;
    }
}
