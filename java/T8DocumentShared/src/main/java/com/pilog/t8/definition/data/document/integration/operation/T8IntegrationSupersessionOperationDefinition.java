package com.pilog.t8.definition.data.document.integration.operation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceOperationMappingDefinition;
import static com.pilog.t8.definition.data.document.integration.T8IntegrationServiceOperationMappingDefinition.P_OPERATION_IID;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8IntegrationSupersessionOperationDefinition extends T8IntegrationServiceOperationMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTEGRATION_SERVICE_OPERATION_MAPPING_SUPERSESSION";
    public static final String DISPLAY_NAME = "Super Session Operation";
    public static final String DESCRIPTION = "An operation mapping that defines the creation operation";

    public enum Datum
    {
    };
    // -------- Definition Meta-Data -------- //

    public static final String P_SUPERSESSIONS = "$P_SUPERSESSIONS";

    public T8IntegrationSupersessionOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    protected List<T8DataParameterDefinition> getOperationInputParameters()
    {
        List<T8DataParameterDefinition> parameterDefinitions;

        parameterDefinitions = new ArrayList<>();
        parameterDefinitions.add(new T8DataParameterDefinition(P_SUPERSESSIONS, "Super Sessions", "A Map containing the super sessions (supersededRecord -> supersedingRecord)", new T8DtMap(T8DataType.CUSTOM_OBJECT, T8DataType.CUSTOM_OBJECT)));
        parameterDefinitions.add(new T8DataParameterDefinition(P_OPERATION_IID, "Operation Instance ID", "The Call and Call Back unique Operation ID.", T8DataType.GUID));

        return parameterDefinitions;
    }
}
