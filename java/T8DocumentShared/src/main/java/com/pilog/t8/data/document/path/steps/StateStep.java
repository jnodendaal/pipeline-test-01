package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;

/**
 * @author Bouwer du Preez
 */
public class StateStep extends DefaultPathStep implements PathStep
{
    public StateStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, T8DataObjectProvider dataObjectProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, dataObjectProvider, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            T8DataObjectState state;
            DataRecord dataRecord;

            dataRecord = (DataRecord)input;
            state = dataObjectProvider.getDataObjectState(dataRecord.getID());
            if (state != null)
            {
                return ArrayLists.newArrayList(state);
            }
            else return new ArrayList<>();
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<T8DataObjectState> evaluateStep(List<T8DataObjectState> states)
    {
        // TODO: Implement some kind of predicate evaluation.

        // Return the result list.
        return states;
    }
}
