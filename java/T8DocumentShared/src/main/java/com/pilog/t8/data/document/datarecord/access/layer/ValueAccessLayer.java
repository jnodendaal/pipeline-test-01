package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public abstract class ValueAccessLayer implements Serializable
{
    protected final RequirementType requirementType;
    protected FieldAccessLayer parentFieldAccessLayer;
    protected PropertyAccessLayer parentPropertyAccessLayer;

    public ValueAccessLayer(RequirementType requirementType)
    {
        this.requirementType = requirementType;
    }

    public RequirementType getRequirementType()
    {
        return requirementType;
    }

    void setParentFieldAccessLayer(FieldAccessLayer fieldAccessLayer)
    {
        this.parentFieldAccessLayer = fieldAccessLayer;
    }

    void setParentPropertyAccessLayer(PropertyAccessLayer propertyAccessLayer)
    {
        this.parentPropertyAccessLayer = propertyAccessLayer;
    }

    public FieldAccessLayer getParentFieldAccessLayer()
    {
        return parentFieldAccessLayer;
    }

    public PropertyAccessLayer getParentPropertyAccessLayer()
    {
        return parentPropertyAccessLayer;
    }

    public abstract boolean isValueAccessEquivalent(ValueAccessLayer value);
    public abstract ValueAccessLayer copy();
    public abstract void setAccess(ValueAccessLayer newAccess);
}
