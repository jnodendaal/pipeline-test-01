package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;

/**
 * @author Bouwer du Preez
 */
public class LowerBoundNumber extends RecordValue
{
    public LowerBoundNumber(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public void setValue(Integer value)
    {
        if (value != null)
        {
            super.setValue(value.toString());
        }
        else super.setValue(null);
    }

    public void setValue(Long value)
    {
        if (value != null)
        {
            super.setValue(value.toString());
        }
        else super.setValue(null);
    }

    @Override
    public LowerBoundNumberContent getContent()
    {
        LowerBoundNumberContent content;

        content = new LowerBoundNumberContent();
        content.setValue(value);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        LowerBoundNumberContent content;

        content = (LowerBoundNumberContent)valueContent;
        setValue(content.getValue());
    }
}
