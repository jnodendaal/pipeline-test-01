package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.io.Serializable;

/**
 * This is the abstract class implementing common functionality for all value content sub-types.
 * ValueContent classes carry only String type data to ensure that any form of a value, not just the
 * correct format and type, can be stored.  Example; a BooleanValueContent does not carry its data
 * as a Boolean type to ensure that values such as 'n', 'Y', '1', '0' can be accommodated.
 *
 * @author Bouwer du Preez
 */
public abstract class ValueContent implements Serializable
{
    private RequirementType type;
    private PropertyContent property;

    public ValueContent(RequirementType type)
    {
        this.type = type;
    }

    void setProperty(PropertyContent property)
    {
        this.property = property;
    }

    public PropertyContent getProperty()
    {
        return property;
    }

    public RecordContent getRecord()
    {
        return property != null ? property.getRecord() : null;
    }

    public RequirementType getType()
    {
        return type;
    }

    public void setType(RequirementType type)
    {
        this.type = type;
    }

    public abstract boolean isEqualTo(ValueContent toContent);
    public abstract boolean isEquivalentTo(ValueContent toContent);
}
