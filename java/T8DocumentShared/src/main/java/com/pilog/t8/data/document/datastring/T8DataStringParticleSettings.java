package com.pilog.t8.data.document.datastring;

import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.org.T8OrganizationStructure.OrganizationSetType;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringParticleSettings implements Serializable
{
    private String orgId;
    private String id;
    private String drId;
    private String recordId;
    private String propertyId;
    private String fieldId;
    private String dataTypeId;
    private String languageId;
    private String dsTypeId;
    private String drInstanceId;
    private int sequence;
    private int priority;
    private boolean include;

    public T8DataStringParticleSettings(String id)
    {
        this.id = id;
        this.include = true;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String particleId)
    {
        this.id = particleId;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getDrId()
    {
        return drId;
    }

    public void setDrId(String drId)
    {
        this.drId = drId;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getDataTypeId()
    {
        return dataTypeId;
    }

    public void setDataTypeId(String dataTypeId)
    {
        this.dataTypeId = dataTypeId;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getDsTypeId()
    {
        return dsTypeId;
    }

    public void setDsTypeId(String dsTypeId)
    {
        this.dsTypeId = dsTypeId;
    }

    public int getSequence()
    {
        return sequence;
    }

    public void setSequence(int sequence)
    {
        this.sequence = sequence;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public boolean isInclude()
    {
        return include;
    }

    public void setInclude(boolean include)
    {
        this.include = include;
    }

    /**
     * This method evaluates the input settings object and determines whether
     * or not it is applicable to the same target as this object.
     * The target of a settings object is the combination of the following fields:
     * -orgId;
     * -drId;
     * -drInstanceId;
     * -recordId;
     * -propertyId;
     * -fieldId;
     * -dataTypeId;
     * -languageId;
     * -dsInstanceId;
     * -dataStringTypeId;
     *
     * @param settings The settings object to compare to this object.
     * @return Boolean true if the input object is applicable to exactly the same target as this object.
     */
    public boolean isEqualTarget(T8DataStringParticleSettings settings)
    {
        if (!Objects.equals(orgId, settings.getOrgId())) return false;
        else if (!Objects.equals(drId, settings.getDrId())) return false;
        else if (!Objects.equals(drInstanceId, settings.getDrInstanceId())) return false;
        else if (!Objects.equals(recordId, settings.getRecordId())) return false;
        else if (!Objects.equals(propertyId, settings.getPropertyId())) return false;
        else if (!Objects.equals(fieldId, settings.getFieldId())) return false;
        else if (!Objects.equals(dataTypeId, settings.getDataTypeId())) return false;
        else if (!Objects.equals(languageId, settings.getLanguageId())) return false;
        else if (!Objects.equals(dsTypeId, settings.getDsTypeId())) return false;
        else return true;
    }

    public boolean isRecordParticle()
    {
        if (dataTypeId != null) return false;
        else if (fieldId != null) return false;
        else if (propertyId != null) return false;
        else return recordId != null;
    }

    public boolean isPropertyParticle()
    {
        if (dataTypeId != null) return false;
        else if (fieldId != null) return false;
        else return propertyId != null;
    }

    public boolean isFieldParticle()
    {
        if (dataTypeId != null) return false;
        else return fieldId != null;
    }

    public boolean isDataTypeParticle()
    {
        return dataTypeId != null;
    }

    public boolean matchesDataStringInstance(T8DataStringGeneratorContext dsContext, T8DataStringInstance dsInstance)
    {
        T8OrganizationStructure orgStructure;
        List<String> instanceOrgIdSet;

        // Get the set of organizations included by the data string instance.
        orgStructure = dsContext.getOrganizationStructure();
        instanceOrgIdSet = orgStructure.getOrganizationIdSet(OrganizationSetType.FAMILY, dsInstance.getOrgId());

        // If any of the data string specifications of this particle is set and does not match the corresponding value in die supplied instance, the instance is not a match.
        if ((orgId != null) && (!instanceOrgIdSet.contains(orgId))) return false;
        else if ((dsTypeId != null) && (!dsTypeId.equals(dsInstance.getTypeID()))) return false;
        else if ((languageId != null) && (!languageId.equals(dsInstance.getLanguageID()))) return false;
        else return true;
    }

    public T8DataStringParticleSettings copy()
    {
        T8DataStringParticleSettings copy;

        copy = new T8DataStringParticleSettings(id);
        copy.orgId = orgId;
        copy.drId = drId;
        copy.recordId = recordId;
        copy.propertyId = propertyId;
        copy.fieldId = fieldId;
        copy.dataTypeId = dataTypeId;
        copy.languageId = languageId;
        copy.dsTypeId = dsTypeId;
        copy.sequence = sequence;
        copy.priority = priority;
        copy.include = include;
        return copy;
    }

    @Override
    public String toString()
    {
        return "T8DataStringParticleSettings{" + "orgId=" + orgId + ", id=" + id + ", drId=" + drId + ", recordId=" + recordId + ", propertyId=" + propertyId + ", fieldId=" + fieldId + ", dataTypeId=" + dataTypeId + ", languageId=" + languageId + ", dataStringTypeId=" + dsTypeId + ", drInstanceId=" + drInstanceId + ", sequence=" + sequence + ", priority=" + priority + ", include=" + include + '}';
    }
}
