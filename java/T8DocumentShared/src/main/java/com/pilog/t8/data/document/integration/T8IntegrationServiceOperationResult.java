package com.pilog.t8.data.document.integration;

import com.pilog.t8.data.document.integration.structure.sap.SAPTable;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8IntegrationServiceOperationResult
{
    private String opertionID;
    private String recordID;
    private String externalReference;
    private Map<String, String> keyMap;
    private boolean success;
    private String errorCode;
    private String exceptionMessage;
    private List<SAPTable> sapTables;

    public T8IntegrationServiceOperationResult()
    {
    }

    public T8IntegrationServiceOperationResult(String opertionID,
                                               String recordID,
                                               String externalReference,
                                               Map<String, String> keyMap,
                                               boolean success,
                                               String errorCode,
                                               String exceptionMessage,
                                               List<SAPTable> sapTables)
    {
        this.opertionID = opertionID;
        this.recordID = recordID;
        this.externalReference = externalReference;
        this.keyMap = keyMap;
        this.success = success;
        this.errorCode = errorCode;
        this.exceptionMessage = exceptionMessage;
        this.sapTables = sapTables;
    }

    public String getOpertionID()
    {
        return opertionID;
    }

    public void setOpertionID(String opertionID)
    {
        this.opertionID = opertionID;
    }

    public String getRecordID()
    {
        return recordID;
    }

    public void setRecordID(String recordID)
    {
        this.recordID = recordID;
    }

    /**
     *
     * This field will return the ERP system reference after creation,
     * and will always have it available on Updates, Extensions and any
     * Flag operations.
     *
     * @return externalReference
     **/

    public String getExternalReference()
    {
        return externalReference;
    }

    public void setExternalReference(String externalReference)
    {
        this.externalReference = externalReference;
    }

    public Map<String, String> getKeyMap()
    {
        return keyMap;
    }

    public void setKeyMap(Map<String, String> keyMap)
    {
        this.keyMap = keyMap;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage)
    {
        this.exceptionMessage = exceptionMessage;
    }
    
    /**
     * @return the sapTables
     */
    public List<SAPTable> getSapTables()
    {
        return sapTables;
    }

    /**
     * @param sapTables the sapTables to set
     */
    public void setSapTables(List<SAPTable> sapTables)
    {
        this.sapTables = sapTables;
    }
}
