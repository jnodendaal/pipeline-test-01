package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationService;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8IntegrationServiceDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTEGRATION_SERVICE";
    public static final String GROUP_IDENTIFIER = "@DG_INTEGRATION_SERVICE";
    public static final String STORAGE_PATH = "/integration_services";
    public static final String DISPLAY_NAME = "Intergation Service";
    public static final String DESCRIPTION = "A definition that specifies the details involved in an interface service call.";
    public static final String IDENTIFIER_PREFIX = "XFACE_";
    public enum Datum {DATA_RECORD_MAPPINGS,
                       OPERATIONS};
    // -------- Definition Meta-Data -------- //

    public T8IntegrationServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.DATA_RECORD_MAPPINGS.toString(), "Data Record Mappings",  "The data record mapping for the root record."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OPERATIONS.toString(), "Operations",  "The operations available for this integration service."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_RECORD_MAPPINGS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataRecordMappingDefinition.TYPE_IDENTIFIER));
        if (Datum.OPERATIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8IntegrationServiceOperationMappingDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public <T> DataRecordIntegrationService<T> getNewIntegrationServiceInstance(T8Context context) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.document.integration.T8DefaultIntegrationService").getConstructor(T8Context.class, T8IntegrationServiceDefinition.class);
        return (DataRecordIntegrationService<T>)constructor.newInstance(context, this);
    }

    public List<T8DataRecordMappingDefinition> getDataRecordMappingDefinitions()
    {
        return (List<T8DataRecordMappingDefinition>) getDefinitionDatum(Datum.DATA_RECORD_MAPPINGS.toString());
    }

    public void setDataRecordMappingDefinitions(List<T8DataRecordMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.DATA_RECORD_MAPPINGS.toString(), definitions);
    }

    public List<T8IntegrationServiceOperationMappingDefinition> getOperationDefinitions()
    {
        return (List<T8IntegrationServiceOperationMappingDefinition>) getDefinitionDatum(Datum.OPERATIONS.toString());
    }

    public void setOperationDefinitions(List<T8IntegrationServiceOperationMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.OPERATIONS.toString(), definitions);
    }
}
