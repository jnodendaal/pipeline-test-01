package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class AbbreviationStep extends DefaultPathStep implements PathStep
{
    public AbbreviationStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getID());
        }
        else if (input instanceof RecordProperty)
        {
            return evaluateStep(((RecordProperty)input).getPropertyID());
        }
        else if (input instanceof RecordValue)
        {
            RecordValue recordValue;

            recordValue = (RecordValue)input;
            if (recordValue.isField())
            {
                return evaluateStep(recordValue.getFieldID());
            }
            else
            {
                return evaluateStep(recordValue.getValueConceptID());
            }
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateStep(((DataRequirement)input).getConceptID());
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateStep(((DataRequirementInstance)input).getConceptID());
        }
        else if (input instanceof PropertyRequirement)
        {
            return evaluateStep(((PropertyRequirement)input).getConceptID());
        }
        else if (input instanceof ValueRequirement)
        {
            return evaluateStep(((ValueRequirement)input).getValue());
        }
        else if (input instanceof String)
        {
            String inputConceptID;

            inputConceptID = (String)input;
            if (T8IdentifierUtilities.isConceptID(inputConceptID))
            {
                return evaluateStep(inputConceptID);
            }
            else throw new RuntimeException("Abbreviation step '" + stepString + "' cannot be executed from content object: " + input);
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                if (inputObject != null)
                {
                    outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
                }
                else if (modifiers.isIncludeNullValues())
                {
                    outputList.add(null);
                }
            }

            return outputList;
        }
        else throw new RuntimeException("Abbreviation step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(String conceptId)
    {
        if (conceptId == null)
        {
            return new ArrayList<>();
        }
        else
        {
            List<String> abbreviations;

            // Create a list to hold the abbreviations.
            abbreviations = new ArrayList<String>();

            // Fetch the abbreviations required.
            if (terminologyProvider != null)
            {
                // If we have a list of language ID's use it, otherwise just fetch the default abbreviation.
                if (idList != null)
                {
                    for (String id : idList)
                    {
                        String abbreviation;

                        abbreviation = terminologyProvider.getAbbreviation(id, conceptId);
                        if (abbreviation != null) abbreviations.add(abbreviation);
                    }
                }
                else
                {
                    String abbreviation;

                    abbreviation = terminologyProvider.getAbbreviation(null, conceptId);
                    if (abbreviation != null) abbreviations.add(abbreviation);
                }
            }
            else throw new RuntimeException("Document Path requires terminology provider, but none is set.");

            // Now run through the remaining abbreviation list and evaluate the predicate expression (if any).
            if (predicateExpressionEvaluator != null)
            {
                Iterator<String> abbreviationIterator;

                // Iterator over the abbreviations and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
                abbreviationIterator = abbreviations.iterator();
                while (abbreviationIterator.hasNext())
                {
                    Map<String, Object> inputParameters;
                    String nextAbbreviation;

                    // Create a map containing the input parameters available to the predicate expression.
                    nextAbbreviation = abbreviationIterator.next();
                    inputParameters = new HashMap<String, Object>();
                    inputParameters.put("ABBREVIATION", nextAbbreviation);

                    // Evaluate the predicate.
                    try
                    {
                        if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                        {
                            abbreviationIterator.remove();
                        }
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                    }
                }
            }

            return abbreviations;
        }
    }
}
