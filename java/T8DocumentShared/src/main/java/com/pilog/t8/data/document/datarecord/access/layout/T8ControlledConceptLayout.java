package com.pilog.t8.data.document.datarecord.access.layout;

import com.pilog.t8.definition.data.document.datarecord.access.layout.T8ControlledConceptLayoutDefinition;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ControlledConceptLayout implements Serializable
{
    private ControlledConceptLayoutType layoutType;
    
    public enum ControlledConceptLayoutType {COMBO_BOX, RADIO_BUTTONS};
    
    public T8ControlledConceptLayout(T8ControlledConceptLayoutDefinition definition)
    {
        this.layoutType = definition.getLayoutType();
    }
    
    public ControlledConceptLayoutType getLayoutType()
    {
        return layoutType;
    }
    
    public void setLayoutType(ControlledConceptLayoutType layoutType)
    {
        this.layoutType = layoutType;
    }
}
