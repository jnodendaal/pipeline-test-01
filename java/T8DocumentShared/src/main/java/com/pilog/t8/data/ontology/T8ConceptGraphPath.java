package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptGraphPath implements Serializable
{
    private final LinkedList<String> conceptIdPath;

    public T8ConceptGraphPath()
    {
        this.conceptIdPath = new LinkedList<String>();
    }

    public int size()
    {
        return conceptIdPath.size();
    }

    public boolean isFirstConceptId(String conceptId)
    {
        return (conceptId.equals(conceptIdPath.peekFirst()));
    }

    public boolean isLastConceptId(String conceptId)
    {
        return (conceptId.equals(conceptIdPath.peekLast()));
    }

    public String getFirstConceptId()
    {
        return conceptIdPath.size() > 0 ? conceptIdPath.getFirst() : null;
    }

    public String getLastConceptId()
    {
        return conceptIdPath.size() > 0 ? conceptIdPath.getLast() : null;
    }

    public boolean canPathBeAdded(T8ConceptPair edge)
    {
        String tailConceptId;
        String headConceptId;

        // Get the concept ID's of the edge.
        tailConceptId = edge.getLeftConceptId();
        headConceptId = edge.getRightConceptId();

        // If the path is empty, just add the new edge.
        if (conceptIdPath.isEmpty()) return true;
        else if (conceptIdPath.peekFirst().equals(headConceptId)) return true; // Path not empty, edge can be added to start of path.
        else if (conceptIdPath.peekLast().equals(tailConceptId)) return true;// Path not empty, edge can be added to end of path.
        else return false;
    }

    public void addPath(List<String> pathIdList)
    {
        if ((pathIdList != null) && (!pathIdList.isEmpty()))
        {
            if (conceptIdPath.isEmpty())
            {
                conceptIdPath.addAll(pathIdList);
            }
            else if (conceptIdPath.peekFirst().equals(pathIdList.get(pathIdList.size()-1)))
            {
                conceptIdPath.addAll(0, pathIdList);
            }
            else if (conceptIdPath.peekLast().equals(pathIdList.get(0)))
            {
                conceptIdPath.addAll(conceptIdPath.size(), pathIdList);
            }
            throw new IllegalArgumentException("Path ID's could not be added to path '" + conceptIdPath + "': " + pathIdList);
        }
    }

    public void addPathEdges(List<T8ConceptPair> edges)
    {
        LinkedList<T8ConceptPair> edgeQueue;
        boolean added;

        added = true;
        edgeQueue = new LinkedList<T8ConceptPair>(edges);
        while (added)
        {
            added = false;
            for (T8ConceptPair edge : edgeQueue)
            {
                if (canPathBeAdded(edge))
                {
                    added = true;
                    addPath(edge);
                    edgeQueue.remove(edge);
                    break;
                }
            }
        }

        if (edgeQueue.size() > 0)
        {
            throw new IllegalArgumentException("Edges could not be added to path '" + conceptIdPath + "': " + edgeQueue);
        }
    }

    public void addPath(T8ConceptPair edge)
    {
        String tailConceptId;
        String headConceptId;

        // Get the concept ID's of the edge.
        tailConceptId = edge.getLeftConceptId();
        headConceptId = edge.getRightConceptId();

        // If the path is empty, just add the new edge.
        if (conceptIdPath.isEmpty())
        {
            conceptIdPath.add(tailConceptId);
            conceptIdPath.add(headConceptId);
        }
        else if (conceptIdPath.peekFirst().equals(headConceptId)) // Path not empty, edge can be added to start of path.
        {
            conceptIdPath.addFirst(tailConceptId);
        }
        else if (conceptIdPath.peekLast().equals(tailConceptId)) // Path not empty, edge can be added to end of path.
        {
            conceptIdPath.addLast(tailConceptId);
        }
        else throw new IllegalArgumentException("Edge '" + edge + "' can not be added to path: " + conceptIdPath);
    }

    public void addHeadConceptId(String conceptId)
    {
        conceptIdPath.addLast(conceptId);
    }

    public void addTailConceptId(String conceptId)
    {
        conceptIdPath.addFirst(conceptId);
    }

    public T8ConceptGraphPath getParentPath(String conceptId)
    {
        int index;

        index = conceptIdPath.indexOf(conceptId);
        if (index > -1)
        {
            T8ConceptGraphPath parentPath;

            parentPath = new T8ConceptGraphPath();
            for (int i = 0; i < index; i++)
            {
                parentPath.addHeadConceptId(conceptIdPath.get(i));
            }

            return parentPath;
        }
        else throw new IllegalArgumentException("Concept Id '" + conceptId + "' not found in path: " + conceptIdPath);
    }

    @Override
    public String toString()
    {
        return "T8ConceptGraphPath{" + "conceptIdPath=" + conceptIdPath + '}';
    }
}
