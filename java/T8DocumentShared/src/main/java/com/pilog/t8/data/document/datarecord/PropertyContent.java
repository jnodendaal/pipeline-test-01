package com.pilog.t8.data.document.datarecord;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class PropertyContent implements Serializable
{
    private String id;
    private String irdi;
    private String fft;
    private ValueContent value;
    private RecordContent record;

    public PropertyContent()
    {
    }

    void setRecord(RecordContent record)
    {
        this.record = record;
    }

    public RecordContent getRecord()
    {
        return record;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public String getFft()
    {
        return fft;
    }

    public void setFft(String fft)
    {
        this.fft = fft;
    }

    public ValueContent getValue()
    {
        return value;
    }

    public void setValue(ValueContent value)
    {
        // If a value is currently set, remove the reference to this property.
        if (this.value != null) this.value.setProperty(null);

        // Set the new value.
        this.value = value;

        // If the new value is not null, set the property reference to this property.
        if (this.value != null) this.value.setProperty(this);
    }

    public boolean isEqualTo(PropertyContent toContent)
    {
        if (toContent != null)
        {
            if (!Objects.equals(toContent.getId(), id)) return false;
            else if (!Objects.equals(toContent.getIrdi(), irdi)) return false;
            else if (!Objects.equals(toContent.getFft(), fft)) return false;
            else if (value == null) return toContent.getValue() == null;
            else return value.isEqualTo(toContent.getValue());
        }
        else return false;
    }

    public boolean isEquivalentTo(PropertyContent toContent)
    {
        if (toContent != null)
        {
            if (!Objects.equals(toContent.getId(), id)) return false;
            else if (!Objects.equals(toContent.getIrdi(), irdi)) return false;
            else if (!Objects.equals(toContent.getFft(), fft)) return false;
            else if (value == null) return toContent.getValue() == null;
            else return value.isEquivalentTo(toContent.getValue());
        }
        else return false;
    }
}
