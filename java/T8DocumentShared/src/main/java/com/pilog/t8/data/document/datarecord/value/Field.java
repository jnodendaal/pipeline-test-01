package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class Field extends RecordValue
{
    private final ValueRequirement subValueRequirement;

    public Field(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
        subValueRequirement = valueRequirement.getFirstSubRequirement();
    }

    public String getId()
    {
        return this.valueRequirement.getFieldID();
    }

    @Override
    public FieldContent getContent()
    {
        FieldContent content;
        RecordValue fieldValue;

        fieldValue = getFieldValue();
        content = new FieldContent(valueRequirement.getFieldID());
        if (fieldValue != null) content.setValue(fieldValue.getContent());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        ValueContent fieldValueContent;
        FieldContent content;

        content = (FieldContent)valueContent;
        fieldValueContent = content.getValue();
        if (fieldValueContent != null)
        {
            ValueRequirement fieldValueRequirement;
            RecordValue newFieldValue;

            fieldValueRequirement = this.valueRequirement.getFirstSubRequirement();
            newFieldValue = RecordValue.createValue(fieldValueRequirement);
            newFieldValue.setContent(fieldValueContent);
            setFieldValue(newFieldValue);
        }
        else setFieldValue(null);
    }

    public void setFieldValue(RecordValue value)
    {
        this.clearSubValues();
        if (value != null) this.setSubValue(value);
    }

    public RecordValue getFieldValue()
    {
        return subValues.size() > 0 ? subValues.get(0) : null;
    }

    /**
     * A convenience method to return the field requirement for the current
     * field.
     *
     * @return The {@code FieldRequirement} for the current {@code Field}
     */
    public FieldRequirement getFieldRequirement()
    {
        return (FieldRequirement)valueRequirement;
    }

    /**
     * Returns the ValueRequirement of the value that may be contained by this field.
     * @return The ValueRequirement of the value that may be contained by this field.
     */
    public ValueRequirement getFieldValueRequirement()
    {
        return subValueRequirement;
    }

    public RecordValue getOrAddValue()
    {
        RecordValue recordValue;

        recordValue = getFieldValue();
        if (recordValue != null)
        {
            return recordValue;
        }
        else if (subValueRequirement != null)
        {
            recordValue = RecordValue.createValue(valueRequirement);
            setSubValue(recordValue);
            return recordValue;
        }
        else throw new IllegalStateException("No value requirement specified for field: " + this.getId());
    }

    public RecordValue setValue(String plainValue, boolean clearDependants)
    {
        ValueRequirement fieldValueRequirement;
        RequirementType requirementType;

        fieldValueRequirement = valueRequirement.getFirstSubRequirement();
        requirementType = fieldValueRequirement.getRequirementType();
        if (RequirementType.getSimpleDataTypes().contains(requirementType))
        {
            RecordValue simpleValue;

            // Get the simple value to update (or create it if it does not yet exist).
            simpleValue = getFirstSubValue();
            if (simpleValue == null)
            {
                simpleValue = RecordValue.createValue(fieldValueRequirement);
                simpleValue.setValue(plainValue);
                setSubValue(simpleValue);
                return simpleValue;
            }
            else
            {
                simpleValue.setValue(plainValue);
                return simpleValue;
            }
        }
        else if (requirementType == RequirementType.CONTROLLED_CONCEPT)
        {
            ControlledConcept controlledConcept;

            // Make sure a controlled concept record value exists.
            controlledConcept = (ControlledConcept)getFirstSubValue();
            if (controlledConcept == null)
            {
                controlledConcept = new ControlledConcept(fieldValueRequirement);
                setSubValue(controlledConcept);
            }

            // Set the new concept ID.
            controlledConcept.setConceptId(plainValue, clearDependants);
            return controlledConcept;
        }
        else throw new RuntimeException("Field value is not of simple type: " + fieldValueRequirement.getRequirementType());
    }

    public DocumentReferenceList addRecordReference(String recordId)
    {
        RequirementType requirementType;

        requirementType = subValueRequirement.getRequirementType();
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            DocumentReferenceList referenceList;

            referenceList = (DocumentReferenceList)getFieldValue();
            if (referenceList == null)
            {
                referenceList = new DocumentReferenceList(subValueRequirement);
                setSubValue(referenceList);
            }

            // Add the new reference.
            referenceList.addReference(recordId);
            return referenceList;
        }
        else throw new RuntimeException("Field Value Requirement is not Document Reference Type: " + requirementType);
    }

    public void removeRecordReference(String recordId)
    {
        RequirementType requirementType;

        requirementType = subValueRequirement.getRequirementType();
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            DocumentReferenceList referenceList;

            referenceList = (DocumentReferenceList)getFieldValue();
            if (referenceList != null)
            {
                referenceList.removeReference(recordId, true);
            }
        } else throw new RuntimeException("Field Value Requirement is not Document Reference Type: " + requirementType);
    }
}
