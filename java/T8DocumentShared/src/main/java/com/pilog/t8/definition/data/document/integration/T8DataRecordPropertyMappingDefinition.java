package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public abstract class T8DataRecordPropertyMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_RECORD_PROPERTY";
    public static final String IDENTIFIER_PREFIX = "RP_MAP_";

    public enum Datum
    {
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRecordPropertyMappingDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract <T extends Object> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8Context context);

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return null;
    }
}
