package com.pilog.t8.data.document.datarecord.event;

import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;

/**
 * @author Bouwer du Preez
 */
public class T8RecordPropertyValueChangedEvent extends T8DataRecordChangedEvent
{
    private final RecordProperty recordProperty;
    private final RecordValue oldValue;
    private final RecordValue newValue;
    private final Object actor;
    
    public T8RecordPropertyValueChangedEvent(RecordProperty property, RecordValue oldValue, RecordValue newValue, Object actor)
    {
        super(property.getParentDataRecord());
        this.recordProperty = property;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.actor = actor;
    }
    
    public RecordProperty getRecordProperty()
    {
        return recordProperty;
    }

    public RecordValue getNewValue()
    {
        return newValue;
    }

    public RecordValue getOldValue()
    {
        return oldValue;
    }
    
    public Object getActor()
    {
        return actor;
    }

    @Override
    public String toString()
    {
        return "T8RecordPropertyValueChangedEvent{" + "recordProperty=" + recordProperty + ", oldValue=" + oldValue + ", newValue=" + newValue + '}';
    }
}
