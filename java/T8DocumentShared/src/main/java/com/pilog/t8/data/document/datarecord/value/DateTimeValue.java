package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.time.T8TimestampFormatter;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateTimeRequirement;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bouwer du Preez
 */
public class DateTimeValue extends EpochValue
{
    public DateTimeValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public void setDateTime(Date date)
    {
        setValue(date != null ? ((Long)date.getTime()).toString() : null);
    }

    public void setDateTime(Long date)
    {
        setValue(date != null ? date.toString() : null);
    }

    public String getValue(String formatPattern)
    {
        if (!Strings.isNullOrEmpty(this.getValue()))
        {
            return new T8TimestampFormatter(formatPattern).format(Long.parseLong(this.getValue()));
        }
        else return this.getValue();
    }

    @Override
    public DateTimeValueContent getContent()
    {
        DateTimeValueContent content;

        content = new DateTimeValueContent();
        content.setDateTime(getValue());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        DateTimeValueContent content;
        String time;

        content = (DateTimeValueContent)valueContent;
        time = content.getDateTime();
        if (time != null)
        {
            // If the input value is an interger (millisecond value), use it as-is.
            if (Strings.isInteger(time))
            {
                setValue(time);
            }
            else
            {
                // The input value is not an integer, so we try to parse it.
                try
                {
                    SimpleDateFormat format;

                    // Attempt to parse the date using the format specified by the date requirement.
                    format = new SimpleDateFormat(((DateTimeRequirement)valueRequirement).getDateTimeFormat());
                    setDateTime(format.parse(time));
                }
                catch (Exception e)
                {
                    // Set the unparseable value.
                    setValue(time);
                }
            }
        }
        else setValue(null);
    }
}
