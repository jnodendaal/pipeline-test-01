package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ValueRequirementStep extends DefaultPathStep implements PathStep
{
    public ValueRequirementStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof RecordProperty)
        {
            RecordProperty inputProperty;
            PropertyRequirement propertyRequirement;
            ValueRequirement valueRequirement;

            inputProperty = (RecordProperty)input;
            propertyRequirement = inputProperty.getPropertyRequirement();
            valueRequirement = propertyRequirement.getValueRequirement();
            return valueRequirement != null ? evaluateValueRequirementStep(ArrayLists.newArrayList(valueRequirement)) : new ArrayList<ValueRequirement>();
        }
        else if (input instanceof RecordValue)
        {
            RecordValue inputValue;

            inputValue = (RecordValue)input;
            return evaluateValueRequirementStep(ArrayLists.newArrayList(inputValue.getValueRequirement()));
        }
        else if (input instanceof PropertyRequirement)
        {
            PropertyRequirement propertyRequirement;
            ValueRequirement valueRequirement;

            propertyRequirement = (PropertyRequirement)input;
            valueRequirement = propertyRequirement.getValueRequirement();
            return valueRequirement != null ? evaluateValueRequirementStep(ArrayLists.newArrayList(valueRequirement)) : new ArrayList<ValueRequirement>();
        }
        else if (input instanceof List)
        {
            List<ValueRequirement> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<ValueRequirement>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<ValueRequirement>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Value Requirement step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<ValueRequirement> evaluateValueRequirementStep(List<ValueRequirement> requirementList)
    {
        // If we have valid ID's to use for narrowing of the value list do it now.
        if (idList != null)
        {
            Iterator<ValueRequirement> requirementIterator;

            requirementIterator = requirementList.iterator();
            while (requirementIterator.hasNext())
            {
                ValueRequirement nextRequirement;

                nextRequirement = requirementIterator.next();
                if (!idList.contains(nextRequirement.getRequirementType().toString()))
                {
                    requirementIterator.remove();
                }
            }
        }

        // Now run through the remaining value list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<ValueRequirement> requirementIterator;

            // Iterator over the requirements and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            requirementIterator = requirementList.iterator();
            while (requirementIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                ValueRequirement nextRequirement;

                // Create a map containing the input parameters available to the predicate expression.
                nextRequirement = requirementIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("VALUE_REQUIREMENT", nextRequirement);
                inputParameters.put("REQUIREMENT_TYPE_ID", nextRequirement.getRequirementType().getID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        requirementIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return requirementList;
    }
}
