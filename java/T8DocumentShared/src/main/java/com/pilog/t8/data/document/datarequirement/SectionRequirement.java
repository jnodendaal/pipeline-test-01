package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class SectionRequirement implements Requirement, Serializable
{
    private String conceptId;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;
    private DataRequirement parent;
    private final ArrayList<PropertyRequirement> propertyRequirements;

    public SectionRequirement()
    {
        this.parent = null;
        this.propertyRequirements = new ArrayList<PropertyRequirement>();
    }

    public SectionRequirement(String conceptId)
    {
        this();
        this.conceptId = conceptId;
    }

    void setParentDataRequirement(DataRequirement dataRequirement)
    {
        this.parent = dataRequirement;
    }

    /**
     * Returns a copy of this object and its logical descendants.  No references to logical parents are copied.
     * @return A copy of this object and its logical descendants.
     */
    public SectionRequirement copy()
    {
        SectionRequirement newRequirement;

        newRequirement = new SectionRequirement(conceptId);
        newRequirement.setCode(code);
        newRequirement.setTerm(term);
        newRequirement.setAbbreviation(abbreviation);
        newRequirement.setDefinition(definition);

        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            newRequirement.addPropertyRequirement(propertyRequirement.copy());
        }

        return newRequirement;
    }

    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.SECTION;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    /**
     * Returns the terminology of this Section Requirement and of all its
     * contents.
     * @param terminologyCollector The collector to which this Section Requirement's terminology
     * will be added.
     */
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        // Add this Section Requirement's terminology.
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.SECTION, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);

        // Add the terminology from the property requirements.
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            propertyRequirement.addContentTerminology(terminologyCollector);
        }
    }

    /**
     * Sets the terminology of this Section Requirement and all of its content
     * elements.
     * @param terminologyProvider The terminology provider from which to fetch
     * terminology.
     */
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        terminology = terminologyProvider.getTerminology(null, conceptId);
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }

        // Set the terminology of the property requirements.
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            propertyRequirement.setContentTerminology(terminologyProvider);
        }
    }

    public int getIndex()
    {
        if (parent != null)
        {
            return parent.getSectionRequirementIndex(this);
        }
        else return -1;
    }

    public DataRequirement getParentDataRequirement()
    {
        return parent;
    }

    public DataRequirementInstance getParentDataRequirementInstance()
    {
        return parent != null ? parent.getParentDataRequirementInstance() : null;
    }

    public String getDataRequirementInstanceID()
    {
        DataRequirementInstance drInstance;

        drInstance = getParentDataRequirementInstance();
        return drInstance != null ? drInstance.getConceptID() : null;
    }

    public int getPropertyRequirementCount()
    {
        return propertyRequirements.size();
    }

    public int getPropertyRequirementIndex(PropertyRequirement propertyRequirement)
    {
        return propertyRequirements.indexOf(propertyRequirement);
    }

    public int incrementPropertyRequirementIndex(PropertyRequirement propertyRequirement)
    {
        int index;

        index = propertyRequirements.indexOf(propertyRequirement);
        if (index > -1)
        {
            if (index < (propertyRequirements.size() - 1))
            {
                int newIndex;

                newIndex = index + 1;
                propertyRequirements.remove(propertyRequirement);
                propertyRequirements.add(newIndex, propertyRequirement);
                return newIndex;
            }
            else return index;
        }
        else return -1;
    }

    public int decrementPropertyRequirementIndex(PropertyRequirement propertyRequirement)
    {
        int index;

        index = propertyRequirements.indexOf(propertyRequirement);
        if (index > -1)
        {
            if (index > 0)
            {
                int newIndex;

                newIndex = index - 1;
                propertyRequirements.remove(propertyRequirement);
                propertyRequirements.add(newIndex, propertyRequirement);
                return newIndex;
            }
            else return 0;
        }
        else return -1;
    }

    /**
     * Returns the property requirement from this section matching the supplied
     * property ID.
     * @param propertyID The ID of the property requirement to fetch.
     * @return The property requirement matching the specified property ID.
     */
    public PropertyRequirement getPropertyRequirement(String propertyID)
    {
        if (propertyID != null)
        {
            for (PropertyRequirement propertyRequirement : propertyRequirements)
            {
                if (propertyID.equals(propertyRequirement.getConceptID()))
                {
                    return propertyRequirement;
                }
            }

            return null;
        }
        else return null;
    }

    public List<PropertyRequirement> getCharacteristicPropertyRequirements()
    {
        List<PropertyRequirement> characteristicPropertyRequirements;

        characteristicPropertyRequirements = new ArrayList<PropertyRequirement>();
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            if (propertyRequirement.isCharacteristic())
            {
                characteristicPropertyRequirements.add(propertyRequirement);
            }
        }

        return characteristicPropertyRequirements;
    }

    public int getPropertyCount()
    {
        return propertyRequirements.size();
    }

    public int getCharacteristicCount()
    {
        int count;

        count = 0;
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            if (propertyRequirement.isCharacteristic()) count++;
        }

        return count;
    }

    public ArrayList<PropertyRequirement> getPropertyRequirements()
    {
        return new ArrayList<PropertyRequirement>(propertyRequirements);
    }

    public List<String> getPropertyRequirementIDList()
    {
        List<String> idList;

        idList = new ArrayList<String>();
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            idList.add(propertyRequirement.getConceptID());
        }

        return idList;
    }

    public void addPropertyRequirement(PropertyRequirement propertyRequirement)
    {
        propertyRequirement.setParentClassRequirement(this);
        propertyRequirements.add(propertyRequirement);
    }

    public void addPropertyRequirement(PropertyRequirement propertyRequirement, int index)
    {
        propertyRequirement.setParentClassRequirement(this);
        propertyRequirements.add(index, propertyRequirement);
    }

    public boolean removePropertyRequirement(PropertyRequirement requirement)
    {
        if ((requirement != null) && (propertyRequirements.contains(requirement)))
        {
            requirement.setParentClassRequirement(null);
            return propertyRequirements.remove(requirement);
        }
        else return false;
    }

    public boolean removePropertyRequirement(String propertyID)
    {
        PropertyRequirement propertyToDelete;

        propertyToDelete = getPropertyRequirement(propertyID);
        if (propertyToDelete != null)
        {
            return removePropertyRequirement(propertyToDelete);
        }
        else return false;
    }

    public PropertyRequirement getEquivalentPropertyRequirement(PropertyRequirement propertyRequirementToCheck)
    {
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            if (propertyRequirement.isEquivalentRequirement(propertyRequirementToCheck)) return propertyRequirement;
        }

        return null;
    }

    @Override
    public boolean isEquivalentRequirement(Requirement requirement)
    {
        if (!(requirement instanceof SectionRequirement))
        {
            return false;
        }
        else
        {
            SectionRequirement classRequirement;

            classRequirement = (SectionRequirement)requirement;
            for (PropertyRequirement propertyRequirement : propertyRequirements)
            {
                if (!propertyRequirement.isEquivalentRequirement(classRequirement.getPropertyRequirement(propertyRequirement.getConceptID())))
                {
                    return false;
                }
            }

            return true;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("SectionRequirement: [");
        builder.append("ID: ");
        builder.append(getConceptID());
        builder.append("]");
        return builder.toString();
    }
}
