package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class OldPrescribedValue implements Serializable
{
    private RequirementType requirementType;
    private String oldValueId;
    private String valueId;
    private String orgId;
    private String drId;
    private String drInstanceId;
    private String propertyId;
    private String fieldId;
    private String value;
    private String lowerBoundValue;
    private String upperBoundValue;
    private String conceptId;
    private String conceptTerm;
    private String uomId;
    private String uomTerm;
    private String qomId;
    private String qomTerm;

    public OldPrescribedValue(RequirementType requirementType, String oldValueId)
    {
        this.requirementType = requirementType;
        this.oldValueId = oldValueId;
    }

    public RequirementType getRequirementType()
    {
        return requirementType;
    }

    public void setRequirementType(RequirementType requirementType)
    {
        this.requirementType = requirementType;
    }

    public String getOldValueId()
    {
        return oldValueId;
    }

    public void setOldValueId(String oldValueId)
    {
        this.oldValueId = oldValueId;
    }

    public String getValueId()
    {
        return valueId;
    }

    public void setValueId(String valueId)
    {
        this.valueId = valueId;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getDrId()
    {
        return drId;
    }

    public void setDrId(String drId)
    {
        this.drId = drId;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getLowerBoundValue()
    {
        return lowerBoundValue;
    }

    public void setLowerBoundValue(String lowerBoundValue)
    {
        this.lowerBoundValue = lowerBoundValue;
    }

    public String getUpperBoundValue()
    {
        return upperBoundValue;
    }

    public void setUpperBoundValue(String upperBoundValue)
    {
        this.upperBoundValue = upperBoundValue;
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public void setConceptId(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public String getConceptTerm()
    {
        return conceptTerm;
    }

    public void setConceptTerm(String conceptTerm)
    {
        this.conceptTerm = conceptTerm;
    }

    public String getUomId()
    {
        return uomId;
    }

    public void setUomId(String uomId)
    {
        this.uomId = uomId;
    }

    public String getUomTerm()
    {
        return uomTerm;
    }

    public void setUomTerm(String uomTerm)
    {
        this.uomTerm = uomTerm;
    }

    public String getQomId()
    {
        return qomId;
    }

    public void setQomId(String qomId)
    {
        this.qomId = qomId;
    }

    public String getQomTerm()
    {
        return qomTerm;
    }

    public void setQomTerm(String qomTerm)
    {
        this.qomTerm = qomTerm;
    }
}
