package com.pilog.t8.data.document.integration.structure.sap;

import java.util.List;

/**
 *
 * @author Andre Scheepers
 */
public class SAPTable 
{

    /**
     * @return the tableName
     */
    public String getTableName()
    {
        return tableName;
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    /**
     * @return the tableSections
     */
    public List<SAPTableSection> getTableSections()
    {
        return tableSections;
    }

    /**
     * @param tableSections the tableSections to set
     */
    public void setTableSections(List<SAPTableSection> tableSections)
    {
        this.tableSections = tableSections;
    }
    private String tableName;
    private List<SAPTableSection> tableSections;
}