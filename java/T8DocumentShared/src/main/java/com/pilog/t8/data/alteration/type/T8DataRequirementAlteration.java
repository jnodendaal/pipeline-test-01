package com.pilog.t8.data.alteration.type;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.datatype.T8DtDataRequirement;
import com.pilog.t8.datatype.T8DtOntologyConcept;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementAlteration implements T8DataAlteration
{
    private final String packageIid;
    private DataRequirement dataRequirement;
    private T8DataAlterationMethod method;
    private int step;
    private final List<T8OntologyConcept> concepts;

    public static final String ALTERATION_ID = "@ALTERATION_DATA_REQUIREMENT";
    public static final String SUPERSEDED_PROPERTY_ATTRIBUTE_ID = "SUPERSEDED_PROPERTY_ID";

    public T8DataRequirementAlteration(String packageIid, int step, DataRequirement dataRequirement, T8DataAlterationMethod method)
    {
        this.packageIid = packageIid;
        this.step = step;
        this.dataRequirement = dataRequirement;
        this.method = method;
        this.concepts = new ArrayList<>();
    }

    public T8DataRequirementAlteration(JsonObject alterationObject)
    {
        JsonArray conceptArray;

        this.packageIid = alterationObject.getString("packageIid");
        this.step = alterationObject.getInteger("step");
        this.method = T8DataAlterationMethod.valueOf(alterationObject.getString("method"));
        this.dataRequirement = new T8DtDataRequirement(null).deserialize(alterationObject.getJsonObject("dataRequirement"));
        this.concepts = new ArrayList<>();

        // Deserialize all concepts that are part of the alteration.
        conceptArray = alterationObject.getJsonArray("concepts");
        if (conceptArray != null)
        {
            for (JsonValue conceptObject : conceptArray)
            {
                concepts.add(new T8DtOntologyConcept(null).deserialize(conceptObject.asObject()));
            }
        }
    }

    @Override
    public T8DataAlterationMethod getAlterationMethod()
    {
        return method;
    }

    public void setAlterationMethod(T8DataAlterationMethod method)
    {
        this.method = method;
    }

    public DataRequirement getDataRequirement()
    {
        return dataRequirement;
    }

    public void setDataRequirement(DataRequirement dataRequirement)
    {
        this.dataRequirement = dataRequirement;
    }

    @Override
    public String getAlterationId()
    {
        return ALTERATION_ID;
    }

    @Override
    public String getPackageIid()
    {
        return packageIid;
    }

    @Override
    public int getStep()
    {
        return step;
    }

    public void setStep(int step)
    {
        this.step = step;
    }

    public List<T8OntologyConcept> getConcepts()
    {
        return new ArrayList<>(concepts);
    }

    public void setConcepts(List<T8OntologyConcept> concepts)
    {
        this.concepts.clear();
        if (concepts != null)
        {
            this.concepts.addAll(concepts);
        }
    }

    public void addConcept(T8OntologyConcept concept)
    {
        if (!containsConcept(concept.getID()))
        {
            this.concepts.add(concept);
        }
        else throw new IllegalArgumentException("Cannot add duplicate concept to Data Requirement Alteration: " + concept);
    }

    public T8OntologyConcept getConcept(String conceptId)
    {
        for (T8OntologyConcept concept : concepts)
        {
            if (conceptId.equals(concept.getID()))
            {
                return concept;
            }
        }

        return null;
    }

    public boolean containsConcept(String conceptId)
    {
        return getConcept(conceptId) != null;
    }

    public T8OntologyConcept removeConcept(String conceptId)
    {
        for (T8OntologyConcept concept : concepts)
        {
            if (conceptId.equals(concept.getID()))
            {
                concepts.remove(concept);
                return concept;
            }
        }

        return null;
    }

    @Override
    public JsonObject serialize()
    {
        T8DtOntologyConcept dtConcept;
        JsonObject alterationObject;
        JsonArray conceptArray;

        // Create the alteration object.
        alterationObject = new JsonObject();
        alterationObject.add("alterationId", getAlterationId());
        alterationObject.add("packageIid", packageIid);
        alterationObject.add("step", step);
        alterationObject.add("method", method.toString());

        // Add the data requirement serialization to the alteration object.
        if (dataRequirement != null)
        {
            JsonObject drObject;

            drObject = T8DocumentSerializer.serializeDataRequirement(dataRequirement);
            alterationObject.add("dataRequirement", drObject);
        }

        // Serialize all concept alterations.
        dtConcept = new T8DtOntologyConcept(null);
        conceptArray = new JsonArray();
        alterationObject.add("concepts", conceptArray);
        for (T8OntologyConcept concept : concepts)
        {
            conceptArray.add(dtConcept.serialize(concept));
        }

        // Return the serialized version of this alteration.
        return alterationObject;
    }
}
