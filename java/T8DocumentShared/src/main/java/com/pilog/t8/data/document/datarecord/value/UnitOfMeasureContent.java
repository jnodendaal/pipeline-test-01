package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class UnitOfMeasureContent extends ValueContent
{
    private String conceptId;
    private String irdi;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;

    public UnitOfMeasureContent()
    {
        super(RequirementType.UNIT_OF_MEASURE);
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public void setConceptId(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public String getCodeId()
    {
        return codeId;
    }

    public void setCodeId(String codeId)
    {
        this.codeId = codeId;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTermId()
    {
        return termId;
    }

    public void setTermId(String termId)
    {
        this.termId = termId;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinitionId()
    {
        return definitionId;
    }

    public void setDefinitionId(String definitionId)
    {
        this.definitionId = definitionId;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviationId()
    {
        return abbreviationId;
    }

    public void setAbbreviationId(String abbreviationId)
    {
        this.abbreviationId = abbreviationId;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public boolean hasContent()
    {
        if (this.conceptId != null) return true;
        else if (this.irdi != null) return true;
        else if (this.term != null) return true;
        else if (this.termId != null) return true;
        else if (this.definition != null) return true;
        else if (this.definitionId != null) return true;
        else if (this.code != null) return true;
        else if (this.codeId != null) return true;
        else if (this.abbreviation != null) return true;
        else if (this.abbreviationId != null) return true;
        else if (this.languageId != null) return true;
        else return false;
    }

    public void setContent(UnitOfMeasureContent content)
    {
        if (content != null)
        {
            this.conceptId = content.getConceptId();
            this.irdi = content.getIrdi();
            this.term = content.getTerm();
            this.termId = content.getTermId();
            this.definition = content.getDefinition();
            this.definitionId = content.getDefinitionId();
            this.code = content.getCode();
            this.codeId = content.getCodeId();
            this.abbreviation = content.getAbbreviation();
            this.abbreviationId = content.getAbbreviationId();
            this.languageId = content.getLanguageId();
        }
        else
        {
            clear();
        }
    }

    public void clear()
    {
        this.conceptId = null;
        this.irdi = null;
        this.term = null;
        this.termId = null;
        this.definition = null;
        this.definitionId = null;
        this.code = null;
        this.codeId = null;
        this.abbreviation = null;
        this.abbreviationId = null;
        this.languageId = null;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof UnitOfMeasureContent)
        {
            UnitOfMeasureContent toUomContent;

            toUomContent = (UnitOfMeasureContent)toContent;
            if (!Objects.equals(conceptId, toUomContent.getConceptId())) return false;
            else if (!Objects.equals(irdi, toUomContent.getIrdi())) return false;
            else if (!Objects.equals(termId, toUomContent.getTermId())) return false;
            else if (!Objects.equals(term, toUomContent.getTerm())) return false;
            else if (!Objects.equals(codeId, toUomContent.getCodeId())) return false;
            else if (!Objects.equals(code, toUomContent.getCode())) return false;
            else if (!Objects.equals(abbreviationId, toUomContent.getAbbreviationId())) return false;
            else if (!Objects.equals(abbreviation, toUomContent.getAbbreviation())) return false;
            else if (!Objects.equals(definitionId, toUomContent.getDefinitionId())) return false;
            else if (!Objects.equals(definition, toUomContent.getDefinition())) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof QualifierOfMeasureContent)
        {
            QualifierOfMeasureContent toQomContent;

            toQomContent = (QualifierOfMeasureContent)toContent;
            if (!Objects.equals(conceptId, toQomContent.getConceptId())) return false;
            else return true;
        }
        else return false;
    }
}
