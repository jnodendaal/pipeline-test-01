package com.pilog.t8.definition.data.document.integration.field;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationFieldMapper;
import com.pilog.t8.definition.data.document.integration.T8DataRecordFieldMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8DefaultFieldMappingDefinition extends T8DataRecordFieldMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_FIELD";
    public static final String DISPLAY_NAME = "Default Field Mapping";
    public static final String DESCRIPTION = "A Data Record field mapping that defines how fields for this record will be mapped";

    public enum Datum
    {
        PROPERTY_ID,
        FIELD_ID,
        MAPPING_TYPE
    };
    // -------- Definition Meta-Data -------- //

    public static enum MappingTypes
    {
        CODE,
        CONCEPT,
        TERM,
        ABBREVIATION,
        DEFINITION,
        VALUE_STRING
    }

    public T8DefaultFieldMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.PROPERTY_ID.toString(), "Property ID", "The Property ID to be mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.FIELD_ID.toString(), "Field ID", "The field ID to be mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MAPPING_TYPE.toString(), "Mapping Type", "The type of mapping that will be performed on this property", MappingTypes.VALUE_STRING.toString(), T8DefinitionDatumOptionType.ENUMERATION));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.MAPPING_TYPE.toString().equals(datumIdentifier)) return createStringOptions(MappingTypes.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <T extends Object> DataRecordIntegrationFieldMapper<T> createNewFieldMapperInstance(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.field.mapping.DefaultFieldMapping").getConstructor(T8ServerContext.class, T8SessionContext.class, T8DefaultFieldMappingDefinition.class);
            return (DataRecordIntegrationFieldMapper<T>) constructor.newInstance(serverContext, sessionContext, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new field mapper instance", ex);
            return null;
        }
    }

    public String getPropertyID()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_ID.toString());
    }

    public void setPropertyID(String propertyID)
    {
        setDefinitionDatum(Datum.PROPERTY_ID.toString(), propertyID);
    }

    public String getFieldID()
    {
        return (String)getDefinitionDatum(Datum.FIELD_ID.toString());
    }

    public void setFieldID(String fieldID)
    {
        setDefinitionDatum(Datum.FIELD_ID.toString(), fieldID);
    }

    public MappingTypes getMappingType()
    {
        return MappingTypes.valueOf((String)getDefinitionDatum(Datum.MAPPING_TYPE.toString()));
    }

    public void setMappingType(MappingTypes type)
    {
        setDefinitionDatum(Datum.MAPPING_TYPE.toString(), type.toString());
    }
}
