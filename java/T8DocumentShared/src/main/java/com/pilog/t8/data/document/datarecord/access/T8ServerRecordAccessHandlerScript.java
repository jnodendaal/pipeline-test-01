package com.pilog.t8.data.document.datarecord.access;

import com.pilog.t8.script.T8ServerContextScript;

/**
 * @author Bouwer du Preez
 */
public interface T8ServerRecordAccessHandlerScript extends T8ServerContextScript
{
    public void setAccessHandler(T8DataRecordAccessHandler accessHandler);
}
