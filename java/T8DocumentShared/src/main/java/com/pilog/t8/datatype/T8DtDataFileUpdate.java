package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileNewAttachment;
import com.pilog.t8.api.datarecordeditor.T8DataFileNewRecord;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordCreation;
import com.pilog.t8.api.datarecordeditor.T8DataFileUpdate;
import com.pilog.t8.api.datarecordeditor.T8DataFileValueUpdate;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.api.datarecordeditor.T8DataFilePropertyAccessUpdate;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordDeletion;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileUpdate extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE";

    public T8DtDataFileUpdate(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileUpdate.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtDataFileValueUpdate dtValueUpdate;
            T8DtDataFileRecordCreation dtRecordCreation;
            T8DtDataFileNewRecord dtNewRecord;
            T8DtDataFileRecordDeletion dtRecordDeletion;
            T8DtDataFileNewAttachment dtNewAttachment;
            T8DtDataFilePropertyAccessUpdate dtPropertyAccess;
            T8DataFileUpdate fileUpdate;
            JsonArray recordCreationArray;
            JsonArray valueUpdateArray;
            JsonArray newRecordArray;
            JsonArray recordDeletionArray;
            JsonArray newAttachmentArray;
            JsonArray propertyAccessUpdateArray;
            JsonObject updateObject;

            // Create the file update object.
            fileUpdate = (T8DataFileUpdate)object;
            updateObject = new JsonObject();
            valueUpdateArray = new JsonArray();
            recordCreationArray = new JsonArray();
            newRecordArray = new JsonArray();
            recordDeletionArray = new JsonArray();
            newAttachmentArray = new JsonArray();
            propertyAccessUpdateArray = new JsonArray();
            updateObject.add("fileId", fileUpdate.getFileId());
            updateObject.add("valueUpdates", valueUpdateArray);
            updateObject.add("recordCreations", recordCreationArray);
            updateObject.add("newRecords", newRecordArray);
            updateObject.add("recordDeletions", recordDeletionArray);
            updateObject.add("newAttachments", newAttachmentArray);
            updateObject.add("propertyAccessUpdates", propertyAccessUpdateArray);

            // Serialize the value updates.
            dtValueUpdate = new T8DtDataFileValueUpdate(null);
            for (T8DataFileValueUpdate valueUpdate : fileUpdate.getValueUpdates())
            {
                JsonValue valueUpdateObject;

                valueUpdateObject = dtValueUpdate.serialize(valueUpdate);
                valueUpdateArray.add(valueUpdateObject);
            }

            // Serialize the record creations.
            dtRecordCreation = new T8DtDataFileRecordCreation(null);
            for (T8DataFileRecordCreation recordCreation : fileUpdate.getRecordCreations())
            {
                JsonValue jsonRecordCreation;

                jsonRecordCreation = dtRecordCreation.serialize(recordCreation);
                recordCreationArray.add(jsonRecordCreation);
            }

            // Serialize the new records.
            dtNewRecord = new T8DtDataFileNewRecord(null);
            for (T8DataFileNewRecord newRecord : fileUpdate.getNewRecords())
            {
                JsonValue jsonNewRecord;

                jsonNewRecord = dtNewRecord.serialize(newRecord);
                newRecordArray.add(jsonNewRecord);
            }

            // Serialize the record deletions.
            dtRecordDeletion = new T8DtDataFileRecordDeletion(null);
            for (T8DataFileRecordDeletion deletion : fileUpdate.getRecordDeletions())
            {
                JsonValue jsonRecordDeletion;

                jsonRecordDeletion = dtRecordDeletion.serialize(deletion);
                recordDeletionArray.add(jsonRecordDeletion);
            }

            // Serialize the new attachments.
            dtNewAttachment = new T8DtDataFileNewAttachment(null);
            for (T8DataFileNewAttachment newAttachment : fileUpdate.getNewAttachments())
            {
                JsonValue jsonNewAttachment;

                jsonNewAttachment = dtNewRecord.serialize(newAttachment);
                newAttachmentArray.add(jsonNewAttachment);
            }

            // Serialize the property access updates.
            dtPropertyAccess = new T8DtDataFilePropertyAccessUpdate(null);
            for (T8DataFilePropertyAccessUpdate accessUpdate : fileUpdate.getPropertyAccessUpdates())
            {
                JsonValue jsonUpdate;

                jsonUpdate = dtPropertyAccess.serialize(accessUpdate);
                propertyAccessUpdateArray.add(jsonUpdate);
            }

            return updateObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileUpdate deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DataFileUpdate fileUpdate;
            JsonArray valueUpdateArray;
            JsonArray recordCreationArray;
            JsonArray newRecordArray;
            JsonArray recordDeletionArray;
            JsonArray newAttachmentArray;
            JsonArray propertyAccessUpdateArray;
            JsonObject object;

            object = (JsonObject)jsonValue;
            fileUpdate = new T8DataFileUpdate(object.getString("fileId"));
            valueUpdateArray = object.getJsonArray("valueUpdates");
            recordCreationArray = object.getJsonArray("recordCreations");
            newRecordArray = object.getJsonArray("newRecords");
            recordDeletionArray = object.getJsonArray("recordDeletions");
            newAttachmentArray = object.getJsonArray("newAttachments");
            propertyAccessUpdateArray = object.getJsonArray("propertyAccessUpdates");

            // Deserialize value updates.
            if (valueUpdateArray != null)
            {
                T8DtDataFileValueUpdate dtValueUpdate;

                dtValueUpdate = new T8DtDataFileValueUpdate(null);
                for (JsonValue jsonValueUpdate : valueUpdateArray.values())
                {
                    fileUpdate.addValueUpdate(dtValueUpdate.deserialize(jsonValueUpdate));
                }
            }

            // Deserialize record creations.
            if (recordCreationArray != null)
            {
                T8DtDataFileRecordCreation dtRecordCreation;

                dtRecordCreation = new T8DtDataFileRecordCreation(null);
                for (JsonValue jsonRecordCreation : recordCreationArray.values())
                {
                    fileUpdate.addRecordCreation(dtRecordCreation.deserialize(jsonRecordCreation));
                }
            }

            // Deserialize new records.
            if (newRecordArray != null)
            {
                T8DtDataFileNewRecord dtNewRecords;

                dtNewRecords = new T8DtDataFileNewRecord(null);
                for (JsonValue jsonNewRecord : newRecordArray.values())
                {
                    fileUpdate.addNewRecord(dtNewRecords.deserialize(jsonNewRecord));
                }
            }

            // Deserialize record deletions.
            if (recordDeletionArray != null)
            {
                T8DtDataFileRecordDeletion dtRecordDeletions;

                dtRecordDeletions = new T8DtDataFileRecordDeletion(null);
                for (JsonValue jsonRecordDeletion : recordDeletionArray.values())
                {
                    fileUpdate.addRecordDeletion(dtRecordDeletions.deserialize(jsonRecordDeletion));
                }
            }

            // Deserialize new records.
            if (newAttachmentArray != null)
            {
                T8DtDataFileNewAttachment dtNewAttachments;

                dtNewAttachments = new T8DtDataFileNewAttachment(null);
                for (JsonValue jsonNewAttachment : newAttachmentArray.values())
                {
                    fileUpdate.addNewAttachment(dtNewAttachments.deserialize(jsonNewAttachment));
                }
            }

            // Deserialize property access updates.
            if (propertyAccessUpdateArray != null)
            {
                T8DtDataFilePropertyAccessUpdate dtAccessUpdates;

                dtAccessUpdates = new T8DtDataFilePropertyAccessUpdate(null);
                for (JsonValue jsonUpdate : propertyAccessUpdateArray.values())
                {
                    fileUpdate.addPropertyAccessUpdate(dtAccessUpdates.deserialize(jsonUpdate));
                }
            }

            return fileUpdate;
        }
        else return null;
    }
}
