package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import java.math.BigInteger;

/**
 * @author Bouwer du Preez
 */
public class IntegerValue extends RecordValue
{
    public IntegerValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public void setValue(Integer value)
    {
        if (value != null)
        {
            super.setValue(value.toString());
        }
        else super.setValue(null);
    }

    public void setValue(Long value)
    {
        if (value != null)
        {
            super.setValue(value.toString());
        }
        else super.setValue(null);
    }

    public BigInteger getBigIntegerValue()
    {
        String stringValue;

        stringValue = getValue();
        return stringValue != null ? new BigInteger(stringValue) : null;
    }

    public Integer getIntegerValue()
    {
        String stringValue;

        return (stringValue = getValue()) != null ? Integer.parseInt(stringValue) : null;
    }

    @Override
    public IntegerValueContent getContent()
    {
        IntegerValueContent content;

        content = new IntegerValueContent();
        content.setValue(value);
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        IntegerValueContent content;

        content = (IntegerValueContent)valueContent;
        setValue(content.getValue());
    }
}
