package com.pilog.t8.definition.data.object;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectHandler;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8EntityDataObjectDefinition extends T8DataObjectDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_OBJECT_ENTITY";
    public static final String DISPLAY_NAME = "Entity Data Object";
    public static final String DESCRIPTION = "An object that is based on a data entity directly.";
    public enum Datum
    {
        DATA_ENTITY_ID,
        ORGANIZATION_ENTITY_FIELD_ID,
        LANGUAGE_ENTITY_FIELD_ID,
        SEARCH_TERMS_ENTITY_FIELD_ID,
        ENTITY_FIELD_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public T8EntityDataObjectDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_ID.toString(), "Data Entity", "The id of the data entity to which the object data will be written."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ORGANIZATION_ENTITY_FIELD_ID.toString(), "Entity Field Id: Organization", "The id of the data entity field in which the object organization is stored."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.LANGUAGE_ENTITY_FIELD_ID.toString(), "Entity Field Id: Language", "The id of the data entity field in which the object langage is stored."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SEARCH_TERMS_ENTITY_FIELD_ID.toString(), "Entity Field Id: Search Terms", "The id of the data entity field in which the object search terms are stored."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.ENTITY_FIELD_MAPPING.toString(), "Field Mapping:  Object to Entity", "A mapping of data object fields the corresponding entity fields where values are persisted."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.DATA_ENTITY_ID.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        else if ((Datum.ORGANIZATION_ENTITY_FIELD_ID.toString().equals(datumId)) || (Datum.LANGUAGE_ENTITY_FIELD_ID.toString().equals(datumId)) || (Datum.SEARCH_TERMS_ENTITY_FIELD_ID.toString().equals(datumId)))
        {
            String entityId;

            entityId = getDataEntityId();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    return createPublicIdentifierOptionsFromDefinitions(entityDefinition.getFieldDefinitions(), true, "Not Used");
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.ENTITY_FIELD_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityId();
            if (entityId != null)
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (entityDefinition != null)
                {
                    List<T8DataObjectFieldDefinition> objectFieldDefinitions;
                    List<T8DataEntityFieldDefinition> entityFieldDefinitions;
                    TreeMap<String, List<String>> identifierMap;

                    objectFieldDefinitions = getFieldDefinitions();
                    entityFieldDefinitions = entityDefinition.getFieldDefinitions();

                    identifierMap = new TreeMap<String, List<String>>();
                    if ((objectFieldDefinitions != null) && (entityFieldDefinitions != null))
                    {
                        ArrayList<String> fieldIds;

                        fieldIds = new ArrayList<String>();
                        for (T8DataEntityFieldDefinition fieldDefinition : entityFieldDefinitions)
                        {
                            fieldIds.add(fieldDefinition.getPublicIdentifier());
                        }
                        Collections.sort(fieldIds);

                        identifierMap.put("$ID", fieldIds);
                        for (T8DataObjectFieldDefinition fieldDefinition : objectFieldDefinitions)
                        {
                            identifierMap.put(fieldDefinition.getIdentifier(), fieldIds);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DataObjectHandler getDataObjectHandler(T8DataTransaction tx)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.data.object.T8EntityDataObjectHandler", new Class<?>[]{T8EntityDataObjectDefinition.class, T8DataTransaction.class}, this, tx);
    }

    public String getDataEntityId()
    {
        return (String)getDefinitionDatum(Datum.DATA_ENTITY_ID);
    }

    public void setDataEntityId(String id)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_ID, id);
    }

    public String getOrganizationEntityFieldId()
    {
        return getDefinitionDatum(Datum.ORGANIZATION_ENTITY_FIELD_ID);
    }

    public void setOrganizationEntityFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.ORGANIZATION_ENTITY_FIELD_ID, fieldId);
    }

    public String getLanguageEntityFieldId()
    {
        return getDefinitionDatum(Datum.LANGUAGE_ENTITY_FIELD_ID);
    }

    public void setLanguageEntityFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.LANGUAGE_ENTITY_FIELD_ID, fieldId);
    }

    public String getSearchTermsEntityFieldId()
    {
        return getDefinitionDatum(Datum.SEARCH_TERMS_ENTITY_FIELD_ID);
    }

    public void setSearchTermsEntityFieldId(String fieldId)
    {
        setDefinitionDatum(Datum.SEARCH_TERMS_ENTITY_FIELD_ID, fieldId);
    }

    public Map<String, String> getEntityFieldMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.ENTITY_FIELD_MAPPING);
    }

    public void setEntityFieldMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.ENTITY_FIELD_MAPPING, mapping);
    }
}
