package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class DateRequirement extends ValueRequirement
{
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    public DateRequirement()
    {
        super(RequirementType.DATE_TYPE);
    }

    public String getDateFormat()
    {
        String format;

        format = (String)this.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.toString());
        return format != null ? format : DEFAULT_DATE_FORMAT;
    }
}
