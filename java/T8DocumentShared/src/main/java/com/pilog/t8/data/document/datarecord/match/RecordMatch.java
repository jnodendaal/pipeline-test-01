package com.pilog.t8.data.document.datarecord.match;

import java.io.Serializable;

/**
 * This class represents the link between two data records that have been
 * matched using some arbitrary methodology.
 * 
 * @author Bouwer du Preez
 */
public class RecordMatch implements Serializable
{
    private final String familyID;
    private final String familyCode;
    private final String recordID1;
    private final String recordID2;

    public RecordMatch(String familyID, String familyCode, String recordID1, String recordID2)
    {
        this.familyID = familyID;
        this.familyCode = familyCode;
        this.recordID1 = recordID1;
        this.recordID2 = recordID2;
    }

    public String getFamilyID()
    {
        return familyID;
    }

    public String getFamilyCode()
    {
        return familyCode;
    }
    
    public String getRecordID1()
    {
        return recordID1;
    }

    public String getRecordID2()
    {
        return recordID2;
    }
}
