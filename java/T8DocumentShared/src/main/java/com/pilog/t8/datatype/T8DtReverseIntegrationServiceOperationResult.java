package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.integration.T8RevereseIntegrationServiceOperationResult;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DtReverseIntegrationServiceOperationResult extends T8AbstractDataType implements T8DataType
{
    public static final String DT_IDENTIFIER = "@REVERSE_INTEGRATION_SERVICE_OPERATION_RESULT";

    public T8DtReverseIntegrationServiceOperationResult(T8DefinitionManager context)
    {
        // No function.
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return DT_IDENTIFIER;
    }

    @Override
    public boolean isType(T8DataType type)
    {
        return type instanceof T8DtReverseIntegrationServiceOperationResult;
    }

    @Override
    public boolean isType(String typeIdentifier)
    {
        return DT_IDENTIFIER.equals(typeIdentifier);
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return DT_IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8RevereseIntegrationServiceOperationResult.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8RevereseIntegrationServiceOperationResult result;
            JsonObject jsonResult;

            // Create the JSON object.
            result = (T8RevereseIntegrationServiceOperationResult)object;
            jsonResult = new JsonObject();
            jsonResult.add("operationId", result.getOperationID());
            jsonResult.add("organizationId", result.getOrganizationID());
            jsonResult.addIfNotNull("domainType", result.getDomainType());
            jsonResult.addIfNotNull("operationType", result.getOperationType());
            jsonResult.addIfNotNull("externalReference", result.getExternalReference());
            jsonResult.addIfNotNull("requestNumber", result.getRequestNumber());
            jsonResult.add("inputParameters", new T8DtMap(T8DataType.STRING, T8DataType.STRING).serialize(result.getInputParameters()));

            // Return the json object.
            return jsonResult;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8RevereseIntegrationServiceOperationResult result;
            JsonObject jsonResult;

            // Get the JSON values.
            jsonResult = jsonValue.asObject();

            // Create the functionality handle object.
            result = new T8RevereseIntegrationServiceOperationResult();
            result.setOperationID(jsonResult.getString("operationId"));
            result.setOrganizationID(jsonResult.getString("organizationId"));
            result.setDomainType(jsonResult.getString("domainType"));
            result.setOperationType(jsonResult.getString("operationType"));
            result.setExternalReference(jsonResult.getString("externalReference"));
            result.setRequestNumber(jsonResult.getString("requestNumber"));
            result.setInputParameters((Map<String, String>)new T8DtMap(T8DataType.STRING, T8DataType.STRING).deserialize(jsonResult.getJsonObject("inputParameters")));

            // Return the completed object.
            return result;
        }
        else return null;
    }
}
