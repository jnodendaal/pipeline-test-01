package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.T8HierarchicalSetType;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceListContent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Stores the content of a record i.e. it's value and FFT data.  All data values in a RecordContent model
 * are carried as String type data to ensure that any form of a value, not just the correct format and type,
 * can be stored.  Example; a BooleanValueContent object does not carry its data as a Boolean type to ensure
 * that values such as 'n', 'Y', '1', '0' can all be accommodated.
 *
 * When RecordConent data is used to populate a DataRecord model, correct transformation and resolution of
 * the String data in the content model is performed to ensure that the final values in the DataRecord model
 * is of the correct type and format.
 *
 * @author Bouwer du Preez
 */
public class RecordContent implements Serializable
{
    private String id;
    private String drInstanceId;
    private String drInstanceIrdi;
    private String fft;
    private RecordContent parent;
    private String insertedById;
    private String insertedByIid;
    private T8Timestamp insertedAt;
    private String updatedById;
    private String updatedByIid;
    private T8Timestamp updatedAt;
    private final LinkedHashMap<String, Object> attributes;
    private final List<PropertyContent> properties;
    private final List<RecordContent> subRecords;

    public RecordContent()
    {
        this.attributes = new LinkedHashMap<>();
        this.properties = new ArrayList<PropertyContent>();
        this.subRecords = new ArrayList<RecordContent>();
    }

    void setParent(RecordContent parent)
    {
        this.parent = parent;
    }

    public RecordContent getParent()
    {
        return parent;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getDrInstanceIrdi()
    {
        return drInstanceIrdi;
    }

    public void setDrInstanceIrdi(String irdi)
    {
        this.drInstanceIrdi = irdi;
    }

    public String getFft()
    {
        return fft;
    }

    public void setFft(String fft)
    {
        this.fft = fft;
    }

    public void addProperty(PropertyContent property)
    {
        property.setRecord(this);
        this.properties.add(property);
    }

    public boolean removeProperty(PropertyContent property)
    {
        if (properties.remove(property))
        {
            property.setRecord(null);
            return true;
        }
        else return false;
    }

    public int getPropertyCount()
    {
        return properties.size();
    }

    public List<PropertyContent> getProperties()
    {
        return new ArrayList<PropertyContent>(properties);
    }

    public PropertyContent getProperty(String propertyId)
    {
        for (PropertyContent property : properties)
        {
            if (property.getId().equals(propertyId))
            {
                return property;
            }
        }

        return null;
    }

    public void addSubRecord(RecordContent subRecord)
    {
        subRecord.setParent(this);
        subRecords.add(subRecord);
    }

    public boolean removeSubRecord(RecordContent subRecord)
    {
        if (subRecords.remove(subRecord))
        {
            subRecord.setParent(null);
            return true;
        }
        else return false;
    }

    public List<RecordContent> getSubRecords()
    {
        return new ArrayList<RecordContent>(subRecords);
    }

    public List<RecordContent> getDescendantRecords()
    {
        ArrayList<RecordContent> descendants;

        descendants = new ArrayList<>(subRecords);
        for (RecordContent subRecord : subRecords)
        {
            descendants.addAll(subRecord.getDescendantRecords());
        }

        return descendants;
    }

    public RecordContent getRootRecord()
    {
        RecordContent nextRecord;

        nextRecord = this;
        while (nextRecord.getParent() != null)
        {
            nextRecord = nextRecord.getParent();
        }

        return nextRecord;
    }

    public String getInsertedById()
    {
        return insertedById;
    }

    public void setInsertedById(String insertedById)
    {
        this.insertedById = insertedById;
    }

    public String getInsertedByIid()
    {
        return insertedByIid;
    }

    public void setInsertedByIid(String insertedByIid)
    {
        this.insertedByIid = insertedByIid;
    }

    public T8Timestamp getInsertedAt()
    {
        return insertedAt;
    }

    public void setInsertedAt(T8Timestamp insertedAt)
    {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedById()
    {
        return updatedById;
    }

    public void setUpdatedById(String updatedById)
    {
        this.updatedById = updatedById;
    }

    public String getUpdatedByIid()
    {
        return updatedByIid;
    }

    public void setUpdatedByIid(String updatedByIid)
    {
        this.updatedByIid = updatedByIid;
    }

    public T8Timestamp getUpdatedAt()
    {
        return updatedAt;
    }

    public void setUpdatedAt(T8Timestamp updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public T8Timestamp getLatestUpdateTime()
    {
        T8Timestamp latestTime;

        latestTime = updatedAt;
        for (RecordContent descendant : getDescendantRecords())
        {
            T8Timestamp descendantTime;

            descendantTime = descendant.getUpdatedAt();
            if (latestTime == null)
            {
                latestTime = descendant.getUpdatedAt();
            }
            else if ((descendantTime != null) && (descendantTime.getMilliseconds() > latestTime.getMilliseconds()))
            {
                latestTime = descendantTime;
            }
        }

        return latestTime;
    }

    public Object getAttribute(String attributeIdentifier)
    {
        return attributes.get(attributeIdentifier);
    }

    public Object setAttribute(String attributeId, Object attributeValue)
    {
        if (attributeId != null)
        {
            return attributes.put(attributeId, attributeValue);
        }
        else throw new RuntimeException("Invalid attribute id: " + attributeId);
    }

    public Object removeAttribute(String attributeId)
    {
        if (attributeId != null)
        {
            return attributes.remove(attributeId);
        }
        else throw new RuntimeException("Invalid attribute id: " + attributeId);
    }

    public boolean containsAttribute(String attributeId)
    {
        return attributes.containsKey(attributeId);
    }

    public Map<String, Object> getAttributes()
    {
        return new LinkedHashMap<>(attributes);
    }

    public void setAttributes(Map<String, Object> newAttributes)
    {
        attributes.clear();
        if (newAttributes != null)
        {
            for (String attributeId : newAttributes.keySet())
            {
                setAttribute(attributeId, newAttributes.get(attributeId));
            }
        }
    }

    /**
     * Returns the parent record that references the specified child record.  This method does not use the sub-record collection of the
     * record in the object model but rather attempts to find the parent by checking the document reference lists in each record in the model.
     * @param childRecordId The id of the child record for which to return a parent record.
     * @return The parent record that contains a reference to the specified child record.  Null if not found.
     */
    public RecordContent getParent(String childRecordId)
    {
        for (RecordContent record : RecordContent.this.getRecordSet())
        {
            if (record.getRecordReferenceIds(true, false).contains(childRecordId))
            {
                return record;
            }
        }

        return null;
    }

    /**
     * Returns the set of all records that are part of the record object model to which this record belongs.
     * @return A set of all records that are part of the record object model to which this record belongs.
     */
    public Set<RecordContent> getRecordSet()
    {
        Map<String, RecordContent> records;
        LinkedList<RecordContent> recordQueue;

        // Create a collection to hold all of the required records.
        records = new HashMap<>();
        records.put(getId(), this);

        // No travers the hierarchy and add all records found to the map (avoiding cyclic traversals).
        recordQueue = new LinkedList<>();
        recordQueue.add(this);
        while (recordQueue.size() > 0)
        {
            RecordContent nextRecord;
            RecordContent parentRecord;

            // Get the next record from the queue.
            nextRecord = recordQueue.pop();

            // Add the parent parent record.
            parentRecord = nextRecord.getParent();
            if ((parentRecord != null) && (!records.containsKey(parentRecord.getId())))
            {
                records.put(parentRecord.getId(), parentRecord);
                recordQueue.add(parentRecord);
            }

            // Add all sub-records that we have not found yet.
            for (RecordContent subRecord : nextRecord.getSubRecords())
            {
                if (!records.containsKey(subRecord.getId()))
                {
                    records.put(subRecord.getId(), subRecord);
                    recordQueue.add(subRecord);
                }
            }
        }

        // Return the set of records found.
        return new HashSet<>(records.values());
    }

    /**
     * Returns a set of all the record id's referenced from this record.  This means that the set may contain references to records that are externally
     * persisted but not part of the current object model.
     * @param includeDependentReferences
     * @param includeIndependentReferences
     * @return The set of Record ID references by this record.
     */
    public Set<String> getRecordReferenceIds(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        HashSet<String> referenceIdSet;

        referenceIdSet = new HashSet<>();
        for (PropertyContent property : properties)
        {
            ValueContent value;

            value = property.getValue();
            if (value instanceof DocumentReferenceListContent)
            {
                DocumentReferenceListContent referenceList;

                referenceList = (DocumentReferenceListContent)value;
                if (referenceList.isIndependent())
                {
                    if (includeIndependentReferences)
                    {
                        referenceIdSet.addAll(referenceList.getReferenceIds());
                    }
                }
                else if (includeDependentReferences)
                {
                    referenceIdSet.addAll(referenceList.getReferenceIds());
                }
            }
        }

        return referenceIdSet;
    }

    /**
     * Returns the specified set of data records from the current object model.
     * This method uses only dependent references.
     * @param setType The type of set to retrieve.
     * @return The set of record requested.
     */
    public Set<RecordContent> getRecordSet(T8HierarchicalSetType setType)
    {
        Set<RecordContent> recordSet;

        recordSet = new HashSet<RecordContent>();
        switch (setType)
        {
            case LINEAGE:    // From a specific node, all ancestors, all descendants including the node itself.
            {
                RecordContent currentRecord;

                // Add all ancestors and this record itself.
                currentRecord = this;
                recordSet.add(currentRecord);
                while (currentRecord.getParent() != null)
                {
                    currentRecord = currentRecord.getParent();
                    recordSet.add(currentRecord);
                }

                // Add all descendants.
                recordSet.addAll(getDescendantRecords());

                // Return the result.
                return recordSet;
            }
            case ANCESTORS:  // From a specific node, all ancestors, excluding the node itself.
            {
                RecordContent currentRecord;

                currentRecord = this;
                while (currentRecord.getParent() != null)
                {
                    currentRecord = currentRecord.getParent();
                    recordSet.add(currentRecord);
                }

                return recordSet;
            }
            case LINE:       // From a specific node, all ancestors, including the node itself.
            {
                RecordContent currentRecord;

                currentRecord = this;
                recordSet.add(currentRecord);
                while (currentRecord.getParent() != null)
                {
                    currentRecord = currentRecord.getParent();
                    recordSet.add(currentRecord);
                }

                return recordSet;
            }
            case SINGLE:     // From a specific node, only the specific node itself.
            {
                recordSet.add(this);
                return recordSet;
            }
            case HOUSE:      // From a specific node, all nodes with the same parent, including the node itself and its parent.
            {
                RecordContent parentRecord;

                parentRecord = getParent();
                if (parentRecord != null)
                {
                    recordSet.addAll(parentRecord.getSubRecords());
                    recordSet.add(parentRecord);
                    return recordSet;
                }
                else return recordSet;
            }
            case SIBLINGS:   // From a specific node, all nodes with the same parent, excluding the node itself.
            {
                RecordContent parentRecord;

                parentRecord = getParent();
                if (parentRecord != null)
                {
                    recordSet.addAll(parentRecord.getSubRecords());
                    recordSet.remove(this);
                    return recordSet;
                }
                else return recordSet;
            }
            case BROOD:      // From a specific node, all nodes with the same parent, including the node itself.
            {
                RecordContent parentRecord;

                parentRecord = getParent();
                if (parentRecord != null)
                {
                    recordSet.addAll(parentRecord.getSubRecords());
                    return recordSet;
                }
                else return recordSet;
            }
            case FAMILY:     // From a specific node, all descendants, including the node itself.
            {
                recordSet.add(this);
                recordSet.addAll(getDescendantRecords());
                return recordSet;
            }
            case DESCENDANTS: // From a specific node, all descendants, excluding the node itself.
            {
                recordSet.addAll(getDescendantRecords());
                return recordSet;
            }
            default:
                throw new IllegalArgumentException("Unsupported set type: " + setType);
        }
    }

    public List<RecordContent> getFamilyRecords()
    {
        List<RecordContent> records;
        LinkedList<RecordContent> queue;

        records = new ArrayList<RecordContent>();
        queue = new LinkedList<RecordContent>();
        queue.add(this);
        while (!queue.isEmpty())
        {
            RecordContent nextRecord;

            nextRecord = queue.removeFirst();
            records.add(nextRecord);
            queue.addAll(nextRecord.getSubRecords());
        }

        return records;
    }

    /**
     * Measures equality of the input object to the current object i.e. checks that
     * all data values in the input object model are the same as the current.
     * @param toRecordContent
     * @param includeDescendantRecordContent
     * @return
     */
    public boolean isEqualTo(RecordContent toRecordContent, boolean includeDescendantRecordContent)
    {
        if (toRecordContent != null)
        {
            if (properties.size() != toRecordContent.getPropertyCount()) return false;
            else
            {
                for (PropertyContent propertyContent : properties)
                {
                    PropertyContent toPropertyContent;

                    toPropertyContent = toRecordContent.getProperty(propertyContent.getId());
                    if (toPropertyContent != null)
                    {
                        if (!propertyContent.isEqualTo(toPropertyContent)) return false;
                    }
                    else return false;
                }

                return true;
            }
        }
        else return false;
    }

    /**
     * Checks the supplied record and measures its content to check if it is logically equivalent to the this record's content i.e. if it has the same value.
     * This method does not guarantee that the two objects are equal in all ways, because they may contain data values that are not essential to the content
     * of the record e.g. terminology.  This method only checks the value content and essential ids e.g. if two concepts have the same id but one has terminology
     * missing or a different term, this method will count them as equivalent.  Another example is two record with unequal number of properties but when the values
     * are compared, some of the properties in one record are found to be empty.  In this case, if the values that are compare are found to be equivalent, the records are
     * also considered to be equivalent, because the empty properties have no effect on the final value of the record.
     * @param toRecordContent The record to check.
     * @param includeDescendantRecordContent
     * @return
     */
    public boolean isEquivalentTo(RecordContent toRecordContent, boolean includeDescendantRecordContent)
    {
        if (toRecordContent != null)
        {
            if (properties.size() != toRecordContent.getPropertyCount()) return false;
            else
            {
                for (PropertyContent propertyContent : properties)
                {
                    PropertyContent toPropertyContent;

                    toPropertyContent = toRecordContent.getProperty(propertyContent.getId());
                    if (toPropertyContent != null)
                    {
                        if (!propertyContent.isEquivalentTo(toPropertyContent)) return false;
                    }
                    else return false;
                }

                return true;
            }
        }
        else return false;
    }

    @Override
    public String toString()
    {
        return "RecordContent{" + "id=" + id + ", drInstanceId=" + drInstanceId + '}';
    }
}
