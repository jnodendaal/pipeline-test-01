package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileValueUpdate;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.api.datarecordeditor.T8DataFilePropertyAccessUpdate;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFilePropertyAccessUpdate extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE_PROPERTY_ACCESS";

    public T8DtDataFilePropertyAccessUpdate(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileValueUpdate.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFilePropertyAccessUpdate accessUpdate;
            JsonObject jsonUpdate;

            accessUpdate = (T8DataFilePropertyAccessUpdate)object;

            // Create the json object.
            jsonUpdate = new JsonObject();
            jsonUpdate.add("recordId", accessUpdate.getRecordId());
            jsonUpdate.add("propertyId", accessUpdate.getPropertyId());
            jsonUpdate.add("visible", accessUpdate.isVisible());
            jsonUpdate.add("required", accessUpdate.isRequired());
            jsonUpdate.add("editable", accessUpdate.isEditable());
            jsonUpdate.add("fftEditable", accessUpdate.isFftEditable());
            jsonUpdate.add("insertEnabled", accessUpdate.isInsertEnabled());
            jsonUpdate.add("updateEnabled", accessUpdate.isUpdateEnabled());
            jsonUpdate.add("deleteEnabled", accessUpdate.isDeleteEnabled());
            return jsonUpdate;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFilePropertyAccessUpdate deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DataFilePropertyAccessUpdate accessUpdate;
            JsonObject jsonUpdate;

            jsonUpdate = (JsonObject)jsonValue;
            accessUpdate = new T8DataFilePropertyAccessUpdate();
            accessUpdate.setRecordId(jsonUpdate.getString("recordId"));
            accessUpdate.setPropertyId(jsonUpdate.getString("propertyId"));
            accessUpdate.setVisible(jsonUpdate.getBoolean("visible"));
            accessUpdate.setRequired(jsonUpdate.getBoolean("required"));
            accessUpdate.setEditable(jsonUpdate.getBoolean("editable"));
            accessUpdate.setFftEditable(jsonUpdate.getBoolean("fftEditable"));
            accessUpdate.setInsertEnabled(jsonUpdate.getBoolean("insertEnabled"));
            accessUpdate.setUpdateEnabled(jsonUpdate.getBoolean("updateEnabled"));
            accessUpdate.setDeleteEnabled(jsonUpdate.getBoolean("deleteEnabled"));
            return accessUpdate;
        }
        else return null;
    }
}
