package com.pilog.t8.data.document.path;

/**
 * @author Bouwer du Preez
 */
public class StepModifiers
{
    private boolean includeNullValues;
    private boolean independent;

    public StepModifiers()
    {
        this.includeNullValues = false;
    }

    public boolean isIndependent()
    {
        return independent;
    }

    public void steIndependent(boolean independent)
    {
        this.independent = independent;
    }

    public boolean isIncludeNullValues()
    {
        return includeNullValues;
    }

    public void setIncludeNullValues(boolean includeNullValues)
    {
        this.includeNullValues = includeNullValues;
    }

    public void parseModifiers(String modifierString)
    {
        if (modifierString.length() > 0)
        {
            String[] modifiers;

            modifiers = modifierString.split(",");
            for (String modifier : modifiers)
            {
                switch (modifier)
                {
                    case "INCLUDE_NULL_VALUES":
                        includeNullValues = true;
                        break;
                    case "INDEPENDENT":
                        independent = true;
                        break;
                }
            }
        }
    }
}
