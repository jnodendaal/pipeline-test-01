package com.pilog.t8.api.datarecordeditor;

import com.pilog.t8.data.document.datarecord.ValueContent;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileValueUpdate implements Serializable
{
    private String recordId;
    private String propertyId;
    private String fieldId;
    private ValueContent newValue;

    public T8DataFileValueUpdate()
    {
    }

    public T8DataFileValueUpdate(String recordId, String propertyId, String fieldId)
    {
        this();
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public ValueContent getNewValue()
    {
        return newValue;
    }

    public void setNewValue(ValueContent newValue)
    {
        this.newValue = newValue;
    }

    public boolean isEquivalent(T8DataFileValueUpdate update)
    {
        if (update == null) return false;
        else if (!Objects.equals(recordId, update.getRecordId())) return false;
        else if (!Objects.equals(propertyId, update.getPropertyId())) return false;
        else if (!Objects.equals(fieldId, update.getFieldId())) return false;
        else
        {
            ValueContent updateContent;

            updateContent = update.getNewValue();
            if (newValue == null) return updateContent == null;
            else if (!newValue.isEquivalentTo(updateContent)) return false;
            else return true;
        }
    }
}
