package com.pilog.t8.data.document.datarecord;

/**
 * This enum describes the distinct types of meta applicable to the data records
 * used by the system.
 * @author Bouwer du Preez
 */
public enum RecordMetaDataType
{
    DESCRIPTION,
    DUPLICATE_DETECTION_HASH,
    DATA_OBJECT,
    ALL,
    NONE
}
