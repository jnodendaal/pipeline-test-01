package com.pilog.t8.data.document.datarecord.view;

import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordViewConstructor
{
    public Map<String, Object> constructViewData(DataRecord dataRecord, T8DataFileAlteration alteration, String languageId);
}
