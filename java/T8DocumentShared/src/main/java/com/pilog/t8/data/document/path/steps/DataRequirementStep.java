package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.path.PathRoot;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class DataRequirementStep extends DefaultPathStep implements PathStep
{
    public DataRequirementStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof PathRoot)
        {
            return evaluateStep(ArrayLists.newArrayList(((PathRoot)input).getDataRecord().getDataRequirement()));
        }
        else if (input instanceof DataRecord)
        {
            return evaluateStep(ArrayLists.newArrayList(((DataRecord)input).getDataRequirement()));
        }
        else if (input instanceof DataRequirement)
        {
            // Recursion: call this method using the parent DR Instance.
            return evaluateStep(rootInput, ((DataRequirement)input).getParentDataRequirementInstance());
        }
        else if (input instanceof DataRequirementInstance)
        {
            List<DataRequirementInstance> subInstances;
            List<DataRequirement> subRequirements;

            subInstances = ((DataRequirementInstance)input).getSubInstances();
            subRequirements = new ArrayList<DataRequirement>();
            for (DataRequirementInstance subInstance : subInstances)
            {
                subRequirements.add(subInstance.getDataRequirement());
            }

            return evaluateStep(subRequirements);
        }
        else if (input instanceof RecordProperty)
        {
            List<DataRecord> subRecords;
            List<DataRequirement> subDrs;
            RecordProperty inputProperty;

            inputProperty = (RecordProperty)input;
            subRecords = inputProperty.getParentDataRecord().getSubRecordsReferencedFromProperty(inputProperty.getPropertyID());
            subDrs = new ArrayList<DataRequirement>();
            for (DataRecord subRecord : subRecords)
            {
                subDrs.add(subRecord.getDataRequirement());
            }

            return evaluateStep(subDrs);
        }
        else if (input instanceof List)
        {
            List<DataRequirementInstance> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<DataRequirementInstance>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<DataRequirementInstance>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Data Requirement step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<DataRequirement> evaluateStep(List<DataRequirement> requirementList)
    {
        // If we have ID's to use for narrowing of the requirement list do it now.
        if (idList != null)
        {
            Iterator<DataRequirement> requirementIterator;

            requirementIterator = requirementList.iterator();
            while (requirementIterator.hasNext())
            {
                DataRequirement nextRequirement;

                nextRequirement = requirementIterator.next();
                if (!idList.contains(nextRequirement.getConceptID()))
                {
                    requirementIterator.remove();
                }
            }
        }

        // Now run through the remaining requirements and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<DataRequirement> requirementIterator;

            // Iterator over the requirements and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            requirementIterator = requirementList.iterator();
            while (requirementIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                DataRequirement nextRequirement;

                // Create a map containing the input parameters available to the predicate expression.
                nextRequirement = requirementIterator.next();
                inputParameters = getPredicateExpressionParameters(nextRequirement.getAttributes());
                inputParameters.put("DR_INSTANCE_ID", nextRequirement.getParentDataRequirementInstance().getConceptID());
                inputParameters.put("DR_ID", nextRequirement.getConceptID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        requirementIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return requirementList;
    }
}
