package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class SectionAccessStep extends DefaultPathStep implements PathStep
{
    public SectionAccessStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecordAccessLayer)
        {
            return evaluateSectionStep(((DataRecordAccessLayer)input).getSectionAccessLayers());
        }
        else if (input instanceof List)
        {
            List<SectionAccessLayer> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<SectionAccessLayer>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<SectionAccessLayer>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Section access step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<SectionAccessLayer> evaluateSectionStep(List<SectionAccessLayer> accessLayerList)
    {
        // If we have ID's to use for narrowing of the list do it now.
        if (idList != null)
        {
            Iterator<SectionAccessLayer> accessLayerIterator;

            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                SectionAccessLayer nextAccessLayer;

                nextAccessLayer = accessLayerIterator.next();
                if (!idList.contains(nextAccessLayer.getSectionID()))
                {
                    accessLayerIterator.remove();
                }
            }
        }

        // Now run through the remaining items in the list and evaluate the predicate expression (if any).
        if (predicateExpression != null)
        {
            Iterator<SectionAccessLayer> accessLayerIterator;

            // Iterate over the sections and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                SectionAccessLayer nextAccessLayer;

                // Create a map containing the input parameters available to the predicate expression.
                nextAccessLayer = accessLayerIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("ACCESS_LAYER", nextAccessLayer);
                inputParameters.put("SECTION_ID", nextAccessLayer.getSectionID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        accessLayerIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return accessLayerList;
    }
}
