package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_TRM_RETRIEVE_TERMINOLOGY = "@OS_API_TRM_RETRIEVE_TERMINOLOGY";
    public static final String OPERATION_API_TRM_RETRIEVE_TERMINOLOGY_LIST = "@OS_API_TRM_RETRIEVE_TERMINOLOGY_LIST";
    public static final String OPERATION_API_TRM_SAVE_TERMINOLOGY = "@OS_API_TRM_SAVE_TERMINOLOGY";

    public static final String PARAMETER_LANGUAGE_ID_LIST = "$P_LANGUAGE_ID_LIST";
    public static final String PARAMETER_CONCEPT_ID_LIST = "$P_CONCEPT_ID_LIST";
    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_CONCEPT_ID = "$P_CONCEPT_ID";
    public static final String PARAMETER_TERMINOLOGY_LIST = "$P_TERMINOLOGY_LIST";
    public static final String PARAMETER_TERMINOLOGY = "$P_TERMINOLOGY";
    public static final String PARAMETER_INCLUDE_DELETION = "$P_INCLUDE_DELETION";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_TRM_RETRIEVE_TERMINOLOGY_LIST);
            definition.setMetaDisplayName("Retrieve Terminology");
            definition.setMetaDescription("Retrieves the requested concept terminology as specified for the current organization.");
            definition.setClassName("com.pilog.t8.api.T8TerminologyApiOperations$ApiRetrieveTerminologyList");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID_LIST, "Concept Id List", "The list of concept Id's for which terminology will be retrieved.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID_LIST, "Language Id List", "The list of terminology languages to retrieve.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERMINOLOGY_LIST, "Terminology List", "The list of concept terminologies retrieved.", T8DataType.LIST));
            definitions.add(definition);
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_TRM_RETRIEVE_TERMINOLOGY);
            definition.setMetaDisplayName("Retrieve Terminology");
            definition.setMetaDescription("Retrieves the requested concept terminology as specified for the current organization.");
            definition.setClassName("com.pilog.t8.api.T8TerminologyApiOperations$ApiRetrieveTerminology");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept Id List", "The list of concept Id's for which terminology will be retrieved.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language Id List", "The list of terminology languages to retrieve.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERMINOLOGY, "Terminology List", "The list of concept terminologies retrieved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_TRM_SAVE_TERMINOLOGY);
            definition.setMetaDisplayName("Save Terminology");
            definition.setMetaDescription("Save the terminology concept.");
            definition.setClassName("com.pilog.t8.api.T8TerminologyApiOperations$ApiSaveTerminology");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERMINOLOGY, "Terminology Concept", "The terminology concept for which terminology will be saved.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DELETION, "Deletion Enabled", "Indicator to show if the deletion of concept data are enabled.", T8DataType.BOOLEAN));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
