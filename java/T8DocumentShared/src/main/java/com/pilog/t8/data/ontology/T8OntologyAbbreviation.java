package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyAbbreviation implements Serializable
{
    private String id;
    private String irdi;
    private String termId;
    private String abbreviation;
    private boolean primary;
    private boolean standard;
    private boolean active;

    /**
     * Creates a new instance of the {@code T8OntologyAbbreviation} linked to
     * the specified term.
     *
     * @param id The {@code String} unique identifier for the current
     *      abbreviation
     * @param termId The {@code String} term ID to which the abbreviation is
     *      linked
     * @param abbreviation The actual abbreviation {@code String} value
     * @param primary Indicates whether or not this abbreviation is the primary abbreviation of its parent concept.
     * @param standard Whether or not the abbreviation has been standardized
     *      externally to the current client context
     * @param active {@code true} if the abbreviation is to be considered active
     *      within the client data structure
     */
    public T8OntologyAbbreviation(String id, String termId, String abbreviation, boolean primary, boolean standard, boolean active)
    {
        this.id = id;
        this.termId = termId;
        this.abbreviation = abbreviation;
        this.primary = primary;
        this.standard = standard;
        this.active = active;
    }

    public T8OntologyAbbreviation(String id, String termId, String abbreviation, boolean standard, boolean active)
    {
        this(id, termId, abbreviation, false, standard, active);
    }

    public T8OntologyAbbreviation copy()
    {
        T8OntologyAbbreviation copy;

        copy = new T8OntologyAbbreviation(id, termId, abbreviation, primary, standard, active);
        return copy;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public String getTermID()
    {
        return termId;
    }

    public void setTermID(String termID)
    {
        this.termId = termID;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public boolean isPrimary()
    {
        return primary;
    }

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public boolean isStandard()
    {
        return standard;
    }

    public void setStandard(boolean standard)
    {
        this.standard = standard;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isEqualTo(T8OntologyAbbreviation inputAbbreviation)
    {
        if (inputAbbreviation == null) return false;
        else if (!Objects.equals(id, inputAbbreviation.getID())) return false;
        else if (!Objects.equals(irdi, inputAbbreviation.getIrdi())) return false;
        else if (!Objects.equals(termId, inputAbbreviation.getTermID())) return false;
        else if (!Objects.equals(abbreviation, inputAbbreviation.getAbbreviation())) return false;
        else if (!Objects.equals(standard, inputAbbreviation.isStandard())) return false;
        else if (!Objects.equals(active, inputAbbreviation.isActive())) return false;
        else return true;
    }

    @Override
    public String toString()
    {
        return "T8OntologyAbbreviation{" + "id=" + id + ", irdi=" + irdi + ", abbreviation=" + abbreviation + '}';
    }
}
