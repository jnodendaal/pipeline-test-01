package com.pilog.t8.data.document.requirementtype;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public enum RequirementType
{
    // Non-Data Type Codes.
// Non-Data Type Codes.
// Non-Data Type Codes.
// Non-Data Type Codes.
    DATA_REQUIREMENT_INSTANCE(
            "DATA_REQUIREMENT_INSTANCE",
            "Data Requirement Instance",
            T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE,
            null,
            null,
            false),
    DATA_REQUIREMENT("DATA_REQUIREMENT",
            "Data Requirement",
            T8OntologyConceptType.DATA_REQUIREMENT,
            null,
            null,
            false),
    SECTION("SECTION",
            "Section", T8OntologyConceptType.SECTION,
            null,
            null,
            false),
    PROPERTY("PROPERTY",
            "Property",
            T8OntologyConceptType.PROPERTY,
            null,
            ArrayLists.typeSafeList(
                RequirementAttribute.UNIQUE
            ),
            false),

    // Data Type Codes.
    ATTACHMENT("ATTACHMENT",
            "Attachment",
            null,
            null,
            ArrayLists.typeSafeList(
                RequirementAttribute.ALLOW_DUPLICATE_FILENAMES,
                RequirementAttribute.MAXIMUM_ATTACHMENTS,
                RequirementAttribute.FILE_TYPE_RESTRICTION,
                RequirementAttribute.FILE_SIZE_RESTRICTION
            ),
            true),
    BAG_TYPE("BAG_TYPE",
            "Bag",
            null,
            null,
            null,
            false),
    BOOLEAN_TYPE("BOOLEAN_TYPE",
            "Boolean",
            null,
            null,
            null,
            false),
    CHOICE_TYPE("CHOICE_TYPE",
            "Choice",
            null,
            null,
            null,
            false),
    COMPLEX_TYPE("COMPLEX_TYPE",
            "Complex Number",
            null,
            null,
            null,
            false),
    COMPOSITE_TYPE("COMPOSITE_TYPE",
            "Composite",
            null,
            null,
            null,
            true),
    CONCEPT_TYPE("CONCEPT_TYPE",
            "Concept Type",
            null,
            null,
            null,
            false),
    CONCEPT_RELATIONSHIP_TYPE("CONCEPT_RELATIONSHIP_TYPE",
            "Concept Relationship",
            T8OntologyConceptType.CONCEPT_RELATIONSHIP_GRAPH,
            null,
            null,
            false),
    CONTROLLED_CONCEPT("CONTROLLED_CONCEPT",
            "Controlled Concept",
            null,
            T8OntologyConceptType.VALUE,
            ArrayLists.typeSafeList
            (
                RequirementAttribute.PRESCRIBED_VALUE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE,
                RequirementAttribute.ALLOW_NEW_VALUE
            ),
            false),
    CONCEPT_DEPENDENCY_TYPE("CONCEPT_DEPENDENCY_TYPE",
            "Concept Dependency",
            null,
            null,
            null,
            false),
    CURRENCY_TYPE("CURRENCY_TYPE",
            "Currency",
            null,
            null,
            null,
            false),
    DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE("DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE",
            "Data Requirement Instance Dependency",
            T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE,
            null,
            null,
            false),
    DATA_REQUIREMENT_DEPENDENCY_TYPE("DATA_REQUIREMENT_DEPENDENCY_TYPE",
            "Data Requirement Dependency",
            T8OntologyConceptType.DATA_REQUIREMENT,
            null,
            null,
            false),
    DATE_TYPE("DATE_TYPE",
            "Date",
            null,
            null,
            ArrayLists.typeSafeList
            (
                RequirementAttribute.DATE_TIME_FORMAT_PATTERN
            ),
            false),
    DATE_TIME_TYPE("DATE_TIME_TYPE",
            "Date & Time",
            null,
            null,
            ArrayLists.typeSafeList
            (
                RequirementAttribute.DATE_TIME_FORMAT_PATTERN
            ),
            false),
    DOCUMENT_REFERENCE("DOCUMENT_REFERENCE",
            "Document Reference",
            null,
            null,
            ArrayLists.typeSafeList
            (
                RequirementAttribute.PRESCRIBED_VALUE,
                RequirementAttribute.INDEPENDENT,
                RequirementAttribute.LOCAL,
                RequirementAttribute.MINIMUM_REFERENCES,
                RequirementAttribute.MAXIMUM_REFERENCES,
                RequirementAttribute.MAXIMUM_REFERENCES_PER_DR_INSTANCE
            ),
            true),
    FIELD_TYPE("FIELD_TYPE",
            "Field",
            T8OntologyConceptType.PROPERTY,
            null,
            null,
            false),
    FIELD_DEPENDENCY_TYPE("FIELD_DEPENDENCY_TYPE",
            "Field Dependency",
            T8OntologyConceptType.PROPERTY,
            null,
            null,
            false),
    FILE_TYPE("FILE_TYPE",
            "File",
            null,
            null,
            null,
            false),
    IMAGINARY_PART_TYPE("IMAGINARY_PART_TYPE",
            "Imaginary Part",
            null,
            null,
            null,
            false),
    INTEGER_TYPE("INTEGER_TYPE",
            "Integer",
            null,
            null,
            ArrayLists.typeSafeList(RequirementAttribute.FORMAT_PATTERN,
                RequirementAttribute.FORMAT_DESCRIPTION,
                RequirementAttribute.MINIMUM_NUMERIC_VALUE,
                RequirementAttribute.MAXIMUM_NUMERIC_VALUE,
                RequirementAttribute.PRESCRIBED_VALUE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE
            ),
            false),
    LANGUAGE_TYPE("LANGUAGE_TYPE",
            "Language",
            null,
            null,
            null,
            false),
    LOCALIZED_TEXT_TYPE("LOCALIZED_TEXT_TYPE",
            "Localized Text",
            null,
            null,
            null,
            false),
    LOCAL_STRING_TYPE("LOCAL_STRING_TYPE",
            "Local String",
            null,
            null,
            null,
            false),
    LOWER_BOUND_NUMBER("LOWER_BOUND_NUMBER",
            "Lower Bound Number",
            null,
            null,
            null,
            false),
    MEASURED_NUMBER("MEASURED_NUMBER",
            "Measured Number",
            null,
            null,
            ArrayLists.typeSafeList(RequirementAttribute.PRESCRIBED_VALUE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE
            ),
            false),
    MEASURED_RANGE("MEASURED_RANGE",
            "Measured Range",
            null,
            null,
            ArrayLists.typeSafeList(RequirementAttribute.PRESCRIBED_VALUE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE
            ),
            false),
    NUMBER("NUMBER",
            "Number",
            null,
            null,
            ArrayLists.typeSafeList(RequirementAttribute.FORMAT_PATTERN,
                RequirementAttribute.FORMAT_DESCRIPTION,
                RequirementAttribute.MINIMUM_NUMERIC_VALUE,
                RequirementAttribute.MAXIMUM_NUMERIC_VALUE,
                RequirementAttribute.PRESCRIBED_VALUE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE
            ),
            false),
    PRESCRIBED_CURRENCY_TYPE("PRESCRIBED_CURRENCY_TYPE",
            "Prescribed Currency",
            null,
            null,
            null,
            false),
    PROPERTY_DEPENDENCY_TYPE("PROPERTY_DEPENDENCY_TYPE",
            "Property Dependency",
            T8OntologyConceptType.PROPERTY,
            null,
            null,
            false),
    QUALIFIER_OF_MEASURE("QUALIFIER_OF_MEASURE",
            "Qualifier of Measure",
            null,
            T8OntologyConceptType.QUALIFIER_OF_MEASURE,
            ArrayLists.typeSafeList(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE
            ),
            false),
    RATIONAL_TYPE("RATIONAL_TYPE",
            "Rational Number",
            null,
            null,
            null,
            false),
    REAL_PART_TYPE("REAL_PART_TYPE",
            "Real Part",
            null,
            null,
            null,
            false),
    REAL_TYPE("REAL_TYPE",
            "Real Number",
            null,
            null,
            ArrayLists.typeSafeList(RequirementAttribute.FORMAT_PATTERN,
                RequirementAttribute.FORMAT_DESCRIPTION,
                RequirementAttribute.MINIMUM_NUMERIC_VALUE,
                RequirementAttribute.MAXIMUM_NUMERIC_VALUE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE,
                RequirementAttribute.PRESCRIBED_VALUE
            ),
            false),
    SEQUENCE_TYPE("SEQUENCE_TYPE",
            "Sequence",
            null,
            null,
            null,
            false),
    SET_TYPE("SET_TYPE",
            "Set",
            null,
            null,
            null,
            false),
    STRING_TYPE("STRING_TYPE",
            "String",
            null,
            null,
            ArrayLists.typeSafeList(RequirementAttribute.FORMAT_PATTERN,
                RequirementAttribute.FORMAT_MASK,
                RequirementAttribute.MAXIMUM_STRING_LENGTH,
                RequirementAttribute.FORMAT_DESCRIPTION,
                RequirementAttribute.CASE,
                RequirementAttribute.RESTRICT_TO_APPROVED_VALUE,
                RequirementAttribute.PRESCRIBED_VALUE
            ),
            false),
    TEXT_TYPE("TEXT_TYPE",
            "Text",
            null,
            null,
            ArrayLists.typeSafeList(
                RequirementAttribute.MAXIMUM_STRING_LENGTH,
                RequirementAttribute.CASE
            ),
            false),
    TIME_TYPE("TIME_TYPE",
            "Time",
            null,
            null,
            ArrayLists.typeSafeList(
                    RequirementAttribute.DATE_TIME_FORMAT_PATTERN
            ),
            false),
    UNIT_OF_MEASURE("UNIT_OF_MEASURE",
            "Unit of Measure",
            null,
            T8OntologyConceptType.UNIT_OF_MEASURE,
            ArrayLists.typeSafeList(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE
            ),
            false),
    UPPER_BOUND_NUMBER("UPPER_BOUND_NUMBER",
            "Upper Bound Number",
            null,
            null,
            null,
            false),
    ONTOLOGY_CLASS("ONTOLOGY_CLASS",
            "Ontology Class",
            T8OntologyConceptType.ONTOLOGY_CLASS,
            null,
            null,
            false),
    YEAR_TYPE("YEAR_TYPE",
            "Year",
            null,
            null,
            null,
            false),
    YEAR_MONTH_TYPE("YEAR_MONTH_TYPE",
            "Year Month",
            null,
            null,
            null,
            false);

    public static final String DEFAULT_POSITIVE_INTEGER_TYPE_FORMAT = "#";
    public static final String DEFAULT_REAL_TYPE_FORMAT = "#.####################";

    private final String id;
    private final String displayName;
    private final T8OntologyConceptType requirementValueConceptType;
    private final T8OntologyConceptType recordValueConceptType;
    private final List<RequirementAttribute> attributes;
    private final boolean collectionType;

    RequirementType(String id, String displayName, T8OntologyConceptType requirementValueConceptType, T8OntologyConceptType recordValueConceptType, List<RequirementAttribute> attributes, boolean collectionType)
    {
        this.id = id;
        this.displayName = displayName;
        this.requirementValueConceptType = requirementValueConceptType;
        this.recordValueConceptType = recordValueConceptType;
        this.attributes = attributes;
        this.collectionType = collectionType;
    }

    public static RequirementType getRequirementType(String id)
    {
        for (RequirementType type : RequirementType.values())
        {
            if (type.getID().equals(id))
            {
                return type;
            }
        }

        return null;
    }

    public String getID()
    {
        return id;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    @Override
    public String toString()
    {
        return id;
    }

    public T8OntologyConceptType getRequirementValueConceptType()
    {
        return requirementValueConceptType;
    }

    public T8OntologyConceptType getRecordValueConceptType()
    {
        return recordValueConceptType;
    }

    public boolean isConceptRequirementValue()
    {
        return requirementValueConceptType != null;
    }

    public boolean isConceptRecordValue()
    {
        return recordValueConceptType != null;
    }

    public boolean isCollectionValue()
    {
        return collectionType;
    }

    public List<RequirementAttribute> getAttributes()
    {
        return attributes;
    }

    @SuppressWarnings("FinalMethod")
    public static final ArrayList<RequirementType> getRootDataTypes()
    {
        ArrayList<RequirementType> typeList;

        typeList = new ArrayList<>();
        typeList.add(ATTACHMENT);
        typeList.add(BAG_TYPE);
        typeList.add(BOOLEAN_TYPE);
        typeList.add(CHOICE_TYPE);
        typeList.add(COMPLEX_TYPE);
        typeList.add(COMPOSITE_TYPE);
        typeList.add(CONTROLLED_CONCEPT);
        typeList.add(CURRENCY_TYPE);
        typeList.add(DATE_TYPE);
        typeList.add(DATE_TIME_TYPE);
        typeList.add(FILE_TYPE);
        typeList.add(INTEGER_TYPE);
        typeList.add(DOCUMENT_REFERENCE);
        typeList.add(LOCALIZED_TEXT_TYPE);
        typeList.add(LOCAL_STRING_TYPE);
        typeList.add(MEASURED_NUMBER);
        typeList.add(MEASURED_RANGE);
        typeList.add(RATIONAL_TYPE);
        typeList.add(REAL_TYPE);
        typeList.add(NUMBER);
        typeList.add(SEQUENCE_TYPE);
        typeList.add(SET_TYPE);
        typeList.add(STRING_TYPE);
        typeList.add(TEXT_TYPE);
        typeList.add(TIME_TYPE);
        typeList.add(YEAR_TYPE);
        typeList.add(YEAR_MONTH_TYPE);

        return typeList;
    }

    /**
     * Returns the list of all data types for which direct RecordValue object
     * can be created.
     *
     * @return
     */
    @SuppressWarnings("FinalMethod")
    public static final ArrayList<RequirementType> getValueDataTypes()
    {
        ArrayList<RequirementType> typeList;

        typeList = new ArrayList<>();
        typeList.add(ATTACHMENT);
        typeList.add(BAG_TYPE);
        typeList.add(BOOLEAN_TYPE);
        typeList.add(CHOICE_TYPE);
        typeList.add(COMPLEX_TYPE);
        typeList.add(COMPOSITE_TYPE);
        typeList.add(CONTROLLED_CONCEPT);
        typeList.add(CURRENCY_TYPE);
        typeList.add(DATE_TYPE);
        typeList.add(DATE_TIME_TYPE);
        typeList.add(DOCUMENT_REFERENCE);
        typeList.add(FIELD_TYPE);
        typeList.add(FILE_TYPE);
        typeList.add(IMAGINARY_PART_TYPE);
        typeList.add(INTEGER_TYPE);
        typeList.add(LANGUAGE_TYPE);
        typeList.add(LOCALIZED_TEXT_TYPE);
        typeList.add(LOCAL_STRING_TYPE);
        typeList.add(LOWER_BOUND_NUMBER);
        typeList.add(MEASURED_NUMBER);
        typeList.add(MEASURED_RANGE);
        typeList.add(NUMBER);
        typeList.add(ONTOLOGY_CLASS);
        typeList.add(PRESCRIBED_CURRENCY_TYPE);
        typeList.add(QUALIFIER_OF_MEASURE);
        typeList.add(RATIONAL_TYPE);
        typeList.add(REAL_PART_TYPE);
        typeList.add(REAL_TYPE);
        typeList.add(SEQUENCE_TYPE);
        typeList.add(SET_TYPE);
        typeList.add(STRING_TYPE);
        typeList.add(TEXT_TYPE);
        typeList.add(TIME_TYPE);
        typeList.add(UNIT_OF_MEASURE);
        typeList.add(UPPER_BOUND_NUMBER);
        typeList.add(ONTOLOGY_CLASS);
        typeList.add(YEAR_TYPE);
        typeList.add(YEAR_MONTH_TYPE);

        return typeList;
    }

    @SuppressWarnings("FinalMethod")
    public static final ArrayList<RequirementType> getNumericDataTypes()
    {
        ArrayList<RequirementType> typeList;

        typeList = new ArrayList<>();
        typeList.add(COMPLEX_TYPE);
        typeList.add(INTEGER_TYPE);
        typeList.add(RATIONAL_TYPE);
        typeList.add(REAL_TYPE);
        typeList.add(NUMBER);

        return typeList;
    }

    public static final List<String> getNumericTypeIds()
    {
        ArrayList<String> typeList;

        typeList = new ArrayList<>();
        typeList.add(COMPLEX_TYPE.toString());
        typeList.add(INTEGER_TYPE.toString());
        typeList.add(RATIONAL_TYPE.toString());
        typeList.add(REAL_TYPE.toString());
        typeList.add(NUMBER.toString());

        return typeList;
    }

    /**
     * Returns all data types that specify a requirement's dependency on another
     * requirement.
     *
     * @return All data types that specify a requirement's dependency on another
     * requirement.
     */
    @SuppressWarnings("FinalMethod")
    public static final ArrayList<RequirementType> getDependencyDataTypes()
    {
        ArrayList<RequirementType> typeList;

        typeList = new ArrayList<>();
        typeList.add(DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE);
        typeList.add(PROPERTY_DEPENDENCY_TYPE);
        typeList.add(FIELD_DEPENDENCY_TYPE);
        typeList.add(CONCEPT_DEPENDENCY_TYPE);

        return typeList;
    }

    @SuppressWarnings("FinalMethod")
    public static final boolean isDependencyType(RequirementType requirementType)
    {
        return getDependencyDataTypes().contains(requirementType);
    }

    /**
     * Returns all the data types with a single value requirement level. I.E.
     * all the types for which a value can be added to a property by adding a
     * single String value linking to a single Value Requirement.
     *
     * @return The list of all simple types.
     */
    @SuppressWarnings("FinalMethod")
    public static final ArrayList<RequirementType> getSimpleDataTypes()
    {
        ArrayList<RequirementType> typeList;

        typeList = new ArrayList<>();
        typeList.add(YEAR_TYPE);
        typeList.add(INTEGER_TYPE);
        typeList.add(REAL_TYPE);
        typeList.add(NUMBER);
        typeList.add(STRING_TYPE);
        typeList.add(TEXT_TYPE);
        typeList.add(FILE_TYPE);
        typeList.add(BOOLEAN_TYPE);
        typeList.add(DATE_TYPE);
        typeList.add(TIME_TYPE);
        typeList.add(DATE_TIME_TYPE);

        return typeList;
    }

    @SuppressWarnings("FinalMethod")
    public static final ArrayList<String> getAllDataTypeIDs()
    {
        ArrayList<String> idList;

        idList = new ArrayList<>();
        for (RequirementType dataType : getAllDataTypes())
        {
            idList.add(dataType.getID());
        }

        return idList;
    }

    @SuppressWarnings("FinalMethod")
    public static final ArrayList<RequirementType> getAllDataTypes()
    {
        ArrayList<RequirementType> typeList;

        typeList = new ArrayList<>();
        typeList.add(ATTACHMENT);
        typeList.add(BAG_TYPE);
        typeList.add(BOOLEAN_TYPE);
        typeList.add(CHOICE_TYPE);
        typeList.add(COMPLEX_TYPE);
        typeList.add(COMPOSITE_TYPE);
        typeList.add(CONTROLLED_CONCEPT);
        typeList.add(CURRENCY_TYPE);
        typeList.add(DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE);
        typeList.add(DATE_TYPE);
        typeList.add(DATE_TIME_TYPE);
        typeList.add(DOCUMENT_REFERENCE);
        typeList.add(FIELD_TYPE);
        typeList.add(FILE_TYPE);
        typeList.add(IMAGINARY_PART_TYPE);
        typeList.add(INTEGER_TYPE);
        typeList.add(LANGUAGE_TYPE);
        typeList.add(LOCALIZED_TEXT_TYPE);
        typeList.add(LOCAL_STRING_TYPE);
        typeList.add(LOWER_BOUND_NUMBER);
        typeList.add(MEASURED_NUMBER);
        typeList.add(MEASURED_RANGE);
        typeList.add(NUMBER);
        typeList.add(ONTOLOGY_CLASS);
        typeList.add(PRESCRIBED_CURRENCY_TYPE);
        typeList.add(QUALIFIER_OF_MEASURE);
        typeList.add(RATIONAL_TYPE);
        typeList.add(REAL_PART_TYPE);
        typeList.add(REAL_TYPE);
        typeList.add(SEQUENCE_TYPE);
        typeList.add(SET_TYPE);
        typeList.add(STRING_TYPE);
        typeList.add(TEXT_TYPE);
        typeList.add(TIME_TYPE);
        typeList.add(UNIT_OF_MEASURE);
        typeList.add(UPPER_BOUND_NUMBER);
        typeList.add(ONTOLOGY_CLASS);
        typeList.add(YEAR_TYPE);
        typeList.add(YEAR_MONTH_TYPE);

        return typeList;
    }

}
