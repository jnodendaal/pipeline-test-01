package com.pilog.t8.definition.data.document.datarecord.access.logic;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.access.logic.T8LogicDataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LogicDataRecordAccessDefinition extends T8Definition implements T8DataRecordAccessDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_ACCESS_V2";
    public static final String DISPLAY_NAME = "Logic Based Data Record Access";
    public static final String DESCRIPTION = "A definition that specifies access to certain parts of a data record using separate logic scripts.";
    public enum Datum
    {
        DATA_FILE_STRUCTURE_ID_LIST,
        INCLUDED_ACCESS_IDENTIFIERS,
        INITIAL_STATE_LOGIC_IDENTIFIERS,
        STATIC_STATE_LOGIC_IDENTIFIERS,
        DYNAMIC_STATE_LOGIC_IDENTIFIERS,
        VALIDATION_LOGIC_IDENTIFIERS
    };
    // -------- Definition Meta-Data -------- //

    private List<T8DataAccessLogicDefinition> initialStateLogicDefinitions;
    private List<T8DataAccessLogicDefinition> staticStateLogicDefinitions;
    private List<T8DataAccessLogicDefinition> dynamicStateLogicDefinitions;
    private List<T8DataAccessLogicDefinition> validationLogicDefinitions;

    public T8LogicDataRecordAccessDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.DATA_FILE_STRUCTURE_ID_LIST.toString(), "Data File Structures",  "The Data File Structures to which this acccess layer is applicable."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.INCLUDED_ACCESS_IDENTIFIERS.toString(), "Included Access Identifiers",  "The identifiers of access layers to include in this one (sequence is important)."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.INITIAL_STATE_LOGIC_IDENTIFIERS.toString(), "Initial State Logic",  "The data access logic that is applied to a record when first created."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.STATIC_STATE_LOGIC_IDENTIFIERS.toString(), "Static State Logic",  "The data access logic that is applied only once to a record when it is loaded from its persisted state."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.DYNAMIC_STATE_LOGIC_IDENTIFIERS.toString(), "Dynamic State Logic",  "The data access logic that is applied to a record every time its content changes."));
        datumTypes.add(T8DefinitionDatumType.identifierList(Datum.VALIDATION_LOGIC_IDENTIFIERS.toString(), "Validation Logic",  "The data access logic that is applied to a record to verify its validity."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INCLUDED_ACCESS_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getTypeDefinitionMetaData(getRootProjectId(), T8LogicDataRecordAccessDefinition.TYPE_IDENTIFIER));
        else if (Datum.INITIAL_STATE_LOGIC_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataAccessLogicDefinition.GROUP_IDENTIFIER));
        else if (Datum.STATIC_STATE_LOGIC_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataAccessLogicDefinition.GROUP_IDENTIFIER));
        else if (Datum.DYNAMIC_STATE_LOGIC_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataAccessLogicDefinition.GROUP_IDENTIFIER));
        else if (Datum.VALIDATION_LOGIC_IDENTIFIERS.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataAccessLogicDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        List<String> includedAccessIdentifiers;
        List<String> initialStateLogicIdentifiers;
        List<String> staticStateLogicIdentifiers;
        List<String> dynamicStateLogicIdentifiers;
        List<String> validationLogicIdentifiers;
        T8DefinitionManager definitionManager;

        // Get the definition manager.
        definitionManager = context.getServerContext().getDefinitionManager();

        // Initialze collections.
        initialStateLogicDefinitions = new ArrayList<T8DataAccessLogicDefinition>();
        staticStateLogicDefinitions = new ArrayList<T8DataAccessLogicDefinition>();
        dynamicStateLogicDefinitions = new ArrayList<T8DataAccessLogicDefinition>();
        validationLogicDefinitions = new ArrayList<T8DataAccessLogicDefinition>();

        // Load the included definitions.
        includedAccessIdentifiers = getIncludedAccessIdentifiers();
        if (includedAccessIdentifiers != null)
        {
            for (String accessId : includedAccessIdentifiers)
            {
                T8LogicDataRecordAccessDefinition accessDefinition;

                accessDefinition = (T8LogicDataRecordAccessDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), accessId, inputParameters);
                if (accessDefinition != null)
                {
                    // Include initial state logic.
                    initialStateLogicDefinitions.addAll(accessDefinition.getInitialStateLogicDefinitions());

                    // Include static state logic.
                    staticStateLogicDefinitions.addAll(accessDefinition.getStaticStateLogicDefinitions());

                    // Include dynamic state logic.
                    dynamicStateLogicDefinitions.addAll(accessDefinition.getDynamicStateLogicDefinitions());

                    // Include validation logic.
                    validationLogicDefinitions.addAll(accessDefinition.getValidationLogicDefinitions());
                }
                else throw new Exception("Included access definition not foud: " + accessId);
            }
        }

        // Fetch initial state logic definitions.
        initialStateLogicIdentifiers = getInitialStateLogicIdentifiers();
        if (initialStateLogicIdentifiers != null)
        {
            for (String logicId : initialStateLogicIdentifiers)
            {
                T8DataAccessLogicDefinition logicDefinition;

                logicDefinition = (T8DataAccessLogicDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), logicId, inputParameters);
                if (logicDefinition != null)
                {
                    initialStateLogicDefinitions.add(logicDefinition);
                }
                else throw new Exception("Logic definition not foud: " + logicId);
            }
        }

        // Fetch staic state logic definitions.
        staticStateLogicIdentifiers = getStaticStateLogicIdentifiers();
        if (staticStateLogicIdentifiers != null)
        {
            for (String logicId : staticStateLogicIdentifiers)
            {
                T8DataAccessLogicDefinition logicDefinition;

                logicDefinition = (T8DataAccessLogicDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), logicId, inputParameters);
                if (logicDefinition != null)
                {
                    staticStateLogicDefinitions.add(logicDefinition);
                }
                else throw new Exception("Logic definition not foud: " + logicId);
            }
        }

        // Fetch dynamic state logic definitions.
        dynamicStateLogicIdentifiers = getDynamicStateLogicIdentifiers();
        if (dynamicStateLogicIdentifiers != null)
        {
            for (String logicId : dynamicStateLogicIdentifiers)
            {
                T8DataAccessLogicDefinition logicDefinition;

                logicDefinition = (T8DataAccessLogicDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), logicId, inputParameters);
                if (logicDefinition != null)
                {
                    dynamicStateLogicDefinitions.add(logicDefinition);
                }
                else throw new Exception("Logic definition not foud: " + logicId);
            }
        }

        // Fetch validation logic definitions.
        validationLogicIdentifiers = getValidationLogicIdentifiers();
        if (validationLogicIdentifiers != null)
        {
            for (String logicId : validationLogicIdentifiers)
            {
                T8DataAccessLogicDefinition logicDefinition;

                logicDefinition = (T8DataAccessLogicDefinition)definitionManager.getInitializedDefinition(context, getRootProjectId(), logicId, inputParameters);
                if (logicDefinition != null)
                {
                    validationLogicDefinitions.add(logicDefinition);
                }
                else throw new Exception("Logic definition not foud: " + logicId);
            }
        }
    }

    @Override
    public T8DataRecordAccessHandler getNewDataRecordAccessHandlerInstance(T8Context context)
    {
        return new T8LogicDataRecordAccessHandler(context, this);
    }

    public List<String> getDataFileStructureIDList()
    {
        return (List<String>)getDefinitionDatum(Datum.DATA_FILE_STRUCTURE_ID_LIST.toString());
    }

    public void setRootDRInstanceIDList(List<String> drInstanceIDList)
    {
        setDefinitionDatum(Datum.DATA_FILE_STRUCTURE_ID_LIST.toString(), drInstanceIDList);
    }

    public List<T8DataAccessLogicDefinition> getInitialStateLogicDefinitions()
    {
        return initialStateLogicDefinitions;
    }

    public List<T8DataAccessLogicDefinition> getStaticStateLogicDefinitions()
    {
        return staticStateLogicDefinitions;
    }

    public List<T8DataAccessLogicDefinition> getDynamicStateLogicDefinitions()
    {
        return dynamicStateLogicDefinitions;
    }

    public List<T8DataAccessLogicDefinition> getValidationLogicDefinitions()
    {
        return validationLogicDefinitions;
    }

    public List<String> getIncludedAccessIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.INCLUDED_ACCESS_IDENTIFIERS.toString());
    }

    public void setIncludedAccessIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.INCLUDED_ACCESS_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getInitialStateLogicIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.INITIAL_STATE_LOGIC_IDENTIFIERS.toString());
    }

    public void setInitialStateLogicIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.INITIAL_STATE_LOGIC_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getStaticStateLogicIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.STATIC_STATE_LOGIC_IDENTIFIERS.toString());
    }

    public void setStaticStateLogicIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.STATIC_STATE_LOGIC_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getDynamicStateLogicIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.DYNAMIC_STATE_LOGIC_IDENTIFIERS.toString());
    }

    public void setDynamicStateLogicIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.DYNAMIC_STATE_LOGIC_IDENTIFIERS.toString(), identifiers);
    }

    public List<String> getValidationLogicIdentifiers()
    {
        return (List<String>)getDefinitionDatum(Datum.VALIDATION_LOGIC_IDENTIFIERS.toString());
    }

    public void setValidationLogicIdentifiers(List<String> identifiers)
    {
        setDefinitionDatum(Datum.VALIDATION_LOGIC_IDENTIFIERS.toString(), identifiers);
    }
}
