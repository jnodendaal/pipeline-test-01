package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8IntegrationOperationDefinition extends T8JavaServerOperationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTEGRATION_OPERATION";
    public static final String DISPLAY_NAME = "Integration Operation";
    public static final String DESCRIPTION = "An operation that defines the integration service and operation that will be called when this operation is executed";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.PROJECT;
    public enum Datum
    {
        INTEGRATION_SERVICE_IDENTIFIER,
        INTEGRATION_SERVICE_OPERATION_IDENTIFIER,
        INPUT_PARAMETER_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public static final String P_OPERATION_ID = "$P_OPERATION_ID";

    public T8IntegrationOperationDefinition(String identifier)
    {
        super(identifier);
        createOutputParameters();
    }

    private void createOutputParameters()
    {
        ArrayList<T8DataParameterDefinition> outputParameterDefinitions;

        outputParameterDefinitions = new ArrayList<>();
        outputParameterDefinitions.add(new T8DataParameterDefinition(P_OPERATION_ID, "Operation ID", "Operation Identifier", T8DataType.GUID));

        setOutputParameterDefinitions(outputParameterDefinitions);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();

        // We want to hide the class name attribute
        datumTypes.stream().filter(datumType -> datumType.getIdentifier().equals(T8JavaServerOperationDefinition.Datum.CLASS_NAME.toString())).findFirst().get().setHidden(true);

        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.INTEGRATION_SERVICE_IDENTIFIER.toString(), "Integration Service", "The identifier of the integration service that will be called to perform the integration operation."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.INTEGRATION_SERVICE_OPERATION_IDENTIFIER.toString(), "Integration Service Operation", "The identifier of the operation on the integration service that will be called.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.INTEGRATION_SERVICE_IDENTIFIER.toString())));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.INPUT_PARAMETER_MAPPING.toString(), "Parameter Mapping (Operation -> Integration Operation)", "The mapping of input parameters from the this operation to the integration operation.").addDatumDependency(new T8DefinitionDatumDependency(TYPE_IDENTIFIER, Datum.INTEGRATION_SERVICE_IDENTIFIER.toString())));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumId) throws Exception
    {
        if (Datum.INTEGRATION_SERVICE_IDENTIFIER.toString().equals(datumId)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8IntegrationServiceDefinition.GROUP_IDENTIFIER));
        else if (Datum.INTEGRATION_SERVICE_OPERATION_IDENTIFIER.toString().equals(datumId))
        {
            String integrationId;

            integrationId = getIntegrationServiceIdentifier();
            if (!Strings.isNullOrEmpty(integrationId))
            {
                T8IntegrationServiceDefinition integrationServiceDefinition;

                integrationServiceDefinition = (T8IntegrationServiceDefinition) definitionContext.getRawDefinition(getRootProjectId(), integrationId);

                return createPublicIdentifierOptionsFromDefinitions(integrationServiceDefinition.getOperationDefinitions());

            }
            else return new ArrayList<>();
        }
        else if (Datum.INPUT_PARAMETER_MAPPING.toString().equals(datumId))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String operationIdentifier;

            optionList = new ArrayList<>();
            operationIdentifier = getIntegrationServiceOperationIdentifier();

            if (operationIdentifier != null)
            {
                List<T8IntegrationServiceOperationMappingDefinition> integrationOperations;
                List<T8DataParameterDefinition> remoteOperationParameterDefinitions = null;
                List<T8DataParameterDefinition> operationParameterDefinitions;
                HashMap<String, List<String>> identifierMap;

                operationParameterDefinitions = getInputParameterDefinitions();
                integrationOperations = ((T8IntegrationServiceDefinition) definitionContext.getRawDefinition(getRootProjectId(), getIntegrationServiceIdentifier())).getOperationDefinitions();

                for (T8IntegrationServiceOperationMappingDefinition integrationOperationDefinition : integrationOperations)
                {
                    if (integrationOperationDefinition.getPublicIdentifier().equals(operationIdentifier))
                    {
                        remoteOperationParameterDefinitions = integrationOperationDefinition.getInputParameters();
                    }
                }

                identifierMap = new HashMap<>();
                if ((remoteOperationParameterDefinitions != null) && (operationParameterDefinitions != null))
                {
                    for (T8DataParameterDefinition operationParameterDefinition : operationParameterDefinitions)
                    {
                        ArrayList<String> identifierList;

                        identifierList = new ArrayList<>();
                        for (T8DataParameterDefinition remoteOperationParameterDefinition : remoteOperationParameterDefinitions)
                        {
                            identifierList.add(remoteOperationParameterDefinition.getPublicIdentifier());
                        }

                        identifierMap.put(operationParameterDefinition.getIdentifier(), identifierList);
                    }
                }

                optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                return optionList;
            }
            return null;
        }
        else return super.getDatumOptions(definitionContext, datumId);
    }

    @Override
    public T8DefinitionActionHandler getDefinitionActionHandler(T8Context context)
    {
        return T8Reflections.getInstance("com.pilog.t8.developer.definitions.actionhandlers.document.integration.T8IntegrationOperationActionHandler", new Class<?>[]{T8Context.class, T8IntegrationOperationDefinition.class}, context, this);
    }

    @Override
    public T8ServerOperation getNewServerOperationInstance(T8Context context, String operationIid)
    {
        return T8Reflections.getInstance("com.pilog.t8.webservices.document.integration.T8IntegrationOperation", new Class<?>[]{T8Context.class, T8IntegrationOperationDefinition.class, String.class}, context, this, operationIid);
    }

    public String getIntegrationServiceIdentifier()
    {
        return getDefinitionDatum(Datum.INTEGRATION_SERVICE_IDENTIFIER);
    }

    public void setIntegrationServiceIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INTEGRATION_SERVICE_IDENTIFIER, identifier);
    }

    public String getIntegrationServiceOperationIdentifier()
    {
        return getDefinitionDatum(Datum.INTEGRATION_SERVICE_OPERATION_IDENTIFIER);
    }

    public void setIntegrationServiceOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.INTEGRATION_SERVICE_OPERATION_IDENTIFIER, identifier);
    }

    public Map<String, String> getInputParameterMapping()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING);
    }

    public void setInputParameterMapping(Map<String, String> mapping)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_MAPPING, mapping);
    }

}
