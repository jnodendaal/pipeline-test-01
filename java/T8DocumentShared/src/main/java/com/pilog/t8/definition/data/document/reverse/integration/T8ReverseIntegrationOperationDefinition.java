package com.pilog.t8.definition.data.document.reverse.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Willem De Bruyn
 */
public class T8ReverseIntegrationOperationDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REVERSE_INTEGRATION_SERVICE_OPERATION";
    public static final String GROUP_IDENTIFIER = "@DG_REVERSE_INTEGRATION_SERVICE_OPERATION";
    public static final String IDENTIFIER_PREFIX = "XFACE_REV_OP_";
    public static final String DISPLAY_NAME = "Reverse Intergation Operation";
    public static final String DESCRIPTION = "A definition that specifies the detail involved in an reverse interface operations for a domain.";

    public enum Datum
    {
        OPERATION_IDENTIFIER,
        SERVER_OPERATION_IDENTIFIER,
        SERVER_OPERATION_RESULT_PARAMETER_IDENTIFIER
    }
    // -------- Definition Meta-Data -------- //

    public T8ReverseIntegrationOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception {

    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATION_IDENTIFIER.toString(), "Operation Identifier", "The reverse integration operation identifier."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.SERVER_OPERATION_IDENTIFIER.toString(), "Server Operation Identifier", "The server operation identifier."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.SERVER_OPERATION_RESULT_PARAMETER_IDENTIFIER.toString(), "Result Parameter Identifier", "The parameter identifier of the parameter on the server operation that will recieve the reverse integration result."));
       return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.SERVER_OPERATION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER));
        else if(Datum.SERVER_OPERATION_RESULT_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String operationId;

            operationId = getServerOperationIdentifier();
            if(!Strings.isNullOrEmpty(operationId))
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition) definitionContext.getRawDefinition(getRootProjectId(), operationId);

                return createPublicIdentifierOptionsFromDefinitions(operationDefinition.getInputParameterDefinitions());
            } else return new ArrayList<>();
        }
        else return null;
    }

    public String getOperationIdentifier()
    {
        return getDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString());
    }

    public void setOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OPERATION_IDENTIFIER.toString(), identifier);
    }

    public String getServerOperationIdentifier()
    {
        return getDefinitionDatum(Datum.SERVER_OPERATION_IDENTIFIER.toString());
    }

    public void setServerOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SERVER_OPERATION_IDENTIFIER.toString(), identifier);
    }

    public String getServerOperationResultParameterIdentifier()
    {
        return (String)getDefinitionDatum(Datum.SERVER_OPERATION_RESULT_PARAMETER_IDENTIFIER.toString());
    }

    public void setServerOperationResultParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.SERVER_OPERATION_RESULT_PARAMETER_IDENTIFIER.toString(), identifier);
    }
}
