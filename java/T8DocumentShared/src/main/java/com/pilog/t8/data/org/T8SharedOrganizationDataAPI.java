package com.pilog.t8.data.org;

import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.api.T8Api;

/**
 * @author Bouwer du Preez
 */
public interface T8SharedOrganizationDataAPI extends T8Api
{
    public String getDefaultContentLanguageID();
    public T8OrganizationStructure getOrganizationStructure();
    public T8OntologyStructure getOntologyDataStructure();
    public T8OrganizationOntologyFactory getOntologyFactory();
    public DocPathExpressionEvaluator getDocPathExpressionEvaluator();
}
