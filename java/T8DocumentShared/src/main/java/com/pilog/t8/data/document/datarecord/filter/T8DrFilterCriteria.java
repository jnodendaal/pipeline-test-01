package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8SubDataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Pieter Strydom
 */
public class T8DrFilterCriteria implements T8DataFilterClause, T8RecordFilterClause, Serializable
{
    private final String drId;
    private final List<T8PropertyFilterCriterion> propertyCriteria;
    private final List<T8HistoryFilterCriterion> historyCriteria;
    private final List<T8CodeFilterCriterion> codeCriteria;
    private final List<T8OrganizationFilterCriterion> organizationCriteria;
    private T8DataFilterCriterion.DataFilterOperator instanceOperator;
    private Object instanceFilterValue;
    private String entityId;
    private String displayName;
    private String instanceLabel;
    private boolean allowPropertyAddition;
    private boolean allowHistoryAddition;
    private boolean allowCodeAddition;
    private boolean allowRemoval;
    private boolean allowInstanceSelection;

    public T8DrFilterCriteria(String drId)
    {
        this.drId = drId;
        this.allowPropertyAddition = true;
        this.allowHistoryAddition = true;
        this.allowInstanceSelection = true;
        this.propertyCriteria = new ArrayList<>();
        this.historyCriteria = new ArrayList<>();
        this.codeCriteria = new ArrayList<>();
        this.organizationCriteria = new ArrayList<>();
    }

    public String getDrId()
    {
        return drId;
    }

    public Object getInstanceFilterValue()
    {
        return instanceFilterValue;
    }

    public void setInstanceFilterValue(Object instanceFilterValue)
    {
        this.instanceFilterValue = instanceFilterValue;
    }

    public T8DataFilterCriterion.DataFilterOperator getInstanceOperator()
    {
        return instanceOperator;
    }

    public void setInstanceOperator(T8DataFilterCriterion.DataFilterOperator operator)
    {
        this.instanceOperator = operator;
    }

    public List<T8PropertyFilterCriterion> getPropertyCriteria()
    {
        return new ArrayList<>(propertyCriteria);
    }

    /**
     * Returns a list of only the property criterion objects that contain
     * active criteria (i.e. is not empty).
     * @return A list of only the property criterion objects that contain
     * active criteria (i.e. is not empty).
     */
    private List<T8PropertyFilterCriterion> getActivePropertyFilterCriteria()
    {
        List<T8PropertyFilterCriterion> criteria;

        criteria = new ArrayList<>();
        for (T8PropertyFilterCriterion propertyCriterion : propertyCriteria)
        {
            if (propertyCriterion.hasCriterion())
            {
                criteria.add(propertyCriterion);
            }
        }

        return criteria;
    }

    public List<T8HistoryFilterCriterion> getHistoryCriteria()
    {
        return new ArrayList<>(historyCriteria);
    }

    /**
     * Returns a list of only the history criterion objects that contain
     * active criteria i.e. are not empty.
     * @return A list of only the history criterion objects that contain
     * active criteria i.e. are not empty.
     */
    private List<T8HistoryFilterCriterion> getActiveHistoryFilterCriteria()
    {
        List<T8HistoryFilterCriterion> criteria;

        criteria = new ArrayList<>();
        for (T8HistoryFilterCriterion historyCriterion : historyCriteria)
        {
            if (historyCriterion.hasCriterion())
            {
                criteria.add(historyCriterion);
            }
        }

        return criteria;
    }

    public List<T8CodeFilterCriterion> getCodeCriteria()
    {
        return new ArrayList<>(codeCriteria);
    }

    public List<T8OrganizationFilterCriterion> getOrganizationCriteria()
    {
        return new ArrayList<>(organizationCriteria);
    }

    /**
     * Returns a list of only the organization criterion objects that contain
     * active criteria i.e. are not empty.
     * @return A list of only the organization criterion objects that contain
     * active criteria i.e. are not empty.
     */
    private List<T8OrganizationFilterCriterion> getActiveOrganizationFilterCriteria()
    {
        List<T8OrganizationFilterCriterion> criteria;

        criteria = new ArrayList<>();
        for (T8OrganizationFilterCriterion organizationCriterion : organizationCriteria)
        {
            if (organizationCriterion.hasCriterion())
            {
                criteria.add(organizationCriterion);
            }
        }

        return criteria;
    }

    /**
     * Returns a list of only the code criterion objects that contain
     * active criteria i.e. are not empty.
     * @return A list of only the code criterion objects that contain
     * active criteria i.e. are not empty.
     */
    private List<T8CodeFilterCriterion> getActiveCodeFilterCriteria()
    {
        List<T8CodeFilterCriterion> criteria;

        criteria = new ArrayList<>();
        for (T8CodeFilterCriterion codeCriterion : codeCriteria)
        {
            if (codeCriterion.hasCriterion())
            {
                criteria.add(codeCriterion);
            }
        }

        return criteria;
    }

    @Override
    public T8DrFilterCriteria copy()
    {
        T8DrFilterCriteria copiedClause;

        copiedClause = new T8DrFilterCriteria(drId);
        copiedClause.setDisplayName(displayName);
        copiedClause.setAllowPropertyAddition(allowPropertyAddition);
        copiedClause.setAllowHistoryAddition(allowHistoryAddition);
        copiedClause.setAllowCodeAddition(allowCodeAddition);
        copiedClause.setAllowRemoval(allowRemoval);
        copiedClause.setAllowInstanceSelection(allowInstanceSelection);
        copiedClause.setInstanceFilterValue(instanceFilterValue);
        copiedClause.setInstanceOperator(instanceOperator);
        for (T8PropertyFilterCriterion propertyCriterion : propertyCriteria)
        {
            copiedClause.addPropertyCriterion(propertyCriterion.copy());
        }

        for (T8HistoryFilterCriterion historyCriterion : historyCriteria)
        {
            copiedClause.addHistoryCriterion(historyCriterion.copy());
        }

        for (T8CodeFilterCriterion codeCriterion : codeCriteria)
        {
            copiedClause.addCodeCriterion(codeCriterion.copy());
        }

        for (T8OrganizationFilterCriterion organizationCriterion : organizationCriteria)
        {
            copiedClause.addOrganizationCriterion(organizationCriterion.copy());
        }

        return copiedClause;
    }

    @Override
    public boolean hasCriteria()
    {
        for (T8PropertyFilterCriterion propertyCriterion : propertyCriteria)
        {
            if (propertyCriterion.hasCriterion()) return true;
        }

        for (T8HistoryFilterCriterion historyCriterion : historyCriteria)
        {
            if (historyCriterion.hasCriterion()) return true;
        }

        for (T8CodeFilterCriterion codeCriterion : codeCriteria)
        {
            if (codeCriterion.hasCriterion()) return true;
        }

        for (T8OrganizationFilterCriterion organizationCriterion : organizationCriteria)
        {
            if (organizationCriterion.hasCriterion()) return true;
        }

        if(instanceFilterValue != null)
        {
            return true;
        }

        return false;
    }

    public void addPropertyCriterion(T8PropertyFilterCriterion criterion)
    {
        propertyCriteria.add(criterion);
    }

    public void addHistoryCriterion(T8HistoryFilterCriterion criterion)
    {
        historyCriteria.add(criterion);
    }

    public void addCodeCriterion(T8CodeFilterCriterion criterion)
    {
        codeCriteria.add(criterion);
    }

    public void addOrganizationCriterion(T8OrganizationFilterCriterion criterion)
    {
        organizationCriteria.add(criterion);
    }

    @Override
    public T8DataFilterCriterion getFilterCriterion(String identifier)
    {
        return null;
    }

    @Override
    public List<T8DataFilterCriterion> getFilterCriterionList()
    {
        return new ArrayList<>();
    }

    @Override
    public void setEntityIdentifier(String id)
    {
        this.entityId = id;
    }

    @Override
    public String getEntityIdentifier()
    {
        return entityId;
    }

    @Override
    public StringBuffer getWhereClause(T8DataTransaction tx, String sqlIdentifier)
    {
        T8DataEntityDefinition dataEntityDefinition;
        T8DataSourceDefinition dataSourceDefinition;
        List<T8PropertyFilterCriterion> activePropertyCriteria;
        List<T8HistoryFilterCriterion> activeHistoryCriteria;
        List<T8CodeFilterCriterion> activeCodeCriteria;
        List<T8OrganizationFilterCriterion> activeOrganizationCriteria;
        T8DataConnection dataConnection;
        T8DatabaseAdaptor adaptor;
        StringBuffer whereClause;
        boolean prefixAnd;

        // Get the database adaptor to use for this source.
        dataEntityDefinition = tx.getDataEntityDefinition(entityId);
        dataSourceDefinition = tx.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
        dataConnection = (tx.getDataConnection(((T8DataConnectionBasedSourceDefinition)dataSourceDefinition).getDataConnectionIdentifier()));
        adaptor = dataConnection.getDatabaseAdaptor();

        // Add all of the property criteria.
        whereClause = new StringBuffer();

        // Add the selected dr instance criteria.
        if (instanceFilterValue != null)
        {
            if (instanceFilterValue instanceof Collection)
            {
                List<String> valueColumns;
                List<Boolean> valueColumnGuidFlags;
                List<String> filterValues;

                filterValues = (List<String>)instanceFilterValue;

                // This is the first part of the filter.
                whereClause.append("SELECT ROOT_RECORD_ID FROM SEARCH_RECORD_INSTANCE I WHERE ");

                // Build up the value columns.
                if (T8IdentifierUtilities.isConceptId((String)filterValues.get(0)))
                {
                    valueColumns = Arrays.asList("I.DR_INSTANCE_ID");
                    valueColumnGuidFlags = Arrays.asList(true);
                }
                else
                {
                    valueColumns = Arrays.asList("I.DR_INSTANCE_TERM", "I.DR_INSTANCE_CODE");
                    valueColumnGuidFlags = Arrays.asList(false, false);
                }

                for (int valueColumnIndex = 0; valueColumnIndex < valueColumns.size(); valueColumnIndex++)
                {
                    String valueColumn;

                    valueColumn = valueColumns.get(valueColumnIndex);

                    if(valueColumnIndex == 0) whereClause.append("(");
                    else whereClause.append(" OR ");

                    for (int valueIndex = 0; valueIndex < filterValues.size(); valueIndex++)
                    {
                        boolean guidValue;
                        String value;

                        value = filterValues.get(valueIndex);
                        guidValue = valueColumnGuidFlags.get(valueColumnIndex);

                        if (valueIndex == 0) whereClause.append((valueColumn + " "));
                        else if (instanceOperator == T8DataFilterCriterion.DataFilterOperator.IN || instanceOperator == T8DataFilterCriterion.DataFilterOperator.NOT_IN)
                        {
                            // If the operator is a IN or NOT_IN don't append the string.
                        }
                        else
                        {
                            whereClause.append(" OR ");
                            whereClause.append((valueColumn + " "));
                        }

                        // Append the filter value based on the specified operator.
                        switch (instanceOperator)
                        {
                            case EQUAL:
                            {
                                String parameterString;

                                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                                whereClause.append(value == null ? " IS NULL" : (" = " + parameterString));
                                break;
                            }
                            case NOT_EQUAL:
                            {
                                String parameterString;

                                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                                whereClause.append(value == null ? " IS NOT NULL" : (" <> " + parameterString));
                                break;
                            }
                            case GREATER_THAN:
                            {
                                whereClause.append(" > ?");
                                break;
                            }
                            case GREATER_THAN_OR_EQUAL:
                            {
                                whereClause.append(" >= ?");
                                break;
                            }
                            case LESS_THAN:
                            {
                                whereClause.append(" < ?");
                                break;
                            }
                            case LESS_THAN_OR_EQUAL:
                            {
                                whereClause.append(" <= ?");
                                break;
                            }
                            case LIKE:
                            {
                                whereClause.append(" LIKE ? ESCAPE '\\'");
                                break;
                            }
                            case STARTS_WITH:
                            {
                                whereClause.append(" LIKE ? ESCAPE '\\'");
                                break;
                            }
                            case ENDS_WITH:
                            {
                                whereClause.append(" LIKE ? ESCAPE '\\'");
                                break;
                            }
                            case NOT_LIKE:
                            {
                                whereClause.append(" NOT LIKE ?");
                                break;
                            }
                            case IN:
                            case NOT_IN:
                            {
                                String parameterString;

                                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";

                                if (valueIndex == 0)
                                {
                                    whereClause.append((instanceOperator == T8DataFilterCriterion.DataFilterOperator.IN) ? " IN (" : " NOT IN (");
                                    whereClause.append(parameterString);
                                }
                                else
                                {
                                   whereClause.append(",");
                                   whereClause.append(parameterString);
                                }

                                if (valueIndex == (filterValues.size() - 1)) whereClause.append(")");

                                break;
                            }
                        }
                    }

                    // Close the where clause when the last value is reached.
                    if (valueColumnIndex == (valueColumns.size() - 1))
                    {
                        whereClause.append(")");
                    }
                }
            }
        }

        // Add all the property criteria.
        activePropertyCriteria = getActivePropertyFilterCriteria();
        for (int propertyIndex = 0; propertyIndex < activePropertyCriteria.size(); propertyIndex++)
        {
            T8PropertyFilterCriterion propertyCriterion;
            String propertyId;
            String fieldId;
            String cteName;

            // Get the property filter criteria.
            propertyCriterion = activePropertyCriteria.get(propertyIndex);
            propertyId = propertyCriterion.getPropertyId();
            fieldId = propertyCriterion.getFieldId();
            cteName = propertyCriterion.getRequiredCteId();
            prefixAnd = false;

            // Add the selection of the sub-query to the where clause.
            if (propertyIndex == 0)
            {
                whereClause.append("SELECT ROOT_RECORD_ID FROM " + cteName + " V WHERE ");
            }
            else
            {
                whereClause.append("AND V.RECORD_ID IN (SELECT V.RECORD_ID FROM " + cteName + " V WHERE ");
            }

            if (!Strings.isNullOrEmpty(drId))
            {
                whereClause.append("V.DR_ID = ");
                whereClause.append(adaptor.getSQLParameterizedHexToGUID());
                prefixAnd = true;
            }

            if (!Strings.isNullOrEmpty(propertyId))
            {
                if (prefixAnd) whereClause.append(" AND ");
                whereClause.append("V.PROPERTY_ID = ");
                whereClause.append(adaptor.getSQLParameterizedHexToGUID());
                prefixAnd = true;
            }

            if (!Strings.isNullOrEmpty(fieldId))
            {
                if (prefixAnd) whereClause.append(" AND ");
                whereClause.append("V.FIELD_ID = ");
                whereClause.append(adaptor.getSQLParameterizedHexToGUID());
                prefixAnd = true;
            }

            // Append the property where clause.
            if (prefixAnd) whereClause.append(" AND ");
            whereClause.append(propertyCriterion.getWhereClause(tx, adaptor));

            // If this is any property criterion other that the first one, close the IN clause.
            if (propertyIndex > 0)
            {
                whereClause.append(")");
            }
        }

        // Add all of the history criteria.
        activeHistoryCriteria = getActiveHistoryFilterCriteria();
        for (int historyIndex = 0; historyIndex < activeHistoryCriteria.size(); historyIndex++)
        {
            T8HistoryFilterCriterion historyCriterion;
            String agentID;
            String agentIID;
            T8HistoryFilterCriterion.HistoryFilterOperator operator;

            // Get the history filter criteria.
            historyCriterion = activeHistoryCriteria.get(historyIndex);
            agentID = historyCriterion.getAgentId();
            agentIID = historyCriterion.getAgentIid();
            operator = historyCriterion.getOperator();

            prefixAnd = false;
            if (activePropertyCriteria.isEmpty())
            {
                whereClause.append("SELECT ROOT_RECORD_ID FROM HISTORY_DATA_RECORD_DOC H WHERE ");
            }
            else
            {
                whereClause.append("AND V.RECORD_ID IN (SELECT H.RECORD_ID FROM HISTORY_DATA_RECORD_DOC H WHERE ");
            }

            if (!Strings.isNullOrEmpty(drId))
            {
                whereClause.append("H.DR_ID = ");
                whereClause.append(adaptor.getSQLParameterizedHexToGUID());
                prefixAnd = true;
            }

            if (!Strings.isNullOrEmpty(agentID))
            {
                if (prefixAnd) whereClause.append(" AND ");
                whereClause.append("H.AGENT_ID = ?");
                prefixAnd = true;
            }

            if (!Strings.isNullOrEmpty(agentIID))
            {
                if (prefixAnd) whereClause.append(" AND ");
                whereClause.append("H.AGENT_IID = ?");
                prefixAnd = true;
            }

            // Append the filter value based on the specified operator.
            switch (operator)
            {
                case ADDED:
                case ADDED_BEFORE:
                {
                    if (prefixAnd) whereClause.append(" AND ");
                    whereClause.append("H.EVENT = 'INSERT' AND H.TIME < ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    break;
                }
                case ADDED_AFTER:
                {
                    if (prefixAnd) whereClause.append(" AND ");
                    whereClause.append("H.EVENT = 'INSERT' AND H.TIME > ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    break;
                }
                case ADDED_ON:
                {
                    if (prefixAnd) whereClause.append(" AND ");
                    whereClause.append("H.EVENT = 'INSERT' AND H.TIME > ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    whereClause.append(" AND H.TIME < ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    break;
                }
                case UPDATED_BEFORE:
                {
                    if (prefixAnd) whereClause.append(" AND ");
                    whereClause.append("H.EVENT = 'UPDATE' AND H.TIME < ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    break;
                }
                case UPDATED_AFTER:
                {
                    if (prefixAnd) whereClause.append(" AND ");
                    whereClause.append("H.EVENT = 'UPDATE' AND H.TIME > ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    break;
                }
                case UPDATED_ON:
                {
                    if (prefixAnd) whereClause.append(" AND ");
                    whereClause.append("H.EVENT = 'INSERT' AND H.TIME > ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    whereClause.append(" AND H.TIME < ");
                    whereClause.append(adaptor.getSQLParameterizedMillisecondsToTimestamp());
                    break;
                }
            }

            // If this is any history criterion other that the first one, close the IN clause.
            if (instanceFilterValue != null || (!activePropertyCriteria.isEmpty()) || (historyIndex > 0))
            {
                whereClause.append(")");
            }
        }

        // Add all of the code criteria.
        activeCodeCriteria = getActiveCodeFilterCriteria();
        for (int codeIndex = 0; codeIndex < activeCodeCriteria.size(); codeIndex++)
        {
            T8CodeFilterCriterion codeCriterion;
            T8CodeFilterCriterion.CodeCriterionTarget target;
            T8DataFilterCriterion.DataFilterOperator operator;
            String codeTypeId;
            Object filterValue;
            boolean caseInsensitive;

            // Get the code filter criterion.
            codeCriterion = activeCodeCriteria.get(codeIndex);
            target = codeCriterion.getTarget();
            codeTypeId = codeCriterion.getCodeTypeId();
            operator = codeCriterion.getOperator();
            filterValue = codeCriterion.getFilterValue();
            caseInsensitive = codeCriterion.isCaseInsensitive();

            // Select from the CTE that contains record codes.
            if ((activePropertyCriteria.isEmpty()) && (activeHistoryCriteria.isEmpty()))
            {
                // This is the first part of the filter.
                whereClause.append("SELECT ROOT_RECORD_ID FROM SEARCH_RECORD_CODES C WHERE ");
            }
            else if (!activePropertyCriteria.isEmpty())
            {
                // Property filter already added.
                whereClause.append("AND V.RECORD_ID IN (SELECT C.RECORD_ID FROM SEARCH_RECORD_CODES C WHERE ");
            }
            else
            {
                // History filter already added.
                whereClause.append("AND H.RECORD_ID IN (SELECT C.RECORD_ID FROM SEARCH_RECORD_CODES C WHERE ");
            }

            prefixAnd = false;
            if (!Strings.isNullOrEmpty(drId))
            {
                whereClause.append("C.DR_ID = ");
                whereClause.append(adaptor.getSQLParameterizedHexToGUID());
                prefixAnd = true;
            }

            if (!Strings.isNullOrEmpty(codeTypeId))
            {
                if (prefixAnd) whereClause.append(" AND ");
                whereClause.append("C.CODE_TYPE_ID = ");
                whereClause.append(adaptor.getSQLParameterizedHexToGUID());
                prefixAnd = true;

            }

            // Prefix an ' AND ' if required.
            if (prefixAnd) whereClause.append(" AND ");

            // Append the filter value based on the specified operator.
            switch (operator)
            {
                case EQUAL:
                {
                    whereClause.append(filterValue == null ? "C.CODE IS NULL" : ("C.CODE = ?"));
                    break;
                }
                case NOT_EQUAL:
                {
                    whereClause.append(filterValue == null ? "C.CODE IS NOT NULL" : ("C.CODE <> ?"));
                    break;
                }
                case GREATER_THAN:
                {
                    whereClause.append("C.CODE > ?");
                    break;
                }
                case GREATER_THAN_OR_EQUAL:
                {
                    whereClause.append("C.CODE >= ?");
                    break;
                }
                case LESS_THAN:
                {
                    whereClause.append("C.CODE < ?");
                    break;
                }
                case LESS_THAN_OR_EQUAL:
                {
                    whereClause.append("C.CODE <= ?");
                    break;
                }
                case LIKE:
                {
                    whereClause.append("C.CODE LIKE ? ESCAPE '\\'");
                    break;
                }
                case STARTS_WITH:
                {
                    whereClause.append("C.CODE LIKE ? ESCAPE '\\'");
                    break;
                }
                case ENDS_WITH:
                {
                    whereClause.append("C.CODE LIKE ? ESCAPE '\\'");
                    break;
                }
                case NOT_LIKE:
                {
                    whereClause.append("C.CODE NOT LIKE ?");
                    break;
                }
                case IS_NOT_NULL:
                {
                    whereClause.append("C.CODE IS NOT NULL");
                    break;
                }
                case IN:
                case NOT_IN:
                {
                    if (filterValue instanceof Collection)
                    {
                        Iterator<?> valueIterator;
                        Collection<?> valueCollection;

                        valueCollection = (Collection)filterValue;
                        valueIterator = valueCollection.iterator();

                        whereClause.append((operator == T8DataFilterCriterion.DataFilterOperator.IN) ? "C.CODE IN (" : "C.CODE NOT IN (");
                        while (valueIterator.hasNext())
                        {
                            valueIterator.next();
                            whereClause.append("?");
                            if (valueIterator.hasNext()) whereClause.append(", ");
                        }
                        whereClause.append(")");
                        break;
                    }
                    else throw new RuntimeException("Invalid parameter type used with IN clause: " + filterValue);
                }
            }

            // If this is any code criterion other that the first one, close the IN clause.
            if (instanceFilterValue != null || (!activePropertyCriteria.isEmpty()) || (!activeHistoryCriteria.isEmpty()) || (codeIndex > 0))
            {
                whereClause.append(")");
            }
        }

        // Add all of the organization criteria.
        activeOrganizationCriteria = getActiveOrganizationFilterCriteria();
        for (int orgIndex = 0; orgIndex < activeOrganizationCriteria.size(); orgIndex++)
        {
            T8OrganizationFilterCriterion orgCriterion;
            T8DataFilterCriterion.DataFilterOperator operator;
            List<String> valueColumns;
            List<Boolean> valueColumnGuidFlags;
            Object filterValue;
            List<String> filterValues;

            // Get the organization filter criterion.
            orgCriterion = activeOrganizationCriteria.get(orgIndex);
            operator = orgCriterion.getOperator();
            filterValue = orgCriterion.getFilterValue();

            if (filterValue instanceof Collection)
            {
                filterValues = (List<String>)filterValue;

                // Select from the CTE that contains record organizations.
                if ((activePropertyCriteria.isEmpty()) && (activeHistoryCriteria.isEmpty()) && (activeCodeCriteria.isEmpty()))
                {
                    // This is the first part of the filter.
                    whereClause.append("SELECT ROOT_RECORD_ID FROM SEARCH_RECORD_ORGANIZATIONS O WHERE ");
                }
                else if (!activePropertyCriteria.isEmpty())
                {
                    // Property filter already added.
                    whereClause.append("AND V.RECORD_ID IN (SELECT O.RECORD_ID FROM SEARCH_RECORD_ORGANIZATIONS O WHERE ");
                }
                else if (!activeHistoryCriteria.isEmpty())
                {
                    // History filter already added.
                    whereClause.append("AND H.RECORD_ID IN (SELECT O.RECORD_ID FROM SEARCH_RECORD_ORGANIZATIONS O WHERE ");
                }
                else
                {
                    // Code filter already added.
                    whereClause.append("AND C.RECORD_ID IN (SELECT O.RECORD_ID FROM SEARCH_RECORD_ORGANIZATIONS O WHERE ");
                }

                // Build up the value columns.
                if (operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL || operator == T8DataFilterCriterion.DataFilterOperator.IS_NOT_NULL)
                {
                    valueColumns = new ArrayList<>();
                    valueColumnGuidFlags = new ArrayList<>();
                    whereClause.append((operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL) ? "O.SEARCH_ORG_ID IS NULL" : "O.SEARCH_ORG_ID IS NOT NULL");
                }
                else if (T8IdentifierUtilities.isConceptID((String)filterValues.get(0)))
                {
                    valueColumns = Arrays.asList("O.SEARCH_ORG_ID");
                    valueColumnGuidFlags = Arrays.asList(true);
                }
                else
                {
                    valueColumns = Arrays.asList("O.SEARCH_TERM", "O.SEARCH_CODE");
                    valueColumnGuidFlags = Arrays.asList(false, false);
                }

                for (int valueColumnIndex = 0; valueColumnIndex < valueColumns.size(); valueColumnIndex++)
                {
                    String valueColumn;

                    valueColumn = valueColumns.get(valueColumnIndex);

                    if(valueColumnIndex == 0) whereClause.append("(");
                    else whereClause.append(" OR ");

                    for (int valueIndex = 0; valueIndex < filterValues.size(); valueIndex++)
                    {
                        boolean guidValue;
                        String value;

                        value = filterValues.get(valueIndex);
                        guidValue = valueColumnGuidFlags.get(valueColumnIndex);

                        if (valueIndex == 0) whereClause.append((valueColumn + " "));
                        else if (operator == T8DataFilterCriterion.DataFilterOperator.IN || operator == T8DataFilterCriterion.DataFilterOperator.NOT_IN)
                        {
                            // If the operator is a IN or NOT_IN don't append the string.
                        }
                        else
                        {
                            whereClause.append(" OR ");
                            whereClause.append((valueColumn + " "));
                        }

                        // Append the filter value based on the specified operator.
                        switch (operator)
                        {
                            case EQUAL:
                            {
                                String parameterString;

                                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                                whereClause.append(value == null ? " IS NULL" : (" = " + parameterString));
                                break;
                            }
                            case NOT_EQUAL:
                            {
                                String parameterString;

                                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";
                                whereClause.append(value == null ? " IS NOT NULL" : (" <> " + parameterString));
                                break;
                            }
                            case GREATER_THAN:
                            {
                                whereClause.append(" > ?");
                                break;
                            }
                            case GREATER_THAN_OR_EQUAL:
                            {
                                whereClause.append(" >= ?");
                                break;
                            }
                            case LESS_THAN:
                            {
                                whereClause.append(" < ?");
                                break;
                            }
                            case LESS_THAN_OR_EQUAL:
                            {
                                whereClause.append(" <= ?");
                                break;
                            }
                            case LIKE:
                            {
                                whereClause.append(" LIKE ? ESCAPE '\\'");
                                break;
                            }
                            case STARTS_WITH:
                            {
                                whereClause.append(" LIKE ? ESCAPE '\\'");
                                break;
                            }
                            case ENDS_WITH:
                            {
                                whereClause.append(" LIKE ? ESCAPE '\\'");
                                break;
                            }
                            case NOT_LIKE:
                            {
                                whereClause.append(" NOT LIKE ?");
                                break;
                            }
                            case IN:
                            case NOT_IN:
                            {
                                String parameterString;

                                parameterString = guidValue ? adaptor.getSQLParameterizedHexToGUID() : "?";

                                if (valueIndex == 0)
                                {
                                    whereClause.append((operator == T8DataFilterCriterion.DataFilterOperator.IN) ? " IN (" : " NOT IN (");
                                    whereClause.append(parameterString);
                                }
                                else
                                {
                                   whereClause.append(",");
                                   whereClause.append(parameterString);
                                }

                                if (valueIndex == (filterValues.size() - 1)) whereClause.append(")");

                                break;
                            }
                        }
                    }

                    // Close the where clause when the last value is reached.
                    if (valueColumnIndex == (valueColumns.size() - 1))
                    {
                        whereClause.append(")");
                    }
                }
                // If this is any organization criterion other than the first one, close the IN clause.
                if (instanceFilterValue != null || (!activePropertyCriteria.isEmpty()) || (!activeHistoryCriteria.isEmpty()) || (!activeCodeCriteria.isEmpty()) || (orgIndex > 0))
                {
                    whereClause.append(")");
                }
            }
        }

        // Return the completed where clause.
        return whereClause;
    }

    @Override
    public List<Object> getWhereClauseParameters(T8DataTransaction tx)
    {
        List<T8OrganizationFilterCriterion> activeOrganizationCriteria;
        List<T8PropertyFilterCriterion> activePropertyCriteria;
        List<T8HistoryFilterCriterion> activeHistoryCriteria;
        List<T8CodeFilterCriterion> activeCodeCriteria;
        T8DataEntityDefinition dataEntityDefinition;
        T8DataSourceDefinition dataSourceDefinition;
        T8DataConnection dataConnection;
        ArrayList<Object> parameterList;
        T8DatabaseAdaptor adaptor;

        // Get the database adaptor to use for this source.
        dataEntityDefinition = tx.getDataEntityDefinition(entityId);
        dataSourceDefinition = tx.getDataSourceDefinition(dataEntityDefinition.getDataSourceIdentifier());
        dataConnection = (tx.getDataConnection(((T8DataConnectionBasedSourceDefinition)dataSourceDefinition).getDataConnectionIdentifier()));
        adaptor = dataConnection.getDatabaseAdaptor();

        // Add all organization criteria paramaters.
        parameterList = new ArrayList<>();
        if (instanceFilterValue != null)
        {
            if (instanceFilterValue instanceof Collection)
            {
                List<String> filterValues;
                int valueColumnCount;
                List<Boolean> valueColumnGuidFlags;

                filterValues = (List<String>)instanceFilterValue;

                // Determine the number of value columns to be used.
                if (T8IdentifierUtilities.isConceptId((String)filterValues.get(0)))
                {
                    valueColumnCount = 1;
                    valueColumnGuidFlags = Arrays.asList(true);
                }
                else
                {
                    valueColumnCount = 2;
                    valueColumnGuidFlags = Arrays.asList(false, false);
                }

                 // This value must correspond to the size of the value columns list in method {@code getWhereClause}.
                for (int valueColumnIndex = 0; valueColumnIndex < valueColumnCount; valueColumnIndex++)
                {
                    for (int valueIndex = 0; valueIndex < filterValues.size(); valueIndex++)
                    {
                        boolean guidValue;
                        String value;

                        value = filterValues.get(valueIndex);
                        guidValue = valueColumnGuidFlags.get(valueColumnIndex);

                        switch (instanceOperator)
                        {
                            case LIKE:
                            case NOT_LIKE:
                            {
                                String parameterString;

                                parameterString = adaptor.getSQLEscapedLikeOperand(value, '\\');
                                if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;
                                if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                                parameterList.add(parameterString);
                                break;
                            }
                            case STARTS_WITH:
                            {
                                String parameterString;

                                parameterString = adaptor.getSQLEscapedLikeOperand(value, '\\');
                                if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                                parameterList.add(parameterString);
                                break;
                            }
                            case ENDS_WITH:
                            {
                                String parameterString;

                                parameterString = adaptor.getSQLEscapedLikeOperand(value, '\\');
                                if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;

                                parameterList.add(parameterString);
                                break;
                            }
                            case IN:
                            case NOT_IN:
                            {
                                // If it's a guid field, we have to convert the filter value because of database vendor dependent implementations.
                                if (guidValue)
                                {
                                    // Iterate over all values in the list and if a value is not null, convert it.
                                    parameterList.add(adaptor.convertHexStringToSQLGUIDString(value));
                                }
                                else
                                {
                                    parameterList.add(value);
                                }
                                break;
                            }
                            case IS_NULL:
                            case IS_NOT_NULL:
                            {
                                // Do nothing, since no filter value is required for this operator type.
                                break;
                            }
                            case MATCHES:
                            {
                                // Do nothing, since no filter value is required for this operator type.
                                break;
                            }
                            case EQUAL:
                            {
                                // Do nothing, since no filter value is required for this operator type.
                                if (value == null) break;
                            }
                            default:
                            {
                                // If it's a guid value, we have to convert the filter value because of database vendor dependent implementations.
                                if (guidValue)
                                {
                                    parameterList.add(adaptor.convertHexStringToSQLGUIDString(value));
                                    break;
                                }
                                else
                                {
                                    parameterList.add(value);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Add property criteria parameters.
        activePropertyCriteria = getActivePropertyFilterCriteria();
        for (int propertyIndex = 0; propertyIndex < activePropertyCriteria.size(); propertyIndex++)
        {
            T8PropertyFilterCriterion propertyCriterion;
            String propertyId;
            String fieldId;

            // Get the property filter criteria.
            propertyCriterion = activePropertyCriteria.get(propertyIndex);
            propertyId = propertyCriterion.getPropertyId();
            fieldId = propertyCriterion.getFieldId();

            if (!Strings.isNullOrEmpty(drId))
            {
                parameterList.add(adaptor.convertHexStringToSQLGUIDString(drId));
            }

            if (!Strings.isNullOrEmpty(propertyId))
            {
                parameterList.add(adaptor.convertHexStringToSQLGUIDString(propertyId));
            }

            if (!Strings.isNullOrEmpty(fieldId))
            {
                parameterList.add(adaptor.convertHexStringToSQLGUIDString(fieldId));
            }

            // Add the property criterion parameters.
            parameterList.addAll(propertyCriterion.getWhereClauseParameters(tx, adaptor));
        }

        // Add history criteria parameters.
        activeHistoryCriteria = getActiveHistoryFilterCriteria();
        for (int historyIndex = 0; historyIndex < activeHistoryCriteria.size(); historyIndex++)
        {
            T8HistoryFilterCriterion historyCriterion;
            String agentId;
            String agentIid;
            T8HistoryFilterCriterion.HistoryFilterOperator operator;
            Long filterTime;

            // Get the history filter criteria.
            historyCriterion = activeHistoryCriteria.get(historyIndex);
            agentId = historyCriterion.getAgentId();
            agentIid = historyCriterion.getAgentIid();
            operator = historyCriterion.getOperator();
            filterTime = historyCriterion.getFilterTime();

            if (!Strings.isNullOrEmpty(drId))
            {
                parameterList.add(adaptor.convertHexStringToSQLGUIDString(drId));
            }

            if (!Strings.isNullOrEmpty(agentId))
            {
                parameterList.add(agentId);
            }

            if (!Strings.isNullOrEmpty(agentIid))
            {
                parameterList.add(agentIid);
            }

            // Append the filter parameters based on the specified operator.
            switch (operator)
            {
                case ADDED:
                {
                    parameterList.add(filterTime);
                    break;
                }
                case ADDED_BEFORE:
                {
                    parameterList.add(filterTime);
                    break;
                }
                case ADDED_AFTER:
                {
                    parameterList.add(filterTime);
                    break;
                }
                case ADDED_ON:
                {
                    Long dayStartTime;
                    Long dayEndTime;

                    dayStartTime = filterTime; // TODO: Calculate the correct start and end time based on timezone.
                    dayEndTime = filterTime;
                    parameterList.add(dayStartTime);
                    parameterList.add(dayEndTime);
                    break;
                }
                case UPDATED_BEFORE:
                {
                    parameterList.add(filterTime);
                    break;
                }
                case UPDATED_AFTER:
                {
                    parameterList.add(filterTime);
                    break;
                }
                case UPDATED_ON:
                {
                    Long dayStartTime;
                    Long dayEndTime;

                    dayStartTime = filterTime; // TODO: Calculate the correct start and end time based on timezone.
                    dayEndTime = filterTime;
                    parameterList.add(dayStartTime);
                    parameterList.add(dayEndTime);
                    break;
                }
            }
        }

        // Add all of the code criteria.
        activeCodeCriteria = getActiveCodeFilterCriteria();
        for (int codeIndex = 0; codeIndex < activeCodeCriteria.size(); codeIndex++)
        {
            T8CodeFilterCriterion codeCriterion;
            T8DataFilterCriterion.DataFilterOperator operator;
            String codeTypeId;
            Object filterValue;
            boolean caseInsensitive;

            // Get the code filter criterion.
            codeCriterion = activeCodeCriteria.get(codeIndex);
            codeTypeId = codeCriterion.getCodeTypeId();
            operator = codeCriterion.getOperator();
            filterValue = codeCriterion.getFilterValue();
            caseInsensitive = codeCriterion.isCaseInsensitive();

            if (!Strings.isNullOrEmpty(drId))
            {
                parameterList.add(adaptor.convertHexStringToSQLGUIDString(drId));
            }

            if (!Strings.isNullOrEmpty(codeTypeId))
            {
                parameterList.add(adaptor.convertHexStringToSQLGUIDString(codeTypeId));
            }

            // Append the filter value based on the specified operator.
            switch (operator)
            {
                case LIKE:
                case NOT_LIKE:
                {
                    String parameterString;

                    parameterString = caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue.toString();
                    parameterString = adaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                    if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;
                    if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                    parameterList.add(parameterString);
                    break;
                }
                case STARTS_WITH:
                {
                    String parameterString;

                    parameterString = caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue.toString();
                    parameterString = adaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                    if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                    parameterList.add(parameterString);
                    break;
                }
                case ENDS_WITH:
                {
                    String parameterString;

                    parameterString = caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue.toString();
                    parameterString = adaptor.getSQLEscapedLikeOperand(parameterString, '\\');
                    if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;

                    parameterList.add(parameterString);
                    break;
                }
                case IN:
                case NOT_IN:
                {
                    if (filterValue != null)
                    {
                        if (filterValue instanceof Collection)
                        {
                            Collection valueList;

                            // Get the value list.
                            valueList = (Collection)filterValue;
                            parameterList.addAll(valueList);
                        }
                        else if (filterValue instanceof T8DataFilter) // A sub-query is used.
                        {
                            T8DataFilter subDataFilter;

                            subDataFilter = (T8DataFilter)filterValue;
                            parameterList.addAll(subDataFilter.getWhereClauseParameters(tx));
                        }
                        else if (filterValue instanceof T8SubDataFilter) // A sub-query is used.
                        {
                            T8SubDataFilter subDataFilter;

                            subDataFilter = (T8SubDataFilter)filterValue;
                            parameterList.addAll(subDataFilter.getDataFilter().getWhereClauseParameters(tx));
                        }
                    }

                    break;
                }
                case IS_NULL:
                case IS_NOT_NULL:
                {
                    // Do nothing, since no filter value is required for this operator type.
                    break;
                }
                case MATCHES:
                {
                    // Do nothing, since no filter value is required for this operator type.
                    break;
                }
                case EQUAL:
                {
                    // Do nothing, since no filter value is required for this operator type.
                    if (filterValue == null) break;
                }
                default:
                {
                    if (filterValue instanceof String)
                    {
                        parameterList.add(caseInsensitive ? ((String)filterValue).toUpperCase() : filterValue);
                        break;
                    }
                    else
                    {
                        parameterList.add(filterValue);
                        break;
                    }
                }
            }
        }

        // Add all organization criteria paramaters.
        activeOrganizationCriteria = getActiveOrganizationFilterCriteria();
        for (int orgIndex = 0; orgIndex < activeOrganizationCriteria.size(); orgIndex++)
        {
            T8OrganizationFilterCriterion organizationCriterion;
            T8DataFilterCriterion.DataFilterOperator operator;
            Object filterValue;
            List<String> filterValues;
            int valueColumnCount;
            List<Boolean> valueColumnGuidFlags;

            // Get the organization filter criteria.
            organizationCriterion = activeOrganizationCriteria.get(orgIndex);
            operator = organizationCriterion.getOperator();
            filterValue = organizationCriterion.getFilterValue();

            if (filterValue instanceof Collection)
            {
                filterValues = (List<String>)filterValue;

                // Determine the number of value columns to be used.
                if (operator == T8DataFilterCriterion.DataFilterOperator.IS_NULL || operator == T8DataFilterCriterion.DataFilterOperator.IS_NOT_NULL)
                {
                    valueColumnCount = 0;
                    valueColumnGuidFlags = new ArrayList<>();
                }
                else if (T8IdentifierUtilities.isConceptID((String)filterValues.get(0)))
                {
                    valueColumnCount = 1;
                    valueColumnGuidFlags = Arrays.asList(true);
                }
                else
                {
                    valueColumnCount = 2;
                    valueColumnGuidFlags = Arrays.asList(false, false);
                }

                 // This value must correspond to the size of the value columns list in method {@code getWhereClause}.
                for (int valueColumnIndex = 0; valueColumnIndex < valueColumnCount; valueColumnIndex++)
                {
                    for (int valueIndex = 0; valueIndex < filterValues.size(); valueIndex++)
                    {
                        boolean guidValue;
                        String value;

                        value = filterValues.get(valueIndex);
                        guidValue = valueColumnGuidFlags.get(valueColumnIndex);

                        switch (operator)
                        {
                            case LIKE:
                            case NOT_LIKE:
                            {
                                String parameterString;

                                parameterString = adaptor.getSQLEscapedLikeOperand(value, '\\');
                                if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;
                                if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                                parameterList.add(parameterString);
                                break;
                            }
                            case STARTS_WITH:
                            {
                                String parameterString;

                                parameterString = adaptor.getSQLEscapedLikeOperand(value, '\\');
                                if (!parameterString.endsWith("%")) parameterString = parameterString + "%";

                                parameterList.add(parameterString);
                                break;
                            }
                            case ENDS_WITH:
                            {
                                String parameterString;

                                parameterString = adaptor.getSQLEscapedLikeOperand(value, '\\');
                                if (!parameterString.startsWith("%")) parameterString = "%" + parameterString;

                                parameterList.add(parameterString);
                                break;
                            }
                            case IN:
                            case NOT_IN:
                            {
                                // If it's a guid field, we have to convert the filter value because of database vendor dependent implementations.
                                if (guidValue)
                                {
                                    // Iterate over all values in the list and if a value is not null, convert it.
                                    parameterList.add(adaptor.convertHexStringToSQLGUIDString(value));
                                }
                                else
                                {
                                    parameterList.add(value);
                                }
                                break;
                            }
                            case IS_NULL:
                            case IS_NOT_NULL:
                            {
                                // Do nothing, since no filter value is required for this operator type.
                                break;
                            }
                            case MATCHES:
                            {
                                // Do nothing, since no filter value is required for this operator type.
                                break;
                            }
                            case EQUAL:
                            {
                                // Do nothing, since no filter value is required for this operator type.
                                if (value == null) break;
                            }
                            default:
                            {
                                // If it's a guid value, we have to convert the filter value because of database vendor dependent implementations.
                                if (guidValue)
                                {
                                    parameterList.add(adaptor.convertHexStringToSQLGUIDString(value));
                                    break;
                                }
                                else
                                {
                                    parameterList.add(value);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Return the complete parameter list.
        return parameterList;
    }

    @Override
    public boolean includesEntity(T8DataEntity entity)
    {
        throw new RuntimeException("Record filter cannot be applied to entity.");
    }

    @Override
    public Set<String> getFilterFieldIdentifiers()
    {
        return new HashSet<>();
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public boolean isAllowPropertyAddition()
    {
        return allowPropertyAddition;
    }

    public void setAllowPropertyAddition(boolean allowPropertyAddition)
    {
        this.allowPropertyAddition = allowPropertyAddition;
    }

    public boolean isAllowHistoryAddition()
    {
        return allowHistoryAddition;
    }

    public void setAllowHistoryAddition(boolean allowHistoryAddition)
    {
        this.allowHistoryAddition = allowHistoryAddition;
    }

    public boolean isAllowCodeAddition()
    {
        return allowCodeAddition;
    }

    public void setAllowCodeAddition(boolean allowCodeAddition)
    {
        this.allowCodeAddition = allowCodeAddition;
    }

    public boolean isAllowInstanceSelection()
    {
        return allowInstanceSelection;
    }

    public void setAllowInstanceSelection(boolean allowInstanceSelection)
    {
        this.allowInstanceSelection = allowInstanceSelection;
    }

    public boolean isAllowRemoval()
    {
        return allowRemoval;
    }

    public void setAllowRemoval(boolean allowRemoval)
    {
        this.allowRemoval = allowRemoval;
    }

    public String getInstanceLabel()
    {
        return instanceLabel;
    }

    public void setInstanceLabel(String label)
    {
        this.instanceLabel = label;
    }
}
