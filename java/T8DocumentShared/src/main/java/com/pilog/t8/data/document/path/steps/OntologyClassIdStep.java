package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class OntologyClassIdStep extends DefaultPathStep implements PathStep
{
    public OntologyClassIdStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, ontologyProvider, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            List<T8OntologyLink> links;
            T8OntologyConcept concept;
            DataRecord dataRecord;

            dataRecord = (DataRecord)input;
            concept = dataRecord.getOntology();
            if (concept != null)
            {
                links = concept.getOntologyLinks();
                return evaluateStep(links);
            }
            else
            {
                links = ontologyProvider.getOntologyLinks(null, dataRecord.getID());
                return evaluateStep(links);
            }
        }
        else if (input instanceof DataRequirementInstance)
        {
            List<T8OntologyLink> links;
            T8OntologyConcept concept;
            DataRequirementInstance drInstance;

            drInstance = (DataRequirementInstance)input;
            concept = drInstance.getOntology();
            if (concept != null)
            {
                links = concept.getOntologyLinks();
                return evaluateStep(links);
            }
            else
            {
                links = ontologyProvider.getOntologyLinks(null, drInstance.getConceptID());
                return evaluateStep(links);
            }
        }
        else if (input instanceof String)
        {
            String conceptId;

            conceptId = (String)input;
            return evaluateStep(ontologyProvider.getOntologyLinks(null, conceptId));
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(List<T8OntologyLink> links)
    {
        List<String> ocIDList;

        // Create a list to hold the organization ID's.
        ocIDList = new ArrayList<String>();

        // If we have onstology structure id's to use for narrowing of the list do it now.
        if (idList != null)
        {
            Iterator<T8OntologyLink> linkIterator;

            linkIterator = links.iterator();
            while (linkIterator.hasNext())
            {
                T8OntologyLink nextLink;

                nextLink = linkIterator.next();
                if (!idList.contains(nextLink.getOntologyStructureID()))
                {
                    linkIterator.remove();
                }
            }
        }

        // Add all organization ID's from the links to the result list.
        for (T8OntologyLink link : links)
        {
            ocIDList.add(link.getOntologyClassID());
        }

        // Return the result list.
        return ocIDList;
    }
}
