package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class DocumentReference implements Serializable
{
    private DocumentReferenceType referenceType;
    private String conceptId;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;

    public enum DocumentReferenceType {RECORD, DR_INSTANCE}

    public DocumentReference(DocumentReferenceType type, String conceptId)
    {
        this.referenceType = type;
        this.conceptId = conceptId;
    }

    public DocumentReferenceType getReferenceType()
    {
        return referenceType;
    }

    public void setReferenceType(DocumentReferenceType referenceType)
    {
        this.referenceType = referenceType;
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public void setConceptId(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public DocumentReferenceContent getContent()
    {
        DocumentReferenceContent content;

        content = new DocumentReferenceContent(referenceType, conceptId);
        content.setCode(code);
        content.setTerm(term);
        content.setDefinition(definition);
        content.setAbbreviation(abbreviation);
        return content;
    }

    public void setContent(DocumentReferenceContent content)
    {
        this.referenceType = content.getReferenceType();
        this.conceptId = content.getConceptId();
        this.code = content.getCode();
        this.term = content.getTerm();
        this.definition = content.getDefinition();
        this.abbreviation = content.getAbbreviation();
    }

    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        if (conceptId != null)
        {
            terminologyCollector.addTerminology(languageId, T8OntologyConceptType.DATA_RECORD, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);
        }
    }

    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        terminology = terminologyProvider.getTerminology(null, conceptId);
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }
    }

    public boolean isEqualTo(DocumentReferenceContent toContent)
    {
        if (!Objects.equals(referenceType, toContent.getReferenceType())) return false;
        else if (!Objects.equals(conceptId, toContent.getConceptId())) return false;
        else if (!Objects.equals(term, toContent.getTerm())) return false;
        else if (!Objects.equals(code, toContent.getCode())) return false;
        else if (!Objects.equals(abbreviation, toContent.getAbbreviation())) return false;
        else if (!Objects.equals(definition, toContent.getDefinition())) return false;
        else return true;
    }

    public DocumentReference copy()
    {
        DocumentReference copy;

        copy = new DocumentReference(referenceType, conceptId);
        copy.setCode(code);
        copy.setTerm(term);
        copy.setAbbreviation(abbreviation);
        copy.setDefinition(definition);
        return copy;
    }

    @Override
    public String toString()
    {
        return "DocumentReference{" + "referenceType=" + referenceType + ", conceptId=" + conceptId + '}';
    }
}
