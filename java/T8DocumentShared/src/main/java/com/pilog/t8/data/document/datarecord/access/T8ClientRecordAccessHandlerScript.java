package com.pilog.t8.data.document.datarecord.access;

import com.pilog.t8.script.T8ClientContextScript;

/**
 * @author Bouwer du Preez
 */
public interface T8ClientRecordAccessHandlerScript extends T8ClientContextScript
{
    public void setAccessHandler(T8DataRecordAccessHandler accessHandler);
}
