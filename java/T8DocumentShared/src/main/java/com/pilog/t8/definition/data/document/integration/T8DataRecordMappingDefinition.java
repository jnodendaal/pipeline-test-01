package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.data.document.integration.organization.T8DataRecordOrganizationMappingDefinition;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataRecordMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_DATA_RECORD";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_DATA_RECORD";
    public static final String DISPLAY_NAME = "Data Record Mapping";
    public static final String DESCRIPTION = "Specifies how this data record should be mapped to an interface";
    public static final String IDENTIFIER_PREFIX = "DRM_";

    public enum Datum
    {
        RECORD_PATH,
        RECORD_INTERFACE_TERM,
        INTERFACE_ORGANIZATION_MAPPINGS,
        EXTERNAL_REFERENCE_DOC_PATH,
        EXTERNAL_NUMBER_MAPPING,
        PROPERTY_DEFINITIONS,
        FIELD_DEFINITIONS,
        SUB_RECORD_DEFINITIONS,
        RECORD_DESCRIPTIONS_INCLUDED,
        RECORD_DESCRIPTION_MAPPINGS,
        FLAG_MAPPING_DEFINITIONS,
        RECORD_CLASSIFICATION_PROPERTY_INCLUDED,
        RECORD_CLASSIFICATION_PROPERTY_MAPPING_TERM,
        RECORD_CLASSIFICATION_STRUCTURE_ID
    };
// -------- Definition Meta-Data -------- //
    public T8DataRecordMappingDefinition(String identifier)
    {
        super(identifier);
    }

    public <T extends Object> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.property.mapping.ClassificationPropertyMapping").getConstructor(T8Context.class, T8DataRecordMappingDefinition.class);
            return (DataRecordIntegrationPropertyMapper<T>) constructor.newInstance(context, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new property mapper instance", ex);
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_PATH.toString(), "Record Path", "The record path that points to this record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_INTERFACE_TERM.toString(), "Record Interface Term", "The term that will be used to map this record to the interface."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INTERFACE_ORGANIZATION_MAPPINGS.toString(), "Interface Organization Mappings", "The list that will be used for the organization mappings of the target system."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.EXTERNAL_REFERENCE_DOC_PATH.toString(), "External Reference Doc Path", "The doc path that will be evaluated to determine the external reference code for this record."));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.EXTERNAL_NUMBER_MAPPING.toString(), "External Number Mapping", "The mapping for the External Number for the Root Data Record."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_DEFINITIONS.toString(), "Property Mappings", "The mappings between the data record properties and the property terms in the target system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Field Mappings", "The mappings between the data record fields and the property terms in the target system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SUB_RECORD_DEFINITIONS.toString(), "Sub Records", "All the sub records to be evaluated."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RECORD_DESCRIPTIONS_INCLUDED.toString(), "Include Record Descriptions", "Include Record Description mappings to the XML file."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.RECORD_DESCRIPTION_MAPPINGS.toString(), "Record Description Mappings", "All the Record Description Mappings to be considered."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FLAG_MAPPING_DEFINITIONS.toString(), "Flag Mappings", "The mapping for the flag definitions for the data record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.RECORD_CLASSIFICATION_PROPERTY_INCLUDED.toString(), "Include Record Classifications", "Include Record Classification mappings to the XML file."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_CLASSIFICATION_PROPERTY_MAPPING_TERM.toString(), "Record Classification Mapping Term", "The classification mapping term that will be used on the XML file."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_CLASSIFICATION_STRUCTURE_ID.toString(), "Record Classification Structure", "The classification structure that the record is linked to."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INTERFACE_ORGANIZATION_MAPPINGS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordOrganizationMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.EXTERNAL_NUMBER_MAPPING.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordExternalNumberingDefinition.GROUP_IDENTIFIER));
        else if(Datum.SUB_RECORD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8SubDataRecordMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.PROPERTY_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordPropertyMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFieldMappingDefinition.GROUP_IDENTIFIER));
        else if (Datum.RECORD_DESCRIPTION_MAPPINGS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordDescriptionMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.FLAG_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFlagMappingDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    public String getRecordPathExpression()
    {
        return (String)getDefinitionDatum(Datum.RECORD_PATH);
    }

    public void setRecordPathExpression(String expression)
    {
        setDefinitionDatum(Datum.RECORD_PATH, expression);
    }

    public String getRecordInterfaceTerm()
    {
        return (String)getDefinitionDatum(Datum.RECORD_INTERFACE_TERM);
    }

    public void setRecordInterfaceTerm(String term)
    {
        setDefinitionDatum(Datum.RECORD_INTERFACE_TERM, term);
    }

    public List<T8DataRecordOrganizationMappingDefinition> getInterfaceOrganizationDefinitions()
    {
        return (List<T8DataRecordOrganizationMappingDefinition>)getDefinitionDatum(Datum.INTERFACE_ORGANIZATION_MAPPINGS);
    }

    public void setInterfaceOrganizationDefinitions(List<T8DataRecordOrganizationMappingDefinition> dataRecordsOrganizationMappingDefinitions)
    {
        setDefinitionDatum(Datum.INTERFACE_ORGANIZATION_MAPPINGS, dataRecordsOrganizationMappingDefinitions);
    }

    public String getExternalReferencePathExpression()
    {
        return (String)getDefinitionDatum(Datum.EXTERNAL_REFERENCE_DOC_PATH);
    }

    public void setExternalReferencePathExpression(String expression)
    {
        setDefinitionDatum(Datum.EXTERNAL_REFERENCE_DOC_PATH, expression);
    }

    public T8DataRecordExternalNumberingDefinition getExternalNumberDefinition()
    {
        return (T8DataRecordExternalNumberingDefinition) getDefinitionDatum(Datum.EXTERNAL_NUMBER_MAPPING);
    }

    public void setExternalNumberDefinition(T8DataRecordExternalNumberingDefinition externalNumberDefinition)
    {
        setDefinitionDatum(Datum.EXTERNAL_NUMBER_MAPPING, externalNumberDefinition);
    }

    public List<T8DataRecordPropertyMappingDefinition> getPropertyDefinitions()
    {
        return (List<T8DataRecordPropertyMappingDefinition>) getDefinitionDatum(Datum.PROPERTY_DEFINITIONS);
    }

    public void setPropertyDefinitions(List<T8DataRecordPropertyMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_DEFINITIONS, definitions);
    }

    public List<T8DataRecordFieldMappingDefinition> getFieldDefinitions()
    {
        return (List<T8DataRecordFieldMappingDefinition>) getDefinitionDatum(Datum.FIELD_DEFINITIONS);
    }

    public void setFieldDefinitions(List<T8DataRecordFieldMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS, definitions);
    }

    public List<T8SubDataRecordMappingDefinition> getSubRecordDefinitions()
    {
        return (List<T8SubDataRecordMappingDefinition>)getDefinitionDatum(Datum.SUB_RECORD_DEFINITIONS);
    }

    public void setSubRecordDefinitions(List<T8SubDataRecordMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.SUB_RECORD_DEFINITIONS, definitions);
    }

    public Boolean isRecordDescritptionIncluded()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.RECORD_DESCRIPTIONS_INCLUDED));
    }

    public void setRecordDescritptionIncluded(Boolean isRecordDescritptionIncluded)
    {
        setDefinitionDatum(Datum.RECORD_DESCRIPTIONS_INCLUDED, isRecordDescritptionIncluded);
    }

    public List<T8DataRecordDescriptionMappingDefinition> getDataRecordDescriptionMappingDefinition()
    {
        return (List<T8DataRecordDescriptionMappingDefinition>)getDefinitionDatum(Datum.RECORD_DESCRIPTION_MAPPINGS);
    }

    public void setDataRecordDescriptionMappingDefinition(List<T8DataRecordDescriptionMappingDefinition> dataRecordDescriptionMappingDefinition)
    {
        setDefinitionDatum(Datum.RECORD_DESCRIPTION_MAPPINGS, dataRecordDescriptionMappingDefinition);
    }

    public List<T8DataRecordFlagMappingDefinition> getFlagDefinitions()
    {
        return (List<T8DataRecordFlagMappingDefinition>)getDefinitionDatum(Datum.FLAG_MAPPING_DEFINITIONS);
    }

    public void setFlagDefinitions(List<T8DataRecordFlagMappingDefinition> dataRecordFlagKeyMappingDefinitions)
    {
        setDefinitionDatum(Datum.FLAG_MAPPING_DEFINITIONS, dataRecordFlagKeyMappingDefinitions);
    }

    public Boolean isRecordClassificationsPropertyIncluded()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.RECORD_CLASSIFICATION_PROPERTY_INCLUDED));
    }

    public void setRecordClassificationsPropertyIncluded(Boolean isRecordClassificationsPropertyIncluded)
    {
        setDefinitionDatum(Datum.RECORD_CLASSIFICATION_PROPERTY_INCLUDED, isRecordClassificationsPropertyIncluded);
    }

    public String getRecordClassificationPropertyTerm()
    {
        return (String)getDefinitionDatum(Datum.RECORD_CLASSIFICATION_PROPERTY_MAPPING_TERM);
    }

    public void setRecordClassificationPropertyTerm(String term)
    {
        setDefinitionDatum(Datum.RECORD_CLASSIFICATION_PROPERTY_MAPPING_TERM, term);
    }

    public String getRecordClassificationStructureID()
    {
        return (String)getDefinitionDatum(Datum.RECORD_CLASSIFICATION_STRUCTURE_ID);
    }

    public void setRecordClassificationStructureID(String descriptionTypeConcept)
    {
        setDefinitionDatum(Datum.RECORD_CLASSIFICATION_STRUCTURE_ID, descriptionTypeConcept);
    }
}
