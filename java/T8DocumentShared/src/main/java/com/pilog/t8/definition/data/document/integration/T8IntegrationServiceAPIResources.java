package com.pilog.t8.definition.data.document.integration;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Hennie Brink
 */
public class T8IntegrationServiceAPIResources implements T8DefinitionResource
{
    public static final String DE_HISTORY_INTEGRATION_SERVICE = "@E_HISTORY_INTEGRATION_SERVICE";
    public static final String DS_HISTORY_INTEGRATION_SERVICE = "@DS_HISTORY_INTEGRATION_SERVICE";

    public static final String EF_EVENT_IID = "$EVENT_IID";
    public static final String EF_DIRECTION = "$DIRECTION";
    public static final String EF_STATUS = "$STATUS";
    public static final String EF_OPERATION_ID = "$OPERATION_ID";
    public static final String EF_OPERATION_IID = "$OPERATION_IID";
    public static final String EF_INSERTED_BY_ID = "$INSERTED_BY_ID";
    public static final String EF_INSERTED_BY_IID = "$INSERTED_BY_IID";
    public static final String EF_MESSAGE_DATA = "$MESSAGE_DATA";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))
        {
            T8TableDataSourceDefinition tableDataSourceDefinition;

            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DS_HISTORY_INTEGRATION_SERVICE);
            tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
            tableDataSourceDefinition.setTableName("HISTORY_INTEGRATION_SERVICE");
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT_IID, "EVENT_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DIRECTION, "DIRECTION", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_STATUS, "STATUS", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OPERATION_ID, "OPERATION_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OPERATION_IID, "OPERATION_IID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_AT, "INSERTED_AT", false, true, T8DataType.DATE_TIME));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_ID, "INSERTED_BY_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_INSERTED_BY_IID, "INSERTED_BY_IID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_MESSAGE_DATA, "MESSAGE_DATA", false, true, T8DataType.LONG_STRING));
            definitions.add(tableDataSourceDefinition);
        }

        // Added entity definitions if needed.
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = new T8DataEntityResourceDefinition(DE_HISTORY_INTEGRATION_SERVICE);
            entityDefinition.setDataSourceIdentifier(DS_HISTORY_INTEGRATION_SERVICE);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT_IID, DS_HISTORY_INTEGRATION_SERVICE + EF_EVENT_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DIRECTION, DS_HISTORY_INTEGRATION_SERVICE + EF_DIRECTION, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_STATUS, DS_HISTORY_INTEGRATION_SERVICE + EF_STATUS, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OPERATION_ID, DS_HISTORY_INTEGRATION_SERVICE + EF_OPERATION_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OPERATION_IID, DS_HISTORY_INTEGRATION_SERVICE + EF_OPERATION_IID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_AT, DS_HISTORY_INTEGRATION_SERVICE + EF_INSERTED_AT, false, true, T8DataType.DATE_TIME));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_ID, DS_HISTORY_INTEGRATION_SERVICE + EF_INSERTED_BY_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_INSERTED_BY_IID, DS_HISTORY_INTEGRATION_SERVICE + EF_INSERTED_BY_IID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_MESSAGE_DATA, DS_HISTORY_INTEGRATION_SERVICE + EF_MESSAGE_DATA, false, true, T8DataType.LONG_STRING));
            definitions.add(entityDefinition);
        }

        return definitions;
    }

}
