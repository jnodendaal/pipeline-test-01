package com.pilog.t8.data.alteration;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationPackageIndexChange implements Serializable
{
    private final IndexChangeType changeType;
    private final T8DataAlterationPackageIndexEntry entry;
    private final int startStep;
    private final int stepChange;

    public enum IndexChangeType {INSERT, UPDATE, DELETE};

    public T8DataAlterationPackageIndexChange(IndexChangeType changeType, T8DataAlterationPackageIndexEntry entry, int startStep, int stepChange)
    {
        this.changeType = changeType;
        this.entry = entry;
        this.startStep = startStep;
        this.stepChange = stepChange;
    }

    public IndexChangeType getChangeType()
    {
        return changeType;
    }

    public T8DataAlterationPackageIndexEntry getEntry()
    {
        return entry;
    }

    public int getStartStep()
    {
        return startStep;
    }

    public int getStepChange()
    {
        return stepChange;
    }
}
