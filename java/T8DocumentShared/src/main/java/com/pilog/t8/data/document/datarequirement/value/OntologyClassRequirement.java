package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;

/**
 * @author Bouwer du Preez
 */
public class OntologyClassRequirement extends ValueRequirement
{
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;

    public OntologyClassRequirement()
    {
        super(RequirementType.ONTOLOGY_CLASS);
    }

    public OntologyClassRequirement(String conceptID)
    {
        super(RequirementType.ONTOLOGY_CLASS, conceptID);
    }

    public String getOntologyClassID()
    {
        return getValue();
    }

    public String getOntologyClassId()
    {
        return getValue();
    }

    /**
     * Adds the terminology of this value Requirement and of all its
     * descendants to the supplied list.
     * @param terminologyCollector The list to which terminology will be added.
     */
    @Override
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        super.addContentTerminology(terminologyCollector);
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.ONTOLOGY_CLASS, getOntologyClassId(), codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);
    }

    /**
     * Sets the terminology of this value Requirement and all of its descendants
     * by retrieval from the supplied terminology provider.
     * @param terminologyProvider The terminology provider form which to fetch terminology.
     */
    @Override
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        super.setContentTerminology(terminologyProvider);
        terminology = terminologyProvider.getTerminology(null, getOntologyClassId());
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }
    }
}
