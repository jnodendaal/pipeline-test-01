package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class RealValueContent extends ValueContent
{
    private String value;

    public RealValueContent()
    {
        super(RequirementType.REAL_TYPE);
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof RealValueContent)
        {
            RealValueContent toRealContent;

            toRealContent = (RealValueContent)toContent;
            return Objects.equals(value, toRealContent.getValue());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof RealValueContent)
        {
            RealValueContent toRealContent;

            toRealContent = (RealValueContent)toContent;
            return Objects.equals(value, toRealContent.getValue());
        }
        else return false;
    }
}
