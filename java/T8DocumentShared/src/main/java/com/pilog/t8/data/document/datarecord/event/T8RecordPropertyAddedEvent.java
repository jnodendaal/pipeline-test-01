package com.pilog.t8.data.document.datarecord.event;

import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordProperty;

/**
 * @author Bouwer du Preez
 */
public class T8RecordPropertyAddedEvent extends T8DataRecordChangedEvent
{
    private final RecordSection recordClass;
    private final RecordProperty recordProperty;
    private final Object actor;
    
    public T8RecordPropertyAddedEvent(RecordSection recordClass, RecordProperty recordProperty, Object actor)
    {
        super(recordClass.getParentDataRecord());
        this.recordClass = recordClass;
        this.recordProperty = recordProperty;
        this.actor = actor;
    }
    
    public RecordSection getRecordClass()
    {
        return recordClass;
    }
    
    public RecordProperty getRecordProperty()
    {
        return recordProperty;
    }
    
    public Object getActor()
    {
        return actor;
    }

    @Override
    public String toString()
    {
        return "T8RecordPropertyAddedEvent{" + "recordClass=" + recordClass + ", recordProperty=" + recordProperty + '}';
    }
}
