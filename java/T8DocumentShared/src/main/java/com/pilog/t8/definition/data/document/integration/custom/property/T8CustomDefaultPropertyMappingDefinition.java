/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.data.document.integration.custom.property;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.definition.data.document.integration.property.T8DefaultPropertyMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Andre Scheepers
 */
public class T8CustomDefaultPropertyMappingDefinition extends T8CustomPropertyMappingDefinition
{
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_CUSTOM_DEFAULT_PROPERTY_MAPPING";
    public static final String DISPLAY_NAME = "Custom Default Property Mapping";
    public static final String DESCRIPTION = "A Custom Default Property mapping to the system which the Integration process will map to.";   
    
    public enum Datum
    {
        PROPERTY_ID,
        MAPPING_TYPE,
        SEQUENCE
    };

    public static enum MappingTypes
    {
        CODE,
        CONCEPT,
        TERM,
        ABBREVIATION,
        DEFINITION,
        VALUE_STRING
    }

    public T8CustomDefaultPropertyMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;
        
        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, T8DefaultPropertyMappingDefinition.Datum.PROPERTY_ID.toString(), "Property ID", "The property ID to be mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, T8DefaultPropertyMappingDefinition.Datum.MAPPING_TYPE.toString(), "Mapping Type", "The type of mapping that will be performed on this property", T8DefaultPropertyMappingDefinition.MappingTypes.VALUE_STRING.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEQUENCE.toString(), "Sequence", "The sequence of the proeprties in the string value."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {        
        if(T8DefaultPropertyMappingDefinition.Datum.MAPPING_TYPE.toString().equals(datumIdentifier)) return createStringOptions(T8DefaultPropertyMappingDefinition.MappingTypes.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }
    
    public <T extends Object> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.property.mapping.DefaultPropertyMapping").getConstructor(T8ServerContext.class, T8SessionContext.class, T8DefaultPropertyMappingDefinition.class);
            return (DataRecordIntegrationPropertyMapper<T>) constructor.newInstance(serverContext, sessionContext, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new property mapper instance", ex);
            return null;
        }
    }

    public String getPropertyID()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_ID.toString());
    }

    public void setPropertyID(String propertyID)
    {
        setDefinitionDatum(Datum.PROPERTY_ID.toString(), propertyID);
    }

    public MappingTypes getMappingType()
    {
        return MappingTypes.valueOf((String)getDefinitionDatum(Datum.MAPPING_TYPE.toString()));
    }

    public void setMappingType(MappingTypes type)
    {
        setDefinitionDatum(Datum.MAPPING_TYPE.toString(), type.toString());
    }

    public String getSequence()
    {
        return (String)getDefinitionDatum(Datum.SEQUENCE.toString());
    }

    public void setSequence(String sequence)
    {
        setDefinitionDatum(Datum.SEQUENCE.toString(), sequence);
    }
}
