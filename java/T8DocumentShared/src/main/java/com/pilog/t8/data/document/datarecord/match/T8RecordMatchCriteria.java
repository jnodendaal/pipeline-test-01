package com.pilog.t8.data.document.datarecord.match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8RecordMatchCriteria implements Serializable
{
    private String id;
    private String orgId;
    private String name;
    private String description;
    private final List<T8RecordMatchCase> cases;

    public T8RecordMatchCriteria(String id)
    {
        this.id = id;
        this.cases = new ArrayList<>();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public T8RecordMatchCase getCase(String caseID)
    {
        for (T8RecordMatchCase matchCase : cases)
        {
            if (matchCase.getId().equals(caseID))
            {
                return matchCase;
            }
        }

        return null;
    }

    public int getCaseIndex(T8RecordMatchCase matchCase)
    {
        return cases.indexOf(matchCase);
    }

    public int incrementCaseIndex(T8RecordMatchCase matchCase)
    {
        int index;

        index = cases.indexOf(matchCase);
        if (index > -1)
        {
            if (index < (cases.size() - 1))
            {
                int newIndex;

                newIndex = index + 1;
                cases.remove(matchCase);
                cases.add(newIndex, matchCase);
                return newIndex;
            }
            else return index;
        }
        else return -1;
    }

    public int decrementCaseIndex(T8RecordMatchCase matchCase)
    {
        int index;

        index = cases.indexOf(matchCase);
        if (index > -1)
        {
            if (index > 0)
            {
                int newIndex;

                newIndex = index - 1;
                cases.remove(matchCase);
                cases.add(newIndex, matchCase);
                return newIndex;
            }
            else return 0;
        }
        else return -1;
    }

    public void addCase(T8RecordMatchCase matchCase)
    {
        cases.add(matchCase);
        matchCase.setParentMatchCriteria(this);
    }

    public boolean removeCase(T8RecordMatchCase matchCase)
    {
        if (cases.remove(matchCase))
        {
            matchCase.setParentMatchCriteria(null);
            return true;
        }
        else return false;
    }

    public List<T8RecordMatchCase> getCases()
    {
        return new ArrayList<T8RecordMatchCase>(cases);
    }
}
