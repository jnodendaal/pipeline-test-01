package com.pilog.t8.data.document.datastring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringFilter implements Serializable
{
    private List<String> recordIDList;
    private List<String> instanceIDList;
    private List<String> typeIDList;
    private List<String> languageIDList;

    public T8DataStringFilter()
    {
    }

    public List<String> getRecordIDList()
    {
        return recordIDList;
    }

    public void setRecordIDList(List<String> recordIDList)
    {
        this.recordIDList = recordIDList;
    }

    public List<String> getInstanceIDList()
    {
        return instanceIDList;
    }

    public void setInstanceIDList(List<String> instanceIDList)
    {
        this.instanceIDList = instanceIDList;
    }

    public List<String> getTypeIDList()
    {
        return typeIDList;
    }

    public void setTypeIDList(List<String> typeIDList)
    {
        this.typeIDList = typeIDList;
    }

    public List<String> getLanguageIDList()
    {
        return languageIDList;
    }

    public void setLanguageIDList(List<String> languageIDList)
    {
        this.languageIDList = languageIDList;
    }

    public static class T8DataStringFilterBuilder
    {
        private List<String> recordIDList;
        private List<String> instanceIDList;
        private List<String> typeIDList;
        private List<String> languageIDList;

        public T8DataStringFilter build()
        {
            T8DataStringFilter filter;

            filter = new T8DataStringFilter();
            filter.setRecordIDList(recordIDList);
            filter.setInstanceIDList(instanceIDList);
            filter.setLanguageIDList(languageIDList);
            filter.setTypeIDList(typeIDList);
            return filter;
        }

        public T8DataStringFilterBuilder records(List<String> recordIDList)
        {
            this.recordIDList = recordIDList;
            return this;
        }

        public T8DataStringFilterBuilder record(String recordID)
        {
            if (recordIDList == null) recordIDList = new ArrayList<>();
            recordIDList.add(recordID);
            return this;
        }

        public T8DataStringFilterBuilder languages(List<String> languageIDList)
        {
            this.languageIDList = languageIDList;
            return this;
        }

        public T8DataStringFilterBuilder language(String languageID)
        {
            if (languageIDList == null) languageIDList = new ArrayList<>();
            languageIDList.add(languageID);
            return this;
        }

        public T8DataStringFilterBuilder types(List<String> typeIDList)
        {
            this.typeIDList = typeIDList;
            return this;
        }

        public T8DataStringFilterBuilder instances(List<String> instanceIDList)
        {
            this.instanceIDList = instanceIDList;
            return this;
        }
    }
}
