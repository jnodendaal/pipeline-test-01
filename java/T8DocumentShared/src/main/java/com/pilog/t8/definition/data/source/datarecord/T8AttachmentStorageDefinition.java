package com.pilog.t8.definition.data.source.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentStorageDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_ATTACHMENT_STORAGE";
    public static final String GROUP_IDENTIFIER = "@DG_ATTACHMENT_STORAGE";
    public static final String GROUP_NAME = "Attachment Storage";
    public static final String GROUP_DESCRIPTION = "Configuration settings pertaining to the various stores where record attachments are located.";
    public static final String DISPLAY_NAME = "Attachment Storage";
    public static final String DESCRIPTION = "A definition that specifies storage configuration for specific attachment types.";
    public static final String IDENTIFIER_PREFIX = "ATTACH_STORE_";
    public enum Datum {DR_INSTANCE_ID_LIST,
                       DATABASE_STORAGE,
                       FILE_CONTEXT_ID};
    // -------- Definition Meta-Data -------- //

    public T8AttachmentStorageDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.DR_INSTANCE_ID_LIST.toString(), "Data Requirement Instances", "The list Data Requirement Instance to which this attachment storage definition applies.  If left empty, this storage definition will be applied to all instances that are not specifically referenced in another storage definition."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DATABASE_STORAGE.toString(), "Database Storage",  "A boolean flag indicating whether or not attachment should be stored in the database."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FILE_CONTEXT_ID.toString(), "File Context",  "The file context (if any) where attachments should be stored."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.FILE_CONTEXT_ID.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8FileContextDefinition.GROUP_IDENTIFIER), true, "No File Context Storage");
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }

    public List<String> getDataRequirementInstanceIDList()
    {
        return (List<String>)getDefinitionDatum(Datum.DR_INSTANCE_ID_LIST.toString());
    }

    public void setDataRequirementInstanceIDList(List<String> drInstanceIDList)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_ID_LIST.toString(), drInstanceIDList);
    }

    public boolean isDatabaseStorage()
    {
        Boolean databaseStorage;

        databaseStorage = (Boolean)getDefinitionDatum(Datum.DATABASE_STORAGE.toString());
        return (databaseStorage != null) && databaseStorage;
    }

    public void setDatabaseStorage(boolean databaseStorage)
    {
        setDefinitionDatum(Datum.DATABASE_STORAGE.toString(), databaseStorage);
    }

    public String getFileContextID()
    {
        return (String)getDefinitionDatum(Datum.FILE_CONTEXT_ID.toString());
    }

    public void setFileContextID(String fileContextID)
    {
        setDefinitionDatum(Datum.FILE_CONTEXT_ID.toString(), fileContextID);
    }
}
