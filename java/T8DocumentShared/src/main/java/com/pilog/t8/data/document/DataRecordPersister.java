package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordMetaDataType;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface DataRecordPersister
{
    /**
     * This method validates (optional) and saves all of the Data Record changes
     * supplied.  After the changes have been saved the ontology or descriptions
     * for the affected Data Records can be generated if required.  If
     * description/ontology generation is specified but non-synchronous
     * generation is set, the transaction passed to this method will be
     * committed after successful updates to the affected records and BEFORE the
     * description/ontology generation is requested from the asynchronous
     * service.  This is necessary to ensure that the service uses the updated
     * data.  In all other scenarios, the transaction will not be touched.
     * @param insertedDataRecords A list of Data Records to insert.
     * @param updatedDataRecords A list of Data Records to update.
     * @param deletedDataRecordIDList A list of Data Record ID's to delete.
     * @param accessIdentifier The Identifier of the Data Access rule set that
     * is applicable to the Data Record Changes.
     * @param metaDataTypes The types of meta data to be generated for the 
     * affected records.
     * @throws java.lang.Exception
     */
    public void persistDataRecordChanges(List<DataRecord> insertedDataRecords, List<DataRecord> updatedDataRecords, List<String> deletedDataRecordIDList, String accessIdentifier, Collection<RecordMetaDataType> metaDataTypes) throws Exception;
}
