package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class QualifierOfMeasureRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public QualifierOfMeasureRequirement()
    {
        super(RequirementType.QUALIFIER_OF_MEASURE);
    }

    public QualifierOfMeasureRequirement(String conceptId)
    {
        super(RequirementType.QUALIFIER_OF_MEASURE, conceptId);
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public String getOntologyClassID()
    {
        OntologyClassRequirement classRequirement;

        classRequirement = (OntologyClassRequirement)getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        return classRequirement != null ? classRequirement.getOntologyClassID() : null;
    }

    public void setOntologyClassId(String ontologyClassId)
    {
        OntologyClassRequirement classRequirement;

        classRequirement = (OntologyClassRequirement)getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        if (classRequirement != null)
        {
            classRequirement.setValue(ontologyClassId);
        }
        else
        {
            classRequirement = (OntologyClassRequirement)ValueRequirement.createValueRequirement(RequirementType.ONTOLOGY_CLASS);
            classRequirement.setValue(ontologyClassId);
            addSubRequirement(classRequirement);
        }
    }
}
