package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class ConceptTypeRequirement extends ValueRequirement
{
    public ConceptTypeRequirement()
    {
        super(RequirementType.CONCEPT_TYPE);
    }
}
