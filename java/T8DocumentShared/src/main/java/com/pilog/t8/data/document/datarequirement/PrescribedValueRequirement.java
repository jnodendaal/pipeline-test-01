package com.pilog.t8.data.document.datarequirement;

/**
 * An interface which is intended to identify specific {@code ValueRequirement}
 * types as standardizable. Any {@code ValueRequirement} which implements this
 * class has to, at the very least, implement the
 * {@link #isRestrictToStandardValue()} method.<br/>
 * <br/>
 * In the event that a {@code ValueRequirement} does not implement the
 * {@link #isPrescribed()} method, the default standard is to return
 * {@code true}.
 *
 * @author Gavin Boshoff
 */
public interface PrescribedValueRequirement
{
    /**
     * Returns {@code true}, iff the {@code ValueRequirement} instance is
     * intended to be standardized. The default assumes it should.
     *
     * @return {@code true} if standardized. {@code false} otherwise
     */
    default boolean isPrescribed()
    {
        return true;
    }

    /**
     * Returns {@code true} if the {@code ValueRequirement} is restricted to a
     * standard value selection.
     *
     * @return {@code true} if restricted. {@code false} otherwise
     */
    boolean isRestrictToStandardValue();
}