package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.alteration.T8DataAlterationPackage;

/**
 *
 * @author Pieter Strydom
 */
public class T8DtDataAlterationPackage  extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_ALTERATION_PACKAGE";
    
    public T8DtDataAlterationPackage(T8DefinitionManager definitionManager)
    {
    }
    
    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes) 
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName() 
    {
         return T8DataAlterationPackage.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object) 
    {
        if (object != null)
        {
            T8DataAlterationPackage alterationPackage;

            alterationPackage = (T8DataAlterationPackage)object;
            return alterationPackage.serialize();
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue) 
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                JsonObject alterationPackageObject;

                alterationPackageObject = jsonValue.asObject();

                return new T8DataAlterationPackage(alterationPackageObject);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deserializing data alteration package.", e);
            }
        }
        else return null;
    }
    
}
