package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Andre Scheepers
 */
public class T8PrescribedValueApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_PV_RETRIEVE_PRESCRIBED_VALUE_SUGGESTIONS = "@OS_API_PV_RETRIEVE_PRESCRIBED_VALUE_SUGGESTIONS";
    public static final String OPERATION_API_PV_REPLACE_PRESCRIBED_VALUE_REFERENCES = "@OS_API_PV_REPLACE_PRESCRIBED_VALUE_REFERENCES";
    public static final String OPERATION_API_PV_REPLACE_PRESCRIBED_VALUES = "@OS_API_PV_REPLACE_PRESCRIBED_VALUES";
    public static final String OPERATION_API_PV_SAVE_PRESCRIBED_VALUES = "@OS_API_PV_SAVE_PRESCRIBED_VALUES";
    public static final String OPERATION_API_PV_INSERT_PRESCRIBED_VALUES = "@OS_API_PV_INSERT_PRESCRIBED_VALUES";
    public static final String OPERATION_API_PV_UPDATE_PRESCRIBED_VALUES = "@OS_API_PV_UPDATE_PRESCRIBED_VALUES";
    public static final String OPERATION_API_PV_DELETE_PRESCRIBED_VALUES = "@OS_API_PV_DELETE_PRESCRIBED_VALUE";
    public static final String OPERATION_API_PV_SET_STANDARD = "@OS_API_PV_SET_STANDARD";
    public static final String OPERATION_API_PV_SET_APPROVED = "@OS_API_PV_SET_APPROVED";

    public static final String PARAMETER_SEARCH_PREFIX = "$P_SEARCH_PREFIX";
    public static final String PARAMETER_VALUE_REQUIREMENT = "$P_VALUE_REQUIREMENT";
    public static final String PARAMETER_VALUE_ID = "$P_VALUE_ID";
    public static final String PARAMETER_REPLACEMENT_VALUE_ID = "$P_REPLACEMENT_VALUE_ID";
    public static final String PARAMETER_STANDARD_INDICATOR = "$P_STANDARD_INDICATOR";
    public static final String PARAMETER_APPROVED_INDICATOR = "$P_APPROVED_INDICATOR";
    public static final String PARAMETER_PRESCRIBED_VALUE = "$P_PRESCRIBED_VALUE";
    public static final String PARAMETER_PRESCRIBED_VALUE_LIST = "$P_PRESCRIBED_VALUE_LIST";
    public static final String PARAMETER_RECORD_VALUES = "$P_RECORD_VALUES";
    public static final String PARAMETER_REFERENCE_COUNT = "$P_REFERENCE_COUNT";
    public static final String PARAMETER_REFERENCE_UPDATE_COUNT = "$P_REFERENCES_UPDATE_COUNT";
    public static final String PARAMETER_PRESCRIBED_VALUE_FILTER = "$P_PRESCRIBED_VALUE_FILTER";
    public static final String PARAMETER_VALUE_ID_LIST = "$P_VALUE_ID_LIST";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_RETRIEVE_PRESCRIBED_VALUE_SUGGESTIONS);
            definition.setMetaDisplayName("Retrieve Prescribed Value Suggestions");
            definition.setMetaDescription("Retrieve prescribed value suggestions for the specified search criteria.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiRetrievePrescribedValueSuggestions");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SEARCH_PREFIX, "Search Prefix", "The prefix that will assist in obtaning search results.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RECORD_VALUES, "Record Values", "A list of Record Value results.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_REPLACE_PRESCRIBED_VALUE_REFERENCES);
            definition.setMetaDisplayName("Replace Prescribed Value References");
            definition.setMetaDescription("Replaces all filtered references to the specified prescribed values with another.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiReplacePrescribedValueReferences");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_VALUE_ID_LIST, "Prescribed Value ID List", "A list of Value ID's that are to be replaced with the new value.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPLACEMENT_VALUE_ID, "Replacement Value ID", "The ID of the prescribed value to replace the identified values.", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REFERENCE_UPDATE_COUNT, "Reference Update Count", "The number of prescribed value references which were updated with the new reference.", T8DataType.INTEGER));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_REPLACE_PRESCRIBED_VALUES);
            definition.setMetaDisplayName("Replace Prescribed Values");
            definition.setMetaDescription("Replaces the specified prescibed values, and deletes the old values.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiReplacePrescribedValues");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_VALUE_ID_LIST, "Prescribed Value ID List", "A list of Value ID's that are to be replaced with the new value and deleted.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REPLACEMENT_VALUE_ID, "Replacement Value ID", "The ID of the prescribed value to replace the identified value references.", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_REFERENCE_UPDATE_COUNT, "Reference Update Count", "The number of prescribed value references which were updated with the new reference.", T8DataType.INTEGER));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_SAVE_PRESCRIBED_VALUES);
            definition.setMetaDisplayName("Save Prescribed Values");
            definition.setMetaDescription("Either updates or creates a prescribed value record.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiSavePrescribedValues");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PRESCRIBED_VALUE_LIST, "Prescribed Value List", "List of objects containing the details for the record to be updated/created.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_INSERT_PRESCRIBED_VALUES);
            definition.setMetaDisplayName("Insert Prescribed Values");
            definition.setMetaDescription("Inserts the supplied new prescribed value.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiInsertPrescribedValues");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PRESCRIBED_VALUE_LIST, "Prescribed Value", "List of objects containing the details for the prescribed value to be inserted.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_UPDATE_PRESCRIBED_VALUES);
            definition.setMetaDisplayName("Update Prescribed Values");
            definition.setMetaDescription("Updates the supplied prescribed value.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiUpdatePrescribedValues");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PRESCRIBED_VALUE_LIST, "Prescribed Value", "List of objects containing the details for the prescribed value to be updated.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_DELETE_PRESCRIBED_VALUES);
            definition.setMetaDisplayName("Delete Prescribed Values");
            definition.setMetaDescription("Removes the specified prescribed values.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiDeletePrescribedValues");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_VALUE_ID_LIST, "Prescribed Value ID List", "A list of the prescribed value ID's which are to be deleted.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_SET_STANDARD);
            definition.setMetaDisplayName("Set Standard");
            definition.setMetaDescription("Sets the standard indicator on the specified prescribed value.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiSetStandard");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_VALUE_ID, "Prescribed Value ID", "The prescribed value ID for which the prescribedized flag is to be set.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_STANDARD_INDICATOR, "Standard Indicator", "The flag value to which the prescribedized indicator should be set.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_PV_SET_APPROVED);
            definition.setMetaDisplayName("Set Approved");
            definition.setMetaDescription("Sets the standard indicator on the specified prescribed value.");
            definition.setClassName("com.pilog.t8.api.T8PrescribedValueApiOperations$ApiSetApproved");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_VALUE_ID, "Prescribed Value ID", "The prescribed value ID for which the prescribedized flag is to be set.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_APPROVED_INDICATOR, "Approved Indicator", "The flag value to which the prescribedized indicator should be set.", T8DataType.BOOLEAN));
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}
