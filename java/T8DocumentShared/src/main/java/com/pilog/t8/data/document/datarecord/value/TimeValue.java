package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.TimeRequirement;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bouwer du Preez
 */
public class TimeValue extends EpochValue
{
    public TimeValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public void setTime(Date time)
    {
        setValue(time != null ? ((Long)time.getTime()).toString() : null);
    }

    public void setTime(Long time)
    {
        setValue(time != null ? time.toString() : null);
    }

    @Override
    public TimeValueContent getContent()
    {
        TimeValueContent content;

        content = new TimeValueContent();
        content.setTime(getValue());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        TimeValueContent content;
        String time;

        content = (TimeValueContent)valueContent;
        time = content.getTime();
        if (time != null)
        {
            // If the input value is an interger (millisecond value), use it as-is.
            if (Strings.isInteger(time))
            {
                setValue(time);
            }
            else
            {
                // The input value is not an integer, so we try to parse it.
                try
                {
                    SimpleDateFormat format;

                    // Attempt to parse the date using the format specified by the date requirement.
                    format = new SimpleDateFormat(((TimeRequirement)valueRequirement).getTimeFormat());
                    setTime(format.parse(time));
                }
                catch (Exception e)
                {
                    // Set the unparseable value.
                    setValue(time);
                }
            }
        }
        else setValue(null);
    }
}
