package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordCreation;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordDeletion;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileRecordDeletion extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_UPDATE_RECORD_DELETION";

    public T8DtDataFileRecordDeletion(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileRecordCreation.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DataFileRecordDeletion deletion;
            JsonObject jsonNewRecord;

            deletion = (T8DataFileRecordDeletion)object;

            // Create the json object.
            jsonNewRecord = new JsonObject();
            jsonNewRecord.add("recordId", deletion.getRecordId());
            jsonNewRecord.add("propertyId", deletion.getPropertyId());
            jsonNewRecord.add("subRecordId", deletion.getSubRecordId());
            return jsonNewRecord;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8DataFileRecordDeletion deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                T8DataFileRecordDeletion deletion;
                JsonObject jsonNewRecord;

                jsonNewRecord = (JsonObject)jsonValue;
                deletion = new T8DataFileRecordDeletion(null, null, null);
                deletion.setRecordId(jsonNewRecord.getString("recordId"));
                deletion.setPropertyId(jsonNewRecord.getString("propertyId"));
                deletion.setSubRecordId(jsonNewRecord.getString("subRecordId"));
                return deletion;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while parsing input record deletion from JSON: " + jsonValue, e);
            }
        }
        else return null;
    }
}
