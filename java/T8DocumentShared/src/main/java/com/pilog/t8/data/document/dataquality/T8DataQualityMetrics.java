package com.pilog.t8.data.document.dataquality;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataQualityMetrics implements Serializable
{
    private int recordCount; // Total number of records used in the aggregation.
    private int propertyCount; // Total number of properties in the Data Requirements used.
    private int propertyDataCount; // Number of properties that have data (FFT or VALUE).
    private int propertyValidCount; // Number of properties that have valid data (VALUE).
    private int propertyVerificationCount; // Number of properties verified against an external source.
    private int propertyAccurateCount; // Number of properties verified against an external source and found to be accurate.
    private int characteristicCount; // Number of characteristic properties in the Data Requirement.
    private int characteristicDataCount; // Number of characteristic properties that have data (FFT or VALUE).
    private int characteristicValidCount; // Number of characteristic properties that have valid data (VALUE).
    private int characteristicVerificationCount; // Number of characteristic properties verified against an external source.
    private int characteristicAccurateCount; // Number of characteristic properties verified against an external source and found to be accurate.

    public T8DataQualityMetrics()
    {
        this.recordCount = 0;
        this.propertyCount = 0;
        this.propertyDataCount = 0;
        this.propertyValidCount = 0;
        this.propertyVerificationCount = 0;
        this.propertyAccurateCount = 0;
        this.characteristicCount = 0;
        this.characteristicDataCount = 0;
        this.characteristicValidCount = 0;
        this.characteristicVerificationCount = 0;
        this.characteristicAccurateCount = 0;
    }

    public void addMetrics(T8DataQualityMetrics metrics)
    {
        this.recordCount += metrics.getRecordCount();
        this.propertyCount += metrics.getPropertyCount();
        this.propertyDataCount += metrics.getPropertyDataCount();
        this.propertyValidCount += metrics.getPropertyValidCount();
        this.propertyVerificationCount += metrics.getPropertyVerificationCount();
        this.propertyAccurateCount += metrics.getPropertyAccurateCount();
        this.characteristicCount += metrics.getCharacteristicCount();
        this.characteristicDataCount += metrics.getCharacteristicDataCount();
        this.characteristicValidCount += metrics.getCharacteristicValidCount();
        this.characteristicVerificationCount += metrics.getCharacteristicVerificationCount();
        this.characteristicAccurateCount += metrics.getCharacteristicAccurateCount();
    }

    public float getAverageCharacteristicCount()
    {
        return (float)this.characteristicCount / (float)this.recordCount;
    }

    public float getAverageCharacteristicDataCount()
    {
        return (float)this.characteristicDataCount / (float)this.recordCount;
    }

    public float getPropertyCompleteness()
    {
        return (float)this.propertyDataCount / (float)this.propertyCount * 100f;
    }

    public float getCharacteristicCompleteness()
    {
        return (float)this.characteristicDataCount / (float)this.characteristicCount * 100f;
    }

    public float getCharacteristicValidity()
    {
        return (float)this.characteristicValidCount / (float)this.characteristicDataCount * 100f;
    }

    public float getNonCharacteristicCompleteness()
    {
        return (float)this.getNonCharacteristicDataCount() / (float)this.getNonCharacteristicCount() * 100f;
    }

    public float getNonCharacteristicValidity()
    {
        return (float)this.getNonCharacteristicValidCount() / (float)this.getNonCharacteristicDataCount() * 100f;
    }

    public int getRecordCount()
    {
        return recordCount;
    }

    public void setRecordCount(int rootRecordCount)
    {
        this.recordCount = rootRecordCount;
    }

    public int getPropertyCount()
    {
        return propertyCount;
    }

    public void setPropertyCount(int propertyCount)
    {
        this.propertyCount = propertyCount;
    }

    public int getPropertyDataCount()
    {
        return propertyDataCount;
    }

    public void setPropertyDataCount(int propertyDataCount)
    {
        this.propertyDataCount = propertyDataCount;
    }

    public int getPropertyVerificationCount()
    {
        return propertyVerificationCount;
    }

    public void setPropertyVerificationCount(int propertyVerificationCount)
    {
        this.propertyVerificationCount = propertyVerificationCount;
    }

    public int getPropertyAccurateCount()
    {
        return propertyAccurateCount;
    }

    public void setPropertyAccurateCount(int propertyAccurateCount)
    {
        this.propertyAccurateCount = propertyAccurateCount;
    }

    public int getPropertyIncorrectCount()
    {
        return propertyDataCount - propertyAccurateCount;
    }

    public int getPropertyValidCount()
    {
        return propertyValidCount;
    }

    public void setPropertyValidCount(int propertyValidCount)
    {
        this.propertyValidCount = propertyValidCount;
    }

    public int getPropertyInvalidCount()
    {
        return propertyDataCount - propertyValidCount;
    }

    public int getCharacteristicCount()
    {
        return characteristicCount;
    }

    public void setCharacteristicCount(int characteristicCount)
    {
        this.characteristicCount = characteristicCount;
    }

    public int getCharacteristicDataCount()
    {
        return characteristicDataCount;
    }

    public void setCharacteristicDataCount(int characteristicDataCount)
    {
        this.characteristicDataCount = characteristicDataCount;
    }

    public int getCharacteristicVerificationCount()
    {
        return characteristicVerificationCount;
    }

    public void setCharacteristicVerificationCount(int characteristicVerificationCount)
    {
        this.characteristicVerificationCount = characteristicVerificationCount;
    }

    public int getCharacteristicAccurateCount()
    {
        return characteristicAccurateCount;
    }

    public void setCharacteristicAccurateCount(int characteristicAccurateCount)
    {
        this.characteristicAccurateCount = characteristicAccurateCount;
    }

    public int getCharacteristicIncorrectCount()
    {
        return characteristicDataCount - characteristicAccurateCount;
    }

    public int getCharacteristicValidCount()
    {
        return characteristicValidCount;
    }

    public void setCharacteristicValidCount(int characteristicValidCount)
    {
        this.characteristicValidCount = characteristicValidCount;
    }

    public int getCharacteristicInvalidCount()
    {
        return characteristicDataCount - characteristicValidCount;
    }

    public int getNonCharacteristicCount()
    {
        return propertyCount - characteristicCount;
    }

    public int getNonCharacteristicDataCount()
    {
        return propertyDataCount - characteristicDataCount;
    }

    public int getNonCharacteristicAccurateCount()
    {
        return propertyAccurateCount - characteristicAccurateCount;
    }

    public int getNonCharacteristicIncorrectCount()
    {
        return getPropertyIncorrectCount() - getCharacteristicIncorrectCount();
    }

    public int getNonCharacteristicValidCount()
    {
        return propertyValidCount - characteristicValidCount;
    }

    public int getNonCharacteristicInvalidCount()
    {
        return getPropertyInvalidCount() - getCharacteristicInvalidCount();
    }

    @Override
    public String toString()
    {
        return "T8DataQualityMetrics{" + "rootRecordCount=" + recordCount + ", propertyCount=" + propertyCount + ", propertyDataCount=" + propertyDataCount + ", propertyValidCount=" + propertyValidCount + ", propertyVerificationCount=" + propertyVerificationCount + ", propertyAccurateCount=" + propertyAccurateCount + ", characteristicCount=" + characteristicCount + ", characteristicDataCount=" + characteristicDataCount + ", characteristicValidCount=" + characteristicValidCount + ", characteristicVerificationCount=" + characteristicVerificationCount + ", characteristicAccurateCount=" + characteristicAccurateCount + '}';
    }
}
