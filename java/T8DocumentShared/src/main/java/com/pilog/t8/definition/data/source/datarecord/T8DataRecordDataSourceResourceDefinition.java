package com.pilog.t8.definition.data.source.datarecord;

/**
 *
 * @author Pieter Strydom
 */
public class T8DataRecordDataSourceResourceDefinition extends T8DataRecordDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATA_RECORD_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8DataRecordDataSourceResourceDefinition(String identifier)
    {
        super(identifier);
    }
}