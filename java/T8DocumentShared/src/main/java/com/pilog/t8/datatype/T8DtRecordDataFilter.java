package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterSerializer;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;

/**
 * @author Bouwer du Preez
 */
public class T8DtRecordDataFilter extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@RECORD_DATA_FILTER";

    public T8DtRecordDataFilter()
    {
    }

    public T8DtRecordDataFilter(T8DefinitionManager context)
    {
    }

    public T8DtRecordDataFilter(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFilter.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        return new T8RecordFilterSerializer().serializeDataFilter((T8DataFilter)object);
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        try
        {
            return new T8RecordFilterSerializer().deserializeDataFilter(jsonValue);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while deserializing data filter.", e);
        }
    }
}
