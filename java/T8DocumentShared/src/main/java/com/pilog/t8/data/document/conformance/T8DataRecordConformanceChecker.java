package com.pilog.t8.data.document.conformance;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError.ConformanceErrorType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.IntegerRequirement;
import com.pilog.t8.data.document.datarequirement.value.RealRequirement;
import com.pilog.t8.data.document.datarequirement.value.StringRequirement;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.utilities.strings.Strings;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.text.MaskFormatter;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordConformanceChecker
{
    private final T8DataRecordValueStringGenerator valueStringGenerator;

    public T8DataRecordConformanceChecker(T8DataRecordValueStringGenerator fftValueStringGenerator)
    {
        this.valueStringGenerator = fftValueStringGenerator;
        this.valueStringGenerator.setIncludeUnresolvedTerms(true);
    }

    public List<T8DataRecordConformanceError> moveInvalidDataToFFT(DataRecord dataRecord)
    {
        List<T8DataRecordConformanceError> conformanceErrors;

        // Create a list for holding all conformance errors.
        conformanceErrors = new ArrayList<>();
        for (RecordSection recordSection : dataRecord.getRecordSections())
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue != null)
                {
                    ValueRequirement valueRequirement;
                    RequirementType requirementType;

                    valueRequirement = recordValue.getValueRequirement();
                    requirementType = valueRequirement.getRequirementType();
                    if (requirementType == RequirementType.COMPOSITE_TYPE)
                    {
                        for (RecordValue fieldValue : recordValue.getSubValues())
                        {
                            List<T8DataRecordConformanceError> validationErrors;

                            // Validate the value and if it is found to be invalid, append its content to the FFT of the property.
                            validationErrors = validateRecordValue(fieldValue);
                            if (validationErrors.size() > 0)
                            {
                                // Move property value to FFT.
                                addRecordValueToPropertyFFT(fieldValue);

                                // Remove the field from the composite type.
                                recordValue.removeSubValue(fieldValue);
                                conformanceErrors.addAll(validationErrors);
                            }
                        }
                    }
                    else
                    {
                        List<T8DataRecordConformanceError> validationErrors;

                        // Validate the value and if it is found to be invalid, append its content to the FFT of the property.
                        validationErrors = validateRecordValue(recordValue);
                        if (validationErrors.size() > 0)
                        {
                            // Move property value to FFT.
                            addRecordValueToPropertyFFT(recordValue);

                            // Remove the existing property value.
                            recordProperty.setRecordValue(null);
                            conformanceErrors.addAll(validationErrors);
                        }
                    }
                }
            }
        }

        return conformanceErrors;
    }

    public List<T8DataRecordConformanceError> validateDataRecord(DataRecord dataRecord)
    {
        List<T8DataRecordConformanceError> conformanceErrors;

        // Create a list for holding all conformance errors.
        conformanceErrors = new ArrayList<T8DataRecordConformanceError>();

        // Check all available record values for conformance to the data requirement.
       for (RecordSection recordSection : dataRecord.getRecordSections())
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue != null)
                {
                    conformanceErrors.addAll(validateRecordValue(recordValue));
                }
            }
        }

        // Check that all requirement properties have values.
        for (SectionRequirement classRequirement : dataRecord.getDataRequirement().getSectionRequirements())
        {
            for (PropertyRequirement propertyRequirement : classRequirement.getPropertyRequirements())
            {
                if (propertyRequirement.isCharacteristic())
                {
                    RecordValue recordValue;
                    String classID;
                    String propertyID;

                    classID = classRequirement.getConceptID();
                    propertyID = propertyRequirement.getConceptID();
                    recordValue = dataRecord.getRecordValue(propertyID);
                    if (recordValue == null)
                    {
                        String drInstanceID;
                        String drID;
                        String sectionID;

                        drInstanceID = dataRecord.getDataRequirementInstance().getConceptID();
                        drID = dataRecord.getDataRequirement().getConceptID();
                        sectionID = propertyRequirement.getParentSectionRequirement().getConceptID();
                        conformanceErrors.add(new T8DataRecordConformanceError(ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, dataRecord.getID(), drInstanceID, drID, classID, sectionID, propertyID, null, "Required property value not found."));
                    }
                }
            }
        }

        return conformanceErrors;
    }

    public List<T8DataRecordConformanceError> validateRecordProperty(RecordProperty recordProperty)
    {
        RecordValue recordValue;

        recordValue = recordProperty.getRecordValue();
        if (recordValue != null)
        {
            return validateRecordValue(recordValue);
        }
        else return new ArrayList<T8DataRecordConformanceError>();
    }

    public List<T8DataRecordConformanceError> validateRecordValue(RecordValue recordValue)
    {
        List<T8DataRecordConformanceError> errors;
        ValueRequirement valueRequirement;
        RequirementType requirementType;
        String value;

        // Get all the inputs required for the validation.
        errors = new ArrayList<T8DataRecordConformanceError>();
        valueRequirement = recordValue.getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        value = recordValue.getValue();

        // Check the data type of the value and validate it accordingly.
        if (RequirementType.BOOLEAN_TYPE.equals(requirementType))
        {
            if (value != null)
            {
                if ((!value.equalsIgnoreCase("true")) && (!value.equalsIgnoreCase("false")))
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "Invalid Boolean value.  Valid values are: {true,false}."));
                }
            }
        }
        else if (RequirementType.STRING_TYPE.equals(requirementType))
        {
            if (value != null)
            {
                StringRequirement stringRequirement;
                String formatPattern;
                String maskPattern;
                int maximumLength;

                stringRequirement = (StringRequirement)valueRequirement;

                // Check the string format.
                formatPattern = stringRequirement.getFormatPattern();
                if (formatPattern != null)
                {
                    if (!value.matches(formatPattern))
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "String value does not comply with required format.  Format pattern: " + formatPattern));
                    }
                }

                // Check the mask.
                maskPattern = stringRequirement.getMask();
                if (maskPattern != null)
                {
                    String maskedValue;

                    maskedValue = maskString(value, maskPattern);
                    if (!value.equals(maskedValue))
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "String value does not comply with required mask.  Mask pattern: " + maskPattern));
                    }
                }

                // Check maximum length if specified.
                maximumLength = stringRequirement.getMaximumStringLength();
                if (maximumLength > -1)
                {
                    if (value.length() > maximumLength)
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "String value exceeds maximum length specification.  Maximum Length: " + maximumLength));
                    }
                }
            }
        }
        else if (RequirementType.INTEGER_TYPE.equals(requirementType))
        {
            if (value != null)
            {
                if (Strings.isInteger(value))
                {
                    IntegerRequirement integerRequirement;
                    BigInteger minimumValue;
                    BigInteger maximumValue;
                    String formatPattern;
                    BigInteger integerValue;

                    // Get the interger value.
                    integerValue = new BigInteger(value);
                    integerRequirement = (IntegerRequirement)valueRequirement;

                    // Check the format requirement.
                    formatPattern = integerRequirement.getFormatPattern();
                    if (formatPattern != null)
                    {
                        DecimalFormat integerFormat;

                        integerFormat = new DecimalFormat(formatPattern);
                        if (!integerFormat.format(integerValue).equals(value))
                        {
                            errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "Integer value does not comply with required format.  Format pattern: " + formatPattern));
                        }
                    }

                    // Check the minimum value requirement.
                    minimumValue = integerRequirement.getMinimumValue();
                    if ((minimumValue != null) && (integerValue.compareTo(minimumValue) < 0))
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "Integer value is less than specified minimum: " + minimumValue));
                    }

                    // Check the maximum value requirement.
                    maximumValue = integerRequirement.getMaximumValue();
                    if ((maximumValue != null) && (integerValue.compareTo(maximumValue) > 0))
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "Integer value is greater than specified maximum: " + maximumValue));
                    }
                }
                else
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "Value is not an integer type: " + value));
                }
            }
        }
        else if (RequirementType.REAL_TYPE.equals(requirementType))
        {
            if (value != null)
            {
                if (Strings.isDouble(value))
                {
                    RealRequirement realRequirement;
                    BigDecimal minimumValue;
                    BigDecimal maximumValue;
                    BigDecimal realValue;
                    String formatPattern;

                    // Get the real value.
                    realValue = new BigDecimal(value);
                    realRequirement = (RealRequirement)valueRequirement;

                    // Check the format requirement.
                    formatPattern = realRequirement.getFormatPattern();
                    if (formatPattern != null)
                    {
                        DecimalFormat decimalFormat;

                        decimalFormat = new DecimalFormat(formatPattern);
                        if (!decimalFormat.format(realValue).equals(value))
                        {
                            errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "Real value does not comply with required format.  Format pattern: " + formatPattern));
                        }
                    }

                    // Check the minimum value requirement.
                    minimumValue = realRequirement.getMinimumValue();
                    if ((minimumValue != null) && (realValue.compareTo(minimumValue) < 0))
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "Real value is less than specified minimum: " + minimumValue));
                    }

                    // Check the maximum value requirement.
                    maximumValue = realRequirement.getMaximumValue();
                    if ((maximumValue != null) && (realValue.compareTo(maximumValue) > 0))
                    {
                        errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "Real value is greater than specified maximum: " + maximumValue));
                    }
                }
                else
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA_FORMAT, recordValue, "Value is not Real Type: " + value));
                }
            }
        }
        else if (RequirementType.MEASURED_NUMBER.equals(requirementType))
        {
            if (value != null)
            {
                RecordValue subValue;

                // Check that a numeric value is specified.
                subValue = recordValue.getDescendantValue(RequirementType.INTEGER_TYPE, null, null);
                if (subValue == null) subValue = recordValue.getDescendantValue(RequirementType.REAL_TYPE, null, null);
                if (subValue == null) errors.add(new T8DataRecordConformanceError(ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, recordValue, "No Numeric Value specified for Measure Number Type."));

                // Check that a UOM is specified.
                subValue = recordValue.getDescendantValue(RequirementType.UNIT_OF_MEASURE, null, null);
                if (subValue != null)
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, recordValue, "No UOM specified for Measure Number Type."));
                }
            }
        }
        else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
        {
            ControlledConcept controlledConcept;
            String conceptId;

            controlledConcept = (ControlledConcept)recordValue;
            conceptId = controlledConcept.getConceptId();
            if (conceptId != null)
            {
                if (!T8IdentifierUtilities.isConceptID(value))
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "String value is not a valid concept ID."));
                }
            }
            else if (!controlledConcept.getRequirement().isAllowNewValue())
            {
                if (!Strings.isNullOrEmpty(controlledConcept.getTerm()))
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "Term " + controlledConcept.getTerm() + " does not match a valid concept and no new values are allowed."));
                }
                else if (!Strings.isNullOrEmpty(controlledConcept.getCode()))
                {
                    errors.add(new T8DataRecordConformanceError(ConformanceErrorType.INVALID_DATA, recordValue, "Code " + controlledConcept.getCode() + " does not match a valid concept and no new values are allowed."));
                }
            }
        }

        // Now validate each of the sub-values.
        for (RecordValue subValue : recordValue.getSubValues())
        {
            errors.addAll(validateRecordValue(subValue));
        }

        // Return the total list of errors.
        return errors;
    }

    public void addRecordValueToPropertyFFT(RecordValue recordValue)
    {
        StringBuilder value;

        value = valueStringGenerator.generateValueString(recordValue);
        if ((value != null) && (value.length() > 0))
        {
            RecordProperty recordProperty;
            String propertyFFT;

            recordProperty = recordValue.getParentRecordProperty();
            propertyFFT = recordProperty.getFFT();
            if (!Strings.isNullOrEmpty(propertyFFT))
            {
                String valueString;

                valueString = value.toString();
                valueString = valueString.trim();
                propertyFFT = propertyFFT.trim();

                propertyFFT = propertyFFT + "," + valueString;
                recordProperty.setFFT(propertyFFT);
            }
            else // If the property FFT is null, just set the value String as the new FFT.
            {
                String valueString;

                valueString = value.toString();
                valueString = valueString.trim();
                recordProperty.setFFT(valueString);
            }
        }
    }

    private static String maskString(String string, String mask)
    {
        try
        {
            MaskFormatter formatter;

            formatter = new MaskFormatter(mask);
            formatter.setValueContainsLiteralCharacters(true);
            return formatter.valueToString(string);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

}
