package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;

/**
 * @author Bouwer du Preez
 */
public interface MeasurableValue extends Value
{
    public String getUomId();
    public RecordValue setUomId(String conceptID);
    public UnitOfMeasure getUom();
    public RecordValue clearUom();

    public String getQomId();
    public RecordValue setQomId(String conceptID);
    public QualifierOfMeasure getQom();
    public RecordValue clearQom();
}
