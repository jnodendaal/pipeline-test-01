package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataFileSaveResult extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_FILE_SAVE_RESULT";

    public T8DtDataFileSaveResult() {}

    public T8DtDataFileSaveResult(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataFileSaveResult.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8DtDataFileValidationReport dtValidationReport;
            T8DataFileSaveResult result;
            JsonObject jsonObject;

            dtValidationReport = new T8DtDataFileValidationReport(null);

            result = (T8DataFileSaveResult)object;
            jsonObject = new JsonObject();
            jsonObject.add("success", result.isSaved());
            jsonObject.add("validationReport", dtValidationReport.serialize(result.getValidationReport()));
            return jsonObject;
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8DtDataFileValidationReport dtValidationReport;
            T8DataFileSaveResult result;
            JsonObject object;

            dtValidationReport = new T8DtDataFileValidationReport(null);

            object = (JsonObject)jsonValue;
            result = new T8DataFileSaveResult(null, null, null, object.getBoolean("success"), null);
            result.setValidationReport(dtValidationReport.deserialize(object.getJsonObject("validationReport")));
            return result;
        }
        else return null;
    }
}
