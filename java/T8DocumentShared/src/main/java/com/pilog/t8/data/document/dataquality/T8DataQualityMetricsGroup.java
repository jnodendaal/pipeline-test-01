package com.pilog.t8.data.document.dataquality;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataQualityMetricsGroup implements Serializable
{
    private final String id;
    private String code;
    private String term;
    private String definition;
    private String ocId;
    private String ocCode;
    private String ocTerm;
    private String ocDefinition;
    private T8DataQualityMetrics metrics;

    public T8DataQualityMetricsGroup(String groupId)
    {
        this.id = groupId;
    }

    public String getId()
    {
        return id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getOcId()
    {
        return ocId;
    }

    public void setOcId(String ocId)
    {
        this.ocId = ocId;
    }

    public String getOcCode()
    {
        return ocCode;
    }

    public void setOcCode(String ocCode)
    {
        this.ocCode = ocCode;
    }

    public String getOcTerm()
    {
        return ocTerm;
    }

    public void setOcTerm(String ocTerm)
    {
        this.ocTerm = ocTerm;
    }

    public String getOcDefinition()
    {
        return ocDefinition;
    }

    public void setOcDefinition(String ocDefinition)
    {
        this.ocDefinition = ocDefinition;
    }

    public T8DataQualityMetrics getMetrics()
    {
        return metrics;
    }

    public void setMetrics(T8DataQualityMetrics metrics)
    {
        this.metrics = metrics;
    }

    @Override
    public String toString()
    {
        return "T8DataQualityMetricsGroup{" + "id=" + id + ", code=" + code + ", term=" + term + ", definition=" + definition + ", metrics=" + metrics + '}';
    }
}
