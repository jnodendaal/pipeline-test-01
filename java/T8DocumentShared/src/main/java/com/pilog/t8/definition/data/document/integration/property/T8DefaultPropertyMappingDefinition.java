package com.pilog.t8.definition.data.document.integration.property;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.definition.data.document.integration.T8DataRecordPropertyMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DefaultPropertyMappingDefinition extends T8DataRecordPropertyMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_PROPERTY";
    public static final String DISPLAY_NAME = "Default Property Mapping";
    public static final String DESCRIPTION = "A Data Record property mapping that defines how properties for this record will be mapped";

    public enum Datum
    {
        INTERFACE_TERM,
        ENABLED_EXPRESSION,
        DEFAULT_VALUE_EXPRESSION,
        PROPERTY_ID,
        MAPPING_TYPE,
        DATE_VALUE,
        DATE_PATTERN
    };
    // -------- Definition Meta-Data -------- //

    public static enum MappingTypes
    {
        CODE,
        CONCEPT,
        TERM,
        ABBREVIATION,
        DEFINITION,
        VALUE_STRING
    }

    public T8DefaultPropertyMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INTERFACE_TERM.toString(), "Interface Term", "The term that will be assigned to this property on the interface."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_EXPRESSION.toString(), "Enabled Expression", "The xpression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DEFAULT_VALUE_EXPRESSION.toString(), "Default Value Expression", "The expression that will be evaluated to determine the default value."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.PROPERTY_ID.toString(), "Property ID", "The property ID to be mapped."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MAPPING_TYPE.toString(), "Mapping Type", "The type of mapping that will be performed on this property", MappingTypes.VALUE_STRING.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.DATE_VALUE.toString(), "Date Value", "Will convert the timestamp value to a date."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATE_PATTERN.toString(), "Date Pattern", "If the value is a date then it willl use the specified date pattern.", "dd.MM.yyyy"));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.MAPPING_TYPE.toString().equals(datumIdentifier)) return createStringOptions(MappingTypes.values());
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <T extends Object> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.property.mapping.DefaultPropertyMapping").getConstructor(T8Context.class, T8DefaultPropertyMappingDefinition.class);
            return (DataRecordIntegrationPropertyMapper<T>) constructor.newInstance(context, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new property mapper instance", ex);
            return null;
        }
    }

    public String getInterfaceTerm()
    {
        return (String)getDefinitionDatum(Datum.INTERFACE_TERM.toString());
    }

    public void setInterfaceTerm(String term)
    {
        setDefinitionDatum(Datum.INTERFACE_TERM.toString(), term);
    }

    public String getEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.ENABLED_EXPRESSION.toString());
    }

    public void setEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_EXPRESSION.toString(), expression);
    }

    public String getDefaultValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString());
    }

    public void setDefaultValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString(), expression);
    }

    public String getPropertyID()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_ID.toString());
    }

    public void setPropertyID(String propertyID)
    {
        setDefinitionDatum(Datum.PROPERTY_ID.toString(), propertyID);
    }

    public MappingTypes getMappingType()
    {
        return MappingTypes.valueOf((String)getDefinitionDatum(Datum.MAPPING_TYPE.toString()));
    }

    public void setMappingType(MappingTypes type)
    {
        setDefinitionDatum(Datum.MAPPING_TYPE.toString(), type.toString());
    }

    public Boolean isDateValue()
    {
        Boolean enabled;

        enabled = (Boolean) getDefinitionDatum(Datum.DATE_VALUE.toString());
        return ((enabled != null) && enabled);
    }

    public void setDateValue(Boolean dateValue)
    {
        setDefinitionDatum(Datum.DATE_VALUE.toString(), dateValue);
    }

    public String getDatePattern()
    {
        return (String) getDefinitionDatum(Datum.DATE_PATTERN.toString());
    }

    public void setDatePattern(String datePattern)
    {
        setDefinitionDatum(Datum.DATE_PATTERN.toString(), datePattern);
    }
}
