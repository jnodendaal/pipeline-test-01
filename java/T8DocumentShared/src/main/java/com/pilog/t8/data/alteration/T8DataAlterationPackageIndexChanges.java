package com.pilog.t8.data.alteration;

import com.pilog.t8.data.alteration.T8DataAlterationPackageIndexChange.IndexChangeType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationPackageIndexChanges implements Serializable
{
    private final List<T8DataAlterationPackageIndexChange> changes;

    public T8DataAlterationPackageIndexChanges()
    {
        this.changes = new ArrayList<T8DataAlterationPackageIndexChange>();
    }

    public List<T8DataAlterationPackageIndexChange> getChanges()
    {
        return new ArrayList<T8DataAlterationPackageIndexChange>(changes);
    }

    public void addChange(T8DataAlterationPackageIndexChange change)
    {
        changes.add(change);
    }

    public void addEntryUpdate(T8DataAlterationPackageIndexEntry entry, int startStep, int stepChange)
    {
        changes.add(new T8DataAlterationPackageIndexChange(IndexChangeType.UPDATE, entry, startStep, stepChange));
    }

    public void addEntryDeletion(T8DataAlterationPackageIndexEntry entry)
    {
        changes.add(new T8DataAlterationPackageIndexChange(IndexChangeType.DELETE, entry, 0, 0));
    }

    public void addEntryInsert(T8DataAlterationPackageIndexEntry entry)
    {
        changes.add(new T8DataAlterationPackageIndexChange(IndexChangeType.INSERT, entry, 0, 0));
    }
}
