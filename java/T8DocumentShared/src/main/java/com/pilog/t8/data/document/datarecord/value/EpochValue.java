/**
 * Created on 16 Mar 2016, 12:47:23 PM
 */
package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.utilities.strings.Strings;

/**
 * A records value which is representable by a {@code long} value defining the
 * milliseconds since January 1, 1970, 00:00:00 GMT.
 *
 * @author Gavin Boshoff
 */
public abstract class EpochValue extends RecordValue
{
    public EpochValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public Long getMilliseconds()
    {
        if (!Strings.isNullOrEmpty(this.getValue()))
        {
            return Long.parseLong(this.getValue());
        } else return null;
    }
}