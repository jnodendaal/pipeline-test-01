package com.pilog.t8.data.document.datarecord.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordChangeListener extends EventListener
{
    /**
     * Last event fired at the end of an adjustment sequence.
     *
     * @param event The {@code T8DataRecordChangedEvent} containing the details
     *      on the event which has occurred
     */
    public void dataRecordChanged(T8DataRecordChangedEvent event);
    public void recordValueChanged(T8RecordValueChangedEvent event);
    public void recordValueStructureChanged(T8RecordValueStructureChangedEvent event);
    public void recordPropertyAdded(T8RecordPropertyAddedEvent event);
    public void recordPropertyValueChanged(T8RecordPropertyValueChangedEvent event);
}
