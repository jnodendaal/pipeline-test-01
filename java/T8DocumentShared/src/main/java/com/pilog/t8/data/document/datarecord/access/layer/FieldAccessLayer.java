package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.org.T8OrganizationStructure.OrganizationSetType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class FieldAccessLayer implements Serializable
{
    private final String fieldId;
    private boolean editable;
    private boolean visible;
    private boolean required;
    private boolean insertEnabled;
    private boolean updateEnabled;
    private boolean deleteEnabled;
    private Set<String> filterConceptIDSet;
    private Set<String> filterConceptGroupIDSet;
    private OrganizationSetType filterOrganizationSetType;
    private final Map<Integer, OrganizationSetType> organizationRestrictions;;
    private PropertyAccessLayer parentAccessLayer;
    private ValueAccessLayer valueAccessLayer;

    public FieldAccessLayer(String fieldId)
    {
        this.fieldId = fieldId;
        this.editable = true;
        this.visible = true;
        this.required = false;
        this.insertEnabled = true;
        this.updateEnabled = true;
        this.deleteEnabled = true;
        this.filterOrganizationSetType = OrganizationSetType.LINEAGE;
        this.organizationRestrictions = new HashMap<Integer, OrganizationSetType>();
    }

    void setParentAccessLayer(PropertyAccessLayer propertyAccessLayer)
    {
        this.parentAccessLayer = propertyAccessLayer;
    }

    public FieldAccessLayer copy()
    {
        FieldAccessLayer copy;

        copy = new FieldAccessLayer(fieldId);
        copy.setEditable(editable);
        copy.setVisible(visible);
        copy.setRequired(required);
        copy.setInsertEnabled(insertEnabled);
        copy.setUpdateEnabled(updateEnabled);
        copy.setDeleteEnabled(deleteEnabled);
        copy.setFilterOrganizationSetType(filterOrganizationSetType);
        copy.setFilterConceptIDSet(filterConceptIDSet);
        copy.setFilterConceptGroupIDSet(filterConceptGroupIDSet);

        if (valueAccessLayer != null)
        {
            copy.setValueAccessLayer(valueAccessLayer.copy());
        }

        return copy;
    }

    public void setAccess(FieldAccessLayer newAccess)
    {
        setEditable(newAccess.isEditable());
        setVisible(newAccess.isVisible());
        setRequired(newAccess.isRequired());
        setInsertEnabled(newAccess.isInsertEnabled());
        setUpdateEnabled(newAccess.isUpdateEnabled());
        setDeleteEnabled(newAccess.isDeleteEnabled());
        setFilterOrganizationSetType(newAccess.getFilterOrganizationSetType());
        setFilterConceptIDSet(newAccess.getFilterConceptIDSet());
        setFilterConceptGroupIDSet(newAccess.getFilterConceptGroupIDSet());

        if (valueAccessLayer != null)
        {
            ValueAccessLayer newValueAccess;

            newValueAccess = newAccess.getValueAccessLayer();
            if (newValueAccess != null)
            {
                valueAccessLayer.setAccess(newValueAccess);
            }
        }
    }

    public PropertyAccessLayer getParentAccessLayer()
    {
        return parentAccessLayer;
    }

    public DataRecord getParentDataRecord()
    {
        return parentAccessLayer != null ? parentAccessLayer.getParentDataRecord() : null;
    }

    public String getFieldID()
    {
        return fieldId;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setEditable(boolean editable)
    {
        // Set the editability of this access layer.
        this.editable = editable;

        // Propagate the editability to parent access layers.
        if (editable) parentAccessLayer.setPathEditable();
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        // Set the visibility on this access layer.
        this.visible = visible;

        // Propagate the visibility to parent access layers.
        if (visible) parentAccessLayer.setPathVisible();
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public boolean isInsertEnabled()
    {
        return insertEnabled;
    }

    public void setInsertEnabled(boolean insertEnabled)
    {
        this.insertEnabled = insertEnabled;
    }

    public boolean isUpdateEnabled()
    {
        return updateEnabled;
    }

    public void setUpdateEnabled(boolean updateEnabled)
    {
        this.updateEnabled = updateEnabled;
    }

    public boolean isDeleteEnabled()
    {
        return deleteEnabled;
    }

    public void setDeleteEnabled(boolean deleteEnabled)
    {
        this.deleteEnabled = deleteEnabled;
    }

    public ValueAccessLayer getValueAccessLayer()
    {
        return valueAccessLayer;
    }

    public void setValueAccessLayer(ValueAccessLayer valueAccessLayer)
    {
        // Clear the parent references of the existing value access layer.
        if (this.valueAccessLayer != null)
        {
            this.valueAccessLayer.setParentFieldAccessLayer(null);
            this.valueAccessLayer.setParentPropertyAccessLayer(null);
        }

        // Set the new value access layer and its parent references.
        this.valueAccessLayer = valueAccessLayer;
        if (this.valueAccessLayer != null)
        {
            this.valueAccessLayer.setParentFieldAccessLayer(this);
            this.valueAccessLayer.setParentPropertyAccessLayer(parentAccessLayer);
        }
    }

    public Set<String> getFilterConceptIDSet()
    {
        return filterConceptIDSet;
    }

    public void setFilterConceptIDSet(Set<String> filterConceptIDSet)
    {
        this.filterConceptIDSet = filterConceptIDSet;
    }

    public Set<String> getFilterConceptGroupIDSet()
    {
        return filterConceptGroupIDSet;
    }

    public void setFilterConceptGroupIDSet(Set<String> filterConceptGroupIDSet)
    {
        this.filterConceptGroupIDSet = filterConceptGroupIDSet;
    }

    public OrganizationSetType getFilterOrganizationSetType()
    {
        return filterOrganizationSetType;
    }

    public void setFilterOrganizationSetType(OrganizationSetType organizationFilterSet)
    {
        this.filterOrganizationSetType = organizationFilterSet;
    }

    public void setFilterOrganizationSetType(String organizationFilterSet)
    {
        this.filterOrganizationSetType = OrganizationSetType.valueOf(organizationFilterSet);
    }

    public Map<Integer, OrganizationSetType> getOrganizationRestrictions()
    {
        return new HashMap<Integer, OrganizationSetType>(organizationRestrictions);
    }

    public OrganizationSetType getOrganizationRestriction(Integer level)
    {
        return organizationRestrictions.get(level);
    }

    public void setOrganizationRestriction(Integer level, OrganizationSetType setType)
    {
        this.organizationRestrictions.put(level, setType);
    }

    public void setOrganizationRestriction(Integer level, String setType)
    {
        this.organizationRestrictions.put(level, OrganizationSetType.valueOf(setType));
    }

    public boolean isFieldAccessEquivalent(FieldAccessLayer fieldAccess)
    {
        if (!Objects.equals(this.editable, fieldAccess.isEditable())) return false;
        else if (!Objects.equals(this.visible, fieldAccess.isVisible())) return false;
        else if (!Objects.equals(this.required, fieldAccess.isRequired())) return false;
        else if (!Objects.equals(this.insertEnabled, fieldAccess.isInsertEnabled())) return false;
        else if (!Objects.equals(this.updateEnabled, fieldAccess.isUpdateEnabled())) return false;
        else if (!Objects.equals(this.deleteEnabled, fieldAccess.isDeleteEnabled())) return false;
        else return true;
    }

    @Override
    public String toString()
    {
        return "FieldAccessLayer{" + "fieldID=" + fieldId + ", editable=" + editable + ", visible=" + visible + ", required=" + required + ", insertEnabled=" + insertEnabled + ", updateEnabled=" + updateEnabled + ", deleteEnabled=" + deleteEnabled + ", filterConceptIDSet=" + filterConceptIDSet + ", filterConceptGroupIDSet=" + filterConceptGroupIDSet + '}';
    }
}
