package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationService;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8IntegrationResponseDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTEGRATION_OPERATION_RESPONSE";
    public static final String TYPE_IDENTIFIER = "@DT_INTEGRATION_OPERATION_RESPONSE";
    public static final String DISPLAY_NAME = "Integration Operation Response";
    public static final String DESCRIPTION = "An integration operation response operation";
    public static final String STORAGE_PATH = "/integration_services";
    public static final String IDENTIFIER_PREFIX = "XFACE_RESP_";

    public enum Datum {
        ORGANIZATION_ID,
        OPERATION_IDENTIFIER,
        OPERATION_RESPONSE_PARAMETER_IDENTIFIER
    };
    // -------- Definition Meta-Data -------- //

    public T8IntegrationResponseDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.ORGANIZATION_ID.toString(), "Organization Identifier", "The organization identifier for which the session context will be created."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.OPERATION_IDENTIFIER.toString(), "Operation Identifier", "The operation that will be called when the integration response web service is hit."));
        datumTypes.add(T8DefinitionDatumType.identifierType(Datum.OPERATION_RESPONSE_PARAMETER_IDENTIFIER.toString(), "Response Parameter Identifier", "The parameter identifier of the parameter on the operation that will recieve the response list."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.OPERATION_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServerOperationDefinition.GROUP_IDENTIFIER));
        else if(Datum.OPERATION_RESPONSE_PARAMETER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            String operationId;

            operationId = getOperationIdentifier();
            if(!Strings.isNullOrEmpty(operationId))
            {
                T8ServerOperationDefinition operationDefinition;

                operationDefinition = (T8ServerOperationDefinition)definitionContext.getRawDefinition(getRootProjectId(), operationId);

                return createPublicIdentifierOptionsFromDefinitions(operationDefinition.getInputParameterDefinitions());
            } else return new ArrayList<>();
        }
        else return null;
    }

    public <T> DataRecordIntegrationService<T> getNewIntegrationServiceInstance(T8ServerContext serverContex, T8SessionContext sessionContext) throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.data.document.integration.T8IntegrationResponseDefinition").getConstructor(T8ServerContext.class, T8SessionContext.class, T8IntegrationResponseDefinition.class);
        return (DataRecordIntegrationService<T>)constructor.newInstance(serverContex, sessionContext, this);
    }

    public String getOrganizationIdentifier()
    {
        return getDefinitionDatum(Datum.ORGANIZATION_ID);
    }

    public void setOrganizationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ORGANIZATION_ID, identifier);
    }

    public String getOperationIdentifier()
    {
        return getDefinitionDatum(Datum.OPERATION_IDENTIFIER);
    }

    public void setOperationIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OPERATION_IDENTIFIER, identifier);
    }

    public String getResponseParameterIdentifier()
    {
        return getDefinitionDatum(Datum.OPERATION_RESPONSE_PARAMETER_IDENTIFIER);
    }

    public void setResponseParameterIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.OPERATION_RESPONSE_PARAMETER_IDENTIFIER, identifier);
    }
}
