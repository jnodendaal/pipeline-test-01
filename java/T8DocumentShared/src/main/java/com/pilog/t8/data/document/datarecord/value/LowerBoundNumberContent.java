package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.strings.Strings;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class LowerBoundNumberContent extends ValueContent
{
    private String value;

    public LowerBoundNumberContent()
    {
        super(RequirementType.LOWER_BOUND_NUMBER);
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public boolean hasContent()
    {
        return Strings.trimToNull(this.value) != null;
    }

    public void setContent(LowerBoundNumberContent content)
    {
        if (content != null)
        {
            this.value = content.getValue();
        }
        else
        {
            clear();
        }
    }

    public void clear()
    {
        this.value = null;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof LowerBoundNumberContent)
        {
            LowerBoundNumberContent toNumberContent;

            toNumberContent = (LowerBoundNumberContent)toContent;
            return Objects.equals(value, toNumberContent.getValue());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof LowerBoundNumberContent)
        {
            LowerBoundNumberContent toNumberContent;

            toNumberContent = (LowerBoundNumberContent)toContent;
            return Objects.equals(value, toNumberContent.getValue());
        }
        else return false;
    }
}
