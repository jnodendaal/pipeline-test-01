package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class DateTimeRequirement extends ValueRequirement
{
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    public DateTimeRequirement()
    {
        super(RequirementType.DATE_TIME_TYPE);
    }

    public String getDateTimeFormat()
    {
        String format;

        format = (String)this.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.toString());
        return format != null ? format : DEFAULT_DATE_TIME_FORMAT;
    }
}
