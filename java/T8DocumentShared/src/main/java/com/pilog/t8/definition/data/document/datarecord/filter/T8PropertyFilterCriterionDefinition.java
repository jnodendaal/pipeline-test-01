package com.pilog.t8.definition.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.filter.T8PropertyFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8ValueFilterCriterion;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PropertyFilterCriterionDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RECORD_FILTER_PROPERTY_CRITERION";
    public static final String GROUP_IDENTIFIER = "@DT_RECORD_FILTER_PROPERTY_CRITERION";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Property Filter Criterion";
    public static final String DESCRIPTION = "A filter criterion applicable to a specific Data Requirement property.";
    private enum Datum
    {
        PROPERTY_ID,
        FIELD_ID,
        DATA_TYPE,
        OPERATOR,
        FILTER_VALUE_EXPRESSION,
        ALLOW_REMOVAL
    };
    // -------- Definition Meta-Data -------- //

    public T8PropertyFilterCriterionDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.PROPERTY_ID.toString(), "Property Id", "The concept ID of the Data Requirement Property to which this criterion is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.FIELD_ID.toString(), "Field Id", "The concept ID of the Data Requirement Field to which this criterion is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DATA_TYPE.toString(), "Data Type", "The data type to include in the filter criterion.", null, T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.OPERATOR.toString(), "Operator", "The operator used by this criterion.", DataFilterOperator.EQUAL.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.FILTER_VALUE_EXPRESSION.toString(), "Filter Value Expresssion", "The expression that will be evaluated to determine the filter value of this criterion."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_REMOVAL.toString(), "Allow Removal", "If enabled, allows the removal of this criterion.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPERATOR.toString().equals(datumIdentifier)) return createStringOptions(DataFilterOperator.values());
        else if (Datum.DATA_TYPE.toString().equals(datumIdentifier))
        {
            List<String> stringValues;

            stringValues = new ArrayList();
            for (Object value : RequirementType.getRootDataTypes())
            {
                stringValues.add(value.toString());
            }

            return createStringOptions(stringValues, true, "Unspecified");
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public T8PropertyFilterCriterion getPropertyFilterCriterionInstance(T8Context context)
    {
        T8PropertyFilterCriterion criteria;

        criteria = new T8PropertyFilterCriterion(getPropertyID());
        criteria.setFieldId(getFieldID());
        criteria.addValueCriterion(T8ValueFilterCriterion.createCriterion(getDataType(), getOperator(), null));
        criteria.setAllowRemoval(isAllowRemoval());
        return criteria;
    }

    public String getPropertyID()
    {
        return (String)getDefinitionDatum(Datum.PROPERTY_ID.toString());
    }

    public void setPropertyID(String propertyID)
    {
        setDefinitionDatum(Datum.PROPERTY_ID.toString(), propertyID);
    }

    public String getFieldID()
    {
        return (String)getDefinitionDatum(Datum.FIELD_ID.toString());
    }

    public void setFieldID(String fieldID)
    {
        setDefinitionDatum(Datum.FIELD_ID.toString(), fieldID);
    }

    public RequirementType getDataType()
    {
        String value;

        value = (String)getDefinitionDatum(Datum.DATA_TYPE);
        return RequirementType.getRequirementType(value);
    }

    public void setDataType(RequirementType type)
    {
        setDefinitionDatum(Datum.DATA_TYPE, type != null ? type.toString() : null);
    }

    public DataFilterOperator getOperator()
    {
        String stringValue;

        stringValue = (String)getDefinitionDatum(Datum.OPERATOR.toString());
        if (Strings.isNullOrEmpty(stringValue)) return DataFilterOperator.EQUAL;
        else return DataFilterOperator.valueOf(stringValue);
    }

    public void setOperator(DataFilterOperator operator)
    {
        setDefinitionDatum(Datum.OPERATOR.toString(), operator.toString());
    }

    public String getFilterValueExpression()
    {
        return (String)getDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString());
    }

    public void setFilterValueExpression(String expression)
    {
        setDefinitionDatum(Datum.FILTER_VALUE_EXPRESSION.toString(), expression);
    }

    public boolean isAllowRemoval()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_REMOVAL.toString());
        return value != null && value;
    }

    public void setAllowRemoval(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_REMOVAL.toString(), false);
    }
}
