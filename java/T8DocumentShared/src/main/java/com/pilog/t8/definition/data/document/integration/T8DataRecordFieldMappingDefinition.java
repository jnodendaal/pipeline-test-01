package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationFieldMapper;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public abstract class T8DataRecordFieldMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_RECORD_FIELD";
    public static final String IDENTIFIER_PREFIX = "RP_MAP_";

    public enum Datum
    {
        INTERFACE_TERM,
        ENABLED_EXPRESSION,
        DEFAULT_VALUE_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //

    public T8DataRecordFieldMappingDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract <T extends Object> DataRecordIntegrationFieldMapper<T> createNewFieldMapperInstance(T8ServerContext serverContext, T8SessionContext sessionContext);

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.INTERFACE_TERM.toString(), "Interface Term", "The term that will be assigned to this field on the interface."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.ENABLED_EXPRESSION.toString(), "Enabled Expression", "The xpression that will be evaluated to determine if this mapping should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.DEFAULT_VALUE_EXPRESSION.toString(), "Defualt Value Expression", "The xpression that will be evaluated to determine the default value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return null;
    }

    public String getInterfaceTerm()
    {
        return (String)getDefinitionDatum(Datum.INTERFACE_TERM.toString());
    }

    public void setInterfaceTerm(String term)
    {
        setDefinitionDatum(Datum.INTERFACE_TERM.toString(), term);
    }

    public String getEnabledExpression()
    {
        return (String)getDefinitionDatum(Datum.ENABLED_EXPRESSION.toString());
    }

    public void setEnabledExpression(String expression)
    {
        setDefinitionDatum(Datum.ENABLED_EXPRESSION.toString(), expression);
    }

    public String getDefaultValueExpression()
    {
        return (String)getDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString());
    }

    public void setDefaultValueExpression(String expression)
    {
        setDefinitionDatum(Datum.DEFAULT_VALUE_EXPRESSION.toString(), expression);
    }
}
