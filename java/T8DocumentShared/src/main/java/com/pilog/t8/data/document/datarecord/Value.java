package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface Value
{
    public Value getSubValue(RequirementType requirementType, String conceptID, Integer index);
    public List<Value> getSubValues(RequirementType requirementType, String conceptID, Integer index);

    public Value getDescendantValue(RequirementType requirementType, String conceptID, Integer index);
    public List<Value> getDescendantValues(RequirementType requirementType, String conceptID, Integer index);

    /**
     * Adds the supplied value at the location identified by the supplied value
     * pointer.  If the value pointer pointer to an collection index that does
     * not yet exist, the collection will be filled with null values to
     * accommodate the specified index.
     * @param subValue The value to add.
     */
    public void setSubValue(Value subValue);

    /**
     * Removes the specified sub-value from this value.
     * @param subValue The sub-value to remove.
     * @return A Boolean flag to indicate if the specified value was found and
     * successfully removed.
     */
    public boolean removeSubValue(Value subValue);

    /**
     * Returns the number of sub-values in this value.
     * @return The number of sub-values in this value.
     */
    public int getSubValueCount();

    /**
     * Normalizes the content of the value, ensuring that it is formatted
     * according to standardized conventions and removing any redundant or empty
     * parts of the object.
     */
    public void normalize();

    /**
     * Returns a Boolean flag indicating whether or not the Value carries useful
     * information.  FFT content can be included if required.
     * @param includeFFT
     * @return
     */
    public boolean hasContent(boolean includeFFT);

    /**
     * Returns a Boolean value indicating whether or not the supplied value and
     * its descendant structure is equivalent to this value and its descendant
     * structure.
     * @param value The value to compare to this value.
     * @return A Boolean value indicating equivalence if true.
     */
    public boolean isEquivalentValue(Value value);
}
