package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class FieldContent extends ValueContent
{
    private String id;
    private String irdi;
    private ValueContent value;

    public FieldContent(String id)
    {
        super(RequirementType.FIELD_TYPE);
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public void setValue(ValueContent value)
    {
        this.value = value;
    }

    public ValueContent getValue()
    {
        return value;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof FieldContent)
        {
            FieldContent toFieldContent;

            toFieldContent = (FieldContent)toContent;
            if (!Objects.equals(id, toFieldContent.getId())) return false;
            else if (!Objects.equals(irdi, toFieldContent.getIrdi())) return false;
            else if (value == null) return toFieldContent.getValue() == null;
            else return value.isEqualTo(toFieldContent.getValue());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof FieldContent)
        {
            FieldContent toFieldContent;

            toFieldContent = (FieldContent)toContent;
            if (!Objects.equals(id, toFieldContent.getId())) return false;
            else if (!Objects.equals(irdi, toFieldContent.getIrdi())) return false;
            else if (value == null) return toFieldContent.getValue() == null;
            else return value.isEquivalentTo(toFieldContent.getValue());
        }
        else return false;
    }
}
