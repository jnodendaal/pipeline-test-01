package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface DataRecordProvider
{
    public String getRecordDrInstanceId(String recordId);

    public T8AttachmentDetails getAttachmentDetails(String attachmentId);
    public Map<String, T8AttachmentDetails> getRecordAttachmentDetails(String recordId);

    public DataRecord getDataRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendentRecords);
    public List<DataRecord> getDataRecords(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendentRecords);

    public DataRecord getDataFile(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings);
    public List<DataRecord> getDataFiles(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings);

    public DataRecord getDataFileByContentRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings);
    public List<DataRecord> getDataFilesByContentRecord(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings);
}
