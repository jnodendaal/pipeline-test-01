package com.pilog.t8.data.document.datarecord.attachment;

import com.pilog.t8.file.T8FileDetails;
import java.io.Serializable;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentValidationResult implements Serializable
{
    private final String attachmentID;
    private final Set<AttachmentValidationError> errors;
    private T8FileDetails attachmentDetails;

    public enum AttachmentValidationError{MISSING_FILE, MISSING_BLOB, ATTACHMENT_NOT_FOUND, FILE_SIZE_MISMATCH};

    public T8AttachmentValidationResult(String attachmentID, AttachmentValidationError... errors)
    {
        this.attachmentID = attachmentID;
        this.errors = EnumSet.noneOf(AttachmentValidationError.class);
    }

    public String getAttachmentID()
    {
        return attachmentID;
    }

    public T8FileDetails getAttachmentDetails()
    {
        return attachmentDetails;
    }

    public void setAttachmentDetails(T8FileDetails attachmentDetails)
    {
        this.attachmentDetails = attachmentDetails;
    }

    public boolean isValid()
    {
        return errors.isEmpty();
    }

    public Set<AttachmentValidationError> getErrors()
    {
        return EnumSet.copyOf(errors);
    }

    public void setErrors(Set<AttachmentValidationError> newErrors)
    {
        this.errors.clear();
        this.errors.addAll(newErrors);
    }

    public void addErrors(AttachmentValidationError... newErrors)
    {
        this.errors.addAll(Arrays.asList(newErrors));
    }
}
