package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author Bouwer du Preez
 */
public class LowerBoundNumberRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public LowerBoundNumberRequirement()
    {
        super(RequirementType.LOWER_BOUND_NUMBER);
    }

    @Override
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public String getFormatPattern()
    {
        return (String)getAttribute(RequirementAttribute.FORMAT_PATTERN.toString());
    }

    public void setFormatPattern(String formatPattern)
    {
        setAttribute(RequirementAttribute.FORMAT_PATTERN.toString(), formatPattern);
    }

    public BigInteger getMinimumValue()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MINIMUM_NUMERIC_VALUE.toString());
        return value != null ? value.toBigInteger() : null;
    }

    public BigInteger getMaximumValue()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_NUMERIC_VALUE.toString());
        return value != null ? value.toBigInteger() : null;
    }
}
