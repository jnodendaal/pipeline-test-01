package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class OntologyCodeStep extends DefaultPathStep implements PathStep
{
    public OntologyCodeStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, ontologyProvider, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            List<T8OntologyCode> ontologyCodes;
            T8OntologyConcept ontology;
            DataRecord record;

            record = (DataRecord)input;
            ontology = record.getOntology();

            // Depending where this is called from, the ontology may have been retrieved for the record
            // Ontology should however never be null
            if (ontology != null)
            {
                ontologyCodes = ontology.getCodes();
            } else ontologyCodes = new ArrayList<>();

            // If no codes were found on the record ontology, we check the ontology provider
            if (ontologyCodes.isEmpty())
            {
                ontology = ontologyProvider.getOntologyConcept(record.getID());
                ontologyCodes = ontology.getCodes();
            }

            return evaluateValueStep(ontologyCodes);
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateValueStep(List<T8OntologyCode> codeList)
    {
        List<String> codeStrings;

        // If we have code type ID's to use for narrowing of the code list do it now.
        if (idList != null)
        {
            Iterator<T8OntologyCode> codeIterator;

            codeIterator = codeList.iterator();
            while (codeIterator.hasNext())
            {
                T8OntologyCode nextCode;

                nextCode = codeIterator.next();
                if (!idList.contains(nextCode.getCodeTypeID()))
                {
                    codeIterator.remove();
                }
            }
        }

        // Now run through the remaining code list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<T8OntologyCode> codeIterator;

            // Iterator over the codes and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            codeIterator = codeList.iterator();
            while (codeIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                T8OntologyCode nextCode;

                // Create a map containing the input parameters available to the predicate expression.
                nextCode = codeIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("CODE", nextCode.getCode());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        codeIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        codeStrings = new ArrayList<String>();
        for (T8OntologyCode code : codeList)
        {
            codeStrings.add(code.getCode());
        }

        return codeStrings;
    }
}
