package com.pilog.t8.data.ontology;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyCodeType implements Serializable
{
    private String id;
    private String displayName;
    private T8OntologyConceptType conceptType;
    
    public T8OntologyCodeType(String id, T8OntologyConceptType conceptType, String displayName)
    {
        this.id = id;
        this.conceptType = conceptType;
        this.displayName = displayName;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public T8OntologyConceptType getConceptType()
    {
        return conceptType;
    }

    public void setConceptType(T8OntologyConceptType conceptType)
    {
        this.conceptType = conceptType;
    }
}
