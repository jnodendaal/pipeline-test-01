package com.pilog.t8.datatype;

import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarequirement.DataRequirement;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataRequirement extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_REQUIREMENT";

    public T8DtDataRequirement(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return DataRequirement.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof DataRequirement)
            {
                DataRequirement dataRequirement;

                dataRequirement = (DataRequirement)object;
                return T8DocumentSerializer.serializeDataRequirement(dataRequirement);
            }
            else throw new IllegalArgumentException("Type expected: " + getDataTypeClassName() + " Type found: " + object.getClass().getCanonicalName());
        }
        else return JsonValue.NULL;
    }

    @Override
    public DataRequirement deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            if (jsonValue.isObject())
            {
                try
                {
                    return T8DocumentSerializer.deserializeDataRequirement(jsonValue.asObject());
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException("Invalid DataRequirement JSON: " + jsonValue, e);
                }
            }
            else throw new IllegalArgumentException("Invalid DataRequirement JSON: " + jsonValue);
        }
        else return null;
    }
}
