package com.pilog.t8.api.datarecordeditor;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileNewAttachment implements Serializable
{
    private String recordId;
    private String propertyId;
    private String fieldId;
    private String filename;
    private byte[] fileData;

    public T8DataFileNewAttachment()
    {
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public byte[] getFileData()
    {
        return fileData;
    }

    public void setFileData(byte[] fileData)
    {
        this.fileData = fileData;
    }
}
