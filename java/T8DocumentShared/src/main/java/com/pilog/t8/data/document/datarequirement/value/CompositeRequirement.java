package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class CompositeRequirement extends ValueRequirement
{
    public CompositeRequirement()
    {
        super(RequirementType.COMPOSITE_TYPE);
    }
    
    public List<FieldRequirement> getFieldRequirements()
    {
        return (List)getSubRequirements(RequirementType.FIELD_TYPE, null, null);
    }
    
    public FieldRequirement getFieldRequirement(String fieldID)
    {
        return (FieldRequirement)getSubRequirement(RequirementType.FIELD_TYPE, fieldID, null);
    }
}
