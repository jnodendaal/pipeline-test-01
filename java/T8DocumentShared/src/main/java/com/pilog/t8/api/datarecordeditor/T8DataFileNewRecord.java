package com.pilog.t8.api.datarecordeditor;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileNewRecord implements Serializable
{
    private String recordId;
    private String propertyId;
    private String fieldId;
    private String temporaryId;
    private DataRecord newRecord;

    public T8DataFileNewRecord(String temporaryId, DataRecord newRecord)
    {
        this.temporaryId = temporaryId;
        this.newRecord = newRecord;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getTemporaryId()
    {
        return temporaryId;
    }

    public void setTemporaryId(String temporaryId)
    {
        this.temporaryId = temporaryId;
    }

    public DataRecord getNewRecord()
    {
        return newRecord;
    }

    public void setNewRecord(DataRecord newRecord)
    {
        this.newRecord = newRecord;
    }
}
