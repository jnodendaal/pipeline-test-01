package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.T8DataFilterSerializer;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Extends {@code T8DataFilterSerializer} to accommodate additional filter clauses employed by record filter criteria.
 *
 * @author Bouwer du Preez
 */
public class T8RecordFilterSerializer extends T8DataFilterSerializer
{
    public T8RecordFilterSerializer()
    {
    }

    public JsonValue serializeRecordFilterCriteria(T8RecordFilterCriteria criteria)
    {
        return serializeRecordFilterCriteria(criteria, DataFilterConjunction.AND);
    }

    public T8DataFilterClause deserializeRecordFilter(JsonValue value)
    {
        if (value != null)
        {
            if (value instanceof JsonObject)
            {
                JsonObject jsonFilter;

                jsonFilter = (JsonObject)value;
                return deserializeFilterClause(jsonFilter);
            }
            else throw new IllegalArgumentException("Input value is not a Data Filter object: " + value);
        }
        else return null;
    }

    /**
     * Overrides default implementation to provide support for additional filter clause typse.
     * @param filterClause The clause to serialize.
     * @param conjunction The conjunction to serialize.
     * @return The JSON serialization of the supplied filter clause.
     */
    @Override
    protected JsonObject serializeFilterClause(T8DataFilterClause filterClause, DataFilterConjunction conjunction)
    {
        if (filterClause instanceof T8RecordFilterCriteria)
        {
            return T8RecordFilterSerializer.this.serializeRecordFilterCriteria((T8RecordFilterCriteria)filterClause, conjunction);
        }
        else if (filterClause instanceof T8DrInstanceFilterCriteria)
        {
            return serializeDrInstanceFilterCriteria((T8DrInstanceFilterCriteria)filterClause, conjunction);
        }
        else if (filterClause instanceof T8DrFilterCriteria)
        {
            return serializeDrFilterCriteria((T8DrFilterCriteria)filterClause, conjunction);
        }
        else if (filterClause instanceof T8DataFilterCriteria)
        {
            return serializeFilterCriteria((T8DataFilterCriteria)filterClause, conjunction);
        }
        else if (filterClause instanceof T8DataFilterCriterion)
        {
            return serializeFilterCriterion((T8DataFilterCriterion)filterClause, conjunction);
        }
        else throw new IllegalArgumentException("Invalid filter clause type: " + filterClause);
    }

    private JsonObject serializeRecordFilterCriteria(T8RecordFilterCriteria filterCriteria, DataFilterConjunction conjunction)
    {
        LinkedHashMap<T8DataFilterClause, DataFilterConjunction> filterClauses;
        JsonObject criteriaObject;
        JsonArray clauseArray;

        // Create the criteria object.
        criteriaObject = new JsonObject();
        criteriaObject.add("type", "@RECORD_FILTER_CRITERIA");
        criteriaObject.add("conjunction", conjunction.toString());
        criteriaObject.add("fieldId", filterCriteria.getRecordFilterFieldIdentifier());
        criteriaObject.add("isAllowCriteriaAddition", filterCriteria.isAllowCriteriaAddition());
        criteriaObject.add("isAllowSubCriteriaAddition", filterCriteria.isAllowSubCriteriaAddition());

        // Create the clause array.
        clauseArray = new JsonArray();
        filterClauses = filterCriteria.getFilterClauses();
        for (T8DataFilterClause clause : filterClauses.keySet())
        {
            clauseArray.add(serializeFilterClause(clause, filterClauses.get((T8RecordFilterClause)clause)));
        }

        // Add the clauses to the criteria and return the object.
        criteriaObject.add("criteria", clauseArray);
        return criteriaObject;
    }

    private JsonObject serializeDrInstanceFilterCriteria(T8DrInstanceFilterCriteria filterCriteria, DataFilterConjunction conjunction)
    {
        List<T8PropertyFilterCriterion> propertyCriteria;
        List<T8CodeFilterCriterion> codeCriteria;
        List<T8OrganizationFilterCriterion> organizationCriteria;
        JsonObject criteriaObject;
        JsonArray propertyArray;
        JsonArray codeArray;
        JsonArray organizationArray;

        // Create the criteria object.
        criteriaObject = new JsonObject();
        criteriaObject.add("conjunction", conjunction.toString());
        criteriaObject.add("type", "@DRI_FILTER_CRITERIA");
        criteriaObject.add("drInstanceId", filterCriteria.getDrInstanceId());
        criteriaObject.add("displayName", filterCriteria.getDisplayName());
        criteriaObject.add("isAllowCodeAddition", filterCriteria.isAllowCodeAddition());
        criteriaObject.add("isAllowPropertyAddition", filterCriteria.isAllowPropertyAddition());
        criteriaObject.add("isAllowHistoryAddition", filterCriteria.isAllowHistoryAddition());
        criteriaObject.add("isAllowRemoval", filterCriteria.isAllowRemoval());

        // Create the property criteria array.
        propertyArray = new JsonArray();
        propertyCriteria = filterCriteria.getPropertyCriteria();
        for (T8PropertyFilterCriterion propertyCriterion : propertyCriteria)
        {
            propertyArray.add(serializePropertyFilterCriterion(propertyCriterion));
        }
        criteriaObject.add("propertyCriteria", propertyArray);

        // Create the code criteria array.
        codeArray = new JsonArray();
        codeCriteria = filterCriteria.getCodeCriteria();
        for (T8CodeFilterCriterion codeCriterion : codeCriteria)
        {
            codeArray.add(serializeCodeFilterCriterion(codeCriterion));
        }
        criteriaObject.add("codeCriteria", codeArray);

         // Create the organization criteria array.
        organizationArray = new JsonArray();
        organizationCriteria = filterCriteria.getOrganizationCriteria();
        for (T8OrganizationFilterCriterion organizationCriterion : organizationCriteria)
        {
            organizationArray.add(serializeOrganizationFilterCriterion(organizationCriterion));
        }
        criteriaObject.add("organizationCriteria", organizationArray);

        return criteriaObject;
    }

    private JsonObject serializeDrFilterCriteria(T8DrFilterCriteria filterCriteria, DataFilterConjunction conjunction)
    {
        List<T8PropertyFilterCriterion> propertyCriteria;
        List<T8CodeFilterCriterion> codeCriteria;
        List<T8OrganizationFilterCriterion> organizationCriteria;
        T8DataFilterCriterion.DataFilterOperator instanceOperator;
        JsonObject criteriaObject;
        JsonArray propertyArray;
        JsonArray codeArray;
        JsonArray organizationArray;

        // Create the criteria object.
        criteriaObject = new JsonObject();
        criteriaObject.add("conjunction", conjunction.toString());
        criteriaObject.add("type", "@DR_FILTER_CRITERIA");
        criteriaObject.add("drId", filterCriteria.getDrId());
        criteriaObject.add("displayName", filterCriteria.getDisplayName());
        criteriaObject.add("isAllowCodeAddition", filterCriteria.isAllowCodeAddition());
        criteriaObject.add("isAllowPropertyAddition", filterCriteria.isAllowPropertyAddition());
        criteriaObject.add("isAllowHistoryAddition", filterCriteria.isAllowHistoryAddition());
        criteriaObject.add("isAllowRemoval", filterCriteria.isAllowRemoval());
        criteriaObject.add("isAllowInstanceSelection", filterCriteria.isAllowInstanceSelection());
        criteriaObject.add("instanceLabel", filterCriteria.getInstanceLabel());
        criteriaObject.add("instanceValue", filterCriteria.getInstanceFilterValue());

        instanceOperator = filterCriteria.getInstanceOperator();
        if (instanceOperator != null) criteriaObject.add("instanceOperator", instanceOperator.toString());

        // Create the property criteria array.
        propertyArray = new JsonArray();
        propertyCriteria = filterCriteria.getPropertyCriteria();
        for (T8PropertyFilterCriterion propertyCriterion : propertyCriteria)
        {
            propertyArray.add(serializePropertyFilterCriterion(propertyCriterion));
        }
        criteriaObject.add("propertyCriteria", propertyArray);

        // Create the code criteria array.
        codeArray = new JsonArray();
        codeCriteria = filterCriteria.getCodeCriteria();
        for (T8CodeFilterCriterion codeCriterion : codeCriteria)
        {
            codeArray.add(serializeCodeFilterCriterion(codeCriterion));
        }
        criteriaObject.add("codeCriteria", codeArray);

         // Create the organization criteria array.
        organizationArray = new JsonArray();
        organizationCriteria = filterCriteria.getOrganizationCriteria();
        for (T8OrganizationFilterCriterion organizationCriterion : organizationCriteria)
        {
            organizationArray.add(serializeOrganizationFilterCriterion(organizationCriterion));
        }
        criteriaObject.add("organizationCriteria", organizationArray);

        return criteriaObject;
    }

    private JsonObject serializePropertyFilterCriterion(T8PropertyFilterCriterion filterCriterion)
    {
        List<T8ValueFilterCriterion> valueCriteria;
        JsonObject criterionObject;
        JsonArray valueArray;

        // Create the criterion object.
        criterionObject = new JsonObject();
        criterionObject.add("propertyId", filterCriterion.getPropertyId());
        criterionObject.add("fieldId", filterCriterion.getFieldId());
        criterionObject.add("isAllowRemoval", filterCriterion.isAllowRemoval());

        // Create the value criteria array.
        valueArray = new JsonArray();
        valueCriteria = filterCriterion.getValueCriteria();
        for (T8ValueFilterCriterion valueCriterion : valueCriteria)
        {
            valueArray.add(serializeValueFilterCriterion(valueCriterion));
        }
        criterionObject.add("valueCriteria", valueArray);
        return criterionObject;
    }

    private JsonObject serializeCodeFilterCriterion(T8CodeFilterCriterion filterCriterion)
    {
        JsonObject criterionObject;
        JsonObject valueObject;

        // Create the criterion object.
        criterionObject = new JsonObject();
        criterionObject.add("codeTypeId", filterCriterion.getCodeTypeId());
        criterionObject.add("target", filterCriterion.getTarget().toString());
        criterionObject.add("isAllowRemoval", filterCriterion.isAllowRemoval());

        // Create the value criteria object.
        valueObject = new JsonObject();
        valueObject.add("operator", filterCriterion.getOperator().toString());
        valueObject.add("value", filterCriterion.getFilterValue());
        criterionObject.add("valueCriteria", valueObject);
        return criterionObject;
    }

    private JsonObject serializeOrganizationFilterCriterion(T8OrganizationFilterCriterion filterCriterion)
    {
        JsonObject criterionObject;
        JsonObject valueObject;
        JsonArray orgTypeIds;
        List<String> orgTypeIdList;

        // Create the criterion object.
        criterionObject = new JsonObject();
        criterionObject.add("label", filterCriterion.getLabel());

        // Create the json array with the org type ids.
        orgTypeIds = new JsonArray();
        orgTypeIdList = filterCriterion.getOrganizationTypeIdList();
        if (orgTypeIdList != null && orgTypeIdList.size() > 0)
        {
            for (String orgTypeId : orgTypeIdList) orgTypeIds.add(orgTypeId);
        }
        criterionObject.add("orgTypeIds", orgTypeIds);

        // Create the value criteria object.
        valueObject = new JsonObject();
        valueObject.add("operator", filterCriterion.getOperator().toString());
        valueObject.add("value", filterCriterion.getFilterValue());
        criterionObject.add("valueCriteria", valueObject);
        return criterionObject;
    }

    private JsonObject serializeValueFilterCriterion(T8ValueFilterCriterion filterCriterion)
    {
        JsonObject criterionObject;
        RequirementType type;

        type = filterCriterion.getDataType();

        // Create the criterion object.
        criterionObject = new JsonObject();
        criterionObject.add("type", type != null ? type.toString() : null);
        criterionObject.add("operator", filterCriterion.getOperator().toString());
        criterionObject.add("value", filterCriterion.getFilterValue());
        return criterionObject;
    }

    /**
     * Overrides default implementation to provider support for additional filter clause types.
     * @param jsonClause The JSON object containing the filter clause data to deserialize.
     * @return The data filter clause deserialized from the supplied JSON object.
     */
    @Override
    protected T8DataFilterClause deserializeFilterClause(JsonObject jsonClause)
    {
        String type;

        type = jsonClause.getString("type");
        if (type != null)
        {
            if (type.equals("@FILTER_CRITERIA"))
            {
                return deserializeFilterCriteria(jsonClause);
            }
            else if (type.equals("@FILTER_CRITERION"))
            {
                return deserializeFilterCriterion(jsonClause);
            }
            else if (type.equals("@RECORD_FILTER_CRITERIA"))
            {
                return deserializeRecordFilterCriteria(jsonClause);
            }
            else if (type.equals("@DR_FILTER_CRITERIA"))
            {
                return deserializeDrFilterCriteria(jsonClause);
            }
            else if (type.equals("@DRI_FILTER_CRITERIA"))
            {
                return deserializeDrInstanceFilterCriteria(jsonClause);
            }
            else throw new RuntimeException("Invalid type '" + type + "' specification found for JSON filter clause: " + jsonClause);
        }
        else throw new RuntimeException("No type specification found for JSON filter clause: " + jsonClause);
    }

    public T8RecordFilterCriteria deserializeRecordFilterCriteria(JsonObject jsonCriteria)
    {
        T8RecordFilterCriteria criteria;
        JsonArray clauseArray;

        criteria = new T8RecordFilterCriteria(null);
        criteria.setRecordFilterFieldIdentifier(jsonCriteria.getString("fieldId"));
        criteria.setAllowCriteriaAddition(jsonCriteria.getBoolean("isAllowCriteriaAddition"));
        criteria.setAllowSubCriteriaAddition(jsonCriteria.getBoolean("isAllowSubCriteriaAddition"));
        clauseArray = jsonCriteria.getJsonArray("criteria");
        if (clauseArray != null)
        {
            for (JsonValue clauseValue : clauseArray)
            {
                T8DataFilterClause clause;
                DataFilterConjunction conjunction;
                JsonObject clauseObject;
                String conjunctionString;

                clauseObject = (JsonObject)clauseValue;
                conjunctionString = clauseObject.getString("conjunction");
                conjunction = Strings.isNullOrEmpty(conjunctionString) ? DataFilterConjunction.AND : DataFilterConjunction.valueOf(conjunctionString);
                clause = deserializeFilterClause(clauseObject);
                criteria.addFilterClause(conjunction, clause);
            }
        }

        return criteria;
    }

    private T8DrInstanceFilterCriteria deserializeDrInstanceFilterCriteria(JsonObject jsonCriteria)
    {
        T8DrInstanceFilterCriteria criteria;
        JsonArray propertyArray;
        JsonArray codeArray;
        JsonArray organizationArray;

        criteria = new T8DrInstanceFilterCriteria(jsonCriteria.getString("drInstanceId"));
        criteria.setDisplayName(jsonCriteria.getString("displayName"));
        criteria.setAllowCodeAddition(jsonCriteria.getBoolean("isAllowCodeAddition"));
        criteria.setAllowPropertyAddition(jsonCriteria.getBoolean("isAllowPropertyAddition"));
        criteria.setAllowHistoryAddition(jsonCriteria.getBoolean("isAllowHistoryAddition"));
        criteria.setAllowRemoval(jsonCriteria.getBoolean("isAllowRemoval"));

        propertyArray = jsonCriteria.getJsonArray("propertyCriteria");
        if (propertyArray != null)
        {
            for (JsonValue propertyValue : propertyArray)
            {
                T8PropertyFilterCriterion propertyCriterion;
                JsonObject jsonPropertyCriterion;

                jsonPropertyCriterion = (JsonObject)propertyValue;
                propertyCriterion = deserializePropertyFilterCriterion(jsonPropertyCriterion);
                criteria.addPropertyCriterion(propertyCriterion);
            }
        }

        codeArray = jsonCriteria.getJsonArray("codeCriteria");
        if (codeArray != null)
        {
            for (JsonValue codeValue : codeArray)
            {
                T8CodeFilterCriterion codeCriterion;
                JsonObject jsonCodeCriterion;

                jsonCodeCriterion = (JsonObject)codeValue;
                codeCriterion = deserializeCodeFilterCriterion(jsonCodeCriterion);
                criteria.addCodeCriterion(codeCriterion);
            }
        }

        organizationArray = jsonCriteria.getJsonArray("organizationCriteria");
        if (organizationArray != null)
        {
            for (JsonValue organizationValue : organizationArray)
            {
                T8OrganizationFilterCriterion organizationCriterion;
                JsonObject jsonOrganizationCriterion;

                jsonOrganizationCriterion = (JsonObject)organizationValue;
                organizationCriterion = deserializeOrganizationFilterCriterion(jsonOrganizationCriterion);
                criteria.addOrganizationCriterion(organizationCriterion);
            }
        }

        return criteria;
    }

    private T8DrFilterCriteria deserializeDrFilterCriteria(JsonObject jsonCriteria)
    {
        T8DrFilterCriteria criteria;
        JsonArray propertyArray;
        JsonArray codeArray;
        JsonArray organizationArray;
        JsonArray instanceValueArray;
        List<String> instanceValueList;

        criteria = new T8DrFilterCriteria(jsonCriteria.getString("drId"));
        criteria.setDisplayName(jsonCriteria.getString("displayName"));
        criteria.setAllowCodeAddition(jsonCriteria.getBoolean("isAllowCodeAddition"));
        criteria.setAllowPropertyAddition(jsonCriteria.getBoolean("isAllowPropertyAddition"));
        criteria.setAllowHistoryAddition(jsonCriteria.getBoolean("isAllowHistoryAddition"));
        criteria.setAllowRemoval(jsonCriteria.getBoolean("isAllowRemoval"));
        criteria.setAllowInstanceSelection(jsonCriteria.getBoolean("isAllowInstanceSelection"));
        criteria.setInstanceLabel(jsonCriteria.getString("instanceLabel"));
        criteria.setInstanceOperator(DataFilterOperator.valueOf(jsonCriteria.getString("instanceOperator")));
        
        instanceValueArray = jsonCriteria.getJsonArray("instanceValue");
        if (instanceValueArray != null)
        {   
            instanceValueList = new ArrayList<>();
            for (JsonValue instanceValue : instanceValueArray)
            {
                instanceValueList.add(instanceValue.asString());
            }
            criteria.setInstanceFilterValue(instanceValueList);
        }

        propertyArray = jsonCriteria.getJsonArray("propertyCriteria");
        if (propertyArray != null)
        {
            for (JsonValue propertyValue : propertyArray)
            {
                T8PropertyFilterCriterion propertyCriterion;
                JsonObject jsonPropertyCriterion;

                jsonPropertyCriterion = (JsonObject)propertyValue;
                propertyCriterion = deserializePropertyFilterCriterion(jsonPropertyCriterion);
                criteria.addPropertyCriterion(propertyCriterion);
            }
        }

        codeArray = jsonCriteria.getJsonArray("codeCriteria");
        if (codeArray != null)
        {
            for (JsonValue codeValue : codeArray)
            {
                T8CodeFilterCriterion codeCriterion;
                JsonObject jsonCodeCriterion;

                jsonCodeCriterion = (JsonObject)codeValue;
                codeCriterion = deserializeCodeFilterCriterion(jsonCodeCriterion);
                criteria.addCodeCriterion(codeCriterion);
            }
        }

        organizationArray = jsonCriteria.getJsonArray("organizationCriteria");
        if (organizationArray != null)
        {
            for (JsonValue organizationValue : organizationArray)
            {
                T8OrganizationFilterCriterion organizationCriterion;
                JsonObject jsonOrganizationCriterion;

                jsonOrganizationCriterion = (JsonObject)organizationValue;
                organizationCriterion = deserializeOrganizationFilterCriterion(jsonOrganizationCriterion);
                criteria.addOrganizationCriterion(organizationCriterion);
            }
        }

        return criteria;
    }

    private T8CodeFilterCriterion deserializeCodeFilterCriterion(JsonObject jsonCriterion)
    {
        T8CodeFilterCriterion codeCriterion;
        JsonObject valueCriteria;
        String codeTypeId;
        String target;
        boolean isAllowRemoval;

        codeTypeId = jsonCriterion.getString("codeTypeId");
        target = jsonCriterion.getString("target");
        isAllowRemoval = jsonCriterion.getBoolean("isAllowRemoval");
        valueCriteria = jsonCriterion.getJsonObject("valueCriteria");

        codeCriterion = new T8CodeFilterCriterion(codeTypeId);
        codeCriterion.setTarget(T8CodeFilterCriterion.CodeCriterionTarget.valueOf(target));
        codeCriterion.setAllowRemoval(isAllowRemoval);

        if (valueCriteria != null)
        {
            String operator;
            String value;

            operator = valueCriteria.getString("operator");
            value = valueCriteria.getString("value");

            codeCriterion.setOperator(DataFilterOperator.valueOf(operator));
            codeCriterion.setFilterValue(value);
        }

        return codeCriterion;
    }

    private T8OrganizationFilterCriterion deserializeOrganizationFilterCriterion(JsonObject jsonCriterion)
    {
        T8OrganizationFilterCriterion organizationCriterion;
        JsonObject valueCriteria;
        JsonArray jsonOrgTypeIds;
        List<String> orgTypeIds;
        String label;

        label = jsonCriterion.getString("label");
        jsonOrgTypeIds = jsonCriterion.getJsonArray("orgTypeIds");
        valueCriteria = jsonCriterion.getJsonObject("valueCriteria");

        orgTypeIds = new ArrayList<>();
        for(JsonValue orgTypeId : jsonOrgTypeIds)
        {
            orgTypeIds.add(orgTypeId.asString());
        }

        organizationCriterion = new T8OrganizationFilterCriterion(orgTypeIds, label);

        if (valueCriteria != null)
        {
            String operator;
            List<String> orgValueList;
            JsonArray orgValueArray;

            operator = valueCriteria.getString("operator");
            organizationCriterion.setOperator(DataFilterOperator.valueOf(operator));
            
            orgValueArray = valueCriteria.getJsonArray("value");
            if (orgValueArray != null)
            {   
                orgValueList = new ArrayList<>();
                for (JsonValue orgValue : orgValueArray)
                {
                    orgValueList.add(orgValue.asString());
                }
                organizationCriterion.setFilterValue(orgValueList);
            }
        }

        return organizationCriterion;
    }

    private T8PropertyFilterCriterion deserializePropertyFilterCriterion(JsonObject jsonCriterion)
    {
        T8PropertyFilterCriterion propertyCriterion;
        JsonArray jsonValueCriteria;
        String propertyId;
        String fieldId;
        boolean isAllowRemoval;

        propertyId = jsonCriterion.getString("propertyId");
        fieldId = jsonCriterion.getString("fieldId");
        isAllowRemoval = jsonCriterion.getBoolean("isAllowRemoval");
        jsonValueCriteria = jsonCriterion.getJsonArray("valueCriteria");

        propertyCriterion = new T8PropertyFilterCriterion(propertyId, fieldId);
        propertyCriterion.setAllowRemoval(isAllowRemoval);

        if (jsonValueCriteria != null)
        {
            for (JsonObject jsonValueCriterion : jsonValueCriteria.objects())
            {
                T8ValueFilterCriterion valueCriterion;

                valueCriterion = deserializeValueFilterCriterion(jsonValueCriterion);
                propertyCriterion.addValueCriterion(valueCriterion);
            }
        }

        return propertyCriterion;
    }

    private T8ValueFilterCriterion deserializeValueFilterCriterion(JsonObject jsonCriterion)
    {
        DataFilterOperator operator;
        RequirementType type;
        Object filterValue;
        String typeString;

        operator = DataFilterOperator.valueOf(jsonCriterion.getString("operator"));
        filterValue = jsonCriterion.getString("value");
        typeString = jsonCriterion.getString("type");
        type = typeString != null ? RequirementType.valueOf(typeString) : null;

        if (type == null)
        {
            return new T8DefaultValueFilterCriterion(type, operator, filterValue);
        }
        else
        {
            switch (type)
            {
                case CONTROLLED_CONCEPT:
                    return new T8ControlledConceptFilterCriterion(operator, filterValue);
                case DOCUMENT_REFERENCE:
                    return new T8DocumentReferenceFilterCriterion(operator, filterValue);
                default:
                    return new T8DefaultValueFilterCriterion(type, operator, filterValue);
            }
        }
    }
}