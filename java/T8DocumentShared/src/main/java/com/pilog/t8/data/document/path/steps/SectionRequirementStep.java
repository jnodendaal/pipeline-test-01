package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class SectionRequirementStep extends DefaultPathStep implements PathStep
{
    public SectionRequirementStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateInstanceStep(((DataRecord)input).getDataRequirement().getSectionRequirements());
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateInstanceStep(((DataRequirementInstance)input).getDataRequirement().getSectionRequirements());
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateInstanceStep(((DataRequirement)input).getSectionRequirements());
        }
        else if (input instanceof List)
        {
            List<SectionRequirement> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<SectionRequirement>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<SectionRequirement>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance Requirement step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<SectionRequirement> evaluateInstanceStep(List<SectionRequirement> sectionList)
    {
        // If we have valid ID's to use for narrowing of the section list do it now.
        if (idList != null)
        {
            Iterator<SectionRequirement> sectionIterator;

            sectionIterator = sectionList.iterator();
            while (sectionIterator.hasNext())
            {
                SectionRequirement nextSection;

                nextSection = sectionIterator.next();
                if (!idList.contains(nextSection.getConceptID()))
                {
                    sectionIterator.remove();
                }
            }
        }

        // Now run through the remaining section list and evaluate the predicate expression (if any).
        if (predicateExpression != null)
        {
            Iterator<SectionRequirement> sectionIterator;

            // Iterator over the sections and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            sectionIterator = sectionList.iterator();
            while (sectionIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                SectionRequirement nextSection;

                // Create a map containing the input parameters available to the predicate expression.
                nextSection = sectionIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("SECTION", nextSection);
                inputParameters.put("SECTION_ID", nextSection.getConceptID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        sectionIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return sectionList;
    }
}
