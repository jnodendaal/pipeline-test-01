package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This synchronizer is used to update a record's content so that it reflects
 * exactly the content of a facsimile that was originally copied from it.
 * This scenario occurs for example, when a record is copied, the copy is sent
 * to some external destination where it is updated and the updated facsimile is
 * then later sent back to the source where the original source record must be
 * updated with all of the changes made to the facsimile.
 *
 * In order for a synchronize operation to be performed, all data records in the
 * facsimile that will be merged/synchronized back into the source record must
 * have the appropriate attribute (SOURCE_RECORD_ID), identifying from which source
 * the record was originally copied.  All records without a source tag will be
 * considered as new additions and will therefore be added as new data to the
 * original.
 *
 * @author Bouwer du Preez
 */
public class T8DataRecordSynchronizer
{
    protected final Set<DataRecord> newDataRecords;
    protected final Set<DataRecord> updatedDataRecords;
    protected final Map<String, String> recordIdMapping;
    protected final Map<String, String> attachmentIdMapping; // Used to store attachment ID's mapped to newly assigned identifiers in destination documents.
    protected final Map<String, String> particleSettingsIdMapping;

    public T8DataRecordSynchronizer()
    {
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.attachmentIdMapping = new HashMap<String, String>();
        this.recordIdMapping = new HashMap<String, String>();
        this.particleSettingsIdMapping = new HashMap<>();
    }

    public void reset()
    {
        newDataRecords.clear();
        updatedDataRecords.clear();
        attachmentIdMapping.clear();
        recordIdMapping.clear();
        particleSettingsIdMapping.clear();
    }

    public Map<String, String> getRecordIdMapping()
    {
        return new HashMap<>(recordIdMapping);
    }

    public Map<String, String> getParticleSettingsIdMapping()
    {
        return new HashMap<>(particleSettingsIdMapping);
    }

    public Map<String, String> getAttachmentIdentifierMapping()
    {
        return new HashMap<>(attachmentIdMapping);
    }

    public Set<DataRecord> getNewDataRecordSet()
    {
        return new HashSet<>(newDataRecords);
    }

    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        return new HashSet<>(updatedDataRecords);
    }

    /**
     * Synchronizes the specified destination data record so that it matches the
     * content of the supplied source record.  In order for this method
     * to work properly, the supplied source record must be tagged with the
     * corresponding destination record id from which its content was originally
     * copied.
     * @param destinationDataRecord The record that will be synchronized so that
     * its contents matches the content of the supplied source record.  All
     * existing record ID's will remain.
     * @param sourceDataRecord The record from which content will be fetched and
     * added/substituted into the destination record.
     * @return The new destination record, containing exactly the content of the
     * supplied source record.
     */
    public DataRecord synchronizeDataRecords(DataRecord destinationDataRecord, DataRecord sourceDataRecord)
    {
        DataRecord newDestinationRecord;
        List<DataRecord> destinationReferrerRecords;

        // Get the parent record.
        destinationReferrerRecords = destinationDataRecord.getReferrerRecords();

        // Copy the source record.
        newDestinationRecord = sourceDataRecord.copy(true, false, true, false, true, true);

        // Apply the record ID's stored in the SOURCE_RECORD_ID tags of the copied documents.
        applySourceRecordIds(newDestinationRecord, destinationDataRecord.getDataFile());

        // Synchornize Ontology.
        synchronizeOntology(newDestinationRecord, destinationDataRecord);

        // Match attchments to those in the existing destination document.
        applyAttachmentIds(newDestinationRecord, destinationDataRecord);

        // Match the particle setting to those in the existing destination document.
        applyParticleSettingsIds(newDestinationRecord, destinationDataRecord);

        // Remove the existing destination record from its parents and add the new destination record.
        for (DataRecord destinationReferrerRecord : destinationReferrerRecords)
        {
            // Remove the destination record from its referrer.
            destinationReferrerRecord.removeSubRecord(destinationDataRecord);

            // Add the new destination record to the parent.
            destinationReferrerRecord.addSubRecord(newDestinationRecord);
            updatedDataRecords.add(destinationReferrerRecord);
        }

        // Return the new destination record.
        return newDestinationRecord;
    }

    private void synchronizeOntology(DataRecord newDestinationRecord, DataRecord oldDestinationRecord)
    {
        T8OntologyConcept destinationConcept;

        // Copy the old destination concept.
        destinationConcept = oldDestinationRecord.getOntology().copy(true, true, true, true, true);

        // Clear the old ontology links on the concept and then transfer ontology links form the new destination record.
        destinationConcept.clearOntologyLinks();
        for (T8OntologyLink link : newDestinationRecord.getOntologyLinks())
        {
            destinationConcept.addOntologyLink(link.copy());
        }

        // Copy all codes from the newDestinationRecord that do not exist yet on the destination concept.  Update those that do exist so that they match.
        for (T8OntologyCode code : newDestinationRecord.getOntology().getCodes())
        {
            T8OntologyCode existingCode;

            // See if there is an existing code with the same type on the master.
            existingCode = destinationConcept.getCodeByType(code.getCodeTypeID());
            if (existingCode == null)
            {
                T8OntologyCode newCode;

                // Existing code not found, so add a new one.
                newCode = code.copy();
                newCode.setID(T8IdentifierUtilities.createNewGUID());
                destinationConcept.addCode(newCode);
            }
            else
            {
                // Existing code found, so update it with the value from the request.
                existingCode.setCode(code.getCode());
            }
        }

        // Set the new destination concept to the old one.
        newDestinationRecord.setOntology(destinationConcept);

        // Synchronize descendants.
        for (DataRecord newDestinationSubRecord : newDestinationRecord.getSubRecords())
        {
            DataRecord oldDestinationSubRecord;

            // Get the corresponding old destination record from which the new destination record was copied.
            oldDestinationSubRecord = oldDestinationRecord.getSubRecord(newDestinationSubRecord.getID());
            if (oldDestinationSubRecord != null)
            {
                synchronizeOntology(newDestinationSubRecord, oldDestinationSubRecord);
            }
            else
            {
                T8OntologyConcept newSubRecordConcept;

                // No corresponding old destination record was found, which means this is a newly added sub-record (not copied from any source).
                // By this time in synchronization a new concept id will have been assigned to the record, so the only steps remaining are to assign new terminology id's.
                newSubRecordConcept = newDestinationSubRecord.getOntology();
                newSubRecordConcept.assignNewTermIDs();
                newSubRecordConcept.assignNewAbbreviationIDs();
                newSubRecordConcept.assignNewDefinitionIDs();
                newSubRecordConcept.assignNewCodeIDs();

                // Assign new ontology id's to all descendants, as they will also be new additions.
                for (DataRecord descendantRecord : newDestinationSubRecord.getDescendantRecords())
                {
                    T8OntologyConcept newDescendantRecordConcept;

                    newDescendantRecordConcept = descendantRecord.getOntology();
                    newDescendantRecordConcept.assignNewTermIDs();
                    newDescendantRecordConcept.assignNewAbbreviationIDs();
                    newDescendantRecordConcept.assignNewDefinitionIDs();
                    newDescendantRecordConcept.assignNewCodeIDs();
                }
            }
        }
    }

    /**
     * This method recursively applies new record ID's to the supplied record
     * and all of its descendants.  For each record, the tag SOURCE_RECORD_ID is
     * used to determine the new record ID of the document.  If a record does
     * not have the SOURCE_RECORD_ID tag, the record's ID will be updated to a
     * newly created ID.
     * @param dataRecord The data record to which the source ID's will be
     * applied.
     */
    private void applySourceRecordIds(DataRecord dataRecord, DataRecord sourceDataFile)
    {
        T8OntologyConcept ontology;
        String newRecordId;
        String existingRecordId;

        // Get the source record ID attribute.
        newRecordId = (String)dataRecord.getAttribute(T8DataRecordMerger.SOURCE_RECORD_ID_ATTRIBUTE_IDENTIFIER);
        if (newRecordId != null)
        {
            // If the source data file still contains the record from which this data record was originally copied, then use the same record ID.
            if (sourceDataFile.containsDataRecord(newRecordId))
            {
                // Log the document update.
                updatedDataRecords.add(dataRecord);
            }
            else // We found the SOURCE_RECORD_ID tag but the original source record no longer exists, so we have to treat this as a new addition.
            {
                // Generate a new record ID.
                newRecordId = T8IdentifierUtilities.createNewGUID();

                // Log the new document.
                newDataRecords.add(dataRecord);
            }
        }
        else // Ok, so this record does not contain the SOURCE_RECORD_ID attribute and so we treat it as a new addition.
        {
            // Generate a new record ID.
            newRecordId = T8IdentifierUtilities.createNewGUID();

            // Log the new document.
            newDataRecords.add(dataRecord);
        }

        // Set the new record ID on the record.
        existingRecordId = dataRecord.getID();
        dataRecord.setID(newRecordId);
        recordIdMapping.put(existingRecordId, newRecordId);

        // Replace parent references.
        for (DataRecord parentRecord : dataRecord.getReferrerRecords())
        {
            parentRecord.replaceDocumentReference(existingRecordId, newRecordId);
        }

        // Update the ontology of the record with the new concept ID.
        ontology = dataRecord.getOntology();
        if (ontology != null) ontology.setID(newRecordId);

        // Apply new ID's to the descendants of this record.
        for (DataRecord subRecord : dataRecord.getSubRecords())
        {
            applySourceRecordIds(subRecord, sourceDataFile);
        }
    }

    /**
     * This method matches attachment in the new destination record with those
     * existing in the old destination record using MD5 checksums.  If an
     * attachment is the new destination document matches one of those in the
     * old document, the new attachment is renamed back to its old ID so that
     * no unnecessary replacement occurs.  If no match is found for the new
     * attachment, a new ID is assigned to the attachment.
     * @param newDestinationRecord The new destination record.
     * @param oldDestinationRecord The old destination record.
     */
    private void applyAttachmentIds(DataRecord newDestinationRecord, DataRecord oldDestinationRecord)
    {
        Map<String, T8AttachmentDetails> newAttachmentDetails;
        Map<String, T8AttachmentDetails> oldAttachmentDetails;

        // Get the old and new attachment details.
        oldAttachmentDetails = oldDestinationRecord.getAttachmentDetails();
        newAttachmentDetails = newDestinationRecord.getAttachmentDetails();
        for (String newAttachmentId : newAttachmentDetails.keySet())
        {
            T8AttachmentDetails newFileDetails;
            String newChecksum;
            boolean found;

            // Get the new attachment details.
            newFileDetails = newAttachmentDetails.get(newAttachmentId);
            newChecksum = newFileDetails.getMD5Checksum();

            // Find an old attachment matching the new one.
            found = false;
            for (String oldAttachmentId : oldAttachmentDetails.keySet())
            {
                T8AttachmentDetails oldFileDetails;
                String oldChecksum;

                // Get the old file details.
                oldFileDetails = oldAttachmentDetails.get(oldAttachmentId);
                oldChecksum = oldFileDetails.getMD5Checksum();

                // Match old and new files based on MD5 checksum.
                if ((newChecksum != null) && (newChecksum.equals(oldChecksum)))
                {
                    // They match, so rename the new attachment to its previous ID.
                    found = true;
                    newDestinationRecord.replaceAttachmentDetails(newAttachmentId, oldAttachmentId, oldFileDetails.copy());
                    break;
                }
            }

            // If a match was not found, we have to copy the attachment details and assign a new ID.
            if (!found)
            {
                String newId;

                newId = T8IdentifierUtilities.createNewGUID();
                newFileDetails.setContextIid(newId);
                newDestinationRecord.replaceAttachmentDetails(newAttachmentId, newId, newFileDetails);
                attachmentIdMapping.put(newAttachmentId, newId);
            }
        }

        // Now process descendant records.
        for (DataRecord newDestinationSubRecord : newDestinationRecord.getSubRecords())
        {
            DataRecord oldDestinationSubRecord;

            oldDestinationSubRecord = oldDestinationRecord.getSubRecord(newDestinationSubRecord.getID());
            if (oldDestinationSubRecord != null)
            {
                applyAttachmentIds(newDestinationSubRecord, oldDestinationSubRecord);
            }
            else
            {
                attachmentIdMapping.putAll(newDestinationSubRecord.assignNewAttachmentIdentifiers(true));
            }
        }
    }

    /**
     * This method matches particle settings in the new destination record with those
     * existing in the old destination record using the target of the settings (not the id).
     * If a setting object in the new destination document matches one of those in the
     * old document, the new settings object's id is set to the old id so that
     * no unnecessary duplication occurs.  If no match is found for the new
     * setting, a new id is assigned to the attachment.
     * @param newDestinationRecord The new destination record.
     * @param oldDestinationRecord The old destination record.
     */
    private void applyParticleSettingsIds(DataRecord newDestinationRecord, DataRecord oldDestinationRecord)
    {
        for (T8DataStringParticleSettings newParticleSettings : newDestinationRecord.getDataStringParticleSettings())
        {
            boolean found;

            // Find an old settings object matching the new one.
            found = false;
            for (T8DataStringParticleSettings oldParticleSettings : oldDestinationRecord.getDataStringParticleSettings())
            {
                // Match old and new settings based on target.
                if (oldParticleSettings.isEqualTarget(newParticleSettings))
                {
                    newParticleSettings.setId(oldParticleSettings.getId());
                    found = true;
                    break;
                }
            }

            // If a match was not found, we have to assign a new id to the settings object.
            if (!found)
            {
                String newId;
                String oldId;

                newId = T8IdentifierUtilities.createNewGUID();
                oldId = newParticleSettings.getId();
                newParticleSettings.setId(newId);
                particleSettingsIdMapping.put(oldId, newId);
            }
        }

        // Now process descendant records.
        for (DataRecord newDestinationSubRecord : newDestinationRecord.getSubRecords())
        {
            DataRecord oldDestinationSubRecord;

            oldDestinationSubRecord = oldDestinationRecord.getSubRecord(newDestinationSubRecord.getID());
            if (oldDestinationSubRecord != null)
            {
                applyParticleSettingsIds(newDestinationSubRecord, oldDestinationSubRecord);
            }
            else
            {
                particleSettingsIdMapping.putAll(newDestinationSubRecord.assignNewDataStringParticleSettingsIds(true));
            }
        }
    }
}
