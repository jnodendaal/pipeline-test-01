package com.pilog.t8.definition.data;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.model.T8DataModelDefinition;
import java.util.ArrayList;
import java.util.Map;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8CoreDataModelDefinition extends T8DataModelDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_MODEL_ORGANIZATION";
    public static final String DISPLAY_NAME = "Organization Data Model";
    public static final String DESCRIPTION = "A definition of the data model to use for organization data.";
    public enum Datum {};
    // -------- Definition Meta-Data -------- //

    public T8CoreDataModelDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public String getOntologyConceptDEID()
    {
        return ONTOLOGY_CONCEPT_DE_IDENTIFIER;
    }

    public String getOntologyConceptGraphDEID()
    {
        return ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;
    }

    public String getOntologyTermDEID()
    {
        return ONTOLOGY_TERM_DE_IDENTIFIER;
    }

    public String getOntologyCodeDEID()
    {
        return ONTOLOGY_CODE_DE_IDENTIFIER;
    }

    public String getOntologyDefinitionDEID()
    {
        return ONTOLOGY_DEFINITION_DE_IDENTIFIER;
    }

    public String getOntologyAbbreviationDEID()
    {
        return ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;
    }

    public String getOntologyDataTypeDEID()
    {
        return ONTOLOGY_CLASS_DE_IDENTIFIER;
    }

    public String getRenderingDEID()
    {
        return DATA_STRING_DE_IDENTIFIER;
    }

    public String getDataStringDetailsDEID()
    {
        return DATA_STRING_DETAILS_DE_IDENTIFIER;
    }

    public String getDataStringInstanceLinkDetailsDEID()
    {
        return DATA_STRING_INSTANCE_LINK_DETAILS_DE_IDENTIFIER;
    }

    public String getDataStringInstanceDetailsDEID()
    {
        return DATA_STRING_INSTANCE_DETAILS_DE_IDENTIFIER;
    }

    public String getDataRecordDEID()
    {
        return DATA_RECORD_DE_IDENTIFIER;
    }

    public String getDataRecordPropertyDEID()
    {
        return DATA_RECORD_PROPERTY_DE_IDENTIFIER;
    }

    public String getDataRecordPropertyDataTypeDEID()
    {
        return DATA_RECORD_PROPERTY_DATA_TYPE_DE_IDENTIFIER;
    }

    public String getDataRecordTagDataEntityId()
    {
        return DATA_RECORD_TAG_DE_IDENTIFIER;
    }

    public String getDataRecordDocAttrDataEntityId()
    {
        return DATA_RECORD_DOC_ATR_DE_IDENTIFIER;
    }

    public String getDataRecordValueDEID()
    {
        return DATA_RECORD_VALUE_DE_IDENTIFIER;
    }

    public String getDataRecordChildrenDEID()
    {
        return DATA_RECORD_CHILDREN_DE_IDENTIFIER;
    }

    public String getDataRecordDocumentDEID()
    {
        return DATA_RECORD_DOCUMENT_DE_IDENTIFIER;
    }

    public String getDataRecordDocumentClassDEID()
    {
        return DATA_RECORD_DOCUMENT_CLASS_DE_IDENTIFIER;
    }

    public String getDataRecordDocumentRootDEID()
    {
        return DATA_RECORD_DOCUMENT_ROOT_DE_IDENTIFIER;
    }

    public String getDataRecordAttachmentDEID()
    {
        return DATA_RECORD_ATTACHMENT_DE_IDENTIFIER;
    }

    public String getDataRecordAttachmentDetailsDEID()
    {
        return DATA_RECORD_ATTACHMENT_DETAILS_DE_IDENTIFIER;
    }

    public String getDataRecordMatchDEID()
    {
        return DATA_RECORD_MATCH_DE_IDENTIFIER;
    }

    public String getDataRecordMatchResolvedDEID()
    {
        return DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;
    }

    public String getDataRecordSearchDEID()
    {
        return DATA_RECORD_SEARCH_DE_IDENTIFIER;
    }

    public String getDataAlterationIndexDEID()
    {
        return DATA_ALTERATION_PACKAGE_INDEX_DE_IDENTIFIER;
    }

    public String getStandardValueDEID()
    {
        return STANDARD_VALUE_DE_IDENTIFIER;
    }

    public String getDataRequirementDEID()
    {
        return DATA_REQUIREMENT_DE_IDENTIFIER;
    }

    public String getDataRequirementChildrenDEID()
    {
        return DATA_REQUIREMENT_CHILDREN_DE_IDENTIFIER;
    }

    public String getDataRequirementDataTypeDEID()
    {
        return DATA_REQUIREMENT_DATA_TYPE_DE_IDENTIFIER;
    }

    public String getDataRequirementDescendantsDEID()
    {
        return DATA_REQUIREMENT_DESCENDANTS_DE_IDENTIFIER;
    }

    public String getDataRequirementDocumentDEID()
    {
        return DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER;
    }

    public String getDataRequirementInstanceDEID()
    {
        return DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER;
    }

    public String getDataRequirementInstanceDescendantsDEID()
    {
        return DATA_REQUIREMENT_INSTANCE_DESCENDANTS_DE_IDENTIFIER;
    }

    public String getDataRequirementInstanceDocumentDEID()
    {
        return DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER;
    }
    public String getDataRequirementPropertyDEID()
    {
        return DATA_REQUIREMENT_PROPERTY_DE_IDENTIFIER;
    }

    public String getDataRequirementValueDEID()
    {
        return DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;
    }

    public String getOrganizationRootDEID()
    {
        return ORG_ROOT_DE_IDENTIFIER;
    }

    public String getOrganizationStructureDEID()
    {
        return ORG_STRUCTURE_DE_IDENTIFIER;
    }

    public String getOrganizationStructurePathDEID()
    {
        return ORG_STRUCTURE_PATH_DE_IDENTIFIER;
    }

    public String getOrganizationDataRequirementCommentDEID()
    {
        return ORG_DR_COMMENT_DE_IDENTIFIER;
    }

    public String getOrganizationDataRequirementValueDEID()
    {
        return DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;
    }

    public String getOrganizationOntologyDEID()
    {
        return ORG_ONTOLOGY_DE_IDENTIFIER;
    }

    public String getOrganizationOntologyConceptCompleteDEID()
    {
        return ORG_ONTOLOGY_CONCEPT_COMPLETE_DE_IDENTIFIER;
    }

    public String getOrganizationOntologyTerminologyDEID()
    {
        return ORG_ONTOLOGY_TERMINOLOGY_DE_IDENTIFIER;
    }

    public String getOrganizationOntologyConceptTermDEID()
    {
        return ORG_ONTOLOGY_CONCEPT_TERM_DE_IDENTIFIER;
    }

    public String getOrganizationOntologyConceptDefinitionDEID()
    {
        return ORG_ONTOLOGY_CONCEPT_DEFINITION_DE_IDENTIFIER;
    }

    public String getOrganizationOntologyConceptCodeDEID()
    {
        return ORG_ONTOLOGY_CONCEPT_CODE_DE_IDENTIFIER;
    }

    public String getOrganizationTerminologyDEID()
    {
        return ORG_TERMINOLOGY_DE_IDENTIFIER;
    }
}
