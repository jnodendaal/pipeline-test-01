package com.pilog.t8.data.ontology;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.org.T8OntologyLink;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyConcept implements Serializable
{
    private final List<T8OntologyLink> ontologyLinks;
    private final List<T8OntologyDefinition> definitions;
    private final List<T8OntologyTerm> terms;
    private final List<T8OntologyCode> codes;
    private T8OntologyConceptType conceptType;
    private boolean standard;
    private boolean active;
    private String id;
    private String irdi;

    /**
     * Defines the possible constituent element types of a concept.
     */
    public enum T8ConceptElementType
    {
        CODE,
        TERM,
        ABBREVIATION,
        DEFINITION
    };

    /**
     * Creates a new instance of the {@code T8OntologyConcept}, for the
     * specified {@code T8OntologyConceptType}.
     *
     * @param id The {@code String} ID for the concept
     * @param conceptType The {@code T8OntologyConceptType} which defines the
     *      specific type of the concept
     * @param standard Whether or not the concept type has been standardized
     *      externally from the client data
     * @param active {@code true} if the concept should be active within the
     *      system
     */
    public T8OntologyConcept(String id, T8OntologyConceptType conceptType, boolean standard, boolean active)
    {
        this.id = id;
        this.conceptType = conceptType;
        this.standard = standard;
        this.active = active;
        this.terms = new ArrayList<>();
        this.definitions = new ArrayList<>();
        this.codes = new ArrayList<>();
        this.ontologyLinks = new ArrayList<>();
    }

    public T8OntologyConcept copy(boolean copyTerms, boolean copyAbbreviations, boolean copyDefinitions, boolean copyCodes, boolean includeOntologyLinks)
    {
        T8OntologyConcept copy;

        // Create the new concept copy.
        copy = new T8OntologyConcept(id, conceptType, standard, active);

        // Copy terms.
        if (copyTerms)
        {
            for (T8OntologyTerm term : getTerms())
            {
                copy.addTerm(term.copy(copyAbbreviations));
            }
        }

        // Copy definitions if required.
        if (copyDefinitions)
        {
            for (T8OntologyDefinition definition : getDefinitions())
            {
                copy.addDefinition(definition.copy());
            }
        }

        // Copy codes if required.
        if (copyCodes)
        {
            for (T8OntologyCode code : getCodes())
            {
                copy.addCode(code.copy());
            }
        }

        // Copy ontology links.
        if (includeOntologyLinks)
        {
            for (T8OntologyLink link : ontologyLinks)
            {
                copy.addOntologyLink(link.copy());
            }
        }

        return copy;
    }

    public void assignNewIDs()
    {
        setID(T8IdentifierUtilities.createNewGUID());
        assignNewTermIDs();
        assignNewAbbreviationIDs();
        assignNewDefinitionIDs();
        assignNewCodeIDs();
    }

    public void assignNewCodeIDs()
    {
        for (T8OntologyCode code : codes)
        {
            code.setID(T8IdentifierUtilities.createNewGUID());
        }
    }

    public void assignNewDefinitionIDs()
    {
        for (T8OntologyDefinition definition : definitions)
        {
            definition.setID(T8IdentifierUtilities.createNewGUID());
        }
    }

    public void assignNewTermIDs()
    {
        for (T8OntologyTerm term : terms)
        {
            term.setID(T8IdentifierUtilities.createNewGUID());
        }
    }

    public void assignNewAbbreviationIDs()
    {
        for (T8OntologyTerm term : terms)
        {
            term.assignNewAbbreviationIDs();
        }
    }

    /**
     * Returns a set of all concepts referenced by this concept.  The returned
     * set includes the ID of this concept itself.
     * @return A set of all concepts referenced by this concept
     */
    public Set<String> getConceptIDSet()
    {
        Set<String> conceptIDSet;

        conceptIDSet = new HashSet<>();
        if (id != null) conceptIDSet.add(id);
        conceptIDSet.addAll(getCodeTypeIDSet());
        conceptIDSet.add(conceptType.getConceptTypeID());

        return conceptIDSet;
    }

    /**
     * Returns the set of organization id's to which this concept is linked.
     * @return The set of organization id's to which this concept is linked.
     */
    public Set<String> getOrgIdSet()
    {
        Set<String> orgIdSet;

        orgIdSet = new HashSet<String>();
        for (T8OntologyLink link : ontologyLinks)
        {
            orgIdSet.add(link.getOrganizationID());
        }

        return orgIdSet;
    }

    /**
     * Returns a set of all the code types available on this concept.
     * @return A set of all Code Type ID's available on this concept.
     */
    public Set<String> getCodeTypeIDSet()
    {
        Set<String> codeTypeIDSet;

        codeTypeIDSet = new HashSet<>();
        for (T8OntologyCode code : codes)
        {
            codeTypeIDSet.add(code.getCodeTypeID());
        }

        return codeTypeIDSet;
    }

    /**
     * Returns all languages ID's used by this concept and its associated parts.
     * @return The complete set of all language ID's used by this concept.
     */
    public Set<String> getLanguageIDSet()
    {
        Set<String> languageIDSet;

        // Create the set.
        languageIDSet = new HashSet<>();
        languageIDSet.addAll(getTermLanguageIDSet());
        languageIDSet.addAll(getDefinitionLanguageIDSet());
        return languageIDSet;
    }

    /**
     * Returns the set of language ID's that are used by the terms of this
     * concept.
     * @return The set of language ID's that are used by the terms of this
     * concept.
     */
    public Set<String> getTermLanguageIDSet()
    {
        Set<String> languageIDSet;

        // Create the set.
        languageIDSet = new HashSet<>();

        // Add all term langauges
        for (T8OntologyTerm term : terms)
        {
            languageIDSet.add(term.getLanguageID());
        }

        return languageIDSet;
    }

    /**
     * Returns the set of language ID's that are used by the definitions of this
     * concept.
     * @return The set of language ID's that are used by the definitions of this
     * concept.
     */
    public Set<String> getDefinitionLanguageIDSet()
    {
        Set<String> languageIDSet;

        // Create the set.
        languageIDSet = new HashSet<>();

        // Add all definition langauges
        for (T8OntologyDefinition definition : definitions)
        {
            languageIDSet.add(definition.getLanguageID());
        }

        return languageIDSet;
    }

    /**
     * Returns the set of concept groups (ODT_ID's) that this concept is linked
     * to.
     * @return The set of concept groups (ODT_ID's) that this concept is linked
     * to.
     */
    public Set<String> getGroupIDSet()
    {
        Set<String> groupIDSet;

        groupIDSet = new HashSet<>();
        for (T8OntologyLink link : ontologyLinks)
        {
            groupIDSet.add(link.getOntologyClassID());
        }

        return groupIDSet;
    }

    /**
     * Returns the ID of the first group to which this concept is linked.  This
     * is a convenience method for cases where the concept is only linked to one
     * group.
     * @return The ID of the first group to which this concept is linked.
     */
    public String getGroupID()
    {
        if (ontologyLinks.size() > 0)
        {
            return ontologyLinks.get(0).getOntologyClassID();
        }
        else return null;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;

        // Set the concept ID of all organization links.
        for (T8OntologyLink link : ontologyLinks)
        {
            link.setConceptID(id);
        }

        // Set the concept ID of all terms.
        for (T8OntologyTerm term : terms)
        {
            term.setConceptID(id);
        }

        // Set the concept ID of all definitions.
        for (T8OntologyDefinition definition : definitions)
        {
            definition.setConceptID(id);
        }

        // Set the concept ID of all codes.
        for (T8OntologyCode code : codes)
        {
            code.setConceptID(id);
        }
    }

    public String getIrdi()
    {
        return irdi;
    }

    public void setIrdi(String irdi)
    {
        this.irdi = irdi;
    }

    public T8OntologyConceptType getConceptType()
    {
        return conceptType;
    }

    public void setConceptType(T8OntologyConceptType conceptType)
    {
        this.conceptType = conceptType;
    }

    public boolean isStandard()
    {
        return standard;
    }

    public void setStandard(boolean standard)
    {
        this.standard = standard;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getTermCount()
    {
        return terms.size();
    }

    public int getTermCountByLanguage(String languageId)
    {
        int count;

        count = 0;
        for (T8OntologyTerm term : terms)
        {
            if (term.getLanguageID().equals(languageId))
            {
                count++;
            }
        }

        return count;
    }

    public int getDefinitionCount()
    {
        return definitions.size();
    }

    public int getDefinitionCountByLanguage(String languageId)
    {
        int count;

        count = 0;
        for (T8OntologyDefinition definition : definitions)
        {
            if (definition.getLanguageID().equals(languageId))
            {
                count++;
            }
        }

        return count;
    }

    public int getCodeCount()
    {
        return codes.size();
    }

    public List<T8OntologyTerm> getTerms()
    {
        return new ArrayList<>(terms);
    }

    public T8OntologyTerm getTermByLanguage(String languageId)
    {
        for (T8OntologyTerm term : terms)
        {
            if (term.getLanguageID().equals(languageId)) return term;
        }

        return null;
    }

    public List<T8OntologyTerm> getTermsByLanguage(String languageId)
    {
        List<T8OntologyTerm> languageTerms;

        languageTerms = new ArrayList<>();
        for (T8OntologyTerm term : terms)
        {
            if (term.getLanguageID().equals(languageId))
            {
                languageTerms.add(term);
            }
        }

        return languageTerms;
    }

    public T8OntologyTerm getTerm(String termId)
    {
        if (termId == null) return null;
        else
        {
            for (T8OntologyTerm term : terms)
            {
                if (term.getID().equals(termId)) return term;
            }

            return null;
        }
    }

    public T8OntologyTerm getPrimaryTerm(String languageId)
    {
        for (T8OntologyTerm term : terms)
        {
            if ((term.getLanguageID().equals(languageId)) && (term.isPrimary())) return term;
        }

        return null;
    }

    public void setPrimaryTerm(String termId)
    {
        T8OntologyTerm primaryTerm;

        // First, find the term to set as primary.
        primaryTerm = getTerm(termId);
        if (primaryTerm != null)
        {
            String languageId;

            // Get the language id of the new primary term.
            languageId = primaryTerm.getLanguageID();
            for (T8OntologyTerm term : terms)
            {
                // Set all terms of the same language as the new primary term as non primary.
                if (term.getLanguageID().equals(languageId))
                {
                    term.setPrimary(term == primaryTerm);
                }
            }
        }
        else throw new IllegalArgumentException("Definition not found: " + termId);
    }

    public boolean containsTermWithID(String termID)
    {
        if (termID == null) return false;
        else
        {
            for (T8OntologyTerm term : terms)
            {
                if (term.getID().equals(termID)) return true;
            }

            return false;
        }
    }

    /**
     * Checks whether the specified term exists in this concept.
     * @param languageID The language ID to use.  If null, language is ignored.
     * @param termString The term to check for.
     * @param caseInsensitive If true, case will be ignored.
     * @return true if the specified term already exists.
     */
    public boolean containsTerm(String languageID, String termString, boolean caseInsensitive)
    {
        if (termString == null) return false;
        else
        {
            String matchString;

            matchString = termString.toUpperCase();
            for (T8OntologyTerm term : terms)
            {
                if ((languageID == null) || (languageID.equals(term.getLanguageID())))
                {
                    if (caseInsensitive)
                    {
                        if (term.getTerm().toUpperCase().equals(matchString)) return true;
                    }
                    else
                    {
                        if (term.getTerm().equals(termString)) return true;
                    }
                }
            }

            return false;
        }
    }

    public void setTerms(List<T8OntologyTerm> terms)
    {
        clearTerms();
        addTerms(terms);
    }

    public void clearTerms()
    {
        terms.clear();
    }

    public List<T8OntologyDefinition> getDefinitions()
    {
        return new ArrayList<>(definitions);
    }

    public T8OntologyDefinition getDefinitionByLanguage(String languageId)
    {
        for (T8OntologyDefinition definition : definitions)
        {
            if (definition.getLanguageID().equals(languageId)) return definition;
        }

        return null;
    }

    public List<T8OntologyDefinition> getDefinitionsByLanguage(String languageId)
    {
        List<T8OntologyDefinition> languageDefinitions;

        languageDefinitions = new ArrayList<>();
        for (T8OntologyDefinition definition : definitions)
        {
            if (definition.getLanguageID().equals(languageId))
            {
                languageDefinitions.add(definition);
            }
        }

        return languageDefinitions;
    }

    public T8OntologyDefinition getDefinition(String definitionId)
    {
        if (definitionId == null) return null;
        else
        {
            for (T8OntologyDefinition definition : definitions)
            {
                if (definition.getID().equals(definitionId)) return definition;
            }

            return null;
        }
    }

    public T8OntologyDefinition getPrimaryDefinition(String languageId)
    {
        for (T8OntologyDefinition definition : definitions)
        {
            if ((definition.getLanguageID().equals(languageId)) && (definition.isPrimary())) return definition;
        }

        return null;
    }

    public void setPrimaryDefinition(String definitionId)
    {
        T8OntologyDefinition primaryDefinition;

        // First, find the definition to set as primary.
        primaryDefinition = getDefinition(definitionId);
        if (primaryDefinition != null)
        {
            String languageId;

            // Get the language id of the new primary definition.
            languageId = primaryDefinition.getLanguageID();
            for (T8OntologyDefinition definition : definitions)
            {
                // Set all definitions of the same language as the new primary definition as non primary.
                if (definition.getLanguageID().equals(languageId))
                {
                    definition.setPrimary(definition == primaryDefinition);
                }
            }
        }
        else throw new IllegalArgumentException("Definition not found: " + definitionId);
    }

    public boolean containsDefinitionWithID(String definitionId)
    {
        if (definitionId == null) return false;
        else
        {
            for (T8OntologyDefinition definition : definitions)
            {
                if (definition.getID().equals(definitionId)) return true;
            }

            return false;
        }
    }

    /**
     * Checks whether the specified definition exists in this concept.
     * @param languageID The language ID to use.  If null, language is ignored.
     * @param definitionString The definition to check for.
     * @param caseInsensitive If true, case will be ignored.
     * @return true if the specified definition already exists.
     */
    public boolean containsDefinition(String languageID, String definitionString, boolean caseInsensitive)
    {
        if (definitionString == null) return false;
        else
        {
            String matchString;

            matchString = definitionString.toUpperCase();
            for (T8OntologyDefinition definition : definitions)
            {
                if ((languageID == null) || (languageID.equals(definition.getLanguageID())))
                {
                    if (caseInsensitive)
                    {
                        if (definition.getDefinition().toUpperCase().equals(matchString)) return true;
                    }
                    else
                    {
                        if (definition.getDefinition().equals(definitionString)) return true;
                    }
                }
            }

            return false;
        }
    }

    public void setDefinitions(List<T8OntologyDefinition> definitions)
    {
        clearDefinitions();
        addDefinitions(definitions);
    }

    public void clearDefinitions()
    {
        definitions.clear();
    }

    public List<T8OntologyCode> getCodes()
    {
        return new ArrayList<>(codes);
    }

    public T8OntologyCode getCode(String codeId)
    {
        if (codeId == null) return null;
        else
        {
            for (T8OntologyCode code : codes)
            {
                if (code.getID().equals(codeId)) return code;
            }

            return null;
        }
    }

    public T8OntologyCode getPrimaryCode()
    {
        for (T8OntologyCode code : codes)
        {
            if (code.isPrimary()) return code;
        }

        return null;
    }

    public void setPrimaryCode(String codeId)
    {
        for (T8OntologyCode code : codes)
        {
            code.setPrimary(code.getID().equals(codeId));
        }
    }

    public boolean containsCodeWithID(String codeID)
    {
        if (codeID == null) return false;
        else
        {
            for (T8OntologyCode code : codes)
            {
                if (code.getID().equals(codeID))
                {
                    return true;
                }
            }

            return false;
        }
    }

    public boolean containsCode(String codeString)
    {
        if (codeString == null) return false;
        else
        {
            for (T8OntologyCode code : codes)
            {
                if (code.getCode().equals(codeString)) return true;
            }

            return false;
        }
    }

    public T8OntologyCode getCodeByType(String codeTypeID)
    {
        if (codeTypeID == null) return null;
        else
        {
            for (T8OntologyCode code : codes)
            {
                if (codeTypeID.equals(code.getCodeTypeID())) return code;
            }

            return null;
        }
    }

    public void setCodes(List<T8OntologyCode> codes)
    {
        clearCodes();
        addCodes(codes);
    }

    public void clearCodes()
    {
        codes.clear();
    }

    public void addTerms(List<T8OntologyTerm> terms)
    {
        for (T8OntologyTerm term : terms)
        {
            addTerm(term);
        }
    }

    public void addTerm(T8OntologyTerm term)
    {
        String termConceptID;

        termConceptID = term.getConceptID();
        if (termConceptID == null) term.setConceptID(id);
        else if (!Objects.equals(id, termConceptID)) throw new IllegalArgumentException("Term contains Concept ID '" + termConceptID + "' that does not correspond to Concept where it is being added: " + id);

        terms.add(term);
    }

    public boolean removeTerm(T8OntologyTerm term)
    {
        return terms.remove(term);
    }

    public void addAbbreviations(List<T8OntologyAbbreviation> abbreviations)
    {
        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            addAbbreviation(abbreviation);
        }
    }

    public void addAbbreviation(T8OntologyAbbreviation abbreviation)
    {
        if (abbreviation.getTermID() == null)
        {
            if (terms.size() == 1)
            {
                terms.get(0).addAbbreviation(abbreviation);
            }
            else throw new IllegalArgumentException("Ambiguous operation.  Cannot add an abbreviation with null term ID to a concept that has more than one term linked to it.");
        }
        else
        {
            for (T8OntologyTerm term : terms)
            {
                if (term.getID().equals(abbreviation.getTermID()))
                {
                    term.addAbbreviation(abbreviation);
                    return;
                }
            }

            throw new IllegalArgumentException("Abbreviation contains Term ID '" + abbreviation.getTermID()+ "' that does not correspond to a Term currently listed with the specified Concept where it is being added: " + id);
        }
    }

    public void addAbbreviation(String termID, T8OntologyAbbreviation abbreviation)
    {
        T8OntologyTerm term;

        term = getTerm(termID);
        if (term != null)
        {
            term.addAbbreviation(abbreviation);
        }
        else throw new IllegalArgumentException("Term not found: " + termID);
    }

    public void addDefinitions(List<T8OntologyDefinition> definitions)
    {
        for (T8OntologyDefinition definition : definitions)
        {
            addDefinition(definition);
        }
    }

    public void addDefinition(T8OntologyDefinition definition)
    {
        String definitionConceptID;

        definitionConceptID = definition.getConceptID();
        if (definitionConceptID == null) definition.setConceptID(id);
        else if (!Objects.equals(id, definitionConceptID)) throw new IllegalArgumentException("Definition contains Concept ID '" + definitionConceptID + "' that does not correspond to Concept where it is being added: " + id);

        definitions.add(definition);
    }

    public boolean removeDefinition(T8OntologyDefinition definition)
    {
        return definitions.remove(definition);
    }

    public void addCodes(List<T8OntologyCode> codes)
    {
        for (T8OntologyCode code : codes)
        {
            addCode(code);
        }
    }

    public void addCode(T8OntologyCode code)
    {
        String codeConceptID;

        codeConceptID = code.getConceptID();
        if (codeConceptID == null) code.setConceptID(id);
        else if (!Objects.equals(id, codeConceptID)) throw new IllegalArgumentException("Code contains Concept ID '" + codeConceptID + "' that does not correspond to Concept where it is being added: " + id);

        codes.add(code);
    }

    public boolean removeCode(T8OntologyCode code)
    {
        return codes.remove(code);
    }

    public void mergeOntology(T8OntologyConcept concept)
    {
        mergeTerms(concept.getTerms());
        mergeDefinitions(concept.getDefinitions());
        mergeCodes(concept.getCodes());
    }

    public void mergeTerms(List<T8OntologyTerm> terms)
    {
        // TODO: merge the terms, checking on ID.
    }

    public void mergeDefinitions(List<T8OntologyDefinition> definitions)
    {
        // TODO: merge the definitions, checking on ID.
    }

    public void mergeCodes(List<T8OntologyCode> codes)
    {
        if (codes != null)
        {
            for (T8OntologyCode code : codes)
            {
                // If we don't already have this code, add it.
                if (!containsEquivalentCode(code))
                {
                    T8OntologyCode copiedCode;

                    copiedCode = code.copy();
                    copiedCode.setConceptID(id);
                    addCode(copiedCode);
                }
            }
        }
    }

    /**
     * @deprecated Use {@code addOntologyLink}.
     */
    @Deprecated
    public void addOrganizationLink(T8OntologyLink ontologyLink)
    {
        ontologyLinks.add(ontologyLink);
    }

    /**
     * @deprecated Use {@code getOntologyLinks}.
     */
    @Deprecated
    public List<T8OntologyLink> getOrganizationLinks()
    {
        return new ArrayList<>(ontologyLinks);
    }

    public void addOntologyLink(T8OntologyLink ontologyLink)
    {
        if (ontologyLink != null)
        {
            ontologyLink.setConceptID(id);
            ontologyLinks.add(ontologyLink);
        }
        else throw new IllegalArgumentException("Cannot add null ontology link to concept.");
    }

    public void addOntologyLinks(Collection<T8OntologyLink> links)
    {
        for (T8OntologyLink link : links)
        {
            addOntologyLink(link);
        }
    }

    public boolean removeOntologyLink(T8OntologyLink link)
    {
        return ontologyLinks.remove(link);
    }

    public void removeOntologyLinks(Collection<T8OntologyLink> links)
    {
        this.ontologyLinks.removeAll(links);
    }

    public List<T8OntologyLink> getOntologyLinks()
    {
        return new ArrayList<>(ontologyLinks);
    }

    /**
     * Checks whether or not this concept is exactly equivalent to the supplied input concept.
     * Component parts of the concept are not checked.
     * @param concept
     * @return True if this component is equivalent to the input concept.
     */
    public boolean isEqualTo(T8OntologyConcept concept)
    {
        if (!Objects.equals(concept.getID(), id)) return false;
        else if (!Objects.equals(concept.getIrdi(), irdi)) return false;
        else if (!Objects.equals(concept.getConceptType(), conceptType)) return false;
        else if (!Objects.equals(concept.isActive(), active)) return false;
        else if (!Objects.equals(concept.isStandard(), standard)) return false;
        else return true;
    }

    public boolean containsEquivalentCode(T8OntologyCode code)
    {
        for (T8OntologyCode ontologyCode : codes)
        {
            if (ontologyCode.isEqualTo(code)) return true;
        }

        return false;
    }

    public boolean containsEquivalentLink(T8OntologyLink link)
    {
        for (T8OntologyLink organizationLink : ontologyLinks)
        {
            if (organizationLink.isEquivalentLink(link)) return true;
        }

        return false;
    }

    public void setOrganizationLinks(List<T8OntologyLink> organizationLinks)
    {
        clearOrganizationLinks();
        if (organizationLinks != null) this.ontologyLinks.addAll(organizationLinks);
    }

    /**
     * Use {@code clearOntologyLinks}.
     * @deprecated
     */
    @Deprecated
    public void clearOrganizationLinks()
    {
        this.ontologyLinks.clear();
    }

    public void clearOntologyLinks()
    {
        this.ontologyLinks.clear();
    }

    public T8ConceptTerminologyList createTerminologies(String orgId)
    {
        T8ConceptTerminologyList terminologyList;
        Set<String> languageIDSet;

        terminologyList = new T8ConceptTerminologyList();
        languageIDSet = getLanguageIDSet();
        for (String languageId : languageIDSet)
        {
            terminologyList.add(createTerminology(languageId, orgId));
        }

        return terminologyList;
    }

    public T8ConceptTerminology createTerminology(String languageId, String orgId)
    {
        T8ConceptTerminology terminology;
        T8OntologyDefinition definition;
        T8OntologyCode code;
        T8OntologyTerm term;

        // Create a new terminology object.
        terminology = new T8ConceptTerminology(conceptType, id);
        terminology.setOrganizationId(orgId);
        terminology.setLanguageId(languageId);

        // Add the Term and Abbreviation if available.
        term = getPrimaryTerm(languageId); // Use the primary term.
        if (term == null) term = getTermByLanguage(languageId); // Primary term not specified, so use the first one in the required language.
        if (term != null)
        {
            T8OntologyAbbreviation abbreviation;

            // Add the term to the terminology.
            terminology.setTermId(term.getID());
            terminology.setTerm(term.getTerm());

            // Add the abbreviation if available.
            abbreviation = term.getPrimaryAbbreviation(); // Use the primary abbreviation.
            if (abbreviation == null) abbreviation = term.getFirstAbbreviation(); // Primary abbreviation not specified, so use the first one available.
            if (abbreviation != null)
            {
                terminology.setAbbreviationId(abbreviation.getID());
                terminology.setAbbreviation(abbreviation.getAbbreviation());
            }
        }

        // Add the Definition if available.
        definition = getPrimaryDefinition(languageId); // Use the primary definition.
        if (definition == null) definition = getDefinitionByLanguage(languageId); // Primary definition not specified, so use the first one in the required language.
        if (definition != null)
        {
            // Add the definition to the terminology.
                terminology.setDefinitionId(definition.getID());
                terminology.setDefinition(definition.getDefinition());
        }

        // Add the Code if available.
        code = getPrimaryCode(); // Use the primary code.
        if (code == null) code = codes.size() > 0 ? codes.get(0) : null; // Primary code not specified, so use the first one available (if any).
        if (code != null)
        {
            terminology.setCodeId(code.getID());
            terminology.setCode(code.getCode());
        }

        // Return the complete terminology.
        return terminology;
    }
}
