package com.pilog.t8.data.document.integration.structure.sap;

import java.util.List;

/**
 *
 * @author Andre Scheepers
 */
public class SAPDataRow
{

    /**
     * @return the fields
     */
    public List<SAPField> getFields()
    {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<SAPField> fields)
    {
        this.fields = fields;
    }
    private List<SAPField> fields;
}