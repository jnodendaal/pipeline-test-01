package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.type.T8CompleteConceptAlteration;
import com.pilog.t8.data.alteration.type.T8DataRequirementAlteration;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataAlteration extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_ALTERATION";

    public T8DtDataAlteration(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8DataAlteration.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            if (object instanceof T8DataAlteration)
            {
                T8DataAlteration alteration;

                alteration = (T8DataAlteration)object;
                return alteration.serialize();
            }
            else throw new IllegalArgumentException("Type expected: " + getDataTypeClassName() + " Type found: " + object.getClass().getCanonicalName());
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            JsonObject alterationObject;
            String alterationId;

            alterationObject = jsonValue.asObject();
            alterationId = alterationObject.getString("alterationId");
            if (alterationId != null)
            {
                switch (alterationId)
                {
                    case T8CompleteConceptAlteration.ALTERATION_ID:
                        return new T8CompleteConceptAlteration(alterationObject);
                    case T8DataRequirementAlteration.ALTERATION_ID:
                        return new T8DataRequirementAlteration(alterationObject);
                    default:
                        throw new IllegalArgumentException("Unsupported data alteration JSON object: " + alterationObject);
                }
            }
            else throw new IllegalArgumentException("Cannot deserialize alteration from json object that does not contain required data: " + alterationObject);
        }
        else return null;
    }
}
