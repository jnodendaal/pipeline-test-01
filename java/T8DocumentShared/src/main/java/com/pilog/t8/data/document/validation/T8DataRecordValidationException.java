package com.pilog.t8.data.document.validation;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordValidationException extends Exception
{
    private final T8DataFileValidationReport validationReport;
    
    public T8DataRecordValidationException(String message, T8DataFileValidationReport validationReport)
    {
        super(message + "\n" + validationReport);
        this.validationReport = validationReport;
    }
    
    public T8DataFileValidationReport getValidationReport()
    {
        return validationReport;
    }
}
