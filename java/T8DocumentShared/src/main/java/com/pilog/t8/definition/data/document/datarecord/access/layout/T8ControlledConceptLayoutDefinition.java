package com.pilog.t8.definition.data.document.datarecord.access.layout;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.access.layout.T8ControlledConceptLayout.ControlledConceptLayoutType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ControlledConceptLayoutDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_ACCESS_CONTROLLED_CONCEPT_LAYOUT";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_RECORD_ACCESS_CONTROLLED_CONCEPT_LAYOUT";
    public static final String DISPLAY_NAME = "Controlled Concept Layout";
    public static final String DESCRIPTION = "A definition that specifies layout of a Controlled Concept Data Type.";
    public static final String IDENTIFIER_PREFIX = "CONTROLLED_CONCEPT_LAYOUT_";
    public enum Datum {LAYOUT_TYPE};
    // -------- Definition Meta-Data -------- //
    
    public T8ControlledConceptLayoutDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.LAYOUT_TYPE.toString(), "Layout Type", "The layout type to apply to the editor.", ControlledConceptLayoutType.COMBO_BOX.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.LAYOUT_TYPE.toString().equals(datumIdentifier)) return createStringOptions(ControlledConceptLayoutType.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }
    
    public ControlledConceptLayoutType getLayoutType()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.LAYOUT_TYPE.toString());
        return value != null ? ControlledConceptLayoutType.valueOf(value) : ControlledConceptLayoutType.COMBO_BOX;
    }
    
    public void setLayoutType(ControlledConceptLayoutType layoutType)
    {
        setDefinitionDatum(Datum.LAYOUT_TYPE.toString(), layoutType.toString());
    }
}
