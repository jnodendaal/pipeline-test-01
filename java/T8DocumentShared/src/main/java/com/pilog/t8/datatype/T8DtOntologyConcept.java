package com.pilog.t8.datatype;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.org.T8OntologyLink;

/**
 *
 * @author Bouwer du Preez
 */
public class T8DtOntologyConcept extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@CONCEPT";

    public T8DtOntologyConcept(T8DefinitionManager definitionManager)
    {
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8OntologyConcept.class.getCanonicalName();
    }

    @Override
    public T8OntologyConcept deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8OntologyConcept concept;
            JsonArray ontologyLinkArray;
            JsonArray definitionArray;
            JsonArray termArray;
            JsonArray codeArray;
            String conceptId;
            JsonObject jsonObject;
            T8OntologyConceptType conceptType;
            Boolean standard;
            Boolean active;

            // Get the content from the object.
            jsonObject = jsonValue.asObject();
            conceptType = T8OntologyConceptType.getConceptType(jsonObject.getString("conceptTypeId"));
            conceptId = jsonObject.getString("conceptId");
            standard = jsonObject.getBoolean("standard");
            active = jsonObject.getBoolean("active");

            // Create the concept object.
            concept = new T8OntologyConcept(conceptId,conceptType, standard, active);
            concept.setIrdi(jsonObject.getString("irdi"));

            // Add all the definitions to the concept.
            definitionArray = jsonObject.getJsonArray("definitions");
            if (definitionArray != null)
            {
                for (int definitionIndex = 0; definitionIndex < definitionArray.size(); definitionIndex++)
                {
                    T8OntologyDefinition definition;

                    definition = deserializeConceptDefinition(conceptId, definitionArray.getJsonObject(definitionIndex));
                    concept.addDefinition(definition);
                }
            }

            // Add all the codes to the concept.
            codeArray = jsonObject.getJsonArray("codes");
            if (codeArray != null)
            {
                for (int codeIndex = 0; codeIndex < codeArray.size(); codeIndex++)
                {
                    T8OntologyCode code;

                    code = deserializeConceptCode(conceptId, codeArray.getJsonObject(codeIndex));
                    concept.addCode(code);
                }
            }

            // Add all the terms to the concept.
            termArray = jsonObject.getJsonArray("terms");
            if (termArray != null)
            {
                for (int termIndex = 0; termIndex < termArray.size(); termIndex++)
                {
                    T8OntologyTerm term;

                    term = deserializeConceptTerm(conceptId, termArray.getJsonObject(termIndex));
                    concept.addTerm(term);
                }
            }

            // Add all the ontology links to the concept.
            ontologyLinkArray = jsonObject.getJsonArray("ontologyLinks");
            if (ontologyLinkArray != null)
            {
                for (int ontologyLinkIndex = 0; ontologyLinkIndex < ontologyLinkArray.size(); ontologyLinkIndex++)
                {
                    T8OntologyLink ontologyLink;

                    ontologyLink = deserializeConceptOntologyLink(conceptId, ontologyLinkArray.getJsonObject(ontologyLinkIndex));
                    concept.addOntologyLink(ontologyLink);
                }
            }

            return concept;
        }
        else return null;
    }

    public static T8OntologyTerm deserializeConceptTerm(String conceptId , JsonObject object)
    {
        T8OntologyTerm termObject;
        String termId;
        String termIrdi;
        String languageId;
        String term;
        Boolean standard;
        Boolean active;
        JsonArray abbreviationArray;

        // Get all values from Json Object.
        termId = object.getString("termId");
        termIrdi = object.getString("termIrdi");
        languageId = object.getString("languageId");
        term = object.getString("term");
        standard = object.getBoolean("standard");
        active = object.getBoolean("active");

        // Create the object.
        termObject = new T8OntologyTerm(termId, conceptId, languageId, term, standard, active);
        termObject.setIrdi(termIrdi);

        // Add all the abbreviations to the concept.
        abbreviationArray = object.getJsonArray("abbreviations");
        if (abbreviationArray != null)
        {
            for (int abbreviationIndex = 0; abbreviationIndex < abbreviationArray.size(); abbreviationIndex++)
            {
                T8OntologyAbbreviation abbreviationObject;

                abbreviationObject = deserializeConceptAbbreviation(termId, abbreviationArray.getJsonObject(abbreviationIndex));
                termObject.addAbbreviation(abbreviationObject);
            }
        }

        // Return the term object.
        return termObject;
    }

    public static T8OntologyAbbreviation deserializeConceptAbbreviation(String termId , JsonObject object)
    {
        T8OntologyAbbreviation abbreviationObject;
        String abbreviationId;
        String abbreviationIrdi;
        String abbreviation;
        Boolean standard;
        Boolean active;

        // Get all values from Json Object.
        abbreviationId = object.getString("abbreviationId");
        abbreviationIrdi = object.getString("abbreviationIrdi");
        abbreviation = object.getString("abbreviation");
        standard = object.getBoolean("standard");
        active = object.getBoolean("active");

        // Create the object.
        abbreviationObject = new T8OntologyAbbreviation(abbreviationId, termId, abbreviation, standard, active);
        abbreviationObject.setIrdi(abbreviationIrdi);

        // Return the abbreviation object.
        return abbreviationObject;
    }

    public static T8OntologyLink deserializeConceptOntologyLink(String conceptId , JsonObject object)
    {
        T8OntologyLink ontologyLinkObject;
        String organizationId;
        String ontologyStructureId;
        String ontologyClassId;

        // Get all values from Json Object.
        organizationId = object.getString("organizationId");
        ontologyStructureId = object.getString("ontologyStructureId");
        ontologyClassId = object.getString("ontologyClassId");

        // Create the object
        ontologyLinkObject = new T8OntologyLink(organizationId, ontologyStructureId, ontologyClassId, conceptId);

        // Return the ontology link object.
        return ontologyLinkObject;
    }

    public static T8OntologyDefinition deserializeConceptDefinition(String conceptId , JsonObject object)
    {
        T8OntologyDefinition definitionObject;
        String definitionId;
        String definitionIrdi;
        String languageId;
        String definition;
        Boolean standard;
        Boolean active;

        // Get all values from Json Object.
        definitionId = object.getString("definitionId");
        definitionIrdi = object.getString("definitionIrdi");
        languageId = object.getString("languageId");
        definition = object.getString("definition");
        standard = object.getBoolean("standard");
        active = object.getBoolean("active");

        // Create the object.
        definitionObject = new T8OntologyDefinition(definitionId, conceptId, languageId, definition, standard, active);
        definitionObject.setIrdi(definitionIrdi);

        // Return the definition object.
        return definitionObject;
    }

    public static T8OntologyCode deserializeConceptCode(String conceptId , JsonObject object)
    {
        T8OntologyCode codeObject;
        String codeId;
        String codeTypeId;
        String code;
        Boolean standard;
        Boolean active;

        // Get all values from Json Object.
        codeId = object.getString("codeId");
        codeTypeId = object.getString("codeTypeId");
        code = object.getString("code");
        standard = object.getBoolean("standard");
        active = object.getBoolean("active");

        // Create the object.
        codeObject = new T8OntologyCode(codeId, conceptId, codeTypeId, code, standard, active);

        // Return the code object.
        return codeObject;
    }

    @Override
    public JsonValue serialize(Object object)
    {
        T8OntologyConcept concept;

        concept = (T8OntologyConcept)object;

        // Do serialization.
        if (concept != null)
        {
            JsonObject conceptObject;
            JsonArray ontologyLinkArray;
            JsonArray definitionArray;
            JsonArray termArray;
            JsonArray codeArray;
            T8OntologyConceptType conceptType;

            // Retrieve concept type.
            conceptType = concept.getConceptType();

            // Create the term array.
            termArray = new JsonArray();
            for (T8OntologyTerm term : concept.getTerms())
            {
                termArray.add(serializeConceptTerm(term));
            }

            // Create the code array.
            codeArray = new JsonArray();
            for (T8OntologyCode code : concept.getCodes())
            {
                codeArray.add(serializeConceptCode(code));
            }

            // Create the definition array.
            definitionArray = new JsonArray();
            for (T8OntologyDefinition definition : concept.getDefinitions())
            {
                definitionArray.add(serializeConceptDefinition(definition));
            }

            // Create the ontologyLink array.
            ontologyLinkArray = new JsonArray();
            for (T8OntologyLink ontologyLink : concept.getOntologyLinks())
            {
                ontologyLinkArray.add(serializeConceptOntologyLink(ontologyLink));
            }

            // Create the concept JSON object.
            conceptObject = new JsonObject();
            conceptObject.add("conceptId", concept.getID());
            conceptObject.add("terms", termArray);
            conceptObject.add("definitions", definitionArray);
            conceptObject.add("codes", codeArray);
            conceptObject.add("ontologyLinks", ontologyLinkArray);
            conceptObject.add("standard", concept.isStandard());
            conceptObject.add("active", concept.isActive());
            conceptObject.addIfNotNull("irdi", concept.getIrdi());
            conceptObject.add("conceptTypeId", conceptType.getConceptTypeID());
            conceptObject.add("conceptTypeTerm", conceptType.getDisplayName());

            // Retrurn the complete concept.
            return conceptObject;
        }
        else return JsonValue.NULL;
    }

    public static JsonObject serializeConceptDefinition(T8OntologyDefinition definition)
    {
        JsonObject definitionObject;

        // Create the definition JSON object.
        definitionObject = new JsonObject();
        definitionObject.add("definitionId", definition.getID());
        definitionObject.add("definitionIrdi", definition.getIrdi());
        definitionObject.add("languageId", definition.getLanguageID());
        definitionObject.add("definition", definition.getDefinition());
        definitionObject.add("standard", definition.isStandard());
        definitionObject.add("active", definition.isActive());

        // Return the definition object
        return definitionObject;
    }

    public static JsonObject serializeConceptTerm(T8OntologyTerm term)
    {
        JsonObject termObject;
        JsonArray abbreviationArray;

        // Create the abbreviation array.
        abbreviationArray = new JsonArray();
        for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
        {
            abbreviationArray.add(serializeConceptAbbreviation(abbreviation));
        }

        // Create the term JSON object.
        termObject = new JsonObject();
        termObject.add("termId", term.getID());
        termObject.add("termIrdi", term.getIrdi());
        termObject.add("languageId", term.getLanguageID());
        termObject.add("term", term.getTerm());
        termObject.add("standard", term.isStandard());
        termObject.add("active", term.isActive());
        termObject.add("abbreviations", abbreviationArray);

        // Return the term object
        return termObject;
    }

    public static JsonObject serializeConceptAbbreviation(T8OntologyAbbreviation abbreviation)
    {
        JsonObject abbreviationObject;

        // Create the abbreviation JSON object.
        abbreviationObject = new JsonObject();
        abbreviationObject.add("abbreviationId", abbreviation.getID());
        abbreviationObject.add("abbreviationIrdi", abbreviation.getIrdi());
        abbreviationObject.add("abbreviation", abbreviation.getAbbreviation());
        abbreviationObject.add("standard", abbreviation.isStandard());
        abbreviationObject.add("active", abbreviation.isActive());

        // Return the abbreviation object
        return abbreviationObject;
    }

    public static JsonObject serializeConceptCode(T8OntologyCode code)
    {
        JsonObject codeObject;

        // Create the code JSON object.
        codeObject = new JsonObject();
        codeObject.add("codeId", code.getID());
        codeObject.add("codeTypeId", code.getCodeTypeID());
        codeObject.add("code", code.getCode());
        codeObject.add("standard", code.isStandard());
        codeObject.add("active", code.isActive());

        // Return the code object
        return codeObject;
    }

    public static JsonObject serializeConceptOntologyLink(T8OntologyLink ontologyLink)
    {
        JsonObject ontologyLinkObject;

        // Create the ontology link JSON object.
        ontologyLinkObject = new JsonObject();
        ontologyLinkObject.add("organizationId", ontologyLink.getOrganizationID());
        ontologyLinkObject.add("ontologyStructureId", ontologyLink.getOntologyStructureID());
        ontologyLinkObject.add("ontologyClassId", ontologyLink.getOntologyClassID());

        // Return the ontology link object
        return ontologyLinkObject;
    }
}
