package com.pilog.t8.data.document;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface TerminologyCollector
{
    public void setLanguage(String languageId);
    public void addTerminology(List<T8ConceptTerminology> terminology);
    public void addTerminology(String languageId, T8OntologyConceptType conceptType, String conceptId, String codeId, String code, String termId, String term, String abbreviationId, String abbreviation, String definitionId, String definition);
}
