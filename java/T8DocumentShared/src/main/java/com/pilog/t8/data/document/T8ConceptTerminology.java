package com.pilog.t8.data.document;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptTerminology implements Serializable
{
    private T8OntologyConceptType conceptType;
    private String organizationId;
    private String ontologyStructureId;
    private String conceptId;
    private String conceptIrdi;
    private String languageId;
    private String languageIrdi;
    private String termId;
    private String termIrdi;
    private String term;
    private String definitionId;
    private String definitionIrdi;
    private String definition;
    private String abbreviationId;
    private String abbreviationIrdi;
    private String abbreviation;
    private String codeId;
    private String code;

    public T8ConceptTerminology(T8OntologyConceptType conceptType, String conceptID)
    {
        if (conceptID == null) throw new IllegalArgumentException("Cannot construct concept terminology with null concept ID.");
        if (conceptType == null) throw new IllegalArgumentException("Cannot construct concept terminology with null concept type.");
        this.conceptType = conceptType;
        this.conceptId = conceptID;
    }

    /**
     * @return
     * @deprecated Use {@code getOntologyStructureId()} instead.
     */
    @Deprecated
    public String getDataStructureID()
    {
        return ontologyStructureId;
    }

    /**
     *
     * @param dataStructureID
     * @deprecated Use {@code setOntologyStructureId()} instead.
     */
    @Deprecated
    public void setDataStructureID(String dataStructureID)
    {
        this.ontologyStructureId = dataStructureID;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    @Deprecated
    public String getConceptID()
    {
        return conceptId;
    }

    @Deprecated
    public void setConceptID(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public T8OntologyConceptType getConceptType()
    {
        return conceptType;
    }

    public void setConceptType(T8OntologyConceptType conceptType)
    {
        this.conceptType = conceptType;
    }

    public void clearDefinition()
    {
        definitionId = null;
        definition = null;
    }

    public void clearTerm()
    {
        termId = null;
        term = null;
    }

    public void clearCode()
    {
        codeId = null;
        code = null;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    @Deprecated
    public String getLanguageID()
    {
        return languageId;
    }

    @Deprecated
    public void setLanguageID(String languageId)
    {
        this.languageId = languageId;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    @Deprecated
    public String getTermID()
    {
        return termId;
    }

    @Deprecated
    public void setTermID(String termID)
    {
        this.termId = termID;
    }

    @Deprecated
    public String getDefinitionID()
    {
        return definitionId;
    }

    @Deprecated
    public void setDefinitionID(String definitionId)
    {
        this.definitionId = definitionId;
    }

    @Deprecated
    public String getAbbreviationID()
    {
        return abbreviationId;
    }

    @Deprecated
    public void setAbbreviationID(String abbreviationId)
    {
        this.abbreviationId = abbreviationId;
    }

    public String getCodeID()
    {
        return codeId;
    }

    public void setCodeID(String codeID)
    {
        this.codeId = codeID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    @Deprecated
    public String getOrganizationID()
    {
        return organizationId;
    }

    @Deprecated
    public void setOrganizationID(String organizationID)
    {
        this.organizationId = organizationID;
    }

    public String getOrganizationId()
    {
        return organizationId;
    }

    public void setOrganizationId(String organizationId)
    {
        this.organizationId = organizationId;
    }

    public String getOntologyStructureId()
    {
        return ontologyStructureId;
    }

    public void setOntologyStructureId(String ontologyStructureId)
    {
        this.ontologyStructureId = ontologyStructureId;
    }

    public String getConceptId()
    {
        return conceptId;
    }

    public void setConceptId(String conceptId)
    {
        this.conceptId = conceptId;
    }

    public String getConceptIrdi()
    {
        return conceptIrdi;
    }

    public void setConceptIrdi(String conceptIrdi)
    {
        this.conceptIrdi = conceptIrdi;
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getLanguageIrdi()
    {
        return languageIrdi;
    }

    public void setLanguageIrdi(String languageIrdi)
    {
        this.languageIrdi = languageIrdi;
    }

    public String getTermId()
    {
        return termId;
    }

    public void setTermId(String termId)
    {
        this.termId = termId;
    }

    public String getTermIrdi()
    {
        return termIrdi;
    }

    public void setTermIrdi(String termIrdi)
    {
        this.termIrdi = termIrdi;
    }

    public String getDefinitionId()
    {
        return definitionId;
    }

    public void setDefinitionId(String definitionId)
    {
        this.definitionId = definitionId;
    }

    public String getDefinitionIrdi()
    {
        return definitionIrdi;
    }

    public void setDefinitionIrdi(String definitionIrdi)
    {
        this.definitionIrdi = definitionIrdi;
    }

    public String getAbbreviationId()
    {
        return abbreviationId;
    }

    public void setAbbreviationId(String abbreviationId)
    {
        this.abbreviationId = abbreviationId;
    }

    public String getAbbreviationIrdi()
    {
        return abbreviationIrdi;
    }

    public void setAbbreviationIrdi(String abbreviationIrdi)
    {
        this.abbreviationIrdi = abbreviationIrdi;
    }

    public String getCodeId()
    {
        return codeId;
    }

    public void setCodeId(String codeId)
    {
        this.codeId = codeId;
    }

    public T8ConceptTerminology copy()
    {
        T8ConceptTerminology copy;

        copy = new T8ConceptTerminology(conceptType, conceptId);
        copy.setAbbreviation(abbreviation);
        copy.setAbbreviationId(abbreviationId);
        copy.setAbbreviationIrdi(abbreviationIrdi);
        copy.setCode(code);
        copy.setCodeId(codeId);
        copy.setConceptId(conceptId);
        copy.setConceptIrdi(conceptIrdi);
        copy.setConceptType(conceptType);
        copy.setOntologyStructureId(ontologyStructureId);
        copy.setDefinition(definition);
        copy.setDefinitionId(definitionId);
        copy.setDefinitionIrdi(definitionIrdi);
        copy.setLanguageId(languageId);
        copy.setLanguageIrdi(languageIrdi);
        copy.setOrganizationId(organizationId);
        copy.setTerm(term);
        copy.setTermId(termId);
        copy.setTermIrdi(termIrdi);
        return copy;
    }

    @Override
    public String toString()
    {
        StringBuilder builder;

        builder = new StringBuilder("T8ConceptTerminology: [");
        builder.append("Concept Type: ");
        builder.append(getConceptType());
        builder.append(",Concept ID: ");
        builder.append(getConceptId());
        builder.append("]");
        return builder.toString();
    }
}
