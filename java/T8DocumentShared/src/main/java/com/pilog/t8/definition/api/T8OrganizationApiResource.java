package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Gavin Boshoff
 */
public class T8OrganizationApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_ORG_GET_ORGANIZATION_STRUCTURE = "@OP_API_ORG_GET_ORGANIZATION_STRUCTURE";
    public static final String OPERATION_API_ORG_SAVE_ORGANIZATION = "@OP_API_ORG_SAVE_ORGANIZATION";
    public static final String OPERATION_API_ORG_MOVE_ORGANIZATION = "@OP_API_ORG_MOVE_ORGANIZATION";
    public static final String OPERATION_API_ORG_RECACHE_ORGANIZATION_STRUCTURE = "@OP_API_ORG_RECACHE_ORGANIZATION_STRUCTURE";
    public static final String OPERATION_API_ORG_REFRESH_ORGANIZATION_STRUCTURE_NESTED_INDICES = "@OP_API_ORG_REFRESH_ORGANIZATION_STRUCTURE_NESTED_INDICES";

    public static final String PARAMETER_ORGANIZATION_STRUCTURE = "$P_ORGANIZATION_STRUCTURE";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_PARENT_ORG_ID = "$P_PARENT_ORG_ID";
    public static final String PARAMETER_ORG_ID = "$P_ORG_ID";
    public static final String PARAMETER_ORG_TYPE_ID = "$P_ORG_TYPE_ID";
    public static final String PARAMETER_CONCEPT = "$P_CONCEPT";
    public static final String PARAMETER_INCLUDE_TERMINOLOGY = "$P_INCLUDE_TERMINOLOGY";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_GET_ORGANIZATION_STRUCTURE);
            definition.setMetaDisplayName("Get Organization Structure");
            definition.setMetaDescription("Returns the entire organization structure for the current session.");
            definition.setClassName("com.pilog.t8.api.T8OrganizationApiOperations$ApiGetOranizationStructure");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ORGANIZATION_STRUCTURE, "Organization Structure", "The organization structure for the current session.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_SAVE_ORGANIZATION);
            definition.setMetaDisplayName("Save Organization");
            definition.setMetaDescription("Save the organization concept");
            definition.setClassName("com.pilog.t8.api.T8OrganizationApiOperations$ApiSaveOranization");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PARENT_ORG_ID, "Parent Organization Id", "The parent organization id for the organization to save.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ORG_TYPE_ID, "Organization Type Id", "The Organization type id for the organization to save.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT, "Organization Concept", "The Organization concept to save.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "Indicator to indicate if the terminology must also be updated.", T8DataType.BOOLEAN));
            definitions.add(definition);
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_MOVE_ORGANIZATION);
            definition.setMetaDisplayName("Move Organization");
            definition.setMetaDescription("Move the Organization");
            definition.setClassName("com.pilog.t8.api.T8OrganizationApiOperations$ApiMoveOranization");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PARENT_ORG_ID, "Parent Organization Id", "The parent organization id for the organization to be moved.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ORG_ID, "Organization Id", "The Organization id for the organization that have to be moved.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "Indicator to indicate if the organization move was successfull.", T8DataType.BOOLEAN));
            definitions.add(definition);
            
            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_RECACHE_ORGANIZATION_STRUCTURE);
            definition.setMetaDisplayName("Recache Organization Structure");
            definition.setMetaDescription("Reloads the Organization Structure content from the persisted source.");
            definition.setClassName("com.pilog.t8.api.T8OrganizationApiOperations$ApiRecacheOranizationStructure");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ORGANIZATION_STRUCTURE, "Organization Structure", "The Organization Structure tree object.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ORG_REFRESH_ORGANIZATION_STRUCTURE_NESTED_INDICES);
            definition.setMetaDisplayName("Refresh Organization Structure Nested Indices");
            definition.setMetaDescription("Refreshes the indices of nested sets in the organization structure.");
            definition.setClassName("com.pilog.t8.api.T8OrganizationApiOperations$ApiRefreshOrganizationStructureNestedIndices");
            definitions.add(definition);

            return definitions;
        }
        else return null;
    }
}