package com.pilog.t8.data.document;

/**
 * @author Bouwer du Preez
 */
public interface CachedDataRequirementInstanceProvider extends DataRequirementInstanceProvider
{
    public void clearCache();
}
