package com.pilog.t8.data.document.integration.structure.sap;

import java.util.List;

/**
 *
 * @author Andre Scheepers
 */
public class SAPTableSection
{

    /**
     * @return the sectionTerm
     */
    public String getSectionTerm()
    {
        return sectionTerm;
    }

    /**
     * @param sectionTerm the sectionTerm to set
     */
    public void setSectionTerm(String sectionTerm)
    {
        this.sectionTerm = sectionTerm;
    }

    /**
     * @return the dataRows
     */
    public List<SAPDataRow> getDataRows()
    {
        return dataRows;
    }

    /**
     * @param dataRows the dataRows to set
     */
    public void setDataRows(List<SAPDataRow> dataRows)
    {
        this.dataRows = dataRows;
    }
    private String sectionTerm;
    private List<SAPDataRow> dataRows;
}