package com.pilog.t8.data.document.datastring;

import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringDataProvider
{
    private final CachedTerminologyProvider terminologyProvider;
    private final DataRecordProvider recordProvider;
    private final List<String> cacheLanguageIDList;
    private final String defaultLanguageID;

    public T8DataStringDataProvider(CachedTerminologyProvider terminologyProvider, DataRecordProvider recordProvider, String defaultLanguageID)
    {
        this.terminologyProvider = terminologyProvider;
        this.recordProvider = recordProvider;
        this.defaultLanguageID = defaultLanguageID;
        this.cacheLanguageIDList = new ArrayList<>();
    }

    public void setCacheLanguageIDList(List<String> languageIdList)
    {
        cacheLanguageIDList.clear();
        if ((languageIdList != null) && (languageIdList.size() > 0))
        {
            cacheLanguageIDList.addAll(languageIdList);
        }
    }

    public DataRecord getDataFile(String recordID) throws Exception
    {
        return recordProvider.getDataFile(recordID, null, false, false, false, false, false, false);
    }

    public void cacheTerminology(Set<String> conceptIDSet) throws Exception
    {
        terminologyProvider.cacheTerminology(cacheLanguageIDList, conceptIDSet);
    }

    public String getConceptTerm(String languageID, String conceptID, boolean revertToDefaultLanguage) throws Exception
    {
        String term;

        // Get the term in the specified language.
        term = terminologyProvider.getTerm(languageID, conceptID);

        // If not term was found for the specified language, try to find it in the default language.
        if ((term == null) && (revertToDefaultLanguage)) term = terminologyProvider.getTerm(defaultLanguageID, conceptID);

        // If the term was not found in the default language, return the concept ID.
        if (term == null) term = conceptID;

        // Return the term;
        return term;
    }

    public String getConceptAbbreviation(String languageID, String conceptID, boolean revertToDefaultLanguage) throws Exception
    {
        String abbreviation;

        // Get the term in the specified language.
        abbreviation = terminologyProvider.getAbbreviation(languageID, conceptID);

        // If not term was found for the specified language, try to find it in the default language.
        if ((abbreviation == null) && (revertToDefaultLanguage)) abbreviation = terminologyProvider.getAbbreviation(defaultLanguageID, conceptID);

        // If the term was not found in the default language, return the concept ID.
        if (abbreviation == null) abbreviation = conceptID;

        // Return the term;
        return abbreviation;
    }
}
