package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */

public class T8DataRecordMatchApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_RECORD_MATCH_RETRIEVE_MATCH_CRITERIA = "@OS_API_RECORD_MATCH_RETRIEVE_MATCH_CRITERIA";
    public static final String OPERATION_API_RECORD_MATCH_SAVE_MATCH_CRITERIA = "@OS_API_RECORD_MATCH_SAVE_MATCH_CRITERIA";
    public static final String OPERATION_API_RECORD_MATCH_DELETE_MATCH_CRITERIA = "@OS_API_RECORD_MATCH_DELETE_MATCH_CRITERIA";
    public static final String OPERATION_API_RECORD_MATCH_GENERATE_CHECKSUMS = "@OS_API_RECORD_MATCH_GENERATE_CHECKSUMS";
    public static final String OPERATION_API_MATCH_DATA_FILES = "@OS_API_MATCH_DATA_FILES";
    public static final String OPERATION_API_MATCH_DATA_FILE_HASH_CODES = "@OS_API_MATCH_DATA_FILE_HASH_CODES";
    public static final String OPERATION_API_DELETE_MATCH_RESULTS = "@OS_API_DELETE_MATCH_RESULTS";

    public static final String OPERATION_API_MTC_GENERATE_FILE_HASH_CODES = "@OP_API_MTC_GENERATE_FILE_HASH_CODES";

    public static final String PARAMETER_CRITERIA_ID = "$P_MATCH_CRITERIA_ID";
    public static final String PARAMETER_MATCH_CRITERIA = "$P_MATCH_CRITERIA";
    public static final String PARAMETER_NEW_CRITERIA = "$P_NEW_CRITERIA";
    public static final String PARAMETER_FILTER = "$P_DATA_FILTER";
    public static final String PARAMETER_MATCH_INSTANCE_ID = "$P_MATCH_INSTANCE_ID";
    public static final String PARAMETER_MATCH_INSTANCE_ID_LIST = "$P_MATCH_INSTANCE_ID_LIST";
    public static final String PARAMETER_HASH_CODE_LIST = "$P_HASH_CODE_LIST";

    // Entity fields defined by the API.
    public static final String F_MATCH_INSTANCE_ID = "$MATCH_INSTANCE_ID";
    public static final String F_MATCH_CRITERIA_ID = "$MATCH_CRITERIA_ID";
    public static final String F_MATCH_CASE_ID = "$MATCH_CASE_ID";
    public static final String F_HASH_CODE_ID = "$HASH_CODE_ID";
    public static final String F_HASH_CODE = "$HASH_CODE";
    public static final String F_ROOT_ORG_ID = "$ROOT_ORG_ID";
    public static final String F_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String F_MATCH_CHECK_ID = "$MATCH_CHECK_ID";
    public static final String F_ORG_ID = "$ORG_ID";
    public static final String F_NAME = "$NAME";
    public static final String F_DESCRIPTION = "$DESCRIPTION";
    public static final String F_EVENT_IID = "$EVENT_IID";
    public static final String F_DATA_EXPRESSION ="$DATA_EXPRESSION";
    public static final String F_CONDITION_EXPRESSION ="$CONDITION_EXPRESSION";
    public static final String F_SEQUENCE  ="$SEQUENCE";
    public static final String F_TYPE = "$TYPE";
    public static final String F_PERCENTAGE = "$PERCENTAGE";
    public static final String F_RECORD_ID1 = "$RECORD_ID1";
    public static final String F_RECORD_ID2 = "$RECORD_ID2";
    public static final String F_MATCH_PLAN_ID = "$MATCH_PLAN_ID";
    public static final String F_FAMILY_ID = "$FAMILY_ID";
    public static final String F_PARENT_FAMILY_ID = "$PARENT_FAMILY_ID";
    public static final String F_INSERTED_BY_ID = "$INSERTED_BY_ID";
    public static final String F_INSERTED_AT = "$INSERTED_AT";
    public static final String F_UPDATED_BY_ID = "$UPDATED_BY_ID";
    public static final String F_UPDATED_AT = "$UPDATED_AT";
    public static final String F_FAMILY_CODE = "$FAMILY_CODE";
    public static final String F_INSERTED_BY_IID = "$INSERTED_BY_IID";
    public static final String F_UPDATED_BY_IID = "$UPDATED_BY_IID";
    public static final String F_EDIT_USER = "$EDIT_USER";
    public static final String F_EDIT_DATE = "$EDIT_DATE";
    public static final String F_EVENT = "$EVENT";
    public static final String F_RECORD_ID ="$RECORD_ID";
    public static final String F_ROOT_RECORD_ID = "$ROOT_RECORD_ID";
    public static final String F_FINALIZED_INDICATOR = "$FINALIZED_INDICATOR";
    public static final String F_RESOLVED_STATE = "$RESOLVED_STATE";
    public static final String F_RECORD2_RESOLVED_STATE = "$RECORD2_RESOLVED_STATE";
    public static final String F_RECORD1_RESOLVED_STATE = "$RECORD1_RESOLVED_STATE";

    // DATA_RECORD_MATCH.
    public static final String DATA_RECORD_MATCH_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH";
    public static final String DATA_RECORD_MATCH_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH";

    // DATA_RECORD_MATCH_RESOLVED.
    public static final String DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_RESOLVED_EXCLUDED";
    public static final String DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_RESOLVED_EXCLUDED";

    // DATA_RECORD_MATCH_LINK.
    public static final String DATA_RECORD_MATCH_LINK_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_LINK";
    public static final String DATA_RECORD_MATCH_LINK_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_LINK";

    // DATA_RECORD_MATCH_CRITERIA.
    public static final String DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_CRITERIA";
    public static final String DATA_RECORD_MATCH_CRITERIA_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_CRITERIA";

    // DATA_RECORD_MATCH_CASE.
    public static final String DATA_RECORD_MATCH_CASE_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_CASE";
    public static final String DATA_RECORD_MATCH_CASE_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_CASE";

    // DATA_RECORD_MATCH_CHECK.
    public static final String DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_CHECK";
    public static final String DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_CHECK";

    // DATA_RECORD_MATCH_HASH.
    public static final String DATA_RECORD_MATCH_HASH_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_HASH";
    public static final String DATA_RECORD_MATCH_HASH_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_HASH";

    // DATA_RECORD_MATCH_FAMILY.
    public static final String DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER = "@DS_DATA_RECORD_MATCH_FAMILY";
    public static final String DATA_RECORD_MATCH_FAMILY_DE_IDENTIFIER = "@E_DATA_RECORD_MATCH_FAMILY";

    // HISTORY_RECORD_MATCH.
    public static final String HISTORY_RECORD_MATCH_DS_IDENTIFIER = "@DS_HISTORY_RECORD_MATCH";
    public static final String HISTORY_RECORD_MATCH_DE_IDENTIFIER = "@E_HISTORY_RECORD_MATCH";

    // HISTORY_RECORD_MATCH_CRITERIA.
    public static final String HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER = "@DS_HISTORY_DATA_RECORD_MATCH_CRITERIA";
    public static final String HISTORY_RECORD_MATCH_CRITERIA_DE_IDENTIFIER = "@E_HISTORY_DATA_RECORD_MATCH_CRITERIA";

    // HISTORY_RECORD_MATCH_CASE.
    public static final String HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER = "@DS_HISTORY_RECORD_MATCH_CASE";
    public static final String HISTORY_RECORD_MATCH_CASE_DE_IDENTIFIER = "@E_HISTORY_RECORD_MATCH_CASE";

    // HISTORY_RECORD_MATCH_CHECK.
    public static final String HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER = "@DS_HISTORY_RECORD_MATCH_CHECK";
    public static final String HISTORY_RECORD_MATCH_CHECK_DE_IDENTIFIER = "@E_HISTORY_RECORD_MATCH_CHECK";

    // Table Names.
    private static final String TABLE_DATA_RECORD_MATCH = "DAT_REC_MTC";
    private static final String TABLE_DATA_RECORD_MATCH_LINK = "DAT_REC_MTC_LNK";
    private static final String TABLE_DATA_RECORD_MATCH_CRITERIA = "DAT_REC_MTC_CRI";
    private static final String TABLE_DATA_RECORD_MATCH_CASE = "DAT_REC_MTC_CSE";
    private static final String TABLE_DATA_RECORD_MATCH_CHECK = "DAT_REC_MTC_CHK";
    private static final String TABLE_DATA_RECORD_MATCH_HASH = "DAT_REC_MTC_HSH";
    private static final String TABLE_DATA_RECORD_MATCH_FAMILY = "DAT_REC_MTC_FAM";
    private static final String TABLE_HISTORY_RECORD_MATCH = "HIS_DAT_REC_MTC";
    private static final String TABLE_HISTORY_RECORD_MATCH_CRITERIA = "HIS_DAT_REC_MTC_CRI";
    private static final String TABLE_HISTORY_RECORD_MATCH_CASE = "HIS_DAT_REC_MTC_CSE";
    private static final String TABLE_HISTORY_RECORD_MATCH_CHECK = "HIS_DAT_REC_MTC_CHK";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        T8SQLDataSourceResourceDefinition sqlDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Data Record Match Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_MATCH_INSTANCE_ID, "MATCH_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID1", "RECORD_ID1", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID2", "RECORD_ID2", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_PLAN_ID", "MATCH_PLAN_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FAMILY_ID", "FAMILY_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PARENT_FAMILY_ID", "PARENT_FAMILY_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_ID", "INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_BY_IID", "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INSERTED_AT", "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_ID", "UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_BY_IID", "UPDATED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$UPDATED_AT", "UPDATED_AT", false, false, T8DataType.TIMESTAMP));

        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Data Record Match Resolved Data source Definition.
        sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER);
        sqlDataSourceDefinition.getLevel();
        sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        sqlDataSourceDefinition.setSQLSelectString("SELECT"
                + " RECORD1_RESOLUTION.RECORD_ID AS RECORD_ID1,"
                + " RECORD1_RESOLUTION.RESOLVED_STATE AS RECORD1_RESOLVED_STATE,"
                + " RECORD2_RESOLUTION.RECORD_ID AS RECORD_ID2, "
                + " RECORD2_RESOLUTION.RESOLVED_STATE AS RECORD2_RESOLVED_STATE,"
                + " DRM.INSTANCE_ID AS MATCH_INSTANCE_ID,"
                + " DRM.FAMILY_ID, "
                + " DRM.MATCH_PLAN_ID, "
                + " RECORD2_RESOLUTION.FINALIZED_INDICATOR "
                + " FROM DAT_REC_MTC DRM "
                + " INNER JOIN DAT_REC_MTC_RES RECORD1_RESOLUTION "
                + " ON RECORD1_RESOLUTION.RECORD_ID =  DRM.RECORD_ID1 "
                + " INNER JOIN DAT_REC_MTC_RES RECORD2_RESOLUTION "
                + " ON RECORD2_RESOLUTION.RECORD_ID = DRM.RECORD_ID2 "
                + "  WHERE RECORD2_RESOLUTION.FINALIZED_INDICATOR = 'Y' AND  "
                + "  RECORD1_RESOLUTION.FINALIZED_INDICATOR ='Y' AND  "
                + " (DRM.INSTANCE_ID = RECORD1_RESOLUTION.MATCH_INSTANCE_ID "
                + " OR DRM.INSTANCE_ID = RECORD2_RESOLUTION.MATCH_INSTANCE_ID)"
        );
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID1", "RECORD_ID1", true, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD1_RESOLVED_STATE",  "RECORD1_RESOLVED_STATE", true, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD2_RESOLUTION_STATE",  "RECORD2_RESOLUTION_STATE", true, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FINALIZED_INDICATOR",  "FINALIZED_INDICATOR", true, true, T8DataType.STRING));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID2",  "RECORD_ID2", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_INSTANCE_ID",  "MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FAMILY_ID",  "FAMILY_ID", false, false, T8DataType.GUID));
        sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_PLAN_ID",  "MATCH_PLAN_ID", false, false, T8DataType.STRING));
        definitions.add(sqlDataSourceDefinition);

        // Data Record Match Criteria Link Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_LINK_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH_LINK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ROOT_ORG_ID", "ROOT_ORG_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_INSTANCE_ID", "DR_INSTANCE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Data Record Match Criteria Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH_CRITERIA);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ORG_ID", "ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME", "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DESCRIPTION", "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        //Data Record Match Case Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_CASE_DS_IDENTIFIER );
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH_CASE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CASE_ID", "MATCH_CASE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME",   "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DESCRIPTION", "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CONDITION_EXPRESSION",   "CONDITION_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_EXPRESSION",   "DATA_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE",   "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Data Record Match Check Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER );
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH_CHECK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CASE_ID", "MATCH_CASE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CHECK_ID", "MATCH_CHECK_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME",   "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DESCRIPTION", "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE", "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PERCENTAGE",   "PERCENTAGE", false, false, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_EXPRESSION",   "DATA_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Data Record Match Hash Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_HASH_DS_IDENTIFIER );
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH_HASH);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_HASH_CODE_ID, "HASH_CODE_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_MATCH_INSTANCE_ID, "MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_MATCH_CRITERIA_ID, "MATCH_CRITERIA_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_MATCH_CASE_ID, "MATCH_CASE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_ID, "RECORD_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_HASH_CODE, "HASH_CODE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_ID, "INSERTED_BY_ID", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_AT, "INSERTED_AT", false, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // Data Record Match Hash Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER );
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_DATA_RECORD_MATCH_FAMILY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_MATCH_INSTANCE_ID, "MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FAMILY_ID, "FAMILY_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_FAMILY_CODE, "FAMILY_CODE", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_ID, "INSERTED_BY_ID", false, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_BY_IID, "INSERTED_BY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INSERTED_AT, "INSERTED_AT", false, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // History Record Match Data source Definition.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_RECORD_MATCH_DS_IDENTIFIER );
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_RECORD_MATCH);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_INSTANCE_ID", "MATCH_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT", "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TIME", "TIME", false, false, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_COUNT", "RECORD_COUNT", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_ID", "INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_IID", "INITIATOR_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$INITIATOR_ORG_ID", "INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // History Record Match Criteria Datasource.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_RECORD_MATCH_CRITERIA);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_INSTANCE_ID", "MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", true, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ORG_ID", "ORG_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME", "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DESCRIPTION", "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // History Record Match Case Data source.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_RECORD_MATCH_CASE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_INSTANCE_ID", "MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", true, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CASE_ID", "MATCH_CASE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME", "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DESCRIPTION", "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CONDITION_EXPRESSION", "CONDITION_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_EXPRESSION", "DATA_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE",   "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // History Record Match Check Datasource.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_RECORD_MATCH_CHECK);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$EVENT_IID", "EVENT_IID", true, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_INSTANCE_ID", "MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CRITERIA_ID", "MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CASE_ID", "MATCH_CASE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$MATCH_CHECK_ID", "MATCH_CHECK_ID", true, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$NAME", "NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DESCRIPTION", "DESCRIPTION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$SEQUENCE", "SEQUENCE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$TYPE", "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$PERCENTAGE", "PERCENTAGE", false, false, T8DataType.DOUBLE));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DATA_EXPRESSION", "DATA_EXPRESSION", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }
    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // Data Record Match Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_MATCH_INSTANCE_ID, DATA_RECORD_MATCH_DS_IDENTIFIER + F_MATCH_INSTANCE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_ID1", DATA_RECORD_MATCH_DS_IDENTIFIER + "$RECORD_ID1", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_ID2", DATA_RECORD_MATCH_DS_IDENTIFIER + "$RECORD_ID2", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_PLAN_ID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$MATCH_PLAN_ID", true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FAMILY_ID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$FAMILY_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PARENT_FAMILY_ID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$PARENT_FAMILY_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_ID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$INSERTED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_BY_IID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$INSERTED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INSERTED_AT", DATA_RECORD_MATCH_DS_IDENTIFIER + "$INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_ID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$UPDATED_BY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_BY_IID", DATA_RECORD_MATCH_DS_IDENTIFIER + "$UPDATED_BY_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$UPDATED_AT", DATA_RECORD_MATCH_DS_IDENTIFIER + "$UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // Data Record Match Resolved Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_INSTANCE_ID", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_ID1", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$RECORD_ID1", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD1_RESOLVED_STATE", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$RECORD1_RESOLVED_STATE", true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FINALIZED_INDICATOR", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$FINALIZED_INDICATOR", true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FAMILY_ID", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$FAMILY_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_ID2", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$RECORD_ID2", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD2_RESOLVED_STATE", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$RECORD2_RESOLVED_STATE", true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_PLAN_ID", DATA_RECORD_MATCH_RESOLVED_DS_IDENTIFIER + "$MATCH_PLAN_ID", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Data Record Match Criteria Link Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_LINK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_LINK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ROOT_ORG_ID", DATA_RECORD_MATCH_LINK_DS_IDENTIFIER + "$ROOT_ORG_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DR_INSTANCE_ID", DATA_RECORD_MATCH_LINK_DS_IDENTIFIER + "$DR_INSTANCE_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", DATA_RECORD_MATCH_LINK_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", true, true, T8DataType.GUID));
        definitions.add(entityDefinition);

        // Data Record Match Criteria Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_CRITERIA_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ORG_ID", DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$ORG_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NAME", DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$NAME", false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DESCRIPTION", DATA_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$DESCRIPTION", false, true, T8DataType.STRING));
        definitions.add(entityDefinition);

        //Data Record Match Case Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_CASE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_CASE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CASE_ID", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER+ "$MATCH_CASE_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NAME", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER+ "$NAME", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DESCRIPTION", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER + "$DESCRIPTION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER+ "$SEQUENCE", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CONDITION_EXPRESSION", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER + "$CONDITION_EXPRESSION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_EXPRESSION", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER + "$DATA_EXPRESSION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", DATA_RECORD_MATCH_CASE_DS_IDENTIFIER + "$TYPE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        //Data Record Match Check Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CASE_ID", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER+ "$MATCH_CASE_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CHECK_ID", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$MATCH_CHECK_ID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NAME", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER+ "$NAME", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DESCRIPTION", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$DESCRIPTION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER+ "$SEQUENCE", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$TYPE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PERCENTAGE", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER+ "$PERCENTAGE", false, false, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_EXPRESSION", DATA_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$DATA_EXPRESSION", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // Data Record Match Hash Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_HASH_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_HASH_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_HASH_CODE_ID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_HASH_CODE_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_MATCH_INSTANCE_ID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_MATCH_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_MATCH_CRITERIA_ID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_MATCH_CRITERIA_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_MATCH_CASE_ID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_MATCH_CASE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_ID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_RECORD_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_HASH_CODE, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_HASH_CODE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_ID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_INSERTED_BY_ID, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_IID, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_AT, DATA_RECORD_MATCH_HASH_DS_IDENTIFIER + F_INSERTED_AT, false, true, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // Data Record Match Family Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(DATA_RECORD_MATCH_FAMILY_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_MATCH_INSTANCE_ID, DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER + F_MATCH_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FAMILY_ID, DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER + F_FAMILY_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_FAMILY_CODE, DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER + F_FAMILY_CODE, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_ID, DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER + F_INSERTED_BY_ID, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_BY_IID, DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER + F_INSERTED_BY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INSERTED_AT, DATA_RECORD_MATCH_FAMILY_DS_IDENTIFIER + F_INSERTED_AT, false, true, T8DataType.TIMESTAMP));
        definitions.add(entityDefinition);

        // Data Record Match History Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_RECORD_MATCH_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_RECORD_MATCH_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", HISTORY_RECORD_MATCH_DS_IDENTIFIER + "$EVENT_IID", true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_INSTANCE_ID", HISTORY_RECORD_MATCH_DS_IDENTIFIER+ "$MATCH_INSTANCE_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT", HISTORY_RECORD_MATCH_DS_IDENTIFIER + "$EVENT", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TIME", HISTORY_RECORD_MATCH_DS_IDENTIFIER+ "$TIME", false, false, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_COUNT", HISTORY_RECORD_MATCH_DS_IDENTIFIER + "$RECORD_COUNT", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INITIATOR_ID", HISTORY_RECORD_MATCH_DS_IDENTIFIER+ "$INITIATOR_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INITIATOR_IID", HISTORY_RECORD_MATCH_DS_IDENTIFIER+ "$INITIATOR_IID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$INITIATOR_ORG_ID", HISTORY_RECORD_MATCH_DS_IDENTIFIER + "$INITIATOR_ORG_ID", false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // History Record Match Criteria Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_RECORD_MATCH_CRITERIA_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$EVENT_IID", true, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_INSTANCE_ID", HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", true, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ORG_ID", HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$ORG_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NAME", HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$NAME", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DESCRIPTION", HISTORY_RECORD_MATCH_CRITERIA_DS_IDENTIFIER + "$DESCRIPTION", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // History Record Match Case Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_RECORD_MATCH_CASE_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$EVENT_IID", true, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_INSTANCE_ID", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CASE_ID", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$MATCH_CASE_ID", true, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NAME", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$NAME", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DESCRIPTION", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$DESCRIPTION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$SEQUENCE", false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CONDITION_EXPRESSION", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$CONDITION_EXPRESSION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_EXPRESSION", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$DATA_EXPRESSION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", HISTORY_RECORD_MATCH_CASE_DS_IDENTIFIER + "$TYPE", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        //History Record Match Check Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_RECORD_MATCH_CHECK_DE_IDENTIFIER);
        entityDefinition.setDataSourceIdentifier(HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$EVENT_IID", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$EVENT_IID", true, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_INSTANCE_ID", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$MATCH_INSTANCE_ID", false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CRITERIA_ID", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$MATCH_CRITERIA_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CASE_ID", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$MATCH_CASE_ID", false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$MATCH_CHECK_ID", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$MATCH_CHECK_ID", true, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$NAME", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$NAME", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DESCRIPTION", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$DESCRIPTION", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$SEQUENCE", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$SEQUENCE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$TYPE", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$TYPE", false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$PERCENTAGE", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$PERCENTAGE", false, false, T8DataType.DOUBLE));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DATA_EXPRESSION", HISTORY_RECORD_MATCH_CHECK_DS_IDENTIFIER + "$DATA_EXPRESSION", false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        return definitions;
    }
   public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        //Retrive Match Criteria.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_RECORD_MATCH_RETRIEVE_MATCH_CRITERIA);
        definition.setMetaDisplayName("Retrieve Match Criteria");
        definition.setMetaDescription("Retrieves the specified data record match criteria.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiRetrieveMatchCriteria");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CRITERIA_ID, "Criteria ID", "The ID of the match criteria to retrieve.", T8DataType.GUID));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_CRITERIA, "Match Criteria", "The match criteria retrieved.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        //Save Match Criteria.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_RECORD_MATCH_SAVE_MATCH_CRITERIA);
        definition.setMetaDisplayName("Save Match Criteria");
        definition.setMetaDescription("Saves the specified data record match criteria.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiSaveMatchCriteria");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_NEW_CRITERIA, "Criteria", "The edited or new match criteria to save.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        //Delete Match Criteria.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_RECORD_MATCH_DELETE_MATCH_CRITERIA);
        definition.setMetaDisplayName("Delete Match Criteria");
        definition.setMetaDescription("Deletes the specified data record match criteria.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiDeleteMatchCriteria");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CRITERIA_ID, "Criteria ID", "The ID of the match criteria to retrieve.", T8DataType.GUID));
        definitions.add(definition);

        //Match Data files.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_MATCH_DATA_FILES);
        definition.setMetaDisplayName("Match Data Files");
        definition.setMetaDescription("Matches Data Files with the specified data record match criteria.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiMatchDataFiles");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CRITERIA_ID, "Criteria ID", "The ID of the match criteria to retrieve.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_CRITERIA,"Criteria Object", "The match criteria to match data files on.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILTER, "Data Filter", "The Data filter to be used to retrieve records to be matched.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_INSTANCE_ID, "Match Instance Id", "The id of this match instance.  All record matches identified during the matching operation will be persisted using this id as reference.", T8DataType.GUID));
        definitions.add(definition);

        //Match Data File hash codes.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_MATCH_DATA_FILE_HASH_CODES);
        definition.setMetaDisplayName("Match Data File Hash Codes");
        definition.setMetaDescription("Matches Data Record Files using the hash codes retrieved from the specified filtered entity.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiMatchDataFileHashCodes");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILTER, "Data Filter", "The Data filter to be used to retrieve records to be matched.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_INSTANCE_ID, "Match Instance Id", "The id of this match instance.  All record matches identified during the matching operation will be persisted using this id as reference.", T8DataType.GUID));
        definitions.add(definition);

        //Generate Checksums.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_RECORD_MATCH_GENERATE_CHECKSUMS);
        definition.setMetaDisplayName("Generate Checksums");
        definition.setMetaDescription("Generates checksums with the specified criteria.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiGenerateFileHashCodes");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CRITERIA_ID, "Criteria ID", "The ID of the match criteria to use.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_CRITERIA,"Criteria Object", "The match criteria to use when generating checksums.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILTER, "Data Filter", "The Data filter to be used to retrieve records to be matched.", T8DataType.CUSTOM_OBJECT));
        definitions.add(definition);

        //Generate Checksums.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_MTC_GENERATE_FILE_HASH_CODES);
        definition.setMetaDisplayName("Generate Checksums");
        definition.setMetaDescription("Generates checksums with the specified criteria.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiGenerateFileHashCodes");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CRITERIA_ID, "Criteria ID", "The ID of the match criteria to use.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_CRITERIA,"Criteria Object", "The match criteria to use when generating checksums.", T8DataType.CUSTOM_OBJECT));
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_FILTER, "Data Filter", "The Data filter to be used to retrieve records to be matched.", T8DataType.CUSTOM_OBJECT));
        definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_HASH_CODE_LIST, "Hash Code List", "The list of hash codes generated.", T8DataType.LIST));
        definitions.add(definition);

        //Delete Match Results.
        definition = new T8JavaServerOperationDefinition(OPERATION_API_DELETE_MATCH_RESULTS);
        definition.setMetaDisplayName("Delete Match Results");
        definition.setMetaDescription("Delete match results that are specified in a list.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordMatchApiOperations$ApiDeleteMatchResults");
        definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_MATCH_INSTANCE_ID_LIST, "Match Instance Id List", "A list of the match instance id's to be deleted.", T8DataType.LIST));
        definitions.add(definition);
        return definitions;
    }
}