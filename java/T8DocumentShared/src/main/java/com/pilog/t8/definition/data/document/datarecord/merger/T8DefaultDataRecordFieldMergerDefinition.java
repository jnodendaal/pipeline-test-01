package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordFieldMerger;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordFieldMergerDefinition extends T8DataRecordFieldMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_FIELD_MERGER_DEFAULT";
    public static final String DISPLAY_NAME = "Default Field Merger";
    public static final String DESCRIPTION = "A definition that specifies merging rules applicable to a data record field.";
    public enum Datum {FIELD_ID,
                       VALUE_MERGER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //
    
    public T8DefaultDataRecordFieldMergerDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FIELD_ID.toString(), "Field", "The Field to which this merge configuration applies."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.VALUE_MERGER_DEFINITIONS.toString(), "Value Mergers",  "The Value merger definitions applicable within the context of this field."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.VALUE_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordValueMergerDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, inputParameters);
    }
    
    @Override
    public T8DataRecordFieldMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordFieldMerger").getConstructor(this.getClass(), T8DataRecordMergeContext.class);
            return (T8DataRecordFieldMerger)constructor.newInstance(this, mergeContext);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Field Merger.", e);
        }
    }
    
    public String getFieldID()
    {
        return (String)getDefinitionDatum(Datum.FIELD_ID.toString());
    }
    
    public void setFieldID(String fieldID)
    {
        setDefinitionDatum(Datum.FIELD_ID.toString(), fieldID);
    }
    
    public List<T8DataRecordValueMergerDefinition> getValueMergerDefinitions()
    {
        return (List<T8DataRecordValueMergerDefinition>)getDefinitionDatum(Datum.VALUE_MERGER_DEFINITIONS.toString());
    }
    
    public void setValueMergerDefinitions(List<T8DataRecordValueMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.VALUE_MERGER_DEFINITIONS.toString(), definitions);
    }    
}
