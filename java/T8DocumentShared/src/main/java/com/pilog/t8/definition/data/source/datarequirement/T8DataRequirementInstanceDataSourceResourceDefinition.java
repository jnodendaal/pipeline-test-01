package com.pilog.t8.definition.data.source.datarequirement;

/**
 *
 * @author Pieter Strydom
 */
public class T8DataRequirementInstanceDataSourceResourceDefinition extends T8DataRequirementInstanceDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATA_REQUIREMENT_INSTANCE_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8DataRequirementInstanceDataSourceResourceDefinition(String identifier)
    {
        super(identifier);
    }
}