package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentStructurePrinter
{
    private PrintWriter writer;
    
    public T8DocumentStructurePrinter(OutputStream stream)
    {
        writer = new PrintWriter(stream);
    }
    
    public static void printStructure(OutputStream outputStream, DataRequirement dataRequirement)
    {
        T8DocumentStructurePrinter printer;
        
        printer = new T8DocumentStructurePrinter(outputStream);
        printer.printStructure(dataRequirement);
        printer.flush();
    }
    
    public void flush()
    {
        writer.flush();
    }
        
    public void printStructure(DataRequirement dataRequirement)
    {
        writer.println("Data Requirement: " + dataRequirement.getConceptID());
        for (SectionRequirement classRequirement : dataRequirement.getSectionRequirements())
        {
            printStructure(classRequirement);
        }
    }
    
    public void printStructure(SectionRequirement classRequirement)
    {
        writer.println("    Class Requirement: " + classRequirement.getConceptID());
        for (PropertyRequirement propertyRequirement : classRequirement.getPropertyRequirements())
        {
            printStructure(propertyRequirement);
        }
    }
    
    public void printStructure(PropertyRequirement propertyRequirement)
    {
        writer.println("        Property Requirement: " + propertyRequirement.getConceptID());
        if (propertyRequirement.getValueRequirement() != null)
        {
            printStructure(propertyRequirement.getValueRequirement());
        }
    }
    
    public void printStructure(ValueRequirement valueRequirement)
    {
        // Add some indentation.
        for (int indent = 0; indent < valueRequirement.getDepth() + 3; indent++)
        {
            writer.print("    ");
        }
        
        // Print the value requirement detail.
        writer.println("Value Requirement: " + valueRequirement);
        
        // Print the sub-value requirements.
        for (ValueRequirement subValueRequirement : valueRequirement.getSubRequirements())
        {
            printStructure(subValueRequirement);
        }
    }
}
