package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class PropertyRequirementStep extends DefaultPathStep implements PathStep
{
    public PropertyRequirementStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateInstanceStep(((DataRecord)input).getDataRequirement().getPropertyRequirements());
        }
        else if (input instanceof RecordSection)
        {
            return evaluateInstanceStep(((RecordSection)input).getSectionRequirement().getPropertyRequirements());
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateInstanceStep(((DataRequirementInstance)input).getDataRequirement().getPropertyRequirements());
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateInstanceStep(((DataRequirement)input).getPropertyRequirements());
        }
        else if (input instanceof SectionRequirement)
        {
            return evaluateInstanceStep(((SectionRequirement)input).getPropertyRequirements());
        }
        else if (input instanceof List)
        {
            List<RecordProperty> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<RecordProperty>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<RecordProperty>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Property step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<PropertyRequirement> evaluateInstanceStep(List<PropertyRequirement> propertyList)
    {
        // If we have valid ID's to use for narrowing of the property list do it now.
        if (idList != null)
        {
            Iterator<PropertyRequirement> propertyIterator;

            propertyIterator = propertyList.iterator();
            while (propertyIterator.hasNext())
            {
                PropertyRequirement nextProperty;

                nextProperty = propertyIterator.next();
                if (!idList.contains(nextProperty.getConceptID()))
                {
                    propertyIterator.remove();
                }
            }
        }

        // Now run through the remaining property list and evaluate the predicate expression (if any).
        if (predicateExpression != null)
        {
            Iterator<PropertyRequirement> propertyIterator;

            // Iterator over the properties and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            propertyIterator = propertyList.iterator();
            while (propertyIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                PropertyRequirement nextProperty;

                // Create a map containing the input parameters available to the predicate expression.
                nextProperty = propertyIterator.next();
                inputParameters = getPredicateExpressionParameters(nextProperty.getAttributes());
                inputParameters.put("PROPERTY_ID", nextProperty.getConceptID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        propertyIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return propertyList;
    }
}
