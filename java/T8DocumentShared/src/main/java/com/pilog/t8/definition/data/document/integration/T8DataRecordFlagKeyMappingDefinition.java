package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8DataRecordFlagKeyMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_RECORD_FLAG_KEY";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_FLAG_KEY";
    public static final String DISPLAY_NAME = "Flag Key Mapping";
    public static final String DESCRIPTION = "Flag Key Mapping";
    public static final String IDENTIFIER_PREFIX = "RP_MAP_";

    public enum Datum
    {
        KEY_ID,
        KEY_VALUE_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public T8DataRecordFlagKeyMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.KEY_ID.toString(), "Key ID", "The Key identifier."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.KEY_VALUE_EXPRESSION.toString(), "Key Value Expression", "The Key value."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        return null;
    }

    public String getKeyID()
    {
        return (String)getDefinitionDatum(Datum.KEY_ID.toString());
    }

    public void setKeyID(String term)
    {
        setDefinitionDatum(Datum.KEY_ID.toString(), term);
    }

    public String getKeyValueExpression()
    {
        return (String)getDefinitionDatum(Datum.KEY_VALUE_EXPRESSION.toString());
    }

    public void setKeyValueExpression(String expression)
    {
        setDefinitionDatum(Datum.KEY_VALUE_EXPRESSION.toString(), expression);
    }
}
