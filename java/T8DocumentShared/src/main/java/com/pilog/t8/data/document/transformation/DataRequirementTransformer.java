package com.pilog.t8.data.document.transformation;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.utilities.xml.dom.DOMHandler;
import com.pilog.t8.utilities.xml.iso.ISOConstants;
import com.pilog.t8.utilities.xml.iso.IXMLHandler;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class DataRequirementTransformer
{
    public Document transform(DataRequirement dataRequirement)
    {
        Document ixmlDocument;
        ArrayList<SectionRequirement> classRequirements;

        ixmlDocument = IXMLHandler.createNewIXMLDocument(dataRequirement.getConceptID(), null, null);
        classRequirements = dataRequirement.getSectionRequirements();
        for (SectionRequirement classRequirement : classRequirements)
        {
            addClassToElement(ixmlDocument.getDocumentElement(), classRequirement);
        }

        return ixmlDocument;
    }

    public DataRequirement transform(Document ixmlDocument)
    {
        DataRequirement dataRequirement;
        ArrayList<Element> classElements;
        int sequence = 0;

        dataRequirement = new DataRequirement();
        dataRequirement.setConceptID(IXMLHandler.getIGID(ixmlDocument));
        classElements = IXMLHandler.getItemElements(ixmlDocument);
        for (Element classElement : classElements)
        {
            dataRequirement.addSectionRequirement(createClassRequirementFromElement(classElement, sequence++));
        }

        return dataRequirement;
    }

    private SectionRequirement createClassRequirementFromElement(Element classElement, int sequence)
    {
        SectionRequirement classRequirement;
        ArrayList<Element> propertyElements;

        classRequirement = new SectionRequirement();
        classRequirement.setConceptID(classElement.getAttribute(ISOConstants.I_ATTRIBUTE_CLASS_REF));
        propertyElements = IXMLHandler.getPropertyElements(classElement);
        for (Element propertyElement : propertyElements)
        {
            classRequirement.addPropertyRequirement(createPropertyRequirementFromElement(propertyElement));
        }

        return classRequirement;
    }

    private PropertyRequirement createPropertyRequirementFromElement(Element propertyElement)
    {
        PropertyRequirement propertyRequirement;
        Element dataTypeElement;

        propertyRequirement = new PropertyRequirement();
        propertyRequirement.setConceptID(propertyElement.getAttribute(ISOConstants.I_ATTRIBUTE_PROPERTY_REF));
        propertyRequirement.setCharacteristic("true".equalsIgnoreCase(propertyElement.getAttribute(ISOConstants.I_ATTRIBUTE_IS_REQUIRED)));
        dataTypeElement = DOMHandler.getFirstChildElement(propertyElement);
        if (dataTypeElement != null)
        {
            propertyRequirement.setValueRequirement(createValueRequirementFromElement(dataTypeElement));
        }

        return propertyRequirement;
    }

    private ValueRequirement createValueRequirementFromElement(Element dataTypeElement)
    {
//        ValueRequirement valueRequirement;
//        ArrayList<Element> childElements;
//        String dataTypeName;
//
//        valueRequirement = new ValueRequirement();
//        dataTypeName = dataTypeElement.getLocalName();
//        childElements = DOMHandler.getChildElements(dataTypeElement);
//        if (ISOConstants.I_ELEMENT_BAG_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            ValueRequirement lowerBoundRequirement;
//            ValueRequirement upperBoundRequirement;
//            String upperBound;
//            String lowerBound;
//
//            valueRequirement.setRequirementType(RequirementType.BAG_TYPE);
//
//            lowerBound = dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_LOWER_BOUND);
//            lowerBound = lowerBound != null ? lowerBound.trim() : "1";
//            if (lowerBound.length() == 0) lowerBound = "1";
//            upperBound = dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_UPPER_BOUND);
//            upperBound = upperBound != null ? upperBound.trim() : "*";
//            if (upperBound.length() == 0) upperBound = "*";
//        }
//        else if (ISOConstants.I_ELEMENT_BOOLEAN_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.BOOLEAN_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_CHOICE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.CHOICE_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_COMPLEX_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.COMPLEX_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_COMPOSITE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.COMPOSITE_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_CONTROLLED_VALUE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.CONTROLLED_CONCEPT);
//        }
//        else if (ISOConstants.I_ELEMENT_CURRENCY_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.CURRENCY_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_DATE_TIME_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.DATE_TIME_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_DATE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.DATE_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_FIELD.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.FIELD_TYPE);
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_PROPERTY_REF));
//        }
//        else if (ISOConstants.I_ELEMENT_FILE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.FILE_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_INTEGER_FORMAT.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_PATTERN));
//        }
//        else if (ISOConstants.I_ELEMENT_INTEGER_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.INTEGER_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_ITEM_REFERENCE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.DOCUMENT_REFERENCE);
//            valueRequirement.setValue(DOMHandler.getElementTextValue(dataTypeElement));
//        }
//        else if (ISOConstants.I_ELEMENT_LOCALIZED_TEXT_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.LOCALIZED_TEXT_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_MEASURE_NUMBER_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.MEASURED_NUMBER);
//        }
//        else if (ISOConstants.I_ELEMENT_MEASURE_RANGE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            ValueRequirement lowerBoundRequirement;
//            ValueRequirement upperBoundRequirement;
//
//            valueRequirement.setRequirementType(RequirementType.MEASURED_RANGE);
//
//            lowerBoundRequirement = new ValueRequirement();
//            lowerBoundRequirement.setRequirementType(RequirementType.LOWER_BOUND_NUMBER);
//            valueRequirement.addSubRequirement(lowerBoundRequirement);
//
//            // Try to parse sub requirements from any available child elements.
//            for (Element childElement : childElements)
//            {
//                ValueRequirement subRequirement;
//
//                subRequirement = createValueRequirementFromElement(childElement);
//                if (subRequirement != null)
//                {
//                    lowerBoundRequirement.addSubRequirement(subRequirement);
//                }
//            }
//            
//            upperBoundRequirement = new ValueRequirement();
//            upperBoundRequirement.setRequirementType(RequirementType.UPPER_BOUND_NUMBER);
//            valueRequirement.addSubRequirement(upperBoundRequirement);
//
//            // Try to parse sub requirements from any available child elements.
//            for (Element childElement : childElements)
//            {
//                ValueRequirement subRequirement;
//
//                subRequirement = createValueRequirementFromElement(childElement);
//                if (subRequirement != null)
//                {
//                    upperBoundRequirement.addSubRequirement(subRequirement);
//                }
//            }
//
//            // End the method now, since all sub requirements will have been processed.
//            return valueRequirement;
//        }
//        else if (ISOConstants.I_ELEMENT_PRESCRIBED_CURRENCY.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.PRESCRIBED_CURRENCY_TYPE);
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_CURRENCY_REF));
//        }
//        else if (ISOConstants.I_ELEMENT_PRESCRIBED_QUALIFIER_OF_MEASURE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.QUALIFIER_OF_MEASURE);
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_QUALIFIER_REF));
//        }
//        else if (ISOConstants.I_ELEMENT_PRESCRIBED_UNIT_OF_MEASURE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.UNIT_OF_MEASURE);
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_UOM_REF));
//        }
//        else if (ISOConstants.I_ELEMENT_RATIONAL_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.RATIONAL_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_REAL_FORMAT.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_PATTERN));
//        }
//        else if (ISOConstants.I_ELEMENT_REAL_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.REAL_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_SEQUENCE_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            ValueRequirement lowerBoundRequirement;
//            ValueRequirement upperBoundRequirement;
//            String upperBound;
//            String lowerBound;
//
//            valueRequirement.setRequirementType(RequirementType.SEQUENCE_TYPE);
//
//            lowerBound = dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_LOWER_BOUND);
//            lowerBound = lowerBound != null ? lowerBound.trim() : "1";
//            if (lowerBound.length() == 0) lowerBound = "1";
//            upperBound = dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_UPPER_BOUND);
//            upperBound = upperBound != null ? upperBound.trim() : "*";
//            if (upperBound.length() == 0) upperBound = "*";
//        }
//        else if (ISOConstants.I_ELEMENT_SET_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            ValueRequirement lowerBoundRequirement;
//            ValueRequirement upperBoundRequirement;
//            String upperBound;
//            String lowerBound;
//
//            valueRequirement.setRequirementType(RequirementType.SET_TYPE);
//
//            lowerBound = dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_LOWER_BOUND);
//            lowerBound = lowerBound != null ? lowerBound.trim() : "1";
//            if (lowerBound.length() == 0) lowerBound = "1";
//            upperBound = dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_UPPER_BOUND);
//            upperBound = upperBound != null ? upperBound.trim() : "*";
//            if (upperBound.length() == 0) upperBound = "*";
//        }
//        else if (ISOConstants.I_ELEMENT_STRING_FORMAT.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_PATTERN));
//        }
//        else if (ISOConstants.I_ELEMENT_STRING_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.STRING_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_TIME_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.TIME_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_VALUE_OF_PROPERTY.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.ONTOLOGY_CLASS);
//            valueRequirement.setValue(dataTypeElement.getAttribute(ISOConstants.I_ATTRIBUTE_VALUE_REF));
//        }
//        else if (ISOConstants.I_ELEMENT_YEAR_MONTH_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.YEAR_MONTH_TYPE);
//        }
//        else if (ISOConstants.I_ELEMENT_YEAR_TYPE.equalsIgnoreCase(dataTypeName))
//        {
//            valueRequirement.setRequirementType(RequirementType.YEAR_TYPE);
//        }
//        else return null;
//
//        // Try to parse sub requirements from any available child elements.
//        for (Element childElement : childElements)
//        {
//            ValueRequirement subRequirement;
//
//            subRequirement = createValueRequirementFromElement(childElement);
//            if (subRequirement != null)
//            {
//                valueRequirement.addSubRequirement(subRequirement);
//            }
//        }
//
//        // Return the newly created ValueRequirement.
//        return valueRequirement;
        return null;
    }

    private void addClassToElement(Element parentElement, SectionRequirement classRequirement)
    {
        ArrayList<PropertyRequirement> propertyRequirements;
        Element classElement;

        classElement = IXMLHandler.addItemElement(parentElement.getOwnerDocument(), classRequirement.getConceptID(), null);
        propertyRequirements = classRequirement.getPropertyRequirements();
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            addPropertyToElement(classElement, propertyRequirement);
        }
    }

    private void addPropertyToElement(Element classElement, PropertyRequirement propertyRequirement)
    {
        Element propertyElement;
        ValueRequirement valueRequirement;

        propertyElement = IXMLHandler.addPropertyElement(classElement, propertyRequirement.getConceptID(), propertyRequirement.isCharacteristic());
        valueRequirement = propertyRequirement.getValueRequirement();
        if (valueRequirement != null)
        {
            addValueToElement(propertyElement, valueRequirement);
        }
    }

    private void addValueToElement(Element parentElement, ValueRequirement valueRequirement)
    {
        ArrayList<ValueRequirement> subRequirements;
        Document parentDocument;
        String value;
        RequirementType requirementType;
        Element newElement;

        parentDocument = parentElement.getOwnerDocument();
        subRequirements = valueRequirement.getSubRequirements();
        value = valueRequirement.getValue();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType.equals(RequirementType.BAG_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_BAG_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.BOOLEAN_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_BOOLEAN_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.CHOICE_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_CHOICE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.COMPLEX_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_COMPLEX_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.COMPOSITE_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_COMPOSITE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.CONTROLLED_CONCEPT))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_CONTROLLED_VALUE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.CURRENCY_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_CURRENCY_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.DATE_TIME_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_DATE_TIME_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.DATE_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_DATE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.FIELD_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_FIELD);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            newElement.setAttribute(ISOConstants.I_ATTRIBUTE_PROPERTY_REF, valueRequirement.getValue());
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.FILE_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_FILE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.INTEGER_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_INTEGER_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.DOCUMENT_REFERENCE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_ITEM_REFERENCE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.LOCALIZED_TEXT_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_LOCALIZED_TEXT_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.LOWER_BOUND_NUMBER))
        {
            // Do nothing, this type does not curently exist in the i-XML schema, so just skip this and add its child types.
            newElement = parentElement;
        }
        else if (requirementType.equals(RequirementType.MEASURED_RANGE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_MEASURE_RANGE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.MEASURED_NUMBER))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_MEASURE_NUMBER_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.PRESCRIBED_CURRENCY_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_PRESCRIBED_CURRENCY);
            newElement.setAttribute(ISOConstants.I_ATTRIBUTE_CURRENCY_REF, value);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.QUALIFIER_OF_MEASURE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_PRESCRIBED_QUALIFIER_OF_MEASURE);
            newElement.setAttribute(ISOConstants.I_ATTRIBUTE_QUALIFIER_REF, value);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.UNIT_OF_MEASURE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_PRESCRIBED_UNIT_OF_MEASURE);
            newElement.setAttribute(ISOConstants.I_ATTRIBUTE_UOM_REF, value);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.RATIONAL_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_RATIONAL_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.REAL_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_REAL_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.SEQUENCE_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_SEQUENCE_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.SET_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_SET_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.STRING_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_STRING_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.TIME_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_TIME_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.UPPER_BOUND_NUMBER))
        {
            // Do nothing, this type does not curently exist in the i-XML schema, so just skip this and add its child types.
            newElement = parentElement;

            // Also end this method here.  By this time, the content of the
            // lower bound will already have been added to the element, so no
            // need to add the upper bound content also.
            // This is done to accomodate a small difference between i-XML
            // structure and database model.
            return;
        }
        else if (requirementType.equals(RequirementType.ONTOLOGY_CLASS))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_VALUE_OF_PROPERTY);
            newElement.setAttribute(ISOConstants.I_ATTRIBUTE_VALUE_REF, value);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.YEAR_MONTH_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_YEAR_MONTH_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else if (requirementType.equals(RequirementType.YEAR_TYPE))
        {
            newElement = parentDocument.createElementNS(ISOConstants.DATA_TYPE_NAMESPACE, ISOConstants.I_ELEMENT_YEAR_TYPE);
            newElement.setPrefix(ISOConstants.DATA_TYPE_NAMESPACE_PREFIX);
            DOMHandler.setElementTextContent(newElement, value);
            parentElement.appendChild(newElement);
        }
        else throw new RuntimeException("Invalid Data Type Code encountered: " + requirementType);

        // Add all sub values to the new element.
        for (ValueRequirement subRequirement : subRequirements)
        {
            this.addValueToElement(newElement, subRequirement);
        }
    }
}
