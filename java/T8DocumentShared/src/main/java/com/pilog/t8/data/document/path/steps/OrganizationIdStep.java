package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class OrganizationIdStep extends DefaultPathStep implements PathStep
{
    public OrganizationIdStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, ontologyProvider, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            List<T8OntologyLink> links;
            DataRecord dataRecord;

            dataRecord = (DataRecord)input;
            links = ontologyProvider.getOntologyLinks(null, dataRecord.getID());
            return evaluateStep(links);
        }
        else if (input instanceof DataRequirementInstance)
        {
            List<T8OntologyLink> links;
            DataRequirementInstance drInstance;

            drInstance = (DataRequirementInstance)input;
            links = ontologyProvider.getOntologyLinks(null, drInstance.getConceptID());
            return evaluateStep(links);
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(List<T8OntologyLink> links)
    {
        List<String> orgIDList;

        // Create a list to hold the organization ID's.
        orgIDList = new ArrayList<String>();

        // Add all organization ID's from the links to the result list.
        for (T8OntologyLink link : links)
        {
            orgIDList.add(link.getOrganizationID());
        }

        // Return the result list.
        return orgIDList;
    }
}
