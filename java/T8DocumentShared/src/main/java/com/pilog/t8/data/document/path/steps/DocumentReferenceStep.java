package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceStep extends DefaultPathStep implements PathStep
{
    public DocumentReferenceStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DocumentReferenceList)
        {
            return evaluateStep(((DocumentReferenceList)input).getReferenceIds());
        }
        else if (input instanceof DataRecord)
        {
            return evaluateStep(rootInput, ((DataRecord)input).getRecordProperties());
        }
        else if (input instanceof RecordProperty)
        {
            RecordValue propertyValue;

            propertyValue = ((RecordProperty)input).getRecordValue();
            if (propertyValue == null)
            {
                return new ArrayList<>();
            }
            else if (propertyValue instanceof DocumentReferenceList)
            {
                return evaluateStep(((DocumentReferenceList)propertyValue).getReferenceIds());
            }
            else if (propertyValue.isComposite())
            {
                return evaluateStep(rootInput, propertyValue.getDescendantValues(RequirementType.DOCUMENT_REFERENCE, null, null));
            }
            else return new ArrayList<>(); // If this property contains no references, just return an empty list.
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Reference step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(List<String> references)
    {
        // No filtering or evaluations to be done.
        return references;
    }
}
