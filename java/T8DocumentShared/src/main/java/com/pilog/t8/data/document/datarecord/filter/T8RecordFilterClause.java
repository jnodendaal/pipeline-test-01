package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterClause;

/**
 * @author Bouwer du Preez
 */
public interface T8RecordFilterClause extends T8DataFilterClause
{
    // No methods yet.  At this stage still a placeholder/marker interface.
}
