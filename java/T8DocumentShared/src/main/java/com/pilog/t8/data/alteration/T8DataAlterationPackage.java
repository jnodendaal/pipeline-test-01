package com.pilog.t8.data.alteration;

import com.pilog.json.JsonObject;
import java.io.Serializable;

/**
 * This class implements a data alteration package.
 * - A data alteration package can contain a number of sequential steps, each one called an alteration {@code T8DataAlteration}.
 * - A data alteration package has an index that is maintained on the server and which contains details of the steps in the package.
 * 
 * @author Bouwer du Preez
 */
public class T8DataAlterationPackage implements Serializable
{
    private String iid; // The Instance of the alteration package, used as primary key to tie all alterations in a single package together.
    private String id; // A meta id used to identify the type of the alteration package.  Each alteration package has its own business logic within the system.
    private String version; // Each package instance will have a version.  Versions are assigned by the source or originator of the packge (SOURCE_ID, SOURCE_IID).
    private String versionRequired; // The package version that must be applied before this version can be applied (optional).
    private String sourceId; // A meta id representing the source or originator of this package.
    private String sourceIid; // An optional instance id applicable if more than one instance of the specified source exists.
    private String displayName; // The display name of this package instance.
    private String description; // The description of this package instance.
    private int appliedStep; // The last step in this package that has already been applied (-1 indicates no steps have been applied).

    public T8DataAlterationPackage()
    {
    }
    
    public T8DataAlterationPackage(JsonObject alterationObject)
    {
        this.iid = alterationObject.getString("packageIid");
        this.id = alterationObject.getString("packageId");
        this.version = alterationObject.getString("version");
        this.versionRequired = alterationObject.getString("versionRequired");
        this.sourceId = alterationObject.getString("sourceId");
        this.sourceIid = alterationObject.getString("sourceIid");
        this.displayName = alterationObject.getString("displayName");
        this.description = alterationObject.getString("description");
        this.appliedStep = alterationObject.getInteger("appliedStep");
    }
    
    public String getIid()
    {
        return iid;
    }

    public void setIid(String iid)
    {
        this.iid = iid;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getVersionRequired()
    {
        return versionRequired;
    }

    public void setVersionRequired(String versionRequired)
    {
        this.versionRequired = versionRequired;
    }

    public String getSourceId()
    {
        return sourceId;
    }

    public void setSourceId(String sourceId)
    {
        this.sourceId = sourceId;
    }

    public String getSourceIid()
    {
        return sourceIid;
    }

    public void setSourceIid(String sourceIid)
    {
        this.sourceIid = sourceIid;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getAppliedStep()
    {
        return appliedStep;
    }

    public void setAppliedStep(int appliedStep)
    {
        this.appliedStep = appliedStep;
    }

    @Override
    public String toString()
    {
        return "T8DataAlterationPackage{" + "iid=" + iid + ", id=" + id + ", version=" + version + ", sourceId=" + sourceId + ", sourceIid=" + sourceIid + ", displayName=" + displayName + ", appliedStep=" + appliedStep + '}';
    }
    
    public JsonObject serialize()
    {
        JsonObject alterationPackageObject;

        // Create the alteration package object.
        alterationPackageObject = new JsonObject();
        alterationPackageObject.add("packageIid", iid);
        alterationPackageObject.add("packageId", id);
        alterationPackageObject.add("version", version);
        alterationPackageObject.add("versionRequired", versionRequired);
        alterationPackageObject.add("sourceId", sourceId);
        alterationPackageObject.add("sourceIid", sourceIid);
        alterationPackageObject.add("displayName", displayName);
        alterationPackageObject.add("description", description);
        alterationPackageObject.add("appliedStep", appliedStep);

        // Return the serialized version of this alteration package.
        return alterationPackageObject;
    }
}
