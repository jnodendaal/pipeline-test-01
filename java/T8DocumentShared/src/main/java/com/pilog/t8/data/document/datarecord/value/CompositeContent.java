package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class CompositeContent extends ValueContent
{
    private final List<FieldContent> fields;

    public CompositeContent()
    {
        super(RequirementType.COMPOSITE_TYPE);
        this.fields = new ArrayList<FieldContent>();
    }

    public List<FieldContent> getFields()
    {
        return new ArrayList<FieldContent>(fields);
    }

    public FieldContent getField(String fieldId)
    {
        for (FieldContent field : fields)
        {
            if (field.getId().equals(fieldId))
            {
                return field;
            }
        }

        return null;
    }

    public void addField(FieldContent field)
    {
        fields.add(field);
    }

    public FieldContent addField(String fieldId)
    {
        FieldContent newField;

        newField = new FieldContent(fieldId);
        fields.add(newField);
        return newField;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof CompositeContent)
        {
            CompositeContent toCompositeContent;

            toCompositeContent = (CompositeContent)toContent;
            for (FieldContent field : fields)
            {
                FieldContent toFieldContent;

                toFieldContent = toCompositeContent.getField(field.getId());
                if (toFieldContent != null)
                {
                    if (!field.isEqualTo(toFieldContent)) return false;
                }
                else return false;
            }

            return true;
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof CompositeContent)
        {
            CompositeContent toCompositeContent;

            toCompositeContent = (CompositeContent)toContent;
            for (FieldContent field : fields)
            {
                FieldContent toFieldContent;

                toFieldContent = toCompositeContent.getField(field.getId());
                if (toFieldContent != null)
                {
                    if (!field.isEqualTo(toFieldContent)) return false;
                }
                else return false;
            }

            return true;
        }
        else return false;
    }
}
