package com.pilog.t8.data.document.datarecord.access.scripted;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.access.layer.ClassificationAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.definition.data.document.datarecord.access.scripted.T8DataRecordAccessScriptDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.scripted.T8ScriptedDataRecordAccessDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptedDataRecordAccessHandler implements T8DataRecordAccessHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ScriptedDataRecordAccessHandler.class);

    private T8Context context;
    private final T8ScriptedDataRecordAccessDefinition definition;
    private final List<T8DataRecordAccessScript> initialStateScripts;
    private final List<T8DataRecordAccessScript> staticStateScripts;
    private final List<T8DataRecordAccessScript> dynamicStateScripts;
    private final List<T8DataRecordAccessScript> validationScripts;

    public T8ScriptedDataRecordAccessHandler(T8Context context, T8ScriptedDataRecordAccessDefinition definition)
    {
        this.context = context;
        this.definition = definition;
        this.initialStateScripts = new ArrayList<>();
        this.staticStateScripts = new ArrayList<>();
        this.dynamicStateScripts = new ArrayList<>();
        this.validationScripts = new ArrayList<>();
        initScripts();
    }

    private void initScripts()
    {
        // Prepare initial state scripts.
        for (T8DataRecordAccessScriptDefinition scriptDefinition : definition.getIncludedInitialStateScripts())
        {
            try
            {
                initialStateScripts.add(scriptDefinition.createScript(context));
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while preparing data record access script " + scriptDefinition + " in access layer " + definition, e);
            }
        }

        // Prepare static state scripts.
        for (T8DataRecordAccessScriptDefinition scriptDefinition : definition.getIncludedStaticStateScripts())
        {
            try
            {
                staticStateScripts.add(scriptDefinition.createScript(context));
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while preparing data record access script " + scriptDefinition + " in access layer " + definition, e);
            }
        }

        // Prepare dynamic state scripts.
        for (T8DataRecordAccessScriptDefinition scriptDefinition : definition.getIncludedDynamicStateScripts())
        {
            try
            {
                dynamicStateScripts.add(scriptDefinition.createScript(context));
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while preparing data record access script " + scriptDefinition + " in access layer " + definition, e);
            }
        }

        // Prepare validation scripts.
        for (T8DataRecordAccessScriptDefinition scriptDefinition : definition.getIncludedValidationScripts())
        {
            try
            {
                validationScripts.add(scriptDefinition.createScript(context));
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while preparing data record access script " + scriptDefinition + " in access layer " + definition, e);
            }
        }
    }

    @Override
    public String getIdentifier()
    {
        return definition.getIdentifier();
    }

    @Override
    public void setInitialState(DataRecord dataRecord)
    {
        for (T8DataRecordAccessScript script : initialStateScripts)
        {
            script.applyScript(dataRecord);
        }
    }

    @Override
    public void setStaticState(DataRecord dataRecord)
    {
        List<String> fileStructureIDList;

        // Apply the static logic.
        for (T8DataRecordAccessScript script : staticStateScripts)
        {
            script.applyScript(dataRecord);
        }

        // Set the accessability of the file.
        fileStructureIDList = definition.getFileDrIids();
        if ((fileStructureIDList != null) && (!fileStructureIDList.isEmpty()))
        {
            for (DataRecord record : dataRecord.getDataFileRecords())
            {
                DataRecord dataFile;

                dataFile = record.getDataFile();
                if (dataFile == null) throw new RuntimeException("No data file found for record: " + record);
                if (!fileStructureIDList.contains(dataFile.getDataRequirementInstanceID()))
                {
                    record.getAccessLayer().setEditable(false);
                    record.getAccessLayer().setDescendentsRequired(false);
                }
            }
        }
    }

    @Override
    public void refreshDynamicState(DataRecord dataRecord)
    {
        List<String> fileStructureIDList;

        // Apply the dynamic logic.
        for (T8DataRecordAccessScript script : dynamicStateScripts)
        {
            script.applyScript(dataRecord);
        }

        // Set the accessability of the file.
        fileStructureIDList = definition.getFileDrIids();
        if ((fileStructureIDList != null) && (!fileStructureIDList.isEmpty()))
        {
            for (DataRecord record : dataRecord.getDataRecords())
            {
                DataRecord dataFile;

                dataFile = record.getDataFile();
                if (dataFile == null) throw new RuntimeException("No data file found for record: " + record);
                if (!fileStructureIDList.contains(record.getDataFile().getDataRequirementInstanceID()))
                {
                    record.getAccessLayer().setEditable(false);
                    record.getAccessLayer().setDescendentsRequired(false);
                }
            }
        }
    }

    @Override
    public T8DataFileValidationReport validateState(DataRecord dataRecord)
    {
        T8DataFileValidationReport validationReport;
        LinkedList<DataRecord> recordQueue;

        // Create a new validation report.
        validationReport = new T8DataFileValidationReport();

        // Create a queue to hold all records to be validated.
        recordQueue = new LinkedList<DataRecord>();
        recordQueue.addAll(dataRecord.getDataFileRecords());

        // Check that all required properties/fields have values.
        while(recordQueue.size() > 0)
        {
            DataRecord nextDataRecord;

            // Get the next record from the queue and set it on the access handler.
            nextDataRecord = recordQueue.pop();

            // Validate the record.
            for (PropertyRequirement propertyRequirement : nextDataRecord.getDataRequirement().getPropertyRequirements())
            {
                PropertyAccessLayer propertyAccessLayer;
                ValueRequirement valueRequirement;

                // Get the applicable property access layer.
                propertyAccessLayer = nextDataRecord.getAccessLayer().getPropertyAccessLayer(propertyRequirement.getConceptID());
                valueRequirement = propertyRequirement.getValueRequirement();
                if (valueRequirement != null)
                {
                    RequirementType requirementType;

                    requirementType = valueRequirement.getRequirementType();
                    if (requirementType == RequirementType.COMPOSITE_TYPE)
                    {
                        for (ValueRequirement fieldRequirement : valueRequirement.getSubRequirements(RequirementType.FIELD_TYPE, null, null))
                        {
                            FieldAccessLayer fieldAccessLayer;

                            // If the field is required, check that it has a value.
                            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(fieldRequirement.getFieldID());
                            if (fieldAccessLayer.isRequired())
                            {
                                RecordProperty requiredProperty;
                                String requiredPropertyID;
                                String requiredFieldID;

                                // Get the required property from the class and if it is empty or has no content, add a validation error to the report.
                                requiredFieldID = fieldRequirement.getValue();
                                requiredPropertyID = propertyRequirement.getConceptID();
                                requiredProperty = nextDataRecord.getRecordProperty(requiredPropertyID);
                                if ((requiredProperty == null) || (!requiredProperty.hasContent(false)))
                                {
                                    validationReport.addRecordValueError(nextDataRecord, requiredPropertyID, requiredFieldID, "Required field has no value.");
                                }
                                else
                                {
                                    RecordValue requiredField;

                                    requiredField = requiredProperty.getDescendantValue(RequirementType.FIELD_TYPE, requiredFieldID, null);
                                    if ((requiredField == null) || (!requiredField.hasContent(false)))
                                    {
                                        validationReport.addRecordValueError(nextDataRecord, requiredPropertyID, requiredFieldID, "Required field has no value.");
                                    }
                                    else
                                    {
                                        // Add all records (if any) referenced by the required property to the record queue for validation.
                                        recordQueue.addAll(nextDataRecord.getSubRecordsReferencedFromProperty(requiredPropertyID));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // If the property is required, check that it has a value.
                        if (propertyAccessLayer.isRequired())
                        {
                            RecordProperty requiredProperty;
                            String requiredPropertyID;

                            // Get the required property from the class and if it is empty or has no content, add a validation error to the report.
                            requiredPropertyID = propertyRequirement.getConceptID();
                            requiredProperty = nextDataRecord.getRecordProperty(requiredPropertyID);
                            if ((requiredProperty == null) || (!requiredProperty.hasContent(false)))
                            {
                                validationReport.addRecordValueError(nextDataRecord, requiredPropertyID, null, "Required property has no value.");
                            }
                            else
                            {
                                // Add all records (if any) referenced by the required property to the record queue for validation.
                                recordQueue.addAll(nextDataRecord.getSubRecordsReferencedFromProperty(requiredPropertyID));
                            }
                        }
                    }
                }
            }

            // Validate the record classification links.
            for (ClassificationAccessLayer classificationAccessLayer : nextDataRecord.getAccessLayer().getClassificationAccessLayers())
            {
                Set<String> ocIdSet;
                String osId;
                boolean classFound;
                boolean structureFound;

                // Get the classification specifications from the access layer.
                osId = classificationAccessLayer.getOntologyStructureID();
                ocIdSet = classificationAccessLayer.getFilterOntologyClassIDSet();

                // Make sure that the record contains the required information.
                classFound = false;
                structureFound = false;
                for (T8OntologyLink link : nextDataRecord.getOntologyLinks())
                {
                    // If the structure id mathes, do the next check.
                    if (osId.equals(link.getOntologyStructureID()))
                    {
                        // If the class also matches, we can continue to the next classification check.
                        structureFound = true;
                        if (ocIdSet.contains(link.getOntologyClassID()))
                        {
                            // Check passed, continue with the next one.
                            classFound = true;
                            break;
                        }
                    }
                }

                if (classificationAccessLayer.isRequired())
                {
                    // The check was not passed as not matching link was found.
                    if (!structureFound)
                    {
                        validationReport.addRecordClassificationError(nextDataRecord, osId, null, "Classification link required.");
                    }
                    else if (!classFound)
                    {
                        validationReport.addRecordClassificationError(nextDataRecord, osId, null, "Invalid classification link.");
                    }
                }
                else
                {
                    // Even if the classification is not required, if we found an invalid link we need to raise the error.
                    if ((structureFound) && (!classFound))
                    {
                        validationReport.addRecordClassificationError(nextDataRecord, osId, null, "Invalid classification link.");
                    }
                }
            }
        }

        // Execute all validation logic.
        for (T8DataRecordAccessScript script : validationScripts)
        {
            validationReport.addValidationReport(script.applyScript(dataRecord));
        }

        // Return the final report.
        return validationReport;
    }
}
